<com:TActivePanel id="bloc_etapeIdentification">

<div  class="form-field" >
	<script type="text/javascript">
		function returnLieuxExecution(idSelectedGeo,numSelectedGeoN2)
		{
			document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_idsSelectedGeoN2').value = idSelectedGeo;
			document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_numSelectedGeoN2').value = numSelectedGeoN2;
			document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_selectedGeoN2').click();
		}
	</script>

<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<div class="content">
	<h3 class="float-left"><com:TTranslate>IDENTIF_CONS</com:TTranslate></h3>
	<div class="float-right margin-fix"><com:TTranslate>TEXT_LE_SYMBOLE</com:TTranslate><span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
	<div class="breaker"></div>
    <com:ActivePanelMessageAvertissement ID="activeInfoMsg" Message="<%=Prado::localize('MSG_INFO_PROCEDURE_REPONDANT_SERVICE_MPS')%>" Visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? true:false %>" />
    <com:ActivePanelMessageAvertissement ID="activeInfoMsgDume" Message="<%=Prado::localize('MSG_INFO_PROCEDURE_DUME')%>" />
    <com:ActivePanelMessageAvertissement ID="activeInfoMsgMarcheSubsequent"
										 Message="<%=Prado::localize('MSG_INFO_MARCHE_SUBSEQUENT')%>"
										 DisplayStyle="None"></com:ActivePanelMessageAvertissement>
	<com:ActivePanelMessageAvertissement ID="activeInfoMsgMarcheSpecifique"
										 Message="<%=Prado::localize('MSG_INFO_MARCHE_SPECIFIQUE')%>"
										 DisplayStyle="None"></com:ActivePanelMessageAvertissement>


	<div class="breaker"></div>

    <com:TActivePanel id="panelRedac" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !(Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
		<div class="spacer"></div>
            <!-- Debut bloc : Je souhaite rédiger les pièces administratives de cette consultation depuis cette plateforme -->
            <com:TActivePanel Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem')%>" cssClass="line">
            <div Class="panel panel-default with-bg top-panel">
                <div class="intitule col-535"><com:TTranslate>DEFINE_MESSAGE_JE_SOUHAITE_REDIGER_LES_PIECES_ADMINISTRATIVES_DE_CETTE_CONSULTATION_DEPUIS_CETTE_PF</com:TTranslate> :</div>
                <div class="content-bloc">
                    <com:TActivePanel id="panelintituleRedactionOui" cssclass="intitule-60">
                        <com:TActiveRadioButton GroupName="intituleRedaction" id="intituleRedactionOui" cssClass="radio" Attributes.onclick="actionChoixPubliciteRedac('<%=$this->intituleRedactionOui->getClientId()%>');cocherDecocherBoutonDonneesOrmeOui('<%=$this->intituleRedactionOui->getClientId()%>');"/>
                        <label for="intituleRedactionOui"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
                    </com:TActivePanel>
                    <com:TActivePanel id="panelintituleRedactionNon" cssclass="intitule-auto">
                        <com:TActiveRadioButton GroupName="intituleRedaction" id="intituleRedactionNon" cssClass="radio" Attributes.onclick="actionChoixPubliciteRedac('<%=$this->intituleRedactionNon->getClientId()%>');cocherDecocherBoutonDonneesOrmeOui('<%=$this->intituleRedactionNon->getClientId()%>');"/>
                        <label for="intituleRedactionNon"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
                    </com:TActivePanel>
				</div>
            </div>
		</com:TActivePanel>
		<!-- Fin bloc : Je souhaite rédiger les pièces administratives de cette consultation depuis cette plateforme -->

		<div class="spacer"></div>
    </com:TActivePanel>

	<com:TPanel cssclass="line" id="panelNumeroProjetAchat" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('NumeroProjetAchat')%>">
		<div class="intitule col-150"><label for="numeroProjetAchatType"><com:TTranslate>DEFINE_FULL_NUMERO_PROJET_ACHAT</com:TTranslate></label> :</div>
		<div class="content-bloc"><com:TLabel ID="numProjetAchat"/></div>

	</com:TPanel>
	<div class="spacer"></div>
	<div class="line">
		<div class="intitule col-150"><label for="intituleAvis"><com:TTranslate>TYPE_ANNONCE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TDropDownList
			id="intituleAvis"
			Enabled="false"
			CssClass="moyen"
			Attributes.title="<%=Prado::localize('TYPE_ANNONCE')%>" >
    	</com:TDropDownList>
	</div>
	<!--Fin Ligne Type d'annonce-->
	<!--Debut Ligne Type de contrat-->
	<com:TPanel cssclass="line" id="panelTypeContrat">
		<div class="intitule col-150"><label for="contratType"><com:TTranslate>DEFINE_TYPE_CONTRAT</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc">
			<com:TActiveDropDownList
				id="contratType"
				CssClass="moyen float-left"
				Attributes.title="<%=Prado::localize('DEFINE_TYPE_CONTRAT')%>"
				OnTextChanged="changeTypeContrat"
				Enabled="<%=($this->getConsultation() instanceof Application\Propel\Mpe\CommonConsultation && $this->getConsultation()->getId())? false:true%>"
					>
				<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
				<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
				<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
    		</com:TActiveDropDownList>
			<div class="col-info-bulle">
				<img title="Info-bulle" alt="Info-bulle" class="picto-info" onmouseout="cacheBulle('infosContrat')" onmouseover="afficheBulle('infosContrat', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
				<div id="infosContrat" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>DEFINE_INFORMATION_INDICATIVE_FIN_STATISTIQUES</com:TTranslate></div></div>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    		<com:TCompareValidator  ControlToValidate="contratType"  ValueToCompare="0"  DataType="String" Operator="NotEqual" ValidationGroup="validateSave" Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DEFINE_TYPE_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
				>
				<prop:ClientSide.OnValidationError>
        			document.getElementById('divValidationSummarySave').style.display='';
        			document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
        			document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
       	    		document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
       	    		document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
        		</prop:ClientSide.OnValidationError>
       	     </com:TCompareValidator>
       	     <com:TCompareValidator  ControlToValidate="contratType"  ValueToCompare="0"  DataType="String" Operator="NotEqual" ValidationGroup="validateInfosConsultation" Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DEFINE_TYPE_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
				>
				<prop:ClientSide.OnValidationError>
        			document.getElementById('divValidationSummary').style.display='';
        			document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
        			document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
       	    		document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
       	    		document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
        		</prop:ClientSide.OnValidationError>
       	     </com:TCompareValidator>
		</div>
	</com:TPanel>
	<!-- Debut du numero AC -->
	<com:TActivePanel cssClass="line" id="panelNumAC" >
		<div class="intitule col-150"><label for="numeroAC"><com:TActiveLabel ID="labelObjetMarche" /></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc">
			<com:TJuiAutoComplete
					Enabled="<%=($this->getConsultation() instanceof Application\Propel\Mpe\CommonConsultation && $this->getConsultation()->getId())? false:true%>"
					ID="numeroAC"
					OnSuggest="suggestNumborsAC"
					Suggestions.DataKeyField="id"
					ResultPanel.CssClass="acomplete"
					cssClass="moyen float-left"
					MinChars="3"
					OnSuggestionSelected="suggestionNumborsACSelected"
					>

				<prop:Suggestions.ItemTemplate>
					<li><%#$this->Data['numero']%> - <%#$this->Data['objet']%></li>
				</prop:Suggestions.ItemTemplate>

			</com:TJuiAutoComplete>

			<com:TCustomValidator
					Id="validatorNumAcValidateInfosConsultation"
					ValidationGroup="validateInfosConsultation"
					ControlToValidate="marcheSubsequent"
					Display="Dynamic"
					ClientValidationFunction="validateNumeroAC"
					ErrorMessage="<%=Prado::localize('MSG_VALIDATION_MARCHE_SPECIFIQUE_SUBSEQUENT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TActiveHiddenField ID="numeroACValue" />
			<com:TActiveHiddenField ID="idContratSelected" />
			<com:TActiveHiddenField ID="marcheSubsequent" />
			<com:TCustomValidator
					Id="validatorNumAc"
					ValidationGroup="validateSave"
					ControlToValidate="marcheSubsequent"
					Display="Dynamic"
					ClientValidationFunction="validateNumeroAC"
					ErrorMessage="<%=Prado::localize('MSG_VALIDATION_MARCHE_SPECIFIQUE_SUBSEQUENT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>

		</div>
	</com:TActivePanel>
	<!-- Fin du numero AC -->


	<com:TPanel cssclass="line" id="panelEntiteAdjudicatrice">
		<div class="intitule col-150"><label for="contratType"></label>&nbsp;</div>
		<div class="content-bloc">
			<div class="check-line">
				<com:TActiveCheckBox id="entiteAdjudicatrice" Attributes.title="<%=Prado::localize('DEFINE_CONSULTATION_PASSEE_EN_TANT_QUE_ENTITE_ADJUDICATRICE')%>"></com:TActiveCheckBox>
				<label for="entiteAdjudicatrice"><com:TTranslate>DEFINE_CONSULTATION_PASSEE_EN_TANT_QUE_ENTITE_ADJUDICATRICE</com:TTranslate></label>
			</div>
			<div class="spacer-small"></div>
		</div>
	</com:TPanel>
	<!--Fin Ligne Type de contrat-->
	<!--Debut Ligne Mode passation -->
	<com:TPanel cssClass="line" ID="panelModePassation">
		<div class="intitule-150">&nbsp;</div>
		<div class="content-bloc bloc-600">
           <div class="line">
            <com:TRadioButton
			cssClass="radio"
			GroupName="modePassation"
			ID="surOffrePrix"
			Text="<%=Prado::localize('SUR_OFFRE_DE_PRIX')%>"
			Attributes.title="<%=Prado::localize('SUR_OFFRE_DE_PRIX')%>"/>
        </div>
		<div class="line">
              <com:TRadioButton
				cssClass="radio"
				GroupName="modePassation"
				ID="auRabais"
				Text="<%=Prado::localize('AU_RABAIS')%>"
				Attributes.title="<%=Prado::localize('AU_RABAIS')%>"/>
        </div>
        </div>
	</com:TPanel>
	<!--Fin Ligne Mode passation-->

	<!--Debut Ligne Categorie principale-->
	<div class="line">
		<div class="intitule col-150"><label for="categorie"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPALE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveDropDownList id="categorie" CssClass="moyen champ-pub cpvCategoryCheck" Attributes.title="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPALE')%>"></com:TActiveDropDownList>
		<com:TCompareValidator
			ControlToValidate="categorie"
            ValueToCompare="0"
            DataType="String"
            Operator="NotEqual"
			ValidationGroup="validateInfosConsultation"
			Display="Dynamic"
			ErrorMessage="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPALE')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummary').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TCompareValidator>
        <com:TCompareValidator
			ControlToValidate="categorie"
            ValueToCompare="0"
            DataType="String"
            Operator="NotEqual"
			ValidationGroup="validateSave"
			Display="Dynamic"
			ErrorMessage="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPALE')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummarySave').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TCompareValidator>
	</div>
	<!--Fin Ligne Categorie principale-->

	<!-- Debut Valeur estimee du contrat -->
	<com:TPanel cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AfficherValeurEstimee') && Application\Service\Atexo\Atexo_Module::isEnabled('Publicite'))%>">
		<div class="intitule col-150"><label for="montantEstimatif"><com:TTranslate>DEFINE_MONTANT_ESTIMATIF_CONTRAT</com:TTranslate><span class="champ-oblig">*</span></label> &nbsp;:</div>
		<div class="content-bloc bloc-180 float-left">
			<com:TTextBox id="montantEstimatifMarche" Attributes.onblur="formatterMontant('<%=$this->montantEstimatifMarche->getClientId()%>');" cssClass="long montant" Attributes.title="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"/> <com:TTranslate>DEFINE_EURO_HT</com:TTranslate>
			<com:TCustomValidator
					ClientValidationFunction="validerMontantEstimeMarche"
					ValidationGroup="validateSave"
					ControlToValidate="montantEstimatifMarche"
					Display="Dynamic"
					Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && !(Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>"
					ErrorMessage="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				>
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TCustomValidator
					ClientValidationFunction="validerMontantEstimeMarche"
					ValidationGroup="validateInfosConsultation"
					ControlToValidate="montantEstimatifMarche"
					Display="Dynamic"
					Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires')&& !(Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>"
					ErrorMessage="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				>
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TRequiredFieldValidator
					ValidationGroup="validateSave"
					ControlToValidate="montantEstimatifMarche"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
			<com:TRequiredFieldValidator
					ValidationGroup="validateInfosConsultation"
					ControlToValidate="montantEstimatifMarche"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
		</div>
		<div class="content-bloc bloc-400 float-left">
			<div class="info-aide"><%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite'))? Prado::localize('DEFINE_MESSAGE_PRECISION_MONTANT_ESTIMATIF_CONTRAT_MODULE_PUB_ACTIVE'):Prado::localize('DEFINE_MESSAGE_PRECISION_MONTANT_ESTIMATIF_CONTRAT_MODULE_PUB_DESACTIVE')%></ù></div>
		</div>
	</com:TPanel>
	<!-- Fin Valeur estimee du contrat -->

	<!--Debut Ligne Type de procedure-->
	<div class="line">
		<div class="intitule col-150"><label for="procedureType"><com:TTranslate>TEXT_TYPE_PROCEDURE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc content-bloc-DUME">
			<com:TActiveDropDownList id="procedureType"
									 CssClass="moyen float-left champ-pub"
									 Attributes.title="<%=Prado::localize('TEXT_TYPE_PROCEDURE')%>"
									 OnCallBack="changeProcedureType"
									 Enabled="<%=($this->getConsultation() instanceof Application\Propel\Mpe\CommonConsultation && $this->getConsultation()->getId())? false:true%>"
			>

				<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
				<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
				<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
			</com:TActiveDropDownList>

			<com:TActivePanel cssclass="col-info-bulle indent-10" id="panelmarcheMPS" style="display:<%=!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps') ?'block':'none'%>;">

				<div class="check-line float-left">
					<com:TActiveCheckBox id="marcheMPS" CssClass="check" Attributes.title="<%=Prado::localize('DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE')%>" attributes.onclick="showHideActiveInfoMsg(this, '<%=$this->activeInfoMsg->panelMessage->ClientId%>');"/>
					<label for="marcheMPS"><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-mps-small.png' class="inline-img" alt="<%=Prado::localize('DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE')%>" /> </label>
				</div>
				<img title="Info-bulle" alt="Info-bulle" class="picto-info inline-picto-info" onmouseout="cacheBulle('infos_marcheMPS')" onmouseover="afficheBulle('infos_marcheMPS', this)" src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif' />
				<div id="infos_marcheMPS" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>DEFINE_INFORMATION_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE_AGENT</com:TTranslate></div></div>
			</com:TActivePanel>
			<com:TActivePanel cssclass="col-info-bulle indent-10" id="paneldumeDemande" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>" style="display:none">
				<div class="check-line float-left">
					<com:TActiveCheckBox id="dumeDemande" CssClass="check" Attributes.title="<%=Prado::localize('DUME')%>" autoPostBack="false" attributes.onclick="traitementDumeDemande(this);"/>
					<label for="dumeDemande"><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-dume.png' class="inline-img" alt="<%=Prado::localize('DUME')%>" /> </label>
				</div>
				<img title="Info-bulle" alt="Info-bulle" class="picto-info inline-picto-info" onmouseout="cacheBulle('infos_dumeDemande')" onmouseover="afficheBulle('infos_dumeDemande', this)" src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif' />
				<div id="infos_dumeDemande" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>INFO_BULL_DUME</com:TTranslate></div></div>
			</com:TActivePanel>

			<com:TActivePanel
				id="procedureOpenClosedBlock"
				visible="true"
				cssClass="procedureOpenClosedBlock"
			>
				<div class="check-line float-left">
					<com:TActiveRadioButtonList ID="groupProcedureOpenClosedChoices" OnCallback="Page.bloc_etapeIdentification.procedureOpenClosedSelected" cssClass="radioButtonPrado">
						<com:TListItem Value="1" Text="<%=Prado::localize('PROCEDURE_OUVERTE')%>" Id="groupProcedureOpen" Selected="true"/>
						<com:TListItem Value="0" Text="<%=Prado::localize('PROCEDURE_RESTREINTE')%>" Id="groupProcedureClosed" />
					</com:TActiveRadioButtonList>
				</div>
			</com:TActivePanel>
		</div>
		<com:TCompareValidator  ControlToValidate="procedureType"  ValueToCompare="0"  DataType="String" Operator="NotEqual" ValidationGroup="validateInfosConsultation" Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_TYPE_PROCEDURE')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
		>
			<prop:ClientSide.OnValidationError>
				document.getElementById('divValidationSummary').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
			</prop:ClientSide.OnValidationError>
		</com:TCompareValidator>
		<com:TCompareValidator  ControlToValidate="procedureType"  ValueToCompare="0"  DataType="String" Operator="NotEqual" ValidationGroup="validateSave" Display="Dynamic"
								ErrorMessage="<%=Prado::localize('TEXT_TYPE_PROCEDURE')%>"
								EnableClientScript="true"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
		>
			<prop:ClientSide.OnValidationError>
				document.getElementById('divValidationSummarySave').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
				document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
			</prop:ClientSide.OnValidationError>
		</com:TCompareValidator>

	</div>
	<!--Fin Ligne Type de procedure-->
	<div class="line">
		<com:TActivePanel
				id="buyersBlock"
				visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ConfPubliciteFrancaise')%>"
				style="display:none"
		>
			<div class="intitule col-450">
				<com:TTranslate>GROUP_BUYERS</com:TTranslate> <span class="champ-oblig">*</span>:
			</div>
			<div class="content-bloc-auto">
				<com:TActiveRadioButtonList ID="groupBuyersChoices" OnCallback="Page.bloc_etapeIdentification.groupBuyersSelected" cssClass="radioButtonPrado">
					<com:TListItem Value="1" Id="groupBuyersYes" />
					<com:TListItem Value="0" Id="groupBuyersNo" Selected="true"/>
				</com:TActiveRadioButtonList>
				<com:TRequiredFieldValidator
						ValidationGroup="validateInfosConsultation"
						ControlToValidate="groupBuyersChoices"
						Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
						ErrorMessage="<%=Prado::localize('GROUP_BUYERS')%>"
						Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
						EnableClientScript="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>"
				>
					<prop:ClientSide.OnValidate>
						if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
						sender.enabled = true;
						} else{
						sender.enabled = false;
						}
					</prop:ClientSide.OnValidate>
				</com:TRequiredFieldValidator>
			</div>
		</com:TActivePanel>
	</div>

	<!--D ébut Ligne Type de formulaire DUME-->
	<com:TActivePanel cssclass="line" id="panelDumeTypeFormulaire"  visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>" style="display:none">
		<div class="intitule col-150"><label for="test"><com:TTranslate>TYPE_FORMULAIRE_DUME</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc">
			<com:TActivePanel cssClass="intitule-auto" id="paneltypeFormulaireStandard">
				<com:TActiveRadioButton
						GroupName="typeFormulaireDume"
						id="typeFormulaireStandard"
						CssClass="radio"

				/>
				<label for="typeFormulaireStandard"><com:TTranslate>DEFINE_STANDARD</com:TTranslate></label>
			</com:TActivePanel>
			<com:TActivePanel cssClass="intitule-auto" id="paneltypeFormulaireSimplifie">
				<com:TActiveRadioButton
						GroupName="typeFormulaireDume"
						id="typeFormulaireSimplifie"
						CssClass="radio"
						Attributes.OnClick="return confirmChangeDumeType('<%=$this->dumeA->getClientId()%>', '<%=$this->labelScript2->getClientId()%>')"
				/>
				<label for="typeFormulaireSimplifie"><com:TTranslate>DEFINE_SIMPLIFIE</com:TTranslate></label>
			</com:TActivePanel>
			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosTypeFormulaireDume', this)" onmouseout="cacheBulle('infosTypeFormulaireDume')" class="picto-info-intitule" alt="Info-bulle">
			<com:TCustomValidator
					ValidationGroup="validateSave"
					ControlToValidate="typeFormulaireStandard"
					ClientValidationFunction="ValidateTypeFormulaireDume"
					Display="Dynamic"
					Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>"
					ErrorMessage="<%=Prado::localize('TYPE_FORMULAIRE_DUME')%>"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
		</div>
		<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosTypeFormulaireDume">
			<div><com:TTranslate>INFO_BULL_TYPE_FORMULAIRE_DUME</com:TTranslate></div>
		</div>
	</com:TActivePanel>
	<!--Fin Ligne Type de formulaire DUME-->
	<!--Debut Ligne Type de procedure dume-->

	<com:TActivePanel id="panelTypeProcedureDume" CssClass="line blocNoPadding" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>" style="display:none">
		<div class="line">
			<div class="intitule col-150"><label for="procedureTypeDume"><com:TTranslate>TYPE_PROCEDURE_DUME</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
			<div class="content-bloc">
				<com:TActiveDropDownList id="procedureTypeDume"
										 CssClass="moyen float-left champ-pub"
										 Attributes.title="<%=Prado::localize('TYPE_PROCEDURE_DUME')%>"
										 autoPostBack="false"
				>
				</com:TActiveDropDownList>
				<com:TActiveHiddenField id="paramTypeProcedureDume" />
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosTypeProcedureDume', this)" onmouseout="cacheBulle('infosTypeProcedureDume')" class="picto-info-intitule" alt="Info-bulle">
				<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosTypeProcedureDume">
					<div><com:TTranslate>INFO_BULL_TYPE_PROCEDURE_DUME</com:TTranslate></div>
				</div>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<com:TCustomValidator ValidationGroup="validateInfosConsultation" ControlToValidate="procedureTypeDume" ClientValidationFunction="ValidateTypeProcedureDume" Display="Dynamic" Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution')%>" ErrorMessage="<%=Prado::localize('TYPE_PROCEDURE_DUME')%>" Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TCustomValidator ValidationGroup="validateSave" ControlToValidate="procedureTypeDume" ClientValidationFunction="ValidateTypeProcedureDume" Display="Dynamic" Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution')%>" ErrorMessage="<%=Prado::localize('TYPE_PROCEDURE_DUME')%>" Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
		</div>
	</com:TActivePanel>

	<!--Fin Ligne Type de procedure dume-->
    <com:TPanel Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
		<com:TActiveHiddenField id="mapaProcedureTypeOrg"/>

        <com:TActivePanel
				cssClass="line"
				id="selectPublicationPublicite"
				style="display:none"
				Attributes.onclick="handleGroupRadioRelatedPub('<%= $this->buyersBlock->ClientId %>'); handleGroupRadioRelatedPub('<%= $this->procedureOpenClosedBlock->ClientId %>', '<%= $this->mapaProcedureTypeOrg->Value %>');">
            <div class="line panel-default with-bg top-panel">
            <div class="intitule col-535"><com:TTranslate>DEFINE_MESSAGE_JE_SOUHAITE_PUBLIER_UN_AVIS_DEPUIS_CETTE_PF_POUR_CETTE_CONSULTATION</com:TTranslate> :</div>
            <div id="panelintitulePublicite" class="content-bloc bloc-validation">
                <com:TActivePanel id="panelintitulePubliciteOui" cssclass="intitule-60">
                    <com:TActiveRadioButton
						Text="<%=Prado::localize('DEFINE_OUI')%>"
						GroupName="intitulePublicite"
						id="intitulePubliciteOui"
						cssClass="radio"
						Attributes.onclick="actionChoixPubliciteRedac('<%=$this->intitulePubliciteOui->getClientId()%>');cocherDecocherBoutonDonneesOrmeOui('<%=$this->intitulePubliciteOui->getClientId()%>');"
					/>
                </com:TActivePanel>
                <com:TActivePanel id="panelintitulePubliciteNon" cssclass="intitule-auto">
                    <com:TActiveRadioButton
						Text="<%=Prado::localize('DEFINE_NON')%>"
						GroupName="intitulePublicite"
						id="intitulePubliciteNon"
						cssClass="radio"
						Attributes.onclick="actionChoixPubliciteRedac('<%=$this->intitulePubliciteNon->getClientId()%>');cocherDecocherBoutonDonneesOrmeOui('<%=$this->intitulePubliciteNon->getClientId()%>');"
					/>
                </com:TActivePanel>
            </div>
            </div>
        </com:TActivePanel>
    </com:TPanel>
	<!--Debut Ligne Reference-->
	<div class="line">
		<div class="intitule col-150"><label for="reference"><com:TTranslate>REFERENCE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc">
			<com:TActiveTextBox id="reference" CssClass="moyen float-left" Attributes.title="<%=Prado::localize('REFERENCE')%>"/>
			<com:THiddenField id="referenceConnecteur"/>
			<div class="col-info-bulle">
				<img title="Info-bulle" alt="Info-bulle" class="picto-info" onmouseout="cacheBulle('infosReference')" onmouseover="afficheBulle('infosReference', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
				<div id="infosReference" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div> <%=Application\Service\Atexo\Atexo_Module::isEnabled('AutoriserCaracteresSpeciauxDansReference')? str_replace("-","- / ", Prado::localize('DEFINE_MIN_CINQ_CARACTERES')):Prado::localize('DEFINE_MIN_CINQ_CARACTERES')%></div></div>
			</div>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<com:TRequiredFieldValidator
					ControlToValidate="reference"
					ValidationGroup="validateInfosConsultation"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('REFERENCE')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
			<com:TRegularExpressionValidator
					ID="formatReference"
					ValidationGroup="validateInfosConsultation"
					ControlToValidate="reference"
					Display="Dynamic"
					EnableClientScript="true"
					ErrorMessage="<%=Prado::localize('REFERENCE') .': '.Prado::localize('FORMAT_INCORRECTE')%>"
					Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRegularExpressionValidator>
			<com:TRequiredFieldValidator
					ControlToValidate="reference"
					ValidationGroup="validateSave"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('REFERENCE')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
			<com:TRegularExpressionValidator
					ID="formatReferenceSave"
					ValidationGroup="validateSave"
					ControlToValidate="reference"
					Display="Dynamic"
					EnableClientScript="true"
					ErrorMessage="<%=Prado::localize('REFERENCE') .': '.Prado::localize('FORMAT_INCORRECTE')%>"
					Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRegularExpressionValidator>
		</div>
	</div>
	<!--Fin Ligne Reference-->

	<!--Debut Ligne Code operation-->
	<com:TPanel cssclass="line" id="panelCodeOperation">
		<div class="intitule col-150"><label for="codeOperation"><com:TTranslate>DEFINE_CODE_OPERATION</com:TTranslate></label> :</div>
		<div class="content-bloc">
			<com:TJuiAutoComplete
					ID="codeOperation"
					OnSuggest="suggestNames"
					Suggestions.DataKeyField="id"
					ResultPanel.CssClass="acomplete"
					cssClass="moyen"
					MinChars="3"
					OnSuggestionSelected="suggestionSelected"
					Frequency="0.4"
					Attributes.title="<%=Prado::localize('DEFINE_CODE_OPERATION')%>"
			>
                <prop:ClientSide.OnLoading>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_numeroACValue').value='';showLoader();</prop:ClientSide.OnLoading>
				<prop:Suggestions.ItemTemplate>
					<li >
						<%#$this->Data['annee_debut']%>-<%#$this->Data['annee_debut']%>
						<br/>
						<%#$this->Data['code']%>
						<br/>
						<%# $this->truncateChaine($this->Data['description'],200, "...")%>
					</li>
				</prop:Suggestions.ItemTemplate>
			</com:TJuiAutoComplete>
			<com:TActiveHiddenField id="idCode"/>
		</div>
		<div class="float-left">
			<img
					src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
					onmouseover="afficheBulle('infosCodeOperation', this)"
					onmouseout="cacheBulle('infosCodeOperation')"
					class="picto-info-intitule"
					alt="Info-bulle"
					title="Info-bulle"
			/>
			<div id="infosCodeOperation"
				 class="info-bulle"
				 onmouseover="mouseOverBulle();"
				 onmouseout="mouseOutBulle();">
				<div><com:TTranslate>DEFINE_INFO_BULLE_CODE_OPERATION</com:TTranslate></div>
			</div>
			<div class="breaker"></div>
		</div>
	</com:TPanel>
	<!--Fin Ligne Code operation-->

	<!--Debut Ligne Domaines activite-->
	<com:TActivePanel ID="entrDomaineActivite" >
		<div class="line">
           				<div class="intitule-150"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate><span class="champ-oblig">*</span> :</div>
			<com:AtexoDomainesActivites ID="domaineActivite" />
		</div>
		<div class="spacer-small"></div>
	</com:TActivePanel>
	<!--Fin Ligne Domaines activite-->
	<!--Debut Ligne Intitule de la consultation-->
	<div class="line" style="display: <%= Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')?'none':''%>;">
		<div class="intitule col-150"><label for="intituleConsultation"><com:TTranslate>DEFINE_INTITULE_CONSULTATION</com:TTranslate></label><span style="display" class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox id="intituleConsultation" Attributes.OnChange="replaceCaracSpecial(this);" CssClass="long" Attributes.title="<%=Prado::localize('DEFINE_INTITULE_CONSULTATION')%>"
					   MaxLength="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_LIMIT_CARACTERE_DESCRIPTION_CONSULTATION')%>"/>
				<com:TRequiredFieldValidator
			ControlToValidate="intituleConsultation"
			ValidationGroup="validateInfosConsultation"
			Display="Dynamic"
			Enabled="<%=(! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE'))%>"
			ErrorMessage="<%=Prado::localize('DEFINE_INTITULE_CONSULTATION')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummary').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TRequiredFieldValidator>
        <com:TRequiredFieldValidator
			ControlToValidate="intituleConsultation"
			ValidationGroup="validateSave"
			Display="Dynamic"
			Enabled="<%=(! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE'))%>"
			ErrorMessage="<%=Prado::localize('DEFINE_INTITULE_CONSULTATION')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummarySave').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TRequiredFieldValidator>
	</div>
	<!--Fin Ligne Intitule de la consultation-->
	<!--Debut Ligne Objet de la consultation-->
	<div class="line">
		<div class="intitule col-150"><label for="objet"><com:TTranslate>DEFINE_OBJET_CONSULTATION</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox id="objet" Attributes.OnChange="replaceCaracSpecial(this);" TextMode="MultiLine" CssClass="long champ-pub" Attributes.title="<%=Prado::localize('DEFINE_OBJET_CONSULTATION')%>" />
		<com:TRequiredFieldValidator
			ControlToValidate="objet"
			ValidationGroup="validateInfosConsultation"
			Display="Dynamic"
			ErrorMessage="<%=Prado::localize('DEFINE_OBJET_CONSULTATION')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummary').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TRequiredFieldValidator>
        <com:TRequiredFieldValidator
			ControlToValidate="objet"
			ValidationGroup="validateSave"
			Display="Dynamic"
			ErrorMessage="<%=Prado::localize('DEFINE_OBJET_CONSULTATION')%>"
			EnableClientScript="true"
			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
			<prop:ClientSide.OnValidationError>
           		document.getElementById('divValidationSummarySave').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
         	</prop:ClientSide.OnValidationError>
        </com:TRequiredFieldValidator>
	</div>
	<!--Fin Ligne Objet de la consultation-->
	<!--Debut Ligne Commentaire interne-->
	<div class="line" style="display: <%= Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')?'none':''%>;">
		<div class="intitule col-150"><label for="commentaire"><com:TTranslate>TEXT_COMENTAIRE_INTERNE</com:TTranslate></label> :</div>
		<com:TActiveTextBox id="commentaire" Attributes.OnChange="replaceCaracSpecial(this);" TextMode="MultiLine" CssClass="long" Attributes.title="<%=Prado::localize('TEXT_COMENTAIRE_INTERNE')%>" />
	</div>
	<!--Fin Ligne Commentaire interne-->
	<com:TActivePanel id="panelLtrefTextAndRadio" >
		<!-- Debut Referentiels zone de texte -->
		<com:AtexoReferentielZoneText id="idReferentielZoneText" consultation="1" obligatoire="1" validationGroup="validateInfosConsultation" validationSummary="divValidationSummary" idPage="IdentificationConsultation" cssClass="intitule-150" mode="1" Type="1"/>
		<!-- Fin Referentiels zone de texte -->
		<!-- Debut Referentiels Radio -->
		<com:AtexoLtRefRadio id="idAtexoLtRefRadio" consultation="1" obligatoire="1" validationGroup="validateInfosConsultation" validationSummary="divValidationSummary" idPage="IdentificationConsultation" cssClass="intitule-150" mode="1" Type="2"/>
		<!-- Fin Referentiels Radio -->
	</com:TActivePanel>
	<div class="spacer-small"></div>
		<!--Debut Bloc Delai Adresse depot / retrait / lieu d'ouverture de Pli -->
	<com:TActivePanel ID="panelAdresseRetraitDossiers" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers') && ! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
		<div class="intitule-bloc intitule-150"><label for="add_retraitDossiers"><com:TTranslate>ADRESSE_RETRAIT_DOSSIERS</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox TextMode="MultiLine" ID="retraitDossiers"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('ADRESSE_RETRAIT_DOSSIERS')%>" cssClass="long" />
			<com:TRequiredFieldValidator
						ID="validatorRetraitDossiers"
						ControlToValidate="retraitDossiers"
						ValidationGroup="validateSave"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('ADRESSE_RETRAIT_DOSSIERS')%>"
						EnableClientScript="true"
			 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummarySave').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
     		</com:TRequiredFieldValidator>
     		<com:TRequiredFieldValidator
						ID="validatorRetraitDossiersInfosCons"
						ControlToValidate="retraitDossiers"
						ValidationGroup="validateInfosConsultation"
						Display="Dynamic"
						Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers'))%>"
						ErrorMessage="<%=Prado::localize('ADRESSE_RETRAIT_DOSSIERS')%>"
						EnableClientScript="true"
			 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
     		</com:TRequiredFieldValidator>
	</com:TActivePanel>
	<com:TActivePanel ID="panelAdresseDepot" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') && ! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
		<div class="intitule-bloc intitule-150"><label for="addDepotDossiers"><com:TTranslate>ADRESSE_DEPOT_BR_OFFRE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox TextMode="MultiLine" ID="addDepotDossiers"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('ADRESSE_DEPOT_OFFRE')%>" cssClass="long" />
			<com:TRequiredFieldValidator
						ID="validatorAddDepotDossiers"
						ControlToValidate="addDepotDossiers"
						ValidationGroup="validateSave"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('ADRESSE_DEPOT_OFFRE')%>"
						EnableClientScript="true"
			 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummarySave').style.display='';
			 				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
     		</com:TRequiredFieldValidator>
     		<com:TRequiredFieldValidator
						ID="validatorAddDepotDossiersInfosCons"
						ControlToValidate="addDepotDossiers"
						ValidationGroup="validateInfosConsultation"
						Display="Dynamic"
						Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseDepotOffres'))%>"
						ErrorMessage="<%=Prado::localize('ADRESSE_DEPOT_OFFRE')%>"
						EnableClientScript="true"
			 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
			 				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
     		</com:TRequiredFieldValidator>
 	</com:TActivePanel>
     <com:TActivePanel ID="panelOuverturePlis" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') && ! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
    	<div class="intitule-bloc intitule-150"><label for="lieuOuverturePlis"><com:TTranslate>LIEU_OUVERTURE_PLIS</com:TTranslate></label><span <%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuOuverturePlisObligatoire')) ? "class='champ-oblig'" : "style='display:none'" %> >*</span> :</div>
    	<com:TActiveTextBox TextMode="MultiLine" ID="lieuOuverturePlis"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('LIEU_OUVERTURE_PLIS')%>" cssClass="long" />
    		<com:TRequiredFieldValidator
    					ID="validatorLieuOuverturePlis"
    					ControlToValidate="lieuOuverturePlis"
    					ValidationGroup="validateInfosConsultation"
    					Display="Dynamic"
    					Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') && Application\Service\Atexo\Atexo_Module::isEnabled('LieuOuverturePlisObligatoire'))%>"
    					ErrorMessage="<%=Prado::localize('LIEU_OUVERTURE_PLIS')%>"
    					EnableClientScript="true"
    		 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
    					<prop:ClientSide.OnValidationError>
    							document.getElementById('divValidationSummary').style.display='';
    		 					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
    		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
    		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
    		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
    					</prop:ClientSide.OnValidationError>
     		</com:TRequiredFieldValidator>
    </com:TActivePanel>
	<div class="spacer-small"></div>
	<!--Debut Ligne Lieu d'execution-->
	<com:TActivePanel id="lieuExecution" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution'))? true:false %>">
		<div class="intitule col-150"><com:TTranslate>TEXT_LIEU_EXECUTION</com:TTranslate><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc bloc-600" id="lieuExe">
			<com:THiddenField ID="idsSelectedGeoN2" />
			<com:THiddenField ID="numSelectedGeoN2" />
			<com:TActiveLabel ID="denominationGeo2T" />
			<span onmouseover="afficheBulle('infosLieuExecution', this)"
			  	onmouseout="cacheBulle('infosLieuExecution')"
			  	class="info-suite"
			  	title="Info-bulle">
			</span>
    		<div class="breaker">&nbsp;</div>
    		<com:THyperLink
    			ID="linkLieuExe1"
    			NavigateUrl="<%=$this->getLieuExecutionUrl()%>"
    			cssClass="bouton-small"
    			Attributes.title="<%=Prado::localize('DEFINIR')%> / <%=Prado::localize('DEFINE_TEXT_EDITER')%>"
    			Text="<%=Prado::localize('DEFINE_DETAILS')%>"
    		/>
        	<com:TCustomValidator ValidationGroup="validateSave" ControlToValidate="idsSelectedGeoN2" ClientValidationFunction="ValidateLieuExecutionFormCons" Display="Dynamic" Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution')%>" ErrorMessage="<%=Prado::localize('TEXT_LIEU_EXECUTION')%>" Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
				<prop:ClientSide.OnValidationError>
	   				document.getElementById('divValidationSummarySave').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
	 			</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TCustomValidator ValidationGroup="validateInfosConsultation" ControlToValidate="idsSelectedGeoN2" ClientValidationFunction="ValidateLieuExecutionFormCons" Display="Dynamic" Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution')%>" ErrorMessage="<%=Prado::localize('TEXT_LIEU_EXECUTION')%>" Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
				<prop:ClientSide.OnValidationError>
	   				document.getElementById('divValidationSummary').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
	 			</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<div class="spacer-mini"></div>
		</div>
		<span style="display:none;">
             <com:TActiveButton
                  ID="selectedGeoN2"
                  OnCommand="Page.bloc_etapeIdentification.displaySelectedGeoN2"
                  OnCallBack="Page.bloc_etapeIdentification.refreshDisplaySelectedGeoN2"
             />
        </span>
	</com:TActivePanel>
	<!--Fin Ligne Lieu d'execution-->

	<!--Début Ligne codes nuts-->
	<com:TActivePanel id="panelCodesNuts" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel'))? true:false %>">
		<div class="intitule-150"><com:TTranslate>TEXT_LIEU_EXECUTION</com:TTranslate><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc bloc-600" id="codesNuts">
    		<com:AtexoReferentiel ID="idAtexoRefNuts" cas="cas5"/>
       	 	<com:TRequiredFieldValidator ValidationGroup="validateSave" ControlToValidate="idAtexoRefNuts.codesRefSec"	Display="Dynamic" Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel')%>" ErrorMessage="<%=Prado::localize('TEXT_LIEU_EXECUTION')%>" Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
             	<prop:ClientSide.OnValidationError>
	   			 	document.getElementById('divValidationSummarySave').style.display='';
	             	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
	             	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
	             	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
	             	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
	 		 	</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
		</div>
	</com:TActivePanel>
	<!--Fin Ligne codes nuts-->



	<!--Debut Ligne CPV-->
			<com:AtexoCodesCpv id="referentielCPV" titre="<%=Prado::localize('TEXT_CODE_CPV')%>" />
			<com:TActiveHiddenField id="enabledCodeCPV" />
	        <com:TTextBox style="display:none" ID="clientIdCodeCPV" Text="<%=$this->enabledCodeCPV->getClientID().'#'.$this->referentielCPV->getClientID().'_cpvPrincipale'%>"/>
			<com:TCustomValidator
				ID="validatorCpv"
				ValidationGroup="validateSave"
				ControlToValidate="clientIdCodeCPV"
				Display="Dynamic"
				ClientValidationFunction="ValidateCPVIdentificationConsultation"
				ErrorMessage="<%=Prado::localize('TEXT_CODE_CPV')%>"
				Text=" "
				EnableClientScript="true">
	     <prop:ClientSide.OnValidationError>
	   		 document.getElementById('divValidationSummarySave').style.display='';
	         document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
	         document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
	         document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
	         document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
	 	 </prop:ClientSide.OnValidationError>
	</com:TCustomValidator>
	<!--Fin Ligne CPV-->
	<com:TActivePanel id="panelLtref" >
		<!-- Debut LT Referentiel -->
		<com:AtexoLtReferentiel id="ltrefCons" consultation="1" obligatoire="1" validationGroup="validateInfosConsultation" validationSummary="divValidationSummary" idPage="IdentificationConsultation" cssClass="intitule-150" mode="1" />
		<!-- Fin LT Referentiel -->
	</com:TActivePanel>
	<div class="spacer-small"></div>
	<!-- Début bouton refresh -->
	<com:TActiveButton ID="buttonRefresh" Display="None" oncallBack="Page.refreshComposants"/>
	<!-- Fin bouton refresh -->

	<!--Debut Ligne Allotissement-->
	<com:TActivePanel Cssclass="line" id="lignePanelalloti">
		<div class="intitule col-150"><com:TTranslate>TEXT_ALLOTISSEMENT</com:TTranslate> :</div>
		<div class="content-bloc bloc-500">
			<com:TActivePanel id="panelalloti" cssclass="intitule-auto clear-both">
				<com:TActiveRadioButton
						GroupName="allotissement"
						id="alloti"
						CssClass="radio"
						Attributes.onclick="traitementConsAlloti(this);"
						onCallback="Page.refreshDonneesComplementaireConsultation"
						ActiveControl.CallbackParameter="1"
				>
					<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
					<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
					<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
					</com:TActiveRadioButton>
				<label for="ctl0_CONTENU_PAGE_bloc_etapeIdentification_alloti"><com:TTranslate>TEXT_CONS_ALLOTIE</com:TTranslate></label>
			</com:TActivePanel>
			<com:TActivePanel id="panelmarcheUnique" cssclass="intitule-auto clear-both">
				<com:TActiveRadioButton
                    GroupName="allotissement"
                    id="marcheUnique"
                    Checked="true"
                    CssClass="radio"
                    Attributes.onclick="traitementConsNonAlloti(this);"
                    onCallback="Page.refreshDonneesComplementaireConsultation"
                    ActiveControl.CallbackParameter="0"
				>
					<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
					<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
					<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
				</com:TActiveRadioButton>
               <label for="marcheUnique"><com:TTranslate>TEXT_CONS_NON_ALLOTIE</com:TTranslate></label>
			</com:TActivePanel>
		</div>
	</com:TActivePanel>
	<com:TPanel cssClass="line" id="panelJustificationNonAllotie"  style="display:none;" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')%>">
		<div class="spacer-small"></div>
		<div class="intitule col-150"><label for="<%=$this->justificationNonAlloti->getClientId()%>"><com:TTranslate>TEXT_JUSTIFICATION_NON_ALLOTI</com:TTranslate></label> :</div>
		<div class="content-bloc-justificatif">
			<com:TTextBox id="justificationNonAlloti"
						  Attributes.title="<%=Prado::Localize('TEXT_JUSTIFICATION_NON_ALLOTI')%>"
						  TextMode="MultiLine"
						  cssClass="long"
						  Attributes.maxlength="<%= Application\Service\Atexo\Atexo_Config::getParameter('MAXLENGTH_JUSTIFICATION_NON_ALLOTI')%>"
			/>
			<img title="Info-bulle" alt="Info-bulle" class="picto-info-justificatif inline-picto-info" onmouseout="cacheBulle('infosJustificationNonAllotie')" onmouseover="afficheBulle('infosJustificationNonAllotie', this)" src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif' />
			<div id="infosJustificationNonAllotie" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>INFO_BULL_JUSTIFICATION_NON_ALLOTI</com:TTranslate></div></div>
		</div>
	</com:TPanel>
	<div class="spacer-small"></div>
	<!--Fin Ligne Allotissement-->
	<com:TActivePanel ID="panelVariantesAutorisees" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && ! Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
	<div Class="line">
		<div class="intitule col-150"><label for="variantesAutorisees"><com:TTranslate>VARIANTES_AUTORISEES</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="content-bloc bloc-400">
			<div class="line">
				<com:TActiveRadioButton GroupName="variantes" ID="variantesNon" Attributes.title="<%=Prado::localize('TEXT_NON')%>"/>
				<label for="variantesNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
			</div>
			<div class="line">
				<com:TActiveRadioButton GroupName="variantes"  ID="variantesOui"  Attributes.title="<%=Prado::localize('FCSP_OUI')%>"/>
				<label for="variantesOui"><com:TTranslate>FCSP_OUI</com:TTranslate></label>
			</div>
		</div>
		</div>
		<div class="spacer-small"></div>
	</com:TActivePanel>
	<!--Debut Ligne Achat responsable-->
	<com:TPanel id="layerClauseConsultation" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause'))? true:false %>">
		<com:AchatResponsableConsultation id="achatResponsableConsultation" mode="1" validationGroup="validateSave" validationSummary="divValidationSummarySave" sourcePage="etapeIdentification" validationGroupBtnValidate="validateInfosConsultation" ValidationSummaryBtnValidate="divValidationSummary" />
	</com:TPanel>
	<com:TTextBox Display="None" id="typageJoFormValue"/>
	<div id="typage-jo" class="content-bloc bloc-580" style="display: <%=(Application\Service\Atexo\Atexo_Module::isEnabled('typageJo2024')) ? '':'none' %>;">
		<typage-jo :raw-data="'<%= htmlspecialchars(json_encode($this->setDataTypageJo())) %>'"></typage-jo>
	</div>
	<!--Fin Ligne Achat responsable-->
	<div class="breaker"></div>
</div>
<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<com:THiddenField id="idAffaire" />
<com:TActiveLabel id="labelScript" style="display:none"/>
<com:TActiveLabel id="labelScript2" style="display:none"/>
<com:TActiveLabel id="labelScriptDUME" style="display:none"/>
<com:TActiveLabel id="labelScriptArticles" style="display:none"/>
<com:TActiveHiddenField id="dumeA" value="0"/>
</div>
<com:TActivePanel cssclass="form-field" id="bloc_caseAttestationConsultation" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('CaseAttestationConsultation')%>">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="line">
			<com:TActiveCheckBox  id="caseAttestationConsultation"/>
			<label for="<%=$this->caseAttestationConsultation->getClientId()%>"><com:TTranslate>ATTESTATION_CONSULTATION</com:TTranslate></label><span class="champ-oblig">*</span>
			<com:TRequiredFieldValidator
					ControlToValidate="caseAttestationConsultation"
					ValidationGroup="validateSave"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('VEUILLEZ_ATTESTER_CONSULTATION')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
			<com:TRequiredFieldValidator
					ControlToValidate="caseAttestationConsultation"
					ValidationGroup="validateInfosConsultation"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('VEUILLEZ_ATTESTER_CONSULTATION')%>"
					EnableClientScript="true"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummarySave').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TRequiredFieldValidator>
			<a href="#infosAttestationConsultation"
			   onmouseover="afficheBulle('infosAttestationConsultation', this)"
			   onmouseout="cacheBulle('infosAttestationConsultation')"
			   onfocus="afficheBulle('infosAttestationConsultation', this)"
			   onblur="cacheBulle('infosAttestationConsultation')" class="picto-info-intitule">
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
			</a>
			<div id="infosAttestationConsultation" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>INFOBULLE_ATTESTATION_CONSULTATION</com:TTranslate></div></div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
</com:TActivePanel>
<!--Debut Modal changement-dume-type-->
<div class="changement-dume-type" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">

			<com:TTranslate>CONFIRMATION_CHANGEMENT_DUME_TYPE</com:TTranslate>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Formulaire-->
	<com:TActiveHiddenField ID="valideChangement" />
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="<%=Prado::localize('DEFINE_ANNULER')%>"
			   title="<%=Prado::localize('DEFINE_ANNULER')%>"

			   onclick="J('.changement-dume-type').dialog('destroy'); return false"
		/>

		<input type="button"
			   class="bouton-moyen float-right"
			   value="<%=Prado::localize('ICONE_OK')%>"
			   title="<%=Prado::localize('ICONE_OK')%>"

			   onclick="changeDumeType();  "
		/>

	</div>
	<!--Fin line boutons-->
</div>


<!--Fin Modal changement-dume-type-->
<com:TActiveLabel id="scriptIntitulePublicite" style="display:none"/>

<com:TActivePanel visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
<script>
	handleGroupRadioRelatedPub('<%= $this->buyersBlock->ClientId %>');
	handleGroupRadioRelatedPub('<%= $this->procedureOpenClosedBlock->ClientId %>', '<%= $this->mapaProcedureTypeOrg->Value %>');


	function handleGroupRadioRelatedPub(groupRadio, mapa)
	{
		const radiosPublicationPubliciteBlock = document.getElementById('<%= $this->selectPublicationPublicite->ClientId %>');
		const radioPublicationPubliciteAlreadyChecked = radiosPublicationPubliciteBlock.querySelector('input[type=radio]:checked');
		const radiosBlock = document.getElementById(groupRadio);

		let displayRadiosBlock = "<%=Application\Service\Atexo\Atexo_Module::isEnabled('ModuleEcoSip')%>";
		if (displayRadiosBlock == 1) {
			return;
		}

		if (radioPublicationPubliciteAlreadyChecked && radiosPublicationPubliciteBlock.style.display == 'block') {
			if (mapa) {
				radiosBlock.style.display = (radioPublicationPubliciteAlreadyChecked.value.indexOf('Oui') >= 0 && mapa == 1) ? 'block' : 'none';
			} else {
				radiosBlock.style.display = (radioPublicationPubliciteAlreadyChecked.value.indexOf('Oui') >= 0) ? 'block' : 'none';
			}
			disableRadio(radiosBlock);
		}

		radiosPublicationPubliciteBlock.addEventListener('click', event => {
			if (mapa) {
				radiosBlock.style.display = (event.target.value.indexOf('Oui') >= 0 && mapa == 1) ? 'block' : 'none';
			} else {
				radiosBlock.style.display = (event.target.value.indexOf('Oui') >= 0) ? 'block' : 'none';
			}
			disableRadio(radiosBlock);
		})
	}

	function disableRadio(groupRadios)
	{
		const radiosGroup = groupRadios.querySelectorAll('input[type=radio]');
		radiosGroup.forEach(element => {
					if (groupRadios.style.display == 'none') {
						element.setAttribute('disabled', 'disabled')
					} else {
						element.removeAttribute('disabled')
					}
				}
		)
	}

</script>
</com:TActivePanel>
