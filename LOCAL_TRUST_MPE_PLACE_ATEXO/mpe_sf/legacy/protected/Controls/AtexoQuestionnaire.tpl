<div class="form-field margin-0 bloc-bdp-formulaire">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
				<div class="float-right"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
				<div class="spacer-mini"></div>
				<com:TRepeater ID="repeaterQuestionnaireItems" >
					<prop:HeaderTemplate>
					<table class="table-results tableau-prix" summary="Tableau des questions">
						<caption>Liste des question</caption>
						<thead>
							<tr>
								<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-40" id="questionnaire_col_01"><com:TTranslate>DEFINE_ITEM</com:TTranslate></th>
								<th class="col-400" id="questionnaire_col_02"><com:TTranslate>DEFINE_QUESTION</com:TTranslate></th>
								<th class="col-400" id="questionnaire_col_04"><com:TTranslate>TEXT_REPONSE_FOURNISSEUR</com:TTranslate></th>
							</tr>
						</thead>
						</prop:HeaderTemplate>
	    				<prop:ItemTemplate>
								<tr class="<%#(($this->ItemIndex%2==0)? 'on':'')%>">
									<td class="col-40" headers="questionnaire_col_01"><%#$this->Data->getNumero()%></td>
									<td class="col-400" headers="questionnaire_col_02">
										<%#$this->Data->getLibelle()%>
										<com:TPanel id="comment" cssClass="commentaire" Visible="<%#$this->Data->getCommentaireAcheteur()!=''%>">
											<div class="intitule"><com:TTranslate>DEFINE_COMMENTAIRE_ACHETEUR</com:TTranslate> :</div>
											<%#$this->Data->getCommentaireAcheteur()%>
										</com:TPanel>
			
									</td>
									<td class="col-400" headers="questionnaire_col_04">
									<!-- Type Text court -->
										<com:TPanel id="panelCourt" Visible="<%#$this->Data->getTypeReponse()==1%>">
											<com:TTextBox id="itemTextCourt" cssClass="montant" Attributes.title="" Text="<%#$this->Data->getValeur()%>" enabled="false"/>
											<com:TRequiredFieldValidator 
												ControlToValidate="itemTextCourt"
												ValidationGroup="validateItemsFormCons"
												Display="Dynamic"
												Enabled="<%#($this->Data->getObligatoire()==1)%>"
												ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_REPONSE_FOURNISSEUR')%>"
												EnableClientScript="true"  						 					 
									 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
												<prop:ClientSide.OnValidationError>
			           				 				document.getElementById('divValidationSummary').style.display='';
			                                        document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
			         							</prop:ClientSide.OnValidationError>
			         						</com:TRequiredFieldValidator>
											<com:TLabel Visible="<%#$this->Data->getObligatoire()==1%>" cssClass="champ-oblig">*</com:TLabel>
										</com:TPanel>
										
										<!-- Type Text Long -->
										<com:TPanel cssClass="commentaire-reponse" id="panelTextLong" Visible="<%#$this->Data->getTypeReponse()==2%>">
											<com:TTextBox id="itemTextLong" TextMode="MultiLine" Text="<%#$this->Data->getValeur()%>" enabled="false"/>
											<com:TLabel Visible="<%#$this->Data->getObligatoire()==1%>" cssClass="champ-oblig">*</com:TLabel>
											<com:TRequiredFieldValidator 
												ControlToValidate="itemTextLong"
												ValidationGroup="validateItemsFormCons"
												Display="Dynamic"
												Enabled="<%#($this->Data->getObligatoire()==1)%>"
												ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_REPONSE_FOURNISSEUR')%>"
												EnableClientScript="true"  						 					 
									 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
												<prop:ClientSide.OnValidationError>
			           				 				document.getElementById('divValidationSummary').style.display='';
			                                        document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
			         							</prop:ClientSide.OnValidationError>
			         						</com:TRequiredFieldValidator>	
										</com:TPanel>
										
										<!-- Type radion bouton -->
										<com:TPanel id="panelRadio" Visible="<%#$this->Data->getTypeReponse()==4%>">
											<span class="check-bloc"><com:TRadioButton id="itemRadiooui" groupName="itemRadio" Checked="<%#$this->Data->getValeur()=='1'%>" enabled="false"/><label for="itemRadiooui"><com:TTranslate>DEFINE_OUI</com:TTranslate></label></span>
											<span class="check-bloc"><com:TRadioButton id="itemRadionon" groupName="itemRadio" Checked="<%#$this->Data->getValeur()=='2'%>" enabled="false"/><label for="itemRadionon"><com:TTranslate>TEXT_NON</com:TTranslate></label></span>
											<com:TLabel Visible="<%#$this->Data->getObligatoire()==1%>" cssClass="champ-oblig">*</com:TLabel>
											<com:TCustomValidator 
													ValidationGroup="validateItemsFormCons" 
													ControlToValidate="itemRadiooui" 
													ClientValidationFunction="validationRadioOuiNon" 
													Display="Dynamic" 
													Enabled="<%#$this->Data->getObligatoire()==1%>"
													ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_REPONSE_FOURNISSEUR')%>"
													Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" EnableClientScript="true" >
												<prop:ClientSide.OnValidationError>
			   				 						document.getElementById('divValidationSummary').style.display='';
		                                            document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
			 									</prop:ClientSide.OnValidationError>
										   </com:TCustomValidator>
										</com:TPanel>
										
										<!-- precision entreprise -->
										<com:TPanel id="panelPrecisionEntrp" cssClass="commentaire-reponse" Visible="<%#($this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE') || $this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('LIBRE'))%>">
											<div class="intitule"><label for="itemPrecisionEntrp"><com:TTranslate>DEFINE_PRECISION_ENTREPRISE</com:TTranslate></label> :</div>
											<com:TTextBox id="itemPrecisionEntrp" TextMode="MultiLine" Text="<%#$this->Data->getValuePrecisionentreprise()%>" enabled="false"/>
											<com:TLabel Visible="<%#$this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE')%>" cssClass="champ-oblig">*</com:TLabel>
											<com:TRequiredFieldValidator 
												ControlToValidate="itemPrecisionEntrp"
												ValidationGroup="validateItemsFormCons"
												Display="Dynamic" 
												Enabled="<%#$this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE')%>"
												ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('DEFINE_PRECISION_ENTREPRISE')%>"
												EnableClientScript="true"  						 					 
									 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
												<prop:ClientSide.OnValidationError>
			           				 				document.getElementById('divValidationSummary').style.display='';
			                                        document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
			         							</prop:ClientSide.OnValidationError>
			         						</com:TRequiredFieldValidator>
										</com:TPanel>
										<com:THiddenField id="idItemFormcons" value="<%#$this->Data->getId()%>"/>
										<com:THiddenField id="idFormcons" value="<%#$this->Data->getIdFormulaireConsultation()%>"/>
										<com:THiddenField id="isObligatoire" value="<%#$this->Data->getObligatoire()%>"/>
										<com:THiddenField id="precisionEntrp" value="<%#$this->Data->getPrecisionEntreprise()%>"/>
										<com:THiddenField id="typeReponse" value="<%#$this->Data->getTypeReponse()%>"/>
									</td>
								</tr>
						</prop:ItemTemplate>
	        			<prop:FooterTemplate>
					</table>
					</prop:FooterTemplate>
        		</com:TRepeater>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>