<!-- Debut bloc composant ajouter -->
<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		
		<div class="content">
			<div class="intitule-auto bloc-150">
				<div class="intitule-auto"><strong><u><com:TTranslate>AJOUTER</com:TTranslate></u></strong></div>
				<div class="breaker"></div>
				<div class="intitule-auto"><com:TTranslate>ORIGINE_DE_LA_PIEACE_A_AJOUTER</com:TTranslate></div>
			</div>
			<div class="intitule-auto bloc-600">
				<div class="intitule-auto bloc-150 indent-10">
					<com:TRadioButton id="pieceLibre" GroupName="pieceDce" Attributes.title="<%=Prado::localize('PIECE_LIBRE')%>" cssClass="radio" checked="true" />
            		<label for="pieceLibre" class="blue bold"><com:TTranslate>PIECE_LIBRE</com:TTranslate></label>
				</div>
				<div class="intitule-auto bloc-400">
					<com:AtexoFileUpload id="fileExterne" Attributes.title="<%=Prado::localize('TEXT_DOCUMENT_EXTERNE')%>" CssClass="file-405" Attributes.size="50"  />
					<div id="errorFileUpload" class="red" ></div>
				</div>
				<div class="spacer"></div>
				<div class="breaker"></div>
				<div class="intitule-auto bloc-150 indent-10">
					<com:TRadioButton id="pieceRedac" GroupName="pieceDce" Attributes.title="<%=Prado::localize('PIECE_DE_REDAC')%>" cssClass="radio" />
	            	<label for="pieceRedac" class="blue bold"><com:TTranslate>PIECE_DE_REDAC</com:TTranslate></label>
				</div>
				<div class="spacer-mini"></div>
				<div class="intitule-auto bloc-600 indent-30">
				<!--Debut Bloc Documents du module REDAC-->
					<com:TActivePanel ID="panelListeDocsRedac" >
						<com:TRepeater ID="repeaterDocsRedac" >
							<prop:HeaderTemplate>
								<table class="table-results" summary="Documents du module REDAC">
									<caption><com:TTranslate>TEXT_DOCUMENTS_DU_MODULE_REDAC</com:TTranslate></caption>
									<thead>
										<tr>
											<th colspan="6" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
										</tr>
										<tr>
											<th class="col-60 center" id="check">&nbsp;</th>
											<th class="col-60" id="type"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
											<th class="col-500" id="nomDocument"><com:TTranslate>TEXT_TITRE_DU_DOCUMENT_FICHIER</com:TTranslate></th>
											<th class="col-150" id="statut"><span title="<%=Prado::localize('DEFINE_STATUS')%>"><com:TTranslate>DEFINE_STATUS</com:TTranslate></span></th>
											<th class="actions actions-inline" id="actionDCE"></th>
										</tr>
									</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
								<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
									<td class="check-col">
										<com:THiddenField ID="idRedac" Value="<%#$this->Data->getId()%>" />
										<com:THiddenField ID="nomRedac" Value="<%#$this->Data->getNom()%>" />
										<com:TPanel id="blocCheck" visible="<%=!($this->page->composantAjouter->isRadio())%>" >
											<com:TCheckBox 
												id="idPieceRedacCheck" 
												Attributes.title="<%=Prado::localize('PIECE_DE_REDAC')%>"
												Attributes.onchange="document.getElementById('ctl0_CONTENU_PAGE_composantAjouter_pieceRedac').checked  = true;"
												cssClass="check" />
										</com:TPanel>
										<com:TPanel id="blocRadio" visible="<%=$this->page->composantAjouter->isRadio()%>">
											<com:TRadioButton 
												id="idPieceRedacRadio" 
												UniqueGroupName="pieceRedac" 
												Attributes.title="<%=Prado::localize('PIECE_DE_REDAC')%>"
												Attributes.onchange="document.getElementById('ctl0_CONTENU_PAGE_composantAjouter_pieceRedac').checked  = true;"
												CssClass="radio" />
										</com:TPanel>
									</td>
									<td class="col-60"><%#$this->Data->getType()%></td>
									<td class="col-500"><strong><%#$this->Data->getTitre()%></strong><div><%#$this->Data->getNom()%></div><div><%#$this->Data->getAuteur()%></div></td>
									<td class="col-150" headers="statut"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/document-statut-3.gif" alt="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->Data->getStatut()%>" title="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->Data->getStatut()%>" class="statut" /></td>
									<td class="actions actions-inline">
										<a title="<%=Prado::localize('DEFINE_TELECHARGER')%>" href="index.php?page=Agent.DownloadDocRedac&id=<%#base64_encode($this->Page->composantAjouter->_consultation->getReference())%>&IdBlob=<%#$this->Data->getId()%>&nomDoc=<%#base64_encode($this->Data->getNom())%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" /></a>
									</td>
								</tr>
							</prop:ItemTemplate>
							<prop:FooterTemplate>
								</table>
							</prop:FooterTemplate>
						</com:TRepeater>
					</com:TActivePanel>
					<com:TActivePanel ID="panelNoElementFound"  >
						<h2><center><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</center></h2>
					</com:TActivePanel>	
				<!--Fin Bloc Documents du module REDAC-->
				</div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
<!--  Fin de Bloc composant ajouter -->

<script type="text/javascript">
	J('#ctl0_CONTENU_PAGE_composantAjouter_fileExterne').on('change', function() {
		J('#errorFileUpload').css({ display: "none" });
		if(this.files[0].size >  J('#MAX_FILE_SIZE').val()){
			J('#errorFileUpload').css({ display: "block" });
			J('#errorFileUpload').text("<%= Prado::localize('TEXT_TAILLE_AUTORISEE_DOC', array('size' =>  Application\Service\Atexo\Atexo_Util::arrondirSizeFile(($this->getFileSizeMax() / 1024)))) %>");
		}
	});
</script>


