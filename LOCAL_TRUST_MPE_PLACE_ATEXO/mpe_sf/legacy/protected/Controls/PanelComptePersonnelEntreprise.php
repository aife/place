<?php

namespace Application\Controls;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Service\PradoPasswordEncoderInterface;
use Application\Propel\Mpe\CommonInscrit;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * Classe de gestion des informations du compte personnel de l'entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class PanelComptePersonnelEntreprise extends MpeTTemplateControl
{
    private ?\Application\Propel\Mpe\CommonInscrit $_inscrit = null;
    private ?string $_company = null;
    private ?bool $_postBack = null;
    private bool $_visible = false;

    /**
     * Modifie la valeur de l'attribut [_company].
     *
     * @param string $value : nouvelle valeur de l'attribut [_company]
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setCompany($value)
    {
        $this->setViewState('company', $value);
        $this->_company = $value;
    }

    public function getCompany()
    {
        $this->getViewState('company');
    }

    /**
     * Modifie la valeur de l'attribut [_inscrit].
     *
     * @param CommonInscrit $value : nouvelle valeur de l'attribut [_inscrit]
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setInscrit($value)
    {
        $this->setViewState('inscrit', $value);
        $this->_inscrit = $value;
    }

    public function getInscrit()
    {
        return $this->getViewState('inscrit');
    }

    /**
     * Modifie la valeur de l'attribut [_postBack].
     *
     * @param bool $value : nouvelle valeur de l'attribut [_postBack]
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    /**
     * recupere la valeur de l'attribut [_postBack].
     *
     * @return bool : la valeur courante de l'attribut [_postBack]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPostBack()
    {
        return $this->_postBack;
    }

    /**
     * Modifie la valeur de l'attribut [_visible].
     *
     * @param bool $value : nouvelle valeur de l'attribut [_visible]
     *
     * @return void
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setVisible($value)
    {
        $this->setViewstate('panel_visible', $value);
    }

    /**
     * recupere la valeur de l'attribut [_visible].
     *
     * @return bool : la valeur courante de l'attribut [_visible]
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getVisible($checkParents = true)
    {
        return $this->getViewstate('panel_visible');
    }

    /**
     * Chargement de la page.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function onLoad($param)
    {
        $this->panelOldPassword->setVisible(false);
        $this->customizeForm();
        if (Atexo_Module::isEnabled('AuthenticateInscritByCert', null)) {
            $this->panelChoixCert->setVisible(true);
        } else {
            $this->panelChoixCert->setVisible(false);
        }
        if (Atexo_Module::isEnabled('AuthenticateInscritByLogin')) {
            $this->panelAuthLoginPass->setVisible(true);
        } else {
            $this->panelAuthLoginPass->setVisible(false);
        }
        if ('ChangingUser' == $_GET['action']) {
            if (! Atexo_CurrentUser::getIdInscrit()) {
                $this->response->redirect(Atexo_Config::getParameter('PF_URL'));
                return;
            }
            if ('Entreprise.FormulaireCompteEntreprise' == $_GET['page']) {
                $this->panelOldPassword->setVisible(true);
            }
            //Désactiver les validateurs de création compte
            $this->Page->emailCreationValidator->SetEnabled(false);
            $this->loginCreationValidator->SetEnabled(false);
            $this->certifCreationValidator->SetEnabled(false);
            //Activer les validateurs de modification de compte de l'UES autentifié
            $this->Page->emailModificationValidator->SetEnabled(true);
            $this->loginModificationValidator->SetEnabled(true);
            $this->certifModificationValidator->SetEnabled(true);
            if (!$this->Page->IsPostBack) {

                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
                if ($inscrit) {
                    $this->script->text = "<script>checkRgpdEntreprise(" . (($inscrit->getRgpdCommunicationPlace() === true) ? '1' : '0') . "," . (($inscrit->getRgpdEnquete() === true) ? '1' : '0') . "," . (($inscrit->getRgpdCommunication() === true) ? '1' : '0') . ")</script>";
                    $this->enqueteRgpd->Checked = $inscrit->getRgpdEnquete();
                    $this->communicationPlaceRgpd->Checked = $inscrit->getRgpdCommunicationPlace();
                    $this->communicationRgpd->Checked = $inscrit->getRgpdCommunication();
                }

                //Désactiver les validateurs de password et conf password
                $this->password->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
                $this->confPassword->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
                $this->passwordValidator->SetEnabled(false);
                if (!is_object($this->getInscrit()) && !isset($_GET['id']) && $inscrit) {
                    $this->setInscrit($inscrit);
                }
                if (is_object($this->getInscrit()) && (new Atexo_Entreprise_Inscrit())->inscritHasCertificate($this->getInscrit()->getId())) {
                    $this->panelChoixLoginMdp->style = 'display:""';
                    $this->panelChoixCert->setVisible(false);
                    $this->CaseLogin->SetChecked(false);
                    $Text = '<script>';
                    $Text .= "document.getElementById('panelAuthLoginPass').style.display = 'none';";
                    $Text .= "document.getElementById('div_cn_certif').style.display = '';";
                    $Text .= "document.getElementById('div_ac_certif').style.display = '';";
                    $Text .= '</script>';
                    $this->JSCert->Text = $Text;
                    $this->cnCertif->Text = (new Atexo_Crypto_Certificat())->getCnCertificat($this->getInscrit()->getCert(), session_id());
                    $this->acCertif->Text = (new Atexo_Crypto_Certificat())->getACCertificat($this->getInscrit()->getCert());
                } else {
                    $this->panelChoixLoginMdp->style = 'display:none';
                }
                $this->displayInscrit();
                if ($inscrit) {
                    $this->Page->setViewState('profil', $this->getInscrit()->getProfil());
                }
            }
        } elseif ('creation' == $_GET['action'] || 'creationByATES' == $_GET['action']) {
            if ('Entreprise.FormulaireCompteEntreprise' == $_GET['page']) {
                $this->Page->oldPasswordModificationValidator->SetEnabled(false);
            }
            // Desactiver les validateurs de modification Compte
            $this->Page->emailModificationValidator->SetEnabled(false);
            $this->loginModificationValidator->SetEnabled(false);
            $this->certifModificationValidator->SetEnabled(false);
            //Activer les validateurs de création compte
            //$this->validateurSiret->SetEnabled(true);
            $this->Page->emailCreationValidator->SetEnabled(true);
            $this->loginCreationValidator->SetEnabled(true);
            $this->certifCreationValidator->SetEnabled(true);
            $this->panelChoixLoginMdp->style = 'display:none';
            if (!$this->getPostBack()) {
                if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
                    $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
                    if (isset($headersArray['IV-USER']) && $headersArray['IV-USER']) {
                        if ($headersArray['Z-GIVENNAME']) {
                            $this->prenomPerso->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-GIVENNAME']));
                        }
                        if ($headersArray['Z-SN']) {
                            $this->nomPerso->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-SN']));
                        }
                        if ($headersArray['Z-MAIL']) {
                            $this->emailPersonnel->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-MAIL']));
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
                    }
                }
                if (Atexo_Config::getParameter('ACTIVER_FC_AUTHENTIFICATION') && !empty($_GET['from']) && Atexo_Util::FC_CALLBACK === $_GET['from']) {
                    $this->emailPersonnel->Text = urldecode(Atexo_Util::atexoHtmlEntities($_GET['email']));
                    $this->emailPersonnel->setEnabled(false);
                    $this->prenomPerso->Text = $_GET['family_name'];
                    $this->nomPerso->Text = $_GET['given_name'];
                    $this->identifiant->Text = $_GET['email'];
                }
            }
        }
    }

    /**
     * Instancie un nouvel objet Inscrit et charge les informations du formulaire dans cet objet.
     *
     *@param Atexo_Entreprise_EntrepriseVo $entreprise : objet entreprise
     *
     * @return CommonInscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setNewInscrit($entreprise)
    {
        $inscrit = new CommonInscrit();
        $inscrit->setNom($this->nomPerso->SafeText);
        $inscrit->setPrenom($this->prenomPerso->SafeText);
        $inscrit->setEmail(trim($this->emailPersonnel->SafeText));
        $inscrit->setTelephone($this->telPersonnel->SafeText);
        $inscrit->setFax($this->faxPersonnel->SafeText);
        if (Atexo_Module::isEnabled('AuthenticateInscritByLogin') || Atexo_Module::isEnabled('AuthenticateInscritByCert')) {
            if (true == $this->CaseLogin->Checked) {
                $inscrit->setLogin($this->identifiant->Text);
                if ('' != $this->password->Text
                    && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->password->Text
                    && '' != $this->confPassword->Text
                    && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->confPassword->Text) {
                    Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $this->password->getText());
                }
            } elseif (((Atexo_CurrentUser::readFromSession('userCert')) !== null)
                && (null !== Atexo_CurrentUser::readFromSession('userCert'))) {
                $inscrit->setCert(Atexo_CurrentUser::readFromSession('userCert'));
                $inscrit->setNumCert(Atexo_CurrentUser::readFromSession('userSerial'));
            }
        }

        if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
            $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
            if (isset($headersArray['IV-USER']) && $headersArray['IV-USER']) {
                $inscrit->setLogin($headersArray['IV-USER']);
                $inscrit->setMdp('');
            }
        }
        if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo && $entreprise->getId() && $entreprise->hasInscrit()) {
            $inscrit->setProfil('1'); //Inscrit simple
        } else {
            $inscrit->setProfil('2'); //ATES
        }
        $inscrit->setDateCreation(date('Y-m-d H:i:s'));
        $inscrit->setDateModification(date('Y-m-d H:i:s'));
        (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($inscrit, $this->Page->etablissements->getSiretMonEtablissement(), Atexo_CurrentUser::getIdEntreprise());

        return $inscrit;
    }

    /**
     * Permet de valider l'unicite du login de l'utilisateur inscrit entreprise a la creation
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyLoginCreation($sender, $param)
    {
        if (true == $this->CaseLogin->Checked) {
            $login = $this->identifiant->getText();
            if ($login) {
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($login);
                if ($inscrit) {
                    $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                    return;
                }
            }
        }
    }

    /**
     * Permet de valider l'unicite de l'email de l'utilisateur inscrit entreprise a la creation
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyMailCreation($sender, $param)
    {
        $mail = $this->emailPersonnel->getText();
        if ($mail) {
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($mail);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->Page->emailCreationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);

                return;
            }
        }
    }

    /**
     * Permet de valider le certificat de l'utilisateur inscrit entreprise a la creation
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyCertifCreation($sender, $param)
    {
        if (false == $this->CaseLogin->Checked) {
            $certif = Atexo_CurrentUser::readFromSession('userCert');
            $serial = Atexo_CurrentUser::readFromSession('userSerial');
            if (!$certif && !$serial && ('' == $this->cnCertif->Text) && ('' == $this->acCertif->SafeText)) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIF_NON_SELECTIONNE');
                $this->certifCreationValidator->setVisible(false);

                return;
            }

            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveCertAuthUE($certif, $serial);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_EXISTE_DEJA');
                $this->certifCreationValidator->setVisible(false);

                return;
            }
        }
    }

    /**
     * Permet de valider le login de l'utilisateur inscrit entreprise a la modification
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyLoginModification($sender, $param)
    {
        if (true === $this->CaseLogin->Checked) {
            $login = $this->identifiant->Text;
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLoginForModValidation($login, Atexo_CurrentUser::getIdInscrit());
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->loginModificationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                return;
            }
        }
    }

    /**
     * Permet de valider l'email de l'utilisateur inscrit entreprise a la modification
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyMailModification($sender, $param)
    {
        $mail = $this->emailPersonnel->Text;

        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMailForModValidation($mail, Atexo_CurrentUser::getIdInscrit());
        if ($inscrit) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->Page->emailModificationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);
            //return;
        }
    }

    /**
     * Permet de valider le certificat de l'utilisateur inscrit entreprise a la modification
     * Affiche un message d'erreur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function verifyCertifModification($sender, $param)
    {
        if ((false === $this->CaseLogin->Checked) && ('' == $this->identifiant->Text) && ('' == $this->password->Text)) {
            $certif = Atexo_CurrentUser::readFromSession('userCert');
            $serial = Atexo_CurrentUser::readFromSession('userSerial');
            if (!$certif && !$serial) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifModificationValidator->ErrorMessage = Prado::localize('TEXT_CERTIF_NON_SELECTIONNE');

                return;
            }
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByCertForModValidation($serial, $this->getInscrit()->getId());
            if ($inscrit) {
                $searchCert = base64_decode($certif);
                foreach ($inscrit as $oneInscrit) {
                    $userCert = base64_decode($oneInscrit->getCert());
                    $cmp = strcmp($searchCert, $userCert);
                    if (0 == $cmp) {
                        $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                        $param->IsValid = false;
                        $this->certifModificationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_EXISTE_DEJA');

                        return;
                    }
                }
            }
        }
    }

    /**
     * Permet de l'affichage des champs du formulaire
     * Gere l'activation/desactivation des validateurs.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function customizeForm()
    {
        if (Atexo_CurrentUser::isATES() && Atexo_Module::isEnabled('AutoriserModifProfilInscritAtes')) {
            $this->InfoProfil->setDisplay('Dynamic');
        } else {
            $this->InfoProfil->setDisplay('None');
            $this->InfoProfil->visible = false;
        }
        $this->panelComptePersonnelEntreprise->visible = false;
        if (true === $this->getVisible()) {
            $this->panelComptePersonnelEntreprise->visible = true;
            if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
                $this->emailValidator->Enabled = false;
            }
            if (!Atexo_Module::isEnabled('InscriptionLibreEntreprise')) {
                if ('Entreprise.EntrepriseInscriptionUsers' == $_GET['page'] // Modification de mon compte via l'accueil
                    || 'Entreprise.EntreprisePopupUpdateUser' == $_GET['page'] && $_GET['id'] == Atexo_CurrentUser::getIdInscrit()) {// Modification de mon compte via la liste des utilisateurs
                    $this->panelNomPerso->SetEnabled(false);
                    $this->panelPrenomPerso->SetEnabled(false);
                    $this->panelLogin->SetEnabled(false);
                }
            }
            $this->msgInscrit->Text = Prado::localize('TEXT_MON_COMPTE_PERSONNEL');
        }
    }

    /**
     * Permet de afficher le mode login.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function goToModeLoginPassword()
    {
        $this->panelChoixLoginMdp->style = 'display:none';
        $this->panelChoixCert->setVisible(true);
        $this->CaseLogin->SetChecked(true);
        $this->password->Text = '';
        $this->confPassword->Text = '';
        $this->passwordValidator->SetEnabled(true);
        $Text = '<script>';
        $Text .= "document.getElementById('panelAuthLoginPass').style.display = '';";
        $Text .= "document.getElementById('div_cn_certif').style.display = 'none';";
        $Text .= "document.getElementById('div_ac_certif').style.display = 'none';";
        $Text .= '</script>';
        $this->JSCert->Text = $Text;
    }

    /**
     * Chargement des champs de l'inscrit(Compte presonnel).
     */
    public function displayInscrit()
    {
        $inscrit = $this->getInscrit();
        if ($inscrit) {
            $this->nomPerso->Text = $inscrit->getNom();
            $this->prenomPerso->Text = $inscrit->getPrenom();
            $this->emailPersonnel->Text = $inscrit->getEmail();
            $this->identifiant->Text = $inscrit->getLogin();
            $this->identifiant->setEnabled(false);
            $this->telPersonnel->Text = $inscrit->getTelephone();
            $this->faxPersonnel->Text = $inscrit->getFax();
            if ('1' == $inscrit->getProfil()) {
                $this->inscritSimple->Checked = true;
            } else {
                $this->adminEntreprise->Checked = true;
            }
        }
    }

    /**
     * Modifier compte Inscrit.
     *
     * @return -1 si erreur, >=0 si OK
     */
    public function updateCompte($inscrit)
    {
        if ($inscrit) {

            $inscrit->setNom($this->nomPerso->Text);
            $inscrit->setPrenom($this->prenomPerso->Text);
            $inscrit->setEmail(trim($this->emailPersonnel->Text));
            $inscrit->setTelephone($this->telPersonnel->Text);
            $inscrit->setFax($this->faxPersonnel->Text);

            //on ne met la date à jour de validation RGPD que si un de 3 choix a été changé. Un test étant fait pour cette condition on set tout le rgpd dans le if
            if (
                $inscrit->getRgpdCommunicationPlace() !== $this->communicationPlaceRgpd->Checked ||
                $inscrit->getRgpdEnquete() !== $this->enqueteRgpd->Checked ||
                $inscrit->getRgpdCommunication() !== $this->communicationRgpd->Checked
            ) {
                $inscrit
                    ->setDateValidationRgpd(new \DateTime())
                    ->setRgpdEnquete($this->enqueteRgpd->Checked)
                    ->setRgpdCommunicationPlace($this->communicationPlaceRgpd->Checked)
                    ->setRgpdCommunication($this->communicationRgpd->Checked);
            }

            if (true == $this->CaseLogin->Checked) {
                $inscrit->setLogin($this->identifiant->Text);
                if (Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->password->Text
                    && '' != $this->password->Text
                    && '' != $this->confPassword->Text
                    && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->confPassword->Text
                ) {
                    Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $this->password->getText());
                }
                $inscrit->setCert(null);
                $inscrit->setNumCert(null);
            } elseif ((false == $this->CaseLogin->Checked)
                && ((Atexo_CurrentUser::readFromSession('userCert')) !== null)
                && (null !== Atexo_CurrentUser::readFromSession('userCert'))
            ) {
                $inscrit->setCert(Atexo_CurrentUser::readFromSession('userCert'));
                $inscrit->setNumCert(Atexo_CurrentUser::readFromSession('userSerial'));
                $inscrit->setLogin(null);
                $inscrit->setMdp(null);
            }
            if (true == $this->inscritSimple->Checked) {
                $inscrit->setProfil('1'); //UES
            } elseif (true == $this->adminEntreprise->Checked) {
                $inscrit->setProfil('2'); //ATES
            }

            $inscrit->setDateModification(date('Y-m-d H:i:s'));
            (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($inscrit, $this->Page->etablissements->getSiretMonEtablissement(), Atexo_CurrentUser::getIdEntreprise());

            return $inscrit;
        } else {
            return false;
        }
    }

    /**
     * Permet d'activer ou desactiver le profil (inscrit simple ou admin) de l'utilsateur
     *    Profil actif si l'utilisateur n'est pas le seul administrateur de l'entreprise
     *    Profil inactif si l'utilisateur est le seul administrateur de l'entreprise.
     *
     * @return bool
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isProfilActif()
    {
        if (('creationByATES' == $_GET['action'] && !isset($_GET['id'])) || ('ChangingUser' == $_GET['action'] && isset($_GET['id']) && $_GET['id'] != Atexo_CurrentUser::getIdInscrit())) {
            return true;
        }
        if ((new Atexo_Entreprise_Inscrit())->isInscritDernierAdmin()) {
            return false;
        }

        return true;
    }

    public function verifierMdp($sender, $param)
    {
        if (Atexo_CurrentUser::PASSWORD_INIT_VALUE !== $this->password->text && '' !== $this->password->text) {
            $param->IsValid = preg_match(Atexo_CurrentUser::PASSWORD_FORMAT, $this->password->text);
            $this->Page->passwordModificationValidator->ErrorMessage = Prado::localize('REGLE_VALIDATION_MDP');
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
        }
    }

    public function checkOldPassword($sender, $param)
    {
        if (Atexo_CurrentUser::PASSWORD_INIT_VALUE !== $this->password->text && '' !== $this->password->text) {
            $encodedPassword = Atexo_CurrentUser::getPassword();
            $encoder = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class);
            $param->IsValid = $encoder->isPasswordValid(Inscrit::class, $encodedPassword, $this->oldPassword->text);
            $this->Page->oldPasswordModificationValidator->ErrorMessage = Prado::localize('OLD_PASSWORD_TEXT');
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
        }
    }
}
