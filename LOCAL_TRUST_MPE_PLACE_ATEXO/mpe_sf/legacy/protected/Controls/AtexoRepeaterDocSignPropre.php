<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_ValeursReferentielles;

class AtexoRepeaterDocSignPropre extends MpeTTemplateControl
{
    public bool $alloti = false;
    public $typeReponse;
    public string $libelleAutrePiece = 'AUTRES_PIECES_DOSSIER_ZIP';

    public function setAlloti($value)
    {
        $this->alloti = $value;
    }

    public function getAlloti()
    {
        return $this->alloti;
    }

    public function setTypeReponse($value)
    {
        $this->typeReponse = $value;
    }

    public function getTypeReponse()
    {
        return $this->typeReponse;
    }

    public function setLibelleAutrePiece($value)
    {
        $this->libelleAutrePiece = $value;
    }

    public function getLibelleAutrePiece()
    {
        return $this->libelleAutrePiece;
    }

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            self::initRepeaterPieces();
        }
    }

    public function initRepeaterPieces()
    {
        if ($this->alloti) {
            $max = Atexo_Config::getParameter('NBRE_INITIAL_DOC_REPONSE_PROPRE_ALLOTI');
        } else {
            $max = Atexo_Config::getParameter('NBRE_INITIAL_DOC_REPONSE_PROPRE');
        }
        $dataPieces = [];
        for ($indexPiece = 0; $indexPiece < $max; ++$indexPiece) {
            $dataPieces[$indexPiece]['fichierPropre'] = '';
        }
        $this->listePiecesRepeater->dataSource = $dataPieces;
        $this->listePiecesRepeater->dataBind();
        $this->nbrePiecesAjouter->text = 0;
    }

    public function traceOperationInscrit($sender, $param)
    {
        if ($this->Page->_consultation) {
            $ref = $this->Page->_consultation->getReference();
            $org = $this->Page->_consultation->getOrganisme();
            $dateDebAction = date('Y-m-d H:i:s');
            $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION13', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
            $this->traceOperationsInsrctis($IdDesciption, $dateDebAction, '', $ref, $org);
        }
    }

    /*
       * Permet de tracer l'acces de l'inscrit à la PF
       */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '')
    {
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = $description;
        if (!$details) {
            $details = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $details,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }
}
