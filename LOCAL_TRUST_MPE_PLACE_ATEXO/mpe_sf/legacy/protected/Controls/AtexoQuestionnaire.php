<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_FormConsultation;

/*
 * Created on 20 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */
class AtexoQuestionnaire extends MpeTTemplateControl
{
    public $_idFormulaireConsultation;
    public $_postBack;
    public $_evaluation;
    public $_idEnveloppe;
    public $_idInscrit;

    public function getIdFormulaireconsultation()
    {
        return $this->_idFormulaireConsultation;
    }

    public function setIdFormulaireconsultation($value)
    {
        $this->_idFormulaireConsultation = $value;
    }

    public function getIdEnveloppe()
    {
        return $this->_idEnveloppe;
    }

    public function setIdEnveloppe($value)
    {
        $this->_idEnveloppe = $value;
    }

    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    public function setIdInscrit($value)
    {
        $this->_idInscrit = $value;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setDataSource($value)
    {
        $this->_dataSource = $value;
    }

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setEvaluation($value)
    {
        $this->_evaluation = $value;
    }

    public function getEvaluation()
    {
        return $this->_evaluation;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $data = $this->retreiveDataRepeaterQuestionnaire($this->getIdFormulaireconsultation(), $this->getEvaluation(), $this->getIdEnveloppe(), $this->getIdInscrit());
            $this->remplirRepeaterQuestionnaireItems($data);
        }
    }

    public function retreiveDataRepeaterQuestionnaire($idFormCons, $evaluation, $idEnv, $idInscrit)
    {
        $data = (new Atexo_FormConsultation())->getListItemsByIdFormConsultation($idFormCons, $evaluation, $idEnv, $idInscrit);

        return $data;
    }

    public function remplirRepeaterQuestionnaireItems($data)
    {
        $this->repeaterQuestionnaireItems->DataSource = $data;
        $this->repeaterQuestionnaireItems->dataBind();
    }
}
