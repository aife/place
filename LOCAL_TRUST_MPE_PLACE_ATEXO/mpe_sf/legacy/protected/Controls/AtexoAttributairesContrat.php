<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Contrat\Atexo_Contrat_AttributaireVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Le bloc des attributaires.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-roadmap
 *
 * @copyright Atexo 2015
 */
class AtexoAttributairesContrat extends MpeTTemplateControl
{
    public static int $myIndexAttributaire = 0;

    /**
     * Permet de initialiser le parametre type contrat.
     *
     * @param int $v le type de contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function setTypeContrat($v)
    {
        $this->setViewState('typeContrat', $v);
    }

    /**
     * Permet de recuperer le parametre type contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getTypeContrat()
    {
        return $this->getViewState('typeContrat');
    }

    /**
     * Permet de setter la data source des attributaires.
     *
     * @param array $v data source
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function setDataSourceAttributaire($v)
    {
        $this->setViewState('dataSourceAttributaire', $v);
    }

    /**
     * Permet de recuperer la data source des attributaires.
     *
     * @return array data source
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getDataSourceAttributaire()
    {
        return $this->getViewState('dataSourceAttributaire');
    }

    /**
     * Permet d'initialiser le composant selon le type de contrta.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initialiserComposant()
    {
        $idTypeContrat = $this->getTypeContrat();
        if ($idTypeContrat && !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($idTypeContrat)) {
            $dataSource = self::getDataSourceAttributaire();
            if (is_array($dataSource) && count($dataSource)) {
                $this->panelActionAjoutAttributaire->setDisplay('None');
            } else {
                $this->panelActionAjoutAttributaire->setDisplay('Dynamic');
            }
        } else {
            $this->panelActionAjoutAttributaire->setDisplay('Dynamic');
        }
        self::chargerCategorieEntreprisePME();
    }

    /**
     * Permet de remplir le repeater des attributaire by dataSource.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function populateData()
    {
        $dataSource = $this->getDataSourceAttributaire();
        $this->repeaterAttributaires->DataSource = $dataSource;
        $this->repeaterAttributaires->DataBind();
        self::initialiserComposant();
    }

    /**
     * Permet de sauvgarder l'attributaire si c'est un nouveau attributaire si non il le modifier.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function saveAttributaire($sender, $param)
    {
        $company = null;
        $etablissementVo = null;
        if ($this->Page->isValid) {
            $script = '';

            $idEntreprise = $this->idEntreprise->value;
            $idEtablissement = $this->idEtablissement->value;
            if ($idEntreprise) {
                $company = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
                $etablissementQuery = new CommonTEtablissementQuery();
                $etablissement = $etablissementQuery->getEtabissementbyIdEseAndIdEtab($idEtablissement, $idEntreprise);
                if ($etablissement instanceof CommonTEtablissement) {
                    $etablissementVo = $etablissement->getEtablissementVo();
                }
            } else {
                if (!$this->attributaireEtranger->checked) {
                    $siren = $this->siren->getSafeText();
                    $siret = $this->siret->getSafeText();
                    $company = (new Atexo_Entreprise())->retrieveCompanyVoBySiren($siren);
                    if (!$company) {
                        $company = new Atexo_Entreprise_EntrepriseVo();
                        $etablissementVo = new Atexo_Entreprise_EtablissementVo();
                        self::fillEntrepriseByData($company, $etablissementVo, $siren, $siret);
                    } else {
                        $etablissementVo = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);
                    }
                } elseif ($this->attributaireEtranger->checked) {
                    $idNational = $this->idNational->getSafeText();
                    if ((0 != $this->pays->SelectedIndex) && '' != $idNational) {
                        $pays = $this->pays->getSelectedItem()->getText();
                        $company = (new Atexo_Entreprise())->retrieveCompanyVoByIdNational($idNational, $pays);
                        if (!$company) {
                            $company = new Atexo_Entreprise_EntrepriseVo();
                            $etablissementVo = new Atexo_Entreprise_EtablissementVo();
                            self::fillEntrepriseByData($company, $etablissementVo, null, null, $idNational, $pays, (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCodeCountryByLibelle((new Atexo_Config())->toPfEncoding($pays)));
                        } else {
                            $etablissementVo = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company);
                        }
                    }
                }
            }
            $dataSourceAttributaire = $this->getDataSourceAttributaire();
            if ($this->indexAttributaire->value) {
                $attributaire = self::getAttributaireByIndex($this->indexAttributaire->value);
                $dataSourceAttributaire = $this->getNewDataSourceAfterDeleteByIndex($this->indexAttributaire->value);
            } else {
                $attributaire = new Atexo_Contrat_AttributaireVo();
                $indexNextElement = $this->getIndexLastElement($dataSourceAttributaire);
                $attributaire->setIndex($indexNextElement);
            }
            self::fillAttributaire($attributaire, $company, $etablissementVo);
            $dataSourceAttributaire[] = $attributaire;

            $this->setDataSourceAttributaire($dataSourceAttributaire);
            self::populateData();
            self::effacerDonneeEntrepriseAttributaire($script, $param);
            $script .= "J('.modal-attributaire').dialog('destroy');";
            $this->scriptLable->Text = '<script>'.$script.'</script>';
        }
    }

    /**
     * Permet de remplire un attributaire par les données à partir du modal.
     *
     * @param Atexo_Contrat_AttributaireVo     $attributaire    l'attributaire
     * @param Atexo_Entreprise_EntrepriseVo    $company         l'entreprise
     * @param Atexo_Entreprise_EtablissementVo $etablissementVo l'etablissement
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillAttributaire(&$attributaire, $company, $etablissementVo)
    {
        if ($attributaire instanceof Atexo_Contrat_AttributaireVo) {
            $attributaire->setEntrepriseVo($company);
            $attributaire->setEtablissementVo($etablissementVo);
            $attributaire->setNomContact($this->nom->getSafeText());
            $attributaire->setPrenomContact($this->prenom->getSafeText());
            $attributaire->setEmailContact($this->email->getSafeText());
            $attributaire->setTelephoneContact($this->tel->getSafeText());
            $attributaire->setFaxContact($this->fax->getSafeText());
        }
    }

    /**
     * Permet de remplir l'entreprise et l'etablissement avec les donnees.
     *
     * @param int             $idOffre           l'id offre
     * @param EtablissementVo $newEtablissement  l'etablissement
     * @param EntrepriseVo    $newCompany        l'entreprise
     * @param string          $siren             le siren
     * @param string          $siret             le siret
     * @param string          $idNational        l'id national
     * @param string          $payEnregistrement pays d'enregistrement
     * @param string          $payAdresse        pays d'adresse
     * @param string          $acronymePays      l'acronyme pays
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillEntrepriseByData(&$newCompany, &$newEtablissement, $siren, $siret, $idNational = null, $payEnregistrement = null, $payAdresse = null, $acronymePays = null)
    {
        if ($siren) {
            $newCompany->setSiren($siren);
            $payEnregistrement = Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            $payAdresse = Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE');
            $acronymePays = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
        }
        if ($siret) {
            $newCompany->setNicsiege($siret);
        }
        if ($idNational) {
            $newCompany->setSirenetranger($idNational);
        }
        $newCompany->setNom($this->nomEntreprise->getSafeText());
        $newCompany->setCodepostal($this->cp->getSafeText());
        $newCompany->setVilleadresse($this->ville->getSafeText());
        $newCompany->setPaysenregistrement($payEnregistrement);
        $newCompany->setPaysadresse($payAdresse);
        $newCompany->setAcronymePays($acronymePays);
        $newCompany->setDateModification(date('Y-m-d H:i:s'));
        $newCompany->setDateCreation(date('Y-m-d H:i:s'));
        $newCompany->setSaisieManuelle('1');
        $newCompany->setCreatedFromDecision('1');
        if ('1' === $this->categorieEntrepriseListe->getSelectedValue()) {
            $newCompany->setCategorieEntreprise(Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME'));
        }
        //$newEtablissement->setCodepostal($this->cp->getSafeText());
        //$newEtablissement->setVille($this->ville->getSafeText());
        self::fillEtablissementByData($newEtablissement, $newCompany, $siret, $idNational, $payEnregistrement);
        $newCompany->setListeEtablissements([$newEtablissement]);
    }

    /**
     * Permet de remplir l'etablissement avec les donnees.
     *
     * @param int             $idOffre           l'id offre
     * @param EtablissementVo $newEtablissement  l'etablissement
     * @param string          $siret             le siret
     * @param string          $idNational        l'id national
     * @param string          $payEnregistrement pays d'enregistrement
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillEtablissementByData(&$newEtablissement, &$newCompany, $siret, $idNational = null, $payEnregistrement = null)
    {
        if ($siret) {
            $newEtablissement->setCodeEtablissement($siret);
        }
        if ($idNational) {
            $newEtablissement->setCodeEtablissement('1');
        }
        $newEtablissement->setPays($payEnregistrement);
        $newEtablissement->setDateCreation(date('Y-m-d H:i:s'));
        $newEtablissement->setDateModification(date('Y-m-d H:i:s'));
        $newEtablissement->setSaisieManuelle('1');
        $newEtablissement->setEstSiege('1');
        $newCompany->setNicSiege($newEtablissement->getCodeEtablissement());
    }

    /**
     * Permet de synchroniser avec Api Gouv Entreprise
     * Si l'entreprise existe dans la BD on le recuperer si non on passe à SGMAP.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function synchroniserAvecApiGouvEntreprise($sender, $param)
    {
        $siren = $this->siren->getSafeText();
        $siret = $this->siret->getSafeText();
        $company = Atexo_Entreprise::retrieveCompanyBySiren($siren);
        if (!$company) {
            //SynchronisationSGMAP
            if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                $company = Atexo_Entreprise::recupererEntrepriseApiGouvEntreprise($siren, true);
                if ($company) {
                    $listeEtablissements = $company->getEtablissements();
                    $newEtablissement = is_array($listeEtablissements) ? array_pop($listeEtablissements) : '';
                    if ($company->getNicsiege() != $siret) {
                        $newEtablissement = (new Atexo_Entreprise_Etablissement())->recupererEtablissementApiGouvEntreprise($siret);
                        if ($newEtablissement) {
                            $newEtablissement->setSaisieManuelle('0');
                            $company->setEtablissements($newEtablissement);
                        }
                    }
                    $company->setListeEtablissements($company->getEtablissements());
                    $company->setSaisieManuelle('0');
                    $company->setDateModification(date('Y-m-d H:i:s'));
                    $company->setDateCreation(date('Y-m-d H:i:s'));
                    $company->setCreatedFromDecision('1');
                    $company = (new Atexo_Entreprise())->save($company);
                }
            }
        }

        if ($company && $company->getId()) {
            $this->idEntreprise->value = $company->getId();
            $etablissementVo = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);
            if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                $this->idEtablissement->value = $etablissementVo->getIdEtablissement();
            }
            $this->idIdentificationEntreprise->setIdEntreprise($company->getId());
            $this->idIdentificationEntreprise->initialiserComposant($param);
            $this->panelInfoEntrepriseAttributaire->setDisplay('None');
        } else {
            $this->idEntreprise->value = '';
            $this->idEtablissement->value = '';
            $this->idIdentificationEntreprise->setIdEntreprise(0);
            $this->idIdentificationEntreprise->initialiserComposant($param);
            $this->panelInfoEntrepriseAttributaire->setDisplay('Dynamic');
        }
    }

    /**
     * Permet d'effacer les données de modal d'ajout d'un attributaire.
     *
     * @param string $script le script
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function effacerDonneeEntrepriseAttributaire(&$script, $param = null)
    {
        $this->panelInfoEntrepriseAttributaire->setDisplay('Dynamic');
        $this->idIdentificationEntreprise->setIdEntreprise(null);
        $this->idIdentificationEntreprise->initialiserComposant($param);
        $this->idEntreprise->value = '';
        $this->idEtablissement->value = '';
        $this->attributaireNationale->checked = true;
        $script .= "isCheckedShowDiv(document.getElementById('".$this->attributaireNationale->getClientId()."'),'etablieFrance');";
        $script .= "enabledCodePostalAttributaire('');";
        $script .= "isCheckedHideDiv(document.getElementById('".$this->attributaireNationale->getClientId()."'),'nonEtablieFrance');J('#cp').removeAttr('disabled','disabled').removeClass('disabled');";
        $this->attributaireEtranger->checked = false;
        $this->pays->setSelectedValue('0');
        $this->idNational->Text = '';
        $this->siren->Text = '';
        $this->siret->Text = '';
        $this->nomEntreprise->Text = '';
        $this->cp->Text = '';
        $this->ville->Text = '';
        $this->nom->Text = '';
        $this->prenom->Text = '';
        $this->email->Text = '';
        $this->tel->Text = '';
        $this->fax->Text = '';
        $this->indexAttributaire->value = 0;
    }

    /**
     * Permet de annuler l'ajout ou la modification d'un attributaire.
     *
     * @param $param
     * @param $sender
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function annulerAttributaire($sender, $param)
    {
        $script = '';
        $this->scriptLable->Text = "";
        $this->effacerDonneeEntrepriseAttributaire($script, $param);
        $script .= "J('.modal-attributaire').dialog('destroy');";
        $this->scriptLable->Text = '<script>'.$script.'</script>';
    }

    /**
     * Permet de valider la liste des attributaires selon le type de contrat.
     *
     * @param bool   $isvalid
     * @param string $script
     * @param array  $arrayMsgErreur
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerInfoAttributaire(&$isValid, &$script, &$arrayMsgErreur)
    {
        $idTypeContrat = $this->getTypeContrat();
        if ($idTypeContrat) {
            $dataSource = self::getDataSourceAttributaire();
            if (!$dataSource || (is_array($dataSource) && 0 == count($dataSource))) {
                $arrayMsgErreur[] = Prado::localize('AJOUTER_AU_MOINS_UN_ATTRIBUTAIRE');
                $script .= "document.getElementById('".$this->ImgErrorAttributaire->getClientId()."').style.display = '';";
                $isValid = false;
            } else {
                if (!(new Atexo_Consultation_Contrat())->isTypeContratMultiById($idTypeContrat) && is_array($dataSource) && count($dataSource) > 1) {
                    $arrayMsgErreur[] = Prado::localize('AJOUTER_UN_SEUL_ATTRIBUTAIRE');
                    $script .= "document.getElementById('".$this->ImgErrorAttributaire->getClientId()."').style.display = '';";
                    $isValid = false;
                } else {
                    $script .= "document.getElementById('".$this->ImgErrorAttributaire->getClientId()."').style.display = 'none';";
                }
            }
        } else {
            $script .= "document.getElementById('".$this->ImgErrorAttributaire->getClientId()."').style.display = 'none';";
        }
    }

    /**
     * Permet de supprimer un attributaire donnée.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function deleteAttributaire($sender, $param)
    {
        $index = $param->CallbackParameter;
        $indexAttributaire = 0;
        $dataAttributairesNew = [];
        $dataAttributairesOld = $this->getDataSourceAttributaire();
        $dataAttributairesNew = $this->getNewDataSourceAfterDeleteByIndex($index);
        $this->setDataSourceAttributaire($dataAttributairesNew);
        self::populateData();
    }

    /**
     * Permet de recuperer le dernier index des elements attributaires.
     *
     * @param array $data
     *
     * @return int $index le prochain index
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getIndexLastElement($data)
    {
        // TODO des test sur les objets
        $index = 0;
        if (is_array($data) && count($data)) {
            $index = $data[0]->getIndex();
            for ($i = 1; $i < count($data); ++$i) {
                $index2 = $data[$i]->getIndex();
                if ($index <= $index2) {
                    $index = $index2;
                }
            }
        }

        return $index + 1;
    }

    /**
     * Permet de valider les information d'un attributaire
     * Dans ce cas on fait la validation pour ne pas ajouter un attributaire qui existe déjà.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerDonneesAttributaireDuplicate($sender, $param)
    {
        $isValid = true;
        $arrayMsgErreur = [];
        $dataSource = $this->getDataSourceAttributaire();
        if ($this->indexAttributaire->value) {
            $index = $this->indexAttributaire->value;
            $dataSource = self::getNewDataSourceAfterDeleteByIndex($index);
        }
        if (is_array($dataSource) && count($dataSource)) {
            if ($this->attributaireNationale->checked) {
                $siren = $this->siren->getSafeText();
            } elseif ($this->attributaireEtranger->checked) {
                $idNational = $this->idNational->getSafeText();
            }

            if ($this->idEntreprise->value) {
                $idEntreprise = $this->idEntreprise->value;
            }

            foreach ($dataSource as $oneAttributaire) {
                if ($oneAttributaire instanceof Atexo_Contrat_AttributaireVo) {
                    $entreprise = $oneAttributaire->getEntrepriseVo();
                    if (!$idEntreprise) {
                        if ($this->attributaireNationale->checked && $siren == $entreprise->getSiren()) {
                            $isValid = false;
                        } elseif ($this->attributaireEtranger->checked && $idNational == $entreprise->getSirenetranger()) {
                            $isValid = false;
                        }
                    } elseif ($idEntreprise == $entreprise->getId()) {
                        $isValid = false;
                    }
                }
            }
        }
        if (!$isValid) {
            $arrayMsgErreur[] = Prado::Localize('ATTRIBUTAIRE_EXISTE');
        }
        self::afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur);
    }

    /**
     * Permet d'afficher les messages d'erreur en cas de validation serveur.
     *
     * @param $param
     * @param $isValid
     * @param $arrayMsgErreur
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function afficherMessagesValidationServeur($param, $isValid, $arrayMsgErreur)
    {
        $script = '';
        if ($isValid) {
            $param->IsValid = true;
            $script .= "document.getElementById('divValidationSummaryAddAttributaire').style.display='none'";
            $this->scriptLable->Text = '<script>'.$script.'</script>';
        } else {
            $param->IsValid = false;
            $script .= "document.getElementById('divValidationSummaryAddAttributaire').style.display='';";
            $this->scriptLable->Text = "<script>$script</script>";

            if (is_array($arrayMsgErreur) && count($arrayMsgErreur)) {
                $errorMsg = implode('</li><li>', $arrayMsgErreur);
                $errorMsg = $errorMsg.'</li>';
                $this->divValidationSummaryAddAttributaire->addServerError($errorMsg, false);
            }

            return;
        }
    }

    /**
     * Permet d'afficher le modal pour modifier l'attributaire.
     *
     * @param $param
     * @param $arrayMsgErreur
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function updateAttributaire($sender, $param)
    {
        $index = $param->CallbackParameter;
        $script = '';
        $attributaireToUpadate = self::getAttributaireByIndex($index);
        if ($attributaireToUpadate instanceof Atexo_Contrat_AttributaireVo) {
            self::fillModalAttributaire($attributaireToUpadate, $script, $param);
            $script .= "openModal('modal-attributaire','popup-moyen1',document.getElementById('".$this->titrePopin->getClientId()."'));";
            $this->scriptLable->Text = '<script>'.$script.'</script>';
        }
    }

    /**
     * Permet de remplire le formulaire d'ajout d'attributaire par les données d'un attributaire.
     *
     * @param Atexo_Contrat_AttributaireVo $attributaire
     * @param string                       $script
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillModalAttributaire($attributaire, &$script, $param)
    {
        if ($attributaire instanceof Atexo_Contrat_AttributaireVo) {
            $this->indexAttributaire->value = $attributaire->getIndex();
            $entreprise = $attributaire->getEntrepriseVo();
            if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
                $idEntreprise = $entreprise->getId();
                $this->idIdentificationEntreprise->setIdEntreprise($idEntreprise);
                $this->idIdentificationEntreprise->initialiserComposant($param);
                if ($idEntreprise) {
                    $this->panelInfoEntrepriseAttributaire->setDisplay('None');
                    $this->idEntreprise->value = $idEntreprise;
                } else {
                    $this->panelInfoEntrepriseAttributaire->setDisplay('Dynamic');
                    if ((new Atexo_Entreprise())->isEntrepriseLocale($entreprise)) {
                        $this->attributaireNationale->checked = true;
                        $this->attributaireEtranger->checked = false;
                        $script .= "isCheckedShowDiv(document.getElementById('".$this->attributaireNationale->getClientId()."'),'etablieFrance');";
                        $script .= "isCheckedHideDiv(document.getElementById('".$this->attributaireNationale->getClientId()."'),'nonEtablieFrance');J('#cp').removeAttr('disabled','disabled').removeClass('disabled');";
                        $this->pays->setSelectedValue('0');
                        $this->idNational->Text = '';
                    } else {
                        $this->attributaireNationale->checked = false;
                        $this->attributaireEtranger->checked = true;
                        $script .= "isCheckedShowDiv(document.getElementById('".$this->attributaireEtranger->getClientId()."'),'nonEtablieFrance');";
                        $script .= "isCheckedHideDiv(document.getElementById('".$this->attributaireEtranger->getClientId()."'),'etablieFrance');J('#cp').attr('disabled','disabled').addClass('disabled');";

                        $this->pays->setSelectedValue((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdCountry($entreprise->getPaysenregistrement()));
                        $this->idNational->Text = $entreprise->getSirenetranger();
                        $this->siren->Text = '';
                    }
                    $this->siren->Text = $entreprise->getSiren();
                    $this->nomEntreprise->Text = $entreprise->getNom();
                    $this->cp->Text = $entreprise->getCodepostal();
                    $this->ville->Text = $entreprise->getVilleadresse();
                    if ($entreprise->getCategorieEntreprise() == Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME')) {
                        $this->categorieEntrepriseListe->setSelectedValue('1');
                    } else {
                        $this->categorieEntrepriseListe->setSelectedValue('0');
                    }
                }
            }
            $etablissement = $attributaire->getEtablissementVo();
            if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
                $idEtablissement = $etablissement->getIdEtablissement();
                if ($idEtablissement) {
                    $this->idEtablissement->value = $idEtablissement;
                }
                if ($etablissement->getCodeEtablissement() && '1' !== $etablissement->getCodeEtablissement()) {
                    $this->siret->Text = $etablissement->getCodeEtablissement();
                }
            }
            $this->nom->Text = $attributaire->getNomContact();
            $this->prenom->Text = $attributaire->getPrenomContact();
            $this->email->Text = $attributaire->getEmailContact();
            $this->tel->Text = $attributaire->getTelephoneContact();
            $this->fax->Text = $attributaire->getFaxContact();
        }
    }

    /**
     * Permet de recuperer l'attributaire by son index.
     *
     * @param int $index l'index
     *
     * @return Atexo_Contrat_AttributaireVo $attributaire l'attributaire
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getAttributaireByIndex($index)
    {
        $attributaire = null;
        $dataAttributaires = $this->getDataSourceAttributaire();
        if (is_array($dataAttributaires)) {
            foreach ($dataAttributaires as $oneAttributaire) {
                if ($oneAttributaire->getIndex() == $index) {
                    $attributaire = $oneAttributaire;
                }
            }
        }

        return $attributaire;
    }

    /**
     * Permet de supprimer un attributaire du dataSource by son index.
     *
     * @param int $index l'index
     *
     * @return array of Atexo_Contrat_AttributaireVo
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getNewDataSourceAfterDeleteByIndex($index)
    {
        $dataAttributairesNew = [];
        $dataAttributairesOld = $this->getDataSourceAttributaire();
        if (is_array($dataAttributairesOld)) {
            foreach ($dataAttributairesOld as $oneAttributaire) {
                if ($oneAttributaire->getIndex() != $index) {
                    $dataAttributairesNew[] = $oneAttributaire;
                }
            }
        }

        return $dataAttributairesNew;
    }

    /**
     * Chargement des categroies entreprise.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function chargerCategorieEntreprisePME()
    {
        $data = ['0' => Prado::localize('TEXT_SELECTIONNER'), '1' => Prado::Localize('OUI_MAJ'), '2' => Prado::Localize('NON_MAJ')];
        $this->categorieEntrepriseListe->DataSource = $data;
        $this->categorieEntrepriseListe->dataBind();
    }
}
