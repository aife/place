<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonRG;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Prado\Prado;

/*
 * Created on 27 dec. 2011
 * Composant de gestion de dce cas modification (sans redac)
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class AtexoDCE extends MpeTPage
{
    public $_reference;
    public $_dce = '';
    public $_rg = '';
    public $_consultation;
    public string $_nomFileDce = '';
    public string $_pathNewDCE = '';
    public string $_fileAdddedName = '';
    public $_fileAddedPath = '';
    public string $_fileAddedPathTmp = '';
    public string $_infileRg = '';
    public string $_fileNameRg = '';
    public string $_NewZipDCE = '';
    public $_fileToScan = '';
    public bool $useUplodeBar = true;
    public $prefix = '';
    public string $statut = '';
    public string $_oldfileName = '';

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    public function getConsultation()
    {
        if (!($this->_consultation instanceof CommonConsultation)) {
            $this->_consultation = $this->Page->getConsultation();
        }

        return $this->_consultation;
    }

    public function getPrefix()
    {
        return $this->prefix;
    }

    public function setPrefix($v)
    {
        $this->prefix = $v;
    }

    public function getUseUplodeBar()
    {
        return $this->useUplodeBar;
    }

    public function setUseUplodeBar($v)
    {
        $this->useUplodeBar = $v;
    }

    public function refrechCompsant()
    {
        $this->uploadBar->refrechCompsant();
        $this->uploadBarNewPieceDce->refrechCompsant();
    }

    public function onLoad($param)
    {
           //Masquage du message d'information sur l'élément MPS
        if (Atexo_Module::isEnabled('MasquerElementsMps')) {
            $this->activeInfoMsg->setVisible(false);
        }

        if ('Agent.FormulaireConsultation' == $_GET['page']) {
            $this->prefix = 'ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_blocDce_';
        } else {
            $this->prefix = 'ctl0_CONTENU_PAGE_blocDce_';
        }
        if ($this->getConsultation() instanceof CommonConsultation) {
            $this->gestionAffichageReglementConsultationInfoMsg();
            $this->hasRg->setValue('0');
            $this->_reference = $this->_consultation->getId();
            $this->_dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, Atexo_CurrentUser::getOrganismAcronym());
            if ((!$this->_dce) || ('0' == $this->_dce->getDce())) {
                $this->panelAjoutPieceDce->SetEnabled(false);
                $this->uploadBarNewPieceDce->SetEnabled(false);
                $this->popupUpdateDCE->SetVisible(false);
                $this->desactivePopupUpdateDCE->SetVisible(true);
                $this->panelRemplacerPieceDce->SetEnabled(false);
            } else {
                $this->desactivePopupUpdateDCE->SetVisible(false);
            }
            $this->_rg = (new Atexo_Consultation_Rg())->getReglement($this->_reference, Atexo_CurrentUser::getCurrentOrganism());
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->_consultation->getIdTypeProcedureOrg());
            if (Atexo_Module::isEnabled('ReglementConsultation') && $procEquivalence instanceof CommonProcedureEquivalence) {
                if ('+1' === $procEquivalence->getReglementCons()
                       || '+0' === $procEquivalence->getReglementCons()
                       || '0' === $procEquivalence->getReglementCons()
                       || '1' === $procEquivalence->getReglementCons()) {
                    $this->panelRg->Attributes->Style = 'display:block';
                } else {
                    $this->panelRg->Attributes->Style = 'display:none';
                }
                if ('0' === $procEquivalence->getReglementCons()) {
                    $this->uploadBarNewReglement->SetEnabled(false);
                }
            }

            if (!$this->Page->IsPostBack) {
                if ($procEquivalence instanceof CommonProcedureEquivalence) {
                    $atexoProcEqui = new Atexo_Consultation_ProcedureEquivalence();
                    $atexoProcEqui->displayElement($this, 'telechargementPartiel', $procEquivalence->getPartialDceDownload());
                }
                if ('1' == $this->_consultation->getPartialDceDownload()) {
                    $this->telechargementPartiel->Checked = true;
                } else {
                    $this->telechargementPartiel->Checked = false;
                }
            }

            $this->setDownlodRgNavigateUrl($this->_reference);
            $this->setDownlodDceNavigateUrl($this->_reference);

            if ($this->_rg instanceof CommonRG) {
                $this->panelDownloadRg->setVisible(true);
            }
            if ($this->_dce instanceof CommonDCE) {
                $this->PanelTelechargementDce->setVisible(true);
            }
        }
    }

    public function setDownlodDceNavigateUrl($reference)
    {
        $this->linkDownloadDce->NavigateUrl = 'index.php?page=Agent.DownloadDce&id='.base64_encode($reference);
    }

    public function setDownlodRgNavigateUrl($reference)
    {
        $this->linkDownloadRg->NavigateUrl = 'index.php?page=Agent.DownloadReglement&id='.base64_encode($reference);
    }

    public function afficherErreur($msg)
    {
        $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
        $this->choixDceVerifyIntegrity->ErrorMessage = Prado::localize($msg);
    }

    public function verifyDceIntegrity()
    {
        if ($this->uploadBar->hasFile()) {
            $infileDce = $this->uploadBar->getLocalName();
            // tester l'integrite du fichier zip
            // -T revient au zip precedent en cas d'echec
            $cmd = 'nice zip -T '.$infileDce;
            exec($cmd, $output, $sys_answer);
            if (0 != $sys_answer) {
                return false;
            }
        }

        return true;
    }

	public function getNewZipDCE($faireCopierPiece=false)
        {
        $u = new Atexo_Util();
    	$newPathTmpDCE = '';
        if (true == $this->remplacerToutDce->Checked) {
            if ((!$this->_dce) || ('0' == $this->_dce->getDce())) {
                $this->statut = Atexo_Config::getParameter('AJOUT_FILE');
            } else {
                $this->statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            }
            if ($this->uploadBar->hasFile()) {
                if ('ZIP' == strtoupper(Atexo_Util::getExtension($this->uploadBar->getFileName()))) {
                    if ($this->verifyDceIntegrity()) {
                        $finalDce = Atexo_Config::getParameter('COMMON_TMP').'choixDce'.session_id().time();
                        $finalDceLocalName = $this->uploadBar->getLocalName();
                        $finalDceName = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->uploadBar->getFileName()));
                        $u->checkDceSize($finalDce);
                        if (rename($finalDceLocalName, $finalDce)) {
                            $newPathTmpDCE = $finalDce;
                            $this->_nomFileDce = $this->_fileToScan = $finalDceName;

                            return $newPathTmpDCE;
                        }
                    }
                }
            }
        } elseif (true == $this->ajoutPieceDce->Checked) {
            $this->statut = Atexo_Config::getParameter('AJOUT_FILE');
            $dce = $this->_dce;
            if ($this->uploadBarNewPieceDce->hasFile()) {
                $this->_fileAddedPath = $this->uploadBarNewPieceDce->getLocalName();
                if ($faireCopierPiece) {
                    $this->_fileAddedPathTmp = Atexo_Config::getParameter('COMMON_TMP').'tmp_Piece_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true));
                    copy($this->uploadBarNewPieceDce->getLocalName(), $this->_fileAddedPathTmp);
                }
                $fileName = $this->uploadBarNewPieceDce->getFileName();
                $fileName = trim(preg_replace('#[ ]{2,}#', ' ', $fileName));
                $this->_fileAdddedName = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($fileName));
                $finalDce = (new Atexo_Consultation_Dce())->updateSelectedFile($dce, false, $fileName, $this->uploadBarNewPieceDce->getLocalName(), Atexo_CurrentUser :: getOrganismAcronym());
					$this->_fileToScan = $fileName;
					$this->_nomFileDce = $dce->getNomDce();
                    $u->checkDceSize($finalDce);

                return $finalDce;
            }
        } elseif (true == $this->remplacerPieceDce->Checked) {
            $this->statut = Atexo_Config::getParameter('REMPLACEMENT_FILE');
            $dce = $this->_dce;
            $indexFileToChange = $this->indexOfFile->Value;
            $this->_fileAdddedName = $newFileToAdd = Atexo_Util::replaceCharactersMsWordWithRegularCharacters(Atexo_Util::OterAccents(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->newFileName->Value))));
            $tmpFilePath = $this->filePath->Value.'tmpDceFile';
            $this->_fileAddedPath = $tmpFilePath;
            if ($faireCopierPiece) {
                $this->_fileAddedPath = Atexo_Config::getParameter('COMMON_TMP').'tmp_Piece_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true));
                copy($tmpFilePath, $this->_fileAddedPath);
            }
            $finalDceName = $dce->getNomDce();
            $this->_fileToScan = $newFileToAdd;
            $this->_nomFileDce = $finalDceName;
            $this->_oldfileName = $this->hiddenOldFileName->value;
            $finalDce = (new Atexo_Consultation_Dce())->updateSelectedFile($dce, $indexFileToChange, $newFileToAdd, $tmpFilePath, Atexo_CurrentUser :: getOrganismAcronym());
                $u->checkDceSize($finalDce);

            return $finalDce;
        }

        return $newPathTmpDCE;
    }

    public function getInfoFileRG()
    {
        $retour = [];
        if ($this->uploadBarNewReglement->hasFile()) {
            $infileRg = Atexo_Config::getParameter('COMMON_TMP').'rg'.session_id().time();
            if (copy($this->uploadBarNewReglement->getLocalName(), $infileRg)) {
                $this->_infileRg = $infileRg;
                $this->_fileNameRg = $this->uploadBarNewReglement->getFileName();
                $retour['infile'] = $infileRg;
                $retour['fileName'] = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->uploadBarNewReglement->getFileName()));
            }
        }

        return $retour;
    }

    public function getInfoFilesAdded()
    {
        $retour = [];
        $i = 0;
        if ('' != $this->_infileRg) {
            $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP').'fileRG/'.session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
            Atexo_Files::createDir($pathDirectoryTmpDceFile);
            $infileRG = $pathDirectoryTmpDceFile.'tmpDceFile';
            Atexo_Blob::copyBlob($this->_infileRg, $infileRG, Atexo_CurrentUser::getCurrentOrganism());
            $retour[$i]['nameFile'] = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->_fileNameRg));
            $retour[$i]['pathFile'] = $infileRG;
            ++$i;
        }
        if (true == $this->ajoutPieceDce->Checked || true == $this->remplacerPieceDce->Checked) {
            $retour[$i]['nameFile'] = $this->_fileAdddedName;
            $retour[$i]['pathFile'] = $this->_fileAddedPath;
            if ($this->_fileAddedPathTmp) {
                $retour[$i]['pathFile'] = $this->_fileAddedPathTmp;
            }
            ++$i;
        }
        if (true == $this->remplacerToutDce->Checked) {
            $zip_finalDCE = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_dce_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
            Atexo_Blob::copyBlob($this->_NewZipDCE, $zip_finalDCE, Atexo_CurrentUser::getCurrentOrganism());

            $retour[$i]['nameFile'] = $this->_nomFileDce;
            $retour[$i]['pathFile'] = $zip_finalDCE;
        }

        return $retour;
    }

    public function getTextDce()
    {
        if ((!$this->_dce) || ('0' == $this->_dce->getDce())) {
            return Prado::localize('AJOUTER_UN_DCE');
        } else {
            return Prado::localize('REMPLACER_TOUT_PIECE_DCE');
        }
    }

    public function getIdBar()
    {
        if ($this->uploadBar->hasFile()) {
            return $this->uploadBar->ClientId;
        }

        return false;
    }

    public function hasFile()
    {
        if ($this->uploadBar->hasFile()) {
            return true;
        } elseif ($this->uploadBarNewPieceDce->hasFile()) {
            return true;
        }

        return false;
    }

    /**
     * Permet de verifier l'extension du DCE.
     *
     * @return bool true si le dce a l'extension zip si non on retourne false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifiyExtensionDce()
    {
        if ($this->uploadBar->hasFile() && 'ZIP' != strtoupper(Atexo_Util::getExtension($this->uploadBar->getFileName()))) {
            return false;
        }

        return true;
    }

    /**
     * Permet de gerer l'affichage du message informatif qui rappelle à l'agent qu'il est en procédure répondant au service MPS.
     *
     * @param $consultation: Objet consultation
     *
     * @return void
     */
    private function gestionAffichageReglementConsultationInfoMsg()
    {
        $this->activeInfoMsg->displayStyle = 'None';

        if ($this->_consultation instanceof CommonConsultation
            && Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism())
            && 1 == $this->_consultation->getMarchePublicSimplifie()
        ) {
            $this->activeInfoMsg->displayStyle = 'Dynamic';
        }
    }
}
