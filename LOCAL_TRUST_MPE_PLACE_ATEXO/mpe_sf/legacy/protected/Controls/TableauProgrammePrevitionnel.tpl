	<!--Debut Bloc Liste des marches-->
		<com:TActivePanel ID="panelListePPs" cssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
				<span class="title">
					<com:TTranslate>TEXT_LISTE_PP</com:TTranslate>
				</span>
			</div>
			<div class="content">
				<div class="table-bloc">
				<com:TRepeater ID="repeaterListePPs" >
						<prop:HeaderTemplate>
						<table class="table-results" summary="<%=Prado::localize('TEXT_LISTE_MARCHES')%>">
							<caption><com:TTranslate>TEXT_LISTE_MARCHES</com:TTranslate></caption>
							<thead>
								<tr>
									<th class="top" colspan="4"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
								</tr>
								<tr>
									<th class="col-450" id="nomFichier"><com:TTranslate>DEFINE_NOM_FICHIER</com:TTranslate></th>
									<th class="col-450" id="PathService" style="display:<%=($this->Page->tableauPP->getCalledFromAgent()=='true') ? 'none;':';'%>"><com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate></th>
									<th class="col-100" id="annee"><com:TTranslate>ANNEE</com:TTranslate></th>
									<th class="col-150" id="datePublication"><com:TTranslate>TEXT_DATE_PUBLICATION</com:TTranslate></th>
									<th  class="actions" id="supprFichier" style="display:<%=($this->Page->tableauPP->getCalledFromAgent()=='false') ? 'none;':';'%>">
										<com:TTranslate>DEFINE_SUPPRIMER</com:TTranslate>
									</th>
								</tr>
							</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
							<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
								<td class="col-690" headers="nomFichier">
									<a href="javascript:popUp('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.PopupListePPsDownloadFile&id=<%#$this->Data->getId()%>&org=<%#$this->Data->getOrganisme()%>');" >
										<%#$this->Data->getNomFichier()%>
									</a> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)
								</td>
								<td class="col-450" headers="PathService" style="display:<%=($this->Page->tableauPP->getCalledFromAgent()=='true') ? 'none;':';'%>" >
									<com:TLabel id="service" Text="<%#$this->Data->getPathService()%>"/>
								</td>
								<td class="col-100" headers="annee">
									<com:TLabel id="annee" Text="<%#$this->Data->getAnnee()%>"/>
								</td>
								<td class="col-150" headers="datePublication">
									<com:TLabel id="datePublication" Text = "<%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDate())%>"/>
								</td>
								<td class="actions" headers="supprFichier" style="display:<%=($this->Page->tableauPP->getCalledFromAgent()=='false') ? 'none;':';'%>">
								<com:PictoDeleteActive
								    visible="<%#($this->Page->tableauPP->getCalledFromAgent()=='false') ? false:true%>"
									ID="deleteButton"
        							OnCommand="Page.tableauPP.deletePP" 
        							OnCallBack="Page.tableauPP.onCallBackRefreshTableau"
        							CommandName = "<%#$this->Data->getId().'##'.$this->Data->getAnnee()%>"
        							AlternateText="<%=Prado::localize('TEXT_SUPPRIMER')%>"
        							Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('TEXT_ALERTE_SUPPRESSION_PP')%>"))return false;'/>
								</td>
							</tr>
						</prop:ItemTemplate>
					<prop:FooterTemplate>
					</table>
				</prop:FooterTemplate>
				</com:TRepeater>
				
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<!-- com:TPanel ID="panelNewSearch" Visible="<%=($this->Page->tableauPP->getCalledFromAgent()=='true') ? false:true%>">
				<com:TLinkButton cssClass="bouton-suivant" Text ="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" onClick ="Page.displaySearchPPs" />
			</com:TPanel-->
			<span style="display:none;">
							<com:TActiveButton 
                            ID="refreshRepeater" 
                            Text="buttonRefreshPieceExternes" 
                            OnCallBack="Page.tableauPP.onCallBackRefreshTableau"/>
                        </span>
                        <!-- com:TJavascriptLogger/-->
		</com:TActivePanel>
		<!--Fin Bloc Liste des marches-->
		<com:TActivePanel ID="panelNoElementFound"  >
					<h2><center><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</center></h2>
				</com:TActivePanel>
					<com:THyperLink 
						 ID="linkAdd"
						 visible="<%=($this->Page->tableauPP->getCalledFromAgent()=='true' && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('CreerAvisProgrammePrevisionnel')) ? true:false%>"
						 NavigateUrl="javascript:popUp('index.php?page=Agent.PopupAddListePP','yes');"
						 cssClass="ajout-el float-left" Text="<%=Prado::localize('TEXT_AJOUTER_PROGRAMME_PREVISIONNEL')%>" />