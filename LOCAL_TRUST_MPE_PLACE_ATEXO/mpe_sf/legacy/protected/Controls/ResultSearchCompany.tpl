<com:TPanel id="panelTextChoixDest"
			visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>">
	<div class="breadcrumbs">
				<span class="normal">
					<com:TTranslate>DEFINE_CONSULTATIONS</com:TTranslate> >
					<com:TTranslate>DEFINE_TEXT_ENVOI_COURRIER_ELECTRONIQUE</com:TTranslate>
                    <com:TTranslate>DEFINE_TEXT_AJOUT_MODIFICATION_DESTINATAIRE</com:TTranslate>
					<com:TTranslate>TEXT_ADRESSES_BD_FOURNISSEURS</com:TTranslate>
				</span>
	</div>
</com:TPanel>

<com:TPanel id="panelTextBDFournisseur"
			visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'false' : 'true' %>">
	<com:TPanel Visible="<%=!($_GET['page'] == 'Entreprise.RechercheCollaborateur') ? 'true' : 'false' %>">
		<div class="clearfix">
			<ul class="breadcrumb">
				<li>
					<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
				</li>
				<li>
					<com:TTranslate>TEXT_BASE_DONNEES_FOURNISSEURS</com:TTranslate>
				</li>
				<li>
					<com:TTranslate>RECHERCHER</com:TTranslate>
					<com:TTranslate>RESULTATS</com:TTranslate>
				</li>
			</ul>
		</div>
	</com:TPanel>
	<com:TPanel Visible="<%=($this->getCalledFrom() == 'entreprise')?true:false%>">
		<div class="clearfix">
			<ul class="breadcrumb">
				<li>
					<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
				</li>
				<li>
					<a href="?page=Entreprise.RechercheCollaborateur">
						<com:TTranslate>TEXT_BOURSE_A_LA_CO_SOUS_TRAITANCE</com:TTranslate>
					</a>
				</li>
				<li>
					<com:TTranslate>TEXT_RECHERCHE_COLLABORATION</com:TTranslate>
					<com:TTranslate>RESULTATS</com:TTranslate>
				</li>
			</ul>
		</div>
	</com:TPanel>
</com:TPanel>






<!--BEGIN LISTE DES ACCES-->
<div class="form-field">
	<div class="content content-resultRechercheCollaborateur">

		<!--BEGIN BTN RECHERCHE TOP-->
		<div class="link-line btns-header">
			<ul class="list-unstyled clearfix m-t-2">
				<li class="pull-left">
					<com:TLinkButton cssClass="btn btn-sm btn-default" Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" onClick ="Page.panelSearch.displayCriteria" />
				</li>
				<li class="pull-right">
					<com:TLinkButton cssClass="btn btn-sm btn-default" Text ="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" onClick ="Page.panelSearch.displayPanelSearch"  />
				</li>
			</ul>
		</div>
		<!--END BTN RECHERCHE TOP-->

		<!--BEGIN LEGENDE-->
		<com:TPanel
				visible="<%=(($this->getCalledFrom() == 'entreprise') && (Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance'))) ? true:false%>">
			<div class="modal fade modal-legende" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
										aria-hidden="true">&times;</span></button>
							<div class="h4 modal-title">
								<com:TTranslate>LEGENDE</com:TTranslate>
							</div>
						</div>
						<div class="modal-body">
							<div class="column bloc-600 legende-statut-collaborateur">
								<div class="h4">
									<com:TTranslate>TYPE_DE_COLLABORATION_SOUHAITEE</com:TTranslate>
								</div>
								<div class="clearfix m-t-2">
									<div class="statut-collaborateur col-md-1 text-center statut-collaborateur-1sur4"
										 title="<%=Prado::localize('TEXT_MANDATAIRE')%>">
									</div>
									<div class="intitule-legende col-md-11">
										<com:TTranslate>MANDATAIRE_DE_GROUPEMENT</com:TTranslate>
									</div>
								</div>
								<div class="clearfix m-t-2">
									<div class="statut-collaborateur col-md-1 text-center statut-collaborateur-2sur4"
										 title="<%=Prado::localize('TEXT_CO_TRAITANT_SOLIDAIRE')%>">
									</div>
									<div class="intitule-legende col-md-11">
										<com:TTranslate>TEXT_CO_TRAITANT_SOLIDAIRE</com:TTranslate>
									</div>
								</div>
								<div class="clearfix m-t-2">
									<div class="statut-collaborateur col-md-1 text-center statut-collaborateur-3sur4"
										 title="<%=Prado::localize('TEXT_CO_TRAITANT_CONJOINT')%>">
									</div>
									<div class="intitule-legende col-md-11">
										<com:TTranslate>TEXT_CO_TRAITANT_CONJOINT</com:TTranslate>
									</div>
								</div>
								<div class="clearfix m-t-2">
									<div class="statut-collaborateur col-md-1 text-center statut-collaborateur-4sur4"
										 title="<%=Prado::localize('TEXT_SOUS_TRAITANT')%>">
									</div>
									<div class="intitule-legende col-md-11">
										<com:TTranslate>TEXT_SOUS_TRAITANT</com:TTranslate>
									</div>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<div class="col-md-2 pull-right">
								<button type="button" class="btn btn-sm btn-block btn-danger" data-dismiss="modal">Fermer</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</com:TPanel>
		<!--END LEGENDE-->

		<!--BEGIN AUCUN RESULTAT-->
		<com:TPanel ID="panelNoElementFound" cssClass="alert alert-warning text-center">
			<com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>
		</com:TPanel>
		<!--BEGIN AUCUN RESULTAT-->

		<!--BEGIN RESULTAT RECHERCHE-->
		<div class="">
			<com:TPanel cssClass="tableCollaborationRsultats" ID="panelElementsFound">
				<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance') && ($this->getCalledFrom() == 'entreprise')) ? '':'none'%>">
					<div class="page-header h4 m-t-3 m-b-0">
						<div class="clearfix">
							<h1 class="h4 m-b-0 pull-left">
								<com:TTranslate>CODE_LISTE_ENTREPRISES_GROUPEMENT</com:TTranslate>
							</h1>
							<div class="pull-right">
								<a href="#" class="btn btn-primary btn-sm" data-toggle="modal"
								   data-target=".modal-legende">
									<com:TTranslate>LEGENDE</com:TTranslate>
									<i class="fa fa-question"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
				<com:PanelMessageAvertissement ID="panelMessageAvertTraduction" Visible="false"/>
				<div style="display:<%=($this->getCalledFrom() == 'agent') ? '':'none'%>">
					<div class="h3 no-indent">
						<com:TTranslate>TEXT_LISTE_ENTREPRISE</com:TTranslate>
					</div>
				</div>
				<!-- BEGIN PAGINATION TOP-->
				<div class="clearfix m-t-2 rechercheFilter">

					<div class="pull-left">
						<div class="h5">
							<strong>
								<com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>
								<com:TLabel ID="nombreElement"/>
							</strong>
						</div>
					</div>

					<div class="form-inline form-group-sm pull-right pagination">
						<div class="form-group pagination-section-resultperpage">
							<label class="m-r-1" for="cons_nbResultsTop">
								<com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
							</label>
							<com:TDropDownList ID="listePageSizeTop" CssClass="form-control" AutoPostBack="true"
											   onSelectedIndexChanged="changePagerLenght"
											   Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>">
								<com:TListItem Text="10" Selected="10"/>
								<com:TListItem Text="20" Value="20"/>
							</com:TDropDownList>
						</div>
						<div class="form-group pagination-section-currentpage">
							<label class="sr-only" for="cons_pageNumberTop">
								<com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate>
							</label>
							<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop">
								<label class="m-l-1 m-r-1">
									<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
								</label>
								<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>'
											  Text="1" CssClass="form-control w-50 text-center"/>
								<label class="nb-total ">/
									<com:TLabel ID="nombrePageTop"/>
								</label>
								<com:TButton ID="DefaultButtonTop" OnClick="goToPage" Attributes.style="display:none"/>
							</com:TPanel>
						</div>
						<div class="form-group liens pagination-section-arrows">
							<label class="m-l-1">
								<com:TPager ID="PagerTop"
											ControlToPaginate="tableauResultSearch"
											FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
											LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
											Mode="NextPrev"
											NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
											PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
											OnPageIndexChanged="pageChanged"/>
							</label>
						</div>
					</div>

				</div>
				<!-- END PAGINATION TOP-->

				<!--BEGIN TABLE RESULT-->
				<div class="table table-striped table-results list-group m-t-2">
					<com:TRepeater ID="tableauResultSearch" AllowPaging="true" PageSize="10" EnableViewState="true" AllowCustomPaging="true">
						<prop:HeaderTemplate>
							<div class="list-group-item header small">
								<div class="kt-vertical-align text-primary">
									<!--BEGIN TRI PAR CODE NAF | TYPE COLLABORATION -->
									<div class="col-md-2">
										<ul class="list-unstyled">
											<li>
												<com:TPanel
														visible="<%#(($_GET['page']=='Agent.SearchActivationCompany')) ? true:false%>">
													<com:TTranslate>DEFINE_STATUS</com:TTranslate>
													<span class="caret" commandname="NOM" oncommand="Page.Trier"></span>
												</com:TPanel>
											</li>
											<li>
												<com:TPanel
														visible="<%#(($_GET['page']=='Agent.SearchActivationCompany')) ? true:false%>">
													<com:TTranslate>TEXT_DATE_CREACTION_COMPTE</com:TTranslate>
													<span class="caret" commandname="NOM" oncommand="Page.Trier"></span>
												</com:TPanel>
											</li>
											<li>
												<com:TTranslate>TEXT_ENTREPRISE</com:TTranslate>
												<com:PictoSort CommandName="NOM" OnCommand="Page.Trier" Visible="false"/>
												<com:TLinkButton CommandName="NOM" OnCommand="Page.Trier" cssClass="">
													<i class="fa fa-angle-down m-l-1"></i>
												</com:TLinkButton>
											</li>
											<li>
												<com:TPanel
														visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>">
													<com:TTranslate>DEFINE_CODE_NAF</com:TTranslate>
												</com:TPanel>
											</li>
											<li>
												<com:TPanel
														visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>">
													<com:TTranslate>TEXT_TYPE_COLLABORATION</com:TTranslate>
												</com:TPanel>

											</li>
										</ul>
									</div>
									<!--END TRI PAR CODE NAF | TYPE COLLABORATION -->

									<!--BEGIN CONTACT | FONCTION | E-MAIL | CODE-POSTAL-->
									<div class="col-md-3">
										<ul class="list-unstyled">
											<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? 'none':''%>"
												id="coordonnees">
												<com:TPanel
														visible="<%#(($this->templateControl->getCalledFrom() == 'entreprise')) ? true:false%>">
													<com:TTranslate>CONTACT</com:TTranslate>
													-
													<com:TTranslate>DEFINE_FONCTION_MAJ</com:TTranslate>
													-
													<com:TTranslate>DEFINE_E_MAIL</com:TTranslate>
												</com:TPanel>
											</li>
											<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? 'none':''%>"
												id="coordonnees">
												<com:TPanel
														visible="<%#(($this->templateControl->getCalledFrom() == 'entreprise')) ? true:false%>">
													<com:TTranslate>TEXT_CP</com:TTranslate>
													-
													<com:TTranslate>TEXT_VILLE</com:TTranslate>
												</com:TPanel>
											</li>
											<li style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion')) && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>"
												id="coordonnees">
												<com:TTranslate>ADRESSE_DU_SIEGE</com:TTranslate>
												<br/>
												<%#$this->templateControl->elementSortedWith('CODEPOSTAL','open')%>
												<com:TTranslate>TEXT_CP</com:TTranslate>
												<%#$this->templateControl->elementSortedWith('CODEPOSTAL','close')%>
												<com:PictoSort CommandName="CODEPOSTAL" OnCommand="Page.Trier"/>
												<br/>
												<%#$this->templateControl->elementSortedWith('REGION','open')%>
												<com:TTranslate>TEXT_REGION</com:TTranslate>
												<%#$this->templateControl->elementSortedWith('REGION','close')%>
												<com:PictoSort CommandName="REGION" OnCommand="Page.Trier"/>
												<br/>
												<%#$this->templateControl->elementSortedWith('PROVINCE','open')%>
												<com:TTranslate>TEXT_PROVINCE</com:TTranslate>
												<%#$this->templateControl->elementSortedWith('PROVINCE','close')%>
												<com:PictoSort CommandName="PROVINCE" OnCommand="Page.Trier"/>
												<br/>
												<%#$this->templateControl->elementSortedWith('VILLEADRESSE','open')%>
												<com:TTranslate>TEXT_VILLE</com:TTranslate>
												<%#$this->templateControl->elementSortedWith('VILLEADRESSE','close')%>
												<com:PictoSort CommandName="VILLEADRESSE" OnCommand="Page.Trier"/>
											</li>
										</ul>
									</div>
									<!--END CONTACT | FONCTION | E-MAIL | CODE-POSTAL-->

									<!--BEGIN DESCRIPTION D'ACTIVITE-->
									<div class="col-md-5">
										<ul class="list-unstyled">
											<li style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe') || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>"
												id="naf">
												<div>
													<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) ? '':'none'%>">
														<com:TTranslate>TEXT_NAF_SEUL</com:TTranslate>
													</label>
													<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) ? '':'none'%>">
														<com:TTranslate>TEXT_NACE</com:TTranslate>
													</label>
													<span class="caret" commandname="CODEAPE"
														  oncommand="Page.Trier"></span>
												</div>
											</li>
											<li id="description">
												<com:TTranslate>TEXT_DESCRIPTION_ACTIVITE</com:TTranslate>
												<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) ? '':'none'%>">
													<com:TTranslate>DEFINE_ACTIVITES_DOMAINE_DEFENSE
													</com:TTranslate>
												</div>
												<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
													<com:TTranslate>TEXT_DOMAINES_ACTIVITES</com:TTranslate>
												</div>
												<!-- D?but domaine d'activit? utilisant LT-Ref -->
												<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
													<com:TTranslate>TEXT_DOMAINES_ACTIVITES</com:TTranslate>
												</div>
												<!-- Fin domaine d'activit? utilisant LT-Ref -->
											</li>
											<li id="agrement"
												style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">
												<com:TLabel
														style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrement')) ? '':'none'%>">
													<com:TTranslate>TEXT_AGREMENT</com:TTranslate>
												</com:TLabel>
												<com:TLabel
														style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
													<com:TTranslate>TEXT_AGREMENT_LT_REFERENTIEL</com:TTranslate>
												</com:TLabel>
											</li>
											<li id="qualification"
												style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">
												<com:TLabel
														style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualification')) ? '':'none'%>">
													<com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
												</com:TLabel>
												<com:TLabel
														style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
													<com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
												</com:TLabel>
											</li>
											<li id="actions"
												style="display:<%#($_GET['page']=='Agent.SearchActivationCompany') ? '':'none'%>">
												<com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
											</li>
											<li id="checkAll">
												<com:TPanel id="panelChoixAllEse"
															Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>">
													<div class="check-col"
														 abbr="<com:TTranslate>TEXT_TOUS</com:TTranslate>">
														<label for="selectAll" style="display: none;">
															<com:TTranslate>TEXT_TOUT_SELECTIONNER</com:TTranslate>
														</label>
														<com:TActiveCheckBox
																id="selectAll"
																Attributes.title="<%=Prado::localize('TEXT_TOUS_AUCUN')%>"
																Attributes.onclick="CheckUnCheckAll(this, 'panelResultSerch_tableauResultSearch', 'entrepriseSelection');"
														/>
													</div>
												</com:TPanel>
											</li>
										</ul>
									</div>
									<!--BEGIN DESCRIPTION D'ACTIVITE-->

									<!--BEGIN NOMBRE ETABLISEMENT-->
									<div class="col-md-2">
										<ul class="list-unstyled text-right">
											<li>
												<th class="col-30" id="nbEtablissements">
													<com:TTranslate>NOMBRE_D_ETABLISSEMENTS</com:TTranslate>
												</th>
											</li>
										</ul>
									</div>
									<!--END NOMBRE ETABLISEMENT-->
								</div>
							</div>
						</prop:HeaderTemplate>
						<prop:ItemTemplate>
							<div class="list-group-item kt-vertical-align">

								<!--BEGIN CONTENT CODE NAF | TYPE COLLABORATION -->
								<div class="col-md-2 entrepriseName col-xs-12">
									<div class="h5 m-b-1 <%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
										<strong><%#$this->Data->getNom()%></strong>
									</div>
									<div class="small">
										<abbr title="">
											<span>
												<%#$this->Data->getPaysenregistrement ()%>
											</span>
										</abbr>
										<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
											- <%#$this->Data->getSiren()%> <%#$this->Data->getSirenetranger()%>
										</span>
									</div>

									<com:TPanel
											visible="<%#(($_GET['page']=='Agent.SearchActivationCompany') && (Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) && $this->Data->getCompteActif()=='0') ? true:false%>"
											cssClass="statut-activation red">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif"
											 alt="<%=Prado::localize('TEXT_INACTIF')%>"
											 title="<%=Prado::localize('TEXT_INACTIF')%>"/>
										<com:TTranslate>TEXT_INACTIF</com:TTranslate>
									</com:TPanel>

									<com:TPanel
											visible="<%#(($_GET['page']=='Agent.SearchActivationCompany') && (Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) && $this->Data->getCompteActif()=='1') ? true:false%>"
											cssClass="statut-activation green">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif"
											 alt="<%=Prado::localize('TEXT_ACTIF')%>"
											 title="<%=Prado::localize('TEXT_ACTIF')%>"/>
										<com:TTranslate>TEXT_ACTIF</com:TTranslate>
									</com:TPanel>

									<com:TPanel
											visible="<%#(($_GET['page']=='Agent.SearchActivationCompany')) ? true:false%>">
										<div class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
											<%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateCreation())%>
										</div>
										<com:TTranslate>CREER_PAR</com:TTranslate>
										:
										<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">  <%#$this->Data->getPrenomAgent() . ' ' . $this->Data->getNomAgent()%></span>
									</com:TPanel>

									<com:TPanel
											visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>">
										<div class="h5 small <%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
											<%#str_replace('#','',$this->Data->getCodeape())%>
										</div>
										<div class="collaboration">
											<com:TRepeater ID="repeaterTypeCollaboration"
														   dataSource="<%#$this->templateControl->getArrayTypeColloboration($this->Data->getTypeCollaboration())%>">
												<prop:ItemTemplate>
													<div class="statut-collaborateur statut-collaborateur-<%#$this->Data['id']%>sur4"
														 title="<%#($this->Data['libelle'])%>"></div>
												</prop:ItemTemplate>
											</com:TRepeater>
										</div>
									</com:TPanel>
								</div>
								<!--BEGIN CONTENT CODE NAF | TYPE COLLABORATION -->

								<div class="col-md-10 col-xs-12 m--cotraitanceBlocDetails">

									<!--BEGIN CONTENT CONTACT | FONCTION | E-MAIL | CODE-POSTAL-->
									<div class="col-md-4 coordonnees col-xs-12">
										<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? 'none':''%>"
											 id="coordonnees">

											<com:TPanel
													visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>">
												<div class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
													<div>
														<%#($this->Data->getContactByIdEntreprise() instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getNom() : ''%> -
														<%#($this->Data->getContactByIdEntreprise() instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getPrenom() : ''%>
													</div>
													<div>
														<%#($this->Data->getContactByIdEntreprise() instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getFonction() : ''%>
													</div>
													<div>
													<span class="text-primary">
														<%#($this->Data->getContactByIdEntreprise()instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getEmail(): ''%>
													</span>
													</div>
													<div>
														<%#($this->Data->getContactByIdEntreprise() instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getCodePostal(): ''%> -
														<%#($this->Data->getContactByIdEntreprise() instanceof
														Application\Propel\Mpe\CommonContactEntreprise)?
														$this->Data->getContactByIdEntreprise()->getVille(): ''%>
													</div>
												</div>
											</com:TPanel>
											<com:TPanel
													visible="<%#(!Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>">
											<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "> <%#$this->Data->getAdresse()%>
												<com:TLabel
														visible="<%#($this->Data->getAdresse2()!='') ? true:false%>"
														Text="<br/>">
                                                    <%#$this->Data->getAdresse2()%>
                                                </com:TLabel>
												<br/>
												<%#$this->Data->getCodepostal()%>
												<br/>
												<%#$this->Data->getVilleadresse()%>
											</span>
											</com:TPanel>
										</div>
									</div>

									<!--END CONTENT CONTACT | FONCTION | E-MAIL | CODE-POSTAL-->
									<div class="col-md-6  p-0">
										<div class="col-md-3 col-xs-12"
											 style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
											<td headers="coordonnees">
										<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "><%#$this->Data->getAdresse()%>
											<com:TLabel visible="<%#($this->Data->getAdresse2()!='') ? true:false%>"
														Text="<br/>">
                                                <%#$this->Data->getAdresse2()%>
                                            </com:TLabel></span>
												<br/>
												<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
											<%#Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1::retrieveDenomination1ByIdGeoN1($this->Data->getRegion())%>
											<br style="display:<%#($this->Data->getRegion()) ? '':'none'%>"/>

											<%# (new Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($this->Data->getProvince())%>
											<br style="display:<%#($this->Data->getProvince()) ? '':'none'%>"/>
											<%#$this->Data->getCodepostal()%>
											<br/>
											<%#$this->Data->getVilleadresse()%>
										</span>
											</td>
										</div>

										<!--BEGIN CONTENT NAF-->
										<div class="col-md-3 col-xs-12"
											 style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe') || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) && ($this->templateControl->getCalledFrom() == 'agent'))? '':'none'%>">
											<td headers="naf">
												<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
													<%#str_replace('#','',$this->Data->getCodeape())%>
												</span>
											</td>
										</div>
										<!--END CONTENT NAF-->

										<!--BEGIN CONTENT DESCRIPTION D'ACTIVITE-->
										<div class="col-md-6 description top col-xs-12 p-0">
											<div class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%>">
												<div>
													<strong>
														<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getDescriptionActivite(),'100')%>
													</strong>
												</div>
												<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) ? '':'none'%>">
													<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getActiviteDomaineDefense(),'100')%>
													<span style="<%#$this->templateControl->isTextTruncated($this->Data->getActiviteDomaineDefense())%>"
														  onmouseover="afficheBulle('ctl0_CONTENU_PAGE_panelResultSerch_tableauResultSearch_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)"
														  onmouseout="cacheBulle('ctl0_CONTENU_PAGE_panelResultSerch_tableauResultSearch_ctl<%#($this->ItemIndex+1)%>_infosConsultation')"
														  class="info-suite">...</span>
													<com:TPanel ID="infosConsultation" CssClass="info-bulle">
														<div><%#$this->Data->getActiviteDomaineDefense()%></div>
													</com:TPanel>
												</div>
												<com:TPanel
														visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>"
														Cssclass="objet-line">
													<%# Application\Service\Atexo\Atexo_Util::truncateTexte((new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites()),100)%>
													<span
															onmouseover="afficheBulle('infosSecteursActivite_<%#$this->ItemIndex%>', this)"
															onmouseout="cacheBulle('infosSecteursActivite_<%#$this->ItemIndex%>')"
															class="info-suite"
															title="Info-bulle">
												<com:TLabel
														Display="<%#(strlen((new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites()))>110) ? 'Dynamic':'None'%>"
														Text="..."/>
											</span>
													<div id="infosSecteursActivite_<%#$this->ItemIndex%>"
														 class="info-bulle"
														 onmouseover="mouseOverBulle();"
														 onmouseout="mouseOutBulle();">
														<div>
															<%#(new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites())%>
														</div>
													</div>
												</com:TPanel>
												<!-- D?but domaine d'activit? utilisant LT-Ref -->
												<com:TPanel
														visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>"
														Cssclass="objet-line">
													<%#$this->TemplateControl->doGetLibelleLtRefDomaineActivite($this->Data)
													%>
													<span
															onmouseover="afficheBulle('infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>', this)"
															onmouseout="cacheBulle('infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>')"
															class="info-suite"
															title="Info-bulle">
												<com:TLabel
														Display="<%#(strlen($this->Data->getLibelleLtRef($this->Data->getDomainesActivites(),Application\Service\Atexo\Atexo_Languages::readLanguageFromSession(),  Application\Service\Atexo\Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'))) > 110) ? 'Dynamic':'None'%>"
														Text="..."/>
											</span>
													<div id="infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>"
														 class="info-bulle"
														 onmouseover="mouseOverBulle();"
														 onmouseout="mouseOutBulle();">
														<div>
															<%#$this->TemplateControl->doGetLibelleLtRefDomaineActivite($this->Data,true)%>
														</div>
													</div>
												</com:TPanel>
												<!-- Fin domaine d'activit? utilisant LT-Ref -->
											</div>
										</div>
										<!--END CONTENT DESCRIPTION D'ACTIVITE-->
									</div>

									<!--BEGIN CONTENT NOMBRE ETABLISEMENT-->
									<div class="col-md-2 nbEtablissements">
										<div class="text-right">
											<strong>
												<%#$this->Data->countCommonTEtablissements()%> &Eacute;tablissements
											</strong>
											<div class="allCons_details m-t-2 col-md-6 p-l-0 pull-right">
												<!--<a class="btn btn-primary btn-sm btn-block"
                                                   href="javascript:popUp('index.php?page=<%#$this->templateControl->getCalledFrom()%>.DetailEntreprise&callFrom=<%#((Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance')) && ($_GET['page']=='Entreprise.RechercheCollaborateur')) ? 'entreprise' : 'agent'%>&idEntreprise=<%#$this->Data->getId()%>','yes');">
                                                    <i class="fa fa-search-plus"></i>
                                                </a>-->
												<div data-toggle="tooltip" data-placement="top" title="<com:TTranslate>TEXT_DETAIL_ENTREPRISE</com:TTranslate> : <%#$this->Data->getNom()%> - <%#$this->Data->getPaysenregistrement ()%> - <%#$this->Data->getSiren()%> <%#$this->Data->getSirenetranger()%>">
													<button type="button" class="btn btn-primary btn-sm btn-block" title="<com:TTranslate>TEXT_DETAIL_ENTREPRISE</com:TTranslate> : <%#$this->Data->getNom()%> - <%#$this->Data->getPaysenregistrement ()%> - <%#$this->Data->getSiren()%> <%#$this->Data->getSirenetranger()%>"
															data-toggle="ajaxmodal"
															data-target="?page=Entreprise.EntrepriseModal&controls=CAtexoModalDetailEntreprise&action=submit&callFrom=<%#((Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance')) && ($_GET['page']=='Entreprise.RechercheCollaborateur')) ? 'entreprise' : 'agent'%>&idEntreprise=<%#$this->Data->getId()%>"
															data-json='{"ajaxmodal":{"callback":""}}'>
														<i class="fa fa-search-plus"></i>
														<span class="sr-only"><com:TTranslate>TEXT_DETAIL_ENTREPRISE</com:TTranslate> : <%#$this->Data->getNom()%> - <%#$this->Data->getPaysenregistrement ()%> - <%#$this->Data->getSiren()%> <%#$this->Data->getSirenetranger()%></span>
													</button>
												</div>
											</div>
										</div>
									</div>
									<!--END CONTENT NOMBRE ETABLISEMENT-->
								</div>
								<com:TPanel id="panelChoixEse" Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>">
									<div class="check-col" headers="checkAll">
										<com:THiddenField id="IdEse" Value="<%#$this->Data->getId()%>"/>
										<com:TActiveCheckBox
												id="entrepriseSelection"
												Attributes.title="<%=Prado::localize('TEXT_DEFINE_SELECTIONNER')%>" checked="<%#$this->TemplateControl->getEtat($this->Data->getId())%>"/>
									</div>
								</com:TPanel>
							</div>
						</prop:ItemTemplate>
					</com:TRepeater>
				</div>
				<!--END TABLE RESULT-->

				<!-- BEGIN PAGINATION BOTTOM-->
				<div class="clearfix m-t-2">
					<div class="form-inline form-group-sm pull-right m--cotraitanceBlocDetailsFooter pagination">
						<div class="form-group pagination-section-resultperpage">
							<label class="m-r-1" for="cons_nbResultsTop">
								<com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
							</label>
							<com:TDropDownList ID="listePageSizeBottom" CssClass="form-control" AutoPostBack="true"
											   onSelectedIndexChanged="changePagerLenght"
											   Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>">
								<com:TListItem Text="10" Selected="10"/>
								<com:TListItem Text="20" Value="20"/>
							</com:TDropDownList>
						</div>
						<div class="form-group pagination-section-currentpage">
							<label class="sr-only" for="cons_pageNumberBottom">
								<com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate>
							</label>
							<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom">
								<label class="m-l-1 m-r-1">
									<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
								</label>
								<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>'
											  Text="1" CssClass="form-control w-50 text-center"/>
								<label class="nb-total ">/
									<com:TLabel ID="nombrePageBottom"/>
								</label>
								<com:TButton ID="DefaultButtonBottom" OnClick="goToPage"
											 Attributes.style="display:none"/>
							</com:TPanel>
						</div>
						<div class="form-group liens pagination-section-arrows">
							<label class="m-l-1">
								<com:TPager ID="PagerBottom"
											ControlToPaginate="tableauResultSearch"
											FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
											LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
											Mode="NextPrev"
											NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
											PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
											OnPageIndexChanged="pageChanged"/>
							</label>
						</div>
					</div>
				</div>
				<!-- END PAGINATION BOTTOM-->
			</com:TPanel>
			<com:TPanel ID="exportEntreprise" CssClass="file-link margin-left-15"
						visible="<%=($this->getCalledFrom() == 'agent') ? true:false%>">
				<com:TLinkButton
						Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'false' : 'true'%>"
						OnClick="genererExcelEntreprise">
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" alt="Export XLS" title="Export XLS"/>
				</com:TLinkButton>
			</com:TPanel>
			<com:TPanel ID="panelValider"
						visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'true' : 'false'%>">
				<div class="boutons-line">
					<com:TButton
							id="valider"
							CssClass="bouton-moyen float-right"
							Attributes.title="<%=Prado::localize('ICONE_VALIDER')%>"
							Text="<%=Prado::localize('ICONE_VALIDER')%>"
							OnClick="validerChoix"/>
				</div>
			</com:TPanel>
		</div>
		<!--BEGIN RESULTAT RECHERCHE-->

		<!--BEGIN BTN RECHERCHE BOTTOM-->
		<div class="link-line btns-footer">
			<ul class="list-unstyled clearfix m-t-2">
				<li class="pull-left">
					<com:TLinkButton cssClass="btn btn-sm btn-default" Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" onClick ="Page.panelSearch.displayCriteria" />
				</li>
				<li class="pull-right">
					<com:TLinkButton cssClass="btn btn-sm btn-default" Text ="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" onClick ="Page.panelSearch.displayPanelSearch"  />
				</li>
			</ul>
		</div>
		<!--END BTN RECHERCHE BOTTOM-->
	</div>
	<!--Fin partitionneur-->
</div>
<!--END LISTE DES ACCES-->

