<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAvisMembresCAO;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_AvisMembresCAO;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/*
 * Created on 27 Fev. 2013
 *
 * by EZZIANI Loubna
 */
class AtexoAvisMembresCAO extends MpeTTemplateControl
{
    public $_idEnv;

    public function setIdEnv($value)
    {
        $this->_idEnv = $value;
    }

    public function getIdEnv()
    {
        return $this->_idEnv;
    }

    public function onLoad($param)
    {
        $idReference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        if (!$this->Page->IsPostBack) {
            $this->remplirRepeaterAvisCAO($idReference, $organisme);
        }
    }

    /*
     * Permet de remplir le repeater avis CAO
     * @param $idReference : id reference de la consultation concernée,$organisme : l'organisme
     */
    public function remplirRepeaterAvisCAO($idReference, $organisme)
    {
        foreach ($this->repeaterAvisLot->getItems() as $item) {
            $sousPli = $item->sousPli->value;
            $formeEnv = $item->formeEnv->value;
            $idOffre = $item->idOffre->value;
            $typeEnv = $item->typeEnv->value;

            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($idReference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultations = (new Atexo_Consultation())->search($criteria);
            if (is_array($consultations) && count($consultations)) {
                $consultation = $consultations[0];
            }
            /* les agents invité à la consultation */
            $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($idReference, $organisme);
            $dataAvis = [];
            if (is_array($guests)) {
                foreach ($guests as $guest) {
                    $idGuest = $guest->getId();
                    if ($guest->getPermanentGuest() || (!$guest->getPermanentGuest() && 1 == $guest->getTypeInvitation())) {
                        // tester si l'agent à l'habilitation accesReponses
                        $hasHbilitation = (new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste($idGuest, 'AccesReponses');
                        if ($hasHbilitation) {
                            $dataAvis[] = $this->getInfoAvisGeust($guest, $this->getIdEnv(), $sousPli, $formeEnv, $idOffre, $typeEnv, $consultation);
                        }
                    }
                }
            }

            $dataSorted = Atexo_Util::sortmddata($dataAvis, 'nom', 'ASC');

            $item->repeaterAvisCAO->DataSource = $dataSorted;
            $item->repeaterAvisCAO->DataBind();
        }
    }

    /*
     * permet d'avoir les information d'avis d'un agent pour un enveloppe donné
     * @param  $guest : l'agent,$idEnveloppe : id de l'enveloppe,$sousPli : sous pli,$formeEnv : la forme de l'enveloppe(electronique ou papier)
     * return l'avis de l'agent
     */
    public function getInfoAvisGeust($guest, $idEnveloppe, $sousPli, $formeEnv, $idOffre, $typeEnv, $consultation)
    {
        $res = [];
        $admisSaisie = false;
        $admissibliteEnv = (new Atexo_Consultation_Responses())->retrieveAdmissibilite($sousPli, $idOffre, $formeEnv, $typeEnv, $consultation);
        if ($admissibliteEnv) {
            $admisSaisie = true;
        }
        if ($guest instanceof Atexo_Consultation_GuestVo) {
            $idAgent = $guest->getId();
            $avisAgent = (new Atexo_AvisMembresCAO())->retrieveAvisMembresCAOByAgent($idAgent, $idEnveloppe, $sousPli, $formeEnv);
            if ($idAgent == Atexo_CurrentUser::getIdAgentConnected() && !$admisSaisie) {
                $res['enabled'] = true;
            } else {
                $res['enabled'] = false;
            }
            $res['idAgent'] = $idAgent;
            $res['idEnv'] = $idEnveloppe;
            if ($avisAgent instanceof CommonAvisMembresCAO) {
                $res['nom'] = $avisAgent->getNomAgent();
                $res['prenom'] = $avisAgent->getPrenomAgent();
                $res['admissibilite'] = $avisAgent->getAdmissibilite();
                $res['commentaire'] = $avisAgent->getCommentaire();
            } else {
                $res['nom'] = $guest->getFirstName();
                $res['prenom'] = $guest->getLastName();
                $res['admissibilite'] = Atexo_Config::getParameter('LOT_A_TRAITER');
                $res['commentaire'] = '';
            }
        }

        return $res;
    }

    /*
     * Permet d'enregistrer l'avis de l'agent connecté
     *
     */
    public function saveAvisMembresCAO()
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($this->repeaterAvisLot->getItems() as $item) {
            $sousPli = $item->sousPli->value;
            $formeEnv = $item->formeEnv->value;
            foreach ($item->repeaterAvisCAO->getItems() as $sousItem) {
                if ($sousItem->lotAdmis->getChecked()) {
                    $admis = Atexo_Config::getParameter('LOT_ADMISSIBLE');
                } elseif ($sousItem->lotNonAdmis->getChecked()) {
                    $admis = Atexo_Config::getParameter('LOT_NON_ADMISSIBLE');
                } elseif ($sousItem->lotATraiter->getChecked()) {
                    $admis = Atexo_Config::getParameter('LOT_A_TRAITER');
                }
                if ($sousItem->idAgent->value == Atexo_CurrentUser::getIdAgentConnected()) {
                    $avisAgent = (new Atexo_AvisMembresCAO())->retrieveAvisMembresCAOByAgent($sousItem->idAgent->value, $sousItem->idEnv->value, $sousPli, $formeEnv);
                    if (!$avisAgent) {
                        $avisAgent = new CommonAvisMembresCAO();
                        $avisAgent->setIdAgent($sousItem->idAgent->value);
                        $avisAgent->setIdEnveloppe($sousItem->idEnv->value);
                    }
                    $avisAgent->setNomAgent($sousItem->nomAgent->Text);
                    $avisAgent->setPrenomAgent($sousItem->prenomAgent->Text);
                    $avisAgent->setDateAction(date('Y-m-d H:i:s'));
                    $avisAgent->setAdmissibilite($admis);
                    $avisAgent->setCommentaire($sousItem->commentaireLot->Text);
                    $avisAgent->setSousPli($sousPli);
                    $avisAgent->setTypeDepotReponse($formeEnv);
                    $avisAgent->save($connection);
                }
            }
        }
    }
}
