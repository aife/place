<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * Created on 4 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class AtexoListeFormulaire extends MpeTPage
{
    protected $_previousLot;
    protected $_previousEnv;
    protected $_refConsultation;
    protected $_organisme;
    protected $_calledFrom;

    public function onLoad($param)
    {
        if ('agent' == $this->getCalledFrom()) {
            $this->setRefConsultation(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
            $this->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $this->messageEntreprise->setVisible(false);
        } elseif ('entreprise' == $this->_calledFrom) {
            $this->setRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $this->setOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            $this->messageEntreprise->setVisible(true);
        }
        $this->remplirRepeaterFormsCand();
        $this->remplirRepeaterForms();
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setRefConsultation($val)
    {
        $this->_refConsultation = $val;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setOrganisme($val)
    {
        $this->_organisme = $val;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setPreviousLot($val)
    {
        $this->_previousLot = $val;
    }

    public function getPreviousLot()
    {
        return $this->_previousLot;
    }

    public function remplirRepeaterFormsCand()
    {
        $data = (new Atexo_FormConsultation())->getListeFormsByTypeEnveloppe($this->getRefConsultation(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), false);
        $this->repeaterFormsCand->DataSource = $data;
        $this->repeaterFormsCand->dataBind();
    }

    public function remplirRepeaterForms()
    {
        $data = (new Atexo_FormConsultation())->getListeFormsByTypeEnveloppe($this->getRefConsultation(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), true);
        $this->repeaterForms->DataSource = $data;
        $this->repeaterForms->dataBind();
    }

    public function getLibelleLot($lot)
    {
        return (new Atexo_FormConsultation())->getDescriptionLot($lot, $this->getOrganisme(), $this->getRefConsultation());
    }

    public function afficherLigneLot($lot)
    {
        if (!$lot) {
            return false;
        }
        if ('' == $this->_previousLot) {
            $this->_previousLot = $lot;

            return true;
        }
        if ('' != $this->_previousLot && $this->_previousLot == $lot) {
            $this->_previousLot = $lot;

            return false;
        }
        if ('' != $this->_previousLot && $this->_previousLot != $lot) {
            $this->_previousLot = $lot;
            $this->_previousEnv = '-1';

            return true;
        }
    }

    public function afficherLigneEnveloppe($env, $lot)
    {
        if ('' == $this->_previousEnv) {
            $this->_previousEnv = $env;

            return true;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv == $env && $this->_previousLot == $lot) {
            $this->_previousEnv = $env;

            return false;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv != $env) {
            $this->_previousEnv = $env;

            return true;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv == $env && $this->_previousLot != $lot) {
            $this->_previousEnv = $env;

            return true;
        }
    }

    public function getUrlEditionForms($formCons)
    {
        if ('agent' == $this->getCalledFrom()) {
            return "javascript:popUp('index.php?page=Agent.PopUpParametrageFormulaireConsultation&id=".$formCons->getId()."','yes');";
        } elseif ('entreprise' == $this->getCalledFrom()) {
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('QUESTIONNAIRE')) {
                return "javascript:popUp('index.php?page=Entreprise.PopUpQuestionnaire&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$formCons->getId()."','yes');";
            }
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
                return "javascript:popUp('index.php?page=Entreprise.PopUpBordereauPrix&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$formCons->getId()."','yes');";
            }
        }
    }

    public function getPictoStatut($formConsultation)
    {
        $idInscrit = Atexo_CurrentUser::getIdInscrit();

        return (new Atexo_FormConsultation())->getPictoStatutForm($formConsultation, $idInscrit, $this->getCalledFrom());
    }

    public function getTitleStatut($formConsultation)
    {
        $idInscrit = Atexo_CurrentUser::getIdInscrit();

        return (new Atexo_FormConsultation())->getTitleStatutForm($formConsultation, $idInscrit, $this->getCalledFrom());
    }

    public function refreshRepeater($sender, $param)
    {
        $this->_previousEnv = '';
        $this->_previousLot = '';
        $this->remplirRepeaterFormsCand();
        $this->remplirRepeaterForms();
        $this->PanelListeFormulaires->render($param->NewWriter);
    }

    public function getUrlVisualisation($formCons)
    {
        if ('agent' == self::getCalledFrom()) {
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('QUESTIONNAIRE')) {
                return "javascript:popUp('index.php?page=Agent.PopUpDetailQuestionnaireAgent&id=".$formCons->getId()."','yes');";
            }
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
                return "javascript:popUp('index.php?page=Agent.PopUpDetailBordereauDePrixAgent&id=".$formCons->getId()."','yes');";
            }
        } elseif ('entreprise' == self::getCalledFrom()) {
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('QUESTIONNAIRE')) {
                return "javascript:popUp('index.php?page=Entreprise.PopUpDetailQuestionnaireEntreprise&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$formCons->getId()."','yes');";
            }
            if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
                return "javascript:popUp('index.php?page=Entreprise.PopUpDetailBordereauDePrixEntreprise&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$formCons->getId()."','yes');";
            }
        }
    }

    public function getTitleEdition($formCons)
    {
        if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('QUESTIONNAIRE')) {
            return Prado::localize('TEXT_EDITER_LE_QUESTIONNAIRE');
        }
        if ($formCons->getTypeFormulaire() == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
            return Prado::localize('TEXT_EDITER_LE_BORDEREAUX_DE_PRIX');
        }
    }
}
