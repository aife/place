<?php

namespace Application\Controls;

/**
 * Page de gestion des données complémentaires de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class InformationsComplementairesConsultation extends MpeTTemplateControl
{
    public $consultation;
    public ?string $_affiche = null; // (=1 pour afficher, 0 sinon)

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        if ($this->consultation !== $consultation) {
            $this->consultation = $consultation;
        }
    }

    // setConsultation()

    /**
     * recupère la valeur.
     */
    public function getAffiche()
    {
        return $this->_affiche;
    }

    // getAffiche()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setAffiche($value)
    {
        if ($this->_affiche !== $value) {
            $this->_affiche = $value;
        }
    }

    // setAffiche()

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
    }
}
