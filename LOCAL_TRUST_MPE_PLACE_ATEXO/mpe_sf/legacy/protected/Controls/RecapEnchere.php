<?php

namespace Application\Controls;

/**
 * Tableau recapitulatif.
 *
 * @author Adil El Kanabi <adil.kanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class RecapEnchere extends MpeTTemplateControl
{
    private $_reference;
    private $_objet;
    private $_entitePublique;
    private $_entiteAchat;

    public function getObjet()
    {
        return $this->_objet;
    }

    public function setObjet($value)
    {
        $this->_objet = $value;
    }

    public function setReference($value)
    {
        $this->_reference = $value;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setEntitePublique($value)
    {
        $this->_entitePublique = $value;
    }

    public function getEntitePublique()
    {
        return $this->_entitePublique;
    }

    public function setEntiteAchat($value)
    {
        $this->_entiteAchat = $value;
    }

    public function getEntiteAchat()
    {
        return $this->_entiteAchat;
    }
}
