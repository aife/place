<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;

/**
 * Composant de gestion du DUME Acheteur.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @copyright Atexo 2018
 *
 * @version 1
 */
class AtexoDumeAcheteur extends MpeTTemplateControl
{
    private string|bool $_Visible = '';
    public $token = '';
    public $idContexteDume = '';

    public function setVisible($visible)
    {
        $this->_Visible = $visible;
    }

    public function getVisible($checkParents = true)
    {
        return $this->_Visible;
    }

    /**
     * Permet de creer le DUME Acheteur de la consultation.
     *
     * @param CommonConsultation $consultation
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function createDumeAcheteur($consultation, $forceCreate)
    {
        return Atexo_Dume_AtexoDume::createDumeContexte($consultation, $forceCreate);
    }

    /**
     * Permetde charger le DUME Acheteur de la consultation.
     *
     * @param CommonConsultation $consultation
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function chargerDumeAcheteur($consultation)
    {

        $idContexteDume = null;
        if ($consultation instanceof CommonConsultation && 1 == $consultation->getDumeDemande()) {
            if ('1' == $consultation->getTypeFormulaireDume()) {
                // simplifié
                $this->panelInfoMsgDumeAcheteurSimplifie->setDisplay('Dynamic');
                $this->panelInfoMsgDumeAcheteur->setDisplay('None');
                $this->lt_dume->setDisplay('None');
            } else {
                $this->panelInfoMsgDumeAcheteur->setDisplay('Dynamic');
                $this->panelInfoMsgDumeAcheteurSimplifie->setDisplay('None');

                $dume = Atexo_Dume_AtexoDume::createOrUpdateDumeContexte($consultation, true);
                $chargerDume = true;
                if ($dume instanceof CommonTDumeContexte) {
                    $idContexteDume = $dume->getContexteLtDumeId();
                    if ($dume->getStatus() == Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION')) {
                        $this->lt_dume->setDisplay('None');
                        $this->panelBlocErreurModifiedDume->setDisplay('Dynamic');
                        $chargerDume = false;
                    }
                }
                if ($chargerDume) {
                    $token = Atexo_Dume_AtexoDume::getTokenDume($idContexteDume);

                    if (!empty($idContexteDume) && !empty($token)) {
                        $this->setToken($token);
                        $this->setIdContexteDume($idContexteDume);

                        // on a retiré Atexo_Config::getParameter('URL_PF_DUME') du dernier argument de la fonction
                        // chargerLtDume selon les préconisation de SLE sur le ticket
                        $this->Page->scriptJs->text = "<script>
                            J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();
                            document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').value = 0;
                            chargerLtDume('$token' , $idContexteDume, '/dume/');</script>";
                        $this->lt_dume->setDisplay('Dynamic');
                        $this->panelBlocErreur->setDisplay('None');
                    } else {
                        $this->Page->scriptJs->text = "<script>J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').value = 1;</script>";
                        $this->lt_dume->setDisplay('None');
                        $this->panelBlocErreur->setDisplay('Dynamic');
                    }
                }
            }
        }
    }

    /**
     * Permet de creer le DUME Acheteur de la consultation.
     *
     * @param CommonConsultation $consultation
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function saveDumeAcheteur($consultation)
    {
        //TODO
    }

    /**
     * Permet de valider le DUME Acheteur de la consultation.
     *
     * @param CommonConsultation $consultation
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function validateDumeAcheteur($consultation)
    {
        return Atexo_Dume_AtexoDume::validerDumeAcheteur($consultation, 'DEMANDE_VALIDATION');
    }

    public function  setToken($token) {
        $this->token = $token;
    }
    public function  setIdContexteDume($idContexteDume) {
        $this->idContexteDume = $idContexteDume;
    }

    public function  getToken() {
        return $this->token;
    }
    public function  getIdContexteDume() {
        return $this->idContexteDume;
    }
}
