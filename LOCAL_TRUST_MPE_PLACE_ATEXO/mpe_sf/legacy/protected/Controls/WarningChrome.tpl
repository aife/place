<div class="modal fade modal-legende message-information-chrome" id="modalWarningChrome" tabindex="-1" role="dialog" aria-labelledby="modalWarningChrome">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="h4 modal-title">
                    <com:TTranslate>TEXT_MSG_INFOS_APPLET_CHROME</com:TTranslate>
                </h2>
            </div>
            <div class="modal-body clearfix">
                <com:PanelMessageAvertissement id="popinMessageWarningChrome" cssClass="message bloc-900" />
            </div>
            <div class="modal-footer">
                <div class="col-md-2 pull-right">
                    <button type="button" class="btn btn-sm btn-block btn-danger" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
</div>

<com:TActiveLabel id="scriptJs" style="display:none;" />