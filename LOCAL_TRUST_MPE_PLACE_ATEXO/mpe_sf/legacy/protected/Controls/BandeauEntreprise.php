<?php

namespace Application\Controls;

use App\Service\Entreprise\Menu\Renderer;
use Application\Service\Atexo\Atexo_Util;

/**
 * Bandeau des pages du coté entreprise
 */
class BandeauEntreprise extends MpeTTemplateControl
{
    public function render($writer)
    {
        $menu = Atexo_Util::getSfService(Renderer::class);

        echo $menu->render();
    }
}
