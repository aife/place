<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Controller de gestion des contenus de reponse transmis.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReponseContenusTransmis extends MpeTTemplateControl
{
    private $_consultation;
    private $_dataSource;
    private $_idOffre;
    private array $_total = ['totalOk' => 0, 'totalNok' => 0, 'totalUnknown' => 0, 'totalUnsigned' => 0, 'totalUnverified' => 0];
    protected $_arrayInfos = [];

    /**
     * recupere la valeur de [_arrayInfos].
     *
     * @return array la valeur courante [_arrayInfos]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getArrayInfos()
    {
        return $this->_arrayInfos;
    }

    /**
     * modifie la valeur de [_arrayInfos].
     *
     * @param array $arrayInfos la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setArrayInfos($arrayInfos)
    {
        $this->_arrayInfos = $arrayInfos;
    }

    /**
     * Recupere la consultation.
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    /**
     * Affecte la consultation.
     */
    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    /**
     * Affecte le DataSource.
     */
    public function setDataSource($dataSource)
    {
        $this->_dataSource = $dataSource;
    }

    /**
     * Recupere le DataSource.
     */
    public function getDataSource()
    {
        return $this->_dataSource;
    }

    /**
     * Recupere la consultation.
     */
    public function getIdOffre()
    {
        return $this->_idOffre;
    }

    /**
     * Affecte la consultation.
     */
    public function setIdOffre($value)
    {
        $this->_idOffre = $value;
    }

    /**
     * Recupere le nom du dossier.
     */
    public function getNomDossier($numLot, $typeEnv)
    {
        $nomDossier = '';
        $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->getConsultation()->getId(), $numLot, $this->getConsultation()->getOrganisme());
        if ($lot instanceof CommonCategorieLot && $lot->getLot()) {
            $nomDossier = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - ';
        }
        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $nomDossier .= Prado::localize('DEFINE_DOSSIER_CANDIDATURE');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $nomDossier .= Prado::localize('DOSSIER_OFFRE_TECHNIQUE');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $nomDossier .= Prado::localize('DEFINE_DOSSIER_OFFRES');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $nomDossier .= Prado::localize('DOSSIER_OFFRE_ANONYMAT');
        }
        if ($lot instanceof CommonCategorieLot && $lot->getLot()) {
            $nomDossier .= ' : '.$lot->getDescription();
        }

        return $nomDossier;
    }

    /**
     * Recupere le nom du dossier.
     */
    public function getNomFormulaire($numLot, $typeEnv)
    {
        $nomFormulaire = '';
        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $nomFormulaire .= Prado::localize('DEFINE_FORMULAIRE_CANDIDATURE');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $nomFormulaire .= Prado::localize('DEFINE_FORMULAIRE_OFFRE');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $nomFormulaire .= Prado::localize('DEFINE_FORMULAIRE_OFFRE');
        } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $nomFormulaire .= Prado::localize('DEFINE_FORMULAIRE_OFFRE');
        }

        return $nomFormulaire;
    }

    /**
     * Permet de determiner le dataSource des repeater des fichiers (pieces typees, editions SUB, pieces libres).
     */
    public function getDataSourceFichiers($dataSourceTypePieces, $typeFichier)
    {
        return $dataSourceTypePieces[$typeFichier]['fichiers'];
    }

    /**
     * Permet de determiner le dataSource des repeater des fichiers (pieces typees, editions SUB, pieces libres).
     */
    public function getDataSourceFichiersEdition($dataSourceTypePieces)
    {
        $arrayEdition = $dataSourceTypePieces[Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION')]['fichiers'];
        $arrayPj = $dataSourceTypePieces[Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')]['fichiers'];
        if ($arrayPj) {
            return array_merge($arrayEdition, $arrayPj);
        }

        return $arrayEdition;
    }

    /**
     * Permet de determiner la cssClass des <td> du repeater des editions SUB.
     */
    public function getCssClassTdRepeaterEditionsSub($itemIndex, $dataSourceEditionsSub)
    {
        $cssClassTr = 'col-piece';
        if (((is_countable($this->getDataSourceFichiersEdition($dataSourceEditionsSub)) ? count($this->getDataSourceFichiersEdition($dataSourceEditionsSub)) : 0) - 1) == $itemIndex) {
            $cssClassTr .= ' piece-last';
        }

        return $cssClassTr;
    }

    /**
     * Construit l'url d'acces au dossier SUB.
     */
    public function getUrl($idEnveloppe, $organisme)
    {
        $url = '?page=Entreprise.PopUpFormulaireEnveloppe';
        $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($idEnveloppe, $organisme);
        if (($dossier instanceof CommonTEnveloppeDossierFormulaire)) {
            $url .= '&idDossier='.$dossier->getIdDossierFormulaire().'&id='.$this->getConsultation()->getId().'&orgAcronyme=';
            $url .= $this->getConsultation()->getOrganisme().'&typeEnv='.$dossier->getTypeEnveloppe().'&lot='.$dossier->getIdLot().'&idEnv='.base64_encode($idEnveloppe);
        }

        return $url;
    }

    /**
     * Recupere le statut du dossier.
     */
    public function getStatutDossier($idEnveloppe, $organisme)
    {
        $statut = null;
								$dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($idEnveloppe, $organisme);
        if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
            $statut = match ($dossier->getStatutValidation()) {
													Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
													Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('DEFINE_EN_VALIDE_FINI'),
													default => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
												};
        }

        return $statut;
    }

    /**
     * Recupere l'image du statut du dossier.
     */
    public function getStatutPictoDossier($idEnveloppe, $organisme)
    {
        $picto = null;
								$dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($idEnveloppe, $organisme);
        if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
            $picto = match ($dossier->getStatutValidation()) {
													Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => 'images/reponse-statut-1sur2.gif',
													Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => 'images/reponse-statut-2sur2.gif',
													default => 'images/reponse-statut-1sur2.gif',
												};
        }

        return $picto;
    }

    /**
     * Permet de recuperer la date de depot de l'offre.
     */
    public function getDateDepotOffre()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $idOffre = $this->getIdOffre();
        $dateDepot = '';
        if ($this->getConsultation() instanceof CommonConsultation) {
            $refCons = $this->getConsultation()->getOrganisme();
            $offre = CommonOffresPeer::retrieveByPK($idOffre, $refCons, $connexion);
            if ($offre instanceof CommonOffres) {
                $this->setViewState('offre', $offre);
                $dateDepot = Atexo_Util::iso2frnDateTime($offre->getUntrusteddate(), false, true);
            }
        }

        return $dateDepot;
    }

    /**
     * Permet de charger la liste des lots.
     */
    public function chargerListeLots()
    {
        if (is_array($this->_dataSource)) {
            $this->repeaterLots->DataSource = $this->_dataSource;
            $this->repeaterLots->DataBind();
        }
    }

    /**
     * Permet de construire le nom du fichier.
     */
    public function getNomFichier($fichier)
    {
        $nomFichier = '';
        if ($fichier instanceof CommonFichierEnveloppe) {
            $nomFichier = (new Atexo_Config())->toPfEncoding($fichier->getNomFichier());
        }

        return $nomFichier;
    }

    /**
     * Permet de construire le lien de telechargement des fichiers.
     */
    public function getLienTelechargementSignature($fichier)
    {
        $lienTelechargement = '';
        if ($fichier instanceof CommonFichierEnveloppe) {
            $lienTelechargement = 'index.php?page=Entreprise.EntrepriseDownloadSignature&fichier='.$fichier->getIdFichier().'&orgAcronyme='.$fichier->getOrganisme();
        }

        return $lienTelechargement;
    }

    /**
     * Permet de telecharger l'integralite des editions.
     */
    public function telechargerIntegraliteEditions($sender, $param)
    {
        $enveloppeDossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdEnveloppeAndOrg($param->CommandParameter, $this->Page->getConsultation()->getOrganisme());
        if ($enveloppeDossier instanceof CommonTEnveloppeDossierFormulaire) {
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($enveloppeDossier->getIdDossierFormulaire());
            if ($dossier instanceof CommonTDossierFormulaire) {
                $tmpZipFile = (new Atexo_FormulaireSub_Edition())->genererIntegraliteEditionsParDossier($dossier->getIdDossierFormulaire());
                //Telechargement du document a la volee
                if (is_file($tmpZipFile)) {
                    DownloadFile::downloadFileContent('liste_Editions_'.$dossier->getIdDossierFormulaire().'.zip', file_get_contents($tmpZipFile));
                }

                return;
            }
        }
    }

    /**
     * retourn la class Css selon module 'InterfaceModuleSub'.
     */
    public function getClassPieceTypees($itemIndex)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
            if (0 == $itemIndex % 2) {
                return 'contenu_01_Offre_lot_01 row-piece-heritee';
            }

            return 'on row-piece-heritee';
        } else {
            if (0 == $itemIndex % 2) {
                return '';
            }

            return 'on';
        }
    }

    /**
     * recupere le picto de signature.
     *
     * @param CommonFichierEnveloppe $fichier
     *
     * @return string $picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getPictoSigned($fichier)
    {
        if ($fichier instanceof CommonFichierEnveloppe && ($fichier->getSignatureFichier() || $fichier->getIdFichierSignature()) && $fichier->getResultatValiditeSignature(null, 'entreprise') != Atexo_Config::getParameter('UNVERIFIED')) {
            return '/picto-certificat-small.gif';
        }

        return '/picto-aucune-signature.gif';
    }

    /**
     * recupere les infos de certificat du signataire.
     *
     * @param mixed $item
     *
     * @return string infos certif
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getInfosSignature($item)
    {
        $typeSignature = null;
								$enveloppeCrypte = null;
								try {
            $dateRemisePlis = '';
            $avecSignature = $this->getConsultation()->getSignatureOffre();
            if ($avecSignature) {
				$fichierEnveloppe = $item->Data;

                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $enveloppe = $fichierEnveloppe->getCommonEnveloppe($connexionCom);

                if ($enveloppe instanceof CommonEnveloppe) {
                    $enveloppeCrypte = $enveloppe->getCryptage();
                }
                $offre = $enveloppe->getOffre();
                if ($offre instanceof CommonOffres) {
                    $xmlStr = $offre->getXmlString();
                    $pos = strpos($xmlStr, 'typeSignature');
                    if ($pos > 0) {
                        $this->setViewState('typeSignature', 'XML');
                        $typeSignature = 'XML';
                    }
                    $dateRemisePlis = $offre->getDateRemisePlie();
                }

                $this->_arrayInfos = current((new Atexo_Consultation_Responses())->getInfosCertif([$fichierEnveloppe], $avecSignature, $typeSignature, $enveloppeCrypte, $this->getPage()->getMaster()->getCalledFrom()));
                $cnSbject = $this->_arrayInfos['CnSubject'] ?? null;
                $cnEmetteur = $this->_arrayInfos['CnEmmeteur'] ?? null;

                self::getTotaux(strtoupper($item->signature->getStatut()));
                self::getInfosDivMessage();
                self::getDetailsPicto();
                self::formateDataInfosSignature($dateRemisePlis);
                self::getInfosJetonSignature($fichierEnveloppe);
                $item->scriptDetails->Text = '<script>function remplir_'.$fichierEnveloppe->getIdFichier().'() {'.$this->BindDetailsSignature($this->_arrayInfos).'}</script>';

                return Prado::localize('CN').' : '.$cnSbject.' ('.Prado::localize('DEFINE_AC').' : '.Prado::localize('CN').'='.$cnEmetteur;
            }
        } catch (Exception $e) {
            Prado::log(' Erreur lors de la recuperation des infos de la signature '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'AtexoReponseContenusTransmis.php');
        }
    }

    /**
     * recupere les infos de certificat du signataire.
     *
     * @param string $dateRemisePlis
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function formateDataInfosSignature($dateRemisePlis)
    {
        $this->_arrayInfos['dateDepot'] = $dateRemisePlis;
        $this->_arrayInfos['dateControle'] = Atexo_Util::iso2frnDateTime(date('Y-m-d H:i:s'));
        $this->_arrayInfos['dateDu'] = Atexo_Util::iso2frnDateTime($this->_arrayInfos['dateDu']);
        $this->_arrayInfos['dateA'] = Atexo_Util::iso2frnDateTime($this->_arrayInfos['dateA']);
    }

    /**
     * recupere les infos du jeton de la signataire.
     *
     * @param CommonFichierEnveloppe $fichierEnveloppe
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getInfosJetonSignature($fichierEnveloppe)
    {
        $this->_arrayInfos['isSignaturePades'] = 'pades' == strtolower($fichierEnveloppe->getTypeSignatureFichier());
        $this->_arrayInfos['urlJetonSignature'] = self::getLienTelechargementSignature($fichierEnveloppe);
        $this->_arrayInfos['titleSignature'] = $this->getTitleSignature(
            $this->_arrayInfos['jetonSignature'],
            $fichierEnveloppe->getSignatureFichier()
        );

        if (Atexo_Module::isEnabled('SurchargeReferentiels') && null !== $fichierEnveloppe->getStatutReferentielCertificat()) {
            $this->_arrayInfos['RefCertif'] = null != $fichierEnveloppe->getNomReferentielFonctionnel() ? $fichierEnveloppe->getNomReferentielFonctionnel() : Prado::localize('DEFINE_NON_REFERENCE');
            $this->_arrayInfos['classRefCertif'] = match (strtoupper($fichierEnveloppe->getStatutValiditeReferentielCertificat($this->_consultation->getSignatureOffre()))) {
													'OK' => 'statut-resultat statut-valide',
													'NOK' => 'statut-resultat statut-erreur',
													'UNKNOWN' => 'statut-resultat statut-avertissement',
													default => 'statut-resultat statut-vide',
												};
        }
    }

    /**
     * recupere les infos de la validite de la signataire
     * permet d'afficher le message de validite et gerer sa classeCss.
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getInfosDivMessage()
    {
        switch ($this->_arrayInfos['resultValiditeSign']['statut']) {
            case Atexo_Config::getParameter('OK'):
                $this->_arrayInfos['classDivMsgPopin'] = 'bloc-message form-bloc-conf msg-confirmation';
                break;
            case Atexo_Config::getParameter('NOK'):
                $this->_arrayInfos['classDivMsgPopin'] = 'bloc-message form-bloc-conf msg-erreur';
                break;
            case Atexo_Config::getParameter('UNKNOWN'):
                $this->_arrayInfos['classDivMsgPopin'] = 'bloc-message form-bloc-conf msg-avertissement';
                break;
        }
        if (null == $this->_arrayInfos['resultValiditeSign']['message']) {
            $this->_arrayInfos['resultValiditeSign']['message'] = Prado::localize('DEFINE_TOUS_CONTROLS_SIGNATURE_VALIDE');
        }
    }

    /**
     * permet de recuperer les totaux des etats des differents fichiers d'une reponse.
     *
     * @param string $statut
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getTotaux($statut)
    {
        switch ($statut) {
            case Atexo_Config::getParameter('OK'):
                $this->_total['totalOk']++;
                break;
            case Atexo_Config::getParameter('NOK'):
                $this->_total['totalNok']++;
                break;
            case Atexo_Config::getParameter('UNKNOWN'):
                $this->_total['totalUnknown']++;
                break;
            case Atexo_Config::getParameter('SANS'):
                $this->_total['totalUnsigned']++;
                break;
            case Atexo_Config::getParameter('UNVERIFIED'):
                $this->_total['totalUnverified']++;
                break;
        }
    }

    /**
     * permet de recuperer le titre du jeton de la signature.
     *
     * @param string $fileNameJeton
     * @param string $signature
     *
     * @return string
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getTitleSignature($fileNameJeton, $signature)
    {
        return Prado::localize('TEXT_TELECHARGER_FICHIER').' - '.Atexo_Util::getContentFileSize($fileNameJeton, base64_decode($signature));
    }

    /**
     *permet de recuperer les pictos de validite.
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getDetailsPicto()
    {
        $pictonValid = Atexo_Controller_Front::t().'/images/picto-check-ok-small.gif';
        $pictoUnknown = Atexo_Controller_Front::t().'/images/picto-validation-incertaine.gif';
        $pictoUnvalid = Atexo_Controller_Front::t().'/images/picto-check-not-ok.gif';
        $msgValid = Prado::localize('TEXT_STATUT_AVIS_VALIDE');
        $msgInvalide = Prado::localize('DEFINE_INVALIDE');
        $msgIncertaine = Prado::localize('DEFINE_INCERTAINE');

        switch (strtoupper($this->_arrayInfos['CertifPeriodValidite'])) {
            case Prado::localize(Atexo_Config::getParameter('OK')):
                $this->_arrayInfos['pictoCertifPeriodValidite'] = $pictonValid;
                                                                          $this->_arrayInfos['altPictoCertifPeriodValidite'] = $msgValid;
                break;
            case Prado::localize(Atexo_Config::getParameter('NOK')):
                $this->_arrayInfos['pictoCertifPeriodValidite'] = $pictoUnvalid;
                                                                          $this->_arrayInfos['altPictoCertifPeriodValidite'] = $msgInvalide;
                break;
            case Prado::localize(Atexo_Config::getParameter('UNKNOWN')):
                $this->_arrayInfos['pictoCertifPeriodValidite'] = $pictoUnknown;
                                                                          $this->_arrayInfos['altPictoCertifPeriodValidite'] = $msgIncertaine;
                break;
        }
        switch (strtoupper($this->_arrayInfos['CertifChaineCertif'])) {
            case Prado::localize(Atexo_Config::getParameter('OK')):
                $this->_arrayInfos['pictoCertifChaineCertif'] = $pictonValid;
                                                                          $this->_arrayInfos['altpictoCertifChaineCertif'] = $msgValid;
                break;
            case Prado::localize(Atexo_Config::getParameter('NOK')):
                $this->_arrayInfos['pictoCertifChaineCertif'] = $pictoUnvalid;
                                                                          $this->_arrayInfos['altpictoCertifChaineCertif'] = $msgInvalide;
                break;
            case Prado::localize(Atexo_Config::getParameter('UNKNOWN')):
                $this->_arrayInfos['pictoCertifChaineCertif'] = $pictoUnknown;
                                                                          $this->_arrayInfos['altpictoCertifChaineCertif'] = $msgIncertaine;
                break;
        }
        switch (strtoupper($this->_arrayInfos['CertifCRL'])) {
            case Prado::localize(Atexo_Config::getParameter('OK')):
                $this->_arrayInfos['pictoCertifCRL'] = $pictonValid;
                                                                          $this->_arrayInfos['altpictoCertifCRL'] = $msgValid;
                break;
            case Prado::localize(Atexo_Config::getParameter('NOK')):
                $this->_arrayInfos['pictoCertifCRL'] = $pictoUnvalid;
                                                                          $this->_arrayInfos['altpictoCertifCRL'] = $msgInvalide;
                break;
            case Prado::localize(Atexo_Config::getParameter('UNKNOWN')):
                $this->_arrayInfos['pictoCertifCRL'] = $pictoUnknown;
                                                                          $this->_arrayInfos['altpictoCertifCRL'] = $msgIncertaine;
                break;
        }
        switch (strtoupper($this->_arrayInfos['SignatireValidite'])) {
            case Prado::localize(Atexo_Config::getParameter('OK')):
                $this->_arrayInfos['pictoSignatureValidite'] = $pictonValid;
                                                                          $this->_arrayInfos['altpictoSignatureValidite'] = $msgValid;
                break;
            case Prado::localize(Atexo_Config::getParameter('NOK')):
                $this->_arrayInfos['pictoSignatureValidite'] = $pictoUnvalid;
                                                                          $this->_arrayInfos['altpictoSignatureValidite'] = $msgInvalide;
                break;
            case Prado::localize(Atexo_Config::getParameter('UNKNOWN')):
                $this->_arrayInfos['pictoSignatureValidite'] = $pictoUnknown;
                                                                          $this->_arrayInfos['altpictoSignatureValidite'] = $msgIncertaine;
                break;
        }
    }

    /**
     * Permet de mettre les totaux des etats des differents fichiers d'une reponse dans la page
     * appel une fct js.
     *
     * @return string
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getTotal()
    {
        return '<script>J("#'.$this->totalOk->getClientId().'").html('.$this->_total['totalOk'].');
	J("#'.$this->totalKo->getClientId().'").html('.$this->_total['totalNok'].');
	J("#'.$this->totalUnknown->getClientId().'").html('.$this->_total['totalUnknown'].');
	J("#'.$this->totalUnsigned->getClientId().'").html('.$this->_total['totalUnsigned'].');
	J("#'.$this->totalUnverified->getClientId().'").html('.$this->_total['totalUnverified'].');</script>';
    }

    /**
     * permet de remplir les details de la signature.
     *
     * @param array $detail tableau des infos de la signature
     *
     * @return string $js
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     esr-2015
     *
     * @copyright Atexo 2015
     */
    public function BindDetailsSignature($detail)
    {
        $js = '';
        foreach ($detail as $key => $value) {
            $js .= 'J("#'.$key.'").html("'.$value.'");';
        }
        $js .= 'J("#pictoPeriodeValidite").attr("src","'.$detail['pictoCertifPeriodValidite'].'");';
        $js .= 'J("#pictoPeriodeValidite").attr("alt","'.$detail['altPictoCertifPeriodValidite'].'");';

        $js .= 'J("#pictoChaineCertif").attr("src","'.$detail['pictoCertifChaineCertif'].'");';
        $js .= 'J("#pictoChaineCertif").attr("alt","'.$detail['altpictoCertifChaineCertif'].'");';

        $js .= 'J("#pictoNonRevocation").attr("src","'.$detail['pictoCertifCRL'].'");';
        $js .= 'J("#pictoNonRevocation").attr("alt","'.$detail['altpictoCertifCRL'].'");';

        $js .= 'J("#NonRepudiation").attr("src","'.$detail['pictoSignatureValidite'].'");';
        $js .= 'J("#NonRepudiation").attr("alt","'.$detail['altpictoSignatureValidite'].'");';

        $js .= 'J("#jetonSignature").attr("href","'.$detail['urlJetonSignature'].'");';
        $js .= 'J("#jetonSignature").attr("title","'.$detail['titleSignature'].'");';

        if ($detail['isSignaturePades']) {
            $js .= 'J("#blocInfoJeton").hide();';
        }
        $js .= 'J("#divMsgPopin").attr("class","'.$detail['classDivMsgPopin'].'");';
        $js .= 'J("#messagePopin").html("'.$detail['resultValiditeSign']['message'].'");';

        $js .= 'J("#RefCertif").attr("class","'.$detail['classRefCertif'].'");';

        return $js;
    }

    public function getOffre()
    {
        $offre = $this->getViewState('offre');
        if (!$offre instanceof CommonOffres) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $idOffre = $this->getIdOffre();
            if ($this->getConsultation() instanceof CommonConsultation) {
                $organisme = $this->getConsultation()->getOrganisme();
                $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexion);
                $this->setViewState('offre', $offre);
            }
        }

        return $offre;
    }

    public function getUrlTelechargementPdfAR()
    {
        $offre = $this->getOffre();
        $return = '';
        if ($this->_consultation instanceof CommonConsultation && $offre instanceof CommonOffres && !empty($offre->getIdPdfEchangeAccuse())) {
            $return = 'index.php?page=Entreprise.DownloadRecapReponse&idFile='
                .  Atexo_Util::getCryptId($offre->getIdPdfEchangeAccuse())
                . '&orgAcronyme='
                . $this->_consultation->getOrganisme()
                . '&callFrom=entreprise';
        }

        return $return;
    }

    public function hasAccuseReception()
    {
        $offre = $this->getOffre();

        return $offre instanceof CommonOffres && !empty($offre->getIdPdfEchangeAccuse());
    }

    /**
     * Permet de verifier les conditions d'affichage des infos de la signature.
     *
     * @param $fichier
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2017
     *
     * @copyright Atexo 2017
     */
    public function verifierConditionsAffichageInfosSignature($fichier)
    {
        return $this->getConsultation() && 1 == $this->getConsultation()->getSignatureOffre()
            && $fichier instanceof CommonFichierEnveloppe
            && (!empty($fichier->getSignatureFichier()) || !empty($fichier->getIdFichierSignature()))
            && $fichier->getIdFichierSignature() != $fichier->getIdFichier();
    }

    /**
     * Permet de verifier si l'offre à une candidature DUME.
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function hasDume()
    {
        $return = false;
        if (Atexo_Module::isEnabled('InterfaceDume') && $this->getConsultation() instanceof CommonConsultation && 1 == $this->getConsultation()->getDumeDemande()) {
            $offre = $this->getOffre();
            if ($offre instanceof CommonOffres) {
                $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);
                if ($candidature instanceof CommonTCandidature) {
                    $return = true;
                }
            }
        }

        return $return;
    }

    /**
     * Permet de retourner l'url du pdf DUME.
     *
     * @return string
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function getUrlTelechargementDume()
    {
        $urlDume = '#';
        if (Atexo_Module::isEnabled('InterfaceDume') && $this->getConsultation() instanceof CommonConsultation && 1 == $this->getConsultation()->getDumeDemande()) {
            $offre = $this->getOffre();
            $lot = 0;
            if ($offre instanceof CommonOffres) {
                $urlDume = '?page=Entreprise.DownloadCandidatureDume&idOffre='.base64_encode($offre->getId()).'&id='.base64_encode($this->getConsultation()->getId()).'&organisme='.base64_encode($this->getConsultation()->getOrganisme());
            }
        }

        return $urlDume;
    }

    /**
     * Permet de retourner l'objet CommonTDumeNumero.
     *
     * @return Obj CommonTDumeNumero ou false
     *
     * @author Florian MARTIN <florian.mart@atexo.com>
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function getInformationDume()
    {
        $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($this->getOffre());
        if ($candidature instanceof CommonTCandidature) {
            $tDumeNumero = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise($candidature->getIdDumeContexte());
            if ($tDumeNumero[0] instanceof CommonTDumeNumero) {
                return $tDumeNumero[0];
            }
        }

        return false;
    }

    /**
     * Permet de retourner le XML du DUME national.
     *
     * @return file
     *
     * @author Florian MARTIN <florian.mart@atexo.com>
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function downloadDumeDemandeXML()
    {
        $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($this->getOffre());
        if ($candidature instanceof CommonTCandidature) {
            $tDumeNumero = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise($candidature->getIdDumeContexte());
            if ($tDumeNumero[0] instanceof CommonTDumeNumero) {
                $tDumeNumero = $tDumeNumero[0];
                $idXML = $tDumeNumero->getBlobIdXml();
                if ($tDumeNumero instanceof CommonTDumeNumero && $idXML && $tDumeNumero->getBlobIdXml() == $idXML) {
                    $organisme = $this->getConsultation()->getOrganisme();
                    (new DownloadFile())->downloadFiles($idXML, $tDumeNumero->getNumeroDumeNational().'.xml', $organisme);
                    exit;
                }
            }
        }
    }

    /**
     * Permet de retourner le PDF du DUME national.
     *
     * @return file
     *
     * @author Florian MARTIN <florian.mart@atexo.com>
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function downloadDumeDemandePDF()
    {
        $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($this->getOffre());
        if ($candidature instanceof CommonTCandidature) {
            $tDumeNumero = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise($candidature->getIdDumeContexte());
            if ($tDumeNumero[0] instanceof CommonTDumeNumero) {
                $tDumeNumero = $tDumeNumero[0];
                $idPDF = $tDumeNumero->getBlobId();
                if ($tDumeNumero instanceof CommonTDumeNumero && $idPDF && $tDumeNumero->getBlobId() == $idPDF) {
                    $organisme = $this->getConsultation()->getOrganisme();
                    (new DownloadFile())->downloadFiles($idPDF, $tDumeNumero->getNumeroDumeNational().'.pdf', $organisme);
                    exit;
                }
            }
        }
    }
}
