<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author Mouslim MITALI<mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoCorriger extends TImageButton
{
    public bool $_activate = false;

    public function onLoad($param)
    {
        if (false == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-corriger.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-corriger.gif');
        }
        $this->setAlternateText(Prado::localize('DESTINATAIRE_CORRIGER'));
        $this->setToolTip(Prado::localize('DESTINATAIRE_CORRIGER'));
    }
}
