<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoRegistreRetraits extends TImageButton
{
    public string $_activate = 'false';
    public string $_clickable = 'true';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-retrait-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-retrait.gif');
        }

        if ('false' == $this->_clickable) {
            $this->Enabled = false;
        }

        $this->setAlternateText(Prado::localize('ICONE_REGISTRE_DES_RETRAITS'));
        $this->setToolTip(Prado::localize('ICONE_REGISTRE_DES_RETRAITS'));
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-retrait-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-retrait.gif');
        }
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($bool)
    {
        $this->_clickable = $bool;
        if ('false' == $this->_clickable) {
            $this->Enabled = false;
        }
    }
}
