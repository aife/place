<!--Debut Bloc Message-->
<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="panel-title">
			<com:TTranslate>TEXT_MESSAGE</com:TTranslate>
		</h2>
	</div>
	<div class="panel-body">
		<div class="form-group form-group-sm clearfix">
			<label class="col-md-2 control-label">
				<com:TTranslate>DEFINE_TEXT_DE</com:TTranslate> :
			</label>
			<div class="col-md-9">
				<com:TLabel ID="expediteur"/>
			</div>
		</div>
		<div class="form-group form-group-sm clearfix">
			<label class="col-md-2 control-label">
				<com:TTranslate>DEFINE_TEXT_A</com:TTranslate> :
			</label>
			<div class="col-md-9">
				<com:TLabel ID="destinataire"/>
			</div>
		</div>
		<div class="form-group form-group-sm clearfix">
			<label class="col-md-2 control-label">
				<com:TTranslate>DEFINE_TEXT_DATE_ENVOI</com:TTranslate> :
			</label>
			<div class="col-md-9">
				<com:TLabel ID="datenvoi"/>
			</div>
		</div>
	</div>
</div>
<blockquote class="blockquote small">
	<div class="form-group form-group-sm clearfix">
		<label class="col-md-2 control-label">
			<com:TTranslate>OBJET</com:TTranslate> :
		</label>
		<div class="col-md-9">
			<com:TLabel ID="objet"/>
		</div>
	</div>
	<div class="form-group form-group-sm clearfix">
		<div class="col-md-2 control-label">
			<com:TTranslate>TEXT_MESSAGE</com:TTranslate> :
		</div>
		<div class="col-md-9"><com:TLabel ID="corps"/></div>
	</div>
	<div class="form-group form-group-sm clearfix">
		<label class="col-md-2 control-label">
			<com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate> :
		</label>
		<div class="col-md-9">
			<com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
				<prop:ItemTemplate>
					<div class="picto-link inline">
						<a href="<%#$this->Data->retrieveUrl($this->Parent->Parent->getCalledFrom(),$this->Parent->Parent->_acronymeOrg,$this->Parent->Parent->_numAccuseReception)%>">
							<%#$this->Data->getNomFichier()%>
							<span class=info-aide> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
						</a>
					</div>
					<br/>
				</prop:ItemTemplate>
			</com:TRepeater>
		</div>
	</div>
    <div class="form-group form-group-sm clearfix">
        <com:TPanel Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ModuleEnvol')%>" ID="dossierVolumineux">
            <div class="spacer-small"></div>
            <div class="line">
                <div class="intitule-100 bold">
					<label class="col-md-2 control-label">
						<com:TTranslate>DOSSIER_VOLUMINEUX</com:TTranslate> :
					</label>
					<div class="col-md-9">
						<com:TLabel ID="DossierVolumineuxLabel"/>
					</div>
				</div>
                <div class="content-bloc bloc-660">
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h2 class="h4 modal-title" id="exampleModalLabel"><com:TTranslate>TELECHARGER_ANNEXES_TECHNIQUES_DCE</com:TTranslate></h2>
                                </div>
                                <div class="modal-body clearfix">
                                    <com:TTranslate>INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE</com:TTranslate>
                                    <div class="text-center clearfix">
                                        <com:TLabel ID="urlDossierVolumineux"/>
                                    </div>
                                </div>
                                <div class="modal-footer clearfix">
                                    <button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><com:TTranslate>TEXT_RETOUR</com:TTranslate></button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </com:TPanel>
    </div>
</blockquote>
<com:TPanel Visible="false" ID="reponseMessage" cssClass="clearfix">
	<com:TButton
			CssClass="btn btn-primary btn-sm pull-right"
			Attributes.value="<%=Prado::localize('REPONSE_AU_MESSAGE')%>"
			Attributes.title="<%=Prado::localize('REPONSE_AU_MESSAGE')%>"
			OnClick="repondre"
	/>
</com:TPanel>
<!--Fin Bloc Liste Message-->