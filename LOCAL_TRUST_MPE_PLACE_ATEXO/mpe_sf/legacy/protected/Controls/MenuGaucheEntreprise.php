<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use DOMDocument;

/**
 * Menu gauche des pages du coté entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class MenuGaucheEntreprise extends MpeTPage
{
    public function onLoad($param)
    {
        /* ****************
        * Supprimer lors du merge !
        $keyWord = $this->quickSearch->Text;
        if ($keyWord) {
                $this->response->redirect(Atexo_Controller_Front::urlPf() ."?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&keyWord=". $keyWord);
        }
         * */
    }

    public function goToSocleIdentifier()
    {
        if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_INDEX_SSO_ENTREPRISE'));
        } elseif (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->response->redirect(Atexo_Config::getParameter('URL_PPP_INSCRIPTION'));
        } elseif (Atexo_Module::isEnabled('AccueilEntreprisePersonnalise')) {
            $this->response->redirect(Atexo_Controller_Front::urlPf().'?page=Entreprise.EntrepriseHome&goto='.urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie').'&inscription');
        } else {
            $this->response->redirect(Atexo_Controller_Front::urlPf().'?page=Entreprise.EntrepriseHome');
        }
    }

    /**
     * personnalisation du menu en fonction des modules activés.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function customizeMenu()
    {
        self::displayMenuArticle133();
        if (!Atexo_CurrentUser::isAgent() && !Atexo_CurrentUser::isAgentSocle() && Atexo_CurrentUser::isConnected() && Atexo_Module::isEnabled('RecherchesFavorites')) {
            self::displayListeMesRecherches();
        }
    }

    /**
     * Permet de lire l'xml indicateurs clés sur le serveur et le stocker en session.
     *
     * @param: l'id de l'element dans le tableau contenant les indicateurs clés.
     *
     * @return l'indicateur clé
     * */
    public function lireXmlIndicateursPortail($element)
    {
        if (Atexo_CurrentUser::readFromSession('indicateursCles')) {
            $res = Atexo_CurrentUser::readFromSession('indicateursCles');

            return $res[$element];
        }
        $fichierXml = Atexo_Config::getParameter('BASE_ROOT_DIR').
                    Atexo_Config::getParameter('DIR_INDICATEURS_CLES').Atexo_Config::getParameter('NAME_XML_INDICATEURS_CLES');
        if (is_file($fichierXml)) {
            $xml = file_get_contents($fichierXml);
            $domDocument = new DOMDocument();
            $domDocument->loadXML(Atexo_Util::toUtf8($xml));
            $statistiquesPortail = $domDocument->getElementsByTagName('StatistiquesPortail')->item(0);
            $data = [];
            $data['nbrConsultationLigne'] = $statistiquesPortail->getAttribute('NombreConsultationsEnLigne');
            $data['nbrConsultationPublies'] = $statistiquesPortail->getAttribute('NombreConsultationsPubliees');
            $data['nbrEntitesPubliques'] = $statistiquesPortail->getAttribute('NombreEntitesPubliques');
            $data['nbrEntrepriseInscrites'] = $statistiquesPortail->getAttribute('NombreEntreprisesInscrites');
            $data['nbrReponsesElectroniq'] = $statistiquesPortail->getAttribute('NombreReponsesElectroniques');
            $data['nbrDCETelecharges'] = $statistiquesPortail->getAttribute('NombreTelechargementsDCE');
            $data['DateDebut'] = Atexo_Util::iso2frnDateTime($statistiquesPortail->getAttribute('DateDebut'), false);
            Atexo_CurrentUser::writeToSession('indicateursCles', $data);

            $res = Atexo_CurrentUser::readFromSession('indicateursCles');

            return $res[$element];
        } else {
            return 0;
        }
    }

    public function displayMenu()
    {
        //Récuperation des pages de chaque menu

        $this->ArrayPages['ANNONCES'] = [
                                            '0' => 'Entreprise.EntrepriseAdvancedSearch', '1' => 'Entreprise.EntrepriseParticipationEnchere', '2' => 'Entreprise.EntrepriseRechercherListeMarches', '3' => 'Entreprise.EntrepriseHome', '4' => 'Entreprise.EntrepriseAccueilAuthentifie', '5' => 'Entreprise.ListePPs', '6' => 'Entreprise.EntrepriseGestionRecherches',
                                            ];
        $this->ArrayPages['ASSISTANCE'] = [
                                            '0' => 'Entreprise.EntrepriseGuide', '1' => 'Entreprise.EntrepriseAide', '2' => 'Entreprise.EntrepriseAutoformation', '3' => 'Entreprise.EntrepriseFaq', '4' => 'Entreprise.AutresOutils', '5' => 'Entreprise.Guides', '6' => 'Entreprise.DocumentsDeReference', '7' => 'Entreprise.EntreprisePremiereVisite',
                                             ];

        $this->ArrayPages['SOCIETES_EXCLUS'] = [
                                            '0' => 'Entreprise.EntrepriseRechercherSocietesExclues',
                                            ];
        $this->ArrayPages['SE_PREPARER'] = [
                                            '0' => 'Entreprise.DiagnosticPoste', '1' => 'Entreprise.EntrepriseAdvancedSearch',
                                            ];
        $this->ArrayPages['OUTILS_DE_SIGNATURE'] = [
                                    '0' => 'Entreprise.SignDocument', '1' => 'Entreprise.VerifierSignature',
                                    ];
        $this->ArrayPages['PANIER_ENTREPRISE'] = [
                                    '0' => 'Entreprise.EntrepriseAdvancedSearch', '1' => 'Entreprise.EntrepriseDetailsConsultation',
                                    ];
        $this->ArrayPages['BOURSE'] = [
                                     '0' => 'Entreprise.RechercheCollaborateur',
                                    ];
        $this->ArrayPages['GUIDES'] = [
                                    '0' => 'Entreprise.EntrepriseGuide', '1' => 'Entreprise.Guides',
                                    ];
        $this->ArrayPages['ENTITES_ACHAT'] = [
                                    '0' => 'Entreprise.EntrepriseVisualiserEntitesAchatsRecherche',
                                    ];
    }

    /**
     * permet d'aficher le menu des rechrches favorites enregistrée.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function displayListeMesRecherches()
    {
        $idCreateur = Atexo_CurrentUser::getIdInscrit();
        $typeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE');
        $this->repeaterRechercheFavoris->DataSource = (new Atexo_Entreprise_Recherches())->retreiveRecherches($idCreateur, $typeCreateur, true);
        $this->repeaterRechercheFavoris->DataBind();
        $this->repeaterRechercheFavorisAvis->DataSource = (new Atexo_Entreprise_Recherches())->retreiveRecherches($idCreateur, $typeCreateur, true, false, false);
        $this->repeaterRechercheFavorisAvis->DataBind();
    }

    /**
     * permet d'aficher le menu article 133.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function displayMenuArticle133()
    {
        $this->panelArticle133RechercheAvancee->Visible = Atexo_Module::isEnabled('Article133GenerationPf');
        $this->panelArticle133Telecharger->Visible = Atexo_Module::isEnabled('Article133UploadFichier');
    }
}
