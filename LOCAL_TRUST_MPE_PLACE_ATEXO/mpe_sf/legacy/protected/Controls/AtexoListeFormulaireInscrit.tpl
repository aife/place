<com:TPanel id="listeFormsEnv" Visible="<%=count($this->repeaterFormulaireEnveloppe->DataSource)>0%>">
<div class="spacer-mini"></div>
<!--Debut Bloc Bordereau des Prix et formulaires-->
<div class="form-bloc margin-0">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content float-none">
		<h2><com:TTranslate>TEXT_FORMULAIRES</com:TTranslate> :</h2>
		<table class="table-results tableau-gestion-doc-prepa" summary="Liste des formulaires">
			<thead>
				<tr>
					<th id="formCand_col_01">Type de formulaire</th>
					<th id="formCand_col_03" class="col-50 center">Statut</th>
					<th id="formCand_col_04" class="actions-small">Actions</th>
				</tr>

			</thead>
			<com:TRepeater ID="repeaterFormulaireEnveloppe">
				<prop:ItemTemplate>
					<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
						<td headers="formCand_col_01"><%#$this->Data->getLibelleTypeFormulaire()%>
						<com:THiddenField id="statutForm" value="<%#$this->Data->getStatut()%>"/>
						</td>
						<td headers="formCand_col_03" class="col-50 center"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoStatut($this->Data->getStatut())%>" alt="<%#Prado::localize('DEFINE_STATUS').' :' .$this->TemplateControl->getTitleStatut($this->Data->getStatut())%>" title="<%#Prado::localize('DEFINE_STATUS').' :' .$this->TemplateControl->getTitleStatut($this->Data->getStatut())%>" class="statut" /></td>
						<td headers="formCand_col_04" class="actions-small">
							<a href="<%#$this->TemplateControl->getUrlVisualisationFormCons($this->Data->getTypeFormulaire(),$this->Data->getId())%>">
								<com:TImage 
									ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif"
									Attributes.alt="<%#Prado::localize('TEXT_VISUALISER')%>"
									ToolTip="<%#Prado::localize('TEXT_VISUALISER')%>"
								/>
							</a>								
						</td>
					</tr>
				</prop:ItemTemplate>
			</com:TRepeater>
		</table>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Bordereau des Prix et formulaires-->
<div class="spacer-mini"></div>
</com:TPanel>
