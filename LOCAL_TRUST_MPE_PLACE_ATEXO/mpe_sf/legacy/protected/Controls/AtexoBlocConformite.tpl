<div class="panel panel-default">
	<div class="panel-heading">
		<h2 class="h4 panel-title"><com:TTranslate>DEFINE_CONFORMITE</com:TTranslate></h2>
	</div>
	<div class="content panel-body">
		<div class="checkbox">
			<label for="ctl0_CONTENU_PAGE_blocConformite_obligDeclaration">
				<com:TActiveImage id="imgObligDeclaration" cssClass="m-r-1"  Style="display:<%=$this->getModeAffichage()?'':'none'%>" />
				<com:TCheckBox id="obligDeclaration" Attributes.title="<%=Prado::localize('MESSAGE_MPE_DECLARATION_HONNEUR')%>" cssclass="check" Style="display:<%=$this->getModeAffichage()?'none':''%>" />
				<com:TTranslate>MESSAGE_MPE_DECLARATION_HONNEUR</com:TTranslate>
			</label>
		</div>
		<div class="line">
			<div class="page-header">
				<h3 class="h4 panel-title">
					<com:TTranslate>VOLET_ASSURANCES_ET_RESPONSABILITE</com:TTranslate>
				</h3>
			</div>
			<div class="checkbox">
				<label for="ctl0_CONTENU_PAGE_blocConformite_assuranceContracte">
					<com:TActiveImage id="imgAssuranceContracte" cssClass="m-r-1" Style="display:<%=$this->getModeAffichage()?'':'none'%>" />
					<com:TCheckBox id="assuranceContracte" Attributes.title="<%=Prado::localize('DEFINE_ENTREPRISE_ASSUREE')%>" cssclass="check" Style="display:<%=$this->getModeAffichage()?'none':''%>"  />
					<com:TTranslate>DEFINE_MESSAGE_ENTREPRISE_ASSUREE</com:TTranslate>
				</label>
			</div>
		</div>
	</div>
</div>
<com:TActiveLabel ID="scriptJs" />