	<!--Debut colonne gauche-->
	<div class="left-part" id="left-part">
		<!--Debut menu -->
		<div id="menu">
			<com:TPanel visible="<%= ! ($this->isConnected())%>">
			<ul id="menuList">
				<li class="menu-open"><span><a href="?page=Agent.AgentHome"><com:TTranslate>DEFINE_INDENTIFIER</com:TTranslate></a></span></li>
			</ul>
			<div class="breaker"></div>
			</com:TPanel>
			<com:TPanel visible="<%=($this->isConnected()) ? true:false%>">
			<ul id="menuList">
				<li class="<%=(true)?'menu-open menu-on':'menu-open'%>">
				<span><a href="javascript:void(0);" onclick="toggleMenu('menuAnnuaire');" accesskey="p">
					<com:TTranslate>TEXT_ANNUAIRE</com:TTranslate></a>
				</span>
					<ul class="ss-menu-open" id="menuAdminPmi" style="display:block">
						<com:TPanel id="plateForm">
							<li class="ss-menu-rub"><com:TTranslate>TEXT_ACHTEURS_PUBLICS</com:TTranslate></li>
							<li class="off">
								<com:TPanel ID="linkSearchAgent" CssClass="config" visible="true">
									<a href="index.php?page=Agent.SearchAchteursPublics"><com:TTranslate>RECHERCHER</com:TTranslate></a>
								</com:TPanel>
								<com:TPanel ID="nolinkSearchAgent" CssClass="config" visible="false">
									<a class="inactive"><com:TTranslate>RECHERCHER</com:TTranslate></a>
								</com:TPanel>
							</li>
						</com:TPanel>
					</ul>
				</li>
			</ul>
			<div class="breaker"></div>	
			</com:TPanel>
</div>
<div class="menu-bottom"></div>

<!--Fin menu -->
</div>