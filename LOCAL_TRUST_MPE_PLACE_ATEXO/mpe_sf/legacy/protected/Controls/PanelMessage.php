<?php

namespace Application\Controls;

/**
 * Panel des message.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelMessage extends MpeTPage
{
    private ?string $_typeMessage = 'msg-confirmation';
    public string $cssClass = 'message bloc-700 confirmation-bloc';
    public string $style = '';

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function setStyle($value)
    {
        $this->style = $value;
    }

    public function getStyle()
    {
        if ('' != $this->style) {
            return 'style="'.$this->style.'"';
        }

        return '';
    }

    public function setMessage($message)
    {
        $this->labelMessage->Text = $message;
        $this->labelMessageSuccess->Text = $message;
    }

    public function setTypeMessage($value)
    {
        $this->_typeMessage = $value;
    }

    public function getTypeMessage()
    {
        return $this->_typeMessage;
    }
}
