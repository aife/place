<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoOk extends TImageButton
{
    public function onLoad($param)
    {
        $this->setImageUrl(Atexo_Controller_Front::t().'/images/bouton-ok.gif');
        $this->setCssClass('ok');
        $this->setAlternateText(\Prado::localize('ICONE_OK'));
        $this->setToolTip(\Prado::localize('ICONE_OK'));
    }
}
