<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="line">
			<div class="intitule-150 bold"><com:TTranslate>DEFINE_NUMERO_COMPLEMENT</com:TTranslate> : </div>
			<div class="content-bloc bloc-580">
				<%=($this->getComplementFormulaire() instanceof Application\Propel\Mpe\CommonTComplementFormulaire && $this->getComplementFormulaire()->getNumeroComplement())?
				$this->getComplementFormulaire()->getNumeroComplement():'-'%>
			</div>
		</div>
		<div class="line">
			<div class="intitule-150 bold"><com:TTranslate>DEFINE_A_REMETTRE_AVANT_LE</com:TTranslate> : </div>
			<div class="content-bloc bloc-580">
				<com:TLabel cssClass="<%=$this->getClasseDateARemettre($this->getComplementFormulaire())%>">
					<%=($this->getComplementFormulaire() instanceof Application\Propel\Mpe\CommonTComplementFormulaire && $this->getComplementFormulaire()->getDateARemettre())?
					 Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->getComplementFormulaire()->getDateARemettre(), true):'-'%>
				</com:TLabel>
			</div>
		</div>
		<div class="line">
			<div class="intitule-150 bold"><com:TTranslate>DEFINE_REMIS_LE</com:TTranslate> : </div>
			<div class="content-bloc bloc-580">
				<%=($this->getComplementFormulaire() instanceof Application\Propel\Mpe\CommonTComplementFormulaire && $this->getComplementFormulaire()->getDateRemisLe())?
				 Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->getComplementFormulaire()->getDateRemisLe(), true):'-'%>
			</div>
		</div>
		<div class="line">
			<div class="intitule-150 bold"><com:TTranslate>TEXT_MOTIF</com:TTranslate> : </div>
			<div class="content-bloc bloc-580">
				<%=($this->getComplementFormulaire() instanceof Application\Propel\Mpe\CommonTComplementFormulaire && $this->getComplementFormulaire()->getMotif())?
				$this->getComplementFormulaire()->getMotif():'-'%>
			</div>
		</div>
		<div class="line">
			<div class="intitule-150 bold"><com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate> : </div>
			<div class="content-bloc bloc-580">
				<%=($this->getComplementFormulaire() instanceof Application\Propel\Mpe\CommonTComplementFormulaire && $this->getComplementFormulaire()->getCommentaire())?
				str_replace("\n", "<br/>",$this->getComplementFormulaire()->getCommentaire()) : '-'%>
			</div>
		</div>
		<div class="table-bloc">
			<table summary="<%=Prado::localize('DEFINE_DEMANDE_COMPLEMENTS')%> - <%=$this->getNomDossier()%>" class="table-results tableau-reponse tableau-complements">
				<thead>
					<tr class="row-title" style="<%=($this->getDossierFormulaire() instanceof Application\Propel\Mpe\CommonTDossierFormulaire)? 'display:;':'display:none;'%>">
						<th class="title" colspan="<%#($this->getCallDepuisDemandeComplementTransmis() === '0')?'3':'2'%>" style="<%=($this->Page->getConsultation()->getAlloti())? 'display:;':'display:none;'%>">
							<a href="javascript:void(0);" onclick="showHidePanelClass(this,'panelDossiersDemandeComplement_lot_<%#$this->getNumLotConcatIdComplement()%>');" class="title-toggle-open">
								<com:TLabel id="infosLots" Text="<%=$this->getNomDossier()%>"/>
							</a>
						</th>
						<th class="title" colspan="<%#($this->getCallDepuisDemandeComplementTransmis() === '0')?'3':'2'%>" style="<%=(!$this->Page->getConsultation()->getAlloti())? 'display:;':'display:none;'%>"></th>
						<th class="actions">
							<com:TActiveLinkButton
								OnCallBack="SourceTemplateControl.supprimerDossier"
								ActiveControl.CallbackParameter=<%#$this->getDossierFormulaire()->getIdDossierFormulaire()%>
								visible="false"
							>
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
							</com:TActiveLinkButton>
						</th>
					</tr>
				</thead>
				<tbody>

					<com:AtexoFormulaireSub
						id="idComposantAtexoFormulaireSub"
						IdHtmlFormulaire="panelDossiersDemandeComplement_lot_<%#$this->getNumLotConcatIdComplement()%> panelFormulaireDemandeComplement_lot_<%#$this->getNumLotConcatIdComplement()%>"
						IdHtlmToggleFormulaire="panelFormulaireDemandeComplement_lot_<%#$this->getNumLotConcatIdComplement()%>"
						ClassFormulaire="on row-title-2 panelDossiersDemandeComplement_lot_<%#$this->getNumLotConcatIdComplement()%>"
						VerifyCallBack="true"
						IdComplementFormulaire="<%=$this->getComplementFormulaire()->getIdComplementFormulaire()%>"
						CallDepuisDemandeComplementTransmis="<%#$this->getCallDepuisDemandeComplementTransmis()%>"
						callFrom="demandeComplement"
					/>
				</tbody>
			</table>
			<!--Fin tableau pieces Candidatures-->

			<!-- Debut bouton télécharger l'ensemble des éditions -->
			<div class="link-line margin-top-10">
				<div class="float-left">
					<com:TLinkButton
						cssClass="bouton-telecharger-long230"
						style="display:<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub') && $this->getCallDepuisDemandeComplementTransmis() === '1')?'':'none'%>"
						OnCommand ="telechargerIntegraliteEditions"
						CommandParameter="<%#$this->getComplementFormulaire()->getIdComplementFormulaire()%>"
					>
						<com:TTranslate>DEFINE_TELECHARGER_INTEGRALITE_EDITIONS</com:TTranslate>
					</com:TLinkButton>
				</div>
			</div>
			<!-- Fin bouton télécharger l'ensemble des éditions -->

		</div>
		<div class=" breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<div class="breaker"></div>
