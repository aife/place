<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalDetailEntreprise" tabindex="-1" role="dialog" aria-labelledby="modalDetailEntreprise">
            <div class="modal-dialog modal-lg" role="document">
                <com:TForm Attributes.name="form" Attributes.class="form" Attributes.method="POST">
                    <div class="modal-content form-horizontal">
                        <div class="modal-header">
                            <h2 class="h4 modal-title" id="myModalLabel">
                                <com:TTranslate>TEXT_DETAIL_ENTREPRISE</com:TTranslate>
                            </h2>
                        </div>
                        <div class="modal-body">
                            <div class="popup-moyen" id="container">
                                <div class="panel-group m-0" id="accordion" role="tablist" aria-multiselectable="true">
                                    <!--BEGIN IDENTIFICATION ENTREPRISE-->
                                    <com:IdentificationEntreprise id="idIdentificationEntreprise"/>
                                    <!--BEGIN IDENTIFICATION ENTREPRISE-->

                                    <!--BEGIN COLLABORATION SOUHAITEE-->
                                    <com:TPanel id="panelCollaboration" cssClass="panel panel-default" Visible="<%=($this->getCalledFrom() == 'agent') ? false : true %>">
                                        <div class="panel-heading" id="collaboration" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCollaboration" aria-expanded="false"
                                             aria-controls="collapseCollaboration">
                                            <h2 class="panel-title h4">
                                                <%=Prado::localize('DEFINE_COLLABORATION_SOUHAITEE')%>
                                            </h2>
                                        </div>
                                        <div id="collapseCollaboration" class="panel-collapse collapse" role="tabpanel" aria-labelledby="collaboration">
                                            <div class="panel-body">
                                                <div id="typeCollaboration" class="small clearfix">
                                                    <div class="line">
                                                        <div class="content-bloc">
                                                            <div class="collaboration">
                                                                <com:TRepeater ID="repeaterTypeCollaboration">
                                                                    <prop:ItemTemplate>
                                                                        <div class="statut-collaborateur statut-collaborateur-<%#$this->Data['id']%>sur4"
                                                                             title="<%#($this->Data['libelle'])%>"></div>
                                                                    </prop:ItemTemplate>
                                                                </com:TRepeater>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('NOM')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="nomContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('PRENOM')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="prenomContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('TEXT_ADRESSE')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="adresseContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('TEXT_ADRESSE_SUITE')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="adresseSuiteContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('TEXT_CP')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="cpContact"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('TEXT_VILLE')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="VilleContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('DEFINE_FONCTION_MAJ')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="fonctionContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('DEFINE_E_MAIL')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="emailContact"/>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix">
                                                            <label class="col-md-5"><%=Prado::localize('TEXT_TELEPHONE')%> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel ID="telContact"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </com:TPanel>
                                    <!--END COLLABORATION SOUHAITEE-->

                                    <!--BEGIN ACTIVITE-->
                                    <div class="panel panel-default">
                                        <div class="panel-heading" id="activite" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseActivite" aria-expanded="false"
                                             aria-controls="collapseActivite">
                                            <h2 class="panel-title h4">
                                                <com:TTranslate>DEFINE_ACTIVITE</com:TTranslate>
                                            </h2>
                                        </div>
                                        <div id="collapseActivite" class="panel-collapse collapse" role="tabpanel" aria-labelledby="activite">
                                            <div class="panel-body">
                                                <div id="activiteEntreprise" class="small clearfix">
                                                    <com:TPanel id="labelActiviteEntreprise" Visible="true">
                                                        <div class="clearfix">
                                                            <label class="col-md-5">
                                                                <com:TTranslate>DEFINE_SITE_INTERNET</com:TTranslate>
                                                                :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel id="labelSiteInternet"/>
                                                            </div>
                                                        </div>
                                                        <!-- Début domaines d'activités -->
                                                        <com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel'))? true:false %>">
                                                            <label>
                                                                <com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>
                                                                : </label>
                                                            <com:AtexoReferentiel ID="idAtexoRefDomaineActivite" cas="cas9"/>
                                                        </com:TPanel>
                                                        <!-- Fin domaines d'activités -->
                                                        <div class="line">
                                                            <label class="col-md-5"><%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel'))?
                                                                Prado::localize('DEFINE_DESCRIPTION_GENERALE_ACTIVITES') : Prado::localize('DEFINE_DESCRIPTION_ACTIVITE') %> :</label>
                                                            <div class="col-md-7">
                                                                <com:TLabel id="labelDescriptionActivite"/>
                                                            </div>
                                                        </div>
                                                        <!-- -->
                                                        <com:TPanel ID="panelActiviteDomaineDefense" cssClass="line" Display="Dynamic" visible="<%=($this->getCalledFrom() == 'agent')? true: false %>">
                                                            <label>
                                                                <com:TTranslate>DEFINE_ACTIVITES_DOMAINE_DEFENSE</com:TTranslate>
                                                                :</label>
                                                            <com:TLabel id="labelActiviteDefense"/>
                                                        </com:TPanel>
                                                    </com:TPanel>

                                                    <com:TPanel ID="panelDocumentsComerciaux" cssClass="line" Display="None">
                                                        <label>
                                                            <com:TTranslate>TEXT_DOCUMENTS_COMMERCIAUX</com:TTranslate>
                                                            :</label>
                                                        <com:TPanel ID="panelLinkDownloadPj" cssClass="picto-link margin-0" display="None">
                                                            <img Attributes.title="<%=Prado::localize('TEXT_TELECHARGER_FICHIER')%>"
                                                                 Attributes.alt="<%=Prado::localize('TEXT_TELECHARGER_FICHIER')%>"
                                                                 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"/>
                                                            <com:TLinkButton onClick="downloadPiece">
                                                                <com:TLabel ID="labelleDocCommercial"/>
                                                                (
                                                                <com:TLabel ID="sizeDocCommercial"/>
                                                                )
                                                            </com:TLinkButton>
                                                        </com:TPanel>
                                                    </com:TPanel>

                                                    <com:TPanel ID="panelQualification" Display="Dynamic" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')? true : false %>">
                                                        <label>
                                                            <com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
                                                            :</label>
                                                        <com:TLabel ID="labelQualifications"/>
                                                    </com:TPanel>

                                                    <com:AtexoLtReferentiel id="ltrefEntr" mode="0" entreprise="1" cssClass="intitule-250 bold"/>

                                                    <!-- Début qualification des entreprises Lt-Ref -->
                                                    <com:TActivePanel ID="panelQualificationLtRef" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel'))? true:false %>"
                                                                      cssClass="line">
                                                        <label for="qualification"><strong>
                                                            <com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
                                                        </strong></label> :
                                                        <com:AtexoReferentiel ID="idAtexoRefQualification" cas="cas9"/>
                                                    </com:TActivePanel>
                                                    <!-- Fin qualification des entreprises Lt-Ref -->

                                                    <com:TPanel ID="panelAgrements" Display="None" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')? true : false %>">
                                                        <label>
                                                            <com:TTranslate>TEXT_AGREMENT</com:TTranslate>
                                                            :</label>
                                                        <com:TLabel ID="labelAgrements"/>
                                                    </com:TPanel>

                                                    <!-- Début certification des entreprises Lt-Ref -->
                                                    <com:TActivePanel ID="panelcertification" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel'))? true : false %>"
                                                                      cssClass="line">
                                                        <label for="certification"><strong>
                                                            <com:TTranslate>TEXT_AGREMENT_LT_REFERENTIEL</com:TTranslate>
                                                        </strong></label> :
                                                        <com:AtexoReferentiel ID="idAtexoCertificatCompteEtpRef" cas="cas9"/>
                                                    </com:TActivePanel>
                                                    <!-- Fin certification des entreprises Lt-Ref -->

                                                    <com:TPanel ID="panelMoyensHumains" cssClass="line" Display="None">
                                                        <label>
                                                            <com:TTranslate>TEXT_MOYENS_HUMAINS</com:TTranslate>
                                                            :</label>>
                                                        <com:TLabel ID="labelMoyensHumains"/>
                                                    </com:TPanel>

                                                    <com:TPanel ID="panelMoyensTechnique" cssClass="line" Display="None">
                                                        <label>
                                                            <com:TTranslate>TEXT_MOYENS_TECHNIQUES</com:TTranslate>
                                                            :</label>
                                                        <com:TLabel ID="labelMoyensTechniques"/>
                                                    </com:TPanel>

                                                    <com:TActivePanel ID="panelPrestation" Display="None">
                                                        <com:PrestationEntreprise ID="prestationEntreprise"/>
                                                    </com:TActivePanel>

                                                    <com:ActiviteEntreprise id="idActiviteEntreprise" visible="<%=($this->getCalledFrom() == 'agent')? true : false %>"/>
                                                    <com:InfoDonneesComplementaires id="idDonneesComplementaires"
                                                                                    Visible="<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && ($this->getCalledFrom() == 'agent')))?true:false %>"/>
                                                    <!--Debut Bloc Document du coffre-fort-->

                                                    <com:TPanel cssclass="toggle-panel"
                                                                visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AccesAgentsCfeBdFournisseur', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())? true:false)%>">
                                                        <div class="content">
                                                            <div class="title-toggle" onclick="togglePanel(this,'docsEntreprise');">
                                                                <com:TTranslate>TEXT_DOCUMENTS_ENTREPRISE</com:TTranslate>
                                                            </div>
                                                            <div class="panel" style="display:none;" id="docsEntreprise">
                                                                <com:AtexoTableauDocCfe ID="tableauDocumentsDuCoffreFort"></com:AtexoTableauDocCfe>
                                                                <h2>
                                                                    <com:TLabel ID="aucunDocCoffreFort" visible="false"></com:TLabel>
                                                                </h2>
                                                            </div>
                                                        </div>
                                                    </com:TPanel>

                                                    <!--Fin Bloc Document du coffre-fort-->
                                                    <!--Debut Bloc etablissements-->
                                                    <com:TPanel ID="panelEtablissements" Display="None">
                                                        <com:AtexoEtablissements ID="listeEtablissments"/>
                                                    </com:TPanel>
                                                    <!--Fin Bloc etablissements-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--END ACTIVITE-->
                                </div>

                                <!--Bloc message-->
                                <com:TPanel id="messageSaisieManuel" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSGMAP') && ($this->getCalledFrom() == 'agent'))? true:false %>"
                                            cssClass="form-bloc margin-0">
                                    <div class="content">
                                        <p class="infos-line margin-0">
                                            <%=Prado::localize('TEXT_MESSAGE_INFO_SAISIE_MANUEL')%>
                                        </p>
                                    </div>
                                </com:TPanel>
                                <!--Fin Bloc message-->

                                <com:DirigeantsEntreprise id="idDirigeantsEntreprise" visible="<%=($this->getCalledFrom() == 'agent')? true: false%>"/>

                                <!-- Debut Bloc Adresses Alectroniques -->
                                <com:AtexoContactDevis visible="<%=($this->getCalledFrom() == 'agent')? true: false%>"/>
                                <!--Fin Bloc Adresses Alectronique -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <button type="reset" class="btn btn-sm btn-default btn-reset" data-dismiss="modal">
                                        <%=Prado::localize('DEFINE_ANNULER')%>
                                    </button>
                                </div>
                                <div class="pull-right">
                                    <button class="btn btn-sm btn-primary" title="<%=Prado::localize('TEXT_IMPRIMER')%>" onclick="J('#modalDetailEntreprise').printThis(); return false">
                                        <%=Prado::localize('TEXT_IMPRIMER')%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </com:TForm>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>