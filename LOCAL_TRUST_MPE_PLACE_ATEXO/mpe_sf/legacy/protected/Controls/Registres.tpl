<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</h2>
</com:TPanel>


<!--Debut Tableau Liste retraits electronique-->
<com:TActivePanel ID="panelListRegistre">
		<com:TPanel ID="panelNombreResulat" CssClass="line-partitioner">
			<h2><com:TActiveLabel ID="textNombreRegistre"/> : <com:TActiveLabel ID="nombreResultat"/></h2>
		</com:TPanel>
		<!--Debut partitionneur-->
			<div class="line-partitioner">
			<div class="partitioner">
				<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
				<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
					<com:TListItem Text="10" Selected="10" />
					<com:TListItem Text="20" Value="20" />
					<com:TListItem Text="50" Value="50" />
					<com:TListItem Text="100" Value="100" />
					<com:TListItem Text="500" Value="500" />
				</com:TDropDownList>
				<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
					<label style="display:none;" for="allCons_pageNumberTop">Aller à la page</label>
					<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
						<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
						<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
						<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
					</com:TPanel>
				<div class="liens">
					<com:TPager ID="PagerTop"
						ControlToPaginate="dataListRegistre"
						FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
				   	 	LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
				    	Mode="NextPrev"
						NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
						PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
						OnPageIndexChanged="pageChanged"/>
				</div>
			</div>
		</div>
		<!--Fin partitionneur-->
		<com:TRepeater ID="dataListRegistre"
			AllowPaging="true"
			PageSize="10"
  			EnableViewState="true"
  			AllowCustomPaging="true" >
			<prop:HeaderTemplate>
					<table summary="Retraits au format électronique" class="table-results">
						<caption>Retraits au format électronique</caption>
						<thead>
							<tr><th class="top" colspan="<%=$this->TemplateControl->getColSpan()%>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th></tr>

							<tr>
								<th class="col-70" id="retrait_el_num"><com:TActiveLinkButton ID="trierNum" onCommand="Parent.TemplateControl.critereTri" onCallBack="Parent.TemplateControl.Trier"><com:TTranslate>NUMERO</com:TTranslate><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" /></com:TActiveLinkButton> /<br /> <com:TTranslate>TEXT_DATE_HEURE</com:TTranslate></th>
								<th class="col-110" id="retrait_el_entreprise"><a href="#"><com:TActiveLinkButton ID="trierEntreprise" onCommand="Parent.TemplateControl.critereTri" CommandName="entreprise" onCallBack="Parent.TemplateControl.Trier"><com:TTranslate>DEFINE_TEXT_STATISTIQUES_ENTREPRISE</com:TTranslate><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" /></com:TActiveLinkButton></a></th>
								<th class="<%=$this->TemplateControl->isRegistrePapier()? 'col-150':'col-170'%>" id="retrait_el_contact" ><com:TTranslate>CONTACT</com:TTranslate></th>
								<th class="<%=$this->TemplateControl->isRegistrePapier()? 'col-180':'col-130'%>" id="retrait_el_adresse" ><com:TTranslate>TEXT_ADRESSE</com:TTranslate></th>
								<com:TLabel ID="retraitFichier" Visible="<%=!$this->TemplateControl->isRegistreDepot()%>">
									<th class="col-150" id="retrait_el_Fichiers"><com:TLabel ID="colonneFichierQuestion" Text="<%=$this->TemplateControl->getNomColonne()%>"/></th>
								</com:TLabel>
								<th class="<%=$this->TemplateControl->isRegistrePapier()? 'col-250':'col-200'%>" id="retrait_el_observations"><com:TTranslate>OBSERVATIONS</com:TTranslate></th>
								<com:TLabel  Visible="<%=$this->TemplateControl->isRegistrePapier() || ($this->TemplateControl->isRegistreElectronique()&& $this->TemplateControl->isRegistreQuestion())%>">
									<th class="col-50" id="retrait_el_actions">Actions</th>
								</com:TLabel>
							</tr>
						</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
				<com:TLabel id="styleTR" Text="<%=($this->ItemIndex%2==0 ? '':'on')%>" Attributes.style="display:none;"/>
							<tr class="<%#$this->styleTR->Text%>">
								<td class="col-70" headers="retrait_el_num">
										<com:TLabel Text="<%=$this->TemplateControl->getElectronicOrPaper()%>. <%#$this->Data->getNumRegistre()%><br />
										<%=($this->Page->getLotsUsedDepot($this->Data->getIdEntreprise(), $this->Data->getIdRegistre()))%><br /><br /><%=str_replace(' ','<br />',$this->Data->getHorodatage())%>"/>
										<com:TPanel visible="<%=$this->TemplateControl->isRegistreDepot()&& Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationVariantesAutorisees')%>">
											<br /><br />
											<com:TImage
													visible="<%=$this->TemplateControl->isIconeOffreVarianteDisplayed($this->Data)%>"
													ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-offre-variante.gif"
													Attributes.alt="<%=Prado::localize('TEXT_OFFRE_VARIANTE')%>"
													Attributes.title="<%=Prado::localize('TEXT_OFFRE_VARIANTE')%>" />
											<com:TImage visible="<%=$this->TemplateControl->isIconeOffreBaseDisplayed($this->Data)%>"
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-offre-base.gif"
														Attributes.alt="<%=Prado::localize('TEXT_OFFRE_BASE')%>"
														Attributes.title="<%=Prado::localize('TEXT_OFFRE_BASE')%>" />
										</com:TPanel>
								</td>

								<td class="col-110" headers="retrait_el_entreprise">
									<span class="blue"><com:TLabel Text="<%#$this->Data->getNomEntreprise()%>"/></span>
									<br />
									<com:TLabel Text="<%#$this->Data->getAccronymePaysEntreprise()%> - <%#$this->Data->getSirenEntreprise()%> "/>
									<com:TLabel Text="<%#$this->Data->getNicEntreprise()%>"/>


									<com:TPanel Visible="<%=($this->TemplateControl->isRegistreRetrait() && ($this->Page->getTiragePlan() != 0)) ? true:false%>">
										<div class="envoi-comp-line" title="<%=Prado::localize('TEXT_ENVOI_POSTAL_COMPLEMENTAIRE')%>">
											<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-cd-rom.gif" Attributes.alt="<%=Prado::localize('TEXT_CDR')%>" Attributes.title="<%=Prado::localize('TEXT_FORMAT_CDR')%>" Visible="<%=($this->Data->getTiragePlan() && ($this->Data->getSupport() == '2' || $this->Data->getSupport() == '3')) ? true:false%>"/>
												<span class="intitule"> <com:TLabel Visible="<%=($this->Data->getTiragePlan() && ($this->Data->getSupport() == '3')) ? true:false%>"> + </com:TLabel> </span>
											<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-papier.gif" Attributes.alt="<%=Prado::localize('TEXT_PAPIER')%>" Attributes.title="<%=Prado::localize('TEXT_FORMAT_PAPIER')%>" Visible="<%=($this->Data->getTiragePlan() && ($this->Data->getSupport() == '1' || $this->Data->getSupport() == '3')) ? true:false%>"/>
										</div>
									</com:TPanel>


									<com:TPanel ID="divSpacerAnnuleLe" CssClass="spacer-small" Visible="<%#$this->Data->getRegistreAnnule()%>"></com:TPanel>
									<com:TPanel ID="divAnnuleLe" CssClass="red" Visible="<%#$this->Data->getRegistreAnnule()%>">
											<com:TTranslate>ANNULE_LE</com:TTranslate> : <com:TLabel Text=" <%#$this->Data->getDateAnnulation()%>"/>
									</com:TPanel>

								</td>
								<td class="col-150" headers="retrait_el_contact">
									<%#$this->Data->getPrenomContact()%> <%#$this->Data->getNomContact()%><br />
									<com:TImage Visible="<%#$this->Data->getMailContact()!= '-'%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" AlternateText="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" ToolTip="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" /><span class="ltr"><com:TLabel Text="<%#($this->Data->getMailContact())? $this->Data->getMailContact():'-'%>"/></span><br />
									<com:TImage Visible="<%#$this->Data->getTelephoneContact()!= '-'%>"  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" AlternateText="<%=Prado::localize('TEXT_TELEPHONE')%>" ToolTip="<%=Prado::localize('TEXT_TELEPHONE')%>" /> <span class="ltr"><com:TLabel Text="<%#($this->Data->getTelephoneContact())? $this->Data->getTelephoneContact():'-'%>"/></span><br />
									<com:TImage Visible="<%#$this->Data->getFaxContact()!= '-'%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" AlternateText="<%=Prado::localize('TEXT_TELECOPIEUR')%>" ToolTip="<%=Prado::localize('TEXT_TELECOPIEUR')%>" /> <span class="ltr"><com:TLabel Text="<%#($this->Data->getFaxContact())? $this->Data->getFaxContact():'-'%>"/></span>
								</td>

								<td class="col-130" headers="retrait_el_adresse">
										<com:TLabel Text="<%#$this->Data->getAdresseContact()%><%=$this->TemplateControl->addBrBeforeNotEmptyText($this->Data->getAdresseContactSuite())%>"/>
										<com:TLabel Text="<%#$this->TemplateControl->addBrBeforeNotEmptyText($this->Data->getCodePostalContact().' '.$this->Data->getVilleContact())%>"/>
										<com:TLabel Text="<%#$this->TemplateControl->addBrBeforeNotEmptyText($this->Data->getPaysEntreprise())%>"/>
								</td>
								<com:TLabel ID="colonne5" Visible="<%=!$this->TemplateControl->isRegistreDepot()%>">
								<td class="col-150 break-word" headers="retrait_el_Fichiers">
									<com:TLabel ID="label2" Visible="<%=$this->TemplateControl->isRegistreRetrait()%>">
										<com:TRepeater ID="fichiers" DataSource="<%#$this->Data->getFichiers()%>"	>
											<prop:ItemTemplate>
												<com:TLabel Text="<%= (strcmp(substr($this->Data, 0 ,25), Application\Service\Atexo\Atexo_Config::getParameter('DEFINE_DCE_INTEGRAL')) == 0) ? Prado::localize(Application\Service\Atexo\Atexo_Config::getParameter('DEFINE_DCE_INTEGRAL')) : substr($this->Data, 0 ,25)%>"/>
												<com:TLabel Attributes.style="<%= strlen($this->Data)>12 ? 'position:relative':'display:none'%>" Attributes.onmouseover="afficheBulle('<%=($this->getClientId())%>_fichierRetrait', this)" Attributes.onmouseout="cacheBulle('<%=($this->getClientId())%>_fichierRetrait')" CssClass="info-suite">...</com:TLabel>
												<com:TPanel ID="fichierRetrait" CssClass="info-bulle" Attributes.onmouseover="mouseOverBulle();" Attributes.onmouseout="mouseOutBulle();">
													<div><com:TLabel Text="<%= $this->Data %>"/></div>
												</com:TPanel>
												<br />
											</prop:ItemTemplate>
										</com:TRepeater>
									</com:TLabel>

									<com:TLabel Visible="<%=$this->TemplateControl->isRegistreQuestion()%>">
										<%#$this->Data->getQuestion()%><br /><br />
										<com:TRepeater DataSource="<%# $this->Data->getFichiers() %>"	>
											<prop:ItemTemplate>
												<com:THyperLink NavigateUrl="index.php?page=Agent.DownLoadQuestion&id=<%= $this->Data['reference'] %>&idQuestion=<%= $this->Data['idQuestion'] %>&type=<%= $this->Data['typeQuestion']%> "><%= substr($this->Data['nomFichier'],0 ,12) %></com:THyperLink>
												<span style="<%=strlen($this->Data['nomFichier'])>10 ? 'position:relative':'display:none'%>" onmouseover="afficheBulle('<%=($this->getClientId())%>_fichierQuestion', this)" onmouseout="cacheBulle('<%=($this->getClientId())%>_fichierQuestion')" class="info-suite">...</span>
												<com:TPanel ID="fichierQuestion" CssClass="info-bulle" Attributes.onmouseover="mouseOverBulle();" Attributes.onmouseout="mouseOutBulle();">
													<div><%=$this->Data['nomFichier']%></div>
												</com:TPanel>
												<%=$this->Data['tailleFichier']%><br />
											</prop:ItemTemplate>
										</com:TRepeater>
									</com:TLabel>
								</td>
								</com:TLabel>
								<td class="col-200" headers="retrait_el_observations">
									<com:TLabel Visible="<%=$this->TemplateControl->isRegistreQuestion()%>">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-user-grey.gif" alt="<%=Prado::localize('AGENT_CHARGE_REPONSE')%>" title="<%=Prado::localize('AGENT_CHARGE_REPONSE')%>" /> : <%#$this->Data->getResponder()%><br />
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" alt="<%=Prado::localize('DATE_TRAITEMENT')%>" title="<%=Prado::localize('DATE_TRAITEMENT')%>" /> : <%#$this->Data->getDateReponse()%>
										<br />
									</com:TLabel>
									<com:TLabel Text="<%#$this->Data->getObservation()%><br />"/>

									<com:THyperLink NavigateUrl="javascript:popUp('?page=Agent.popUpObservationRegistre&id=<%#$this->Data->getIdRegistre()%>&type=<%#$this->Data->getTypeRegistre()%>&callBackButton=<%=$this->TemplateControl->ID.'_'%>refreshRepeater','yes');">
										 <com:PictoEditImage
											 visible="<%#($this->Page->isEtatArchiveRealiseConsulotation() || ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='false')) ? false:true%>"
											 Activate="true"
											 attributes.alt="<%=Prado::localize('GERER_OBSERVATION')%>"
											 attributes.title="<%=Prado::localize('GERER_OBSERVATION')%>"
										 >
									 </com:PictoEditImage></com:THyperLink>
									 <com:PictoEdit
										 visible="<%#$this->Page->isEtatArchiveRealiseConsulotation() && ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true')%>"
										 attributes.alt="<%=Prado::localize('GERER_OBSERVATION')%>"
										 attributes.title="<%=Prado::localize('GERER_OBSERVATION')%>"/>
								</td>
								<com:TLabel  Visible="<%=$this->TemplateControl->isRegistrePapier()%>">
										<td class="col-50" headers="depot_pap_actions">
												  <com:THyperLink NavigateUrl="javascript:popUp('?page=Agent.popUpAjoutRegistrePapier&id=<%#$this->Data->getIdRegistre()%>&consultationId=<%=$this->TemplateControl->getReference()%>&callBackButton=<%=$this->TemplateControl->ID.'_'%>refreshRepeater&typeRegistre=<%#$this->TemplateControl->getTypeRegistre()%>','yes');">
													  <com:PictoEditImage visible="<%#($this->Page->isEtatArchiveRealiseConsulotation() || ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='false')) ? false:true%>" Activate="true"
													  	>
												  	  </com:PictoEditImage>
												  </com:THyperLink>
												  <div class="spacer-mini"></div>
												  <com:PictoDeleteActive Visible="<%# $this->TemplateControl->isRegistrePapier() && $this->TemplateControl->registreDepotClosed($this->Data->getIdRegistre()) && ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true') %>" Attributes.onclick='return confirm("<%=Prado::localize('MESSAGE_CONFIRME_REGISTRE_PAPIER')%>")' CommandName="<%#$this->Data->getIdRegistre()%>" OnCommand="SourceTemplateControl.deleteRegistrePapier" OnCallBack="SourceTemplateControl.onCallBackRefreshRepeater"/>
												  <div class="spacer-mini"></div>
												  <com:TLabel Visible="<%=$this->TemplateControl->isRegistreDepot()  && Application\Service\Atexo\Atexo_Module::isEnabled('AnnulerDepot')%>">
												  		<a onclick='return confirm("<%=Prado::localize('MESSAGE_CONFIRME_ANNULER_REGISTRE_PAPIER')%>")'
														     href="javascript:popUp('?page=Agent.popUpDateAnnulation&consultationId=<%#$this->Data->getId()%>&id=<%#$this->Data->getIdRegistre()%>&type=<%#$this->Data->getTypeRegistre()%>&callBackButton=<%=$this->TemplateControl->ID."_"%>refreshRepeater','yes');">
														 		<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-annuler.gif"
																  			Attributes.alt="<%=Prado::localize('ANNULER_OFFRE')%>"
																  			ToolTip="<%=Prado::localize('ANNULER_OFFRE')%>"
																  			Visible="<%#$this->TemplateControl->isRegistrePapier() && $this->TemplateControl->registreDepotClosed($this->Data->getIdRegistre()) && !$this->Data->getRegistreAnnule()&& ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true') %>" />
													 	 </a>
														  <com:TActiveImageButton ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-restaurer.gif"
													  								Attributes.alt="<%=Prado::localize('RESTAURER_OFFRE')%>"
																		  			ToolTip="<%=Prado::localize('RESTAURER_OFFRE')%>"
																		  			Visible="<%#($this->TemplateControl->isRegistrePapier())&& $this->TemplateControl->registreDepotClosed($this->Data->getIdRegistre()) && $this->Data->getRegistreAnnule()&& ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true') %>"
																		  			Attributes.onclick='return confirm("<%=Prado::localize('MESSAGE_CONFIRME_RESTAURER_REGISTRE_PAPIER')%>")'
																		  			CommandName="<%#$this->Data->getIdRegistre()%>"
																		  			OnCommand="SourceTemplateControl.restaurerRegistrePapier"
																		  			OnCallBack="SourceTemplateControl.onCallBackRefreshRepeater"/>
												</com:TLabel>
                                                <com:TLabel  Visible="<%#$this->TemplateControl->isRegistreQuestion() && ($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true') && $this->TemplateControl->isRegistrePapier() && !($this->TemplateControl->isRegistreRetrait() || $this->TemplateControl->isRegistreDepot())%>">
                                                        <com:TActiveLinkButton
       	                                    				Attributes.title='<%=Prado::localize("DEFINE_MSG_REPONDRE_QUESTION")%>'
		       	                                    		OnCallBack="SourceTemplateControl.setIdItem"
				           									ActiveControl.CallbackParameter="<%#$this->Data->getIdRegistre()%>"
		       	                                    	>
			                                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%# ($this->TemplateControl->isQuestionAnswered($this->Data->getIdRegistre()))?'picto-suivi-msg-envoye.gif':'picto-suivi.gif' %>" alt="<%=Prado::localize('DEFINE_REPONDRE')%>" title="<%# ($this->TemplateControl->isQuestionAnswered($this->Data->getIdRegistre()))?Prado::localize('DEFINE_TEXT_REPONSE_QUESTION_ENVOYEE'):Prado::localize('DEFINE_TEXT_REPONSE_QUESTION')%>" />
			                                            </com:TActiveLinkButton>
                                                </com:TLabel>
										</td>
									</com:TLabel>
									<com:TLabel  Visible="<%=$this->TemplateControl->isRegistreQuestion() && $this->TemplateControl->isRegistreElectronique() && !($this->TemplateControl->isRegistreRetrait() || $this->TemplateControl->isRegistreDepot())%>">
								        <td class="actions" headers="question_el_actions">
								        	<com:TPanel id="pictoReponse" visible="<%#($this->Page->getPictoActive($this->Data->getTypeRegistre())=='true')%>">
       	                                    	<com:TActiveLinkButton
       	                                    		Attributes.title='<%=Prado::localize("DEFINE_MSG_REPONDRE_QUESTION")%>'
       	                                    		OnCallBack="SourceTemplateControl.setIdItem"
		           									ActiveControl.CallbackParameter="<%#$this->Data->getIdRegistre()%>"
                                                    visible="<%#!$this->TemplateControl->isReadOnly()%>"
       	                                    		>
	                                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%# ($this->TemplateControl->isQuestionAnswered($this->Data->getIdRegistre()))?'picto-suivi-msg-envoye.gif':'picto-suivi.gif' %>" alt="<%=Prado::localize('DEFINE_REPONDRE')%>" title="<%# ($this->TemplateControl->isQuestionAnswered($this->Data->getIdRegistre()))?Prado::localize('DEFINE_TEXT_REPONSE_QUESTION_ENVOYEE'):Prado::localize('DEFINE_TEXT_REPONSE_QUESTION')%>" />
	                                            </com:TActiveLinkButton>
	                                        </com:TPanel>
                                        </td>
                                    </com:TLabel>
							</tr>
				</prop:ItemTemplate>
				<prop:FooterTemplate>
					</table>
				</prop:FooterTemplate>
		</com:TRepeater>
		<!--Debut partitionneur-->
		<div class="line-partitioner">
			<div class="partitioner">
				<div class="intitule"><strong><label for="decision_nbResultsBottom"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
				<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
					<com:TListItem Text="10" Selected="10"/>
					<com:TListItem Text="20" Value="20"/>
					<com:TListItem Text="50" Value="50" />
					<com:TListItem Text="100" Value="100" />
					<com:TListItem Text="500" Value="500" />
				</com:TDropDownList>
				<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
				<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
					<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
					<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
					<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
				</com:TPanel>
				<div class="liens">
					<com:TPager ID="PagerBottom"
						ControlToPaginate="dataListRegistre"
						FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
						LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
						Mode="NextPrev"
						NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
						PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
						OnPageIndexChanged="pageChanged"/>
				</div>
			</div>
		</div>
		<!--Fin partitionneur-->
</com:TActivePanel>
<com:TLabel  ID="labelAjouterRetraitPapier" visible='<%=((!$this->Page->isEtatArchiveRealiseConsulotation()) && ($this->isRegistrePapier()) && ($this->Page->getPictoActive($this->getTypeRegistre()) == "true")) ? true:false%>'>
	<a href="javascript:popUp('?page=Agent.popUpAjoutRegistrePapier&consultationId=<%=$this->getReference()%>&callBackButton=<%=$this->ID."_"%>refreshRepeater&typeRegistre=<%=$this->getTypeRegistre()%>','yes');" class="ajout-el"><%=$this->getAjouterTypeRegistre()%></a>
</com:TLabel>
<com:TActiveButton ID="refreshRepeater" onCallBack="onCallBackRefreshRepeater" Attributes.style="display:none"/>
<com:TLabel ID="javaScript" Attributes.syle="display:none"/>

