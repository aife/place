<com:TActivePanel id="panelVariantes" style="display:<%=($this->afficher)? '':'none'%>" cssclass="toggle-panel form-toggle donnees-redac">
	<div class="content">
		<div class="cover"></div>
		<a class="title-toggle-open" onclick="togglePanel(this,'<%=$this->panelSousVariantes->getClientId()%>');" href="javascript:void(0);"><com:TTranslate>DEFINE_CCAG</com:TTranslate> - <com:TTranslate>DEFINE_VARIANTE</com:TTranslate></a>
		<com:TPanel cssClass="panel no-indent panel-donnees-complementaires" style="display:block;" id="panelSousVariantes">
			<!--Debut Ligne CCAG-->
			<div class="line">
				<div class="intitule col-300"><label for="CcagRef"><com:TTranslate>DEFINE_CCAG_REFERENCE</com:TTranslate></label> <span class="champ-oblig">*</span>:</div>
				<com:TDropDownList 
					id="CcagRef" 
					CssClass="moyen float-left"
					Attributes.title="<%=Prado::localize('DEFINE_CCAG_REFERENCE')%>"
					Enabled="<%=$this->enabledElement()%>"
				/>
				<div class="col-info-bulle">
					<img title="Info-bulle"
						 alt="Info-bulle"
						 class="picto-info-intitule"
						 onmouseout="cacheBulleWithJquery(this)"
						 onmouseover="afficheBulleWithJquery(this)"
						 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
					/>
					<div id="infosCCAGVariantes"
						 class="info-bulle"
						 onmouseover="mouseOverBulle();"
						 onmouseout="mouseOutBulle();">
						<div><com:TTranslate>TEXT_INFO_BULLE_CCAG_VARIANTES</com:TTranslate></div>
					</div>
				</div>
			</div>
			<!--Fin Ligne CCAG-->
			<div class="spacer-small"></div>
			<!--Debut Ligne Droits de propriété intellectuelle-->
			<div id="divCcagDpi"class="line">
				<div class="intitule col-300"><label for="dpi"><com:TTranslate>DEFINE_DROITS_PROPRIETE_INTELECTUELLE</com:TTranslate></label>&nbsp;:</div>
				<com:TDropDownList
						id="CcagDpi"
						CssClass="moyen"
						Attributes.title="<%=Prado::localize('DEFINE_DROITS_PROPRIETE_INTELECTUELLE')%>"
						Enabled="<%=$this->enabledElement()%>"
						/>
				<div class="spacer-small"></div>
			</div>
			<!--Fin Ligne Droits de propriété intellectuelle-->
			<!--Debut Ligne Variantes-->
			<div class="line">
				<div class="intitule col-300"><com:TTranslate>DEFINE_VARIANTE</com:TTranslate> :</div>
				<div class="content-bloc check-list">
					<div class="intitule-auto">
						<com:TActiveCheckBox 
							id="varianteAutorisee" 
							Attributes.title="<%=Prado::localize('DEFINE_VARIANTE_AUTORISEE')%>" 
							CssClass="check"
							Enabled="<%=$this->enabledElement()%>" 
						/>
						<label for="varianteAutorisee"><com:TTranslate>DEFINE_VARIANTE_AUTORISEE</com:TTranslate></label>
					</div>
					<div class="intitule-auto clear-both">
						<com:TActiveCheckBox
							id="varianteExigee"
							Attributes.title="<%=Prado::localize('DEFINE_VARIANTE_EXIGEE')%>"
							CssClass="check"
							Enabled="<%=$this->enabledElement()%>"
						/>
						<label for="varianteAutorisee"><com:TTranslate>DEFINE_VARIANTE_EXIGEE</com:TTranslate></label>
					</div>
					<div class="intitule-auto clear-both">
						<com:TActiveCheckBox
								id="varianteTechniquesObligatoires"
								Attributes.title="<%=Prado::localize('PRESTATION_SUPPLEMENTAIRE_EVENTUELLES')%>"
								CssClass="check"
								Enabled="<%=$this->enabledElement()%>"
								Attributes.onclick="isCheckedShowDiv(this,'<%=$this->desccription_varvianteTech->getClientId()%>');"
						/>
						<label for="varianteTechniquesObligatoires"><com:TTranslate>PRESTATION_SUPPLEMENTAIRE_EVENTUELLES</com:TTranslate></label>
					</div>
					<div class="breaker"></div>

					<com:TPanel  cssClass="content-bloc-auto" id="desccription_varvianteTech" style="display:none;">
						<com:TTextBox
								id="descVariantesTech"
								TextMode="MultiLine"
								Attributes.MaxLength="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_DUREE_MARCHE_LIBRE')%>"
								CssClass="moyen"
								Attributes.title="<%=Prado::localize('DEFINE_DESC_VARIANTES_TECHNIQUES_OBLIGATOIRES')%>"
						/>
					</com:TPanel>

				</div>
			</div>
		</com:TPanel>
		<div class="breaker"></div>
		<com:TLabel ID="scriptLabel" style="display:none" />
	</div>
</com:TActivePanel>
