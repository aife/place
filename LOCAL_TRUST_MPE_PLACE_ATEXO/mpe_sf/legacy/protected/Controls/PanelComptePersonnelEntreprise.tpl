<link href="../themes/css/bootstrap-toggle.min.css" rel="stylesheet">
<com:TPanel id="panelComptePersonnelEntreprise" cssClass="panel panel-default">
    <div class="panel-heading">
        <com:TLabel ID="msgInscrit"/>
    </div>
    <div class="panel-body">
        <div class="section">
            <div class="section-heading page-header m-t-0">
                <h3 class="h4 m-0">
                    <com:TTranslate>TEXT_INFORMATIONS_PERSONNELLES</com:TTranslate>
                </h3>
            </div>
            <div class="section-body clearfix">
                <!--Debut Bloc Mes informations personnelles-->
                <div class="col-md-6">
                    <com:TPanel ID="panelNomPerso" cssClass="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_nomPerso" class="col-md-4 control-label">
                            <com:TTranslate>TEXT_NOM_SANS_POINTS</com:TTranslate>
                            <span class="text-danger">*</span> :
                        </label>
                        <div class="col-md-8">
                            <com:TTextBox id="nomPerso" Attributes.title="<%=Prado::localize('TEXT_NOM_SANS_POINTS')%>" cssClass="form-control"/>
                        </div>
                        <com:TRequiredFieldValidator
                                ControlToValidate="nomPerso"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_NOM_SANS_POINTS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </com:TPanel>
                    <com:TPanel ID="panelPrenomPerso" cssClass="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <div class="clearfix">&nbsp;</div>
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_prenomPerso" class="col-md-4 control-label">
                            <com:TTranslate>TEXT_PRENOM_SANS_POINTS</com:TTranslate>
                            <span class="text-danger">*</span> :
                        </label>
                        <div class="col-md-8">
                            <com:TTextBox id="prenomPerso" Attributes.title="<%=Prado::localize('TEXT_PRENOM_SANS_POINTS')%>" cssClass="form-control"/>
                        </div>
                        <com:TRequiredFieldValidator
                                ControlToValidate="prenomPerso"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_PRENOM_SANS_POINTS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </com:TPanel>
                    <div class="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <div class="clearfix">&nbsp;</div>
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_emailPersonnel" class="control-label col-md-4">
                            <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
                            <span class="text-danger">*</span> :
                        </label>
                        <div class="col-md-8">
                            <com:TTextBox id="emailPersonnel" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" cssClass="form-control"/>
                        </div>
                        <com:TRequiredFieldValidator
                                ControlToValidate="emailPersonnel"
                                ID="mailValidator"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ControlToValidate="emailPersonnel"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ID="emailValidator"
                                ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
                                ClientValidationFunction="validatorEmailNonObligatoire"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span> ">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_telPersonnel" class="control-label col-md-4">
                            <com:TTranslate>TEXT_TEL</com:TTranslate>
                            <span class="text-danger">* </span> :
                        </label>
                        <div class="col-md-8">
                            <com:TTextBox id="telPersonnel" Attributes.title="<%=Prado::localize('TEXT_TEL')%>" cssClass="form-control"/>
                        </div>
                        <com:TRequiredFieldValidator
                                ControlToValidate="telPersonnel"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_TEL')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TRegularExpressionValidator
                                ID="formattelPersonnel"
                                Display="Dynamic"
                                ValidationGroup="validateCreateCompte"
                                ControlToValidate="telPersonnel"
                                Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte')%>"
                                RegularExpression="<%= Application\Service\Atexo\Atexo_Config::getParameter('EXPRESSION_REGULIERE_VALIDATION_TELEPHONE_FAX')%>"
                                ErrorMessage="<%=Prado::localize('FORMAT_TEL')%>"
                                Text="<span title='Format incorrecte' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRegularExpressionValidator>
                        <com:TCustomValidator
                                ID="nombreCaracteresNumTel"
                                ValidationGroup="validateCreateCompte"
                                ControlToValidate="telPersonnel"
                                ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                Display="Dynamic"
                                ErrorMessage="<%=str_replace('XXX',  Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_TEL'))%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>"
                                EnableClientScript="true">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                    <div class="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <div class="clearfix">&nbsp;</div>
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_faxPersonnel" class="control-label col-md-4">
                            <com:TTranslate>TEXT_FAX</com:TTranslate>
                            :
                        </label>
                        <div class="col-md-8">
                            <com:TTextBox id="faxPersonnel" Attributes.title="<%=Prado::localize('TEXT_FAX')%>" cssClass="form-control"/>
                        </div>
                        <com:TRegularExpressionValidator
                                ID="formatfaxPersonnel"
                                Display="Dynamic"
                                ValidationGroup="validateCreateCompte"
                                ControlToValidate="faxPersonnel"
                                Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte')%>"
                                RegularExpression="<%= Application\Service\Atexo\Atexo_Config::getParameter('EXPRESSION_REGULIERE_VALIDATION_TELEPHONE_FAX')%>"
                                ErrorMessage="<%=Prado::localize('FORMAT_FAX')%>"
                                Text="<span title='Format incorrecte' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Format incorrecte' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRegularExpressionValidator>
                        <com:TCustomValidator
                                ID="nombreCaracteresNumFax"
                                ValidationGroup="validateCreateCompte"
                                ControlToValidate="faxPersonnel"
                                ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                Display="Dynamic"
                                ErrorMessage="<%=str_replace('XXX',  Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_FAX'))%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                EnableClientScript="true">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
                <!--Fin Bloc Mes informations personnelles-->
            </div>
        </div>
        <!--Debut Bloc Mes informations Identification-->
        <div class="section">
            <div class="form-field"
            <%=(!Application\Service\Atexo\Atexo_Module::isEnabled('AuthenticateInscritByLogin') && !Application\Service\Atexo\Atexo_Module::isEnabled('AuthenticateInscritByCert'))? 'style="display:none"' :''%>>
            <div class="section-heading page-header m-t-3">
                <h3 class="h4 m-0">
                    <com:TTranslate>TEXT_INFORMATIONS_IDENTIFICATION</com:TTranslate>
                </h3>
            </div>
            <com:TPanel id="panelAuthLoginPass" cssclass="section-body clearfix">
                <div class="container-fluid">
                    <com:TPanel ID="panelLogin" cssClass="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_identifiant" class="control-label col-md-2">
                            <com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate>
                            <span class="text-danger">* </span> :
                        </label>
                        <div class="col-md-10">
                            <com:TTextBox id="identifiant" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>" cssClass="form-control"/>
                        </div>
                        <com:TCustomValidator
                                ControlToValidate="identifiant"
                                ClientValidationFunction="validateLogin"
                                ID="loginValidator"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCustomValidator
                                ControlToValidate="identifiant"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ID="loginCreationValidator"
                                onServerValidate="Page.PanelInscrit.verifyLoginCreation"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        </com:TCustomValidator>
                        <com:TCustomValidator
                                ControlToValidate="identifiant"
                                ValidationGroup="validateCreateCompte"
                                ID="loginModificationValidator"
                                EnableClientScript="true"
                                onServerValidate="Page.PanelInscrit.verifyLoginModification"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        </com:TCustomValidator>
                    </com:TPanel>
                    <div class="form-group form-group-sm">
                        <div class="clearfix">&nbsp;</div>
                        <com:TPanel id="panelOldPassword">
                            <label for="ctl0_CONTENU_PAGE_PanelInscrit_oldPassword" class="control-label col-md-2">
                                <com:TTranslate>OLD_PASSWORD_TEXT</com:TTranslate>
                                <span class="text-danger">*</span> :
                            </label>
                            <div class="col-md-10">
                                <com:TTextBox id="oldPassword" PersistPassword="true" text="xxxxxxx" TextMode="Password" Attributes.title="<%=Prado::localize('OLD_PASSWORD_TEXT')%>" cssClass="form-control"/>
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </com:TPanel>
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_password" class="control-label col-md-2">
                            <span data-toggle="tooltip" data-html="true" title="<com:TTranslate>REGLE_VALIDATION_MDP</com:TTranslate>" class="m-r-1">
                                <i class="fa fa-question-circle text-info"></i>
                            </span>
                            <com:TTranslate>TEXT_MOT_PASSE_SANS_POINT</com:TTranslate>
                            <span class="text-danger">*</span> :
                        </label>
                        <div class="col-md-10">
                            <com:TTextBox id="password" PersistPassword="true" TextMode="Password" Attributes.title="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>" cssClass="form-control"/>
                        </div>
                        <com:TCustomValidator
                                ControlToValidate="password"
                                ID="passwordValidator"
                                ClientValidationFunction="validatePassword"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCustomValidator
                                ValidationGroup="validateCreateCompte"
                                ControlToValidate="password"
                                ClientValidationFunction="verifierMdp"
                                ErrorMessage="<%=Prado::localize('REGLE_VALIDATION_MDP')%>"
                                visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? true:false%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                    <!--  -->
                    <div class="form-group form-group-sm <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                        <div class="clearfix">&nbsp;</div>
                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_confPassword" class="control-label col-md-2">
                            <com:TTranslate>TEXT_CONFIRMATION_MOT_PASSE</com:TTranslate>.
                            <span class="text-danger">* </span> :
                        </label>
                        <div class="col-md-10">
                            <com:TTextBox id="confPassword" TextMode="Password" PersistPassword="true" Attributes.title="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>" cssClass="form-control"/>
                        </div>
                        <com:TCustomValidator
                                ControlToValidate="confPassword"
                                ID="confPasswordValidator"
                                ValidationGroup="validateCreateCompte"
                                ClientValidationFunction="validateConfPassword"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCompareValidator
                                ValidationGroup="validateCreateCompte"
                                ID="confPasswordCompareValidator"
                                ControlToValidate="password"
                                ControlToCompare="confPassword"
                                DataType="String"
                                Display="Dynamic"
                                Operator="Equal"
                                ErrorMessage="<%=Prado::localize('TEXT_DEUX_PASSWORD_DIFF')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>
                    </div>
                </div>
            </com:TPanel>
            <!--  -->
            <div class="picto-link inline">
                <!-- Bloc Identification par certificat -->
                <div class="content">
                    <div class="line">
                        <span style="display:none"><com:TRadioButton ID="CaseLogin" Checked="true"/></span>
                        <div id="div_cn_certif" style="display:none">
                            <div class="intitule-150"><label for="<%=$this->getClientId()%>cnCertif">
                                <com:TTranslate>TEXT_PROPRIETAIRE_CERTIF</com:TTranslate>
                            </label></div>
                            <com:TLabel ID="cnCertif"/>
                            <com:TCustomValidator
                                    ControlToValidate="CaseLogin"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    ID="certifCreationValidator"
                                    EnableClientScript="true"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                    onServerValidate="Page.PanelInscrit.verifyCertifCreation"/>
                            <com:TCustomValidator
                                    ControlToValidate="CaseLogin"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    ID="certifModificationValidator"
                                    EnableClientScript="true"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                    onServerValidate="Page.PanelInscrit.verifyCertifModification"/>
                        </div>
                    </div>
                    <div class="line">
                        <div id="div_ac_certif" style="display:none">
                            <div class="intitule-150"><label for="<%=$this->getClientId()%>acCertif">
                                <com:TTranslate>TEXT_AC_CERTIF</com:TTranslate>
                            </label></div>
                            <com:TLabel ID="acCertif"/>
                        </div>
                        <div class="spacer-mini"></div>
                        <com:TPanel id="panelChoixCert">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arrow-blue.gif" alt=""/>
                            <com:THyperLink ID="linkChoixCert" Text="<%=Prado::localize('TEXT_PASSER_CERTIF_NUMERIQUE')%>"
                                            NavigateUrl="javascript:popUpSetSize('secure/getCert.php?<%=session_name()%>=<%=session_id()%>','10px','20px','yes');"/>
                        </com:TPanel>
                        <com:TPanel id="panelChoixLoginMdp">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arrow-blue.gif" alt=""/>
                            <com:TLinkButton ID="linkChoixLoginMdp" Text="<%=Prado::localize('TEXT_PASSER_MODE_LOGIN_PASS')%>" onclick="goToModeLoginPassword"/>
                        </com:TPanel>
                    </div>
                </div>
                <!-- Fin block identification par certificat -->
            </div>
        </div>
        <!--Fin Bloc Mes informations Identification-->
        <!--Debut Bloc Message-->
        <!--Fin Bloc Message-->
        <!--Debut Bloc Informations Profil-->
        <com:TPanel ID="InfoProfil" cssClass="form-group form-group-sm clearfix" Display="<%=(Application\Service\Atexo\Atexo_CurrentUser::isATES() && Application\Service\Atexo\Atexo_Module::isEnabled('AutoriserModifProfilInscritAtes')) ? 'Dynamic':'None'%>">
            <div class="clearfix">&nbsp;</div>
            <div class="container-fluid">
                <label class="control-label col-md-2 <%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? '':'overflow-hidden'%>">
                    <com:TTranslate>DEFINE_PROFIL_USER_RNTREPRISE</com:TTranslate> :
                </label>
                <div class="col-md-10">
                    <label class="radio-inline" for="ctl0_CONTENU_PAGE_PanelInscrit_inscritSimple">
                        <com:TRadioButton enabled="<%=($this->isProfilActif())? true:false%>" id="inscritSimple" GroupName="RadioGroup"  Attributes.title="<%=Prado::localize('DEFINE_UES_SIMPLE')%>"/>
                        <com:TTranslate>DEFINE_UES_SIMPLE</com:TTranslate>
                    </label>
                    <label class="radio-inline" for="ctl0_CONTENU_PAGE_PanelInscrit_adminEntreprise">
                        <com:TRadioButton enabled="<%=($this->isProfilActif())? true:false%>" id="adminEntreprise" GroupName="RadioGroup" Attributes.title="<%=Prado::localize('DEFINE_UES_SIMPLE')%>" />
                        <com:TTranslate>DEFINE_ATES_ENTREPRISE</com:TTranslate>
                    </label>
                </div>
            </div>
        </com:TPanel>
        <!--Fin Bloc Informations Profil-->
        <div class="section" id="rpgdEntreprise"
             style="display: <%=(Application\Service\Atexo\Atexo_Module::isEnabled('RecueilConsentementRgpd')) ? 'block' :'none'%>"
        >
            <div class="form-field">
                <div class="section-heading page-header m-t-3">
                    <h3 class="h4 m-0">
                        <com:TTranslate>ENTREPRISE_VERIFICATION_RGPD</com:TTranslate>
                    </h3>
                </div>
                <div class="content" id="contentRgpd">
                    <p class="margin-bottom-15 page-header">
                        <com:TTranslate>RGPD_TEXTE</com:TTranslate>
                    </p>
                    <div class="rgpd-bloc">
                        <div class="section-heading">

                            <div class="row">
                                <div class="col-lg-8">
                                    <com:TTranslate>RGPD_COMMUNICATION_PLACE</com:TTranslate>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <com:TCheckBox  id="communicationPlaceRgpd"
                                                    Attributes.data-toggle="toggle"
                                                    Attributes.data-on=" "
                                                    Attributes.data-off=" "
                                                    Attributes.data-onstyle="success"
                                                    Attributes.data-offstyle="danger"
                                                    Attributes.data-size="mini"
                                                    Attributes.data-style="ios">
                                    </com:TCheckBox>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="rgpd-bloc">
                        <div class="section-heading">
                            <div class="row">
                                <div class="col-lg-8">
                                    <com:TTranslate>RGPD_ENQUETE</com:TTranslate>
                                </div>
                                <div class="col-lg-4 text-right">
                                    <com:TCheckBox  id="enqueteRgpd"
                                                    Attributes.data-toggle="toggle"
                                                    Attributes.data-on=" "
                                                    Attributes.data-off=" "
                                                    Attributes.data-onstyle="success"
                                                    Attributes.data-offstyle="danger"
                                                    Attributes.data-size="mini"
                                                    Attributes.data-style="ios">
                                    </com:TCheckBox>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rgpd-bloc">
                        <div class="section-heading">
                            <div class="row">
                                <div class="col-lg-11">
                                    <com:TTranslate>RGPD_COMMUNICATION</com:TTranslate>
                                </div>

                                <div class="col-lg-1 text-right">
                                    <com:TCheckBox  id="communicationRgpd"
                                                    Attributes.data-toggle="toggle"
                                                    Attributes.data-on=" "
                                                    Attributes.data-off=" "
                                                    Attributes.data-onstyle="success"
                                                    Attributes.data-offstyle="danger"
                                                    Attributes.data-size="mini"
                                                    Attributes.data-style="ios">
                                    </com:TCheckBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <com:TLabel ID="JSCert"/>
    <com:TLabel id="script" />
</com:TPanel>