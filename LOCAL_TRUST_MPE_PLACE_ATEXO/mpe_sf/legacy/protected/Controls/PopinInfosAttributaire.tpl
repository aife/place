<!-- Content for Popover Detail -->
<div id="<%=$this->getIdDiv(). $this->getRefreshComposant()%>" class="hidden">
    <div class="popover-heading">
        <div class="nom-entreprise">
            <com:TActiveLabel id="nomEntreprise"/>
        </div>
    </div>
    <div class="popover-body" data-placement="bottom">
            <%=$this->getSiretAlertDivForPopInfos()%>
                <com:TTranslate>TEXT_SIRET</com:TTranslate></strong> : <com:TActiveLabel id="siretEntreprise"/>
                <%=$this->getSiretAlertMsg()%>
            </div>
        <div class="col-200"><strong><com:TTranslate>CONTACT</com:TTranslate></strong> :  <com:TActiveLabel id="nomContact"/><br />
            <img alt="<%=Prado::localize('DEFINE_ADRESSE_ELECTRONIQUE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" /><span class="ltr"><span><com:TActiveLabel id="adresseMail"/></span></span><br />
            <img alt="<%=Prado::localize('DEFINE_TEL')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" /> <span class="ltr"><span><com:TActiveLabel id="tel"/></span></span><br />
        </div>
        <div>
            <strong><com:TTranslate>TEXT_ADRESSE</com:TTranslate></strong> : <br />
            <com:TActiveLabel id="adresse"/> <br />
            <com:TActiveLabel id="codePostal"/>
            <com:TActiveLabel id="ville"/> <br />
            <com:TActiveLabel id="pays"/>
        </div>
        <div class="boutons" style="display: <%=($this->afficherLienFicheFournisseur())? 'block':'none'%>">
            <com:TActiveHyperLink
                    id="lienFicheFournisseur"
                    cssClass="bouton float-right testP">
                <span class="left-popin">&nbsp;</span>
                <com:TActiveImage Attributes.alt=""
                                  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-rechercher.png"/>Voir la fiche fournisseur
            </com:TActiveHyperLink>
        </div>
    </div>
</div>

<!-- NavigateUrl="<%= $this->lienNavigateUrlFicheFournisseur %>" -->
<!-- End Content for Popover Detail  comments -->
<script>
    var element = document.getElementById('<%=$this->lienFicheFournisseur->getClientId()%>');
    element.href="<%= $this->lienNavigateUrlFicheFournisseur %>";
    element.target="_blank";
</script>