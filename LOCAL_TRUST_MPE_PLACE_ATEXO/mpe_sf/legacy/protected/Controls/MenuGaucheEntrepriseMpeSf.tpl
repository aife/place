<!--Debut colonne gauche-->
<div class="left-part" id="left-part">
    <!--Debut menu -->
    <div id="menu">
        <ul id="menuList">
            <li style="display:<%=($this->isConnected()) ? 'none':'block'%>" class="menu-open"><span><com:TLinkButton OnClick="goToSocleIdentifier"><com:TTranslate>DEFINE_SE_CONNECTER_INSCRIRE</com:TTranslate></com:TLinkButton></span></li>
            <li style="display:<%=($this->isConnected() && Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && !$this->isAgent() && !$this->isAgentSocle()  && !$this->isAdmin()) ?
				'block':'none'%>;" class="<%=(in_array
				($_GET['page'],
				$this->ArrayPages['PANIER_ENTREPRISE']) && isset($_GET['panierEntreprise']))?'menu-open menu-on':'menu-open'%>"><span class="panier"><a accesskey="p" title="<%=Prado::localize('MENU_ACTIF')%>" onclick="toggleMenu('menuPanier');" href="javascript:void(0);"><com:TTranslate>DEFINE_MON_PANIER</com:TTranslate></a></span>
                <ul style="display: block;" id="menuPanier" class="ss-menu-open">
                    <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'none':'block'%>;" class="on"><a title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%>" href="javascript:void(0);" onclick="toggleSousMenu(this,'ssMenuPanier');"><com:TTranslate>DEFINE_CONSULTATION_EN_COURS</com:TTranslate></a>
                        <ul class="ss-menu-open" id="ssMenuPanier" style="display:block;">
                            <li class="off"><a title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('TEXT_TOUTES_CONSULTATION')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise"><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></a></li>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;retraits" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_AVEC_RETRAIT')%>" ><com:TTranslate>DEFINE_AVEC</com:TTranslate></a>/<a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansRetraits" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_SANS_RETRAIT')%>"><com:TTranslate>DEFINE_SANS_RETRAIT</com:TTranslate></a></li>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;questions" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_AVEC_QUESTION_POSEE')%>"><com:TTranslate>DEFINE_AVEC</com:TTranslate></a>/<a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansQuestions" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_SANS_QUESTION_POSEE')%>"><com:TTranslate>DEFINE_SANS_QUESTION_POSEE</com:TTranslate></a></li>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;depots"  title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_AVEC_DEPOT')%>"><com:TTranslate>DEFINE_AVEC</com:TTranslate></a>/<a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansDepots" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_SANS_DEPOT')%>"><com:TTranslate>DEFINE_SANS_DEPOT</com:TTranslate></a></li>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;echanges" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_AVEC_MESSAGE_ECHANGE')%>"><com:TTranslate>DEFINE_AVEC</com:TTranslate></a>/<a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansEchanges" title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_SANS_MESSAGE_ECHANGE')%>" ><com:TTranslate>DEFINE_SANS_MESSAGE_ECHANGE</com:TTranslate></a></li>
                        </ul>
                    </li>
                    <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'block':'none'%>;" class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise"><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></a></li>
                    <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'none':'block'%>;" class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;panierEntreprise&amp;dateFinEnd"><com:TTranslate>DEFINE_CONSULTATIONS_CLOTUREES</com:TTranslate></a></li>
                    <li class="off">
                        <a title="<%=Prado::localize('DEFINE_MON_PANIER')%> - <%=Prado::localize('TEXT_RECHERCHE_AVANCEE')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons&amp;panierEntreprise"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="<%=(in_array($_GET['page'],$this->ArrayPages['ANNONCES']) && !isset($_GET['orgTest']))?'menu-open menu-on':'menu-open'%>"><span><a title="<%=Prado::localize('MENU_ACTIF')%>" href="javascript:void(0);" onclick="toggleMenu('menuAnnonces');" accesskey="a"><com:TTranslate>DEFINE_ANNONCES</com:TTranslate></a></span>
                <ul class="ss-menu-open" id="menuAnnonces" style="display:block">
                    <li class="ss-menu-rub"><com:TTranslate>DEFINE_CONSULTATION_EN_COURS</com:TTranslate></li>
                    <li class="off"><label for="ctl0_menuGaucheEntreprise_quickSearch" style="display:none;"><com:TTranslate>TEXT_RECHERCHE_RAPIDE</com:TTranslate></label>
                        <form name="consultationSearchForm" id="consultationSearchForm" action="/app.php/consultation/recherche" method="get">
                            <com:TPanel DefaultButton="imageOk">
                                <com:TTextBox ID="quickSearch"
                                              CssClass="rechercher"
                                              Text="<%=Prado::localize('TEXT_RECHERCHE_RAPIDE')%>"
                                              Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_RAPIDE')%>"
                                              Attributes.onfocus="clearOnFocus(event,this);showDiv('infosRechercheRapide');"
                                              Attributes.onmouseover="clearOnFocus(event,this);"
                                              Attributes.onblur="hideDiv('infosRechercheRapide');" />
                                <com:PictoOk  ID="imageOk"
                                              OnClick="goToResultSearch"
                                              Attributes.title="<%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                                />
                            </com:TPanel>
                        </form>
                        <div class="info-bulle recherche-rapide" id="infosRechercheRapide" style="display:none;"><div><%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%></div></div>
                    </li>
                    <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('MenuEntrepriseConsultationsEnCours') && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;" class="off"><a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AllCons&amp;EnCours" ><com:TTranslate>RECHERCHE_CONSULTATIONS_EN_COURS</com:TTranslate></a></li>
                    <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('MenuEntrepriseToutesLesConsultations')) ? 'block':'none'%>;" class="off"><a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('TEXT_TOUTES_CONSULTATION')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AllCons"><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></a></li>
                    <li class="off"><a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('DEFINE_CONSULTATION_EN_COURS')%> - <%=Prado::localize('TEXT_RECHERCHE_AVANCEE')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></a></li>
                    <com:TRepeater ID="repeaterRechercheFavoris" >
                        <prop:HeaderTemplate>
                            <li style="display:block" class="on">
                                <a onclick="toggleSousMenu(this,'ssMesRecherches');" href="javascript:void(0);"><com:TTranslate>TEXT_MES_ALERTES_RECHERCHES_SAUVEGARDEES</com:TTranslate></a>
                                <ul style="display: block;" id="ssMesRecherches" class="ss-menu-open">
                        </prop:HeaderTemplate>
                        <prop:ItemTemplate>
                            <li class="off">
                                <a class=" lien-recherche" href="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&idRecherche=<%#$this->Data->getId()%>" title="<%#$this->Data->getDenomination()%>"><%#$this->Data->getDenomination()%></a>
                            </li>
                        </prop:ItemTemplate>
                        <prop:FooterTemplate>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseGestionRecherches"><com:TTranslate>TEXT_GERER_MES_ALERTES_RECHERCHES_SAUVEGARDEES</com:TTranslate></a> </li>
                </ul>
            </li>
            </prop:FooterTemplate>
            </com:TRepeater>
            <li class="ss-menu-rub"><com:TTranslate>TEXT_AUTRES_ANNONCES</com:TTranslate></li>
            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceInformation')))? 'block':'none'%>"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AvisInformation"><com:TTranslate>DEFINE_TOUTES_ANNONCES_INFORMATION</com:TTranslate></a></li>
            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceExtraitPv')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AvisExtraitPV"><com:TTranslate>TEXT_TOUS_LES_EXTRAITS_PV</com:TTranslate></a></li>
            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AvisAttribution"><com:TTranslate>DEFINE_TOUTES_ANNONCES_ATTRIBUTION</com:TTranslate></a></li>
            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AvisRapportAchevement"><com:TTranslate>TEXT_TOUS_LES_RAPPORTS_ACHEVEMENT</com:TTranslate></a></li>
            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceDecisionResiliation')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AvisdecisionResiliation"><com:TTranslate>TEXT_TOUTES_LES_DECISION_REALISATION</com:TTranslate></a></li>
            <li class="off"><a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('TEXT_AUTRES_ANNONCES')%> - <%=Prado::localize('TEXT_RECHERCHE_AVANCEE')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AllAnn"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></a></li>

            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceProgrammePrevisionnel')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>">
                <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.ListePPs">
                    <com:TTranslate>ANNONCE_PROGRAMME_PREVISIONNEL</com:TTranslate></a>
            </li>
            <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AutreAnnonceSyntheseRapportAudit')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>">
                <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.ListeSRA">
                    <com:TTranslate>ANNONCE_SYNTHESE_RAPPORT_AUDIT</com:TTranslate></a>
            </li>

            <li style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('EncheresEntreprise')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;">
                <com:TPanel id="enchereEntreprise" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('EncheresEntreprise')%>" >
                    <ul>
                        <li class="ss-menu-rub"><com:TTranslate>DEFINE_TEXT_ENCHERES</com:TTranslate></li>
                        <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseParticipationEnchere"><com:TTranslate>DEFINE_PARTICIPATION</com:TTranslate></a></li>
                    </ul>
                </com:TPanel>
            </li>
            <li class="ss-menu-rub" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('Article133UploadFichier') || Application\Service\Atexo\Atexo_Module::isEnabled('Article133GenerationPf')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;" title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('TEXT_LISTE_MARCHES')%>"><com:TTranslate>TEXT_LISTE_MARCHES</com:TTranslate></li>
            <li class="off" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Article133GenerationPf') && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;" >
                <com:TPanel id="panelArticle133RechercheAvancee">
                    <a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('TEXT_LISTE_MARCHES')%> - <%=Prado::localize('TEXT_RECHERCHE_AVANCEE')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseRechercherListeMarches&search">
                        <com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
                    </a>
                </com:TPanel>
            </li>
            <li class="off" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Article133UploadFichier') && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;" >
                <com:TPanel id="panelArticle133Telecharger">
                    <a title="<%=Prado::localize('DEFINE_ANNONCES')%> - <%=Prado::localize('TEXT_LISTE_MARCHES')%> - <%=Prado::localize('DEFINE_TELECHARGER')%>" href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseRechercherListeMarches&download">
                        <com:TTranslate>DEFINE_TELECHARGER</com:TTranslate>
                    </a>
                </com:TPanel>
            </li>
            <!-- debut menu consultations annulees -->
            <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnulerConsultation')) ? 'block':'none'%>;" >
                <com:TPanel visible="true">
                    <ul>
                        <li class="ss-menu-rub"><com:TTranslate>DEFINE_TEXT_CONSULTATIONS_ANNULEES</com:TTranslate></li>
                        <li class="off">
                            <com:TPanel ID="listerConsAnnulees" CssClass="config" visible="true">
                                <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AllCons&amp;consAnnulee=1"><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></a>
                            </com:TPanel>
                        </li>
                        <li class="off">
                            <com:TPanel ID="serchConsAnnulees" CssClass="config" visible="true">
                                <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons&amp;consAnnulee=1"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></a>
                            </com:TPanel>
                        </li>
                    </ul>
                </com:TPanel>
            </li>
            <!-- Fin menu consultations annulees -->
        </ul>
        </li>



        <!-- Start Menu BOURSE_A_CO_TRAITANCE -->
        <li class="<%=(in_array($_GET['page'],$this->ArrayPages['BOURSE']))?'menu-open menu-on':'menu-open'%>"  style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance') && $this->isConnected()) ? 'block':'none'%>;"><span><a title="<%=Prado::localize('MENU_ACTIF')%>" accesskey="m" onclick="toggleMenu('menuBourses');" href="javascript:void(0);" ><com:TTranslate>TEXT_BOURSE_A_LA_CO_SOUS_TRAITANCE</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="menuBourses" style="display: block;">
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.RechercheCollaborateur"><com:TTranslate>TEXT_RECHERCHE_COLLABORATION</com:TTranslate></a></li>
            </ul>
        </li>
        <!-- End Menu BOURSE_A_CO_TRAITANCE -->
        <!-- Start Menu ENTITES_ACHAT -->
        <li class="<%=(in_array($_GET['page'],$this->ArrayPages['ENTITES_ACHAT']))?'menu-open menu-on':'menu-open'%>" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AnnuaireEntitesAchatVisibleParEntreprise')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;"><span><a title="<%=Prado::localize('MENU_ACTIF')%>" href="javascript:void(0);" onclick="toggleMenu('entitesAchat');" accesskey="t"><com:TTranslate>TEXT_ENTITES_ACHAT</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="entitesAchat" style="display:none;">
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseVisualiserEntitesAchatsRecherche"><com:TTranslate>TEXT_CONSULTER_ENTITES_ACHAT</com:TTranslate></a></li>
            </ul>
        </li>
        <!-- End Menu ENTITES_ACHAT -->
        <!-- Start Menu OUTILS_DE_SIGNATURE -->
        <li class="<%=(in_array($_GET['page'],$this->ArrayPages['OUTILS_DE_SIGNATURE']))?'menu-open menu-on':'menu-open'%>" style="display:<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'block':'none'%>;"><span><a title="<%=Prado::localize('MENU_ACTIF')%>" href="javascript:void(0);" onclick="toggleMenu('menuOutilsDeSignature');" accesskey="t"><com:TTranslate>DEFINE_TEXT_OUTILS_DE_SIGNATURE</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="menuOutilsDeSignature" style="display:block">
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>app.php/entreprise/signer-document&amp;callFrom=entreprise"><com:TTranslate>SIGNER_UN_DOCUMENT</com:TTranslate></a></li>
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>entreprise/verification-signature&amp;callFrom=entreprise"><com:TTranslate>VERIFIER_SIGNATURE</com:TTranslate></a></li>
            </ul>
        </li>
        <!-- End Menu OUTILS_DE_SIGNATURE -->
        <li  class="<%=(in_array($_GET['page'],$this->ArrayPages['ASSISTANCE']) && isset($_GET['Aide']) )?'menu-open menu-on':'menu-open'%>"><span><a title="<%=Prado::localize('MENU_ACTIF')%>" href="javascript:void(0);" onclick="toggleMenu('menuAssistance');" accesskey="m"><com:TTranslate>DEFINE_TEXT_ASSISTANCE</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="menuAssistance" style="display:none;">
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntreprisePremiereVisite"><com:TTranslate>TEXT_PREMIERE_VISITE</com:TTranslate></a></li>
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseGuide&Aide" title="<%=Prado::localize('DEFINE_TEXT_ASSISTANCE')%> - <%=Prado::localize('DEFINE_TEXT_ASSISTANCE_GUIDE')%>" ><com:TTranslate>DEFINE_TEXT_ASSISTANCE_GUIDE</com:TTranslate></a></li>
                <li class="off"><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LT_MPE_SF')%>/entreprise/aide/assistance-telephonique"><com:TTranslate>DEFINE_TEXT_ASSISTANCE_TELEPHONIQUE</com:TTranslate></a></li>
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.DocumentsDeReference&callFrom=entreprise" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DocumentsReference')) ? 'block':'none'%>;"><com:TTranslate>DEFINE_DOCUMENTS_REFERENCE</com:TTranslate></a></li>
                <li style="display:<%=((Prado::localize('TEXT_AUTOFORMATION') != 'TEXT_AUTOFORMATION') && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;">
                    <com:TPanel ID="autoFormation" visible="<%= (Prado::localize('TEXT_AUTOFORMATION') != 'TEXT_AUTOFORMATION') ? true: false%>">
                        <ul>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAutoformation"><com:TTranslate>TEXT_AUTOFORMATION</com:TTranslate></a></li>
                        </ul>
                    </com:TPanel>
                </li>
                <li style="display:<%=( !( Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH')) && (Prado::localize('TEXT_FAQ') != 'TEXT_FAQ') && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;">
                    <com:TPanel ID="faq" visible="<%= (!( Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH')) && (Prado::localize('TEXT_FAQ') != 'TEXT_FAQ')) ? true: false%>">
                        <ul>
                            <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseFaq"><com:TTranslate>TEXT_FAQ</com:TTranslate></a></li>
                        </ul>
                    </com:TPanel>
                </li>
                <li class="off" style="display:<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'block':'none'%>;"><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LT_MPE_SF')%>/entreprise/aide/outils-informatiques"><com:TTranslate>TEXT_OUTILS_INFORMATIQUES</com:TTranslate></a></li>
            </ul>
        </li>
        <li class="<%=(in_array($_GET['page'],$this->ArrayPages['SOCIETES_EXCLUS'])) ? 'menu-open menu-on':'menu-open'%>" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('MenuAgentSocietesExclues')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'block':'none'%>">
            <span><a href="javascript:void(0);" onclick="toggleMenu('societesExclues');" ><com:TTranslate>TEXT_SOCIETES_EXCLUES</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="societesExclues" style="display: none;">
                <li class="off">
                    <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseRechercherSocietesExclues&amp;search=1" title="<%=Prado::localize('TEXT_SOCIETES_EXCLUES')%> - <%=Prado::localize('TEXT_RECHERCHE_AVANCEE')%>" >
                        <com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
                    </a>
                </li>
            </ul>
        </li>
        <li class="<%=(in_array($_GET['page'],$this->ArrayPages['SE_PREPARER'])&& !isset($_GET['orgTest']))?'menu-open menu-on':'menu-open'%>" style="display:<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')) ? 'block':'none'%>;"><span><a href="javascript:void(0);" title="<%=Prado::localize('MENU_ACTIF')%>" onclick="toggleMenu('menuPreparation');" accesskey="m"><com:TTranslate>SE_PREPARER_A_REPONDRE</com:TTranslate></a></span>
            <ul style="display:none;" id="menuPreparation" class="ss-menu-open">
                <li class="off"><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LT_MPE_SF')%>/entreprise/footer/diagnostic-poste"><com:TTranslate>TESTER_CONFIGURATION_MON_POSTE</com:TTranslate></a></li>
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseAdvancedSearch&amp;AllCons&amp;orgTest"><com:TTranslate>TEXT_CONSULTATION_TEST</com:TTranslate></a></li>
            </ul>
        </li>
        <li class="menu-open" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('MenuEntrepriseIndicateursCles')) && (!Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches'))) ? 'block':'none'%>;"><span><a href="javascript:void(0);" onclick="toggleMenu('indicateursCles');" accesskey="m"><com:TTranslate>TEXT_INDICATEURS_CLES</com:TTranslate></a></span>
            <ul id="indicateursCles" class="ss-menu-open">
                <li class="off"><a><%=$this->lireXmlIndicateursPortail('nbrConsultationLigne')%><com:TTranslate>TEXT_CONSULTATIONS_EN_LIGNE</com:TTranslate></a></li>
                <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'none':'block'%>"><a><%=$this->lireXmlIndicateursPortail('nbrEntitesPubliques')%><com:TTranslate>TEXT_ENTITES_PUBLIQUES_INSCRITES</com:TTranslate></a></li>
                <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'none':'block'%>"><a><%=$this->lireXmlIndicateursPortail('nbrEntrepriseInscrites')%><com:TTranslate>TEXT_ENTREPRISES_INSCRITES</com:TTranslate></a></li>
                <li class="ss-menu-rub"><com:TTranslate>TEXT_DEPUIS_LE</com:TTranslate> <%=$this->lireXmlIndicateursPortail('DateDebut')%></li>
                <li class="off"><a><%=$this->lireXmlIndicateursPortail('nbrConsultationPublies')%><com:TTranslate>CONSULTATIONS_PUBLIEES</com:TTranslate></a></li>
                <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'none':'block'%>"><a><%=$this->lireXmlIndicateursPortail('nbrDCETelecharges')%><com:TTranslate>DCE_TELECHARGES</com:TTranslate></a></li>
                <li class="off" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches')))? 'none':'block'%>"><a><%=$this->lireXmlIndicateursPortail('nbrReponsesElectroniq')%><com:TTranslate>REPONSES_ELECTRONIQUES_RECUES</com:TTranslate></a></li>
            </ul>
        </li>
        <!-- Start Menu GUIDES -->
        <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PublierGuides') && $this->isConnected())?'block':'none'%>" class="<%=(in_array($_GET['page'],$this->ArrayPages['GUIDES']) && isset($_GET['Guides']))?'menu-open menu-on':'menu-open'%>"><span><a href="javascript:void(0);" title="<%=Prado::localize('MENU_ACTIF')%>" onclick="toggleMenu('menuGuide');" accesskey="d" ><com:TTranslate>TEXT_GUIDES</com:TTranslate></a></span>
            <ul class="ss-menu-open" id="menuGuide" style="display:block">
                <li class="off"><a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.EntrepriseGuide&Guides" title="<%=Prado::localize('TEXT_GUIDES')%> - <%=Prado::localize('DEFINE_TEXT_ASSISTANCE_GUIDE')%>"><com:TTranslate>DEFINE_TEXT_ASSISTANCE_GUIDE</com:TTranslate></a></li>
                <li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PublierGuides') && $this->isConnected())? 'block':'none'%>">
                    <com:TPanel ID="GuidesMultimedia" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('PublierGuides') ? true: false%>">
                        <ul>
                            <li class="off">
                                <a href="<%=$this->TemplateControl->urlPf()%>?page=Entreprise.Guides&calledFrom=entreprise"><com:TTranslate>TEXT_GUIDES_UTILISATEUR8MULTIMEDIA</com:TTranslate></a>
                            </li>
                        </ul>
                    </com:TPanel>
                </li>
            </ul>
        </li>
        <!-- Start Menu GUIDES -->
        </ul>
    </div>
    <div class="menu-bottom"></div>
    <!--Fin menu -->
    <script type="text/javascript">
        if (document.getElementById('menuAnnonces') != null ) document.getElementById('menuAnnonces').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['ANNONCES'])&& !isset($_GET['orgTest']))?'block':'none'%>';
        if (document.getElementById('menuAssistance') != null ) document.getElementById('menuAssistance').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['ASSISTANCE']))?'block':'none'%>';
        if (document.getElementById('societesExclues') != null ) document.getElementById('societesExclues').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['SOCIETES_EXCLUS']))?'block':'none'%>';
        if (document.getElementById('menuPreparation') != null ) document.getElementById('menuPreparation').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['SE_PREPARER'])&& isset($_GET['orgTest']))?'block':'none'%>';
        if (document.getElementById('menuOutilsDeSignature') != null ) document.getElementById('menuOutilsDeSignature').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['OUTILS_DE_SIGNATURE']))?'block':'none'%>';
        if (document.getElementById('menuPanier') != null ) document.getElementById('menuPanier').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['PANIER_ENTREPRISE']))?'block':'none'%>';
        if (document.getElementById('menuBourses') != null ) document.getElementById('menuBourses').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['BOURSE']))?'block':'none'%>';
        if (document.getElementById('entitesAchat') != null ) document.getElementById('entitesAchat').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['ENTITES_ACHAT']))?'block':'none'%>';
    </script>
</div>
<!--Fin colonne gauche-->
