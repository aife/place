<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Prado\Prado;

/**
 * Template de choix d'une entié d'achat.
 */
class ChoixOrganisme extends MpeTPage
{
    private string $_showServices = 'true';
    private $_defaultOrganisme = null;
    private string $_actifsOrgsOnly = 'true';
    private string $_remplirDataSources = 'true';
    private string $_callPagePanelRefresh = 'true';
    private string $_showPictoOk = 'true';

    public function getShowService()
    {
        return $this->_showServices;
    }

    public function setShowService($value)
    {
        $this->_showServices = $value;
    }

    public function getDefaultOrganisme()
    {
        return $this->_defaultOrganisme;
    }

    public function setDefaultOrganisme($value)
    {
        $this->_defaultOrganisme = $value;
    }

    public function getActifsOrgsOnly()
    {
        return $this->_actifsOrgsOnly;
    }

    public function setActifsOrgsOnly($value)
    {
        $this->_actifsOrgsOnly = $value;
    }

    public function getRemplirDataSources()
    {
        return $this->_remplirDataSources;
    }

    public function setRemplirDataSources($value)
    {
        $this->_remplirDataSources = $value;
    }

    public function getCallPagePanelRefresh()
    {
        return $this->_callPagePanelRefresh;
    }

    public function setCallPagePanelRefresh($value)
    {
        $this->_callPagePanelRefresh = $value;
    }

    public function getShowPictoOk()
    {
        return $this->_showPictoOk;
    }

    public function setShowPictoOk($value)
    {
        $this->_showPictoOk = $value;
    }

    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack && 'true' == $this->getRemplirDataSources()) {
            $this->displayOrganismes();
        }
    }

    /**
     * affichage de toutes les ministères.
     */
    public function displayOrganismes()
    {
        if ('true' == $this->getActifsOrgsOnly()) {
            $orgsActif = true;
        } else {
            $orgsActif = false;
        }
        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, null, $orgsActif);
        $arrayOrga = [];
        foreach ($ArrayOrganismes as $key => $value) {
            //$services=Atexo_EntityPurchase::getCachedEntities($key);
            $arrayOrga[$key] = $value.' ( '.Prado::localize('DEFINE_SERVICES').' : '.(new Atexo_EntityPurchase())->getCountService($key).'   /   '.Prado::localize('AGENTS_ACTIFS').' : '.(new Atexo_Agent())->getCountAgent($key, '1').'   /   '.Prado::localize('AGENTS_DESACTIVES').' : '.(new Atexo_Agent())->getCountAgent($key, '0').' )'; //count(Atexo_Agent::getAllAgent($key)).' )';
        }
        $ArrayOrganismes = $arrayOrga;
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ( '.Prado::localize('TEXT_ENTITES').' : '.count($ArrayOrganismes).'   /   '.Prado::localize('DEFINE_SERVICES').' : '.(new Atexo_EntityPurchase())->getCountAllServicesPortail().'   /   '.Prado::localize('AGENTS_ACTIFS').' : '.(new Atexo_Agent())->getCountAgent(null, '1').'   /   '.Prado::localize('AGENTS_DESACTIVES').' : '.(new Atexo_Agent())->getCountAgent(null, '0').' )'.' ---');
        $this->organisme->dataSource = $ArrayOrganismes;
        $this->organisme->DataBind();

        if (null !== $this->getDefaultOrganisme()) {
            $this->organisme->SelectedValue = $this->getDefaultOrganisme();
        }
    }

    /*
    *  affichage de toutes les entités d'achats
    *
    */
    public function displayEntityPurchase($sender, $param)
    {
        if ('true' == $this->getShowService()) {
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->organisme->SelectedValue, true);
            $this->listeEntites->dataSource = $listEntityPurchase;
            $this->listeEntites->DataBind();
            $this->panelChoixMinistere->render($param->getNewWriter());
            if ('true' == $this->getCallPagePanelRefresh()) {
                $this->Page->panelRefresh->render($param->getNewWriter());
            }
        } else {
            $this->Page->onCallBackOrganismeSelected($sender, $param, $this->organisme->SelectedValue);
        }
    }

    /**
     * Enter description here...
     *
     * @return unknown
     */
    public function getSelectedEntityPurchase()
    {
        return $this->listeEntites->SelectedValue;
    }
}
