<?php

namespace Application\Controls;

use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author loubna EZZIAI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class ResultSearchAgent extends MpeTPage
{
    public $critereTri;
    public $triAscDesc;
    public $organisme;
    public $nbrElementRepeater;
    public $criteriaVoAgent;
    //donner le code libelle d'un referentiel pour recuperer que les agent ayant ce referentiel
    private bool $withReferentiel = false;

    public function setWithReferentiel($withReferentiel)
    {
        $this->withReferentiel = $withReferentiel;
    }

    public function getWithReferentiel()
    {
        return $this->withReferentiel;
    }

    public function setCriteriaVoAgent($value)
    {
        $this->criteriaVoAgent = $value;
    }

    public function getCriteriaVoAgent()
    {
        return $this->Page->getViewState('CriteriaVoAgent');
    }

    public function onLoad($param)
    {
        if ('' != Atexo_Config::getParameter('CODE_LIBELLE_REF_AGENT_FOR_SEARCH')) {
            self::setWithReferentiel(Atexo_Config::getParameter('CODE_LIBELLE_REF_AGENT_FOR_SEARCH'));
        }
        if (!$this->isPostBack) {
            $this->hideComponent();
        } else {
            self::repeaterDataBind();
        }
    }

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function repeaterDataBind()
    {
        $criteriaVoAgent = $this->getCriteriaVoAgent();
        if ($criteriaVoAgent instanceof Atexo_Agent_CriteriaVo) {
            $criteriaVoAgent->setLimit(0);
            $criteriaVoAgent->setOffset(0);
            $nombreElement = (new Atexo_Agent())->search($criteriaVoAgent, true, $this->withReferentiel);
            $this->nbrElementRepeater = $nombreElement;
            $this->setViewState('nbrElements', $nombreElement);
            if ($nombreElement) {
                $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
                $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
                $this->RepeaterAgent->setVirtualItemCount($nombreElement);
                $this->RepeaterAgent->setCurrentPageIndex(0);
                self::showComponent();
                self::populateData($criteriaVoAgent);
            } else {
                $this->setViewState('listeAgents', []);
                $this->RepeaterAgent->DataSource = [];
                $this->RepeaterAgent->DataBind();
                self::hideComponent();
            }
        } else {
            $this->RepeaterAgent->DataSource = [];
            $this->RepeaterAgent->DataBind();
            self::hideComponent();
        }
    }

    /*
     * Permet de remplir le repeaterAgent
     */
    public function populateData(Atexo_Agent_CriteriaVo $criteriaVoAgent)
    {
        $offset = $this->RepeaterAgent->CurrentPageIndex * $this->RepeaterAgent->PageSize;
        $limit = $this->RepeaterAgent->PageSize;
        if ($offset + $limit > $this->RepeaterAgent->getVirtualItemCount()) {
            $limit = $this->RepeaterAgent->getVirtualItemCount() - $offset;
        }
        $criteriaVoAgent->setLimit($limit);
        $criteriaVoAgent->setOffset($offset);

        $dataAgent = (new Atexo_Agent())->search($criteriaVoAgent, false, $this->withReferentiel);
        $this->setViewState('listeAgents', $dataAgent);
        if (count($dataAgent)) {
            $this->nbrElementRepeater = $this->getViewState('nbrElements');
            $this->RepeaterAgent->DataSource = $dataAgent;
            $this->RepeaterAgent->DataBind();
            self::showComponent();
        } else {
            self::hideComponent();
        }
    }

    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->PagerTop->setVisible(false);
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->pageNumberBottom->Text;
                break;
        }
        self::toPage($numPage);
    }

    public function toPage($numPage)
    {
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->RepeaterAgent->CurrentPageIndex = $numPage - 1;
            $this->pageNumberBottom->Text = $numPage;
            $this->pageNumberTop->Text = $numPage;
            $this->RepeaterAgent->PageSize = $this->nbResultsTop->getSelectedValue();
            $this->populateData($this->getCriteriaVoAgent());
        } else {
            $this->pageNumberTop->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->RepeaterAgent->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nbResultsBottom':
                 $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                 $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }

        $this->RepeaterAgent->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nbrElements');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterAgent->PageSize);
        if ($this->pageNumberTop->Text > $this->nombrePageTop->Text) {
            $this->pageNumberTop->Text = $this->nombrePageTop->Text;
            $this->pageNumberBottom->Text = $this->nombrePageBottom->Text;
        }
        $this->RepeaterAgent->setCurrentPageIndex($this->pageNumberTop->Text - 1);
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));

        $this->populateData($this->getCriteriaVoAgent()); //exit;
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterAgent->CurrentPageIndex = $param->NewPageIndex;
        $this->pageNumberBottom->Text = $param->NewPageIndex + 1;
        $this->pageNumberTop->Text = $param->NewPageIndex + 1;
        $this->RepeaterAgent->PageSize = $this->nbResultsTop->getSelectedValue();
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        $this->populateData($this->getCriteriaVoAgent());
    }

    /**
     * Trier un tableau des agents sélcetionnés.
     *
     * @param sender
     * @param param
     */
    public function sortAgents($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        if ('Nom' == $sender->getActiveControl()->CallbackParameter) {
            $sortElement = 'Nom';
            $this->setCritereAndTypeTri('Nom');
        } elseif ('Prenom' == $sender->getActiveControl()->CallbackParameter) {
            $sortElement = 'Prenom';
            $this->setCritereAndTypeTri('Prenom');
        } else {
            return;
        }
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));

        $this->RepeaterAgent->PageSize = $this->nbResultsTop->getSelectedValue();
        $this->nombrePageTop->Text = ceil($this->getNbrElementRepeater() / $this->RepeaterAgent->PageSize);
        $this->nombrePageBottom->Text = ceil($this->getNbrElementRepeater() / $this->RepeaterAgent->PageSize);

        $criteriaVo = $this->Page->getViewState('CriteriaVoAgent');
        $criteriaVo->setSortByElement($sortElement);
        $criteriaVo->setSensOrderBy($this->triAscDesc);
        $this->Page->setViewState('CriteriaVoAgent', $criteriaVo);
        //Remplissage du tabeau des agents
        self::populateData($criteriaVo);

        // Re-affichage du TActivePanel
        $this->panelRefresh->render($param->getNewWriter());
        self::showComponent();
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    public function genererExcel()
    {
        $criteriaAgent = $this->getCriteriaVoAgent();
        $criteriaAgent->setLimit(0);
        $criteriaAgent->setOffset(0);
        (new Atexo_GenerationExcel())->generateExcelAnnuaireAgents($criteriaAgent, false);
    }

    public function intialiserRepeater()
    {
        $this->RepeaterAgent->DataSource = [];
        $this->RepeaterAgent->DataBind();
        $this->nbResultsTop->setSelectedValue(10);
        $this->nbResultsBottom->setSelectedValue(10);
        $this->pageNumberTop->Text = 1;
        $this->pageNumberBottom->Text = 1;
        $this->RepeaterAgent->setCurrentPageIndex(0);
    }

    public function getLibelleReferentielsAgent()
    {
        $text = null;
        $refsAgent = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielForAgent();
        foreach ($refsAgent as $oneRef) {
            $text .= Prado::localize($oneRef->getCodeLibelle()).'/<br/>';
        }

        return trim($text, '/<br/>');
    }
}
