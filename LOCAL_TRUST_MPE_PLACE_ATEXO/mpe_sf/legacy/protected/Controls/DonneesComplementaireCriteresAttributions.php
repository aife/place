<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTCritereAttributionPeer;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTSousCritereAttribution;
use Application\Propel\Mpe\CommonTSousCritereAttributionPeer;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielPeer;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

class DonneesComplementaireCriteresAttributions extends MpeTTemplateControl
{
    const PONDERATION = 'Ponderation';
    const DECROISSANT = 'OrdreDecroissant';
    public const ID_CRITERE_PONDERATION = 2;
    public const LABEL_CRITERE_PONDERATION = 'layerDefinitionCriteres_2';

    private $_donneeComplementaire;
    private $_donneeComplementaireCons;
    private $_connexion;
    public $critereDifferent;
    public $_objet;
    private $mode;
    private ?string $idElement = null;
    private $composant; // valeur possible donneesComplementaires ou identification
    private $consultation;

    public function getConsultation()
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getConsultation();
        }

        return $this->consultation;
    }

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function getDonneeComplementaireCons()
    {
        return $this->_donneeComplementaireCons;
    }

    public function setDonneeComplementaireCons($donneeComplementaireCons)
    {
        $this->_donneeComplementaireCons = $donneeComplementaireCons;
    }

    public function getObjet()
    {
        if ($this->_objet) {
            return $this->_objet;
        } else {
            return $this->getViewState('objet');
        }
    }

    public function setObjet($value)
    {
        $this->setViewState('objet', $value);
        $this->_objet = $value;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * recupère la valeur.
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setIdElement($value)
    {
        $this->idElement = $value;
    }

    /**
     * @return mixed
     */
    public function getComposant()
    {
        return $this->composant;
    }

    /**
     * @param mixed $composant
     */
    public function setComposant($composant)
    {
        $this->composant = $composant;
    }

    public function getConnexion()
    {
        return $this->_connexion;
    }

    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    public function getTypePonderation()
    {
        return self::PONDERATION;
    }

    public function getTypeOrdreDecroissant()
    {
        return self::DECROISSANT;
    }

    public function onLoad($param)
    {
        $this->scriptLabel->Text = '';
        $this->consultation = $this->getConsultation();

        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac') && !$this->Page->IsPostBack) {
            self::chargerComposant();
        }
    }

    /**
     * Remplir liste Forme Groupement.
     *
     * @param bool $recharger
     *
     * @throws Atexo_Config_Exception
     */
    public function fillCriteresAttribution($recharger = false)
    {
        $dataSource = [];
        $dataCriteresAttribution = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), true);

        if ($recharger) {
            $dataSource['layerDefinitionVide1'] = (new Atexo_Config())->toPfEncoding(Prado::localize('TEXT_SELECTIONNER').'...');
        } else {
            $dataSource['layerDefinitionVide1'] = Prado::localize('TEXT_SELECTIONNER').'...';
        }

        if ($dataCriteresAttribution) {
            foreach ($dataCriteresAttribution as $oneVal) {
                if ($recharger) {
                    $dataSource[$oneVal->getLibelle2()] = (new Atexo_Config())->toPfEncoding($oneVal->getLibelleValeurReferentiel());
                } else {
                    $dataSource[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
                }
            }
        }

        $this->criteresAttribution->DataSource = $dataSource;
        $this->criteresAttribution->DataBind();
    }

    public function populateData($recharger = false)
    {
        $idCritereAttribution = null;
        // initialisation des composants
        self::fillCriteresAttribution($recharger);
        if ($this->getDonneeComplementaireCons() instanceof CommonTDonneeComplementaire) {
            $idCritereAttribution = $this->getDonneeComplementaireCons()->getIdCritereAttribution();
        } elseif ($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $idCritereAttribution = $this->_donneeComplementaire->getIdCritereAttribution();
        }
        if ($idCritereAttribution) {
            $valeurRef = CommonValeurReferentielPeer::retrieveByPK($idCritereAttribution, Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $this->_connexion);
            $this->criteresAttribution->setSelectedValue($valeurRef->getLibelle2());
            switch ($valeurRef->getLibelle2()) {
                case 'layerDefinitionCriteres_1':
                    self::populateDecroissant();
                    break;
                case 'layerDefinitionCriteres_2':
                    self::populatePonderation();
                    break;
            }
            if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) && $this->_donneeComplementaire->getCriteresIdentiques()) {
                $this->copy_attrib->checked = true;
            } else {
                $this->copy_attrib->checked = false;
            }
        }
    }

    public function populateDecroissant()
    {
        $criteres = self::populateCriteres(self::DECROISSANT);
        $this->repeaterCritereAttribOrdreDecroissant->DataSource = $criteres[self::DECROISSANT];
        $this->repeaterCritereAttribOrdreDecroissant->DataBind();

        $this->tableauAttributionCriteria->DataSource = $criteres[self::DECROISSANT];
        $this->tableauAttributionCriteria->DataBind();
    }

    public function populatePonderation()
    {
        $criteres = self::populateCriteres(self::PONDERATION);
        $this->repeaterCritereAttribPonderation->DataSource = $criteres[self::PONDERATION];
        $this->repeaterCritereAttribPonderation->DataBind();

        $this->tableauAttributionCriteria->DataSource = $criteres[self::PONDERATION];
        $this->tableauAttributionCriteria->DataBind();
    }

    public function populateCriteres($type)
    {
        $criteresAttribution = null;
        $i = 0;
        $array = $this->getViewState('criteres'.$type);
        if (!is_array($array)) {
            if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) && $this->_donneeComplementaire->getIdDonneeComplementaire()) {
                $criteresAttribution = (new CommonTCritereAttribution())->retrieveCritereByIdDonneeComplementaire($this->_donneeComplementaire->getIdDonneeComplementaire(), $this->_connexion);
            }
            if (is_array($criteresAttribution)) {
                foreach ($criteresAttribution as $oneCritere) {
                    $oneCritere->index = $i;
                    $array[$type][$i]['critereVal'] = $oneCritere;
                    if ($oneCritere instanceof CommonTCritereAttribution) {
                        $sousCriteres = $oneCritere->getSousCriteres($this->_connexion);
                        $totalPonderationSousCritere = 0;
                        foreach ($sousCriteres as $oneSousCritere) {
                            $oneSousCritere->indexPere = $i;
                            $totalPonderationSousCritere += $oneSousCritere->getPonderation();
                            $array[$type][$i]['sousCirteres'][] = $oneSousCritere;
                        }
                        $array[$type][$i]['critereVal']->setTotalPonderationSousC($totalPonderationSousCritere);
                    }
                    ++$i;
                }
            }
            $totalPonderation = self::getTotalPonderation($array[$type]);
            $this->totalPonderation->value = $totalPonderation;
            $this->setViewState('criteres'.$type, $array);
            $this->setViewState('criteres'.$type.'toSave', $array);
            $this->setViewState("count_$type", $i);
        }

        return $array;
    }

    public function getTotalPonderation($array)
    {
        $totalPonderation = 0;
        if (is_array($array) && count($array)) {
            foreach ($array as $critere) {
                $oneCritere = $critere['critereVal'];
                if ($oneCritere instanceof CommonTCritereAttribution) {
                    $totalPonderation += $oneCritere->getPonderation();
                }
            }
        }

        return $totalPonderation;
    }

    public function saveCritereAttribution(&$donneeComplementaire, $param = false)
    {
        $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->_donneeComplementaire = $donneeComplementaire;
        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $idCriteresAttribution = $this->criteresAttribution->getSelectedValue();
            $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $idCriteresAttribution);
            if ($valeurRef instanceof CommonValeurReferentiel) {
                $donneeComplementaire->setIdCritereAttribution($valeurRef->getId());
                if ($this->copy_attrib->checked) {
                    // supprimer les anciens critére des lots
                    if ($this->getObjet() instanceof CommonConsultation) {
                        $lots = $this->getObjet()->getAllLots();
                        foreach ($lots as $lot) {
                            $donneesComplLot = $lot->getDonneComplementaire();
                            if ($donneesComplLot instanceof CommonTDonneeComplementaire) {
                                (new Atexo_DonnesComplementaires())->deleteCritereAttribution($donneesComplLot, $this->_connexion);
                                $donneesComplLot->save($this->_connexion);
                            }
                        }
                    }
                    $donneeComplementaire->setCriteresIdentiques(1);
                } else {
                    if (self::getIsAlloti()) {
                        self::deleteOldCriteres($donneeComplementaire);
                    }
                    $donneeComplementaire->setCriteresIdentiques(0);
                }
            } else {
                $donneeComplementaire->setIdCritereAttribution(0);
                $donneeComplementaire->setCriteresIdentiques(0);
            }
            switch ($idCriteresAttribution) {
                case 'layerDefinitionVide1':
                    //selectionner : supprimer les criteres deja enregistrés (si exist)
                    $donneeComplementaire->setCriteresIdentiques(0);
                    self::deleteOldCriteres($donneeComplementaire);
                    break;
                case 'layerDefinitionCriteres_1':
                    //pondration : suprimmer les critere pondration deja enregistrer (si existe) et ajouter les nouveaux critere decroissant
                    self::saveCriteres(self::DECROISSANT, self::PONDERATION);
                    break;
                case 'layerDefinitionCriteres_2':
                 //pondration : suprimmer les critere pondration deja enregistrer (si existe) et ajouter les nouveaux critere pondration
                    self::saveCriteres(self::PONDERATION, self::DECROISSANT, $param);
                    break;
                case 'layerDefinitionVide':
                 //plus bas : supprimer les criteres deja enregistrés (si exist)
                    $donneeComplementaire->setCriteresIdentiques(0);
                    self::deleteOldCriteres($donneeComplementaire);
                    break;
                case 'layerDefinitionVide_2':
                 //plus bas : supprimer les criteres deja enregistrés (si exist)
                    $donneeComplementaire->setCriteresIdentiques(0);
                    self::deleteOldCriteres($donneeComplementaire);
                    break;
                case 'layerDefinitionVide_3':
                //plus bas : supprimer les criteres deja enregistrés (si exist)
                    $donneeComplementaire->setCriteresIdentiques(0);
                    self::deleteOldCriteres($donneeComplementaire);
                    break;
            }
        }
    }

    public function saveCriteres($type, $changed = false, $param = false)
    {
        // si on change de critere d'attribution on supprime les anciens criteres
        if ($changed) {
            $criteres = $this->getViewState('criteres'.$changed);
            $criteresASupprimer = $criteres[$changed];
            if (is_array($criteresASupprimer)) {
                foreach ($criteresASupprimer as $critere) {
                    $oneCritere = $critere['critereVal'];
                    if ($oneCritere instanceof CommonTCritereAttribution) {
                        $sousCritersToDelete = $oneCritere->getSousCriteres($this->_connexion);
                        foreach ($sousCritersToDelete as $sousCriterToDelete) {
                            $sousCriterToDelete->delete($this->_connexion);
                        }
                    }
                    CommonTCritereAttributionPeer::doDelete($oneCritere, $this->_connexion);
                }
            }
        }

        $criteres = $this->getViewState('criteres'.$type.'toSave');
        $this->setViewState('criteres'.$type, $criteres);
        // criteres Ajoutés
        $criteresAttributionInsert = $criteres[$type];
        if (is_array($criteresAttributionInsert)) {
            foreach ($criteresAttributionInsert as $critere) {
                $oneCritere = $critere['critereVal'];
                if (!$oneCritere->getIdCritereAttribution()) {
                    $oneCritere->setIdDonneeComplementaire($this->_donneeComplementaire->getIdDonneeComplementaire());
                    $oneCritere->save($this->_connexion);
                }
                $sousCriteres = $critere['sousCirteres'];
                if (is_array($sousCriteres)) {
                    foreach ($sousCriteres as $sousCritere) {
                        if (!$sousCritere->getIdSousCritereAttribution()) {
                            $sousCritere->setIdCritereAttribution($oneCritere->getIdCritereAttribution());
                            $sousCritere->save($this->_connexion);
                        }
                    }
                }
            }
        } else {
            $criteresAttributionInsert = [];
        }

        if (self::PONDERATION == $type && $param) {
            $this->repeaterCritereAttribPonderation->DataSource = $criteresAttributionInsert;
            $this->repeaterCritereAttribPonderation->DataBind();
            $this->panelCriteresPonderation->render($param->NewWriter);
        } elseif (self::DECROISSANT == $type && $param) {
            $this->repeaterCritereAttribOrdreDecroissant->DataSource = $criteresAttributionInsert;
            $this->repeaterCritereAttribOrdreDecroissant->DataBind();
            $this->panelCriteresOrdreDecroissant->render($param->NewWriter);
        }

        //sous criteres supprimées
        $sousCriteres = $this->getViewState('sousCriteresSupprime'.$type);
        if (is_array($sousCriteres)) {
            foreach ($sousCriteres as $oneSousCritere) {
                if ($oneSousCritere->getIdSousCritereAttribution()) {
                    CommonTSousCritereAttributionPeer::doDelete($oneSousCritere, $this->_connexion);
                }
            }
        }

        // criteres Supprimés
        $criteres = $this->getViewState('criteresSupprime');
        $criteresAttribution = $criteres[$type];
        if (is_array($criteresAttribution)) {
            foreach ($criteresAttribution as $critere) {
                $oneCritere = $critere['critereVal'];
                if (self::deleteThisCritere($oneCritere, $criteresAttributionInsert)) {
                    //supprimer les sous critéres
                    if ($oneCritere instanceof CommonTCritereAttribution) {
                        $sousCritersToDelete = $oneCritere->getSousCriteres($this->_connexion);
                        foreach ($sousCritersToDelete as $sousCriterToDelete) {
                            $sousCriterToDelete->delete($this->_connexion);
                        }
                    }
                    CommonTCritereAttributionPeer::doDelete($oneCritere, $this->_connexion);
                }
            }
        }
    }

    /*
     * permet de vérifier si on doit supprimer une critére
     * s'il n'est pas parmis les critéres à inserer
     */
    public function deleteThisCritere($oneCritere, $criteresAttributionInsert)
    {
        if ($oneCritere->getIdCritereAttribution()) {
            $id = $oneCritere->getIdCritereAttribution();
            if (is_array($criteresAttributionInsert)) {
                foreach ($criteresAttributionInsert as $critere) {
                    $oneCritereIn = $critere['critereVal'];
                    if ($oneCritereIn->getIdCritereAttribution() == $id) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function ajoutCriteresPonderation($sender, $param)
    {
        if ('' != $this->enonceCritere_ponderation->Text && '' != $this->ponderationCritere->Text) {
            $criteres = $this->getViewState('criteres'.self::PONDERATION);
            $i = $this->getViewState('count_'.self::PONDERATION);
            if (!$i) {
                $i = 0;
            }
            if (!is_array($criteres[self::PONDERATION])) {
                $criteres[self::PONDERATION] = [];
            }
            $totalPonderation = 0;
            foreach ($criteres[self::PONDERATION] as $critere) {
                $oneCritere = $critere['critereVal'];
                $totalPonderation += $oneCritere->getPonderation();
            }
            if (is_numeric($this->ponderationCritere->Text) && $this->ponderationCritere->Text <= 100) {
                $totalPonderation += $this->ponderationCritere->Text;
                if ($totalPonderation <= 100) {
                    $this->labelServerSideTotalPonderation->Text = "<script>document.getElementById('validSummaryPonderation').style.display = 'none';</script>";
                    $this->totalPonderation->value = $totalPonderation;
                    $criterePonderation = new CommonTCritereAttribution();
                    $criterePonderation->setPonderation((new Atexo_Config())->toPfEncoding($this->ponderationCritere->Text));
                    $criterePonderation->setEnonce((new Atexo_Config())->toPfEncoding($this->enonceCritere_ponderation->Text));
                    $criterePonderation->index = $i;
                    $sousCriteres = [];
                    $criteres[self::PONDERATION][$i] = ['critereVal' => $criterePonderation, 'sousCirteres' => $sousCriteres];
                    ++$i;
                    $this->repeaterCritereAttribPonderation->DataSource = $criteres[self::PONDERATION];
                    $this->repeaterCritereAttribPonderation->DataBind();
                    $this->panelCriteresPonderation->render($param->NewWriter);
                    $this->setViewState('criteres'.self::PONDERATION, $criteres);
                    $this->setViewState('count_'.self::PONDERATION, $i);
                } else {
                    $this->labelServerSideTotalPonderation->Text = "<script>document.getElementById('validSummaryPonderation').style.display = 'block';</script>";
                    $this->validSummaryPonderation->addServerError(Prado::localize('DEFINE_MSG_ERREUR_PONDERATION'), false);
                    return;
                }
            }

            $this->ponderationCritere->Text = '';
            $this->enonceCritere_ponderation->Text = '';
            $this->scriptLabel->Text = "<script>J('#panel_ajoutSousCritereAttribution_ponderation').hide();
		 										J('#panel_ajoutCritereAttribution_ponderation').hide();J('#lien_ajoutCritere_ponderation').show();</script>";
        }
    }

    public function ajoutCriteresDecroissant($sender, $param)
    {
        if ('' != $this->enonceCritere_ordreDecroissant->Text) {
            $criteres = $this->getViewState('criteres'.self::DECROISSANT);
            $i = $this->getViewState('count_'.self::DECROISSANT);
            if (!$i) {
                $i = 0;
            }
            if (!is_array($criteres[self::DECROISSANT])) {
                $criteres[self::DECROISSANT] = [];
            }
            $critereDecroissant = new CommonTCritereAttribution();
            $critereDecroissant->setEnonce((new Atexo_Config())->toPfEncoding($this->enonceCritere_ordreDecroissant->Text));
            //$critereDecroissant->setOrdre();
            $critereDecroissant->index = $i;
            $sousCriteres = [];
            $criteres[self::DECROISSANT][$i] = ['critereVal' => $critereDecroissant, 'sousCirteres' => $sousCriteres];
            ++$i;

            $this->repeaterCritereAttribOrdreDecroissant->DataSource = $criteres[self::DECROISSANT];
            $this->repeaterCritereAttribOrdreDecroissant->DataBind();
            $this->panelCriteresOrdreDecroissant->render($param->NewWriter);
            $this->setViewState('criteres'.self::DECROISSANT, $criteres);
            $this->setViewState('count_'.self::DECROISSANT, $i);
            $this->enonceCritere_ordreDecroissant->Text = '';
            $this->scriptLabel->Text = "<script>J('#panel_ajoutSousCritereAttribution_ordreDecroissant').hide();
		 										J('#panel_ajoutCritereAttribution_ordreDecroissant').hide();J('#lien_ajoutCritere_ordreDecroissant').show();</script>";
        }
    }

    public function suppressionCriteresPonderation($sender, $param)
    {
        $criteres = $this->getViewState('criteres'.self::PONDERATION);
        $key = $param->CallBackParameter;
        $oneCritere = $criteres[self::PONDERATION][$key];
        $criteresSupprime = $this->getViewState('criteresSupprime');
        $criteresSupprime[self::PONDERATION][] = $oneCritere;
        $this->setViewState('criteresSupprime', $criteresSupprime);
        $totalPonderation = $this->totalPonderation->value;
        $totalPonderation -= $criteres[self::PONDERATION][$key]['critereVal']->getPonderation();
        $this->totalPonderation->value = $totalPonderation;
        unset($criteres[self::PONDERATION][$key]);
        $this->repeaterCritereAttribPonderation->DataSource = $criteres[self::PONDERATION];
        $this->repeaterCritereAttribPonderation->DataBind();
        $this->panelCriteresPonderation->render($param->NewWriter);
        $this->setViewState('criteres'.self::PONDERATION, $criteres);
        $this->enonceCritere_ponderation->setText('');
        $this->ponderationCritere->setText('');
    }

    public function suppressionCriteresDecroissant($sender, $param)
    {
        $criteres = $this->getViewState('criteres'.self::DECROISSANT);
        $key = $param->CallBackParameter;
        $oneCritere = $criteres[self::DECROISSANT][$key];
        $criteresSupprime = $this->getViewState('criteresSupprime');
        $criteresSupprime[self::DECROISSANT][] = $oneCritere;
        $this->setViewState('criteresSupprime', $criteresSupprime);

        unset($criteres[self::DECROISSANT][$key]);
        $this->repeaterCritereAttribOrdreDecroissant->DataSource = $criteres[self::DECROISSANT];
        $this->repeaterCritereAttribOrdreDecroissant->DataBind();
        $this->panelCriteresOrdreDecroissant->render($param->NewWriter);
        $this->setViewState('criteres'.self::DECROISSANT, $criteres);
    }

    public function rafraichissementJs()
    {
        $script = '<script>'.
                  "displayOptionByClass(document.getElementById('".$this->criteresAttribution->ClientId."'),'".$this->alloti->getClientId()."');";
        if ($this->isObjetConsultation()) {
            if ($this->getIsAlloti()) {
                $script .= "toggleDisplay(document.getElementById('".$this->copy_attrib->ClientId."'),'definir-criteres".$this->getIdPanel()."');";
            } else {
                $script .= " J('.definir-criteres".$this->getIdPanel()."').show();";
            }
        }
        $script .= '</script>';
        $this->scriptLabel->Text = $script;
    }

    public function deleteOldCriteres(&$donneeComplementaire)
    {
        $critereAttributions = $donneeComplementaire->getAllCriteresAttributions();
        if (is_array($critereAttributions)) {
            foreach ($critereAttributions as $oneCritere) {
                if ($oneCritere instanceof CommonTCritereAttribution) {
                    //supprimer les sous critéres
                    $sousCritersToDelete = $oneCritere->getSousCriteres($this->_connexion);
                    foreach ($sousCritersToDelete as $sousCriterToDelete) {
                        $sousCriterToDelete->delete($this->_connexion);
                    }
                }
                CommonTCritereAttributionPeer::doDelete($oneCritere, $this->_connexion);
            }
        }
    }

    public function chargerComposant($recharger = false)
    {
        $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::populateData($recharger);
        self::rafraichissementJs();
    }

    public function enregistrerCritere($sender, $param)
    {
        $idCriteresAttribution = $this->criteresAttribution->getSelectedValue();
        switch ($idCriteresAttribution) {
            case 'layerDefinitionCriteres_1':
                self::ajoutCriteresDecroissant($sender, $param);
                                                $criteresD = $this->getViewState('criteres'.self::DECROISSANT);
                                                $this->setViewState('criteres'.self::DECROISSANT.'toSave', $criteresD);
                $this->tableauAttributionCriteria->DataSource = $criteresD[self::DECROISSANT];
                break;
            case 'layerDefinitionCriteres_2':
                self::ajoutCriteresPonderation($sender, $param);
                                                $criteresP = $this->getViewState('criteres'.self::PONDERATION);
                                                $this->setViewState('criteres'.self::PONDERATION.'toSave', $criteresP);
                $this->tableauAttributionCriteria->DataSource = $criteresP[self::PONDERATION];
                break;
        }

        // Repeater tableau détails critéres attribution page donnes complementaires
        $this->tableauAttributionCriteria->DataBind();
        $this->PanelAttributionCriteria->render($param->NewWriter);

        $this->scriptLabel->Text = '<script>'.
                                    "J('.modal-criteres').dialog('close');".
                                    '</script>';
    }

    public function annulerCritere($sender, $param)
    {
        $this->scriptLabel->Text = '';
        $idCriteresAttribution = $this->criteresAttribution->getSelectedValue();
        switch ($idCriteresAttribution) {
            case 'layerDefinitionCriteres_1':
                self::annulerCriteresDecroissant($sender, $param);
             // no break
            case 'layerDefinitionCriteres_2':
                self::annulerCriteresPonderation($sender, $param);
        }
        $this->scriptLabel->Text = '<script>'.
                                    "J('.modal-criteres').dialog('close');".
                                    '</script>';
    }

    public function annulerCriteresDecroissant($sender, $param)
    {
        $this->enonceCritere_ordreDecroissant->Text = '';
        $criteresD = self::getCriteresApresAnnuler(self::DECROISSANT);
        $this->repeaterCritereAttribOrdreDecroissant->DataSource = $criteresD;
        $this->repeaterCritereAttribOrdreDecroissant->DataBind();
        $this->panelCriteresOrdreDecroissant->render($param->NewWriter);
    }

    public function annulerCriteresPonderation($sender, $param)
    {
        $this->enonceCritere_ponderation->Text = '';
        $this->ponderationCritere->Text = '';
        $criteresP = self::getCriteresApresAnnuler(self::PONDERATION);
        $totalPonderation = self::getTotalPonderation($criteresP);
        $this->totalPonderation->value = $totalPonderation;
        $this->repeaterCritereAttribPonderation->DataSource = $criteresP;
        $this->repeaterCritereAttribPonderation->DataBind();
        $this->panelCriteresPonderation->render($param->NewWriter);
    }

    /*
     * permet de retourner les critéres aprés le click sur annuler
     */
    public function getCriteresApresAnnuler($type)
    {
        $criteresSave = $this->getViewState('criteres'.$type.'toSave');
        $this->setViewState('criteres'.$type, $criteresSave);
        $criteresAnuuler = $criteresSave[$type];
        if (!is_array($criteresAnuuler)) {
            $criteresAnuuler = [];
        }

        return $criteresAnuuler;
    }

    /*
     * Permet de valider les données des critéres d'attribution
     */
    public function validateCritereAttribution(&$isValid, &$arrayMsgErreur, $alloti = false)
    {
        $idCriteresAttribution = $this->criteresAttribution->getSelectedValue();
        if ($this->copy_attrib->checked || !$alloti) {
            if ('layerDefinitionCriteres_2' == $idCriteresAttribution) {
                if ('100' != $this->totalPonderation->value) {
                    $isValid = false;
                    $arrayMsgErreur[] = Prado::localize('DEFINE_CRITERES_ATTRIBUTION').' - '.Prado::localize('DEFINE_SOMME_PONDERATION_EGAL_CENT');
                }
                $repeaterCritereAttrib = 'repeaterCritereAttrib'.self::PONDERATION;
                $erreurTrouver = false;
                foreach ($this->$repeaterCritereAttrib->getItems() as $item) {
                    if (!$erreurTrouver) {
                        $totalSouPonderation = intval($item->totalPonderationSousCritere->value);
                        if ($totalSouPonderation) {
                            if (Atexo_Config::getParameter('SOMME_PONDERATION_SOUS_CRITERE_CENT')) {
                                $ponderationPere = 100;
                            } else {
                                $ponderationPere = intval($item->criterePonderationHidden->value);
                            }
                            if ($totalSouPonderation !== $ponderationPere) {
                                $erreurTrouver = true;
                                $isValid = false;
                                $arrayMsgErreur[] = Prado::localize('DEFINE_CRITERES_ATTRIBUTION').' - '.Prado::localize('SOMME_PONDERATION_INCORRECTE');
                            }
                        }
                    }
                }
            } elseif ('layerDefinitionCriteres_1' == $idCriteresAttribution) {
                $repeaterCritereAttrib = 'repeaterCritereAttrib'.self::DECROISSANT;
                if (0 == (is_countable($this->$repeaterCritereAttrib->Items) ? count($this->$repeaterCritereAttrib->Items) : 0)) {
                    $isValid = false;
                    $arrayMsgErreur[] = Prado::localize('DEFINE_CRITERES_ATTRIBUTION');
                }
            }
        }
    }

    /*
     * Permet de savoir si la consultation est alloti
     */
    public function getIsAlloti()
    {
        if ($this->getObjet() instanceof CommonConsultation) {
            $consultation = $this->getObjet();
            if ($consultation->getAlloti()) {
                return true;
            }

            return false;
        }

        return false;
    }

    /*
     * Permet de retourner si les critere sont identique à ceux de la consultation
     */
    public function isCritereIdentiqueCons()
    {
        $this->reloadDonneeComplementaire();
        if (!($this->getObjet() instanceof CommonConsultation) && ($this->getDonneeComplementaireCons() instanceof CommonTDonneeComplementaire)
            && ($this->getDonneeComplementaireCons() instanceof CommonTDonneeComplementaire) && ($this->getDonneeComplementaireCons()->getCriteresIdentiques())) {
            return true;
        }

        return false;
    }

    public function reloadDonneeComplementaire()
    {
        if (isset($_GET['id'])) {
            $reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
            if (!is_int($reference)) {
                $reference = base64_decode($reference);
            }
            $consultation = $this->retrieveConsultation($reference);
            if ($consultation) {
                $donneesComplementaireCons = $consultation->getDonneComplementaire();
                $this->setDonneeComplementaireCons($donneesComplementaireCons);
            }
        }
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $refConsultation reference  de la consultation
     */
    public function retrieveConsultation($refConsultation)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($refConsultation);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = (new Atexo_Consultation())->search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /*
     * Permet de retourner si l'objet est consultation
     */
    public function isObjetConsultation()
    {
        if ($this->getObjet() instanceof CommonConsultation) {
            return true;
        }

        return false;
    }

    /*
     * afficher le panele pour ajouter un sous critere
     */
    public function definirSousCritere($sender, $param)
    {
        if (null !== $param->CallbackParameter) {
            $definirElements = $param->CallbackParameter;
            $idsElement = explode('#', $definirElements);
            if (is_array($idsElement) && count($idsElement) > 1) {
                $indexPere = 'criterePereIndexElement'.$idsElement[1];
                $this->$indexPere->value = $idsElement[0];
                if (isset($idsElement[2])) {
                    $this->IndexItemPere->value = $idsElement[2];
                }
                $this->enonceCritere_ponderation->setText('');
                $this->ponderationCritere->setText('');
                $this->scriptJs->Text = "";
                $this->scriptJs->Text = "<script> J('#panel_ajoutSousCritereAttribution_".$idsElement[1]."').show();
										J('#panel_ajoutCritereAttribution_".$idsElement[1]."').hide();J('#lien_ajoutCritere_".$idsElement[1]."').show();</script>";
            }
        }
    }

    /*
     * Permet d'ajouter un sous critere
     */
    public function ajoutSousCriteres($sender, $param)
    {
        if ($param->CallbackParameter) {
            $this->labelServerSideTotalPonderation->Text = "<script>document.getElementById('validSummarySousPonderation').style.display = 'none';</script>";
            $type = $param->CallbackParameter;
            $allCriteres = $this->getViewState('criteres'.$type);
            $criteres = $allCriteres[$type];
            $ennonceCritere = 'enonceSousCritere'.$type;
            if ('' != $this->$ennonceCritere->Text && (self::DECROISSANT == $type || (self::PONDERATION == $type && '' != $this->ponderationSousCritere->Text))) {
                ///// TODO
                $indexElementPere = 'criterePereIndexElement'.$type;
                foreach ($criteres as $oneCritere) {
                    if ($oneCritere['critereVal']->index == $this->$indexElementPere->value) {
                        $sousCriteres = $oneCritere['sousCirteres'];
                        $totalSousCritere = 0;
                        foreach ($sousCriteres as $sousCritere){
                            $totalSousCritere += $sousCritere->getPonderation();
                        }
                        $sousCritere = new CommonTSousCritereAttribution();
                        if (self::PONDERATION == $type) {
                            $ponderation = (new Atexo_Config())->toPfEncoding($this->ponderationSousCritere->Text);
                            $critereAttributionParent = $oneCritere['critereVal'];
                            if (($ponderation + $totalSousCritere) > $critereAttributionParent->getPonderation()) {
                                $this->labelServerSideTotalPonderation->Text = "<script>document.getElementById('validSummarySousPonderation').style.display = 'block';</script>";
                                $this->validSummarySousPonderation->addServerError(Prado::localize('SOMME_PONDERATION_INCORRECTE'), false);
                                return;
                            }
                            $totalPonderation = $oneCritere['critereVal']->getTotalPonderationSousC();
                            $totalPonderation += $ponderation;
                            $sousCritere->setPonderation($ponderation);
                            $oneCritere['critereVal']->setTotalPonderationSousC($totalPonderation);
                            $this->ponderationSousCritere->Text = '';
                        }
                        $sousCritere->setEnonce((new Atexo_Config())->toPfEncoding($this->$ennonceCritere->Text));
                        $this->$ennonceCritere->Text = '';
                        if ($oneCritere['critereVal']->getIdCritereAttribution()) {
                            $sousCritere->setIdCritereAttribution($oneCritere['critereVal']->getIdCritereAttribution());
                        }
                        $sousCritere->indexPere = $oneCritere['critereVal']->index;
                        $sousCriteres[] = $sousCritere;
                        $allCriteres[$type][$this->$indexElementPere->value] = ['critereVal' => $oneCritere['critereVal'], 'sousCirteres' => $sousCriteres];
                    }
                }
                $this->setViewState('criteres'.$type, $allCriteres);
                $repeaterCritereAttrib = 'repeaterCritereAttrib'.$type;
                $this->$repeaterCritereAttrib->DataSource = $allCriteres[$type];
                $this->$repeaterCritereAttrib->DataBind();
                $panelCriteres = 'panelCriteres'.$type;
                $this->$panelCriteres->render($param->NewWriter);

                $this->scriptJs->Text = "<script> J('#panel_ajoutCritereAttribution_".$type."').hide();
											J('#panel_ajoutSousCritereAttribution_".$type."').hide();J('#lien_ajoutCritere_".$type."').show();</script>";
            }
        }
    }

    /*
     * Supprime un sous critere
     */
    public function suppressionSousCriteres($sender, $param)
    {
        if ($param->CallbackParameter) {
            $definirElements = $param->CallbackParameter;
            $idsElement = explode('#', $definirElements);
            if (is_array($idsElement) && count($idsElement) > 1) {
                $indexParent = $idsElement[0];
                $indexElement = $idsElement[1];
                $type = $idsElement[2];
                $allCriteres = $this->getViewState('criteres'.$type);
                $criteresSupprime = $this->getViewState('sousCriteresSupprime'.$type);
                $criteresSupprime[] = $allCriteres[$type][$indexParent]['sousCirteres'][$indexElement];
                $this->setViewState('sousCriteresSupprime'.$type, $criteresSupprime);
                if (self::PONDERATION == $type) {
                    $sousCritere = $allCriteres[$type][$indexParent]['sousCirteres'][$indexElement];
                    $ponderation = (int) $sousCritere->getPonderation();
                    $totalSousPonderation = $allCriteres[$type][$indexParent]['critereVal']->getTotalPonderationSousC() - $ponderation;
                    $allCriteres[$type][$indexParent]['critereVal']->setTotalPonderationSousC($totalSousPonderation);
                }
                unset($allCriteres[$type][$indexParent]['sousCirteres'][$indexElement]);
                $this->setViewState('criteres'.$type, $allCriteres);
                $repeaterCritereAttrib = 'repeaterCritereAttrib'.$type;
                $this->$repeaterCritereAttrib->DataSource = $allCriteres[$type];
                $this->$repeaterCritereAttrib->DataBind();
                $panelCriteres = 'panelCriteres'.$type;
                $this->$panelCriteres->render($param->NewWriter);
                $this->enonceCritere_ponderation->setText('');
                $this->ponderationCritere->setText('');
                $this->scriptJs->Text = "<script> J('#panel_ajoutSousCritereAttribution_".$type."').hide();
									J('#panel_ajoutCritereAttribution_".$type."').hide();J('#lien_ajoutCritere_".$type."').show();</script>";
            }
        }
    }

    public function isCritereIdentiqueChecked()
    {
        if ($this->copy_attrib->checked) {
            return true;
        }

        return false;
    }

    /*
     * Permet d'afficher les elements du composant selon le mode choisi
     */
    public function enabledElement()
    {
        return match ($this->getMode()) {
            0 => false,
            1 => true,
            default => true,
        };
    }

    /*
     * on l'utilise si on veux afficher plusieurs instance de ce composant dans une même page
     */
    public function getIdPanel()
    {
        if ($this->idElement) {
            return '_'.$this->idElement;
        } else {
            return '';
        }
    }

    /*
     *  Permet de retourner la valeur courante de l'id de critere d'attribution
     *
     *  @return Integer id critere
     *  @author : loubna.ezziani@atexom.com
     *  @version : since 4.5.0
     */
    public function getCurrentIdCritereAttribution()
    {
        $idCriteresAttribution = $this->criteresAttribution->getSelectedValue();
        $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $idCriteresAttribution);
        if ($valeurRef instanceof CommonValeurReferentiel) {
            return $valeurRef->getId();
        }
    }

    public function rendPanel($param)
    {
        $this->panelCriteresAttribution->render($param->getNewWriter());
    }

    public function fillRepeaterAttributionCriteria(): void
    {
        $consultation = $this->getConsultation();

        $criteresAttribution = $consultation?->getDonneComplementaire()?->getAllCriteresAttributions();

        $this->tableauAttributionCriteria->DataSource = $criteresAttribution;
        $this->tableauAttributionCriteria->DataBind();
    }

    public function getIdCritereAttribution()
    {
        $idCritereAttribution = null;
        if ($this->getDonneeComplementaireCons() instanceof CommonTDonneeComplementaire) {
            $idCritereAttribution = $this->getDonneeComplementaireCons()->getIdCritereAttribution();
        } elseif ($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $idCritereAttribution = $this->_donneeComplementaire->getIdCritereAttribution();
        }

        return $this->criteresAttribution->getSelectedValue() === self::LABEL_CRITERE_PONDERATION
            ? self::ID_CRITERE_PONDERATION
            : $idCritereAttribution
            ;
    }

}
