<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class AtexoRepeaterPiecesReponse extends MpeTTemplateControl
{
    private array $_dataSource = [];
    private $_nbreResultats;
    private $_postBack;
    private $_typeReponse;
    private $_pieceTitle;
    private $_container;

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->fillRepeaterPiecesReponse();
        }
    }

    public function setPieceTitle($value)
    {
        $this->_pieceTitle = $value;
    }

    public function getPieceTitle()
    {
        return $this->_pieceTitle;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setDataSource($value)
    {
        $this->_dataSource = $value;
    }

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setNbreResultats($value)
    {
        $this->_nbreResultats = $value;
    }

    public function getNbreResultats()
    {
        return $this->_nbreResultats;
    }

    public function setTypeReponse($value)
    {
        $this->_typeReponse = $value;
    }

    public function getTypeReponse()
    {
        return $this->_typeReponse;
    }

    public function setContainer($value)
    {
        $this->_container = $value;
    }

    public function getContainer()
    {
        return $this->_container;
    }

    public function fillRepeaterPiecesReponse()
    {
        $dataPieces = $this->getDataPieces();
        $indexPiece = is_countable($dataPieces) ? count($dataPieces) : 0;

        $nbrePiecesArajouter = 1;
        if (0 == $indexPiece) {
            $nbrePiecesArajouter = 1;
        }
        if ('candidature' == $this->getTypeReponse()) {
            $textPiece = Prado::Localize('DEFINE_TEXT_FICHIER').' n° ';
        } else {
            $textPiece = Prado::Localize('DEFINE_AUTRE_PIECE').' n° ';
        }
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataPieces[$indexPiece]['pieceText'] = $textPiece;
            $dataPieces[$indexPiece]['intitulePiece'] = '';
            $dataPieces[$indexPiece]['numPiece'] = $indexPiece + 1;

            ++$indexPiece;
        }
        if ($nbrePiecesArajouter) {
            $this->remplirPieces($dataPieces);
        }
    }

    public function getDataPieces()
    {
        $indexPiece = 0;
        $dataPieces = [];

        foreach ($this->listePiecesRepeater->getItems() as $item) {
            $dataPieces[$indexPiece]['pieceText'] = Atexo_Util::utf8ToIso($item->pieceText->Text);
            $dataPieces[$indexPiece]['intitulePiece'] = Atexo_Util::utf8ToIso($item->intitulePiece->Text);
            $dataPieces[$indexPiece]['numPiece'] = $item->numPiece->Text;
            $dataPieces[$indexPiece]['deleteBtn'] = $item->deleteBtn->Visible;
            ++$indexPiece;
        }
        $this->setDataSource($dataPieces);

        return $dataPieces;
    }

    public function remplirPieces($dataPieces)
    {
        $this->listePiecesRepeater->dataSource = $dataPieces;
        $this->listePiecesRepeater->dataBind();
        $indexPiece = 0;
        foreach ($this->listePiecesRepeater->getItems() as $item) {
            $item->pieceText->Text = $dataPieces[$indexPiece]['pieceText'];
            $item->intitulePiece->Text = $dataPieces[$indexPiece]['intitulePiece'];
            $item->numPiece->Text = $dataPieces[$indexPiece]['numPiece'];
            $item->deleteBtn->Visible = $dataPieces[$indexPiece]['deleteBtn'];
            ++$indexPiece;
        }
        //$this->nbrPieces->Text = prado::localize('DEFINE_NOMBRE_Piece') . " : " . $nombrePieces;
    }

    public function addPiece($sender, $param)
    {
        $dataPieces = $this->getDataPieces();
        $indexPiece = is_countable($dataPieces) ? count($dataPieces) : 0;

        $nbrePiecesArajouter = 1;
        //$dataPieces[]['intitulePiece'] = '';
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataPieces[$indexPiece]['pieceText'] = Prado::Localize('DEFINE_TEXT_FICHIER').' n° ';
            $dataPieces[$indexPiece]['intitulePiece'] = '';
            $dataPieces[$indexPiece]['numPiece'] = $indexPiece + 1;
            ++$indexPiece;
        }
        $this->remplirPieces($dataPieces);
        $this->panelPieces->render($param->getNewWriter());
    }

    public function deleteItemPiece($sender, $param)
    {
        $finalData = [];
        $i = 0;
        if ('delete' == $param->CommandName) {
            $dataPieces = $this->getDataPieces();
            unset($dataPieces[$param->Item->ItemIndex]); //print_r($dataPieces);exit;
            foreach ($dataPieces as $un) {
                $finalData[$i]['pieceText'] = $un['pieceText'];
                $finalData[$i]['intitulePiece'] = $un['intitulePiece'];
                $finalData[$i]['numPiece'] = $i + 1;
                $finalData[$i]['deleteBtn'] = $un['deleteBtn'];
                ++$i;
            }
            $this->remplirPieces($finalData);
        }
    }

    public function styleTableauPieces()
    {
        if ($this->Page->_acteEngagement) {
            return true;
        } else {
            return false;
        }
    }
}
