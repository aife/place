<!--Debut Bloc-->
	<com:TRepeater ID="repeaterAvisLot">
		<prop:ItemTemplate>
		 <div class="toggle-panel">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div onclick="togglePanel(this,'panel_avisMembres_<%#$this->Data['sousPli']%>');" class="title-toggle-open"><%#$this->Data['description']%></div>
				<span id="test"></span>
				<div id="panel_avisMembres_<%#$this->Data['sousPli']%>" style="display:block;" class="panel">
				<com:THiddenField id="sousPli" Value="<%#$this->Data['sousPli']%>" />
				<com:THiddenField id="formeEnv" Value="<%#$this->Data['formeEnv']%>" />
				<com:THiddenField id="idOffre" Value="<%#$this->Data['idOffre']%>" />
				<com:THiddenField id="typeEnv" Value="<%#$this->Data['typeEnv']%>" />
					<!--Debut Tableau -->
					<div class="table-bloc">
						<com:TRepeater ID="repeaterAvisCAO" EnableViewState="true" >
							<prop:HeaderTemplate>
								<table summary="Liste des avis par meembres" class="table-results">
									<thead>
										<tr>
											<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
										</tr>
										<tr>
											<th class="col-180" id="membre"><com:TTranslate>DEFINE_NOM_PRENOM</com:TTranslate></th>
											<th class="col-80 center" id="admissible"><com:TTranslate>ADMISSIBLE</com:TTranslate></th>
											<th class="col-80 center" id="nonAdmissible"><com:TTranslate>NON_ADMISSIBLE</com:TTranslate></th>
											<th class="col-80 center" id="aTraiter"><com:TTranslate>A_TRAITER</com:TTranslate></th>
											<th class="col-300" id="commentaire"><com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate></th>
										</tr>
									</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
									<tbody>
										<tr>
											<com:THiddenField id="idAgent" Value="<%#$this->Data['idAgent']%>" />
											<com:THiddenField id="idEnv" Value="<%#$this->Data['idEnv']%>" />
											<td class="col-180" headers="membre"><com:TLabel ID="nomAgent" TEXT="<%#$this->Data['nom']%>"><%#$this->Data['nom']%></com:TLabel> <com:TLabel ID="prenomAgent" TEXT="<%#$this->Data['prenom']%>"><%#$this->Data['prenom']%></com:TLabel></td>
											<td class="col-80 center" headers="admissible">
												<label for="admissible_Lot1" style="display:none;">
														<com:TTranslate>ADMISSIBLE</com:TTranslate>
												</label>
												<com:TRadioButton ID="lotAdmis" GroupName="admissibillite" Enabled="<%#$this->Data['enabled']%>" Checked="<%#$this->Page->isLotAdmis($this->Data['admissibilite'])%>" Attributes.title="<%=Prado::localize('ADMISSIBLE')%>" ></com:TRadioButton>
											<td class="col-80 center" headers="nonAdmissible">
												<label for="non_admissible_Lot1" style="display:none;">
														<com:TTranslate>NON_ADMISSIBLE</com:TTranslate>
												</label>
												<com:TRadioButton ID="lotNonAdmis" GroupName="admissibillite" Enabled="<%#$this->Data['enabled']%>" Checked="<%#$this->Page->isLotNonAdmis($this->Data['admissibilite'])%>" Attributes.title="<%=Prado::localize('NON_ADMISSIBLE')%>"></com:TRadioButton>
											<td class="col-80 center" headers="aTraiter">
												<label for="aTraiter_Lot1" style="display:none;">
														<com:TTranslate>A_TRAITER</com:TTranslate>
												</label>
												<com:TRadioButton ID="lotATraiter" GroupName="admissibillite"  Enabled="<%#$this->Data['enabled']%>" Checked="<%#$this->Page->isLotATraiter($this->Data['admissibilite'])%>" Attributes.title="<%=Prado::localize('A_TRAITER')%>" ></com:TRadioButton>
											<td class="col-300" headers="commentaire">
												<label for="commentaire_Lot1" style="display:none;">
														<com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate>
												</label>
												<com:TTextBox ID="commentaireLot" TextMode="MultiLine"  Enabled="<%#$this->Data['enabled']%>" Text="<%#$this->Data['commentaire']%>" Attributes.title="<%=Prado::localize('DEFINE_COMMENTAIRE')%>" CssClass="bloc-300"/>
										</tr>
									</tbody>
							</prop:ItemTemplate>
			    			<prop:FooterTemplate>
										</table>
							</prop:FooterTemplate>
					   </com:TRepeater>
					</div>
					<div class="spacer-small"></div>
					<!--Fin Tableau -->
					
				</div>						
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		</prop:ItemTemplate>
	</com:TRepeater>
	
<!--Fin Bloc-->