<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_SyntheseRapportAudit;

/*
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */

class AtexoTableauSyntheseRapportAudit extends MpeTTemplateControl
{
    private string $_postBack = '';
    private bool $_calledFromAgent = true;
    private bool $_organisme = false;
    private bool $_serviceId = false;
    private bool $_annee = false;
    private bool $_classification = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCalledFromAgent($value)
    {
        $this->_calledFromAgent = $value;
    }

    public function getCalledFromAgent()
    {
        return $this->_calledFromAgent;
    }

    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setServiceId($value)
    {
        $this->_serviceId = $value;
    }

    public function getServiceId()
    {
        return $this->_serviceId;
    }

    public function setAnnee($value)
    {
        $this->_annee = $value;
    }

    public function getAnnee()
    {
        return $this->_annee;
    }

    public function setClassification($value)
    {
        $this->_classification = $value;
    }

    public function getClassification()
    {
        return $this->_classification;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->displaySyntheseRapportAudit();
        }
        if ('false' == $this->getCalledFromAgent()) {
            $this->linkAdd->setVisible(false);
        }
    }

    /**
     * affiche la liste des Synthèse des Rapport d'audit.
     */
    public function displaySyntheseRapportAudit()
    {
        $dataSource = [];
        $dataSource = (new Atexo_SyntheseRapportAudit())->retreiveSyntheseRapportAudit($this->getOrganisme(), $this->getServiceId(), $this->getAnnee(), $this->getClassification());

        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            $this->panelListeSRA->setDisplay('None');
            $this->panelNoElementFound->setDisplay('Dynamic');
        } else {
            $this->repeaterListeSRA->DataSource = $dataSource;
            $this->repeaterListeSRA->DataBind();
            $this->panelListeSRA->setDisplay('Dynamic');
            $this->panelNoElementFound->setDisplay('None');
        }
    }

    /**
     * Refresh du tableau des listes Synthèse des Rapport d'audit.
     */
    public function onCallBackRefreshTableau($sender, $param)
    {
        $this->displaySyntheseRapportAudit();
        $this->panelListeSRA->render($param->getNewWriter());
    }

    /**
     * Supprimer une Synthèse des Rapport d'audit.
     */
    public function deleteSRA($sender, $param)
    {
        (new Atexo_SyntheseRapportAudit())->doDeleteSRA($param->CommandName, Atexo_CurrentUser::getOrganismAcronym());
    }

    /*
     * retourne le nombre de colonne à aficher dans le tableau
     */
    public function setColSpan()
    {
        if ('false' == $this->Page->tableauSRA->getCalledFromAgent() ||
          ('true' == $this->Page->tableauSRA->getCalledFromAgent() && !Atexo_CurrentUser::hasHabilitation('CreerAnnonceSyntheseRapportAudit'))
        ) {
            return 3;
        } else {
            return 4;
        }
    }
}
