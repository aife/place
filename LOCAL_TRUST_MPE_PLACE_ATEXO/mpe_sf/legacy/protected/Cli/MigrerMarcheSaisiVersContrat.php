<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Exception;
use PDO;

/**
 * Classe de numerotation des accord cadres.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_MigrerMarcheSaisiVersContrat extends Atexo_Cli
{
    public static function run()
    {
        $connexion = null;
        $sql = null;
        $logger = Atexo_LoggerManager::getLogger('contrat');
        $logger->info('Début de la migration des marché saisi en 2015 vers contrat');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexion->beginTransaction();
            $arrayOrganismeChorus = [];

            $sql = <<<SQL
select M.id AS cle_table_source, IF(M.objetMarche LIKE '%Marché%subséquent%','7','1') as type_contrat, M.organisme,if(M.idService IS NULL,0,M.idService) as service_id,NULL AS num_EJ,
NULL AS entreprise_id,NULL AS id_etablissement,NULL AS id_offre,NULL AS type_enveloppe,
    M.objetMarche as objet_marche,IF(M.montantMarche IS NULL,NULL,CAST((REPLACE(REPLACE(M.montantMarche,' ',''),',','.')) as DECIMAL(65,2))) as montant_marche,
	M.idMarcheTrancheBudgetaire as tranche_budgetaire,0 as publication_montant,
    0 as publication_contrat,NULL as statutEJ,NULL as numero_marche , NULL as num_long_OEAP,4 as statut_contrat, M.natureMarche as categorie,
	M.dateNotification as date_notification_reelle, Null as date_fin_marche_previsionnel,
    NULL AS date_notification,NULL AS date_fin_marche_reelle,NULL as date_decision,NOW() as date_creation,NOW() as date_modification, '0' AS envoi_interface, NULL  as reference_libre,
    NULL AS inscrit_id,NULL AS nom_inscrit, NULL AS prenom_inscrit, NULL AS email_inscrit, NULL AS telephone_inscrit, NULL AS fax_inscrit, NULL AS lot, NULL AS reference,1 as hors_passation,
    1 as genereNumerotation,M.nomAttributaire as raisonSociale,M.ville,M.codePostal,M.acronymePays_Attributaire as acronymePays,M.pays_Attributaire as pays,M.siren_Attributaire as siren,
    M.nic_Attributaire as nic,M.identifiantNational_Attributaire as identifiantNational,M.rc_ville_attributaire,M.rc_num_attributaire
FROM Marche M
WHERE M.isManuel='1'
AND M.numeroMarcheAnnee='2015';


SQL;
            $logger->info('Sql de récupération des marchés saisi : '.$sql."\n");
            //return;
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();
            $nbreMarchesSaisi = $statement->rowCount();
            $logger->info('Nombre des marchés saisi : '.$nbreMarchesSaisi."\n");
            $i = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $logger->info('Marche  : '.$row['cle_table_source'].' Organisme ('.$row['organisme'].") \n");
                self::createEntrepriseEtablissement($row, $connexion, $logger);
                $increment = Atexo_Util::validateNumeroSequanceByLenghtElement($i++, 3);
                $row['reference_libre'] = 'MARCHE_ART.133_2015_'.$increment;
                $contrat = (new Atexo_Consultation_Contrat())->createContrat($row, $connexion, $logger);
            }
            $connexion->commit();
        } catch (Exception $e) {
            $connexion->rollback();
            $logger->error("Erreur migration contrat : $sql ");
            $logger->error('Erreur migration contrat : '.$e->getMessage().$e->getTraceAsString());
        }
        $logger->info('Fin de la migration des marché saisi en 2015 vers contrat');
    }

    public function getEntreprise($siren, $idNational, $pays)
    {
        if ($siren) {
            return Atexo_Entreprise::retrieveCompanyBySiren($siren);
        } elseif ($idNational) {
            return (new Atexo_Entreprise())->retrieveCompanyByIdNational($idNational, $pays);
        }

        return false;
    }

    public function getEtablissement($siret, $idEntreprise)
    {
        if ($siret) {
            $etablissementQuery = new CommonTEtablissementQuery();

            return $etablissementQuery->getEtabissementbyIdEseAndCodeEtab($siret, $idEntreprise);
        }

        return false;
    }

    public function createEntrepriseEtablissement(&$row, $connexion, $logger)
    {
        $etablissement = null;
        $logger->info(" creation de l'entreprise et l'etablissement");
        $siren = $row['siren'];
        $idNational = $row['identifiantNational'];
        $nic = $row['nic'];
        $pays = $row['pays'];

        $company = self::getEntreprise($siren, $idNational, $pays);
        if (!$company) {
            $company = new Entreprise();
            $company->setSiren($siren);
            $company->setSirenetranger($idNational);
            $company->setPaysenregistrement($pays);
            $company->setNom($row['raisonSociale']);
            $company->setAcronymePays($row['acronymePays']);
            $company->setSaisieManuelle('1');
            $company->setDateCreation(date('Y-m-d H:i:s'));
            $company->setDateModification(date('Y-m-d H:i:s'));
            $company->save($connexion);
        } else {
            $etablissement = self::getEtablissement($nic, $company->getId());
        }
        if (!$etablissement) {
            $etablissement = new CommonTEtablissement();
            $etablissement->setIdEntreprise($company->getId());
            $etablissement->setCodeEtablissement($nic ?: '1');
            $etablissement->setCodePostal($row['codePostal']);
            $etablissement->setVille($row['ville']);
            $etablissement->setPays($pays);
            $etablissement->setSaisieManuelle('1');
            $etablissement->setDateCreation(date('Y-m-d H:i:s'));
            $etablissement->setDateModification(date('Y-m-d H:i:s'));
            $etablissement->save($connexion);
        }
        $row['entreprise_id'] = $company->getId();
        $row['id_etablissement'] = $etablissement->getIdEtablissement();
        $logger->info(' ID entreprise = '.$company->getId().' id etablissement = '.$etablissement->getIdEtablissement());
    }
}
