<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;

/**
 * Recupère les CRL sur un serveur centralisé.
 *
 * @category Atexo
 */
class Cli_InstallOpenSSL extends Atexo_Cli
{
    public static function run()
    {
        $mpePath = realpath(__DIR__.'/../../');
        $root_dir = Atexo_Config::getParameter('BASE_ROOT_DIR');
        if (!is_dir($root_dir)) {
            echo 'Création dossier '.$root_dir."\n";
            mkdir($root_dir, 0764, true);
        }

        echo "\nCopie des fichiers nécessaire à OpenSSL TS \n";
        $openssl_cnf = file_get_contents("$mpePath/ressources/openssl/openssl.cnf");
        $openssl_cnf = str_replace('###COMMON_DIR###', $root_dir, $openssl_cnf);
        file_put_contents("$root_dir/common/openssl.cnf", $openssl_cnf);
        file_put_contents("$root_dir/common/serial", "AEA3742F\n");
        // Generation AC
        echo "\nGénération CA...\n";
        shell_exec("bash '$mpePath/ressources/openssl/certificats.sh' -d $root_dir/common/ca -n \"".Atexo_Config::getParameter('PF_SHORT_NAME').'" -e "'.Atexo_Config::getParameter('PF_MAIL_FROM').'" ');
        // Récupération des CRL
        echo "\nRécupération CRL...\n";
        include 'protected/bin/Cli/Crl.php';
        Cli_Crl::run();
        // copie du root_ca
        $trust_dir = Atexo_Config::getParameter('TRUST_DIR');
        echo "Copie du root_ca dans le dossier trust et hash OpenSSL\n";
        //shell_exec("cp $root_dir/common/ca/certs/root_ca.crt $root_dir/common/trust");
        //shell_exec("cd $root_dir/common/trust && ./trust.sh");

        shell_exec("cp $root_dir/common/ca/certs/root_ca.crt $trust_dir");
        shell_exec("cd $trust_dir && ./trust.sh");
    }
}
