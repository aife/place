<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Envoi le mail d'alertes aux agents des consultations clotures ce jour.
 *
 * @category Atexo
 */
class Cli_AlerteAgentsClotureConsultation extends Atexo_Cli
{
    public static function run()
    {
        Prado::log('Début envoi des alertes ', TLogger::INFO, 'Atexo.Cli.Cron.AlerteAgents');
        $commonConsultationQuery = new CommonConsultationQuery();
        $commonConsultations = $commonConsultationQuery->getConsultationsCloturees();
        if (is_array($commonConsultations)) {
            foreach ($commonConsultations as $commonConsultation) {
                if ($commonConsultation instanceof CommonConsultation) {
                    /***********Préparation de la session***************/
                    $session = [0 => '',
                        1 => [], 2 => '',
                        3 => '', 4 => '',
                        5 => '', 6 => '',
                        7 => $commonConsultation->getOrganisme(),
                        8 => '', 9 => '',
                        10 => '', 11 => '',
                        12 => '', 13 => [],
                        14 => '', 15 => '',
                        16 => '', 17 => '',
                        18 => '', 19 => '',
                        20 => '', 21 => '',
                        22 => '', 23 => '',
                        24 => '', 25 => '',
                        26 => '', 27 => '',
                        28 => '', 29 => '',
                        30 => '', 31 => '',
                        32 => '', 33 => '',
                        34 => '', 35 => '',
                        36 => '', 37 => '',
                        38 => '', 39 => '',
                        40 => '', 41 => '',
                        42 => '', 43 => '',
                        44 => '', 45 => '',
                        46 => '', 47 => '',
                        48 => '', 49 => '',
                        50 => '', 51 => '',
                        52 => '', 53 => '',
                        54 => '', 55 => '',
                        56 => '', 57 => '',
                        58 => '', 59 => '',
                        60 => '', 61 => '',
                        62 => '', 63 => '',
                        64 => '', 65 => '',
                    ];
                    session_id(uniqid());
                    $_SESSION[session_id()] = serialize($session);
                    $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($commonConsultation->getId(), $commonConsultation->getOrganisme());
                    $corpsMail = (new Atexo_Message())->getContentMailForSynthesisAchievedConsultation($commonConsultation);
                    $params['consultationCloture'] = true;
                    $listIdAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris($commonConsultation->getId(), $params);

                    if (is_array($guests)) {
                        foreach ($guests as $guest) {
                            if ($guest->getAlerteClotureConsultation() &&
                                $guest->getMailAgent() &&
                                $guest->getActif() &&
                                (false !== array_search($guest->getId(), $listIdAgents)
                                    || Atexo_Config::getParameter('SPECIFIQUE_PLACE'))
                            ) {
                                $replaced = ['VALEUR_NOM_DEST', 'VALEUR_PRENOM_DEST'];
                                $replacedBy = [$guest->getLastName(), $guest->getFirstName()];
                                $mail = str_replace($replaced, $replacedBy, $corpsMail);
                                $from = Atexo_Config::getParameter('PF_MAIL_FROM', $commonConsultation->getOrganisme());
                                $pfName = null;

                                if ($commonConsultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                                    $plateformeVirtuelle = $commonConsultation->getCommonPlateformeVirtuelle();
                                    $from = $plateformeVirtuelle->getNoReply();
                                    $pfName = $plateformeVirtuelle->getFromPfName();
                                }

                                $to = $guest->getMailAgent();
                                $subject = Prado::localize('DEFINE_TEXT_OBJET_MAIL_CONSULTATION_CLOTURE');
                                Atexo_Message::simpleMail(
                                    $from,
                                    $to,
                                    $subject,
                                    $mail,
                                    '',
                                    '',
                                    false,
                                    true,
                                    true,
                                    $pfName
                                );
                            }
                        }
                    }
                }
            }
        } else {
            Prado::log('Aucune consultation trouvee ', TLogger::INFO, 'Atexo.Cli.Cron.AlerteAgents');
        }
        Prado::log('Fin envoi des alertes ', TLogger::INFO, 'Atexo.Cli.Cron.AlerteAgents');
    }
}
