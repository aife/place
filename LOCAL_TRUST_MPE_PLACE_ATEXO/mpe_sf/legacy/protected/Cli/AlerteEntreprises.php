<?php

namespace Application\Cli;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Service\Configuration\PlateformeVirtuelleService;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Alertes;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Envoi le mail d'alertes aux entreprises des consultations mises en ligne la veille.
 * Cron à lancer APRES minuit.
 *
 * @category Atexo
 */
class Cli_AlerteEntreprises extends Atexo_Cli
{
    public static function run()
    {
        $time_start = microtime(true);
        $logger = Atexo_LoggerManager::getLogger('alertesentreprise');
        $logger->info('Debut envoi des alertes ');
        //nettoyage du repertoire : supression des fichiers dates depuis plus que 3 semaines
        $logger->info('nettoyage du repertoire : supression des fichiers dates depuis plus que 3 semaines');
        self::nettoyerRepAlerte();

        $plateformesVirtuellesOrgs = [];
        $footerText = Prado::localize('TEXT_ABONNEMENT_ALERTES');
        $baseFooterForHtml = (new Atexo_Message())->genererFooterMailFormatHtml(true);

        /** @var PlateformeVirtuelleService $plateformeVirtuelleService */
        $plateformeVirtuelleService = Atexo_Util::getSfService(PlateformeVirtuelleService::class);

        $consultation = new Atexo_Consultation();
        $limit = Atexo_Config::getParameter('TRANCHE_ALERTE_ENTREPRISE');
        $offset = 0;
        $criteriaAlertes = (new Atexo_Entreprise_Alertes())->retreiveAlertesJoinInscrit($limit, $offset);

        if (is_array($criteriaAlertes)) {
            $modules = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
            $typeAcces = Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE');
            $etatConsultation = Atexo_Config::getParameter('STATUS_CONSULTATION');
            $forAlertesEntreprises = true;

            if (!self::construireTableConsultationsAlertes()) {
                $forAlertesEntreprises = false;
            }

            while (is_array($criteriaAlertes) && count($criteriaAlertes)) {
                $logger->info("traitement du lot limit : $limit offset : $offset nbre de critères : ".count($criteriaAlertes));
                foreach ($criteriaAlertes as $row) {
                    try {
                        $criteriaVo = (new Atexo_Entreprise_Recherches())->unserializeCriteria($row['xmlCriteria']);
                        $criteriaVo = (new Atexo_Entreprise_Alertes())->genererCriteriaVoAlerte($criteriaVo, $modules);
                        if ($criteriaVo instanceof Atexo_Consultation_CriteriaVo) {
                            $criteriaVo->setPeriodicite($row['periodicite']);
                            $criteriaVo->setTypeAcces($typeAcces);
                            $criteriaVo->setEtatConsultation($etatConsultation);
                            $criteriaVo->setcalledFromPortail(true);

                            $arrayConsultation = $consultation->search($criteriaVo, false, false, $forAlertesEntreprises);
                            $countCons = count($arrayConsultation);

                            if ($criteriaVo->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                                $typeNouvelleAvis = Prado::localize('TEXT_NOUVELLE_CONSULTATION');
                                $enteteMailText = PHP_EOL.Prado::localize('TEXT_ENTETE_NOUVELLE_CONSULTATION').PHP_EOL;
                                $templateEnteteMailHtml = (new Atexo_Message())->genererEntetMailFormatHtml(true, true);
                            } else {
                                $typeNouvelleAvis = Prado::localize('TEXT_NOUVELLE_ANNONCE');
                                $enteteMailText = PHP_EOL.Prado::localize('TEXT_ENTETE_NOUVELLE_ANNONCE').PHP_EOL;
                                $templateEnteteMailHtml = (new Atexo_Message())->genererEntetMailFormatHtml(false, true);
                            }
                            $logger->info('les alertes ids : '.$row['ids'].' nombre consultations : '.$countCons);

                            if ($countCons > 0) {
                                $alertes = explode('#_#', $row['liste_mail']);

                                if (is_array($alertes)) {
                                    $logger->info('liste mail : '.print_r($alertes, true));
                                    foreach ($alertes as $alerte) {
                                        $alerte = explode('|', $alerte);

                                        $plateformeVirtuelleId = $alerte[3] ?? null;

                                        $plateformeVirtuelle = null;
                                        $pfMailFrom = Atexo_Config::getParameter('PF_MAIL_FROM');
                                        $pfName = Atexo_Config::getParameter('PF_LONG_NAME');
                                        $pfReference = Atexo_Config::getParameter('PF_URL_REFERENCE');
                                        $codeDesign = null;

                                        if (!empty($plateformeVirtuelleId)) {
                                            /** @var PlateformeVirtuelle $plateformeVirtuelle */
                                            $plateformeVirtuelle = $plateformeVirtuelleService
                                                ->getPlateformeVirtuelleById($plateformeVirtuelleId);
                                            $pfMailFrom = $plateformeVirtuelle->getNoReply();
                                            $pfName = $plateformeVirtuelle->getFromPfName();
                                            $pfReference = $plateformeVirtuelle->getProtocole()
                                                .'://'
                                                .$plateformeVirtuelle->getDomain();
                                            $codeDesign = $plateformeVirtuelle->getCodeDesign();

                                            if (!isset($plateformesVirtuellesOrgs[$plateformeVirtuelleId])) {
                                                $plateformesVirtuellesOrgs[$plateformeVirtuelleId] = $plateformeVirtuelleService
                                                    ->getAcronymesOrgEligibleByDomaineId($plateformeVirtuelleId);
                                            }
                                        }
                                        $mailSubjectPrefix = $pfName;

                                        $tableauConsultationText = '';
                                        $tableauConsultationHtml = '';
                                        $count = 0;

                                        /** @var CommonConsultation $oneConsultation */
                                        foreach ($arrayConsultation as $oneConsultation) {
                                            $acronyme = $oneConsultation->getOrganisme();

                                            if (!empty($plateformeVirtuelleId) && !in_array(
                                                $acronyme,
                                                $plateformesVirtuellesOrgs[$plateformeVirtuelleId]
                                            )) {
                                                continue;
                                            }
                                            if ($plateformeVirtuelle) {
                                                $urlConsultation = $pfReference
                                                    .'/?page=entreprise.EntrepriseAdvancedSearch&AllCons&id='
                                                    .$oneConsultation->getId()
                                                    .'&orgAcronyme='.$oneConsultation->getOrganisme();
                                            } else {
                                                $urlConsultation = (new Atexo_Consultation())->getUrlAccesDirectProcPubliciteOuverte($oneConsultation->getId(), $oneConsultation->getOrganisme());
                                            }

                                            // cas format text
                                            $cartoucheEntreprise = (new Atexo_Message())->getInfoConsultationFromObject($oneConsultation);
                                            $lienAccesDirect = Prado::localize('TEXT_LIEN_ACCES_DIRECT').' : '
                                                .$urlConsultation;
                                            $tableauConsultationText .= "\n\n$cartoucheEntreprise\n$lienAccesDirect\n\n\n";
                                            // cas format html
                                            $couleur = (0 == $count % 2) ? '#ffffff' : '#EDEBEB';
                                            $listeCons = (new Atexo_Message())->genererListeConsForMailFormatHtml($oneConsultation, $urlConsultation);
                                            $tableauConsultationHtml .= "<tr bgcolor=\"$couleur\" style=\"background:$couleur;\">$listeCons\n";
                                            ++$count;
                                        }

                                        if (0 === $count) {
                                            continue;
                                        }

                                        $footerForHtml = str_replace('PF_LONG_NAME', $pfName, $baseFooterForHtml);

                                        $enteteMailHtml = str_replace(
                                            'IMAGE_BANDEAU',
                                            Atexo_Util::getImageBandeau($codeDesign),
                                            $templateEnteteMailHtml
                                        );

                                        $footerHtml = $footerForHtml;
                                        (new Atexo_Message())->genererFooterMailFormatHtmlFromCriteria($footerHtml, $row['xmlCriteria']);

                                        $email = $alerte[0];

                                        if (Atexo_Util::checkMail($email)) {
                                            $format = ('1' == $alerte[1]) ? true : false;
                                            $denomination = $alerte[2];
                                            if ($format) {
                                                $enteteMailHtml = str_replace('VALEUR_NOM_ALERTE', $denomination, $enteteMailHtml);
                                                $nameFile = 'alerteId_'.uniqid();

                                                $footerHtmlDefinitif = str_replace(
                                                    'LIEN_PRINT_PDF',
                                                    $pfReference
                                                    .'?page=Entreprise.GetMailFormatPdf&amp;idAlerte='
                                                    .$nameFile,
                                                    $footerHtml
                                                );
                                                $message = $enteteMailHtml.$tableauConsultationHtml.$footerHtmlDefinitif;
                                                Atexo_Util::writeFile(Atexo_Config::getParameter('CHEMIN_REP_SENT_MAIL_ALERTE').'/'.$nameFile, ($message));
                                            } else {
                                                $message = $enteteMailText.$tableauConsultationText.$footerText;
                                            }

                                            $ret = Atexo_Message::simpleMail(
                                                $pfMailFrom,
                                                $email,
                                                $mailSubjectPrefix.' - '.$typeNouvelleAvis.' : '.$count.(($denomination) ? ' - '.$denomination : ''),
                                                $message,
                                                '',
                                                '',
                                                false,
                                                $format,
                                                false,
                                                $pfName
                                            );

                                            $logger->info("Envoi mail {$email} ({$count} cons. pour {$denomination} ) ".(($ret) ? 'OK' : 'ERR'));
                                        }
                                        unset($enteteMailHtml);
                                        unset($nameFile);
                                        unset($footerHtmlDefinitif);
                                        unset($message);
                                        unset($alerte);
                                    }
                                    unset($alertes);
                                }
                                unset($arrayConsultation);
                            }
                        }
                    } catch (Exception $e) {
                        $logger->error('Erreur lors traitement alerte '.$row['xmlCriteria'].' :'.$e->getMessage().' '.$e->getTraceAsString());
                        Prado::log('Erreur lors traitement alerte '.$row['xmlCriteria'].' :'.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR, 'Atexo.Cli.AlerteEntreprises');
                    }
                    unset($tableauConsultationText);
                    unset($tableauConsultationHtml);
                    unset($criteriaVo);
                    unset($pfMailFrom);
                    unset($row);
                }
                $offset += $limit;
                $criteriaAlertes = (new Atexo_Entreprise_Alertes())->retreiveAlertesJoinInscrit($limit, $offset);
            }
        }
        if (!function_exists('memory_get_usage') || !function_exists('memory_get_peak_usage')) {
            $memoire = '';
        } else {
            $m1 = Atexo_Util::GetSizeName(memory_get_usage());
            $m2 = Atexo_Util::GetSizeName(memory_get_usage(true));
            $memoire = <<<EOF
Memoire:  $m1 (reelle:  $m2 ) - Peak: $m1 (reelle: $m2 )
EOF;
        }
        $time_end = microtime(true);
        $time = Atexo_Util::GetElapsedTime(round($time_end - $time_start, 4));
        $logger->info('Fin envoi des alertes in '.$time.' '.$memoire);
    }

    public static function nettoyerRepAlerte()
    {
        $dossierAlertes = Atexo_Config::getParameter('CHEMIN_REP_SENT_MAIL_ALERTE');
        if (!is_dir($dossierAlertes)) {
            mkdir($dossierAlertes, '0777');
        } else {
            if ($dh = opendir($dossierAlertes)) {
                $seconde_par_jours = (24 * 60 * 60);
                while (($file = readdir($dh)) != false) {
                    $date2 = date('Y-m-d H:i:s', filemtime($dossierAlertes.'/'.$file));
                    if (((strtotime(date('Y-m-d H:i:s')) - strtotime($date2)) / $seconde_par_jours) > 21) {
                        if (!is_dir($file)) {
                            unlink($dossierAlertes.'/'.$file);
                        }
                    }
                }
                closedir($dh);
            }
        }
    }

    public static function construireTableConsultationsAlertes()
    {
        $nbrCons = null;
        $logger = Atexo_LoggerManager::getLogger('alertesentreprise');

        try {
            $tableCreerEtRemplie = true;
            $sqlCreerTableDesConsultationsAlertes = <<<QUERY
DROP TABLE IF EXISTS consultation_alertes;
CREATE TABLE consultation_alertes LIKE consultation;
QUERY;
            $statement = Atexo_Db::getLinkCommon()->prepare($sqlCreerTableDesConsultationsAlertes);
            $res = $statement->execute();
            if ($res > 0) {
                $time_start = microtime(true);
                unset($statement);
                $logger->info('La table temporaire consultation_alertes a été créée');
                $sqlRemplirLaTableDesConsultationsAlertes = <<<QUERY
INSERT INTO consultation_alertes SELECT * FROM consultation where 
DATE_FIN_UNIX > unix_timestamp() 
 AND consultation.id_etat_consultation='0'
 AND date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' AND date_mise_en_ligne_calcule is not null
 AND consultation.type_acces='1'
 AND consultation.doublon='0'
 AND consultation_annulee='0';
QUERY;
                $statement = Atexo_Db::getLinkCommon()->prepare($sqlRemplirLaTableDesConsultationsAlertes);
                $res = $statement->execute();
                $time_end = microtime(true);
                $time = Atexo_Util::GetElapsedTime(round($time_end - $time_start, 4));

                if ($res <= 0) {
                    $logger->error("/!\ : La table temporaire consultation_alertes n'a pas été remplie");
                    $tableCreerEtRemplie = false;
                } else {
                    $logger->info('La table temporaire consultation_alertes a été remplie en '.$time);
                }

                unset($statement);
                $logger->info("Comptage du nbre d'entrée dans la table consultation_alertes");
                $sqlNbreConsultationsAlertes = <<<QUERY
SELECT COUNT(*) as NbCons FROM consultation_alertes;
QUERY;
                $statement = Atexo_Db::getLinkCommon()->prepare($sqlNbreConsultationsAlertes);
                $statement->setFetchMode(PDO::FETCH_ASSOC);
                $statement->execute();
                while ($row = $statement->fetchAll()) {
                    $nbrCons = $row[0]['NbCons'];
                }
                if ($nbrCons <= 0) {
                    $logger->error("/!\ : La table temporaire consultation_alertes ne contient aucune entrée");
                    $tableCreerEtRemplie = false;
                } else {
                    $logger->info('La table temporaire consultation_alertes contient '.$nbrCons.' consultations');
                }
            } else {
                $logger->error("/!\ : La table temporaire consultation_alertes n'a pas été créée");
                $tableCreerEtRemplie = false;
            }
        } catch (Exception $e) {
            $logger->error('Erreur lors du démarrage du traitement des alertes :'.$e->getMessage().' '.$e->getTraceAsString());
            Prado::log('Erreur lors du démarrage du traitement des alertes :'.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR, 'Atexo.Cli.AlerteEntreprises');
            $tableCreerEtRemplie = false;
        }

        return $tableCreerEtRemplie;
    }
}
