<?php

namespace Application\Cli;

use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneQuery;
use Application\Service\Atexo\Atexo_ArchivageExterne;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Exception;
use Prado\Prado;

/**
 * Cron de génération groupée et asynchrone des archives des consultations.
 *
 * @author Oumar KONATE (OKO) <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since 4.8.0
 */
class Cli_GenerationAsynchroneArchivesConsultations extends Atexo_Cli
{
    /**
     * Execution du cron de generation des telechargements.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function run()
    {
        $fileLog = Atexo_LoggerManager::getLogger('archivage');
        try {
            $fileLog->info("\nDebut lancement de la generation: ".date('Y-m-d H:i:s')."\n");
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $telechargementAsynchroneQuery = new CommonTTelechargementAsynchroneQuery(Atexo_Config::getParameter('COMMON_DB'));
            $telechargementAsynchrones = $telechargementAsynchroneQuery->recupererTelechargementsAsynchronesByTagGeneration('0');
            if ($telechargementAsynchrones instanceof PropelObjectCollection) {
                foreach ($telechargementAsynchrones as $telechargement) {
                    if ($telechargement instanceof CommonTTelechargementAsynchrone) {
                        $fileLog->info("\nDebut generation du telechargement numero ".$telechargement->getId()."\n\n");
                        $listePathsZipArchives = [];
                        $organisme = $telechargement->getOrganismeAgent();
                        self::startSession($organisme);
                        $fichiers = $telechargement->getCommonTTelechargementAsynchroneFichiers();
                        if ($fichiers instanceof PropelObjectCollection) {
                            foreach ($fichiers as $fichier) {
                                $fileLog->info('Generation du fichier archive de la consultation id = '.$fichier->getIdReferenceTelechargement()."\n");
                                if ($fichier instanceof CommonTTelechargementAsynchroneFichier) {
                                    $consultation = CommonConsultationPeer::retrieveByPK($fichier->getIdReferenceTelechargement(), $connexion);
                                    if(!$consultation instanceof CommonConsultation) {
                                        $consultation = (new CommonConsultationQuery())->findByReference($fichier->getIdReferenceTelechargement(), $connexion);
                                    }
                                    if($consultation instanceof CommonConsultation) {
                                        //cas 1 : récupération de l'archive
                                        self::recupererArchive($consultation, $listePathsZipArchives, $connexion, $fileLog);
                                        //cas 2 : si jamais le module archive par lot est activé, alors on construit une archive par lot
                                        if (Atexo_Module::isEnabled('ArchiveParLot') && $consultation->getAlloti()) {
                                            $fileLog->info('Consultation allotie '."\n");
                                            //Générer le zip archive pour chaque lot et mettre le chemin dans le tableau $listePathsZipArchives
                                            self::genererZipArchiveLots($consultation, $listePathsZipArchives, $connexion, $fileLog);
                                        }
                                    }
                                }
                            }
                        }
                        $fileLog->info("Construction du fichier qui va contenir les dossiers d'archives des consultations "."\n");
                        //Creer un zip contenant les archives generees
                        if (is_array($listePathsZipArchives) && count($listePathsZipArchives)) {
                            $dateGeneration = date('Y-m-d H:i:s');
                            $nomFichierGen = self::formaterNomFichier('Archive_'.$dateGeneration.'.zip');
                            //Creer le repertoire s'il n'existe pas
                            self::verifierEtCreerRepertoire(Atexo_Config::getParameter('FICHIERS_ARCHIVE_ASYNCHRONE_DIR'), $fileLog);
                            $repertoire = Atexo_Config::getParameter('FICHIERS_ARCHIVE_ASYNCHRONE_DIR').'/'.$organisme;
                            self::verifierEtCreerRepertoire($repertoire, $fileLog);
                            $cheminZipArchive = $repertoire.'/'.$telechargement->getId();
                            $fileLog->info("Ajout des dossiers d'archives dans le fichier: ".$nomFichierGen."\n");
                            foreach ($listePathsZipArchives as $path) {
                                (new Atexo_Zip())->addFile2Zip($cheminZipArchive, $path['pathFile'], $path['nameFile'], 'CREATE');
                                $fileLog->info("Ajout du dossier d'archive [".$path['nameFile'].'] --> OK');
                            }
                            $fileLog->info("Fin ajout des dossiers d'archives des consultations dans le fichier: ".$nomFichierGen."\n\n");
                            //Enregistrement nom du fichier, date de generation et tag generation
                            $telechargement->setDateGeneration($dateGeneration);
                            $telechargement->setNomFichierTelechargement($nomFichierGen);
                            $telechargement->setTailleFichier(Atexo_Util::getTailleFichier($cheminZipArchive, 'Ko', false));
                            $telechargement->setTagFichierGenere('1');
                            $telechargement->save($connexion);
                            $fileLog->info('Mise a jour des infos de telechargement en base de donnees --> OK');
                            //Envoi du mail
                            self::sendMailToAgent($telechargement->getEmailAgent(), $telechargement->getDateGeneration(), $telechargement->getOrganismeAgent(), $fileLog);
                            $fileLog->info("\nEnvoi mail de confirmation a l'agent --> OK");
                        }
                        $fileLog->info("\nFin generation du telechargement numero ".$telechargement->getId()."\n");
                    }
                }
            }
            $fileLog->info("\nFin de la generation: ".date('Y-m-d H:i:s')."\n\n");
        } catch (Exception $ex) {
            $message = "\nErreur lors de la generation des archives: ".print_r($ex->getMessage(), true);
            $fileLog->error($message."\n\n");
        }
    }

    /**
     * Permet de generer le fichier d'archive d'une consultation ou d'un lot.
     *
     * @param CommonConsultation $consultation:          objet consultation
     * @param array              $listePathsZipArchives: liste des chemins des archives
     * @param PropelPDO          $connexion:             objet connexion
     * @param string             $numLot:                numero du lot
     * @param Logger             $logger:                logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function genererZipArchive($consultation, &$listePathsZipArchives, $connexion, &$logger, $numLot = '0')
    {
        $tmpDir = Atexo_Config::getParameter('COMMON_TMP');
        $pathZip = '';
        $nomFichier = '';
        if ('0' != $numLot) {
            if ($consultation instanceof CommonConsultation) {
                $tree = new Atexo_Consultation_Tree_Arborescence($consultation->getId(), $connexion, $consultation->getOrganisme(), true, $numLot);
                $pathZip = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $consultation, $consultation->getOrganisme(), 'archive.xml', true, $numLot, $connexion);
                $nomFichier = $consultation->getReferenceUtilisateur().'_lot'.$numLot.'.zip';
            }
        } else {
            $tree = new Atexo_Consultation_Tree_Arborescence($consultation->getId(), $connexion, $consultation->getOrganisme(), true);
            $pathZip = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $consultation, $consultation->getOrganisme(), 'archive.xml', true, false, $connexion);
            $nomFichier = $consultation->getReferenceUtilisateur().'.zip';
        }
        $logger->info("\tGeneration du fichier d'archive pathFile : ".$pathZip.' et nameFile '.$nomFichier."\n");
        $listePathsZipArchives[] = ['pathFile' => $pathZip, 'nameFile' => $nomFichier];
    }

    /**
     * Permet de generer les fichiers d'archive des lots.
     *
     * @param CommonConsultation $consultation:          objet consultation
     * @param array              $listePathsZipArchives: liste des chemins des archives
     * @param PropelPDO          $connexion:             objet connexion
     * @param Logger             $logger:                logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function genererZipArchiveLots($consultation, &$listePathsZipArchives, $connexion, &$logger)
    {
        $lots = $consultation->getAllLots();
        foreach ($lots as $lot) {
            if ($lot instanceof CommonCategorieLot) {
                $logger->info("\tGeneration du fichier d'archive pour le lot numero : ".$lot->getLot()."\n");
                $getLot = $lot->getLot();
                self::genererZipArchive($consultation, $listePathsZipArchives, $connexion,  $logger, $getLot);
            }
        }
    }

    /**
     * Permet de récupérer les fichiers d'archive en fonction du fait qu'elle soit à distance ATLAS ou local.
     *
     * @param CommonConsultation $consultation:          objet consultation
     * @param array              $listePathsZipArchives: liste des chemins des archives
     * @param PropelPDO          $connexion:             objet connexion
     * @param Logger             $logger:                logger
     */
    public static function recupererArchive($consultation, &$listePathsZipArchives, $connexion, &$logger)
    {
        //on vérifie si l'archive est stockée dans la table consultation_archive, que la fragmentation a bien eu lieu et que son état de transmission est finalisée
        $consultationArchive = (new CommonConsultationArchiveQuery())
            ->filterByOrganisme($consultation->getOrganisme())
            ->filterByConsultationId($consultation->getId())
            ->findOne($connexion);

        //si jamais l'archive existe et il y a une entrée dans la table
        if ($consultationArchive instanceof CommonConsultationArchive) {
            $cheminFichierZip = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();

            //cas 1 : stockage des archives à distance de type ATLAS, on récupère l'archive à distance si la fragmentation a bien eu lieu et la transmission de tous les blocs finalisées
            if ($consultationArchive->getStatusFragmentation()
               && ($consultationArchive->getStatusGlobalTransmission() == Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_FINALISEE'))
               && !Atexo_ArchivageExterne::getDACFromAtlas($consultationArchive, $cheminFichierZip)
            ) {
                $logger->error('Erreur récupération DAC depuis ATLAS : fichier non complet');
                //la tentative de récupération des archives n'a pas fonctionné, dans ce cas on récupére l'archive en local
                $cheminFichierZip = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();
                $logger->info('La récupération du DAC va se faire en local à travers le chemin : '.$cheminFichierZip);
            }

            //si l'archive existe alors ok, on la retourne
            if (is_file($cheminFichierZip)) {
                $nomFichier = (new Atexo_Files())->getNomFileXmlAndZip($consultation, $consultation->getServiceId()).'.zip';
                $listePathsZipArchives[] = ['pathFile' => $cheminFichierZip, 'nameFile' => $nomFichier];
            } //l'archive n'existe pas, alors on tente de la re-générer
            else {
                $logger->error('La récupération du DAC en local est en échec à travers le chemin : '.$cheminFichierZip);
                $logger->info('Le DAC va être regénéré pour la consultation ');
                self::genererZipArchive($consultation, $listePathsZipArchives, $connexion,  $logger);
            }
        }
        //dans ce cas l'archive n'existe pas et il faut la re-générer, id_etat_consultation 5
        else {
            $logger->info("Il n'y a pas d'entree pour l'archive dans consultation archive, le statut de la consultation est : ".$consultation->getIdEtatConsultation());
            $logger->info('Le DAC va être regénéré pour la consultation ');

            self::genererZipArchive($consultation, $listePathsZipArchives, $connexion, $logger);
        }
    }

    /**
     * Permet d'initialiser une session.
     *
     * @param string $organisme: organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function startSession($organisme)
    {
        $session = [
           0 => '',
           1 => [],
           2 => '',
           3 => '',
           4 => '',
           5 => '',
           6 => '',
           7 => $organisme,
           8 => '',
           9 => '',
           10 => '',
           11 => '',
           12 => '',
           13 => [],
           14 => '',
           15 => '',
           16 => '',
           17 => '',
           18 => '',
           19 => '',
           20 => '',
           21 => '',
           22 => '',
           23 => '',
           24 => '',
           25 => '',
           26 => '',
           27 => '',
           28 => '',
           29 => '',
           30 => '',
           31 => '',
           32 => '',
           33 => '',
           34 => '',
           35 => '',
           36 => '',
           37 => '',
           38 => '',
           39 => '',
           40 => '',
           41 => '',
           42 => '',
           43 => '',
           44 => '',
           45 => '',
           46 => '',
           47 => '',
           48 => '',
           49 => '',
           50 => '',
           51 => '',
           52 => '',
           53 => '',
           54 => '',
           55 => '',
           56 => '',
           57 => '',
           58 => '',
           59 => '',
           60 => '',
           61 => '',
           62 => '',
           63 => '',
           64 => '',
           65 => '',
        ];
        session_id(uniqid());
        $_SESSION[session_id()] = serialize($session);
    }

    /**
     * Permet de formater les noms des fichiers.
     *
     * @param string $fileName: le nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function formaterNomFichier($texte)
    {
        $texte = str_replace(' ', '_', $texte);
        $texte = str_replace('-', '_', $texte);
        $texte = str_replace(':', '_', $texte);

        return $texte;
    }

    /**
     * Permet de verifier si le repertoire existe et le creer le cas echeant.
     *
     * @param string $repertoire: le repertoire a creer
     * @param Logger $logger:     logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function verifierEtCreerRepertoire($repertoire, &$logger)
    {
        try {
            if (!is_dir($repertoire)) {
                $logger->info('Creation du repertoire: ['.$repertoire.'] ');
                $return = Atexo_Util::createDirectory($repertoire);
                if ($return) {
                    $logger->info(': OK'."\n");
                }
            }
        } catch (Exception $e) {
            $message = 'Erreur lors de la creation du repertoire: ['.$repertoire.'] :'.$e->getMessage()."\n";
            $logger->error($message);
        }
    }

    /**
     * Permet d'envoyer un email apres la generation des archives.
     *
     * @param string $email:              email de l'agent
     * @param string $dateTelechargement: date de telechargement
     * @param string $org:                organisme
     * @param Logger $logger:             Logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function sendMailToAgent($email, $dateTelechargement, $org, &$logger)
    {
        $pfUrl = Atexo_MultiDomaine::getPfUrl(
            $org,
            true,
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );

        try {
            $corps = Prado::localize('TEXT_CORPS_MESSAGE_MAIL_CONFIRMATION_GENERATION_ASYNCHRONE_ARCHIVES');
            $objet = Prado::localize('TEXT_OBJET_MESSAGE_MAIL_CONFIRMATION_GENERATION_ASYNCHRONE_ARCHIVES');
            [$a, $m, $j_et_heure] = explode('-', $dateTelechargement);
            [$j, $heure] = explode(' ', $j_et_heure);
            [$h, $min, $sec] = explode(':', $heure);
            $dateTelechargement = "$j/$m/$a";
            $heureTelechargement = "$h:$min:$sec";
            $corps = str_replace('[__DATE_TELECHARGEMENT__]', $dateTelechargement, $corps);
            $corps = str_replace('[__HEURE_TELECHARGEMENT__]', $heureTelechargement, $corps);
            $lienTelechargement = $pfUrl.'?page=Agent.MesTelechargements';
            $lienProfond = '<a href="'.$lienTelechargement.'">'.Prado::localize('DEFINE_MES_TELECHARGEMENTS').'</a>';
            $corps = str_replace('[__LIEN_PROFOND_MENU_TELECHARGEMENT__]', $lienProfond, $corps);
            $corps = str_replace('[__ALLER_A_LA_LIGNE__]', '<br/><br/>', $corps);
            $corps = str_replace('[__URL_PF__]', $pfUrl, $corps);
            /*
             * @todo:  voir le PF mail from
             */
            //Envoi du mail
            Atexo_Message::simpleMail(Atexo_Util::toUtf8(Atexo_Config::getParameter('PF_MAIL_FROM', $org)), $email, $objet, $corps, '', '', false, true);
        } catch (Exception $ex) {
            $message = "Erreur lors de l'envoi du mail de confirmation a l'agent [".$email.'] :'.$ex->getMessage()."\n";
            $logger->error($message);
        }
    }
}
