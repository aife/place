<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusNomsFichiers;
use Application\Propel\Mpe\CommonChorusNomsFichiersPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_SendFiles;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Reception;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Transmission;
use DOMDocument;

/**
 * Classe de Test de chorus (Transmission et réception).
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_TestChorus extends Atexo_Cli
{
    public static function run()
    {
        echo "\r\n\r\n";

        if (!is_dir('/tmp/TestChorus/')) {
            mkdir('/tmp/TestChorus/');
        }

        $idEchange = null;
        $type = null;
        $organisme = null;
        $xmlFileName = null;

        for ($i = 0; $i < $_SERVER['argc']; ++$i) {
            if ('transmission' == $_SERVER['argv'][$i]) {
                $type = 1;
            }
            if ('reception_CEN' == $_SERVER['argv'][$i]) {
                $type = 2;
            }
            if ('reception_FSO' == $_SERVER['argv'][$i]) {
                $type = 3;
            }
            if ('createFiles' == $_SERVER['argv'][$i]) {
                $type = 4;
            }
            if ('send' == $_SERVER['argv'][$i]) {
                $type = 5;
            }
            if ('sqlScript' == $_SERVER['argv'][$i]) {
                $type = 6;
            }
            if ('reception_CIR' == $_SERVER['argv'][$i]) {
                $type = 7;
            }
            if ('reception_FIR' == $_SERVER['argv'][$i]) {
                $type = 8;
            }

            if (strstr($_SERVER['argv'][$i], 'xmlFileName=')) {
                $donnees = explode('=', $_SERVER['argv'][$i]);
                $xmlFileName = $donnees[1];
            }

            if (strstr($_SERVER['argv'][$i], 'organisme=')) {
                $donnees = explode('=', $_SERVER['argv'][$i]);
                $organisme = $donnees[1];
            }
            if (strstr($_SERVER['argv'][$i], 'echange=')) {
                $donnees = explode('=', $_SERVER['argv'][$i]);
                $idEchange = $donnees[1];
            }
        }

        if (!$type) {
            echo "Paramètre transmission/send/reception_CEN/reception_FSO/createFiles/reception_CIR/reception_FIR non précisé\r\n";

            return;
        }

        if (1 == $type && !$organisme) {
            echo "Organisme inconnu. Veuillez indiquer : organisme=NOM_ORGANISME\r\n";

            return;
        }

        if (1 != $type && 6 != $type && !$xmlFileName) {
            echo "Nom du fichier xml de reception inconnu. Veuillez indiquer son nom : xmlFileName=XML_FILE_NAME. PS : mettre dans repertoire /tmp/TestChorus/ \r\n";

            return;
        }

        if (1 == $type) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($idEchange) {
                $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $organisme, $connexion);
                $sendFicheNavette = (new Atexo_Chorus_Echange())->ficheNavetteNotDuplicated($echange);
                $idEjAppliExtValue = null;
                $xml = (new Atexo_Chorus_Transmission())->createFluxFen($echange, $organisme, $idEjAppliExtValue, $sendFicheNavette);
                $i = 1;
                do {
                    $fileName = 'TestChorus'.$i.'.xml';
                    ++$i;
                } while (is_file('/tmp/TestChorus/'.$fileName));

                Atexo_Util::write_file('/tmp/TestChorus/'.$fileName, $xml);
            } else {
                echo "Veuillez Choisir un échange  en indiquant : echange=ID_ECHANGE \r\n";
                $c = new Criteria();
                $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
                $echanges = CommonChorusEchangePeer::doSelect($c, $connexion);
                echo "-- Id  -- Decision -- Nom createur --  Prenom  -- date création\r\n";
                if (is_array($echanges)) {
                    foreach ($echanges as $echange) {
                        echo '-- '.$echange->getId().' --      '.$echange->getIdDecision().'   -- '.$echange->getNomCreateur().' -- '.$echange->getPrenomCreateur().' -- '.$echange->getDateCreation()."\r\n";
                    }
                }
            }
        } elseif (5 == $type) {
            if (is_file('/tmp/TestChorus/'.$xmlFileName)) {
                $result = Atexo_SendFiles::sendFile('/tmp/TestChorus/'.$xmlFileName, Atexo_Config::getParameter('URL_ACCES_CHORUS'), true, Atexo_Config::getParameter('CHORUS_SSLCERT'), Atexo_Config::getParameter('CHORUS_SSLCERTPASSWD'));
                if ($result) {
                    echo 'le fichier a bien ete envoye';
                    system('mv '.$xmlFileName.' '.Atexo_Config::getParameter('COMMON_TMP'));
                } else {
                    echo "le fichier n'a pas été envoyé";
                }
            } else {
                echo "Xml de reception $xmlFileName non trouvé dans /tmp/TestChorus/ \r\n";
            }
        } elseif (2 == $type) {
            if (is_file('/tmp/TestChorus/'.$xmlFileName)) {
                $xml = file_get_contents('/tmp/TestChorus/'.$xmlFileName);

                (new Atexo_Chorus_Reception())->receptionCompteRendu($xml, $xmlFileName);
            } else {
                echo "Xml de reception $xmlFileName non trouvé dans /tmp/TestChorus/ \r\n";
            }
        } elseif (3 == $type) {
            if (is_file('/tmp/TestChorus/'.$xmlFileName)) {
                $xml = file_get_contents('/tmp/TestChorus/'.$xmlFileName);

                (new Atexo_Chorus_Reception())->receptionMessageSuivi($xml, $xmlFileName);
            } else {
                echo "Xml de reception $xmlFileName non trouvé dans /tmp/TestChorus/ \r\n";
            }
        } elseif (4 == $type) {
            $dir = '/tmp/TestChorus/'.substr($xmlFileName, 0, -4);
            if (is_dir($dir)) {
                system('rm -rf /tmp/TestChorus/'.substr($xmlFileName, 0, -4));
            }
            mkdir($dir);

            $xml = file_get_contents('/tmp/TestChorus/'.$xmlFileName);
            $xml = str_replace('ej:', '', $xml);

            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);

            $engagementJuridique = $domDocument->getElementsByTagName('EngagementJuridique')->item(0);

            $pjs = $engagementJuridique->getElementsByTagName('PJ');
            $i = 1;
            foreach ($pjs as $pj) {
                $type = $pj->getElementsByTagName('Type')->item(0);
                $description = $pj->getElementsByTagName('Description')->item(0);
                if ($description->nodeValue && $type->nodeValue) {
                    $nomFichier = $description->nodeValue.'.'.$type->nodeValue.'.zip';
                    $data = $pj->getElementsByTagName('Data')->item(0);
                    $i = 1;
                    if (is_file($dir.'/'.$nomFichier)) {
                        do {
                            $nomFichier = substr($nomFichier, 0, -3).$i.'.zip';
                        } while (is_file($dir.'/'.$nomFichier));
                    }

                    $fp = fopen($dir.'/'.$nomFichier, 'w');
                    fwrite($fp, base64_decode($data->nodeValue));
                    fclose($fp);
                }
            }
        } elseif (6 == $type) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $organismes = CommonOrganismePeer::doSelect(new Criteria(), $connexion);
            if (is_array($organismes)) {
                foreach ($organismes as $organisme) {
                    $c = new Criteria();
                    $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
                    $chorusEchanges = CommonChorusEchangePeer::doSelect($c, $connexion);

                    if (is_array($chorusEchanges)) {
                        foreach ($chorusEchanges as $chorusEchange) {
                            $c = new Criteria();
                            $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, $chorusEchange->getNomFichier());
                            $chorusNomFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $connexion);
                            if (!($chorusNomFichier instanceof CommonChorusNomsFichiers)) {
                                $chorusNomFichier = new CommonChorusNomsFichiers();
                                $chorusNomFichier->setIdEchange($chorusEchange->getId());
                                $chorusNomFichier->setNomFichier($chorusEchange->getNomFichier());
                                $chorusNomFichier->setAcronymeOrganisme($organisme->getAcronyme());
                                $chorusNomFichier->save($connexion);
                            }
                        }
                    }
                }
            }
        } elseif (7 == $type) {
            if (is_file('/tmp/TestChorus/'.$xmlFileName)) {
                $xml = file_get_contents('/tmp/TestChorus/'.$xmlFileName);
                (new Atexo_Chorus_Reception())->receptionCompteRenduCIR($xml, $xmlFileName);
            } else {
                echo "Xml de reception $xmlFileName non trouvé dans /tmp/TestChorus/ \r\n";
            }
        } elseif (8 == $type) {
            if (is_file('/tmp/TestChorus/'.$xmlFileName)) {
                $xml = file_get_contents('/tmp/TestChorus/'.$xmlFileName);
                (new Atexo_Chorus_Reception())->receptionCompteRenduFIR($xml, $xmlFileName);
            } else {
                echo "Xml de reception $xmlFileName non trouvé dans /tmp/TestChorus/ \r\n";
            }
        }
    }
}
