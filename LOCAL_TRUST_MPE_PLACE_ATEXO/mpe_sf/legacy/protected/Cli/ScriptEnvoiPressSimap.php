<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdresseFacturationJal;
use Application\Propel\Mpe\CommonAdresseFacturationJalPeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonAvisPubPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonDestinatairePubPeer;
use Application\Propel\Mpe\Om\BaseCommonAdresseFacturationJalPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Exception;
use Prado\Prado;

/**
 * publie les avis press pour simap MPE-.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since 2016-simap
 *
 * @category Atexo
 */
class Cli_ScriptEnvoiPressSimap extends Atexo_Cli
{
    public static function run()
    {
        $consultationId = null;
        $org = null;
        $arguments = $_SERVER['argv'];
        for ($i = 2; $i < 4; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('ref' == $arg[0]) {
                $consultationId = $arg[1];
            }
            if ('org' == $arg[0]) {
                $org = $arg[1];
            }
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ID, $consultationId);
        $c->add(CommonConsultationPeer::ORGANISME, $org);
        $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($consultation instanceof CommonConsultation) {
            echo 'Debut de traitement pour consultation reference = '.$consultationId.' organisme = '.$org;
            $c = new Criteria();
            $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonAvisPubPeer::ORGANISME, $org);
            $listeAvis = CommonAvisPubPeer::doSelect($c, $connexion);
            if (is_array($listeAvis) && count($listeAvis)) {
                foreach ($listeAvis as $avis) {
                    try {
                        if ($avis instanceof CommonAvisPub) {
                            $criteria = new Criteria();
                            $criteria->add(CommonDestinatairePubPeer::ID_AVIS, $avis->getId());
                            $criteria->add(CommonDestinatairePubPeer::ORGANISME, $org);
                            $criteria->add(CommonDestinatairePubPeer::ID_SUPPORT, Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE'));
                            $destinatairePresse = CommonDestinatairePubPeer::doSelectOne($criteria, $connexion);
                            if ($destinatairePresse instanceof CommonDestinatairePub) {
                                self::SendToPress($avis, $consultation, $destinatairePresse, $connexion);
                                echo "\n Fin du traitement\n";
                            }
                        }
                    } catch (Exception $e) {
                        $log = "\n\n";
                        $log .= "____________________________________\n";
                        $log .= "\n\n Envoi des avis à OPOCE, PRESSE, SIMAP2 <br/>";
                        $log .= "\n\n";
                        $log .= "____________________________________\n";
                        $log .= $e->getMessage();
                        //Envoie de mail
                        echo $log;
                    }
                }
            }
        } else {
            echo 'Aucune consultation trouvée avec reference = '.$consultationId.' organisme = '.$org;
        }
    }

    /**
     * Envoi à la presse.
     **/
    public function SendToPress($avis, $consultation, $destinataire, $connexionCom)
    {
        $libelleServiceAgentConnecte = null;
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $cc = Atexo_Config::getParameter('MAIL_PRODUCTION');
        $taille = 0;
        $validePJ = false;
        $envoiMail = false;
        $critere = new Criteria();

        //Début regénération de l'avis presse à afficher dans le mail
        $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avis->getTypeAvis());
        if ($modele) {
            $pieceJointe = (new Atexo_Publicite_AvisPub())->CommonAnnoncePressPieceJointeByIdAndOrganisme($avis->getIdAvisPresse(), $avis->getOrganisme(), $connexionCom);
            if ($pieceJointe instanceof CommonAnnoncePressPieceJointe) {
                $tableauContenuGenerationDocPresse = (new Atexo_Publicite_AvisPub())->genererAvisPressePortail($modele, $connexionCom, $avis->getId(), $consultation, 'fr', false, date('Y-m-d H:i:s'));
                $pieceJointe->setTaille(filesize($tableauContenuGenerationDocPresse[1]));
                $pieceJointe->setPiece($tableauContenuGenerationDocPresse[0]);
                $pieceJointe->save($connexionCom);
            }
        }
        //Fin regénération de l'avis presse à afficher dans le mail

        if ($destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
            $annoncePress = \Atexo_AnnoncePress_Press::getAnnoncePressByIdDest($destinataire->getId(), $destinataire->getOrganisme());
            if ($annoncePress) {
                $critere->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $annoncePress->getOrganisme());
                $critere->add(CommonAnnoncePressPieceJointePeer::ID_ANNONCE_PRESS, $annoncePress->getId());
                $PJs = $annoncePress->getCommonAnnoncePressPieceJointes($critere, $connexionCom);
                if (is_array($PJs) && 0 != count($PJs)) {
                    $validePJ = true;
                }
                $dossier = '';
                $zipPj = (new Atexo_Message())->getZipPj($PJs, $taille, $annoncePress->getOrganisme(), $dossier);
                $taille = str_replace('.', ',', round($taille / (1_048_985.64)));
                //Ajout du logo au zip
                if ($avis->getIdBlobLogo()) {
                    $atexoBlob = new Atexo_Blob();
                    $blobContent = $atexoBlob->return_blob_to_client($avis->getIdBlobLogo(), $avis->getOrganisme());
                    //On recupère l'adresse de facturation
                    $adresseFact = BaseCommonAdresseFacturationJalPeer::retrieveByPK($annoncePress->getIdAdresseFacturation(), $annoncePress->getOrganisme(), $connexionCom);
                    $fileNameLogo = '';
                    if ($adresseFact instanceof CommonAdresseFacturationJal) {
                        $fileNameLogo = $adresseFact->getNomFichier();
                    }
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP').'/'.$fileNameLogo;
                    $fp = fopen($tmpFile, 'w');
                    fwrite($fp, $blobContent);
                    fclose($fp);
                    $taille = $taille + filesize($tmpFile);
                    (new Atexo_Zip())->addFileToZip($tmpFile, $zipPj);
                }
                // TODO test sur lecture du fichier
                if ($zipPj) {
                    $code = (new Atexo_Files())->read_file($zipPj);
                }
                $fileName = $consultation->getReferenceUtilisateur().'_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');

                //Début recupération des informations du mail
                //$organePresse = "";
                $newTextMail = '';
                $infoFacturation = '';
                $meilleuresSalutations = '';
                $infosOrganisme = '';
                $valeurSip = false;
                if ($annoncePress->getIdAdresseFacturation()) {
                    $adresseFacturation = CommonAdresseFacturationJalPeer::retrieveByPK($annoncePress->getIdAdresseFacturation(), $annoncePress->getOrganisme(), $connexionCom);
                    if ($adresseFacturation instanceof CommonAdresseFacturationJal) {
                        $infoFacturation .= Prado::localize('DEFINE_COMMUNICATION_UTILISER_FACTURATION').' :
';
                        $dateMiseEnLigne = ($consultation->getDateMiseEnLigneCalcule() && '0000-00-00 00:00:00' != $consultation->getDateMiseEnLigneCalcule()) ? Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule(), false) : date('d/m/Y');
                        $infoFacturation .= '<< '.str_replace('[__ref__cons__]', $consultation->getReferenceUtilisateur(), str_replace('[__date_mise_ligne__]', $dateMiseEnLigne, Prado::localize('DEFINE_CONTENU_COMMUNICATION_UTILISER_FACTURATION'))).' :
'.$consultation->getIntituleTraduit().' >>'.'

';
                        $infoFacturation .= Prado::localize('DEFINE_TEXT_PARVENIR_ADRESSE_SUIVANTE').' : '.'
';
                        $infoFacturation .= $adresseFacturation->getInformationFacturation().'

';

                        $logoAPublier = ($avis->getIdBlobLogo()) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
                        $newTextMail .= Prado::localize('TEXT_LOGO_A_PUBLIER').' : '.$logoAPublier.'

';
                        $pouvoirsAdjudicateurs = '';
                        $newTextMail .= Prado::localize('DEFINE_TEXT_CAS_PROBLEME_CONTACTER').'
';
                        //Début agent Créateur consultation
                        $agentCreateurAvis = (new Atexo_Agent())->retrieveAgent($consultation->getIdCreateur());
                        $email = '';
                        $fax = '';
                        $tel = '';
                        $nomPrenom = '';
                        if ($agentCreateurAvis instanceof CommonAgent) {
                            $email = $agentCreateurAvis->getEmail();
                            $fax = $agentCreateurAvis->getNumFax();
                            $tel = $agentCreateurAvis->getNumTel();
                            $nomPrenom = $agentCreateurAvis->getNom().' '.$agentCreateurAvis->getPrenom();
                        }
                        //Fin agent Créateur consultation
                        //Début détermination des pouvoirs adjudicateurs
                        $serviceConsultation = $consultation->getServiceId();
                        $libellePereServiceAgentConnecte = '';
                        $libellePremierFilsServiceAgentConnecte = '';
                        $libelleDivision = '';
                        if (0 != $serviceConsultation) {
                            $listeParents = Atexo_EntityPurchase::getArParentsId($serviceConsultation, $consultation->getOrganisme(), false);
                            if (is_array($listeParents) && count($listeParents) > 0 && $listeParents[0] === 0) {
                                $listeParents[0] = null;
                            }
                            if (1 == count($listeParents)) {//Cas n=2, on doit voir seulement le service de l'agent connecté
                                $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[1], $consultation->getOrganisme());
                                $libelleDivision = $libelleServiceParentDeAgentConnecte;
                            } elseif (2 == count($listeParents)) {//Cas n=3, on doit voir le service de l'agent connecté et le père
                                $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[0], $consultation->getOrganisme());
                                $libelleDivision = $libelleServiceParentDeAgentConnecte;
                            } elseif (count($listeParents) >= 3) {//Cas n>3, on doit voir le service de l'agent connecté et les deux premiers parents de l'arborescence
                                $libellePereServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 2], $consultation->getOrganisme());
                                $libellePremierFilsServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 3], $consultation->getOrganisme());
                                $libelleDivision = $libellePremierFilsServiceAgentConnecte;
                            }
                            $libelleServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($serviceConsultation, $consultation->getOrganisme());
                        }
                        //Fin détermination des pouvoirs adjudicateurs
                        $pouvoirsAdjudicateurs .= $nomPrenom.'
';
                        if ($libellePereServiceAgentConnecte) {
                            $pouvoirsAdjudicateurs .= $libellePereServiceAgentConnecte.'
';
                        }
                        if ($libelleDivision) {
                            $pouvoirsAdjudicateurs .= $libelleDivision.'
';
                        }
                        if ($libelleServiceAgentConnecte) {
                            $pouvoirsAdjudicateurs .= $libelleServiceAgentConnecte.'
';
                        }
                        if ($email) {
                            $pouvoirsAdjudicateurs .= Prado::localize('EMAIL').' : '.$email.'
';
                        }
                        if ($tel) {
                            $pouvoirsAdjudicateurs .= Prado::localize('DEFINE_TEL').' : '.$tel.'
';
                        }
                        if ($fax) {
                            $pouvoirsAdjudicateurs .= Prado::localize('TEXT_FAX').' : '.$fax.'
';
                        }
                        $newTextMail .= $pouvoirsAdjudicateurs.'
';
                        if ($adresseFacturation->getFacturationSip()) {
                            $valeurSip = true;
                        }
                    }
                    $meilleuresSalutations .= '
';
                    $meilleuresSalutations .= Prado::localize('CORDIALEMENT').'.

';
                    $infosOrganisme .= Prado::localize('BAS_PAGE_MAIL').'
';
                }
                //Fin recupération des informations du mail
                //recuperation de la liste des destinataires annonce press
                $listDestAnnoncePress = \Atexo_AnnoncePress_Press::getListeDestAnnoncePress($annoncePress->getId(), $annoncePress->getOrganisme(), true);
                if (is_array($listDestAnnoncePress) && count($listDestAnnoncePress)) {
                    foreach ($listDestAnnoncePress as $listDestFromJal) {
                        //$listDestFromJal = Atexo_Publicite_AnnonceJAL::getEmailByIdJAl($oneDest->getId(),$oneDest->getOrganisme());
                        //if($listDestFromJal){
                        //$infosComplementairesObjet = $consultation->getReference().'/'.$avis->getId().'/'.$destinataire->getId().' '.$listDestFromJal->getNom();
                        $infosObjet = '';
                        $infosObjet = str_replace('[organes_de_presse_selectionnees]', $listDestFromJal->getNom(), $annoncePress->getObjet());
                        $infoOrganePresse = Prado::localize('ORGANE_PRESSE').' : '.$listDestFromJal->getNom().'

';
                        $corpsMessage = nl2br($annoncePress->getTexte()).'
';
                        $corpsMessage .= $infoOrganePresse;
                        $corpsMessage .= $newTextMail;
                        //if($valeurSip) {
                        $corpsMessage .= $infoFacturation;
                        //}
                        $corpsMessage .= $meilleuresSalutations;
                        $corpsMessage .= $infosOrganisme;
                        $corpsMessage = strip_tags($corpsMessage);
                        echo "\n Préparation d'envoi du mail à ".$listDestFromJal->getEmail();
                        if ($validePJ) {
                            $envoiMail = (new Atexo_Message())->mailWithAttachement($from, $listDestFromJal->getEmail(), $infosObjet, $corpsMessage, $cc, $code, $fileName, '');
                        } else {
                            $envoiMail = Atexo_Message::simpleMail($from, $listDestFromJal->getEmail(), $infosObjet, $corpsMessage, $cc);
                        }
                        if ($envoiMail) {
                            echo "\n Mail envoyé à ".$listDestFromJal->getEmail();
                        }
                    }
                    if (is_file($zipPj)) {
                        if (!unlink($zipPj)) {
                            echo 'Impossible de supprimer le fichier : '.$zipPj;
                        }
                        Atexo_Util::removeDirectoryRecursively($dossier);
                    }
                }
            }
        }
    }
}
