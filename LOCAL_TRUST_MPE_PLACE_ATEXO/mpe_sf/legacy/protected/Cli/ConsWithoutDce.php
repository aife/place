<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Prado\Prado;

class Cli_ConsWithoutDce extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter en param le mail: \r\n";
        } else {
            $email = $arguments[2];
            $arrayConsWithoutDCE = [];
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setDateFinStart(date('d/m/Y'));
            $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
            $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
            $criteriaVo->setcalledFromPortail(true);
            $consultationHandler = new Atexo_Consultation();
            $arrayCons = $consultationHandler->search($criteriaVo, false);
            if (is_array($arrayCons) && count($arrayCons)) {
                $htmlMessage = '';
                //$message = "\n" . Prado::localize('TEXT_ENTETE_NOUVELLE_CONSULTATION') . "\n";
                $atexoMessage = new Atexo_Message();
                $rawTemplate = $atexoMessage->render('PATH_FILE_MAIL_CONSULTATION_WITHOUT_DCE');
                foreach ($arrayCons as $consultation) {
                    if ($consultation instanceof CommonConsultation) {
                        $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $consultation->getOrganisme());
                        if (!($dce instanceof CommonDCE)) {
                            $arrayConsWithoutDCE[] = $consultation;
                            $corpsMail = $rawTemplate;
                            //$cartoucheEntreprise = Atexo_Message::getInfoConsultationFromObject($consultation);
                            $cartoucheEntreprise = (new Atexo_Message())->_getInfoConsultationFromObject($consultation, false, true, $corpsMail);
                            //$message .= "\n\n----------------------------------------------------------------------------------------------------------\n\n";
                            $htmlMessage .= '<hr>';
                            $htmlMessage .= $cartoucheEntreprise;
                        }
                    }
                }
                $corpsMail = str_replace('TEXT_ENTETE_NOUVELLE_CONSULTATION', Prado::localize('TEXT_ENTETE_NOUVELLE_CONSULTATION'), $htmlMessage);
                $corpsMail = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $corpsMail);
                $corpsMail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corpsMail);

                $pfMailFrom = Atexo_Config::getParameter('PF_MAIL_FROM');
                Atexo_Message::simpleMail($pfMailFrom, $email, 'Consultation sans DCE  : '.count($arrayConsWithoutDCE).'/'.count($arrayCons), $htmlMessage, '', '', false, true);
            }
        }
    }
}
