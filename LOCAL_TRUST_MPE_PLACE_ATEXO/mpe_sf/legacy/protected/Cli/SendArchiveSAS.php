<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Exception;

/**
 * Envoi des DAC vers le SAS MEGALIS.
 *
 * @author Mohamed Wazni  <mwa@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Cli_SendArchiveSAS extends Atexo_Cli
{
    public static function run()
    {
        $code = null;
        try {
            echo "\n\n-------- Debut Archivage Externe dans le SAS -------\n\n";
            $dateActuelle = date('Y-m-d');
            $file = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_DAC_A_TRANSMETTRE');
            $fileArchiveEnvoi = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_DAC_TRANSMIS').$dateActuelle.'.csv';
            $fileNonTransmis = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_ENVOI_ERREUR');
            $fileTmp = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_STATUT_ENVOI').'/tmp_'.$dateActuelle.'.csv';
            echo $file."\n";
            if (is_file($file)) {
                if ($handle = fopen($file, 'r')) {
                    $ligne = 0;
                    while (($LigneCvs = fgets($handle, 10000)) !== false) {
                        $uneLigneCvs = explode('|', $LigneCvs);
                        $statut = 0;
                        // on appelle la methode que va envoyer les données vers la platforme externe
                        if ('' != $uneLigneCvs[0] && 'OK' != $uneLigneCvs[4]) {
                            //$identifiant = $uneLigneCvs[0]
                            //$acronyme  = $uneLigneCvs[1]
                            //$date_generation_archive = $uneLigneCvs[2]
                            //$chemin_archive = $uneLigneCvs[3]
                            //copier les archives dans le SAS qui sert de zone de tampon
                            system('cp '.escapeshellarg($uneLigneCvs[3]).' '.escapeshellarg(Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_STOCKAGE_SAS_ARCHIVE')), $statut);
                            //copier l'empreinte md5 dans le SAS qui sert de zone de tampon
                            if (!$statut) {
                                echo 'Déplacement du fichier archive dans le SAS : ok '.$uneLigneCvs[3].' '.escapeshellarg(Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_STOCKAGE_SAS_ARCHIVE'))."\n";
                                system('cp '.escapeshellarg($uneLigneCvs[3].'.md5').' '.escapeshellarg(Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_STOCKAGE_SAS_ARCHIVE')), $statut);
                            }
                            // copier les données dans un autre fichier avec la valeur de statut envoi et la date d'envoi
                            if (!$statut) {
                                echo 'Déplacement du fichier md5 le SAS : ok '.$uneLigneCvs[3].'.md5'.' '.escapeshellarg(Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR_STOCKAGE_SAS_ARCHIVE'))."\n";
                                $data = $uneLigneCvs[0].'|'.$uneLigneCvs[1].'|'.$uneLigneCvs[2].'|'.$uneLigneCvs[3].'|OK|'.date('Y-m-d H:i:s')."\n";
                                Atexo_Util::writeFile($fileArchiveEnvoi, $data, 'a+');
                            } else {
                                $dataErreur = $uneLigneCvs[0].'|'.$uneLigneCvs[1].'|'.$uneLigneCvs[2].'|'.$uneLigneCvs[3].'|NON|'.$code.'|'.date('Y-m-d H:i:s')."\n";
                                $data = $uneLigneCvs[0].'|'.$uneLigneCvs[1].'|'.$uneLigneCvs[2].'|'.$uneLigneCvs[3].'|NON|'.date('Y-m-d H:i:s')."\n";
                                Atexo_Util::writeFile($fileNonTransmis, $dataErreur, 'a+');
                                Atexo_Util::writeFile($fileTmp, $data, 'a+');
                            }
                        }
                        ++$ligne;
                    }
                    fclose($handle);
                }
            }

            //          if(is_file($file)){
            //              system("mv " . $file . " " . $file. "old");
            //          }
            if (is_file($fileTmp)) {
                system('mv '.$fileTmp.' '.$file);
            } else {
                Atexo_Util::writeFile($file, '');
            }
            // Envoi du mail avec pieces jointes
            $zipFileNameTmp = Atexo_Config::getParameter('COMMON_TMP').'/'.'log_archive_'.date('Ymdhis').'_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
            // ajout des fichiers dans le ZIP
            (new Atexo_Zip())->addFileToZip($file, $zipFileNameTmp);
            (new Atexo_Zip())->addFileToZip($fileArchiveEnvoi, $zipFileNameTmp);
            (new Atexo_Zip())->addFileToZip($fileNonTransmis, $zipFileNameTmp);
            $code = file_get_contents($zipFileNameTmp);
            $to = Atexo_Config::getParameter('PF_MAIL_ARCHIVAGE_TO');
            $from = Atexo_Config::getParameter('PF_MAIL_FROM');
            $zipName = 'log_archive_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
            // envoi du mail
            (new Atexo_Message())->mailWithAttachement($from, $to, 'Rapport de transmission des Archives dans le SAS', '', '', $code, $zipName, '');
            echo "-------- Fin Archivage Externe -------\n\n";
        } catch (Exception $e) {
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }
}
