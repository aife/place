<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\AnnoncesMarches\Atexo_AnnoncesMarches_Consultation;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use DOMDocument;
use Exception;

/**
 * Cron de mise à  jour des consultations du portail Annonces-Marches via webServices.
 * Si l'acronyme d'un organisme partenaire est passé en param on récupere les cons de ce dernier selon ces deliveryPlace.
 *
 * @category Atexo
 */
class Cli_AnnoncesMarches extends Atexo_Cli
{
    public static function run()
    {
        try {
            $tender = new Atexo_AnnoncesMarches_Consultation();
            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                    $argument = explode('=', $arguments[$i]);
                    $_GET[$argument[0]] = $argument[1] ?: true;
                }
            }
            $acronyme = $_SERVER['argv'][2];
            // Appel des consultations partenaires d'un organisme donner en param avec possibilité de filtrer ou non pour l'organisme par lieu exécution
            if ($acronyme && (is_countable($_SERVER['argv']) ? count($_SERVER['argv']) : 0) >= 3) {
                $critereOrgParam = null;
                $deliveryPlaceParam = null;

                if (!isset($_GET['notUseDeliveryPlace'])) {
                    $deliveryPlaceParam = Atexo_Config::getParameter('DELIVERY_PLACE_REQUEST_INTERFACES_ANNONCES_PARTENAIRES');
                }
                if (isset($_GET['critereorg'])) {
                    $critereOrgParam = $_GET['critereorg'];
                }
                $entity = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);
                if (($entity instanceof CommonOrganisme) && ('1' == $entity->getActive())) {
                    if ('achatpublic' == $entity->getAcronyme()) {
                        return;
                    }
                    $xmlRequest = self::generateRequestTender($critereOrgParam, $deliveryPlaceParam);
                    $tender->insertTenders($entity, $xmlRequest, true);
                } else {
                    throw new Exception("L'organisme partenaire n'existe pas en base \n");
                }
            } else {
                $entitys = (new Atexo_Organismes())->retrieveActiveOrganismes(true, true);
                if ($entitys) {
                    foreach ($entitys as $oneEntity) {
                        if ('achatpublic' == $oneEntity->getAcronyme()) {
                            continue;
                        }
                        $pathXmlRequest = Atexo_Config::getParameter('XML_REQUEST_INTERFACES_ANNONCE_MARCHES');
                        $xml = file_get_contents($pathXmlRequest);
                        $tender->insertTenders($oneEntity, $xml);
                    }
                } else {
                    throw new Exception('La liste des plates formes manquante');
                }
            }
        } catch (Exception $e) {
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }

    public static function generateRequestTender($critereOrg = null, $deliveryPlace = null)
    {
        $domDocument = new DOMDocument('1.0', 'utf-8');
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        self::addAttribute($domDocument, $transaction, 'Version', '1.0');
        self::addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        self::addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        self::addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', 'interfaces-annonce-marches.xsd');
        $request = $domDocument->createElement('Request');
        $transaction->appendChild($request);
        $tender = $domDocument->createElement('Tender');
        $request->appendChild($tender);
        //En cas d'un critere : critereorg=boamp ou boamp le nom de l'org a traitée
        if ($critereOrg) {
            self::addAttribute($domDocument, $tender, 'organism', $critereOrg);
        }
        if ($deliveryPlace) {
            self::addAttribute($domDocument, $tender, 'deliveryPlace', $deliveryPlace);
        }

        return $domDocument->saveXml();
    }

    public function addAttribute($domDocument, $masterTag, $attributeName, $attributeContent)
    {
        $attribute = $domDocument->createAttribute($attributeName);
        $masterTag->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode($attributeContent);
        $attribute->appendChild($attributeValue);
    }
}
