<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Cron de dedoublonnage des avis BOAMP sur ANM.
 *
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 */
class Cli_ANMDedoublonnageBOAMP extends Atexo_Cli
{
    public static function run()
    {
        $argument = [];
        try {
            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument[] = $arguments[$i];
                }
            }
            $dateActuelle = date('Y-m-d');
            $date = Atexo_Util::dateMoinJour($dateActuelle, '1');
            if (is_array($argument)) {
                foreach ($argument as $param) {
                    if ($param) {
                        if ('-f' == $param) {
                            $date = false;
                        } elseif (is_numeric($param)) {
                            $date = Atexo_Util::dateMoinJour($dateActuelle, $param);
                        }
                    }
                }
            }
            self::deepDeduplicate($date);
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE').'/dedoublonnage_erreur.log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            fwrite(STDERR, date('Y-m-d H:i:s')."\t".'-> erreur : '.$e->getMessage()."\n");
        }
    }

    public static function deepDeduplicate($date)
    {
        try {
            /**Initialisation**/

            $word_seuil = 0.001;

            $intitule_word_hash = [];
            $intitule_word_hash = self::getInituleFrequentWords($word_seuil);

            $objet_word_hash = [];
            $objet_word_hash = self::getObjetFrequentWords($word_seuil);

            $fichierLog = '';
            //Repertoire et fichier de log
            if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE'))) {
                system('mkdir '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE')));
                fwrite(STDOUT, date('Y-m-d H:i:s')."\t"."\r\n-> repertoire suivant cree   ".Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE').'....');
            }
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE').'dedoublonnage_textes_log'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';

            //Connexion a la BDD
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

            //Compteur
            $cpt = 0;

            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Debut du traitement : deduplication de l\'ensemble des consultations'."\n");

            //Liste des organismes BOAMP
            $entity_array = [Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_MAPA'),
                                    Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_NATIONALE'),
                                    Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_EUROP'), ];

            foreach ($entity_array as $entity) {
                /**Recuperation de l'ensemble des consultations **/
                fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Recherche de toutes les avis de l\'entite '.$entity.'...'."\n");

                //Preparation et execution de la requete
                $query = 'select ORGANISME, ID, REFERENCE_UTILISATEUR, OBJET, INTITULE, DATEFIN, DOUBLON, DOUBLON_DE, URL_CONSULTATION_ACHAT_PUBLIQUE, URL_CONSULTATION_EXTERNE, URL_CONSULTATION_AVIS_PUB from consultation where organisme = "'.$entity.'" and datedebut >= "'.$date.'" and doublon = "0" order by DATEDEBUT, REFERENCE_UTILISATEUR ;';
                $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
                $statement->execute([]);
                //Recuperation des resultats
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
                $n = is_countable($rows) ? count($rows) : 0;

                fwrite(STDOUT, date('Y-m-d H:i:s')."\t"."Requete executee:\n");
                fwrite(STDOUT, date('Y-m-d H:i:s')."\t".$query."\n");
                fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Nombre de resultats: '.$n."\n");

                /**Traitement**/
                for ($i = 0; $i < $n; ++$i) {
                    $row_cpt = self::deduplicateRow($connexion, $rows[$i], $intitule_word_hash, $objet_word_hash);
                    $cpt += $row_cpt;
                }
            }

            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Nombre de deduplications effectuees : '.$cpt."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fin de traitement'."\n");
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_DEDOUBLONNAGE').'/dedoublonnage_erreur.log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            fwrite(STDERR, date('Y-m-d H:i:s')."\t".'-> erreur : '.$e->getMessage()."\n");
        }
    }

    public static function deduplicateRow($connexion, $row, &$intitule_word_hash, &$objet_word_hash)
    {
        //Recuperation des consultations ayant meme reference_utilisateur et meme date de fin
        $query = 'select ORGANISME, ID , URL_CONSULTATION_EXTERNE from consultation where ORGANISME NOT LIKE "%boamp%";';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute([]);

        //Recuperation des resultats
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $n = is_countable($rows) ? count($rows) : 0;

        //Aucune consultation ayant la meme ref et date de fin => sortie de fonction
        if (!$n) {
            return false;
        }

        //Recuperation du lien profond de l'avis a dedoubler
        $url_consultation_achat_publique = $row['URL_CONSULTATION_ACHAT_PUBLIQUE'];
        $url_consultation_achat_publique_array = explode('||', $url_consultation_achat_publique);

        $var_match = false;
        foreach ($rows as $row_item) {
            //Regle url_consultation_externe
            if (!empty($url_consultation_achat_publique) && count($url_consultation_achat_publique_array)) {
                foreach ($url_consultation_achat_publique_array as $url) {
                    if (self::matchOnUrlConsultationAchatPublique($row_item['URL_CONSULTATION_EXTERNE'], $url)) {
                        $var_match = true;
                        break;
                    }
                }
            }

            if ($var_match) {
                //On a trouve un double sur la regle url_consultation_externe
                self::linkRows($connexion, $row, $row_item);

                return true;
            } else {
                if (self::matchOnOtherFields($row_item, $row)) {
                    //On a trouve un double en verifiant les champs intitule, objet, datefin, reference_utilisateur
                    self::linkRows($connexion, $row, $row_item);

                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

    public static function linkRows($connexion, $duplicate, $row)
    {
        //Recuperation des enregistrements sous forme objet
        $duplicate_item = (new Atexo_Consultation())->retrieveConsultation($duplicate['ID'], $duplicate['ORGANISME']);
        $row_item = (new Atexo_Consultation())->retrieveConsultation($row['ID'], $row['ORGANISME']);

        //Mise a jour de la consultation
        $row_item->setUrlConsultationAvisPub($duplicate_item->getUrlConsultationAvisPub());
        $row_item->save($connexion);

        //Mise a jour de l'avis BOAMP
        $duplicate_item->setDoublon('1');
        $idDoublonDe = '#'.$row_item->getOrganisme().'#'.$row_item->getId().'#';
        $duplicate_item->setDoublonDe($idDoublonDe);
        $duplicate_item->save($connexion);
    }

    public static function getFrequentWords($array, $threshold)
    {
        $count = is_countable($array) ? count($array) : 0;
        $words_hash = [];
        $words_amount = 0;

        for ($i = 0; $i < $count; ++$i) {
            $buffer = trim(strtolower($array[$i]));
            $buffer = str_replace('\'', ' ', $buffer);
            $tab = explode(' ', $buffer);

            foreach ($tab as $key => $value) {
                //On considere qu'un mot "parasite" est constitue de 2 a 5 lettres
                if (preg_match('#^[a-z]{2,5}$#', $value)) {
                    if (!array_key_exists($value, $words_hash)) {
                        $words_hash[$value] = 1;
                    } else {
                        $numb = $words_hash[$value];
                        $words_hash[$value] = $numb + 1;
                    }
                }
                //On incremente le compteurs de mots pour par la suite calculer la frequences des mots supposes "parasites"
                ++$words_amount;
            }
        }

        unset($array);

        $new_word_hash = [];
        foreach ($words_hash as $word => $numb) {
            $frequence = (float) $numb / $words_amount;
            //On ne garde que les mots ayant une frequence superieure au seuil defini
            if ($frequence > $threshold) {
                $new_word_hash[$word] = $numb;
            }
        }

        unset($words_hash);

        return array_keys($new_word_hash);
    }

    public static function getInituleFrequentWords($threshold)
    {
        $query = 'select distinct(intitule) from consultation;';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute([]);

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $count = is_countable($rows) ? count($rows) : 0;

        $array = [];
        for ($i = 0; $i < $count; ++$i) {
            $array[] = trim(strtolower($rows[$i]['intitule']));
        }

        unset($rows);

        return self::getFrequentWords($array, $threshold);
    }

    public static function getObjetFrequentWords($threshold)
    {
        $query = 'select distinct(objet) from consultation;';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute([]);

        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $count = is_countable($rows) ? count($rows) : 0;

        $array = [];

        for ($i = 0; $i < $count; ++$i) {
            $array[] = trim(strtolower($rows[$i]['objet']));
        }

        unset($rows);

        return self::getFrequentWords($array, $threshold);
    }

    public static function reduceNoise($text, $hash)
    {
        //On transforme la chaine en tableau
        $buffer = explode(' ', $text);

        //On enleve tous les mots consideres comme parasites
        $buffer = str_replace($hash, '', $buffer);

        $array_text = [];
        //On supprime les entrees vides du tableau genere
        foreach ($buffer as $key => $word) {
            if (!empty($word)) {
                $array_text[] = $word;
            }
        }

        //On renvoie le tableau de la chaine
        return $array_text;
    }

    public static function compareWords($text1, $text2, $words_hash)
    {
        //On enleve le bruit dans la permiere chaine
        $cleared_text1 = self::reduceNoise(trim($text1), $words_hash);
        //On enleve le bruit dans la deuxieme chaine
        $cleared_text2 = self::reduceNoise(trim($text2), $words_hash);

        //On compare la chaine 2 a la chaine 1
        $array_source = [];
        $array_target = [];

        $array_source = $cleared_text1;
        $array_target = $cleared_text2;

        $match_numb = 0;

        for ($i = 0,$n = is_countable($array_source) ? count($array_source) : 0; $i < $n; ++$i) {
            if ($array_source[$i] == $array_target[$i]) {
                ++$match_numb;
            }
        }
        if ($n) {
            $p1 = (float) $match_numb / $n;
        } else {
            $p1 = 0;
        }

        //On compare la chaine 1  a la chaine 2
        $array_source = [];
        $array_target = [];

        $array_source = $cleared_text2;
        $array_target = $cleared_text1;

        $match_numb = 0;
        for ($j = 0,$m = is_countable($array_source) ? count($array_source) : 0; $j < $m; ++$j) {
            if ($array_source[$j] == $array_target[$j]) {
                ++$match_numb;
            }
        }
        if ($m) {
            $p2 = (float) $match_numb / $m;
        } else {
            $p2 = 0;
        }

        //On en deduit la similarite
        $threshold = (float) 0.9;
        $var_match = (int) false;

        if (0 == $p1 || 0 == $p2) {
            //Si au moins une proportion est nulle, les chaines ne sont pas identiques
            $var_match = false;
        } elseif (1 == $p1 || 1 == $p2) {
            //Si au moins une proportion vaut 1, l'une des chaines est une sous-chaine de l'autre
            $var_match = true;
        } elseif ($p1 == $p2 && $p1 >= $threshold) {
            // Si les deux frequences sont egales, elles sont similaires moyennant un degre de similarite
            $var_match = true;
        } else {
            $var_match = false;
        }

        return $var_match;
    }

    public static function matchOnUrlConsultationAchatPublique($ref_url, $outer_url)
    {
        $short_outer_url = null;
        $short_ref_url = null;
        //Remplacement de "index.php" et "index.php5"
        $ref_url = str_replace('index.php5', '', $ref_url);
        $ref_url = str_replace('index.php', '', $ref_url);
        $outer_url = str_replace('index.php5', '', $outer_url);
        $outer_url = str_replace('index.php', '', $outer_url);

        //Remplacement particulier pour maximilien
        $match = [];
        $var_match = preg_match('/.*[a-z]+\.(maximilien\.fr.*\??.*)/', $ref_url, $match);
        if ($var_match) {
            $short_ref_url = $match[1];
        }

        $match = [];
        $var_match = preg_match('/.*[a-z]+\.(maximilien\.fr.*\??.*)/', $outer_url, $match);
        if ($var_match) {
            $short_outer_url = $match[1];
        }

        //Comparaison exacte entre les deux URLs
        if ($short_outer_url == $short_ref_url) {
            return true;
        } elseif ($ref_url == $outer_url) {
            return true;
        } else {
            return false;
        }
    }

    public static function matchOnOtherFields($row_item, $row)
    {
         $intitule_word_hash = null;
        $objet_word_hash = null;
        $connexion = null;
        //Comparaison reference_utilisateur, datefin, intitule, objet
        if ($row_item['REFERENCE_UTILISATEUR'] != $row['REFERENCE_UTILISATEUR'] || $row_item['DATEFIN'] != $row['DATEFIN']) {
            return false;
        }

        //Comparaison intitule vs intitule
        $row_item_intitule = $row_item['INTITULE'];
        $row_intitule = $row['INTITULE'];

        if (!empy($row_item_intitule) && !empty($row_intitule)) {
            $var_match = (int) self::compareWords($row_item_intitule, $row_intitule, $intitule_word_hash);
            if (!$var_match) {
                return false;
            }
        }

        //Comparaison objet vs objet
        $row_item_objet = $row_item['OBJET'];
        $row_objet = $row['OBJET'];

        $var_match = (int) self::compareWords($row_item_objet, $row_objet, $objet_word_hash);

        if (!$var_match) {
            return false;
        } else {
            //On a trouve un double sur la regle objet
            self::linkRows($connexion, $row, $row_item);

            return true;
        }
    }
}
