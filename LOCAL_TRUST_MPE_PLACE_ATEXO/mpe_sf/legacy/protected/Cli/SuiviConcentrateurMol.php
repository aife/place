<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonAnnonce;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_Alertes;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Annonce;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use DateTime;
use Exception;

/**
 * Class Cli_SuiviConcentrateurMol.
 */
class Cli_SuiviConcentrateurMol extends Atexo_Cli
{
    private static $codeRetour;
    private static $oversight;

    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) > 2) {
            $nombreJours = $arguments[2];
        } else {
            $nombreJours = 15;
        }

        $params = [];

        try {
            $params['debutExecution'] = new DateTime('now');
            $params['service'] = 'BOAMP';
            $params['nomBatch'] = 'Cli_SuiviConcentrateurMol';
            $params['variablesEntree'] = serialize($arguments);

            self::$codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
            self::$oversight = $params['oversight'] = self::oversight($params);
        } catch (Exception $exceptionCLI) {
            $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL
                .'Erreur : '.$exceptionCLI->getMessage().PHP_EOL
                .'Trace : '.$exceptionCLI->getTraceAsString().PHP_EOL;
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($oversightError);
        }

        //le suivi de syncrhonisation MOL ne se fait que pour une PF de type MPE
        //ou le suivi de syncrhonisation MOL se fait pour une PF de type MPE et RSEM avec possibilité d'avoir plusieurs code
        if (('0' == Atexo_Config::getParameter('TYPE_SUIVI_CONCENTRATEUR_MOL')) || ('2' == Atexo_Config::getParameter(
            'TYPE_SUIVI_CONCENTRATEUR_MOL'
        ))) {
            $uidPF = Atexo_Config::getParameter('UID_PF_MPE');
            $recupAnnonceRSEM = false;
            $fileLog = Atexo_LoggerManager::getLogger('publicite');
            $fileLog->info('Début lancement doSuivi pour le UID_PF_MPE '.$uidPF);
            self::doSuivi($nombreJours, $uidPF, $recupAnnonceRSEM);
            $fileLog->info('Fin lancement doSuivi pour le UID_PF_MPE '.$uidPF);
            $uidPFDouble = Atexo_Config::getParameter('UID_PF_MPE_DOUBLE');
            if ($uidPFDouble) {
                $fileLog->info('Début lancement doSuivi pour le UID_PF_MPE_DOUBLE '.$uidPFDouble);
                self::doSuivi($nombreJours, $uidPFDouble, $recupAnnonceRSEM);
                $fileLog->info('Fin lancement doSuivi pour le UID_PF_MPE_DOUBLE '.$uidPFDouble);
            }
        }

        //le suivi de syncrhonisation MOL ne se fait que pour une PF de type RSEM avec possibilité d'avoir plusieurs code
        //ou le suivi de syncrhonisation MOL se fait pour une PF de type MPE et RSEM avec possibilité d'avoir plusieurs code
        if (('1' == Atexo_Config::getParameter('TYPE_SUIVI_CONCENTRATEUR_MOL')) || ('2' == Atexo_Config::getParameter(
            'TYPE_SUIVI_CONCENTRATEUR_MOL'
        ))) {
            $listeCodeUidPF = explode('#', Atexo_Config::getParameter('UID_PF_RSEM'));

            if (is_array($listeCodeUidPF) && $listeCodeUidPF) {
                foreach ($listeCodeUidPF as $uidPF) {
                    if ($uidPF) {
                        $recupAnnonceRSEM = true;
                        self::doSuivi($nombreJours, $uidPF, $recupAnnonceRSEM);
                    }
                }
            }
        }

        try {
            $params['code'] = self::$codeRetour;
            $params['finExecution'] = 'endScript';
            self::oversight($params);
        } catch (Exception $exceptionCLI) {
            $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL
                .'Erreur : '.$exceptionCLI->getMessage().PHP_EOL
                .'Trace : '.$exceptionCLI->getTraceAsString().PHP_EOL;
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($oversightError);
        }
    }

    /**
     * @param $nombreJours
     * @param $uidPF
     * @param $recupAnnonceRSEM
     */
    public static function doSuivi($nombreJours, $uidPF, $recupAnnonceRSEM)
    {
        $options = [];
        try {
            $fileLog = Atexo_LoggerManager::getLogger('publicite');
            //préparation de la séssion
            self::preparerSession();
            $_dateExec = date('Y-m-d H:i:s');
            $pathRequest = Atexo_Config::getParameter('PATH_REQUEST_CONCENTRATEUR');
            $dateDebut = Atexo_Util::dateDansPasse(date('Y-m-d H:i:s'), 0, $nombreJours, 0, 0);
            $dateFin = date('Y-m-d H:i:s');
            $xmlRequest = file_get_contents($pathRequest);
            if (false === $xmlRequest) {
                throw new Exception('File content could not be read : '.$pathRequest);
            }

            $xmlRequest = str_replace('CONS_UID_PF', $uidPF, $xmlRequest);
            $xmlRequest = str_replace('CONS_DATE_DEBUT', $dateDebut, $xmlRequest);
            $xmlRequest = str_replace('CONS_DATE_FIN', $dateFin, $xmlRequest);

            $options['location'] = Atexo_Config::getParameter('URL_CONCENTRATEUR_MONITEUR_WS_SUIVI');
            $options['uri'] = '';
            $options['trace'] = '1';
            $options['exceptions'] = '1';

            if ('' != Atexo_Config::getParameter('URL_PROXY')) {
                $options['proxy_host'] = Atexo_Config::getParameter('URL_PROXY');
                $options['proxy_port'] = intval(Atexo_Config::getParameter('PORT_PROXY'));
                $context = stream_context_create(
                    [
                        'ssl' => [
                            'SNI_enabled' => false,
                        ],
                        'http' => [
                            'proxy' => $options['proxy_host'].':'.$options['proxy_port'],
                        ],
                    ]
                );
                $options['stream_context'] = $context;
            }

            $soapClient = new \SoapClient(null, $options);
            $xmlResponseRecu = $soapClient->generateXmlResponse(base64_encode($xmlRequest));

            if ($xmlResponseRecu) {
                $xmlResponse = base64_decode($xmlResponseRecu, true);

                if (!$xmlResponse) {
                    $xmlResponse = $xmlResponseRecu;
                }

                $xmlResponse = str_replace('&', '&amp;', $xmlResponse);
                $xml = simplexml_load_string($xmlResponse);
                $tabCast = (array) $xml;
                $tab1 = (array) $tabCast['avis_concentrateur'];

                if (isset($tab1['uid_consultation'])) {
                    $tab = ['0' => $tabCast['avis_concentrateur']];
                } else {
                    $tab = $tabCast['avis_concentrateur'];
                }

                $connexionCom = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
                );

                if (is_array($tab) && count($tab) > 0) {
                    foreach ($tab as $oneAvis) {
                        $oneAvis1 = (array) $oneAvis;
                        $uidConsultation = $oneAvis1['uid_consultation'];
                        $organisme = $oneAvis1['organisme'];
                        $elemUidCons = explode('##', $uidConsultation);
                        $consultationId = $elemUidCons[1];
                        $refUtilisateur = $oneAvis1['reference_utilisateur'];
                        $tabCommonConsultation = [];

                        if ($recupAnnonceRSEM) {
                            $tabCommonConsultation = (new Atexo_Consultation())->retrieveConsultationByReferenceUtilisateur($refUtilisateur);
                        } else {
                            $consQ = new CommonConsultationQuery();
                            $tabCommonConsultation[] = $consQ->getConsultation(
                                $consultationId,
                                $organisme,
                                $connexionCom
                            );
                        }

                        $nomFichierAnononce = utf8_encode(base64_decode($oneAvis1['nom_fichier_annonce']));

                        if (is_array($tabCommonConsultation) && count($tabCommonConsultation) > 0) {
                            foreach ($tabCommonConsultation as $commonConsultation) {
                                if ($commonConsultation instanceof CommonConsultation) {
                                    $oldDateMiseEnligne = $commonConsultation->getDateMiseEnLigneCalcule();

                                    if ($recupAnnonceRSEM) {
                                        $organisme = $commonConsultation->getOrganisme();
                                    }

                                    $commonAnnonce = (new Atexo_Publicite_Annonce())->retreiveCommonAnnoncebyFileNameAndRefConsultation($commonConsultation->getId(), $nomFichierAnononce, true);

                                    if (!($commonAnnonce instanceof CommonAnnonce)) {
                                        $cas = 'cas1';

                                        if ($organisme && $uidConsultation) {
                                            if ($recupAnnonceRSEM) {
                                                $nomFichierXml = '_'.$nomFichierAnononce;
                                            } else {
                                                try {
                                                    $params['debutExecution'] = new DateTime('now');
                                                    $params['service'] = 'BOAMP';
                                                    $params['nomBatch'] = 'Cli_SuiviConcentrateurMol';
                                                    $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                                                    self::oversight($params);
                                                } catch (Exception $exceptionCLI) {
                                                    $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionCLI->getMessage(
                                                    ).PHP_EOL.'Trace : '.$exceptionCLI->getTraceAsString(
                                                    ).PHP_EOL;
                                                    $logger = Atexo_LoggerManager::getLogger('supervision');
                                                    $logger->error($oversightError);
                                                }

                                                $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($commonConsultation->getCompteBoampAssocie(), $organisme);

                                                if ($acheteurPublic instanceof CommonAcheteurPublic) {
                                                    $nomFichierXml = strtoupper(
                                                        $acheteurPublic->getBoampLogin()
                                                    ).'_'.$nomFichierAnononce;
                                                } else {
                                                    $fileLog->info(
                                                        $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|CompteBoampAssocie Not Found'
                                                    );
                                                    $nomFichierXml = '_'.$nomFichierAnononce;
                                                }
                                            }

                                            $commonAnnonce = new CommonAnnonce();
                                            $annonce = new CommonAnnonceBoamp();
                                            $annonce->setOrganisme($organisme);
                                            $annonce->setConsultationId($commonConsultation->getId());
                                            $annonce->setStatutDestinataire(
                                                Atexo_Config::getParameter('DESTINATAIRE_ENVOYE')
                                            );
                                            $annonce->setNomFichierXml($nomFichierXml);

                                            if ($oneAvis1['date_envoi']) {
                                                $annonce->setDateEnvoi($oneAvis1['date_envoi']);
                                            }

                                            $commonAnnonce->setOrganisme($organisme);
                                            $commonAnnonce->setConsultationId($commonConsultation->getId());
                                            $commonAnnonce->setNomFichierXml($nomFichierXml);
                                            $commonAnnonce->setDateEnvoi($oneAvis1['date_envoi']);

                                            try {
                                                $info = self::getInfoBoamp($oneAvis);

                                                if (is_array($info)) {
                                                    self::setInfoAnnonceBoamp($annonce, $info, $_dateExec);
                                                    self::setInfoCommonAnnonce($commonAnnonce, $info, $_dateExec);
                                                }

                                                if ($info['etat'] == Atexo_Config::getParameter(
                                                    'DESTINATAIRE_STATUT_PU'
                                                )) {
                                                    (new Atexo_Agent_Alertes())->sendAlertePublication('BOAMP', $commonConsultation, false);
                                                }

                                                if (($info['etat'] == Atexo_Config::getParameter(
                                                    'DESTINATAIRE_STATUT_RG'
                                                ))
                                                    || ($info['etat'] == Atexo_Config::getParameter(
                                                        'DESTINATAIRE_STATUT_RX'
                                                    ))
                                                    || ($info['etat'] == Atexo_Config::getParameter(
                                                        'DESTINATAIRE_STATUT_RE'
                                                    ))
                                                ) {
                                                    (new Atexo_Agent_Alertes())->sendAlerteEchecPublication('BOAMP', $commonConsultation);
                                                }
                                            } catch (Exception) {
                                                self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                $fileLog->error(
                                                    $organisme."| Pas de nouvelles informations pour l'Annonce ".$nomFichierAnononce
                                                );
                                            }

                                            try {
                                                $annonce->save($connexionCom);
                                            } catch (Exception $e) {
                                                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                $fileLog->error(
                                                    $organisme."| Erreur lors de la sauvegarde de l'Annonce ".$nomFichierAnononce
                                                );
                                            }

                                            $commonAnnonce->setIdBoamp($annonce->getIdBoamp());
                                            $commonAnnonce->save($connexionCom);
                                            $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne(
                                                $commonConsultation
                                            );
                                            $commonConsultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                                            $commonConsultation->save($connexionCom);
                                            $fileLog->info(
                                                $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|'.$dateMiseEnligneCalcule.'|'.$cas.'|OK'
                                            );

                                            if (!$recupAnnonceRSEM) {
                                                try {
                                                    $params['finExecution'] = 'endScript';
                                                    self::oversight($params);

                                                    unset($params['finExecution']);
                                                    unset($params['oversight']);
                                                } catch (Exception $exceptionCLI) {
                                                    $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionCLI->getMessage(
                                                    ).PHP_EOL.'Trace : '.$exceptionCLI->getTraceAsString(
                                                    ).PHP_EOL;
                                                    $logger = Atexo_LoggerManager::getLogger('supervision');
                                                    $logger->error($oversightError);
                                                }
                                            }
                                        } else {
                                            $fileLog->info(
                                                $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|UidCons or organisme Not Found'.print_r(
                                                    $oneAvis,
                                                    true
                                                )
                                            );
                                        }
                                    } else {
                                        if ($recupAnnonceRSEM) {
                                            $nomFichierXml = '_'.$nomFichierAnononce;
                                        } else {
                                            $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($commonConsultation->getCompteBoampAssocie(), $organisme);

                                            if ($acheteurPublic instanceof CommonAcheteurPublic) {
                                                $nomFichierXml = strtoupper(
                                                    $acheteurPublic->getBoampLogin()
                                                ).'_'.$nomFichierAnononce;
                                            } else {
                                                $fileLog->info(
                                                    $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|CompteBoampAssocie Not Found'
                                                );
                                                $nomFichierXml = '_'.$nomFichierAnononce;
                                            }
                                        }

                                        $annonce = (new Atexo_Publicite_Destinataire())->retrieveAnnonceByFileNameAndRefConsultataion($commonConsultation->getId(), $nomFichierAnononce, $organisme);

                                        if (!($annonce instanceof CommonAnnonceBoamp)) {
                                            $cas = 'cas2';
                                            $annonce = new CommonAnnonceBoamp();
                                            $annonce->setConsultationId($commonConsultation->getId());
                                            $annonce->setOrganisme($organisme);
                                            $annonce->setStatutDestinataire(
                                                Atexo_Config::getParameter('DESTINATAIRE_ENVOYE')
                                            );
                                            $annonce->setNomFichierXml($nomFichierXml);
                                            $annonce->setDateEnvoi($oneAvis1['date_envoi']);

                                            try {
                                                $info = self::getInfoBoamp($oneAvis);

                                                if (is_array($info)) {
                                                    self::setInfoAnnonceBoamp($annonce, $info, $_dateExec);
                                                    self::setInfoCommonAnnonce($commonAnnonce, $info, $_dateExec);

                                                    if ($info['etat'] == Atexo_Config::getParameter(
                                                        'DESTINATAIRE_STATUT_PU'
                                                    )) {
                                                        (new Atexo_Agent_Alertes())->sendAlertePublication('BOAMP', $commonConsultation, false);
                                                    }

                                                    if (($info['etat'] == Atexo_Config::getParameter(
                                                        'DESTINATAIRE_STATUT_RG'
                                                    ))
                                                        || ($info['etat'] == Atexo_Config::getParameter(
                                                            'DESTINATAIRE_STATUT_RX'
                                                        ))
                                                        || ($info['etat'] == Atexo_Config::getParameter(
                                                            'DESTINATAIRE_STATUT_RE'
                                                        ))
                                                    ) {
                                                        (new Atexo_Agent_Alertes())->sendAlerteEchecPublication('BOAMP', $commonConsultation);
                                                    }
                                                }

                                                $annonce->save($connexionCom);
                                                $commonAnnonce->setIdBoamp($annonce->getIdBoamp());
                                                $commonAnnonce->save($connexionCom);
                                                $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne(
                                                    $commonConsultation
                                                );
                                                $commonConsultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                                                $commonConsultation->save($connexionCom);
                                                $fileLog->info(
                                                    $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|'.$dateMiseEnligneCalcule.'|'.$cas.'|OK'
                                                );
                                            } catch (\Exception) {
                                                self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                $fileLog->info(
                                                    $organisme."| Pas de nouvelles informations pour l'Annonce ".$nomFichierAnononce
                                                );
                                            }
                                        } else {
                                            $cas = 'cas3';

                                            try {
                                                $info = self::getInfoBoamp($oneAvis);

                                                if (is_array($info)) {
                                                    if ($info['etat'] != $annonce->getStatutDestinataire()) {
                                                        self::setInfoAnnonceBoamp($annonce, $info, $_dateExec);
                                                        self::setInfoCommonAnnonce($commonAnnonce, $info, $_dateExec);

                                                        if ($info['etat'] == Atexo_Config::getParameter(
                                                            'DESTINATAIRE_STATUT_PU'
                                                        )) {
                                                            (new Atexo_Agent_Alertes())->sendAlertePublication('BOAMP', $commonConsultation, false);
                                                        }

                                                        if (($info['etat'] == Atexo_Config::getParameter(
                                                            'DESTINATAIRE_STATUT_RG'
                                                        ))
                                                            || ($info['etat'] == Atexo_Config::getParameter(
                                                                'DESTINATAIRE_STATUT_RX'
                                                            ))
                                                            || ($info['etat'] == Atexo_Config::getParameter(
                                                                'DESTINATAIRE_STATUT_RE'
                                                            ))) {
                                                            (new Atexo_Agent_Alertes())->sendAlerteEchecPublication('BOAMP', $commonConsultation);
                                                        }

                                                        $commonAnnonce->save($connexionCom);
                                                        $annonce->save($connexionCom);
                                                    }
                                                }

                                                $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne(
                                                    $commonConsultation
                                                );
                                                $commonConsultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                                                $commonConsultation->save($connexionCom);
                                                $fileLog->info(
                                                    $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|'.$dateMiseEnligneCalcule.'|'.$cas.'|OK'
                                                );
                                            } catch (Exception $exep) {
                                                self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                                $fileLog->error($organisme.'|'.$exep->getMessage());
                                            }

                                            $fileLog->info(
                                                $oneAvis1['organisme'].'|'.$oneAvis1['uid_consultation'].'|'.$nomFichierAnononce.'|OK Annonce found'
                                            );
                                        }
                                    }

                                    echo $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|'.$dateMiseEnligneCalcule.'|'.$cas.'|OK'."\r\n";

                                    if (Atexo_Module::isEnabled('InterfaceDume') &&
                                        '1' == $commonConsultation->getDumeDemande() &&
                                        (
                                            $oldDateMiseEnligne != $commonConsultation->getDateMiseEnLigneCalcule() &&
                                            Atexo_Util::validateDate(
                                                $commonConsultation->getDateMiseEnLigneCalcule(),
                                                'Y-m-d H:i:s'
                                            ) &&
                                            !empty($commonConsultation->getDateMiseEnLigneCalcule()) &&
                                            '0000-00-00 00:00:00' != $commonConsultation->getDateMiseEnLigneCalcule()
                                        )
                                    ) {
                                        $retour = Atexo_Dume_AtexoDume::publierDumeAcheteur($commonConsultation);

                                        if (is_array(
                                            $retour
                                        ) && true == $retour['erreur']) {//MPE-5200 demande de BKA bloquer la publication de la consultation si lt_dume atexo ne repond pas
                                            $oldDateMiseEnligne = $oldDateMiseEnligne ?: '0000-00-00 00:00:00';
                                            $commonConsultation->setDateMiseEnLigneCalcule($oldDateMiseEnligne);
                                            $commonConsultation->save($connexionCom);
                                            $fileLog->error(
                                                $oneAvis1['organisme'].'|'.$oneAvis1['uid_consultation'].'|'.$nomFichierAnononce.'|Dume non publié =>ne pas publié de la consultation : erreur '.$retour['messageErreur']
                                            );
                                        } else {
                                            Atexo_Dume_AtexoDume::createJmsJobToPublishDume($commonConsultation);
                                            $fileLog->error(
                                                $oneAvis1['organisme'].'|'.$oneAvis1['uid_consultation'].'|'.$nomFichierAnononce.'|Dume publié'
                                            );
                                        }
                                    }
                                } else {
                                    $fileLog->info(
                                        $organisme.'|'.$uidConsultation.'|'.$nomFichierAnononce.'|'.$dateMiseEnligneCalcule.'|'.$cas.'|Consultation Not Found'
                                    );
                                }
                            }
                        }
                    }
                } else {
                    $fileLog->info('|||responseWs table vide '.$xmlResponse);
                }
            } else {
                $fileLog->info('|||responseWs vide '.$xmlResponseRecu);
            }
        } catch (Exception $e) {
            self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $fileLog->error($e->getMessage());
        }
    }

    /**
     * @param $oneAvis
     *
     * @return null
     */
    public static function getInfoBoamp($oneAvis)
    {
        $info = [];
        $value = 0;

        if ((count((array) $oneAvis->suivi) > 0)) {
            foreach ($oneAvis->suivi->AVIS->children() as $oneSupport) {
                if ('BOA' == (string) $oneSupport['CODE'] || 'BPW' == (string) $oneSupport['CODE']) {
                    $idJO = (string) $oneSupport->ANNONCE['ID_JO'];

                    foreach ($oneSupport->children() as $fils) {
                        if ($fils->INFOS_PUB) {
                            $urlPdf = (string) $fils->INFOS_PUB['URL_PDF'];
                            $urlBoamp = (string) $fils->INFOS_PUB['URL'];
                            $parType = (string) $fils->INFOS_PUB['PAR_TYPE'];
                            $NumAnnPar = (string) $fils->INFOS_PUB['NUM_ANN_PAR'];
                            $parution = (string) $fils->INFOS_PUB['PARUTION'];
                            $datePub = (string) $fils->INFOS_PUB['DATE_PUB'];
                        }

                        if ('STATUT' == (string) $fils->getName()) {
                            if ((float) $fils['VALUE'] > $value) {
                                $value = (float) $fils['VALUE'];
                            }
                        }
                    }
                }

                if ('JOU' == (string) $oneSupport['CODE']) {
                    $idJO = (string) $oneSupport->ANNONCE['ID_JO'];

                    foreach ($oneSupport->children() as $fils) {
                        if ($fils->INFOS_PUB) {
                            $urlPdf = (string) $fils->INFOS_PUB['URL_PDF'];
                            $urlBoamp = (string) $fils->INFOS_PUB['URL'];
                            $parType = (string) $fils->INFOS_PUB['PAR_TYPE'];
                            $NumAnnPar = (string) $fils->INFOS_PUB['NUM_ANN_PAR'];
                            $parution = (string) $fils->INFOS_PUB['PARUTION'];
                            $datePub = (string) $fils->INFOS_PUB['DATE_PUB'];
                        }

                        if ('STATUT' == (string) $fils->getName()) {
                            if ((float) $fils['VALUE'] > $value) {
                                $value = (float) $fils['VALUE'];
                            }
                        }
                    }
                }
            }

            $etat = self::getState((string) $value);
            $info['idJo'] = $idJO;
            $info['etat'] = $etat;
            $info['urlPdf'] = $urlPdf;
            $info['parType'] = $parType;
            $info['numAnnPar'] = $NumAnnPar;
            $info['parution'] = $parution;
            $info['datePub'] = $datePub;
            $info['urlBoamp'] = $urlBoamp;
        }

        if ('' != $info['idJo'] ||
            '' != $info['etat'] ||
            '' != $info['urlPdf'] ||
            '' != $info['parType'] ||
            '' != $info['numAnnPar'] ||
            '' != $info['parution'] ||
            '' != $info['datePub'] ||
            '' != $info['urlBoamp']
        ) {
            return $info;
        } else {
            return null;
        }
    }

    /**
     * @param $annonce
     * @param $info
     * @param $_dateExec
     *
     * @return mixed
     */
    public static function setInfoAnnonceBoamp(&$annonce, $info, $_dateExec)
    {
        $annonce->setDateMaj($_dateExec);

        if ($info['etat']) {
            $annonce->setStatutDestinataire($info['etat']);
        }

        if ($info['idJo']) {
            $annonce->setIdJo($info['idJo']);
        }

        if ($info['urlPdf']) {
            $annonce->setLienPdf($info['urlPdf']);
        }

        if ($info['datePub']) {
            $annonce->setDatepub($info['datePub']);
        }

        if ($info['parution']) {
            $annonce->setParution($info['parution']);
        }

        if ($info['numAnnPar']) {
            $annonce->setNumAnn($info['numAnnPar']);
        }

        if ($info['parType']) {
            $annonce->setTypeBoamp($info['parType']);
        }

        if ($info['urlBoamp']) {
            $annonce->setLienBoamp($info['urlBoamp']);
        }

        return $annonce;
    }

    /**
     * @param $commonAnnonce
     * @param $info
     * @param $_dateExec
     *
     * @return mixed
     */
    public static function setInfoCommonAnnonce(&$commonAnnonce, $info, $_dateExec)
    {
        $commonAnnonce->setDateMaj($_dateExec);

        if ($info['datePub']) {
            $commonAnnonce->setDatepub($info['datePub']);
        }

        return $commonAnnonce;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public static function getState($value)
    {
        $valueState = [
            '0.2' => 'AV',
            '0.4' => 'CP',
            '0.10' => 'JAL',
            '0.11' => 'JAL',
            '2.5' => 'PU',
            '2.0' => 'PU',
            '2.14' => 'Publie JAL',
            '3.1' => 'RG',
            '3.3' => 'RE',
            '3.6' => 'RX',
            '3.13' => 'Rejet JAL',
            '4.4' => 'En attente de reemission sur le support',
        ];

        return $valueState[$value];
    }

    public static function preparerSession()
    {
        $session = [
            0 => '',
            1 => [],
            2 => '',
            3 => '',
            4 => '',
            5 => '',
            6 => '',
            7 => '',
            8 => '',
            9 => '',
            10 => '',
            11 => '',
            12 => '',
            13 => [],
            14 => '',
            15 => '',
            16 => '',
            17 => '',
            18 => '',
            19 => '',
            20 => '',
            21 => '',
            22 => '',
            23 => '',
            24 => '',
            25 => '',
            26 => '',
            27 => '',
            28 => '',
            29 => '',
            30 => '',
            31 => '',
            32 => '',
            33 => '',
            34 => '',
            35 => '',
            36 => '',
            37 => '',
            38 => '',
            39 => '',
            40 => '',
            41 => '',
            42 => '',
            43 => '',
            44 => '',
            45 => '',
            46 => '',
            47 => '',
            48 => '',
            49 => '',
            50 => '',
            51 => '',
            52 => '',
            53 => '',
            54 => '',
            55 => '',
            56 => '',
            57 => '',
            58 => '',
            59 => '',
            60 => '',
            61 => '',
            62 => '',
            63 => '',
            64 => '',
            65 => '',
        ];

        session_id(uniqid());
        $_SESSION[session_id()] = serialize($session);
    }
}
