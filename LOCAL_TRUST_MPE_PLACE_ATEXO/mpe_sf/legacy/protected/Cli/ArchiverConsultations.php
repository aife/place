<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveArcade;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Exception;

/**
 * génération des DIC pour chaque organisme.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_ArchiverConsultations extends Atexo_Cli
{
    public static function run()
    {
        $serialize = null;
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if(!empty($arguments[$i])) {
                    $argument = explode('=', $arguments[$i]);
                    $_GET[$argument[0]] = ('' != $argument[1]) ? $argument[1] : true;
                }
            }
            $serialize = json_encode($arguments, JSON_THROW_ON_ERROR);
        }

        $forceArchivage = (isset($_GET['organismeArchive']) && 'true' == $_GET['organismeArchive']) ? true : false;

        //répertoire de stockage racine des archives valeur par défaut /srv/mpe/data/mpe3/FichiersArchive/
        $fichiersArchivesDirectory = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR');

        if (empty($fichiersArchivesDirectory)) {
            echo 'Le paramètre FICHIERS_ARCHIVE_DIR n\'est pas configuré.';
            exit(1);
        }

        if (!is_dir(trim($fichiersArchivesDirectory))) {
            system('mkdir '.escapeshellarg($fichiersArchivesDirectory));
            echo "\r\n-> répertoire suivant créé   ".$fichiersArchivesDirectory.'....';
        }
        $c = new Criteria();
        if (isset($_GET['org'])) {
            $c->add(CommonOrganismePeer::ACRONYME, $_GET['org']);
            echo '-> Cron lancé uniquement sur la base : '.$_GET['org']." \r\n";
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            .Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organismes = CommonOrganismePeer::doSelect($c, $connexion);
        Propel::close();
        if (is_array($organismes)) {
            foreach ($organismes as $organisme) {
                $params = [];
                $params['service'] = 'MPE';
                $params['nomBatch'] = 'Cli_ArchiverConsultations';
                $echangesInterfaces = null;
                $params['poids'] = 0;
                self::oversight($params);
                $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                $informationMetier = [];

                $result = null;
                //on traite l'organisme si il n'est pas déjà archivé
                if (!self::isOrganismeArchivee($organisme->getAcronyme(), $forceArchivage)) {
                    $directoryLog = Atexo_Config::getParameter('FICHIERS_ARCHIVE_LOG_DIR');
                    $nomFichierLog = $directoryLog.'/log_CronArchive_'.$organisme->getAcronyme().Atexo_Util::getDateAujourdhui2();

                    $log = "\r\n\r\n_________________________________\r\n";
                    $log .= 'Log du '.date('d/m/Y H:i:s')."\r\n";
                    $log .= "_________________________________\r\n";
                    $log .= '-> ---------------------- Organisme : '.$organisme->getDenominationOrg().'/'.$organisme->getAcronyme()."--------------\r\n";
                   Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');

                    $informationMetier['Organisme'] = $organisme->getAcronyme();

                    $params['informationMetier'] = json_encode($informationMetier, JSON_THROW_ON_ERROR);

                    $session = [0 => '',
                        1 => [],
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => $organisme->getAcronyme(),
                        8 => '',
                        9 => '',
                        10 => '',
                        11 => '',
                        12 => '',
                        13 => [],
                        14 => '',
                        15 => '',
                        16 => '',
                        17 => '',
                        18 => '',
                        19 => '',
                        20 => '',
                        21 => '',
                        22 => '',
                        23 => '',
                        24 => '',
                        25 => '',
                        26 => '',
                        27 => '',
                        28 => '',
                        29 => '',
                        30 => '',
                        31 => '',
                        32 => '',
                        33 => '',
                        34 => '',
                        35 => '',
                        36 => '',
                        37 => '',
                        38 => '',
                        39 => '',
                        40 => '',
                        41 => '',
                        42 => '',
                        43 => '',
                        44 => '',
                        45 => '',
                        46 => '',
                        47 => '',
                        48 => '',
                        49 => '',
                        50 => '',
                        51 => '',
                        52 => '',
                        53 => '',
                        54 => '',
                        55 => '',
                        56 => '',
                        57 => '',
                        58 => '',
                        59 => '',
                        60 => '',
                        61 => '',
                        62 => '',
                        63 => '',
                        64 => '',
                        65 => '',
                    ];

                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                        .Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $c = new Criteria();
                    $c->add(CommonConfigurationOrganismePeer::ORGANISME, $organisme->getAcronyme());

                    $module = CommonConfigurationOrganismePeer::doSelectOne($c, $connexion);
                    Propel::close();

                    //répertoire de stockage racine des archives par organisme /srv/mpe/data/mpe3/FichiersArchive/acronyme_organisme
                    //si archivage annuel alors on met les archives dans un répertoire nommé AAAA
                    if (!isset($_GET['annuel'])) {
                        $pathDirArchiveOrganisme = $fichiersArchivesDirectory.$organisme->getAcronyme();
                    } else {
                        $pathDirArchiveOrganisme = $fichiersArchivesDirectory.$organisme->getAcronyme().'/'.(date('Y') - 2);
                    }
                    if (!is_dir($pathDirArchiveOrganisme)) {
                        system('mkdir  -p '.escapeshellarg($pathDirArchiveOrganisme));
                        $log = "\r\n-> répertoire suivant créé  ".$pathDirArchiveOrganisme.'....';
                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                        echo $log;
                    }

                    try {
                        $log = "\r\n-> Démarrage création arborescence des répertoires par service ....";
                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                        echo $log;

                        $critiria = new Criteria();
                        $critiria->add(CommonServicePeer::ORGANISME, $organisme->getAcronyme());
                        if (isset($_GET['service_id'])) {
                            $critiria->add(CommonServicePeer::ID, $_GET['service_id']);
                        }

                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                            .Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $services = CommonServicePeer::doSelect($critiria, $connexion);
                        Propel::close();

                        //on cree un repertoire pour chaque service
                        foreach ($services as $service) {
                            $log = '.';
                           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                            echo $log;
                            if (!Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && !(new Atexo_EntityPurchase())->hasChild($service->getId(), $organisme->getAcronyme())) {
                                $listeParent = Atexo_EntityPurchase::getAllParents($service->getId(), $organisme->getAcronyme());
                                $cheminDir = $pathDirArchiveOrganisme;
                                for ($index = count($listeParent) - 1; $index >= 0; --$index) {
                                    if ($listeParent[$index]['id']) {
                                        //le "id" du tableau listeParent renvoi l'identifiant du service
                                        $cheminDir .= '/'.$listeParent[$index]['id'];
                                        if (!is_dir($cheminDir)) {
                                            system('mkdir '.escapeshellarg($cheminDir));
                                        }
                                    }
                                }
                                $cheminDir .= '/'.$service->getId();
                                if (!is_dir($cheminDir)) {
                                    system('mkdir '.escapeshellarg($cheminDir));
                                }
                            }
                        }
                        $log = "\r\n-> L'arborescence des repertoires a été créé ....100%";
                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                       echo $log;

                        $c = new Criteria();
                        if (isset($_GET['etatCons']) && $_GET['etatCons']) {
                            $c->add(CommonConsultationPeer::ID_ETAT_CONSULTATION, $_GET['etatCons']);
                        } else {
                            $c->add(CommonConsultationPeer::ID_ETAT_CONSULTATION, Atexo_Config::getParameter('STATUS_A_ARCHIVER'));
                        }

                        $c->add(CommonConsultationPeer::ORGANISME, $organisme->getAcronyme());
                        if (isset($_GET['annuel'])) {
                            $date = date('Y') - 2;
                            $c->add(CommonConsultationPeer::DATEFIN, $date.'-12-31', Criteria::LESS_EQUAL);
                        } else {
                            if (isset($_GET['date_debut']) && !isset($_GET['date_fin'])) {
                                $c->add(CommonConsultationPeer::DATEFIN, $_GET['date_debut'], Criteria::GREATER_EQUAL);
                            }
                            if (isset($_GET['date_fin']) && !isset($_GET['date_debut'])) {
                                $c->add(CommonConsultationPeer::DATEFIN, $_GET['date_fin'], Criteria::LESS_EQUAL);
                            }
                            if (isset($_GET['date_debut']) && isset($_GET['date_fin'])) {
                                $c->add(CommonConsultationPeer::DATEFIN, $_GET['date_debut'], Criteria::GREATER_EQUAL);
                                $c->addAnd(CommonConsultationPeer::DATEFIN, $_GET['date_fin'], Criteria::LESS_EQUAL);
                            }

                            if (isset($_GET['date_archivage_debut']) && !isset($_GET['date_archivage_fin'])) {
                                $c->add(CommonConsultationPeer::DATE_ARCHIVAGE, $_GET['date_archivage_debut'], Criteria::GREATER_EQUAL);
                            }
                            if (isset($_GET['date_archivage_fin']) && !isset($_GET['date_archivage_debut'])) {
                                $c->add(CommonConsultationPeer::DATE_ARCHIVAGE, $_GET['date_fin'], Criteria::LESS_EQUAL);
                            }
                            if (isset($_GET['date_archivage_debut']) && isset($_GET['date_archivage_fin'])) {
                                $c->add(CommonConsultationPeer::DATE_ARCHIVAGE, $_GET['date_archivage_debut'], Criteria::GREATER_EQUAL);
                                $c->addAnd(CommonConsultationPeer::DATE_ARCHIVAGE, $_GET['date_archivage_fin'], Criteria::LESS_EQUAL);
                            }

                            if (isset($_GET['org']) && isset($_GET['id'])) {
                                $c->add(CommonConsultationPeer::ID, $_GET['id']);
                                $c->add(CommonConsultationPeer::ORGANISME, $_GET['org']);
                            }
                        }
                        $c->add(CommonConsultationPeer::ID_TYPE_AVIS, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                        $c->add(CommonConsultationPeer::CONSULTATION_EXTERNE, 0);
                        if (isset($_GET['service_id'])) {
                            $c->add(CommonConsultationPeer::SERVICE_ID, $_GET['service_id']);
                        }

                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                            .Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $consultations = CommonConsultationPeer::doSelect($c, $connexion);
                        Propel::close();

                        $idService = '';
                        $nbreConsultations = count($consultations);
                        $numConsultationEncoursTraitement = 0;
                        $nbreConsultationArchivee = 0;
                        $log = '-> Il y a '.$nbreConsultations." à traiter \r\n";
                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                        echo $log;

                        $params['nbFlux'] = $nbreConsultations;
                        foreach ($consultations as $consultation) {
                            try {
                                $resultConsultation = null;

                                $paramsConsultation = [];
                                $paramsConsultation['service'] = 'MPE';
                                $paramsConsultation['nomBatch'] = 'Cli_ArchiverConsultations';

                                $echangesInterfaces = null;
                                $paramsConsultation['oversight'] = self::oversight($paramsConsultation);
                                $paramsConsultation['poids'] = 0;

                                $numConsultationEncoursTraitement = $numConsultationEncoursTraitement + 1;
                                $log = "-> Démarragee traitement consultation : numero "
                                    . $numConsultationEncoursTraitement
                                    . "/" . $nbreConsultations . " id=" . $consultation->getId()
                                    . "; refUser=" . $consultation->getReferenceUtilisateur()
                                    . " dateFin=" . $consultation->getDatefin() . "\r\n";
                               Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                echo $log;
                                $cheminDir = $pathDirArchiveOrganisme;
                                if (Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && $consultation->getDateFin()) {
                                    $cheminDir .= '/'.Atexo_Util::getAnneeFromDate($consultation->getDateFin());
                                }
                                if ($consultation->getServiceId()) {
                                    $c = new Criteria();
                                    $c->add(CommonServicePeer::ID, $consultation->getServiceId());
                                    $c->add(CommonServicePeer::ORGANISME, $organisme->getAcronyme());

                                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                                        .Atexo_Config::getParameter('CONST_READ_WRITE'));
                                    $service = CommonServicePeer::doSelectOne($c, $connexion);
                                    Propel::close();

                                    if ($service) {
                                        $idService = $service->getId();
                                        $listeParent = Atexo_EntityPurchase::getAllParents($service->getId(), $organisme->getAcronyme());
                                        for ($index = count($listeParent) - 1; $index >= 0; --$index) {
                                            if ($listeParent[$index]['id']) {
                                                $cheminDir .= '/'.$listeParent[$index]['id'];
                                            }
                                        }
                                        $cheminDir .= '/'.$service->getId();
                                    }
                                } else {
                                    $idService = '0';
                                }

                                if (Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && !is_dir($cheminDir)) {
                                    $log = '-> La création du chemin  '.escapeshellarg($cheminDir)."\n";
                                   Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                    echo $log;
                                    if (!mkdir($cheminDir, 0755, true)) {
                                        $log = '-> Problème lors de la création du dossier '.escapeshellarg($cheminDir)."\n";
                                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                        throw new Exception(' Problème lors de la création du dossier '.escapeshellarg($cheminDir)."\n");
                                    }
                                }

                                $nomFichier = (new Atexo_Files())->getNomFileXmlAndZip($consultation, $idService);

                                //si jamais on a renseigné dans le paramétrage de l'application qu'il faut mettre dans le nom du fichier d'archive
                                //le siren et le siren ou l'acronyme si les infos non renseignees
                                if (Atexo_Config::getParameter('NOM_FICHIER_ARCHIVE_AVEC_ACRONYME_SIREN')) {
                                    $nomFichier = $organisme->getAcronyme().'_';
                                    $siren = $organisme->getSiren();
                                    if ($siren) {
                                        $nomFichier = $siren.'_';
                                    }
                                }
                                $nomFichierZip = $nomFichier.'.zip';

                                if (Atexo_Module::isEnabled('ArchiveParLot') && $consultation->getAlloti()) {
                                    $listeLots = $consultation->getAllLots();
                                    if (is_countable($listeLots) ? count($listeLots) : 0) {
                                        foreach ($listeLots as $unLot) {
                                            $nomFichierZipLot = Atexo_Consultation_Archive::getNomArchiveLot($nomFichier, $unLot->getLot());

                                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                                                . Atexo_Config::getParameter('CONST_READ_WRITE'));
                                            self::createArchiveZip($consultation,
                                                $organisme->getAcronyme(),
                                                $cheminDir,
                                                $nomFichierZipLot,
                                                $unLot->getLot(),
                                                $connexion,
                                                $log
                                            );

                                            Propel::close();
                                        }
                                    }
                                }

                                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                                    . Atexo_Config::getParameter('CONST_READ_WRITE'));

                                self::createArchiveZip($consultation,
                                    $organisme->getAcronyme(),
                                    $cheminDir,
                                    $nomFichierZip,
                                    false,
                                    $connexion,
                                    $log
                                );
                                Propel::close();

                                if (is_file($cheminDir.'/'.$nomFichierZip)) {
                                    if (!isset($_GET['etatCons'])) {
                                        $paramsConsultation['poids'] = filesize($cheminDir.'/'.$nomFichierZip);
                                        $resultConsultation = 'archive complète';
                                        $paramsConsultation['code'] = Atexo_Oversight_Oversight::$codeSuccess;

                                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                                            .Atexo_Config::getParameter('CONST_READ_WRITE'));
                                        $consultation = CommonConsultationPeer::retrieveByPk($consultation->getId(), $connexion);
                                        $connexion->beginTransaction();

                                        $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));
                                        $consultation->setDateArchivage(date('Y-m-d H:i:s'));
                                        $consultation->save($connexion);

                                        $log = "\t\t-> Passage de la consultation à létat : Archive réalisé\r\n";
                                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                        echo $log;
                                        $nbreConsultationArchivee = $nbreConsultationArchivee + 1;
                                        // ce module une fois activé permet d'envoyer les archives au système d'archivage externe de la plate-forme

                                        $chemin_archive = $cheminDir.'/'.$nomFichierZip;

                                        $consultationArchive = (new CommonConsultationArchiveQuery())
                                            ->filterByConsultationId($consultation->getId())
                                            ->findOne($connexion);
                                        //si jamais l'entrée n'existe pas dans la table consultation_archive alors on en créé une
                                        //si elle existe,on ne fait rien
                                        if (!($consultationArchive instanceof CommonConsultationArchive)) {
                                            $consultationArchive = new CommonConsultationArchive();
                                            $consultationArchive->setOrganisme($consultation->getOrganisme());
                                            $consultationArchive->setConsultationId($consultation->getId());
                                            $consultationArchive->setCheminFichier("./" . substr($chemin_archive, strlen($fichiersArchivesDirectory)));
                                            $consultationArchive->setDateArchivage($consultation->getDateArchivage());
                                            $consultationArchive->setPoidsArchivage(filesize($chemin_archive));
                                            [$y, $m, $d] = explode('-', $consultation->getDatedebut());
                                            $consultationArchive->setAnneeCreationConsultation($y);
                                            if ($module instanceof CommonConfigurationOrganisme && $module->getArchivageConsultationSaeExterneEnvoiArchive()) {
                                                $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_NON_DEMARREE'));
                                            } else {
                                                $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_NON_CONCERNEE'));
                                            }

                                            $consultationArchive->save($connexion);
                                            $log = "\t\t-> Enregistrement des données dans la table consultation_archive\r\n";
                                           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                            echo $log;

                                            if ($module instanceof CommonConfigurationOrganisme && $module->getInterfaceArchiveArcadePmi() && !isset($_GET['sansArcade'])) {
                                                $consultationArchiveArcade = new CommonConsultationArchiveArcade();
                                                $consultationArchiveArcade->setConsultationArchiveId($consultationArchive->getId());
                                                $consultationArchiveArcade->save($connexion);
                                                $log = "\t\t-> Enregistrement des données dans la table consultation_archive_arcade\r\n";
                                                Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                                echo $log;
                                            }
                                        }
                                        $connexion->commit();
                                    }
                                } else {
                                    $log = "\t\t-> Erreur lors du déplacement du zip, la consultation ne passe pas à l'état Archive réalisé\r\n";
                                   Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                    echo $log;
                                    $resultConsultation = "Erreur lors du déplacement du zip, la consultation ne passe pas à l'état Archive réalisé";
                                    $paramsConsultation['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                }
                            }//fin try
                            catch (\Exception $e) {
                                $log = "-> erreur : " . $e->getMessage() . ' ' . $e->getTraceAsString();
                               Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                echo $log;

                                $resultConsultation = 'Erreur : '.$e->getMessage();
                                $paramsConsultation['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                $connexion->rollBack();
                            }

                            Propel::close();

                            self::oversight($paramsConsultation);

                            unset($paramsConsultation['oversight']);
                        }//fin foreach

                        if (isset($_GET['annuel'])) {
                            //si des archives ont été réalisées alors on génére le tar.gz sinon on ne fait rien
                            if ($nbreConsultationArchivee > 0) {
                                system('cd '.escapeshellarg($pathDirArchiveOrganisme).' ; cd ../; tar cvfz '
                                    .escapeshellarg($organisme->getAcronyme().'_archives_'
                                        .(date('Y') - 2).'.tar.gz '.(date('Y') - 2)));
                                self::loggueOrganismeArchivee($organisme->getAcronyme());
                                $log = "\t\t-> Création de l'archive annuel : ".$organisme->getAcronyme()
                                    .'_archives_'.(date('Y') - 2).' dans le répertoire '
                                    .$pathDirArchiveOrganisme.'/'."\r\n\r\n";
                               Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                echo $log;
                            } else {
                                $log = "\t\t-> Aucune consultation archivéé, pas de création de l'archive annuelle\r\n\r\n";
                               Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                                echo $log;
                            }

                            //on remonte d'un cran pour supprimer les fichiers individuels
                            system('rm -rf '.escapeshellarg($pathDirArchiveOrganisme));
                            $log = "\t\t-> suppression du répertoire d'archives individuelles : ".$pathDirArchiveOrganisme."\r\n\r\n";
                           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                            echo $log;                        }
                    } catch (Exception $e) {
                        $log = '-> erreur : '.$e->getMessage() . ' ' . $e->getTraceAsString();
                       Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
                        echo $log;
                        echo $e;

                        $result = 'erreur : '.$e->getMessage();
                        $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    }

                    if (filesize($nomFichierLog)) {
                        $log = file_get_contents($nomFichierLog);
                        $message = Atexo_Message::getGenericEmailFormat($log);
                        Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'Rapport de generation Archives', $message, '', '', false, true);
                    }
                }//fin if isOrganismeArchivee
                else {
                    echo '-> ---------------------- Organisme : '.$organisme->getDenominationOrg().'/'.$organisme->getAcronyme()." déjà archive--------------\r\n";
                    $result = '-> ---------------------- Organisme : '.$organisme->getDenominationOrg().'/'.$organisme->getAcronyme()." déjà archive--------------\r\n";

                    $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                }
                self::oversight($params);
                unset($params['oversight']);
            }//fin de foreach organismes
        }
    }

    private static function isOrganismeArchivee($acronymeOrganisme, $forceArchivage = false)
    {
        $organismeArchivee = false;
        $fileLog = Atexo_Config::getParameter('FICHIERS_ARCHIVE_LOG_DIR').'/fichierOrganismeTraites.csv';
        if (is_file($fileLog)) {
            $fichierOrganismesTraitesHandle = fopen($fileLog, 'r+');
            //une ligne $data comprend une info de type acronyme_organisme;1 si l'archive a été réalisée
            while (($data = fgetcsv($fichierOrganismesTraitesHandle, 100, ';')) !== false) {
                //organisme trouve et archive
                if (($acronymeOrganisme == $data[0]) && '1' == $data[1]) {
                    $organismeArchivee = true;
                    break;
                }
            }

            fclose($fichierOrganismesTraitesHandle);
        }
        if ($forceArchivage) {
            $organismeArchivee = true;
        }

        return $organismeArchivee;
    }

    private static function loggueOrganismeArchivee($acronymeOrganisme)
    {
        $fichierOrganismesTraitesHandle = fopen(Atexo_Config::getParameter('FICHIERS_ARCHIVE_LOG_DIR').'/fichierOrganismeTraites.csv', 'a+');

        //une ligne $data comprend une info de type acronyme_organisme;1 si l'archive a été réalisée
        //sinon on écrit dans le fichier la ligne
        $data = $acronymeOrganisme.";1\n";
        fwrite($fichierOrganismesTraitesHandle, $data);
        fclose($fichierOrganismesTraitesHandle);
    }

    /**
     * permet de creer le fichier zip de l'archive.
     *
     * @param CommonConsultation $consultation  objet CommonConsultation
     * @param string             $orgAccronyme  l'accronyme de l'organisme
     * @param string             $cheminDir     lerepertoire ou mettre l'archive
     * @param string             $nomFichierZip le nom du fichier zip
     * @param string             $numlot        le numero du lot
     * @param string             $connexion     la connexion à utiliser pour l'acces à la BD
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */

    public static function createArchiveZip($consultation, $orgAccronyme, $cheminDir, $nomFichierZip, $numlot = false, $connexion = false, $log = null)
    {
        try {
            $directoryLog = Atexo_Config::getParameter('FICHIERS_ARCHIVE_LOG_DIR');
            $nomFichierLog = $directoryLog . '/log_CronArchive_' . $orgAccronyme . Atexo_Util::getDateAujourdhui2();
            $tree = new Atexo_Consultation_Tree_Arborescence($consultation->getId(), $connexion, $orgAccronyme, true, $numlot);
            $pathZipCreated = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, Atexo_Config::getParameter("COMMON_TMP"), $consultation, $orgAccronyme, "archive.xml", true, $numlot,
                $connexion, true);
            $log = "\t\t-> Création du fichier zip " . $nomFichierZip . " : OK ....\r\n";
           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
            echo $log;
            system("mv " . $pathZipCreated . " " . escapeshellarg($cheminDir . "/" . $nomFichierZip));

            $log = "\t\t-> Déplacement du fichier zip ".$nomFichierZip.' dans le repertoire correspondant : '.$cheminDir."....\r\n";
           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
            echo $log;
        //générer l'empreinte md5 et la stocker au même niveau que l'archive initiale
        if (is_file($cheminDir.'/'.$nomFichierZip)) {
            $msgGenerique = "\t\t-> Création de l'empreinte %s du fichier zip ".$nomFichierZip.' dans le repertoire correspondant : '.$cheminDir."....\r\n";
            $empreinte_md5 = hash_file('md5', $cheminDir.'/'.$nomFichierZip);
            $fileNameArchive_md5 = $cheminDir.'/'.$nomFichierZip.'.md5';
            Atexo_Util::writeFile($fileNameArchive_md5, $empreinte_md5);
            $msgMd5 = sprintf($msgGenerique, 'md5');
            $log = $msgMd5;
           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
            echo $log;
            $empreinte_sha256 = hash_file('sha256', $cheminDir.'/'.$nomFichierZip);
            $fileNameArchive_sha256 = $cheminDir.'/'.$nomFichierZip.'.sha256';
            Atexo_Util::writeFile($fileNameArchive_sha256, $empreinte_sha256);
            $msgSha256 = sprintf($msgGenerique, 'sha256');
            $log = $msgSha256;
           Atexo_Util::writeLogFile($directoryLog, $nomFichierLog, $log, 'a+');
            echo $log;
        }

        } catch (\Exception $e) {
            throw $e;
        }

    }
}
