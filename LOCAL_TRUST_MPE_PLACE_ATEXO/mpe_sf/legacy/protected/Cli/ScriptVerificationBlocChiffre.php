<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Supprime les blobs chiffes depuis date, il prend en parametre le mode et la date.
 *
 * @category Atexo
 */
class Cli_ScriptVerificationBlocChiffre extends Atexo_Cli
{
    public static function run()
    {
        $sql = <<<QUERY
SELECT
        blocFichierEnveloppe.id_bloc_fichier,
        blocFichierEnveloppe.id_blob_chiffre,
        blocFichierEnveloppe.organisme,
        fichierEnveloppe.id_fichier,
        fichierEnveloppe.id_enveloppe,
        Offres.id,
        Offres.consultation_id,
        Offres.date_depot,
        Offres.date_fin_chiffrement,
        Offres.statut_offres,
        Enveloppe.statut_enveloppe,
        consultation.date_mise_en_ligne_calcule,
        consultation.datefin
FROM
        blocFichierEnveloppe ,
        fichierEnveloppe,
        Enveloppe,
        Offres,
        consultation
WHERE
        blocFichierEnveloppe.id_fichier = fichierEnveloppe.id_fichier
    AND
        blocFichierEnveloppe.organisme = fichierEnveloppe.organisme
    AND
        fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
    AND
        fichierEnveloppe.organisme = Enveloppe.organisme
    AND
        Enveloppe.offre_id = Offres.id
    AND
        Enveloppe.organisme = Offres.organisme
    AND
        Offres.statut_offres in( 1, 9, 2)
    AND
        Offres.date_depot IS NOT NULL
    AND
        Offres.consultation_id = consultation.id
    AND
        Offres.organisme = consultation.organisme
QUERY;

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute();
        $file = Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/verificationBlocChiffre'.date('YmdHis').'.csv';
        $entete = "Organisme;Consultation;date mise en ligne;date fin;Offre;statut Offre;date depot;date fin chiffrement;Enveloppe;statut enveloppe;Fichier;BlocFichierEnveloppe;file;Statut blob;\n";
        Atexo_Util::write_file($file, $entete, 'a');
        while ($fichier = $statement->fetch(PDO::FETCH_ASSOC)) {
            $pathFile = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$fichier['organisme'].'/files/'.$fichier['id_blob_chiffre'].'-0';
            if (!file_exists($pathFile)) {
                $pathFile = Atexo_Blob::getPathFile($fichier['id_blob_chiffre'], $fichier['organisme']);
            }
            $found = is_file($pathFile) ? 'TROUVE' : 'NON TROUVE';
            $log = $fichier['organisme'].';'.$fichier['consultation_id'].';'.$fichier['date_mise_en_ligne_calcule'].';'.$fichier['datefin'].';'.$fichier['id'].';'.$fichier['statut_offres'].';'.$fichier['date_depot'].';'.$fichier['date_fin_chiffrement'].';'.$fichier['id_enveloppe'].';'.$fichier['statut_enveloppe'].';'.$fichier['id_fichier'].';'.$fichier['id_bloc_fichier'].';'.$pathFile.';'.$found.";\n";
            Atexo_Util::write_file($file, $log, 'a');
            echo $log;
            unset($fichier);
        }
        echo 'le log est enregisté dans le chemin : '.$file."\n";

        return true;
    }
}
