<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Exception;

/**
 * Cron de calcul des entrees actives de Referentiel_org_denomination.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 *
 * @category Atexo
 *
 * @copyright Atexo 2012
 */
class Cli_ComputeReferentielOrgDenomination extends Atexo_Cli
{
    public static function run()
    {
        try {
            $requeteSelect = "SELECT DISTINCT (`denomination_adapte`), org_denomination FROM `consultation` WHERE  `consultation`.`denomination_adapte` != '' AND `consultation`.`datefin` >= NOW() AND `consultation`.`date_mise_en_ligne_calcule` <= NOW()";

            echo "Lister dans la table 'Consultation' ensemble des clés valeurs uniques de 'denomination_adapte',org_denomination pour lesquelles datefin >= NOW() \n
	        	 date_mise_en_ligne_calcule<= NOW(). On ne recherche recherche que les clés valeurs des consultations visible des entreprises et non cloturées";

            $result = Atexo_Db::getLinkCommon()->query($requeteSelect);

            echo 'requete passée avec succès, il y a '.(is_countable($result) ? count($result) : 0).' résultat trouvé';

            echo "démarrage de l'insertion des entrées si non existantes dans la table Referentiel_org_denomination";
            foreach ($result as $row) {
                (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomination($row['denomination_adapte'], $row['org_denomination']);
            }
            echo "fin de l'insertion des entrées si non existantes dans la table Referentiel_org_denomination";

            $requete = "UPDATE `Referentiel_org_denomination` set `Referentiel_org_denomination`.`actif_recherche_avancee`='1', `Referentiel_org_denomination`.`date_maj_actif_recherche_avancee`=NOW() where `Referentiel_org_denomination`.`denomination_adapte` in (SELECT DISTINCT `denomination_adapte` FROM `consultation` WHERE  `consultation`.`denomination_adapte` != '' AND `consultation`.`datefin` >= DATE_SUB(NOW(), INTERVAL 1 HOUR))";

            echo "Lister dans la table 'Consultation' ensemble des clés valeurs uniques de 'denomination_adapte' pour lesquelles datefin > NOW()-1H \n 
			aller dans Table Referentiel_org_denomination et pour chaque valeur de l'extrait précédent, MAJ de ARA à 1, MAJ de Date_MAJ_ARA à NOW\n
			passage de la requête suivante : ".$requete;

            Atexo_Db::getLinkCommon()->query($requete);

            echo 'requete passée avec succès';

            $requete = "UPDATE `Referentiel_org_denomination` set `Referentiel_org_denomination`.`actif_recherche_avancee`='0', `Referentiel_org_denomination`.`date_maj_actif_recherche_avancee`=NOW() where `Referentiel_org_denomination`.`actif_recherche_avancee`='1' AND `Referentiel_org_denomination`.`date_maj_actif_recherche_avancee` < DATE_SUB(NOW(), INTERVAL 1 HOUR)";

            echo "Une fois la liste traitée, passer à ARA = 0 et Date_MAJ_ARA à NOW pour toutes les lignes pour lequelles ARA = 1 ET Date_MAJ_ARA < NOW - 1H\n
			passage de la requête suivante : ".$requete;

            Atexo_Db::getLinkCommon()->query($requete);

            echo 'requete passée avec succès';
        } catch (Exception $e) {
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }
}
