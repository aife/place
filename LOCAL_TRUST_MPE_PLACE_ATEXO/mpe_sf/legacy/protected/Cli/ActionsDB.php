<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use Exception;

class Cli_ActionsDB extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter l'un des params : \r\n";
            echo "\t-> 1- launchActionsConsultations pour les champs de la consultation \r\n";
            echo "\t-> 2- updateAgentPassword pour la modification du mot de passe \r\n";
            echo "\t-> 3- migrerPathService pour les champs de service\r\n";
            echo "\t-> 4- migrerModuleCommunEtModuleOrganisme pour l insertion \r\n";
            echo "\t-> 5- viderAncienChamp pour vider les anciens champs \r\n";
            exit;
        }
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if ('launchActionsConsultations' == $arguments[$i]) {
                    self::launchActionsConsultations();
                    echo "Fin Actions Consultations \r\n";
                }
                if ('updateAgentPassword' == $arguments[$i]) {
                    self::updateAgentPassword();
                    echo "Fin Changement Password \r\n";
                }
                if ('migrerPathService' == $arguments[$i]) {
                    self::migrerPathService();
                    echo "Fin Migration Path service\r\n";
                }
                if ('migrerModuleCommunEtModuleOrganisme' == $arguments[$i]) {
                    self::migrerModuleCommunEtModuleOrganisme();
                    echo "Fin Migration Module Commune Et Module Organisme \r\n";
                }
                if ('viderAncienChamp' == $arguments[$i]) {
                    self::viderAncienChamp();
                    echo "Fin de vidage des anciens champs \r\n";
                }
            }
        }
    }

    public function viderAncienChamp()
    {
        $sqlCons = " UPDATE consultation SET titre='', ";
        $sqlCons .= " titre_fr='', ";
        $sqlCons .= " titre_en='', ";
        $sqlCons .= " titre_es='', ";
        $sqlCons .= " titre_su='', ";
        $sqlCons .= " titre_du='', ";
        $sqlCons .= " titre_cz='', ";
        $sqlCons .= " titre_ar='', ";
        $sqlCons .= " titre_it='', ";
        $sqlCons .= " resume='', ";
        $sqlCons .= " resume_fr='', ";
        $sqlCons .= " resume_en='', ";
        $sqlCons .= " resume_es='', ";
        $sqlCons .= " resume_su='', ";
        $sqlCons .= " resume_du='', ";
        $sqlCons .= " resume_cz='', ";
        $sqlCons .= " resume_it='', ";
        $sqlCons .= " type_procedure='', ";
        $sqlCons .= " reponse_electronique='', ";
        $sqlCons .= " id_type_validation='', ";
        $sqlCons .= " type_mise_en_ligne=''; ";

        $sqlAgent = "UPDATE Agent SET mdp=''; ";

        $sqlService = " UPDATE Service SET path_service='', ";
        $sqlService .= " path_service_fr='', ";
        $sqlService .= " path_service_en='', ";
        $sqlService .= " path_service_es='', ";
        $sqlService .= " path_service_su='', ";
        $sqlService .= " path_service_du='', ";
        $sqlService .= " path_service_cz='', ";
        $sqlService .= " path_service_it='', ";
        $sqlService .= " path_service_ar='' ;";
        try {
            Atexo_Db::getLinkCommon()->query($sqlAgent);
            echo "Agent : champ mdp vide \r\n";
            Atexo_Db::getLinkCommon()->query($sqlCons);
            echo "CommonConsultation  : champs vides \r\n";
            Atexo_Db::getLinkCommon()->query(' TRUNCATE TABLE  `ModuleCommun`');
            echo "ModuleCommun  : table vide \r\n";
            Atexo_Db::getLinkCommon()->query(' TRUNCATE TABLE  `ModuleOrganisme`');
            echo "ModuleOrganisme  : table vide \r\n";
        } catch (Exception $ex) {
            Propel::log(' - erreur exécution - ', Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
            echo $ex->getMessage()."\r\n";
        }
        $allOrganismes = (new Atexo_Organismes())->retrieveAllOrganismes();
        foreach ($allOrganismes as $org) {
            try {
                Atexo_Db::getLinkOrganism($org->getAcronyme())->query($sqlService);
                echo 'Service ('.$org->getAcronyme().") : champs pathService vide \r\n";
                Atexo_Db::getLinkOrganism($org->getAcronyme())->query($sqlCons);
                echo 'Consultation ('.$org->getAcronyme().") : champs vides \r\n";
                Atexo_Db::closeOrganism($org->getAcronyme());
            } catch (Exception $ex) {
                Propel::log($org->getAcronyme().' - erreur exécution - ', Propel::LOG_ERR);
                Propel::log($ex->getMessage(), Propel::LOG_ERR);
                echo $ex->getMessage()."\r\n";
            }
        }
    }

    public function launchActionsConsultations()
    {
        $sql = ' UPDATE consultation SET intitule = titre, ';
        $sql .= ' intitule_fr = titre_fr, ';
        $sql .= ' intitule_en = titre_en, ';
        $sql .= ' intitule_es = titre_es, ';
        $sql .= ' intitule_su = titre_su, ';
        $sql .= ' intitule_du = titre_du, ';
        $sql .= ' intitule_cz = titre_cz, ';
        $sql .= ' intitule_ar = titre_ar, ';
        $sql .= ' intitule_it = titre_it, ';
        $sql .= ' objet = resume, ';
        $sql .= ' objet_fr = resume_fr, ';
        $sql .= ' objet_en = resume_en, ';
        $sql .= ' objet_es = resume_es, ';
        $sql .= ' objet_su = resume_su, ';
        $sql .= ' objet_du = resume_du, ';
        $sql .= ' objet_cz = resume_cz, ';
        $sql .= ' objet_it = resume_it, ';
        $sql .= ' type_acces = type_procedure, ';
        $sql .= ' autoriser_reponse_electronique  = reponse_electronique, ';
        $sql .= ' id_regle_validation  = id_type_validation, ';
        $sql .= ' regle_mise_en_ligne = type_mise_en_ligne ';

        try {
            Atexo_Db::getLinkCommon()->query($sql);
            echo "CommonConsultation  : champs MAJ \r\n";
        } catch (Exception $ex) {
            Propel::log(' - erreur exécution - ', Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
            echo $ex->getMessage()."\r\n";
        }

        //Get all organismes:
        $allOrganismes = (new Atexo_Organismes())->retrieveAllOrganismes();
        foreach ($allOrganismes as $org) {
            try {
                //Get all $org Consultations:
                Atexo_Db::getLinkOrganism($org->getAcronyme())->query($sql);
                echo 'Consultation ('.$org->getAcronyme().") : champs MAJ \r\n";
                Atexo_Db::closeOrganism($org->getAcronyme());
            } catch (Exception $ex) {
                Propel::log(' - erreur exécution - ', Propel::LOG_ERR);
                Propel::log($ex->getMessage(), Propel::LOG_ERR);
                echo $ex->getMessage()."\r\n";
            }
        }
    }

    public function updateAgentpassword()
    {
        $sql = 'UPDATE Agent SET password = mdp ';
        Atexo_Db::getLinkCommon()->query($sql);
    }

    public function migrerPathService()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                try {
                    $sql = ' UPDATE Service SET chemin_complet = path_service, ';
                    $sql .= ' chemin_complet_fr = path_service_fr, ';
                    $sql .= ' chemin_complet_en = path_service_en, ';
                    $sql .= ' chemin_complet_es = path_service_es, ';
                    $sql .= ' chemin_complet_su = path_service_su, ';
                    $sql .= ' chemin_complet_du = path_service_du, ';
                    $sql .= ' chemin_complet_cz = path_service_cz, ';
                    $sql .= ' chemin_complet_it = path_service_it, ';
                    $sql .= ' chemin_complet_ar = path_service_ar ';

                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                    echo 'Service ('.$uneOrganisme->getAcronyme().") : champs MAJ \r\n";
                } catch (Exception $ex) {
                    echo "\t KO \n".$sql;
                    Propel::log($uneOrganisme->getAcronyme().' - erreur exécution requête : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public function migrerModuleCommunEtModuleOrganisme()
    {
        $sqlMC = ' INSERT INTO configuration_plateforme (`code_cpv`, `multi_linguisme_entreprise`, `gestion_fournisseurs_docs_mes_sous_services`, `authenticate_inscrit_by_cert`, `authenticate_inscrit_by_login`, `base_qualifiee_entreprise_insee`, `gestion_boamp_mes_sous_services`, `gestion_bi_cle_mes_sous_services`, `nom_entreprise_toujours_visible`, `gestion_jal_mes_sous_services`, `choix_langue_affichage_consultation`, `compte_entreprise_donnees_complementaires`, `annuaire_entites_achat_visible_par_entreprise`, `affichage_recherche_avancee_agent_ac_sad_transversaux`, `encheres_entreprise`, `socle_interne`, `module_certificat`, `socle_externe_agent`, `afficher_image_organisme`, `socle_externe_entreprise`, `portail_defense_entreprise`, `compte_entreprise_province`, `compte_entreprise_telephone3`, `compte_entreprise_tax_prof`, `compte_entreprise_rcville`, `compte_entreprise_declaration_honneur`, `compte_entreprise_qualification`, `compte_entreprise_moyens_techniques`, `compte_entreprise_prestations_realisees`, `compte_entreprise_chiffre_affaire_production_biens_services`, `enveloppe_offre_technique`, `compte_inscrit_choix_profil`, `procedure_adaptee`, `compte_entreprise_siren`, `compte_entreprise_activation_inscription_par_agent`, `menu_entreprise_consultations_en_cours`, `compte_entreprise_capital_social`, `mail_activation_compte_inscrit_entreprise`, `decision_date_notification`, `decision_pmi_pme`, `decision_nature_prestations`, `decision_objet_marche`, `decision_note`, `decision_fiche_recensement`, `registre_papier_mail_obligatoire`, `menu_entreprise_indicateurs_cles`, `ajout_rpa_champ_email`, `ajout_rpa_champ_telephone`, `ajout_rpa_champ_fax`, `entreprise_poser_question_sans_pj`, `url_demarche_agent`, `url_demarche_entreprise`, `siret_detail_entite_achat`, `presence_elu`, `gerer_mon_service`, `depouillement_enveloppe_depend_RAT_enveloppe_precedente`, `consultation_adresse_retrais_dossiers`, `consultation_adresse_depot_offres`, `consultation_caution_provisoire`, `consultation_lieu_ouverture_plis`, `consultation_qualification`, `consultation_agrement`, `consultation_echantillons_demandes`, `consultation_reunion`, `consultation_visite_des_lieux`, `consultation_prix_acquisition`, `resultat_analyse_avant_decision`, `creation_inscrit_par_ates`, `consultation_variantes_autorisees`, `recherche_avancee_par_type_org`, `menu_agent_societes_exclues`, `recherche_avancee_par_domaines_activite`, `recherche_avancee_par_qualification`, `recherche_avancee_par_agrement`, `contact_administratif_dans_detail_consultation_cote_entreprise`, `consultation_pieces_dossiers`, `gerer_adresses_service`, `traduire_annonces`, `afficher_bloc_actions_dans_details_annonces`, `autoriser_une_seule_reponse_principale_par_entreprise`, `generation_avis`, `passation_appliquer_donnees_ensemble_lots`, `autre_annonce_extrait_pv`, `autre_annonce_rapport_achevement`, `ajout_fichier_joint_autre_annonce`, `consultation_mode_passation`, `compte_entreprise_identifiant_unique`, `gerer_certificats_agent`, `autre_annonce_programme_previsionnel`, `annuler_consultation`, `cfe_entreprise_accessible_par_agent`, `compte_entreprise_code_nace_referentiel`, `code_nut_lt_referentiel`, `lieux_execution`, `compte_entreprise_domaine_activite_lt_referentiel`, `consultation_domaines_activites_lt_referentiel`, `compte_entreprise_agrement_lt_referentiel`, `compte_entreprise_qualification_lt_referentiel`, `reponse_pas_a_pas`, `agent_controle_format_mot_de_passe`, `entreprise_validation_email_inscription`, `telecharger_dce_avec_authentification`, `authentification_basic`, `reglement_consultation`, `annonces_marches`, `cfe_date_fin_validite_obligatoire`, `associer_documents_cfe_consultation`, `compte_entreprise_region`, `compte_entreprise_telephone2`, `compte_entreprise_cnss`, `compte_entreprise_rcnum`, `compte_entreprise_domaine_activite`, `compte_inscrit_code_nic`, `compte_entreprise_code_ape`, `compte_entreprise_documents_commerciaux`, `compte_entreprise_agrement`, `compte_entreprise_moyens_humains`, `compte_entreprise_activite_domaine_defense`, `compte_entreprise_donnees_financieres`, `enveloppe_anonymat`, `publicite_format_xml`, `article_133_generation_pf`, `entreprise_repondre_consultation_apres_cloture`, `telechargement_outil_verif_horodatage`, `affichage_code_cpv`, `consultation_domaines_activites`, `statistiques_mesure_demat`, `publication_procure`, `menu_entreprise_toutes_les_consultations`, `compte_entreprise_cp_obligatoire`, `annuler_depot`, `traduire_entite_achat_arabe`, `traduire_organisme_arabe`, `decision_cp`, `decision_tranche_budgetaire`, `decision_classement`, `decision_afficher_detail_candidat_par_defaut`, `article_133_upload_fichier`, `multi_linguisme_agent`, `compte_entreprise_ifu`, `gestion_organisme_par_agent`, `utiliser_lucene`, `utiliser_page_html_lieux_execution`, `prado_validateur_format_date`, `prado_validateur_format_email`, `socle_externe_ppp`, `validation_format_champs_stricte`, `poser_question_necessite_authentification`, `autoriser_modif_profil_inscrit_ates`, `unicite_reference_consultation`, `registre_papier_rcnum_rcville_obligatoires`, `registre_papier_adresse_cp_ville_obligatoires`, `telecharger_dce_sans_identification`, `gestion_entreprise_par_agent`, `autoriser_caracteres_speciaux_dans_reference`, `inscription_libre_entreprise`, `afficher_code_service`, `authenticate_agent_by_login`, `entreprise_controle_format_mot_de_passe`, `autre_annonce_information`, `creer_autre_annonce`, `generer_acte_dengagement`, `authenticate_agent_by_cert`)  SELECT `code_cpv`, `multi_linguisme_entreprise`, `gestion_fournisseurs_docs_mes_sous_services`, `authenticate_inscrit_by_cert`, `authenticate_inscrit_by_login`, `base_qualifiee_entreprise_insee`, `gestion_boamp_mes_sous_services`, `gestion_bi_cle_mes_sous_services`, `nom_entreprise_toujours_visible`, `gestion_jal_mes_sous_services`, `choix_langue_affichage_consultation`, `compte_entreprise_donnees_complementaires`, `annuaire_entites_achat_visible_par_entreprise`, `affichage_recherche_avancee_agent_ac_sad_transversaux`, `encheres_entreprise`, `socle_interne`, `module_certificat`, `socle_externe_agent`, `afficher_image_organisme`, `socle_externe_entreprise`, `portail_defense_entreprise`, `compte_entreprise_province`, `compte_entreprise_telephone3`, `compte_entreprise_tax_prof`, `compte_entreprise_rcville`, `compte_entreprise_declaration_honneur`, `compte_entreprise_qualification`, `compte_entreprise_moyens_techniques`, `compte_entreprise_prestations_realisees`, `compte_entreprise_chiffre_affaire_production_biens_services`, `enveloppe_offre_technique`, `compte_inscrit_choix_profil`, `procedure_adaptee`, `compte_entreprise_siren`, `compte_entreprise_activation_inscription_par_agent`, `menu_entreprise_consultations_en_cours`, `compte_entreprise_capital_social`, `mail_activation_compte_inscrit_entreprise`, `decision_date_notification`, `decision_pmi_pme`, `decision_nature_prestations`, `decision_objet_marche`, `decision_note`, `decision_fiche_recensement`, `registre_papier_mail_obligatoire`, `menu_entreprise_indicateurs_cles`, `ajout_rpa_champ_email`, `ajout_rpa_champ_telephone`, `ajout_rpa_champ_fax`, `entreprise_poser_question_sans_pj`, `url_demarche_agent`, `url_demarche_entreprise`, `siret_detail_entite_achat`, `presence_elu`, `gerer_mon_service`, `depouillement_enveloppe_depend_RAT_enveloppe_precedente`, `consultation_adresse_retrais_dossiers`, `consultation_adresse_depot_offres`, `consultation_caution_provisoire`, `consultation_lieu_ouverture_plis`, `consultation_qualification`, `consultation_agrement`, `consultation_echantillons_demandes`, `consultation_reunion`, `consultation_visite_des_lieux`, `consultation_prix_acquisition`, `resultat_analyse_avant_decision`, `creation_inscrit_par_ates`, `consultation_variantes_autorisees`, `recherche_avancee_par_type_org`, `menu_agent_societes_exclues`, `recherche_avancee_par_domaines_activite`, `recherche_avancee_par_qualification`, `recherche_avancee_par_agrement`, `contact_administratif_dans_detail_consultation_cote_entreprise`, `consultation_pieces_dossiers`, `gerer_adresses_service`, `traduire_annonces`, `afficher_bloc_actions_dans_details_annonces`, `autoriser_une_seule_reponse_principale_par_entreprise`, `generation_avis`, `passation_appliquer_donnees_ensemble_lots`, `autre_annonce_extrait_pv`, `autre_annonce_rapport_achevement`, `ajout_fichier_joint_autre_annonce`, `consultation_mode_passation`, `compte_entreprise_identifiant_unique`, `gerer_certificats_agent`, `autre_annonce_programme_previsionnel`, `annuler_consultation`, `cfe_entreprise_accessible_par_agent`, `compte_entreprise_code_nace_referentiel`, `code_nut_lt_referentiel`, `lieux_execution`, `compte_entreprise_domaine_activite_lt_referentiel`, `consultation_domaines_activites_lt_referentiel`, `compte_entreprise_agrement_lt_referentiel`, `compte_entreprise_qualification_lt_referentiel`, `reponse_pas_a_pas`, `agent_controle_format_mot_de_passe`, `entreprise_validation_email_inscription`, `telecharger_dce_avec_authentification`, `authentification_basic`, `reglement_consultation`, `annonces_marches`, `cfe_date_fin_validite_obligatoire`, `associer_documents_cfe_consultation`, `compte_entreprise_region`, `compte_entreprise_telephone2`, `compte_entreprise_cnss`, `compte_entreprise_rcnum`, `compte_entreprise_domaine_activite`, `compte_inscrit_code_nic`, `compte_entreprise_code_ape`, `compte_entreprise_documents_commerciaux`, `compte_entreprise_agrement`, `compte_entreprise_moyens_humains`, `compte_entreprise_activite_domaine_defense`, `compte_entreprise_donnees_financieres`, `enveloppe_anonymat`, `publicite_format_xml`, `article_133_generation_pf`, `entreprise_repondre_consultation_apres_cloture`, `telechargement_outil_verif_horodatage`, `affichage_code_cpv`, `consultation_domaines_activites`, `statistiques_mesure_demat`, `publication_procure`, `menu_entreprise_toutes_les_consultations`, `compte_entreprise_cp_obligatoire`, `annuler_depot`, `traduire_entite_achat_arabe`, `traduire_organisme_arabe`, `decision_cp`, `decision_tranche_budgetaire`, `decision_classement`, `decision_afficher_detail_candidat_par_defaut`, `article_133_upload_fichier`, `multi_linguisme_agent`, `compte_entreprise_ifu`, `gestion_organisme_par_agent`, `utiliser_lucene`, `utiliser_page_html_lieux_execution`, `prado_validateur_format_date`, `prado_validateur_format_email`, `socle_externe_ppp`, `validation_format_champs_stricte`, `poser_question_necessite_authentification`, `autoriser_modif_profil_inscrit_ates`, `unicite_reference_consultation`, `registre_papier_rcnum_rcville_obligatoires`, `registre_papier_adresse_cp_ville_obligatoires`, `telecharger_dce_sans_identification`, `gestion_entreprise_par_agent`, `autoriser_caracteres_speciaux_dans_reference`, `inscription_libre_entreprise`, `afficher_code_service`, `authenticate_agent_by_login`, `entreprise_controle_format_mot_de_passe`, `autre_annonce_information`, `creer_autre_annonce`, `generer_acte_dengagement`, `authenticate_agent_by_cert` FROM ModuleCommun ;';
        $sqlMO = ' INSERT INTO configuration_organisme (`organisme`, `encheres`, `consultation_pj_autres_pieces_telechargeables`, `no_activex`, `gestion_mapa`, `article_133_upload_fichier`, `centrale_publication`, `organisation_centralisee`, `presence_elu`, `traduire_consultation`, `suivi_passation`, `numerotation_ref_cons`, `pmi_lien_portail_defense_agent`, `interface_archive_arcade_pmi`, `desarchivage_consultation`, `alimentation_automatique_liste_invites`, `interface_chorus_pmi`, `archivage_consultation_sur_pf`, `autoriser_modification_apres_phase_consultation`, `importer_enveloppe`, `export_marches_notifies`, `acces_agents_cfe_bd_fournisseur`, `acces_agents_cfe_ouverture_analyse`) SELECT `organisme`, `encheres`, `consultation_pj_autres_pieces_telechargeables`, `no_activex`, `gestion_mapa`, `article_133_upload_fichier`, `centrale_publication`, `organisation_centralisee`, `presence_elu`, `traduire_consultation`, `suivi_passation`, `numerotation_ref_cons`, `pmi_lien_portail_defense_agent`, `interface_archive_arcade_pmi`, `desarchivage_consultation`, `alimentation_automatique_liste_invites`, `interface_chorus_pmi`, `archivage_consultation_sur_pf`, `autoriser_modification_apres_phase_consultation`, `importer_enveloppe`, `export_marches_notifies`, `acces_agents_cfe_bd_fournisseur`, `acces_agents_cfe_ouverture_analyse` FROM ModuleOrganisme ;';

        try {
            Atexo_Db::getLinkCommon()->query($sqlMC);
            echo "ModuleCommun  : table bien inserer\r\n";
            Atexo_Db::getLinkCommon()->query($sqlMO);
            echo "ModuleOrganisme : table bien inserer\r\n";
        } catch (Exception $ex) {
            echo "\t KO \n";
            Propel::log(' - erreur exécution requête : '.$sqlMC."\n".$sqlMO, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }
}
