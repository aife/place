<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use Exception;

/**
 * Classe de calcule de la date de mise en ligne.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_DateMiseEnLigne extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if ('dateMiseEnligne' == $arguments[$i]) {
                    (new Atexo_Statistiques_RequetesStatistiques())->setDateMiseEnLigneCalcule();
                }
                if ('lowerAcronymeUrl' == $arguments[$i]) {
                    self::lowerAcronymeUrl();
                }
            }
        }
    }

    public static function lowerAcronymeUrl()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::CONSULTATION_EXTERNE, 1, Criteria::EQUAL);
        $arrayConsultations = CommonConsultationPeer::doSelect($c, $connexionCom);
        foreach ($arrayConsultations as $Consultation) {
            try {
                if ($Consultation instanceof CommonConsultation) {
                    $urlConsultation = $Consultation->getUrlConsultationExterne();
                    $arrayUrl = explode('/', $urlConsultation);
                    $urlConsultation = str_replace($arrayUrl[3], strtolower($arrayUrl[3]), $urlConsultation);
                    $Consultation->setUrlConsultationExterne($urlConsultation);
                    $Consultation->save($connexionCom);
                    echo 'Update consultation : '.$Consultation->getOrganisme().' '.$Consultation->getId().' '.$urlConsultation."\n";
                }
            } catch (Exception $e) {
                echo $e;
            }
        }
    }
}
