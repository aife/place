<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\AnnoncesMarches\Atexo_AnnoncesMarches_Dedoblonnage;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Cron de deboblonnage.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 */
class Cli_AnnoncesMarchesDedoblonnage extends Atexo_Cli
{
    public static function run()
    {
        $argument = [];
        try {
            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument[] = $arguments[$i];
                }
            }
            $dateActuelle = date('Y-m-d');
            $date = Atexo_Util::dateMoinJour($dateActuelle, '1');
            if (is_array($argument)) {
                foreach ($argument as $param) {
                    if ($param) {
                        if ('-f' == $param) {
                            $date = false;
                        } elseif (is_numeric($param)) {
                            $date = Atexo_Util::dateMoinJour($dateActuelle, $param);
                        }
                    }
                }
            }
            self::updateConsultation($date);
            echo "Fin de traitement de dedoblonnage \n";
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE').'/dedoblonnage_erreur.log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }

    public static function updateConsultation($date)
    {
        $organism = [];
        $message = null;
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            // fichier de log
            if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE'))) {
                system('mkdir '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE')));
                echo "\r\n-> répertoire suivant créé   ".Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE').'....';
            }
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE').'dedoblonnage_log'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
            // organismes
            $organism[] = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_MAPA');
            $organism[] = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_NATIONALE');
            $organism[] = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_EUROP');
            $nombre = 0;
            foreach ($organism as $org) {
                echo 'organisme en cours : '.$org."  \n";
                $arrayRefConcultation = (new Atexo_Consultation())->getConsultationByOrganismeAndDoublon($org, $date, 0);
                $dedeoblonage = new Atexo_AnnoncesMarches_Dedoblonnage();
                $dedeoblonage->setRegle(1);

                if (is_array($arrayRefConcultation)) {
                    foreach ($arrayRefConcultation as $ref) {
                        $consultation = (new Atexo_Consultation())->retrieveConsultation($ref, $org);
                        $urls = $consultation->getUrlConsultationAchatPublique();
                        $arrayUrls = explode('||', $urls);
                        for ($i = 0; $i < count($arrayUrls); ++$i) {
                            if ($arrayUrls[$i]) {
                                $consultationDoublon = $dedeoblonage->executeRegle(self::adapterFormatUrl($arrayUrls[$i]), $org);
                                if ($consultationDoublon  instanceof CommonConsultation) {
                                    $consultationDoublon->setUrlConsultationAvisPub($consultation->getUrlConsultationAvisPub());
                                    $consultationDoublon->save($connexionCom);
                                    ++$nombre;
                                    $consultation->setDoublon('1');
                                    $idDoublonDe = '#'.$consultationDoublon->getOrganisme().'#'.$consultationDoublon->getId().'#';
                                    $consultation->setDoublonDe($idDoublonDe);
                                    $consultation->save($connexionCom);
                                    $message .= 'Organisme '.$org.' : Consultation  numéro  '.$consultation->getId()." est marquée comme doublon selon la régle url \n";
                                    Atexo_Util::write_file($fichierLog, '||CONSULTATION | ETAT_DOUBLON : '.date('Y-m-d H:i:s').' Consultation  numéro : '.
                                     $consultation->getId()." est marquée comme doublon selon la régle url \r\n", 'a+');
                                } else {
                                    echo 'Organisme '.$org.' : la Consultation '.$ref." n'est pas marquée comme doublon \n";
                                }
                            }
                        }
                        unset($consultation);
                    }
                }
            }
            echo 'le Nombre des consultations marquées comme doublon est : '.$nombre." \n";
            echo $message."\n";
            echo "Fin de traitement \n";
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_DEDOBLONNAGE').'/dedoblonnage_erreur.log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }

    public static function adapterFormatUrl($url)
    {
        $url = Atexo_Util::atexoHtmlEntitiesDecode($url);
        $url = str_replace('index.php5', '', $url);
        $url = str_replace('index.php', '', $url);

        return $url;
    }
}
