<?php

namespace Application\Cli;

use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Khalid Atlassi <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_ArchivageAutomatique extends Atexo_Cli
{
    public static function run()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        self::display("Debut de l'archivage automatique:");

        if ($allOrganisme) {
            foreach ($allOrganisme as $unOrganisme) {
                self::selectConsultationParEtatEtDate($unOrganisme->getAcronyme());
            }
        }
        self::display('Done');
    }

    private static function selectConsultationParEtatEtDate($organisme)
    {
        self::display("Archivage automatique de l'organisme: ".$organisme);

        //Exclure les consultations SAD  AC
        $exclusionIdSadAc = " AND id_type_procedure_org NOT IN (SELECT Type_Procedure_Organisme.id_type_procedure FROM Type_Procedure_Organisme WHERE 
		Type_Procedure_Organisme.organisme LIKE '".$organisme."'
		AND (( Type_Procedure_Organisme.sad = '1') OR (Type_Procedure_Organisme.accord_cadre ='1')))";

        //Condition Préparation
        $conditionPreparation = '  ( ID_ETAT_CONSULTATION = 0'
            ." AND ( DATEFIN > CURDATE() OR DATEFIN LIKE '0000-00-00 00:00:00')"
            ." AND ( DATE_MISE_EN_LIGNE_CALCULE = ''"
                 ." OR DATE_MISE_EN_LIGNE_CALCULE LIKE '0000-00-00%'"
                 .' OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()'
                 .' OR DATE_MISE_EN_LIGNE_CALCULE IS NULL'
            .'  )'
            ." AND DATEDEBUT !='0000-00-00' AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL ".Atexo_Config::getParameter('DUREE_STATUT_PREPARATION')
            .' MONTH))';

        //Condition Ouverture et Analyse
        $conditionOuvertureAnalyse = " OR (ID_ETAT_CONSULTATION = 0 AND DATEFIN != '' AND DATEFIN NOT LIKE '0000-00-00%'"
            .' AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL '.Atexo_Config::getParameter('DUREE_STATUT_OA')
            .' MONTH)) ';

        //Condition Decision
        $conditionDecision = ' OR (ID_ETAT_CONSULTATION = '.Atexo_Config::getParameter('STATUS_DECISION')
            .' AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL '.Atexo_Config::getParameter('DUREE_STATUT_DECISION').' MONTH)'
                .' AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL '.Atexo_Config::getParameter('DUREE_STATUT_OA')
            .' MONTH)) ';

        $conditionCommun = " DEPOUILLABLE_PHASE_CONSULTATION = '0'"
            .' AND ID_TYPE_AVIS = '.Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION').$exclusionIdSadAc;

        $consultations = [];
        $selectQuery = "SELECT id, organisme FROM consultation WHERE organisme LIKE '" . $organisme
            . "' AND (" . $conditionCommun . ') AND (' . $conditionPreparation
            . $conditionOuvertureAnalyse . $conditionDecision . ')';
        foreach (Atexo_Db::getLinkCommon()->query($selectQuery) as $row) {
            $consultations[] = ['id' => $row['id'], 'organisme' => $row['organisme']];
        }

        $query = 'UPDATE consultation SET ID_ETAT_CONSULTATION = '.Atexo_Config::getParameter('STATUS_A_ARCHIVER')
            ." WHERE organisme LIKE '".$organisme."' AND (".$conditionCommun.') AND ( '.$conditionPreparation.$conditionOuvertureAnalyse.$conditionDecision.')';
        Atexo_Db::getLinkCommon()->query($query);

        foreach ($consultations as $consultation) {
            if (Atexo_Module::isEnabled('EchangesDocuments', $consultation['organisme'])) {
                $echangeDocumentaireService = Atexo_Util::getSfService(EchangeDocumentaireService::class);
                $echangeDocumentaireService->createAutomatedEchanges($consultation['id']);
            }
        }
    }
}
