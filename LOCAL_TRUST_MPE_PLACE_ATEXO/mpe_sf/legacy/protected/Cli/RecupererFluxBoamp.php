<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * Cron de récuperation des consultations du portail BOAMP.
 *
 * @author khadija CHOUIKA  <khadija.chouika@atexo.com>
 *
 * @category Atexo
 */
class Cli_RecupererFluxBoamp extends Atexo_Cli
{
    public static function run()
    {
        try {
            $params = [];
            $params['service'] = 'BOAMP';
            $params['nomBatch'] = 'Cli_RecupererFluxBoamp';
            self::oversight($params);
            echo "Début de transfert des avis BOAMP \n ";
            $ftp_server = Atexo_Config::getParameter('SERVEUR_FTP_BOAMP');

            //Mise en place d'une connexion
            $mdp = Atexo_Config::getParameter('PWD_SERVEUR_FTP_BOAMP');
            $login = Atexo_Config::getParameter('LOGIN_SERVEUR_FTP_BOAMP');
            $conn_id = ftp_connect($ftp_server) or exit('Impossible de se connecter au serveur '.$ftp_server);
            ftp_login($conn_id, $login, $mdp);
            ftp_pasv($conn_id, true);

            //lister les répértoires d'un dossier distant (ftp)
            $arrayAvis = ftp_nlist($conn_id, '');
            $arraydetailAvis = self::ftp_get_filelist($conn_id, '.');
            $nbreFiles = self::recuperationDonneesBoampZip($arrayAvis, $arraydetailAvis, $conn_id);
            self::envoyerEmail();
            echo 'Fin de transfert des avis BOAMP :'.$nbreFiles." fichiers récupérés \n";

            //Fermeture de la connexion
            ftp_close($conn_id);
        } catch (Exception $e) {
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_LOGS').'log_recuperartionAvisBoamp'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
            Atexo_Util::write_file($fichierLog, '||RECUPERATION_ZIP | ERREUR_EXCEPTION : '.$e->getMessage()." \r\n", 'a+');
        }
    }

    /**
     * @param $arrayAvis
     * @param $arraydetailAvis
     * @param $conn_id
     * @param $echangesInterfaces
     *
     * @return int
     *
     * @throws Atexo_Config_Exception
     */
    public static function recuperationDonneesBoampZip($arrayAvis, $arraydetailAvis, $conn_id)
    {
        $paramsCvs = [];
        try {
            $transfert = 0;
            $emplacementAnnonceBOAMPLocal = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_A_TRAITER');
            $emplacementFilesDejaTraite = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_TRAITES');
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_LOGS').'log_recuperartionAvisBoamp'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';

            if ($arrayAvis) {
                for ($i = 0; $i < (is_countable($arrayAvis) ? count($arrayAvis) : 0); ++$i) {
                    $file_parts = pathinfo($arrayAvis[$i]);
                    $fichier = $arrayAvis[$i];
                    $dateFichier = $arraydetailAvis[$fichier]['date_modified'].'_';
                    $fichierDest = $emplacementAnnonceBOAMPLocal.'/'.$dateFichier.$fichier;
                    $fichierDejaTraite = $emplacementFilesDejaTraite.'/'.$dateFichier.$fichier;

                    if ('.' != $fichier && '..' != $fichier && ('taz' == $file_parts['extension'])) {
                        //Si le fichier existe, on ne le recupère pas
                        if (file_exists($fichierDejaTraite) || file_exists($fichierDest)) {
                            continue;
                        }

                        /** @var $params */
                        $params = [];
                        $params['debutExecution'] = new DateTime('now');
                        $params['service'] = 'BOAMP';
                        $params['nomBatch'] = 'Cli_RecupererFluxBoamp';

                        if (!ftp_get($conn_id, $fichierDest, $fichier, FTP_BINARY)) {
                            $params['code'] = Atexo_Oversight_Oversight::$codeErrorSomeFilesOrFoldersAreNotFound;
                            $paramsCvs['poids'] = 0;
                            $params['erreur'] = 'Problème de récupération du fichier '.$fichierDest." \n";
                            echo 'Problème de récupération du fichier '.$fichierDest." \n";
                            Atexo_Util::write_file($fichierLog, '||RECUPERATION_ZIP | ERREUR : '.$fichierDest." Problème de récupération du fichier \r\n", 'a+');
                        } else {
                            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                            /*$paramsCvs['poids'] = filesize($fichierDest); // Vérifier si l'ajout est OK*/
                            echo $fichier." est récuperé avec succès $fichierDest \n ";
                            Atexo_Util::writeFile($fichierLog, '||RECUPERATION_ZIP | SUCCES  : Le fichier  '.$fichier." est récuperé avec succès \r\n", 'a+');
                            ++$transfert;
                        }

                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionCLI) {
                            $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionCLI->getMessage().PHP_EOL.'Trace : '.$exceptionCLI->getTraceAsString().PHP_EOL;
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error($oversightError);
                        }
                    }
                }

                return $transfert;
            } else {
                return $transfert;
            }
        } catch (Exception $e) {
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_LOGS').'log_recuperartionAvisBoamp'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
            Atexo_Util::write_file($fichierLog, '||RECUPERATION_ZIP | ERREUR_EXCEPTION : '.$e->getMessage()." \r\n", 'a+');
            echo 'Erreur  : '.$e->getMessage()."\n";
        }
    }

    public static function envoyerEmail()
    {
        try {
            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_BOAMP_FICHIER_LOGS').'log_recuperartionAvisBoamp'.'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
            $to = Atexo_Config::getParameter('PF_MAIL_BOAMP_TO');
            $from = Atexo_Config::getParameter('PF_MAIL_FROM');

            if (file_exists($fichierLog)) {
                $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_BOAMP_INTERACTION'));
                $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
                $message = str_replace('SALUTATION', 'Bonjour', $message);
                $message = str_replace('TITRE', '', $message);
                $message = str_replace('CORPS_MAIL', 'En pièce jointe la synthèse de la récupérations des avis du BOAMP .', $message);

                $zipFileNameTmp = Atexo_Config::getParameter('COMMON_TMP').'/'.'log_recuperation_boamp_'.date('Y-m-d').'_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
                (new Atexo_Zip())->addFileToZip($fichierLog, $zipFileNameTmp);

                $code = file_get_contents($zipFileNameTmp);
                $zipName = 'log_recuperation_boamp_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
                //$message = "\n Bonjour, \n\n";
                //$message .= "En pièce jointe la synthèse de la récupérations des avis du BOAMP .";
                (new Atexo_Message())->mailWithAttachement($from, $to, 'Rapport de Récupération des avis BOAMP', $message, '', $code, $zipName, '', true);
            }
        } catch (Exception $e) {
            Atexo_Util::write_file($fichierLog, '||ENVOI EMAIL | ERREUR_EXCEPTION : '.$e->getMessage()." \r\n", 'a+');

            echo "Erreur d'envoi de l'email : ".$e->getMessage()."\n";
        }
    }

    /**
     * @param $con
     * @param $path
     *
     * @return array
     */
    private static function ftp_get_filelist($con, $path)
    {
        $files = [];
        $contents = ftp_rawlist($con, $path);

        if (is_countable($contents) ? count($contents) : 0) {
            foreach ($contents as $line) {
                preg_match("#([drwx\-]+)([\s]+)([0-9]+)([\s]+)([0-9]+)([\s]+)([a-zA-Z0-9\.]+)([\s]+)([0-9]+)([\s]+)([a-zA-Z]+)([\s ]+)([0-9]+)([\s]+)([0-9]+):([0-9]+)([\s]+)([a-zA-Z0-9\.\-\_ ]+)#si", $line, $out);

                if (1 != $out[3] && ('.' == $out[18] || '..' == $out[18])) {
                    // do nothing
                } else {
                    $name = $out[18];
                    $files[$name]['rights'] = $out[1];
                    $files[$name]['type'] = 1 == $out[3] ? 'file' : 'folder';
                    $files[$name]['owner_id'] = $out[5];
                    $files[$name]['owner'] = $out[7];
                    $files[$name]['date_modified'] = $out[11].'-'.$out[13].'-'.$out[15].$out[16].'';
                }
            }
        }

        return $files;
    }
}
