<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_SendFiles;

/**
 * envoie le fichier d'archive des consultations vers le ministère.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_SendArchiveArcade_S39 extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                $argument = explode('=', $arguments[$i]);
                $_GET[$argument[0]] = $argument[1] ?: true;
            }
        }

        $module = new Atexo_Module();
        $modules = $module->getOrganismesActivedArcadeModule();
        $pathDirArcadTmp = Atexo_Config::getParameter('CHEMIN_INTERFACE_TMP_ARCADE'); // /var/arcadTmp/

        if ($modules) {
            foreach ($modules as $oneModule) {
                $pathToOrganisme = $pathDirArcadTmp.$oneModule->getOrganisme();
                if (is_dir($pathToOrganisme)) {
                    $file = $pathToOrganisme.'/Arcade_'.date('Y').'_39'.'.tar.gz';
                    $file = '/srv/var/pmi/FichiersArchive/arcade/a4n/Arcade_2014_39.tar.gz';
                    echo $file;
                    //                     if(is_file($file)) {
                    $url = Atexo_Config::getParameter('URL_RECEPTION_ARCADE');

                    //par défaut authentification forte
                    $sslCert = Atexo_Config::getParameter('ARCADE_SSLCERT');
                    $sslCertPwd = Atexo_Config::getParameter('ARCADE_SSLCERTPASSWD');
                    $authentificationForte = true;

                    //si précisé authentification faible alors on écrase le paramétrage par défaut
                    if (isset($_GET['authentificationFaible'])) {
                        $sslCert = null;
                        $sslCertPwd = null;
                        $authentificationForte = false;
                    }

                    if (Atexo_SendFiles::sendFile($file, $url, $authentificationForte, $sslCert, $sslCertPwd)) {
                        echo 'le fichier a bien ete envoye';
                        system('mv '.escapeshellarg($file).' '.escapeshellarg(Atexo_Config::getParameter('COMMON_TMP')));
                        echo 'fichier bien reçu';
                    } else {
                        echo "le fichier n'a pas été envoyé";
                    }

                    //                     }else {
//                          echo "no File in==>".$pathToOrganisme."/Arcade_".date('Y')."_".date('W').".tar.gz"."\n";
//                      }
                } else {
                    echo 'no DIR in==>'.$pathToOrganisme."\n";
                }
            }
        }
    }
}
