<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Supprime les blobs chiffes depuis date, il prend en parametre le mode et la date.
 *
 * @category Atexo
 */
class Cli_ScriptSuppressionBlobsChiffres extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        $help = false;
        $modeReel = false;
        $pas = 0;
        $dateDebut = '';
        $dateFin = '';
        $statutsEnv = null;
        $statutsCons = null;
        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        for ($i = 2; $i < $nbrArg; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('mode' == $arg[0]) {
                if ('reel' == $arg[1]) {
                    $modeReel = true;
                }
            }

            if ('date_debut' == $arg[0]) {
                $u = new Atexo_Util();
                if ($u->validateDate($arg[1], 'Y-m-d')) {
                    $dateDebut = $arg[1];
                }
            }
            if ('date_fin' == $arg[0]) {
                $u = new Atexo_Util();
                if ($u->validateDate($arg[1], 'Y-m-d')) {
                    $dateFin = $arg[1];
                }
            }

            if ('statut_pli' == $arg[0]) {
                $statutsEnv = explode(',', $arg[1]);
            }

            if ('statut_cons' == $arg[0]) {
                $statutsCons = explode(',', $arg[1]);
            }
            if ('pas' == $arg[0]) {
                $pas = (int) $arg[1];
            }
            if ('help' == $arg[0]) {
                $help = true;
            }
        }
        if ($help) {
            echo "
Parametres :

    mode : reel / simule par defaut simule si pas renseigne
    s'ecrit comme suit mode=reel ou mode=simule

    date_debut : date commencement de suppression des blobs chiffres anterieurement, on se base sur la date du depôt( untrusteddate table Offres )
    cette donnee est obligatoire
    s'ecrit comme suit date=Y-m-d

    date_fin : date de fin suppression des blobs chiffres anterieurement, on se base sur la date du depôt( untrusteddate table Offres )
    cette donnee est obligatoire
    s'ecrit comme suit date=Y-m-d

    statut_pli : liste des statuts des enveloppes separes par des virgules par defaut ouvert(2,3,7)
          option possible :
                1 : ferme,
                2 : ouvert en ligne,
                3 : ouvert hors ligne,
                4 : refuse,
                7 : ouvert a distance

    statut_cons : liste des statuts des consultations par defaut statut archive realisee
          option possible :
                0 : consultation en ligne
                4 : decision
                5 : a archiver
                6 : archive realise

    pas : traiter les info par tranche pour ne pas avoir de probleme de performence par defaut 100

    help : aide

Fonctionnement :

    on recupere les offres dont la date depôt est anterieure a la date precise
    si mode simulation , on boucle sur les resultats afin d'alimenter le fichier csv retourne
    si mode reel, on boucle sur les resultats, on supprime le blob du disque, de la base (blob_organisme) et on met le champ id_bloc_fichier de la table blocFichierEnveloppe = 0 pour enlever le lien et il ne ressort pas dans une prochaine requête

a la fin de l'execution on affiche le chemin du fichier de log csv
ce log contient :
         id_offre | consultation_id | statut_consultation | organisme | entreprise_id | nom_entreprise_inscrit | date_depot | statut_pli | id_bloc_fichier | id_blob_chiffre_supprimer | Taille | STATUT

            ";
        } else {
            if (empty($dateDebut) || empty($dateFin)) {
                echo "merci de renseigner une date sous format 'date=Y-m-d'\n";

                return false;
            }

            self::doTraitement($dateDebut, $dateFin, $modeReel, $pas, $statutsEnv, $statutsCons);
        }

        return true;
    }

    public static function doTraitement($dateDebut, $dateFin, $modeReel, $pas = 0, $statutsEnv = null, $statutCons = null)
    {
        $params = [];
        $nbreTotal = 0;
        $statutsEnv = ($statutsEnv) ?: [Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')];
        $statutCons = ($statutCons) ?: [Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')];
        $pas = ($pas) ?: 1_000_000;
        $baseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR');

        $count = 'count(*) as nbreBlobAtraite';
        $critereStatutEnv = "AND
            Enveloppe.statut_enveloppe in ('".implode("','", $statutsEnv)."' ) ";
        $critereStatutCons = "AND
        consultation.id_etat_consultation  in ('".implode("','", $statutCons)."' ) ";
        $select = 'Offres.id as id_offre,
    concat(Offres.id, Offres.organisme) as identifiant,
    Offres.consultation_id as consultation_id,
    Offres.organisme as organisme,
    Offres.entreprise_id as entreprise_id,
    Offres.nom_entreprise_inscrit as nom_entreprise_inscrit,
    Offres.untrusteddate as date_depot,
    consultation.id_etat_consultation as statut_consultation,
    Enveloppe.statut_enveloppe,
    consultation.datefin as datefin,
	blocFichierEnveloppe.id_bloc_fichier as id_bloc_fichier,
    blocFichierEnveloppe.id_blob_chiffre as id_blob_chiffre';

        $sql = <<<QUERY
SELECT
    :SELECT
FROM
    blocFichierEnveloppe ,
    fichierEnveloppe ,
    Enveloppe,
    Offres,
    consultation
WHERE
        blocFichierEnveloppe.id_blob_chiffre is not null
    AND
        blocFichierEnveloppe.id_blob_chiffre != 0
    AND
        blocFichierEnveloppe.id_fichier = fichierEnveloppe.id_fichier
    AND
        blocFichierEnveloppe.organisme = fichierEnveloppe.organisme
    AND
        fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
    AND
        fichierEnveloppe.organisme = Enveloppe.organisme
    AND
        Enveloppe.offre_id = Offres.id
    AND
        Enveloppe.organisme = Offres.organisme
    AND
        Offres.untrusteddate < :dateDepotFin
    AND
        Offres.untrusteddate > :dateDepotDebut
    AND
        consultation.id = Offres.consultation_id
    AND
       consultation.organisme = Offres.organisme
    $critereStatutEnv
    $critereStatutCons

:LIMIT_OFFSET

QUERY;
        $params[':dateDepotDebut'] = $dateDebut;
        $params[':dateDepotFin'] = $dateFin;
        $query = str_replace(':SELECT', $count, $sql);
        $query = str_replace(':LIMIT_OFFSET', '', $query);
        $statement = Atexo_Db::getLinkCommon()->prepare($query);
        $statement->execute($params);
        $res = $statement->fetchAll(PDO::FETCH_ASSOC);
        $nbreLigne = $res[0]['nbreBlobAtraite'];

        $sufix = '_'.$dateDebut.'_'.$dateFin.'_'.date('Y-m-d_H-i-s');
        $ligneEntete = "id_offre | consultation_id | statut_consultation | organisme | entreprise_id | nom_entreprise_inscrit | date_depot | statut_pli | id_bloc_fichier | id_blob_chiffre_supprimer | Taille | STATUT\n";
        $path = self::getPathLog();
        if ($modeReel) {
            $file = $path.'/logReel_'.$sufix.'.csv';
            echo "\nMode : Reel\n";
        } else {
            $file = $path.'/logSimulation_'.$sufix.'.csv';
            echo "\nMode : Simulation\n";
        }
        Atexo_Util::write_file($file, $ligneEntete, 'a');
        ob_flush();
        flush();
        echo "\nnombre de ligne a traiter $nbreLigne\n";
        $j = 0;
        do {
            $i = $j;
            $j += $pas;
            echo "\n", date('Y-m-d H:i:s ') , "traitement tranche de $i a $j\n";
            $query = str_replace(':SELECT', $select, $sql);
            $query = str_replace(':LIMIT_OFFSET', "LIMIT $i, $pas", $query);
            //echo "\n\n",$query;
            $statement = Atexo_Db::getLinkCommon()->prepare($query);
            $statement->execute($params);
            $res = $statement->fetchAll(PDO::FETCH_ASSOC);
            $nbreTotal += is_countable($res) ? count($res) : 0;
            self::doTraitementReel($res, $modeReel, $file, $baseRootDir);
            unset($res);
            unset($statement);
        } while ($j < $nbreLigne);
        $ligneFin = "\n\n\n TOTAL BLOB : ".$nbreTotal;
        Atexo_Util::write_file($file, $ligneFin, 'a');

        echo $ligneFin, "\n\nle fichier genee est enregistre sur ".$file."\n";
    }

    public static function doTraitementReel($res, $modeReel, $file, $baseRootDir)
    {
        try {
            foreach ($res as $blobInfo) {
                $ligneCsv = $blobInfo['id_offre'].'|';
                $ligneCsv .= $blobInfo['consultation_id'].'|';
                $ligneCsv .= self::getStatutCons($blobInfo['statut_consultation'], $blobInfo['datefin']).'|';
                $ligneCsv .= $blobInfo['organisme'].'|';
                $ligneCsv .= $blobInfo['entreprise_id'].'|';
                $ligneCsv .= $blobInfo['nom_entreprise_inscrit'].'|';
                $ligneCsv .= $blobInfo['date_depot'].'|';
                $ligneCsv .= $blobInfo['id_bloc_fichier'].'|';
                $ligneCsv .= $blobInfo['id_blob_chiffre'].'|';
                $taille = 0;
                try {
                    $return = self::deleteBlob(
                        $modeReel,
                        $blobInfo['id_bloc_fichier'],
                        $blobInfo['id_blob_chiffre'],
                        $blobInfo['organisme'],
                        $baseRootDir
                    );

                    $taille = $return['taille'];
                    $statut = $return['statut'];
                } catch (Exception $e) {
                    $statut = 'ERREUR LORS DU TRAIITEMENT DE CE BLOC '.$e->getMessage().'  '.$e->getTraceAsString();
                }
                $ligneCsv .= $taille.'|';
                $ligneCsv .= $statut."\n";
                Atexo_Util::write_file($file, $ligneCsv, 'a');
                ob_flush();
                flush();
                unset($ligneCsv);
                unset($taille);
                unset($statut);
                unset($blobInfo);
                unset($return);
            }
            unset($res);
        } catch (Exception $e) {
            $fileLogErreur = self::getPathLog().'/erreurReel_'.date('_Y_m_d').'.log';
            $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString()."\n";
            echo $log;
            Atexo_Util::write_file($fileLogErreur, $log, 'a');
            if (isset($file)) {
                echo 'les blocs traites sont avant erreur sont dans le fichier '.$file."\n";
            }
        }
    }

    public static function getPathLog()
    {
        $path = Atexo_Config::getParameter('LOG_DIR').'suppressionBlobsChiffes';
        if (!is_dir($path)) {
            mkdir($path, '0755');
        }

        return $path;
    }

    public static function getStatutCons($idStatut, $date)
    {
        $statut = '';
        $statut = match ($idStatut) {
            0 => ($date > date('Y-m-d H:i:s')) ? 'consultation en ligne' : 'Ouverture Analyse',
            4 => 'decision',
            5 => 'a archiver',
            6 => 'Archive realise',
            default => $statut,
        };

        return $statut;
    }

    public static function deleteBlob($modeReel, $idBlocFichier, $idBlob, $organisme, $baseRootDir)
    {
        $params = [];
        $paramsUpdate = [];
        $pathFile = $baseRootDir.'/'.$organisme.'/files/'.$idBlob.'-0';
        if (!file_exists($pathFile)) {
            $pathFile = Atexo_Blob::getPathFile($idBlob, $organisme);
        }

        $statut = 'DELETE KO';
        $taille = 0;
        $fileExist = is_file($pathFile);
        if ($fileExist) {
            $taille = filesize($pathFile);
            echo 'le chemin du fichier a supprimer est '.$pathFile.' et sa taille est de '.$taille."\n";
        } else {
            echo "le fichier n'existe pas sur le disque, la suppression de la base de donnees va etre realisee";
        }

        if ($fileExist) {
            $taille = filesize($pathFile);
        }
        if ($modeReel) {
            if (unlink($pathFile) || !$fileExist) {
                $statut = 'DELETE OK';
                $sqlBlobFile = <<<QUERY
DELETE FROM
blobOrganisme_file
WHERE
	blobOrganisme_file.id = :id
AND
	blobOrganisme_file.organisme like :organisme
QUERY;

                $sqlUpdate = <<<QUERY
UPDATE
blocFichierEnveloppe
SET
blocFichierEnveloppe.id_blob_chiffre = 0
WHERE
	blocFichierEnveloppe.id_bloc_fichier = :id_bloc_fichier
AND
	blocFichierEnveloppe.organisme = :organisme

QUERY;
                $params['id'] = $idBlob;
                $params['organisme'] = $organisme;

                $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlobFile);
                $statement->execute($params);
                unset($statement);

                $paramsUpdate[':id_bloc_fichier'] = $idBlocFichier;
                $paramsUpdate[':organisme'] = $organisme;

                $statement = Atexo_Db::getLinkCommon()->prepare($sqlUpdate);
                $statement->execute($paramsUpdate);
                unset($statement);
            }
        } else {
            $statut = 'DELETE OK';
        }

        return ['statut' => $statut, 'taille' => $taille];
    }
}
