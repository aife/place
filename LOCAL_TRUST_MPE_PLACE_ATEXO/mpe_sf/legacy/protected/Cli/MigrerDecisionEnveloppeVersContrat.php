<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Exception;
use PDO;

/**
 * Classe de numerotation des accord cadres.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_MigrerDecisionEnveloppeVersContrat extends Atexo_Cli
{
    public static function run()
    {
        $listeConsultation = [];
        $connexion = null;
        $sql = null;
        $migrationStart = time();
        $logger = Atexo_LoggerManager::getLogger('contrat');
        $param = '';
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo " -> Merci de preciser le chemin du fichier csv qui contient la liste des consultations : \n";
            echo " -> Le fichier ne doit pas avoir un entete et sous la forme organisme;reference \n";

            return;
        }
        $cheminCSV = $arguments[2];
        if (!is_file($cheminCSV)) {
            echo "Le fichier $cheminCSV n'existe pas \n";

            return;
        }
        $handle = fopen($cheminCSV, 'r');
        $i = 0;
        while (($data = fgetcsv($handle, 8000, ';')) !== false) {
            $listeConsultation[$i] = ['id' => $data[1], 'organisme' => $data[0]];
            $logger->info('Consultation '.$data[0].','.$data[1]);
            ++$i;
        }
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $connexion->beginTransaction();

            $sqlCritereVerif = self::critereConsultation($listeConsultation, 'consultation_id', 'organisme');
            $sqlverification = <<<SQLVERIF
SELECT * from t_cons_lot_contrat WHERE
$sqlCritereVerif
SQLVERIF;

            $statementVerif = Atexo_Db::getLinkCommon(true)->prepare($sqlverification);
            $statementVerif->execute();
            $nbreContratsExistantes = $statementVerif->rowCount();
            if ($nbreContratsExistantes) {
                $message = "Attention, des contrats existent déjà pour ces consultations : \n";
                while ($rowVerif = $statementVerif->fetch(PDO::FETCH_ASSOC)) {
                    $message .= '		Organisme : '.$rowVerif['organisme'].' Consultation : '.$rowVerif['consultation_id'].' lot : '.$rowVerif['lot'].' contrat : '.$rowVerif['id_contrat_titulaire']."\n";
                }
                $message .= 'Etes vous sur de bien vouloir continuer [oui/non] : ';
                echo $message;
                $logger->info($message);
                $confirmation = trim(fgets(STDIN));
                $logger->info("Confirmation : $confirmation");
                if ('oui' !== $confirmation) {
                    echo "Merci de verifier la liste des consultations fournies dans $cheminCSV \n";
                    $logger->info('Annulation confirmation');

                    return;
                }
            }
            //return;
            $sqlCritereConsultation = self::critereConsultation($listeConsultation, 'C.id', 'C.organisme');
            $arrayOrganismeChorus = [];

            $sql = <<<SQL
SELECT  DE.id_decision_enveloppe AS cle_table_source,1 as type_contrat,DE.numero_marche as num_EJ, DE.organisme,C.service_id,O.entreprise_id,O.id_etablissement,O.id as id_offre,DE.type_enveloppe,
    DE.objet_marche,IF(DE.montant_marche IS NULL,NULL,CAST((REPLACE(REPLACE(DE.montant_marche,' ',''),',','.')) as DECIMAL(65,2))) as montant_marche,DE.tranche_budgetaire,0 as publication_montant,
    0 as publication_contrat,DE.statutEJ,DE.numero_marche , NULL as num_long_OEAP,1 as statut_contrat, C.categorie, DE.date_notification,DE.date_fin_marche_previsionnel,
    DE.date_notification_reelle,DE.date_fin_marche_reelle,DL.date_decision,NOW() as date_creation,NOW() as date_modification, DE.envoi_interface, C.reference_utilisateur as reference_libre,
    O.inscrit_id, O.nom_inscrit, O.prenom_inscrit, O.email_inscrit, O.telephone_inscrit, O.fax_inscrit, DE.lot, C.id,0 as hors_passation
  FROM decisionEnveloppe DE,Offres O,DecisionLot DL,consultation C
  WHERE DE.type_enveloppe = 1
	AND DE.id_offre = O.id
	AND DE.organisme = O.organisme
	AND DE.organisme = DL.organisme
	AND DE.consultation_id = DL.consultation_id
	AND DE.lot = DL.lot
	AND DL.id_type_decision = '2'
	AND C.id = DL.consultation_id
	AND C.organisme = DL.organisme
	AND $sqlCritereConsultation
UNION
SELECT  DE.id_decision_enveloppe AS cle_table_source,1 as type_contrat,DE.numero_marche as num_EJ, DE.organisme,C.service_id,O.entreprise_id,O.id_etablissement,O.id as id_offre,DE.type_enveloppe,
    DE.objet_marche,IF(DE.montant_marche IS NULL,NULL,CAST((REPLACE(REPLACE(DE.montant_marche,' ',''),',','.')) as DECIMAL(65,2))) as montant_marche,DE.tranche_budgetaire,0 as publication_montant,
    0 as publication_contrat,DE.statutEJ,DE.numero_marche , NULL as num_long_OEAP,1 as statut_contrat, C.categorie, DE.date_notification,DE.date_fin_marche_previsionnel,
    DE.date_notification_reelle,DE.date_fin_marche_reelle,DL.date_decision,NOW() as date_creation,NOW() as date_modification, DE.envoi_interface, C.reference_utilisateur as reference_libre,

    NULL as inscrit_id, O.nom as nom_inscrit, O.prenom as prenom_inscrit, O.email as email_inscrit, O.telephone as telephone_inscrit, O.fax as fax_inscrit, DE.lot, C.id,0 as hors_passation
  FROM decisionEnveloppe DE,Offre_papier O,DecisionLot DL,consultation C
  WHERE DE.type_enveloppe = 2
	AND DE.id_offre = O.id
	AND DE.organisme = O.organisme
	AND DE.organisme = DL.organisme
	AND DE.consultation_id = DL.consultation_id
	AND DE.lot = DL.lot
	AND DL.id_type_decision = '2'
	AND C.id = DL.consultation_id
	AND C.organisme = DL.organisme
	AND $sqlCritereConsultation
SQL;
            $logger->info('Sql de récupération des decisions enveloppes : '.$sql."\n");
            //return;
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();
            $nbreDecisionEnveloppe = $statement->rowCount();
            $logger->info('Nombre des decisions enveloppes : '.$nbreDecisionEnveloppe."\n");
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $logger->info('Decision  : '.$row['cle_table_source'].' consultation ('.$row['organisme'].', '.$row['id'].") \n");
                $organisme = $row['organisme'];
                if (!isset($arrayOrganismeChorus[$organisme])) {
                    $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
                    $configurationOrganisme = $configurationOrgQuery->findOneByOrganisme($organisme, $connexion);
                    if ($configurationOrganisme instanceof CommonConfigurationOrganisme) {
                        if ($configurationOrganisme->getInterfaceChorusPmi()) {
                            $arrayOrganismeChorus[$organisme] = true;
                        } else {
                            $arrayOrganismeChorus[$organisme] = false;
                        }
                    } else {
                        $arrayOrganismeChorus[$organisme] = false;
                    }
                }
                if (false == $arrayOrganismeChorus[$organisme]) {
                    $row['genereNumerotation'] = true;
                } else {
                    $row['genereNumerotation'] = true;
                }

                $row['numero_marche'] = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($row['numero_marche']);
                $contratTitulaire = (new Atexo_Consultation_Contrat())->createContrat($row, $connexion, $logger);
                $idContrat = $contratTitulaire->getIdContratTitulaire();
                $idDecision = $row['cle_table_source'];
                // mise a jour des autres tables
                $sqlUpdate = <<<SQLUPDATETABLES
UPDATE Chorus_echange C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.organisme='$organisme';

UPDATE chorus_numeros_marches C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.acronyme_organisme='$organisme';

UPDATE chorus_noms_fichiers C SET C.id_echange= $idContrat WHERE C.id_echange = $idDecision AND C.acronyme_organisme='$organisme' AND C.type_fichier='FSO';

UPDATE Autres_Pieces_Mise_Disposition AP SET AP.`id_decision_enveloppe`= $idContrat WHERE AP.id_decision_enveloppe = $idDecision AND AP.org='$organisme';

UPDATE Pieces_Mise_Disposition P SET P.`id_decision_enveloppe`= $idContrat WHERE P.id_decision_enveloppe = $idDecision AND P.org='$organisme';

UPDATE Marche M SET M.`id_decision_enveloppe`= $idContrat WHERE M.id_decision_enveloppe= $idDecision AND M.organisme='$organisme';

UPDATE Contrat C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.organisme='$organisme';

UPDATE t_contrat_titulaire T, Chorus_echange C SET T.`statut_contrat`=3 WHERE T.id_contrat_titulaire = C.id_decision AND T.organisme=C.organisme AND T.id_contrat_titulaire = $idContrat AND T.organisme = '$organisme';

UPDATE t_contrat_titulaire T SET T.`statut_contrat`='4' WHERE T.id_contrat_titulaire = $idContrat AND T.organisme = '$organisme' AND T.date_notification IS NOT NULL ;

SQLUPDATETABLES;
                $logger->info("Sql d'update des autres tables : ".$sqlUpdate."\n");
                $statementUpdate = $connexion->prepare($sqlUpdate);
                $statementUpdate->execute();
                $statementUpdate->closeCursor();
            }
            $connexion->commit();
            $migrationEnd = time();
            $migrationTimeExecute = $migrationEnd - $migrationStart;
            $messageFin = "Temps d'execution de la migration : ".gmdate('H:i:s', $migrationTimeExecute);

            $logger->info($messageFin);
            echo "$messageFin \n";
        } catch (Exception $e) {
            $connexion->rollback();
            $logger->error("Erreur migration contrat : $sql ");
            $logger->error('Erreur migration contrat : '.$e->getMessage().$e->getTraceAsString());
        }
    }

    public function critereConsultation($consultations, $champsReference, $champsOrganisme)
    {
        $sql = ' (0 ';
        if (is_array($consultations)) {
            foreach ($consultations as $oneConsultation) {
                $sql .= "OR ($champsReference = '".$oneConsultation['id']."' AND $champsOrganisme = '".$oneConsultation['organisme']."')";
            }
        }
        $sql .= ' ) ';

        return $sql;
    }
}
