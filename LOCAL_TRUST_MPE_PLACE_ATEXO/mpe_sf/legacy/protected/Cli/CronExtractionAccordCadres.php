<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ExtractAccordsCadres;
use Exception;

/*
 * Created on 24 déc. 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class Cli_CronExtractionAccordCadres extends Atexo_Cli
{
    public static function run()
    {
        $Orgs = [];
        $arguments = $_SERVER['argv'];

        if ($arguments[2]) {
            $Orgs = explode('=', $arguments[2]);
        }
        $listeOrg = explode(',', $Orgs[1]);
        $logger = Atexo_LoggerManager::getLogger('cli');
        $logger->info('Début de lancement du CronExtractionAccordCadres');
        self::display('Début');
        self::display('les organismes exclues de la recherche sont : '.$Orgs[1]);
        $logger->info('les organismes exclues de la recherche sont : '.$Orgs[1]);
        try {
            (new Atexo_Consultation_ExtractAccordsCadres())->genererCsvAccordCadres($listeOrg, $logger);
        } catch (Exception $e) {
            self::display('Erreur : '.$e->getMessage().' '.$e->getTraceAsString());
            $logger->error('Erreur : '.$e->getMessage().' '.$e->getTraceAsString());
        }
        $logger->info('Done');
        self::display('Done');
    }
}
