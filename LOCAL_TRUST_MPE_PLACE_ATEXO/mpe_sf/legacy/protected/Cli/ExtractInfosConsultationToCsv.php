<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Exception;
use PDO;

/*
 * Created on 14 sept. 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class Cli_ExtractInfosConsultationToCsv extends Atexo_Cli
{
    public static function run()
    {
        $listeOrg = (new Atexo_Organismes())->retrieveAllOrganismes();
        $directory = Atexo_Config::getParameter('PATH_FILES_CSV_CONS');
        $nomFichier = $directory.'infosCons_'.date('Y_m_d_H_i_s').'.csv';
        if (!is_dir($directory)) {
            $cmd = "mkdir $directory;";
            $cmd .= "chmod 777 $directory;";
            system($cmd);
        }

        if ($fp = fopen($nomFichier, 'a')) {
            $entete = ['nombreTelechargementDce' => 'Nombre de DCE téléchargés sur la consultation',
                                       'organisme' => 'acronyme',
                                       'reference_utilisateur' => 'reference utilisateur',
                                       'id' => 'id',
                                       'intitule' => 'intitule',
                                       'datedebut' => 'date de mise en ligne',
                                       'datefin' => 'date limite de remise des plis',
                                       'denomination_org' => 'nom organisme',
                                       'libelle' => 'catégorie',
                                       'libelle_type_procedure' => 'libelle type de procédure',
                                       'cpv' => 'codes cpv associé',
                                       'servicePath' => 'chemin complet du service',
                                       'referentielsLochalles' => 'liste des codes du référentiel associé : Lochalles',
                                       'referentielsFrtp' => 'liste des codes du référentiel associé : Frtp',
                                       ];
            fputcsv($fp, $entete, '|');
        }
        try {
            foreach ($listeOrg as $org) {
                echo "Extraction des données de l'organisme : ".$org->getAcronyme()."\n";
                self::extracteDataToCsv($org->getAcronyme(), $nomFichier, $entete);
            }
        } catch (Exception $e) {
            echo "\n\n";
            echo $e->getMessage();
            echo "\n\n";
        }
        echo 'Fichier créé : '.$nomFichier."\n";
    }

    public static function extracteDataToCsv($org, $nomFichier, $entete)
    {
        $sql = "(
				SELECT count( Telechargement.id ) AS nombreTelechargementDce, consultation.organisme, consultation.reference_utilisateur, consultation.id, consultation.intitule, consultation.datedebut, consultation.datefin, Organisme.denomination_org, Service.id as idService, CategorieConsultation.libelle,Type_Procedure_Organisme.libelle_type_procedure,consultation.code_cpv_1 , consultation.code_cpv_2 	
				FROM consultation
				LEFT JOIN Telechargement ON consultation.id = Telechargement.consultation_id, Organisme, Service, CategorieConsultation,Type_Procedure_Organisme
				WHERE consultation.organisme = '".$org."'
				AND Type_Procedure_Organisme.id_type_procedure = consultation.id_type_procedure_org
				AND Type_Procedure_Organisme.organisme = consultation.organisme
				AND consultation.organisme = Telechargement.organisme
				AND consultation.organisme = Organisme.acronyme
				AND Service.organisme = Organisme.acronyme
				AND consultation.organisme = Service.organisme
				AND consultation.service_id = Service.id 
				AND CategorieConsultation.id = consultation.categorie
				AND consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure		
				GROUP BY  consultation.id 
				)
				UNION
				(
				SELECT count( Telechargement.id ) AS nombreTelechargementDce, consultation.organisme, consultation.reference_utilisateur, consultation.id, consultation.intitule, consultation.datedebut, consultation.datefin, Organisme.denomination_org, '0', CategorieConsultation.libelle,Type_Procedure_Organisme.libelle_type_procedure, consultation.code_cpv_1 , consultation.code_cpv_2 	
				FROM consultation
				LEFT JOIN Telechargement ON consultation.id = Telechargement.consultation_id, Organisme, CategorieConsultation,Type_Procedure_Organisme
				WHERE consultation.organisme = '".$org."'
				AND Type_Procedure_Organisme.id_type_procedure = consultation.id_type_procedure_org
				AND Type_Procedure_Organisme.organisme = consultation.organisme
				AND consultation.organisme = Telechargement.organisme
				AND consultation.organisme = Organisme.acronyme
				AND consultation.service_id is null 
				AND CategorieConsultation.id = consultation.categorie
				AND consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure
				GROUP BY consultation.id
				)
				UNION
				(
				SELECT '0',consultation.organisme, consultation.reference_utilisateur, consultation.id, consultation.intitule, consultation.datedebut, consultation.datefin, Organisme.denomination_org, Service.id as idService, CategorieConsultation.libelle,Type_Procedure_Organisme.libelle_type_procedure,consultation.code_cpv_1 , consultation.code_cpv_2 	
				FROM consultation, Organisme, Service, CategorieConsultation,Type_Procedure_Organisme
				WHERE consultation.organisme = '".$org."'
				AND DATE_MISE_EN_LIGNE_CALCULE NOT LIKE  ''
                AND DATE_MISE_EN_LIGNE_CALCULE NOT LIKE '0000-00-00%'
                AND DATE_MISE_EN_LIGNE_CALCULE < SYSDATE()
                AND DATE_MISE_EN_LIGNE_CALCULE IS NOT NULL
				AND Type_Procedure_Organisme.id_type_procedure = consultation.id_type_procedure_org
				AND Type_Procedure_Organisme.organisme = consultation.organisme
				AND consultation.organisme = Organisme.acronyme
				AND Service.organisme = Organisme.acronyme
				AND consultation.organisme = Service.organisme
				AND consultation.service_id = Service.id 
				AND CategorieConsultation.id = consultation.categorie
				AND consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure		
				AND consultation.id not in (SELECT DISTINCT(consultation_id) FROM Telechargement)
				GROUP BY Service.id, consultation.id 		
				)
				UNION
				(
				SELECT '0', consultation.organisme, consultation.reference_utilisateur, consultation.id, consultation.intitule, consultation.datedebut, consultation.datefin, Organisme.denomination_org, '0', CategorieConsultation.libelle,Type_Procedure_Organisme.libelle_type_procedure, consultation.code_cpv_1 , consultation.code_cpv_2 	
				FROM consultation, Organisme, CategorieConsultation,Type_Procedure_Organisme
				WHERE consultation.organisme = '".$org."'
				AND DATE_MISE_EN_LIGNE_CALCULE NOT LIKE  ''
                AND DATE_MISE_EN_LIGNE_CALCULE NOT LIKE '0000-00-00%'
                AND DATE_MISE_EN_LIGNE_CALCULE < SYSDATE()
                AND DATE_MISE_EN_LIGNE_CALCULE IS NOT NULL
				AND Type_Procedure_Organisme.id_type_procedure = consultation.id_type_procedure_org
				AND Type_Procedure_Organisme.organisme = consultation.organisme
				AND consultation.organisme = Organisme.acronyme
				AND consultation.service_id is null 
				AND CategorieConsultation.id = consultation.categorie
				AND consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure
				AND consultation.id not in (SELECT DISTINCT(consultation_id) FROM Telechargement)
				GROUP BY consultation.id
				)
				ORDER BY consultation.id
";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($fp = fopen($nomFichier, 'a')) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $row['cpv'] = $row['code_cpv_1'].$row['code_cpv_2'];
                $row['servicePath'] = Atexo_EntityPurchase::getPathEntityById($row['idService'], $row['organisme']);
                $row['referentielsLochalles'] = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('CODE_LOCHALLES'), $row['id'], 0, $row['organisme']);
                $row['referentielsFrtp'] = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('TEXT_LT_REFERENTIEL_FRTP'), $row['id'], 0, $row['organisme']);
                if ('' != $row['referentielsFrtp']) {
                    echo '</br>'.$row['id'].'-->'.$row['referentielsFrtp'];
                }
                unset($row['code_cpv_1']);
                unset($row['code_cpv_2']);
                unset($row['idService']);
                fputcsv($fp, $row, '|');
            }
        } else {
            echo "impossible d'acceder au fichier '";
            exit;
        }
        fclose($fp);
    }
}
