<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Verifie le nombre de téléchargment par organisme.
 *
 * @author adil el kanabi
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_VerifyTelechargements extends Atexo_Cli
{
    public static function run()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $organismes = CommonOrganismePeer::doSelect($c, $connexion);

        $log = '';
        $total = 0;
        if (is_array($organismes)) {
            foreach ($organismes as $organisme) {
                $c2 = new Criteria();
                $c2->add(CommonTelechargementPeer::ORGANISME, $organisme->getAcronyme());
                $telechargements = CommonTelechargementPeer::doSelect($c2, $connexion);
                $telechargements = is_array($telechargements) ? $telechargements : [];

                $total += count($telechargements);
                $log .= '-> Organisme '.$organisme->getAcronyme().' : '.count($telechargements)."\r\n";
            }
        }

        $log .= "\r\nTotal des téléchargements : ".$total;
        if ($log) {
            Atexo_Util::write_file('/tmp/log_verification_telechargement.log', $log);
        }
    }
}
