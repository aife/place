<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Exception;

/**
 * Cli de suivi de publication du DUME.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 *
 * @version 1.0
 *
 * @since 2017-esr
 *
 * @copyright Atexo 2018
 */
class Cli_SuiviPublicationDume extends Atexo_Cli
{
    public static function run()
    {
        $logger = Atexo_LoggerManager::getLogger('dume');
        try {
            $logger->info('-------- DEBUT SUIVI PUBLICATION DUME --------');
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $tdcq = new CommonTDumeContexteQuery();
            $tDumeContextesEnAttenteDePub = (array) $tdcq->filterByTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'))
                                                         ->filterByStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLIE'))
                                                         ->find($cnx);

            foreach ($tDumeContextesEnAttenteDePub as $tDumeContexte) {
                if ($tDumeContexte instanceof CommonTDumeContexte) {
                    $logger->info('DEBUT SUIVI PUBLICATION DUME idContexteDume : '.$tDumeContexte->getContexteLtDumeId());
                    if (Atexo_Dume_AtexoDume::getInfoDumeNationnal($tDumeContexte)) {
                        $tDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'));
                        $tDumeContexte->save($cnx);
                    } else {
                        $logger->error('SUIVI PUBLICATION DUME TERMINE EN ECHEC idContexteDume : '.$tDumeContexte->getContexteLtDumeId());
                    }
                    $logger->info('FIN SUIVI PUBLICATION DUME idContexteDume : '.$tDumeContexte->getContexteLtDumeId());
                }
            }
        } catch (Exception $e) {
            $logger->error(' Erreur est survenu au moment du suivi de la publication de DUME'.$e->getMessage().' '.$e->getTraceAsString());
        }
        $logger->info('-------- FIN SUIVI PUBLICATION DUME --------');
    }
}
