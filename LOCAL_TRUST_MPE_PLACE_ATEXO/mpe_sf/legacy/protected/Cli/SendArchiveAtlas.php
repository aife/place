<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationArchiveBlocPeer;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Service\Atexo\Atexo_ArchivageExterne;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * Envoi des DAC vers l'exterieure au système atlas.
 *
 * @author khadija CHOUIKA  <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Cli_SendArchiveAtlas extends Atexo_Cli
{
    //taille maximale à ne pas dépasser par défaut 15Go
    const MAX_SIZE_TRANSFERT = '16106127360';

    public static function run()
    {
        $connexionCom = null;
        //Paramètre constant pendant l'exécution du cron
        $cert = Atexo_Config::getParameter('ATLAS_SSLCERT');
        $pwdCert = Atexo_Config::getParameter('ATLAS_SSLCERTPASSWD');
        $path = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR');
        $maxSizeTransfert = self::MAX_SIZE_TRANSFERT;
        $isQuotaDepasse = false;
        $params = [];
        $params['service'] = 'Atlas';
        $params['nomBatch'] = 'Cli_SendArchiveAtlas';
        $params['poids'] = 0;
        $echangesInterfaces = null;
        self::oversight($params);
        try {
            $fileLog = Atexo_LoggerManager::getLogger('archivage');
            $fileLog->info('-------- Debut Transmission des Archives vers ATLAS -------');

            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument = explode('=', $arguments[$i]);
                    if ($argument[1]) {
                        $maxSizeTransfert = $argument[1] * 1024 * 1024 * 1024; //poids passé en paramètre en Go
                    }
                }
            }

            $fileLog->info('----> Seuil de transfert utilisé : '.$maxSizeTransfert);

            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $currentSizeTransfert = 0;

            //récuperer la liste des consultation_archive qui sont éligibles à la transmission
            $listeConsultationArchive = (new CommonConsultationArchiveQuery())->getListConsultationArchiveATransmettre();

            $fileLog->info('----> Nombre de consultation_archive qui sont éligibles à la transmission (status_global_transmission = NON_DEMARREE ou EN_COURS et status_fragmentation oui) = '.(is_countable($listeConsultationArchive) ? count($listeConsultationArchive) : 0));

            $paramsCvs = [];
            $paramsCvs['service'] = 'Atlas';
            $paramsCvs['nomBatch'] = 'Cli_SendArchiveAtlas';
            $ligne = 0;

            foreach ($listeConsultationArchive as $consultationArchive) {
                //si le quota de transfert n'est pas encore dépassé alors on tente d'envoyer les blocs
                if (!$isQuotaDepasse) {
                    $connexionCom->beginTransaction();
                    $referenceConsultation = $consultationArchive->getConsultationRef();
                    $consultationArchiveId = $consultationArchive->getId();
                    $fileLog->info(' Traitement de la consultation_archive id/ref_consultation : '.$consultationArchiveId.'/'.$referenceConsultation);

                    //Pour chaque consultation_archive récupérer tous les consultation_archive_bloc avec status_transmission = 0
                    //si cette liste est vide, alors tous les blocs ont été envoyés et dans ce cas l'objet $consultationArchive consultation_archive doit être mis à jour de manière à ce qu'il soit mis en statut global FIANALISE
                    if (0 == $consultationArchive->countCommonConsultationArchiveBlocs((new Criteria())->add(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, '0'), $connexionCom)) {
                        $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_FINALISEE'));
                        $consultationArchive->save($connexionCom);
                        $fileLog->error(' La consultation_archive id/ref_consultation ne CONTIENT AUCUN BLOC A TRANSMETTRE : '.$consultationArchiveId.'/'.$referenceConsultation.' Le statut global de transmission a été mis à jour pour mise en cohérence.');
                    } else {
                        $result = '';
                        $code = -1;
                        $paramsCvs['poids'] = $consultationArchive->getPoidsArchivage();
                        $listeConsultationArchiveBloc = $consultationArchive->getCommonConsultationArchiveBlocs((new Criteria())->add(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, '0'), $connexionCom);
                        foreach ($listeConsultationArchiveBloc as $consultationArchiveBloc) {
                            //on traite chaque bloc non transmis et on tente de l'envoyer
                            $docId = $consultationArchiveBloc->getDocId();
                            $compId = $consultationArchiveBloc->getCompId();
                            //on gère la non régression : si jamais $compId est null on prend la valeur de la référence de la consultation
                            if (is_null($compId)) {
                                $compId = $referenceConsultation;
                            }

                            $filePathToConsultationArchiveBloc = $path.$consultationArchiveBloc->getCheminFichier();
                            $currentSizeTransfert += $consultationArchiveBloc->getPoidsBloc();

                            //on teste si on ne depasse le seuil maximal de transfert si on envoie le bloc
                            //si on dépasse, alors on arrête, sinon on transfert le bloc
                            if ($currentSizeTransfert <= $maxSizeTransfert) {
                                $code = -1;
                                //envoie du bloc $filename
                                $fileLog->info(" parametre d'envoi : ".$docId.' : '.$compId.' : '.$filePathToConsultationArchiveBloc);

                                if (is_file($filePathToConsultationArchiveBloc)) {
                                    $dateDebutEnvoi = new DateTime('now');
                                    $statutEnvoiBloc = Atexo_ArchivageExterne::sendDACtoAtlas($docId, $compId, $filePathToConsultationArchiveBloc, true, $cert, $pwdCert, $code, $fileLog);

                                    if (!$statutEnvoiBloc && '403' != $code) {//on tente une 2eme fois d'envoyer le bloc
                                        $result = 'statutEnvoiBloc vaut '.$statutEnvoiBloc.' et code vaut '.$code." l'envoi a échoué, on tente d'envoyer une seconde fois le bloc car échec";
                                        $fileLog->info($result);
                                        $statutEnvoiBloc = Atexo_ArchivageExterne::sendDACtoAtlas($docId, $compId, $filePathToConsultationArchiveBloc, true, $cert, $pwdCert, $code, $fileLog);
                                    }

                                    $dateFinEnvoi = new DateTime('now');

                                    if (!$statutEnvoiBloc && '403' != $code) {//si l'envoi a echoué une 2eme fois on annule le traitement des blocs
                                        $paramsCvs['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
                                        if ('408' == $code) {
                                            $paramsCvs['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceDoesNotRespond;
                                        }

                                        $result = 'statutEnvoiBloc vaut '.$statutEnvoiBloc.' et code vaut '.$code." l'envoi a échoué une seconde fois, on annule le traitement des blocs";
                                        $fileLog->info($result);
                                        $consultationArchiveBloc->setDateEnvoiDebut($dateDebutEnvoi);
                                        $consultationArchiveBloc->setDateEnvoiFin($dateFinEnvoi);
                                        $consultationArchiveBloc->setStatusTransmission(false);
                                        $consultationArchiveBloc->setErreur($result);
                                        $consultationArchiveBloc->save($connexionCom);

                                        //si le transfert du bloc est en échec on décremente le seuil, on considère que l'archive n'est pas stockée dans ATLAS
                                        $currentSizeTransfert -= $consultationArchiveBloc->getPoidsBloc();

                                        break;
                                    } else {
                                        $paramsCvs['code'] = Atexo_Oversight_Oversight::$codeSuccess;

                                        $consultationArchiveBloc->setDateEnvoiDebut($dateDebutEnvoi);
                                        $consultationArchiveBloc->setDateEnvoiFin($dateFinEnvoi);
                                        $consultationArchiveBloc->setStatusTransmission(true);
                                        $consultationArchiveBloc->setErreur('');
                                        $consultationArchiveBloc->save($connexionCom);

                                        $fileLog->info("mise à jour de consultationArchiveBloc suite envoi bloc avec SUCCES : statutEnvoiBloc=$statutEnvoiBloc, code_reponse=$code, referenceConsultation=$referenceConsultation, docId=$docId");

                                        // on supprime le bloc si la transmission a reussi
                                        if (is_file($filePathToConsultationArchiveBloc)) {
                                            unlink($filePathToConsultationArchiveBloc);
                                            $fileLog->info("suppression du fichier : $filePathToConsultationArchiveBloc");
                                        }

                                        //on met à jour le statut de la consultation_archive à EN_COURS pour indiquer que c'est en cours de traitement
                                        $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_EN_COURS'));
                                        $consultationArchive->save($connexionCom);
                                        $fileLog->info(' La consultation_archive id/ref_consultation est EN_COURS de traitement pour transmission : '.$consultationArchiveId.'/'.$referenceConsultation.' Le statut global de transmission a été mis à jour.');
                                    }
                                } else {
                                    //le fichier bloc n'existe pas sur le disque, on arrête l'envoi des blocs pour cette consultation et on la passe en statut ERREUR
                                    $result = "Le fichier $filePathToConsultationArchiveBloc n'existe pas sur disque, nous ne pouvons pas transmettre le bloc, arrêt de transmission de tous les blocs pour cette consultation";
                                    $fileLog->error($result);
                                    $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_ERREUR'));
                                    $consultationArchive->save($connexionCom);
                                    break;
                                }
                            } else {
                                $fileLog->error(" quota de transmission dépassé=$currentSizeTransfert, on arrête la transmission des blocs et des archives");
                                $isQuotaDepasse = true;
                                break;
                            }
                        }
                        unset($listeConsultationArchiveBloc);
                        ++$ligne;
                        self::oversight($paramsCvs);
                        unset($paramsCvs['oversight']);
                    }
                    $connexionCom->commit();
                } else {
                    //le quota de transfert est dépassé alors on arrête la transmission des blocs et le traitement des consultations archives
                    break;
                }
            }
            unset($listeConsultationArchive);
            unset($connexionCom);

            //récuperer la liste des consultation_archive qui sont éligibles à la transmission
            $listeConsultationArchive = (new CommonConsultationArchiveQuery())->getListConsultationArchiveATransmettre();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexionCom->beginTransaction();
            foreach ($listeConsultationArchive as $consultationArchive) {
                //on recherche à la fin de tous les blocs s'il reste encore des blocs non transmis
                //si cette liste est vide, alors tous les blocs ont été envoyés et dans ce cas l'objet $consultationArchive consultation_archive doit être mis à jour de manière à ce qu'il soit mis en statut global FIANALISE
                if (0 == $consultationArchive->countCommonConsultationArchiveBlocs((new Criteria())->add(CommonConsultationArchiveBlocPeer::STATUS_TRANSMISSION, '0'), $connexionCom)) {
                    $consultationArchive->setStatusGlobalTransmission(Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_FINALISEE'));
                    $consultationArchive->save($connexionCom);
                    $fileLog->info(' La consultation_archive id/ref_consultation ne CONTIENT PLUS DE BLOC A TRANSMETTRE : '.$consultationArchiveId.'/'.$referenceConsultation.' Le statut global de transmission a été mis à jour.');
                }
            }
            $connexionCom->commit();

            unset($listeConsultationArchive);
            $msg = '';
            if (0 == $ligne) {
                $msg .= "\n0 consultation_archive traitée";
            }
            $msg .= "\n-------- Fin Transmission des Archives vers ATLAS -------";
            $fileLog->info($msg);
        } catch (Exception $e) {
            $fileLog->error(' erreur : '.$e->getMessage());
            $connexionCom->rollBack();
        }
    }
}
