<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * permet de supprimer les donnees de la base de donnees qui ne concerne pas le ou les organisme donnes en parametre.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.10.0
 *
 * @copyright Atexo 2015
 */
class Cli_ExtraireDonneesOrganismes extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter au moin un accronyme d'organisme à garder. ".PHP_EOL;
            echo 'les accronymes des organismes à garder doivent être separé par un espace.'.PHP_EOL;
            echo 'Ex : ExtraireDonneesOrganismes org1 org2 org3 '.PHP_EOL;
        } else {
            $arrayAllOrg = [];
            $arrayOrgToKeep = [];
            $all = false;
            $fichierLog = Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.'/logSupressionOrganismes.log';
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $arrayOrgToKeep[] = $arguments[$i];
                }
            }
            try {
                $script_suppression_org = self::getRootPath().'/mpe/protected/var/scripts-sql/delete_Organismes.sql';
                if (is_file($script_suppression_org)) {
                    preg_match_all("/^((SET|DROP|DELETE) .*;)\s*(?=$)$/Usm", file_get_contents($script_suppression_org), $arrayReq);
                }
                $query = 'SELECT acronyme FROM Organisme';
                foreach ($statement = Atexo_Db::getLinkCommon()->query($query) as $row) {
                    $arrayAllOrg[] = $row['acronyme'];
                }
                $listeOrgs = implode(',', $arrayAllOrg);
                Atexo_Util::write_file($fichierLog, "liste des organismes existants : $listeOrgs", 'a');
                foreach ($arrayOrgToKeep as $oneOrgToKeep) {
                    if (!in_array($oneOrgToKeep, $arrayAllOrg)) {
                        Atexo_Util::write_file($fichierLog, "l'organisme $oneOrgToKeep n'existe pas.veuillez rectifier votre commande".PHP_EOL, 'a');
                        echo "l'organisme $oneOrgToKeep n'existe pas.veuillez rectifier votre commande ".PHP_EOL;
                        exit;
                    }
                }

                foreach ($arrayAllOrg as $organisme) {
                    if (in_array($organisme, $arrayOrgToKeep)) {//empty($organisme) || ?
                        continue;
                    }
                    if (!$all) {
                        echo "voulez vous vraiment supprimer toutes les données de l'organisme : ".$organisme.'? [Yes],[YesAll],[No]'.PHP_EOL;
                        $value = trim(fgets(STDIN));
                        while ('Yes' !== $value && 'YesAll' !== $value && 'No' !== $value) {
                            echo 'Merci de saisir une des valeurs : Yes,YesAll ou No'.PHP_EOL;
                            $value = trim(fgets(STDIN));
                        }
                    } else {
                        $value = 'Yes';
                    }
                    if ('YesAll' == $value) {
                        $all = true;
                        $value = 'Yes';
                    }
                    if ('Yes' === $value) {
                        echo PHP_EOL.'___________________________________________'.PHP_EOL;
                        echo "suppression des donnees de l organisme : $organisme".PHP_EOL;
                        Atexo_Util::write_file($fichierLog, PHP_EOL.'___________________________________________'.PHP_EOL, 'a');
                        Atexo_Util::write_file($fichierLog, "suppression des donnees de l organisme : $organisme".PHP_EOL, 'a');
                        foreach ($arrayReq[1] as $sql) {
                            try {
                                $sql = str_replace('##ACCRONYME_ORG##', $organisme, $sql);
                                if (!$statement = Atexo_Db::getLinkCommon()->query($sql)) {
                                    echo "ERROR while executing query in '$organisme' : ".substr($sql, 0, 77)."...\n";
                                    Atexo_Util::write_file($fichierLog, "ERROR while executing query in '$organisme' : ".substr($sql, 0, 77)."...\n", 'a');
                                } else {
                                    echo $sql.PHP_EOL;
                                    echo 'Deleted rows : '.$statement->rowCount().PHP_EOL;
                                    echo '-------------------------------------------------------------------'.PHP_EOL;
                                    Atexo_Util::write_file($fichierLog, $sql.PHP_EOL, 'a');
                                    Atexo_Util::write_file($fichierLog, 'Deleted rows : '.$statement->rowCount().PHP_EOL, 'a');
                                    Atexo_Util::write_file($fichierLog, '-------------------------------------------------------------------'.PHP_EOL, 'a');
                                }
                            } catch (Exception $e) {
                                Prado::log('Erreur UpdateDB.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ExtraireDonneesOrganismes.php');
                            }
                        }
                    } elseif ('No' === $value) {
                        continue;
                    }
                }
                echo '***********************************************'.PHP_EOL;
                echo '*    fichier log disponible : '.$fichierLog;
                echo '***********************************************'.PHP_EOL;
            } catch (Exception $e) {    //echo $e->getMessage();
                Prado::log('Erreur ExtraireDonneesOrganismes.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ExtraireDonneesOrganismes.php');
            }
        }
    }
}
