<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_SearchLucene;
use Exception;
use PDO;

/**
 * Mise à jour des indexs lucene entreprise.
 *
 * @category Atexo
 */
class Cli_IndexLuceneEntreprise extends Atexo_Cli
{
    public static function run()
    {
        echo ' début de lancement du script de mise à jour des index lucene des entreprises '.date('Y-m-d H:i:s');
        try {
            $sql = 'SELECT * FROM Entreprise';
            $statement = Atexo_Db::getLinkCommon(true)->query($sql);
            $statement->setFetchMode(PDO::FETCH_CLASS, 'CommonEntreprise');
            /*
            $results = $statement->fetchAll();
            if($results) {
                $searchLucene = new Atexo_Entreprise_SearchLucene();
                foreach ($results as $entreprise) {
                    echo "\n Gestion d'entreprise dont ID => ".$entreprise->getId();
                    $searchLucene->addEntreprise($entreprise);
                }
            }
            */
            $searchLucene = new Atexo_Entreprise_SearchLucene();
            while ($entreprise = $statement->fetch()) {
                echo "\n Gestion d'entreprise dont ID => ".$entreprise->getId();
                $searchLucene->addEntreprise($entreprise);
            }
            echo "\n ->Done";
        } catch (Exception $e) {
            echo "\n ERREUR => ".$e->getMessage()." \n ".$e->getTraceAsString();
        }
        echo "\n fin de lancement du script de mise à jour des index lucene des entreprises  ".date('Y-m-d H:i:s');
        echo "\n\t____________________________________\n";
    }
}
