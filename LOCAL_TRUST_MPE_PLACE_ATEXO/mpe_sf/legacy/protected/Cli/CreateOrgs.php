<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdministrateur;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 *  Crée des organismes à partir d'un fichier csv fromCsv ou depuis le socle fromSocle.
 *
 * @category Atexo
 */
class Cli_CreateOrgs extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if ('fromSocle' == $arguments[$i]) {
                    self::createOrgsFromSocle();
                }
                if ('fromCsv' == $arguments[$i]) {
                    $j = $i + 1;
                    if ($arguments[$j]) {
                        $fileName = $arguments[$j];
                    } else {
                        $fileName = Atexo_Config::getParameter('CSV_CREATION_ORGANISMES');
                    }
                    self::createOrgsFromCsv($fileName);
                }
            }
        }
    }

    public static function createOrgsFromSocle()
    {
        $clientSOAP = new \SoapClient(null, [
                                    'location' => Atexo_Config::getParameter('URL_INDEX_SSO_WS'),
                                    'uri' => '',
                                    'trace' => 1,
                                    'exceptions' => 0,
        ]);
        $arrayWsOrgs = $clientSOAP->getAllOrganismes();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoOrganisme = new Atexo_Organismes();
        if (is_array($arrayWsOrgs)) {
            foreach ($arrayWsOrgs as $wsOrg) {
                $c = new Criteria();
                $c->add(CommonOrganismePeer::ACRONYME, $wsOrg['acronyme']);
                $org = CommonOrganismePeer::doSelectOne($c, $connexionCom);
                if ($org) {
                    echo 'ERRO : Organisme  '.$wsOrg['acronyme']." est dejà créé \r\n";
                } else {
                    $administrateur = new CommonAdministrateur();
                    $administrateur->setLogin(utf8_decode($wsOrg['acronyme']));
                    $administrateur->setOriginalLogin(utf8_decode($wsOrg['acronyme']));
                    $administrateur->setMdp(Atexo_Util::generateCode(5));
                    $administrateur->setNom('NOM');
                    $administrateur->setPrenom('PRENOM');
                    $administrateur->setEmail('EMAIL');
                    $administrateur->save($connexionCom);
                    $infos = [];

                    $infos['description'] = ($wsOrg['description_org']) ? utf8_decode($wsOrg['description_org']) : 'description';
                    $infos['email'] = ($wsOrg['email']) ? utf8_decode($wsOrg['email']) : 'mail';
                    $infos['adresse'] = ($wsOrg['adresse']) ? utf8_decode($wsOrg['adresse']) : 'adresse';
                    $infos['cp'] = ($wsOrg['cp']) ? utf8_decode($wsOrg['cp']) : '12345';
                    $infos['ville'] = ($wsOrg['ville']) ? utf8_decode($wsOrg['ville']) : 'ville';
                    $infos['pays'] = 'France';
                    $infos['tel'] = '123456789';
                    $infos['telecopie'] = '123456789';
                    $insee = $wsOrg['categorie_insee'] ?: '7.3';
                    try {
                        $atexoOrganisme->create($wsOrg['acronyme'], '', utf8_decode($wsOrg['denomination_org']), $wsOrg['type_article_org'], $insee, $infos, true, $administrateur->getId());
                        echo "-> Succès ligne $ligne: organisme '".$wsOrg['acronyme']."' crée avec succès.\r\n";
                    } catch (Exception $e) {
                        echo "-> Erreur ligne $ligne : Echec lors de la création de l'organisme '".$wsOrg['acronyme']."'.\r\n";
                        echo $e->__toString();
                    }
                    //$org = new CommonOrganisme();
                }
            }
        }
    }

    public static function createOrgsFromCsv($fileName)
    {
        $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoOrganisme = new Atexo_Organismes();
        if ($handle = fopen($fileName, 'r')) {
            //lire la premier ligne qui ne contient que le nom de colonnes et positionner la tete de lecture sur la 2ème ligne
            $uneLigneCvs = fgetcsv($handle, 10000, ';');
            $ligne = 2;
            while (($uneLigneCvs = fgetcsv($handle, 10000, ';')) !== false) {
                $c = new Criteria();
                $c->add(CommonOrganismePeer::ACRONYME, $uneLigneCvs[1]);
                $organisme = CommonOrganismePeer::doSelectOne($c, $commonConnexion);
                if ($organisme instanceof CommonOrganisme) {
                    echo '-> Succès ligne '.$ligne.": organisme '".$uneLigneCvs[1]."' existe déja.\r\n";
                } else {
                    if ($uneLigneCvs[1]) {
                        $administrateur = new CommonAdministrateur();
                        $administrateur->setLogin($uneLigneCvs[1]);
                        $administrateur->setOriginalLogin($uneLigneCvs[1]);
                        $administrateur->setMdp(Atexo_Util::generateCode(5));
                        $administrateur->setNom('NOM');
                        $administrateur->setPrenom('PRENOM');
                        $administrateur->setEmail('EMAIL');
                        $administrateur->save($commonConnexion);
                        $infos = [];
                        $infos['description'] = $uneLigneCvs[5] ?: 'description';
                        $infos['email'] = $uneLigneCvs[16] ?: 'mail';
                        $infos['adresse'] = $uneLigneCvs[6] ?: 'adresse';
                        $infos['cp'] = $uneLigneCvs[7] ?: '12345';
                        $infos['ville'] = $uneLigneCvs[8] ?: 'ville';
                        $infos['pays'] = 'France';
                        $infos['tel'] = '123456789';
                        $infos['telecopie'] = '123456789';
                        $insee = $uneLigneCvs[4] ?: '7.3';
                        try {
                            $atexoOrganisme->create($uneLigneCvs[1], '', $uneLigneCvs[3], $uneLigneCvs[2], $insee, $infos, true, $administrateur->getId());
                            echo '-> Succès ligne '.$ligne.": organisme '".$uneLigneCvs[1]."' crée avec succès.\r\n";
                        } catch (Exception $e) {
                            echo '-> Erreur ligne '.$ligne." : Echec lors de la création de l'organisme '".$uneLigneCvs[1]."'.\r\n";
                            echo $e->__toString();
                        }
                    }
                }
            }
        }
    }
}
