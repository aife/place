<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceMoniteur;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Moniteur;

/**
 * Page de suivi des annonces au boamp.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_SuiviMoniteur extends Atexo_Cli
{
    public static $_connexionCom;
    public static ?string $_logFilePathMoniteurSuivi = null;

    public static function run()
    {
        self::$_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::$_logFilePathMoniteurSuivi = Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR_SUIVI');
        self::doSuiviAnnonceMoniteur();
    }

    public static function doSuiviAnnonceMoniteur()
    {
        $entitys = (new Atexo_Organismes())->retrieveAllEntity();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($entitys as $oneEntity) {
            $entityAcronyme = $oneEntity->getAcronyme();
            $comptesMoniteur = (new Atexo_Publicite_Moniteur())->retrieveCompteMoniteur($entityAcronyme);
            $annoncesEmises = (new Atexo_Publicite_Destinataire())->retreiveAnnoncesMoniteurEtatEmis($entityAcronyme);

            $suiviXml = '';

            //on ne contacte le moniteur pour récupérer le suivi si il existe le compte moniteur
            // et qu'il y a des annonces qui ont été émises et pour lesquelles on attend un retour
            if ($comptesMoniteur && ((is_countable($annoncesEmises) ? count($annoncesEmises) : 0) > 0)) {
                $suiviXml = self::webGetMoniteurSuiviInfo($comptesMoniteur, $entityAcronyme);
            }
            if ('' != $suiviXml) {
                $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($suiviXml);
                $nodeAnnonce = $domXml->getElementsByTagName('annonce');
                $nombreAnnonce = is_countable($nodeAnnonce) ? count($nodeAnnonce) : 0;
                for ($i = 0; $i < $nombreAnnonce; ++$i) {
                    $idMoniteur = $nodeAnnonce[$i]->getAttribute('idMoniteur');
                    $nodeSupport = $nodeAnnonce[$i]->getElementsByTagName('support');
                    foreach ($nodeSupport as $oneSupport) {
                        $code = $oneSupport->getAttribute('code');
                        // enregistrer le statut dans le support moniteur
                        if ('MON' == $code || 'MOL' == $code) {
                            $nodeStatut = $oneSupport->getElementsByTagName('statut');
                            $date = '';
                            $code = '';
                            $commentaire = '';
                            foreach ($nodeStatut as $oneStatut) {
                                $newDate = str_replace('T', ' ', $oneStatut->getAttribute('date'));
                                if (Atexo_Util::iso2UnixTimeStamp($date) - Atexo_Util::iso2UnixTimeStamp($newDate) < 0) {
                                    $date = $newDate;
                                    $code = $oneStatut->getAttribute('code');
                                    $commentaire = $oneStatut->getAttribute('commentaire');
                                }
                            }
                            if ('' != $idMoniteur) {
                                //recherche de l'annonce identifié par idMoniteur
                                $annonceMoniteur = (new Atexo_Publicite_Destinataire())->retreiveAnnonceMoniteurByNumAnnonce($idMoniteur, $entityAcronyme);
                                if ($annonceMoniteur instanceof CommonAnnonceMoniteur) {
                                    //on met à jour uniquement si les statuts ont changé
                                    if ($annonceMoniteur->getStatutDestinataire() != $code) {
                                        $annonceMoniteur->setStatutDestinataire($code);
                                        $annonceMoniteur->setDatepub($date);
                                        $annonceMoniteur->setDateMaj(date('Y-m-d H:i:s'));
                                        $annonceMoniteur->setCommentaire(utf8_decode($commentaire));
                                        $annonceMoniteur->save($connexionCom);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            unset($suiviXml);
            unset($oneEntity);
            unset($comptesMoniteur);
            unset($entityAcronyme);
        }
        unset($entitys);

        return;
    }

    /**
     * téléchargement du suivi.
     */
    public static function webGetMoniteurSuiviInfo($compteMoniteur, $organisme)
    {
        $log = null;
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\nTraitement du compte Moniteur :".(new Atexo_Publicite_Moniteur())->retrieveMoniteurLoginPassword($compteMoniteur)."de l'organisme : $organisme<br/>";
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        if (!$compteMoniteur->getMoniteurLogin() || !$compteMoniteur->getMoniteurPassword()) {
            $log .= "Compte Moniteur non paramétré.\n";

            return '';
        }
        $url = (new Atexo_Publicite_Moniteur())->retrieveMoniteurSuiviUrl($compteMoniteur);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($ch, CURLOPT_TIMEOUT, 4);
        curl_setopt($ch, CURLOPT_USERPWD, (new Atexo_Publicite_Moniteur())->retrieveMoniteurLoginPassword($compteMoniteur));
        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }
        $resp = curl_exec($ch);
        $resp = str_replace('&lt;', '<', $resp);
        $resp = str_replace('&gt;', '>', $resp);
        $resp = str_replace(']]>', ' ', $resp);
        $resp = str_replace('<![CDATA[', ' ', $resp);
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= $resp;
        $filePath = self::$_logFilePathMoniteurSuivi.'RetourMoniteur_'.date('YmdHisu').'.log';
        Atexo_Util::writeLogFile(self::$_logFilePathMoniteurSuivi, $filePath, $log);
        unset($log);
        unset($filePath);
        unset($url);
        unset($ch);

        return $resp;
    }
}
