<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Weka\Atexo_Weka_FicheWeka;

/**
 * Permet de générer les JS et contenus html WEKA.
 *
 * @author Oumar KONATE
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE4
 */
class Cli_GenererContenuWeka extends Atexo_Cli
{
    public static function run()
    {
        //Inserer en base de données les données WEKA
        if (is_file(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_data.xml')) {
            echo "\n-> insertion données xml en base : \r\n";
            (new Atexo_Weka_FicheWeka())->insererDonneesWeka(Atexo_Config::getParameter('CHEMIN_DONNEES_WEKA').'weka_data.xml');
        }
        //Générer l'xml des données WEKA
        echo "\n-> génération xml des regles weka : \r\n";
        $cheminXml = (new Atexo_Weka_FicheWeka())->genererXmlWeka();
        echo "\t chemin file = $cheminXml \r\n\r\n";
    }
}
