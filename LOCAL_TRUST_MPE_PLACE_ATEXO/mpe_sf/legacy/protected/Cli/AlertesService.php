<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;

/**
 * Mise à jour des alertes des services.
 *
 * @category Atexo
 */
class Cli_AlertesService extends Atexo_Cli
{
    public static function run()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $services = CommonServicePeer::doSelect($c, $connexion);
        foreach ($services as $service) {
            Atexo_AlerteMetier::lancerAlerte('CronService', $service);
        }
    }
}
