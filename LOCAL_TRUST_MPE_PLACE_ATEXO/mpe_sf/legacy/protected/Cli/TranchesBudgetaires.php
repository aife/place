<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;

/**
 * generation des tranches budgetaires.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-develop
 *
 * @copyright Atexo 2016
 */
class Cli_TranchesBudgetaires extends Atexo_Cli
{
    public static function run()
    {
        $annee = date('Y');
        $anneePrecedante = $annee - 1;
        $orgReference = Atexo_Config::getParameter('ORGANISME_REFERENCE');

        $sql = <<<QUERY
INSERT INTO
    `Tranche_Article_133`
    ( `acronyme_org`, `millesime`, `Libelle_tranche_budgetaire`, `borne_inf`, `borne_sup`)
    SELECT *
    FROM
    (
        SELECT
            `acronyme_org`,
            $annee,
            `Libelle_tranche_budgetaire`,
            `borne_inf`,
            `borne_sup`
        FROM
            Tranche_Article_133
        WHERE
            millesime='$anneePrecedante'
    )  AS tmp
    WHERE NOT EXISTS (
        SELECT
            `id`
        FROM
            `Tranche_Article_133` as ta133
        WHERE
            tmp.acronyme_org = ta133.acronyme_org
            AND millesime= '$annee'
    )
QUERY;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute();

        $sqlReference = <<<QUERY
INSERT INTO
    `Tranche_Article_133`
    ( `acronyme_org`, `millesime`, `Libelle_tranche_budgetaire`, `borne_inf`, `borne_sup`)
    SELECT *
    FROM
    (
        SELECT
            `acronyme`,
            $annee,
            `Libelle_tranche_budgetaire`,
            `borne_inf`,
            `borne_sup`
        FROM
            Tranche_Article_133,
            Organisme
        WHERE
            millesime='$anneePrecedante'
            AND acronyme_org ='$orgReference'
    )  AS tmp
    WHERE NOT EXISTS (
        SELECT
            `id`
        FROM
            `Tranche_Article_133` as ta133
        WHERE
            tmp.acronyme = ta133.acronyme_org
            AND`millesime`= '$annee'
    )
QUERY;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sqlReference);
        $statement->execute();
    }
}
