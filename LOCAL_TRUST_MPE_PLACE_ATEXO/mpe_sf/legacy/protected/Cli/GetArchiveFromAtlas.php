<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveBlocPeer;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Atexo_ArchivageExterne;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Envoi des DAC vers le SAS MEGALIS
 * @author Mohamed Wazni  <mwa@atexo.com>
 * @copyright Atexo 2012
 * @version 1.0
 * @since MPE-4.0
 * @package
 * @subpackage
 */
class Cli_GetArchiveFromAtlas extends Atexo_Cli
{
    public static function run()
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $arrayArguments = [];
            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; $i++) {
                    $argument = explode("=", $arguments[$i]);
                    $arrayArguments[$argument[0]] = ($argument[1] != "") ? $argument[1] : true;
                }
            }

            if (isset($arrayArguments["archiveConsultationId"])) {
                $consultationArchive = (new CommonConsultationArchiveQuery())
                    ->filterById($arrayArguments["archiveConsultationId"])
                    ->findOne($connexion);
            } else {
                echo "\n\n Erreur arguments \n\n";
            }

            if ($consultationArchive instanceof CommonConsultationArchive) {
                $cheminFichierZip = null;
                Atexo_ArchivageExterne::getDACFromAtlas($consultationArchive, $cheminFichierZip);
                echo 'Chemin de l\'archive : ' . $cheminFichierZip;
                echo "\n\n";
            } else {
                echo "archive non trouvé \n\n";
            }
        } catch (Exception $e) {
            echo "-> erreur : " . $e->getMessage() . "\n";
        }
    }
}