<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_GenerateXmlStats;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_StatistiquesIndicateursCles;

/**
 * génération du xml des indicateurs clés.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_IndicateursCles extends Atexo_Cli
{
    public static function run()
    {
        self::stockerXmlIndicateursCles(self::GenerateXmlIndicateursCles());
    }

    public static function GenerateXmlIndicateursCles()
    {
        $dateDebut = Atexo_Config::getParameter('DATE_DEBUT_PORTAIL');
        $xmlStatistiquesRequest = '<?xml version="1.0" encoding="UTF-8"?>'
        .'<Transaction Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00"><Control>'
        .'<From login="String" password="String" email="String"/></Control><Message PurposeCode="REQUETE" MessageAction="EXPORT_STATISTIQUES_PORTAIL"/></Transaction>';
        $data = (new Atexo_Interfaces_GenerateXmlStats())->getDataFromXml($xmlStatistiquesRequest);

        $statsArray = (new Atexo_Statistiques_StatistiquesIndicateursCles())->getStatistiquesIndicateursClesEntreprises($dateDebut);
        $donnee = array_merge([$data['login'], $data['pass'], $data['email'], 'REPONSE', $data['messageAction']], $statsArray);
        $xmlStats = (new Atexo_Interfaces_GenerateXmlStats())->generateXmlPortail($donnee);

        return $xmlStats;
    }

    public static function stockerXmlIndicateursCles($xml)
    {
        $directory = Atexo_Config::getParameter('BASE_ROOT_DIR').Atexo_Config::getParameter('DIR_INDICATEURS_CLES');
        if (!is_dir($directory)) {
            $cmd = "mkdir $directory;";
            $cmd .= "chmod 777 $directory;";
            system($cmd);
        }
        Atexo_Util::write_file($directory.Atexo_Config::getParameter('NAME_XML_INDICATEURS_CLES'), $xml);
    }
}
