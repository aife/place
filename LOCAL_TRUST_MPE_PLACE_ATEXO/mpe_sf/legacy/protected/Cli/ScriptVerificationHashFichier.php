<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use PDO;

/**
 * Supprime les blobs chiffes depuis date, il prend en parametre le mode et la date.
 *
 * @category Atexo
 */
class Cli_ScriptVerificationHashFichier extends Atexo_Cli
{
    public static function run()
    {
        $params = [];
        $arguments = $_SERVER['argv'];
        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        $dateOuvertureDebut = null;
        $dateOuvertureFin = null;

        for ($i = 2; $i < $nbrArg; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('dateOuvertureDebut' == $arg[0]) {
                $dateOuvertureDebut = $arg[1];
            }
            if ('dateOuvertureFin' == $arg[0]) {
                $dateOuvertureFin = $arg[1];
            }
        }

        $sql = <<<QUERY
SELECT
        fichierEnveloppe.id_fichier,
        fichierEnveloppe.organisme,
        fichierEnveloppe.id_blob,
        fichierEnveloppe.hash,
        Offres.date_heure_ouverture,
        Offres.id,
        Offres.consultation_id
FROM
        fichierEnveloppe,
        Enveloppe,
        Offres
WHERE

        fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
    AND
        fichierEnveloppe.id_blob is not null
    AND
        fichierEnveloppe.organisme = Enveloppe.organisme
    AND
        Enveloppe.offre_id = Offres.id
    AND
        Enveloppe.organisme = Offres.organisme
    AND
        Offres.date_heure_ouverture > :dateOuvertureDebut
    AND
        Offres.statut_offres != 99
QUERY;

        if (!empty($dateOuvertureFin)) {
            $sql .= '    AND  Offres.date_heure_ouverture < :dateOuvertureFin ';
            $params[':dateOuvertureFin'] = $dateOuvertureFin;
        }
        $params[':dateOuvertureDebut'] = $dateOuvertureDebut;

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute($params);
        while ($fichier = $statement->fetch(PDO::FETCH_ASSOC)) {
            $pathFile = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$fichier['organisme'].'/files/'.$fichier['id_blob'].'-0';
            if (!file_exists($pathFile)) {
                $pathFile = Atexo_Blob::getPathFile($fichier['id_blob'], $fichier['organisme']);
            }
            $hashCalcule = strtolower(sha1_file($pathFile));
            if ($hashCalcule != strtolower($fichier['hash'])) {
                echo 'Consultation: ' , $fichier['consultation_id'] , ' Organisme: ' , $fichier['organisme'] ,
                    ' Offres: ' , $fichier['id'] , ' Date Ouverture: ' , $fichier['date_heure_ouverture'] ,
                    ' Fichier: ' , $fichier['id_fichier'] ,  ' a un hash invalid ==> hash calcule: ' , $hashCalcule , ' --> hash en base:' , $fichier['hash']."\n";
            }
            unset($fichier);
        }

        return true;
    }
}
