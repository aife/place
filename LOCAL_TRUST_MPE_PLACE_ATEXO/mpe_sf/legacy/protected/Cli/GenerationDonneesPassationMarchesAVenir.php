<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonPassationConsultation;
use Application\Propel\Mpe\CommonPassationConsultationPeer;
use Application\Propel\Mpe\CommonPassationMarcheAVenir;
use Application\Propel\Mpe\CommonPassationMarcheAVenirPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;

/**
 * Script correctif : génération des données passation_consultation et passation_marche_a_venir, pour les cas où la consultation
 * existe dans l'export csv marchés conclus mais pas dans l'export csv suivi passation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_GenerationDonneesPassationMarchesAVenir extends Atexo_Cli
{
    public static function run()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                self::genererDonneesPassationMarchesAVenir($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function genererDonneesPassationMarchesAVenir($organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $decisions = (new Atexo_Consultation_Decision())->retrieveAttributaireInformation($organisme);

        foreach ($decisions as $decision) {
            if ($decision['consultation_id']) {
                $c = new Criteria();
                $c->add(CommonPassationConsultationPeer::CONSULTATION_ID, $decision['consultation_id']);
                $c->add(CommonPassationConsultationPeer::ORGANISME, $organisme);
                $passation = CommonPassationConsultationPeer::doSelectOne($c, $connexionCom);

                if (!$passation || !($passation instanceof CommonPassationConsultation)) {
                    $passation = new CommonPassationConsultation();
                    $passation->setReference($decision['consultation_id']);
                    $passation->setOrganisme($organisme);
                    $passation->save($connexionCom);
                }
                $critere = new Criteria();
                $critere->add(CommonPassationMarcheAVenirPeer::ID_PASSATION_CONSULTATION, $passation->getId());
                $critere->add(CommonPassationMarcheAVenirPeer::ORGANISME, $organisme);
                $critere->add(CommonPassationMarcheAVenirPeer::LOT, $decision['lot']);
                $passationMarcheVenir = CommonPassationMarcheAVenirPeer::doSelectOne($critere, $connexionCom);
                if (!$passationMarcheVenir || !($passationMarcheVenir instanceof CommonPassationMarcheAVenir)) {
                    $passationMarcheVenir = new CommonPassationMarcheAVenir();
                    $passationMarcheVenir->setIdPassationConsultation($passation->getId());
                    $passationMarcheVenir->setOrganisme($organisme);
                    $passationMarcheVenir->setLot($decision['lot']);
                    $passationMarcheVenir->save($connexionCom);
                }
            }
        }
    }
}
