<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Reception;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Transmission;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Exception;
use Prado\Prado;

/**
 * Mise à jour des alertes metiers.
 *
 * @category Atexo
 */
class Cli_SendChorusFlux extends Atexo_Cli
{
    public static function run()
    {
        try {
            (new Atexo_Chorus_Util())->loggerInfos(">>>>>>>>>>>>>>>>>>> Début de lancement du cron d'envoi flux FEN <<<<<<<<<<<<<<<<<<< \n");
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            //TODO: recupere les echanges chorus aux status 'STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'
            $echanges = (new Atexo_Chorus_Echange())->retreiveEchangesByStatut(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'), $connexion);
            if (is_array($echanges)) {
                (new Atexo_Chorus_Util())->loggerInfos("Nombre d'echanges a traiter : ".count($echanges));
                foreach ($echanges as $echange) {
                    $erreursEnvoiFluxEchange = [];
                    (new Atexo_Chorus_Util())->loggerInfos('Debut traitement Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : ');
                    (new Atexo_Chorus_Util())->loggerInfos('Type de flux : '.(self::isContratAcSad($echange)) ? 'FEN211' : 'FEN111');
                    $erreurTransmission = false;
                    $organismeObject = Atexo_Organismes::retrieveOrganismeByAcronyme($echange->getOrganisme());
                    $idOrganisme = $organismeObject->getId();
                    //Envoi de FEN
                    $idEjAppliExtValue = $echange->getIdEjAppliExt();
                    //on génére le idEjAppliExtValue uniquement si la valeur n'existe pas
                    if ('' == $idEjAppliExtValue) {
                        $idEjAppliExtValue = (new Atexo_Chorus_Util())->generateChorusUniqueId($idOrganisme, $echange->getIdDecision(), 'Z', $erreursEnvoiFluxEchange, $echange->getUuidExterneExec());
                    }
                    (new Atexo_Chorus_Util())->loggerInfos('Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : idEjAppliExtValue = '.$idEjAppliExtValue);
                    if (!self::isContratAcSad($echange)) {
                        $sendFicheNavette = (new Atexo_Chorus_Echange())->ficheNavetteNotDuplicated($echange, $erreursEnvoiFluxEchange);
                    }
                    if (!self::isContratAcSad($echange)) {
                        $xml = (new Atexo_Chorus_Transmission())->createFluxFen($echange, $echange->getOrganisme(), $idEjAppliExtValue, $sendFicheNavette, false, $erreursEnvoiFluxEchange);
                    } else {
                        $xml = (new Atexo_Chorus_Transmission())->createFluxFenModeIntegre($echange, $echange->getOrganisme(), $erreursEnvoiFluxEchange);
                    }
                    (new Atexo_Chorus_Util())->loggerInfos('Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : xml  '.$xml);
                    if (false !== $xml) {
                        if (!self::isContratAcSad($echange)) {
                            $xmlValide = strstr($xml, '<IdEJAppliExt>');
                            $baliseIdEJAppliExtPresenteVide = strstr($xml, '<IdEJAppliExt></IdEJAppliExt>');

                            //si jamais la balise <IdEJAppliExt> n'est pas dans le flux on affiche une erreur
                            if (!$xmlValide) {
                                $erreurTransmission = true;
                            }
                        } else {
                            $baliseIdEJAppliExtPresenteVide = false;
                        }
                        //si jamais la balise <IdEJAppliExt></IdEJAppliExt> n'est pas dans le flux alors c'est que le flux est potentiellement bon
                        if (!$baliseIdEJAppliExtPresenteVide) {
                            $typeFlux = (self::isContratAcSad($echange)) ? Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN211') : Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN111');
                            $numSequance = (new Atexo_Chorus_Echange())->getNumeroSequenceByType($typeFlux, $connexion, $erreursEnvoiFluxEchange);
                            if(!empty($numSequance)) {
                                $nameFile = (new Atexo_Chorus_Util())->getFilePrefix(self::isContratAcSad($echange)).Atexo_Config::getParameter(
                                        'CODE_APPLICATION_FIR_CHORUS'
                                    ).'001_'.
                                    Atexo_Config::getParameter('CODE_APPLICATION_FIR_CHORUS').'001'.(new Atexo_Chorus_Util())->getCodeInterface(
                                        self::isContratAcSad($echange)
                                    ).$numSequance;
                                (new Atexo_Chorus_Util())->loggerInfos(
                                    'Echange [Id = '.$echange->getId().', Organisme = '.$echange->getOrganisme().'] : Debut Envoi  Nom Fichier '.$nameFile
                                );
                                $resultatEnvoi = (new Atexo_Chorus_Echange())->sendFluxToCHorus($nameFile, $xml, $erreursEnvoiFluxEchange);
                                (new Atexo_Chorus_Util())->loggerInfos(
                                    'Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : Resultat envoi = '.print_r($resultatEnvoi, true)
                                );
                                if ($resultatEnvoi) {
                                    (new Atexo_Chorus_Util())->loggerInfos(
                                        'Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : Flux envoye '.$nameFile
                                    );
                                    (new Atexo_Chorus_Echange())->saveEchange(
                                        $echange,
                                        $nameFile,
                                        Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER'),
                                        Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'),
                                        Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS'),
                                        null,
                                        null,
                                        null,
                                        $idEjAppliExtValue,
                                        $connexion,
                                        $erreursEnvoiFluxEchange
                                    );
                                    (new Atexo_Chorus_Echange())->desactiverMessageRejetEchange($echange, $connexion);
                                } else {
                                    (new Atexo_Chorus_Util())->loggerErreur(
                                        'Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : ErreurTransmission a Chorus]'
                                    );
                                    $erreurTransmission = true;
                                }
                            }
                        } else {
                            (new Atexo_Chorus_Util())->loggerErreur('Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme()."] : Le flux n'est pas valide : ".$xml);
                            $erreurTransmission = true;
                        }
                    } else {
                        (new Atexo_Chorus_Util())->loggerErreur('Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] , ErreurTransmission [ '.prado::localize('RETOUR_CHORUS_ENVOI_REJETE_FLUX_XML_INCORRECT')." ] : Le flux n'est pas valide : ".$xml);
                        $erreurTransmission = true;
                        $erreur = Atexo_Config::getParameter('CODE_ERREUR_REJET_CHORUS');
                        $libelleErreur = prado::localize('RETOUR_CHORUS_ENVOI_REJETE_FLUX_XML_INCORRECT');
                        $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                        $echange->setErreurRejet($erreur.'  '.$libelleErreur);
                        $echange->setStatutechange(Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER'));
                        $echange->setDateEnvoi(date('Y-m-d H:i:s'));
                        $echange->save($connexion);
                        (new Atexo_Chorus_Echange())->desactiverMessageRejetEchange($echange, $connexion, null, $erreursEnvoiFluxEchange);
                        (new Atexo_Chorus_Reception())->sendCourrierRejetChorusAgent($echange, $echange->getIdDecision(), $echange->getOrganisme(), $connexion, true, true);
                    }
                    if ($erreurTransmission) {
                        (new Atexo_Chorus_Util())->loggerErreur('Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme()."] : Impossible d'envoyer le flux vers CHORUS [ ".Prado::localize('TEXT_MSG_ERREUR_TRANSMISSION_CHORUS'.' ]'));
                    }
                    if (!empty($erreursEnvoiFluxEchange)) {
                        //Envoyer mail au support
                        (new Atexo_Chorus_Util())->envoyerMailSupportChorus("Erreur lors de l'envoi du flux Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().'](routine : SendChorusFlux)', print_r($erreursEnvoiFluxEchange, true).PHP_EOL.PHP_EOL.'Echange: '.PHP_EOL.print_r($echange, true));
                    }
                }
            }
            (new Atexo_Chorus_Util())->loggerInfos(">>>>>>>>>>>>>>>>>>> Fin de lancement du cron d'envoi flux FEN <<<<<<<<<<<<<<<<<<< \n");
        } catch (Exception $ex) {
            (new Atexo_Chorus_Util())->loggerErreur("Impossible d'envoyer le flux vers CHORUS [ ".$ex->getMessage().' ]');
        }
    }

    /**
     * Precise si le contrat correspondant a l'echange en parametre est de type accord cadre/sad ou pas.
     *
     * @param CommonChorusEchange $echange : objet echange chorus
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function isContratAcSad($echange)
    {
        try {
            if ($echange instanceof CommonChorusEchange && $echange->isTypeFluxFen211()) {
                return true;
            }

            return false;
        } catch (Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreur('Erreur lors de la precision du type de flux : '.$e->getMessage());
        }
    }
}
