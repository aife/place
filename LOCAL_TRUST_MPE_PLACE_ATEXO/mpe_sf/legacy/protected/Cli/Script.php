<?php

namespace Application\Cli;

use App\Entity\HabilitationAgent;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAcheteurPublicPeer;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonHabilitationProfil;
use Application\Propel\Mpe\CommonHabilitationProfilPeer;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Propel\Mpe\CommonProcedureEquivalencePeer;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulaireQuery;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Propel\Mpe\CommonTrancheArticle133;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Company;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Entites;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Organismes;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Avenant;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Passation;
use DOMDocument;
use Exception;
use PDO;
use Spreadsheet_Excel_Writer;

/**
 * Cron pour le path des services.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @category Atexo
 */
class Cli_Script extends Atexo_Cli
{
    public static function run()
    {
        $param = '';
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter un param : \r\n";
            echo "-> pathService : pour calculer le chemin des services SIGLE/SIGLE - LIBELLE \r\n";
            echo "-> decisionForTypeProcedure : pour rensigner les decisions aux types de procedures qui n'ont aucune decision \r\n";
            echo "-> addHabilitationModifApresValidation : pour ajouter les habilitations ModificationApresValidation aux agents \r\n";
            echo "\t ->pf: pour les procedures formalisees \r\n";
            echo "\t ->mi: pour les MAPA inferieures au montant parametre \r\n";
            echo "\t ->ms: pour les MAPA superieures au montant parametre \r\n";
            echo "-> addHabilitationForProfil : pour ajouter les deux habilitations GestionCompteGroupeMoniteur et GestionCentralePub aux deux profils 1 et 2 \r\n";
            echo "-> addModifsHabiToProfil : pour ajouter les 3 habilitations ModifierConsultationApresValidation (MapaInferieurMontant, MapaSuperieurMontant, ProceduresFormalisees) \r\n";
            echo "-> migrationSoclePPP : pour la reprise des donnees du socle e-Bourgogne (socle PPP) \r\n";
            echo "\t ->organisms: pour les organims \r\n";
            echo "\t ->entities: pour les entities \r\n";
            echo "\t ->agents: pour les agents \r\n";
            echo "\t ->companies: pour les companies \r\n";
            echo "\t ->employees: pour les employees \r\n";
            echo "-> correctionMontantsDejaSaisies : pour enlever les espaces et remplacer les virgules par des points dans les tables parametrees \r\n";
            echo "\t ->decisionEnveloppe: pour la table decisionEnveloppe  \r\n";
            echo "\t ->avenant: pour la table avenant \r\n";
            echo "\t ->passationConsultation: pour la table Passation_consultation \r\n";
            echo "\t ->passationMarcheAvenir: pour la table passation_marche_a_venir \r\n";
            echo "-> suppressionDoublonsDecisionEnveloppe : pour enlever les doublons dans la table decisionEnveloppe \r\n";
            echo "-> correctionDatesSaisies : pour mettre les dates au format unique jj/mm/aaaa dans les tables parametrees \r\n";
            echo "\t ->avenant: pour la table avenant \r\n";
            echo "\t ->passationConsultation: pour la table Passation_consultation \r\n";
            echo "\t ->passationMarcheAvenir: pour la table passation_marche_a_venir \r\n";
            echo "-> correctionMontantMarche : pour corriger le format du montantMarche de la table Marche cote organisme \r\n";
            echo "-> executerScriptModificationMontantMarche : pour changer le type du champ 'montantMarche' de double en varchar \r\n";
            echo "-> executerScriptJustificatifs : pour Passer le parametre 'Document accessible par les acheteurs publics' a Oui pour les documents \r\n";
            echo "-> tentativesMdpToActif: pour desactiver les agents qui ont tentativesMdp=3 de la table agent \r\n";
            echo "-> desactiverModuleAutoriserModifProfilInscritAtes: pour desactiver le module AutoriserModifProfilInscritAtes \r\n";
            echo "-> correctionTableEntreprise: pour corriger le probleme de la synchro plusieurs entreprises avec le meme siren \r\n";
            echo "-> ajoutContraiteCleSecondaireTablePassationMarcheAVenir: Permet d'ajouter une contrainte cle secondaire a la table passation_marche_a_venir, qui pointe vers la table passation_consultation \r\n";
            echo "-> migrationDonneesLtRef: Permet de migrer les codes cpv, nace, nut, deja existants pour l'utilisation du LT-Referentiel";
            echo "\n\t ->cpv: Permet de migrer les codes cpv \r\n";
            echo "\t ->nace: Permet de migrer les codes nace \r\n";
            echo "-> migrationNomFichierDce: Permet de migrer le texte 'Dce integral' en 'DCE_INTEGRAL' dans la table 'Telechargement'. Le nouveau texte 'DCE_INTEGRAL' est en correspondance avec messages.xml\r\n";
            echo "-> scriptsParametrageTraductionBdIntl: Permet d'executer des requetes de donnees specifiques a la plateforme INTL \r\n";
            echo "-> suppressionChampCodesCpvNonUtilises: Permet de supprimer les champs codes cpv non utilises suite a l'utilisation de Lt-Ref \r\n";
            echo "-> insertionDonneesTableJournaux: Permet d'inserer des donnees dans la table journaux pour le client megalis \r\n";
            echo "-> ajoutNouveauCompteInscritEtAlertes: permet d'ajouter un compte utilisateur entreprise (SOCIETEST) avec le login = alerte.v3 et ajout de deux alertes (Alerte complete et Alerte CPV Informatique) pour cet inscrit.  \r\n";
            echo "-> suppressionDoublonsPassationsDonneesLots: permet de supprimer les doublons generes dans la table passation_marche_a_venir pour les besoins du mantis 0006525  \r\n";
            echo "-> miseAJourTagNameChorusTypeProcedure: permet de mettre a jour le champ 'tag_name_chorus' dans la table 'typeProcedure' base de donnees organisme \r\n";
            echo "-> migrationCodesCpvChorusEchange: permet de migrer les codes cpv pour 'chorus_echange' \r\n";
            echo "-> miseAJourHabilitationAgentEnvoyerMessage: permet de mettre a jour les habilitations 'envoyer_message' et 'suivre_message'. Mettre ces habilitations a 1 pour tous les agents qui avaient l'habilitation a modifier une consultation apres validation a \r\n";
            echo "-> miseAJourHabilitationProfilEnvoyerPublicite: permet de mettre a 1 l'habilitations 'envoyer_publicite' pour tous les profils ayant l'habilitation a creer_publicite a \r\n";
            echo "-> migrateDataTable: permet de migrer les donnees des tables de la base organisme vers la base commune \r\n";
            echo "\t ->tableSource: le nom de la table a migrer \r\n";
            echo "\t ->newTable: la table de destination, elle peut etre differente de la table a migrer \r\n";
            echo "\t ->pathFile: chemin du fichier qui contient les champs a mettre a jour. Ce parametre est a renseigner pour la mise a jour \r\n";
            echo "-> executeFile: permet de lire un fichier et l executer \r\n";
            echo "-> importToConsultation: permet d inserer les donnees du fichier csv dans la table consultation: ./mpe script importToConsultation organisme repertoireFichiers \r\n";
            echo "\t ->organisme: organisme de la consultation \r\n";
            echo "\t ->repertoireFichiers: repertoire dans lequel sont stockes les fichiers html \r\n";
            echo "-> MajAcheteurPublic : permet de reaffectation les acheteurs public aux bonnex valeurs referentiels et suuprimer les doublant de la table VeleurRefentiel\r\n";
            echo "-> MajVerificationCertificat : force la validation des certificats de signatures (pour palier au probleme du Module de validation)\r\n";
            echo "-> MajDenominationOrg : L'ajout de la denomination adapatee  des consultations en ligne \r\n";
            echo "-> MigratonRefTrancheBudgetaireArticle133 : Migration des valeurs referentiel des tranches budgetaires vers la nouvelle Table Tranche article 133 \r\n";
            echo "-> UpdateTrancheBudgetaireDecision : Update de l'id Tranche budgetaire des decisions enveloppes par l'id du nouveau referentiel Tranche Article 133  \r\n";
            echo "-> UpdateTrancheBudgetaireMarche : Update de l'id Tranche budgetaire des marches par l'id du nouveau referentiel Tranche Article 133  \r\n";
            echo "-> migrerDateEnvoiDateAr : Permet de migrer les donnees aof : date envoi premier message et date premier AR  \r\n";
            echo "-> UpdateNoDocExtSIMAP  \r\n";
            echo "-> updateTypeProcedureOrganismePLACE  \r\n";
        }
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if ('pathService' == $arguments[$i]) {
                    self::setPathService();
                }
                if ('decisionForTypeProcedure' == $arguments[$i]) {
                    self::setTypeDecisionForTypeProcedure();
                }
                if ('addHabilitationModifApresValidation' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::setHabilitationModifApresValidation($param);
                }
                if ('addHabilitationForProfil' == $arguments[$i]) {
                    self::addHabilitationForProfil();
                }
                if ('addModifsHabiToProfil' == $arguments[$i]) {
                    self::addModifsHabiToProfil();
                }
                if ('migrationSoclePPP' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::soclePPP($param);
                    continue;
                }
                if ('correctionMontantsDejaSaisies' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::correctionMontantBd($param);
                }
                if ('suppressionDoublonsDecisionEnveloppe' == $arguments[$i]) {
                    self::suppressionDoublonsDecisionEnveloppe();
                }
                if ('correctionDatesSaisies' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::correctionDatesBd($param);
                }
                if ('correctionMontantMarche' == $arguments[$i]) {
                    self::correctionMontantMarche();
                }
                if ('executerScriptModificationMontantMarche' == $arguments[$i]) {
                    self::executerScriptMontantMarche();
                }
                if ('executerScriptJustificatifs' == $arguments[$i]) {
                    self::executerScriptJustificatifs();
                }
                if ('tentativesMdpToActif' == $arguments[$i]) {
                    self::updateTentativesMdpActif();
                }
                if ('desactiverModuleAutoriserModifProfilInscritAtes' == $arguments[$i]) {
                    self::desactiverModuleAutoriserModifProfilInscritAtes();
                }
                if ('correctionTableEntreprise' == $arguments[$i]) {
                    self::correctionTableEntreprise();
                }
                if ('ajoutContraiteCleSecondaireTablePassationMarcheAVenir' == $arguments[$i]) {
                    self::AjoutContraintePassationMarcheAVenir();
                }
                if ('migrationDonneesLtRef' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::migrationDonneesLtRef($param);
                }
                if ('migrationNomFichierDce' == $arguments[$i]) {
                    self::migrationNomFichierDce();
                }
                if ('scriptsParametrageTraductionBdIntl' == $arguments[$i]) {
                    self::scriptsParametrageTraductionBDIntl();
                }
                if ('suppressionChampCodesCpvNonUtilises' == $arguments[$i]) {
                    self::suppressionChampCodesCpvNonUtilises();
                }
                if ('insertionDonneesTableJournaux' == $arguments[$i]) {
                    self::insertionDonneesTableJournaux();
                }
                if ('ajoutNouveauCompteInscritEtAlertes' == $arguments[$i]) {
                    self::ajoutNouveauCompteInscritEtAlertes();
                }
                if ('suppressionDoublonsPassationsDonneesLots' == $arguments[$i]) {
                    self::suppressionDoublonsPassationsDonneesLots();
                }
                if ('miseAJourTagNameChorusTypeProcedure' == $arguments[$i]) {
                    self::miseAJourTagNameChorusTypeProcedure();
                }
                if ('migrationCodesCpvChorusEchange' == $arguments[$i]) {
                    self::migrationCodesCpvChorusEchange();
                }
                if ('miseAJourHabilitationAgentEnvoyerMessage' == $arguments[$i]) {
                    self::miseAJourHabilitationAgentEnvoyerMessage();
                }
                if ('miseAJourHabilitationProfilEnvoyerPublicite' == $arguments[$i]) {
                    self::miseAJourHabilitationProfilEnvoyerPublicite();
                }
                if ('migrateDataTable' == $arguments[$i]) {
                    self::migrateDataTable($arguments[3], $arguments[4], $arguments[5]);
                }
                if ('executeFile' == $arguments[$i]) {
                    self::executeFile($arguments[3]);
                }

                if ('importAgent' == $arguments[$i]) {
                    self::importAgent();
                }
                if ('importToConsultation' == $arguments[$i]) {
                    for ($i = 3; $i < $_SERVER['argc']; ++$i) {
                        $param = $param.','.$arguments[$i];
                    }
                    self::importToConsultation($param);
                }
                //mantis :15260
                if ('MajAcheteurPublic' == $arguments[$i]) {
                    self::MajAcheteurPublic();
                }
                if ('MajVerificationCertificat' == $arguments[$i]) {
                    self::MajVerificationCertificat();
                }
                //Referentiel normalise des denomonation Org
                if ('MajDenominationOrg' == $arguments[$i]) {
                    self::udapteDenomAdapteConsultation();
                }
                // Referentiel des tranches budgetaires
                if ('MigratonRefTrancheBudgetaireArticle133' == $arguments[$i]) {
                    self::migrationTrancheBudgetaire();
                }
                // update de l'id tranche budgetaire pas le nouveau Referentiel Tranche Article 1333
                if ('UpdateTrancheBudgetaireDecision' == $arguments[$i]) {
                    self::updateTrancheBudgetaireDecision();
                }
                if ('UpdateTrancheBudgetaireMarche' == $arguments[$i]) {
                    self::updateTrancheBudgetaireMarche();
                }
                if ('migrerDateEnvoiDateAr' == $arguments[$i]) {
                    self::migrerDonneesAofDateEnvoiMessageDateAr();
                }
                if ('UpdateNoDocExtSIMAP' == $arguments[$i]) {
                    self::updateNoDocExtSIMAP();
                }
                if ('updateTypeProcedureOrganismePLACE' == $arguments[$i]) {
                    self::generateScriptUpdateTypeProcedureOrganismePLACE();
                }
                if ('updateTailleTelechargement' == $arguments[$i]) {
                    self::updateTailleTelechargement();
                }
            }
        }
    }

    /***
     * stocker dans la table service la valeur de path de chaque service
     *
     */
    public static function setPathService()
    {
        $allOrganisme = Atexo_Organismes::retrieveOrganismes(true, null, false);
        foreach ($allOrganisme as $OneOrganisme) {
            $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $services = Atexo_EntityPurchase::getCachedEntities($OneOrganisme->getAcronyme());
            foreach ($services as $oneService) {
                try {
                    $pathServiceParent = Atexo_EntityPurchase::getPathServiceById($oneService->getId(), $OneOrganisme->getAcronyme());
                    $pathServiceParentAr = Atexo_EntityPurchase::getPathServiceById($oneService->getId(), $OneOrganisme->getAcronyme(), 'ar');
                    $oneService->setCheminComplet($pathServiceParent);
                    $oneService->setCheminCompletAr($pathServiceParentAr);
                    $oneService->save($connexionOrg);
                    echo "\t- Mise a jour de servicee:".$oneService->getLibelle().' ('.$OneOrganisme->getAcronyme().")\n";
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
            echo '- BDD Organimse: '.$OneOrganisme->getAcronyme()." terminer\n";
        }
    }

    public static function setTypeDecisionForTypeProcedure()
    {
        $entitys = (new Atexo_Organismes())->retrieveAllEntity();
        foreach ($entitys as $oneEntity) {
            $procEquivalences = null;
            $c = new Criteria();
            $connexionOrg = Propel::getConnection($oneEntity->getAcronyme());
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_A_RENSEIGNER, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_ATTRIBUTION_MARCHE, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_DECLARATION_SANS_SUITE, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_DECLARATION_INFRUCTUEUX, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_SELECTION_ENTREPRISE, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_ATTRIBUTION_ACCORD_CADRE, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_ADMISSION_SAD, '+0', Criteria::NOT_EQUAL);
            $c->add(CommonProcedureEquivalencePeer::TYPE_DECISION_AUTRE, '+0', Criteria::NOT_EQUAL);
            $procEquivalences = CommonProcedureEquivalencePeer::doSelect($c, $connexionOrg);
            $idTypeProcedureTraite = '';
            if (is_array($procEquivalences) && count($procEquivalences)) {
                foreach ($procEquivalences as $oneprocEquiv) {
                    $oneprocEquiv->setTypeDecisionARenseigner('+0');
                    $oneprocEquiv->setTypeDecisionAttributionMarche('+0');
                    $oneprocEquiv->setTypeDecisionDeclarationSansSuite('+0');
                    $oneprocEquiv->setTypeDecisionDeclarationInfructueux('+0');
                    $oneprocEquiv->setTypeDecisionSelectionEntreprise('+0');
                    $oneprocEquiv->setTypeDecisionAutre('+0');
                    $oneprocEquiv->save($connexionOrg);
                    $idTypeProcedureTraite .= $oneprocEquiv->getIdTypeProcedure().',';
                }
            }
            echo 'Organisme : '.$oneEntity->getAcronyme().' ok types de procedure ('.$idTypeProcedureTraite.")\n\r";
        }
    }

    public static function addHabilitationForProfil()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $habilitationProfil1 = CommonHabilitationProfilPeer::retrieveByPK('1', $connexionCom);
        if ($habilitationProfil1 instanceof CommonHabilitationProfil) {
            $habilitationProfil1->setGestionCompteGroupeMoniteur(1);
            $habilitationProfil1->setGestionCentralePub(1);
            $habilitationProfil1->save($connexionCom);
            echo 'Modification du Profil : '.$habilitationProfil1->getLibelle()."\r\n";
        }
        $habilitationProfil2 = CommonHabilitationProfilPeer::retrieveByPK('2', $connexionCom);
        if ($habilitationProfil2 instanceof CommonHabilitationProfil) {
            $habilitationProfil2->setGestionCompteGroupeMoniteur(1);
            $habilitationProfil2->setGestionCentralePub(1);
            $habilitationProfil2->save($connexionCom);
            echo 'Modification du Profil : '.$habilitationProfil2->getLibelle()."\r\n";
        }
    }

    public static function addModifsHabiToProfil()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $habilitationProfils = CommonHabilitationProfilPeer::doSelect($c, $connexionCom);
        if (is_array($habilitationProfils)) {
            foreach ($habilitationProfils as $oneProfil) {
                if ($oneProfil instanceof CommonHabilitationProfil) {
                    if ($oneProfil->getModifierConsultationApresValidation()) {
                        $oneProfil->setModifierConsultationMapaInferieurMontantApresValidation('1');
                        $oneProfil->setModifierConsultationMapaSuperieurMontantApresValidation('1');
                        $oneProfil->setModifierConsultationProceduresFormaliseesApresValidation('1');
                        echo 'OUI : Profil : '.$oneProfil->getLibelle()." a l'habilitation modifier apres validation\r\n";
                    } else {
                        $oneProfil->setModifierConsultationMapaInferieurMontantApresValidation('0');
                        $oneProfil->setModifierConsultationMapaSuperieurMontantApresValidation('0');
                        $oneProfil->setModifierConsultationProceduresFormaliseesApresValidation('0');
                        echo 'NON : Profil : '.$oneProfil->getLibelle()." n'a pas l'habilitation modifier apres validation\r\n";
                    }
                    $oneProfil->save($connexionCom);
                }
            }
        }
    }

    public static function soclePPP($param)
    {
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
            if (is_array($arrayParam)) {  //echo count($arrayParam);
                for ($i = 0; $i < count($arrayParam); ++$i) {
                    self::generateXMLSoclePPP($arrayParam[$i]);
                }
            }
        } else {
            ob_start();
            self::generateXMLSoclePPP(null);
            ob_end_flush();
        }
    }

    public static function generateXMLSoclePPP($param = null)
    {
        $arrayGlobal = [];
        //Verification de l'existence du repertoire, creation le cas echeant
        $pathXmlsSoclePPP = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP');
        if (!is_dir($pathXmlsSoclePPP)) {
            system('mkdir '.escapeshellarg($pathXmlsSoclePPP));
        }

        //Export des donnees Organismes
        if (null == $param || 'organisms' == $param) {
            $organisme = new Atexo_InterfaceWs_Organismes();
            if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) {
                $organismes = [Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_Config::getParameter('ORGANISME_TEST_SOCLE_PPP'))]; // Juste Pour les tests unnitaires
            } else {
                $organismes = (new Atexo_Organismes())->retrieveAllEntity();
            }

            //Pre-traitement sur les organismes car on n'exporte pas ceux dont l'acronyme n'est pas un trigramme /[a-z][1-9][a-z]/
            $new_organismes = self::treatOrganismsForMigration($organismes);

            $contentXmlOrganims = $organisme->getXmlResponseOrganismes($new_organismes, true);
            if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'organisms.xml', $contentXmlOrganims);
                echo 'Le fichier XML des organisms a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'organisms.xml'."\n";
            } else {
                if (Atexo_Util::validateXml($contentXmlOrganims, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                    $succes = 'Succes';
                    Atexo_Util::write_file($pathXmlsSoclePPP.'organisms.xml', $contentXmlOrganims);
                    echo 'Le fichier XML des organisms a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'organisms.xml'."\n";
                } else {
                    $succes = 'Echec';
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($contentXmlOrganims, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                    Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_organisms_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                    Atexo_Util::write_file($pathXmlsSoclePPP.'organisms_non_valide.xml', $contentXmlOrganims);
                }
            }

            $entitesPubliqueCardinaliteAvant = is_countable($new_organismes) ? count($new_organismes) : 0;
            $entitesPubliqueCardinaliteApres = self::getCountElementsFromXml($contentXmlOrganims, 'Organism');

            $arrayGlobal[] = ['1' => 'Entites publiques', '2' => $entitesPubliqueCardinaliteAvant, '3' => $entitesPubliqueCardinaliteApres, '4' => $succes];
        }
        if (null == $param || 'entities' == $param) {
            $entities = new Atexo_InterfaceWs_Entites();
            if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) { // Juste pour les testes Unitaires
                $services = Atexo_EntityPurchase::getAllServicesAndPoles(Atexo_Config::getParameter('ORGANISME_TEST_SOCLE_PPP'));
            } else {
                $services = (new Atexo_EntityPurchase())->getAllServicesPlateForme();
            }

            $new_services = self::treatEntitiesForMigration($services);

            $contentXmlEntite = $entities->generateResponseEntites($new_services, null, null, null, null, true, true);

            if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'entities.xml', $contentXmlEntite);
                echo 'Le fichier XML des entities a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'entities.xml'."\n";
            } else {
                if (Atexo_Util::validateXml($contentXmlEntite, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                    $succes = 'Succes';
                    Atexo_Util::write_file($pathXmlsSoclePPP.'entities.xml', $contentXmlEntite);
                    echo 'Le fichier XML des entities a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'entities.xml'."\n";
                } else {
                    $succes = 'Echec';
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($contentXmlEntite, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                    Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_entities_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                    Atexo_Util::write_file($pathXmlsSoclePPP.'entities_non_valide.xml', $contentXmlEntite);
                }
            }
            $entitesAchatCardinaliteAvant = is_countable($new_services) ? count($new_services) : 0;
            $entitesAchatCardinaliteApres = self::getCountElementsFromXml($contentXmlEntite, 'Entity');
            $arrayGlobal[] = ['1' => "Entites d'achat", '2' => $entitesAchatCardinaliteAvant, '3' => $entitesAchatCardinaliteApres, '4' => $succes];
        }
        if (null == $param || 'agents' == $param) {
            $entities = new Atexo_InterfaceWs_Entites();
            $c = new Criteria();
            if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) { // Juste pour les testes Unitaires
                $c->add(CommonAgentPeer::ORGANISME, Atexo_Config::getParameter('ORGANISME_TEST_SOCLE_PPP'));
            }
            $agents = CommonAgentPeer::doSelect($c, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
            $new_agents = self::treatAgentsForMigration($agents);
            $contentXmlAgent = $entities->generateResponseEntites(null, $new_agents, null, null, null, true, true);

            if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'agents.xml', $contentXmlAgent);
                echo 'Le fichier XML des agents a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'agents.xml'."\n";
            } else {
                if (Atexo_Util::validateXml($contentXmlAgent, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                    $succes = 'Succes';
                    Atexo_Util::write_file($pathXmlsSoclePPP.'agents.xml', $contentXmlAgent);
                    echo 'Le fichier XML des agents a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'agents.xml'."\n";
                } else {
                    $succes = 'Echec';
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($contentXmlAgent, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                    Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_agents_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                    Atexo_Util::write_file($pathXmlsSoclePPP.'agents_non_valide.xml', $contentXmlAgent);
                }
            }
            $agentCardinaliteAvant = is_countable($new_agents) ? count($new_agents) : 0;
            $agentCardinaliteApres = self::getCountElementsFromXml($contentXmlAgent, 'Agent');
            $arrayGlobal[] = ['1' => 'Agents', '2' => $agentCardinaliteAvant, '3' => $agentCardinaliteApres, '4' => $succes];
        }
        if (null == $param || 'companies' == $param) {
            $company = new Atexo_InterfaceWs_Company();
            $entreprises = (new Atexo_Entreprise())->retreiveEntreprises();
            $contentXmlCompanies = $company->generateXmlReponseCompany($entreprises, null, null, true);
            if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'companies.xml', $contentXmlCompanies);
                echo 'Le fichier XML des companies a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'companies.xml'."\n";
            } else {
                if (Atexo_Util::validateXml($contentXmlCompanies, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                    $succes = 'Succes';
                    Atexo_Util::write_file($pathXmlsSoclePPP.'companies.xml', $contentXmlCompanies);
                    echo 'Le fichier XML des companies a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'companies.xml'."\n";
                } else {
                    $succes = 'Echec';
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($contentXmlCompanies, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                    Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_companies_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                    Atexo_Util::write_file($pathXmlsSoclePPP.'companies_non_valide.xml', $contentXmlCompanies);
                }
            }
            $entrepriseCardinaliteAvant = count($entreprises);
            $entrepriseCardinaliteApres = self::getCountElementsFromXml($contentXmlCompanies, 'Company');
            $arrayGlobal[] = ['1' => 'Entreprises', '2' => $entrepriseCardinaliteAvant, '3' => $entrepriseCardinaliteApres, '4' => $succes];
        }
        if (null == $param || 'employees' == $param) {
            $company = new Atexo_InterfaceWs_Company();
            $inscrits = (new Atexo_Entreprise_Inscrit())->retreiveInscrits();
            $contentXmlEmployees = $company->generateXmlReponseCompany(null, $inscrits, null, true);
            if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'employees.xml', $contentXmlEmployees);
                echo 'Le fichier XML des employees a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'employees.xml'."\n";
            } else {
                if (Atexo_Util::validateXml($contentXmlEmployees, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                    $succes = 'Succes';
                    Atexo_Util::write_file($pathXmlsSoclePPP.'employees.xml', $contentXmlEmployees);
                    echo 'Le fichier XML des employees a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'employees.xml'."\n";
                } else {
                    $succes = 'Echec';
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($contentXmlEmployees, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                    Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_employees_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                    Atexo_Util::write_file($pathXmlsSoclePPP.'employees_non_valide.xml', $contentXmlEmployees);
                }
            }
            $employeesCardinaliteAvant = is_countable($inscrits) ? count($inscrits) : 0;
            $employeesCardinaliteApres = self::getCountElementsFromXml($contentXmlEmployees, 'Employee');
            $arrayGlobal[] = ['1' => 'Operateurs economiques', '2' => $employeesCardinaliteAvant, '3' => $employeesCardinaliteApres, '4' => $succes];
        }

        self::generateRapportExtraction($arrayGlobal);
        self::generateRapportExtractionCSV($arrayGlobal);
        //echo "\n termine\n\r";
    }

    public static function treatOrganismsForMigration($_t_organisms)
    {
        $_t_new_organisms = [];

        foreach ($_t_organisms as $_organism) {
            if (preg_match('/[a-z][0-9][a-z]/', $_organism->getAcronyme())) {
                $_t_new_organisms[] = $_organism;
            }
        }

        return $_t_new_organisms;
    }

    public static function treatEntitiesForMigration($_t_entities)
    {
        $_t_new_entities = [];

        foreach ($_t_entities as $_entity) {
            if (preg_match('/[a-z][0-9][a-z]/', $_entity['acronymOrganism'])) {
                $_t_new_entities[] = $_entity;
            }
        }

        return $_t_new_entities;
    }

    public static function treatAgentsForMigration($_t_agents)
    {
        $_t_new_agents = [];

        foreach ($_t_agents as $_agent) {
            if (preg_match('/[a-z][0-9][a-z]/', $_agent->getOrganisme())) {
                $_t_new_agents[] = $_agent;
            }
        }

        return $_t_new_agents;
    }

    public static function generateRapportExtractionCSV($data)
    {
        $file = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP').'PPP-rapport-extraction-reprise-donnees-socle.csv';

        $header = ['1' => 'Type de donnees',
                         '2' => 'Cardinalite avant extraction',
                         '3' => 'Cardinalite apres extraction',
                         '4' => "Verification de l'extraction",
                         '5' => 'Commentaires', ];

        array_unshift($data, $header);
        $handle = fopen($file, 'w');

        foreach ($data as $fields) {
            fputcsv($handle, $fields);
        }

        fclose($handle);
    }

    public static function generateRapportExtraction($arrayInfos)
    {
        $rapportFileName = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP').'PPP-rapport-extraction-reprise-donnees-socle.xls';

        $workbook = new Spreadsheet_Excel_Writer($rapportFileName);

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(11);
        $formatStantardGras->setFontFamily('Calibri');
        $formatStantardGras->setAlign('center');
        $formatStantardGras->setFgColor('22');
        $formatStantardGras->setBold(1);
        $formatStantardGras->setBorder(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(11);
        $formatStantard->setFontFamily('Calibri');
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $worksheet = &$workbook->addWorkSheet('rapport extraction');

        $worksheet->setColumn(0, 0, '22.00');
        $worksheet->setColumn(0, 1, '25.00');
        $worksheet->setColumn(0, 2, '25.00');
        $worksheet->setColumn(0, 3, '25.00');
        $worksheet->setColumn(0, 4, '34.00');

        $worksheet->write(0, 0, 'Type de donnees', $formatStantardGras);
        $worksheet->write(0, 1, 'Cardinalite avant extraction', $formatStantardGras);
        $worksheet->write(0, 2, 'Cardinalite apres extraction', $formatStantardGras);
        $worksheet->write(0, 3, "Verification de l'extraction", $formatStantardGras);
        $worksheet->write(0, 4, 'Commentaires', $formatStantardGras);

        $worksheet->setRow(1, '12.75');
        $worksheet->setRow(2, '12.75');
        $worksheet->setRow(3, '12.75');
        $worksheet->setRow(4, '12.75');
        $worksheet->setRow(5, '12.75');

        $j = 0;
        for ($i = 1; $i <= (is_countable($arrayInfos) ? count($arrayInfos) : 0); ++$i) {
            //Recuperation des valeurs a ajouter dans le rapport
            $data_type = $arrayInfos[$j][1];
            $numb_before = $arrayInfos[$j][2];
            $numb_after = $arrayInfos[$j][3];
            $succes = $arrayInfos[$j][4];

            $worksheet->write($i, 0, $data_type, $formatStantard);
            $worksheet->write($i, 1, $numb_before, $formatStantard);
            $worksheet->write($i, 2, $numb_after, $formatStantard);
            $worksheet->write($i, 3, $succes, $formatStantard);
            ++$j;
        }

        /*
        $worksheet->write(1, 0, "Entites publiques", $formatStantard);
        $worksheet->write(2, 0, "Entites d'achat", $formatStantard);
        $worksheet->write(3, 0, "Agents", $formatStantard);
        $worksheet->write(4, 0, "Entreprises", $formatStantard);
        $worksheet->write(5, 0, "Operateurs economiques", $formatStantard);

        //Entites publiques
        $entitesPubliqueCardinaliteAvant= count(Atexo_Organismes::retrieveAllEntity());
        $entitesPubliqueCardinaliteApres = self::getCountElementsFromXml($contentXmlOrganims,'Organism');

        $worksheet->write(1, 1, $entitesPubliqueCardinaliteAvant, $formatStantard);
        $worksheet->write(1, 2, $entitesPubliqueCardinaliteApres, $formatStantard);
        if($entitesPubliqueCardinaliteAvant==$entitesPubliqueCardinaliteApres) {
            $verifExtractionEntitePub= "Succï¿½s";
        }else {
            $verifExtractionEntitePub= "Echec";
        }
        $worksheet->write(1, 3, $verifExtractionEntitePub, $formatStantard);
        $worksheet->write(1, 4, "", $formatStantard);

        //Entites d'achat
        $entitesAchatCardinaliteAvant= count(Atexo_EntityPurchase::getAllServicesPlateForme());
        $entitesAchatCardinaliteApres = self::getCountElementsFromXml($contentXmlEntite,'Entity');

        $worksheet->write(2, 1, $entitesAchatCardinaliteAvant, $formatStantard);
        $worksheet->write(2, 2, $entitesAchatCardinaliteApres, $formatStantard);

        if($entitesAchatCardinaliteAvant==$entitesAchatCardinaliteApres) {
            $verifExtractionEntiteAchat= "Succï¿½s";
        }else {
            $verifExtractionEntiteAchat= "Echec";
        }
        $worksheet->write(2, 3, $verifExtractionEntiteAchat, $formatStantard);
        $worksheet->write(2, 4, "", $formatStantard);

        //Agents
        $agentCardinaliteAvant= count(CommonAgentPeer::doSelect(new Criteria(),Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'))));
        $agentCardinaliteApres = self::getCountElementsFromXml($contentXmlAgent,'Agent');

        $worksheet->write(3, 1, $agentCardinaliteAvant, $formatStantard);
        $worksheet->write(3, 2, $agentCardinaliteApres, $formatStantard);
        if($agentCardinaliteAvant==$agentCardinaliteApres) {
            $verifExtractionAgents= "Succï¿½s";
        }else {
            $verifExtractionAgents= "Echec";
        }
        $worksheet->write(3, 3, $verifExtractionAgents, $formatStantard);
        $worksheet->write(3, 4, "", $formatStantard);

        //Entreprises
        $entrepriseCardinaliteAvant= count(Atexo_Entreprise::retreiveEntrepriseAnnuaireDefense($fromDate,$defense));
        $entrepriseCardinaliteApres = self::getCountElementsFromXml($contentXmlCompanies,'Company');

        $worksheet->write(4, 1, $entrepriseCardinaliteAvant, $formatStantard);
        $worksheet->write(4, 2, $entrepriseCardinaliteApres, $formatStantard);
        if($entrepriseCardinaliteAvant==$entrepriseCardinaliteApres) {
            $verifExtractionEntreprises= "Succï¿½s";
        }else {
            $verifExtractionEntreprises= "Echec";
        }
        $worksheet->write(4, 3, $verifExtractionEntreprises, $formatStantard);
        $worksheet->write(4, 4, "", $formatStantard);

        //Operateurs economique ou Employees

        $employeesCardinaliteAvant= count(Atexo_Entreprise_Inscrit::retreiveInscritAnnuaireDefense($fromDate,$defense));
        $employeesCardinaliteApres = self::getCountElementsFromXml($contentXmlEmployees,'Employee');

        $worksheet->write(5, 1, $employeesCardinaliteAvant, $formatStantard);
        $worksheet->write(5, 2, $employeesCardinaliteApres, $formatStantard);
        if($employeesCardinaliteAvant==$employeesCardinaliteApres) {
            $verifExtractionEmpoyees= "Succï¿½s";
        }else {
            $verifExtractionEmpoyees= "Echec";
        }
        $worksheet->write(5, 3, $verifExtractionEmpoyees, $formatStantard);
        $worksheet->write(5, 4, "", $formatStantard);
        */
        $workbook->close();
    }

    public static function getCountElementsFromXml($xmlContent, $element)
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlContent);
        $nodeOrganism = $domDocument->getElementsByTagName($element);

        return $nodeOrganism->length;
    }

    public static function setHabilitationModifApresValidation($param)
    {
        $connexion = $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $arrayParam = [];
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
        }
        $habilitationAgent = self::retrieveAgentHabiliteModifApresValidation();
        if ($habilitationAgent && is_array($habilitationAgent)) {
            /** @var HabilitationAgent $uneHabilitationAgent */
            foreach ($habilitationAgent as $uneHabilitationAgent) {
                echo "\n\n agent id= ".$uneHabilitationAgent->getAgent()->getId()."\n";
                if (in_array('mi', $arrayParam) || 0 == count($arrayParam)) {
                    $uneHabilitationAgent->setModifierConsultationMapaInferieurMontantApresValidation(1);
                    echo "\t mise a jour habilitationMapaInferieurMontant\n";
                }
                if (in_array('ms', $arrayParam) || 0 == count($arrayParam)) {
                    $uneHabilitationAgent->setModifierConsultationMapaSuperieurMontantApresValidation(1);
                    echo "\t mise a jour habilitationMapaSuperieurMontant\n";
                }
                if (in_array('pf', $arrayParam) || 0 == count($arrayParam)) {
                    $uneHabilitationAgent->setModifierConsultationProceduresFormaliseesApresValidation(1);
                    echo "\t mise a jour habilitationProcedureFormalisee\n";
                }
                $uneHabilitationAgent->save($connexion);
            }
        }
    }

    /*
     * retourne les agents ayant l'habilitation modification apres validation
     */
    public static function retrieveAgentHabiliteModifApresValidation()
    {
        $c = new Criteria();
        $c->add(CommonHabilitationAgentPeer::MODIFIER_CONSULTATION_APRES_VALIDATION, 1, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $habilitationAgent = CommonHabilitationAgentPeer::doSelect($c, $connexionCom);

        return $habilitationAgent ?: false;
    }

    /*
     * Correction des montants dans la base de donnees.
     */
    public static function correctionMontantBd($param)
    {
        $arrayParam = [];
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '------------------------------------ ORGANISME: '.$uneOrganisme->getAcronyme()." ------------------------------------\n";
                if (in_array('decisionEnveloppe', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionMontantDecisionEnveloppe($uneOrganisme->getAcronyme());
                }
                if (in_array('avenant', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionMontantAvenant($uneOrganisme->getAcronyme());
                }
                if (in_array('passationConsultation', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionPassationConsultation($uneOrganisme->getAcronyme());
                }
                if (in_array('passationMarcheAvenir', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionPassationMarcheAVenir($uneOrganisme->getAcronyme());
                }
            }
        }
    }

    public static function correctionMontantDecisionEnveloppe($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $decisionEnveloppe = self::retrieveAllDecisionEnveloppe($organisme);

        if ($decisionEnveloppe && is_array($decisionEnveloppe)) {
            foreach ($decisionEnveloppe as $uneDecisionEnveloppe) {
                $uneDecisionEnveloppe->setMontantMarche(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $uneDecisionEnveloppe->getMontantMarche()))))))))));
                $uneDecisionEnveloppe->save($connexionorg);
                echo 'DecisionEnveloppe id = '.$uneDecisionEnveloppe->getIdDecisionEnveloppe()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function correctionMontantAvenant($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avenant = (new Atexo_SuiviPassation_Avenant())->retrieveAllAvenant($organisme);

        if ($avenant && is_array($avenant)) {
            foreach ($avenant as $unAvenant) {
                $unAvenant->setMontantAvenantHt(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $unAvenant->getMontantAvenantHt()))))))))));
                $unAvenant->setMontantAvenantTtc(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $unAvenant->getMontantAvenantTtc()))))))))));
                $unAvenant->setPourcentageAugmentationMarcheInitial(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $unAvenant->getPourcentageAugmentationMarcheInitial()))))))))));
                $unAvenant->setPourcentageAugmentationCumule(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $unAvenant->getPourcentageAugmentationCumule()))))))))));
                $unAvenant->setMontantTotalMarcheToutAvenantCumule(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $unAvenant->getMontantTotalMarcheToutAvenantCumule()))))))))));
                $unAvenant->save($connexionorg);
                echo 'Avenant id = '.$unAvenant->getIdAvenant()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function correctionPassationConsultation($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationConsultation = (new Atexo_SuiviPassation_Passation())->retrieveAllPassationConsultation($organisme);

        if ($passationConsultation && is_array($passationConsultation)) {
            foreach ($passationConsultation as $passation) {
                $passation->setMontantEstimeConsultation(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $passation->getMontantEstimeConsultation()))))))))));
                $passation->save($connexionorg);
                echo 'passationConsultation id = '.$passation->getId()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function correctionPassationMarcheAVenir($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrieveAllPassationMarcheAVenir($organisme);

        if ($passationMarcheAVenir && is_array($passationMarcheAVenir)) {
            foreach ($passationMarcheAVenir as $passation) {
                $passation->setMontantEstime(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $passation->getMontantEstime()))))))))));
                $passation->setMinBonCommande(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $passation->getMinBonCommande()))))))))));
                $passation->setMaxBonCommande(preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $passation->getMaxBonCommande()))))))))));
                $passation->save($connexionorg);
                echo 'passationMarcheAVenir id = '.$passation->getId()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function retrieveAllDecisionEnveloppe($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme);
        $decisionEnveloppe = CommonDecisionEnveloppePeer::doSelect($c, $connexion);

        return $decisionEnveloppe;
    }

    public static function suppressionDoublonsDecisionEnveloppe()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '------------------------------------ ORGANISME: '.$uneOrganisme->getAcronyme()." ------------------------------------\n";
                self::SupprimerDoublonsDecisionEnveloppe($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function SupprimerDoublonsDecisionEnveloppe($organisme)
    {
        $decisionEnveloppe = self::retrieveAllDecisionEnveloppe($organisme);
        $tableauDoublon = [];
        if (is_array($decisionEnveloppe) && count($decisionEnveloppe)) {
            foreach ($decisionEnveloppe as $decision) {
                $tableauDoublon[$decision->getConsultationId().'-'.$decision->getIdOffre().'-'.$decision->getLot()][$decision->getIdDecisionEnveloppe()] = $decision->getIdDecisionEnveloppe();
            }
        }

        if (is_array($tableauDoublon) && count($tableauDoublon)) {
            foreach ($tableauDoublon as $doublons) {
                if (count($doublons) > 1) {
                    $i = 0;
                    foreach ($doublons as $doublon) {
                        if ($i >= 1) {
                            self::deleteDecisionEnveloppe($doublon, $organisme);
                            echo "\t";
                            echo ' decisionEnveloppe id = '.$doublon.'   supprime ';
                            echo "\n";
                        }
                        ++$i;
                    }
                }
            }
        }
    }

    public static function deleteDecisionEnveloppe($idDecicion, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c->add(CommonDecisionEnveloppePeer::ID_DECISION_ENVELOPPE, $idDecicion);
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme);
        $deleteDecision = CommonDecisionEnveloppePeer::doDelete($c, $connexion);
    }

    /*
     * Correction des dates dans la base de donnees.
     */
    public static function correctionDatesBd($param)
    {
        $arrayParam = [];
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '------------------------------------ ORGANISME: '.$uneOrganisme->getAcronyme()." ------------------------------------\n";
                if (in_array('avenant', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionDatesAvenant($uneOrganisme->getAcronyme());
                }
                if (in_array('passationConsultation', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionDatePassationConsultation($uneOrganisme->getAcronyme());
                }
                if (in_array('passationMarcheAvenir', $arrayParam) || 0 == count($arrayParam)) {
                    self::correctionDatePassationMarcheAVenir($uneOrganisme->getAcronyme());
                }
            }
        }
    }

    public static function correctionDatePassationConsultation($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationConsultation = (new Atexo_SuiviPassation_Passation())->retrieveAllPassationConsultation($organisme);

        if ($passationConsultation && is_array($passationConsultation)) {
            foreach ($passationConsultation as $passation) {
                $passation->setDateDeliberationFinanciere(self::corrigerFormatDate($passation->getDateDeliberationFinanciere()));
                $passation->setDateDeliberationAutorisantSignatureMarche(self::corrigerFormatDate($passation->getDateDeliberationAutorisantSignatureMarche()));
                $passation->setDateNotificationPrevisionnelle(self::corrigerFormatDate($passation->getDateNotificationPrevisionnelle()));
                $passation->setDateReceptionProjetDCEServiceValidateur(self::corrigerFormatDate($passation->getDateReceptionProjetDCEServiceValidateur()));
                $passation->setDateFormulationsPremieresObservations(self::corrigerFormatDate($passation->getDateFormulationsPremieresObservations()));
                $passation->setDateRetourProjetDCEFinalise(self::corrigerFormatDate($passation->getDateRetourProjetDCEFinalise()));
                $passation->setDateValidationProjetDCEParServiceValidateur(self::corrigerFormatDate($passation->getDateValidationProjetDCEParServiceValidateur()));
                $passation->setDateValidationProjetDCEVuePar(self::corrigerFormatDate($passation->getDateValidationProjetDCEVuePar()));
                $passation->setDateReceptionProjetAAPCParServiceValidateur(self::corrigerFormatDate($passation->getDateReceptionProjetAAPCParServiceValidateur()));
                $passation->setDateFormulationsPremieresObservationsAAPC(self::corrigerFormatDate($passation->getDateFormulationsPremieresObservationsAAPC()));
                $passation->setDateRetourProjetAAPCFinalise(self::corrigerFormatDate($passation->getDateRetourProjetAAPCFinalise()));
                $passation->setDateValidationProjetAAPCParServiceValidateur(self::corrigerFormatDate($passation->getDateValidationProjetAAPCParServiceValidateur()));
                $passation->setDateValidationProjetAAPCParServiceValidateurVuPar(self::corrigerFormatDate($passation->getDateValidationProjetAAPCParServiceValidateurVuPar()));
                $passation->setDateEnvoiPublicite(self::corrigerFormatDate($passation->getDateEnvoiPublicite()));
                $passation->setDateEnvoiInvitationsRemettreOffre(self::corrigerFormatDate($passation->getDateEnvoiInvitationsRemettreOffre()));
                $passation->setDateLimiteRemiseOffres(self::corrigerFormatDate($passation->getDateLimiteRemiseOffres()));
                $passation->setDateReunionOuvertureCandidatures(self::corrigerFormatDate($passation->getDateReunionOuvertureCandidatures()));
                $passation->setDateReunionOuvertureOffres(self::corrigerFormatDate($passation->getDateReunionOuvertureOffres()));

                $passation->save($connexionorg);
                echo 'passationConsultation id = '.$passation->getId()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function correctionDatePassationMarcheAVenir($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $passationMarcheAVenir = (new Atexo_SuiviPassation_Passation())->retrieveAllPassationMarcheAVenir($organisme);

        if ($passationMarcheAVenir && is_array($passationMarcheAVenir)) {
            foreach ($passationMarcheAVenir as $passation) {
                $passation->setDateReceptionAnalyseOffre(self::corrigerFormatDate($passation->getDateReceptionAnalyseOffre()));
                $passation->setDateFormulationObservationProjetRapport(self::corrigerFormatDate($passation->getDateFormulationObservationProjetRapport()));
                $passation->setDateRetourProjetRapportFinalise(self::corrigerFormatDate($passation->getDateRetourProjetRapportFinalise()));
                $passation->setDateValidationProjetRapport(self::corrigerFormatDate($passation->getDateValidationProjetRapport()));
                $passation->setDateReunionAttribution(self::corrigerFormatDate($passation->getDateReunionAttribution()));
                $passation->setDateEnvoiCourrierCondidatNonRetenu(self::corrigerFormatDate($passation->getDateEnvoiCourrierCondidatNonRetenu()));
                $passation->setDateSignatureMarchePa(self::corrigerFormatDate($passation->getDateSignatureMarchePa()));
                $passation->setDateReceptionControleLegalite(self::corrigerFormatDate($passation->getDateReceptionControleLegalite()));
                $passation->setDateFormulationObservationDossier(self::corrigerFormatDate($passation->getDateFormulationObservationDossier()));
                $passation->setDateRetourDossierFinalise(self::corrigerFormatDate($passation->getDateRetourDossierFinalise()));
                $passation->setDateTransmissionPrefecture(self::corrigerFormatDate($passation->getDateTransmissionPrefecture()));
                $passation->setDateValidationRapportInformation(self::corrigerFormatDate($passation->getDateValidationRapportInformation()));

                $passation->save($connexionorg);
                echo 'passationMarcheAVenir id = '.$passation->getId()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function correctionDatesAvenant($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avenant = (new Atexo_SuiviPassation_Avenant())->retrieveAllAvenant($organisme);

        if ($avenant && is_array($avenant)) {
            foreach ($avenant as $unAvenant) {
                $unAvenant->setDateReceptionProjetParSecretaireCao(self::corrigerFormatDate($unAvenant->getDateReceptionProjetParSecretaireCao()));
                $unAvenant->setDateReceptionProjetParChargeEtude(self::corrigerFormatDate($unAvenant->getDateReceptionProjetParChargeEtude()));
                $unAvenant->setDateObservationParSv(self::corrigerFormatDate($unAvenant->getDateObservationParSv()));
                $unAvenant->setDateRetourProjet(self::corrigerFormatDate($unAvenant->getDateRetourProjet()));
                $unAvenant->setDateValidationProjet(self::corrigerFormatDate($unAvenant->getDateValidationProjet()));
                $unAvenant->setDateValidationProjetVuPar(self::corrigerFormatDate($unAvenant->getDateValidationProjetVuPar()));
                $unAvenant->setDateCao(self::corrigerFormatDate($unAvenant->getDateCao()));
                $unAvenant->setDateCp(self::corrigerFormatDate($unAvenant->getDateCp()));
                $unAvenant->setDateSignatureAvenant(self::corrigerFormatDate($unAvenant->getDateSignatureAvenant()));
                $unAvenant->setDateReceptionDossier(self::corrigerFormatDate($unAvenant->getDateReceptionDossier()));
                $unAvenant->setDateFormulationObservationParSvSurSdossier(self::corrigerFormatDate($unAvenant->getDateFormulationObservationParSvSurSdossier()));
                $unAvenant->setDateRetourDossierFinalise(self::corrigerFormatDate($unAvenant->getDateRetourDossierFinalise()));
                $unAvenant->setDateValidationDossierFinalise(self::corrigerFormatDate($unAvenant->getDateValidationDossierFinalise()));
                $unAvenant->setDateTransmissionPrefecture(self::corrigerFormatDate($unAvenant->getDateTransmissionPrefecture()));
                $unAvenant->setDateNotification(self::corrigerFormatDate($unAvenant->getDateNotification()));

                $unAvenant->save($connexionorg);
                echo 'Avenant id = '.$unAvenant->getIdAvenant()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function corrigerFormatDate($dateACorriger)
    {
        $separateur = '';
        if (preg_match('-', $dateACorriger)) {
            $date = explode('-', $dateACorriger);

            if (1 == strlen($date[1])) {
                $date[1] = '0'.$date[1];
            }
            if (2 == strlen($date[0])) {
                if (((int) ($date[0]) >= 0) && ((int) ($date[0]) <= 10)) {
                    $date[0] = '20'.$date[0];
                } else {
                    $date[0] = '19'.$date[0];
                }
            }

            return $date[2].'/'.$date[1].'/'.(string) $date[0];
        } elseif (preg_match('/', $dateACorriger)) {
            $date = explode('/', $dateACorriger);

            if (1 == strlen($date[1])) {
                $date[1] = '0'.$date[1];
            }
            if (2 == strlen($date[2])) {
                if (((int) ($date[2]) >= 0) && ((int) ($date[2]) <= 10)) {
                    $date[2] = '20'.$date[2];
                } else {
                    $date[2] = '19'.$date[2];
                }
            }

            return $date[0].'/'.$date[1].'/'.(string) $date[2];
        }
    }

    public static function correctionMontantMarche()
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                self::corrigerMontantMarche($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function corrigerMontantMarche($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $marche = self::retrieveAllMarche($connexionorg);
        if ($marche && is_array($marche)) {
            foreach ($marche as $uneMarche) {
                $uneMarche->setMontantMarche(self::getFormatMontantCorrige($uneMarche->getMontantMarche()));
                $uneMarche->save($connexionorg);
                echo 'Marche id = '.$uneMarche->getId()."                                         mise a jour: OK  \n";
            }
        }
    }

    public static function retrieveAllMarche($connexionOrg = false)
    {
        if (!$connexionOrg) {
            $connexionOrg = Propel::getConnection(Atexo_CurrentUser::getCurrentOrganism());
        }
        $c = new Criteria();
        $marches = CommonMarchePeer::doSelect($c, $connexionOrg);
        if ($marches) {
            return $marches;
        } else {
            return false;
        }
    }

    public static function getFormatMontantCorrige($montant)
    {
        return preg_replace('/[^0-9\.,]/', '', str_replace(' ', '', str_replace('.', ',', str_replace('euros', '', str_replace('euro', '', str_replace('Euro', '', str_replace('Euros', '', str_replace('EURO', '', str_replace('EUROS', '', $montant)))))))));
    }

    public static function executerScriptMontantMarche()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                self::executerScriptUpdateMarche($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function executerScriptUpdateMarche($organisme)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $script = "ALTER TABLE `Marche` CHANGE `montantMarche` `montantMarche` VARCHAR( 50 ) NULL DEFAULT '0';";
        $statement = Atexo_Db::getLinkOrganism($organisme, false)->query($script);
        if ($statement) {
            echo "mise a jour:              OK  \n";
        } else {
            echo "mise a jour:              KO \n";
        }
        Atexo_Db::closeOrganism($organisme);
    }

    public static function executerScriptJustificatifs()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $script = "UPDATE `Justificatifs` SET `visible_par_agents` = '1';";
        $statement = Atexo_Db::getLinkCommon()->query($script, PDO::FETCH_ASSOC);
        if ($statement) {
            echo "mise a jour:              OK  \n";
        } else {
            echo "mise a jour:              KO \n";
        }
    }

    public static function updateTentativesMdpActif()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agents = (new Atexo_Agent())->getAllAgentTentativesMdpMax();
        if (is_array($agents)) {
            foreach ($agents as $agent) {
                $agent->setTentativesMdp('0');
                $agent->setActif('0');
                $agent->save($connexionCom);
                echo 'Agent id = '.$agent->getId()." mise a jour: OK  \n";
            }
        }
    }

    public static function desactiverModuleAutoriserModifProfilInscritAtes()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $script = "UPDATE `configuration_plateforme` SET `autoriser_modif_profil_inscrit_ates` = '0'";
        $statement = Atexo_Db::getLinkCommon()->query($script, PDO::FETCH_ASSOC);
        if ($statement) {
            echo "mise a jour:              OK  \n";
        } else {
            echo "mise a jour:              KO \n";
        }
    }

    public static function correctionTableEntreprise()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $script = "SELECT distinct siren, count(siren) as nbre FROM Entreprise WHERE siren IS NOT NULL AND siren !='' GROUP BY siren HAVING count(siren) > 1;";
        $statement = Atexo_Db::getLinkCommon()->query($script, PDO::FETCH_ASSOC);
        $index = 1;
        $messages = '';
        $log = '';
        foreach ($statement as $row) {
            $messages = "$index- Entreprise SIREN = '".$row['siren']."' nombre= '".$row['nbre']."' \r\n";
            $log .= $messages;
            echo $messages;
            $c = new Criteria();
            $c->add(EntreprisePeer::SIREN, $row['siren']);
            $c->addAscendingOrderByColumn(EntreprisePeer::ID);
            $companies = EntreprisePeer::doSelect($c, $connexionCom);
            $countCompanies = count($companies);
            if ($row['nbre'] == $countCompanies) {
                $laseCompany = $companies[$countCompanies - 1];
                $firstCompany = array_shift($companies);
                if (($firstCompany instanceof Entreprise && $laseCompany instanceof Entreprise)) {
                    $messages = "\t First Entreprise '".$firstCompany->getId()."' Nom : '".$firstCompany->getNom()."' idInitial : '".$firstCompany->getIdInitial()."'\r\n";
                    $log .= $messages;
                    echo $messages;
                    $messages = "\t Last Entreprise '".$laseCompany->getId()."' Nom : '".$laseCompany->getNom()."' idInitial : '".$laseCompany->getIdInitial()."'\r\n";
                    $log .= $messages;
                    echo $messages;
                    $firstCompany->setIdInitial($laseCompany->getIdInitial());
                    $firstCompany->setNom($laseCompany->getNom());
                    $firstCompany->save($connexionCom);
                    $indexCompanies = '1';
                    if ($companies && is_array($companies) && $countCompanies > 1) {
                        foreach ($companies as $oneCompany) {
                            $arrayInscrit = [];
                            $cr = new Criteria();
                            $cr->add(CommonInscritPeer::ENTREPRISE_ID, $oneCompany->getId());
                            $arrayInscrit = CommonInscritPeer::doSelect($cr, $connexionCom);
                            $indexInscrit = '1';
                            if ($arrayInscrit && is_array($arrayInscrit) && count($arrayInscrit) > 0) {
                                /*$messages = "\t\t $indexCompanies- Entreprise ".$oneCompany->getId()." \r\n";
                                $log .= $messages;
                                echo $messages;*/
                                foreach ($arrayInscrit as $oneInscrit) {
                                    $sql = '';
                                    $sql = "UPDATE `Inscrit` SET `entreprise_id` = '".$firstCompany->getId()."' WHERE `Inscrit`.`id` =".$oneInscrit->getId().' ';
                                    Atexo_Db::getLinkCommon()->query($sql, PDO::FETCH_ASSOC);
                                    $messages = "\t\t $indexInscrit- Inscrit '".$oneInscrit->getId()."' Entreprise '".$oneCompany->getId()."' affecte a '".$firstCompany->getId()."'\r\n";
                                    $log .= $messages;
                                    echo $messages;
                                    ++$indexInscrit;
                                }
                            }
                            $oneCompany->delete($connexionCom);
                            $messages = "\t $indexCompanies- Suppression Entreprise '".$oneCompany->getId()."' \r\n";
                            $log .= $messages;
                            echo $messages;
                            ++$indexCompanies;
                        }
                    }
                }
            } else {
                echo 'probleme '.$row['siren']." \r\n";
            }
            ++$index;
        }
        Atexo_Util::write_file(__DIR__.'/../../var/log/logCorrectionEntreprise.log', $log, 'a');
    }

    public static function AjoutContraintePassationMarcheAVenir()
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                self::executerScriptAjoutContrainte($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function executerScriptAjoutContrainte($organisme)
    {
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $sql = ' ALTER TABLE `passation_marche_a_venir` ADD INDEX ( `id_passation_consultation`  ) ; ';
        $sql .= ' set foreign_key_checks=0; ';
        $sql .= ' ALTER TABLE `passation_marche_a_venir` ADD FOREIGN KEY ( `id_passation_consultation`  ) REFERENCES `Passation_consultation`  ( `id` ); ';
        Atexo_Db::getLinkOrganism($organisme)->query($sql);
        echo "\t                     Contrainte ajoutee avec succes \r\n";
        Atexo_Db::closeOrganism($organisme);
    }

    public static function migrationDonneesLtRef($param)
    {
        $arrayParam = [];
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
        }
        if (0 == count($arrayParam) || (!in_array('cpv', $arrayParam) && !in_array('nace', $arrayParam))) {
            echo "\n\t                     vous n'avez pas speficifie de parametre ou parametre incorrecte !!! \n\n";

            return;
        }
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                if (in_array('cpv', $arrayParam)) {
                    self::migrationCodesCpv($uneOrganisme->getAcronyme());
                }
                if (in_array('nace', $arrayParam)) {
                    self::migrationCodesNace($uneOrganisme->getAcronyme());
                }
            }
        }
    }

    public static function migrationCodesCpv($organisme)
    {
        $separateur = "'#'";
        $like = "'#%'";
        $connexionorg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
        $sql = " UPDATE `consultation` SET `code_cpv_2` = CONCAT( $separateur, CONCAT_WS( $separateur, `code_cpv_2` , `code_cpv_3` , `code_cpv_4` )) WHERE  `code_cpv_2` NOT LIKE '' AND `code_cpv_2` NOT LIKE $like; ";
        $sql .= " UPDATE `CategorieLot` SET `code_cpv_2` = CONCAT( $separateur, CONCAT_WS( $separateur, `code_cpv_2` , `code_cpv_3` , `code_cpv_4` )) WHERE  (`code_cpv_2` NOT LIKE '' AND `code_cpv_2` IS NOT NULL AND `code_cpv_2` NOT LIKE $like) AND (`code_cpv_3` NOT LIKE '' AND `code_cpv_4` NOT LIKE '') ; ";
        $sql .= " UPDATE `CategorieLot` SET `code_cpv_2` = CONCAT( $separateur, `code_cpv_2` ) WHERE  (`code_cpv_2` NOT LIKE '' AND `code_cpv_2` IS NOT NULL AND `code_cpv_2` NOT LIKE $like) AND (`code_cpv_3` LIKE '' AND `code_cpv_4` LIKE '') ; ";
        $sql .= " UPDATE `CategorieLot` SET `code_cpv_2` = CONCAT( $separateur, CONCAT_WS( $separateur, `code_cpv_2` , `code_cpv_4` )) WHERE  (`code_cpv_2` NOT LIKE '' AND `code_cpv_2` IS NOT NULL AND `code_cpv_2` NOT LIKE $like) AND (`code_cpv_3` LIKE '' AND `code_cpv_4` NOT LIKE '') ; ";
        $sql .= " UPDATE `CategorieLot` SET `code_cpv_2` = CONCAT( $separateur, CONCAT_WS( $separateur, `code_cpv_2` , `code_cpv_3` )) WHERE  (`code_cpv_2` NOT LIKE '' AND `code_cpv_2` IS NOT NULL AND `code_cpv_2` NOT LIKE $like) AND (`code_cpv_3` NOT LIKE '' AND `code_cpv_4` LIKE '') ; ";
        try {
            Atexo_Db::getLinkOrganism($organisme)->query($sql);
            Atexo_Db::getLinkCommon()->query($sql);
            Atexo_Db::closeOrganism($organisme);
            echo "\t                     migration cpv : succes \r\n";
        } catch (Exception $ex) {
            Propel::log($organisme.' - migration code cpv : '.$sql, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }

    public static function migrationCodesNace($organisme)
    {
        $separateur = "'#'";
        $like = "'#%'";
        $sql = " UPDATE `Entreprise`  SET `codeape` = CONCAT( $separateur, `codeape` ) WHERE `codeape` NOT LIKE $like; ";
        try {
            Atexo_Db::getLinkCommon()->query($sql);
            echo "\t                     migration nace : succes \r\n";
        } catch (Exception $ex) {
            Propel::log($organisme.' - migration code nace : '.$sql, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }

    public static function migrationNomFichierDce()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo '.................................... ORGANISME: '.$uneOrganisme->getAcronyme()." ....................................\n";
                $c = new Criteria();
                $telechargement = CommonTelechargementPeer::doSelect($c, Propel::getConnection($uneOrganisme->getAcronyme()));
                try {
                    if (is_array($telechargement) && count($telechargement)) {
                        foreach ($telechargement as $unTelechargement) {
                            if ('DCE intï¿½gral' == $unTelechargement->getNomsFichiersDce()) {
                                $unTelechargement->setNomsFichiersDce(Atexo_Config::getParameter('DEFINE_DCE_INTEGRAL'));
                                $unTelechargement->save(Propel::getConnection($uneOrganisme->getAcronyme()));
                                echo "\t                     migration effectuee avec succes \r\n";
                            }
                        }
                    }
                } catch (Exception $ex) {
                    Propel::log($uneOrganisme->getAcronyme().' - migration Text DCE_INTEGRAL : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function scriptsParametrageTraductionBDIntl()
    {
        $scriptCOMMON = [];
        $scriptOrganisme = [];
        // Contenu de la table 'CommonDocumentsAttaches'
        /*$scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('1', 'Statuti e certificati di registrazione', 'Statuts et certificats d''enregistrement', '', 'Status and registration certificates', '', '', '', '', 'Statuti e certificati di registrazione')  ON DUPLICATE KEY UPDATE `nom_document` = 'Statuti e certificati di registrazione', `nom_document_fr` = 'Statuts et certificats d''enregistrement', `nom_document_en` = 'Status and registration certificates', `nom_document_it` = 'Statuti e certificati di registrazione';";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('2', 'Certificati di pagamento delle imposte', 'Attestations fiscales', '', 'Certificates of tax payment', '', '', '', '', 'Certificati di pagamento delle imposte') ON DUPLICATE KEY UPDATE `nom_document` = 'Certificati di pagamento delle imposte', `nom_document_fr` = 'Attestations fiscales', `nom_document_en` = 'Certificates of tax payment', `nom_document_it` = 'Certificati di pagamento delle imposte' ;";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('3', 'Certificati di pagamento dei contributi sociali', 'Attestations sociales', '', 'Certificates of social tax payment', '', '', '', '', 'Certificati di pagamento dei contributi sociali') ON DUPLICATE KEY UPDATE  `nom_document` = 'Certificati di pagamento dei contributi sociali', `nom_document_fr` = 'Attestations sociales', `nom_document_en` = 'Certificates of social tax payment', `nom_document_it` = 'Certificati di pagamento dei contributi sociali' ;";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('4', 'Certificati di assicurazione', 'Attestations d''assurance', '', 'Certificates of insurance', '', '', '', '', 'Certificati di assicurazione') ON DUPLICATE KEY UPDATE  `nom_document` = 'Certificati di assicurazione', `nom_document_fr` = 'Attestations d''assurance', `nom_document_en` = 'Certificates of insurance', `nom_document_it` = 'Certificati di assicurazione';";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('5', 'Certificato di non-condanna', 'Attestation de non condamnation', '', 'Certificate of non-conviction', '', '', '', '', 'Certificato di non-condanna') ON DUPLICATE KEY UPDATE `nom_document` = 'Certificato di non-condanna', `nom_document_fr` = 'Attestation de non condamnation',`nom_document_en` = 'Certificate of non-conviction',`nom_document_it` = 'Certificato di non-condanna'  ;";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('6', 'Delega di firma', 'Dï¿½lï¿½gations de signature', '', 'Delegation of signature', '', '', '', '', 'Delega di firma') ON DUPLICATE KEY UPDATE `nom_document` = 'Delega di firma', `nom_document_fr` = 'Dï¿½lï¿½gations de signature', `nom_document_en` = 'Delegation of signature', `nom_document_it` = 'Delega di firma'; ";
        $scriptCOMMON[] = " INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES ('7', 'Altro', 'Autres', '', 'Other', '', '', '', '', 'Altro') ON DUPLICATE KEY UPDATE `nom_document` = 'Altro', `nom_document_fr` = 'Autres', `nom_document_en` = 'Other', `nom_document_it` = 'Altro' ; ";

        // Contenu de la table 'CommonFormejuridique'
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Societï¿½ a responsabilitï¿½ limitata', NULL , 'Societï¿½ a responsabilitï¿½ limitata', 'Sociï¿½tï¿½ ï¿½ responsabilitï¿½ limitï¿½e', 'Limited company ', '', '', '', '', '', 'Societï¿½ a responsabilitï¿½ limitata') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Societï¿½ a responsabilitï¿½ limitata', `libelle_formejuridique_fr` = 'Sociï¿½tï¿½ ï¿½ responsabilitï¿½ limitï¿½e', `libelle_formejuridique_en` = 'Limited company', `libelle_formejuridique_it` = 'Societï¿½ a responsabilitï¿½ limitata' ;";
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Ditta individuale', NULL , 'Ditta individuale', 'Entreprise individuelle', 'Sole proprietorship', '', '', '', '', '', 'Ditta individuale') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Ditta individuale', `libelle_formejuridique_fr` = 'Entreprise individuelle', `libelle_formejuridique_en` = 'Sole proprietorship', `libelle_formejuridique_it` = 'Ditta individuale' ;";
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Partenariato', NULL , 'Partenariato', 'Partenariat', 'Partnership', '', '', '', '', '', 'Partenariato') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Partenariato', `libelle_formejuridique_fr` = 'Partenariat', `libelle_formejuridique_en` = 'Partnership', `libelle_formejuridique_it` = 'Partenariato' ;";
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Ente pubblico', NULL , 'Ente pubblico', 'Etablissement public', 'Public body', '', '', '', '', '', 'Ente pubblico') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Ente pubblico', `libelle_formejuridique_fr` = 'Etablissement public', `libelle_formejuridique_en` = 'Public body', `libelle_formejuridique_it` = 'Ente pubblico' ;";
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Cooperativa', NULL , 'Cooperativa', 'Cooperative', 'Co-operative', '', '', '', '', '', 'Cooperativa') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Cooperativa', `libelle_formejuridique_fr` = 'Cooperative', `libelle_formejuridique_en` = 'Co-operative', `libelle_formejuridique_it` = 'Cooperativa' ;";
        $scriptCOMMON[] = " INSERT INTO `formejuridique` (`formejuridique` ,`ordre` ,`libelle_formejuridique` ,`libelle_formejuridique_fr` ,`libelle_formejuridique_en` ,`libelle_formejuridique_es` ,`libelle_formejuridique_su` ,`libelle_formejuridique_du` ,`libelle_formejuridique_cz` ,`libelle_formejuridique_ar` ,`libelle_formejuridique_it`)VALUES ('Altro', NULL , 'Altro', 'Autre', 'Other', '', '', '', '', '', 'Altro') ON DUPLICATE KEY UPDATE `libelle_formejuridique` = 'Altro', `libelle_formejuridique_fr` = 'Autre', `libelle_formejuridique_en` = 'Other', `libelle_formejuridique_it` = 'Altro' ;";

        // table TypeAvis
        $scriptCOMMON[] = " UPDATE `TypeAvis` SET `intitule_avis_en` = 'Preinformation notice', `intitule_avis_es` = 'Anuncio previo', `intitule_avis_it` = 'Avviso di preinformazione' WHERE `TypeAvis`.`id` =2; ";
        $scriptCOMMON[] = " UPDATE `TypeAvis` SET `intitule_avis_en` = 'Contract notice', `intitule_avis_es` = 'Anuncio de licitaciï¿½n', `intitule_avis_it` = 'Bando di gara' WHERE `TypeAvis`.`id` =3; ";
        $scriptCOMMON[] = " UPDATE `TypeAvis` SET `intitule_avis_en` = 'Contract award notice', `intitule_avis_es` = 'Anuncio de adjudicaciï¿½n', `intitule_avis_it` = 'Avviso di aggiudicazione' WHERE `TypeAvis`.`id` =4; ";

        // table TypeProcedure
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Appel d''offres ouvert', `libelle_type_procedure_en` = 'Open procedure', `libelle_type_procedure_it` = 'Procedura apperta' WHERE `TypeProcedure`.`id_type_procedure` =1 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Appel d''offres restreint', `libelle_type_procedure_en` = 'Restricted procedure', `libelle_type_procedure_it` = 'Procedura ristretta' WHERE `TypeProcedure`.`id_type_procedure` =2 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Procï¿½dure adaptï¿½e', `libelle_type_procedure_en` = 'Procedure below thresholds', `libelle_type_procedure_it` = 'Procedura sotto soglia' WHERE `TypeProcedure`.`id_type_procedure` =3 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Concours ouvert', `libelle_type_procedure_en` = 'Open design contest', `libelle_type_procedure_it` = 'Concorso apperto' WHERE `TypeProcedure`.`id_type_procedure` =4 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Concours restreint', `libelle_type_procedure_en` = 'Restricted design contest', `libelle_type_procedure_it` = 'Concorso ristretto' WHERE `TypeProcedure`.`id_type_procedure` =5 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Dialogue compï¿½titif', `libelle_type_procedure_en` = 'Competitive dialogue', `libelle_type_procedure_it` = 'Dialogo competitivo' WHERE `TypeProcedure`.`id_type_procedure` =7 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Marchï¿½ nï¿½gociï¿½', `libelle_type_procedure_en` = 'Negociated procedure', `libelle_type_procedure_it` = 'Procedura negoziata' WHERE `TypeProcedure`.`id_type_procedure` =8 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Procï¿½dure autre', `libelle_type_procedure_en` = 'Other procedure', `libelle_type_procedure_it` = 'Altra procedura' WHERE `TypeProcedure`.`id_type_procedure` =15 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Accord-Cadre - Sï¿½lection des Attributaires', `libelle_type_procedure_en` = 'Framework agreement - Selection of suppliers', `libelle_type_procedure_it` = 'Accordo quadro - qualificazione dei titolare' WHERE `TypeProcedure`.`id_type_procedure` =30 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Accord-Cadre - Marchï¿½ Subsï¿½quent', `libelle_type_procedure_en` = 'Framework agreement - Individual contract', `libelle_type_procedure_it` = 'Accordo quadro - apalto' WHERE `TypeProcedure`.`id_type_procedure` =31 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Systï¿½me d''Acquisition Dynamique - Admission', `libelle_type_procedure_en` = 'Dynamic purchasing system - Admission', `libelle_type_procedure_it` = 'Sistema dinamico di acquisizione - ammissione' WHERE `TypeProcedure`.`id_type_procedure` =32 ; ";
        $scriptCOMMON[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Systï¿½me d''Acquisition Dynamique - Marchï¿½ Spï¿½cifique', `libelle_type_procedure_en` = 'Dynamic purchasing system - Specific contract', `libelle_type_procedure_it` = 'Sistema dinamico di acquisizione - appalto' WHERE `TypeProcedure`.`id_type_procedure` =33 ; ";

        // table Service_Mertier_Profils
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Administrateur Entitï¿½ Publique',`libelle_fr` = 'Administrateur Entitï¿½ Publique', `libelle_en` = 'Administrator Public Entity', `libelle_it` = 'Amministratore Ente Pubblico' WHERE `id_service_metier` = 1 AND `id_externe` = 1 ; ";
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Administrateur Service',`libelle_fr` = 'Administrateur Service', `libelle_en` = 'Administrator Department', `libelle_it` = 'Amministratore Servizio' WHERE `id_service_metier` = 1 AND `id_externe` = 2 ; ";
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Acheteur',`libelle_fr` = 'Acheteur', `libelle_en` = 'Buyer', `libelle_it` = 'Acquirente' WHERE `id_service_metier` = 1 AND `id_externe` = 4 ; ";
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Administrateur / Acheteur',`libelle_fr` = 'Administrateur / Acheteur', `libelle_en` = 'Administrator / Buyer', `libelle_it` = 'Amministratore / Acquirente' WHERE `id_service_metier` = 1 AND `id_externe` = 5 ; ";
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Acheteur MAPA < 90 kEUR',`libelle_fr` = 'Acheteur MAPA < 90 kEUR', `libelle_en` = 'Buyer below threshold < 20 kEUR', `libelle_it` = 'Acquirente sotto soglia < 20 kEUR' WHERE `id_service_metier` = 1 AND `id_externe` = 6 ; ";
        $scriptCOMMON[] = " UPDATE `Service_Mertier_Profils` SET `libelle` = 'Observateur',`libelle_fr` = 'Observateur', `libelle_en` = 'Viewer', `libelle_it` = 'Osservatore' WHERE `id_service_metier` = 1 AND `id_externe` = 8 ; ";
        */

        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = 'ï¿½ l''appel d''offres avec prï¿½sï¿½lection' WHERE `ValeurReferentiel`.`id` =30 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'au concours' WHERE `ValeurReferentiel`.`id` =33 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'dossier du concours' WHERE `ValeurReferentiel`.`id` =34 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'des articles 51 et 53' WHERE `ValeurReferentiel`.`id` =35 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Ministï¿½re ou toute autre autoritï¿½ nationale ou fï¿½dï¿½rale, y compris leurs subdivisions rï¿½gionales ou locales' WHERE `ValeurReferentiel`.`id` =36 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Agence/office national(e) ou fï¿½dï¿½ral(e)' WHERE `ValeurReferentiel`.`id` =37 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Collectivitï¿½ territoriale' WHERE `ValeurReferentiel`.`id` =38 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Agence/office rï¿½gional(e) ou local(e)' WHERE `ValeurReferentiel`.`id` =39 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Organisme de droit public' WHERE `ValeurReferentiel`.`id` =40 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Institution/agence europï¿½enne ou organisation europï¿½enne' WHERE `ValeurReferentiel`.`id` =41 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Autre' WHERE `ValeurReferentiel`.`id` =42 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Services gï¿½nï¿½raux des administrations publiques' WHERE `ValeurReferentiel`.`id` =43 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Dï¿½fense' WHERE `ValeurReferentiel`.`id` =44 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Ordre et sï¿½curitï¿½ publique' WHERE `ValeurReferentiel`.`id` =45 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = ' ',`libelle_valeur_referentiel_fr` = 'Environnement' WHERE `ValeurReferentiel`.`id` =46 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Affaires ï¿½conomiques et financiï¿½res' WHERE `ValeurReferentiel`.`id` =47 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Santï¿½' WHERE `ValeurReferentiel`.`id` =48 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Production, transport et distribution de gaz ou de chaleur' WHERE `ValeurReferentiel`.`id` =49 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Electricitï¿½s' WHERE `ValeurReferentiel`.`id` =50 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Prospection et extraction de pï¿½trole et de gaz' WHERE `ValeurReferentiel`.`id` =51 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Prospection et extraction de charbon ou autres combustibles' WHERE `ValeurReferentiel`.`id` =52 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Eau' WHERE `ValeurReferentiel`.`id` =53 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Services postaux' WHERE `ValeurReferentiel`.`id` =54 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Services de chemin de fer' WHERE `ValeurReferentiel`.`id` =55 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Services de chemin de fer urbains, de tramway ou d''autobus' WHERE `ValeurReferentiel`.`id` =56 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Activitï¿½s portuaires' WHERE `ValeurReferentiel`.`id` =57 ; ";
        $scriptCOMMON[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '',`libelle_valeur_referentiel_fr` = 'Activitï¿½s aï¿½roportuaires' WHERE `ValeurReferentiel`.`id` =58 ; ";

        /*
                //  table ReferentielTypeXml
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('2' , '2#3', 'OJEU_02 - Avis de marchï¿½', 'OJEU_02 - Avis de marchï¿½', 'OJEU_02 - Contract notice', '', '', '', '', '', 'GUUE_02 - Bando di gara', NULL) ON DUPLICATE KEY UPDATE `libelle_type` = 'OJEU_02 - Avis de marchï¿½', `libelle_type_fr` = 'OJEU_02 - Avis de marchï¿½', `libelle_type_en` = 'OJEU_02 - Contract notice', `libelle_type_it` = 'GUUE_02 - Bando di gara';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('4', '2#3', 'OJEU_03 - Avis d''attribution', 'OJEU_03 - Avis d''attribution', 'OJEU_03 - Contract award notice', '', '', '', '', '', 'GUUE_03 - Avviso relativo agli appalti aggiudicati', '13') ON DUPLICATE KEY UPDATE `libelle_type` = 'OJEU_03 - Avis d''attribution', `libelle_type_fr` = 'OJEU_03 - Avis d''attribution', `libelle_type_en` = 'OJEU_03 - Contract award notice', `libelle_type_it` = 'GUUE_03 - Avviso relativo agli appalti aggiudicati';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('9' , '2#3', 'OJEU_12 - Avis Concours', 'OJEU_12 - Avis Concours', 'OJEU_12 - Design contest notice', '', '', '', '', '', 'GUUE_12 - Avviso di concorso di progettazione', NULL) ON DUPLICATE KEY UPDATE `libelle_type` = 'OJEU_12 - Avis Concours', `libelle_type_fr` = 'OJEU_12 - Avis Concours', `libelle_type_en` = 'OJEU_12 - Design contest notice', `libelle_type_it` = 'GUUE_12 - Avviso di concorso di progettazione';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('10' , '2#3', 'OJEU_14 - Formulaire rectificatif / annulation', 'OJEU_14 - Formulaire rectificatif / annulation', 'OJEU_14 - Additional information corrigendum', '', '', '', '', '', 'GUUE_14 - Rettifiche', NULL) ON DUPLICATE KEY UPDATE `libelle_type` = '+OJEU_14 - Formulaire rectificatif / annulation',`libelle_type_fr` = '+OJEU_14 - Formulaire rectificatif / annulation',`libelle_type_en` = '+OJEU_14 - Additional information corrigendum',`libelle_type_it` = '+GUUE_14 - Rettifiche' ;";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('11' , '2#3', 'OJEU_05 - Secteurs Spï¿½ciaux', 'OJEU_05 - Secteurs Spï¿½ciaux', 'OJEU_05 - Contract notice - Utilities', '', '', '', '', '', 'GUUE_05 - Bando di gara - Settori speciali', NULL) ON DUPLICATE KEY UPDATE  `libelle_type` = 'OJEU_05 - Secteurs Spï¿½ciaux',`libelle_type_fr` = 'OJEU_05 - Secteurs Spï¿½ciaux',`libelle_type_en` = 'OJEU_05 - Contract notice - Utilities',`libelle_type_it` = 'GUUE_05 - Bando di gara - Settori speciali';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('12' , '2', 'OJEU_13 - Rï¿½sultat Concours', 'OJEU_13 - Rï¿½sultat Concours', 'OJEU_13 - Results of design contest', '', '', '', '', '', 'GUUE_13 - Risultati di concorsi di progettazione', '9') ON DUPLICATE KEY UPDATE  `libelle_type` = 'OJEU_13 - Rï¿½sultat Concours',`libelle_type_fr` = 'OJEU_13 - Rï¿½sultat Concours',`libelle_type_en` = 'OJEU_13 - Results of design contest',`libelle_type_it` = 'GUUE_13 - Risultati di concorsi di progettazione' ;";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('14' , '2', 'OJEU_10 - Concession de travaux publics', 'OJEU_10 - Concession de travaux publics', 'OJEU_10 - Public works concession', '', '', '', '', '', 'OJEU_10 - Concessione di lavori pubblici', NULL) ON DUPLICATE KEY UPDATE  `libelle_type` = 'OJEU_10 - Concession de travaux publics', `libelle_type_fr` = 'OJEU_10 - Concession de travaux publics', `libelle_type_en` = 'OJEU_10 - Public works concession', `libelle_type_it` = 'OJEU_10 - Concessione di lavori pubblici';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('15' , '2', 'OJEU_01T - Avis de prï¿½information - Travaux', 'OJEU_01T - Avis de prï¿½information - Travaux', 'OJEU_01T - Prior information notice - Works', '', '', '', '', '', 'OJEU_01T - Avviso di preinformazione - Lavori', NULL) ON DUPLICATE KEY UPDATE  `libelle_type` = 'OJEU_01T - Avis de prï¿½information - Travaux', `libelle_type_fr` = 'OJEU_01T - Avis de prï¿½information - Travaux', `libelle_type_en` = 'OJEU_01T - Prior information notice - Works', `libelle_type_it` = 'OJEU_01T - Avviso di preinformazione - Lavori';";
                $scriptOrganisme[] = " INSERT INTO `ReferentielTypeXml` (`id` ,`id_destinataire` ,`libelle_type` ,`libelle_type_fr` ,`libelle_type_en` ,`libelle_type_es` ,`libelle_type_su` ,`libelle_type_du` ,`libelle_type_cz` ,`libelle_type_ar` ,`libelle_type_it` ,`id_avis_marche`)VALUES ('16' , '2', 'OJEU_01FS - Avis de prï¿½information - Fournitures / Services', 'OJEU_01FS - Avis de prï¿½information - Fournitures / Services', 'OJEU_01FS - Prior information notice - Supplies / Services', '', '', '', '', '', 'OJEU_01FS - Avviso di preinformazione - Forniture / Servizi', NULL) ON DUPLICATE KEY UPDATE  `libelle_type` = 'OJEU_01FS - Avis de prï¿½information - Fournitures / Services', `libelle_type_fr` = 'OJEU_01FS - Avis de prï¿½information - Fournitures / Services', `libelle_type_en` = 'OJEU_01FS - Prior information notice - Supplies / Services', `libelle_type_it` = 'OJE';";

                //  table TypeProcedure
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Appel d''offres ouvert', `libelle_type_procedure_en` = 'Open procedure', `libelle_type_procedure_it` = 'Procedura apperta' WHERE `TypeProcedure`.`id_type_procedure` =1; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Appel d''offres restreint - Candidature', `libelle_type_procedure_en` = 'Restricted procedure - preselection', `libelle_type_procedure_it` = 'Procedura ristretta - qualificazione' WHERE `TypeProcedure`.`id_type_procedure` =2; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Appel d''offres restreint - Offre', `libelle_type_procedure_en` = 'Restricted procedure - bid', `libelle_type_procedure_it` = 'Procedura ristretta - offerta' WHERE `TypeProcedure`.`id_type_procedure` =3; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Procï¿½dure adaptï¿½e < 90 k EUR HT', `libelle_type_procedure_en` = 'Procedure below thresholds < 90 k EUR', `libelle_type_procedure_it` = 'Procedura sotto soglia < 90 k EUR' WHERE `TypeProcedure`.`id_type_procedure` =4; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Procï¿½dure adaptï¿½e &#8805; 90 k EUR HT', `libelle_type_procedure_en` = 'Procedure below thresholds &#8805; 90 k EUR', `libelle_type_procedure_it` = 'Procedura sotto soglia &#8805; 90 k EUR' WHERE `TypeProcedure`.`id_type_procedure` =5; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Concours ouvert', `libelle_type_procedure_en` = 'Open design contest', `libelle_type_procedure_it` = 'Concorso apperto' WHERE `TypeProcedure`.`id_type_procedure` =8; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Concours restreint - Candidature', `libelle_type_procedure_en` = 'Restricted design contest - preselection', `libelle_type_procedure_it` = 'Concorso ristretto - qualificazione' WHERE `TypeProcedure`.`id_type_procedure` =9; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Concours restreint - Offre', `libelle_type_procedure_en` = 'Restricted design contest - bid', `libelle_type_procedure_it` = 'Concorso ristretto - offerta' WHERE `TypeProcedure`.`id_type_procedure` =10; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Dialogue compï¿½titif - Candidature', `libelle_type_procedure_en` = 'Competitive dialogue - preselection', `libelle_type_procedure_it` = 'Dialogo competitivo - qualificazione' WHERE `TypeProcedure`.`id_type_procedure` =11; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Dialogue compï¿½titif - Offre', `libelle_type_procedure_en` = 'Competitive dialogue - bid', `libelle_type_procedure_it` = 'Dialogo competitivo - offerta' WHERE `TypeProcedure`.`id_type_procedure` =12; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Procï¿½dure Autre', `libelle_type_procedure_en` = 'Other procedure', `libelle_type_procedure_it` = 'Altra procedura' WHERE `TypeProcedure`.`id_type_procedure` =17; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Marchï¿½ nï¿½gociï¿½', `libelle_type_procedure_en` = 'Negociated procedure', `libelle_type_procedure_it` = 'Procedura negoziata' WHERE `TypeProcedure`.`id_type_procedure` =18; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Accord-Cadre - 1 Sï¿½lection des Attributaires', `libelle_type_procedure_en` = 'Framework agreement - 1 Selection of suppliers', `libelle_type_procedure_it` = 'Accordo quadro - 1 Qualificazione dei titolare' WHERE `TypeProcedure`.`id_type_procedure` =30; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Accord-Cadre - 2 Marchï¿½ Subsï¿½quent', `libelle_type_procedure_en` = 'Framework agreement - 2 Individual contract', `libelle_type_procedure_it` = 'Accordo quadro - 2 Apalto' WHERE `TypeProcedure`.`id_type_procedure` =31; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Systï¿½me d''Acquisition Dynamique - Admission', `libelle_type_procedure_en` = 'Dynamic purchasing system - Admission', `libelle_type_procedure_it` = 'Sistema dinamico di acquisizione - Ammissione' WHERE `TypeProcedure`.`id_type_procedure` =32; ";
                $scriptOrganisme[] = " UPDATE `TypeProcedure` SET `libelle_type_procedure_fr` = 'Systï¿½me d''Acquisition Dynamique - Marchï¿½ Spï¿½cifique', `libelle_type_procedure_en` = 'Dynamic purchasing system - Specific contract', `libelle_type_procedure_it` = 'Sistema dinamico di acquisizione - Appalto' WHERE `TypeProcedure`.`id_type_procedure` =33; ";
        */
        //Contenu de la table EchangeTypeAR
        $scriptOrganisme[] = " UPDATE `EchangeTypeAR` SET `libelle` = 'Senza soggetto',`libelle_fr` = 'Sans objet',`libelle_en` = 'No subject',`libelle_it` = 'Senza soggetto' WHERE `EchangeTypeAR`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `EchangeTypeAR` SET `libelle` = 'Non letto',`libelle_fr` = 'Non retirï¿½',`libelle_en` = 'Not read',`libelle_it` = 'Non letto' WHERE `EchangeTypeAR`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `EchangeTypeAR` SET `libelle` = 'Data e ora',`libelle_fr` = 'Date et heure',`libelle_en` = 'Date and time',`libelle_it` = 'Data e ora' WHERE `EchangeTypeAR`.`id` =3 ; ";

        //Contenu de la table EchangeFormat
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'Scambi sulla piattaforma', `libelle_fr` = 'Echange plate-forme', `libelle_en` = 'Exchange through platform', `libelle_it` = 'Scambi sulla piattaforma' WHERE `EchangeFormat`.`id` = 1; ";
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'E-mail fuori piattaforma', `libelle_fr` = 'Courriel hors plate-forme', `libelle_en` = 'E-mail beyond platform', `libelle_it` = 'E-mail fuori piattaforma' WHERE `EchangeFormat`.`id` = 2; ";
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'Posta raccomandata', `libelle_fr` = 'Courrier recommandï¿½', `libelle_en` = 'Certified mail', `libelle_it` = 'Posta raccomandata' WHERE `EchangeFormat`.`id` = 3; ";
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'Posta semplice', `libelle_fr` = 'Courrier simple', `libelle_en` = 'Simple mail', `libelle_it` = 'Posta semplice' WHERE `EchangeFormat`.`id` = 4; ";
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'Fax', `libelle_fr` = 'Tï¿½lï¿½copie', `libelle_en` = 'Fax', `libelle_it` = 'Fax' WHERE `EchangeFormat`.`id` = 5; ";
        $scriptOrganisme[] = " UPDATE `EchangeFormat` SET `libelle` = 'Altro', `libelle_fr` = 'Autre', `libelle_en` = 'Other', `libelle_it` = 'Altro' WHERE `EchangeFormat`.`id` = 6; ";
        //contenu de la table DecisionPassationMarcheAVenir
        $scriptOrganisme[] = " UPDATE `DecisionPassationMarcheAVenir` SET `libelle` = 'Attribuito',`libelle_fr` = 'Attribuï¿½',`libelle_en` = 'Awarded',`libelle_it` = 'Attribuito' WHERE `DecisionPassationMarcheAVenir`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `DecisionPassationMarcheAVenir` SET `libelle` = 'Infruttuoso',`libelle_fr` = 'Infructueux',`libelle_en` = 'Unsuccessful',`libelle_it` = 'Infruttuoso' WHERE `DecisionPassationMarcheAVenir`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `DecisionPassationMarcheAVenir` SET `libelle` = 'Senza seguito',`libelle_fr` = 'Sans suite',`libelle_en` = 'No action',`libelle_it` = 'Senza seguito' WHERE `DecisionPassationMarcheAVenir`.`id` =3 ; ";
        //contenu de la table DecisionPassationConsultation
        $scriptOrganisme[] = " UPDATE `DecisionPassationConsultation` SET `libelle` = 'Continua',`libelle_fr` = 'Poursuite',`libelle_en` = 'Continued',`libelle_it` = 'Continua' WHERE `DecisionPassationConsultation`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `DecisionPassationConsultation` SET `libelle` = 'Infruttuoso',`libelle_fr` = 'Infructueux',`libelle_en` = 'Unsuccessful',`libelle_it` = 'Infruttuoso' WHERE `DecisionPassationConsultation`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `DecisionPassationConsultation` SET `libelle` = 'Senza seguito',`libelle_fr` = 'Sans suite',`libelle_en` = 'No action',`libelle_it` = 'Senza seguito' WHERE `DecisionPassationConsultation`.`id` =3 ; ";
        //contenu de la table AvisCao
        $scriptOrganisme[] = " UPDATE `AvisCao` SET `libelle` = 'Favorevole',`libelle_fr` = 'Avis favorable',`libelle_en` = 'Favorable',`libelle_it` = 'Favorevole' WHERE `AvisCao`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `AvisCao` SET `libelle` = 'Sfavorevole',`libelle_fr` = 'Avis dï¿½favorable',`libelle_en` = 'Unfavorable',`libelle_it` = 'Sfavorevole' WHERE `AvisCao`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `AvisCao` SET `libelle` = 'Senza seguito',`libelle_fr` = 'Sans suite',`libelle_en` = 'No action',`libelle_it` = 'Senza seguito' WHERE `AvisCao`.`id` =3 ; ";
        //contenu de la table TypeAvenant
        $scriptOrganisme[] = " UPDATE `TypeAvenant` SET `libelle` = '1-Nessuna incidenza finanziaria',`libelle_fr` = '1-Sans incidence financiï¿½re',`libelle_en` = '1-No cost impact',`libelle_it` = '1-Nessuna incidenza finanziaria' WHERE `TypeAvenant`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `TypeAvenant` SET `libelle` = '2-Emendamento aumentando l''importo iniziale',`libelle_fr` = '2-Avenant augmentant le montant initial',`libelle_en` = '2-Change order increasing contract initial price',`libelle_it` = '2-Emendamento aumentando l''importo iniziale' WHERE `TypeAvenant`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `TypeAvenant` SET `libelle` = '3-Emendamento riducendo l''importo iniziale',`libelle_fr` = '3-avenant diminuant le montant initial',`libelle_en` = '2-Change order reducing contract initial price',`libelle_it` = '3-Emendamento riducendo l''importo iniziale' WHERE `TypeAvenant`.`id` =3 ; ";
        //contenu de la table mode_execution_contrat
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '1 - Ordinario (senza tranche, solo a prezzo fisso)', `libelle_fr` = '1 - Ordinaire (Sans tranche, exclusivement ï¿½ prix forfaitaire)', `libelle_en` = '1 - Regular (no tranch, only lump-sum prices)', `libelle_it` = '1 - Ordinario (senza tranche, solo a prezzo fisso)' WHERE `mode_execution_contrat`.`id` = 1; ";
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '2 - A ordini (senza tranche, solo con ordini d''acquisiti)', `libelle_fr` = '2 - A bons de commande (Sans tranche, exclusivement ï¿½ bons de commande)', `libelle_en` = '2 - Umbrella (no tranch, only with purchase orders)', `libelle_it` = '2 - A ordini (senza tranche, solo con ordini d''acquisiti)' WHERE `mode_execution_contrat`.`id` = 2; ";
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '3 - A tranches (con tranche ferma e tranches condizionali, solo a prezzo fisso)', `libelle_fr` = '3 - A Tranches (Avec TF et TC, exclusivement ï¿½ prix forfaitaire)', `libelle_en` = '3 - With tranches (with firm tranch and conditional tranches, only lump-sum prices)', `libelle_it` = '3 - A tranches (con tranche ferma e tranches condizionali, solo a prezzo fisso)' WHERE `mode_execution_contrat`.`id` = 3; ";
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '4 - A tranche e a ordini (con tranche ferma e tranches condizionali, in parte a ordini)',`libelle_fr` = '4 - A tranches et bons de commande (Avec TF et TC, en partie ï¿½ bons de commande)',`libelle_en` = '4 - With tranches and umbrella (with firm tranch and conditional tranches, partially with purchase orders)',`libelle_it` = '4 - A tranche e a ordini (con tranche ferma e tranches condizionali, in parte a ordini)' WHERE `mode_execution_contrat`.`id` =4 ; ";
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '5 - A fase',`libelle_fr` = '5 - A phases',`libelle_en` = '5 - With phases',`libelle_it` = '5 - A fase' WHERE `mode_execution_contrat`.`id` =5 ; ";
        $scriptOrganisme[] = " UPDATE `mode_execution_contrat` SET `libelle` = '6 - Altri',`libelle_fr` = '6 - Autres',`libelle_en` = '6 - Other',`libelle_it` = '6 - Altri' WHERE `mode_execution_contrat`.`id` =6 ; ";
        //contenu de la table nature_acte_juridique
        $scriptOrganisme[] = " UPDATE `nature_acte_juridique` SET `libelle` = '1-Appalto iniziale',`libelle_fr` = '1-Contrat initial',`libelle_en` = '1-Initial contract',`libelle_it` = '1-Appalto iniziale' WHERE `nature_acte_juridique`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `nature_acte_juridique` SET `libelle` = '2-Appalto supplementare',`libelle_fr` = '2-Contrat complï¿½mentaire',`libelle_en` = '2-Additional contract',`libelle_it` = '2-Appalto supplementare' WHERE `nature_acte_juridique`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `nature_acte_juridique` SET `libelle` = '3-Contratto sulla base di un accordo quadro',`libelle_fr` = '3-Contrat sur la base d''un accord cadre',`libelle_en` = '3-Individual contract under a framework agreement',`libelle_it` = '3-Contratto sulla base di un accordo quadro' WHERE `nature_acte_juridique`.`id` =3 ; ";
        $scriptOrganisme[] = " UPDATE `nature_acte_juridique` SET `libelle` = '4-Appalto di definizione',`libelle_fr` = '4-Marchï¿½ de dï¿½finition',`libelle_en` = '4-Design contract',`libelle_it` = '4-Appalto di definizione' WHERE `nature_acte_juridique`.`id` =4 ; ";
        $scriptOrganisme[] = " UPDATE `nature_acte_juridique` SET `libelle` = '5-Altri',`libelle_fr` = '5-Autre',`libelle_en` = '5-Other',`libelle_it` = '5-Altri' WHERE `nature_acte_juridique`.`id` =5 ; ";
        //contenu de la table ValeurReferentiel
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Stato',`libelle_valeur_referentiel_fr` = 'Etat',`libelle_valeur_referentiel_en` = 'State',`libelle_valeur_referentiel_it` = 'Stato' WHERE `ValeurReferentiel`.`id` =1 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Regione',`libelle_valeur_referentiel_fr` = 'Rï¿½gion',`libelle_valeur_referentiel_en` = 'Region',`libelle_valeur_referentiel_it` = 'Regione' WHERE `ValeurReferentiel`.`id` =2 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Reparto',`libelle_valeur_referentiel_fr` = 'Dï¿½partement',`libelle_valeur_referentiel_en` = 'County',`libelle_valeur_referentiel_it` = 'Reparto' WHERE `ValeurReferentiel`.`id` =3 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Cittï¿½',`libelle_valeur_referentiel_fr` = 'Commune',`libelle_valeur_referentiel_en` = 'City',`libelle_valeur_referentiel_it` = 'Cittï¿½' WHERE `ValeurReferentiel`.`id` =4 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Ente pubblico nazionale',`libelle_valeur_referentiel_fr` = 'Etablissement public national',`libelle_valeur_referentiel_en` = 'National publi authority',`libelle_valeur_referentiel_it` = 'Ente pubblico nazionale' WHERE `ValeurReferentiel`.`id` =5 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Ente pubblico locale',`libelle_valeur_referentiel_fr` = 'Etablissement public territorial',`libelle_valeur_referentiel_en` = 'Local public authority',`libelle_valeur_referentiel_it` = 'Ente pubblico locale' WHERE `ValeurReferentiel`.`id` =6 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Altro',`libelle_valeur_referentiel_fr` = 'Autre',`libelle_valeur_referentiel_en` = 'Other',`libelle_valeur_referentiel_it` = 'Altro' WHERE `ValeurReferentiel`.`id` =7 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Sconosciuto',`libelle_valeur_referentiel_fr` = 'Non renseignï¿½',`libelle_valeur_referentiel_en` = 'Not filled-in',`libelle_valeur_referentiel_it` = 'Sconosciuto' WHERE `ValeurReferentiel`.`id` =9 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = 'Sï¿½',`libelle_valeur_referentiel_fr` = 'Oui',`libelle_valeur_referentiel_en` = 'Yes',`libelle_valeur_referentiel_it` = 'Sï¿½' WHERE `ValeurReferentiel`.`id` =10 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = 'Non',`libelle_valeur_referentiel_en` = 'No',`libelle_valeur_referentiel_it` = 'Non' WHERE `ValeurReferentiel`.`id` =11 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '4 000 - 19 999.99',`libelle_valeur_referentiel_fr` = '4 000 HT ï¿½ 19 999,99 HT',`libelle_valeur_referentiel_en` = '4 000 - 19 999.99',`libelle_valeur_referentiel_it` = '4 000 - 19 999.99' WHERE `ValeurReferentiel`.`id` =12 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '20 000 - 49 999.99',`libelle_valeur_referentiel_fr` = '20 000 HT ï¿½ 49 999,99 HT',`libelle_valeur_referentiel_en` = '20 000 - 49 999.99',`libelle_valeur_referentiel_it` = '20 000 - 49 999.99' WHERE `ValeurReferentiel`.`id` =13 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '50 000 - 89 999.99',`libelle_valeur_referentiel_fr` = '50 000 HT ï¿½ 89 999,99 HT',`libelle_valeur_referentiel_en` = '50 000 - 89 999.99',`libelle_valeur_referentiel_it` = '50 000 - 89 999.99' WHERE `ValeurReferentiel`.`id` =14 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '90 000 - 132 999.99',`libelle_valeur_referentiel_fr` = '90 000 HT ï¿½ 132 999,99 HT',`libelle_valeur_referentiel_en` = '90 000 - 132 999.99',`libelle_valeur_referentiel_it` = '90 000 - 132 999.99' WHERE `ValeurReferentiel`.`id` =15 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '133 000 - 205 999.99',`libelle_valeur_referentiel_fr` = '133 000 HT ï¿½ 205 999,99 HT',`libelle_valeur_referentiel_en` = '133 000 - 205 999.99',`libelle_valeur_referentiel_it` = '133 000 - 205 999.99' WHERE `ValeurReferentiel`.`id` =16 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '206 000 - 999 999.99',`libelle_valeur_referentiel_fr` = '206 000 HT ï¿½ 999 999,99 HT',`libelle_valeur_referentiel_en` = '206 000 - 999 999.99',`libelle_valeur_referentiel_it` = '206 000 - 999 999.99' WHERE `ValeurReferentiel`.`id` =17 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '1 000 000 - 2 999 999.99',`libelle_valeur_referentiel_fr` = '1 000 000 HT ï¿½ 2 999 999,99 HT',`libelle_valeur_referentiel_en` = '1 000 000 - 2 999 999.99',`libelle_valeur_referentiel_it` = '1 000 000 - 2 999 999.99' WHERE `ValeurReferentiel`.`id` =18 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '3 000 000 - 5 149 999.99',`libelle_valeur_referentiel_fr` = '3 000 000 HT ï¿½ 5 149 999,99 HT',`libelle_valeur_referentiel_en` = '3 000 000 - 5 149 999.99',`libelle_valeur_referentiel_it` = '3 000 000 - 5 149 999.99' WHERE `ValeurReferentiel`.`id` =19 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel` = '5 150 000 - ...',`libelle_valeur_referentiel_fr` = '5 150 000 HT et plus.',`libelle_valeur_referentiel_en` = '5 150 000 - ...',`libelle_valeur_referentiel_it` = '5 150 000 - ...' WHERE `ValeurReferentiel`.`id` =20 ; ";
        //Contenu du referentiel pour la tranche budgetaire
        /*$scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '4 000 - 19 999.99' WHERE `ValeurReferentiel`.`id` =12 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '20 000 - 49 999.99' WHERE `ValeurReferentiel`.`id` =13 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '50 000 - 89 999.99' WHERE `ValeurReferentiel`.`id` =14 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '90 000 - 132 999.99' WHERE `ValeurReferentiel`.`id` =15 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '133 000 - 205 999.99' WHERE `ValeurReferentiel`.`id` =16 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '206 000 - 999 999.99' WHERE `ValeurReferentiel`.`id` =17 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '1 000 000 - 2 999 999.99' WHERE `ValeurReferentiel`.`id` =18 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '3 000 000 - 5 149 999.99' WHERE `ValeurReferentiel`.`id` =19 ; ";
        $scriptOrganisme[] = " UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_fr` = '5 150 000 - ...' WHERE `ValeurReferentiel`.`id` =20 ; ";
        */

        //Execution des requetes de la base Commune
        $log = '';
        try {
            echo "\n MISE A JOUR DE LA BD COMMUNE : ";
            foreach ($scriptCOMMON as $query) {
                if (Atexo_Db::getLinkCommon()->query($query)) {
                    echo '.';
                } else {
                    $message = " BD COMMUNE : ERROR when executing query : $query ";
                    $log .= $message."\r\n";
                }
            }

            //Execution des requtes pour les BDD Organisme
            $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
            if ($allOrganisme) {
                foreach ($allOrganisme as $uneOrganisme) {
                    echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                    foreach ($scriptOrganisme as $query) {//echo $query;
                        if (Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($query)) {
                            echo '.';
                        } else {
                            $message = " ORGANISME $uneOrganisme->getAcronyme(): ERROR when executing query : $query ";
                            $log .= $message."\r\n";
                        }
                        Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                    }
                }
            }
            Propel::log($log, Propel::LOG_ERR);
            echo "\n\n";
        } catch (Exception $ex) {
            Propel::log($log, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }

    public static function suppressionChampCodesCpvNonUtilises()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        $sql = ' ALTER TABLE `consultation` DROP `code_cpv_3`; ';
        $sql .= ' ALTER TABLE `consultation`  DROP `code_cpv_4`; ';
        $sql .= ' ALTER TABLE `consultation`  DROP `libelle_code_cpv_1`; ';
        $sql .= ' ALTER TABLE `consultation`  DROP `libelle_code_cpv_2`; ';
        $sql .= ' ALTER TABLE `consultation`  DROP `libelle_code_cpv_3`; ';
        $sql .= ' ALTER TABLE `consultation`  DROP `libelle_code_cpv_4`; ';

        $sql .= ' ALTER TABLE `CategorieLot` DROP `code_cpv_3`; ';
        $sql .= ' ALTER TABLE `CategorieLot`  DROP `code_cpv_4`; ';
        $sql .= ' ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_1`; ';
        $sql .= ' ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_2`; ';
        $sql .= ' ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_3`; ';
        $sql .= ' ALTER TABLE `CategorieLot`  DROP `libelle_code_cpv_4`; ';

        echo "\n BASE COMMUNE : ";
        try {
            Atexo_Db::getLinkCommon()->query($sql);
            echo "\t                    champs supprimes avec succes \n";
        } catch (Exception $ex) {
            echo "\t KO \n";
            Propel::log('Base commune - erreur suppression : '.$sql, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }

        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                try {
                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    echo "\t                    champs supprimes avec succes \n";
                } catch (Exception $ex) {
                    echo "\t KO \n";
                    Propel::log($organisme.' - erreur suppression : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
                Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
            }
        }
    }

    public static function insertionDonneesTableJournaux()
    {
        $sql = " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Ouest-France'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Presse-Ocï¿½an'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Le Courrier du Pays de Retz'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - L'Echo d'Ancenis'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Eclaireur de Chï¿½teaubriant'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - L'Echo de la Presqu'Ile'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - L'Echo de l'Ouest'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - L'Hebdo de Sï¿½vre & Maine'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Loire Atlantique Agricole'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Les Infos'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - L'Informateur Judiciaire'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 44 - Le Moniteur des TP & Bï¿½timent'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Ouest-France'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Les Nouvelles l'Echo'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Le Publicateur Libre'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Avenir Agricole de la Mayenne'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Le Courrier de la Mayenne'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 53 - Le Haut Anjou'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 50 - Ouest-France'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 50 - La Presse de la Manche'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 50 - La Gazette de la Manche'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 50 - Agriculteur Normand'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Ouest-France'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - L'Action Rï¿½publicaine (Edition de Nogent)'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Le Journal de l'Orne'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - L'Orne Combattante'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - L'Orne Hebdo'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Le Perche'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Le Publicateur Libre'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Le Rï¿½veil Normand'); ";
        $sql .= " INSERT INTO `Journaux` (`ID_JOURNAL`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (null, 1, '- 61 - Agriculteur Normand'); ";

        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                try {
                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    echo "\t                    insertion effectuee avec succes. \n";
                    Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                } catch (Exception $ex) {
                    echo "\t KO \n";
                    Propel::log($uneOrganisme->getAcronyme().' - erreur insertion : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function ajoutNouveauCompteInscritEtAlertes()
    {
        //Insertion table Inscrit
        $sql = ' INSERT INTO `Inscrit` (`id`, `entreprise_id`, `login`, `mdp`, `num_cert`, `cert`, `civilite`, `nom`, `prenom`, `adresse`, `codepostal`, `ville`, `pays`, `email`, `telephone`, `categorie`, `motstitreresume`, `periode`, `siret`, `fax`, `code_cpv`, `id_langue`, `profil`, `adresse2`, `bloque`, `id_initial`, `inscrit_annuaire_defense`, `date_creation`, `date_modification`, `tentatives_mdp`, `uid`) VALUES ';
        $sql .= " (null, 1, 'alerte.v3', 'b7124ab3f60eec664940f58cd11d876ae911c9e0', NULL, NULL, 0, 'ALERTE V3', 'alertev3', '231, rue Saint-Honorï¿½', '58768', 'Paris', 'France', 'alerte.v3@atexo.com', '0153430540', NULL, NULL, 0, '00019', '', NULL, NULL, 1, 'Escalier A - 3ï¿½me ï¿½tage', '0', 0, '0', '2010-11-16 11:22:45', '2010-11-16 11:22:45', 0, NULL);";
        try {
            Atexo_Db::getLinkCommon()->query($sql);
            echo "\t                     ajout inscrit 'ALERTE V3' avec succes !!!\r\n";
        } catch (Exception $ex) {
            Propel::log(' probleme execution requete : '.$sql, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
        $inscritAjoute = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin('alerte.v3');
        if ($inscritAjoute) {
            //Insertion table Alerte
            $xml1 = '<?xml version="1.0" encoding="UTF-8"?>\n<alerte>\n	<organisme>NULL</organisme>\n	 <entiteAchat>NULL</entiteAchat>\n	 <typeAnnonce>3</typeAnnonce>\n	<typeProcedure>NULL</typeProcedure>\n	<categorie>NULL</categorie>\n	<lieuExecution>NULL</lieuExecution>\n	<cpv1>NULL</cpv1>\n	<cpv2>NULL</cpv2>\n	 <cpv3>NULL</cpv3>\n	 <cpv4>NULL</cpv4>\n	 <libelle_cpv1>NULL</libelle_cpv1>\n	<libelle_cpv2>NULL</libelle_cpv2>\n	 <libelle_cpv3>NULL</libelle_cpv3>\n	 <libelle_cpv4>NULL</libelle_cpv4>\n	 <motCle></motCle>\n<domainesActivite>NULL</domainesActivite><qualifications>NULL</qualifications><agrements>NULL</agrements></alerte>\n';
            $xml2 = '<?xml version="1.0" encoding="UTF-8"?>\n<alerte>\n	<organisme>NULL</organisme>\n	 <entiteAchat>NULL</entiteAchat>\n	 <typeAnnonce>3</typeAnnonce>\n	<typeProcedure>NULL</typeProcedure>\n	<categorie>NULL</categorie>\n	<lieuExecution>NULL</lieuExecution>\n	<cpv1>NULL</cpv1>\n	<cpv2>#30211300#48900000#72000000</cpv2>\n	 <cpv3>NULL</cpv3>\n	 <cpv4>NULL</cpv4>\n	 <libelle_cpv1>NULL</libelle_cpv1>\n	<libelle_cpv2>NULL</libelle_cpv2>\n	 <libelle_cpv3>NULL</libelle_cpv3>\n	 <libelle_cpv4>NULL</libelle_cpv4>\n	 <motCle></motCle>\n<domainesActivite>NULL</domainesActivite><qualifications>NULL</qualifications><agrements>NULL</agrements></alerte>\n';
            $idInscrit = $inscritAjoute->getId();
            $sql = ' INSERT INTO `Alerte` (`id`, `id_inscrit`, `denomination`, `periodicite`, `xmlCriteria`, `categorie`) VALUES ';
            $sql .= " (null, $idInscrit, 'Alerte complï¿½te', '1', '$xml1', NULL); ";
            $sql .= ' INSERT INTO `Alerte` (`id`, `id_inscrit`, `denomination`, `periodicite`, `xmlCriteria`, `categorie`) VALUES ';
            $sql .= " (null, $idInscrit, 'Alerte CPV Informatique', '1', '$xml2', NULL);";
            try {
                Atexo_Db::getLinkCommon()->query($sql);
                echo "\t                     ajout alertes pour l'inscrit 'ALERTE V3' (Alerte complete, Alerte CPV Informatique) avec succes !!!\r\n";
            } catch (Exception $ex) {
                Propel::log(' probleme execution requete : '.$sql, Propel::LOG_ERR);
                Propel::log($ex->getMessage(), Propel::LOG_ERR);
            }
        } else {
            echo "\t                     inscrit 'ALERTE V3' non insere !!!\r\n";
        }
    }

    public static function suppressionDoublonsPassationsDonneesLots()
    {
        //Cas des consultations non alloties

        $sql = ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6827 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6519 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6617 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5585 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5205 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5951 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6231 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5897 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5507 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5487 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6475 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6165 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6549 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6553 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5889 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6587 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6591 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6603 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6597 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6639 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6647 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6185 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6119 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5821 ; ';

        //Cas des consultations alloties
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5543 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5545 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6607 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2826 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2827 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2828 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2829 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2830 ; ';
        $sql .= " UPDATE `passation_marche_a_venir` SET `id` = '6814' WHERE `passation_marche_a_venir`.`id` =0  ; ";
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2670 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2671 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 2672 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6087 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 4331 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 4333 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 4335 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6213 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6215 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5275 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5277 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5279 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5281 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5283 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5213 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 5215 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6521 ; ';
        $sql .= ' DELETE FROM `passation_marche_a_venir` WHERE `passation_marche_a_venir`.`id` = 6523 ; ';

        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                try {
                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    echo "\t                    doublons supprimes avec succes. \n";
                    Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                } catch (Exception $ex) {
                    echo "\t KO \n".$sql;
                    Propel::log($uneOrganisme->getAcronyme().' - erreur execution requete : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function miseAJourTagNameChorusTypeProcedure()
    {
        $sql = " UPDATE `TypeProcedure` SET `tag_name_chorus` = '1' WHERE `TypeProcedure`.`id_type_procedure` =1 ;";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =2 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '2' WHERE `TypeProcedure`.`id_type_procedure` =3 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '9' WHERE `TypeProcedure`.`id_type_procedure` =4 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '9' WHERE `TypeProcedure`.`id_type_procedure` =5 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '7' WHERE `TypeProcedure`.`id_type_procedure` =8 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =9 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '7' WHERE `TypeProcedure`.`id_type_procedure` =10 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =11 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '6' WHERE `TypeProcedure`.`id_type_procedure` =12 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =13 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =14 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =15 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =16 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =17 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '4' WHERE `TypeProcedure`.`id_type_procedure` =18 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =19 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =20 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =22 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =23 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '5' WHERE `TypeProcedure`.`id_type_procedure` =24 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =30 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =31 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '10' WHERE `TypeProcedure`.`id_type_procedure` =32 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '8' WHERE `TypeProcedure`.`id_type_procedure` =33 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '9' WHERE `TypeProcedure`.`id_type_procedure` =6 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '9' WHERE `TypeProcedure`.`id_type_procedure` =7 ; ";
        $sql .= " UPDATE `TypeProcedure` SET `tag_name_chorus` = '9' WHERE `TypeProcedure`.`id_type_procedure` >= '34' AND `TypeProcedure`.`mapa` = '1'; ";

        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                echo "\n ORGANISME ".$uneOrganisme->getAcronyme().' : ';
                try {
                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    echo "\t                    mise a jour effectuee avec succes. \n";
                    Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                } catch (Exception $ex) {
                    echo "\t KO \n".$sql;
                    Propel::log($uneOrganisme->getAcronyme().' - erreur execution requete : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function migrationCodesCpvChorusEchange()
    {
        $separateur = "'#'";
        $like = "'#%'";
        $sql = " UPDATE `Chorus_echange` SET `cpv_2` = CONCAT( $separateur, CONCAT_WS( $separateur, `cpv_2` , `cpv_3` , `cpv_4` )) WHERE  `cpv_2` NOT LIKE '' AND `cpv_2` NOT LIKE $like; ";
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                $connexionorg = Propel::getConnection($uneOrganisme->getAcronyme().Atexo_Config::getParameter('CONST_READ_WRITE'));
                try {
                    Atexo_Db::getLinkOrganism($uneOrganisme->getAcronyme())->query($sql);
                    Atexo_Db::closeOrganism($uneOrganisme->getAcronyme());
                    echo "\t                     migration cpv : succes \r\n";
                } catch (Exception $ex) {
                    Propel::log($uneOrganisme->getAcronyme().' - migration code cpv : '.$sql, Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function miseAJourHabilitationAgentEnvoyerMessage()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $habilitationsAgent = CommonHabilitationAgentPeer::doSelect($c, $connexionCom);
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                try {
                    if (is_array($habilitationsAgent) && count($habilitationsAgent)) {
                        foreach ($habilitationsAgent as $habilitation) {
                            $habilitation->setEnvoyerMessage($habilitation->getModifierConsultationApresValidation());
                            $habilitation->setSuivreMessage($habilitation->getModifierConsultationApresValidation());
                            $habilitation->save($connexionCom);
                        }
                    }
                    echo "\t      ORGANISME : ".$uneOrganisme->getAcronyme()."               mise a jour effectuee avec succes \r\n";
                } catch (Exception $ex) {
                    $ex->getMessage();
                    Propel::log($uneOrganisme->getAcronyme().' - probleme mise e jour ', Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function miseAJourHabilitationProfilEnvoyerPublicite()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $habilitationsProfil = CommonHabilitationProfilPeer::doSelect($c, $connexionCom);
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                try {
                    if (is_array($habilitationsProfil) && count($habilitationsProfil)) {
                        foreach ($habilitationsProfil as $habilitation) {
                            $habilitation->setEnvoyerPublicite($habilitation->getPublierConsultation());
                            $habilitation->save($connexionCom);
                        }
                    }
                    echo "\t      ORGANISME : ".$uneOrganisme->getAcronyme()."               mise a jour effectuee avec succes \r\n";
                } catch (Exception $ex) {
                    $ex->getMessage();
                    Propel::log($uneOrganisme->getAcronyme().' - probleme mise a jour ', Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
        }
    }

    public static function migrateDataTable($table, $newTable, $pathFile)
    {
        if (!$table || !$newTable) {
            echo "\n =======> veuillez renseigner le nom de la table a migrer et la table destination: ./mpe script migrateDataTable tableSource tableDestination pathFile <======= \n\n";

            return;
        }
        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        $prefixe = Atexo_Config::getParameter('DB_PREFIX');
        $statement = Atexo_Db::getLinkCommon();
        if ($allOrganisme) {
            foreach ($allOrganisme as $uneOrganisme) {
                if ('q1s' != $uneOrganisme->getAcronyme()) {
                    continue;
                }
                try {
                    $acronyme = $uneOrganisme->getAcronyme();
                    $withOrg = false;
                    $nameFieldOrganisme = '';
                    $fromField = '';
                    $intoField = '';
                    $arrayField = [];
                    $nameFieldOrganisme = 'organisme';
                    $arrayField = (new Atexo_Db())->getNomColumns($newTable, $nameFieldOrganisme);
                    $implodField = implode(', ', $arrayField);
                    if ($nameFieldOrganisme) {
                        $intoField = $nameFieldOrganisme.', '.$implodField;
                        $fromField = "'".$acronyme."', ".$implodField;
                        $withOrg = true;
                    } else {
                        $intoField = $implodField;
                        $fromField = $implodField;
                    }
                    //Requete de migration des donnees
                    $sql = "INSERT INTO `$newTable` ($intoField) SELECT $fromField FROM `mpe_q1s`.`$table` ";

                    if ($pathFile) {
                        $handle = fopen($pathFile, 'a+');
                        $fcontent = fread($handle, filesize($pathFile));
                        $sql .= ' ON DUPLICATE KEY UPDATE '.$fcontent;
                    }

                    $sql .= ';';
                    $resultat = $statement->query($sql);
                    if (!$withOrg) {// Cas des tables qui ne contiennent pas le champ organisme
                        echo "\n\t      table `$table` : migration effectuee avec succes  \n\n";

                        return;
                    }
                    echo "\n\t      ORGANISME ".$uneOrganisme->getAcronyme().' : migration effectuee avec succes  ';
                } catch (Exception $ex) {
                    if (!$withOrg) {
                        echo "\n\t      table `$table` : erreur survenue lors de la migration  \n\n";
                    }
                    echo "\n\t      ORGANISME ".$uneOrganisme->getAcronyme().' : erreur survenue lors de la migration  ';
                    Propel::log("\n".date('d M Y h:i:s').'       [error: ORGANISME '.$uneOrganisme->getAcronyme().' : erreur survenue lors de la migration ]', Propel::LOG_ERR);
                    Propel::log($ex->getMessage(), Propel::LOG_ERR);
                }
            }
            echo "\n\n";
        }
    }

    public static function executeFile($pathFile)
    {
        if (!$pathFile) {
            echo "\n =======> veuillez renseigner le chemin du fichier que vous voulez executer: ./mpe script executeFile pathFile <======= \n\n";

            return;
        } else {
            if (file_exists($pathFile)) {
                preg_match_all("/((UPDATE|ALTER|CREATE|INSERT|REPLACE).*;)[ ]?[\n]{0,1}/Uxs", file_get_contents($pathFile), $sql);
                foreach ($sql[1] as $query) {
                    try {
                        if (!Atexo_Db::getLinkCommon()->query($query)) {
                            echo 'ERROR when executing query';
                        } else {
                            echo '.';
                        }
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                }
            } else {
                echo "\n =======> Le chemin du fichier donne est introuvable <======= \n\n";
            }
        }
    }

    public static function importAgent()
    {
        $sqlAgentValue = [];
        $sqlHabValue = [];
        $sqlAgentSeviceMetierValue = [];
        $start = 'debut insertion a '.date('H:i:s');
        $file = __DIR__.'/../../var/RepriseDonnees/modeles-reprise-donnees-ANFH-Agent.csv';
        $agent = '';
        $handle = fopen($file, 'r');
        $sqlAgent = 'INSERT INTO `Agent` (`id`, `login`, `email`, `nom`, `prenom`, `organisme`, `service_id`, `date_creation`, `date_modification`, `actif`, `password`) VALUES';
        $sqlHab = 'INSERT INTO `HabilitationAgent` (`id_agent`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_anonymat_en_ligne`, `ouvrir_offre_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`, `statistiques_QoS`, `ouvrir_anonymat_a_distance`, `gestion_compte_jal`, `gestion_centrale_pub`, `Gestion_Compte_Groupe_Moniteur`, `ouvrir_offre_technique_en_ligne`, `ouvrir_offre_technique_a_distance`, `activation_compte_entreprise`, `importer_enveloppe`, `suivi_seul_registre_depots_papier`, `suivi_seul_registre_retraits_papier`, `suivi_seul_registre_questions_papier`, `suivi_seul_registre_depots_electronique`, `suivi_seul_registre_retraits_electronique`, `suivi_seul_registre_questions_electronique`, `modifier_consultation_mapa_inferieur_montant_apres_validation`, `modifier_consultation_mapa_superieur_montant_apres_validation`, `modifier_consultation_procedures_formalisees_apres_validation`, `gerer_les_entreprises`, `portee_societes_exclues`, `portee_societes_exclues_tous_organismes`, `modifier_societes_exclues`, `supprimer_societes_exclues`, `resultat_analyse`, `gerer_adresses_service`, `gerer_mon_service`, `download_archives`, `creer_annonce_extrait_pv`, `creer_annonce_rapport_achevement`, `gestion_certificats_agent`, `creer_avis_programme_previsionnel`, `annuler_consultation`, `envoyer_publicite`, `liste_marches_notifies`, `suivre_message`, `envoyer_message`, `suivi_flux_chorus_transversal`) VALUES';
        $sqlAgentServiceMetier = 'INSERT INTO `Agent_Service_Metier` (`id_agent`, `id_service_metier`, `id_profil_service`, `date_creation`, `date_modification`) VALUES';
        $sSqlAgentServiceMetier = ", 1, 2, '".date('Y-m-d H:i:s')."', '".date('Y-m-d H:i:s')."');";
        $habAgent = ",'1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '1', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '1', '0')";
        $id = '47';
        while (($uneLigneCvs = fgetcsv($handle, 10000, ';')) !== false) {
            $nom = addslashes($uneLigneCvs[0]);
            $prenom = addslashes($uneLigneCvs[1]);
            $login = addslashes($uneLigneCvs[4]);
            $mdp = $uneLigneCvs[2];
            $idService = addslashes($uneLigneCvs[5]);
            $mail = addslashes($uneLigneCvs[3]);

            $sqlAgentValue[] = $sqlAgent.'('.$id.",'".$login."','".$mail."','".$nom."','".$prenom."','t5y','".$idService."','".date('Y-m-d H:i:s')."','".date('Y-m-d H:i:s')."','1','".sha1($mdp)."');";
            $sqlHabValue[] = $sqlHab.'('.$id.$habAgent.';';
            $sqlAgentSeviceMetierValue[] = $sqlAgentServiceMetier.'('.$id.$sSqlAgentServiceMetier.';';
            ++$id;
        }

        $sqlAgent = ''; //implode("\n", $sqlAgentValue);
        $sqlHab = implode("\n", $sqlHabValue);
        $sqlAgentServiceMetier = implode("\n", $sqlAgentSeviceMetierValue);

        $sql = $sqlAgent."\n\n\n-------------------------\n\n".$sqlHab."\n\n\n-------------------------\n\n".$sqlAgentServiceMetier;

        Atexo_Util::write_file(__DIR__.'/../../var/RepriseDonnees/agent.sql', $sql, 'a');
    }

    public static function importToConsultation($param)
    {
        $arrayParam = [];
        $parametre = substr($param, 1, strlen($param));
        if ($parametre) {
            $arrayParam = explode(',', $parametre);
        }

        $organisme = $arrayParam[0];
        $repertoireHtml = $arrayParam[1];
        $annee = $arrayParam[2];

        $fileLog = __DIR__.'/../../var/log/migrationLux.csv';
        if (!is_file($fileLog)) {
            Atexo_Util::write_file($fileLog, 'Date Debut '.date('d/m/Y  H:i:s')."\r\n\n\n");
            Atexo_Util::write_file($fileLog, "Reference utilisateur|Champ|Message|Erreur \r\n", 'a');
        }
        $nombreConsultationTraitee = 0;
        $nombreConsultationsInserees = 0;
        $nombreAvisInseres = 0;
        //$file = dirname(__FILE__) . "/../../var/RepriseDonnees/avis-$annee.csv";
        $myDirectory = opendir($repertoireHtml) or exit('Erreur');
        while ($file = @readdir($myDirectory)) {
            if (!is_dir($repertoireHtml.'/'.$file) && '.' != $file && '..' != $file) {
                $tableauExtensions = explode('.', $file);
                if ('csv' == $tableauExtensions[1]) {
                    $handle = fopen($repertoireHtml.'/'.$file, 'r');
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

                    $ligne = 0;
                    try {
                        while (($uneLigneCvs = fgetcsv($handle, 10000, '|')) !== false) {
                            if (0 != $ligne && 1 != $ligne) {
                                ++$nombreConsultationTraitee;
                                //Recuperation de la consultation par reference utilisateur
                                $consultation = (new Atexo_Consultation())->getConsultationByReferenceUser(trim($uneLigneCvs[0]), $organisme);
                                $typeAnnonce = explode('-', trim($uneLigneCvs[11]));
                                $typeAvis = trim($uneLigneCvs[11]);

                                $dateFin = Atexo_Util::frnDate2iso(trim($uneLigneCvs[10])).' 00:00:00';
                                $dateMiseEnligneCalculeNationale = Atexo_Util::frnDate2iso(trim($uneLigneCvs[8])).' 00:00:00';
                                $dateMiseEnligneCalculeEuropeenne = Atexo_Util::frnDate2iso(trim($uneLigneCvs[9])).' 00:00:00';
                                $nouvelleCons = false;
                                if (!($consultation instanceof CommonConsultation)) {
                                    $nouvelleCons = true;
                                    $consultation = new CommonConsultation();
                                }

                                $idTypeProcedure = self::correspondanceTypesProcedureRepriseDonneesLux(trim($uneLigneCvs[12]));
                                $idTypeAvis = self::correspondanceTypesAvisRepriseDonneesLux($typeAvis);
                                if ('NOT_FOUND' == $idTypeProcedure && 'NOT_FOUND' == $idTypeAvis) {
                                    Atexo_Util::write_file($fileLog, trim($uneLigneCvs[0]).'|'.trim($uneLigneCvs[12])."|correspondance type procedure| non trouve \r\n", 'a');
                                    Atexo_Util::write_file($fileLog, trim($uneLigneCvs[0]).'|'.$typeAvis."|correspondance type avis| non trouve \r\n", 'a');
                                    continue;
                                }
                                if ('NOT_FOUND' == $idTypeProcedure) {
                                    Atexo_Util::write_file($fileLog, trim($uneLigneCvs[0]).'|'.trim($uneLigneCvs[12])."|correspondance type procedure| non trouve \r\n", 'a');
                                    continue;
                                } else {
                                    $consultation->setIdTypeProcedure($idTypeProcedure);
                                    $consultation->setIdTypeProcedureOrg($idTypeProcedure);
                                }
                                if ('NOT_FOUND' == $idTypeAvis) {
                                    Atexo_Util::write_file($fileLog, trim($uneLigneCvs[0]).'|'.$typeAvis."|correspondance type avis| non trouve \r\n", 'a');
                                    continue;
                                } else {
                                    $consultation->setIdTypeAvis($idTypeAvis);
                                }

                                $consultation->setOrganisme($organisme);
                                $consultation->setReferenceUtilisateur(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(trim($uneLigneCvs[0])));
                                $consultation->setObjet(Atexo_Util::replaceCharactersMsWordWithRegularCharacters(trim($uneLigneCvs[1])));
                                if ('Travaux' == trim($uneLigneCvs[3])) {
                                    $consultation->setCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
                                } elseif ('Fournitures' == trim($uneLigneCvs[3])) {
                                    $consultation->setCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
                                } elseif ('Services' == trim($uneLigneCvs[3])) {
                                    $consultation->setCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
                                }
                                $consultation->setDatefin($dateFin);

                                $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));
                                if (preg_match('/^L.*$/', trim($uneLigneCvs[11]))) {
                                    $consultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalculeNationale);
                                    $consultation->setDatevalidation($dateMiseEnligneCalculeNationale);
                                } elseif (preg_match('/^E.*$/', trim($uneLigneCvs[11]))) {
                                    $consultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalculeEuropeenne);
                                    $consultation->setDatevalidation($dateMiseEnligneCalculeEuropeenne);
                                }
                                $consultation->setIdTypeValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2'));
                                $consultation->setPoursuivreAffichage(Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE_VALEUR2'));
                                $consultation->setTypeMiseEnLigne('2');
                                $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                                $consultation->setReponseElectronique('0');
                                $consultation->setServiceId('0');
                                $consultation->setServiceAssocieId('0');
                                $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE'));
                                $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                                $consultation->setAutoriserReponseElectronique('0');
                                if ($consultation->save($connexionCom)) {
                                    ++$nombreConsultationsInserees;
                                }
                                if ($nouvelleCons) {
                                    echo "\n consultation refUtilisateur = ".$consultation->getReferenceUtilisateur().' - RefTechnique = '.$consultation->getId()." : inseree avec succes \n";
                                } else {
                                    echo "\n consultation refUtilisateur = ".$consultation->getReferenceUtilisateur().' - RefTechnique = '.$consultation->getId()." : mis a jour avec succes \n";
                                }

                                $returnPdfGeneration = self::genererPdfMigrationLux($consultation, $repertoireHtml.'/');
                                if (is_array($returnPdfGeneration) && !is_object($returnPdfGeneration[2])) {
                                    self::migrationAvisLux($consultation, $returnPdfGeneration[0], $returnPdfGeneration[1], $nombreAvisInseres);
                                } elseif (is_array($returnPdfGeneration) && is_object($returnPdfGeneration[2])) {
                                    self::migrationAvisLux($consultation, $returnPdfGeneration[0], $returnPdfGeneration[1], $nombreAvisInseres, $returnPdfGeneration[2]);
                                } else {
                                    $fileLog = __DIR__.'/../../var/log/migrationLux.csv';
                                    Atexo_Util::write_file($fileLog, $consultation->getReferenceUtilisateur().'||GENERATION_PDF : ERREUR_EXCEPTION| Probleme execution commande: '.$returnPdfGeneration." \r\n", 'a');
                                }
                            }
                            ++$ligne;
                        }
                    } catch (Exception $e) {
                        $fileLog = __DIR__.'/../../var/log/migrationLux.csv';
                        Atexo_Util::write_file($fileLog, $consultation->getReferenceUtilisateur().'||SAUVEGARDE_CONSULTATION : ERREUR_EXCEPTION| '.$e->getMessage()." \r\n", 'a');
                    }
                }
            }
        }
        $rapportMigration = "\n\n\n Nombre de consultations traitees = ".$nombreConsultationTraitee." \n";
        $rapportMigration .= ' Nombre de consultations inserees = '.$nombreConsultationsInserees." \n";
        $rapportMigration .= " Nombre d'avis inseres = ".$nombreAvisInseres." \n\n\n";
        echo $rapportMigration;
        Atexo_Util::write_file($fileLog, $rapportMigration, 'a');
    }

    public static function genererPdfMigrationLux($consultation, $repertoireHtml)
    {
        $fileLog = null;
        $fileHtml = null;
        try {
            $pdfLog = __DIR__.'/../../var/log/pdf.log';
            if (!is_file($pdfLog)) {
                system('touch '.escapeshellarg($pdfLog));
            }

            $fileName = $consultation->getReferenceUtilisateur();
            $fileHtml = trim($fileName).'.html';

            if (!is_file($repertoireHtml.$fileHtml)) {
                Atexo_Util::write_file($fileLog, $consultation->getReferenceUtilisateur().'||generation pdf : ERREUR_FICHIER_N_EXISTE_PAS| le fichier html '.$fileHtml." n'existe pas \r\n", 'a');
            }

            $cmd = " cd $repertoireHtml ;";
            $cmd .= ' /usr/bin/htmldoc -f '.trim($fileName).'.pdf --webpage -t pdf14 '.trim($fileName).'.html ';

            system("$cmd 2>&1 > ".$pdfLog, $sys_answer);
            if ('0' != $sys_answer) {
                return $cmd;
            }
            echo "\t le fichier ".$fileName.".pdf generee avec succes \n";
            $searchAvis = (new Atexo_Publicite_Avis())->retreiveListAvis($consultation->getId(), $consultation->getOrganisme());
            if ($searchAvis[0] instanceof CommonAVIS) {
                return [0 => trim($fileName).'.pdf', 1 => $repertoireHtml, 2 => $searchAvis[0]];
            }

            return [0 => trim($fileName).'.pdf', 1 => $repertoireHtml, 2 => null];
        } catch (Exception $e) {
            $fileLog = __DIR__.'/../../var/log/migrationLux.csv';
            Atexo_Util::write_file($fileLog, $consultation->getReferenceUtilisateur().'||generation pdf : ERREUR_EXCEPTION| fichier='.$repertoireHtml.$fileHtml.' ### '.$e->getMessage()." \r\n", 'a');
        }
    }

    public static function migrationAvisLux($consultation, $fileName, $filePath, &$nombreAvisInseres, $objetCommonAvis = null)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (is_object($objetCommonAvis) && $objetCommonAvis instanceof CommonAVIS) {
                $newAvis = $objetCommonAvis->copy();
                $newAvis->setId($objetCommonAvis->getId());
                $newAvis->setNew(false);
            } else {
                $newAvis = new CommonAVIS();
            }
            $newAvis->setOrganisme($consultation->getOrganisme());
            $newAvis->setConsultationId($consultation->getId());
            $newAvis->setDateCreation(date('Y-m-d H:i'));
            $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));

            $filePdf = $filePath.$fileName;
            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            $arrayTimeStampAvis = $atexoCrypto->timeStampFile($filePdf);
            if (is_array($arrayTimeStampAvis)) {
                $newAvis->setHorodatage($arrayTimeStampAvis['horodatage']);
                $newAvis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
            }

            $avisIdBlob = $atexoBlob->insert_blob($fileName, $filePdf, $consultation->getOrganisme());
            //$newAvis->setAgentId("");
            $newAvis->setAvis($avisIdBlob);
            $newAvis->setNomFichier($fileName);
            //@unlink($file);
            //Module PubliciteOpoce active
            $newAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            $newAvis->setDatePub(date('Y-m-d'));
            $newAvis->setTypeDocGenere(Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS'));
            $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES'));
            $newAvis->setLangue('fr');
            if ($newAvis->save($connexionCom)) {
                ++$nombreAvisInseres;
            }
            if (is_object($objetCommonAvis) && $objetCommonAvis instanceof CommonAVIS) {
                echo "\t avis n = ".$newAvis->getId()." mise a jour avec succes \n";
            } else {
                echo "\t avis n = ".$newAvis->getId()." inseree avec succes \n";
            }
        } catch (Exception $e) {
            $fileLog = __DIR__.'/../../var/log/migrationLux.csv';
            Atexo_Util::write_file($fileLog, $consultation->getReferenceUtilisateur().'||generation pdf : ERREUR_GENERATION_AVIS| '.$e->getMessage()." \r\n", 'a');
        }
    }

    public static function correspondanceTypesProcedureRepriseDonneesLux($typeProcedure)
    {
        $idTypeProcedure = match ($typeProcedure) {
            'E-1 Procï¿½dure ouverte (soumission publique)' => '7',
            'E-2 Procï¿½dure restreinte' => '8',
            'L-1 Procï¿½dure ouverte (soumission publique)' => '1',
            'L-2 Procï¿½dure restreinte' => '2',
            'E-3 Procï¿½dure nï¿½gociï¿½e' => '30',
            "OF-1 Demande d'offres" => '50',
            'E-4 Concours' => '40',
            'E-7 Transparence ex ante volontaire' => '38',
            'E-6 Attribution marchï¿½ nï¿½gociï¿½ sans publication' => '39',
            default => 'NOT_FOUND',
        };

        return $idTypeProcedure;
    }

    public static function correspondanceTypesAvisRepriseDonneesLux($typeAvis)
    {
        $idTypeAvis = match ($typeAvis) {
            "EU02a - Avis d'adjudication" => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            'EU02b - Appel de candidatures (restreinte)' => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            'EU02c - Appel de candidatures (nï¿½gociï¿½)' => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            "EU03a - Avis d'attribution de marchï¿½" => Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'),
            "EU03b - Avis d'attribution de marchï¿½ (nï¿½gociï¿½)" => Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'),
            'EU03c - Attribution marchï¿½ nï¿½gociï¿½ sans publicat.' => Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'),
            'EU04 - Avis de concours' => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            'EU05 - Rï¿½sultats de concours' => Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'),
            'EU15 - Avis de transparence ex-ante volontaire' => Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'),
            "LU01 - Avis d'adjudication" => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            'LU02 - Appel de candidatures' => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            "LU03 - Avis d'attribution de marchï¿½" => Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'),
            "OF01 - Demande d'offres" => Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'),
            default => 'NOT_FOUND',
        };

        return $idTypeAvis;
    }

    public static function MajAcheteurPublic()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $critere = new Criteria();
        $critere->add(CommonAcheteurPublicPeer::TYPE_POUVOIR_ACTIVITE, '', Criteria::NOT_LIKE);
        $critere->addAnd(CommonAcheteurPublicPeer::TYPE_POUVOIR_ACTIVITE, null, Criteria::NOT_EQUAL);
        $result = CommonAcheteurPublicPeer::doSelect($critere, $connexion);
        foreach ($result as $oneResult) {
            $arrayTypePouvoiractiviteNew = [];
            $typepouvoirActivite = $oneResult->getTypePouvoirActivite();
            $arrayTypePouvoiractivite = explode('#', $typepouvoirActivite);
            foreach ($arrayTypePouvoiractivite as $oneElement) {
                $oneElement = match ($oneElement) {
                    '7_142', '7_96', '7_50' => '7_50',
                    '7_144', '7_98', '7_52' => '7_52',
                    '7_146', '7_100', '7_54' => '7_54',
                    '7_148', '7_102', '7_56' => '7_56',
                    '7_150', '7_104', '7_58' => '7_58',
                    '7_152', '7_106', '7_60' => '7_60',
                    '8_108', '8_154', '8_62' => '8_62',
                    '8_110', '8_156', '8_64' => '8_64',
                    '8_112', '8_158', '8_66' => '8_66',
                    '8_114', '8_160', '8_68' => '8_68',
                    '8_116', '8_162', '8_70' => '8_70',
                    '8_118', '8_164', '8_72' => '8_72',
                    '8_120', '8_166', '8_74' => '8_74',
                    '8_122', '8_168', '8_76' => '8_76',
                    '8_124', '8_170', '8_78' => '8_78',
                    '8_126', '8_172', '8_80' => '8_80',
                    default => false,
                };
                if ($oneElement) {
                    $arrayTypePouvoiractiviteNew[] = $oneElement;
                }
            }
            $res = [];
            foreach ($arrayTypePouvoiractiviteNew as $one) {
                $exist = false;
                foreach ($res as $un) {
                    if ($one == $un) {
                        $exist = true;
                    }
                }
                if (!$exist) {
                    $res[] = $one;
                }
            }
            $oneResult->setTypePouvoirActivite(implode('#', $arrayTypePouvoiractiviteNew).'#');
            $oneResult->save($connexion);
            //suppression des doublants
            $sql = 'Delete FROM `ValeurReferentiel` WHERE `id_referentiel` =7 AND `id` NOT IN ( 50, 52, 54, 56, 58, 60 )';
            $req = 'Delete FROM `ValeurReferentiel` WHERE `id_referentiel` =8 AND `id` NOT IN ( 62,64,66,68,70,72,74,76,78,80)';
            Atexo_Db::getLinkCommon(true)->query($sql);
            Atexo_Db::getLinkCommon(true)->query($req);
        }
    }

    public static function MajVerificationCertificat()
    {
        //Insertion table Inscrit
        $sql = " UPDATE `fichierEnveloppe` SET `verification_certificat` = '0-0-0' WHERE `verification_certificat` LIKE '1-0-0';";
        $sql .= " UPDATE `fichierEnveloppe` SET `verification_certificat` = '0-0-1' WHERE `verification_certificat` LIKE '1-0-1';";
        try {
            Atexo_Db::getLinkCommon()->query($sql);
            echo "\t                     mise a jour fichierEnveloppe realisee avec succes !!!\r\n";
        } catch (Exception $ex) {
            Propel::log(' probleme execution requete : '.$sql, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }

    /* **********************************************************************
       * permet de calculer le denom org adapte des consultations ayant cette
       * valeur Null et qui sont mise en ligne et inserer au meme temps dans
       * la table Referentiel Org denomination
       * */
    public static function udapteDenomAdapteConsultation()
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $requete = <<<QUERY
Select
	consultation.id,
	consultation.organisme
FROM
	consultation
LEFT OUTER JOIN Annonce ON consultation.id = Annonce.consultation_id AND consultation.organisme = Annonce.organisme
LEFT JOIN CategorieLot ON CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme
LEFT OUTER JOIN Organisme on Organisme.acronyme =consultation.organisme
WHERE
	consultation.type_acces='1' AND
	consultation.id_type_avis='3' AND
	consultation.doublon='0' AND
	Organisme.active = '1' AND
	DATE_FIN_UNIX > UNIX_TIMESTAMP() AND
	consultation_annulee='0' AND
	(
		(
			date_mise_en_ligne_calcule!='' AND
			date_mise_en_ligne_calcule IS NOT NULL AND
			date_mise_en_ligne_calcule not like '0000-00-00%' AND
			date_mise_en_ligne_calcule<=NOW()
		)
	)
QUERY;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
            $results = $statement->execute();
            $arrayConsultations = [];
            if ($results) {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $arrayConsultations[$row['id']] = $row['organisme'];
                }
            }
            foreach ($arrayConsultations as $key => $org) {
                if ($consultation = (new Atexo_Consultation())->retrieveConsultation($key, $org)) {
                    $denomination_adapte = strtoupper(Atexo_Util::formatterChaine($consultation->getOrgDenomination()));
                    $consultation->setDenominationAdapte($denomination_adapte);
                    $consultation->save($connexionCom);
                    (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomonation($consultation);
                    unset($consultation);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /* ****************************************************************************
    * permet la migration des tranches budgetaires de valeur referentiels Org vers
    * la nouvelle table Tranche article 133 ( affecte les annees avant 2013
    * ***************************************************************************/

    public static function migrationTrancheBudgetaire()
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $requeteOrganisme = 'SELECT id,acronyme FROM Organisme';
            $statement2 = Atexo_Db::getLinkCommon(true)->prepare($requeteOrganisme);
            $resultsOrg = $statement2->execute();
            $arrayOrg = [];
            if ($resultsOrg) {
                while ($row = $statement2->fetch(PDO::FETCH_ASSOC)) {
                    $arrayOrg[$row['id']] = $row['acronyme'];
                }
            }

            $arrayAnnees = ['0' => '2008', '1' => '2009', '2' => '2010', '3' => '2011', '4' => '2012', '5' => '2013'];
            foreach ($arrayOrg as $key => $value) {
                // recuperation de la liste des ref tranche budgetaire d'un organisme
                $requete = "SELECT * FROM ValeurReferentielOrg  WHERE organisme = '".$value."'  AND id_referentiel = ".Atexo_Config::getParameter('REFERENTIEL_TRANCHE_BUDGETAIRE');
                $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
                $results = $statement->execute();
                $arrayReferentiels = [];
                if ($results) {
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        if ($row['libelle_valeur_referentiel_fr']) {
                            $arrayReferentiels[$row['libelle_valeur_referentiel_fr']] = $row['libelle_valeur_referentiel'];
                        } else {
                            $arrayReferentiels[$row['libelle_valeur_referentiel']] = $row['libelle_valeur_referentiel'];
                        }
                    }
                    foreach ($arrayAnnees as $cle => $annee) {
                        //insertion des tranches 0 a 4000
                        $newArticle = new CommonTrancheArticle133();
                        $newArticle->setAcronymeOrg($value);
                        $newArticle->setMillesime($annee);
                        $newArticle->setLibelleTrancheBudgetaire('0 ï¿½ 3 999,99 HT');
                        $newArticle->setBorneInf('0');
                        $newArticle->setBorneSup('3 999,99');

                        $newArticle->save($connexionCom);
                        unset($newArticle);
                        echo "Tranche dubgetaire inseree avec succes \n \r";
                        foreach ($arrayReferentiels as $ref => $valeurRef) {
                            $newArticle = new CommonTrancheArticle133();
                            $newArticle->setAcronymeOrg($value);
                            $newArticle->setMillesime($annee);
                            $newArticle->setLibelleTrancheBudgetaire($ref);
                            $arrayBorne = self::getBornes($valeurRef);
                            if ($arrayBorne) {
                                $newArticle->setBorneInf($arrayBorne[0]);
                                $newArticle->setBorneSup($arrayBorne[1]);
                            }
                            $newArticle->save($connexionCom);
                            unset($newArticle);
                            echo "Tranche dubgetaire inseree avec succes \n \r";
                        }
                    }
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /*********************************************************************
     * retour un tableau de borns a partir d'une chaine separe par un tiret
     **********************************************************************/
    public static function getBornes($chaine)
    {
        $result = [];
        if ($chaine) {
            if (strstr($chaine, 'ï¿½')) {
                $chaine = str_replace('ï¿½', '-', $chaine);
                $chaine = str_replace('HT', '', $chaine);
            }
            $chaines = explode('-', $chaine);
            $result[0] = str_replace(' ', '', str_replace('.', ',', $chaines[0]));
            $borneSup = str_replace(' ', '', $chaines[1]);
            if ('...' != $borneSup) {
                $result[1] = str_replace('.', ',', $chaines[1]);
            } else {
                $result[1] = null;
            }

            return $result;
        }

        return false;
    }

    /*********************************************************************
    * Update id tranche budgetaire de decision enveloppe par le nouveau
    * refentiel Tranche Article 133
    **********************************************************************/
    public static function updateTrancheBudgetaireDecision()
    {
        try {
            echo "*************************** Debut de script de mise a jour des decision enveloppe *************************** \n \r";
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $parametreRep = Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE');
            if (!is_dir($parametreRep)) {
                echo 'le repertoire '.$parametreRep." est bien cree avec succes \n \r";
                system('mkdir '.escapeshellarg($parametreRep));
            }
            $fichierLog = $parametreRep.'FichierLogUpdateDecision.csv';
            $fichierLogObjet = $parametreRep.'FichierLogUpdateDecisionObjet.csv';
            if (!is_file($fichierLog)) {
                Atexo_Util::writeFile($fichierLog, "#RefDecision#organisme#reference consultation#Libelle Tranche#Id Tranche Old#Id tranche Update#Date Notification#Date decision#Etat Update Tranche # \n", 'a+');
                Atexo_Util::writeFile($fichierLogObjet, "|RefMarche|organisme|Objet|Objet modifie| \n", 'a+');
            }
            $statment = Atexo_Db::getLinkCommon();
            $sql = 'SELECT * FROM decisionEnveloppe';
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $results = $statement->execute();
            $j = 0;
            if ($results) {
                while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $objetDecision = (new Atexo_Consultation_Decision())->retrieveDecisionById($data['id_decision_enveloppe'], $data['organisme']);
                    if ($objetDecision) {
                        $idNewRef = '';
                        $update = 'false';
                        $libelle = '';
                        $oldObjet = '';
                        $fromDateDecison = '';
                        // Mise a jour du champ objet remplace les retours a la ligne par des tirets
                        $oldObjet = $objetDecision->getObjetmarche();
                        $objetAdapte = Atexo_Util::replaceRetourALaLigne($objetDecision->getObjetmarche(), ' - ');
                        $objetDecision->setObjetMarche($objetAdapte);
                        // Traitement de l'id Tranche
                        $idTrancheOld = $objetDecision->getTrancheBudgetaire();
                        if ($idTrancheOld) {
                            if ($libelle = self::getLibelleTrancheBudgetaire($idTrancheOld, $objetDecision->getOrganisme())) {
                                $dateDecision = false;
                                if (!$objetDecision->getDateNotification()) {
                                    $consultationLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($objetDecision->getConsultationId(), $objetDecision->getLot(), $objetDecision->getOrganisme());
                                    if ($consultationLot) {
                                        $dateDecision = $consultationLot->getDateDecision();
                                        $fromDateDecison = $dateDecision;
                                    }
                                }
                                if ($idNewRef = self::getNewIdTrancheBudgetaire($objetDecision->getOrganisme(), $libelle, $objetDecision->getDateNotification(), $dateDecision)) {
                                    $objetDecision->setTrancheBudgetaire($idNewRef);
                                    $update = 'true';
                                }
                            }
                        }
                        $objetDecision->save($connexionCom);
                        Atexo_Util::writeFile(Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateDecision.csv', '#'.$objetDecision->getIdDecisionEnveloppe().'#'.$objetDecision->getOrganisme().'#'.$objetDecision->getConsultationId().'#'.$libelle.'#'.$idTrancheOld.'#'.$idNewRef.'#'.$objetDecision->getDateNotification().'#'.$fromDateDecison.'#'.$update."#\n", 'a+');
                        Atexo_Util::writeFile(Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateDecisionObjet.csv', '|'.$objetDecision->getIdDecisionEnveloppe().'|'.$objetDecision->getOrganisme().'|'.$oldObjet.'|'.$objetDecision->getObjetMarche()." \n", 'a+');

                        echo "la mise a jour de l'objet Decision enveloppe Numero :".$objetDecision->getIdDecisionEnveloppe().' - '.$objetDecision->getOrganisme().' est a '.$update." \n \r";
                        unset($objetDecision);
                    }
                }
                echo "\r\n ===> path file : ".Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateDecision.csv'."\r\n";
                echo $j;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function getNewIdTrancheBudgetaire($org, $libelle, $annee, $anneeDecision = false)
    {
        $annee = Atexo_Util::getAnneeFromDate($annee);
        if ($org && $libelle) {
            if (!$annee && $anneeDecision) {
                $annee = Atexo_Util::getAnneeFromDate($anneeDecision);
            }
            if ($objetTranche = (new Atexo_TrancheBudgetaire())->retrieveTrancheBudgetaireByOrgLibelleAnnee($org, $libelle, $annee)) {
                return $objetTranche->getId();
            }
        }

        return false;
    }

    public static function getLibelleTrancheBudgetaire($idTranche, $org)
    {
        if ($idTranche && $org) {
            $arrayRef = (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_TRANCHE_BUDGETAIRE'), $org, true);
            foreach ($arrayRef as $key => $libelle) {
                if ($key == $idTranche) {
                    return $libelle;
                }
            }
        }

        return false;
    }

    /*********************************************************************
    * Update id tranche budgetaire des marches par le nouveau
    * refentiel Tranche Article 133
    **********************************************************************/
    public static function updateTrancheBudgetaireMarche()
    {
        try {
            echo "*************************** Debut de script de mise a jour des Marches  *************************** \n \r";
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $parametreRep = Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE');
            if (!is_dir($parametreRep)) {
                echo 'le repertoire '.$parametreRep." est bien cree avec succes \n \r";
                system('mkdir '.escapeshellarg($parametreRep));
            }
            $fichierLog = $parametreRep.'FichierLogUpdateMarche.csv';
            $fichierLogObjet = $parametreRep.'FichierLogUpdateMarcheObjet.csv';
            if (!is_file($fichierLog)) {
                Atexo_Util::writeFile($fichierLog, "#RefMarche#organisme#Libelle Tranche#Id Tranche Old#Id tranche Update#Annee Marche#Etat Update# \n", 'a+');
                Atexo_Util::writeFile($fichierLogObjet, "|RefMarche|organisme|Objet|Objet modifie| \n", 'a+');
            }
            $statment = Atexo_Db::getLinkCommon();
            $sql = 'SELECT * FROM Marche ';
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $results = $statement->execute();
            if ($results) {
                while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $id = $data['id'];
                    $org = $data['organisme'];
                    $objetMarche = CommonMarchePeer::retrieveByPK($id, $org, $connexionCom);
                    if ($objetMarche) {
                        $idNewRef = '';
                        $update = 'false';
                        $libelle = '';
                        // Mise a jour du champ objet remplace les retours a la ligne par des tirets
                        $oldObjet = $objetMarche->getObjetmarche();
                        $objetAdapte = Atexo_Util::replaceRetourALaLigne($objetMarche->getObjetmarche(), ' - ');
                        $objetMarche->setObjetmarche($objetAdapte);
                        $idTrancheOld = $objetMarche->getIdmarchetranchebudgetaire();
                        if ($idTrancheOld) {
                            if ($libelle = self::getLibelleTrancheBudgetaire($idTrancheOld, $objetMarche->getOrganisme())) {
                                if ($idNewRef = self::getNewIdTrancheBudgetaire($objetMarche->getOrganisme(), $libelle, $objetMarche->getDateNotification())) {
                                    $objetMarche->setIdmarchetranchebudgetaire($idNewRef);
                                    $update = 'true';
                                }
                            }
                        }
                        $objetMarche->save($connexionCom);
                        Atexo_Util::writeFile(Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateMarche.csv', '#'.$objetMarche->getId().'#'.$objetMarche->getOrganisme().'#'.$libelle.'#'.$idTrancheOld.'#'.$idNewRef.'#'.$objetMarche->getDateNotification().'#'.$update."#\n", 'a+');
                        Atexo_Util::writeFile(Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateMarcheObjet.csv', '|'.$objetMarche->getId().'|'.$objetMarche->getOrganisme().'|'.$oldObjet.'|'.$objetMarche->getObjetmarche()." \n", 'a+');
                        echo "la mise a jour de l'objet Marche Numero :".$objetMarche->getId().' est a '.$update." \n \r";
                        unset($objetMarche);
                    }
                }
            }
            echo "\r\n ===> path file : ".Atexo_Config::getParameter('LOGS_UPDATE_TRANCHE_BUDGETAIRE').'FichierLogUpdateMarche.csv'."\r\n";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /***
         * Remplace les retour a la ligne par des tirets
         */
    public function remplacerRetourAlaLigne($objet)
    {
        $objet = str_replace("\n", ' - ', $objet);
        $objet = str_replace("\r\n", ' - ', $objet);
        $objet = str_replace("\r", ' - ', $objet);

        return $objet;
    }

    /**
     * Permet de migrer les dates envois premier message et date premier accuse de reception pour AOF.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private static function migrerDonneesAofDateEnvoiMessageDateAr()
    {
        try {
            $logger = Atexo_LoggerManager::getLogger('cli');
            self::logMessage($logger, "-> DEBUT Migration AOF date envoi 1er message et date 1er AR \n");
            $complementFormulaires = CommonTComplementFormulaireQuery::create()->find();
            if ($complementFormulaires instanceof PropelObjectCollection && !empty($complementFormulaires)) {
                self::logMessage($logger, "\t -> Nombre de demandes de complements a traiter: ".count($complementFormulaires)." \n");
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $organisme = '';
                foreach ($complementFormulaires as $complementFormulaire) {
                    if ($complementFormulaire instanceof CommonTComplementFormulaire) {
                        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
                        if (empty($organisme)) {
                            $organisme = (new Atexo_FormulaireSub_ComplementFormulaire())->getOrganismeByIdComplement($complementFormulaire->getIdComplementFormulaire());
                            self::logMessage($logger, "\t -> Recuperation de l'organisme. Org = $organisme \n");
                        }
                        $dateAr = '';
                        $dateEnvoi = '';
                        $relationEchange = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($complementFormulaire->getIdComplementFormulaire(), $typeRelation, $organisme);
                        if ($relationEchange[0] instanceof CommonRelationEchange) {
                            $dateEnvoi = $relationEchange[0]->getDateEnvoi();
                            $destinataireEchange = (new Atexo_Message())->getEchangeDestinataireByIdEchange($relationEchange[0]->getIdEchange(), $organisme);
                            if ($destinataireEchange[0] instanceof CommonEchangeDestinataire) {
                                $dateAr = $destinataireEchange[0]->getDateAr();
                            }
                        }
                        if ((!$complementFormulaire->getDateEnvoi1erMail() && !empty($dateEnvoi)) || (!$complementFormulaire->getDate1erAr() && !empty($dateAr))) {
                            $message = "\t -> Sauvegarde idComplement = ".$complementFormulaire->getIdComplementFormulaire().' ( ';
                            if (!$complementFormulaire->getDateEnvoi1erMail() && !empty($dateEnvoi)) {
                                $complementFormulaire->setDateEnvoi1erMail($dateEnvoi);
                                $message .= "modification date envoi 1er message = $dateEnvoi";
                            }
                            if (!$complementFormulaire->getDate1erAr() && !empty($dateAr)) {
                                $complementFormulaire->setDate1erAr($dateAr);
                                $message .= " modification date 1er AR = $dateAr";
                            }
                            $message .= " ) \n";
                            $complementFormulaire->save($connexion);
                            self::logMessage($logger, $message);
                        }
                    }
                }
            }
            self::logMessage($logger, "-> FIN Migration AOF date envoi 1er message et date 1er AR \n");
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('cli');
            $logger->error('Migration AOF date envoi 1er message et date 1er AR : '.print_r($e, true));
        }
    }

    /**
     * Permet de logger et afficher sur la console le message.
     *
     * @param Logger : logger
     * @param $message : message a logger et a afficher
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private static function logMessage($logger, $message)
    {
        echo $message;
        $logger->info($message);
    }

    /**
     * redressement de la table  FormXmlDestinataireOpoce le champ no_doc_ext.
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6
     *
     * @copyright Atexo 2015
     */
    public static function updateNoDocExtSIMAP()
    {
        try {
            $sql = "SELECT  FX.xml, FX.id , FX.`organisme` FROM `FormXmlDestinataireOpoce` FX WHERE FX.no_doc_ext = '' AND FX.xml !='';";
            $statement = Atexo_Db::getLinkCommon(true)->query($sql);

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $xml = simplexml_load_string(base64_decode($row['xml']));
                $arrayInfos = json_decode(json_encode((array) $xml, JSON_THROW_ON_ERROR), true, 512, JSON_THROW_ON_ERROR);
                $noDocExt = $arrayInfos['SENDER']['NO_DOC_EXT'];
                $sqlUpdate = "Update FormXmlDestinataireOpoce set no_doc_ext='".$noDocExt."' where id='".$row['id']."' AND organisme like '".$row['organisme']."';";
                try {
                    Atexo_Db::getLinkCommon(true)->query($sqlUpdate);
                    Atexo_Util::write_file(__DIR__.'/../../var/log/LogScriptSimap.log', 'Requete : '.$sqlUpdate." | OK \n", 'a+');
                } catch (Exception $e) {
                    Atexo_Util::write_file(__DIR__.'/../../var/log/LogScriptSimap.log', 'Requete : '.$sqlUpdate.' | KO | '.$e->getMessage()."\n", 'a+');
                }
            }
        } catch (Exception $e) {
            Atexo_Util::write_file(__DIR__.'/../../var/log/LogScriptSimap.log', 'Requete : '.$sql.' | KO | '.$e->getMessage()."\n", 'a+');
        }
    }

    public static function generateScriptUpdateTypeProcedureOrganismePLACE()
    {
        echo "\n\n---------------------------------------------> DEBUT GENERATION SCRIPT <------------------------------------------------";
        $pathFileCsv = '/tmp/script_sql_maj_type_procedure_organisme_PLACE.csv';
        if (is_file($pathFileCsv)) {
            $pathFileLog = '/tmp/script_sql_maj_type_procedure_organisme_PLACE.sql';
            $fp = fopen($pathFileLog, 'a');

            $pathFileCsvHandle = fopen($pathFileCsv, 'r+');
            while (($data = fgetcsv($pathFileCsvHandle, 1000, ';')) !== false) {
                $id = $data[0];
                $org = $data[1];
                $oa = $data[3];
                $mn = $data[4];
                $dc = $data[5];
                $autre = $data[6];
                $sad = $data[7];
                $accordCadre = $data[8];
                $pn = $data[9];

                $script = "UPDATE `Type_Procedure_Organisme` SET `ao`='$oa', `mn`='$mn', `dc`='$dc', `autre`='$autre', `sad`='$sad', `accord_cadre`='$accordCadre', `pn`='$pn' WHERE  `id_type_procedure`=$id AND `organisme`='$org';";
                echo "\n-> SQL : $script";
                fwrite($fp, "$script \n");
            }
            fclose($pathFileCsvHandle);
            fclose($fp);

            echo "\n\n -> Chemin fichier SQL = $pathFileLog";
        }
        echo "\n\n---------------------------------------------> FIN GENERATION SCRIPT <------------------------------------------------";
    }

    /**
     * redressement de la table  Telechargement  remplissage de la taille des fichiers telecharges.
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function updateTailleTelechargementNonAnonyme()
    {
        try {
            $sql = <<<QUERY
UPDATE
	Telechargement
SET
	Telechargement.poids_telechargement = (
	SELECT
		IFNULL(DCE.taille_dce,0)
	FROM
		DCE
	WHERE
		Telechargement.consultation_id = DCE.consultation_id
	AND
		Telechargement.organisme = DCE.organisme

	ORDER BY DCE.id DESC
	LIMIT 0,1
		)
WHERE
		Telechargement.noms_fichiers_dce = 'DCE_INTEGRAL'
	OR
		Telechargement.noms_fichiers_dce = 'DCE integral'

QUERY;

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->prepare($sql)->execute();
            echo "\n",' Mise a  jour des telechargements OK';
        } catch (Exception $e) {
            echo "\n",' ERREUR : '.$e->getMessage()."\n".$e->getTraceAsString()."\n";
        }
    }

    /**
     * redressement de la table TelechargementAnonyme remplissage de la taille des fichiers téléchargés.
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function updateTailleTelechargementAnonyme()
    {
        try {
            $sql = <<<QUERY
UPDATE
	TelechargementAnonyme
SET
	TelechargementAnonyme.poids_telechargement = (
	SELECT
		IFNULL(DCE.taille_dce,0)
	FROM
		DCE
	WHERE
		TelechargementAnonyme.consultation_id = DCE.consultation_id
	AND
		TelechargementAnonyme.organisme = DCE.organisme

	ORDER BY DCE.id DESC
	LIMIT 0,1
		)
WHERE
		TelechargementAnonyme.noms_fichiers_dce = 'DCE integral'
	OR
		TelechargementAnonyme.noms_fichiers_dce = 'DCE_INTEGRAL'
QUERY;

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->prepare($sql)->execute();
            echo "\n",' Mise à  jour des telechargements anonymes OK';
        } catch (Exception $e) {
            echo "\n",' ERREUR : '.$e->getMessage()."\n".$e->getTraceAsString()."\n";
        }
    }

    public static function updateTailleDCE()
    {
        $param = [];
        try {
            $sql = <<<QUERY
SELECT
	organisme,
	consultation_id,
	id,
	dce
FROM
	DCE dce
WHERE
		dce.taille_dce is null
	AND
		dce.id = (
			SELECT
				max(id)
			FROM
				DCE d
			WHERE
					d.organisme= dce.organisme
				AND
					d.consultation_id = dce.consultation_id
		)
QUERY;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $stmt = $connexion->prepare($sql);
            $stmt->execute();
            $blob = new Atexo_Blob();
            $sqlUpdate = <<<QUERY
UPDATE
	DCE
SET
	taille_dce = :taille
WHERE
		id = :id
	AND
		organisme = :organisme
	AND
		consultation_id = :consultation_id

QUERY;

            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $param[':taille'] = ($blob->getTailFile($row['dce'], $row['organisme'])) ?: 0;
                if ($param[':taille']) {
                    $param[':consultation_id'] = $row['consultation_id'];
                    $param[':organisme'] = $row['organisme'];
                    $param[':id'] = $row['id'];
                    $stmtUpdate = $connexion->prepare($sqlUpdate);
                    $stmtUpdate->execute($param);
                    print_r($param);
                    unset($stmtUpdate);
                }
            }
            echo "\n",' Mise à  jour des dces OK';
        } catch (Exception $e) {
            echo "\n",' ERREUR : '.$e->getMessage()."\n".$e->getTraceAsString()."\n";
        }
    }

    public static function updateTailleTelechargement()
    {
        echo "\n Debut MAJ DCE";
        self::updateTailleDCE();
        echo "\n fin MAJ DCE";
        echo "\n Debut MAJ Telechargement";
        self::updateTailleTelechargementNonAnonyme();
        echo "\n Fin MAJ Telechargement";
        echo "\n Debut MAJ Telechargement anonyme";
        self::updateTailleTelechargementAnonyme();
        echo "\n Fin MAJ Telechargement anonyme";
    }
}
