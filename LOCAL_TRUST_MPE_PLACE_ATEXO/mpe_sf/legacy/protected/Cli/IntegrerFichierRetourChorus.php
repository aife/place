<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Reception;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use AtexoCrypto\Dto\Fichier;
use Exception;

/**
 * Intègre les fichier reçu par Chorus.
 *
 * @author adil el kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_IntegrerFichierRetourChorus extends Atexo_Cli
{
    public static function run()
    {
        $bool = null;
        $params = [];
        $params['service'] = 'Chorus';
        $params['webserviceBatch'] = 'Cli_IntegrerFichierRetourChorus';
        self::oversight($params);
        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;

        try {
            (new Atexo_Chorus_Util())->loggerInfosReception(">>>>>>>>>>>>>>>>>>> DEBUT INTEGRATION DES FLUX CHORUS <<<<<<<<<<<<<<<<<<< \n");
            $pointeur = opendir(Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION'));
            //Début préparation de la séssion
            self::preparerSession();
            //Fin préparation de la séssion
            $i = 0;
            while ($fichier = readdir($pointeur)) {
                (new Atexo_Chorus_Util())->loggerInfosReception('Debut traitement du fichier : '.$fichier);
                if (('.' != $fichier) && ('..' != $fichier)) {
                    if (is_file(Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION').$fichier)) {
                        $xml = file_get_contents(Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION').$fichier);
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recuperation de l'xml : \n".$xml);
                        if(!empty($xml)) {
                            if (strstr($fichier, 'CEN')) {
                                $bool = (new Atexo_Chorus_Reception())->receptionCompteRendu($xml, $fichier);
                            } elseif (strstr($fichier, 'FSO')) {
                                $bool = (new Atexo_Chorus_Reception())->receptionMessageSuivi($xml, $fichier);
                            } elseif (strstr($fichier, 'CIR')) {
                                $bool = (new Atexo_Chorus_Reception())->receptionCompteRenduCIR($xml, $fichier);
                            } elseif (strstr($fichier, 'FIR')) {
                                $bool = (new Atexo_Chorus_Reception())->receptionCompteRenduFIR($xml, $fichier);
                            }
                            if (!is_dir(Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE'))) {
                                mkdir(Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE'));
                            }
                        } else {
                            $objet = "Erreur lors du traitement du fichier : "
                                . $fichier . "(routine : IntegrerFichierRetourChorus)";
                            $date = date("Y-m-d H:i:s");
                            $message = <<<STR
Bonjour,
Un fichier vide a été détecté  et il a été  ignoré .
Nom fichier : $fichier
date et heure : $date
STR;
                            (new Atexo_Chorus_Util())->envoyerMailSupportChorus( $objet, $message);
                        }

                        if ($bool) {
                            ++$i;
                            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                            $params['poids'] = filesize(Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION').$fichier);
                            system('mv '.Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION').$fichier.' '.Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE').$fichier);
                            (new Atexo_Chorus_Util())->loggerInfosReception('Fichier '.$fichier.' deplace , Source = '.Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION').$fichier.' , Destination = '.Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE').$fichier);
                        } else {
                            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                        }
                        $params['oversight'] = self::oversight($params);
                    }
                }
                (new Atexo_Chorus_Util())->loggerInfosReception('Fin traitement du fichier : '.$fichier);
            }
            (new Atexo_Chorus_Util())->loggerInfosReception(">>>>>>>>>>>>>>>>>>> FIN INTEGRATION DES FLUX CHORUS <<<<<<<<<<<<<<<<<<< \n");
        } catch (Exception $e) {
            $erreurMessage = "Erreur lors de l'integration des retours CHORUS , Methode: Cli_IntegrerFichierRetourChorus::run, Exception [".$e->getMessage().']';
            (new Atexo_Chorus_Util())->loggerErreurReception($erreurMessage);
            echo $erreurMessage;
        }
    }

    public static function preparerSession()
    {
        try {
            $session = [0 => '',
                1 => [],
                2 => '',
                3 => '',
                4 => '',
                5 => '',
                6 => '',
                7 => '',
                8 => '',
                9 => '',
                10 => '',
                11 => '',
                12 => '',
                13 => [],
                14 => '',
                15 => '',
                16 => '',
                17 => '',
                18 => '',
                19 => '',
                20 => '',
                21 => '',
                22 => '',
                23 => '',
                24 => '',
                25 => '',
                26 => '',
                27 => '',
                28 => '',
                29 => '',
                30 => '',
                31 => '',
                32 => '',
                33 => '',
                34 => '',
                35 => '',
                36 => '',
                37 => '',
                38 => '',
                39 => '',
                40 => '',
                41 => '',
                42 => '',
                43 => '',
                44 => '',
                45 => '',
                46 => '',
                47 => '',
                48 => '',
                49 => '',
                50 => '',
                51 => '',
                52 => '',
                53 => '',
                54 => '',
                55 => '',
                56 => '',
                57 => '',
                58 => '',
                59 => '',
                60 => '',
                61 => '',
                62 => '',
                63 => '',
                64 => '',
                65 => '',
            ];
            session_id(uniqid());
            $_SESSION[session_id()] = serialize($session);
            (new Atexo_Chorus_Util())->loggerInfosReception('Preparation de la session : OK ');
        } catch (Exception $e) {
            $erreurMessage = 'Erreur lors de la preparation de la session , Methode: Cli_IntegrerFichierRetourChorus::preparerSession, Exception ['.$e->getMessage().']';
            (new Atexo_Chorus_Util())->loggerErreurReception($erreurMessage);
            echo $erreurMessage;
        }
    }
}
