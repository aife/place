<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Cron pour intialiser une BD.
 *
 * @author boukataya Fatima Zahra <fatima.boukataya@atexo.com>
 */
class Cli_InitialisePF extends Atexo_Cli
{
    public static function run()
    {
        $_GET = [];

        Prado::log("Début de l'initialisation des bases de données COMMON et ORGANISMES ", TLogger::INFO, 'Atexo.Cli.InitialiseDBs');
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                $argument = explode('=', $arguments[$i]);
                $_GET[$argument[0]] = $argument[1] ?: true;
            }
        }
        self::initialiseDBandDataFolders();
    }

    public static function initialiseDBandDataFolders()
    {
        $scriptCOMMON = [];
        $scriptOrganisme = [];
        //Vider la BD commune.
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Agent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Agent_Service_Metier`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `HabilitationAgent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Socle_Habilitation_Agent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Certificat_Agent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `SuiviAcces`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Historique_suppression_agent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Annonce`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `consultation`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `CategorieLot`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `consultation_document_cfe`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `InvitationConsultationTransverse`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Entreprise`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Entreprise_info_exercice`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `responsableengagement`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Certificats_Entreprises`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `trace_operations_inscrit`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Inscrit`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Alerte`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Offre`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `blob`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `blob_file`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Justificatifs`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `InscritHistorique`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Societes_Exclues`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `sso_agent`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `sso_entreprise`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Telechargement`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `AffiliationService`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Service`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `QuestionDCE`';
        $scriptCOMMON[] = ' TRUNCATE TABLE  `Retrait_Papier`';
        echo 'Vider les tables de la BD Commune...';
        foreach ($scriptCOMMON as $query) {
            echo '...';
            try {
                Atexo_Db::getLinkCommon()->query($query);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
        echo "\n";

        //Vider les BDs organismes.

        $scriptOrganisme[] = ' TRUNCATE TABLE  `AcheteurPublic`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Admissibilite_Enveloppe_Lot`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Admissibilite_Enveloppe_papier_Lot`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Annonce`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `AnnonceJAL`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `AnnonceJALPieceJointe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `AnnonceMoniteur`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `AVIS`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `blob`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `blob_file`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `blocFichierEnveloppe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `blocFichierEnveloppeTemporaire`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `CategorieLot`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `CertificatChiffrement`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `CertificatPermanent`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Complement`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `CompteMoniteur`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `consultation`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `DATEFIN`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `DCE`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `decisionEnveloppe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `DecisionLot`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `DestinataireAnnonceJAL`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `destinataire_centrale_pub`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `DocumentExterne`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Echange`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EchangeDestinataire`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EchangePieceJointe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `enchere`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereEntreprisePmi`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereFichierDetail`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereOffre`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereOffreReference`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EncherePmi`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereReference`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereTrancheBaremeReference`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereTranchesBaremeNETC`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `EnchereValeursInitiales`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `enchere_entreprise`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Enveloppe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Enveloppe_papier`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `fichierEnveloppe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `FichierLot`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `fichiers_liste_marches`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `gestion_adresses`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `historiques_consultation`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `JAL`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Journaux`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Marche`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `MarcheAnnee`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `MarchePublie`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Mesure_avancement`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Numerotation_ref_cons_auto`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Offre`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Offre_papier`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Ordre_Du_Jour`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `passation_marche_a_venir`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Passation_consultation`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `programme_previsionnel`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Publicite`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `QuestionDCE`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `ReceptionEnveloppe`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `ReferentielDestinationFormXml`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `RPA`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `visite_lieux`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `RG`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Retrait_Papier`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `AffiliationService`';
        $scriptOrganisme[] = ' TRUNCATE TABLE  `Service`';

        $organismes = (new Atexo_Organismes())->retrieveAllOrganismes();
        foreach ($organismes as $oneOrganisme) {
            $org = $oneOrganisme->getAcronyme();
            echo 'Vider les tables de la BD et les dossiers Data PF '.$org.'...';
            foreach ($scriptOrganisme as $query) {
                try {
                    echo '...';
                    Atexo_Db::getLinkOrganism($org)->query($query);
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }
            }
            //Vider les dossiers Data PF:Organismes:
            try {
                system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').$org.'/files/*');
                system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').$org.'/indexLuceneAgent/');
                system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').$org.'/indexLuceneArchive/');
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
            echo "\n";
            Atexo_Db::closeOrganism($org);
        }

        //Vider les dossiers Data PF:Common:
        //Vider les dossiers FichiersArchive, indicateursCles, logs, statistiques, tmp, images.
        try {
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/files/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/logs/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/tmp/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/indexLucene/');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/indexLuceneEntreprise/');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'common/indexLucenePortail/');

            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'FichiersArchive/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'indicateursCles/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'statistiques/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'tmp/*');
            system('rm -rf '.Atexo_Config::getParameter('BASE_ROOT_DIR').'images/*');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }
}
