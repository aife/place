<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdresseFacturationJal;
use Application\Propel\Mpe\CommonAdresseFacturationJalPeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\CommonHistoriqueAvisPub;
use Application\Propel\Mpe\Om\BaseCommonAdresseFacturationJalPeer;
use Application\Service\Atexo\AnnoncePress\Press;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Soap\Atexo_Soap_InterfaceWsTED;
use Exception;
use Prado\Prado;

/**
 * publie les avis de publicité des consultations à OPOCE et SIMAP2.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 *
 * @category Atexo
 */
class Cli_PublicationAvis extends Atexo_Cli
{
    public static function run()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCom->beginTransaction();
        $listeAvis = (new Atexo_Publicite_AvisPub())->retreiveAvisEnvoiPlanifie();
        if (is_array($listeAvis) && count($listeAvis)) {//Debut Fin
            $message = 'Debut Cron '.date('Y-m-d H:i:s');
            $logger->info($message);
            echo $message;

            foreach ($listeAvis as $avis) {
                try {
                    if (!$avis->getDatePublication() && $avis->getDateValidation()) {//L'entité coordinatrice et(ou) SIP ont validé l'envoie de la publicité à OPOCE
                        $message = 'Avis id = '.$avis->getid();
                        $logger->info($message);
                        echo $message;
                        $c = new Criteria();
                        $c->add(CommonConsultationPeer::ID, $avis->getConsultationId());
                        $c->add(CommonConsultationPeer::ORGANISME, $avis->getOrganisme());
                        $consultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);
                        if ($consultation instanceof CommonConsultation) {
                            $message = 'Consultation: Organisme = '.$consultation->getOrganisme().' - Ref utilisateur = '.$consultation->getReferenceUtilisateur().' - Id Technique = '.$consultation->getId().' trouvée ';
                            $logger->info($message);
                            echo $message;
                            if ($consultation->getDateMiseEnLigneSouhaitee() && $consultation->getDateMiseEnLigneSouhaitee() <= date('Y-m-d')) {
                                //Initialisation des variables
                                $publicationOpoce = true;
                                $publicationSimap = true;
                                $publicationPresse = true;
                                $datePublicationOpoce = '';
                                $datePublicationPresse = '';

                                //-------------------------------------------- Publication OPOCE ----------------------------------------------------
                                //Verification de l'existence d'un destinataire opoce

                                $destinataireOpoce = (new Atexo_Publicite_AvisPub())
                                    ->getDestinataireComplet(
                                        $avis->getId(),
                                        $avis->getOrganisme(),
                                        $connexionCom,
                                        Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')
                                    );
                                $submissionId = '';
                                if ($destinataireOpoce instanceof CommonDestinatairePub) {
                                    $message = 'Destinataire OPOCE idDest = '.$destinataireOpoce->getId().' trouvé';
                                    echo $message;
                                    $logger->info($message);
                                    //Verification si avis Opoce publié
                                    if ($destinataireOpoce->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB')) {
                                        if ($destinataireOpoce->getIdDossier() && $destinataireOpoce->getIdDispositif()) {
                                            $datePublicationOpoce = Atexo_Publicite_AnnonceSub::envoyerPubSimap($destinataireOpoce);
                                            $datePublicationOpoce = Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION') ? Atexo_Util::dateDansFutur($datePublicationOpoce, Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION')) : $datePublicationOpoce;
                                        }
                                    } elseif ($destinataireOpoce->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_ESENDER')) {
                                        $submissionId = (new Atexo_Publicite_AvisPub())->getCodeRetour($destinataireOpoce->getId(), $destinataireOpoce->getOrganisme());
                                        if (!$submissionId) {
                                            $datePublicationOpoce = self::publicationAvisOpoce($avis, $consultation, $destinataireOpoce, $connexionCom, $message);
                                            $logger->info($message);
                                        } else {
                                            $publicationOpoce = true;
                                            $message = 'OPOCE déjà publié ';
                                            $logger->info($message);
                                            echo $message;
                                        }
                                    } elseif ($destinataireOpoce->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_TED')) {
                                        $datePublicationOpoce = self::publicationAvisTypeTED($avis, $destinataireOpoce);
                                        $datePublicationOpoce = Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION') ? Atexo_Util::dateDansFutur($datePublicationOpoce, Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION')) : $datePublicationOpoce;
                                    }
                                    $logger->info('Date Pub : '.$datePublicationOpoce);
                                    if (!$datePublicationOpoce) {
                                        $message = 'Publication OPOCE KO ';
                                        $logger->info($message);
                                        echo $message;
                                        $publicationOpoce = false;
                                    } else {//L'envoi à OPOCE a reussi
                                        $message = 'Publication OPOCE OK ';
                                        $logger->info($message);
                                        echo $message;
                                    }
                                } else {
                                    if ($submissionId) {
                                        $message = "\n\t-> Destinataire OPOCE déjà publié ";
                                    } else {
                                        $message = "\n\t-> Destinataire OPOCE statut complet non trouvée ";
                                    }
                                    $logger->info($message);
                                    echo $message;
                                }

                                //------------------------------------------- Publication avis SIMAP2 -----------------------------------------------
                                if ($publicationOpoce) {
                                    //Verification de l'existence d'un destinataire SIMAP2

                                    $destinatairePortail = (new Atexo_Publicite_AvisPub())->getDestinataireComplet($avis->getId(), $avis->getOrganisme(), $connexionCom, Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2'));
                                    if ($destinatairePortail instanceof CommonDestinatairePub) {
                                        $message = 'Destinataire Simap2 idDest = '.$destinatairePortail->getId().' trouvé';
                                        $logger->info($message);
                                        echo $message;
                                        if (self::publicateSimap2($avis, $destinatairePortail, $consultation, $datePublicationOpoce, $connexionCom)) {//L'envoi à SIMAP2 reussi
                                            $message = 'Publication Simap2 OK ';
                                            $logger->info($message);
                                            echo $message;
                                        } else {//Echec envoi SIMAP2
                                            $message = 'Publication Simap2 KO ';
                                            $logger->info($message);
                                            echo $message;
                                            $publicationSimap = false;
                                        }
                                    } else {
                                        $message = 'Destinataire SIMAP2 (statut complet) non trouvée ';
                                        $logger->info($message);
                                        echo $message;
                                    }
                                }

                                //-------------------------------------------- Publication Presse ----------------------------------------------------
                                if ($publicationSimap && $publicationOpoce) {
                                    //Verification de l'existence d'un destinataire Presse

                                    $destinatairePresse = (new Atexo_Publicite_AvisPub())->getDestinataireComplet($avis->getId(), $avis->getOrganisme(), $connexionCom, Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE'));
                                    if ($destinatairePresse instanceof CommonDestinatairePub && $destinatairePresse->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET')) {
                                        $message = 'Destinataire Presse idDest = '.$destinatairePresse->getId().' trouvé';
                                        $logger->info($message);
                                        echo $message;
                                        $datePublicationPresse = self::SendToPress($avis, $consultation, $destinatairePresse, $connexionCom);
                                        if (!$datePublicationPresse) {
                                            $message = 'Publication Presse KO ';
                                            $logger->info($message);
                                            echo $message;
                                            $publicationPresse = false;
                                        } else {
                                            $message = 'Publication Presse OK ';
                                            $logger->info($message);
                                            echo $message;
                                        }
                                    } else {
                                        $message = 'Destinataire Presse statut complet non trouvée ';
                                        $logger->info($message);
                                        echo $message;
                                    }
                                }


                                //--------------------------------------- Publication DUME ----------------------------------
                                if (
                                    Atexo_Module::isEnabled('InterfaceDume') &&
                                    '1' == $consultation->getDumeDemande() &&
                                    (
                                        Atexo_Util::validateDate(
                                            $consultation->getDateMiseEnLigneCalcule(),
                                            'Y-m-d H:i:s'
                                        ) &&
                                        !empty($consultation->getDateMiseEnLigneCalcule()) &&
                                        '0000-00-00 00:00:00' != $consultation->getDateMiseEnLigneCalcule()
                                    )
                                ) {
                                    $retour = Atexo_Dume_AtexoDume::publierDumeAcheteur($consultation);

                                    if (is_array($retour) && true == $retour['erreur']) {
                                        $logger->error(
                                            $consultation->getOrganisme() . '|' . $consultation->getId()
                                            . '|Dume non publié =>ne pas publié de la consultation : erreur '
                                            . $retour['messageErreur']
                                        );
                                    } else {
                                        Atexo_Dume_AtexoDume::createJmsJobToPublishDume($consultation);
                                        $logger->error(
                                            $consultation->getOrganisme() . '|' . $consultation->getId()
                                            . '|Dume publié'
                                        );
                                    }
                                }
                                //Mise à jour du statut global de l'avis
                                if ($publicationOpoce && $publicationPresse && $publicationSimap) {
                                    self::miseAJourStatutAvisApresPublication($avis, $connexionCom);
                                    $connexionCom->commit();
                                }
                            } else {
                                if (!$consultation->getDateMiseEnLigneSouhaitee() || '' == $consultation->getDateMiseEnLigneSouhaitee() || null == $consultation->getDateMiseEnLigneSouhaitee()) {
                                    $message = 'Pas de date de mise en ligne souhaitée ';
                                    $logger->info($message);
                                    echo $message;
                                } else {
                                    $message = 'Consultation  '.$consultation->getOrganisme().' - '.$consultation->getId().' ne respecte pas les règles de publication. Date de mise en ligne souhaitée = '.$consultation->getDateMiseEnLigneSouhaitee();
                                    $logger->info($message);
                                    echo $message;
                                }
                            }
                        }
                    }
                    echo "\n";
                } catch (Exception $e) {
                    $subject = 'SIMAP2 : Problème à la publication';
                    $connexionCom->rollBack();
                    $log = "\n\n";
                    $log .= "____________________________________\n";
                    $log .= "\n\n Envoi des avis à OPOCE, PRESSE, SIMAP2 <br/>";
                    $log .= "\n\n";
                    $log .= "____________________________________\n";
                    $log .= $e->getMessage();
                    $htmlMessage = Atexo_Message::getGenericEmailFormat($log);
                    //Envoie de mail
                    Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), Atexo_Config::getParameter('MAIL_TECHNIQUE_GESTION_PUB'), $subject, $htmlMessage, '', '', false, true);
                    $logger->error(' Erreur  '.$e->getMessage().' '.$e->getTraceAsString());
                }
            }
        } else {
            $logger->info('AUCUN AVIS TROUVE');
            echo "\n\t --------------------------------> AUCUN AVIS TROUVE <----------------------------------- \n\n";
        }
    }

    /**
     * Gère la publication de l'avis de publicité à OPOCE.
     *
     */
    public static function publicationAvisOpoce($avis, $consultation, $destinataire, $connexionCom, &$log)
    {
        //$connexionCom = Propel::getConnection( Atexo_Config::getParameter('COMMON_DB'). Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')
                                                && $destinataire->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET')) {
            //Récuperation du formulaire xml correspondant au destinataire OPOCE
            $refDestFormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($destinataire->getId(), $destinataire->getOrganisme());
            if ($refDestFormXml instanceof CommonFormXmlDestinataireOpoce) {
                $xml = $refDestFormXml->getXml();
                //Vérification de l'xml avant envoie
                $rapportXml = (new Atexo_Soap_InterfaceWsTED())->parseNotice(base64_decode($xml));
                if (is_array($rapportXml)) {
                    if ('false' == $rapportXml['erreur']) {
                        //Envoie de l'xml à OPOCE
                        $rapportEnvoie = (new Atexo_Soap_InterfaceWsTED())->submitNotice(base64_decode($xml));
                        if ('false' == $rapportEnvoie['erreur'] && $rapportEnvoie['resultat']) {
                            //Enregistrer le code id renvoyé lors du submit dans la table : FormXmlDestinataireOpoce
                            $refDestFormXml->setCodeRetour($rapportEnvoie['resultat']);
                            $refDestFormXml->save($connexionCom);

                            //Mise à jour du statut du destinataire
                            $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION'));
                            $datePublication = date('Y-m-d H:i:s');
                            $destinataire->setDatePublication($datePublication);
                            $destinataire->save($connexionCom);
                            //Mise à jour des informations en BD
                            $connexionCom->commit();

                            return $datePublication;
                        } elseif ('true' == $rapportEnvoie['erreur']) {
                            $refDestFormXml->setMessageRetour('submit : '.$rapportEnvoie['libelle_erreur']);
                            $refDestFormXml->save($connexionCom);
                            //Sauvegarde de l'erreur dans l'historique de l'avis
                            $historique = (new Atexo_Publicite_AvisPub())->getLastHistoriqueByStatut($avis->getId(), Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'), $avis->getOrganisme());
                            if ($historique instanceof CommonHistoriqueAvisPub) {
                                $historique->setMotifRejet('submit : '.$rapportEnvoie['libelle_erreur']);
                                $historique->save($connexionCom);
                            }
                            $connexionCom->commit();
                            $message = 'Erreur lors du submit. MESSAGE_ERREUR : '.$rapportEnvoie['libelle_erreur'];
                            $log .= $message;
                            echo $message;

                            return false;
                        }
                    } else {
                        $refDestFormXml->setMessageRetour('parse : '.$rapportXml['libelle_erreur']);
                        $refDestFormXml->save($connexionCom);
                        //Sauvegarde de l'erreur dans l'historique de l'avis
                        $historique = (new Atexo_Publicite_AvisPub())->getLastHistoriqueByStatut($avis->getId(), Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'), $avis->getOrganisme());
                        if ($historique instanceof CommonHistoriqueAvisPub) {
                            $historique->setMotifRejet('parse : '.$rapportXml['libelle_erreur']);
                            $historique->save($connexionCom);
                        }
                        $connexionCom->commit();
                        $message = 'Erreur lors du parse. MESSAGE_ERREUR : '.$rapportXml['libelle_erreur'];
                        $log .= $message;
                        echo $message;

                        return false;
                    }
                } else {
                    $refDestFormXml->setMessageRetour('technical : '.print_r($rapportXml, true));
                    $refDestFormXml->save($connexionCom);
                    //Sauvegarde de l'erreur dans l'historique de l'avis
                    $historique = (new Atexo_Publicite_AvisPub())->getLastHistoriqueByStatut($avis->getId(), Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'), $avis->getOrganisme());
                    if ($historique instanceof CommonHistoriqueAvisPub) {
                        $historique->setMotifRejet('technical : '.print_r($rapportXml, true));
                        $historique->save($connexionCom);
                    }
                    $connexionCom->commit();
                    $message = 'Erreur technique. MESSAGE_ERREUR : '.print_r($rapportXml, true);
                    $log .= $message;
                    echo $message;

                    return false;
                }
            }
        }
    }

    /**
     * Envoi à la presse.
     **/
    public static function SendToPress($avis, $consultation, $destinataire, $connexionCom)
    {
        $libelleServiceAgentConnecte = null;
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $cc = Atexo_Config::getParameter('MAIL_PRODUCTION');
        $taille = 0;
        $validePJ = false;
        $envoiMail = false;
        $critere = new Criteria();
        $pjRtfOK = false;
        $logger = Atexo_LoggerManager::getLogger('publicite');
        //Début regénération de l'avis presse à afficher dans le mail
        $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avis->getTypeAvis());
        if ($modele) {
            $pieceJointe = (new Atexo_Publicite_AvisPub())->CommonAnnoncePressPieceJointeByIdAndOrganisme($avis->getIdAvisPresse(), $avis->getOrganisme(), $connexionCom);
            if ($pieceJointe instanceof CommonAnnoncePressPieceJointe) {
                $tableauContenuGenerationDocPresse = (new Atexo_Publicite_AvisPub())->genererAvisPressePortail($modele, $connexionCom, $avis->getId(), $consultation, 'fr', false, date('Y-m-d H:i:s'));
                $atexoBlob = new Atexo_Blob();
                $size = ($tableauContenuGenerationDocPresse[0]) ? $atexoBlob->getTailFile($tableauContenuGenerationDocPresse[0], $pieceJointe->getOrganisme()) : 0;
                if ($size > Atexo_Config::getParameter('TAILLE_MINIM_RTF_AVIS_PRESS')) {
                    $logger->info("size $size du rtf OK avis ".$avis->getId());
                    $pjRtfOK = true;
                } else {
                    $logger->info("size $size du rtf KO avis ".$avis->getId());
                    $cmd = Atexo_Config::getParameter('COMMANDE_RESTART_OPENOFFICE');
                    if ($cmd) {
                        $logger->info('Redemarrage openOffice cmd '.$cmd);
                        exec($cmd);
                        $tableauContenuGenerationDocPresse = (new Atexo_Publicite_AvisPub())->genererAvisPressePortail($modele, $connexionCom, $avis->getId(), $consultation, 'fr', false, date('Y-m-d H:i:s'));
                        $logger->info('Regeneration du rtf avis '.$avis->getId());
                        $atexoBlob = new Atexo_Blob();
                        $size = ($tableauContenuGenerationDocPresse[0]) ? $atexoBlob->getTailFile($tableauContenuGenerationDocPresse[0], $pieceJointe->getOrganisme()) : 0;
                        if ($size > Atexo_Config::getParameter('TAILLE_MINIM_RTF_AVIS_PRESS')) {
                            $logger->info('size du rtf OK avis '.$avis->getId());
                            $pjRtfOK = true;
                        } else {
                            $logger->info("size $size du rtf KO avis ".$avis->getId());
                        }
                    }
                }

                if ($pjRtfOK) {
                    $pieceJointe->setTaille($size);
                    $pieceJointe->setPiece($tableauContenuGenerationDocPresse[0]);
                    $pieceJointe->save($connexionCom);
                }
            }
        }
        //Fin regénération de l'avis presse à afficher dans le mail

        if ($pjRtfOK && $destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE') && $destinataire->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET')) {
            $annoncePress = (new Press())->getAnnoncePressByIdDest($destinataire->getId(), $destinataire->getOrganisme());
            if ($annoncePress) {
                $critere->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $annoncePress->getOrganisme());
                $critere->add(CommonAnnoncePressPieceJointePeer::ID_ANNONCE_PRESS, $annoncePress->getId());
                $PJs = $annoncePress->getCommonAnnoncePressPieceJointes($critere, $connexionCom);
                if (is_array($PJs) && 0 != count($PJs)) {
                    $validePJ = true;
                }
                $dossier = '';
                $zipPj = (new Atexo_Message())->getZipPj($PJs, $taille, $annoncePress->getOrganisme(), $dossier);
                $taille = str_replace('.', ',', round($taille / (1_048_985.64)));
                //Ajout du logo au zip
                if ($avis->getIdBlobLogo()) {
                    $atexoBlob = new Atexo_Blob();
                    $blobContent = $atexoBlob->return_blob_to_client($avis->getIdBlobLogo(), $avis->getOrganisme());
                    //On recupère l'adresse de facturation
                    $adresseFact = BaseCommonAdresseFacturationJalPeer::retrieveByPK($annoncePress->getIdAdresseFacturation(), $annoncePress->getOrganisme(), $connexionCom);
                    $fileNameLogo = '';
                    if ($adresseFact instanceof CommonAdresseFacturationJal) {
                        $fileNameLogo = $adresseFact->getNomFichier();
                    }
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP').'/'.$fileNameLogo;
                    $fp = fopen($tmpFile, 'w');
                    fwrite($fp, $blobContent);
                    fclose($fp);
                    $taille = $taille + filesize($tmpFile);
                    (new Atexo_Zip())->addFileToZip($tmpFile, $zipPj);
                }
                // TODO test sur lecture du fichier
                if ($zipPj) {
                    $code = (new Atexo_Files())->read_file($zipPj);
                }
                $fileName = $consultation->getReferenceUtilisateur().'_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');

                //Début recupération des informations du mail
                //$organePresse = "";
                $htmlMessage = new Atexo_Message();
                $corpsEmail = $htmlMessage->render('PATH_FILE_MAIL_PUBLICATION_AVIS');
                $newTextMail = '';
                $infoFacturation = '';
                $meilleuresSalutations = '';
                $infosOrganisme = '';
                $valeurSip = false;
                if ($annoncePress->getIdAdresseFacturation()) {
                    $adresseFacturation = CommonAdresseFacturationJalPeer::retrieveByPK($annoncePress->getIdAdresseFacturation(), $annoncePress->getOrganisme(), $connexionCom);
                    if ($adresseFacturation instanceof CommonAdresseFacturationJal) {
                        //$infoFacturation .= Prado::localize('DEFINE_COMMUNICATION_UTILISER_FACTURATION').' :';
                        $corpsEmail = str_replace('DEFINE_COMMUNICATION_UTILISER_FACTURATION', Prado::localize('DEFINE_COMMUNICATION_UTILISER_FACTURATION'), $corpsEmail);

                        $corpsEmail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corpsEmail);
                        $corpsEmail = str_replace('SALUTATION', Prado::localize('DEFINE_TEXT_BONJOUR'), $corpsEmail);
                        $corpsEmail = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $corpsEmail);

                        $dateMiseEnLigne = ($consultation->getDateMiseEnLigneCalcule() && '0000-00-00 00:00:00' != $consultation->getDateMiseEnLigneCalcule()) ? Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule(), false) : date('d/m/Y');

                        $chaine1 = str_replace('[__date_mise_ligne__]', $dateMiseEnLigne, Prado::localize('DEFINE_CONTENU_COMMUNICATION_UTILISER_FACTURATION'));
                        $chaine2 = '<< '.str_replace('[__ref__cons__]', $consultation->getReferenceUtilisateur(), $chaine1);

                        $corpsEmail = str_replace('VALEUR_REFERENCE', $chaine2, $corpsEmail);
                        $corpsEmail = str_replace('VALEUR_INTITULE_CONSULTATION', $consultation->getIntituleTraduit().' >>', $corpsEmail);

                        //$infoFacturation .= '<< '.str_replace('[__ref__cons__]', $consultation->getReferenceUtilisateur(),$chaine1).' :'.$consultation->getIntituleTraduit().' >>'.'';

                        $corpsEmail = str_replace('DEFINE_TEXT_PARVENIR_ADRESSE_SUIVANTE', Prado::localize('DEFINE_TEXT_PARVENIR_ADRESSE_SUIVANTE'), $corpsEmail);
                        //$infoFacturation .= Prado::localize('DEFINE_TEXT_PARVENIR_ADRESSE_SUIVANTE').' : '.'';
                        //$infoFacturation .= $adresseFacturation->getInformationFacturation().'';
                        $corpsEmail = str_replace('VALEUR_TEXT_PARVENIR_ADRESSE_SUIVANTE', $adresseFacturation->getInformationFacturation(), $corpsEmail);

                        $logoAPublier = ($avis->getIdBlobLogo()) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');

                        $corpsEmail = str_replace('TEXT_LOGO_A_PUBLIER', Prado::localize('TEXT_LOGO_A_PUBLIER'), $corpsEmail);
                        //$newTextMail .= Prado::localize('TEXT_LOGO_A_PUBLIER').' : '.$logoAPublier.'';
                        $corpsEmail = str_replace('VALEUR_LOGO_A_PUBLIER', $logoAPublier, $corpsEmail);

                        $pouvoirsAdjudicateurs = '';

                        //$newTextMail .= Prado::localize('DEFINE_TEXT_CAS_PROBLEME_CONTACTER').'';
                        $corpsEmail = str_replace('DEFINE_TEXT_CAS_PROBLEME_CONTACTER', Prado::localize('DEFINE_TEXT_CAS_PROBLEME_CONTACTER'), $corpsEmail);

                        //Début agent Créateur consultation
                        $agentCreateurAvis = (new Atexo_Agent())->retrieveAgent($consultation->getIdCreateur());
                        $email = '';
                        $fax = '';
                        $tel = '';
                        $nomPrenom = '';
                        if ($agentCreateurAvis instanceof CommonAgent) {
                            $email = $agentCreateurAvis->getEmail();
                            $fax = $agentCreateurAvis->getNumFax();
                            $tel = $agentCreateurAvis->getNumTel();
                            $nomPrenom = $agentCreateurAvis->getNom().' '.$agentCreateurAvis->getPrenom();
                        }
                        //Fin agent Créateur consultation
                        //Début détermination des pouvoirs adjudicateurs
                        $serviceConsultation = $consultation->getServiceId();
                        $libellePereServiceAgentConnecte = '';
                        $libellePremierFilsServiceAgentConnecte = '';
                        $libelleDivision = '';
                        if (0 != $serviceConsultation) {
                            $listeParents = Atexo_EntityPurchase::getArParentsId($serviceConsultation, $consultation->getOrganisme(), false);

                            if (is_array($listeParents) && count($listeParents) > 0 && $listeParents[0] === 0) {
                                $listeParents[0] = null;
                            }

                            if (1 == count($listeParents)) {//Cas n=2, on doit voir seulement le service de l'agent connecté
                                $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[1], $consultation->getOrganisme());
                                $libelleDivision = $libelleServiceParentDeAgentConnecte;
                            } elseif (2 == count($listeParents)) {//Cas n=3, on doit voir le service de l'agent connecté et le père
                                $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[0], $consultation->getOrganisme());
                                $libelleDivision = $libelleServiceParentDeAgentConnecte;
                            } elseif (count($listeParents) >= 3) {//Cas n>3, on doit voir le service de l'agent connecté et les deux premiers parents de l'arborescence
                                $libellePereServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 2], $consultation->getOrganisme());
                                $libellePremierFilsServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 3], $consultation->getOrganisme());
                                $libelleDivision = $libellePremierFilsServiceAgentConnecte;
                            }
                            $libelleServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($serviceConsultation, $consultation->getOrganisme());
                        }
                        //Fin détermination des pouvoirs adjudicateurs
                        $pouvoirsAdjudicateurs .= $nomPrenom.'';
                        if ($libellePereServiceAgentConnecte) {
                            $pouvoirsAdjudicateurs .= $libellePereServiceAgentConnecte.''.'<br>';
                        }
                        if ($libelleDivision) {
                            $pouvoirsAdjudicateurs .= $libelleDivision.'<br>';
                        }
                        if ($libelleServiceAgentConnecte) {
                            $pouvoirsAdjudicateurs .= $libelleServiceAgentConnecte.'<br>';
                        }

                        $corpsEmail = str_replace('POUVOIRS_ADJUDICATEURS', $pouvoirsAdjudicateurs, $corpsEmail);
                        $corpsEmail = str_replace('DEFINE_EMAIL', Prado::localize('EMAIL'), $corpsEmail);
                        $corpsEmail = str_replace('DEFINE_TEL', Prado::localize('DEFINE_TEL'), $corpsEmail);
                        $corpsEmail = str_replace('TEXT_FAX', Prado::localize('TEXT_FAX'), $corpsEmail);
                        $corpsEmail = str_replace('VALEUR_EMAIL', $email, $corpsEmail);
                        $corpsEmail = str_replace('VALEUR_TEL', $tel, $corpsEmail);
                        $corpsEmail = str_replace('VALEUR_FAX', $fax, $corpsEmail);

                        if ($adresseFacturation->getFacturationSip()) {
                            $valeurSip = true;
                        }
                    }
                    $corpsEmail = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corpsEmail);
                    //$meilleuresSalutations .= Prado::localize('CORDIALEMENT').'.';
                    //$infosOrganisme .= Prado::localize('BAS_PAGE_MAIL').'';
                    $corpsEmail = str_replace('INFOS_ORGANISME', Prado::localize('BAS_PAGE_MAIL'), $corpsEmail);
                }
                //Fin recupération des informations du mail
                //recuperation de la liste des destinataires annonce press
                $listDestAnnoncePress = (new Press())->getListeDestAnnoncePress($annoncePress->getId(), $annoncePress->getOrganisme(), true);
                if (is_array($listDestAnnoncePress) && count($listDestAnnoncePress)) {
                    foreach ($listDestAnnoncePress as $listDestFromJal) {
                        //$listDestFromJal = Atexo_Publicite_AnnonceJAL::getEmailByIdJAl($oneDest->getId(),$oneDest->getOrganisme());
                        //if($listDestFromJal){
                        //$infosComplementairesObjet = $consultation->getReference().'/'.$avis->getId().'/'.$destinataire->getId().' '.$listDestFromJal->getNom();
                        $infosObjet = '';
                        $infosObjet = str_replace('[organes_de_presse_selectionnees]', $listDestFromJal->getNom(), $annoncePress->getObjet());
                        $corpsEmail2 = str_replace('VALEUR_ORGANE_PRESSE', $listDestFromJal->getNom(), $corpsEmail);
                        $corpsEmail2 = str_replace('ORGANE_PRESSE', Prado::localize('ORGANE_PRESSE'), $corpsEmail2);
                        //$infoOrganePresse = Prado::localize('ORGANE_PRESSE').' : '.$listDestFromJal->getNom().'';
                        $corpsEmail2 = str_replace('TEXT_ANNONCE', $annoncePress->getTexte(), $corpsEmail2);
                        //$corpsMessage = $annoncePress->getTexte().'';
                        //$corpsMessage .= $infoOrganePresse;
                        //$corpsMessage .= $newTextMail;
                        //if($valeurSip) {
                        //$corpsMessage .= $infoFacturation;
                        //}
                        //$corpsMessage .= $meilleuresSalutations;
                        //$corpsMessage .= $infosOrganisme;

                        if ($validePJ) {
                            (new Atexo_Message())->mailWithAttachement($from, $listDestFromJal->getEmail(), $infosObjet, $corpsEmail2, $cc, $code, $fileName, '', true);
                            $envoiMail = true;
                        } else {
                            Atexo_Message::simpleMail($from, $listDestFromJal->getEmail(), $infosObjet, $corpsEmail2, $cc, '', '', false, true);
                            $envoiMail = true;
                        }
                        //}
                    }
                }
            }
            if (is_file($zipPj)) {
                if (!unlink($zipPj)) {
                    $logger->error('Impossible de supprimer le fichier : '.$zipPj.' fonction SendToPress');
                }
                Atexo_Util::removeDirectoryRecursively($dossier);
            }
            if ($envoiMail) {
                //Mise à jour du statut du destinataire
                $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_PRESSE_ENVOYE'));
                $datePublication = date('Y-m-d H:i:s');
                $destinataire->setDatePublication($datePublication);
                $destinataire->save($connexionCom);

                return $datePublication;
            } else {
                return false;
            }
        }

        return false;
    }

    public static function publicateSimap2($avis, $destinataire, $consultation, $datePublicationDest, $connexionCom)
    {
        if (!$datePublicationDest) {//Le cas où pas de destinataire Opoce
            $datePublicationDest = date('Y-m-d H:i:s');
        }
        $return = false;
        $publicationAvisPortail = false;
        $miseAjourSupport = false;
        $miseAjourConsultation = false;
        $nouvelleConsultation = false;
        if (!$consultation->getDateMiseEnLigneCalcule() || '0000-00-00 00:00:00' == $consultation->getDateMiseEnLigneCalcule()) {
            $nouvelleConsultation = true;
        }

        //Début regénération de l'avis portail avec la date de publication
        $avisPortail = (new Atexo_Publicite_Avis())->retreiveAvisById($avis->getIdAvisPortail(), $connexionCom, $avis->getOrganisme());
        if ($avisPortail instanceof CommonAVIS) {
            $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avis->getTypeAvis());
            if ($modele) {
                $tableauContenuGeneration = (new Atexo_Publicite_AvisPub())
                    ->genererAvisPressePortail($modele, $connexionCom, $avis->getId(), $consultation, 'fr', true, $datePublicationDest);
                if ($tableauContenuGeneration[0]) {//Le fichier avis portail est généré
                    $avisPortail->setAvis($tableauContenuGeneration[0]);
                    $avisPortail->setDatePub(date('Y-m-d'));
                    if ($avisPortail->getStatut() != Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
                        $avisPortail->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                        $avisPortail->save($connexionCom);
                        $publicationAvisPortail = true;
                    }
                }
            }
        }
        //Fin regénération de l'avis portail avec la date de publication

        //Mise à jour du statut du destinataire
        $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_PORTAIL_PUBLIE'));
        $datePublication = date('Y-m-d H:i:s');
        $destinataire->setDatePublication($datePublication);
        if ($destinataire->save($connexionCom)) {
            $miseAjourSupport = true;
        }
        //mise à jour de la date mise en ligne calcule
        $updateDateMiseEnligneCalcule = max($datePublicationDest, $consultation->getDatevalidation());
        if (!$consultation->getDateMiseEnLigneCalcule() || '0000-00-00 00:00:00' == $consultation->getDateMiseEnLigneCalcule()) {
            $consultation->setDateMiseEnLigneCalcule($updateDateMiseEnligneCalcule);
            if ($consultation->save($connexionCom)) {
                $miseAjourConsultation = true;
            }
        }
        if (($publicationAvisPortail && $miseAjourSupport && !$nouvelleConsultation && !$miseAjourConsultation)
            || ($publicationAvisPortail && $miseAjourSupport && $nouvelleConsultation && $miseAjourConsultation)) {
            $return = true;
        } else {
            $return = false;
        }

        return $return;
    }

    /*
     * Permet de mettre à jour le statut de l'avis de publicite après la publication et regenerer l'avis presse et portail. Egalement renseigner l'historique
    */
    public static function miseAJourStatutAvisApresPublication(CommonAvisPub $avis, $connexion)
    {
        //Mise à jour du statut global de l'avis
        $avis->setStatut(Atexo_Config::getParameter('STATUT_AVIS_ENVOYE'));
        $avis->setDatePublication(date('Y-m-d H:i:s'));
        $avis->save($connexion);
        //Mise à jour de l'historique de l'avis
        (new Atexo_Publicite_AvisPub())->createHistorique($avis->getOrganisme(), $avis->getId(), date('Y-m-d H:i:s'), Atexo_Config::getParameter('STATUT_AVIS_ENVOYE'), $connexion, null, '0', true);
    }

    public static function publicationAvisTypeTED($avis, $destinataireOpoce)
    {
        $connexion = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $statut = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE');
            $datePublication = date('Y-m-d H:i:s');
            $connexion->beginTransaction();
            $avisOpoce = (new Atexo_Publicite_Avis())->retreiveAvisByIdAndRefCons($avis->getIdAvisPdfOpoce(), $avis->getConsultationId(), $avis->getOrganisme(), $connexion);
            if ($avisOpoce instanceof CommonAVIS && $avisOpoce->getType() == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                $avisOpoce->setDatePub(date('Y-m-d'));
                $avisOpoce->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                $avisOpoce->save($connexion);
            }
            $destinataireOpoce->setEtat($statut);
            $destinataireOpoce->setDatePublication($datePublication);
            $destinataireOpoce->save($connexion);
            (new Atexo_Publicite_AvisPub())->createHistorique($destinataireOpoce->getOrganisme(), $destinataireOpoce->getIdAvis(), $datePublication, $statut, $connexion, null, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
            $connexion->commit();

            return $datePublication;
        } catch (Exception $e) {
            $connexion->rollBack();
            $log = "\n\n";
            $log .= "\n\t____________________________________\n";
            $log .= "\n\n Envoi des avis à OPOCE, PRESSE, SIMAP2 <br/>";
            $log .= "\n\n";
            $log .= "\n\t____________________________________\n";
            $log .= $e->getMessage();
            //Envoie de mail
            $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_PUB').'envoiPub.log';
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_PUB'), $filePath, $log, 'a');
            //throw $e;
        }
    }
}
