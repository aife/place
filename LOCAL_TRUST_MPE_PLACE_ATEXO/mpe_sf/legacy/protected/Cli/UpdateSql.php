<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Mise à jour des bases de données.
 *
 * @category Atexo
 */
class Cli_UpdateSql extends Atexo_Cli
{
    public static function run()
    {
        $sql_script_common = self::getRootPath().'/mpe/protected/var/scripts-sql/maj_COMMON_DB.sql';
        $sql_script_orga = self::getRootPath().'/mpe/protected/var/scripts-sql/maj_Organismes.sql';

        // ---------------------------
        // MAJ SQL dans BDD Commune
        // ---------------------------
        echo 'Mise a jour de la BDD Commune: '.Atexo_Config::getParameter('COMMON_DB')." ($sql_script_common) .\n";
        preg_match_all("/[\n]{1}((UPDATE|ALTER|CREATE|INSERT|REPLACE).*;)[ ]?[\n]{0,1}/Uxs", file_get_contents($sql_script_common), $sql_COMMON);

        foreach ($sql_COMMON[1] as $query) {
            try {
                if (!Atexo_Db::getLinkCommon()->query($query)) {
                    echo "ERROR when executing query in '".Atexo_Config::getParameter('COMMON_DB')."' : ".substr($query, 0, 77)."...\n";
                } else {
                    echo '.';
                }
            } catch (Exception $e) {    //echo $e->getMessage();
                Prado::log('Erreur UpdateSql.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'UpdateSql.php');
            }
        }
        echo "\n";

        preg_match_all("/[\n]{1}((UPDATE|ALTER|CREATE|INSERT|REPLACE).*;)[ ]?[\n]{0,1}/Uxs", file_get_contents($sql_script_orga), $sql_ORGA);
        $query = 'SELECT acronyme FROM Organisme';
        $arrayOrg = [];
        foreach (Atexo_Db::getLinkCommon()->query($query) as $row) {
            $organisme = $row['acronyme'];
            $arrayOrg[] = $organisme;
            echo "\n_________________________________________\nMise a jour de la BDD Organisme: $organisme.\n";
            foreach ($sql_ORGA[1] as $query) {
                try {
                    if (!Atexo_Db::getLinkOrganism($organisme)->query($query)) {
                        echo "ERROR when executing query in '$organisme' : ".substr($query, 0, 77)."...\n";
                    } else {
                        echo '.';
                    }
                } catch (Exception $e) {    //echo $e->getMessage();
                    Prado::log('Erreur UpdateSql.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'UpdateSql.php');
                }
            }
        }
        echo "\n";
        // insertion dans la table Organisme_Service_Metier pour chaque organisme une ligne pour mpe
        foreach ($arrayOrg as $organisme) {
            try {
                $queryTestExist = "SELECT `organisme` FROM `Organisme_Service_Metier` WHERE `organisme` = '".$organisme."'";
                $statement = Atexo_Db::getLinkCommon()->query($queryTestExist);
                if (!($statement->fetch(PDO::FETCH_ASSOC))) {
                    $queryOrgServiceMetier = "INSERT INTO `Organisme_Service_Metier` (`organisme`, `id_service_metier`) VALUES('".$organisme."', 1);";
                    if (!Atexo_Db::getLinkCommon()->query($queryOrgServiceMetier)) {
                        echo "ERROR when executing query in '$organisme' : ".substr($queryOrgServiceMetier, 0, 77)."...\n";
                    } else {
                        echo "\nActivation du service MPE pour l'organisme '$organisme' ...\n";
                    }
                }
            } catch (Exception $e) {    //echo $e->getMessage();
                Prado::log('Erreur UpdateSql.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'UpdateSql.php');
            }
        }
    }
}
