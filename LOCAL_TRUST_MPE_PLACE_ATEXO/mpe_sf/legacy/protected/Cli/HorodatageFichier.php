<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Util;

/**
 * Permet d'horodater un fichier qu'on lui passe en parametre.
 *
 * @param $fileName: le nom du fichier à horodater
 *
 * @return $file: un fichier stocké sur le serveur contenant les date et horodatage
 *
 * @category Atexo
 */
class Cli_HorodatageFichier extends Atexo_Cli
{
    public static function run()
    {
        $arrayParam = $_SERVER['argv'];
        $fileName = $arrayParam[2];
        if ($fileName) {
            $atexoCrypto = new Atexo_Crypto();
            $arrayTimeStampDateFin = $atexoCrypto->timeStampFile($fileName);
            if ($arrayTimeStampDateFin) {
                $fileName = Atexo_Config::getParameter('COMMON_TMP').'/horodatageInterface_MPE_GM.txt';
                Atexo_Util::write_file($fileName, $arrayTimeStampDateFin['untrustedDate'].'#####'.$arrayTimeStampDateFin['horodatage'], 'r+');
            }
        }
    }
}
