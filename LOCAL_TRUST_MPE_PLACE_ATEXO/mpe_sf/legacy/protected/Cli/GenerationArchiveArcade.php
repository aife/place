<?php

namespace Application\Cli;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonArchiveArcade;
use Application\Propel\Mpe\CommonArchiveArcadeQuery;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationArchiveArcadePeer;
use Application\Propel\Mpe\CommonConsultationArchiveArcadeQuery;
use Application\Service\Atexo\Atexo_Consultation;

/**
 * génération des tar.gz pour le système ARCADE.
 *
 * @author Mohamed Wazni <mwa@atexo.com>
 * @copyright Atexo 2019
 *
 * @version 1.0
 */
class Cli_GenerationArchiveArcade extends Atexo_Cli
{
    //taille maximale à ne pas dépasser par défaut 5Go
    const MAX_SIZE_TAR_GZ = '5';

    public static function run()
    {
        $logger = Atexo_LoggerManager::getLogger('archivage');
        try {
            //étape 0 : déterminer la semaine et l'année à traiter
            //par défaut annee courante et semaine courante
            $annee = intval(date('Y'));
            $week = intval(date('W'));
            $maxSizeTarGz = self::MAX_SIZE_TAR_GZ;

            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument = explode('=', $arguments[$i]);
                    $_GET[$argument[0]] = ('' != $argument[1]) ? $argument[1] : true;
                }
            }
            if (isset($_GET['annee'])) {
                $annee = intval($_GET['annee']);
                if (!(($annee >= 2010) && ($annee <= 2099))) {
                    $logger->error("Le paramètre annee n'est pas sur 4 digits et/ou n'est pas un numérique, veuillez renseigner un nombre compris entre 2010 et 2099");
                    echo "Le paramètre annee n'est pas sur 4 digits et/ou n'est pas un numérique, veuillez renseigner un nombre compris entre 2010 et 2099";

                    return;
                }
            }
            if (isset($_GET['week'])) {
                $week = intval($_GET['week']);
                if (!(($week >= 1) && ($week <= 53))) {
                    $logger->error("Le paramètre week n'est pas sur 1 ou 2 digits et/ou n'est pas un numérique, veuillez renseigner un nombre compris entre 1 et 53");
                    echo "Le paramètre week n'est pas sur 1 ou 2 digits et/ou n'est pas un numérique, veuillez renseigner un nombre compris entre 1 et 53";

                    return;
                }
            }
            if (isset($_GET['poidsMaxArchive'])) {
                $maxSizeTarGz = floatval($_GET['poidsMaxArchive']);
                if ($maxSizeTarGz > self::MAX_SIZE_TAR_GZ) {
                    $logger->error('Le paramètre poidsMaxArchive est supérieure à la limite autorisée de : '.self::MAX_SIZE_TAR_GZ.' Go, il ne va pas être pris en compte');
                    echo 'Le paramètre poidsMaxArchive est supérieure à la limite autorisée de : '.self::MAX_SIZE_TAR_GZ.' Go, il ne va pas être pris en compte';
                }
            }
            $acronyme = $_GET['acronyme'];
            if (!isset($acronyme)) {
                $logger->error('Le paramètre acronyme est obligatoire, veuillez le renseigner');
                echo 'Le paramètre acronyme est obligatoire, veuillez le renseigner';

                return;
            }

            $logger->info("Démarrage de la generation de l'archive pour ARCADE, annee : ".$annee.', semaine : '.$week);

            //étape 1 : vérifier si l'entrée pour la semaine et l'année existe
            $archiveArcade = (new Cli_GenerationArchiveArcade())->retrieveArchiveArcade($annee, $week);
            if ($archiveArcade instanceof CommonArchiveArcade) {
                $logger->info("l'archive arcade existe en BD pour l'annee : ".$annee.' et la semaine : '.$week);
                // si existe alors, vérifier qu'elle est terminée (chemin du fichier tar.gz existe sur disque et en BD)
                $cheminFichierTarGz = $archiveArcade->getCheminFichier();
                $tmpArcadeDirectory = (new Cli_GenerationArchiveArcade())->getTmpArcadeDirectory();
                if (null != $cheminFichierTarGz) {
                    // --> oui terminer le script avec un message d'info
                    if (is_file($cheminFichierTarGz)) {
                        $logger->info('le fichier archive Arcade existe et se nomme '.$cheminFichierTarGz."\n le script va s'arrêté car il n'y a rien à faire");
                    } // --> non terminer le script avec une erreur
                    else {
                        $logger->error('le fichier archive Arcade existe en base de donnée et se nomme '.$cheminFichierTarGz.".\n Cependant, le fichier sur le disque : ".$cheminFichierTarGz."n'existe pas. \n Cela est ANORMAL, le script va s'arrêté car il n'y a rien à faire");
                    }
                } else {
                    //le chemin du fichier est null donc l'archive n'a pas été fabriqué, il faut passer à l'étape 3 pour que cela soit fait
                    $logger->info("le fichier archive Arcade n'existe pas en BD, nous passons à l'étape de constitution du fichier du répertoire Arcade puis à la fabrication du tar.gz ");
                    (new Cli_GenerationArchiveArcade())->createTarGzForArcade($archiveArcade, $maxSizeTarGz, $acronyme);
                }
            } else {
                //étape 2 : si n'existe pas créer une entrée vide pour la semaine et l'année à traiter dans la table archive_arcade
                $logger->info("l'archive arcade n'existe pas en BD pour l'annee : ".$annee.' et la semaine : '.$week);
                $archiveArcade = (new Cli_GenerationArchiveArcade())->createArchiveArcade($annee, $week);
                $logger->info("l'archive arcade dont l'identifiant est ".$archiveArcade->getId()." a été créée en base de données pour l'annee : ".$annee.' et la semaine : '.$week);
                $logger->info("nous passons à l'étape de constitution du fichier du répertoire Arcade puis à la fabrication du tar.gz ");
                (new Cli_GenerationArchiveArcade())->createTarGzForArcade($archiveArcade, $maxSizeTarGz, $acronyme);
            }
        } catch (\Exception $e) {
            $logger->error("Une execution est survenue pendant l'exécution du script : ".$e->getMessage());
        }
    }

    /**
     * @param $annee
     * @param $week
     *
     * @return CommonArchiveArcade s'il y a une ligne dans la table correspondant à l'année et la semaine passée en paramètre, false sinon
     */
    private function retrieveArchiveArcade($annee, $week)
    {
        $archiveArcade = null;
        $connexion = (new Cli_GenerationArchiveArcade())->getConnexion();
        $archiveArcadeQuery = new CommonArchiveArcadeQuery();
        $archiveArcadeQuery->filterByAnnee($annee);
        $archiveArcadeQuery->filterByNumSemaine($week);

        return $archiveArcadeQuery->findOne($connexion);
    }

    private function getLogger()
    {
        return Atexo_LoggerManager::getLogger('archivage');
    }

    private function getConnexion()
    {
        return Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
    }

    private function getTmpArcadeDirectory()
    {
        return Atexo_Config::getParameter('CHEMIN_INTERFACE_TMP_ARCADE');
    }

    private function createArchiveArcade($annee, $week)
    {
        $connexion = (new Cli_GenerationArchiveArcade())->getConnexion();
        $archiveArcade = new CommonArchiveArcade();
        $archiveArcade->setAnnee($annee);
        $archiveArcade->setNumSemaine($week);
        $archiveArcade->save($connexion);

        return (new Cli_GenerationArchiveArcade())->retrieveArchiveArcade($annee, $week);
    }

    //étape 3 : lister les consultations archivées de la table consultation_archive_arcade associées à aucune semaine
    //          et pouvant être transmises car seuil non dépassée,
    //          pour chaque consultation, copier l'archive dans le répertoire cible temporaire tar.gz
    //étape 4 : fabriquer le fichier des unités
    //étape 5 : faire un tar.gz du répertoire et mettre à jour la bd table archive_arcade pour indiquer que archive prête à être envoyé
    //étape 6 : supprimer le répertoire temporaire

    /**
     * @param $archiveArcade l'objet archive arcade pour lequel on doit creer l'archive tar.gz
     * @param $maxSizeTarGz la taille maximale à ne pas dépasser pour le poids en Go de l'archive tar.gz
     * @param $acronyme acronyme de l'organisme à traiter
     *
     * @return bool true si les opérations se sont bien déroulées, false sinon
     */
    private function createTarGzForArcade($archiveArcade, $maxSizeTarGz, $acronyme)
    {
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();

        $listConsultationArchiveArcadeAIntegrer = (new Cli_GenerationArchiveArcade())->getListeConsultationsArchivesAIntegrer($maxSizeTarGz);
        //creation du répertoire qui va contenir les fichiers zip pour l'archive tar.gz
        $pathDirArcadeNumSemaine = (new Cli_GenerationArchiveArcade())->getDirectoryTarGz($archiveArcade);
        if (is_dir($pathDirArcadeNumSemaine)) {
            $logger->info('Le répertoire de stockage des archives pour tar.gz existe  : '.$pathDirArcadeNumSemaine);

            $logger->info('Démarrage de la copie des archives');

            foreach ($listConsultationArchiveArcadeAIntegrer as $item) {
                if ((new Cli_GenerationArchiveArcade())->copierArchive($item, $pathDirArcadeNumSemaine)) {
                    $consultationArcheId = $item->getId();
                    $consultationArchiveArcade = (new CommonConsultationArchiveArcadeQuery())->findOneByConsultationArchiveId($consultationArcheId);
                    $consultationArchiveArcade->setArchiveArcadeId($archiveArcade->getId());
                    $consultationArchiveArcade->save((new Cli_GenerationArchiveArcade())->getConnexion());
                    $logger->info("La consultation archive  : ".$archiveArcade->getId()." a été associé avec succès à l'archive Arcade : ".$archiveArcade->getId());
                } else {
                    $logger->error("La consultation archive  : ".$archiveArcade->getId()." n'a été pas été associé à l'archive Arcade : ".$archiveArcade->getId());
                }
            }
            $logger->info('Fin de la copie des archives');

            if ((new Cli_GenerationArchiveArcade())->genererUnitesServices($pathDirArcadeNumSemaine, $acronyme)) {
                $logger->info('Démarrage de la création du fichier tar.gz');
                //A ce stade le répertoire des archives est prêt pour être compressé au format tar.gz
                chdir((new Cli_GenerationArchiveArcade())->getTmpArcadeDirectory().'/');
                system('tar cvfz '.(new Cli_GenerationArchiveArcade())->getFileNameTarGz($archiveArcade).'.tar.gz '.(new Cli_GenerationArchiveArcade())->getFileNameTarGz($archiveArcade));

                //s'il existe et que son poids est conséquent on met à jour la BD pour indiquer le chemin vers le fichier avec son poids en octect
                if (is_file($pathDirArcadeNumSemaine.'.tar.gz')) {
                    $archiveArcade->setPoidsArchive(filesize($pathDirArcadeNumSemaine.'.tar.gz'));
                    $archiveArcade->setCheminFichier($pathDirArcadeNumSemaine.'.tar.gz');
                    $archiveArcade->save((new Cli_GenerationArchiveArcade())->getConnexion());
                    $logger->info("l'archive arcade dont l'identifiant est ".$archiveArcade->getId().' a été mise à jour avec le poids '.$archiveArcade->getPoidsArchive().' et le chemin : '.$archiveArcade->getCheminFichier());
                    $logger->info('Fin de la création du fichier tar.gz');
                    $logger->info('Démarrage de la suppression des fichiers/répertoires temporaires');
                    if ((new Cli_GenerationArchiveArcade())->removeTmpDirOrFile($pathDirArcadeNumSemaine)) {
                        $logger->info('Fin de la suppression des fichiers/répertoires temporaires avec succès');
                    } else {
                        $logger->error('Erreur lors de la suppression des fichiers/répertoires temporaires');
                    }
                } else {
                    $logger->error("La génération du fichier des archives tar.gz va être arrêtée car le fichier n'a pas pu être produit");
                }
            } else {
                $logger->error("La génération du fichier des archives va être arrêtée car le fichier des unités n'a pas pu être produit");
            }
        } else {
            $logger->error("Le répertoire de stockage des archives pour tar.gz n'existe pas : ".$pathDirArcadeNumSemaine."\n Le script va s'arrêter, veuillez vérifier les droits d'écriture sur ce répertoire");
        }
    }

    private function getListeConsultationsArchivesAIntegrer($maxSizeTarGz)
    {
        $limitePoidsTarGz = $maxSizeTarGz * 1024 * 1024 * 1024;
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();
        $consultationArchiveArcade = new CommonConsultationArchiveArcadeQuery();
        $logger->info('la limite de poids est de  '.$limitePoidsTarGz);

        //récupérer toutes les consultations_archive qui ne sont associées à aucune archive arcade
        $listConsultationArchiveArcade = $consultationArchiveArcade->findByArchiveArcadeId(null);
        $logger->info('il y a '.(is_countable($listConsultationArchiveArcade) ? count($listConsultationArchiveArcade) : 0).' consultation_archive qui ne sont associées à aucune semaine/annee');

        $seuil = 0;
        $listConsultationArchiveArcadeAIntegrer = [];
        foreach ($listConsultationArchiveArcade as $item) {
            $consultationArchive = (new CommonConsultationArchiveQuery())->findPk($item->getConsultationArchiveId());
            if(!(($seuil+$consultationArchive->getPoidsArchivage()) > $limitePoidsTarGz)) {
                $logger->info("la limite de poids n'a pas été atteinte, l'archive consultation id ".$consultationArchive->getId()." va être ajoutée au fichier tar.gz en cours de construction");
                $listConsultationArchiveArcadeAIntegrer [] = $consultationArchive;
                $seuil +=$consultationArchive->getPoidsArchivage();
            } else {
                $logger->info("la limite de poids a été atteinte (".$limitePoidsTarGz."), la construction de la liste des archives à envoyer est terminée");
                break;
            }
        }
        $logger->info('il y a '.count($listConsultationArchiveArcadeAIntegrer).' consultations archives qui vont être intégrées au fichier tar.gz pour un poids total en Go de '.$seuil / 1024 / 1024 / 1024);

        return $listConsultationArchiveArcadeAIntegrer;
    }

    /**
     * @param $archiveArcade
     *
     * @return string chemin vers le répertoire de l'archive tar.gz à créer
     */
    private function getDirectoryTarGz($archiveArcade)
    {
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();
        $pathDirArcadeNumSemaine = (new Cli_GenerationArchiveArcade())->getTmpArcadeDirectory().'/'.(new Cli_GenerationArchiveArcade())->getFileNameTarGz($archiveArcade);
        if (!is_dir($pathDirArcadeNumSemaine)) {
            system('mkdir  -p '.escapeshellarg($pathDirArcadeNumSemaine));
            $logger->info('Le répertoire suivant a été créé :'.$pathDirArcadeNumSemaine);
            echo 'Le répertoire suivant a été créé : '.$pathDirArcadeNumSemaine;
        }

        return $pathDirArcadeNumSemaine;
    }

    /**
     * @param $archiveArcade
     *
     * @return string nom de l'archive tar.gz à créer
     */
    private function getFileNameTarGz($archiveArcade)
    {
        $fileNameArcadeNumSemaine = 'Arcade_'.$archiveArcade->getAnnee().'_'.$archiveArcade->getNumSemaine();

        return $fileNameArcadeNumSemaine;
    }

    /**
     * @param $pathDirOrFile chemin vers le répertoire ou le fichier à supprimer
     *
     * @return bool true si la suppression du répertoire ou du fichier a bien été réalisée, false sinon
     */
    private function removeTmpDirOrFile($pathDirOrFile)
    {
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();
        if (is_dir($pathDirOrFile)) {
            system('rm  -rf '.escapeshellarg($pathDirOrFile));
            $logger->info('Le répertoire suivant a été supprimé :'.$pathDirOrFile);
            echo 'Le répertoire suivant a été supprimé : '.$pathDirOrFile;

            return true;
        } elseif (is_file($pathDirOrFile)) {
            system('rm  -f '.escapeshellarg($pathDirOrFile));
            $logger->info('Le fichier suivant a été supprimé :'.$pathDirOrFile);
            echo 'Le fichier suivant a été supprimé : '.$pathDirOrFile;

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $consultationArchive l'objet consultation_archive à copier
     * @param $pathDirArcadeNumSemaine le répertoire de destination
     *
     * @return bool true si l'opération s'est bien déroulée, false sinon
     */
    private function copierArchive($consultationArchive, $pathDirArcadeNumSemaine)
    {
        $cheminAbsolueArchive = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();

        $archiveName = (new Cli_GenerationArchiveArcade())->getArchiveNameForArcade($consultationArchive);
        if (is_file($cheminAbsolueArchive)) {
            $result = copy($cheminAbsolueArchive, $pathDirArcadeNumSemaine.'/'.$archiveName);

            if ($result) {
                $logger->info('La copie du fichier '.$cheminAbsolueArchive.' vers '.$pathDirArcadeNumSemaine.'/'.$archiveName.' a réussi');

                return true;
            } else {
                $logger->info('La copie du fichier '.$cheminAbsolueArchive.' vers '.$pathDirArcadeNumSemaine.'/'.$archiveName.' a échoué');
                $errors = error_get_last();
                echo 'COPY ERROR: '.$errors['type'];
                echo "<br />\n".$errors['message'];

                return false;
            }
        } else {
            $logger->error("L'archive ".$cheminAbsolueArchive." n'existe pas");

            return false;
        }
    }

    /**
     * @param $consultationArchive
     *
     * @return string calcule le nom de l'archive zip à envoyer à Arcade
     *
     * @throws Atexo_Config_Exception
     */
    private  function getArchiveNameForArcade($consultationArchive)
    {
        $consultation = (new Atexo_Consultation())->retreiveConsultationByReferenceAndAcronyme(
            $consultationArchive->getConsultationRef(),
            $consultationArchive->getOrganisme()
        );

        if ($consultation === false) {
            $consultation = (new Atexo_Consultation())->retrieveConsultationById(
                $consultationArchive->getConsultationId()
            );
        }

        if ($consultation->getServiceId() > 0) {
            $service = CommonServicePeer::retrieveByPK($consultation->getServiceId());
            $sigleService = $service->getSigle();
        } else {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($consultationArchive->getOrganisme());
            $sigleService = $organisme->getSigle();
        }

        $id = $consultation->getId();
        if ($consultation->getReference() !== null
            && $consultationArchive->getConsultationRef() === $consultation->getReference()) {
            $id = $consultation->getReference();
        }

        $nomFichier = str_replace(
            '/',
            '_',
            trim(Atexo_Util::OterAccents($sigleService)) . "_archives_" . preg_replace('/[^-~[:alnum:]]/',
                '_',
                Atexo_Util::OterAccents($consultation->getReferenceutilisateur())) . '_' . $id);

        // Identification du nom du fichier on ajoute le champ organisme de rpa pour interface specifique arcade_pmi
        $rpa = (new Atexo_EntityPurchase_RPA())->getResponsabeArchiveByIdService(
            $consultation->getServiceId(),
            $consultationArchive->getOrganisme()
        );

        if (($rpa instanceof CommonRPA) && $rpa->getOrganisme()) {
            $nomFichier = $rpa->getOrganisme().'_'.$nomFichier;
        }

        return $nomFichier.'.zip';
    }

    /**
     * @param $pathDirArcadeNumSemaine chemin de destination du fichier généré
     * @param $acronyme acronyme de l'organisme pour lequel on genère le fichier des unités
     *
     * @return bool
     *
     * @throws PropelException
     */
    private function genererUnitesServices($pathDirArcadeNumSemaine, $acronyme)
    {
        $connexion = (new Cli_GenerationArchiveArcade())->getConnexion();
        $logger = (new Cli_GenerationArchiveArcade())->getLogger();

        $unitesServices = ("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<racine>");
        $cheminFichierUnitesServices = $pathDirArcadeNumSemaine.'/Unites_PLACE_'.date('Ymd').'.xml';
        $critiria = new Criteria();
        $critiria->add(CommonServicePeer::ORGANISME, $acronyme);
        $services = CommonServicePeer::doSelect($critiria, $connexion);

        foreach ($services as $service) {
            $entitePublique = Atexo_EntityPurchase::getPathServiceById($service->getId(), $acronyme, null, true);
            $entitePublique = explode(' - ', $entitePublique);
            $entitePublique = str_replace(' / ', '/', $entitePublique[0]);
            $unitesServices .= ('<unite><identifiant>'.$service->getId().'</identifiant><libelle>'.$service->getLibelle().'</libelle><sigle>'.$entitePublique."</sigle></unite>\n");
        }
        $unitesServices .= ('</racine>');
        Atexo_Util::writeFile($cheminFichierUnitesServices, $unitesServices);
        if (is_file($cheminFichierUnitesServices)) {
            $logger->info('Le fichier des Unités a correctement été généré');

            return true;
        } else {
            $logger->error('Le génération du fichier des Unités a échoué');

            return false;
        }
    }
}
