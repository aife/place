<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;

class Cli_GetPathDic extends Atexo_Cli
{
    public static function run()
    {

        $arguments = $_SERVER['argv'];
        $lots = null;
        $consultationId = null;
        $acronymeOrganisme =  null;
        $nomFichier = null;

        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        for ($i = 2; $i < $nbrArg; ++$i) {
            $args = explode('=', $arguments[$i]);
            if ('lots' == $args[0]) {
               $lots = $args[1];
            }
            if ('consultation_id' == $args[0]) {
                $consultationId = $args[1];
            }
            if ('acronyme_organisme' == $args[0]) {
                $acronymeOrganisme = $args[1];
            }
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultation = CommonConsultationPeer::retrieveByPK($consultationId, $connexionCom);

        if (!$consultation instanceof CommonConsultation) {
            throw new Atexo_Exception('Expected parameter to be either consultation or reference');
        }

        $pathZip = null;
        $numerosLots = explode('#', Atexo_Util::atexoHtmlEntities(base64_decode($lots)));
        $tmpDir = Atexo_Config::getParameter('COMMON_TMP');

        if (Atexo_Module::isEnabled('ArchiveParLot') && $lots && count($numerosLots) > 0) {
            $mode = false;
            foreach ($numerosLots as $oneLot) {
                $tree = new Atexo_Consultation_Tree_Arborescence($consultation->getId(), $connexionCom, $acronymeOrganisme, true, $oneLot);
                $pathZipLot = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $consultation, $acronymeOrganisme, 'archive.xml', true, $oneLot);
                $zip[$oneLot]['path'] = $pathZipLot;
                $zip[$oneLot]['nameFile'] = $tree->getNom();
            }
            $zipFile = $tmpDir.'zipDacArchives'.session_id().time().'.zip';
            foreach ($zip as $numLot => $uneArchive) {
                if (!$pathZip) {
                    $mode = 'CREATE';
                }
                $pathZip = (new Atexo_Zip())->addFile2Zip($zipFile, $uneArchive['path'], $nomFichier . $uneArchive['nameFile'].'.zip', $mode);
            }
            $downloadedName = 'DI_'.$consultation->getReferenceUtilisateur();
        } else {
            $tree = new Atexo_Consultation_Tree_Arborescence($consultation->getId(), $connexionCom, $acronymeOrganisme, true, Atexo_Util::atexoHtmlEntities(base64_decode($lots)));
            $pathZip = (new Atexo_Consultation_Archive())->generateZipArborescence($tree, $tmpDir, $consultation, $acronymeOrganisme, 'archive.xml', true, base64_decode($lots));
            $downloadedName = $nomFichier . $consultation->getReferenceUtilisateur(true);
        }

        echo '#'. $pathZip . '#';
    }
}
