<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonChorusNumeroSequence;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use AtexoCrypto\Dto\Fichier;
use DateTime;
use Exception;

/**
 * Class Cli_EnvoyerFluxChorus : CRON d'envoi HTTP des flux vers CHORUS.
 *
 * @category Atexo
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Cli_EnvoyerFluxChorus extends Atexo_Cli
{
    private static ?int $nbFlux = null;
    private static ?int $codeRetour = null;

    /**
     * Lancement du CLI.
     */
    public static function run($return = false)
    {
        $statutCronEnvoiFlux = null;
        try {
            $params = [];
            $params['debutExecution'] = new DateTime('now');
            $params['service'] = 'Chorus';
            $params['nomBatch'] = 'Cli_EnvoyerFluxChorus';
            self::oversight($params);

            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux(">>>>>>>>>>>>>>>>>>> DEBUT lancement routine d'envoi HTTP des flux vers CHORUS (Cli_EnvoyerFluxChorus) <<<<<<<<<<<<<<<<<<<".PHP_EOL);
            $statutCronEnvoiFlux = 1;
        } catch (Exception $exceptionOversight) {
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        //Recuperation des echanges CHORUS pour l'envoi HTTP des flux
        $chorusEchangeQuery = new CommonChorusEchangeQuery();
        $listeEchanges = $chorusEchangeQuery->recupererEchangesChorusAEnvoyer();
        self::$nbFlux = count($listeEchanges);
        if (is_array($listeEchanges) && !empty($listeEchanges)) {
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Nombre de flux a envoyer : '.count($listeEchanges).PHP_EOL);
            foreach ($listeEchanges as $echange) {
                self::$codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
                $erreursCronEnvoiFluxEchange = [];
                try {
                    if ($echange instanceof CommonChorusEchange) {
                        $params['informationMetier'] = json_encode(['id_echange' => $echange->getId(), 'organisme' => $echange->getOrganisme(), 'id_contrat' => $echange->getUuidExterneExec()], JSON_THROW_ON_ERROR);
                        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Debut traitement flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec().' *** Type de flux = '.$echange->getLibelleTypeFlux().' ***]');

                        //Debut transaction
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $connexion->beginTransaction();

                        //Si pas de flux ou flux invalide : changer le statut a "STATUT_ECHANGE_CHORUS_FLUX_A_GENERER" et logger les details
                        if (empty($echange->getTmpFileName()) || !is_file(realpath($echange->getTmpFileName()))) {
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiTentativeRegenerationFlux('DEBUT changement statut du flux CHORUS : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , Flux = '".$echange->getTmpFileName()."']");
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Le flux '".$echange->getTmpFileName()."' n'existe pas ou n'est pas valide : changement de statut en 'STATUT_ECHANGE_CHORUS_FLUX_A_GENERER' ...");

                            $echange->setStatutechange(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));
                            $echange->save($connexion);
                            $connexion->commit();

                            //Detruire la connexion
                            unset($connexion);

                            $raisonChangementStatut = (empty($echange->getTmpFileName())) ? ' *** Flux non genere ***' : ' *** Flux non valide *** (Flux = '.$echange->getTmpFileName().')';
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Le statut du flux '".$echange->getTmpFileName()."' a ete mis a jour avec *** SUCCES *** a 'STATUT_ECHANGE_CHORUS_FLUX_A_GENERER' : ".PHP_EOL."Raison du changement de statut = $raisonChangementStatut ".PHP_EOL.'Voir les details de la generation dans le fichier : interfacePlaceChorusEnvoiTentativeRegenerationFlux.log ');
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Transaction comitee avec *** SUCCES ***  : flux '".$echange->getTmpFileName().PHP_EOL);
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiTentativeRegenerationFlux("Raison du changement de statut = $raisonChangementStatut");
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiTentativeRegenerationFlux("Le statut du flux a ete mis a jour avec *** SUCCES *** a 'STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'");
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiTentativeRegenerationFlux('Transaction comitee avec *** SUCCES *** ');
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiTentativeRegenerationFlux('FIN changement statut du flux CHORUS : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , Flux = '".$echange->getTmpFileName()."']".PHP_EOL.PHP_EOL);

                            $params['code'] = Atexo_Oversight_Oversight::$codeErrorSomeFilesOrFoldersAreNotFound;
                            $params['oversight'] = self::oversight($params);
                            --self::$nbFlux;

                            continue;
                        }

                        $atexoChorusEchange = new Atexo_Chorus_Echange();
                        $derniereSequenceAvantIncrement = $atexoChorusEchange->getLastSequenceFlux($echange->getLibelleTypeFlux());
                        $dernierNumeroSequenceAvantIncrement = ($derniereSequenceAvantIncrement instanceof CommonChorusNumeroSequence) ? $derniereSequenceAvantIncrement->getNumero() : '';
                        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Dernier numero de sequence avant generation d'un nouveau : ".($dernierNumeroSequenceAvantIncrement));
                        $nomFluxChorus = (new Atexo_Chorus_Echange())->construireNomFluxChorus($echange, $connexion, $erreursCronEnvoiFluxEchange);

                        if (false !== $nomFluxChorus && !empty($nomFluxChorus)) {
                            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Le nom du flux CHORUS est *** VALIDE *** : $nomFluxChorus");

                            $contenuFlux = self::getContenuFluxTemporaire($echange, $erreursCronEnvoiFluxEchange);
                            if (false !== $contenuFlux) {
                                //Envoi HTTP du flux
                                $resultatEnvoi = (new Atexo_Chorus_Echange())->sendFluxToCHorus($nomFluxChorus, $contenuFlux, $erreursCronEnvoiFluxEchange);

                                if (true === $resultatEnvoi) {
                                    //Modification statut echange
                                    $typeFichier = ($echange->getTypeFluxAEnvoyer() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR')) ? Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS') : Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS');
                                    $returnSave = (new Atexo_Chorus_Echange())->saveEchange($echange, $nomFluxChorus, Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER'), Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'), $typeFichier, Atexo_CurrentUser::getIdAgentConnected(), Atexo_CurrentUser::getFirstNameAgentConnected(), Atexo_CurrentUser::getLastName(), null, $connexion);
                                    if (false !== $returnSave) {
                                        //Desactivation message rejet
                                        $returnDesactRejet = (new Atexo_Chorus_Echange())->desactiverMessageRejetEchange($echange, $connexion, Atexo_LoggerManager::getLogger('chorus'), $erreursCronEnvoiFluxEchange);
                                        if (false !== $returnDesactRejet) {
                                            $connexion->commit();
                                            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Transaction comitee avec *** SUCCES ***  [id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , fichier = '".$echange->getTmpFileName()."']");

                                            $params['nbFlux'] = 1;
                                            if (is_file(realpath($echange->getTmpFileName()))) {
                                                $params['poids'] = filesize($echange->getTmpFileName());
                                            }

                                            //suppression du fichier temporaire
                                            self::suppressionFichierTemporaire($echange, $erreursCronEnvoiFluxEchange);
                                        } else {
                                            $connexion->rollBack();
                                            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux(' *** Transaction annulee (RollBack): *** La desactivation du message de rejet a echouee : [id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , fichier = '".$echange->getTmpFileName()."']");
                                            self::decrementerNumeroSequence($echange->getLibelleTypeFlux(), $dernierNumeroSequenceAvantIncrement);
                                            self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                        }
                                    } else {
                                        $connexion->rollBack();
                                        (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux(" *** Transaction annulee (RollBack): *** la sauvegarde des infos dans 'Chorus_Echange' et 'Chorus_Noms_Fichiers' a echouee : [id_echange = ".$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , fichier = '".$echange->getTmpFileName()."']");
                                        self::decrementerNumeroSequence($echange->getLibelleTypeFlux(), $dernierNumeroSequenceAvantIncrement);
                                        self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                    }
                                } else {
                                    $connexion->rollBack();
                                    (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux(" *** Transaction annulee (RollBack): *** l'envoi du fichier '".$echange->getTmpFileName()."' a echoue : [id_echange = ".$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec()." , fichier = '".$echange->getTmpFileName()."']");
                                    self::decrementerNumeroSequence($echange->getLibelleTypeFlux(), $dernierNumeroSequenceAvantIncrement);
                                    self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
                                }
                            } else {
                                $connexion->rollBack();
                                $erreur = 'Le flux temporaire est *** VIDE *** ou *** NON VALIDE *** . Fichier = '.$echange->getTmpFileName();
                                $erreursCronEnvoiFluxEchange[] = $erreur;
                                (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);
                                self::decrementerNumeroSequence($echange->getLibelleTypeFlux(), $dernierNumeroSequenceAvantIncrement);

                                self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                            }
                        } else {
                            $connexion->rollBack();
                            $erreur = "Le nom du flux CHORUS n'est *** PAS VALIDE *** : $nomFluxChorus";
                            $erreursCronEnvoiFluxEchange[] = $erreur;
                            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);
                            self::decrementerNumeroSequence($echange->getLibelleTypeFlux(), $dernierNumeroSequenceAvantIncrement);
                            self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                        }

                        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Fin traitement flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getUuidExterneExec().' *** Type de flux = '.$echange->getLibelleTypeFlux().' ***]'.PHP_EOL);

                        //Detruire la connexion
                        unset($connexion);
                    } else {
                        $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
                        $erreursCronEnvoiFluxEchange[] = $erreur;
                        (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);
                        self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    }
                } catch (Exception $e) {
                    $erreur = "Erreur lors de l'envoi HTTP des flux vers Chorus.".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_EnvoyerFluxChorus::run()';
                    $erreursCronEnvoiFluxEchange[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                    $params['resultat'] = $erreur;
                    self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
                }

                if (!empty($erreursCronEnvoiFluxEchange)) {
                    $statutCronEnvoiFlux = 0;
                    //Envoyer mail au support
                    (new Atexo_Chorus_Util())->envoyerMailSupportChorus("Erreur lors de l'envoi du flux Chorus (routine : Cli_EnvoyerFluxChorus)", print_r($erreursCronEnvoiFluxEchange, true).PHP_EOL.PHP_EOL.'Echange: '.PHP_EOL.print_r($echange, true));
                }
                $params['code'] = self::$codeRetour;
                self::oversight($params);
                --self::$nbFlux;
            }//END foreach
        } else {
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Aucun flux a envoyer.'.PHP_EOL);
        }

        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux(">>>>>>>>>>>>>>>>>>> FIN lancement routine d'envoi HTTP des flux vers CHORUS (Cli_EnvoyerFluxChorus) : *** STATUT = '$statutCronEnvoiFlux' *** <<<<<<<<<<<<<<<<<<<".PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL);

        if ($return) {
            return $statutCronEnvoiFlux;
        } else {
            echo $statutCronEnvoiFlux;
        }
    }

    /**
     * Permet de recuperer le contenu du flux temporaire.
     *
     * @param CommonChorusEchange $echange          : objet echange Chorus
     * @param array               $erreursEnvoiFlux : liste des erreurs d'envoi de flux
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function getContenuFluxTemporaire($echange, &$erreursEnvoiFlux = null)
    {
        if ($echange instanceof CommonChorusEchange) {
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Debut recuperation du contenu du flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' type de flux = '.$echange->getLibelleTypeFlux().']');
            try {
                if (is_file($echange->getTmpFileName())) {
                    $contenuFlux = file_get_contents(realpath($echange->getTmpFileName()));
                    if (!empty($contenuFlux)) {
                        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Le flux temporaire est valide et non vide. Fichier = '.$echange->getTmpFileName());

                        return $contenuFlux;
                    }
                    $erreur = 'Le flux temporaire est *** VIDE *** . Fichier = '.$echange->getTmpFileName();
                    $erreursEnvoiFlux[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                    return false;
                } else {
                    $erreur = "Le fichier a envoyer n'est pas valide. Fichier = ".$echange->getTmpFileName();
                    $erreursEnvoiFlux[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                    return false;
                }
            } catch (Exception $e) {
                $erreur = "Erreur lors de la recuperation du contenu du flux CHORUS : Chemin fichier = '".$echange->getTmpFileName()."'".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_EnvoyerFluxChorus::getContenuFlux';
                $erreursEnvoiFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                return false;
            }
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Fin recuperation du contenu du flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' type de flux = '.$echange->getLibelleTypeFlux().']');
        } else {
            $erreur = 'Recuperation du contenu du flux. '.PHP_EOL."L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

            return false;
        }
    }

    /**
     * Permet de supprimer le fichier temporaire sur le serveur.
     *
     * @param CommonChorusEchange $echange          : objet chorus echange
     * @param array               $erreursEnvoiFlux : liste des erreurs d'envoi de flux
     *
     * @return bool : true en cas de succes, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function suppressionFichierTemporaire($echange, &$erreursEnvoiFlux = null)
    {
        if ($echange instanceof CommonChorusEchange) {
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Debut suppression du fichier temporaire : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' Chemin fichier = '.$echange->getTmpFileName().']');
            try {
                if (is_file($echange->getTmpFileName())) {
                    if (unlink(realpath($echange->getTmpFileName()))) {
                        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Le fichier '".$echange->getTmpFileName()."' a ete supprime sur le serveur avec *** SUCCES ***");

                        return true;
                    } else {
                        $erreur = "Une erreur est survenue lors de la suppression du fichier '".$echange->getTmpFileName()."' : Id echange = ".$echange->getId().' , Org = '.$echange->getOrganisme();
                        $erreursEnvoiFlux[] = $erreur;
                        (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                        return false;
                    }
                } else {
                    (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Le fichier '".$echange->getTmpFileName()."' n'existe pas sur le serveur ou n'est pas valide.");
                }
            } catch (Exception $e) {
                $erreur = 'Erreur lors de la suppression du fichier temporaire : Id echange = '.$echange->getId().' , Org = '.$echange->getOrganisme()." , Chemin fichier = '".$echange->getTmpFileName()."'".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_EnvoyerFluxChorus::suppressionFichierTemporaire';
                $erreursEnvoiFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                return false;
            }
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Fin suppression du fichier temporaire : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' Chemin fichier = '.$echange->getTmpFileName().']');
        } else {
            $erreur = 'Suppression du fichier temporaire sur le serveur. '.PHP_EOL."L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

            return false;
        }
    }

    /**
     * Permet de decrementer le numero de sequence.
     *
     * @param string    $typeFlux             type de flux
     * @param string    $numeroSequenceBefore numero de sequence avant l'increment
     * @param PropelPDO $connexion            connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2016
     *
     * @copyright Atexo 2017
     */
    public static function decrementerNumeroSequence($typeFlux, $numeroSequenceBefore, $connexion = null, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        try {
            $atexoChorusEchange = new Atexo_Chorus_Echange();
            $sequenceAfter = $atexoChorusEchange->getLastSequenceFlux($typeFlux);
            $numeroSequenceAfter = ($sequenceAfter instanceof CommonChorusNumeroSequence) ? $sequenceAfter->getNumero() : '';

            if (0 != strcmp($numeroSequenceBefore, $numeroSequenceAfter) && (int) $numeroSequenceAfter > 0) {
                if (empty($connexion)) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                }
                --$numeroSequenceAfter;
                $sequenceAfter->setNumero($numeroSequenceAfter);
                $sequenceAfter->save($connexion);

                $logger->info('Numero de sequence decremente avec SUCCES via ma methode Cli_EnvoyerFluxChorus::decrementerNumeroSequence. Dernier numero de sequence = '.$sequenceAfter->getNumero());
            }
        } catch (Exception $e) {
            $logger->error('Erreur decrementation du numero de sequence.'.PHP_EOL.'Error : '.PHP_EOL.$e->getMessage().PHP_EOL.'Trace : '.PHP_EOL.$e->getTraceAsString());
        }
    }
}
