<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Exception;

/*
 * Created on 27 juin 2013
 *
 * @author César BOULET <cesar.boulet@atexo.com>
 * @package
 */
class Cli_GenerationEditionSUB extends Atexo_Cli
{
    public static function run()
    {
        echo "\n-> Start demande générations éditions SUB. \n";
        if (self::startLancementUniqueCli(self::class)) {
            try {
                $listEdition = (new Atexo_FormulaireSub_Edition())->recupererEditionsAGenerer();
                if ((is_countable($listEdition) ? count($listEdition) : 0) > 0) {
                    try {
                        foreach ($listEdition as $Edition) {
                            //Génération de l'édition
                            (new Atexo_FormulaireSub_Edition())->genererEdition($Edition);
                            echo '.';
                        }
                    } catch (Exception $e) {
                        self::stopLancementUniqueCli(self::class);
                        echo '-> erreur : '.$e->getMessage()."\n";
                    }
                }
                self::stopLancementUniqueCli(self::class);
                echo "\n-> done. \n";
            } catch (Exception $e) {
                self::stopLancementUniqueCli(self::class);
                echo "\n-> Error. \n".$e->getMessage();
            }
        }
        echo "\n-> Fin demande générations éditions SUB. \n";
    }
}
