<?php

namespace Application\Cli;

/**
 * Copy PROPEL classes in local directory.
 */
class PropelPostGeneratorTools
{
    public static function copyOnlyOurModifiedClass($nomTable, $renamed = false, $all = false)
    {
        $propelClassesDir = __DIR__ . '/../Library/Propel/generator/MPE/build/classes/Application/Propel/Mpe';

        if ($renamed && 'Entreprise' != $nomTable) {
            $nomTable = 'Common' . str_replace('_', '', ucwords($nomTable, '_'));
        } else {
            $nomTable = str_replace('_', '', ucwords($nomTable, '_'));
        }

        $classOm1 = 'Base' . $nomTable . '.php';
        $classOm2 = 'Base' . $nomTable . 'Peer.php';
        $classOm3 = 'Base' . $nomTable . 'Query.php';
        $classMap = $nomTable . 'TableMap.php';
        $classMpe1 = $nomTable . '.php';
        $classMpe2 = $nomTable . 'Peer.php';
        $classMpe3 = $nomTable . 'Query.php';

        $fileContent = self::correctUse(file_get_contents($propelClassesDir . '/om/' . $classOm1));
        file_put_contents(__DIR__ . '/../Propel/Mpe/Om/' . $classOm1, $fileContent);

        $fileContent = self::correctUse(file_get_contents($propelClassesDir . '/om/' . $classOm2));
        file_put_contents(__DIR__ . '/../Propel/Mpe/Om/' . $classOm2, $fileContent);

        $fileContent = self::correctUse(file_get_contents($propelClassesDir . '/om/' . $classOm3));
        file_put_contents(__DIR__ . '/../Propel/Mpe/Om/' . $classOm3, $fileContent);

        $fileContent = self::correctUse(file_get_contents($propelClassesDir . '/map/' . $classMap));
        file_put_contents(__DIR__ . '/../Propel/Mpe/Map/' . $classMap, $fileContent);

        if ($all) {
            system('cp -f ' . $propelClassesDir . '/' . $classMpe1 . ' ' . __DIR__ . '/../Propel/Mpe/');
            system('cp -f ' . $propelClassesDir . '/' . $classMpe2 . ' ' . __DIR__ . '/../Propel/Mpe/');
            system('cp -f ' . $propelClassesDir . '/' . $classMpe3 . ' ' . __DIR__ . '/../Propel/Mpe/');
        }
    }

    public static function correctUse($file)
    {
        $arraySearch = [
            'use \BasePeer;',
            'use \Criteria;',
            'use \Propel;',
            'use \PropelException;',
            'use \PropelPDO;',
            'use \ModelCriteria;',
            'use \PropelObjectCollection;',
            'use \BaseObject;',
            'use \Persistent;',
            'use \ModelJoin;',
            'use \PropelCollection;',
            'use \PropelDateTime;',
            'use \RelationMap;',
            'use \TableMap;',
        ];
        $arrayReplace = [
            'use Application\Library\Propel\Util\BasePeer;',
            'use Application\Library\Propel\Query\Criteria;',
            'use Application\Library\Propel\Propel;',
            'use Application\Library\Propel\Exception\PropelException;',
            'use Application\Library\Propel\Connection\PropelPDO;',
            'use Application\Library\Propel\Query\ModelCriteria;',
            'use Application\Library\Propel\Collection\PropelObjectCollection;',
            'use Application\Library\Propel\Om\BaseObject;',
            'use Application\Library\Propel\Om\Persistent;',
            'use Application\Library\Propel\Query\ModelJoin;',
            'use Application\Library\Propel\Collection\PropelCollection;',
            'use Application\Library\Propel\Util\PropelDateTime;',
            'use Application\Library\Propel\Map\RelationMap;',
            'use Application\Library\Propel\Map\TableMap;',
        ];

        return str_replace($arraySearch, $arrayReplace, $file);
    }
}
