<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInvitationConsultationTransverse;
use Application\Propel\Mpe\CommonInvitationConsultationTransversePeer;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Exception;

/**
 * Mise à jour des alertes des consultations.
 *
 * @category Atexo
 */
class Cli_RedressementEntiteEligible extends Atexo_Cli
{
    public static function run()
    {
        $connexion = null;
        $start = 'début de lancement du script de redressement des entités eligibles  '.date('Y-m-d H:i:s');
        $logger = Atexo_LoggerManager::getLogger('cli');
        $logger->info($start);
        echo $start."\n";
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            $logger->info("La recuperation des objets InvitationConsTransverseret dont l'id contrat est null");
            $listeInvitationCons = (new Atexo_Consultation_InvitationConsTransverse())->retrieveInvitationConsultationTransverseByIdContrat(null, $connexion);
            $idsToDelete = [];
            if (is_array($listeInvitationCons) && count($listeInvitationCons)) {
                $typesContratACSad = [Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE'), Atexo_Config::getParameter('TYPE_CONTRAT_SAD')];
                foreach ($listeInvitationCons as $invConstTransvers) {
                    if ($invConstTransvers instanceof CommonInvitationConsultationTransverse) {
                        $consultationId = $invConstTransvers->getConsultationId();
                        $organisme = $invConstTransvers->getOrganismeEmetteur();
                        $lot = $invConstTransvers->getLot();
                        $logger->info('La recuperation des contrats AC et SAD dont la reference = '.$consultationId.' organisme = '.$organisme.' lot = '.$lot);
                        $contrats = (new Atexo_Consultation_Contrat())->getContratByRefConsEtLot($consultationId, $organisme, [$lot], $typesContratACSad, $connexion);
                        if (is_array($contrats) && count($contrats)) {
                            $logger->info('Le nombre des contrats à traiter = '.count($contrats));
                            foreach ($contrats as $oneContrat) {
                                $logger->info("L'insertion de l'objet InvitationConsTransverseret pour id contrat =  ".$oneContrat->getIdContratTitulaire());
                                $newInvConstTransvers = new CommonInvitationConsultationTransverse();
                                $invConstTransvers->copyInto($newInvConstTransvers);
                                $logger->info("L'enregistrement du nouveau objet InvitationConsTransverseret ");
                                $newInvConstTransvers->setIdContratTitulaire($oneContrat->getIdContratTitulaire());
                                $newInvConstTransvers->save($connexion);

                                $contratMulti = $oneContrat->getCommonTContratMulti();
                                if ($contratMulti instanceof CommonTContratMulti) {
                                    $contratMultiToAdd = $contratMulti;
                                }
                            }

                            if ($contratMultiToAdd instanceof CommonTContratMulti) {
                                $newInvConstTransvers = new CommonInvitationConsultationTransverse();
                                $invConstTransvers->copyInto($newInvConstTransvers);
                                $logger->info("L'enregistrement du nouveau objet InvitationConsTransverseret ");
                                $newInvConstTransvers->setIdContratTitulaire($contratMultiToAdd->getIdContratTitulaire());
                                $newInvConstTransvers->save($connexion);
                            }
                            $logger->info("L'id de l'objet InvitationConsTransverseret à supprimer  est  ".$invConstTransvers->getId());
                            $idsToDelete[] = $invConstTransvers->getId();
                        } else {
                            $logger->info('Auccun contrat pour cet objet ');
                        }
                    }
                }
            }
            if (is_array($idsToDelete) && count($idsToDelete)) {
                $logger->info('La suppression des objet  InvitationConsTransverseret dont id = '.print_r($idsToDelete, true));
                CommonInvitationConsultationTransversePeer::doDelete($idsToDelete, $connexion);
            }
            $connexion->commit();
        } catch (Exception $e) {
            echo 'Erreur => '.$e->getMessage();
            $connexion->rollBack();
            $logger->error('Erreur => '.$e->getMessage()." \n ".$e->getTraceAsString());
        }

        $fin = ' Fin de lancement du script de redressement des entités eligibles  '.date('Y-m-d H:i:s');
        echo $fin."\n";
        $logger->info($fin);
        self::Display('Done');
    }
}
