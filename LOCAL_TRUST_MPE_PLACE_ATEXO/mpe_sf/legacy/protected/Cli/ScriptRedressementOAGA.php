<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Mise à jour des alertes metiers.
 *
 * @category Atexo
 */
class Cli_ScriptRedressementOAGA extends Atexo_Cli
{
    public static function run()
    {
        $filePath = 'scriptRedressementOAGA.log';

        $log = '=>Debut de lancement du cron ScriptRedressementOAGA '.date('Y-m-d H:i:s')." \r\n";
        $requete = 'select DISTINCT C.organisme,C.id_oa,C.code,C.id_service ,C.actif from Chorus_groupement_achat as C
INNER JOIn Chorus_groupement_achat as C2 on C.organisme=C2.organisme AND C.id_oa=C2.id_oa AND C.code=C2.code AND C.id_service= C2.id_service and C.id!=C2.id
ORDER BY C.code ASC';

        $log .= '=>Execution de la requete => '.$requete." \r\n";
        $statement = Atexo_Db::getLinkCommon(false)->query($requete);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $log .= '=>La recuperation du dernier element dont (organiseme='.$row['organisme'].', idService='.$row['id_service'].' , idOa='.$row['id_oa'].' , code='.$row['code'].") \r\n";
            $requetNbrElment = "select * from Chorus_groupement_achat as C where C.organisme='".$row['organisme']."' AND C.id_oa=".$row['id_oa']." AND C.code='".$row['code']."' AND C.id_service=".$row['id_service'].' ;';
            $statementNbrElment = Atexo_Db::getLinkCommon(false)->query($requetNbrElment);
            $rowcount = (int) $statementNbrElment->rowCount();
            $log .= "=> nombre d'elements doublon : ".($rowcount)." \r\n";
            $requeteGaTo = "select C.id from Chorus_groupement_achat as C WHERE C.organisme='".$row['organisme']."' AND C.id_oa=".$row['id_oa']." AND C.code='".$row['code']."' AND C.id_service=".$row['id_service'].' order by C.id ASC LIMIT 1,'.($rowcount - 1).';';
            $log .= '=> requete pour recuperer le ga à modifier : '.$requeteGaTo." \r\n";
            $statement2 = Atexo_Db::getLinkCommon(false)->query($requeteGaTo);
            $log .= "=>Il y a des ligne a modifier  \r\n";
            //$connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            while ($row2 = $statement2->fetch(PDO::FETCH_ASSOC)) {
                $requeteGaToUP = "UPDATE Chorus_groupement_achat SET actif='0' where id= ".$row2['id'];
                $log .= '=>requete de mise à jour :   '.$requeteGaToUP." \r\n";
                if (!Atexo_Db::getLinkCommon()->query($requeteGaToUP)) {
                    $log .= 'ERROR when executing query in '.$requeteGaToUP."...\r\n";
                } else {
                    $log .= "=>OK  \r\n";
                }
            }
        }
        echo "\r\n Log enregistre   : ".Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath." \r\n";
        $log .= '=>Fin de lancement du cron ScriptRedressementOAGA '.date('Y-m-d H:i:s');
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
    }
}
