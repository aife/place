<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonJustificatifs;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Importer les document du coffre fort.
 *
 * @category Atexo
 */
class Cli_ImportDocCoffreFort extends Atexo_Cli
{
    public static function run()
    {
        echo "\t\t---------- Debut Import------------\r\n";
        $pathTmp = '/tmp/';
        $dirName = 'ImportDocumentCoffreFort';
        $pathZipFile = $pathTmp.$dirName.'.zip';
        $mpePathTmp = Atexo_Config::getParameter('COMMON_TMP').$dirName.'/';
        $dirNamePath = $mpePathTmp;
        $csv = $mpePathTmp.'Export_Documents.csv';
        if (is_dir($mpePathTmp)) {
            Atexo_Util::removeDir(Atexo_Config::getParameter('COMMON_TMP').$dirName.'/');
        }
        $cmd = 'unzip '.$pathZipFile.' -d '.Atexo_Config::getParameter('COMMON_TMP');
        $sys_answer = shell_exec($cmd);
        if (0 == strlen($sys_answer)) {
            echo "\r\n---------- Les fichiers a imoprter sont introuvables ------------\r\n";
        } else {
            $handle = fopen($csv, 'r');
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $row = 0;
            while (($data = fgetcsv($handle, 8000, ';')) !== false) {
                $idEntreprise = self::getIdEntrepriseByIdInitial($data[1]);
                if (!$idEntreprise) {
                    echo "\r\n----------  Entreprise introuvable ----> ".$data[1]." !!! ------------\r\n";
                } else {
                    if (is_file($dirNamePath.$data[0].'_'.$data[1])) {
                        echo "\r\n----------  ".$data[0].'_'.$data[1].'_'.$data[3]." ------------\r\n";
                        //echo $log ; exit;
                        $docFile = new CommonJustificatifs();
                        $atexoBlob = new Atexo_Blob();
                        $docIdBlob = $atexoBlob->insert_blob($data[3], $dirNamePath.$data[0].'_'.$data[1], $connexionCom);
                        $docFile->setJustificatif($docIdBlob);
                        $docFile->setIdDocument($data[0]);
                        $docFile->setIdEntreprise($idEntreprise);
                        $docFile->setIntituleJustificatif($data[3]);
                        $docFile->setNom($data[2]);
                        $docFile->setStatut(0);
                        $docFile->setTaille($data[4]);
                        ////$docFile->setDateFinValidite();
                        $docFile->setVisibleParAgents(1);
                        $docFile->save($connexionCom);
                    //print_r($connexionCom);exit;
                    } else {
                        echo "\r\n---------- Le Fichier ".$data[0].'_'.$data[1].'_'.$data[3]." est Introuvable !!! ------------\r\n";
                        //echo $log ; exit;
                    }
                    ++$row;
                    //echo $log;
                }
            }
            fclose($handle);
            echo "\r\n---------- Game Over !!! ------------\r\n";
            // Atexo_Util::removeDir($dirNamePath);
            Atexo_Util::removeDir(Atexo_Config::getParameter('COMMON_TMP').$dirName.'/');
            @unlink($pathZipFile);
            exit;
        }
    }

    public static function getIdEntrepriseByIdInitial($idEntreprise)
    {
        $entreprise = self::retrieveCompanyByIdInitial($idEntreprise);
        if ($entreprise) {
            return $entreprise->getId();
        } else {
            return false;
        }
    }

    /**
     * retourne un objet entrprise identifié par if initial.
     */
    public function retrieveCompanyByIdInitial($idInitial)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(EntreprisePeer::ID_INITIAL, $idInitial);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);
        if ($company) {
            return $company;
        } else {
            return false;
        }
    }
}
