<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Cron pour exporter une table.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @category Atexo
 */
class Cli_ExportMarches extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        //print_r($arguments); exit;
        if ((is_countable($arguments) ? count($arguments) : 0) < 4) {
            echo "Merci d'ajouter des params dans l'ordre : \r\n";
            echo "\t-> 1- le nom de la table \r\n";
            echo "\t-> 2- le chemin du repertoire de destination (/ a la fin) \r\n";
            echo "\t-> 3- nom du fichier de destination (si rien n'est mis alors le fiche se nomme 'nom de la table'_date('Y_m_d_h_i_s').csv \r\n";
            echo "\t-> 4- annee publication \r\n";
            exit;
        }
        if (is_array($arguments)) {
            $table = $arguments['2'];
            $fileDestination = $arguments['3'];
            $nom_fichier = $arguments['4'];
            $anneePublication = $arguments['5'];
            if (!is_dir($fileDestination)) {
                system('mkdir '.escapeshellarg($fileDestination));
                echo "\r\n-> répertoire suivant créé   ".$fileDestination.'....';
            }

            self::exporterDonneesTablesVersCsv($table, $fileDestination, $nom_fichier, $anneePublication);
        }
    }

    public static function exporterDonneesTablesVersCsv($table, $fileDestination, $nom_fichier = '', $anneePublication = '')
    {
        $entete = '';
        $separateur = '|';
        $content = '';

        $statment = Atexo_Db::getLinkCommon();

        $nameFile = $nom_fichier.'.csv';

        if ('' == $nom_fichier) {
            $nameFile = $table.'_'.date('Y_m_d_h_i_s').'.csv';
        }

        $sqlEntete = "SHOW COLUMNS FROM $table";
        $dataEntete = $statment->query($sqlEntete, PDO::FETCH_ASSOC);
        foreach ($dataEntete as $resEntete) {
            $entete .= $resEntete['Field'].$separateur;
            $champs[] = $resEntete['Field'];
        }
        $entete .= "\r\n";
        $content .= $entete;
        $sql = "SELECT * FROM $table where organisme not in ('pmi-min-1','a7z','i5o') and valide='1' ";
        if ('' != $anneePublication) {
            $sql .= "AND numeroMarcheAnnee = '$anneePublication'";
        }
        $data = $statment->query($sql, PDO::FETCH_ASSOC);
        foreach ($data as $res) {
            $ligne = '';
            foreach ($champs as $oneChamp) {
                if ('idMarcheTrancheBudgetaire' == $oneChamp) {
                    $ligne .= self::getBorneMinMax($res[$oneChamp]).'#';
                }
                if ('idService' == $oneChamp) {
                    $ligne .= self::getLibelleEntiteAchat($res['organisme'], $res[$oneChamp]).'#';
                }
                $ligne .= self::enleverRetourAlaLigne($res[$oneChamp]).$separateur;
            }
            echo '.';
            $ligne .= "\r\n";
            $content .= $ligne;
        }
        Atexo_Util::write_file($fileDestination.$nameFile, $content, 'w');
        echo "\r\n ===> path file : ".$fileDestination.$nameFile."\r\n";
    }

    private static function enleverRetourAlaLigne($mot)
    {
        $mot = str_replace("\n", '', $mot);
        $mot = str_replace("\r\n", '', $mot);
        $mot = str_replace("\r", '', $mot);

        return $mot;
    }

    private static function getBorneMinMax($idMarcheTrancheBudgetaire)
    {
        return match ($idMarcheTrancheBudgetaire) {
            '12' => '4000#19999,99',
            '13' => '20000#49999,99 ',
            '14' => '50000#89999,99',
            '15' => '90000#124999,99',
            '16' => '125000#192999,99',
            '17' => '193000#999999,99',
            '18' => '1000000#2999999,99',
            '19' => '3000000#4844999,99',
            '20' => '4850000#1000000000',
            default => '0#0',
        };
    }

    private static function getLibelleEntiteAchat($organisme, $idService)
    {
        $entitePublique = '';
        $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

        if ($organismeObjet) {
            $entitePublique = $organismeObjet->getDenominationOrg();
        }
        $service = (new Atexo_EntityPurchase())->getEntityById($idService, $organisme);

        if ($service && $service instanceof CommonService) {
            $entitePublique .= '#'.$service->getCheminComplet();
        }

        return self::enleverRetourAlaLigne($entitePublique);
    }
}
