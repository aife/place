<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\Om\BaseCommonAvisPubPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPdf;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Exception;
use Prado\Prado;

/**
 * Classe de suivi des annonces de publicite depuis le concentrateur.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-develop
 *
 * @copyright Atexo 2016
 */
class Cli_SuiviPubliciteSimap extends Atexo_Cli
{
    private const ID_DOSSIER_LIMIT = 10;

    /**
     * Lancement du cron de suivi des annonces de publicite depuis le concentrateur.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function run()
    {
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces(">>>>>>>>>>>>>>>>>>> DEBUT LANCEMENT DU CRON DE SUIVI DE LA PUBLICITE SIMAP <<<<<<<<<<<<<<<<<<< \n");
        try {
            //Recuperation des annonces pour suivi concentrateur
            $destinatairesNonValide = (new Atexo_Publicite_AvisPub())->getDestinatairesValidesEcoEtNonPublieOpoce(null, true);
            $destinatairesValide = (new Atexo_Publicite_AvisPub())->getDestinatairesValidesEtPubliesOpoce(true);
            $destinataires = array_merge($destinatairesNonValide, $destinatairesValide);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Recuperation des annonces en base de donnees pour suivi concentrateur '.print_r($destinataires, true));

            //Appel du ws concentrateur pour mise a jour des annonces
            $donneesMaj = null;
            if (is_array($destinataires) && count($destinataires)) {
                $listeIdDossiers = array_chunk(self::getListeIdDossiers($destinataires), self::ID_DOSSIER_LIMIT);
                foreach ($listeIdDossiers as $idDossiers) {
                    (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Liste des ID annonces pour interroger le concentrateur : '.print_r($idDossiers, true));
                    $donneesMaj[] = (new Atexo_Publicite_AnnonceSub())->recupererDonneesMajConcentrateur($idDossiers);
                }
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Donnees pour mise a jour au niveau de mpe : '.print_r($donneesMaj, true));
            } else {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Aucune annonce a traiter');
            }

            //Mise a jour des donnees au niveau de mpe
            foreach ($donneesMaj as $dataToUpdate) {
                self::UpdateAnnoncesFromConcentrateur($dataToUpdate, $destinataires);
            }
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin mise a jour des annonces au niveau de MPE');
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur suivi des annonces de publicite : '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::run");
        }
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces(">>>>>>>>>>>>>>>>>>> FIN LANCEMENT DU CRON DE SUIVI DES ANNONCES DE PUBLICITE <<<<<<<<<<<<<<<<<<< \n");
    }

    /**
     * Permet de construire la liste des Identifiants SUB des annonces.
     *
     * @param array $listeAnnonces : liste des destinataires
     *
     * @return array : liste des identifiants
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getListeIdDossiers($listeDestinataires)
    {
        $listeIds = [];
        try {
            if (is_array($listeDestinataires)) {
                foreach ($listeDestinataires as $destinataire) {
                    if ($destinataire instanceof CommonDestinatairePub) {
                        $listeIds[] = $destinataire->getIdDossier();
                    }
                }
            }

            return $listeIds;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la constitution des ID des dossiers a envoyer au concentrateur : listeAnnonces = '.$listeDestinataires.' , Erreur = '.$e->getMessage()." \nMethode : Cli_SuiviPubliciteSimap::getListeIdDossiers");
        }
    }

    /**
     * Permet de mettre a jour les annonces de publicite MPE par les donnees de mise a jour Concentrateur.
     *
     * @param array $donneesMaj : donnees de mise a jour du concentrateur
     * @param array $annonces   : annonces a mettre a jour
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function UpdateAnnoncesFromConcentrateur($donneesMaj, $destinataires)
    {
        try {
            if (is_array($donneesMaj)) {
                $destinataires = self::reorganiserListeDestinataires($destinataires);
                foreach ($donneesMaj as $donneeMaj) {
                    if (is_array($donneeMaj)) {
                        foreach ($donneeMaj as $donnee) {
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                            try {
                                $donnee['dateTraitement'] = Atexo_Util::timestampToFormat($donnee['dateTraitement'] / 1000, 'Y-m-d H:i:s');
                                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut mis a jour annonce [id_dossier_sub = '.$donnee['idDossierSub'].'] , [support publication = '.$donnee['supportPublication'].']');
                                $destinataire = $destinataires[$donnee['idDossierSub']];
                                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Destinataire de publicite a mettre a jour : '.print_r($destinataire, true));
                                if ($destinataire instanceof CommonDestinatairePub) {
                                    $connexion->beginTransaction();
                                    $etat = self::getCorespondanceEtat($donnee['statut']);
                                    (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Destinataire de publicite statut : '.$etat);
                                    $oldEtat = $destinataire->getEtat();
                                    $destinataire->setEtat($etat);
                                    $destinataire->setDateModification($donnee['dateTraitement']);
                                    if ($etat == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE')) {
                                        $destinataire->setDatePublication($donnee['dateTraitement']);
                                    }
                                    $destinataire->save($connexion);
                                    $refDestFormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($destinataire->getId(), $destinataire->getOrganisme());
                                    if (!($refDestFormXml instanceof CommonFormXmlDestinataireOpoce)) {
                                        $refDestFormXml = (new Atexo_Publicite_AvisPub())->createNewXmlDestOpoce($destinataire->getId(), $destinataire->getOrganisme(), '', '');
                                    }
                                    if ($refDestFormXml instanceof CommonFormXmlDestinataireOpoce) {
                                        $refDestFormXml->setCodeRetour($donnee['numeroAvis']);
                                        $refDestFormXml->setIdJoue($donnee['numeroJoue']);
                                        $refDestFormXml->setDatePubJoue(substr($donnee['dateTraitement'], 0, 10));
                                        $refDestFormXml->setLienPublication($donnee['lienPublication']);
                                        $refDestFormXml->setMessageRetour($donnee['messageStatut']);
                                        $refDestFormXml->setNoDocExt($donnee['noDocExt']);
                                        $refDestFormXml->setXml($donnee['xml']);
                                        $refDestFormXml->save($connexion);
                                    }
                                    $avisPub = BaseCommonAvisPubPeer::retrieveByPK($destinataire->getIdAvis(), $destinataire->getOrganisme(), $connexion);
                                    if ($avisPub instanceof CommonAvisPub) {
                                        $consultation = (new Atexo_Consultation())->retrieveConsultation($avisPub->getConsultationId(), $avisPub->getOrganisme());
                                        if ($consultation instanceof CommonConsultation) {
                                            $avisPortailPdfDejaGenere = (new Atexo_Publicite_Avis())->retreiveListAvis($avisPub->getConsultationId(), $avisPub->getOrganisme(), Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED'), $avisPub->getTypeAvis());
                                            if (!$avisPortailPdfDejaGenere) {
                                                $pdfContent = (new Atexo_FormulaireSub_AnnonceEchange())->recupererPdfSimap($destinataire->getIdDossier());
                                                if ($pdfContent && '' != $pdfContent && 'null' != $pdfContent) {
                                                    $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s');
                                                    system('touch '.$filePdf);
                                                    $fileNew = Prado::localize('TEXT_AVIS_TED');
                                                    $avisPortail = (new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($filePdf, $consultation, 'fr', true, $pdfContent, $fileNew, true);
                                                    if ($avisPortail instanceof CommonAVIS && $avisPortail->getStatut() != Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
                                                        $avisPortail->setDatePub(date('Y-m-d'));
                                                        $avisPortail->setTypeAvisPub($avisPub->getTypeAvis());
                                                        $avisPortail->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                                                        $avisPortail->save($connexion);
                                                        //Mise à jour de l'avis global
                                                        $avisPub->setIdAvisPdfOpoce($avisPortail->getId());
                                                        $avisPub->save($connexion);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    if ($etat && $etat != $oldEtat) {
                                        (new Atexo_Publicite_AvisPub())->createHistorique($avisPub->getOrganisme(), $avisPub->getId(), date('Y-m-d H:i:s'), $avisPub->getStatut(), $connexion, $donnee['messageStatut'], Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'), true);
                                    }
                                    $connexion->commit();
                                    (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin transaction');
                                    (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Destinataire de publicite mis a jour avec SUCCES');
                                }
                            } catch (Exception $e) {
                                $connexion->rollBack();
                                (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la mise a jour des annonces depuis le concentrateur : [id_dossier_sub = '.$donnee['idDossierSub'].'] , [support publication = '.$donnee['supportPublication'].'] , Erreur = '.$e->getMessage());
                            }
                        }

                        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin mis a jour annonce [id_dossier_sub = '.$donnee['idDossierSub'].'] , [support publication = '.$donnee['supportPublication'].']');
                    }
                }
            }
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la mise a jour des annonces depuis le concentrateur : Donnees maj = '.$donneesMaj.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::UpdateAnnoncesFromConcentrateur");
        }
    }

    /**
     * Permet de reorganiser la liste des destinataires
     * Construit un tableau (idDossier => CommonDestinatairePub).
     *
     * @param array $annonces : liste des destinataires à reorganiser
     *
     * @return array : liste reorganisee des destinataires
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function reorganiserListeDestinataires($destinataires)
    {
        try {
            $resultat = [];
            if (is_array($destinataires)) {
                foreach ($destinataires as $destinataire) {
                    if ($destinataire instanceof CommonDestinatairePub) {
                        $resultat[$destinataire->getIdDossier()] = $destinataire;
                    }
                }
            }

            return $resultat;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la reorganisation de la liste des annonces : Liste des annonces = '.$destinataires.' , Erreur = '.$e->getMessage()." \nMethode : reorganiserListeDestinataires");
        }
    }

    /**
     * Permet de reorganiser la liste des supports des annonces de publicite
     * Construit un tableau (codeSupport => $CommonTSupportAnnonceConsultation).
     *
     * @param array $listeSupportsAnnonces : liste des supports a reorganiser
     *
     * @return array : liste reorganisee des supports des annonces de publicite
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function reorganiserListeSupports($listeSupportsAnnonces)
    {
        try {
            $resultat = [];
            if (!empty($listeSupportsAnnonces)) {
                foreach ($listeSupportsAnnonces as $supportAnnonce) {
                    if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                        $support = $supportAnnonce->getSupportPublication();
                        if ($support instanceof CommonTSupportPublication) {
                            $resultat[$support->getCode()] = $supportAnnonce;
                        }
                    }
                }
            }

            return $resultat;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la reorganisation de la liste des supports des annonces de publicite : Liste supports = '.$listeSupportsAnnonces.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::reorganiserListeSupports");
        }
    }

    public static function getCorespondanceEtat($etat)
    {
        $statut = 0;
        $statut = match ($etat) {
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_INTEGRE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_REJETE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE'),
            default => $statut,
        };

        return $statut;
    }
}
