<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTypeProcedurePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\WebService\Atexo_WebService_SoapClientEMS;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/*
 * Created on 26 nov. 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class Cli_AlerteNewsletterSimap extends Atexo_Cli
{
    public static function run()
    {
        $categorie = [];
        $categorie[] = Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX');
        $categorie[] = Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES');
        $categorie[] = Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES');
        foreach ($categorie as $oneCategorie) {
            self::makeNewsLetter($oneCategorie);
        }
    }

    public static function makeNewsLetter($categorie)
    {
        $newsletter = self::genererBodyNewsLetter($categorie);
        if ($newsletter) {
            $newsletter = Atexo_Util::toUtf8($newsletter);
            self::soapClient($categorie, $newsletter);
        }
    }

    //genere le code html à envoyer a CheetahMail
    public static function genererBodyNewsLetter($categorieCons)
    {
        $retour = self::genererListeConst($categorieCons);
        if ($retour) {
            $bodyNewsLetter = file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_BODY_NEWSLETTER'));
            $bodyNewsLetter = str_replace('NOMBRE_CONSULTATION_PUBLIES', $retour[1], $bodyNewsLetter);
            $textCategorie = self::getTextCategorieConsultation($categorieCons);
            $bodyNewsLetter = str_replace('TEXT_CATEGORIE', $textCategorie, $bodyNewsLetter);
            $bodyNewsLetter .= $retour[0];
            $bodyNewsLetter = str_replace('TEXT_CONTAINER', $textCategorie, $bodyNewsLetter);

            return $bodyNewsLetter;
        }

        return false;
    }

    public static function genererListeConst($categorieCons)
    {
        $retour = [];
        $consultationArray = self::getConsultationPubliees($categorieCons);
        $nbreConsCategorieFound = 0;
        if (is_array($consultationArray) && count($consultationArray) > 0) {
            foreach ($consultationArray as $oneConsultation) {
                ++$nbreConsCategorieFound;
                $ligne = file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_LIGNE_CONS_NEWSLETTER'));
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $typeProcedureObject = CommonTypeProcedurePeer::retrieveByPK($oneConsultation->getIdTypeProcedure(), $connexionCom);
                if ($oneConsultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $lienAcces = (new Atexo_Consultation())->getUrlAccesDirectProcPubliciteOuverte($oneConsultation->getId(), $oneConsultation->getOrganisme());
                } else {
                    $lienAcces = (new Atexo_Consultation())->getUrlAccesDirectParTypeAvis($oneConsultation->getId(), $oneConsultation->getOrganisme(), $oneConsultation->getIdTypeAvis());
                }

                $objetConsultation = Atexo_Util::truncateTexte($oneConsultation->getObjet(), 130);
                if (Atexo_Util::isTextTruncated($oneConsultation->getObjet(), 130)) {
                    $objetConsultation .= '...';
                }

                $ligne = str_replace('TEXT_OBJET_CONSULTATION', $objetConsultation, $ligne);
                $ligne = str_replace('DATE_FIN_CONSULTATION', Atexo_Util::iso2frnDateTime($oneConsultation->getDatefin(), false), $ligne);
                $ligne = str_replace('DATE_PUB_CONSULTATION', Atexo_Util::iso2frnDateTime($oneConsultation->getDateMiseEnLigneCalcule(), false), $ligne);
                $ligne = str_replace('ABBREVIATION_TYPE_PROCEDURE', $typeProcedureObject->getLibelleTypeProcedure(), $ligne);
                $ligne = str_replace('TYPE_AVIS', self::getTypeAvis($oneConsultation->getIdTypeAvis()), $ligne);
                $ligne = str_replace('LIEN_ACCES_DIRECT', $lienAcces, $ligne);
                $retour[0] .= $ligne;
            }
            $textCategorie = self::getTextCategorieConsultation($categorieCons);
            $retour[0] = $retour[0].'</table> <!-- /'.$textCategorie.' -->';
            $retour[1] = $nbreConsCategorieFound;

            return $retour;
        } else {
            $ligne = file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_PAS_DE_CONS_NEWSLETTER'));
            $retour[0] .= $ligne;
            $textCategorie = self::getTextCategorieConsultation($categorieCons);
            $retour[0] = $retour[0].'</table> <!-- /'.$textCategorie.' -->';
            $retour[1] = $nbreConsCategorieFound;

            return $retour;
        }
    }

    //renvoi la liste des consultations publiées depuis hier jsq present
    public static function getConsultationPubliees($categorieCons = false)
    {
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
        $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        $criteriaVo->setCategorieConsultation($categorieCons);
        $criteriaVo->setcalledFromPortail(true);
        if (!Atexo_Module::isEnabled('AnnoncesMarches')) {
            $criteriaVo->setExclureConsultationExterne('1');
        }
        $criteriaVo->setPublieHier(true);
        $criteriaVo->setInformationMarche(true);
        $consultationArray = (new Atexo_Consultation())->search($criteriaVo);

        return $consultationArray;
    }

    public static function getTextCategorieConsultation($codeCategorie): string
    {
        return match ($codeCategorie) {
            Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX') => 'Travaux',
            Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES') => 'Fournitures',
            Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES') => 'Services',
            default => '',
        };
    }

    //invocation du WS de CheetahMail
    public static function soapClient($categorie, $newsToSend)
    {
        try {
            $wsdlUrl = Atexo_Config::getParameter('URL_CHEETAHMAIL_WS');
            $Login = Atexo_Config::getParameter('LOGIN_CHEETAHMAIL_WS');
            $Password = Atexo_Config::getParameter('PASSWORD_CHEETAHMAIL_WS');
            $id = Atexo_Config::getParameter('IDMLIST_CHEETAHMAIL_WS');
            $options = ['trace' => 1, 'exceptions' => 1, 'proxy_host' => Atexo_Config::getParameter('URL_PROXY'), 'proxy_port' => Atexo_Config::getParameter('PORT_PROXY')];
            $monContent = new Atexo_WebService_SoapClientEMS($Login, $Password, $id, $wsdlUrl, $options);
            //Création du tableau de paramètres
            $idContentCheetahMail = self::getIdContent($categorie);
            $Params = ['containerId' => $idContentCheetahMail,
                                'contentKey' => Atexo_Util::toUtf8('yes'),
                                'description' => 'Newsletter',
                                'state' => 1,
                                'message' => $newsToSend, ];
            //Appel de la webméthode
            $result = $monContent->EMSContentSave($Params);
            //Affichage du résultat
            echo $result->EMSContentSaveResult;
            echo " \n envoi des consultations de la categorie ".$categorie."\n";
        } catch (Exception $e) {
            Prado::log("Erreur lors de l\'envoi de la newsletter :  ".$e->getMessage(), TLogger::INFO, 'Atexo.Cli.AlerteNewsletterSimap');
        }
    }

    /*
     * fonction de correspondance entre idCategorieConsultataion et l'id du container sur l'application cheetahMail
     */
    public static function getIdContent($idCategorie)
    {
        return match ($idCategorie) {
            '1' => '11',
            '2' => '12',
            '3' => '13',
            default => '',
        };
    }

    public static function getTypeAvis($idTypeAvis)
    {
        return match ($idTypeAvis) {
            Atexo_Config::getParameter('TYPE_AVIS_INFORMATION') => Prado::localize('TEXT_AVIS_INFORMATION'),
            Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION') => Prado::localize('TEXT_AVIS_CONSULTATION'),
            default => '',
        };
    }
}
