<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use DOMDocument;

/**
 * Permet de générer l'XML des données consultations avec decisions enregistrées il y a 10 mn.
 *
 * @category Atexo
 */
class Cli_GenerationXmlInterfacePleiade extends Atexo_Cli
{
    public static function run()
    {
        $allOrganisme = (new Atexo_Organismes())->retrieveActiveOrganismes(true, true);
        if ($allOrganisme) {
            $dataOffres = [];
            foreach ($allOrganisme as $uneOrganisme) {
                $dataOffresOneOrg = self::recupererDonneesConsultations($uneOrganisme->getAcronyme());
                $dataOffres = array_merge($dataOffres, $dataOffresOneOrg);
            }
            if (count($dataOffres)) {
                self::createXmlDonneesConsultations($dataOffres);
            } else {
                echo " \n\t ===> pas de consultation <=== \n\n";
            }
        }
    }

    public static function recupererDonneesConsultations($organisme)
    {
        $dataOffresPapiers = (new Atexo_Consultation_Decision())->recupererInfosOffresPapiers($organisme);
        $dataOffresElec = (new Atexo_Consultation_Decision())->recupererInfosOffresElectroniques($organisme);

        if (is_array($dataOffresElec) && count($dataOffresElec)) {
            foreach ($dataOffresElec as $key => $value) {
                //On boucle sur les enveloppes papiers pour ajouter les enveloppes electroniques qui n'y sont pas. La recherche se fait par clé primaire
                if (isset($dataOffresPapiers[$key]) && is_array($dataOffresPapiers[$key])) {
                    $arrayDateReponseElect = $value['min_date_ouverture'];
                    $arrayDateReponsePapier = $dataOffresPapiers[$key]['min_date_ouverture'];
                    if ($arrayDateReponseElect[1] && $arrayDateReponsePapier[1]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][1] = min($arrayDateReponseElect[1], $arrayDateReponsePapier[1]);
                    } elseif (!$arrayDateReponsePapier[1] && $arrayDateReponseElect[1]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][1] = $arrayDateReponseElect[1];
                    } elseif ($arrayDateReponsePapier[1] && !$arrayDateReponseElect[1]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][1] = $arrayDateReponsePapier[1];
                    }
                    if ($arrayDateReponseElect[2] && $arrayDateReponsePapier[2]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][2] = min($arrayDateReponseElect[2], $arrayDateReponsePapier[2]);
                    } elseif (!$arrayDateReponsePapier[2] && $arrayDateReponseElect[2]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][2] = $arrayDateReponseElect[2];
                    } elseif ($arrayDateReponsePapier[2] && !$arrayDateReponseElect[2]) {
                        $dataOffresPapiers[$key]['min_date_ouverture'][2] = $arrayDateReponsePapier[2];
                    }
                } else {
                    $dataOffresPapiers[$key] = $value;
                }
            }
        }

        return $dataOffresPapiers;
    }

    public static function createXmlDonneesConsultations($data)
    {
        //Création du document XML
        $domDocument = new DOMDocument('1.0', 'UTF-8');

        //Definir le tag consultations
        $consultations = $domDocument->createElement('consultations');
        $consultations = $domDocument->appendChild($consultations);
        if (is_array($data) && count($data)) {
            foreach ($data as $uneData) {
                //Definir le tag consultation
                $consultation = $domDocument->createElement('consultation');
                $consultation = $consultations->appendChild($consultation);
                //Definir le tag id_consultation
                $reference = $domDocument->createElement('id_consultation', self::appliquerHttpEncoding($uneData['reference']));
                $reference = $consultation->appendChild($reference);
                //Definir le tag intitule_consultation
                $intitule = $domDocument->createElement('intitule_consultation', self::appliquerHttpEncoding($uneData['intitule']));
                $intitule = $consultation->appendChild($intitule);
                //Definir le tag objet_consultation
                $objet = $domDocument->createElement('objet_consultation', self::appliquerHttpEncoding($uneData['objet']));
                $objet = $consultation->appendChild($objet);
                //Definir le tag type de procedure
                $typeProcedure = $domDocument->createElement('type_procedure', self::appliquerHttpEncoding($uneData['type_procedure']));
                $typeProcedure = $consultation->appendChild($typeProcedure);
                //Definir le tag commentaire_interne
                $commentaire = $domDocument->createElement('commentaire_interne', self::appliquerHttpEncoding($uneData['commentaire']));
                $commentaire = $consultation->appendChild($commentaire);
                //Definir le tag date_ouverture_candidatures
                if (isset($uneData['type_env'][1]) && '1' == $uneData['type_env'][1]) {
                    $dateOuvertureCandidature = $domDocument->createElement('date_ouverture_candidatures', self::getDateFormatee($uneData['min_date_ouverture'][1]));
                    $dateOuvertureCandidature = $consultation->appendChild($dateOuvertureCandidature);
                } else {
                    $dateOuvertureCandidature = $domDocument->createElement('date_ouverture_candidatures', '');
                    $dateOuvertureCandidature = $consultation->appendChild($dateOuvertureCandidature);
                }
                //Definir le tag date_limite_depot_candidature
                $dateLimitDepotCandidature = $domDocument->createElement('date_limite_depot_candidatures', self::getDateFormatee($uneData['dateFin']));
                $dateLimitDepotCandidature = $consultation->appendChild($dateLimitDepotCandidature);
                //Definir le tag date_ouverture_offres
                if (isset($uneData['type_env'][2]) && '2' == $uneData['type_env'][2]) {
                    $dateOuvertureOffres = $domDocument->createElement('date_ouverture_offres', self::getDateFormatee($uneData['min_date_ouverture'][2]));
                    $dateOuvertureOffres = $consultation->appendChild($dateOuvertureOffres);
                } else {
                    $dateOuvertureOffres = $domDocument->createElement('date_ouverture_offres', '');
                    $dateOuvertureOffres = $consultation->appendChild($dateOuvertureOffres);
                }
                //Definir le tag date_limite_depot_offres
                $dateLimiteDepotOffres = $domDocument->createElement('date_limite_depot_offres', self::getDateFormatee($uneData['dateFin']));
                $dateLimiteDepotOffres = $consultation->appendChild($dateLimiteDepotOffres);
                //Definir le tag date_attribution_marche
                $dateAttributionMarche = $domDocument->createElement('date_attribution_marche', self::getDateFormatee($uneData['date_decision']));
                $dateAttributionMarche = $consultation->appendChild($dateAttributionMarche);
                //Definir le tag date_envoi_dce
                $dateEnvoiDce = $domDocument->createElement('date_envoi_dce', self::getDateFormatee($uneData['dateMiseEnLigne']));
                $dateEnvoiDce = $consultation->appendChild($dateEnvoiDce);
                //Definir le tag date_envoi_publication
                $dateEnvoiPub = $domDocument->createElement('date_envoi_publication', self::getDateFormatee($uneData['dateMiseEnLigne']));
                $dateEnvoiPub = $consultation->appendChild($dateEnvoiPub);
                //Definir le tag decision_attribution
                $decisionAttribution = $domDocument->createElement('decision_attribution', self::appliquerHttpEncoding($uneData['type_decision']));
                $decisionAttribution = $consultation->appendChild($decisionAttribution);
                //Definir le tag id_lot
                $idLot = $domDocument->createElement('id_lot', $uneData['numero_lot']);
                $idLot = $consultation->appendChild($idLot);
            }
        }
        if (!is_dir(Atexo_Config::getParameter('PATH_XML_INTERFACE_PLEIADE'))) {
            system('mkdir '.Atexo_Config::getParameter('PATH_XML_INTERFACE_PLEIADE'));
        }
        $arrayDate = explode('-', date('Y-m-d'));
        $arrayTime = explode('-', date('H-i-s'));
        $fileName = 'MPE_'.$arrayDate[0].$arrayDate[1].$arrayDate[2].$arrayTime[0].$arrayTime[1].$arrayTime[2].'.xml';
        (new Atexo_Files())->string2file($domDocument->saveXML(), Atexo_Config::getParameter('PATH_XML_INTERFACE_PLEIADE').'/'.$fileName);
        echo $domDocument->saveXML();
        echo " \n\t Fichier ===> ".Atexo_Config::getParameter('PATH_XML_INTERFACE_PLEIADE')."/$fileName \n\n";
    }

    /**
     * Retour la date au format YYYYMMDD.
     */
    public function getDateFormatee($_isoDate)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen($_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }
        [$a, $m, $j_et_heure] = explode('-', $_isoDate);
        [$j] = explode(' ', $j_et_heure);

        return $a.$m.$j;
    }

    public function appliquerHttpEncoding($chaine)
    {
        if ('UTF-8' != Atexo_Config::getParameter('HTTP_ENCODING') && 'utf8' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            return Atexo_Util::toUtf8($chaine);
        }

        return $chaine;
    }
}
