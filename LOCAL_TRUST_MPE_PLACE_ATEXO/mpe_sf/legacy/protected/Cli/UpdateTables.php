<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonTNumeroReponse;
use Application\Propel\Mpe\CommonTNumeroReponsePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use AtexoCrypto\Dto\Offre;
use PDO;

/**
 * commentaires.
 *
 * @author khadija chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 5.0
 *
 * @since MPE-4.0
 */
class Cli_UpdateTables extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter un param : \r\n";
        }
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                /*if ($arguments[$i] == 'NumeroReponse') {
                    self::insertNumeroReponseOffres();
                 }
                if($arguments[$i] == 'NumeroReponsePapier'){
                    self::insertNumeroReponseOffres(true);
                }*/
                if ('InitTNumeroReponse' == $arguments[$i]) {
                    self::insertNumeroReponseOffresNewTanle();
                }
            }
        }
    }

    /***
    * Permet d'inserer les numeros de reponse dans la table Offres
    * et offre_papier
    *****/
    public function insertNumeroReponseOffres($papier = false)
    {
        $arrayCons = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($papier) {
            $sql = 'SELECT id,consultation_id,organisme,numero_reponse FROM Offre_papier ORDER BY `date_depot` ';
            $TableName = 'offre_papier';
        } else {
            $sql = 'SELECT id,consultation_id,organisme,numero_reponse FROM `Offres` ORDER BY `untrusteddate` ';
            $TableName = 'offres';
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $results = $statement->execute();
        $fichirLog = Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/UpdateTables-'.date('Y').'-'.date('m').'-'.date('j').'.log';
        if ($results) {
            Atexo_Util::writeFile($fichirLog, ' Debut script Update '.$TableName.' '.date('d/m/Y  H:i:s')." \n", 'a+');
            echo ' Debut script Update '.$TableName.'  '.date('d/m/Y  H:i:s')."\n";
            $numReponse = 1;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $save = false;
                if (isset($arrayCons[$row['consultation_id'].$row['organisme']])) {
                    ++$arrayCons[$row['consultation_id'].$row['organisme']];
                    $numReponse = $arrayCons[$row['consultation_id'].$row['organisme']];
                } else {
                    $arrayCons[$row['consultation_id'].$row['organisme']] = 1;
                    $numReponse = 1;
                }
                if (('0' == $row['numero_reponse']) ||
                     (null == $row['numero_reponse']) ||
                     ('' == $row['numero_reponse'])) {
                    $save = true;
                }
                if ($save) {
                    if ($papier) {
                        $offre = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($row['id'], $row['organisme']);
                    } else {
                        $offre = (new Atexo_Interfaces_Offre())->getOffreByIdOffre($row['id']);
                    }
                    if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
                        $offre->setNumeroReponse($numReponse);
                        $offre->save($connexion);

                        Atexo_Util::writeFile($fichirLog, ' Num Offre = '.$offre->getId().' reference Consultation '.$row['consultation_id'].': numero_reponse '.
                                   (int) $row['numero_reponse'].' => '.$numReponse."\n", 'a+');
                        echo "****************************************** \n";
                        echo '* Num Offre = '.$offre->getId().' reference Consultation '.$row['consultation_id'].': numero_reponse '.
                               (int) $row['numero_reponse']." => $numReponse \n";
                        unset($offre);
                    }
                }
            }
            echo ' Fin script Update '.$TableName." \n";
            Atexo_Util::writeFile($fichirLog, ' Fin script Update '.$TableName."  \n", 'a+');
            Atexo_Util::writeFile($fichirLog, "******************************************  \n", 'a+');
        }
        unset($arrayCons);
    }

    /**
     * @param $ref
     * @param $org
     * @param $connexion
     *
     * @return int
     */
    public static function getNbrOffres($consultationId, $org, $connexion)
    {
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $org);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        $countOffres = CommonOffresPeer::doCount($c, $connexion);

        return $countOffres ?: 0;
    }

    /***
     * Permet d'inserer les numeros de reponse dans la table t_numero_reponse
     * et offre_papier
     *****/
    public static function insertNumeroReponseOffresNewTanle()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $sql = <<<QUERY
SELECT DISTINCT CONCAT(consultation.id, consultation.organisme), consultation.id, consultation.organisme,
consultation.datefin, consultation.reference_utilisateur, consultation.intitule, consultation.objet, datemiseenligne ,
datevalidation, regle_mise_en_ligne , consultation.categorie, date_format(consultation.dateDebut,'%d/%m/%Y') as datedebut,
consultation.code_procedure as code_procedure , 1 as invitedWithHisHabilitation, 0 as invitedReadOnly, 0 as invitedValidationOnly
FROM consultation INNER JOIN Organisme on Organisme.acronyme =consultation.organisme
WHERE 1 AND consultation.type_acces='1' AND consultation.id_type_avis='3' AND consultation.doublon='0'
AND Organisme.active = '1' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND consultation.id_etat_consultation='0'
AND consultation_annulee='0' AND ( (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL
AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' AND date_mise_en_ligne_calcule<=now())) )
ORDER BY ordre ASC , datefin DESC
QUERY;
        echo ' Debut script Update t_numero_reponse  '.date('d/m/Y  H:i:s')."\n";
        $stmt = $connexion->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $nbr = self::getNbrOffres($row['id'], $row['organisme'], $connexion);
            echo ' Consulatation => '.$row['id'].' org => '.$row['organisme'].' nbr reponses : '.$nbr."\n";
            $tnumReponse = CommonTNumeroReponsePeer::retrieveByPK($row['id']);
            if (!($tnumReponse instanceof CommonTNumeroReponse)) {
                $tnumReponse = new CommonTNumeroReponse();
            }
            $tnumReponse->setId($row['id']);
            $tnumReponse->setConsultationId($row['id']);
            $tnumReponse->setOrganisme($row['organisme']);
            $tnumReponse->setNumeroReponse($nbr);
            $tnumReponse->save($connexion);
            unset($tnumReponse);
        }
        echo ' Fin script Update t_numero_reponse  '.date('d/m/Y  H:i:s')."\n";
    }
}
