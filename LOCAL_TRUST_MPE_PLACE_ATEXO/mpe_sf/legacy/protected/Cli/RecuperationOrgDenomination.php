<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Exception;

/**
 * Cron de recuperation des org_denomination de la table consultation
 * pour l'insertion dans la table Referentiel_org_denomination.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 *
 * @category Atexo
 *
 * @copyright Atexo 2012
 */
class Cli_RecuperationOrgDenomination extends Atexo_Cli
{
    public static function run()
    {
        try {
            $cheminFichierATraiter = self::telechargerFichierReferentielOrgDenominationFromANM();
            if ($cheminFichierATraiter) {
                self::majDeLaTableReferentielOrgDenomination($cheminFichierATraiter);
            } else {
                echo "\r\n-> la mise à jour de la table Referentiel_org_denomination n'a pas pu être faite car le fichier n'a pas été récupéré ... ";
            }
        } catch (Exception $e) {
            echo '-> erreur : '.$e->getMessage()."\n";
        }
    }

    private static function telechargerFichierReferentielOrgDenominationFromANM()
    {
        echo "\n-> Recuperation du fichier Referentiel_org_denomination.csv depuis ANM et stockage au niveau ";

        $fileDestination = Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/synchro_referentiel/';
        $nom_fichier = 'Referentiel_org_denomination.csv';

        if (!is_dir($fileDestination)) {
            system('mkdir '.escapeshellarg($fileDestination));
            echo "\r\n-> répertoire suivant créé   ".$fileDestination.'....';
        }

        $wget = "wget --http-user= --http-passwd= 'https://www.annonces-marches.com/Referentiel_org_denomination.csv'  --no-check-certificate --output-document=".$fileDestination.$nom_fichier;

        system($wget, $res);

        if (!$res) {
            echo "\r\n-> le fichier a ete recupere avec succès ... \r\n Voici le resultat de l'execution de la commande wget : ";
            print_r($res);
            if (is_file($fileDestination.$nom_fichier)) {
                return $fileDestination.$nom_fichier;
            }
        } else {
            echo "\r\n-> execution de la commande  ".$wget.' en echec ....';

            return false;
        }
    }

    private static function majDeLaTableReferentielOrgDenomination($cheminFichierATraiter)
    {
        $handle = fopen($cheminFichierATraiter, 'r');
        $ligne = 0;

        $sqlArrayInsert = [];
        $sqlArrayUpdate = [];

        while (($uneLigneCvs = fgetcsv($handle, 10000, '|')) !== false) {
            if (0 != $ligne) {
                $sqlArrayInsert[] = "INSERT INTO `Referentiel_org_denomination` (`denomination`, `denomination_adapte`, `denomination_normalise`, `traite`) 
				SELECT '".addslashes($uneLigneCvs[1])."', '".addslashes($uneLigneCvs[2])."', '".addslashes($uneLigneCvs[3])."', '".$uneLigneCvs[4]."' FROM DUAL 
				WHERE NOT EXISTS (
					SELECT 1 FROM `Referentiel_org_denomination` WHERE  
					`denomination` = '".addslashes($uneLigneCvs[1])."' AND
					`denomination_adapte` = '".addslashes($uneLigneCvs[2])."'
				)
				LIMIT 1;";

                $sqlArrayUpdate[] = " UPDATE `Referentiel_org_denomination` set 
						`denomination_normalise` = '".addslashes($uneLigneCvs[3])."',
						`traite`= '".$uneLigneCvs[4]."'
							WHERE 
							`denomination_adapte` = '".addslashes($uneLigneCvs[2])."'";
            }

            ++$ligne;
        }

        echo 'nombre de ligne traitée : '.$ligne;

        //Execution des requetes de la base Commune
        echo "\n MISE A JOUR DE LA TABLE : Referentiel_org_denomination";

        $log = '';
        try {
            foreach ($sqlArrayInsert as $query) {
                if (Atexo_Db::getLinkCommon()->query($query)) {
                    echo '.';
                } else {
                    $message = " Referentiel_org_denomination : ERROR when executing query : $query ";
                    $log .= $message."\r\n";
                    echo $log;
                }
            }
            foreach ($sqlArrayUpdate as $query) {
                if (Atexo_Db::getLinkCommon()->query($query)) {
                    echo '.';
                } else {
                    $message = " Referentiel_org_denomination : ERROR when executing query : $query ";
                    $log .= $message."\r\n";
                    echo $log;
                }
            }
            Propel::log($log, Propel::LOG_ERR);
            echo "\n\n";
        } catch (Exception $ex) {
            Propel::log($log, Propel::LOG_ERR);
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }
}
