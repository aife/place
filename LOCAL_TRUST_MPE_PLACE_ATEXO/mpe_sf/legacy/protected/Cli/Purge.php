<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Purge les fichiers temporaires (dossier COMMON_TMP) et les logs BOAMP (LOG_FILE_PATH_BOAMP).
 *
 * @category Atexo
 */
class Cli_Purge extends Atexo_Cli
{
    // RETENTION en heures
    const RETENTION = 9;
    const RETENTION_DCE = 9;

    // Retention en jours
    const RETENTION_BOAMP = 15;
    const RETENTION_SUIVI = 10;
    const RETENTION_INTERFACE = 15;

    /**
     * Purge les fichiers temporaires (dossier COMMON_TMP) et les logs BOAMP (LOG_FILE_PATH_BOAMP).
     */
    public static function run()
    {
        $dir = str_replace('//', '/', Atexo_Config::getParameter('COMMON_TMP'));
        self::display("Dossier temporaire : $dir");
        if (strrpos($dir, '/tmp/') > 0) {
            $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('COMMON_TMP')));
            $user = $user['name'];
            if ('root' == $user) {
                throw new Exception("L'utilisateur root ne peut être propriétaire du dossier tmp");
            }
            if ('' == $user || false === $user || !isset($user)) {
                throw new Exception("L'utilisateur n'a pas été trouvé");
            }
            self::display("User : $user");
            Prado::log('Purge en cours...', TLogger::INFO, 'Atexo.Cli.Cron.Purge');
            $cmd = 'find '.$dir.' -mindepth  1 -mmin +'.(self::RETENTION * 60).' -user '.$user.' -exec rm -rf {} \;';
            $cmd_dce = 'find '.$dir.' -mindepth  1 -name "dce_*" -mmin +'.(self::RETENTION_DCE * 60).' -user '.$user.' -exec rm -rf {} \;';
            $cmd_boamp = 'find '.Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP').' -mindepth  1 -type f -mtime +'.self::RETENTION_BOAMP.' -user '.$user.' -exec rm -rf {} \;';
            $cmd_boamp_suivi = 'find '.Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP_SUIVI').' -mindepth  1 -type f -mtime +'.self::RETENTION_SUIVI.' -user '.$user.' -exec rm -rf {} \;';
            $cmd_moniteur = 'find '.Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR').' -mindepth  1 -type f -mtime +'.self::RETENTION_BOAMP.' -user '.$user.' -exec rm -rf {} \;';
            $cmd_moniteur_suivi = 'find '.Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR_SUIVI').' -mindepth  1 -type f -mtime +'.self::RETENTION_SUIVI.' -user '.$user.' -exec rm -rf {} \;';
            $cmd_interface = 'find '.Atexo_Config::getParameter('LOG_FILE_INTERFACES').' -mindepth  1 -type f -mtime +'.self::RETENTION_INTERFACE.' -user '.$user.' -exec rm -rf {} \;';
            if (is_dir($dir)) {
                system($cmd);
            }
            if (is_dir($dir)) {
                system($cmd_dce);
            }
            if (is_dir(Atexo_Config::getParameter('LOG_FILE_INTERFACES'))) {
                system($cmd_interface);
            }
            if (strrpos(Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP'), '/boamp') > 0) {
                if (is_dir(Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP'))) {
                    system($cmd_boamp);
                }
                if (is_dir(Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP_SUIVI'))) {
                    system($cmd_boamp_suivi);
                }
                if (is_dir(Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR'))) {
                    system($cmd_moniteur);
                }
                if (is_dir(Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR_SUIVI'))) {
                    system($cmd_moniteur_suivi);
                }
            }
        } else {
            throw new Exception("Le dossier temporaire doit s'appeler tmp.");
        }
    }
}
