<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Exception;

/**
 * Cron de mise a jour des consultations du portail Annonces-Marches depuis AchatPublic.Com (APC).
 *
 * @category Atexo
 */
class Cli_AnnoncesMarchesOmnikles extends Atexo_Cli
{
    protected static string $acronyme = '';
    protected static string $lieuExecution = '';

    public static function run()
    {
        setlocale(LC_TIME, 'fr_FR');
        try {
            $options = new \Zend_Console_Getopt('-o:e:l:');
            //$options = getopt('-o:e:l:');

            if (isset($options->e)) {
                self::$acronyme = $options->e;
            } else {
                echo "l'option -e (acronyme de l'organisme) est obligatoire \n";

                return false;
            }
            if (isset($options->l)) {
                self::$lieuExecution = $options->l;
            }
            if (isset($options->o)) {
                self::PostFile($options->o);
            } else {
                echo "l'option -o (code de l'organisme) est obligatoire \n";

                return false;
            }
        } catch (\Zend_Console_Getopt_Exception $e) {
            self::displayError($e->getMessage().PHP_EOL.$e->getUsageMessage());
        }
    }

    /*
     * permet de creer la requete qui sera envoyer au WS Omnikles
     */
    public static function createRequeste($codeOrg)
    {
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_REQUEST_OMNIKLES');
        $handle = fopen($filePath, 'r');
        $contents = fread($handle, filesize($filePath));
        fclose($handle);
        $contents = Atexo_Util::xmlStrReplace($contents, '##CODE_ORG##', $codeOrg);
        $pathFile = Atexo_Config::getParameter('COMMON_TMP').'/Omnikles_'.uniqid();
        Atexo_Util::write_file($pathFile, $contents, 'w');

        return $pathFile;
    }

    /*
     * permet d'envoyer le fichier de request WS Omnikles
     */
    public static function PostFile($codeOrg)
    {
        $result = null;
        $e = null;
        try {
            $pathFile = self::createRequeste($codeOrg);
            $url = Atexo_Config::getParameter('URL_POST_OMNIKLES');
            $postfields = [];
            $postfields['xml'] = '@'.$pathFile;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
            //definition du proxy si existe
            $urlProxy = Atexo_Config::getParameter('URL_PROXY');
            $portProxy = Atexo_Config::getParameter('PORT_PROXY');
            $curlOptProxy = $urlProxy.':'.$portProxy;
            if ('' != $urlProxy) {
                curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            }
            $result = curl_exec($ch);
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'), date('Y-m-d H:i:s').' -- >  erreur de communication avec le Ws \n'.$e->getMessage().'\n', 'a+');
        }
        $xml = simplexml_load_string($result);
        if ($xml) {
            $xml = (array) $xml;
            $tender = (array) $xml['Response'];
            if (is_array($tender['Tender']) && count($tender['Tender']) > 0) {
                foreach ($tender['Tender'] as $OneCons) {
                    self::AddConsultation($OneCons);
                }
            }
        } else {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'), date('Y-m-d H:i:s').' -- >  chaine xml non valide \n'.$e->getMessage().'\n', 'a+');
        }
    }

    /*
     * permet de créer/modifier une consultation
     */
    public static function AddConsultation($OneCons)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $cons = (array) ($OneCons);
        $organismeName = $cons['@attributes']['organismeName'];
        $refUser = $cons['@attributes']['reference'];
        $categoryName = $cons['@attributes']['category'];
        $codeCP = $cons['@attributes']['organismeCP'];
        $city = $cons['@attributes']['organismeCity'];
        $dateMiseEnLigneCalcule = $cons['@attributes']['dateMiseOnLigneCalcule'];
        if (!in_array($categoryName, ['Travaux', 'Fournitures', 'Services'])) {
            $categoryName = 'Services';
        }
        $abbreviationTypeProc = $cons['@attributes']['procedureType'];
        $idtypeprocedure = (new Atexo_Consultation_ProcedureType())->getIdTypeProcedureByAbbreviation($abbreviationTypeProc);
        $category = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($categoryName);
        $objet = html_entity_decode($cons['Subject'], ENT_NOQUOTES, 'ISO-8859-1');
        $dateCloture = preg_replace('#([0-9]+)/([0-9]+)/([0-9]+) ([0-9]+):([0-9]+)#', '$3-$2-$1 $4:$5:00', $cons['@attributes']['closureDate']);
        $lienDirect = $cons['Url'];
        $lien = '/redir.php?url='.base64_encode($lienDirect);
        $expldeLien = explode('&idDossier=', $lienDirect);
        $idDossier = current(explode('&', $expldeLien[1]));
        $newRef = sha1(self::$acronyme.$organismeName.$refUser.$idDossier);
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REF_ORG_PARTENAIRE, $newRef);
        $c->add(CommonConsultationPeer::ORGANISME, self::$acronyme);
        $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);
        $create = false;
        if (!($commonConsultation instanceof CommonConsultation)) {
            $commonConsultation = new CommonConsultation();
            $commonConsultation->setOrganisme(self::$acronyme);
            $commonConsultation->setRefOrgPartenaire($newRef);
            $commonConsultation->setDatedebut(date('Y-m-d H:i:s'));
            $create = true;
        }

        $commonConsultation->setOrgDenomination($organismeName.'('.$codeCP.' - '.$city.')');
        $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($organismeName)));
        $commonConsultation->setReferenceUtilisateur($refUser);
        $commonConsultation->setIdTypeProcedure($idtypeprocedure);
        $commonConsultation->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
        $commonConsultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        $commonConsultation->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1'));
        $commonConsultation->setDateMiseEnLigneCalcule($dateMiseEnLigneCalcule);
        $commonConsultation->setServiceId('0');
        $commonConsultation->setServiceAssocieId('0');
        $commonConsultation->setConsultationExterne('1');
        $commonConsultation->setCategorie($category->getId());
        $commonConsultation->setDatefin($dateCloture);
        $commonConsultation->setIntitule($objet);
        $commonConsultation->setObjet($objet);
        if ('' != self::$lieuExecution) {
            $commonConsultation->setLieuExecution(self::$lieuExecution);
        }
        $commonConsultation->setUrlConsultationExterne($lien);
        $commonConsultation->setUrlConsultationAchatPublique($lienDirect);
        try {
            $commonConsultation->save($connexionCom);
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'), date('Y-m-d H:i:s').' -- >  \n'.$e->getMessage().'\n', 'a+');
        }
        // Referntiel org denomonation debut
        if ($commonConsultation->getDenominationAdapte()) {
            (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomonation($commonConsultation);
        }
        if ($create) {
            echo ' -consultation créée: '.$refUser.' -- '.$categoryName."\n ";
        } else {
            echo ' -consultation modifiée: '.$refUser.' -- '.$categoryName."\n ";
            (new Atexo_Consultation_Lots())->deleteCommonLots(self::$acronyme, $commonConsultation->getId());
            $commonConsultation->setAlloti('0');
        }
        $ListeLot = (@json_decode(@json_encode($OneCons, JSON_THROW_ON_ERROR), 1, 512, JSON_THROW_ON_ERROR));
        if (is_array($ListeLot['Lots']['Lot']) && count($ListeLot['Lots']['Lot']) > 0) {
            if (!isset($ListeLot['Lots']['Lot'][0])) {
                $tmp = $ListeLot['Lots']['Lot'];
                $ListeLot['Lots']['Lot'] = [];
                $ListeLot['Lots']['Lot'][] = $tmp;
            }
            foreach ($ListeLot['Lots']['Lot'] as $oneLot) {
                self::addLot($oneLot, $commonConsultation);
            }
            $commonConsultation->setAlloti('1');
        }
        try {
            $commonConsultation->save($connexionCom);
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'), date('Y-m-d H:i:s').' -- >  \n'.$e->getMessage().'\n', 'a+');
        }
    }

    /*
     * permet d'ajouter un lot
     */
    public static function addLot($lotData, $commonConsultation)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $description = is_array($lotData['Title']) ? '' : $lotData['Title'];
            $sujet = is_array($lotData['Subject']) ? '' : $lotData['Subject'];
            $lotObjet = new CommonCategorieLot();
            $lotObjet->setlot($lotData['@attributes']['number']);
            $lotObjet->setCodeCpv1($lotData['@attributes']['cpv']);
            $lotObjet->setConsultationId($commonConsultation->getId());
            $lotObjet->setOrganisme(self::$acronyme);
            $lotObjet->setDescription($description);
            $lotObjet->setDescriptionDetail($sujet);
            $lotObjet->setCategorie($commonConsultation->getCategorie());
            $lotObjet->save($connexionCom);
            echo ' ---insertion du lot numero '.$lotData['@attributes']['number'].' pour la consultation : '.$commonConsultation->getReferenceUtilisateur()." \n ";
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'), date('Y-m-d H:i:s').' -- >  \n'.$e->getMessage().'\n', 'a+');
        }
    }
}
