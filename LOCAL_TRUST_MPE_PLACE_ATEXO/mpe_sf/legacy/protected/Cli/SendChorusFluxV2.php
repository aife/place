<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * Class Cli_SendChorusFluxV2 : CRON de generation et d'envoi HTTP des flux vers CHORUS.
 *
 * @category Atexo
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Cli_SendChorusFluxV2 extends Atexo_Cli
{
    /**
     * Execution de la commande.
     */
    public static function run()
    {
        /** @var $params */
        $params = [];
        $params['debutExecution'] = new DateTime('now');
        $params['service'] = 'Chorus';
        $params['nomBatch'] = 'Cli_SendChorusFluxV2';
        self::oversight($params);
        $returnStatutLancementCli = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;

        (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Debut lancement routine de generation et d'envoi de flux vers CHORUS (Cli_SendChorusFluxV2)");

        try {
            $returnStatutLancementGenFlux = Cli_GenerationFluxChorus::run(true);
            $returnStatutLancementEnvoiFlux = Cli_EnvoyerFluxChorus::run(true);

            (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Valeur de retour de la routine (Cli_GenerationFluxChorus) = $returnStatutLancementGenFlux");
            (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Valeur de retour de la routine (Cli_EnvoyerFluxChorus) = $returnStatutLancementEnvoiFlux");
            (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Pour plus de details sur les logs d'execution des routines Cli_GenerationFluxChorus et Cli_EnvoyerFluxChorus, consultez les fichiers 'interfacePlaceChorusGenerationFlux.log' et 'interfacePlaceChorusEnvoiFlux.log'");

            if (0 == $returnStatutLancementGenFlux && 1 == $returnStatutLancementEnvoiFlux) {
                $returnStatutLancementCli = Atexo_Oversight_Oversight::$codeSuccess;
            }
            $params['code'] = $returnStatutLancementCli;

            (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Valeur de retour de la routine (Cli_SendChorusFluxV2) = $returnStatutLancementCli");
            (new Atexo_Chorus_Util())->loggerInfosGenerationEnvoiFlux("Fin lancement routine de generation et d'envoi de flux vers CHORUS (Cli_SendChorusFluxV2)".PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL);

            $params['finExecution'] = 'endScript';
            self::oversight($params);
        } catch (Exception $e) {
            $erreur = "Erreur lors du lancement du CLI de generation et d'envoi HTTP des flux vers Chorus (Cli_SendChorusFluxV2).".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_SendChorusFluxV2::run()';
            echo $erreur;
            (new Atexo_Chorus_Util())->loggerErreursGenerationEnvoiFlux($erreur);
            (new Atexo_Chorus_Util())->envoyerMailSupportChorus("Erreur lors du lancement du CLI de generation et d'envoi HTTP des flux vers Chorus (Cli_SendChorusFluxV2).", $erreur);
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $params['resultat'] = $erreur;
            $params['finExecution'] = 'endScript';
            self::oversight($params);
        }
        echo $returnStatutLancementCli;

        return $returnStatutLancementCli;
    }
}
