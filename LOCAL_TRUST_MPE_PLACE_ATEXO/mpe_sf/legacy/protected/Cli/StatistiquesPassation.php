<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_GenerationCsv;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Cron de génération des statistiques de passation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @category Atexo
 */
class Cli_StatistiquesPassation extends Atexo_Cli
{
    public static function run()
    {
        $organismes = CommonOrganismePeer::doSelect(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
        if (is_array($organismes)) {
            foreach ($organismes as $organisme) {
                $session = [0 => '',
                    1 => [],
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => '',
                    6 => '',
                    7 => $organisme->getAcronyme(),
                    8 => '',
                    9 => '',
                    10 => '',
                    11 => '',
                    12 => '',
                    13 => [],
                    14 => '',
                    15 => '',
                    16 => '',
                    17 => '',
                    18 => '',
                    19 => '',
                    20 => '',
                    21 => '',
                    22 => '',
                    23 => '',
                    24 => '',
                    25 => '',
                    26 => '',
                    27 => '',
                    28 => '',
                    29 => '',
                    30 => '',
                    31 => '',
                    32 => '',
                    33 => '',
                    34 => '',
                    35 => '',
                    36 => '',
                    37 => '',
                    38 => '',
                    39 => '',
                    40 => '',
                    41 => '',
                    42 => '',
                    43 => '',
                    44 => '',
                    45 => '',
                    46 => '',
                    47 => '',
                    48 => '',
                    49 => '',
                    50 => '',
                    51 => '',
                    52 => '',
                    53 => '',
                    54 => '',
                    55 => '',
                    56 => '',
                    57 => '',
                    58 => '',
                    59 => '',
                    60 => '',
                    61 => '',
                    62 => '',
                    63 => '',
                    64 => '',
                    65 => '',
                ];

                session_id(uniqid());
                $_SESSION[session_id()] = serialize($session);

                if (Atexo_Module::isEnabled('SuiviPassation', $organisme->getAcronyme())) {
                    $dirName = Atexo_Config::getParameter('STATISTIQUES_PASSATIONS_PATH').$organisme->getAcronyme().'/';

                    if (!is_dir($dirName)) {
                        mkdir($dirName, 0777, true);
                    }

                    $csvMarchesConclus = (new Atexo_GenerationCsv())->genererCvsMarchesConclus($organisme->getAcronyme());
                    Atexo_Util::write_file($dirName.'Export_Marches_Conclus.csv', $csvMarchesConclus);

                    $csvPassation = (new Atexo_GenerationCsv())->genererCvsSuiviPassation($organisme->getAcronyme());
                    Atexo_Util::write_file($dirName.'Export_Passation.csv', $csvPassation);

                    $csvAvenant = (new Atexo_GenerationCsv())->genererCsvExportAvenants($organisme->getAcronyme());
                    Atexo_Util::write_file($dirName.'Export_Avenants.csv', $csvAvenant);
                }
            }
        }
    }
}
