<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Exception;
use PDO;
use Prado\Prado;

/*
 * Created on 16 mai 2013
 *
 * @author Mohamed Wazni <mohamed.wazni@atexo.com>
 * @package
 */
class Cli_ExtractMarchesConclus extends Atexo_Cli
{
    public static function run()
    {
        $entete = null;
        $directory = Atexo_Config::getParameter('COMMON_TMP');
        $nomFichier = $directory.'/exportMarchesConclus_'.date('Y_m_d_H_i_s').'.csv';

        if ($fp = fopen($nomFichier, 'a')) {
            $entete = ['servicePath' => 'chemin complet du service',
                                       'organisme' => 'Organisme',
                                       'objetMarche' => 'Objet du marché',
                                       'dateNotification' => 'Date de notification',
                                       'attributaire' => 'Attributaire',
                                       'codePostal' => 'Code postal',
                                       'montant' => 'Montant EUR HT',
                                       'libelle' => 'Nature',
                                       'libelle_type_procedure' => 'libelle type de procédure',
                                       'cpv' => 'codes cpv associé',
                                       ];
            fputcsv($fp, $entete, '|');
        }
        try {
            echo "Extraction des données  : \n";
            self::searchMarchesConclus($nomFichier, $entete);
        } catch (Exception $e) {
            echo "\n\n";
            echo $e->getMessage();
            echo "\n\n";
        }
        echo 'Fichier créé : '.$nomFichier."\n";
    }

    public static function searchMarchesConclus($nomFichier, $entete)
    {
        $sql = " SELECT * FROM (  
 					SELECT C.service_id as idService, C.organisme, D.objet_marche, D.date_notification, O.nom_entreprise_inscrit as nom_attributaire, O.code_postal_inscrit as code_postal,  
 					D.montant_marche, D.categorie, C.id_type_procedure_org, C.code_cpv_1, C.code_cpv_2
 					FROM Offres O, decisionEnveloppe D, consultation C  
 					WHERE O.id = D.id_offre 
					 AND 
					 D.consultation_id = C.id AND O.organisme = D.organisme 
					 AND D.organisme = C.organisme  
					 AND D.date_notification 
					 IS NOT NULL 
					 AND D.type_enveloppe = '1' 
					 
					 UNION  
					 
					 SELECT C.service_id as idService, C.organisme, D.objet_marche, D.date_notification, P.nom_entreprise as nom_attributaire, P.code_postal as code_postal,  
					 D.montant_marche, D.categorie, C.id_type_procedure_org, C.code_cpv_1, C.code_cpv_2
					 FROM Offre_papier P, decisionEnveloppe D, consultation C  
					 WHERE P.id = D.id_offre 
					 AND D.consultation_id = C.id AND P.organisme = D.organisme 
					 AND D.organisme = C.organisme AND D.date_notification IS NOT NULL 
					 AND D.type_enveloppe = '2' 
					 )  res WHERE 1 = 1 
				";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($fp = fopen($nomFichier, 'a')) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $row['cpv'] = $row['code_cpv_1'].$row['code_cpv_2'];
                $row['servicePath'] = Atexo_EntityPurchase::getPathEntityById($row['idService'], $row['organisme']);
                $row['objet_marche'] = str_replace("\n", '', $row['objet_marche']);
                $row['objet_marche'] = str_replace("\r\n", '', $row['objet_marche']);
                $row['objet_marche'] = str_replace("\r", '', $row['objet_marche']);

                $row['id_type_procedure_org'] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($row['id_type_procedure_org'], false, $row['organisme']);

                if (1 == $row['categorie']) {
                    $row['categorie'] = Prado::localize('DEFINE_TRAVAUX');
                } elseif (2 == $row['categorie']) {
                    $row['categorie'] = Prado::localize('DEFINE_FOURNITURES');
                } elseif (3 == $row['categorie']) {
                    $row['categorie'] = Prado::localize('DEFINE_SERVICES');
                }

                unset($row['objet_marche']);
                unset($row['id_type_procedure_org']);
                unset($row['categorie']);
                unset($row['code_cpv_1']);
                unset($row['code_cpv_2']);
                unset($row['idService']);
                fputcsv($fp, $row, '|');
            }
        } else {
            echo "impossible d'acceder au fichier '";
            exit;
        }
        fclose($fp);
    }
}
