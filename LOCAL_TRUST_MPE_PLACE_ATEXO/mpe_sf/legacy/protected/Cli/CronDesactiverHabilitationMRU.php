<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;

/**
 * Classe CronEnvoiNewsletter permettant d'envoyer une newsletter.
 *
 * @author  Houriya MAATALLA <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class Cli_CronDesactiverHabilitationMRU extends Atexo_Cli
{
    public static function run()
    {
        $org = null;
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            $org = $arguments[2];
        }
        self::display('Début ');
        self::MajHabilitationsAgent($org);
        self::display('Done');
    }

    private static function MajHabilitationsAgent($org)
    {
        $listeAgents = Atexo_Agent::retriveAgentsByOrganisme($org, true);
        foreach ($listeAgents as $agent) {
            $sql = "UPDATE `HabilitationAgent` SET `module_redaction_uniquement` = '0' ".
                    'WHERE `HabilitationAgent`.`id_agent` ='.$agent->getId().' LIMIT 1 ;';
            Atexo_Db::getLinkCommon()->query($sql);
        }
    }
}
