<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Exception;
use PDO;

/**
 * Cron pour exporter les marches  d'une annee.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @category Atexo
 */
class Cli_ExportMarcheParAnnee extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if (is_array($arguments)) {
            if (count($arguments) < 3) {
                echo "Merci d'ajouter des params dans l'ordre : \r\n";
                echo "\t-> 1- le chemin du repertoire de destination (/ a la fin) \r\n";
                echo "\t-> 2- nom du fichier de destination (si rien n'est mis alors le fiche se nomme 'Export_Marches_Annee_'l'annee de publication'.csv \r\n";
                echo "\t-> 3- annee publication \r\n";
                exit;
            }
            $fileDestination = $arguments['2'];
            $nom_fichier = $arguments['3'];
            $anneePublication = $arguments['4'];
            if (!is_dir($fileDestination)) {
                system('mkdir '.escapeshellarg($fileDestination));
                echo "\r\n-> répertoire suivant créé   ".$fileDestination.'....';
            }
            self::exporterDonneesMarcheVersCsv($anneePublication, $fileDestination, $nom_fichier);
        }
    }

    public static function exporterDonneesMarcheVersCsv($annee, $fileDestination, $nom_fichier = '')
    {
        try {
            echo "Debut de traitement \n";
            if (!$nom_fichier) {
                $nom_fichier = 'Export_Marches_Annee_';
            }
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $nameFile = $nom_fichier.$annee.'.csv';
            $sql = " SELECT DISTINCT id_contrat_titulaire FROM `t_contrat_titulaire` as contrat  LEFT JOIN MarchePublie
 on ( contrat.organisme = MarchePublie.organisme AND ( (contrat.service_id = MarchePublie.idService) OR (contrat.service_id is null AND MarchePublie.idService is NULL)))
 WHERE
  YEAR(contrat.date_notification) = '".$annee."'
  AND contrat.date_notification IS NOT NULL
  AND contrat.contrat_class_key='1'
  AND contrat.publication_contrat='0'
  AND MarchePublie.isPubliee = '1'
  AND MarchePublie.numeroMarcheAnnee = '".$annee."' ";
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $results = $statement->execute();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $lines = "Année de notification|Entité Publique|Entite d'achat|Nom Attributaire|SIRET Attributaire|Date de notification|Code Postal Attributaire|Ville|Nature du Marché|Objet du marché|Tranche budgetaire|Montant| \n";
            Atexo_Util::write_file($fileDestination.$nameFile, $lines, 'w');
            if ($results) {
                $services = [];
                $organismes = [];
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $contrat = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($row['id_contrat_titulaire'])->findOne($connexion);
                    if ($contrat instanceof CommonTContratTitulaire) {
                        $lines = $contrat->getDateNotification('Y').'|';
                        //organisme
                        if (!isset($organismes[$contrat->getOrganisme()])) {
                            $organismes[$contrat->getOrganisme()] = Atexo_Util::replaceRetourALaLigne((new Atexo_Organismes())->getDenominationOrganisme($contrat->getOrganisme()), ' ');
                        }
                        $lines .= $organismes[$contrat->getOrganisme()].'|';
                        //service
                        if (!isset($services[$contrat->getServiceId().'#'.$contrat->getOrganisme()])) {
                            $services[$contrat->getServiceId().'#'.$contrat->getOrganisme()] = Atexo_Util::replaceRetourALaLigne(self::getServiceMarche($contrat->getServiceId(), $contrat->getOrganisme()), ' ');
                        }
                        $lines .= $services[$contrat->getServiceId().'#'.$contrat->getOrganisme()].'|';
                        $lines .= $contrat->getNomTitulaireContrat().'|';
                        $lines .= ($contrat->getSirenTitulaireContrat() ? $contrat->getSirenTitulaireContrat().$contrat->getNicTitulaireContrat() : $contrat->getIdentifiantNationalAttributaire()).'|';
                        $lines .= $contrat->getDateNotification('d/m/Y').'|';
                        $lines .= $contrat->getCodePostalEtablissementContrat().'|';
                        $lines .= $contrat->getVilleEtablissementContrat().'|';
                        $lines .= ((0 != $contrat->getCategorie()) ? (new Atexo_Consultation_Category())->retrieveCatgorieConsultationTraduit($contrat->getCategorie(), $langue) : Prado::localize('DEFINE_TOUS')).'|';
                        //objet
                        $lines .= Atexo_Util::replaceRetourALaLigne($contrat->getObjetContrat(), ' - ').'|';
                        $lines .= $contrat->getLibelleTrancheBudgetaire().'|';
                        //Montant
                        $lines .= ($contrat->afficherMontantContrat() ? Atexo_Util::getMontantArronditEspace($contrat->getMontantContrat()) : '').'|';
                        $lines .= " \n";
                        Atexo_Util::write_file($fileDestination.$nameFile, str_replace(';', ',', utf8_decode(html_entity_decode($lines, ENT_QUOTES))), 'a+');
                        unset($lines);
                    }
                }
            }
            echo "\r\n ===> path file : ".$fileDestination.$nameFile."\r\n";
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public static function getServiceMarche($idService, $org)
    {
        if (0 != $idService) {
            $service = Atexo_EntityPurchase::retrieveEntityById($idService, $org);
            if ($service instanceof CommonService) {
                return $service->getPathServiceTraduit();
            }
        }

        return ' ';
    }
}
