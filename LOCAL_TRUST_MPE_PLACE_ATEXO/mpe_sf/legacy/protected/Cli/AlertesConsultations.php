<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Mise à jour des alertes des consultations.
 *
 * @category Atexo
 */
class Cli_AlertesConsultations extends Atexo_Cli
{
    public static function run()
    {
        $start = ' début de lancement du cron 1 '.date('Y-m-d H:i:s');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $yesterday = Atexo_Util::dateDansFutur(date('Y-m-d'), 0, -1, 0, 0);
        $yesterday = explode(' ', $yesterday);
        $vnull = '';
        $monthAgo = Atexo_Util::dateDansFutur(date('Y-m-d'), 0, 0, -3, 0);

        $crit0 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $vnull, Criteria::NOT_EQUAL);
        $crit1 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $yesterday[0], Criteria::LESS_THAN);

        // Perform AND at level 1 ($crit0 $crit1 )
        $crit0->addAnd($crit1);
        $crit2 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $vnull, Criteria::EQUAL);
        $crit3 = $c->getNewCriterion(CommonConsultationPeer::DATEFIN, $monthAgo, Criteria::LESS_THAN);

        // Perform AND at level 1 ($crit2 $crit3 )
        $crit2->addAnd($crit3);

        // Perform OR at level 0 ($crit0 $crit2 )
        $crit0->addOr($crit2);

        $c->add($crit0);
        $consultations = CommonConsultationPeer::doSelect($c, $connexion);
        foreach ($consultations as $consultation) {
            $resAlerte = Atexo_AlerteMetier::lancerAlerte('CronConsultationResultatAnalyseOrExtraitPV', $consultation);
        }
        $fin = ' Fin de lancement du cron 1 '.date('Y-m-d H:i:s');
        echo $start."\n".$fin."\n";
        self::Display('Done');

        $start = ' début de lancement du cron 2 '.date('Y-m-d H:i:s');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $vnull = '';
        $montantMax = '100000000';
        $c = new Criteria();
        $c->addJoin(CommonDecisionEnveloppePeer::CONSULTATION_ID, CommonConsultationPeer::ID);
        $c->addJoin(CommonConsultationPeer::ORGANISME, CommonDecisionEnveloppePeer::ORGANISME);
        $c->add(CommonDecisionEnveloppePeer::MONTANT_MARCHE, $montantMax, Criteria::GREATER_THAN);
        $c->add(CommonDecisionEnveloppePeer::DATE_FIN_MARCHE_PREVISIONNEL, $vnull, Criteria::NOT_EQUAL);
        $c->add(CommonDecisionEnveloppePeer::DATE_FIN_MARCHE_PREVISIONNEL, date('Y-m-d'), Criteria::GREATER_THAN);
        $consultations = CommonConsultationPeer::doSelect($c, $connexion);

        foreach ($consultations as $consultation) {
            $resAlerte = Atexo_AlerteMetier::lancerAlerte('CronConsultationRapportAchevement', $consultation);
        }
        $fin = ' Fin de lancement du cron 2 '.date('Y-m-d H:i:s');
        echo $start."\n".$fin."\n";
        self::Display('Done');

        $start = ' début de lancement du cron 3 '.date('Y-m-d H:i:s');
        $listConsultation = new CommonConsultationQuery();
        $consultations = $listConsultation->getListConsultationEnAttenteDeMisEenLigne();

        foreach ($consultations as $consultation) {
            $resAlerte = Atexo_AlerteMetier::lancerAlerte('CronConsultationAttenteDeMiseEnLigne', $consultation);
        }
        $fin = ' Fin de lancement du cron 3 '.date('Y-m-d H:i:s');
        echo $start."\n".$fin."\n";
        self::Display('Done');
    }
}
