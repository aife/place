<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Il récupère par organisme la liste des blobs de la table blocFichierEnveloppeTemporaire potentiellement supprimable.
 * Un blob de blocFichierEnveloppeTemporaire est supprimable s'il le fichier existe sur disque et s'il n'est pas référencé dans la table blocFichierEnveloppe.
 *
 * @category Atexo
 */
class Cli_ScriptSuppressionblocFichierEnveloppeTemporaire extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        $help = false;
        $modeReel = false;
        $organisme = null;

        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        for ($i = 2; $i < $nbrArg; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('mode' == $arg[0]) {
                if ('reel' == $arg[1]) {
                    $modeReel = true;
                }
            }

            if ('organisme' == $arg[0]) {
                $organisme = $arg[1];
            }

            if ('help' == $arg[0]) {
                $help = true;
            }
        }
        if ($help) {
            echo "
Parametres :

    mode : reel / simule par defaut simule si pas renseigne
    s'ecrit comme suit mode=reel ou mode=simule

	organisme : acronyme de l'organisme sur lequel est execute le programme
	
	help : aide

Fonctionnement :
	On boucle sur les entrées de la table blocFichierEnveloppeTemporaire correspondant à l'organisme passée en paramètre.
	On vérifie pour chaque entrée si le fichier correspondant au blob existe sur disque.
	Si c'est le cas, on regarde s'il est référencé dans la table blocFichierEnveloppe
		si oui, on efface l'entrée de la table blocFichierEnveloppeTemporaire
		si non, on efface l'entrée de la table blocFichierEnveloppeTemporaire et on le supprime du disuqe
	Si ce n'est pas le cas, on efface l'entrée de la table blocFichierEnveloppeTemporaire et on loggue si ce blob est référencé dans la table blocFichierEnveloppe
	
    si mode simulation , on boucle sur les résultats afin d'alimenter le fichier csv retourné
    si mode réel, on supprime les fichiers sur le disque et si l'opération réussie on supprime de la BD les entrées 
à la fin de l'exécution on affiche les chemins des fichiers de log csv
            ";
        } else {
            if (null == $organisme) {
                echo "le traitement va etre realise sur tous les organismes \n";
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $listOrganismes = CommonOrganismePeer::doSelect(new Criteria(), $connexion);
                foreach ($listOrganismes as $unOrganisme) {
                    echo "le traitement va etre realise sur l'organisme ".$unOrganisme->getAcronyme()."\n";
                    self::doTraitement($unOrganisme->getAcronyme(), $modeReel);
                }
            } else {
                echo "le traitement va etre realise sur l'organisme $organisme\n";
                self::doTraitement($organisme, $modeReel);
            }
        }

        return true;
    }

    public static function doTraitement($organisme, $modeReel)
    {
        $params = [];
        $nbreTotal = 0;

        $tailleTotal = 0;

        $baseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR');

        $sqlBlocFichierEnveloppeTemporaire = <<<QUERY
SELECT id, organisme, uid, type_env, sous_pli, type_fichier, numero_ordre_fichier, numero_ordre_bloc, id_blob FROM `blocFichierEnveloppeTemporaire` 
WHERE 
	blocFichierEnveloppeTemporaire.`organisme` = :organisme	
QUERY;

        $params[':organisme'] = $organisme;
        $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlocFichierEnveloppeTemporaire);
        $statement->execute($params);
        $nbreBlocFichierEnveloppeTemporaire = $statement->rowCount();

        $ligneEntete = "id | organisme | uid | type_env | sous_pli | type_fichier | numero_ordre_fichier | numero_ordre_bloc | id_blob | fichier existe | blocFichierEnveloppe existe | Taille supprimee | STATUT suppression | \n";
        $path = self::getPathLog();
        $sufix = date('Y-m-d').'_'.$organisme;
        if ($modeReel) {
            $file = $path.'/logReelBlocFichierEnveloppeTemporaire_'.$sufix.'.csv';
            echo "\nMode : Reel\n";
        } else {
            $file = $path.'/logSimulationBlocFichierEnveloppeTemporaire_'.$sufix.'.csv';
            echo "\nMode : Simulation\n";
        }
        $logFileGlobal = $path.'/logGlobal_'.date('Y-m-d').'.csv';

        Atexo_Util::write_file($file, $ligneEntete, 'a');
        ob_flush();
        flush();
        echo "\nNombre d'éléments à traiter $nbreBlocFichierEnveloppeTemporaire pour l'organisme $organisme :\n";

        $compteur = 0;
        $listeBlocFichierEnveloppeTemporaire = $statement->fetchAll(PDO::FETCH_ASSOC);
        try {
            foreach ($listeBlocFichierEnveloppeTemporaire as $unBlocFichierEnveloppeTemporaire) {
                ++$compteur;
                echo "\n traitement ".$compteur.' sur '.$nbreBlocFichierEnveloppeTemporaire.' - bloc '.$unBlocFichierEnveloppeTemporaire['organisme'].' '.$unBlocFichierEnveloppeTemporaire['id']."\n";
                $ligneCsv = $unBlocFichierEnveloppeTemporaire['id'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['organisme'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['uid'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['type_env'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['sous_pli'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['type_fichier'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['numero_ordre_fichier'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['numero_ordre_bloc'].'|';
                $ligneCsv .= $unBlocFichierEnveloppeTemporaire['id_blob'].'|';
                $taille = 0;

                try {
                    $return = self::deleteBlocFichierEnveloppeTemporaire(
                        $modeReel,
                        $unBlocFichierEnveloppeTemporaire['id_blob'],
                        $unBlocFichierEnveloppeTemporaire['organisme'],
                        $unBlocFichierEnveloppeTemporaire['uid'],
                        $baseRootDir
                    );
                    $fichierExisteSurDisque = $return['fichierExisteSurDisque'];
                    $blocFichierEnveloppeExiste = $return['blocFichierEnveloppeExiste'];
                    $taille = $return['taille'];
                    $statut = $return['statut'];
                    $tailleTotal += $taille;
                } catch (Exception $e) {
                    $statut = 'ERREUR LORS DU TRAITEMENT DU BLOC '.$unBlocFichierEnveloppeTemporaire['organisme'].' '.$unBlocFichierEnveloppeTemporaire['id'].' '.$e->getMessage().'  '.$e->getTraceAsString();
                }

                $ligneCsv .= $fichierExisteSurDisque.'|';
                $ligneCsv .= $blocFichierEnveloppeExiste.'|';
                $ligneCsv .= $taille.'|';
                $ligneCsv .= $statut."\n";
                Atexo_Util::write_file($file, $ligneCsv, 'a');
                ob_flush();
                flush();
                unset($ligneCsv);
                unset($fichierExisteSurDisque);
                unset($blocFichierEnveloppeExiste);
                unset($taille);
                unset($statut);
                unset($unBlocFichierEnveloppeTemporaire);
                unset($return);
            }
            unset($listeBlocFichierEnveloppeTemporaire);
        } catch (Exception $e) {
            $fileLogErreur = self::getPathLog().'/erreurReel_'.date('Y-m-d').'.log';
            $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString()."\n";
            echo $log;
            Atexo_Util::write_file($fileLogErreur, $log, 'a');
            if (isset($file)) {
                echo 'les blocs traités sont avant erreur sont dans le fichier '.$file."\n";
            }
        }
        echo "\n\n fin de traitement, le fichier généré est enregistré sur ".$file."\n";
        Atexo_Util::write_file($logFileGlobal, $organisme.'|'.$nbreBlocFichierEnveloppeTemporaire.'|'.'|'.$tailleTotal."\n", 'a');
    }

    public static function getPathLog()
    {
        $path = Atexo_Config::getParameter('LOG_DIR').'ScriptSuppressionblocFichierEnveloppeTemporaire';
        if (!is_dir($path)) {
            mkdir($path, '0755');
        }

        return $path;
    }

    private static function deleteBlocFichierEnveloppeTemporaire($modeReel, $idBlocFichierEnveloppeTemporaire, $organisme, $uid, $baseRootDir)
    {
        $params = [];
        //rechercher le blob sur disque et base de données dans la table blocFichierEnveloppe

        $fichierExisteSurDisque = '?';
        $blocFichierEnveloppeExiste = '?';
        $statut = '?';
        $taille = 0;

        $sqlBlocFichierEnveloppe = <<<QUERY
SELECT 
	`id_bloc_fichier`, `organisme`, `id_fichier`, numero_ordre_bloc, id_blob_chiffre, id_blob_dechiffre
FROM
	blocFichierEnveloppe 
WHERE 
	blocFichierEnveloppe.`organisme` = :organisme
AND
	blocFichierEnveloppe.id_blob_chiffre = :id_blob_chiffre
QUERY;

        $params[':organisme'] = $organisme;
        $params[':id_blob_chiffre'] = $idBlocFichierEnveloppeTemporaire;

        $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlocFichierEnveloppe);
        $statement->execute($params);
        $nbreBlocFichierEnveloppe = $statement->rowCount();

        echo "\nIl y a $nbreBlocFichierEnveloppe blocFichierEnveloppe à traiter pour le blob $idBlocFichierEnveloppeTemporaire de l'organisme $organisme\n";
        $tailleTotal = 0;

        if ($nbreBlocFichierEnveloppe < 1) {
            $blocFichierEnveloppeExiste = 'BLOC_NOT_EXIST';
            // si le nombre < 1, supprimer le fichier sur le disque si existant et supprimer l'entrée dans la table blocFichierEnveloppeTemporaire
            $result = self::deleteBlob($modeReel, $idBlocFichierEnveloppeTemporaire, $organisme, $baseRootDir, $uid, $blocFichierEnveloppeExiste);
            $fichierExisteSurDisque = $result['fichierExisteSurDisque'];
            $taille = $result['taille'];
            $statut = $result['statut'];
        } else {
            //si le nombre >= 1, supprimer l'entrée dans la table blocFichierEnveloppeTemporaire
            echo "\n \tAucun traitement sur la table blocFichierEnveloppe, mais suppression de la table blocFichierEnveloppeTemporaire\n";
            $blocFichierEnveloppeExiste = 'BLOC_EXIST';
            $result = self::deleteBlob($modeReel, $idBlocFichierEnveloppeTemporaire, $organisme, $baseRootDir, $uid, $blocFichierEnveloppeExiste);
            $fichierExisteSurDisque = $result['fichierExisteSurDisque'];
            $taille = $result['taille'];
            $statut = $result['statut'];
        }

        unset($nbreBlocFichierEnveloppe);
        unset($statement);

        return ['fichierExisteSurDisque' => $fichierExisteSurDisque, 'blocFichierEnveloppeExiste' => $blocFichierEnveloppeExiste, 'taille' => $taille, 'statut' => $statut];
    }

    private static function deleteBlob($modeReel, $idBlocFichierEnveloppeTemporaire, $organisme, $baseRootDir, $uid, $blocFichierEnveloppeExiste)
    {
        $params = [];
        $file = null;
        $pathFile = $baseRootDir.'/'.$organisme.'/files/'.$idBlocFichierEnveloppeTemporaire.'-0';
        if (!file_exists($pathFile)) {
            $pathFile = Atexo_Blob::getPathFile($idBlocFichierEnveloppeTemporaire, $organisme);
        }

        $statut = 'DELETE KO';
        $fichierExisteSurDisque = '?';
        $taille = 0;
        try {
            $fileExist = is_file($pathFile);
            if ($fileExist) {
                $taille = filesize($pathFile);
                echo 'le chemin du fichier à supprimer est '.$pathFile.' et sa taille est de '.$taille."\n";
                $fichierExisteSurDisque = 'FILE_EXIST';
            } else {
                echo "le fichier n'existe pas sur le disque, la suppression de la base de donnees va être réalisée\n";
                $fichierExisteSurDisque = 'FILE_NOT_EXIST';
            }
            if ($modeReel && 'BLOC_NOT_EXIST' === $blocFichierEnveloppeExiste) {
                if (unlink($pathFile) || !$fileExist) {
                     $sqlBlobFile = <<<QUERY
DELETE FROM
blobOrganisme_file
WHERE
	blobOrganisme_file.id = :idBlob
AND
	blobOrganisme_file.organisme like :organisme
QUERY;

                    $sqlBlocFichierEnveloppeTemporaire = <<<QUERY
DELETE FROM
blocFichierEnveloppeTemporaire
WHERE
	blocFichierEnveloppeTemporaire.uid = :uid
AND
	blocFichierEnveloppeTemporaire.organisme like :organisme
QUERY;

                    $params['idBlob'] = $idBlocFichierEnveloppeTemporaire;
                    $params['organisme'] = $organisme;

                    $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlobFile);
                    $statement->execute($params);
                    unset($statement);
                    unset($params);

                    $params['organisme'] = $organisme;
                    $params['uid'] = $uid;
                    $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlocFichierEnveloppeTemporaire);
                    $statement->execute($params);
                    unset($statement);
                    unset($params);
                    $statut = 'DELETE OK';
                }
            } else {
                $statut = 'DELETE STBY';
            }

            return ['fichierExisteSurDisque' => $fichierExisteSurDisque, 'taille' => $taille, 'statut' => $statut];
        } catch (Exception $e) {
            $fileLogErreur = self::getPathLog().'/erreurReel_'.date('Y-m-d').'.log';
            $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString()."\n";
            echo $log;
            Atexo_Util::write_file($fileLogErreur, $log, 'a');
            if (isset($file)) {
                echo 'les blocs traités sont avant erreur sont dans le fichier '.$file."\n";
            }
        }
    }
}
