<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use DOMDocument;
use Exception;
use PDO;

/**
 * Cron pour le path des services.
 *
 * @author salwa drissi  <salwa.drissi@atexo.com>
 * @copyright Atexo 2016
 *
 * @category Atexo
 */
class Cli_Redressement extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter un param : \r\n";
            echo "-> redressementTrancheBudgetaire  redressement t_contrat_titulaire : MPE-1744 \r\n";
            echo "-> redressementOffreAvec2Envelop  redressement table Offres dans le cas de deux enveloppes de meme type : MPE-3663 \r\n";
            echo "-> supprimerSignatureXmlReponse  supprimer balise signature pour les xml_string > 100000 traitement des 90 , 1ere \r\n";
            echo "-> recupBlocsChiffres  recup les blobc chifrés par id offre \r\n";
        }
        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                if ('redressementTrancheBudgetaire' == $arguments[$i]) {
                    self::redressementTrancheBudgetaire();
                }
                if ('redressementOffreAvec2Envelop' == $arguments[$i]) {
                    self::redressementOffreAvec2Envelop();
                }
                if ('supprimerSignatureXmlReponse' == $arguments[$i]) {
                    self::supprimerSignatureXmlReponse();
                }

                if ('recupBlocsChiffres' == $arguments[$i]) {
                    self::recupBlocsChiffres($arguments[$i + 1]);
                }
            }
        }
    }

    public static function supprimerSignatureXmlReponse()
    {
        try {
            echo " Debut Traitement \n";
            $updateReq = '';
            $query = <<<QUERY
        SELECT untrusteddate, consultation_id, CHAR_LENGTH( `xml_string` ),id,xml_string
FROM `Offres`
WHERE `Offres`.`untrusteddate` > '2017-03-01 00:00:00'
AND CHAR_LENGTH( `xml_string` ) >100000
AND statut_offres =1
ORDER BY CHAR_LENGTH( `xml_string` ) DESC
LIMIT 0,90
QUERY;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute([]);
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                echo 'ref Consultation : '.$row['consultation_id'].' id offre : '.$row['id']."\n";
                $doc = new DOMDocument();
                $doc->loadXML($row['xml_string']);

                $xmlsign = $doc->documentElement;

                $fichiers = $xmlsign->getElementsByTagName('Fichier');
                foreach ($fichiers as $fichier) {
                    $sigs = $fichier->getElementsByTagName('Signature');
                    foreach ($sigs as $sig) {
                        $fichier->removeChild($sig);
                    }
                }
                $newXml = $doc->saveXML();

                $updateReq .= "UPDATE Offres SET xml_string='".addslashes($newXml)."' \n where id = '".$row['id']."'; \n";
            }
            $fileLogs = Atexo_Config::getParameter('LOG_DIR').'logSupprimerSignatureXmlReponse_'.date('Y_m_d_his').'.log';
            Atexo_Util::write_file($fileLogs, $updateReq);
            echo ' Fin traitement voir log :'.$fileLogs."\n";
        } catch (Exception $e) {
            Propel::log(' - erreur exécution - supprimerSignatureXmlReponse '.$e->getMessage(), Propel::LOG_ERR);
        }
    }

    public static function redressementOffreAvec2Envelop()
    {
        try {
            $updateReq = '';
            $deletReq = '';
            $updateReqOffres = '';
            $log = " Debut Traitement \n";
            echo $log;
            $query = <<<QUERY
        		SELECT O.organisme,O.xml_string,O.untrusteddate,O.statut_offres ,E1.offre_id,E1.id_enveloppe_electro AS id_env1,
        		E1.statut_enveloppe AS statut_env1,E1.type_env AS type_env1,E1.sous_pli AS sous_pli1,
        		E2.id_enveloppe_electro AS id_env2,E2.statut_enveloppe AS statut_env2,E2.type_env AS type_env2,E2.sous_pli AS sous_pli2,
        		E2.`dateheure_ouverture` AS date_ouvert2,E2.agent_id_ouverture AS id_agent_ouv2,E1.`dateheure_ouverture` AS date_ouvert1,E1.agent_id_ouverture AS id_agent_ouv1
        		FROM Enveloppe E2, Enveloppe E1 ,Offres O
        		WHERE O.id=E1.offre_id AND O.untrusteddate >"2017-03-01 00:00:00" AND O.statut_offres!='99' AND E2.offre_id=E1.offre_id
        		AND E2.id_enveloppe_electro>E1.id_enveloppe_electro AND O.xml_string!=''
        		AND E1.type_env=E2.type_env AND E1.sous_pli = E2.sous_pli AND E1.organisme=E2.organisme ORDER BY `O`.`untrusteddate` DESC
QUERY;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute([]);
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $log1 = 'id Offre : '.$row['offre_id'].', statut offre : '.$row['statut_offres'].
                    "\nid enveloppe 1  : ".$row['id_env1'].', statut enveloppe 1 : '.$row['statut_env1'].
                    ', type enveloppe 1 : '.$row['type_env1'].', sous pli enveloppe 1 (lot) : '.$row['sous_pli1'].
                    "\nid enveloppe 2  : ".$row['id_env2'].', statut enveloppe 2 : '.$row['statut_env2'].
                    ', type enveloppe 2 : '.$row['type_env2'].', sous pli enveloppe 2 (lot) : '.$row['sous_pli2'].
                    " \n";
                echo $log1;
                $log .= $log1;
                $doc = new DOMDocument();
                $doc->loadXML($row['xml_string']);

                $xmlsign = $doc->documentElement;

                $enveloppes = $xmlsign->getElementsByTagName('Enveloppe');
                foreach ($enveloppes as $enveloppe) {
                    $idEnveloppe = $enveloppe->getAttribute('idEnveloppe');
                    $numLot = $enveloppe->getAttribute('numLot');
                    $type = $enveloppe->getAttribute('type');
                    if ($numLot == $row['sous_pli1'] && $type == $row['type_env1']) {
                        if ($idEnveloppe == $row['id_env1']) {
                            $updateReq .= "UPDATE fichierEnveloppe SET id_enveloppe='".addslashes($idEnveloppe)."' where id_enveloppe = '".$row['id_env2']."'; \n";
                            $deletReq .= "DELETE FROM Enveloppe WHERE id_enveloppe_electro='".$row['id_env2']."';\n";
                        //$dateOuv = $row['date_ouvert1'];
                            //$idAgentOuvert = $row['id_agent_ouv1'];
                        } elseif ($idEnveloppe == $row['id_env2']) {
                            $updateReq .= "UPDATE fichierEnveloppe SET id_enveloppe='".addslashes($idEnveloppe)."' where id_enveloppe = '".$row['id_env1']."'; \n";
                            //$dateOuv = $row['date_ouvert2'];
                            //$idAgentOuvert = $row['id_agent_ouv2'];
                            $deletReq .= "DELETE FROM Enveloppe WHERE id_enveloppe_electro='".$row['id_env1']."';\n";
                        }
                    }
                }
                $updateReqOffres .= self::initReqFermerOffre($row['offre_id'], $row['organisme']).";\n";
            }

            $fileLogs = Atexo_Config::getParameter('LOG_DIR').'redressementOffreAvec2Envelop'.date('Y_m_d_his').'.log';
            $fileUpdateReq = Atexo_Config::getParameter('LOG_DIR').'1_update_fichierEnveloppe_'.date('Y_m_d_his').'.sql';
            $fileUpdateReqOffres = Atexo_Config::getParameter('LOG_DIR').'3_update_Offres_'.date('Y_m_d_his').'.sql';
            $fileDeletReq = Atexo_Config::getParameter('LOG_DIR').'2_delete_enveloppe_'.date('Y_m_d_his').'.sql';
            echo ' Fin traitement voir log :'.$fileLogs."\n";
            $log .= ' Fin traitement voir log :'.$fileLogs.' Pour les requetes voir : '.$fileUpdateReq.' ET '.$fileDeletReq." ET $fileUpdateReqOffres \n";
            Atexo_Util::write_file($fileLogs, $log);
            Atexo_Util::write_file($fileUpdateReq, $updateReq);
            Atexo_Util::write_file($fileDeletReq, $deletReq);
            Atexo_Util::write_file($fileUpdateReqOffres, $updateReqOffres);
        } catch (Exception $e) {
            Propel::log(' - erreur exécution - redressementOffreAvec2Envelop '.$e->getMessage(), Propel::LOG_ERR);
        }
    }

    public static function initReqFermerOffre($idOffre, $organisme)
    {
        $statutFerme = Atexo_Config::getParameter('STATUT_ENV_FERME');
        $statutEncoursDechiffrement = Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS');
        $query = <<<QUERY
		UPDATE Offres O SET statut_offres ='2' WHERE id = $idOffre AND (
SELECT 	count(*)
FROM	Enveloppe
WHERE	Enveloppe.offre_id=$idOffre	AND Enveloppe.organisme='$organisme'
AND	(	Enveloppe.statut_enveloppe=$statutFerme OR	Enveloppe.statut_enveloppe=$statutEncoursDechiffrement )	)=0
QUERY;

        return $query;
    }

    public static function redressementTrancheBudgetaire()
    {
        $c = new Criteria();
        $c->add(CommonTContratTitulairePeer::DATE_NOTIFICATION, null, Criteria::ISNOTNULL);
        $c->addAnd(CommonTContratTitulairePeer::DATE_NOTIFICATION, '2015-01-01', Criteria::GREATER_EQUAL);
        $c->addAnd(CommonTContratTitulairePeer::DATE_NOTIFICATION, '2016-12-31', Criteria::LESS_EQUAL);

        $contratTitulaire = CommonTContratTitulairePeer::doSelect($c);
        $log = " Debut traitement pour les contrats avec date notification not NULL et entre 2015-01-01- et 2016-12-31 \r\n ";
        echo $log;
        $log = " ID CONTRAT | DATE NOTIFICATION | DATE PREVUE NOTIFICATION | MONTANT | ANNEE | ORG | ID TRANCHE ACTUEL | NOUVEAU ID TRANCHE  \r\n ";
        $ch = '';
        $trancheTrouves = 0;
        $tranchemodifies = 0;
        $trancheNonTrouves = 0;
        $trancheNonModifie = 0;
        foreach ($contratTitulaire as $contrat) {
            $ch = $contrat->getIdContratTitulaire().' | '.$contrat->getDateNotification('d/m/Y').'|'.$contrat->getDatePrevueNotification('d/m/Y').'| '.$contrat->getMontantContrat().' | '.$contrat->getDateNotification('Y').' | '.$contrat->getOrganisme().' | '.$contrat->getIdTrancheBudgetaire()." \r\n";
            $idTranche = (new Atexo_TrancheBudgetaire())->getIdTrancheByMontant($contrat->getMontantContrat(), $contrat->getDateNotification('Y'), $contrat->getOrganisme());
            $ch .= ' | '.$idTranche.' ';
            if ('' != $idTranche && $idTranche) {
                ++$trancheTrouves;
                if ($contrat->getIdTrancheBudgetaire() != $idTranche) {
                    ++$tranchemodifies;
                    $contrat->setIdTrancheBudgetaire($idTranche);
                    $contrat->save();
                } else {
                    ++$trancheNonModifie;
                }
            } else {
                ++$trancheNonTrouves;
            }
            echo $ch;
            $log .= $ch;
        }

        $c = new Criteria();
        $c->add(CommonTContratTitulairePeer::DATE_NOTIFICATION, null, Criteria::ISNULL);
        $c->addAnd(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, null, Criteria::ISNOTNULL);
        $c->addAnd(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, '2015-01-01', Criteria::GREATER_EQUAL);
        $c->addAnd(CommonTContratTitulairePeer::DATE_PREVUE_NOTIFICATION, '2016-12-31', Criteria::LESS_EQUAL);
        $contratTitulaire = CommonTContratTitulairePeer::doSelect($c);

        foreach ($contratTitulaire as $contrat) {
            $ch = $contrat->getIdContratTitulaire().' | '.$contrat->getDateNotification('d/m/Y').'|'.$contrat->getDatePrevueNotification('d/m/Y').'| '.$contrat->getMontantContrat().' | '.$contrat->getDateNotification('Y').' | '.$contrat->getOrganisme().' | '.$contrat->getIdTrancheBudgetaire()." \r\n";
            $idTranche = (new Atexo_TrancheBudgetaire())->getIdTrancheByMontant($contrat->getMontantContrat(), $contrat->getDatePrevueNotification('Y'), $contrat->getOrganisme());
            $ch .= ' | '.$idTranche.' ';
            if ('' != $idTranche && $idTranche) {
                ++$trancheTrouves;
                if ($contrat->getIdTrancheBudgetaire() != $idTranche) {
                    ++$tranchemodifies;
                    $contrat->setIdTrancheBudgetaire($idTranche);
                    $contrat->save();
                } else {
                    ++$trancheNonModifie;
                }
            } else {
                ++$trancheNonTrouves;
            }
            echo $ch;
            $log .= $ch;
        }
        $log .= " Fin traitement \r\n";
        $log .= $trancheTrouves." tranches traitées \r\n";
        $log .= $tranchemodifies." tranches modifiées \r\n";
        $log .= $trancheNonModifie." tranches non modifiées \r\n";
        $log .= $trancheNonTrouves." tranches non trouvées \r\n";

        echo $trancheTrouves." tranches traitées \r\n";
        echo $tranchemodifies." tranches modifiées \r\n";
        echo $trancheNonModifie." tranches non modifiées \r\n";
        echo $trancheNonTrouves." tranches non trouvées \r\n";

        $fileLogs = Atexo_Config::getParameter('LOG_DIR').'logRedressementTrancheBudetaire_'.date('Y_m_d_his').'.log';
        Atexo_Util::write_file($fileLogs, $log, 'a+');
        echo ' Fin traitement voir log :'.$fileLogs;
    }

    public static function recupBlocsChiffres($idOffre)
    {
        try {
            if (!$idOffre) {
                echo " - erreur exécution - recup bloc chifre => id offre manquant  \n";
                Propel::log(' -  erreur exécution - recup bloc chifre => id offre manquant ', Propel::LOG_ERR);

                return false;
            }
            echo " Debut Traitement \n";
            $updateReq = '';
            $query = <<<QUERY
        SELECT untrusteddate, consultation_id, CHAR_LENGTH( `xml_string` ),id,xml_string,organisme
FROM `Offres`
WHERE id = $idOffre
QUERY;

            $path = Atexo_Config::getParameter('COMMON_TMP').time().'_offre_'.$idOffre.'/';
            if (0 != Atexo_Files::createDir($path)) {
                echo ' - erreur exécution - recup bloc chifre => createDir : '.$path."\n";
                Propel::log(' - erreur exécution - recup bloc chifre => createDir : '.$path, Propel::LOG_ERR);

                return false;
            }
            $destZipPath = $path.$idOffre.'.zip';

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute([]);
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                echo 'ref Consultation : '.$row['consultation_id'].' id offre : '.$row['id']."\n";
                $doc = new DOMDocument();
                $doc->loadXML($row['xml_string']);

                $xmlsign = $doc->documentElement;

                $fichiers = $xmlsign->getElementsByTagName('Fichier');
                foreach ($fichiers as $fichier) {
                    $blocsChiffrements = $fichier->getElementsByTagName('BlocsChiffrement');
                    foreach ($blocsChiffrements as $bloc) {
                        $elementBloc = $bloc->getElementsByTagName('BlocChiffrement');
                        foreach ($elementBloc as $unBloc) {
                            $idBlob = $unBloc->getAttribute('nomBloc');
                            if (0 != Atexo_Blob::copyBlob($idBlob, $path.$idBlob, $row['organisme'])) {
                                echo ' - erreur exécution - recup bloc chifre => copyBlob id blob : '.$idBlob."\n";
                                Propel::log(' - erreur exécution - recup bloc chifre => copyBlob id blob : '.$idBlob, Propel::LOG_ERR);

                                return false;
                            }
                            if ('' === (new Atexo_Zip())->addFileToZip($path.$idBlob, $destZipPath)) {
                                echo ' - erreur exécution - recup bloc chifre => addFilleToZip : '.$idBlob."\n";
                                Propel::log(' - erreur exécution - recup bloc chifre => addFilleToZip : '.$idBlob, Propel::LOG_ERR);

                                return false;
                            }
                        }
                    }
                }
                file_put_contents($path.'ReponsesAnnonce.xml', $row['xml_string']);
                if ('' === (new Atexo_Zip())->addFileToZip($path.'ReponsesAnnonce.xml', $destZipPath)) {
                    echo " - erreur exécution - recup bloc chifre => addFilleToZip : ReponsesAnnonce.xml\n";
                    Propel::log(' - erreur exécution - recup bloc chifre => addFilleToZip : ReponsesAnnonce.xml', Propel::LOG_ERR);

                    return false;
                }

                echo 'Voir => '.$destZipPath." \n";
            }
        } catch (Exception $e) {
            echo '- erreur exécution - supprimerSignatureXmlReponse '.$e->getMessage()."\n";
            Propel::log(' - erreur exécution - supprimerSignatureXmlReponse '.$e->getMessage(), Propel::LOG_ERR);
        }
    }
}
