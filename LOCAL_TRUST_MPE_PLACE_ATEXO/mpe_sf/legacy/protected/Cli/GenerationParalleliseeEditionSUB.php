<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Exception;

/*
 * Created on 27 juin 2013
 *
 * @author César BOULET <cesar.boulet@atexo.com>
 * @package
 */
class Cli_GenerationParalleliseeEditionSUB extends Atexo_Cli
{
    public static function run()
    {
        $nbreEditionATraiter = '5';

        $arguments = $_SERVER['argv'];
        if ($arguments['2']) {
            $nbreEditionATraiter = $arguments['2'];
        }

        $cliName = self::class.'_'.$nbreEditionATraiter;

        self::writeToLog("--> INFO : Démarrage demande générations éditions SUB avec $nbreEditionATraiter édition(s) en paramètre à traiter");
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $listEditionEnCoursTraitement = [];
        if (self::startLancementUniqueCli($cliName)) {
            try {
                $listEdition = (new Atexo_FormulaireSub_Edition())->recupererEditionsAGenerer($nbreEditionATraiter);
                self::writeToLog('--> INFO : il y a '.(is_countable($listEdition) ? count($listEdition) : 0).' éditions qui ont été récupérées et qui vont être traitées (STATUT_EDITION_A_GENERER)');
                try {
                    $connexion->beginTransaction();
                    foreach ($listEdition as $Edition) {
                        //on s'assure que l'édition est bien a généré avant de la basculer en cours de traitement
                        if ($Edition->getStatutGeneration() == Atexo_Config::getParameter('STATUT_EDITION_A_GENERER')) {
                            //permet d'indiquer que l'édition est en cours de traitement et ainsi ne va pas être traité par un autre cron lancé en //
                            $Edition->setStatutGeneration(Atexo_Config::getParameter('STATUT_EDITION_EN_COURS_GENERATION'));
                            $Edition->save($connexion);
                            $listEditionEnCoursTraitement[] = $Edition;
                            self::writeToLog("--> INFO : l'edition ".$Edition->getIdEditionFormulaire().' passe au STATUT_EDITION_EN_COURS_GENERATION avec succes');
                        }
                        //l'edition est passée à un autre statut entre temps alors on ne la traite pas
                        else {
                            self::writeToLog("--> WARN : l'edition ".$Edition->getIdEditionFormulaire().' est au statut '.$Edition->getStatutGeneration().', elle ne va pas être traitée');
                        }
                    }
                    $connexion->commit();
                } catch (Exception $e) {
                    $connexion->rollBack();
                    self::stopLancementUniqueCli($cliName);
                    self::writeToLog("--> ERROR : Une exception est survenu pendant le traitement des éditions (passage de STATUT_EDITION_A_GENERER a STATUT_EDITION_EN_COURS_GENERATION), ROLLBACK FAIT : message erreur = $e->getMessage() ");
                }

                if (count($listEditionEnCoursTraitement) > 0) {
                    self::writeToLog('--> INFO : il y a '.count($listEditionEnCoursTraitement).' éditions à traiter (STATUT_EDITION_EN_COURS_GENERATION)');
                    try {
                        while ($Edition = array_shift($listEditionEnCoursTraitement)) {
                            //Génération de l'édition
                            $time_start = microtime(true);
                            self::writeToLog("--> INFO : demarrage de la generation de l'édition ".$Edition->getIdEditionFormulaire());
                            (new Atexo_FormulaireSub_Edition())->genererEdition($Edition);
                            self::writeToLog("--> INFO : fin de la generation de l'édition ".$Edition->getIdEditionFormulaire());
                            $time_end = microtime(true);
                            $time = self::GetElapsedTime(round($time_end - $time_start, 4));
                            self::writeToLog("--> INFO : durée de la generation de l'édition ".$Edition->getIdEditionFormulaire().' est de '.$time);
                        }
                    } catch (Exception $e) {
                        self::writeToLog("--> ERROR : Une exception est survenu pendant le traitement des éditions (STATUT_EDITION_EN_COURS_GENERATION), message erreur = $e->getMessage() ");
                        self::writeToLog('--> INFO : il y a '.count($listEditionEnCoursTraitement).' à rebasculer au STATUT_EDITION_A_GENERER');

                        while ($Edition = array_shift($listEditionEnCoursTraitement)) {
                            $Edition->setStatutGeneration(Atexo_Config::getParameter('STATUT_EDITION_A_GENERER'));
                            $Edition->save($connexion);
                            self::writeToLog("--> INFO : l'edition $Edition->getIdEditionFormulaire() passe au STATUT_EDITION_A_GENERER avec succes");
                        }
                        self::stopLancementUniqueCli($cliName);
                    }
                }
                self::stopLancementUniqueCli($cliName);
                self::writeToLog('--> INFO : Fin du traitement de la generation des editions');
            } catch (Exception $e) {
                self::stopLancementUniqueCli($cliName);
                self::writeToLog("--> ERROR : Une exception est survenu avant le démarrage du traitement des éditions, message erreur = $e->getMessage() ");
            }
        }
    }

    private static function GetElapsedTime($seconds)
    {
        $time = [];
        $elapsed = '';
        $milli = $seconds - intval($seconds);
        $temp = $seconds % 3600;
        $time[0] = intval(($seconds - $temp) / 3600);
        $time[2] = $temp % 60;
        $time[1] = ($temp - $time[2]) / 60;
        if ($time[0] > 0) {
            $elapsed .= "{$time[0]} hrs";
        }
        if ($time[1] > 0) {
            $elapsed .= " {$time[1]} min ";
        }
        $elapsed .= ' '.($time[2] + $milli).' sec';

        return str_replace('  ', ' ', $elapsed);
    }

    private static function writeToLog($messageErreur, $messageException = '')
    {
        $log = "\n\t".date('Y-m-d H:i:s').$messageErreur.' '.$messageException;
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), $filePath, $log, 'a');
    }
}
