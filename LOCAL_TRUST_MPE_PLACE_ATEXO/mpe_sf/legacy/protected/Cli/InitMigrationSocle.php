<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonAffiliationServiceQuery;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentServiceMetierQuery;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonResponsableengagementQuery;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Controller\Cli;
use Prado\Prado;
use Prado\Util\TLogger;

/*
 * Cron initialisant les champs "id_initial" pour la migration de donnees
 * @author Ameline PLACIDE <ameline.placide@atexo.com> @copyright Atexo 2014
 * @package Atexo_Cli @subpackage Cron
 */
class Cli_InitMigrationSocle extends Atexo_Cli
{
    public static function run()
    {
        // Debut du traitement
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Debut du traitement : initialisation des champs "id_initial" pour la migration du socle'.
                         "\n"
        );
        Prado::log(
            'Debut du traitement : initialisation des champs "id_initial" pour la migration du socle',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );

        // Mise a jour des organismes
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Mise a jour des organismes'."\n"
        );
        Prado::log(
            'Mise a
          jour des organismes',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateOrganismes();
        Prado::log(
            'Fin de la mise a jour des organismes',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s').
                         "\t".'Fin de la mise a jour des organismes'."\n"
        );

        // Mise a jour des services
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Mise a jour des services'."\n"
        );
        Prado::log(
            'Mise a jour des services',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateServices();
        Prado::log(
            'Fin de la mise a jour des services',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Fin de la mise a jour des services'."\n"
        );

        // Mise a jour des affiliations services
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Mise a jour des affiliations pole/service'."\n"
        );
        Prado::log(
            'Mise a jour des affiliations pole/service',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateAffiliationService();
        Prado::log(
            'Fin de la mise a jour
          des affiliations pole/service',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Fin de la mise a jour des affiliations pole/service'."\n"
        );

        // Mise a jour des agents
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Mise a jour des agents'."\n"
        );
        Prado::log(
            'Mise a jour des agents',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateAgents();
        Prado::log(
            'Fin de la mise a jour des agents',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Fin de la mise a jour des agents'.
                         "\n"
        );

        // Mise a jour des entreprises
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Mise a jour des entreprises'."\n"
        );
        Prado::log(
            'Mise a jour des  entreprises ',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateEntreprises();
        Prado::log(
            'Fin de la mise a jour des  entreprises ',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Fin de la mise a jour des entreprises'."\n"
        );

        // Mise a jour des inscrits
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Mise a jour des inscrits'."\n"
        );
        Prado::log(
            'Mise a jour des  inscrits ',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateInscrits();
        Prado::log(
            'Fin de la mise a jour des inscrits ',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Fin de la mise a jour des inscrits'.
                         "\n"
        );

        // Mise a jour des acces des agents aux applications
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Mise a jour des acces des agents aux applications'.
                         "\n"
        );
        Prado::log(
            'Mise a jour des acces des agents aux applications',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateAgentAccesApplications();
        Prado::log(
            'Fin de la mise a jour des acces des agents aux applications',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Fin de la mise a jour des acces des agents aux applications'.
                         "\n"
        );

        // Mise a jour des inscrits
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Mise a jour des managers des entreprises'."\n"
        );
        Prado::log(
            'Mise a jour des managers des entreprises',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        self::updateEntrepriseManagers();
        Prado::log(
            'Fin de la mise a jour des managers des entreprises',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".
                         'Fin de la mise a jour des managers des entreprises'.
                         "\n"
        );

        // Fin de traitement
        fwrite(
            STDOUT,
            date('Y-m-d H:i:s')."\t".'Fin du traitement'."\n"
        );
        Prado::log(
            'Fin du traitement',
            TLogger::INFO,
            'Atexo.Cli.Cron.InitMigrationSocle'
        );
    }

    public static function updateOrganismes()
    {
        $organismes = CommonOrganismeQuery::create()->distinct()
            ->orderById()
            ->find();

        $id_initial = 1;

        foreach ($organismes as $organisme) {
            $organisme->setIdInitial($id_initial);
            ++$id_initial;
            $organisme->save();
        }
    }

    public static function updateServices()
    {
        $organismes = CommonOrganismeQuery::create()->distinct()
            ->orderById()
            ->find();

        $id_initial = 1;

        foreach ($organismes as $organisme) {
            $services = CommonServiceQuery::create()->filterByOrganisme(
                $organisme->getAcronyme()
            )
                ->orderById()
                ->find();

            foreach ($services as $service) {
                $service->setIdExterne($id_initial++);
                $service->save();
            }
        }
    }

    public static function updateAffiliationService()
    {
        $organismes = CommonOrganismeQuery::create()->distinct()
            ->orderById()
            ->find();

        foreach ($organismes as $organisme) {
            $affiliations = CommonAffiliationServiceQuery::create()->filterByOrganisme(
                $organisme->getAcronyme()
            )
                ->orderByIdPole()
                ->find();

            foreach ($affiliations as $affiliation) {
                // Recuperation du service
                $service = CommonServiceQuery::create()->filterByOrganisme(
                    $organisme->getAcronyme()
                )
                    ->filterById($affiliation->getIdService())
                    ->findOne();

                // Recuperation du pole
                $pole = CommonServiceQuery::create()->filterByOrganisme(
                    $organisme->getAcronyme()
                )
                    ->filterById($affiliation->getIdPole())
                    ->findOne();

                // Mise a jour de l'affiliation avec les valeurs de id_initial
                if ($service && $pole && $service->getIdExterne() && $pole->getIdExterne()) {
                    $affiliation->setIdService($service->getIdExterne());
                    $affiliation->setIdPole($pole->getIdExterne());
                    $affiliation->save();
                }
            }
        }
    }

    public static function updateAgents()
    {
        $agents = CommonAgentQuery::create()->orderByOrganisme()
            ->orderById()
            ->find();

        $id_initial = 1;

        foreach ($agents as $agent) {
            if ($agent->getServiceId()) {
                $service = CommonServiceQuery::create()->filterByOrganisme(
                    $agent->getOrganisme()
                )
                    ->filterById($agent->getServiceId())
                    ->findOne();

                if ($service && $service->getIdExterne()) {
                    $agent->setServiceId($service->getIdExterne());
                }
            } else {
                $agent->setServiceId(0);
            }

            $agent->setIdExterne($id_initial++);
            $agent->save();
        }
    }

    public function updateAgentAccesApplications()
    {
        $agentServiceMetiers = CommonAgentServiceMetierQuery::create()->orderByIdAgent()->find();

        foreach ($agentServiceMetiers as $agentServiceMetier) {
            $agent = CommonAgentQuery::create()->filterById($agentServiceMetier->getIdAgent())
                ->findOne();

            if ($agent && $agent->getIdExterne()) {
                $agentServiceMetier->setIdAgent($agent->getIdExterne());
                $agentServiceMetier->save();
            }
        }
    }

    public static function updateEntreprises()
    {
        $entreprises = EntrepriseQuery::create()->orderById()->find();

        $id_initial = 1;

        foreach ($entreprises as $entreprise) {
            $entreprise->setIdInitial($id_initial++);
            $entreprise->save();
        }
    }

    public function updateEntrepriseManagers()
    {
        $entrepriseManagers = CommonResponsableengagementQuery::create()->orderById()->find();

        foreach ($entrepriseManagers as $entrepriseManager) {
            $entreprise = EntrepriseQuery::create()->filterById(
                $entrepriseManager->getEntrepriseId()
            )
                ->findOne();

            if ($entreprise && $entreprise->getIdInitial()) {
                $entrepriseManager->setEntrepriseId($entreprise->getIdInitial());
                $entrepriseManager->save();
            }
        }
    }

    public static function updateInscrits()
    {
        $inscrits = CommonInscritQuery::create()->orderById()->find();

        $id_initial = 1;

        foreach ($inscrits as $inscrit) {
            $entreprise = EntrepriseQuery::create()->filterById(
                $inscrit->getEntrepriseId()
            )
                ->findOne();

            if ($entreprise && $entreprise->getIdInitial()) {
                $inscrit->setEntrepriseId($entreprise->getIdInitial());
            }

            $inscrit->setIdInitial($id_initial++);
            $inscrit->save();
        }
    }
}
