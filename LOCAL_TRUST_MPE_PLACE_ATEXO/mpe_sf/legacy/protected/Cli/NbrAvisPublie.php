<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonHistoriqueNbrConsultationsPubliees;
use Application\Propel\Mpe\CommonHistoriqueNbrConsultationsPublieesQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

class Cli_NbrAvisPublie extends Atexo_Cli
{
    public static function run()
    {
        try {
            echo '-------------Debut du traitement-------------'.PHP_EOL;
            echo 'recuperation du nombre des avis publie durant la journée '.PHP_EOL;
            $typeAvisConsultation = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
            $sql = <<<SQL
            SELECT COUNT(reference) AS NbrAvis
            FROM consultation
            WHERE date_mise_en_ligne_calcule >= (SELECT concat(subdate(current_date, 1 ) ,' 00:00'))
            AND date_mise_en_ligne_calcule <= (SELECT concat(subdate(current_date, 1 ) ,' 23:59'))
            AND date_mise_en_ligne_calcule is not null
            AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%'
            AND ID_TYPE_AVIS = $typeAvisConsultation
SQL;
            $nbrAvisHier = $nbrAvisPublies = 0;
            $datePublication = date('Y-m-d');
            $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $nbrAvisPublies = $row['NbrAvis'];
            }
            $historiqueHier = CommonHistoriqueNbrConsultationsPublieesQuery::create()->orderByIdHistoriqueNbrCons('DESC')->findOne();
            if ($historiqueHier instanceof CommonHistoriqueNbrConsultationsPubliees) {
                $nbrAvisHier = $historiqueHier->getNbrConsultationsPubliees();
                $dateLastHitorique = $historiqueHier->getDatePublication('%Y-%m-%d');
            }
            echo "sauvegarde de l'historique du nombre des avis publies ".PHP_EOL;
            $historiqueNbrAvis = new CommonHistoriqueNbrConsultationsPubliees();
            $historiqueNbrAvis->setDatePublication($datePublication);
            $historiqueNbrAvis->setNbrConsultationsPubliees($nbrAvisPublies);
            $historiqueNbrAvis->save();

            echo 'Comparaison des avis publies '.PHP_EOL;
            echo "  =>nombre d'avis publies aujourd'hui le $datePublication => ".$nbrAvisPublies.PHP_EOL;
            echo "  =>nombre d'avis publies hier le $dateLastHitorique => ".$nbrAvisHier.PHP_EOL;
            $delta = abs(($nbrAvisPublies - $nbrAvisHier) / $nbrAvisPublies);
            if ($delta >= Atexo_Config::getParameter('SEUIL_DELTA_AVIS_PUBLIES')) {
                $destinataire = Atexo_Config::getParameter('DESTINATAIRES_NBR_AVIS_PUBLIES');
                $objet = "[ERREUR] : Nombre d'avis mise en place";
                $corpsMail = <<<TXT
Bonjour,
une erreur de récupération des annonces a potentiellement eu lieu,
le nombre d'avis publiés aujourd'hui le $datePublication est => $nbrAvisPublies
le nombre d'avis publiés hier le $dateLastHitorique est => $nbrAvisHier
TXT;
                $message = Atexo_Message::getGenericEmailFormat($corpsMail);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $destinataire, $objet, $message, '', '', false, true);
            }
            echo '-------------Fin du traitement-------------'.PHP_EOL;
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_CONS_PUBLIEES').'/logNbrConsPubliees.log', date('Y-m-d H:i:s').' Erreur  : '.PHP_EOL.'Erreur =>'.$e.PHP_EOL, 'a+');
            echo 'Erreur  : '.$e->getMessage();
            exit;
        }
    }
}
