<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Newsletter\Atexo_Newsletter_Newsletter;

/**
 * Classe CronEnvoiNewsletter permettant d'envoyer une newsletter.
 *
 * @author  Houriya MAATALLA <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 */
class Cli_CronEnvoiNewsletter extends Atexo_Cli
{
    /**
     * Lancer le cli sans option pour traiter ttes les newsletters
     * Lancer le cli avec l'option 'Redac' pour traiter les newsletter de type Redac et que ne seront envoyés que pour les redacteurs
     * Lancer le cli avec l'option 'Mpe' pour traiter les newsletter de type Mpe qui seront envoyés pour ts les agents.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public static function run()
    {
        $typeNewsLetter = null;
        $arguments = $_SERVER['argv'];
        if (3 == (is_countable($arguments) ? count($arguments) : 0)) {
            if ('Redac' == $arguments[2]) {
                $typeNewsLetter = 'Redac';
            } elseif ('Mpe' == $arguments[2]) {
                $typeNewsLetter = 'Mpe';
            }
        }

        self::display("Début de l'envoi automatique des newsletters :");
        self::EnvoiNewsletter($typeNewsLetter);
        self::display('Done');
    }

    private static function EnvoiNewsletter($typeNewsLetter = false)
    {
        $Newsletters = Atexo_Newsletter_Newsletter::retrieveNewsletterByStatut(Atexo_Config::getParameter('STATUS_ENVOI_PLANIFIE'), null, $typeNewsLetter);
        if (is_array($Newsletters) && count($Newsletters) > 0) {
            foreach ($Newsletters as $OneNewsletter) {
                $idNewsletter = (new Atexo_Newsletter_Newsletter())->envoyerNewsletter($OneNewsletter);
                if ($idNewsletter) {
                    echo 'newsletter envoyee avec succes'.PHP_EOL;
                } else {
                    echo 'Echec d\'envoi';
                }
            }
        } else {
            echo 'Pas de newsletter à envoyer '.PHP_EOL;
        }
    }
}
