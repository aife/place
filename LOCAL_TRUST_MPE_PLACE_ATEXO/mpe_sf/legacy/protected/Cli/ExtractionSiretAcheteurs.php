<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Cron d'export de la liste des entites achat.
 *
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 */
class Cli_ExtractionSiretAcheteurs extends Atexo_Cli
{
    public static function run()
    {
        try {
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Debut du traitement : export des entites achat'."\n");

            //Recuperation des services
            $new_entity_array = self::extractService();
            $cpt = is_countable($new_entity_array) ? count($new_entity_array) : 0;

            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Nombre de services exportes : '.$cpt."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fin de traitement'."\n");

            self::writeBuffer($new_entity_array);
        } catch (Exception $e) {
            fwrite(STDERR, 'Erreur : '.$e->getMessage()."\n");
        }
    }

    public static function extractService()
    {
        //Recuperation de l'ensemble des organismes
        $query = <<<EOL
			SELECT acronyme, sigle
			FROM Organisme
			WHERE active = "1"
			AND acronyme in (
				"b4n","c8v","f5j","d4t","f0g","k0l","g7h","a4n","g6l","d3f","h8j","e8r","d2v","j9k", "t5y"
			)
			ORDER BY id ASC;
EOL;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute([]);

        //Recuperation des resultats
        $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
        $n = is_countable($rows) ? count($rows) : 0;

        //Parcours des resultats
        $new_entity_array = [];
        $level2 = 1;

        foreach ($rows as $organisme) {
            //Recuperation de la taille max de l'arbre du pole a generer
            if ('g7h' == $organisme['acronyme'] || 'a4n' == $organisme['acronyme']) {
                $max_height = 4;
            } else {
                $max_height = 3;
            }

            //Recuperation des services strictement poles et des services non affilies a un pole
            $query = <<<EOL
				(SELECT DISTINCT(S.id), S.siren, S.complement, S.libelle, S.sigle, S.chemin_complet AS path
				FROM Service S
				INNER JOIN AffiliationService ON service_parent_id = S.id 
				WHERE AffiliationService.organisme = "{$organisme['acronyme']}"
				AND S.organisme = "{$organisme['acronyme']}"
				AND service_parent_id NOT IN (
					SELECT service_id
					FROM AffiliationService
					WHERE AffiliationService.organisme = "{$organisme['acronyme']}"
				))
				UNION
				(SELECT DISTINCT(S2.id), S2.siren, S2.complement, S2.libelle, S2.sigle, S2.chemin_complet AS path
				FROM Service S2
				WHERE S2.organisme = "{$organisme['acronyme']}"
				AND S2.id NOT IN (
					SELECT service_parent_id
					FROM AffiliationService
					WHERE AffiliationService.organisme = "{$organisme['acronyme']}"
				)
				AND S2.id NOT IN (
					SELECT service_id
					FROM AffiliationService
					WHERE AffiliationService.organisme = "{$organisme['acronyme']}"
				)
				ORDER BY S2.id ASC);
EOL;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute([]);

            //Recuperation des resultats
            $service_rows = $statement->fetchAll(PDO::FETCH_ASSOC);
            $n = is_countable($service_rows) ? count($service_rows) : 0;

            $assoc_sigle_siret = [];
            $level_array = [];

            //Parcours des resultats
            foreach ($service_rows as $service) {
                self::treatService($new_entity_array, $assoc_sigle_siret, $organisme, $service, $level2, $level_array, $max_height);
            }

            ++$level2;
        }

        return $new_entity_array;
    }

    public static function treatService(&$new_entity_array, &$assoc_sigle_siret, $organisme, $service, $level2, &$level_array, $max_height, $current_height = 1, $parent = null)
    {
        $position = null;
        //Recuperer les informations du service
        //On recupere les donnees de l'element courant
        $item = self::updateAttributes($service, $current_height, $max_height, $parent, $organisme, $level2);

        //Verifier la validite du siret
        if (preg_match('#\d{14}#', $item['siret'])) {
            //Verifier si un autre service ne possede deja pas le mm siret
            $exists = false;
            foreach ($assoc_sigle_siret as $key => $elm) {
                if (isset($elm['siret'][0]) && $elm['siret'] == $item['siret'] && $elm['height'] == $item['height'] && $elm['parent_sigle'] == $item['parent_sigle']) {
                    $exists = true;
                    $position = $key;
                    break;
                }
            }
            if ($exists) {
                //Mettre a jour le tableau associatif
                if (!in_array($item['sigle_siret'], $assoc_sigle_siret[$position]['sigles'])) {
                    $assoc_sigle_siret[$position]['sigles'][] = $item['sigle_siret'];
                }
            } else {
                $assoc_sigle_siret[] = ['parent_sigle' => $item['parent_sigle'], 'height' => $item['height'], 'siret' => $item['siret'], 'sigles' => [$item['sigle_siret']]];
                //On ajoute au tableau final si cet element n'a pas deja ete ajoute
                $exists = false;
                foreach ($new_entity_array as $key => $elm) {
                    if ($elm['silo'] == $item['silo'] && $elm['sigle_siret'] == $item['sigle_siret'] && $elm['id'] == $item['id']) {
                        $exists = true;
                        $position = $key;
                        break;
                    }
                }
            }
            if (!$exists) {
                $new_entity_array[] = $item;
            }
        }

        //Rechercher si le service est un pole pour traiter ses sous-services
        $query = <<<EOL
				SELECT S.id, S.siren, S.complement, S.libelle, S.sigle, S.chemin_complet AS path
				FROM Service S
				INNER JOIN AffiliationService ON service_id = S.id
				WHERE AffiliationService.organisme = "{$organisme['acronyme']}"
				AND S.organisme = "{$organisme['acronyme']}"
				AND service_parent_id = "{$service['id']}"
				ORDER BY service_id ASC;
EOL;

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute([]);

        $res_row = $statement->fetchAll(PDO::FETCH_ASSOC);
        $n = is_countable($res_row) ? count($res_row) : 0;

        if ($n > 0) {
            foreach ($res_row as $pole_service) {
                self::treatService($new_entity_array, $assoc_sigle_siret, $organisme, $pole_service, $level2, $level_array, $max_height, $current_height + 1, $item);
            }
        }
    }

    public static function initItem($organisme, $level2)
    {
        $silo_to_OA = ['b4n' => 'C002',
                              'c8v' => 'C006',
                              'f5j' => 'C005',
                              'd4t' => 'C003',
                              'f0g' => 'C381',
                              'k0l' => 'C004',
                              'g7h' => 'C701',
                              'a4n' => 'C071',
                              'g6l' => 'C004',
                              'd3f' => 'C001',
                              'h8j' => 'C008',
                              'e8r' => 'C010',
                              'd2v' => 'C009',
                              'j9k' => 'C004',
                             ];

        $item = null;
        $item['id'] = '';
        $item['silo'] = $organisme['acronyme'];
        $item['oa'] = $silo_to_OA[$organisme['acronyme']];
        $item['code1'] = '1-00000001';
        $item['sigle1'] = 'ETAT';
        $item['code2'] = '2-'.str_pad($level2, 8, '0', STR_PAD_LEFT);
        $item['sigle2'] = $organisme['sigle'];
        $item['code3'] = '';
        $item['sigle3'] = '';
        $item['code4'] = '';
        $item['sigle4'] = '';
        $item['code5'] = '';
        $item['sigle5'] = '';
        $item['siret'] = '';
        $item['libelle_siret'] = '';
        $item['sigle_siret'] = '';
        $item['parent_sigle'] = '';

        return $item;
    }

    public static function updateAttributes($service, $current_height, $max_height, $parent, $organisme, $level2)
    {
        $item = self::initItem($organisme, $level2);

        $item['id'] = trim($service['id']);
        $item['path'] = trim($service['path']);
        $item['siret'] = trim($service['siren']).trim($service['complement']);
        $item['libelle_siret'] = trim($service['libelle']);
        $item['sigle_siret'] = trim($service['sigle']);
        $item['current_height'] = $current_height;
        $item['height'] = 2;
        $item['parent_sigle'] = $parent['sigle_siret'];

        if (2 == $current_height) {
            $item['sigle3'] = $parent['sigle_siret'];
            $item['parent_sigle'] = $item['sigle3'];
            $item['height'] = 3;
        } elseif (3 == $current_height && 3 == $max_height) {
            $item['sigle3'] = $parent['sigle3'];
            $item['sigle4'] = $parent['sigle_siret'];
            $item['parent_sigle'] = $item['sigle4'];
            $item['height'] = 4;
        } elseif ($current_height > 3 && 3 == $max_height) {
            $item['sigle3'] = $parent['sigle3'];
            $item['sigle4'] = $parent['parent_sigle'];
            $item['parent_sigle'] = $item['sigle4'];
            $item['height'] = 4;
        } elseif (3 == $current_height && 4 == $max_height) {
            $item['sigle3'] = $parent['sigle3'];
            $item['sigle4'] = $parent['sigle_siret'];
            $item['parent_sigle'] = $item['sigle4'];
            $item['height'] = 4;
        } elseif ($current_height >= 4 && 4 == $max_height) {
            $item['sigle3'] = $parent['sigle3'];
            $item['sigle4'] = $parent['sigle4'];
            $item['sigle5'] = $parent['sigle_siret'];
            $item['parent_sigle'] = $item['sigle5'];
            $item['height'] = 5;
        }

        return $item;
    }

    public static function encodeToUtf8($string)
    {
        return mb_convert_encoding($string, 'UTF-8', mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
    }

    public static function encodeToIso($string)
    {
        return mb_convert_encoding($string, 'ISO-8859-1', mb_detect_encoding($string, 'UTF-8, ISO-8859-1, ISO-8859-15', true));
    }

    public static function initBuffer()
    {
        $line = 'Identifiant'.';'.'Silo'.';'.'Code 1'.';'.'Sigle 1'.';'.'Code 2'.';'.'Sigle 2'.';';
        $line .= 'Code 3'.';'.'Sigle 3'.';'.'Code 4'.';'.'Sigle 4'.';'.'Code 5'.';'.'Sigle 5'.';';
        $line .= 'SIRET'.';'.'Libelle SIRET'.';'.'Sigle SIRET'.';'.'OA'.";\n";
        $encoding = mb_detect_encoding($line, 'auto');
        $line = mb_convert_encoding($line, 'UTF-8', $encoding);
        $buffer = '';
        $buffer .= $line;

        return $buffer;
    }

    public static function generateBuffer($entity_array)
    {
        $buffer = self::initBuffer();

        $level_array = ['3' => [], '4' => [], '5' => []];
        foreach ($entity_array as $key => $entity) {
            $organism = $entity['sigle2'];

            for ($i = 3; $i < 6; ++$i) {
                $code_field = 'code'.$i;
                $sigle_field = 'sigle'.$i;
                $value = $entity[$sigle_field];

                if (!empty($value)) {
                    //Parcours du tableau des services exportes (verification du sigle et de l'organisme)
                    $exists = false;
                    for ($k = 0,$n = count($level_array[$i]); $k < $n; ++$k) {
                        $item = null;
                        $item = $level_array[$i][$k];
                        if ($item['code'] == $value && $item['organism'] == $organism) {
                            $exists = true;
                            break;
                        }
                    }
                    //Si le service exporte n'existe pas dans le tableau des services exportes
                    if (!$exists) {
                        $item = null;
                        $item['code'] = $value;
                        $item['organism'] = $organism;
                        $level_array[$i][$k] = $item;
                    }
                    $pos = $k + 1;
                    $entity[$code_field] = $i.'-'.str_pad($pos, 8, '0', STR_PAD_LEFT);
                }
            }

            //Forcer l'encodage en UTF-8
            foreach ($entity as $key => $value) {
                //Transformer les caracteres HTML
                $value = Atexo_Util::ascii_to_entities($value);
                //Forcer l'encodage en ISO
                $value = Atexo_Util::stripWordsCharacters($value);
                $value = self::encodeToIso($value);
                //Forcer l'encodage en UTF-8
                $value = self::encodeToUtf8($value);
                //Mise a jour de la valeur correctement encodee
                $entity[$key] = $value;
            }

            $line = self::encodeLine($entity);
            $buffer .= $line;
        }

        return $buffer;
    }

    public static function encodeLine($entity)
    {
        $line = $entity['id'].';';
        $line .= $entity['silo'].';';
        $line .= $entity['code1'].';';
        $line .= $entity['sigle1'].';';
        $line .= $entity['code2'].';';
        $line .= $entity['sigle2'].';';
        $line .= $entity['code3'].';';
        $line .= $entity['sigle3'].';';
        $line .= $entity['code4'].';';
        $line .= $entity['sigle4'].';';
        $line .= $entity['code5'].';';
        $line .= $entity['sigle5'].';';
        $line .= $entity['siret'].';';
        $line .= $entity['libelle_siret'].';';
        $line .= $entity['sigle_siret'].';';
        $line .= $entity['oa'].";\n";
        $line = self::encodeToUtf8($line);

        return $line;
    }

    public static function writeBuffer($entity_array)
    {
        //Recuperation des donnees a ecrire dans le fichier
        $buffer = self::generateBuffer($entity_array);

        //Recuperation du repertoire dans lequel ecrire le fichier
        if (!is_dir(Atexo_Config::getParameter('REPERTOIRE_ARCHIVE_CSV_LISTE_SIRET_ACHETEUR'))) {
            mkdir(Atexo_Config::getParameter('REPERTOIRE_ARCHIVE_CSV_LISTE_SIRET_ACHETEUR'));
        }
        system('mv '.Atexo_Config::getParameter('REPERTOIRE_CSV_LISTE_SIRET_ACHETEUR').'liste_siret_acheteurs_*'.' '.Atexo_Config::getParameter('REPERTOIRE_ARCHIVE_CSV_LISTE_SIRET_ACHETEUR'));

        if (!is_dir(Atexo_Config::getParameter('REPERTOIRE_CSV_LISTE_SIRET_ACHETEUR'))) {
            mkdir(Atexo_Config::getParameter('REPERTOIRE_CSV_LISTE_SIRET_ACHETEUR'));
        }

        //Recuperation du nom du fichier a generer
        $date = date('Y-m-d_H-i-s');
        $file_UTF16 = Atexo_Config::getParameter('REPERTOIRE_CSV_LISTE_SIRET_ACHETEUR').'liste_siret_acheteurs_'.$date.'.csv';

        //Conversion des donnees en UTF-16
        $encoded_buffer = iconv('UTF-8', 'UTF-16', $buffer);

        //Ecriture du fichier final
        file_put_contents($file_UTF16, $encoded_buffer);
    }
}
