<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;

/**
 * Dump toutes les BDD du MPE (common+orga) et archive bz2 dans backups.
 *
 * @category Atexo
 */
class Cli_BackupDb extends Atexo_Cli
{
    public static function run()
    {
        $organisme = [];
        $backup_path = self::getRootPath().'/backups';
        self::display('Backup Path : '.$backup_path);
        system('mkdir -p '.$backup_path);
        $cdn = ' -h '.Atexo_Config::getParameter('HOSTSPEC').' -u '.Atexo_Config::getParameter('USERNAME').' ';
        if ('' != Atexo_Config::getParameter('PASSWORD')) {
            $cdn .= ' -p'.Atexo_Config::getParameter('PASSWORD');
        }
        if (Atexo_Config::getParameter('DB_PREFIX')) {
            $prefix = Atexo_Config::getParameter('DB_PREFIX').'_';
        } else {
            $prefix = '';
        }
        $query = 'SELECT acronyme FROM Organisme';
        foreach (Atexo_Db::getLinkCommon()->query($query) as $row) {
            $organisme = $prefix.$row['acronyme'];
            self::display("Backup $organisme... \n");
            system("mysqldump $cdn --opt --quote-names $organisme > $backup_path/$organisme.sql");
        }
        $commune = $prefix.Atexo_Config::getParameter('COMMON_DB');
        self::display('Backup '.$commune."... \n");
        system("nice mysqldump $cdn --opt --quote-names $commune > $backup_path/$commune.sql");
        self::display('Archive dans bdd_'.date('Y-m-d_H_i').".tar.bz2... \n");
        system("cd $backup_path && nice tar jcf bdd_".date('Y-m-d_H_i').".tar.bz2 *.sql && rm -f $prefix*.sql");
    }
}
