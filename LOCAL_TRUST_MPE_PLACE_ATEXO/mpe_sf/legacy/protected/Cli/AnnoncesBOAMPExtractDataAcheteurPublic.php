<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use AtexoCrypto\Dto\Fichier;
use Exception;

/**
 * Cron d'extraction des informations acheteurs publics des annonces BOAMP.
 *
 * @author Mohamed Wazni  <mohamed.wazni@atexo.com>
 *
 * @category Atexo
 */
class Cli_AnnoncesBOAMPExtractDataAcheteurPublic extends Atexo_Cli
{
    public static function run()
    {
        $argument = [];
        $files = [];
        $arrayLog = [];
        $k = null;
        try {
            $arguments = $_SERVER['argv'];
            if (is_array($arguments)) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument[] = $arguments[$i];
                }
            }
            $emplacementAnnonceBOAMP = Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_A_TRAITER');
            $dir = opendir($emplacementAnnonceBOAMP) or exit("Erreur le repertoire $emplacementAnnonceBOAMP n'existe pas");
            if ($dir) {
                echo "Début d'analyse des annonces \n";
                $fichircvs = Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'Rapport_fichiers_analyses-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
                if (!is_file($fichircvs)) {
                    Atexo_Util::writeFile($fichircvs, "Date||organisme||Fichier||Date Parution||Numero de parution||Lien_BOAMP||Nojo||Type d'avis||NOM_CP_VILLE||IDENT_PRM||IDENT_TEL||IDENT_FAX".
                                            "||IDENT_MEL||DOCUMENTS_NOM||DOCUMENTS_TEL||DOCUMENTS_FAX||DOCUMENTS_MEL||Url Avis||Profil Acheteur \n", 'a+');
                }
                if ($argument) {
                    $count = count($argument);
                    for ($i = 0; $i < $count; ++$i) {
                        $fileParam = $emplacementAnnonceBOAMP.$argument[$i].'.taz';
                        if (file_exists($fileParam)) {
                            $files[] = $argument[$i].'.taz';
                        }
                    }
                } else {
                    while ($file = @readdir($dir)) {
                        $files[] = $file;
                    }
                }
                $count = count($files);
                for ($i = 0; $i < $count; ++$i) {
                    $file = $files[$i];
                    $file_parts = pathinfo($file);
                    $pathTmp = Atexo_Config::getParameter('COMMON_TMP');
                    if ('.' != $file && '..' != $file && ('taz' == $file_parts['extension'])) {
                        $pathTazTmp = $pathTmp.'BOAMP_TAZ/';
                        if (!is_dir($pathTazTmp)) {
                            system('mkdir '.escapeshellarg($pathTazTmp));
                            echo "\r\n-> répertoire suivant créé   ".$pathTazTmp.'....';
                        }
                        $pathTazTmpTraite = $pathTmp.'BOAMP_FILES_TRAITE/';
                        if (!is_dir($pathTazTmpTraite)) {
                            system('mkdir '.escapeshellarg($pathTazTmpTraite));
                            echo "\r\n-> répertoire suivant créé   ".$pathTazTmpTraite.'....';
                        }
                        $fileToExtract = $emplacementAnnonceBOAMP.'/'.$file;
                        if (self::extractTarFiles($fileToExtract, $pathTazTmp)) {
                            $rep = opendir($pathTazTmp);
                            $resultat = self::traiterXml($rep, $pathTazTmp, $file);
                            if (true == $resultat[0]) {
                                ++$k;
                                $arrayLog[$k] = $resultat[1];
                                system('mv '.$emplacementAnnonceBOAMP.'/'.$file.' '.Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_TRAITES').'  >> '.
                                        Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'boamp_log 2>&1');
                                system('mv  '.$pathTazTmp.'* '.$pathTazTmpTraite);
                            } else {
                                echo $file." non traité \n";
                            }
                        } else {
                            echo "\r\n-> Erreur de désarchivage du fichier".$emplacementAnnonceBOAMP.'/'.$file."\n";
                            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'/boamp_log', date('Y-m-d H:i:s').'Erreur de désarchivage du fichier : '.
                                                    $emplacementAnnonceBOAMP.'/'.$file."\r\n", 'a+');
                        }
                    }
                }
                if ((is_countable($arrayLog) ? count($arrayLog) : 0) > 0) {
                    self::envoyerEmail($arrayLog);
                }
                echo "Fin de traitement de transfert \n";
            }
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'/boamp_log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            echo 'Erreur  : '.$e->getMessage();
            exit;
        }
    }

    /*********************************************
     * Traiter le repertoire d'un taz extrait
     *********************************************/
    public function traiterXml($rep, $pathTazTmp, $file)
    {
        try {
            $resultat = [];
            //array_splice($resultat, 0, count($input));
            $resultat[0] = false;
            while ($fichier = @readdir($rep)) {
                $parie_fichier = pathinfo($fichier);
                if ('.' != $fichier && '..' != $fichier && 'xml' == $parie_fichier['extension']) {
                    if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_XML_TRAITES'))) {
                        system('mkdir -p '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_XML_TRAITES')));
                        echo "\r\n-> répertoire suivant créé   ".Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_XML_TRAITES').'....';
                    }
                    // copier le fichier xml
                    system('cp  '.$pathTazTmp.'/'.$fichier.' '.Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_XML_TRAITES'));
                    $xmlTender = file_get_contents($pathTazTmp.'/'.$fichier);
                    if ($xmlTender) {
                        echo $fichier."\n";
                        $dataLog = self::insertTender($xmlTender, $parie_fichier, $file);
                        if (0 == $dataLog['error']) {
                            $data = $dataLog['log'];
                            if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_TRAITES'))) {
                                system('mkdir -p '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_TRAITES')));
                                echo "\r\n-> répertoire suivant créé   ".Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_TRAITES').'....';
                            }
                            if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS'))) {
                                system('mkdir -p '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS')));
                                echo "\r\n-> répertoire suivant créé   ".Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'....';
                            }
                            //log
                            $fichierLog = Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').$parie_fichier['filename'].'-'.date('Y').'-'.date('m').'-'.date('j').'.csv';
                            if (!is_file($fichierLog)) {
                                Atexo_Util::write_file($fichierLog, 'Date Debut '.date('d/m/Y  H:i:s')."\r\n\n");
                                Atexo_Util::write_file($fichierLog, "Référence consultation|Message \r\n", 'a+');
                                Atexo_Util::write_file($fichierLog, 'Fichier : '.$file."\r\n", 'a+');
                            }
                            Atexo_Util::writeFile($fichierLog, $data, 'a+');
                            Atexo_Util::write_file($fichierLog, 'Date Fin '.date('d/m/Y  H:i:s'), 'a+');
                            $resultat[0] = true;
                            $resultat[1] = $fichierLog;
                        }
                    }
                } elseif ('HTM' == $parie_fichier['extension']) {
                    if (!is_dir(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES'))) {
                        system('mkdir -p '.escapeshellarg(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES')));
                        echo "\r\n-> répertoire suivant créé   ".Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES').'....';
                    }
                    system('cp  '.$pathTazTmp.'/'.$fichier.' '.Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES'));
                }
            }

            return $resultat;
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'/boamp_log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            echo 'Erreur  : '.$e->getMessage();
        }
    }

    public function insertTender($xmlTender, $filename, $fileTazname)
    {
        $numeroparution = null;
        $dateEnvoi = null;
        $lien = null;
        $nombreAvistraite = null;
        $dataLog = null;
        //$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        try {
            //$connexionCom->beginTransaction();
            if ($xmlTender) {
                $xmlTender = str_replace('<!DOCTYPE PARUTION_BOAMP SYSTEM "BoampAB.dtd">', '', $xmlTender);
                $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($xmlTender);
                // recuperation de la date de parution qui est la date de mise on ligne
                if ($nodeParution = $domXml->getElementsByTagName('PARUTION_BOAMP')) {
                    $dateEnvoi = trim(Atexo_Util::utf8ToIso($nodeParution->item(0)->getAttribute('dateparution')));
                    //$dateBoamp = Atexo_Util::convertDateChaineIso($dateEnvoi);
                    $numeroparution = Atexo_Util::utf8ToIso($nodeParution->item(0)->getAttribute('numparution'));
                }
                $nodeAnnonce = $domXml->getElementsByTagName('ANNONCE_REF');
                $nombreAvis = is_countable($nodeAnnonce) ? count($nodeAnnonce) : 0;
                echo 'Parution Numéro : '.$numeroparution.' -- Nombre des Avis de la parution '.$nombreAvis." \n";
                $nombreAvistraite = 0;

                for ($i = 0; $i < $nombreAvis; ++$i) {
                    $urlConsultation = '';
                    $noms = '';
                    $tels = '';
                    $faxs = '';
                    $mels = '';
                    $result = [];
                    $nojo = '';
                    $orgnism = '';
                    $valOrganisme = '';
                    $infoAcheteurPublic = '';
                    $urlProfilAcheteur = '';
                    // l'organisme depend du nom du fichier TAZ
                    if (strstr(strtolower($fileTazname), Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_TYPE_MAPA'))) {
                        $orgnism = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_MAPA');
                    } elseif (strstr(strtolower($fileTazname), Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_TYPE_NATIONALE'))) {
                        $orgnism = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_NATIONALE');
                    } elseif (strstr(strtolower($fileTazname), Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_TYPE_EUROP'))) {
                        $orgnism = Atexo_Config::getParameter('ORGANISME_ANM_BOAMP_EUROP');
                    }
                    //echo $orgnism;exit;
                    //$orgnism = Atexo_Config::getParameter("ORGANISME_ANM_BOAMP");
                    $nodeGestion = $nodeAnnonce->item($i)->getElementsByTagName('GESTION');
                    //$nodeHtml = $nodeGestion[0]->getElementsByTagName('NOM_HTML');

                    if ($nodeNojo = $nodeGestion->item(0)->getElementsByTagName('NOJO')) {
                        $codeNojo = Atexo_Util::utf8ToIso($nodeNojo->item(0)->nodeValue);
                        $nojo = str_replace(' ', '', $codeNojo);
                    }
                    if ($vaR4 = $nodeGestion->item(0)->getElementsByTagName('R4')) {
                        $varR4 = $vaR4->item(0)->nodeValue;
                    } else {
                        $varR4 = '';
                    }
                    if ($nojo) {
                        $lien = 'http://boamp.fr/avis/detail/'.$nojo.'/0';
                    }
                    // categorie
                    if ($nodeDonnee = $nodeAnnonce->item($i)->getElementsByTagName('DONNEES')) {
                        if ($documents = $nodeDonnee->item(0)->getElementsByTagName('DOCUMENTS')) {
                            $result = self::getInfoFromDocuments($documents, $orgnism);
                            if ($result[0]) {
                                $urlConsultation = $result[0];
                                $noms = $result[1];
                                $tels = $result[2];
                                $faxs = $result[3];
                                $mels = $result[4];
                            }
                        }
                    }
                    if ($nodeIdent = $nodeDonnee->item(0)->getElementsByTagName('IDENT')) {
                        $valOrganisme = self::getDenominationOrg($nodeIdent);
                        $infoAcheteurPublic = self::getInfoAcheteurPublic($nodeIdent);
                    }
                    // url de l'achteur
                    if ($nodeUrl = $nodeIdent->item(0)->getElementsByTagName('URL_PROFIL_ACHETEUR')) {
                        $urlProfilAcheteur = rtrim(trim(Atexo_Util::utf8ToIso($nodeUrl->item(0)->nodeValue)), '.');
                    }
                    Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'Rapport_fichiers_analyses-'.date('Y').'-'.date('m').'-'.date('j').'.csv', date('Y-m-d H:i:s').'||'.$orgnism.'||'.$filename['filename'].'||'.$dateEnvoi.'||'.$numeroparution.'||'.$lien.'||'.$nojo.'||'.$varR4.'||'.$valOrganisme.'||'.$infoAcheteurPublic.
                    '||'.$noms.'||'.$tels.'||'.$faxs.'||'.$mels.'||'.$urlConsultation.'||'.$urlProfilAcheteur." \n", 'a+');
                    ++$nombreAvistraite;
                }
            }
            echo 'Le nombre des avis traités : '.$nombreAvistraite." \n";
            $dataLog .= "\n\n";
            // $connexionCom->commit();
            return ['error' => 0, 'log' => $dataLog];
        } catch (Exception $e) {
            echo 'Erreur : -> '.$e;
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'boamp_error', date('Y-m-d H:i:s').
                                ' : Erreur lors de la recuperation OU insersion  des consultations : '.$e." \n", 'a+');

            return ['error' => 1, 'log' => null];
        }
    }

    public function envoyerEmail($arrayLog)
    {
        try {
            $to = 'mwa@atexo.com,lma@atexo.com,pli@atexo.com';
            $from = Atexo_Config::getParameter('PF_MAIL_FROM');
            $zipFileNameTmp = Atexo_Config::getParameter('COMMON_TMP').'/'.'log_traitempent_boamp_'.date('Y_d_m_H_i_s').'_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
            $zip = false;
            for ($i = 1; $i <= (is_countable($arrayLog) ? count($arrayLog) : 0); ++$i) {
                (new Atexo_Zip())->addFileToZip($arrayLog[$i], $zipFileNameTmp);
                (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'Rapport_fichiers_analyses-'.date('Y').'-'.date('m').'-'.date('j').
                            '.csv', $zipFileNameTmp);
                $zip = true;
            }
            if ($zip) {
                $code = file_get_contents($zipFileNameTmp);
                $zipName = 'log_Transfert_boamp_'.Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');
                //$message = "\n Bonjour, \n\n";
                //$message .= "En pièce jointe la synthèse de l'extraction des informations du BOAMP.";
                $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_BOAMP_INTERACTION'));
                $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
                $message = str_replace('SALUTATION', 'Bonjour', $message);
                $message = str_replace('TITRE', '', $message);
                $message = str_replace('CORPS_MAIL', 'En pièce jointe la synthèse de l\'extraction des informations du BOAMP.', $message);
                (new Atexo_Message())->mailWithAttachement($from, $to, "Rapport de L'insertion des consultation BOAMP", $message, '', $code, $zipName, '', true);
            }
        } catch (Exception $e) {
            Atexo_Util::writeFile(Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'/boamp_log', date('Y-m-d H:i:s').' Erreur  : '.$e." \n ", 'a+');
            echo "Erreur d'envoi de l'email : ".$e->getMessage()."\n";
        }
    }

    public static function extractTarFiles($fileToExtract, $pathTazTmp)
    {
        system(
            'tar xzvf '.$fileToExtract.' -C '.escapeshellarg($pathTazTmp).'  >> '.Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_FICHIER_LOGS').'boamp_error_tar 2>&1',
            $sys_answer
        );
        if (0 == $sys_answer) {
            return true;
        } else {
            return false;
        }
    }

    public static function getDenominationOrg($nodeIdent)
    {
        $cp = null;
        $ville = null;
        if ($nodeIdent) {
            if ($nomOrgansime = $nodeIdent->item(0)->getElementsByTagName('NOM')) {
                $org = Atexo_Util::utf8ToIso($nomOrgansime->item(0)->nodeValue);
                $organsime = rtrim(trim($org), '.');
                if ($nodeCp = $nodeIdent->item(0)->getElementsByTagName('CP')) {
                    $cp = trim(Atexo_Util::utf8ToIso($nodeCp->item(0)->nodeValue), '.');
                }
                if ($nodeVille = $nodeIdent->item(0)->getElementsByTagName('VILLE')) {
                    $ville = trim(Atexo_Util::utf8ToIso($nodeVille->item(0)->nodeValue), '.');
                }
                // service non encore traité (la balise qui correspond au sevice au flux xml boamp non encore identifie)
                //$service = null;
                // la dénimication de l'organisme sous la forme : EntitePublic - EntiteAchat(CP - VILLE).
                $valOrganisme = $organsime.' ('.$cp.' - '.$ville.')';
                $valOrganisme = str_replace("\n", '', $valOrganisme);
                $valOrganisme = str_replace("\r\n", '', $valOrganisme);
                $valOrganisme = str_replace("\r", '', $valOrganisme);

                return $valOrganisme;
            }
        }

        return false;
    }

    public static function getInfoAcheteurPublic($nodeIdent)
    {
        if ($nodeIdent) {
            $nomAcheteurPublic = 'PRM';
            $telAcheteurPublic = 'TEL';
            $faxAcheteurPublic = 'FAX';
            $melAcheteurPublic = 'MEL';

            if ($nodePRM = $nodeIdent->item(0)->getElementsByTagName('PRM')) {
                $nomAcheteurPublic = trim(Atexo_Util::utf8ToIso($nodePRM->item(0)->nodeValue), ',');
                $nomAcheteurPublic = str_replace("\n", '', $nomAcheteurPublic);
                $nomAcheteurPublic = str_replace("\r\n", '', $nomAcheteurPublic);
                $nomAcheteurPublic = str_replace("\r", '', $nomAcheteurPublic);
            }

            if ($nodeTEL = $nodeIdent->item(0)->getElementsByTagName('TEL')) {
                $telAcheteurPublic = trim(Atexo_Util::utf8ToIso($nodeTEL->item(0)->nodeValue), '.');
                $telAcheteurPublic = str_replace("\n", '', $telAcheteurPublic);
                $telAcheteurPublic = str_replace("\r\n", '', $telAcheteurPublic);
                $telAcheteurPublic = str_replace("\r", '', $telAcheteurPublic);
            }

            if ($nodeFAX = $nodeIdent->item(0)->getElementsByTagName('FAX')) {
                $faxAcheteurPublic = trim(Atexo_Util::utf8ToIso($nodeFAX->item(0)->nodeValue), '.');
                $faxAcheteurPublic = str_replace("\n", '', $faxAcheteurPublic);
                $faxAcheteurPublic = str_replace("\r\n", '', $faxAcheteurPublic);
                $faxAcheteurPublic = str_replace("\r", '', $faxAcheteurPublic);
            }

            if ($nodeMEL = $nodeIdent->item(0)->getElementsByTagName('MEL')) {
                $melAcheteurPublic = trim(Atexo_Util::utf8ToIso($nodeMEL->item(0)->nodeValue), '.');
                $melAcheteurPublic = str_replace("\n", '', $melAcheteurPublic);
                $melAcheteurPublic = str_replace("\r\n", '', $melAcheteurPublic);
                $melAcheteurPublic = str_replace("\r", '', $melAcheteurPublic);
            }

            return $nomAcheteurPublic.'||'.$telAcheteurPublic.'||'.$faxAcheteurPublic.'||'.$melAcheteurPublic;
        }

        return false;
    }

    public static function adapterFormatUrl($url)
    {
        $url = Atexo_Util::atexoHtmlEntitiesDecode($url);
        $url = str_replace('index.php5', '', $url);
        $url = str_replace('index.php', '', $url);

        return $url;
    }

    public static function getInfoFromDocuments($nodesDocuments, $org)
    {
        $urls = '';
        $noms = '';
        $tels = '';
        $faxs = '';
        $mels = '';

        $result = [];
        for ($i = 0; $i < (is_countable($nodesDocuments) ? count($nodesDocuments) : 0); ++$i) {
            if ($nodes = $nodesDocuments->item($i)->children()) {
                for ($k = 0; $k < (is_countable($nodes) ? count($nodes) : 0); ++$k) {
                    if ($nodes->item($k)->length) {
                        if ('URL' == $nodes->item($k)->nodeName) {
                            $urlPf = rtrim(trim($nodes->item($k)->nodeValue), '.');
                            if ($urlPf) {
                                $urlPf = self::adapterFormatUrl($urlPf);
                                $urlPf = str_replace("\n", '', $urlPf);
                                $urlPf = str_replace("\r\n", '', $urlPf);
                                $urlPf = str_replace("\r", '', $urlPf);
                                $urls .= $urlPf.'##';
                                $result[0] = $urls;
                            }
                        }

                        if ('NOM' == $nodes->item($k)->nodeName) {
                            $nom = rtrim(trim($nodes->item($k)->nodeValue), ',');
                            if ($nom) {
                                $nom = str_replace("\n", '', $nom);
                                $nom = str_replace("\r\n", '', $nom);
                                $nom = str_replace("\r", '', $nom);
                                $noms .= $nom.'##';
                                $result[1] = $noms;
                            }
                        }

                        if ('TEL' == $nodes->item($k)->nodeName) {
                            $tel = rtrim(trim($nodes->item($k)->nodeValue), ',');
                            if ($tel) {
                                $tel = str_replace("\n", '', $tel);
                                $tel = str_replace("\r\n", '', $tel);
                                $tel = str_replace("\r", '', $tel);
                                $tels .= $tel.'##';
                                $result[2] = $tels;
                            }
                        }

                        if ('FAX' == $nodes->item($k)->nodeName) {
                            $fax = rtrim(trim($nodes->item($k)->nodeValue), ',');
                            if ($fax) {
                                $fax = str_replace("\n", '', $fax);
                                $fax = str_replace("\r\n", '', $fax);
                                $fax = str_replace("\r", '', $fax);
                                $faxs .= $fax.'##';
                                $result[3] = $faxs;
                            }
                        }

                        if ('MEL' == $nodes->item($k)->nodeName) {
                            $mel = rtrim(trim($nodes->item($k)->nodeValue), ',');
                            if ($mel) {
                                $mel = str_replace("\n", '', $mel);
                                $mel = str_replace("\r\n", '', $mel);
                                $mel = str_replace("\r", '', $mel);
                                $mels .= $mel.'##';
                                $result[4] = $mels;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }

    public function AdapteDate($chaine)
    {
        $jour = substr(trim($chaine), 0, 2);
        $mois = substr(trim($chaine), 2, 2);
        $annee = substr(trim($chaine), 4, 6);
        $date = mktime(0, 0, 0, $mois, $jour, $annee);

        return date('Y-m-d H:i:s', $date);
    }

    public function getAnnonceDateFinByDelais($node)
    {
        if ($nodedateLimite = $node->item(0)->getElementsByTagName('receptionOffres')) {
            $dateLimite = trim(Atexo_Util::utf8ToIso($nodedateLimite->item(0)->nodeValue));
            $dateLimitBoamp = str_replace('T', ' ', $dateLimite);

            return $dateLimitBoamp;
        }

        return false;
    }

    public function getBaliseForm($nodeDonnee)
    {
        $nodeForme = false;
        /*if($nodeDonnee){
            if($nodeForme = $nodeDonnee->get_elements_by_tagname('FORM_17')){

            }elseif($nodeForme = $nodeDonnee->get_elements_by_tagname('FORM_18')){

            }elseif($nodeForme = $nodeDonnee->get_elements_by_tagname('FORM_16')){

            }elseif($nodeForme = $nodeDonnee->get_elements_by_tagname('FORM_19')){

            }
        }*/
        return $nodeForme;
    }

    public function getDateLimiteFromHtml($htmFile)
    {
        $find = false;
        $file = Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES').$htmFile;
        if (!file_exists($file)) {
            $file = Atexo_Config::getParameter('DIR_LOGS_EXTRACT_ANNONCES_BOAMP_HTML_TRAITES').strtolower($htmFile);
        }
        if (file_exists($file)) {
            $fileHandle = fopen($file, 'r+');
            while (!feof($fileHandle)) {
                $buffer = fgets($fileHandle);
                if (!$find) {
                    if (strstr($buffer, '<i>Date limite de r&eacute;ception des offres ou des demandes de participation :')) {
                        $find = true;
                    } elseif (strstr($buffer, '<i>Date limite de r&eacute;ception des offres :')) {
                        $find = true;
                    } elseif (strstr($buffer, '<i>Date limite de r&eacute;ception des candidatures :')) {
                        $find = true;
                    } elseif (strstr($buffer, '<i>Date limite de r&eacute;ception des projets ou des demandes de participation')) {
                        $find = true;
                    }
                } else {
                    return self::AdaterDateLimiteFromHtml($buffer);
                    //echo  $dateLimitBoamp." \n";
                        //echo $buffer." \n";
                }
            }
            fclose($fileHandle);
        }

        return false;
    }

    public static function AdaterDateLimiteFromHtml($chainedate)
    {
        $arrayHeur = [];
        if ($chainedate) {
            $chainedate = str_replace('<br>', '', trim($chainedate));
            $date = Atexo_Util::utf8ToIso($chainedate);
            $arrayDateheur = explode(',', $date);
            $arrayDate = Atexo_Util::convertDateChaineIso(html_entity_decode($arrayDateheur[0]), false);
            $chaine = $arrayDateheur[1];
            $chaine = str_replace(' ', '', trim($chaine));
            $chaine = str_replace('à', '', html_entity_decode($chaine));
            if (count($arrayDateheur) >= 2) {
                $arrayHeur['heure'] = substr($chaine, 0, 2);
                $arrayHeur['minute'] = substr($chaine, 3, 2);
            } else {
                $arrayHeur['heure'] = 0;
                $arrayHeur['minute'] = 0;
            }

            $newTstamp = mktime($arrayHeur['heure'], $arrayHeur['minute'], 0, $arrayDate[1], $arrayDate[0], $arrayDate[2]);
            $dateLimitBoamp = date('Y-m-d H:i:s', $newTstamp);

            return $dateLimitBoamp;
        } else {
            return false;
        }
    }
}
