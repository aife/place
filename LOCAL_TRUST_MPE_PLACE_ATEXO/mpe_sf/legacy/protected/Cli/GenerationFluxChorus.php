<?php

namespace Application\Cli;

use App\Service\WebServices\WebServicesExec;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;
use Prado\Prado;

/**
 * Class Cli_GenerationFluxChorus : CRON de generation des flux CHORUS.
 *
 * @category Atexo
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Cli_GenerationFluxChorus extends Atexo_Cli
{
    private static $codeRetour;
    private static ?int $nbFlux = null;

    /**
     * Lancement du CLI.
     */
    public static function run($return = false)
    {
        $statutCronGenerationFlux = null;
        $params = [];
        $params['debutExecution'] = new DateTime('now');
        $params['service'] = 'Chorus';
        $params['nomBatch'] = 'Cli_GenerationFluxChorus';
        self::oversight($params);

        (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('>>>>>>>>>>>>>>>>>>> DEBUT lancement routine de generation des flux (Cli_GenerationFluxChorus) <<<<<<<<<<<<<<<<<<<'.PHP_EOL);

        //Recuperation des echanges CHORUS pour la generation de flux
        $chorusEchangeQuery = new CommonChorusEchangeQuery();
        $listeEchanges = $chorusEchangeQuery->recupererEchangesChorusFluxAGenerer();

        self::$nbFlux = count($listeEchanges);

        if (is_array($listeEchanges) && !empty($listeEchanges)) {
            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Nombre de flux a generer : '.count($listeEchanges).PHP_EOL);
            foreach ($listeEchanges as $echange) {
                $statutCronGenerationFlux = Atexo_Oversight_Oversight::$codeSuccess;
                $erreursGenerationFluxEchange = [];
                try {
                    if ($echange instanceof CommonChorusEchange) {
                        $params['informationMetier'] = json_encode(['id_echange' => $echange->getId(), 'organisme' => $echange->getOrganisme(), 'id_contrat' => $echange->getIdDecision()], JSON_THROW_ON_ERROR);
                        (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Debut traitement flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision().']');
                        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        $contrat = null;
                        try {
                            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                            if ($echange->getUuidExterneExec()) {
                                $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                            }
                        } catch (\Exception $exception) {
                            $erreurMessage = "Erreur au moment de comunication avec Exec : ".print_r($exception->getMessage(), true).PHP_EOL.PHP_EOL.'Echange : [id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision().']';
                            (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreurMessage);
                            $erreursGenerationFluxEchange[] = $erreurMessage;
                            $statutCronGenerationFlux = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                            $params['poids'] = 0;
                            $params['code'] = $statutCronGenerationFlux;
                        }
                        if ($contrat) {
                            //Debut transaction
                            $connexion->beginTransaction();
                            switch ($echange->getTypeFluxAEnvoyer()) {
                                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR'):
                                    $tmpFileName = self::genererFluxFIR($echange, $connexion, $erreursGenerationFluxEchange);
                                    $params['typeFlux'] = 'FIR0120B';
                                    break;

                                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211'):
                                    $tmpFileName = self::genererFluxFEN211($echange, $connexion, $erreursGenerationFluxEchange);
                                    $params['typeFlux'] = 'FEN211';
                                    break;

                                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111'):
                                    $tmpFileName = self::genererFluxFEN111($echange, $contrat, $connexion, $erreursGenerationFluxEchange);
                                    $params['typeFlux'] = 'FEN111';
                                    break;
                            }
                            //Fin transaction : commit
                            if (is_file($tmpFileName)) {
                                $connexion->commit();
                                (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Transaction comitee avec *** SUCCES ***  [id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision()." , fichier = '".$echange->getTmpFileName()."']");
                                $params['poids'] = filesize($tmpFileName);
                                $params['code'] = $statutCronGenerationFlux;
                            } else {
                                $connexion->rollBack();
                                (new Atexo_Chorus_Util())->loggerInfosGenerationFlux(" *** Transaction annulee (RollBack): *** le fichier '$tmpFileName' n'existe pas : [id_echange = ".$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision()." , fichier = '".$echange->getTmpFileName()."']");
                                $statutCronGenerationFlux = Atexo_Oversight_Oversight::$codeErrorSomeFilesOrFoldersAreNotFound;
                                $params['poids'] = 0;
                                $params['code'] = $statutCronGenerationFlux;
                            }
                        }

                        //Detruire la connexion
                        unset($connexion);

                        if (!empty($erreursGenerationFluxEchange)) {
                            //Changer le statut de l'echange CHORUS a 'STATUS_ECHANGE_CHORUS_NON_ENVOYER'
                            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux("Debut changement du statut de l'echange : ".$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision().']');
                            $connexionForError = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $echange->setStatutechange(Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER'));
                            $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                            $echange->setErreurRejet('('.Atexo_Config::getParameter('CODE_ERREUR_REJET_CHORUS').')  '.Prado::localize('LIBELLE_MESSAGE_ERREUR_GENERATION_FLUX_CHORUS'));
                            $echange->save($connexionForError);
                            $connexionForError->commit();
                            unset($connexionForError);
                            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux("Statut modifie avec *** SUCCES ***. Nouveau statut = '".$echange->getStatutechange()."' , Libelle nouveau statut = '".$echange->getLibelleStatutEchange()."'");
                            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux("Fin changement du statut de l'echange : ".$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision().']');
                        }

                        (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin traitement flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' , id_contrat = '.$echange->getIdDecision().']'.PHP_EOL);
                    } else {
                        $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
                        (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);
                        $erreursGenerationFluxEchange[] = $erreur;

                        $statutCronGenerationFlux = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                        $params['code'] = $statutCronGenerationFlux;
                    }
                } catch (Exception $e) {
                    $erreur = 'Erreur lors de la generation du flux '.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_GenerationFluxChorus::run()';
                    (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);
                    $erreursGenerationFluxEchange[] = $erreur;
                    $statutCronGenerationFlux = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['code'] = $statutCronGenerationFlux;
                }
                $params['oversight'] = self::oversight($params);
                if (!empty($erreursGenerationFluxEchange)) {
                    //Envoyer mail au support
                    (new Atexo_Chorus_Util())->envoyerMailSupportChorus('Erreur lors de la generation du flux (routine : GenerationFluxChorus)', print_r($erreursGenerationFluxEchange, true).PHP_EOL.PHP_EOL.'Echange: '.PHP_EOL.print_r($echange, true));
                    $params['nbFlux'] = 0;
                }

                --self::$nbFlux;
            }// END foreach
        } else {
            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Aucun flux a generer.'.PHP_EOL);
        }

        (new Atexo_Chorus_Util())->loggerInfosGenerationFlux(">>>>>>>>>>>>>>>>>>> Fin lancement routine de generation des flux (Cli_GenerationFluxChorus) : *** STATUT = '$statutCronGenerationFlux' *** <<<<<<<<<<<<<<<<<<<".PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL.PHP_EOL);

        if ($return) {
            return $statutCronGenerationFlux;
        } else {
            echo $statutCronGenerationFlux;
        }
    }

    /**
     * Permet de generer le flux FIR
     * Met a jour le statut de l'echange chorus.
     *
     * @param CommonChorusEchange $echange               : objet Chorus Echange
     * @param PropelPDO           $connexion             : connexion
     * @param array               $erreursGenerationFlux : liste des erreurs de generation de flux
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function genererFluxFIR($echange, &$connexion, &$erreursGenerationFlux): string|bool
    {
        if ($echange instanceof CommonChorusEchange) {
            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Debut generation du flux FIR - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            try {
                $tmpFilePath = (new Atexo_Chorus_Echange())->genererXmlFluxFIR($echange, $connexion, $erreursGenerationFlux);

                if (is_file($tmpFilePath)) {
                    $logger = Atexo_LoggerManager::getLogger('chorus');
                    $returnUpdate = (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($echange->getId(), $echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'), null, $connexion, $logger, $erreursGenerationFlux, $tmpFilePath);
                    if (false === $returnUpdate) {
                        $logger->info("Erreur survenue lors de la mise a jour des informations de la table 'Chorus_Echange'");

                        return false;
                    }
                } else {
                    $connexion->rollBack();
                    $erreur = "Le fichier temporaire n'a pas ete stocke dans la table 'Chorus_Echange'. Le fichier '".$tmpFilePath."' n'existe pas. Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().']';
                    $erreursGenerationFlux[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                    return false;
                }

                return $tmpFilePath;
            } catch (Exception $e) {
                $connexion->rollBack();
                $erreur = 'Erreur lors de la generation du flux FIR : {id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_GenerationFluxChorus::run()';
                $erreursGenerationFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                return false;
            }

            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin generation du flux FIR - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
        } else {
            $connexion->rollBack();
            $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursGenerationFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

            return false;
        }
    }

    /**
     * Permet de generer le flux FEN211
     * Met a jour le statut de l'echange chorus.
     *
     * @param CommonChorusEchange $echange               : objet Chorus Echange
     * @param PropelPDO           $connexion             : connexion
     * @param array               $erreursGenerationFlux : liste des erreurs de generation de flux
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function genererFluxFEN211($echange, &$connexion, &$erreursGenerationFlux): string|bool
    {
        if ($echange instanceof CommonChorusEchange) {
            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Debut generation du flux FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            try {
                $tmpFilePath = (new Atexo_Chorus_Echange())->genererXmlFluxFEN211($echange, $connexion, $erreursGenerationFlux);
                if (is_file($tmpFilePath)) {
                    $logger = Atexo_LoggerManager::getLogger('chorus');
                    $returnUpdate = (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($echange->getId(), $echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'), null, $connexion, $logger, $erreursGenerationFlux, $tmpFilePath);
                    if (false === $returnUpdate) {
                        $logger->info("Erreur survenue lors de la mise a jour des informations de la table 'Chorus_Echange'");

                        return false;
                    }
                } else {
                    $connexion->rollBack();
                    $erreur = "Le fichier temporaire n'a pas ete stocke dans la table 'Chorus_Echange'. Le fichier '".$tmpFilePath."' n'existe pas. Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().']';
                    $erreursGenerationFlux[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                    return false;
                }

                (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin generation du flux FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                return $tmpFilePath;
            } catch (Exception $e) {
                $connexion->rollBack();
                $erreur = 'Erreur lors de la generation du flux FEN211 : {id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_GenerationFluxChorus::genererFluxFEN211';
                $erreursGenerationFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                return false;
            }
            (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin generation du flux FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
        } else {
            $connexion->rollBack();
            $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursGenerationFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

            return false;
        }
    }

    /**
     * Permet de generer le flux FEN211
     * Met a jour le statut de l'echange chorus.
     *
     * @param CommonChorusEchange     $echange               : objet Chorus Echange
     * @param PropelPDO               $connexion             : connexion
     * @param array                   $erreursGenerationFlux : liste des erreurs de generation de flux
     * @param CommonTContratTitulaire $contrat               : objet contrat
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public static function genererFluxFEN111($echange, $contrat, &$connexion, &$erreursGenerationFlux): string|bool
    {
        if ($echange instanceof CommonChorusEchange) {
            if ($contrat) {
                (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Debut generation du flux FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
                try {
                    $tmpFilePath = (new Atexo_Chorus_Echange())->genererXmlFluxFEN111($echange, $contrat, $connexion, $erreursGenerationFlux);
                    if (is_file($tmpFilePath)) {
                        $logger = Atexo_LoggerManager::getLogger('chorus');
                        $returnUpdate = (new Atexo_Chorus_Echange())->updateChorusEchangeForTransmission($echange->getId(), $echange->getOrganisme(), Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_ENVOI_PLANIFIE'), null, $connexion, $logger, $erreursGenerationFlux, $tmpFilePath);
                        if (false === $returnUpdate) {
                            $logger->info("Erreur survenue lors de la mise a jour des informations de la table 'Chorus_Echange'");

                            return false;
                        }
                    } else {
                        $connexion->rollBack();
                        $erreur = "Le fichier temporaire n'a pas ete stocke dans la table 'Chorus_Echange'. Le fichier '".$tmpFilePath."' n'existe pas. Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().']';
                        $erreursGenerationFlux[] = $erreur;
                        (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                        return false;
                    }

                    (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin generation du flux FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                    return $tmpFilePath;
                } catch (Exception $e) {
                    $connexion->rollBack();
                    $erreur = 'Erreur lors de la generation du flux FEN111 : {id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Cli_GenerationFluxChorus::genererFluxFEN111';
                    $erreursGenerationFlux[] = $erreur;
                    (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                    return false;
                }
                (new Atexo_Chorus_Util())->loggerInfosGenerationFlux('Fin generation du flux FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            } else {
                $connexion->rollBack();
                $erreur = "Le contrat n'est pas une instance de CommonTContratTitulaire : ".print_r($contrat, true);
                $erreursGenerationFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

                return false;
            }
        } else {
            $connexion->rollBack();
            $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursGenerationFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursGenerationFlux($erreur);

            return false;
        }
    }
}
