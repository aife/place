<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use AtexoCrypto\Dto\Fichier;
use Exception;
use PDO;

/**
 * Cron identifiant les fichiers blob a purger.
 *
 * @author Ameline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2014
 */

//Classe BlobfileTableDescripteur
class BlobfileTableDescripteur
{
    /**
     * Attributs.
     */
    //Nom de la table
    protected $_table;
    //Nom du champs de l'ID du fichier blob
    protected $_champs_id_blob;
    //Nom du champs du nom du fichier blob
    protected $_champs_nom_fichier;
    //Nom du champs de l'organisme du fichier blob
    protected $_champs_organisme;

    /**
     * Constructeur.
     */
    public function __construct($_o_table)
    {
        $this->_table = $_o_table['table'];
        $this->_champs_id_blob = $_o_table['champs_id_blob'];
        $this->_champs_nom_fichier = $_o_table['champs_nom_fichier'];
        $this->_champs_organisme = $_o_table['champs_organisme'];
    }

    /**
     * Getters et Setters.
     */
    public function get_table()
    {
        return $this->_table;
    }

    public function get_champs_id_blob()
    {
        return $this->_champs_id_blob;
    }

    public function get_champs_nom_fichier()
    {
        return $this->_champs_nom_fichier;
    }

    public function get_champs_organisme()
    {
        return $this->_champs_organisme;
    }

    public function set_table($_table)
    {
        $this->_table = $_table;
    }

    public function set_champs_id_blob($_champs_id_blob)
    {
        $this->_champs_id_blob = $_champs_id_blob;
    }

    public function set_champs_nom_fichier($_champs_nom_fichier)
    {
        $this->_champs_nom_fichier = $_champs_nom_fichier;
    }

    public function set_champs_organisme($_champs_organisme)
    {
        $this->_champs_organisme = $_champs_organisme;
    }

    /**
     * Methodes.
     */
    public static function genereBlobfileTableDescripteur($e_blob_table)
    {
        $table = null;
        $champs_id_blob = null;
        $champs_nom_fichier = null;
        $champs_organisme = null;
        if (isset($e_blob_table['table']) && isset($e_blob_table['champs_id_blob']) && isset($e_blob_table['champs_nom_fichier']) && isset($e_blob_table['champs_organisme'])) {
            $table = $e_blob_table['table'];
            $champs_id_blob = $e_blob_table['champs_id_blob'];
            $champs_nom_fichier = $e_blob_table['champs_nom_fichier'];
            $champs_organisme = $e_blob_table['champs_organisme'];
        }

        $e_blob = new BlobfileTableDescripteur($table, $champs_id_blob, $champs_nom_fichier, $champs_organisme);

        return $e_blob;
    }
}

class BlobfileTableParcoureur
{
    /**
     * Constantes.
     */

    //Modes de configuration
    const CONFIGURATION_PLATEFORME = 1;
    const CONFIGURATION_ORGANISME = 0;

    //Modele de nom de fichier
    const MODELE_NOM_FICHIER = '_blob_to_purge_';
    //Extension du fichier de sortie
    const EXTENSION_FICHIER = '.txt';

    //Type de sortie
    const SORTIE_FICHIER = 0;
    const SORTIE_BDD = 1;

    //Table de journalisation
    const NOM_LOG_TABLE = 't_historisation_blob_purge';
    const CHAMPS_NOM_TABLE_LOG_TABLE = 'nom_table';
    const CHAMPS_ID_BLOB_LOG_TABLE = 'id_blob';
    const CHAMPS_NOM_FICHIER_LOG_TABLE = 'nom_fichier';
    const CHAMPS_ORGANISME_LOG_TABLE = 'organisme';
    const CHAMPS_DATE_IDENTIFICATION_LOG_TABLE = 'date_identification';

    //Objet representant la table referencant les objets blob
    protected $_blob_reference_table = null;
    //Objet representant la table contenant les objets blob
    protected $_blob_table = null;
    //Acronyme de l'organisme concerne
    protected string $_organisme = '';

    //Liste des organismes
    protected string $_t_organismes = '';
    //Tableau associatif des tables blob a parcourir
    protected $_t_blob_tables = null;
    //Tableau associatif des tables blob de references a parcourir
    protected $_t_blob_reference_tables = null;

    //Requete SQL a exectuer
    protected string $_query = '';
    //Resultats bruts de la requete SQL
    protected $_rows = null;

    //Date d'execution du script
    protected string $_date_execution = '';

    /**
     * Getters et setters.
     */
    public function get_config_plateforme()
    {
        return $this->_config_plateforme;
    }

    public function set_config_plateforme($_config_plateforme)
    {
        $this->_config_plateforme = $_config_plateforme;
    }

    public function get_mode_sortie()
    {
        return $this->_mode_sortie;
    }

    public function set_mode_sortie($_mode_sortie)
    {
        $this->_mode_sortie = $_mode_sortie;
    }

    public function get_plateforme()
    {
        return $this->_plateforme;
    }

    public function set_plateforme($_plateforme)
    {
        $this->_plateforme = $_plateforme;
    }

    public function get_blob_reference_table()
    {
        return $this->_blob_reference_table;
    }

    public function get_blob_table()
    {
        return $this->_blob_table;
    }

    public function get_organisme()
    {
        return $this->_organisme;
    }

    public function set_blob_reference_table($_blob_reference_table)
    {
        $this->_blob_reference_table = $_blob_reference_table;
    }

    public function set_blob_table($_blob_table)
    {
        $this->_blob_table = $_blob_table;
    }

    public function set_organisme($_organisme)
    {
        $this->_organisme = $_organisme;
    }

    public function get_input_blob_tables()
    {
        return $this->_input_blob_tables;
    }

    public function set_input_blob_tables($_input_blob_tables)
    {
        $this->_input_blob_tables = $_input_blob_tables;
    }

    public function get_input_blob_reference_tables()
    {
        return $this->_input_blob_reference_tables;
    }

    public function set_input_blob_reference_tables($_input_blob_reference_tables)
    {
        $this->_input_blob_reference_tables = $_input_blob_reference_tables;
    }

    public function get_t_organismes()
    {
        return $this->_t_organismes;
    }

    public function set_t_organismes($_t_organismes)
    {
        $this->_t_organismes = $_t_organismes;
    }

    public function get_t_blob_tables()
    {
        return $this->_t_blob_tables;
    }

    public function get_t_blob_reference_tables()
    {
        return $this->_t_blob_reference_tables;
    }

    public function set_t_blob_tables($_t_blob_tables)
    {
        $this->_t_blob_tables = $_t_blob_tables;
    }

    public function set_t_blob_reference_tables($_t_blob_reference_tables)
    {
        $this->_t_blob_reference_tables = $_t_blob_reference_tables;
    }

    public function get_query()
    {
        return $this->_query;
    }

    public function set_query($_query)
    {
        $this->_query = $_query;
    }

    public function get_rows()
    {
        return $this->_rows;
    }

    public function set_rows($_rows)
    {
        $this->_rows = $_rows;
    }

    public function get_date_execution()
    {
        return $this->_date_execution;
    }

    public function set_date_execution($_date_execution)
    {
        $this->_date_execution = $_date_execution;
    }

    /**
     * Constructeur.
     */
    public function __construct(protected $_plateforme, protected $_mode_sortie, /**
     * Attributs.
     */
    protected $_config_plateforme, protected $_input_blob_tables, protected $_input_blob_reference_tables)
    {
        $this->_init();
    }

    /**
     * Methodes.
     */
    protected function _retrouveOrganismes()
    {
        //Preparation de la requete
        $query = 'SELECT DISTINCT(acronyme) FROM Organisme';
        $this->set_query($query);
        //Execution de la requete
        $this->_executeRequeteSelect();
        //Recuperation des resultats
        $rows = $this->get_rows();

        //Creation d'un nouveau tableau pour generer la liste des organismes
        $_t_organismes = [];
        foreach ($rows as $row) {
            $_t_organismes[] = $row['acronyme'];
        }

        return $_t_organismes;
    }

    protected function _formateBlobfileTableDescripteurs($filename)
    {
       //Recuperation du contenu du fichier dans un tableau
        $t_file = file($filename);

        //Parcours de chaque ligne du tableau pour initialiser le tableau des tables Blob
        $t_blob_tables = [];

        foreach ($t_file as $line) {
            if (!empty($line) && '' != $line) {
                $t_line = explode(';', $line);
                $o_blob_table = null;
                $o_blob_table['table'] = $t_line[0];
                $o_blob_table['champs_id_blob'] = $t_line[1];
                $o_blob_table['champs_nom_fichier'] = $t_line[2];
                $o_blob_table['champs_organisme'] = $t_line[3];
                $t_blob_tables[] = $o_blob_table;
            }
        }

        //Forcage de la liberation d'espace memoire
        unset($t_file);
        unset($t_line);
        unset($o_blob_table);

        //Renvoie du tableau formate
        return $t_blob_tables;
    }

    protected function _init()
    {
        //Mise a jour de la date d'execution du script
        $date_execution = date('Ymd');
        $this->set_date_execution($date_execution);

        //Generation de la liste des organismes
        $_t_organismes = $this->_retrouveOrganismes();
        //Mise a jour de l'attribut correspondant
        $this->set_t_organismes($_t_organismes);

        //Generation du tableau des tables blob a partir du fichier d'entree
        $_t_blob_tables = $this->_formateBlobfileTableDescripteurs($this->get_input_blob_tables());
        //Mise a jour de l'attribut correspondant
        $this->set_t_blob_tables($_t_blob_tables);

        //Generation du tableau des tables blob de reference a partir du fichier d'entree
        $_t_blob_reference_tables = $this->_formateBlobfileTableDescripteurs($this->get_input_blob_reference_tables());
        //Mise a jour de l'attribut correspondant
        $this->set_t_blob_reference_tables($_t_blob_reference_tables);

        //Forcage de la liberation d'espace memoire
        if (isset($_t_organismes)) {
            unset($_t_organismes);
        }
        unset($_t_blob_tables);
        unset($_t_blob_reference_tables);
    }

    protected function _genereRequete()
    {
        $query = null;
        //Recuperation des objets BlobfileTableDescripteur
        $o_blob_table = $this->get_blob_table();
        //$o_blob_reference_table = $this->get_blob_reference_table();

        //Preparation de la requete principale
        $query1 = 'SELECT '.$o_blob_table->get_champs_id_blob();

        $champs_nom_fichier = $o_blob_table->get_champs_nom_fichier();
        if (!empty($champs_nom_fichier)) {
            $query1 .= ', '.$champs_nom_fichier;
        }

        $where_query1 = ' ';
        $champs_organisme = $o_blob_table->get_champs_organisme();
        if (!empty($champs_organisme)) {
            $query1 .= ', '.$champs_organisme;
            if (self::CONFIGURATION_ORGANISME == $this->get_config_plateforme()) {
                $where_query1 = ' WHERE '.$champs_organisme.' = "'.$this->get_organisme().'" ';
            }
        }

        $query1 .= ' FROM '.$o_blob_table->get_table();

        $query1 .= $where_query1; //.$o_blob_table->get_champs_id_blob().' IN ';

        /*
                //Preparation de la sous-requete
                $query2 = '( SELECT '.$o_blob_reference_table->get_champs_id_blob().' FROM '.$o_blob_reference_table->get_table();

                $champs_organisme = $o_blob_reference_table->get_champs_organisme();
                if($this->get_config_plateforme() == self::CONFIGURATION_ORGANISME && !empty($champs_organisme)){
                    $query2 .= ' WHERE '.$champs_organisme.' = "'.$this->get_organisme().'" )';
                } else {
                    $query2 .= ' )';
                }

                //Concatenation en une requete finale
                $query = $query1.$query2;
        */
        //Mise a jour de l'attribut de l'instance
        $this->set_query($query1);

        echo $query."\n";
    }

    protected function _executeRequeteSelect()
    {
        if ($this->get_query()) {
            //Recuperation de la requete
            $query = $this->get_query();
            //Preparation de la requete
            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            //Execution de la requete
            $statement->execute([]);
            //Recuperation du resultat
            $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
            $n = is_countable($rows) ? count($rows) : 0;

            //Journalisation
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t"."Requete executee:\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".$query."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Nombre de resultats: '.$n."\n");

            $this->set_rows($rows);
        }
    }

    protected function _executeRequete()
    {
        if ($this->get_query()) {
            //Recuperation de la requete
            $query = $this->get_query();
            //Preparation de la requete
            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            //Execution de la requete
            $statement->execute([]);
            //Recuperation du resultat
            $n = $statement->rowCount();

            //Journalisation
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t"."Requete executee:\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".$query."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Nombre de lignes affectees: '.$n."\n");

            $this->set_rows($n);
        }
    }

    public function parcours()
    {
        if (self::CONFIGURATION_ORGANISME == $this->get_config_plateforme()) {
            //Recuperation de la liste des organismes
            $t_organismes = $this->get_t_organismes();

            if (!empty($t_organismes) && (is_countable($t_organismes) ? count($t_organismes) : 0)) {
                //Traitement par organisme
                foreach ($t_organismes as $e_organisme) {
                    //Mise a jour de l'attribut organisme
                    $this->set_organisme($e_organisme);
                    //Traitement pour l'organisme en cours
                    $this->_parcoursTables();
                }
            }
        } elseif (self::CONFIGURATION_PLATEFORME == $this->get_config_plateforme()) {
            $this->_parcoursTables();
        }
    }

    protected function _parcoursTables()
    {
        //Recuperation de la liste des tables de reference
        $t_blob_reference_tables = $this->get_t_blob_reference_tables();
        //Recuperation de la liste des tables contenant les blobs
        $t_blob_tables = $this->get_t_blob_tables();

        if (!empty($t_blob_reference_tables) && (is_countable($t_blob_reference_tables) ? count($t_blob_reference_tables) : 0) && !empty($t_blob_tables) && (is_countable($t_blob_tables) ? count($t_blob_tables) : 0)) {
            //Traitement pour chaque table de refecence
            foreach ($t_blob_reference_tables as $e_blob_reference_table) {
                //Instanciation d'un objet BlobfileTableDescripteur
                $this->set_blob_reference_table(new BlobfileTableDescripteur($e_blob_reference_table));

                //Traitement pour chaque paire table reference / table contenant les blobs
                foreach ($t_blob_tables as $e_blob_table) {
                    //Instanciation d'un objet BlobfileTableDescripteur
                    $this->set_blob_table(new BlobfileTableDescripteur($e_blob_table));

                    //Recherche des blobs obsoletes
                    $this->_rechercheBlobObsoletes();
                }
            }
        }

        //Forcage de la liberation d'espace memoire
        unset($t_blob_tables);
        unset($t_blob_reference_tables);
    }

    protected function _rechercheBlobObsoletes()
    {
        //Generation de la requete
        $this->_genereRequete();

        //Execution de la requete
        $this->_executeRequeteSelect();

        //Traitement du resultat de la requete
        $this->_traiteResultats();
    }

    protected function _traiteResultats()
    {
       $file = null;
        //Recuperation du resultat de la requete SQL
        $rows = $this->get_rows();

        if (!empty($rows) && (is_countable($rows) ? count($rows) : 0)) {
            if (self::CONFIGURATION_PLATEFORME == $this->get_config_plateforme()) {
                $file = $this->get_plateforme().self::MODELE_NOM_FICHIER.$this->get_date_execution().self::EXTENSION_FICHIER;
            } elseif (self::CONFIGURATION_ORGANISME == $this->get_config_plateforme()) {
                $file = $this->get_plateforme().'_'.$this->get_organisme().self::MODELE_NOM_FICHIER.$this->get_date_execution().self::EXTENSION_FICHIER;
            }

            if (self::SORTIE_FICHIER == $this->get_mode_sortie()) {
                $this->_sortieFichier($file);
            } elseif (self::SORTIE_BDD == $this->get_mode_sortie()) {
                $this->_sortieBDD();
            }
        }

        //Forcage de la liberation d'espace memoire
        unset($rows);
    }

    protected function _sortieFichier($file)
    {
        try {
            //Recuperation du tableau des resultats de la requete
            $rows = $this->get_rows();

            //Ouverture du flux de sortie
            $handle = fopen($file, 'a');

            //Parcours de chaque ligne des resultats
            foreach ($rows as $row) {
                //Generation de la ligne a ecrire
                $line = $this->_ecritLigne($row);
                //Ecriture dans le flux de sortie
                fwrite($handle, $line);
            }

            //Fermetrure du flux
            fclose($handle);

            //Forcage de la liberation d'espace memoire
            unset($rows);
            unset($row);
        } catch (Exception $ex) {
            fwrite(STDERR, date('Y-m-d H:i:s')."\t".'Erreur : '.$ex->getMessage()."\n");
        }
    }

    protected function _ecritLigne($row)
    {
        $line = $this->get_table()."\t";
        //Parcours des champs de chaque ligne
        foreach ($row as $key => $value) {
            $line .= $value."\t";
        }
        $line .= "\n";
    }

    protected function _ecritLigneRequete($rows)
    {
        $buffer = '';
        foreach ($rows as $row) {
            //Mise a jour des valeurs des champs
            if (empty($champs_nom_fichier)) {
                $row[$champs_nom_fichier] = '';
            }
            if (empty($champs_organisme)) {
                $row[$champs_organisme] = '';
            }
            //Formatage de la requete
            $new_row = [$row[$champs_id_blob], $row[$champs_nom_fichier], $row[$champs_organisme]];
            $buffer .= '( '.implode(',', $new_row).' )'."\n";
        }

        return $buffer;
    }

    protected function _sortieBDD()
    {
        //Recuperation du tableau des resultats de la requete
        $rows = $this->get_rows();
        $n = is_countable($rows) ? count($rows) : 0;

        if ($n) {
            //Recuperation des noms des champs de la table contenant les IDs des blobs
            //$nom_table          = $this->get_blob_table()->get_table();
            //$champs_id_blob     = $this->get_blob_table()->get_champs_id_blob();
            //$champs_nom_fichier = $this->get_blob_table()->get_champs_nom_fichier();
            //$champs_organisme   = $this->get_blob_table()->get_champs_organisme();

            //Preparation de la requete
            //$query = 'INSERT INTO '.self::NOM_LOG_TABLE.' ( '.self::CHAMPS_NOM_TABLE_LOG_TABLE.', '.self::CHAMPS_ID_BLOB_LOG_TABLE.', '.self::CHAMPS_NOM_FICHIER_LOG_TABLE.', '
            //      .self::CHAMPS_ORGANISME_LOG_TABLE.', '.self::CHAMPS_DATE_IDENTIFICATION_LOG_TABLE.' ) VALUES '."\n";

            //Parcours des résultats et preparation des valeurs a inserer en base
            $buffer = $this->_ecritLigneRequete($rows);

            //Mise a jour de l'attribut de l'objet courant
            $this->set_query(/*$_query.*/ $buffer);

            //Execution de la requete
            try {
                $this->_executeRequete();
            } catch (Exception $ex) {
                fwrite(STDERR, date('Y-m-d H:i:s')."\t".'Erreur : '.$ex->getMessage()."\n");
            }
        }
    }
}

class Cli_BlobfileObsoleteIdentifieur extends Atexo_Cli
{
    //Type de sortie
    const SORTIE_FICHIER = 0;
    const SORTIE_BDD = 1;

    public static function run()
    {
        $input_organismes = null;
        $mode_sortie = null;
        try {
            $numb_argv = $_SERVER['argc'];
            $t_argv = $_SERVER['argv'];

            //Recuperation des arguements passes en parametre
            if ($numb_argv < 6) {
                $displayHelp = '';
                self::displayError('Pas assez d\'arguments fourni!', $displayHelp);
            }

            //Recuperation du mode de sortie
            if ('-of' == $t_argv[2]) {
                $mode_sortie = self::SORTIE_FICHIER;
            } elseif ('-odb' == $t_argv[2]) {
                $mode_sortie = self::SORTIE_BDD;
            } else {
                $displayHelp = 'Tapez "-of" pour une sortie fichier, "-odb" pour une sortie base de donnees';
                self::displayError('Le mode de sortie n\'est pas reconnu!', $displayHelp);
            }

            //Recuperation de la plateforme
            $plateforme = $t_argv[3];

            //Recuperation du fichier contenant le details des tables blobs
            $input_blob_tables = $t_argv[4];
            if (!file_exists($input_blob_tables)) {
                self::displayError('Le fichier "'.$input_blob_tables.'" n\'existe pas!');
            }
            //Verifier le format du fichier

            //Recuperation du fichier contenant le details des tables de reference
            $input_blob_reference_tables = $t_argv[5];
            if (!file_exists($input_blob_reference_tables)) {
                self::displayError('Le fichier "'.$input_blob_reference_tables.'" n\'existe pas!');
            }
            //Verifier le format du fichier

            //Recuperation du mode de parcours (tables organismes ou plateforme)
            $config_plateforme = '';
            if (isset($t_argv[6])) {
                $config_plateforme = (int) $t_argv[6];
            } else {
                $config_plateforme = 0;
            }

            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Debut du traitement : recherche de blobs obsoletes pour la plateforme "'.$plateforme.'"'."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fichier contenant le details des tables blob "'.$input_blob_tables.'"'."\n");
            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fichier contenant le details des tables referencant les blobs "'.$input_blob_reference_tables.'"'."\n");
            if (!empty($input_organismes)) {
                fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fichier contenant la liste des organismes "'.$input_organismes.'"'."\n");
            }

            //Instanciation du parcoureur
            $parcoureur = new BlobfileTableParcoureur($plateforme, $mode_sortie, $config_plateforme, $input_blob_tables, $input_blob_reference_tables);

            echo '<pre>';
            print_r($parcoureur);
            echo '</pre>';
            //Execution du parcours
            $parcoureur->parcours();

            fwrite(STDOUT, date('Y-m-d H:i:s')."\t".'Fin du traitement'."\n");
        } catch (Exception $ex) {
            fwrite(STDERR, date('Y-m-d H:i:s')."\t".'Erreur : '.$ex->getMessage()."\n");
        }
    }
}
