<?php

namespace Application\Cli;

use Application\Library\Propel\Query\Criteria;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Supprime les blobs chiffes depuis date, il prend en parametre le mode et la date.
 *
 * @category Atexo
 */
class Cli_ScriptSuppressionBlobsOrphelins extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        $help = false;
        $path = '';
        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        for ($i = 2; $i < $nbrArg; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('path' == $arg[0]) {
                $path = $arg[1];
            }
            if ('help' == $arg[0]) {
                $help = true;
            }
        }
        if ($help) {
            echo '
Paramètres :

    path : chemin du dossier à analyser

    help : aide

Fonctionnement :
vérification que le plob est orphelin , ecriture de la commande de suppression dans un fichier


            ';
        } else {
            if (empty($path)) {
                echo "merci de renseigner le dossier à analyser\n";

                return false;
            }
            self::doTraitement($path);
        }

        return true;
    }

    public static function doTraitement($path)
    {
        if (is_dir($path)) {
            $pointeur = opendir($path);
            //pour chaque fichier et dossier
            while ($fichier = readdir($pointeur)) {
                //on ne traite pas les . et ..
                if (('.' != $fichier) && ('..' != $fichier)) {
                    echo "$path/$fichier\n";
                    if (is_dir($path.'/'.$fichier)) {
                        //  echo "appel methode doTraitement\n";
                        self::doTraitement($path.'/'.$fichier);
                    } else {
                        //echo "appel methode traiterCSV\n";
                        self::traiterCSV($path, $fichier);
                    }
                }
            }
        }

        return false;
    }

    public static function traiterCSV($path, $file)
    {
        $connexion = null;
        $fileLog = self::getPathLog();
        if (is_file($path.'/'.$file)) {
            if (($handle = fopen($path.'/'.$file, 'r')) !== false) {
                while (($data = fgetcsv($handle, 100, ',')) !== false) {
                    $supprimer = false;
                    if (is_file($data[0])) {
                        $dossiers = explode('/', $data[0]);
                        if (in_array('files', $dossiers)) {
                            $fichier = explode('-', $dossiers[count($dossiers) - 1]);
                            $idBlob = $fichier[0];
                            $common = false;
                            $commonOuAcronyme = $dossiers[count($dossiers) - 3]; //vaut soit common soit l'acronyme de l'organisme
                            if ('common' === $commonOuAcronyme) {
                                $common = true;
                            }

                            if ($common) {
                                $blob = \CommonBlobPeer::retrieveByPK($idBlob, $connexion);
                                if (!($blob instanceof \CommonBlob)) {
                                    $supprimer = true;
                                }
                            } else {
                                $acronyme = $commonOuAcronyme;
                                $criteria = new Criteria();
                                $criteria->add(\CommonBlobOrganismePeer::ID, $idBlob);
                                $criteria->add(\CommonBlobOrganismePeer::ORGANISME, $acronyme);
                                $blob = \CommonBlobOrganismePeer::doSelectOne($criteria, $connexion);
                                if (!($blob instanceof \CommonBlobOrganisme)) {
                                    $supprimer = true;
                                }
                                unset($criteria);
                                unset($blob);
                            }
                            unset($fichier);
                            unset($idBlob);
                            unset($common);
                            unset($commonOuAcronyme);
                        }
                        unset($dossiers);
                    }
                    if ($supprimer) {
                        $command = "rm  $data[0];$data[1];$data[2] \n";
                        Atexo_Util::write_file($fileLog, $command, 'a');
                        unset($command);
                    }
                    unset($supprimer);
                    unset($data);
                }
                fclose($handle);
            }
        }
        unset($fileLog);

        return true;
    }

    public static function getPathLog()
    {
        $path = Atexo_Config::getParameter('LOG_DIR').'suppressionBlobsOrphelin';
        if (!is_dir($path)) {
            mkdir($path);
        }

        return $path.'/listeCommandes_'.date('Y_m_d');
    }
}
