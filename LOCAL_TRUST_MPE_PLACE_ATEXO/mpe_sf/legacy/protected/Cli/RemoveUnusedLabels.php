<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use DOMDocument;

/**
 * @author Khalid Atlassi <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_RemoveUnusedLabels extends Atexo_Cli
{
    //Exemple d'appel:
    //      ./mpe RemoveUnusedLabels 'protected/config/messages/messages.default.fr.xml'
    // Ou   ./mpe RemoveUnusedLabels 'protected/config/messages/messages.default.fr.xml' --remove
    // Le 1er exemple récupère la liste des libéllés à supprimer sans modifier la source
    // le 2eme exemple effectue l'opération de la suppression sur le fichier xml source

    public static function run()
    {
        if (isset($_SERVER['argv'][2])) {
            self::display('Génération de la liste des libéllés.');
            system('protected/bin/extractLabels.sh');
            $xmlFile = $_SERVER['argv'][2];
            $arrayList = self::removeUnusedLabelsFromXml($xmlFile, '/tmp/total_protected');
            if (is_countable($arrayList) ? count($arrayList) : 0) {
                $rmv = 'supprimés';
                if ('--remove' != $_SERVER['argv'][3]) {
                    $rmv = 'à supprimer';
                }
                self::display('"'.$_SERVER['argv'][2].'" >> Nombre des libéllés '.$rmv.': '.(is_countable($arrayList) ? count($arrayList) : 0));
                self::display('   (la liste des libéllés '.$rmv.' est enregistrée dans: "'.str_replace('.xml', '_RemovedLabels.xml', $xmlFile).'")');
            } else {
                self::display("Il n'y a aucun libéllé à supprimer.");
            }
            self::display("Fin de l'éxecution du cli");
        } else {
            self::display('Aide:');
            self::display('  ./mpe RemoveUnusedLabels [fileName] [--remove]');
            self::display('  [fileName]: spécifier le fichier xml');
            self::display("  [--remove]: effectue l'opération de la suppression sur le fichier xml [fileName]");
        }
    }

    public static function removeUnusedLabelsFromXml($xmlFile, $cmpFile)
    {
        $labelsFromDb = [];
        $removedLabels = [];
        $metaData = [];
        $xmlInput = [];
        $listOfUsedLabels = [];

        $dom = new DOMDocument();
        //Chargement du fichier XML
        $dom->load($xmlFile);
        //Chargement de la liste des libéllés à comparer avec les libellés du fichier xml
        $labelsFromProject = file($cmpFile);
        //Chargement de libéllés depuis la BD

        $allOrganisme = (new Atexo_Organismes())->retrieveAllOrganismes();
        if ($allOrganisme) {
            $labelsFromDb = self::getAllLibelle($allOrganisme[0]->getAcronyme());
        }
        //Fusion de la liste des libellés à comparer
        $arrayToCmp = array_merge($labelsFromProject, $labelsFromDb);

        //Chargemùent des metadonnées du fichier XML se trouvant dans le tag 'file'
        $elements = $dom->getElementsByTagName('file');
        foreach ($elements as $elt) {
            $metaData['datatype'] = $elt->getAttribute('datatype');
            $metaData['date'] = $elt->getAttribute('date');
            $metaData['original'] = $elt->getAttribute('original');
            $metaData['source-language'] = $elt->getAttribute('source-language');
            $metaData['target-language'] = $elt->getAttribute('target-language');
        }

        //Récuperation de la liste des libellés depuis le fichier XML
        $elts = $dom->getElementsByTagName('trans-unit');
        //Sauvegarde des infos concernant le chaque libellé dans un array
        $i = 0;
        foreach ($elts as $elt) {
            $xmlInput[$i]['source'] = $elt->firstChild->nodeValue;
            $xmlInput[$i]['target'] = $elt->lastChild->nodeValue;
            $xmlInput[$i]['id'] = $elt->getAttribute('id');
            ++$i;
        }
        //Comparaison et suppression des libellés non utilisés
        for ($j = 0; $j < count($xmlInput); ++$j) {
            if (!self::existsIn(trim($xmlInput[$j]['source']), $arrayToCmp)) {
                $removedLabels[] = $xmlInput[$j];
            } else {
                $listOfUsedLabels[] = $xmlInput[$j];
            }
        }
        //Creation du fichier xml resultant
        if (count($removedLabels)) {
            if ('--remove' == $_SERVER['argv'][3]) {
                self::createXML($xmlFile, $listOfUsedLabels, $metaData);
            }
            self::createXML(str_replace('.xml', '_RemovedLabels.xml', $xmlFile), $removedLabels, $metaData);
        }

        return $removedLabels;
    }

    public static function getAllLibelle($org)
    {
        $etatutEnveloppeList = (new Atexo_Consultation())->getStatutEnveloppe($org);
        $echangeTypeMessageList = (new Atexo_Message())->getEchangeTypeMessage($org);
        $etatConsultationList = (new Atexo_Consultation())->getEtatConsultation(Atexo_Config::getParameter('COMMON_DB'));
        $typeDecisionList = (new Atexo_Consultation_Decision())->getTypeDecision();

        return array_merge($etatutEnveloppeList, $echangeTypeMessageList, $etatConsultationList, $typeDecisionList);
    }

    public static function existsIn($value, $elts)
    {
        foreach ($elts as $elt) {
            if (trim($value) === trim($elt)) {
                return 1;
            }
        }

        return 0;
    }

    public static function createXML($destination, $messages, $metaData)
    {
        //Création du document XML
        $file = new DOMDocument('1.0', 'UTF-8');

        //Definir le tag root
        $root = $file->createElement('xliff');
        $root->setAttribute('version', '1.0');
        $root = $file->appendChild($root);
        //Definir le tag file et ses attributs
        $elementFile = $file->createElement('file');
        $elementFile->setAttribute('datatype', $metaData['datatype']);
        $elementFile->setAttribute('date', $metaData['date']);
        $elementFile->setAttribute('original', $metaData['original']);
        $elementFile->setAttribute('source-language', $metaData['source-language']);
        $elementFile->setAttribute('target-language', $metaData['target-language']);
        $elementFile = $root->appendChild($elementFile);

        //Definir le tag body
        $elementBody = $file->createElement('body');
        $elementBody = $elementFile->appendChild($elementBody);

        //Definir les tags des libellés
        $i = 0;
        foreach ($messages as $unMessage) {
            $elementTransUnit = $file->createElement('trans-unit');
            $elementTransUnit->setAttribute('id', $unMessage['id']);
            $elementTransUnit = $elementBody->appendChild($elementTransUnit);

            $elementSource = $file->createElement('source');
            $elementSource = $elementTransUnit->appendChild($elementSource);
            $textSource = $file->createTextNode($unMessage['source']);
            $textSource = $elementSource->appendChild($textSource);
            $elementTarget = $file->createElement('target');
            $elementTarget = $elementTransUnit->appendChild($elementTarget);
            $textTarget = $file->createTextNode($unMessage['target']);
            $textTarget = $elementTarget->appendChild($textTarget);
            ++$i;
        }

        //Formatage du fichier xml avant la sauvegarde
        $xmlString = $file->saveXML();
        $search = ['<body>', '</trans-unit>', '><file', '><body', '</file>', '</xliff>'];
        $replace = ["<body>\n\t\t", "</trans-unit>\n\t\t", ">\n\t<file", ">\n\t\t<body", "\n\t</file>", "\n</xliff>"];
        $newXml = str_replace($search, $replace, $xmlString);
        $file->loadXML($newXml);

        //Sauvegarde du fichier xml destination
        $file->save($destination);
    }
}
