<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_SendFiles;
use Application\Service\Atexo\Controller\Cli;
use Exception;

/**
 * Class Cli_SendFENToChorus
 * Envoi manuel de flux FEN111 et FEN211 vers CHORUS.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Cli_SendFENToChorus extends Atexo_Cli
{
    public static function run()
    {
        $arrayNameFiles = [];
        $logger = Atexo_LoggerManager::getLogger('chorus');
        try {
            echo PHP_EOL.PHP_EOL.'-------- Debut Envoi Fichier vers Chorus -------'.PHP_EOL.PHP_EOL;

            $prefixName = '';
            $dirName = Atexo_Config::getParameter('DIR_FILE_CHORUS_ENVOI');
            echo " -> Repertoire de recuperation des fichiers : $dirName ".PHP_EOL;

            $arguments = $_SERVER['argv'];
            if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
                echo ' -> Merci de preciser les noms des flux FEN111 ou FEN211 a envoyer a chorus ( Exemple : ./mpe SendFENToChorus FEN0111A_PMI001_PMI001011122053 FEN0211A_PMI001_PMI001021100074 ) '.PHP_EOL.PHP_EOL;

                return;
            }

            $arrayNameFiles = [];
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                $arrayNameFiles[] = $arguments[$i];
            }

            echo PHP_EOL.' -> Liste des fichiers a envoyer a CHORUS : '.print_r($arrayNameFiles, true);

            if (!empty($arrayNameFiles)) {
                foreach ($arrayNameFiles as $nameFile) {
                    $cheminCompletFichier = $dirName.$prefixName.$nameFile;
                    echo PHP_EOL.' -> Debut envoi du fichier '.$cheminCompletFichier;

                    if (!strstr($nameFile, 'FEN')) {
                        echo PHP_EOL." -> Le fichier $nameFile doit etre un FEN111 ou FEN211 ".PHP_EOL;
                        echo ' -> Traitement fichier suivant ... '.PHP_EOL;
                        continue;
                    }

                    if (!is_file($cheminCompletFichier)) {
                        echo PHP_EOL." -> Le fichier $cheminCompletFichier n'existe pas sur le serveur".PHP_EOL;
                        echo ' -> Traitement fichier suivant ... '.PHP_EOL;
                        continue;
                    }

                    $result = Atexo_SendFiles::sendFile($cheminCompletFichier, Atexo_Config::getParameter('URL_ACCES_CHORUS'), true, Atexo_Config::getParameter('CHORUS_SSLCERT'), Atexo_Config::getParameter('CHORUS_SSLCERTPASSWD'));
                    if (false === $result) {
                        echo PHP_EOL." -> Erreur envoi du fichier $cheminCompletFichier vers CHORUS ".PHP_EOL;
                        echo PHP_EOL.PHP_EOL.' -> Traitement fichier suivant ... '.PHP_EOL;
                        continue;
                    }

                    echo PHP_EOL.'****************************** Fichier envoye avec SUCCES ******************************';
                    echo PHP_EOL.'Resultat envoi = '.print_r($result, true);
                    echo PHP_EOL.'Fin envoi du fichier '.$cheminCompletFichier.' ';
                }
            }
        } catch (Exception $e) {
            $erreur = PHP_EOL.PHP_EOL.'-> Erreur envoi de fichiers vers Chorus : liste des fichiers : '.print_r($arrayNameFiles, true).PHP_EOL.PHP_EOL.' Erreur : '.$e->getMessage().PHP_EOL.PHP_EOL.'Trace : '.$e->getTraceAsString();
            echo $erreur;
            $logger->error($erreur);
        }
        echo PHP_EOL.PHP_EOL.'-------- Fin Envoi Fichier vers Chorus -------'.PHP_EOL.PHP_EOL;
    }
}
