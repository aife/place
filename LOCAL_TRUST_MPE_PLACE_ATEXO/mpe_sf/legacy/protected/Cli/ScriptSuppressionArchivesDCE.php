<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Il calcule par organisme et consultation le nombre de versions de DCE qui sont potentiellement supprimable.
 * Une version de DCE est potentiellement supprimable si et seulement si :
 * cette version de DCE a été créée avant la date de mise en ligne calculée de la consultation et qu'elle n'est pas unique (autrement dit : il y a eu au moins une modification sur le DCE avant la mise en ligne de la consultation)
 * ou que la consultation est archivée et que cette version de DCE a été créée avant la date de fin de la consultation et qu'elle n'est pas unique.
 *
 * @category Atexo
 */
class Cli_ScriptSuppressionArchivesDCE extends Atexo_Cli
{
    public static function run()
    {
        $MPE_BIN = null;
        $logger = self::intiLog();
        $arguments = $_SERVER['argv'];
        $help = false;
        $modeReel = false;
        $pas = 0;
        $organisme = null;
        $statutsCons = null;
        $type_dce_avant_mise_en_ligne = true;

        $nbrArg = is_countable($arguments) ? count($arguments) : 0;
        for ($i = 2; $i < $nbrArg; ++$i) {
            $arg = explode('=', $arguments[$i]);
            if ('mode' == $arg[0]) {
                if ('reel' == $arg[1]) {
                    $modeReel = true;
                }
            }

            if ('organisme' == $arg[0]) {
                $organisme = $arg[1];
            }

            if ('pas' == $arg[0]) {
                $pas = (int) $arg[1];
            }
            if ('help' == $arg[0]) {
                $help = true;
            }
            if ('statut_cons' == $arg[0]) {
                $statutsCons = explode(',', $arg[1]);
            }
            if ('type_dce' == $arg[0]) {
                if ('avant_date_fin' == $arg[1]) {
                    $type_dce_avant_mise_en_ligne = false;
                }
            }
        }
        if ($help) {
            echo "
Parametres :

    mode : reel / simule par defaut simule si pas renseigne
    s'ecrit comme suit mode=reel ou mode=simule

	organisme : acronyme de l'organisme sur lequel est execute le programme
	
	statut_cons : liste des statuts des consultations par défaut statut archive réalisée
          option possible :
                0 : consultation en ligne
                4 : décision
                5 : à archiver
                6 : archive réalisé
	
	type_dce : les types de DCE à supprimer par défaut on supprime les DCEs qui ont été créés avant la mise en ligne de la consultation sauf le dernier
          option possible :
                avant_mise_en_ligne : supprime les versions des DCEs avant la mise en ligne de la consultation sauf le dernier (fonctionnement par défaut)
                avant_date_fin : supprime les versions des DCEs avant la date de fin de la consultation sauf le dernier (fonctionnement uniquement sur les consultations archivées)

    pas : traiter les infos par tranche pour ne pas avoir de probleme de performence par defaut 100

    help : aide

Fonctionnement :
	Si on lance comme suit $MPE_BIN ScriptSuppressionArchivesDCE statut_cons=x,y,z type_dce=avant_mise_en_ligne (ou rien pour la valeur ou autre chose) alors 
    on recupere pour les consultations avec les filtres qui correspondent aux parametres passes en ligne de commande 
	on recherche parmi ces consultations celles ont plusieurs DCE ajoutes avant la date de mise en ligne calculee
	les consultations qui n'ont qu'un seul DCE sont ignorees
	on boucle sur ces consultations et on regarde celles qui ont plusieurs versions de DCE et qui datent d'avant la mise en ligne de la consultation
	pour ces consultations on supprime les DCEs et on ne garde que le dernier
	
	Si le parametre la valeur type_dce=avant_date_fin, on lance comme suit $MPE_BIN ScriptSuppressionArchivesDCE type_dce=avant_date_fin
	le parametre statut_cons est ignore et vaut par défaut 6.
	on recupere pour les consultations archivées avec les filtres qui correspondent aux parametres passes en ligne de commande 
	on recherche parmi ces consultations celles ont plusieurs DCE ajoutes avant la date de fin
	les consultations qui n'ont qu'un seul DCE sont ignorees
	on boucle sur ces consultations et on regarde celles qui ont plusieurs versions de DCE et qui datent d'avant la fin de la consultation
	pour ces consultations on supprime les DCEs et on ne garde que le dernier
	
    si mode simulation , on boucle sur les résultats afin d'alimenter le fichier csv retourné
    si mode réel, on supprime le fichier sur le disque et si l'opération réussie on supprime de la BD le DCE 
à la fin de l'exécution on affiche les chemins des fichiers de log csv
            ";
        } else {
            if (null == $organisme) {
                $logger->info('le traitement va etre realise sur tous les organismes');
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $listOrganismes = CommonOrganismePeer::doSelect(new Criteria(), $connexion);
                foreach ($listOrganismes as $unOrganisme) {
                    $logger->info("le traitement va etre realise sur l'organisme ".$unOrganisme->getAcronyme());
                    self::doTraitement($unOrganisme->getAcronyme(), $type_dce_avant_mise_en_ligne, $modeReel, $statutsCons, $pas, $logger);
                }
            } else {
                $logger->info("le traitement va etre realise sur l'organisme $organisme");
                self::doTraitement($organisme, $type_dce_avant_mise_en_ligne, $modeReel, $statutsCons, $pas, $logger);
            }
        }

        return true;
    }

    public static function doTraitement($organisme, $type_dce_avant_mise_en_ligne, $modeReel, $statutCons = null, $pas = 0, $logger = null)
    {
        $params = [];
        if (null === $logger) {
            $logger = self::intiLog();
        }
        $nbreTotal = 0;
        $pas = ($pas) ?: 100;

        $tailleTotal = 0;
        $nbreTotalDCE = 0;

        $statutCons = ($statutCons) ?: [Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')];
        $critereDate = 'date_mise_en_ligne_calcule';
        if ($type_dce_avant_mise_en_ligne) {
            $logger->info('le traitement va etre realise sur toutes les consultations en fonction des paramètres passés');
        } else {
            $logger->info('le traitement va etre realise sur toutes les consultations archivées et sur les DCEs dont la date de creation est inférieure à date de fin (type_dce=avant_date_fin)');
            $statutCons = [Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')];
            $critereDate = 'datefin';
        }
        $baseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR');

        $critereStatutCons = "AND
        consultation.id_etat_consultation  in ('".implode("','", $statutCons)."' ) ";

        $sqlConsultations = <<<QUERY
SELECT 
	consultation.`id`, consultation.`organisme`, consultation.`reference`, consultation.`reference_utilisateur`, consultation.`datedebut`, consultation.`datefin`, consultation.`datevalidation`, consultation.`date_mise_en_ligne_calcule`, consultation.id_etat_consultation, COUNT(DCE.id) as nombreDCE
FROM
	consultation 
LEFT JOIN DCE 
	on (consultation.`organisme`=DCE.organisme and consultation.`id`=DCE.consultation_id and DCE.untrusteddate < consultation.$critereDate) 
WHERE 
	consultation.`organisme` = :organisme
	$critereStatutCons
GROUP BY
	consultation.`organisme`, consultation.`id` 
HAVING
	count(DCE.id) > 1

QUERY;

        $params[':organisme'] = $organisme;
        $statement = Atexo_Db::getLinkCommon()->prepare($sqlConsultations);
        $statement->execute($params);
        $nbreConsultations = $statement->rowCount();

        $ligneEntete = "organisme | reference | reference_utilisateur | datedebut | datefin | datevalidation | date_mise_en_ligne_calcule | id_etat_consultation | nombreDCE | Taille supprimee | STATUT\n";
        $path = self::getPathLog();
        $sufix = date('Y-m-d').'_'.$organisme;
        if ($modeReel) {
            $file = $path.'/logReelConsultation_'.$sufix.'.csv';
            $logger->info('Mode : Reel');
        } else {
            $file = $path.'/logSimulationConsultation_'.$sufix.'.csv';
            $logger->info('Mode : Simulation');
        }
        $logFileGlobal = $path.'/logGlobal_'.date('Y-m-d').'.csv';

        Atexo_Util::write_file($file, $ligneEntete, 'a');
        ob_flush();
        flush();
        $logger->info("Nombre de consultations à traiter $nbreConsultations pour l'organisme $organisme :");

        $listeConsultations = $statement->fetchAll(PDO::FETCH_ASSOC);
        try {
            foreach ($listeConsultations as $uneConsultation) {
                $logger->info('traitement de la consultation '.$uneConsultation['organisme'].' '.$uneConsultation['reference']);
                $ligneCsv = $uneConsultation['organisme'].'|';
                $ligneCsv .= $uneConsultation['reference'].'|';
                $ligneCsv .= $uneConsultation['reference_utilisateur'].'|';
                $ligneCsv .= $uneConsultation['datedebut'].'|';
                $ligneCsv .= $uneConsultation['datefin'].'|';
                $ligneCsv .= $uneConsultation['datevalidation'].'|';
                $ligneCsv .= $uneConsultation['date_mise_en_ligne_calcule'].'|';
                $ligneCsv .= $uneConsultation['id_etat_consultation'].'|';
                $ligneCsv .= $uneConsultation['nombreDCE'].'|';
                $date = $uneConsultation['date_mise_en_ligne_calcule'];
                $taille = 0;

                try {
                    if (!$type_dce_avant_mise_en_ligne) {
                        $date = $uneConsultation['datefin'];
                    }

                    $return = self::deleteArchivesDCE(
                        $modeReel,
                        $uneConsultation['organisme'],
                        $uneConsultation['reference'],
                        $date,
                        $baseRootDir,
                        $uneConsultation['id'],
                        $logger
                    );

                    $taille = $return['taille'];
                    $statut = $return['statut'];
                    $tailleTotal += $taille;
                    $nbreTotalDCE += $return['nbreDCE'];
                } catch (Exception $e) {
                    $statut = 'ERREUR LORS DU TRAITEMENT DE LA CONSULTATION '.$uneConsultation['organisme'].' '.$uneConsultation['reference'].' '.$e->getMessage().'  '.$e->getTraceAsString();
                }

                $ligneCsv .= $taille.'|';
                $ligneCsv .= $statut."\n";
                Atexo_Util::write_file($file, $ligneCsv, 'a');
                ob_flush();
                flush();
                unset($ligneCsv);
                unset($taille);
                unset($statut);
                unset($uneConsultation);
                unset($return);
            }
            unset($listeConsultations);
        } catch (Exception $e) {
            $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString();
            $logger->error($log);
            if (isset($file)) {
                $logger->info('les blocs traités sont avant erreur sont dans le fichier '.$file);
            }
        }
        $logger->info('fin de traitement, le fichier génée est enregistré sur '.$file);
        Atexo_Util::write_file($logFileGlobal, $organisme.'|'.$nbreConsultations.'|'.$nbreTotalDCE.'|'.$tailleTotal."\n", 'a');
    }

    public static function intiLog()
    {
        $logger = Atexo_LoggerManager::getLogger('archivage');

        return $logger;
    }

    public static function getPathLog()
    {
        $path = Atexo_Config::getParameter('LOG_DIR').'suppressionArchivesDCE';
        if (!is_dir($path)) {
            mkdir($path, '0755');
        }

        return $path;
    }

    private static function deleteArchivesDCE($modeReel, $organisme, $referenceConsultation, $date, $baseRootDir, $consultationId, $logger = \NUL)
    {
        $params = [];
        //rechercher les DCEs qui ont été créés avant la date passee en parametre
        if (null === $logger) {
            $logger = self::intiLog();
        }
        $statut = 'AUCUN DCE';
        $taille = 0;

        $sqlDCE = <<<QUERY
SELECT 
	DCE.`id`, DCE.`consultation_id`, DCE.`dce` as id_blob, DCE.untrusteddate
FROM
	DCE 
WHERE 
	DCE.`organisme` = :organisme
AND
	DCE.consultation_id = :consultationId
AND
	DCE.untrusteddate < :date
ORDER BY
	DCE.`untrusteddate` ASC 
QUERY;

        $params[':organisme'] = $organisme;
        $params[':consultationId'] = $consultationId;
        $params[':date'] = $date;

        $statement = Atexo_Db::getLinkCommon()->prepare($sqlDCE);
        $statement->execute($params);
        $nbreDCEs = $statement->rowCount();

        $logger->info("Il y a $nbreDCEs DCEs à traiter pour la consultation $referenceConsultation de l'organisme $organisme dont la date est $date");
        $tailleTotal = 0;
        $statutGlobal = '';

        if ($nbreDCEs > 1) {
            // si le nombre > 1, supprimer les n-1 premiers avec n = dernier créé avant la date
            //boucler sur les n-1 et appeler la méthode deleteBlob
            $ligneEntete = "organisme | referenceConsultation | idDCE | idBlob | date max | untrusteddate | taille blob | STATUT\n";
            $path = self::getPathLog();
            $sufix = date('Y-m-d').'_'.$organisme;
            if ($modeReel) {
                $file = $path.'/logReelDCE_'.$sufix.'.csv';
                $logger->info('Mode : Reel');
            } else {
                $file = $path.'/logSimulationDCE_'.$sufix.'.csv';
                $logger->info('Mode : Simulation');
            }
            Atexo_Util::write_file($file, $ligneEntete, 'a');
            ob_flush();
            flush();

            $listeDCEs = $statement->fetchAll(PDO::FETCH_ASSOC);
            $nbreDCEsSupprimes = 1;
            try {
                foreach ($listeDCEs as $unDCE) {
                    $logger->info('traitement du DCE '.$unDCE['id']." de l'organisme / consultation : ".$organisme.', '.$referenceConsultation);
                    $ligneCsv = $organisme.'|';
                    $ligneCsv .= $referenceConsultation.'|';
                    $ligneCsv .= $unDCE['id'].'|';
                    $ligneCsv .= $unDCE['id_blob'].'|';
                    $ligneCsv .= $date.'|';
                    $ligneCsv .= $unDCE['untrusteddate'].'|';

                    try {
                        if ($nbreDCEsSupprimes < $nbreDCEs) {
                            $result = self::deleteBlob($modeReel, $unDCE['id'], $unDCE['id_blob'], $organisme, $baseRootDir);
                            $taille = $result['taille'];
                            $statut = $result['statut'];
                            if (('DELETE OK' === $statut) || ('DELETE STBY' === $statut)) {
                                ++$nbreDCEsSupprimes;
                                $tailleTotal += $taille;
                                $statutGlobal .= '-'.$statut;
                            }
                        } else {
                            $taille = '?';
                            $statut = 'CONSERVE';
                        }
                    } catch (Exception $e) {
                        $statut = 'ERREUR LORS DU TRAITEMENT DU DCE '.$unDCE['id']." de l'organisme / consultation : ".$organisme.', '.$referenceConsultation.' '.$e->getMessage().'  '.$e->getTraceAsString();
                    }

                    $ligneCsv .= $taille.'|';
                    $ligneCsv .= $statut."\n";
                    Atexo_Util::write_file($file, $ligneCsv, 'a');
                    ob_flush();
                    flush();
                    unset($ligneCsv);
                    unset($taille);
                    unset($statut);
                    unset($unDCE);
                    unset($result);
                }
                unset($listeDCEs);
            } catch (Exception $e) {
                $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString()."\n";
                $logger->error($log);
                if (isset($file)) {
                    $logger->info('les blocs traités sont avant erreur sont dans le fichier '.$file);
                }
                $logger->info('fin de traitement, le fichier génée est enregistré sur '.$file);
            }
        } else {
            //si le nombre <= 1, ne rien faire
            $logger->info('Aucun traitement sur les DCEs de cette consultation');
        }

        return ['statut' => $statutGlobal, 'taille' => $tailleTotal, 'nbreDCE' => $nbreDCEsSupprimes];
    }

    private static function deleteBlob($modeReel, $idDCE, $idBlob, $organisme, $baseRootDir, $logger = null)
    {
        $params = [];
        $file = null;
        if (null === $logger) {
            $logger = self::intiLog();
        }
        $pathFile = $baseRootDir.'/'.$organisme.'/files/'.$idBlob.'-0';
        if (!file_exists($pathFile)) {
            $pathFile = Atexo_Blob::getPathFile($idBlob, $organisme);
        }

        $statut = 'DELETE KO';
        $taille = 0;
        try {
            $fileExist = is_file($pathFile);
            if ($fileExist) {
                $taille = filesize($pathFile);
                $logger->info('le chemin du fichier à supprimer est '.$pathFile.' et sa taille est de '.$taille);
            } else {
                $logger->info("le fichier n'existe pas sur le disque, la suppression de la base de donnees va être réalisée");
            }
            if ($modeReel) {
                if (unlink($pathFile) || !$fileExist) {
                    $sqlBlobFile = <<<QUERY
DELETE FROM
blobOrganisme_file
WHERE
	blobOrganisme_file.id = :idBlob
AND
	blobOrganisme_file.organisme like :organisme
QUERY;

                    $sqlDCE = <<<QUERY
DELETE FROM
DCE
WHERE
	DCE.id = :idDCE
AND
	DCE.organisme like :organisme
QUERY;

                    $params['idBlob'] = $idBlob;
                    $params['organisme'] = $organisme;

                    $statement = Atexo_Db::getLinkCommon()->prepare($sqlBlobFile);
                    $statement->execute($params);
                    unset($statement);
                    unset($params);

                    $params['organisme'] = $organisme;
                    $params['idDCE'] = $idDCE;
                    $statement = Atexo_Db::getLinkCommon()->prepare($sqlDCE);
                    $statement->execute($params);
                    unset($statement);
                    unset($params);
                    $statut = 'DELETE OK';
                }
            } else {
                $statut = 'DELETE STBY';
            }

            return ['statut' => $statut, 'taille' => $taille];
        } catch (Exception $e) {
            $log = 'une erreur est survenu lors de la suppression des blocs : '.$e->getMessage().'  '.$e->getTraceAsString()."\n";
            $logger->error($log);
            if (isset($file)) {
                $logger->info('les blocs traités sont avant erreur sont dans le fichier '.$file);
            }
        }
    }
}
