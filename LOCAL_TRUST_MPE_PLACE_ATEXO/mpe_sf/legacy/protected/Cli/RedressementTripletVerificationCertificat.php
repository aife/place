<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;

class Cli_RedressementTripletVerificationCertificat extends Atexo_Cli
{
    /*
     *Apres l evolution 16117: MPE v4 : Validation des certificats les nouvelles valeurs pour
     *la chaine et la validité des dates sont 0=OK; 1=UNKOWN; 2=KO
     *au lieu de 0=OK et 1=KO
     *CRL = $resultat[1]
     *validite = $resultat[2]
     *chaine = $resultat[0]
     */
    public static function run()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $fichierEnveloppes = CommonFichierEnveloppePeer::doSelect($c, $connexion);
        $i = 0;

        echo "Veuillez patienter un moment.................... \n";
        foreach ($fichierEnveloppes as $one) {
            $resultat = explode('-', $one->getVerificationCertificat());
            if ('1' == $resultat[0]) {
                $newResultat0 = '2';
            }
            if ('1' == $resultat[2]) {
                $newResultat2 = '2';
            }
            if ('1' == $resultat[0] || '1' == $resultat[2]) {
                ++$i;
                echo 'Traitement de fichierEnveloppe id = '.$one->getIdFichier()."\n";
                echo 'valeur actuelle '.$one->getVerificationCertificat()."\n";

                echo 'nouvelle valeur '.$newResultat0.'-'.$resultat[1].'-'.$newResultat2."\n";

                $one->setVerificationCertificat($newResultat0.'-'.$resultat[1].'-'.$newResultat2);
                $one->save($connexion);
                echo '.';
            }
        }
        echo "\nNombre des lignes redressées : ".$i." \n";
    }
}
