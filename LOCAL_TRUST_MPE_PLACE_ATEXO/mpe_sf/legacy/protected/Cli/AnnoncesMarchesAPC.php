<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonGeolocalisationN2Peer;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;

/**
 * Cron de mise a jour des consultations du portail Annonces-Marches depuis AchatPublic.Com (APC).
 *
 * @category Atexo
 */
class Cli_AnnoncesMarchesAPC extends Atexo_Cli
{
    const FETCH_FROM_APC = 1;
    const FETCH_FROM_LOCAL_CACHE = 0;

    protected static string $acronyme = '';
    protected static string $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)';
    protected static string $urlRecherche = 'https://www.achatpublic.com/sdm/ent/gen/ent_recherche.do';
    protected static string $urlDetail = 'https://www.achatpublic.com/sdm/ent/gen/ent_detail.do?selected=0&PCSLID=';
    protected static int $nbResults = 2000; // nb de resultat à recupérer
    protected static int $nbDetail = 30; // nb de cons. à récupérer toutes les heures
    protected static string $organismeFiltre = '';
    protected static string $lieuExecution = '';

    // Regex pour la page de détail d'une consultation
    private static string $regexLieuExecution = '#<table width="100%" border="0" cellspacing="2" cellpadding="0">(.*)</table>#Us';
    private static string $regexInfosConsultation = '#<table width="650px" class="arial_12 fixed_grey">(.*)</table>[^<]*</td>[^<]*</tr>[^<]*</table>#Us';
    private static string $regexDetailLots = '#<td class="ligne_moteur0[2,3]">(.*)</td>.*<td class="ligne_moteur0[2,3]">(.*)</td>#Us';

    // Regex pour la recherche FULL (résultat de la page de recherche)
    private static string $regexBlocsConsultations = '#<tr class="hand_cursor[^>]+">(.*)</tr>\r\n[^<]+\r\n#Us';
    private static string $regexDetailConsultation = "#detail\(document.getElementById\('consulId'\),'([^']+)'#";

    /**
     * Point d'entrée du script.
     */
    public static function run()
    {
        setlocale(LC_TIME, 'fr_FR');
        try {
            $options = new \Zend_Console_Getopt('-afd:o:s:l:');
            //$options = getopt('-afd:o:s:l:');
            if (isset($options->s)) {
                self::$acronyme = $options->s;
            }
            if (isset($options->a)) {
                if (isset($options->d)) {
                    echo "Les options -a et -d ne peuvent etre utilise en meme temps.\n";
                    exit(1);
                }
                if (isset($options->o)) {
                    self::$organismeFiltre = urlencode($options->o);
                }
                if (isset($options->l)) {
                    self::$lieuExecution = $options->l;
                }
                if (isset($options->f)) {
                    self::fetchAllConsultations(self::FETCH_FROM_APC);
                } else {
                    self::fetchAllConsultations(self::FETCH_FROM_LOCAL_CACHE);
                }
            } elseif (isset($options->d)) {
                if ('RANDOM' == $options->d) {
                    $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $c = new Criteria();
                    $c->setLimit(self::$nbDetail);
                    $c->add(CommonConsultationPeer::ORGANISME, self::$acronyme);
                    $c->add(CommonConsultationPeer::DATEDEBUT, '0000-00-00');
                    $c->add(CommonConsultationPeer::DATEFIN, date('Y-m-d'), Criteria::GREATER_EQUAL);
                    $c->add(CommonConsultationPeer::ID_TYPE_PROCEDURE, ['33', '32'], Criteria::NOT_IN);
                    $c->addDescendingOrderByColumn(CommonConsultationPeer::ID);
                    $commonConsultations = CommonConsultationPeer::doSelect($c, $connexionCom);
                    $i = 0;
                    foreach ($commonConsultations as $commonConsultation) {
                        $url = $commonConsultation->getUrlConsultationAchatPublique();
                        $id = substr($url, strrpos($url, '=') + 1); //On récupère l'ID via l'url APC
                        $sleep = random_int(30, 120);
                        ++$i;
                        echo '['.date('Y-m-d H:i:s')." $i/".self::$nbDetail."] Traitement de $id dans $sleep secondes...\n";
                        sleep($sleep);
                        self::fetchConsultation($id);
                        echo '['.date('Y-m-d H:i:s')." $i/".self::$nbDetail."] Traitement de $id terminé\n\n";
                    }
                } else {
                    self::fetchConsultation($options->d);
                }
            } else {
                echo "USAGE:  -a [-f] | -d ID\n";
                echo "\t-a : Recupere toutes les consulations\n";
                echo "\t-f : Utilise avec -a. Va cherche sur APC au lieu du cache local\n";
                echo "\t-d ID : Recupere les informations de cette consultation (ID coté APC (commence par CSL..))\n";
                echo "\t-d RANDOM : Recupere les informations de ".self::$nbDetail." consultations aleatoirement sur 1 heure\n";
            }
        } catch (\Zend_Console_Getopt_Exception $e) {
            self::displayError($e->getMessage().PHP_EOL.$e->getUsageMessage());
        }
    }

    /**
     * Connexion HTTP.
     *
     * @param string $url  URL HTTP
     * @param string $post Valeur des champs à poster (url_encode) ou null si simple GET
     */
    protected static function httpGet($url, $post = null, $id = 'none')
    {
        $params = [];
        $params[CURLOPT_URL] = $url;
        $params[CURLOPT_COOKIEFILE] = '/tmp/apc_cookie.'.$id.'.tmp';
        $params[CURLOPT_COOKIEJAR] = '/tmp/apc_cookie.'.$id.'.tmp';
        $params[CURLOPT_HEADER] = false;
        $params[CURLOPT_RETURNTRANSFER] = true;
        $params[CURLOPT_SSL_VERIFYPEER] = false;
        $params[CURLOPT_USERAGENT] = self::$userAgent;
        $params[CURLOPT_HTTPHEADER] = ['Accept-Language: fr,fr-fr;q=0.8,en-us;q=0.5,en;q=0.3'];
        $params[CURLOPT_BUFFERSIZE] = 64192;
        $params[CURLOPT_CONNECTTIMEOUT] = Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT');
        if (null !== $post) {
            $params[CURLOPT_POST] = true;
            $params[CURLOPT_POSTFIELDS] = $post;
        }
        $ch = curl_init();
        curl_setopt_array($ch, $params);
        $html = curl_exec($ch);
        curl_close($ch);

        return $html;
    }

    /**
     * Récupère le détail d'une consultation particulière.
     *
     * @param string $id Identifiant APC de la consultation (de type CSL_****)
     */
    protected static function fetchConsultation($id)
    {
        $infos = [];
        $lots = [];
        $urlDetail = self::$urlDetail.$id;
        $localCachePage = Atexo_Config::getParameter('COMMON_TMP').'/achatpublic_'.md5($urlDetail).'.html.gz';
        @unlink($localCachePage);
        $uniqid = uniqid();
        // On récupère la page en cache si elle existe pour éviter de réinterroger APC
        if (!is_file($localCachePage)) {
            echo "Getting $urlDetail from APC...\n";
            $html = self::httpGet($urlDetail, null, $uniqid);
            file_put_contents($localCachePage, gzencode($html));
        } else {
            echo "local\n";
            $html = implode("\n", gzfile($localCachePage));
        }
        // Recuperation des lieux d'exécutions
        preg_match(self::$regexLieuExecution, $html, $lieuExec);
        $tmpLE = str_replace('</br>', '|', $lieuExec[1]);
        $tmpLE = preg_replace('/[0-9]+ -/', '', $tmpLE);
        $tmpLE = trim(implode('', preg_split("/\s?<[^<]+>\s?/", preg_replace("/\s{2,}/", ' ', html_entity_decode($tmpLE, ENT_QUOTES)), -1, PREG_SPLIT_NO_EMPTY)));
        $html = str_replace($lieuExec[0], $tmpLE, $html);
        // on récupère les infos de la consulation
        preg_match(self::$regexInfosConsultation, $html, $consBloc);
        $listing = preg_split("/\s?<[^<]+>\s?/", preg_replace("/\s{4,}| :(<)/", ' \\1', html_entity_decode($consBloc[1], ENT_QUOTES)), -1, PREG_SPLIT_NO_EMPTY);
        for ($i = 0; $i < (is_countable($listing) ? count($listing) : 0); $i = $i + 2) {
            if ('Il reste' == substr(trim($listing[$i]), 0, 8)) {
                --$i;
                continue;
            }
            if ('D\'après les informations saisies ci-dessus, un dépôt effectué maintenant pourrait être jugé hors délai par la personne publique.' == trim($listing[$i])) {
                --$i;
                continue;
            }
            // Gestion des champs vides
            if ('Description' == trim($listing[$i]) && ('Lieu d\'exécution' == trim($listing[$i + 1]) || 'Date d\'ouverture de la salle' == trim($listing[$i + 1]))
                ||
                'Date d\'ouverture de la salle' == trim($listing[$i]) && 'Date limite de dépôt des plis' == trim($listing[$i + 1])
               ) {
                $infos[trim($listing[$i])] = '';
                if ('Date d\'ouverture de la salle' == $listing[$i]) {
                    $infos[trim($listing[$i])] = strftime('%d %B %Y - %Hh%M');
                }
                --$i; // Pour repartir au bon endroit dans le for (on a comblé un trou)
            } else {
                $infos[trim($listing[$i])] = trim($listing[$i + 1]);
            }
        }

        foreach ($infos as $k => $v) {
            $k = preg_replace('/ ?:$/', '', $k);
            $infos_tmp[$k] = $v;
        }
        $infos = $infos_tmp;

        // on récupère les lots de la consultation
        preg_match_all(self::$regexDetailLots, $html, $lotsBlocs);
        foreach ($lotsBlocs[1] as $k => $v) {
            $lots[preg_replace("/\s{2,}/", '', $v)] = trim($lotsBlocs[2][$k]);
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        // Mise à jour de la Description de la consultation ainsi que de la date de mise en ligne
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ORGANISME, self::$acronyme);
        $c->add(CommonConsultationPeer::URL_CONSULTATION_ACHAT_PUBLIQUE, $urlDetail);
        $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);
        if ($commonConsultation instanceof CommonConsultation) {
            // Description de la consultation
            $commonConsultation->setObjet($infos['Description']);
            // Date de mise en ligne (date de début)
            $d = strptime(str_replace(' (heure de Paris)', '', $infos['Date d\'ouverture de la salle']), '%d %B %Y - %Hh%M');
            $d['tm_year'] += 1900;
            ++$d['tm_mon'];
            $date_debut = "{$d['tm_year']}-{$d['tm_mon']}-{$d['tm_mday']} {$d['tm_hour']}:{$d['tm_min']}:00";
            if ('0000' != $d['tm_year']) {
                $commonConsultation->setDatedebut($date_debut);
                $commonConsultation->setDatemiseenligne($date_debut);
                $commonConsultation->setDatemiseenlignecalcule($date_debut);
                //$commonConsultation->setDatevalidation(date("Y-m-d H:i:s"));
            }
        } else {
            echo "consultation non trouvee. Etes vous sur de l'ID ?";
            exit(1);
        }

        // Mise à jour des lieux d'exécutions
        // TODO gérer pr les régions et Pays
        $lieuExecution = ',';
        $arrayLieuExecution = preg_split('\|', $infos['Lieu d\'exÃ©cution']);
        foreach ($arrayLieuExecution as $eltLieuExecution) {
            if ('' == $eltLieuExecution) {
                continue;
            }
            $c = new Criteria();
            $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, '(%) '.str_replace(' ', '-', trim($eltLieuExecution)), Criteria::LIKE);
            $geoLoc = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
            if ($geoLoc instanceof CommonGeolocalisationN2 && $commonConsultation instanceof CommonConsultation) {
                $lieuExecution .= $geoLoc->getId().',';
            } else {
                file_put_contents('/tmp/lieuExecutionInconnu', "$eltLieuExecution\n", FILE_APPEND);
            }
        }
        if (',' != $lieuExecution) {
            $commonConsultation->setLieuExecution($lieuExecution);
        }

        // Gestion des lots
        if ((is_countable($lots) ? count($lots) : 0) > 0 && $commonConsultation instanceof CommonConsultation) {
            $commonConsultation->setAlloti('1');
            foreach ($lots as $number => $description) {
                $categoryLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(self::$acronyme, $commonConsultation->getId(), $number);
                //$categoryLot = CommonCategorieLotPeer::retrieveByPK(self::$acronyme, $commonConsultation->getId(), $number, $connexionCom);
                if (!($categoryLot instanceof CommonCategorieLot)) {
                    $categoryLot = new CommonCategorieLot();
                }
                $categoryLot->setOrganisme(self::$acronyme);
                $categoryLot->setConsultationId($commonConsultation->getId());
                $categoryLot->setLot($number);
                $categoryLot->setDescription($description);
                $categoryLot->save($connexionCom);
            }
        }

        preg_match('#/sdm/ent/pub/affichageAvis.do\?docs=[0-9]+#', $html, $avisPublicite);
        $avis = self::httpGet('https://www.achatpublic.com'.$avisPublicite[0], null, $uniqid);

        preg_match('#Nom, adresses et point\(s\) de contact&nbsp;: </(b|B)>.*([0-9]{5})&nbsp;([^,<]+).*\xA0</div>#', $avis, $adresse);
        if (isset($adresse[1]) && isset($adresse[2])) {
            $code_postal = $adresse[1];
            $ville = $adresse[2];
            echo "CP: $code_postal - V: $ville";
            if (!str_contains($commonConsultation->getOrgDenomination(), $code_postal)) {
                $commonConsultation->setOrgDenomination($commonConsultation->getOrgDenomination()." ($code_postal - $ville)");
                $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($commonConsultation->getOrgDenomination())));
            }
        }
        preg_match('#Correspondant&nbsp;: </(b|B)>.*([0-9]{5}) ([^,<]+).*\xA0?</div>#', $avis, $adresse);
        if (isset($adresse[1]) && isset($adresse[2])) {
            $code_postal = $adresse[1];
            $ville = $adresse[2];
            echo "CP: $code_postal - V: $ville";
            if (!str_contains($commonConsultation->getOrgDenomination(), $code_postal)) {
                $commonConsultation->setOrgDenomination($commonConsultation->getOrgDenomination()." ($code_postal - $ville)");
                $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($commonConsultation->getOrgDenomination())));
            }
        }
        preg_match('#Identification de l\'organisme qui passe le march.*>.*([0-9]{5}) ([^,<]+).*#', $avis, $adresse);
        echo $avis;
        if (isset($adresse[1]) && isset($adresse[2])) {
            $code_postal = $adresse[1];
            $ville = $adresse[2];
            echo "CP: $code_postal - V: $ville";
            if (!str_contains($commonConsultation->getOrgDenomination(), $code_postal)) {
                $commonConsultation->setOrgDenomination($commonConsultation->getOrgDenomination()." ($code_postal - $ville)");
                $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($commonConsultation->getOrgDenomination())));
            }
        }
        preg_match('#Classification CPV \(Vocabulaire Commun pour les marchés publics\)&nbsp;: </b>([^<]*)\xA0<#Us', $avis, $cpv);
        if (isset($cpv[1])) {
            $cpv_liste = preg_split('/,/', $cpv[1]);
            $commonConsultation->setCodeCpv1(array_shift($cpv_liste));
            $commonConsultation->setCodeCpv2(implode('#', $cpv_liste));
        }
        // On enregistre les modifications sur la consulation
        $commonConsultation->save($connexionCom);
        unlink("/tmp/apc_cookie.$uniqid.tmp");
    }

    /**
     * Récupération des consultations depuis le moteur de recherche APC.
     *
     * Si $fetchMode vaut FETCH_FROM_LOCAL_CACHE, on va juste reparser le dernier fichier recupéré (achatpublic.html)
     * (utile si on veut juste réintégrer en base sans tout récupérer sur achatpublic)
     * Si $fetchMode vaut FETCH_FROM_APC, on va aller tout rechercher aur AchatPublic
     */
    protected static function fetchAllConsultations($fetchMode = self::FETCH_FROM_LOCAL_CACHE)
    {
        $allCons = [];
        $localCachePage = Atexo_Config::getParameter('COMMON_TMP').'/achatpublic.html';
        $post = 'debug=jg&objetRecherche=-1&reference=&personnepublique='.self::$organismeFiltre.'&region=&departement=&procedure=-1&marche=-1&intitule=&codeCPV=&jour='
                 .date('d').'&mois='.date('m').'&annee='.date('Y').'&precisionDate=apres&orderby=0&page=0&nbAffiche='.self::$nbResults;

        // Correspondance entre le nom complet coté AP et l'abbreviation coté MPE
        $typeProcedure = [
            "Appel d'Offres Ouvert" => 'AOO',
            'Ouverte' => 'AOO',
            "Appel d'offre ouvert (deux enveloppes)" => 'AOO',
            "Appel d'Offres Restreint" => 'AOR',
            'Concours Restreint' => 'CRES',
            'Marché négocié avec mise en concurrence et publicité' => 'MN',
            "Ouverte - Système d'acquisition dynamique" => 'SAD-MS',
            'Marché de Conception-Réalisation' => 'AUT',
            'Délégation de service public' => 'AUT',
            'Dialogue Compétitif' => 'DCO',
            "MPA 'Dialogue compétitif'" => 'DCO',
            'MPA à une enveloppe' => 'MAPA',
            'MPA' => 'MAPA',
            'MPA restreint' => 'MAPA',
            "MPA 'Ouvert'" => 'MAPA',
            'MPA Ouvert' => 'MAPA',
            "MPA 'Restreint'" => 'MAPA',
            'MPA Restreint' => 'MAPA',
            "MPA 'négocié sans mise en concurrence'" => 'MAPA',
            'MPA négocié sans mise en concurrence' => 'MAPA',
        ];

        // Si $fetchMode == 1, on va donc récupérer (via cURL) la page de résultat de recherche sur AchatPublic
        // Si $fetchMode == 0, on récupère la dernière page sauvegardée.
        if (self::FETCH_FROM_APC == $fetchMode) {
            echo "Recuperation page APC\n";
            $html = self::httpGet(self::$urlRecherche, $post);
            echo "Recuperation page APC terminée\n";
            // on sauvegarde la page pour un éventuel relancement avec $fetchMode == 0
            file_put_contents($localCachePage, $html);
        } else {
            if (!is_file($localCachePage)) {
                return false;
            }
            $html = file_get_contents($localCachePage);
        }

        // La magie opère ici, regex pour isoler un bloc de consultation pour tous les récupérer dans un tableau $consBlocs
        preg_match_all(self::$regexBlocsConsultations, $html, $consBlocs, PREG_SET_ORDER);
        // on vide un peu la ram
        unset($html);
        $i = 0;
        // on parcours tous les blocs de code HTML des consultations pour en extraire les données
        foreach ($consBlocs as $consBloc) {
            // on recupère l'id de la consultation (utilisé pour le lien direct)
            preg_match(self::$regexDetailConsultation, $consBloc[0], $detail);
            // on recupère les 6 champs dans le tableau html, on vire les espaces, on décode les entités HTML
            $allCons[++$i] = preg_split("/\s?<[^<]+>\s?/", preg_replace("/\s{4,}/", ' ', html_entity_decode($consBloc[0], ENT_QUOTES)), -1, PREG_SPLIT_NO_EMPTY);

            // on crée le lien direct APC avec l'id de la consulation récuperé plus haut pour avoir notre 7ème champ
            $allCons[$i]['url'] = self::$urlDetail.$detail[1];
        }

        echo (is_countable($allCons) ? count($allCons) : 0)." consultations\n";

        // Insertion en base de données de toutes les consultations récupérées
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        foreach ($allCons as $cons) {
            $organismeName = $cons[0];
            $reference = $cons[5];
            $typeProcedureName = $cons[7];
            $objet = $cons[1];
            $categorieName = $cons[2];
            // on reécrit la date en format SQL
            $dateCloture = preg_replace('#([0-9]+)/([0-9]+)/([0-9]+) - ([0-9]+):([0-9]+)#', '$3-$2-$1 $4:$5:00', $cons[3]);
            $lienDirectAPC = $cons['url'];
            $lien = '/redir.php?url='.base64_encode($lienDirectAPC);

            echo "Orga: $organismeName - Ref: $reference - Proc: $typeProcedureName - Objet: $objet - Categorie: $categorieName - Date Clot: $dateCloture \n";

            if (!isset($typeProcedure[$typeProcedureName])) {
                file_put_contents('/tmp/typeProcedureInconnus', "$typeProcedureName\n", FILE_APPEND);
            }

            // on vire les consultations de tests d'APC
            if ('Consultation TEST supervision' == $reference) {
                continue;
            }
            if ('test apc' == $reference) {
                continue;
            }
            if ('01/03/2013 - 12:00' == $categorieName) {
                continue;
            }

//            // on crée notre référence interne avec le nom de l'orga et la référence pour être sur de l'unicité
//            $newRef = sha1(self::$acronyme . $organismeName . $reference . $lienDirectAPC);
//            // on récupère l'ancienne consultation si elle existe, ou on la crée le cas échéant
//            $commonConsultation = CommonConsultationPeer::retrieveByPK($newRef, self::$acronyme, $connexionCom);

            $c = new Criteria();
            $c->add(CommonConsultationPeer::ORGANISME, self::$acronyme);
            $c->add(CommonConsultationPeer::URL_CONSULTATION_ACHAT_PUBLIQUE, $lienDirectAPC);
            $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);
            if (!($commonConsultation instanceof CommonConsultation)) {
                $commonConsultation = new CommonConsultation();
                $commonConsultation->setOrganisme(self::$acronyme);
                //$commonConsultation->setRefOrgPartenaire($newRef);
                $commonConsultation->setUrlConsultationAchatPublique($lienDirectAPC);
                $commonConsultation->setOrgDenomination($organismeName);
                $commonConsultation->setDateMiseEnLigneCalcule(date('Y-m-d H:i:s'));
                $commonConsultation->setObjet($objet);
            }
            $commonConsultation->setOrgDenomination($organismeName);
            $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($organismeName)));
            $commonConsultation->setReferenceUtilisateur($reference);
            $commonConsultation->setIdTypeProcedure((new Atexo_Consultation_ProcedureType())->getIdTypeProcedureByAbbreviation($typeProcedure[$typeProcedureName]));
            $commonConsultation->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $commonConsultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
            $commonConsultation->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1'));
            $commonConsultation->setServiceId('0');
            $commonConsultation->setServiceAssocieId('0');
            $commonConsultation->setAutoriserReponseElectronique('2');
            $commonConsultation->setConsultationExterne('1');
            $commonConsultation->setCategorie((new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($categorieName)->getId());
            if ('' != self::$lieuExecution) {
                $commonConsultation->setLieuExecution(self::$lieuExecution);
            }
            $commonConsultation->setDatefin($dateCloture);
            $commonConsultation->setIntitule($objet);
            $commonConsultation->setObjet($objet);
            $commonConsultation->setUrlConsultationExterne($lien);
            $commonConsultation->setUrlConsultationAchatPublique($lienDirectAPC);
            $commonConsultation->setDateFinUnix(strtotime($commonConsultation->getDatefin().' + '.$commonConsultation->getPoursuivreAffichage().' '.$commonConsultation->getPoursuivreAffichageUnite()));

            $commonConsultation->save($connexionCom);
            // Referntiel org denomonation debut
            if ($commonConsultation->getDenominationAdapte()) {
                (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomonation($commonConsultation);
            }
        }
    }
}
