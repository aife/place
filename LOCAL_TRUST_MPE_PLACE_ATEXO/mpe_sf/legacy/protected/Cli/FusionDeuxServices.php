<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTFusionnerServices;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Cron pour le path des services.
 *
 * @author salwa drissi <salwa.drissi@atexo.com>
 * @copyright Atexo 2015
 *
 * @category Atexo
 */
class Cli_FusionDeuxServices extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if ((is_countable($arguments) ? count($arguments) : 0) < 3) {
            echo "Merci d'ajouter des parametres : \r\n";
            echo "soit : \r\n";
            echo "-> id service source \r\n";
            echo "-> id service destination  \r\n";
            echo "-> Acronyme  organisme  \r\n";
            echo " ou bien : \r\n";
            echo "-> FromTable \r\n";
        } elseif (is_array($arguments) && (5 == count($arguments) || 3 == count($arguments))) {
            if (3 == count($arguments)) {
                if ('FromTable' == $arguments[2]) {
                    self::fusionServicesFromTable();
                }
            } else {
                if ($arguments[2] == $arguments[3]) {
                    echo "Le service source doit être different du service de destination \r\n";
                } else {
                    echo 'service source : '.$arguments[2]." \r\n";
                    echo 'service destination : '.$arguments[3]." \r\n";
                    echo 'Acronyme  organisme : '.$arguments[4]." \r\n";

                    echo "Pour valider vos choix taper O sinon taper N \r\n";
                    $value = trim(fgets(STDIN));
                    if ('O' == $value) {
                        self::fusionServices($arguments[2], $arguments[3], $arguments[4]);
                    } else {
                        echo "Traitement annulé \r\n";
                    }
                }
            }
        }
    }

    /**
     * permet de migrer les données d'un service X => Y au niveau du même organisme avec déplacement des archives.
     *
     * @param int    $idSource      id service source
     * @param int    $idDestination id service distination
     * @param string $org           acronyme
     */
    public static function fusionServices($idSource, $idDestination, $org, $log = '', $forcerFusionnement = false)
    {
        $service = null;
        $serviceDestination = null;
        $contratTitulaireSource = [];
        $contratMultiSource = [];
        $contratTitulaireDistinatione = [];
        $contratMultiDistinatione = [];
        $filePath = date('Y_m_d').'_fusionServices.log';

        try {
            if (($idSource || $idDestination) && $org) {
                $log .= 'service source : '.$idSource." \r\n";
                $log .= 'service destination : '.$idDestination." \r\n";
                $log .= 'Acronyme  organisme : '.$org." \r\n";
                if ($idSource) {
                    $c = new Criteria();
                    $c->add(CommonServicePeer::ID, $idSource);
                    $c->add(CommonServicePeer::ORGANISME, $org);
                    $service = CommonServicePeer::doSelectOne($c);
                }
                if ($idDestination) {
                    $c = new Criteria();
                    $c->add(CommonServicePeer::ID, $idDestination);
                    $c->add(CommonServicePeer::ORGANISME, $org);
                    $serviceDestination = CommonServicePeer::doSelectOne($c);
                }
                if ($service instanceof CommonService || $serviceDestination instanceof CommonService) {
                    $sql = "SELECT count(distinct(id)) as nbr FROM  `Agent` WHERE `service_id`='".$idSource."' and `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo "nombre d'agent au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= "nombre d'agent au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(id)) as nbr FROM  `Echange` WHERE `service_id`='".$idSource."' and `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo "nombre d'echange au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= "nombre d'echange au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(id)) as nbr FROM  `Historique_suppression_agent` WHERE `service_id`='".$idSource."' and `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo "nombre d'historique suppression agent au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= "nombre d'historique suppression agent au niveau du service  ".$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(id)) as nbr FROM  `Marche` WHERE `service_id`='".$idSource."' and `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de marche au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de marche au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(id)) as nbr FROM  `RPA`  WHERE `service_id` = '".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de RPA au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de RPA au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(id_auto)) as nbr FROM  `SuiviAcces` WHERE `service_id`='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de SuiviAcces au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de SuiviAcces au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(reference)) as nbr FROM  `consultation` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de consultation au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de consultation au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(reference)) as nbr FROM  `consultation`  WHERE service_associe_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de consultation au niveau du service service_associe_id '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de consultation au niveau du service service_associe_id '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }
                    $sql = "SELECT count(distinct(reference)) as nbr FROM  `consultation` WHERE service_validation='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de consultation au niveau du service service_validation '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de consultation au niveau du service service_validation '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    //SDR

                    $sql = "SELECT count(distinct(id)) as nbr FROM  `AcheteurPublic` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de compte boamp au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de compte boamp au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    $sql = "SELECT count(distinct(id)) as nbr FROM  `CertificatPermanent` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de CertificatPermanent au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de CertificatPermanent au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    $sql = "SELECT count(distinct(id_contrat_titulaire)) as nbr FROM  `t_contrat_titulaire` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de contrat_titulaire au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de contrat_titulaire au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    $sql = "SELECT count(distinct(id)) as nbr FROM  `t_decision_selection_entreprise` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de decision_selection_entreprise au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de decision_selection_entreprise au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    $sql = "SELECT count(distinct(id_donnees_consultation)) as nbr FROM  `t_donnees_consultation` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'nombre de donnees_consultation au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                        $log .= 'nombre de donnees_consultation au niveau du service  '.$idSource.' est de : '.$row['nbr']."\r\n";
                    }

                    $sql = "SELECT contrat_titulaire,contrat_multi,annee FROM  `t_numerotation_automatique` WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'contrat_titulaire et contrat_multi du service source '.$idSource.'  : '.$row['contrat_titulaire'].'  --  '.$row['contrat_multi']." pour l'année : ".$row['annee']."\r\n";
                        $log .= 'contrat_titulaire et contrat_multi du service source '.$idSource.'  : '.$row['contrat_titulaire'].'  --  '.$row['contrat_multi']." pour l'année : ".$row['annee']."\r\n";
                        $contratTitulaireSource[$row['annee']] = $row['contrat_titulaire'];
                        $contratMultiSource[$row['annee']] = $row['contrat_multi'];
                    }

                    $updateNumerotation = false;
                    $sql = "SELECT contrat_titulaire,contrat_multi, annee FROM  `t_numerotation_automatique` WHERE service_id='".$idDestination."' AND `organisme`='".$org."'";
                    $statement = Atexo_Db::getLinkCommon(false)->query($sql);
                    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                        echo 'contrat_titulaire et contrat_multi du service  distination '.$idDestination.'  : '.$row['contrat_titulaire'].'  --  '.$row['contrat_multi']." pour l'année : ".$row['annee']."\r\n";
                        $log .= 'contrat_titulaire et contrat_multi du service distination '.$idDestination.'  : '.$row['contrat_titulaire'].'  --  '.$row['contrat_multi']." pour l'année : ".$row['annee']."\r\n";
                        $contratTitulaireDistinatione[$row['annee']] = $row['contrat_titulaire'];
                        $contratMultiDistinatione[$row['annee']] = $row['contrat_multi'];
                    }
                    $arrayInsert = [];
                    $arrayUpdate = [];
                    foreach ($contratTitulaireSource as $annee => $num) {
                        if (empty($contratTitulaireDistinatione[$annee]) && empty($contratMultiDistinatione[$annee])) {
                            $arrayInsert[$annee]['contrat_titulaire'] = $num;
                        } elseif ($num > $contratTitulaireDistinatione[$annee]) {
                            $arrayUpdate[$annee]['contrat_titulaire'] = $num;
                        }
                    }

                    foreach ($contratMultiSource as $annee => $num) {
                        if (empty($contratTitulaireDistinatione[$annee]) && empty($contratMultiDistinatione[$annee])) {
                            $arrayInsert[$annee]['contrat_multi'] = $num;
                        } elseif ($num > $contratMultiDistinatione[$annee]) {
                            $arrayUpdate[$annee]['contrat_multi'] = $num;
                        }
                    }

                    if (!$forcerFusionnement) {
                        echo "Pour valider le traitement taper O sinon taper N \r\n";
                        $value = trim(fgets(STDIN));
                    } else {
                        $value = 'O';
                    }

                    if ('O' == $value) {
                        $log .= "Début \r\n";
                        echo "Début \r\n";
                        echo ' Agent =>';
                        $log .= ' Agent =>';
                        $sqlUpdate = "UPDATE `Agent` SET `service_id`='".$idDestination."' WHERE `service_id`='".$idSource."' and `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' Echange =>';
                        $log .= ' Echange =>';
                        $sqlUpdate = " UPDATE `Echange` SET `service_id`='".$idDestination."' WHERE `service_id`='".$idSource."' and `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' Historique_suppression_agent =>';
                        $log .= ' Historique_suppression_agent =>';
                        $sqlUpdate = " UPDATE `Historique_suppression_agent` SET `service_id` ='".$idDestination."' WHERE `service_id`='".$idSource."' and `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' Marche =>';
                        $log .= ' Marche =>';
                        $sqlUpdate = " UPDATE `Marche` SET `service_id` ='".$idDestination."' WHERE `service_id`='".$idSource."' and `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' RPA =>';
                        $log .= ' RPA =>';
                        $sqlUpdate = " UPDATE `RPA` SET `service_id`='".$idDestination."' WHERE `service_id` = '".$idSource."' AND `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }
                        echo ' SuiviAcces =>';
                        $log .= ' SuiviAcces =>';
                        $sqlUpdate = " UPDATE `SuiviAcces` SET `service_id` ='".$idDestination."' WHERE `service_id`='".$idSource."' AND `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' consultation service_id =>';
                        $log .= ' consultation service_id =>';
                        $sqlUpdate = " UPDATE `consultation` SET service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' consultation service_associe_id =>';
                        $log .= ' consultation service_associe_id =>';
                        $sqlUpdate = " UPDATE `consultation` SET service_associe_id='".$idDestination."' WHERE service_associe_id='".$idSource."' AND `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' consultation service_validation => ';
                        $log .= ' consultation service_validation =>';
                        $sqlUpdate = "  UPDATE `consultation` SET service_validation='".$idDestination."' WHERE service_validation='".$idSource."' AND `organisme`='".$org."';";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' AcheteurPublic => ';
                        $log .= ' AcheteurPublic =>';
                        $sqlUpdate = "UPDATE `AcheteurPublic` set service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' CertificatPermanent => ';
                        $log .= ' CertificatPermanent =>';
                        $sqlUpdate = "UPDATE `CertificatPermanent` set service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' t_contrat_titulaire => ';
                        $log .= ' t_contrat_titulaire =>';
                        $sqlUpdate = "UPDATE `t_contrat_titulaire` set service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' t_decision_selection_entreprise => ';
                        $log .= ' t_decision_selection_entreprise =>';
                        $sqlUpdate = "UPDATE `t_decision_selection_entreprise` set service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }

                        echo ' t_donnees_consultation => ';
                        $log .= ' t_donnees_consultation =>';
                        $sqlUpdate = "UPDATE `t_donnees_consultation` set service_id='".$idDestination."' WHERE service_id='".$idSource."' AND `organisme`='".$org."'";
                        try {
                            Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                            echo "OK \r\n";
                            $log .= "OK \r\n";
                        } catch (Exception $e) {
                            $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                            echo "NOK \r\n";
                        }
                        echo ' t_numerotation_automatique => ';
                        $log .= ' t_numerotation_automatique =>';

                        if (count($arrayInsert)) {
                            foreach ($arrayInsert as $annee => $arrayInfo) {
                                $sqlInsert = " INSERT t_numerotation_automatique (id,organisme,service_id,annee,contrat_titulaire,contrat_multi)VALUES(NULL,'".$org."','".$idDestination."','".$annee."','".$arrayInfo['contrat_titulaire']."','".$arrayInfo['contrat_multi']."')";
                                echo $sqlInsert."\r\n";
                                try {
                                    Atexo_Db::getLinkCommon(false)->query($sqlInsert);
                                    echo "OK \r\n";
                                    $log .= "OK \r\n";
                                } catch (Exception $e) {
                                    $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                                    echo "NOK \r\n";
                                }
                            }
                        }
                        if (count($arrayUpdate)) {
                            foreach ($arrayUpdate as $annee => $arrayInfo) {
                                $sqlUpdate = "UPDATE `t_numerotation_automatique` set contrat_titulaire='".$arrayInfo['contrat_titulaire']."', contrat_multi ='".$arrayInfo['contrat_multi']."' WHERE service_id='".$idDestination."' AND `organisme`='".$org."' AND annee='".$annee."'";
                                echo $sqlUpdate."\r\n";
                                try {
                                    Atexo_Db::getLinkCommon(false)->query($sqlUpdate);
                                    echo "OK \r\n";
                                    $log .= "OK \r\n";
                                } catch (Exception $e) {
                                    $log .= ' Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
                                    echo "NOK \r\n";
                                }
                            }
                        }

                        echo " PAS de deplacement des archives :\r\n ";
                        $log .= " PAS de deplacement des archives :\r\n ";
                    }
                }
            }
        } catch (Exception $e) {
            $log .= 'Erreur  '.$e->getMessage()."\r\n".$e->getTraceAsString()."\r\n";
            echo 'erreur '.$e->getMessage()."\r\n";
        }
        echo "Fin \r\n";
        $log .= "Fin \r\n";
        Atexo_Util::writeFile(Atexo_Config::getParameter('LOG_DIR').'/'.$filePath, $log, 'a');
    }

    public static function fusionServicesFromTable()
    {
        $filePath = date('Y_m_d').'_fusionServices.log';

        echo "Fusionner des service from la table t_fusionner_services \r\n";
        $log = "Fusionner des service from la table t_fusionner_services \r\n";
        Atexo_Util::writeFile(Atexo_Config::getParameter('LOG_DIR') . '/' . $filePath, $log, 'a');

        $listeServiceToFusionne = (new Atexo_EntityPurchase())->retrievefusionServiceNotMerged();

        if (is_array($listeServiceToFusionne) && count($listeServiceToFusionne)) {
            foreach ($listeServiceToFusionne as $serviceToFusionne) {
                if ($serviceToFusionne instanceof CommonTFusionnerServices) {
                    echo "l'id de serviceToFusionne " . $serviceToFusionne->getId() . " \r\n";
                    $log = "l'id de serviceToFusionne " . $serviceToFusionne->getId() . " \r\n";
                    Atexo_Util::writeFile(Atexo_Config::getParameter('LOG_DIR') . '/' . $filePath, $log, 'a');

                    self::fusionServices(
                        $serviceToFusionne->getIdServiceSource(),
                        $serviceToFusionne->getIdServiceCible(),
                        $serviceToFusionne->getOrganisme(),
                        '',
                        true
                    );

                    /**
                     * Cela permet d'ouvrir une connexion (donc un begin) qu'à la fin de l'ensemble des transactions
                     * (déplacement de fichier, update, etc..)
                     * car sinon la connexion reste trop longtemps ouverte le temps du traitement.
                     */
                    $connexion = Propel::getConnection(
                        Atexo_Config::getParameter('COMMON_DB')
                        . Atexo_Config::getParameter('CONST_READ_WRITE')
                    );

                    $serviceToFusionne->setDateFusion(date('Y-m-d H:i:s'));
                    $serviceToFusionne->setDonneesFusionnees('1');
                    $serviceToFusionne->save($connexion);
                }
            }
        } else {
            echo "Il n'y a pas des service à fusionner dans la table t_fusionner_services\r\n";
            $log = "Il n'y a pas des service à fusionner dans la table t_fusionner_services\r\n";
            $log .= "Fin \r\n";
            Atexo_Util::writeFile(Atexo_Config::getParameter('LOG_DIR') . '/' . $filePath, $log, 'a');
        }
    }
}
