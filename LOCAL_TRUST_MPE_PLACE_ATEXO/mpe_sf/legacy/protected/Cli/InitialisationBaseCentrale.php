<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use PDO;
use ZipArchive;

/**
 * Created by PhpStorm.
 * User: Hassouni
 * Date: 08/03/2017
 * Time: 10:32.
 */
class Cli_InitialisationBaseCentrale extends Atexo_Cli
{
    public static function run()
    {
        echo "Début Récupération \n";
        $FilePath = Atexo_Config::getParameter('BASE_ROOT_DIR').'initialisationBaseCentrale/';
        if (!is_dir($FilePath)) {
            Atexo_Util::createDirectory($FilePath);
        }

        echo "  Récupération entreprise \n";
        self::recuperationEntreprise($FilePath);

        echo "  Récupération etablissement \n";
        self::recuperationEtablissement($FilePath);

        echo "  Récupération des documents \n";
        $commonTDocumentTypeQuery = new CommonTDocumentTypeQuery();
        $typesDocuments = $commonTDocumentTypeQuery->recupererTypesDocuments();

        foreach ($typesDocuments as $typeDocument) {
            self::recuperationAttestation($FilePath, $typeDocument);
        }

        echo "  Création du Zip \n";
        self::createZip($FilePath, $FilePath);

        echo "Fin script \n";
    }

    /**
     * récupération des entreprises synchronisé sur MPE , dans un fichier CSV dont le format est : SIREN;JSON;DATE_CREATION.
     *
     * @param chemin dépot de fichier csv en sortie
     */
    private static function recuperationEntreprise($FilePath)
    {
        $nbrLigne = 0;
        $csv = "SIREN;JSON;DATE_CREATION \r\n";

        $querySynchro = self::sqlHistoriqueSynchro('ENTREPRISE');

        $statement = Atexo_Db::getLinkCommon()->query($querySynchro);
        $resultatSynchro = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultatSynchro as $synchro) {
            $siren = $synchro['code'];
            $json = $synchro['jeton'];
            $dateCreation = $synchro['date_creation'];

            $ligneCsv = "$siren;$json;$dateCreation \r\n";
            ++$nbrLigne ;
            $csv .= $ligneCsv;
        }

        $csvPath = $FilePath.'csvEntrepriseBaseCentrale.csv';
        if (!$handle = fopen($csvPath, 'w')) {
            echo "Impossible d'ouvrir le fichier (".$csvPath.") \n";

            return;
        }
        if (false === fwrite($handle, $csv)) {
            echo "Impossible d'écrire dans le fichier  (".$csvPath.") \n";

            return;
        }

        echo "      $nbrLigne entreprises récuépré \n";
    }

    /**
     * récupération des établissements synchronisé sur MPE , dans un fichier CSV dont le format est : SIREN;NIC;JSON;DATE_CREATION.
     *
     * @param chemin dépot de fichier csv en sortie
     */
    private static function recuperationEtablissement($FilePath)
    {
        $nbrLigne = 0;
        $csv = "SIREN;NIC;JSON;DATE_CREATION \r\n";

        $querySynchro = self::sqlHistoriqueSynchro('ETABLISSEMENT');
        $statement = Atexo_Db::getLinkCommon()->query($querySynchro);
        $resultatSynchro = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultatSynchro as $synchro) {
            $siren = substr($synchro['code'], 0, 9);
            $nic = substr($synchro['code'], 9, 5);
            $json = $synchro['jeton'];
            $dateCreation = $synchro['date_creation'];

            $ligneCsv = "$siren;$nic;$json;$dateCreation \r\n";
            ++$nbrLigne ;
            $csv .= $ligneCsv;
        }

        $csvPath = $FilePath.'csvEtablissementBaseCentrale.csv';
        if (!$handle = fopen($csvPath, 'w')) {
            echo "Impossible d'ouvrir le fichier (".$csvPath.") \n";

            return;
        }
        if (false === fwrite($handle, $csv)) {
            echo "Impossible d'écrire dans le fichier  (".$csvPath.") \n";

            return;
        }

        echo "      $nbrLigne etablissements récuépré \n";
    }

    /**
     * requetes de récupération de l'historique de synchronisation Entrprise/Etablissement.
     *
     * @param string $type_objet valeurs possible : ("ENTREPRISE","ETABLISSEMENT")
     *
     * @return string
     */
    private static function sqlHistoriqueSynchro($type_objet)
    {
        $sql = " SELECT DISTINCT `code`,  id_synchro , `jeton`, `date_creation` FROM
                (
                    ( (SELECT `code`, MAX(`id_historique`) id_synchro , `jeton`,`date_creation`  FROM `t_historique_synchronisation_SGMAP` WHERE `type_objet` ='".$type_objet."' Group by `id_objet`)
                    UNION
                    (SELECT DISTINCT `code` , MAX(`id_historique`) id_synchro, `jeton` , `date_creation`  FROM `t_historique_synchronisation_SGMAP` WHERE `id_objet` is null AND `type_objet` ='".$type_objet."' group by `code` )
                 ) synchro ) GROUP BY `code` ";

        return $sql;
    }

    /**
     * récupération des documents par type dans un fichier csv de format : SIREN;NIC;JSON;TYPE_DATA;NOM_DOCUMENT;DATE_CREATION;HASH;TAILLE_DOCUMENT;EXTENSION
     * et le document est déposé dans un repertoire portant le nom du type document.
     *
     * @param $FilePath
     * @param CommonTDocumentType $typeDocument
     */
    private static function recuperationAttestation($FilePath, $typeDocument)
    {
        $typeData = self::correspondanceTypeDocumentMpeVersLtApi($typeDocument->getIdTypeDocument());
        $folderAttestation = $FilePath.$typeData.'/';
        if (!is_dir($folderAttestation)) {
            Atexo_Util::createDirectory($folderAttestation);
        }

        echo '      Récupération '.$typeDocument->getCode()." \n";

        $nbrLigne = 0;
        $nbrfile = 0;
        $csv = "SIREN;NIC;JSON;TYPE_DATA;NOM_DOCUMENT;DATE_CREATION;HASH;TAILLE_DOCUMENT;EXTENSION \r\n";

        $querySynchro = self::sqlDocumentEntreprise($typeDocument->getIdTypeDocument());
        $statement = Atexo_Db::getLinkCommon()->query($querySynchro);
        $resultatSynchroDocuement = $statement->fetchAll(PDO::FETCH_ASSOC);
        foreach ($resultatSynchroDocuement as $synchro) {
            $idEntreprise = $synchro['id_entreprise'];
            $idEtablissement = $synchro['id_etablissement'];
            $nomDocument = $synchro['nom_document'];
            $json = json_encode(['url' => 'https://apientreprise.fr/attestations/'.utf8_encode($nomDocument)], JSON_THROW_ON_ERROR);

            $dateCreation = $synchro['date_recuperation'];
            $hash = $synchro['hash'];
            $tailleDocument = $synchro['taille_document'];
            $extensionDocument = $synchro['extension_document'];

            if ($typeDocument->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, null, false);
                if ($entreprise instanceof Entreprise) {
                    $siren = $entreprise->getSiren();
                    $nic = null;
                }
            } elseif ($typeDocument->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                $etablissement = (new CommonTEtablissementQuery())->getEtabissementbyId($idEtablissement);
                if ($etablissement instanceof CommonTEtablissement) {
                    $siren = $etablissement->getEntreprise()->getSiren();
                    $nic = $etablissement->getCodeEtablissement();
                }
            }

            $ligneCsv = "$siren;$nic;$json;$typeData;$nomDocument;$dateCreation;$hash;$tailleDocument;$extensionDocument \r\n";
            ++$nbrLigne ;
            $csv .= $ligneCsv;

            $atexoBlob = new Atexo_Blob();
            $content = $atexoBlob->return_blob_to_client($synchro['id_blob'], Atexo_Config::getParameter('COMMON_BLOB'));
            if ($content) {
                $fileAttestation = $folderAttestation.''.utf8_encode($nomDocument).'.'.$extensionDocument;
                if (!$handle = fopen($fileAttestation, 'w')) {
                    echo "Impossible d'ouvrir le fichier (".$fileAttestation.") \n";
                }
                if (false === fwrite($handle, $content)) {
                    echo "Impossible d'écrire dans le fichier  (".$fileAttestation.")  \n";
                }
                ++$nbrfile;
            }
        }

        $nomCsv = 'csv'.$typeDocument->getCode().'BaseCentrale.csv';
        $csvPath = $FilePath.$nomCsv;
        if (!$handle = fopen($csvPath, 'w')) {
            echo "Impossible d'ouvrir le fichier (".$csvPath.')';

            return;
        }
        if (false === fwrite($handle, $csv)) {
            echo "Impossible d'écrire dans le fichier  (".$csvPath.')';

            return;
        }

        echo "          $nbrLigne attestations récuéprées \n";
        echo "          $nbrfile fichiers trouvés \n";
    }

    /**
     * requetes de récuperation de l'historique de synchro de document.
     *
     * @param $type_objet
     *
     * @return string
     */
    private static function sqlDocumentEntreprise($type_objet)
    {
        $sql = "SELECT id_entreprise , id_etablissement , nom_document , id_type_document , date_recuperation , hash , taille_document , extension_document , id_blob
                FROM `t_document_entreprise` document
                JOIN`t_entreprise_document_version` versionDocument ON document.`id_derniere_version` = versionDocument.`id_version_document`
                WHERE `id_type_document` = '".$type_objet."' AND `id_etablissement` =0 ";

        return $sql;
    }

    /**
     * correspondance type document MPE VS LT_API.
     *
     * @param $idTypeDucomentMpe
     *
     * @return int
     */
    private static function correspondanceTypeDocumentMpeVersLtApi($idTypeDucomentMpe)
    {
        return match ($idTypeDucomentMpe) {
            Atexo_Config::getParameter('TYPE_ATTESTATION_FISCALE') => 4,
            Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_MARCHE_PUBLIC') => 5,
            Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_VIGILANCE') => 6,
            4 => 8,
            5 => 13,
            6 => 9,
            7 => 10,
            default => '',
        };
    }

    /**
     * Permet la création d'une archive Zip à partir d'un dossier.
     *
     * @param string $dirSource
     * @param string $dirDest
     * @param string $racine
     * @param int    $reload
     * @param object $oZip
     *
     * @return dossier Zipé
     */
    private static function createZip($dirSource, $dirDest, $racine = '', $reload = null, $oZip = null)
    {
        // si le dossier existe
        if ($dir = opendir($dirSource)) {
            //si la fonction est lancé pour la première fois on créait l'objet
            if (!$reload) {
                // on créait le chemin du dossier final
                $pathZip = substr($dirDest, 0, -1).'.zip';
                $oZip = new ZipArchive();
                $oZip->open($pathZip, ZipArchive::CREATE);
            }// sinon on récupère l'object passé en param
            else {
                $oZip = $oZip;
            }

            while (($file = readdir($dir)) !== false) {
                // on évite le dossier parent et courant
                if ('..' != $file && '.' != $file) {
                    // Si c'est un dossier on relance la fonction
                    if (is_dir($dirSource.$file)) {
                        self::createZip($dirSource.$file.'/', '', $file.'/', 1, $oZip);
                    }// sinon c'est un fichier donc on l'ajoute à l'archive
                    else {
                        $oZip->addFile($dirSource.$file, iconv('UTF-8', 'IBM850', $racine.$file));
                    }
                }
            }
            // on ferme l'archive
            if (!$reload) {
                echo "      Zip crée à l'emplacement :".$pathZip." \n";

                return $oZip->close();
            }
        }
    }
}
