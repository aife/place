<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use AtexoCrypto\Dto\Consultation;
use Exception;

/**
 * Mise à jour des donnes consultation.
 *
 * @category Atexo
 */
class Cli_RedressementDonneesConsultationContrat extends Atexo_Cli
{
    public static function run()
    {
        $connexion = null;
        $start = 'début de lancement du script de redressement des donnees consultation '.date('Y-m-d H:i:s');
        $logger = Atexo_LoggerManager::getLogger('cli');
        $logger->info($start);
        echo $start."\n";
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            $logger->info('La recuperation des contrats ');
            $contrats = (new Atexo_Consultation_Contrat())->getContratsSansDonneesConsultation(0, $connexion);
            if (is_array($contrats) && count($contrats)) {
                foreach ($contrats as $oneContrat) {
                    if ($oneContrat instanceof CommonTContratTitulaire) {
                        $consultationId = $oneContrat->getRefConsultation();
                        $organisme = $oneContrat->getOrganisme();
                        $service = $oneContrat->getServiceId();
                        $logger->info('La recuperation de la consultation dont  reference = '.$consultationId.' organisme = '.$organisme);
                        $consultation = (new Atexo_Consultation())->retrieveCommonConsultation($consultationId, $organisme);
                        if ($consultation instanceof CommonConsultation) {
                            $logger->info('Consultation existe');
                            $donneesConsultation = new CommonTDonneesConsultation();
                            (new Atexo_Consultation_Contrat())->fillDonneesConsultation($donneesConsultation, $consultation);
                            $donneesConsultation->setIdContratTitulaire($oneContrat->getIdContratTitulaire());
                            $logger->info("l'enregistrement des donnees consultation");
                            $donneesConsultation->save($connexion);
                        } else {
                            $logger->info("Consultation n'existe pas");
                        }
                    }
                }
            }
            $connexion->commit();
        } catch (Exception $e) {
            echo 'Erreur => '.$e->getMessage();
            $connexion->rollBack();
            $logger->error('Erreur => '.$e->getMessage()." \n ".$e->getTraceAsString());
        }

        $fin = ' Fin de lancement du script de redressement des donnees consultation  '.date('Y-m-d H:i:s');
        echo $fin."\n";
        $logger->info($fin);
        self::Display('Done');
    }
}
