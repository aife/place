<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonArchiveArcade;
use Application\Propel\Mpe\CommonArchiveArcadeQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_SendFiles;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;

/**
 * envoie le fichier d'archive des consultations vers le ministère.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_SendArchiveArcade extends Atexo_Cli
{
    private static ?int $codeRetour = null;
    private static ?int $nbFlux = null;

    public static function run()
    {
        $params = [];
        self::$codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
        self::$nbFlux = 0;
        $logger = self::getLogger();

        $params['debutExecution'] = new DateTime('now');
        $params['service'] = 'Arcade';
        $params['nomBatch'] = 'Cli_SendArchiveArcade';
        self::oversight($params);

        $archiveListToSend = self::retrieveArchiveArcadeToSend();
        $logger->info('Il y a '.count($archiveListToSend).' archive_arcade à envoyer');

            Propel::close();


            if(count($archiveListToSend)>0) {
	    		foreach($archiveListToSend as $item) {
                    self::$codeRetour = Atexo_Oversight_Oversight::$codeSuccess;
	    			$fileToSend = $item->getCheminFichier();
	    			if(is_file($fileToSend) && filesize($fileToSend)>0) {
                        $params['poids'] = filesize($fileToSend);
                        $url=Atexo_Config::getParameter('URL_RECEPTION_ARCADE');

                    //par défaut authentification forte
                    $sslCert = Atexo_Config::getParameter('ARCADE_SSLCERT');
                    $sslCertPwd = Atexo_Config::getParameter('ARCADE_SSLCERTPASSWD');
                    $authentificationForte = true;
                    $dateDebutEnvoi = new DateTime('now');
                    $erreurEnvoiFlux = [];

                    if (Atexo_SendFiles::sendFile($fileToSend, $url, $authentificationForte, $sslCert, $sslCertPwd, $logger, $erreurEnvoiFlux)) {
                        $logger->info('le fichier '.$fileToSend.' a bien ete envoye à Arcade');
                        $dateFinEnvoi = new DateTime('now');
                        $item->setDateEnvoiDebut($dateDebutEnvoi);
                        $item->setDateEnvoiFin($dateFinEnvoi);
                        $item->setStatusTransmission(1);
                        $item->save(self::getConnexion());

                        system('mv '.escapeshellarg($fileToSend).' '.escapeshellarg(Atexo_Config::getParameter('COMMON_TMP')));
                        $logger->info('le fichier '.$fileToSend.' a été déplacé dans '.Atexo_Config::getParameter('COMMON_TMP'));

                        $params['resultat'] = 'le fichier a bien ete envoye + fichier bien reçu';
                        $params['nbFlux'] = 1;
                    } else {
                        $logger->error("le fichier n'a pas pu être  envoyé");
                        $dateFinEnvoi = new DateTime('now');
                        $item->setDateEnvoiDebut($dateDebutEnvoi);
                        $item->setDateEnvoiFin($dateFinEnvoi);
                        $item->setStatusTransmission(0);
                        if (null != $erreurEnvoiFlux) {
                            $item->setErreur(implode(' ', $erreurEnvoiFlux));
                        }
                        $item->save(self::getConnexion());

                        $params['resultat'] = "le fichier n'a pas été envoyé";
                        $params['nbFlux'] = 0;
                        self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorRemoteServiceReturnsAnError;
                    }
                } else {
                    $logger->error('le fichier '.$fileToSend." n'existe pas ou son poids est égal à 0");
                    $dateDebutEnvoi = new DateTime('now');
                    $dateFinEnvoi = new DateTime('now');
                    $item->setDateEnvoiDebut($dateDebutEnvoi);
                    $item->setDateEnvoiFin($dateFinEnvoi);
                    $item->setStatusTransmission(0);
                    $item->setErreur('le fichier '.$fileToSend." n'existe pas ou son poids est égal à 0");

                    $item->save(self::getConnexion());

                    $params['resultat'] = 'no File in==>'.$fileToSend."\n";
                    self::$codeRetour = Atexo_Oversight_Oversight::$codeErrorSomeFilesOrFoldersAreNotFound;
                }

                    $params['code'] = self::$codeRetour;
	    			self::oversight($params);
                    Propel::close();
	    		}
	    	}

        return self::$codeRetour;
    }

    private static function getLogger()
    {
        return Atexo_LoggerManager::getLogger('archivage');
    }

    private static function getConnexion()
    {
        return Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
    }

    /**
     * Cette méthode recherche toutes les entrées en BD telles que :
     * poids_archive et chemin_fichier non null et non vides, status_transmission <> 1.
     *
     * @return array of CommonArchiveArcade liste des archives arcade non envoyées
     */
    private static function retrieveArchiveArcadeToSend()
    {
        $connexion = self::getConnexion();
        $archiveArcadeQuery = new CommonArchiveArcadeQuery();
        $archiveArcadeQuery->filterByPoidsArchive(0, Criteria::GREATER_THAN);
        $archiveArcadeQuery->filterByCheminFichier(null, Criteria::NOT_EQUAL);
        $archiveArcadeQuery->filterByStatusTransmission(0, Criteria::EQUAL);

        return $archiveArcadeQuery->find($connexion);
    }
}
