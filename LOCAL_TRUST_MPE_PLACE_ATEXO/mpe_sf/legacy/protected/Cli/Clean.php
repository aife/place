<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Languages;

/**
 * Nettoyage de tous les fichiers générés + cache. (A faire après chaque MAJ).
 *
 * @category Atexo
 */
class Cli_Clean extends Atexo_Cli
{
    const CALL_FROM_CLI = 'cli';

    protected static array $languages = ['fr', 'en', 'es', 'it', 'ar', 'du', 'su'];

    public static function run()
    {
        $mpePath = null;
        try {
            apcu_clear_cache();
            $projectDir = explode('/', realpath(__DIR__.'/../../'));
            $projet = end($projectDir);
            $rootPath = realpath(__DIR__.'/../../../');

            $mpePath = "$rootPath/$projet";

            $folders = ['/pages', '/assets', '/protected/runtime', '/protected/var/log', '/protected/var/cache', '/themes'];
            $user = posix_getpwuid(fileowner(getenv('BASE_ROOT_DIR')));
            $cfg_apache_user = $user['name'];
            if ('root' != $user['name']) {
                foreach ($folders as $folder) {
                    if (!is_dir($mpePath.$folder)) {
                        mkdir($mpePath.$folder);
                        shell_exec('chown -R '.$cfg_apache_user." {$mpePath}{$folder}");
                    }
                }
                shell_exec('chown '.$cfg_apache_user." {$mpePath}/protected");
                shell_exec('chown '.$cfg_apache_user." {$mpePath}/protected/config/messages/");
            }

            self::display('Deleting cache files...');
            system('rm -rf '.$mpePath.'/pages/*');
            system('rm -rf '.$mpePath.'/protected/var/cache/*');
            system('rm -rf '.$mpePath.'/protected/runtime/application.xml-*/sqlite.cache');
            system('rm -rf '.$mpePath.'/protected/runtime/application.xml-*/config.cache');
            system('rm -rf '.$rootPath.'/public/assets/*');

            if (extension_loaded('apc') && ini_get('apc.enable_cli')) {
                self::display('Vidage du Cache APC CLI :');
                self::display('APC CLI system'.(!apcu_clear_cache() ? ' not' : '').' cleaned');
                self::display('APC CLI user'.(!apcu_clear_cache() ? ' not' : '').' cleaned');
            }
            if (extension_loaded('Zend OPcache') && ini_get('opcache.enable_cli')) {
                self::display('Vidage du Cache OPCache CLI :');
                self::display('OPCache CLI system'.(!opcache_reset() ? ' not' : '').' cleaned');
            }

            self::display('Done ! '.$rootPath.' is clean.');

            self::display('Generating language files...');
            Atexo_Languages::setLanguageCatalogue(self::CALL_FROM_CLI);
        } catch (\Exception $exception) {
            self::displayError($exception->getMessage(), false, false);
        } catch (\Error $error) {
            self::displayError($error->getMessage(), false, false);
        }

        self::display('Start warmup.. wait few seconds..');
        system($mpePath.'/clean-themes.sh');

        self::display('Generating localized JS files...');
        Atexo_Languages::generateLocalizedJsFiles('cli');

        self::display('Warmup done !');
    }
}
