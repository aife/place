<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\Om\BaseCommonAvisPubPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPdf;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Soap\Atexo_Soap_InterfaceWsTED;
use DomDocument;
use Exception;
use Prado\Prado;

/**
 * Met à jour les status des avis de publicité des consultations avec OPOCE.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 *
 * @category Atexo
 */
class Cli_SuiviAvisPublicite extends Atexo_Cli
{
    public static function run()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexion->beginTransaction();
        //Mise a jour des statuts des destinataires
        $logger->info('Debut traitement : Mise a jour des statuts des destinataires');
        $listeDestinataires = (new Atexo_Publicite_AvisPub())->getDestinatairesValidesEcoEtNonPublieOpoce($connexion);
        if (is_array($listeDestinataires) && count($listeDestinataires)) {
            foreach ($listeDestinataires as $destinataire) {
                try {
                    if ($destinataire instanceof CommonDestinatairePub) {
                        $refDestFormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($destinataire->getId(), $destinataire->getOrganisme());
                        if ($refDestFormXml instanceof CommonFormXmlDestinataireOpoce) {
                            //Récuperation du rapport de suivi de l'avis
                            $rapportSuivi = (new Atexo_Soap_InterfaceWsTED())->getNoticeStatus($refDestFormXml->getCodeRetour());
                            $logger->info('retour du WS TED Destinataire id : '.$destinataire->getId().' organisme : '.$destinataire->getOrganisme().' retour : '.print_r($rapportSuivi, true));
                            if (is_array($rapportSuivi)) {
                                if ('false' == $rapportSuivi['erreur']) {
                                    if ('SUBMITTED' == $rapportSuivi['resultat']) {
                                        $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_INTEGRE'));
                                        //Recuperation et stockage de l'avis pdf OPOCE
                                        $avisPub = BaseCommonAvisPubPeer::retrieveByPK($destinataire->getIdAvis(), $destinataire->getOrganisme(), $connexion);
                                        if ($avisPub instanceof CommonAvisPub) {
                                            $consultation = (new Atexo_Consultation())->retrieveConsultation($avisPub->getConsultationId(), $avisPub->getOrganisme());
                                            if ($consultation instanceof CommonConsultation) {
                                                $avisPortailPdfDejaGenere = (new Atexo_Publicite_Avis())->retreiveListAvis($avisPub->getConsultationId(), $avisPub->getOrganisme(), Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED'), $avisPub->getTypeAvis());
                                                if (!$avisPortailPdfDejaGenere) {
                                                    $arrayPdfContent = (new Atexo_Soap_InterfaceWsTED())->generatePDFTED(base64_decode($refDestFormXml->getXml()), $refDestFormXml->getCodeRetour());
                                                    if ('false' == $arrayPdfContent['erreur']) {
                                                        $pdfContent = $arrayPdfContent['resultat']; //var_dump($pdfContent);exit;
                                                        if ($pdfContent && '' != $pdfContent && 'null' != $pdfContent) {
                                                            $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s');
                                                            system('touch '.$filePdf);
                                                            $fileNew = Prado::localize('TEXT_AVIS_TED');
                                                            $avisPortail = (new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($filePdf, $consultation, 'fr', true, $pdfContent, $fileNew, true);
                                                            if ($avisPortail instanceof CommonAVIS && $avisPortail->getStatut() != Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
                                                                $avisPortail->setDatePub(date('Y-m-d'));
                                                                $avisPortail->setTypeAvisPub($avisPub->getTypeAvis());
                                                                $avisPortail->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                                                                $avisPortail->save($connexion);
                                                                //Mise à jour de l'avis global
                                                                $avisPub->setIdAvisPdfOpoce($avisPortail->getId());
                                                                $avisPub->save($connexion);
                                                            }
                                                        } else {
                                                            $refDestFormXml->setMessageRetour('recuperation pdf : PDF TED VIDE ');
                                                            $refDestFormXml->save($connexion);
                                                        }
                                                    } else {
                                                        $refDestFormXml->setMessageRetour('recuperation pdf : '.$arrayPdfContent['libelle_erreur']);
                                                        $refDestFormXml->save($connexion);
                                                    }
                                                }
                                            }
                                        }
                                    } elseif ('PUBLISHED' == $rapportSuivi['resultat']) {
                                        $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE'));
                                    } elseif ('SUBMISSION_PENDING' == $rapportSuivi['resultat']) {
                                        $destinataire->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION'));
                                    }
                                    $destinataire->setDateModification(date('Y-m-d H:i:s'));
                                    $destinataire->save($connexion);
                                } elseif ('true' == $rapportSuivi['erreur']) {
                                    $refDestFormXml->setMessageRetour('getNoticeStatus: '.$rapportSuivi['libelle_erreur']);
                                    $refDestFormXml->save($connexion);
                                    continue;
                                }
                            } else {
                                $refDestFormXml->setMessageRetour('TECHNICAL: '.print_r($rapportSuivi, true));
                                $refDestFormXml->save($connexion);
                                continue;
                            }
                        }
                    }
                    $connexion->commit();
                } catch (Exception $e) {
                    $connexion->rollBack();
                    $log = "\n\n";
                    $log .= "\n\t_______________________".date('Y-m-d H:i:s')."_______________________\n";
                    $log .= "\n\n Suivi des avis OPOCE: mise a jour des statuts des destinataires <br/>";
                    $log .= "\n\n";
                    $log .= "\n\t___________________________________________________________\n";
                    $log .= $e->getMessage();
                    //Envoie de mail
                    $message = Atexo_Message::getGenericEmailFormat($log);
                    Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), Atexo_Config::getParameter('MAIL_TECHNIQUE_GESTION_PUB'), 'SIMAP2 : Problème au suivi', $message, '', '', false, true);
                    $logger->error(' Erreur  '.$e->getMessage().' '.$e->getTraceAsString());
                    throw $e;
                }
            }//End foreach
        }
        $logger->info('Fin traitement : Mise a jour des statuts des destinataires');
        $logger->info('Debut traitement :Récuperation des liens de publication');
        //Récuperation des liens de publication
        $listeDestinatairesPublies = (new Atexo_Publicite_AvisPub())->getDestinatairesValidesEtPubliesOpoce();
        if (is_array($listeDestinatairesPublies) && count($listeDestinatairesPublies)) {
            foreach ($listeDestinatairesPublies as $destinataire) {
                try {
                    if ($destinataire instanceof CommonDestinatairePub) {
                        $refDestFormXml = (new Atexo_Publicite_AvisPub())->getXmlDestOpoce($destinataire->getId(), $destinataire->getOrganisme());
                        if ($refDestFormXml instanceof CommonFormXmlDestinataireOpoce) {
                            //Récuperation du rapport de suivi de l'avis
                            $rapportPub = (new Atexo_Soap_InterfaceWsTED())->getNoticeReport($refDestFormXml->getCodeRetour());
                            $logger->info('retour du WS TED Destinataire id : '.$destinataire->getId().' organisme : '.$destinataire->getOrganisme().' retour : '.print_r($rapportPub, true));
                            if ('false' == $rapportPub['erreur']) {
                                $lienPub = '';
                                if ($rapportPub['resultat'] && '' != $rapportPub['resultat']) {
                                    $lienPub = '';
                                    $xml = (new Atexo_Soap_InterfaceWsTED())->getLastResponseNoticeReport($refDestFormXml->getCodeRetour());
                                    $dom = new DomDocument();
                                    //Chargement du fichier XML
                                    $dom->loadXML($xml);
                                    $noticeReportResponses = $dom->getElementsByTagName('getNoticeReportResponse');
                                    foreach ($noticeReportResponses as $noticeReportResponse) {
                                        $noticeReports = $noticeReportResponse->getElementsByTagName('noticeReport');
                                        foreach ($noticeReports as $noticeReport) {
                                            $publicationInfos = $noticeReport->getElementsByTagName('publicationInfo');
                                            foreach ($publicationInfos as $publicationInfo) {
                                                $tedLinks = $publicationInfo->getElementsByTagName('tedLinks');
                                                foreach ($tedLinks as $tedLink) {
                                                    $teds = $tedLink->getElementsByTagName('tedLink');
                                                    foreach ($teds as $ted) {
                                                        //Recuperation du lien de publication
                                                        if ($ted->getAttribute('language') == strtoupper(Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
                                                            $lienPub = $ted->nodeValue;
                                                            $refDestFormXml->setLienPublication($lienPub);
                                                        }
                                                    }
                                                }
                                                //Récupération de l'identifiant joue
                                                $idJoue = '';
                                                $noDocOjs = $publicationInfo->getElementsByTagName('noDocOjs');
                                                foreach ($noDocOjs as $noDocOj) {
                                                    $idJoue = $noDocOj->nodeValue;
                                                }
                                                //Récupération de la date de publication au joue
                                                $datePubJoue = '';
                                                $publicationDates = $publicationInfo->getElementsByTagName('publicationDate');
                                                foreach ($publicationDates as $publicationDate) {
                                                    $datePubJoue = $publicationDate->nodeValue;
                                                }
                                                if ($idJoue) {
                                                    $refDestFormXml->setIdJoue($idJoue);
                                                }
                                                if ($datePubJoue) {
                                                    $refDestFormXml->setDatePubJoue(substr($datePubJoue, 0, 10));
                                                }
                                            }
                                        }
                                    }
                                }
                                $refDestFormXml->save($connexion);
                            } elseif ('true' == $rapportPub['erreur']) {
                                $refDestFormXml->setMessageRetour($rapportPub['libelle_erreur']);
                                $refDestFormXml->save($connexion);
                            }
                        }
                    }
                } catch (Exception $e) {
                    $connexion->rollBack();
                    $logger->error(' Erreur  '.$e->getMessage().' '.$e->getTraceAsString());
                    throw $e;
                }
            }//End foreach
        }
        $logger->info('fin traitement :Récuperation des liens de publication');
    }
}
