<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use DateTime;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Mise à jour des bases de données.
 *
 * @category Atexo
 */
class Cli_UpdateDB extends Atexo_Cli
{
    public static function run($type = null, $force = false)
    {
        echo "#################################################\n";
        echo  "# L'updateDB est désactiver pour cette version. #\n";
        echo "#################################################\n";
        return;

        $debut = new DateTime();
        $sqlBaseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR').'/scripts-sql';
        if (!file_exists($sqlBaseRootDir)) {
            mkdir($sqlBaseRootDir, 0755, true);
        }

        $sqlCustomRootDir = Atexo_Config::getParameter('DOCUMENT_ROOT_DIR').'/../config/scripts-sql';
        $useCustomSql = false;
        if (file_exists($sqlCustomRootDir)) {
            $listCustomSqlFiles = glob($sqlCustomRootDir.'/*.sql');
            $useCustomSql = !empty($listCustomSqlFiles);
        }

        $sql_script_orga = $sqlBaseRootDir.'/maj_Organismes.sql';
        $sql_script_orga_md5 = $sqlBaseRootDir.'/maj_Organismes.sql.md5';

        $mpe_structure_update_md5 = $sqlBaseRootDir.'/MPE_structure_update.md5';
        $mpe_data_update_md5 = $sqlBaseRootDir.'/MPE_data_update.md5';
        $mpe_data_custom_update_md5 = $sqlBaseRootDir.'/MPE_data_custom_update.md5';

        $queriesStructureExecuteFile = $sqlBaseRootDir.'/queriesStructureUpdateExecute.bkp';
        $queriesDataExecuteFile = $sqlBaseRootDir.'/queriesDataUpdateExecute.bkp';
        $queriesCustomDataExecuteFile = $sqlBaseRootDir.'/queriesCustomDataUpdateExecute.bkp';

        // ---------------------------
        // MAJ SQL dans BDD Commune
        // ---------------------------
        $prefix = Atexo_Config::getParameter('DB_PREFIX');
        $db_name = "{$prefix}_".Atexo_Config::getParameter('COMMON_DB');
        echo "------------------------------\nMise a jour de la structure de la BDD : ".$db_name.(('structure' == $type && true === $force) ? 'FORCE' : '').PHP_EOL;
        ob_start();
        require __DIR__.'/../scripts-sql/update/MPE_structure_update.php';
        $updateStructureDbQueries = ob_get_contents();
        ob_end_clean();
        preg_match_all("/^((ALTER|CREATE|DROP|UPDATE) .*;)\s*(?=$)$/Usm", $updateStructureDbQueries, $sql_update_structure);
        if (file_exists($queriesStructureExecuteFile)) {
            $contentStructureQueriesExecuteFile = unserialize(file_get_contents($queriesStructureExecuteFile));
        }
        $queriesStructureExecute = is_array($contentStructureQueriesExecuteFile) ? $contentStructureQueriesExecuteFile : [];

        $countStructureExecuteQueries = 0;
        if (!is_file($mpe_structure_update_md5) || md5($updateStructureDbQueries) != file_get_contents($mpe_structure_update_md5) || ('structure' == $type && true === $force)) {
            foreach ($sql_update_structure[1] as $query) {
                $base64Query = base64_encode($query);
                if (empty($queriesStructureExecute[$base64Query])) {
                    try {
                        if (false != Atexo_Db::getLinkCommon()->query($query)) {
                            $queriesStructureExecute[$base64Query] = $query;
                            ++$countStructureExecuteQueries;
                            echo '.';
                        }
                    } catch (Exception $e) {
                        Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$query, TLogger::ERROR, 'UpdateDB.php');
                    }
                } else {
                    echo 'O';
                }
            }
            file_put_contents($queriesStructureExecuteFile, serialize($queriesStructureExecute));
            echo "\n";

            if (is_writable(dirname($mpe_structure_update_md5))) {
                file_put_contents($mpe_structure_update_md5, md5($updateStructureDbQueries));
            }
        } else {
            echo "Le script SQL de mise à jour de la structure n'a pas bougé depuis la dernière exécution, aucune modification\n";
        }

        echo "------------------------------\nMise a jour des données de la BDD : ".$db_name.(('data' == $type && true === $force) ? 'FORCE' : '').PHP_EOL;
        ob_start();
        include __DIR__.'/../scripts-sql/update/MPE_data_update.php';
        $updateDataDbQueries = ob_get_contents();
        ob_end_clean();
        preg_match_all("/^((UPDATE|INSERT|REPLACE|DELETE) .*;)\s*(?=$)$/Usm", $updateDataDbQueries, $sql_update_data);
        if (file_exists($queriesDataExecuteFile)) {
            $contentDataQueriesExecuteFile = unserialize(file_get_contents($queriesDataExecuteFile));
        }
        $queriesDataExecute = is_array($contentDataQueriesExecuteFile) ? $contentDataQueriesExecuteFile : [];

        $countDataExecuteQueries = 0;
        if (!is_file($mpe_data_update_md5) || md5($updateDataDbQueries) != file_get_contents($mpe_data_update_md5) || ('data' == $type && true === $force)) {
            foreach ($sql_update_data[1] as $query) {
                $base64Query = base64_encode($query);
                if (empty($queriesDataExecute[$base64Query])) {
                    try {
                        if (false != Atexo_Db::getLinkCommon()->query($query)) {
                            $queriesDataExecute[$base64Query] = $query;
                            ++$countDataExecuteQueries;
                            echo '.';
                        }
                    } catch (Exception $e) {
                        Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$query, TLogger::ERROR, 'UpdateDB.php');
                    }
                } else {
                    echo 'O';
                }
            }
            file_put_contents($queriesDataExecuteFile, serialize($queriesDataExecute));
            echo "\n";

            if (is_writable(dirname($mpe_data_update_md5))) {
                file_put_contents($mpe_data_update_md5, md5($updateDataDbQueries));
            }
        } else {
            echo "Le script SQL de mise à jour des données n'a pas bougé depuis la dernière exécution, aucune modification\n";
        }

        if ($useCustomSql) {
            echo "------------------------------\nMise a jour des données personnalisées : ".$db_name."\n";
            ob_start();
            include '../scripts-sql/update/MPE_data_custom_update.php';
            $updateCustomDataDbQueries = ob_get_contents();
            ob_end_clean();
            preg_match_all("/^((UPDATE|INSERT|REPLACE|DELETE) .*;)\s*(?=$)$/Usm", $updateCustomDataDbQueries, $sql_update_custom_data);
            if (file_exists($queriesCustomDataExecuteFile)) {
                $contentCustomDataQueriesExecuteFile = unserialize(file_get_contents($queriesCustomDataExecuteFile));
            }
            $queriesCustomDataExecute = is_array($contentCustomDataQueriesExecuteFile) ? $contentCustomDataQueriesExecuteFile : [];

            $countCustomDataExecuteQueries = 0;
            if (!is_file($mpe_data_custom_update_md5) || md5($updateCustomDataDbQueries) != file_get_contents($mpe_data_custom_update_md5)) {
                foreach ($sql_update_custom_data[1] as $query) {
                    $base64Query = base64_encode($query);
                    if (empty($queriesCustomDataExecute[$base64Query])) {
                        try {
                            if (false != Atexo_Db::getLinkCommon()->query($query)) {
                                $queriesCustomDataExecute[$base64Query] = $query;
                                ++$countCustomDataExecuteQueries;
                                echo '.';
                            }
                        } catch (Exception $e) {
                            Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$query, TLogger::ERROR, 'UpdateDB.php');
                        }
                    } else {
                        echo 'O';
                    }
                }
                file_put_contents($queriesCustomDataExecuteFile, serialize($queriesCustomDataExecute));
                echo "\n";

                if (is_writable(dirname($mpe_data_custom_update_md5))) {
                    file_put_contents($mpe_data_custom_update_md5, md5($updateCustomDataDbQueries));
                }
            } else {
                echo "Le script SQL de mise à jour personnalisé n'a pas bougé depuis la dernière exécution, aucune modification\n";
            }
        }

        echo PHP_EOL.'---------------------------'.PHP_EOL.'Mise a jour BDD organismes: '.(('organisme' == $type && true === $force) ? 'FORCE' : '').PHP_EOL;
        if (!is_file($sql_script_orga_md5) || md5_file($sql_script_orga) != file_get_contents($sql_script_orga_md5) || ('organisme' == $type && true === $force)) {
            preg_match_all("/^((UPDATE|ALTER|CREATE|INSERT|REPLACE|DROP|DELETE) .*;)\s*(?=$)$/Usm", file_get_contents($sql_script_orga), $sql_ORGA);
            $query = 'SELECT acronyme FROM Organisme';
            if ('annonces-marches' == Atexo_Config::getParameter('DB_PREFIX')) {
                $query = "SELECT acronyme FROM Organisme WHERE active = '0'";
            }
            $arrayOrg = [];
            $i = 0;
            try {
                foreach (Atexo_Db::getLinkCommon()->query($query) as $row) {
                    $organisme = $row['acronyme'];
                    if (empty($organisme)) {
                        continue;
                    }
                    ++$i;
                    $arrayOrg[] = $organisme;
                    echo "\n_________________________________________\n$i - Mise a jour de la BDD Organisme: $organisme.\n";
                    foreach ($sql_ORGA[1] as $sql) {
                        try {
                            $sql = str_replace('##ACCRONYME_ORG##', $organisme, $sql);
                            if (!Atexo_Db::getLinkCommon()->query($sql)) {
                                echo "ERROR when executing query in '$organisme' : ".substr($sql, 0, 77)."...\n";
                            } else {
                                echo '.';
                            }
                        } catch (Exception $e) {    //echo $e->getMessage();
                            Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$sql, TLogger::ERROR, 'UpdateDB.php');
                        }
                    }
                }
            } catch (Exception $e) {    //echo $e->getMessage();
                Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$sql, TLogger::ERROR, 'UpdateDB.php');
            }

            if (is_writable(dirname($sql_script_orga_md5))) {
                file_put_contents($sql_script_orga_md5, md5_file($sql_script_orga));
            }
            echo "\n";
            // insertion dans la table Organisme_Service_Metier pour chaque organisme une ligne pour mpe
            foreach ($arrayOrg as $organisme) {
                try {
                    $queryTestExist = "SELECT `organisme` FROM `Organisme_Service_Metier` WHERE `organisme` = '".$organisme."'";
                    $statement = Atexo_Db::getLinkCommon()->query($queryTestExist);
                    if (!($statement->fetch(PDO::FETCH_ASSOC))) {
                        $queryOrgServiceMetier = "INSERT INTO `Organisme_Service_Metier` (`organisme`, `id_service_metier`) VALUES('".$organisme."', 1);";
                        if (!Atexo_Db::getLinkCommon()->query($queryOrgServiceMetier)) {
                            echo "ERROR when executing query in '$organisme' : ".substr($queryOrgServiceMetier, 0, 77)."...\n";
                        } else {
                            echo "\nActivation du service MPE pour l'organisme '$organisme' ...\n";
                        }
                    }
                } catch (Exception $e) {    //echo $e->getMessage();
                    Prado::log('Erreur UpdateDB.php '.$e->getMessage().PHP_EOL.'Commande en erreur : '.$queryOrgServiceMetier, TLogger::ERROR, 'UpdateDB.php');
                }
            }
        } else {
            echo "Le script SQL n'a pas bougé depuis la dernière exécution, aucune modification\n";
        }
        Cli_CheckAndInitConfig::run();

        $fin = new DateTime();
        $interval = $fin->diff($debut);
        if ((int) $interval->format('%d') > 0) {
            $time = $interval->format('%D jour(s) %H heure(s) %I minute(s) %S seconde(s) ');
        } else {
            $time = $interval->format('%H heure(s) %I minute(s) %S seconde(s) ');
        }
        echo "\nLe temps d'exécution est de ".
            $time
            ."\n\n";
        Prado::log("Le temps d'exécution est de ".$time, TLogger::INFO, 'UpdateDB.php');
    }
}
