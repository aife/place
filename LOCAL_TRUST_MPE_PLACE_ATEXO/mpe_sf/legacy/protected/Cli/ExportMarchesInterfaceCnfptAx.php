<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Soap\Atexo_Soap_InterfaceCnfptAx;
use Exception;

/**
 * Gère l'export des marchés conclus pour le CNFPT.
 *
 * @author Oumar KONATE
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE4
 */
class Cli_ExportMarchesInterfaceCnfptAx extends Atexo_Cli
{
    public static function run()
    {
        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('>>>>>>>>>>> Debut lancement du cron <<<<<<<<<<');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            //Recupération des données
            $logger->info('Recuperation des decisions a exporter');
            $dataCons = (new Atexo_Soap_InterfaceCnfptAx())->recupererDataMarches();
            if (is_array($dataCons) && count($dataCons)) {
                $logger->info('Decision(s) trouve(s)');
                foreach ($dataCons as $cons) {
                    try {
                        $logger->info('-> Traitement de la decision');
                        //Construction du tableau des données pour la requete soap
                        $logger->info("Construction de la requete 'GFIAtexoConsultation'");
                        $tableauRequest = (new Atexo_Soap_InterfaceCnfptAx())->createDataRequest($cons);
                        $logger->info("Requete a envoyer: \n".print_r($tableauRequest, true));

                        //Transmission des données
                        if (is_array($tableauRequest)) {
                            $logger->info('Envoi de la requete via ws');
                            $resultat = (new Atexo_Soap_InterfaceCnfptAx())->envoyerDataMarches($tableauRequest);
                            $recId = (new Atexo_Soap_InterfaceCnfptAx())->isSendWsSucces($resultat);
                            if (false !== $recId) {
                                $logger->info('Requete envoyee avec SUCCES: ****** RecId = '.$recId.' ******');
                                //Modification de la table DecisionEnveloppe pour taguer le champ "envoi_interface" à "1"
                                $idContratTitulaire = (new Atexo_Soap_InterfaceCnfptAx())->getIdDecisionFromDataCons($cons);
                                //$organisme = Atexo_Soap_InterfaceCnfptAx::getOrganismeFromDataCons($cons);
                                if ($idContratTitulaire) {
                                    $logger->info("Tag du champ 'Envoi_Interface' a 1 de la table 'tContratTitulaire'");

                                    $contratTitulaire = CommonTContratTitulairePeer::retrieveByPK($idContratTitulaire, $connexion);
                                    if ($contratTitulaire instanceof CommonTContratTitulaire) {
                                        $contratTitulaire->setEnvoiInterface('1');
                                        $contratTitulaire->save($connexion);
                                        $logger->info("Table 'tContratTitulaire' mis a jour avec SUCCES");
                                    }
                                }
                            } else {
                                $erreur = "ECHEC lors de l'envoi de la requete: ".print_r($resultat, true);
                                $logger->error($erreur);
                            }
                            unset($resultat);
                        }
                        unset($tableauRequest);
                    } catch (Exception|\SoapFault $e) {
                        $logger->error($e->getMessage());
                    }
                }
            } else {
                $logger->info('Aucune decision trouvee');
            }
        } catch (Exception $e) {
            $logger->error($e->getMessage());
        }
        $logger->info('>>>>>>>>>>> Fin traitement <<<<<<<<<<');
    }
}
