<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Controller\Cli;
use Exception;

/**
 * Class Cli_GenerationFluxChorus : CRON de generation des flux CHORUS.
 *
 * @category Atexo
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Cli_RegenererFicheModificative extends Atexo_Cli
{
    /**
     * Lancement du CLI.
     */
    public static function run()
    {
        $startTime = date('Y-m-d H:i:s');
        echo "\t-> Debut de lancement de cli RegenererFicheModificative ".$startTime."\r\n";
        (new Atexo_Chorus_Util())->loggerInfos('Debut de lancement de cli RegenererFicheModificative '.$startTime);
        $arguments = $_SERVER['argv'];
        $idEchange = null;
        $organisme = null;
        if (is_array($arguments)) {
            if (count($arguments) > 3) {
                for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                    $argument = explode('=', $arguments[$i]);
                    $_GET[$argument[0]] = ('' != $argument[1]) ? $argument[1] : true;
                }
            } else {
                echo "Merci d'ajouter des params comme suit: \r\n";
                echo "\t-> idEchang=value organisme=value\r\n";
                echo "\t-> 2- ou bien \r\n";
                echo "\t-> 2- dateDebut=value dateFin=value ( les date doit etre dans le format annee-mois-jour)\r\n";
            }
        }
        try {
            $idEchange = $_GET['idEchange'];
            $organisme = $_GET['organisme'];
            $dateDebut = $_GET['dateDebut'];
            $dateFin = $_GET['dateFin'];
            if ($idEchange && $organisme) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la recuperation de l'echange dont idEchange = '".$idEchange."' organisme = ".$organisme);
                $chorusEchangeQuery = new CommonChorusEchangeQuery();
                $echangeChorus = $chorusEchangeQuery->recupererEchangeChorus($idEchange, $organisme, $connexion);
                if ($echangeChorus) {
                    (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative  l'echange dont idEchange = ".$idEchange.' organisme = '.$organisme.' existe');
                    (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation de la fiche modificative de l'idEchange = ".$idEchange.' organisme = '.$organisme);
                    $ficheModificative = (new Atexo_Chorus_Echange())->retrieveFicheModificativeByIdEchange($idEchange, $organisme, $connexion);
                    if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                        (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative  la fiche modificative existe son id_fiche_modificative = '.$ficheModificative->getIdFicheModificative());
                        (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation du contrat dont l'id = ".$echangeChorus->getIdDecision());
                        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
                        $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($echangeChorus->getIdDecision(), $connexion);
                        if ($contrat instanceof CommonTContratTitulaire) {
                            (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation le  contrat dont l'id = ".$echangeChorus->getIdDecision().' existe');
                            (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative appele à la methode generatePdfFicheModificative');
                            (new Atexo_Chorus_Echange())->generatePdfFicheModificative($ficheModificative, $contrat, $connexion);
                            (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative fin de la genration de FicheModificative');
                        } else {
                            (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation le  contrat dont l'id = ".$echangeChorus->getIdDecision()." n'existe pas ");
                        }
                    } else {
                        (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative  la fiche modificative de l'idEchange = ".$idEchange.' organisme = '.$organisme." n'existe pas");
                    }
                } else {
                    (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative  l'echange dont idEchange = ".$idEchange.' organisme = '.$organisme." n'existe pas");
                }
            } elseif ($dateDebut && $dateFin) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative les parmetres dateDebut = '.$dateDebut.' datefin = '.$dateFin);
                if (Atexo_Util::isFormatDateIso($dateDebut) && Atexo_Util::isFormatDateIso($dateFin)) {
                    (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative la récuperation de la fiche modificative dont la date de création >= '.$dateDebut.' et date création <='.$dateFin);
                    $fichesModificative = (new Atexo_Chorus_Echange())->retrieveFicheModificativeAGenerer($dateDebut, $dateFin, $connexion);
                    if (is_array($fichesModificative) && count($fichesModificative)) {
                        foreach ($fichesModificative as $oneFicheModificat) {
                            if ($oneFicheModificat instanceof CommonTChorusFicheModificative) {
                                (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation le  contrat dont l'id echnage = ".$oneFicheModificat->getIdEchange());
                                $contrat = (new Atexo_Consultation_Contrat())->recupererContratsByIdEchange($oneFicheModificat->getIdEchange());

                                if ($contrat instanceof CommonTContratTitulaire) {
                                    (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation le  contrat dont l'id echnage = ".$oneFicheModificat->getIdEchange().' existe son id=  '.$contrat->getIdContratTitulaire());
                                    (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative appele à la methode generatePdfFicheModificative');
                                    (new Atexo_Chorus_Echange())->generatePdfFicheModificative($oneFicheModificat, $contrat, $connexion);
                                    (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative fin de la genration de FicheModificative');
                                } else {
                                    (new Atexo_Chorus_Util())->loggerInfos(" cli RegenererFicheModificative la récuperation le  contrat dont l'id echnage = ".$oneFicheModificat->getIdEchange()." n'existe pas ");
                                }
                            }
                        }
                    } else {
                        (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative il n y a pas des fiches modificative dont la date de création >= '.$dateDebut.' et date création <='.$dateFin);
                    }
                } else {
                    (new Atexo_Chorus_Util())->loggerInfos(' cli RegenererFicheModificative verifier le format des dates ils doivent etre annee-mois-jour ');
                }
            }
        } catch (Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreur(' cli RegenererFicheModificative ERREUR = > '.$e->getMessage().'  '.$e->getTraceAsString());
        }

        echo 'chemin des logs => '.Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS').date('Y').'/'.date('m').'/'.date('d').'/interfacePlaceChorus.log';
        $endTime = date('Y-m-d H:i:s');
        echo "\t-> Debut de lancement de cli RegenererFicheModificative ".$endTime."\r\n";
        (new Atexo_Chorus_Util())->loggerInfos('Fin de lancement de cli RegenererFicheModificative '.$endTime);
    }
}
