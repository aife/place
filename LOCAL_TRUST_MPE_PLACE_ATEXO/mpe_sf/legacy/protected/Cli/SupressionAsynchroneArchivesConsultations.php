<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Exception;
use Prado\Util\TLogger;

/**
 * Cron de supression des telechargements.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since 4.8.0
 */
class Cli_SupressionAsynchroneArchivesConsultations extends Atexo_Cli
{
    /**
     * Execution du cron de supression des telechargements.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function run()
    {
        try {
            $fileLog = Atexo_LoggerManager::getLogger('archivage');

            $fileLog->info('--------- Debut du cron de supression des archives qui ont depasse '.Atexo_Config::getParameter('NBR_JRS_DISPONIBILITE_ARCHIVE_ASYNCHRONE_PF')." jrs sur la PF---------\n");
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $fileLog->info("Recuperation des archives a supprimer... \n");
            $archivesToDelete = self::getListArchiveToDelete($connexion);
            $fileLog->info('il y a '.count($archivesToDelete)."  archives a supprimer\n");
            if (count($archivesToDelete)) {
                self::deleteArchive($archivesToDelete, $connexion, $fileLog);
            }
            $fileLog->info("--------- Fin du cron de supression des archives ---------\n");
        } catch (Exception $e) {
            $fileLog->error('erreur : '.$e->getMessage()."\n");
        }
    }

    /**
     * permet de recuperer la liste des telechargements a supprimer.
     *
     * @param string $connexion PropelPDO
     *
     * @return array $arrayTelechargement tableau des objets CommonTTelechargementAsynchrone
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function getListArchiveToDelete($connexion)
    {
        $c = new Criteria();
        $date = Atexo_Util::dateDansPasse(date('Y-m-d'), 0, Atexo_Config::getParameter('NBR_JRS_DISPONIBILITE_ARCHIVE_ASYNCHRONE_PF'));
        $c->add(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $date, Criteria::LESS_THAN);
        $c->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE, 1);
        $c->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME, 0);
        $c->add(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, Atexo_Config::getParameter('TYPE_TELECHARGEMENT_ASYNCHRONE_ARCHIVE'));
        $arrayTelechargement = CommonTTelechargementAsynchronePeer::doSelect($c, $connexion);
        if ($arrayTelechargement) {
            return $arrayTelechargement;
        }

        return [];
    }

    /**
     * permet de supprimer des telechargements (CommonTTelechargementAsynchrone).
     *
     * @param $archivesToDelete array tableau des telechargements a supprimer
     * @param string $connexion PropelPDO
     * @param Logger $logger:   logger
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function deleteArchive($archivesToDelete, $connexion, &$logger)
    {
        foreach ($archivesToDelete as $telechargementSupprime) {
            try {
                (new Atexo_Consultation_Archive())->deleteArchive($telechargementSupprime, $connexion);
                $logger->info(" Supression de l'archive ".$telechargementSupprime->getNomFichierTelechargement()."\n");
            } catch (Exception $e) {
                $logger->error(' Erreur lors de la suppresion d un telechargement (AtexoTelechargement.php) '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'AtexoTelechargement.php');
            }
        }
    }
}
