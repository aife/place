<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class Cli_InviteToSubscribe extends Atexo_Cli
{
    public static function run()
    {
        echo "\nDébut \n";
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $nombreJours = 2;
        $Telechargements = self::getListLasDownloaders($nombreJours, $connexionCom);
        if (is_countable($Telechargements) ? count($Telechargements) : 0) {
            foreach ($Telechargements as $unTelechargement) {
                $email = $unTelechargement->getEmail();
                $Inscrit = self::checkEmailInInsctit($email, $connexionCom);
                if (!$Inscrit) {
                    echo "Envoie de l'invitation à ".$email." ...\n";
                    self::InviteDowloader($email);
                }
            }
        }
        echo "Fin \n\n";
    }

    /*
     * retoyurne la liste des telechargement depuis un nombre de jour donné
     */
    public static function getListLasDownloaders($nombreJours, $connexionCom = false)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $since = Atexo_Util::dateDansPasse(date('Y-m-d H:i:s'), 0, $nombreJours, 0, 0, true);
        $c = new Criteria();
        $c->add(CommonTelechargementPeer::DATETELECHARGEMENT, $since, Criteria::GREATER_THAN);
        $Telechargements = CommonTelechargementPeer::doSelect($c, $connexionCom);

        return $Telechargements;
    }

    /*
     * cherche l existance d'un inscrit par son mail
     */
    public static function checkEmailInInsctit($email, $connexionCom = false)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonInscritPeer::EMAIL, $email, Criteria::LIKE);
        $Inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);

        return $Inscrit ?: false;
    }

    /*
     * envoie une invitation d'inscrption
     */
    public static function InviteDowloader($email)
    {
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $subject = Prado::localize('SUBJECT_INVITAION_INSCRIPTION_PF');
        $content = Prado::localize('MESSAGE_INVITAION_INSCRIPTION_PF');
        $message = Atexo_Message::getGenericEmailFormat($content);

        Atexo_Message::simpleMail($from, $email, $subject, $message, '', '', false, true);
    }
}
