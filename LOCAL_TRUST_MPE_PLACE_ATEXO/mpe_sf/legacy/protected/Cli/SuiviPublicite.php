<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Exception;

/**
 * Classe de suivi des annonces de publicite depuis le concentrateur.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Cli_SuiviPublicite extends Atexo_Cli
{
    /**
     * Lancement du cron de suivi des annonces de publicite depuis le concentrateur.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function run()
    {
        $connexion = null;
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces(">>>>>>>>>>>>>>>>>>> DEBUT LANCEMENT DU CRON DE SUIVI DES ANNONCES DE PUBLICITE <<<<<<<<<<<<<<<<<<< \n");
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut transaction : Connexion = '.print_r($connexion, true));
            $connexion->beginTransaction();
            //Recuperation des annonces pour suivi concentrateur
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut recuperation des annonces en base de donnees pour suivi concentrateur');
            $annonces = (new Atexo_Publicite_AnnonceSub())->recupererAnnoncesSuiviPubliciteConcentrateur($connexion);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin recuperation des annonces en base de donnees pour suivi concentrateur');

            //Appel du ws concentrateur pour mise a jour des annonces
            $donneesMaj = null;
            if (!empty($annonces)) {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Nombre d'annonces a traiter = ".count($annonces));
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut constitution de la liste des ID des annonces pour interroger le concentrateur');
                $listeIdAnnonces = self::getListeIdAnnonces($annonces);
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Liste des ID annonces pour interroger le concentrateur : '.print_r($listeIdAnnonces, true));
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin constitution de la liste des ID des annonces pour interroger le concentrateur');

                $donneesMaj = (new Atexo_Publicite_AnnonceSub())->recupererDonneesMajConcentrateur($listeIdAnnonces);
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Donnees pour mise a jour au niveau de mpe : '.print_r($donneesMaj, true));
            } else {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Aucune annonce a traiter');
            }

            //Mise a jour des donnees au niveau de mpe
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut mise a jour des annonces au niveau de MPE');
            self::UpdateAnnoncesFromConcentrateur($donneesMaj, $annonces, $connexion);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin mise a jour des annonces au niveau de MPE');

            $connexion->commit();
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Connexion commitee avec SUCCES : connexion = '.print_r($connexion, true));
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin transaction : Connexion = '.print_r($connexion, true));
        } catch (Exception $e) {
            $connexion->rollBack();
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Connexion annulee 'rollBack' : connexion = ".print_r($connexion, true));
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur suivi des annonces de publicite : '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::run");
        }
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces(">>>>>>>>>>>>>>>>>>> FIN LANCEMENT DU CRON DE SUIVI DES ANNONCES DE PUBLICITE <<<<<<<<<<<<<<<<<<< \n");
    }

    /**
     * Permet de construire la liste des Identifiants SUB des annonces.
     *
     * @param array $listeAnnonces : liste des annonces
     *
     * @return array : liste des identifiants
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function getListeIdAnnonces($listeAnnonces)
    {
        $listeIds = [];
        try {
            if (!empty($listeAnnonces)) {
                foreach ($listeAnnonces as $annonce) {
                    if ($annonce instanceof CommonTAnnonceConsultation) {
                        $listeIds[] = $annonce->getIdDossierSub();
                    }
                }
            }

            return $listeIds;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la constitution des ID des annonces a envoyer au concentrateur : listeAnnonces = '.$listeAnnonces.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::getListeIdAnnonces");
        }
    }

    /**
     * Permet de mettre a jour les annonces de publicite MPE par les donnees de mise a jour Concentrateur.
     *
     * @param array $donneesMaj : donnees de mise a jour du concentrateur
     * @param array $annonces   : annonces a mettre a jour
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function UpdateAnnoncesFromConcentrateur($donneesMaj, $annonces, $connexion)
    {
        try {
            if (!empty($donneesMaj)) {
                $annonces = self::reorganiserListeAnnonces($annonces);
                foreach ($donneesMaj as $donneeMaj) {
                    if (!empty($donneeMaj)) {
                        foreach ($donneeMaj as $donnee) {
                            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut mis a jour annonce [id_dossier_sub = '.$donnee['idDossierSub'].'] , [support publication = '.$donnee['supportPublication'].']');

                            $annonce = $annonces[$donnee['idDossierSub']];
                            if ($annonce instanceof CommonTAnnonceConsultation) {
                                $annonce->setDateModification(date('Y-m-d H:i:s'));
                                $annonce->save($connexion);
                                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Annonce de publicite mis a jour avec SUCCES');
                            }

                            if ($annonce instanceof CommonTAnnonceConsultation) {
                                $supportsAnnonces = $annonce->getCommonTSupportAnnonceConsultations();
                                $supportsAnnonces = self::reorganiserListeSupports($supportsAnnonces);
                                $support = $supportsAnnonces[$donnee['supportPublication']];

                                if ($support instanceof CommonTSupportAnnonceConsultation) {
                                    if ($donnee['statut'] != $support->getStatut()) {
                                        $donnee['dateTraitement'] = Atexo_Util::timestampToFormat($donnee['dateTraitement'] / 1000, 'Y-m-d H:i:s');
                                        $support->setNumeroAvis($donnee['numeroAvis']);
                                        $support->setStatut($donnee['statut']);
                                        $support->setMessageStatut($donnee['messageStatut']);
                                        $support->setDateStatut($donnee['dateTraitement']);
                                        if ($donnee['statut'] == Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION')) {
                                            $support->setDateEnvoiSupport($donnee['dateTraitement']);
                                        }
                                        if ($donnee['statut'] == Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE')) {
                                            $support->setDatePublicationSupport($donnee['dateTraitement']);
                                        }
                                        $support->save($connexion);
                                        //historique
                                        Atexo_Publicite_AnnonceSub::saveHistorique($annonce->getId(), $support->getStatut(), $support->getDateStatut(), $support->getId(), $connexion, $support->getMessageStatut());
                                        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Support de l'annonce de publicite mis a jour avec SUCCES");
                                    }
                                }
                            }

                            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin mis a jour annonce [id_dossier_sub = '.$donnee['idDossierSub'].'] , [support publication = '.$donnee['supportPublication'].']');
                        }
                    }
                }
            }
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la mise a jour des annonces depuis le concentrateur : Donnees maj = '.$donneesMaj.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::UpdateAnnoncesFromConcentrateur");
        }
    }

    /**
     * Permet de reorganiser la liste des annonces
     * Construit un tableau (idDossierSub => $CommonTAnnonceConsultation).
     *
     * @param array $annonces : liste des annonces à reorganiser
     *
     * @return array : liste reorganisee des annonces
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function reorganiserListeAnnonces($annonces)
    {
        try {
            $resultat = [];
            if (!empty($annonces)) {
                foreach ($annonces as $annonce) {
                    if ($annonce instanceof CommonTAnnonceConsultation) {
                        $resultat[$annonce->getIdDossierSub()] = $annonce;
                    }
                }
            }

            return $resultat;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la reorganisation de la liste des annonces : Liste des annonces = '.$annonces.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::reorganiserListeAnnonces");
        }
    }

    /**
     * Permet de reorganiser la liste des supports des annonces de publicite
     * Construit un tableau (codeSupport => $CommonTSupportAnnonceConsultation).
     *
     * @param array $listeSupportsAnnonces : liste des supports a reorganiser
     *
     * @return array : liste reorganisee des supports des annonces de publicite
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function reorganiserListeSupports($listeSupportsAnnonces)
    {
        try {
            $resultat = [];
            if (!empty($listeSupportsAnnonces)) {
                foreach ($listeSupportsAnnonces as $supportAnnonce) {
                    if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                        $support = $supportAnnonce->getSupportPublication();
                        if ($support instanceof CommonTSupportPublication) {
                            $resultat[$support->getCode()] = $supportAnnonce;
                        }
                    }
                }
            }

            return $resultat;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur lors de la reorganisation de la liste des supports des annonces de publicite : Liste supports = '.$listeSupportsAnnonces.' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::reorganiserListeSupports");
        }
    }
}
