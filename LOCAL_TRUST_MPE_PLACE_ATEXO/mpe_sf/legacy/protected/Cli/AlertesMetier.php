<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Mise à jour des alertes metiers.
 *
 * @category Atexo
 */
class Cli_AlertesMetier extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        if (3 != (is_countable($arguments) ? count($arguments) : 0)) {
            echo "Merci d'ajouter un param : \r\n";
            echo "-> nom de l'objet concerné par l'alerte  \r\n";

            return;
        }
        if ('plateForme' == $arguments[2]) {
            self::alertesPlateForme();
        } elseif ('service' == $arguments[2]) {
            self::alertesServices();
        } elseif ('consultation' == $arguments[2]) {
            self::alertesConsultations();
        }
    }

    public static function alertesPlateForme()
    {
        $log = ' début de lancement du cron Plate Forme '.date('Y-m-d H:i:s');
        try {
            Atexo_AlerteMetier::lancerAlerte('CronPF', null);
        } catch (Exception $ex) {
            $log .= "\n Error : ".$ex->getMessage();
        }

        $log .= "\n Fin de lancement du cron Plate Forme ".date('Y-m-d H:i:s');
        $log .= "\n ->Done";
        $log .= "\n\t____________________________________\n";
        echo $log;
        $filePath = 'alertesMetier.log';
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
    }

    public static function alertesServices()
    {
        $log = ' début de lancement du cron Services '.date('Y-m-d H:i:s');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $services = CommonServicePeer::doSelect($c, $connexion);
            foreach ($services as $service) {
                Atexo_AlerteMetier::lancerAlerte('CronService', $service);
            }
        } catch (Exception $ex) {
            $log .= "\n Error : ".$ex->getMessage();
        }

        $log .= "\n Fin de lancement du cron Services ".date('Y-m-d H:i:s');
        $log .= "\n ->Done";
        $log .= "\n\t____________________________________\n";
        echo $log;
        $filePath = 'alertesMetier.log';
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
    }

    public static function alertesConsultations()
    {
        $log = ' début de lancement du cron Consultations '.date('Y-m-d H:i:s');

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        // 1ème cron
        $c = new Criteria();
        $yesterday = Atexo_Util::dateDansFutur(date('Y-m-d'), 0, -1, 0, 0);
        $yesterday = explode(' ', $yesterday);
        $vnull = '';
        $monthAgo = Atexo_Util::dateDansFutur(date('Y-m-d'), 0, 0, -3, 0);
        $monthAndDaysAgo = Atexo_Util::dateDansFutur(date('Y-m-d'), 0, -10, -3, 0);
        $crit0 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $vnull, Criteria::NOT_EQUAL);
        $crit1 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $yesterday[0], Criteria::LESS_THAN);

        // Perform AND at level 1 ($crit0 $crit1 )
        $crit0->addAnd($crit1);

        $crit2 = $c->getNewCriterion(CommonConsultationPeer::DATE_DECISION, $vnull, Criteria::EQUAL);
        $crit3 = $c->getNewCriterion(CommonConsultationPeer::DATEFIN, $monthAgo, Criteria::LESS_THAN);
        $crit4 = $c->getNewCriterion(CommonConsultationPeer::DATEFIN, '0000-00-00 00:00:00', Criteria::NOT_EQUAL);
        $crit5 = $c->getNewCriterion(CommonConsultationPeer::DATEFIN, $monthAndDaysAgo, Criteria::GREATER_THAN);

        $crit7 = $c->getNewCriterion(CommonConsultationPeer::ID_ETAT_CONSULTATION, '0', Criteria::EQUAL);

        $crit6 = $c->getNewCriterion(CommonConsultationPeer::ID_TYPE_AVIS, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), Criteria::EQUAL);
        $crit8 = $c->getNewCriterion(CommonConsultationPeer::ID_ARCHIVEUR, null, Criteria::EQUAL);
        $crit9 = $c->getNewCriterion(CommonConsultationPeer::DATE_ARCHIVAGE, null, Criteria::EQUAL);

        // Perform AND at level 1 ($crit2 $crit3 $crit4 $crit5)
        $crit2->addAnd($crit3);
        $crit2->addAnd($crit4);
        $crit2->addAnd($crit5);
        $crit2->addAnd($crit7);

        // Perform OR at level 0 ($crit0 $crit2 )
        $crit0->addOr($crit2);
        // Perform AND at level 0 ($crit0 $crit6 )
        $crit0->addAnd($crit6);
        $crit0->addAnd($crit8);
        $crit0->addAnd($crit9);

        $c->add($crit0);
        $consultations = CommonConsultationPeer::doSelect($c, $connexion);

        foreach ($consultations as $consultation) {
            $resAlerte = Atexo_AlerteMetier::lancerAlerte('CronConsultationResultatAnalyseOrExtraitPV', $consultation);
        }

        // 2ème cron
        $c = new Criteria();
        $vnull = '';
        $montantMax = '100000000';
        $c = new Criteria();
        $c->addJoin(CommonDecisionEnveloppePeer::CONSULTATION_ID, CommonConsultationPeer::ID);
        $c->addJoin(CommonConsultationPeer::ORGANISME, CommonDecisionEnveloppePeer::ORGANISME);
        $c->add(CommonDecisionEnveloppePeer::MONTANT_MARCHE, $montantMax, Criteria::GREATER_THAN);
        $c->add(CommonDecisionEnveloppePeer::DATE_FIN_MARCHE_PREVISIONNEL, $vnull, Criteria::NOT_EQUAL);
        $c->add(CommonDecisionEnveloppePeer::DATE_FIN_MARCHE_PREVISIONNEL, date('Y-m-d'), Criteria::LESS_THAN);
        $c->add(CommonConsultationPeer::ID_TYPE_AVIS, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), Criteria::EQUAL);
        $consultations = CommonConsultationPeer::doSelect($c, $connexion);

        foreach ($consultations as $consultation) {
            $resAlerte = Atexo_AlerteMetier::lancerAlerte('CronConsultationRapportAchevement', $consultation);
        }

        $log .= "\n Fin de lancement du cron Consultations ".date('Y-m-d H:i:s');
        $log .= "\n ->Done";
        $log .= "\n\t____________________________________\n";
        echo $log;
        $filePath = 'alertesMetier.log';
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
    }
}
