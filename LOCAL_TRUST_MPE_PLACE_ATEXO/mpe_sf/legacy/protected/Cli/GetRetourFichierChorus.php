<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Logger;

/**
 * Récupère les fichiers qui sont au niveau de CHORUS.
 *
 * @author Mohamed Wazni
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */

class Cli_GetRetourFichierChorus extends Atexo_Cli {

    public static function run() {
        $params = array();
        $params['service'] = "Chorus";
        $params['nomBatch'] = "Cli_GetRetourFichierChorus";

        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info("Début de la commande Cli_GetRetourFichierChorus");

        self::oversight($params);
        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;

        $baseurl = Atexo_Config::getParameter('URL_FICHIERS_RETOUR_CHORUS');

		$htmlIndex = self::getUrl($baseurl, $logger);

        if (false == $htmlIndex) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorRemoteServiceDoesNotRespond;
        }

        $fileNames = self::getNomFichierRetour($htmlIndex);

        $directory_name = Atexo_Config::getParameter('DIR_FILE_CHORUS_RECEPTION');
        $archive_directory_name = Atexo_Config::getParameter('DIR_FILE_CHORUS_ARCHIVE');

        if (null != $directory_name && !is_dir($directory_name)) {
            mkdir($directory_name, 0777, true);
        }

        foreach($fileNames as $file) {
        	if(!is_file($archive_directory_name.$file)) {
        		$fileContent = self::getUrl($baseurl.$file, $logger);
        		$fileContent=substr($fileContent, strpos($fileContent, "<?"));
                if(!empty($fileContent)) {
                    (new Atexo_Files())->string2file($fileContent, $directory_name.$file);
                } else {
                    $objet = "Erreur lors de la récupération du fichier : "
                    . $file . "(routine : GetRetourFichierChorus)";
                    $date = date("Y-m-d H:i:s");
                    $message = <<<STR
Bonjour,
Un fichier vide a été détecté  et il a été  ignoré .
Nom fichier : $file
date et heure : $date
STR;
                    (new Atexo_Chorus_Util())->envoyerMailSupportChorus( $objet, $message);
                    $logger->error("Cli_GetRetourFichierChorus Fichier vide " . $directory_name . $file);
                }
                self::oversight($params);
            }
        }

        $logger->info("Fin de la commande Cli_GetRetourFichierChorus");
    }

    /**
     * @return bool|string
     */
    public static function getUrl(string $page)
    {
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $errorMessage = sprintf("Erreur lors de l'appel de l'url %s", $page);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $page);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch,CURLOPT_SSLCERT,Atexo_Config::getParameter('CHORUS_SSLCERT'));
            curl_setopt($ch,CURLOPT_SSLCERTPASSWD, Atexo_Config::getParameter('CHORUS_SSLCERTPASSWD'));
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));

            //definition du proxy si existe
            $urlProxy = Atexo_Config::getParameter("URL_PROXY");
            $portProxy = Atexo_Config::getParameter("PORT_PROXY");
            $curlOptProxy = $urlProxy.":".$portProxy;
            if ($urlProxy!=""){
                curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            }

            $html = curl_exec($ch);
            /* in case of an error*/
            if(curl_exec($ch) === false) {
                echo '<p class="small">Error. Please check URL: <strong style="color:#ae3100">' . curl_error($ch).'</p></strong>';
                $logger->error($errorMessage);
                $logger->error(curl_error($ch));
            }

            curl_close($ch);
        } catch (\Exception $e) {
            $logger->error($errorMessage);
            $logger->error(sprintf(
                "Erreur %s %s Trace: %s", PHP_EOL, $e->getMessage(), $e->getTraceAsString()
            ));
        }

        if(!$html){
        	return false;
        }else{
        	return $html;
        }
    }

    public static function getNomFichierRetour($html)
    {
        preg_match_all("/(<([a\ehref=\"]+)[^>]*>)(.*?)(<\/\\2>)/", $html, $matches, PREG_SET_ORDER);
        $fileNames = [];
        foreach ($matches as $val) {
            $myRefArray = explode('"', $val[1]);
            if (strstr($myRefArray[1], 'CEN') || strstr($myRefArray[1], 'FSO') || strstr($myRefArray[1], 'FIR') || strstr($myRefArray[1], 'CIR')) {
                $fileNames[] = $myRefArray[1];
            }
        }

        return $fileNames;
    }
}
