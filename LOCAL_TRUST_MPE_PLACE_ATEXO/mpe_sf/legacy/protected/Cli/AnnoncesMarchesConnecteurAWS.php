<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Exception;

/**
 * Cron de mise a jour des consultations du portail Annonces-Marches depuis AchatPublic.Com (APC).
 *
 * @category Atexo
 */
class Cli_AnnoncesMarchesConnecteurAWS extends Atexo_Cli
{
    protected static string $acronyme = '';
    protected static string $lieuExecution = '';
    protected static bool $erreur = false;

    public static function run()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $options = new \Zend_Console_Getopt('-o:');
            //$options = getopt('-o:e:l:');

            if (isset($options->o)) {
                self::$acronyme = $options->o;
            } else {
                echo "l'option -o (acronyme de l'organisme) est obligatoire \n";

                return false;
            }
        } catch (\Zend_Console_Getopt_Exception $e) {
            $logger->error($e->getMessage().PHP_EOL.$e->getUsageMessage());
        }
        try {
            $migrationStart = time();
            $param = '';
            $pathDirXml = Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_A_TRAITER');
            $logger->info('Debut Traitement - Repertoire des Xmls à traités : '.$pathDirXml);

            $listeXml = self::getListXmlATraites($pathDirXml, $logger);
            if (false !== $listeXml) {
                if (is_array($listeXml) && count($listeXml)) {
                    $logger->info('Nombre de xml a traites : '.count($listeXml));
                    foreach ($listeXml as $oneFileXml) {
                        self::$erreur = false;
                        $logger->info('Traitement Xml : '.$oneFileXml);
                        $xml = (new Atexo_Publicite_Boamp())->getXmlDocument(file_get_contents($oneFileXml));
                        if ($xml) {
                            $fluxExportAWS = $xml->getElementsByTagName('fluxExportAWS');
                            $dateFichier = $fluxExportAWS->item(0)->getAttribute('dateFichier');
                            $listeAvis = $xml->getElementsByTagName('avis');
                            $logger->info('Info Xml dateFichier : '.$dateFichier);
                            if (is_array($listeAvis) && count($listeAvis) > 0) {
                                $logger->info("Nombre d'avis  a traites : ".count($listeAvis));
                                foreach ($listeAvis as $OneCons) {
                                    self::addConsultation($OneCons, $logger);
                                }
                            }
                        } else {
                            $logger->error("chaine xml non valide $oneFileXml ");
                            self::$erreur = true;
                        }
                        if (true == self::$erreur) {
                            $nouveauNomFichier = date('Y_m_d_H_i_s').'_'.basename($oneFileXml);
                            $logger->error("Erreur Fin traitement du xml  $oneFileXml avec des erreurs ");
                            $logger->info("Deplacement de $oneFileXml vers le dossier des xmls avec erreur : ".Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_AVEC_ERREUR'));
                            rename($oneFileXml, Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_AVEC_ERREUR').$nouveauNomFichier);
                        } else {
                            $nouveauNomFichier = date('Y_m_d_H_i_s').'_'.basename($oneFileXml);
                            $logger->info("Succes : Fin traitement du xml  $oneFileXml avec succes");
                            $logger->info("Deplacement de $oneFileXml vers le dossier des xmls traités : ".Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_TRAITES'));
                            rename($oneFileXml, Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_TRAITES').$nouveauNomFichier);
                        }
                    }
                } else {
                    $logger->info('Aucun xml a traite : '.print_r($listeXml, true));
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            $logger->error('Erreur lors du traitements '.$e->getMessage());
        }
    }

    protected static function getListXmlATraites($directory, $logger)
    {
        $listeXml = [];
        if (($dir = opendir($directory)) === false) {
            $logger->error('Repertoire non trouve : '.$directory);

            return false;
        }
        while (($file = readdir($dir)) !== false) {
            $path = $directory.DIRECTORY_SEPARATOR.$file;
            if ('.' === $file || '..' === $file || is_dir($path)) {
                continue;
            } elseif (self::validateXml($path, $logger)) {
                $listeXml[] = $path;
            } else {
                $logger->error("Le fichier  $path n'est pas valide selon la xsd : ".Atexo_Config::getParameter('PATH_FILE_XSD_CONNECTEUR_AWS'));
                $nouveauNomFichier = date('Y_m_d_H_i_s').'_'.basename($path);
                $logger->info("Deplacement de $path vers le dossier des xmls avec erreur : ".Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_AVEC_ERREUR').$nouveauNomFichier);
                rename($path, Atexo_Config::getParameter('DIR_LOGS_ANNONCES_CONNECTEUR_AWS_XML_AVEC_ERREUR').$nouveauNomFichier);
            }
        }
        closedir($dir);

        return $listeXml;
    }

    public static function validateXml($path, $logger)
    {
        return true; //(is_file($path) && Atexo_Util::validateXmlBySchema(file_get_contents($path),Atexo_Config::getParameter('PATH_FILE_XSD_CONNECTEUR_AWS'),$logger));
    }

    /*
     * permet de créer/modifier une consultation
     */
    public static function addConsultation($cons, $logger)
    {
        $idtypeprocedure = null;
        $lieuxExecution = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $correspondanceTypeAvis = ['AAPC' => '3', 'RECTIF' => 'RECTIF', 'ATTR' => '4'];
        //$cons = (array)($OneCons);
        $typeAvis = $cons->getAttribute('type_avis');
        $idTypeAvis = $correspondanceTypeAvis[$typeAvis];
        $refUser = (new Atexo_Util())->toHttpEncoding($cons->getAttribute('Reference'));
        $refExterne = $cons->getAttribute('ReferenceSupport');
        if ('RECTIF' == $idTypeAvis || !$idTypeAvis) {
            $logger->error('Type de avis non encore gerer  : '.$refUser.' -- '.$refExterne);

            return;
        }
        $categoryName = self::getContentNode($cons, 'nature');

        if (!in_array($categoryName, ['TRAVAUX', 'FOURNITURES', 'SERVICES'])) {
            $categoryName = 'Services';
        }
        $category = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($categoryName);

        $abbreviationTypeProc = self::getContentNode($cons, 'procedure');
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationInterface($abbreviationTypeProc, self::$acronyme);
        if (!$typeProcedure instanceof CommonTypeProcedureOrganisme) {
            $logger->info('Type de procedure non trouvee : '.$refUser.' -- '.$refExterne.' -- abbreviation : '.$abbreviationTypeProc);
        } else {
            $idtypeprocedure = $typeProcedure->getIdTypeProcedurePortail();
        }
        $objet = (new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($cons, 'objet')));
        $dateMiseEnLigneCalcule = self::iso860TimeToIso(trim(self::getContentNode($cons, 'datePublication')));
        $dateCloture = trim(self::getContentNode($cons, 'dateExpiration'));

        //$dateCloture       = preg_replace('#([0-9]+)/([0-9]+)/([0-9]+) ([0-9]+):([0-9]+)#', '$3-$2-$1 $4:$5:00', $cons["@attributes"]['closureDate']);
        $acheteurNode = $cons->getElementsByTagName('acheteur');
        $organismeName = (new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($acheteurNode[0], 'nom')));

        $codeCP = trim(self::getContentNode($acheteurNode[0], 'codePostal'));
        $city = strtolower((new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($acheteurNode[0], 'ville'))));

        $liens = $cons->getElementsByTagName('liens');
        $lienDirect = self::getContentNode($liens[0], 'urlAvis');

        $lien = '/redir.php?url='.base64_encode($lienDirect);
        $newRef = sha1(self::$acronyme.'#'.$refExterne);
        $departementsNode = $cons->getElementsByTagName('departement');
        if (is_countable($departementsNode) ? count($departementsNode) : 0) {
            $lieuxExecution = self::getIdsLieuxExecution($departementsNode);
        }
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REF_ORG_PARTENAIRE, $newRef);
        $c->add(CommonConsultationPeer::ORGANISME, self::$acronyme);
        $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);
        $create = false;
        if (!($commonConsultation instanceof CommonConsultation)) {
            $commonConsultation = new CommonConsultation();
            $commonConsultation->setOrganisme(self::$acronyme);
            $commonConsultation->setRefOrgPartenaire($newRef);
            $commonConsultation->setDatedebut(date('Y-m-d H:i:s'));
            $create = true;
        }

        $commonConsultation->setOrgDenomination($organismeName.'('.$codeCP.' - '.$city.')');
        $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($organismeName)));
        $commonConsultation->setReferenceUtilisateur($refUser);
        $commonConsultation->setIdTypeProcedure($idtypeprocedure);
        $commonConsultation->setIdTypeAvis($idTypeAvis);
        $commonConsultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        $commonConsultation->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1'));
        $commonConsultation->setDateMiseEnLigneCalcule($dateMiseEnLigneCalcule);
        $commonConsultation->setServiceId('0');
        $commonConsultation->setServiceAssocieId('0');
        $commonConsultation->setConsultationExterne('1');
        $commonConsultation->setCategorie($category->getId());
        $commonConsultation->setDatefin($dateCloture);
        $commonConsultation->setIntitule($objet);
        $commonConsultation->setObjet($objet);
        $commonConsultation->setLieuExecution($lieuxExecution);
        $commonConsultation->setUrlConsultationExterne($lien);
        $commonConsultation->setUrlConsultationAchatPublique($lienDirect);
        try {
            $commonConsultation->save($connexionCom);
        } catch (Exception $e) {
            $logger->error("Erreur lors de la creation de la consultation $refExterne ".$e->getMessage());
            self::$erreur = true;
            //Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS_OMNIKLES'),date("Y-m-d H:i:s") .' -- >  \n'.$e->getMessage().'\n',"a+");
        }
        // Referntiel org denomonation debut
        if ($commonConsultation->getDenominationAdapte()) {
            (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomonation($commonConsultation);
        }
        if ($create) {
            $logger->info('Avis creee: '.$refUser.' -- '.$refExterne.' -- '.$commonConsultation->getId());
        } else {
            $logger->info('Avis modifiee : '.$refUser.' -- '.$refExterne.' -- '.$commonConsultation->getId());
            (new Atexo_Consultation_Lots())->deleteCommonLots(self::$acronyme, $commonConsultation->getId());
            $commonConsultation->setAlloti('0');
        }
        if ($idTypeAvis == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $lotsNode = $cons->getElementsByTagName('lot');
            $logger->info('Nombre de lots  a traites : '.(is_countable($lotsNode) ? count($lotsNode) : 0));
            if (is_countable($lotsNode) ? count($lotsNode) : 0) {
                foreach ($lotsNode as $oneLot) {
                    $numLot = $objet = (new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($oneLot, 'numeroLot')));
                    $description = (new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($oneLot, 'intituleLot')));
                    $descriptionDetail = (new Atexo_Util())->toHttpEncoding(trim(self::getContentNode($oneLot, 'description')));
                    $dataLot = ['numero' => $numLot, 'description' => $description, 'descriptionDetail' => $descriptionDetail];
                    self::addLot($dataLot, $commonConsultation, $logger);
                }
                $commonConsultation->setAlloti('1');
            }
        }
        try {
            $commonConsultation->save($connexionCom);
        } catch (Exception $e) {
            self::$erreur = true;
            $logger->error("Erreur lors de la creation de la consultation $refExterne ".$e->getMessage());
        }
    }

    /*
     * permet d'ajouter un lot
     */

    /*
     * convertis une date format C (frn860) to iso.
     */
    public function frn860TimeToIso($date)
    {
        sscanf($date, '%u/%u/%4uT%u:%u:%u', $jour, $mois, $annee, $heure, $minute, $seconde);
        $newTstamp = mktime($heure, $minute, $seconde, $mois, $jour, $annee);

        return date('Y-m-d H:i:s', $newTstamp);
    }

    public function iso860TimeToIso($date)
    {
        $time = strtotime($date);

        return date('Y-m-d H:i:s', $time);
    }

    public static function getIdsLieuxExecution($departements)
    {
        $arrayDenom2 = [];
        $arrayids = [];
        foreach ($departements as $oneDep) {
            $arrayDenom2[] = $oneDep->get_content();
        }
        if (is_array($arrayDenom2)) {
            $lieux = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByArrayDenomination2($arrayDenom2);
            if (is_array($lieux)) {
                foreach ($lieux as $oneLieu) {
                    $arrayids[] = $oneLieu->getId();
                }
            }
        }
        if (is_array($arrayids)) {
            return ','.implode($arrayids, ',').',';
        } else {
            return '';
        }
    }

    public static function addLot($lotData, $commonConsultation, $logger)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $lotObjet = new CommonCategorieLot();
            $lotObjet->setlot($lotData['numero']);
            $lotObjet->setConsultationId($commonConsultation->getId());
            $lotObjet->setOrganisme($commonConsultation->getOrganisme());
            $lotObjet->setDescription($lotData['description']);
            $lotObjet->setDescriptionDetail($lotData['descriptionDetail']);
            $lotObjet->setCategorie($commonConsultation->getCategorie());
            $lotObjet->save($connexionCom);
            $logger->info("insertion du lot numero '".$lotData['numero']."' pour la consultation : '".$commonConsultation->getReferenceUtilisateur()."' (".$commonConsultation->getId().')');
        } catch (Exception $e) {
            $logger->error("Erreur lors de la creation des lots de la consultation  '".$commonConsultation->getId()."' ".$e->getMessage());
            self::$erreur = true;
        }
    }

    public static function getContentNode($node, $name)
    {
        $content = null;
        $contentNode = $node->getElementsByTagName($name);
        if ($contentNode->item(0)) {
            $content = $contentNode->item(0)->nodeValue;
        }

        return $content;
    }
}
