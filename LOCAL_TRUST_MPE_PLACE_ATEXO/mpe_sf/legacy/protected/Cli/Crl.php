<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Recupère les CRL sur un serveur centralisé.
 *
 * @category Atexo
 */
class Cli_Crl extends Atexo_Cli
{
    const PROTOCOL = 'https';
    const HOST = 'ac.local-trust.com';
    const PORT = 443;
    const FILE = 'trust.tgz';
    const FILE_ALTERNATE = 'trust_alternate.tgz';

    /**
     * Fetch and install all certificates (CA Root+certs) and CRL.
     */
    public static function run()
    {
        self::getTarCRL(self::FILE);
        self::getTarCRL(self::FILE_ALTERNATE);
        system('cd '.Atexo_Config::getParameter('TRUST_DIR').' && for file in `cat todel.list`;do rm -f $file;done;./trust.sh > /dev/null 2>&1;rm -f todel.list');
    }

    protected static function getTarCRL($file)
    {
        $url = self::PROTOCOL.'://'.self::HOST.':'.self::PORT.'/'.$file;
        self::display('Connecting to '.self::HOST.':'.self::PORT.'...');

        self::display("Downloading $url ...");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $trust = curl_exec($ch);
        $info = curl_getinfo($ch);

        if (false === $trust || 200 != $info['http_code']) {
            $output = "No data returned for $url [".$info['http_code'].']';
            if (curl_error($ch)) {
                $output .= "\n".curl_error($ch);
            }
            curl_close($ch);
            self::display($output);
            self::display("Impossible to get $url, trying locally");
            if (!is_dir(Atexo_Config::getParameter('TRUST_DIR'))) {
                mkdir(Atexo_Config::getParameter('TRUST_DIR'), 0755, true);
            }
            touch(Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'todel.list');
            copy(self::getRootPath().'/mpe/ressources/openssl/trust.sh', Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'trust.sh');
            copy(self::getRootPath().'/mpe/ressources/openssl/Makefile.crl', Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'Makefile.crl');
            copy(self::getRootPath().'/mpe/ressources/openssl/Makefile.crt', Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'Makefile.crt');
            chmod(Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'trust.sh', 0755);
            chmod(Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'Makefile.crl', 0755);
            chmod(Atexo_Config::getParameter('TRUST_DIR').DIRECTORY_SEPARATOR.'Makefile.crt', 0755);

            return false;
        } else {
            self::display('Download OK : '.$file.' ['.Atexo_Util::GetSizeName($info['size_download']).' in '
                .number_format($info['total_time'], 2).'sec @'.Atexo_Util::GetSizeName($info['speed_download']).'/sec]');
        }
        curl_close($ch);
        $tgz = str_replace('//', '/', Atexo_Config::getParameter('COMMON_TMP').'/'.$file);
        self::display("Fetching file $tgz");
        file_put_contents($tgz, $trust);
        if (!is_dir(Atexo_Config::getParameter('TRUST_DIR'))) {
            mkdir(Atexo_Config::getParameter('TRUST_DIR'), 0775, true);
            self::display('Création du dossier "'.Atexo_Config::getParameter('TRUST_DIR').'"');
        }
        $dest_dir = realpath(str_replace('//', '/', Atexo_Config::getParameter('TRUST_DIR').'/../'));
        self::display('Decompressing file into '.$dest_dir);
        system("tar zxf $tgz -C ".$dest_dir);
        self::display('END (all is OK)');
    }
}
