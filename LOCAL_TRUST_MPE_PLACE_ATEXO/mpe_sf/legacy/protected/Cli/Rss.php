<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_FluxRss;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Rss;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Consultation;
use Application\Service\Atexo\Organisme\Atexo_Organisme_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Rss\Atexo_Rss_Item;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Symfony\Component\HttpFoundation\Request;

/**
 * Generation de flux RSS a  partir d'un webService (Tenders)
 * Le flux renvoye par le script est egalement stocke a  la racine du mpe : /rss.xml.
 *
 * @category Atexo
 */
class Cli_Rss extends Atexo_Cli
{
    public static function run()
    {
        // Encoding du flux RSS
        $encoding = 'UTF-8';
        // Url de la plateforme (balise <link> du channel)
        $urlPF = Atexo_Config::getParameter('PF_URL_REFERENCE');
        // Langue du flux
        $lang = 'fr';
        // Indique la durée de vie en minute pour le flux (les agrégateurs RSS ne vont pas rafraichir)
        $ttl = '1440'; // 24 Heures

        // Titre du channel (balise <title> du channel)
        $title = Atexo_Config::getParameter('PF_LONG_NAME');
        // Description (tag <description> du channel RSS)
        $description = 'Consultation du jour sur '.Atexo_Config::getParameter('PF_SHORT_NAME');

        $arrayOfFluxRss = (new Atexo_FluxRss())->getAllFluxRss();
        if (is_array($arrayOfFluxRss) && count($arrayOfFluxRss)) {
            foreach ($arrayOfFluxRss as $fluxRss) {
                try {
                    $tenderXml = $fluxRss->getTenderXml();
                    // Creation du Channel RSS
                    $rssFeed = self::createNewRssFeed($encoding, $urlPF, $lang, $ttl, $title, $description);
                    self::appendTender($rssFeed, $tenderXml);

                    //Depalcement du flux RSS au niveua de DIR_FLUX_RSS
                    $oldfluxRss = realpath(__DIR__.'/../../../').'/'.$fluxRss->getNomFichier();
                    $newFluxRss = Atexo_Config::getParameter('DIR_FLUX_RSS').'/'.$fluxRss->getNomFichier();

                    if (!is_dir(Atexo_Config::getParameter('DIR_FLUX_RSS'))) {
                        mkdir(Atexo_Config::getParameter('DIR_FLUX_RSS'));
                    }

                    if (is_file($oldfluxRss)) {
                        rename($oldfluxRss, $newFluxRss);
                    }
                    //Sauvegarde du nouveau flux RSS  au niveua de DIR_FLUX_RSS
                    $rssFeed->save($newFluxRss);
                } catch (Exception $ex) {
                    Prado::log('Flux RSS Erreur de génération : '.$ex->getMessage().' FileName = '.$fluxRss->getNomFichier(), TLogger::ERROR, 'Atexo.Cli.RSS');
                }
            }
        }
    }

    /*
     * permet de creer un nouveau rss
    */
    public static function createNewRssFeed($encoding, $urlPF, $lang, $ttl, $title, $description)
    {
        $rssFeed = new Atexo_Rss($encoding);
        $rssFeed->setProtectString(true); //enlève les tags html
        $rssFeed->setTitle($title);
        $rssFeed->setDescription($description);
        $rssFeed->setLink($urlPF);
        $rssFeed->setLanguage($lang);
        $rssFeed->setPubDate(date('Y-m-d'));
        $rssFeed->setLastBuildDate(date('Y-m-d H:i:s'));
        $rssFeed->setTimeToLive($ttl);

        return $rssFeed;
    }

    public static function appendTender(&$rssFeed, $tenderXml)
    {
        $xmlTender = null;
        // Appel de l'xml request pour recuperer toutes les consultations
        $xmlReqContent = simplexml_load_string($tenderXml);
        if ($xmlReqContent) {
            $request = $xmlReqContent->Request;
            try {
                if (isset($request->Tender)) {
                    // Appel de la classe de l'interface WS qui retourne le flux avec des objets Tender
                    $consInterface = new Atexo_InterfaceWs_Consultation();
                    $xmlTender = $consInterface->getXmlTender($request->Tender);
                    if ($xmlTender && Atexo_Util::isValidXml($xmlTender)) {
                        $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($xmlTender);
                        if ($domXml instanceof \DOMDocument) {
                            $nodeTender = $domXml->getElementsByTagName('Tender');
                            $nombreTender = count($nodeTender);
                            // Ajout des Items
                            for ($i = 0; $i < $nombreTender; ++$i) {
                                $acronyme = $nodeTender[$i]->getAttribute('organism');
                                $criteria = new Atexo_Organisme_CriteriaVo();
                                $criteria->setAcronyme($acronyme);
                                $arrayOrganismes = (new Atexo_Organismes())->search($criteria);
                                $service = $nodeTender[$i]->getAttribute('service');
                                $reference = $nodeTender[$i]->getAttribute('reference');
                                $referenceId = $nodeTender[$i]->getAttribute('referenceId');
                                //les variables $buyerEmail et $appelOffreId sont valorisées uniquement pour les consultations avec marche_public_simplifie = 1
                                //et si le flux RSS est celui qui est dédié au MPS
                                $buyerEmail = $nodeTender[$i]->getAttribute('buyerEmail');
                                $appelOffreId = $nodeTender[$i]->getAttribute('appelOffreId');
                                $type = $nodeTender[$i]->getAttribute('type');
                                $procedureType = $nodeTender[$i]->getAttribute('procedureType');
                                $dateOuverture = Atexo_Util::formateIsoDate(Atexo_Util::iso860TimeToIso($nodeTender[$i]->getAttribute('dateOnLineCalculated')), 'r');
                                $category = $nodeTender[$i]->getAttribute('category');
                                $closureDate = Atexo_Util::formateIsoDate(Atexo_Util::iso860TimeToIso($nodeTender[$i]->getAttribute('closureDate')), 'r');
                                $pubDate = $nodeTender[$i]->getAttribute('dateOnLineCalculated');
                                $deliveryPlace = $nodeTender[$i]->getAttribute('deliveryPlace');
                                $nodeTitle = $nodeTender[$i]->getElementsByTagName('Title');

                                $title = $nodeTitle[0]->nodeValue;

                                $nodeSubject = $nodeTender[$i]->getElementsByTagName('Subject');
                                $description = $nodeSubject[0]->nodeValue;
                                if (strlen($description) < 1) {
                                    $description = ' ';
                                }
                                $nodeUrl = $nodeTender[$i]->getElementsByTagName('Url');
                                $url = $nodeUrl[0]->nodeValue;
                                $cpv = $nodeTender[$i]->getAttribute('cpv');
                                $codeOperation = $nodeTender[$i]->getAttribute('codeOperation');
                                // S'il n'y a pas d'intitulé (car non obligatoire dans MPE), on prend le début de la description (50 premiers caract.) en tant que titre
                                if (null === $title || !isset($title) || '' == $title) {
                                    $title = wordwrap($description, 50, "...\n");
                                    if (strpos($title, "\n") > 0) {
                                        $title = substr($title, 0, strpos($title, "\n"));
                                    }
                                }
                                $rssItem = new Atexo_Rss_Item();
                                $rssItem->setTitle(Atexo_Util::atexoHtmlEntitiesDecode($title));
                                $rssItem->setDescription(Atexo_Util::atexoHtmlEntitiesDecode($description));
                                $rssItem->setLink($url);
                                $rssItem->setGuid($referenceId.'_'.$reference);
                                $rssItem->setPubDate($pubDate);
                                $rssItem->setCategory('Avis', $type);
                                $rssItem->setCategory('Titre', Atexo_Util::atexoHtmlEntitiesDecode($title));
                                $rssItem->setCategory('Objet', Atexo_Util::atexoHtmlEntitiesDecode($description));
                                $rssItem->setCategory('Procedure', $procedureType);
                                $rssItem->setCategory('DateLimite', $closureDate);
                                $rssItem->setCategory('DateOuverture', $dateOuverture);
                                $rssItem->setCategory('Category', $category);
                                $rssItem->setCategory('Organisme', $acronyme);
                                if ($arrayOrganismes) {
                                    $rssItem->setCategory('Organisme_nom', Atexo_Util::atexoHtmlEntitiesDecode($arrayOrganismes[0]->getDenominationOrg()));
                                    if ($arrayOrganismes[0]->getSiren()) {
                                        $rssItem->setCategory('Organisme_siren', $arrayOrganismes[0]->getSiren());
                                    }
                                    if ($arrayOrganismes[0]->getCp()) {
                                        $rssItem->setCategory('Organisme_code_postal', $arrayOrganismes[0]->getCp());
                                    }
                                }
                                $rssItem->setCategory('EntiteAchat', Atexo_Util::atexoHtmlEntitiesDecode($service));
                                if ($deliveryPlace) {
                                    $rssItem->setCategory('LieuxExecution', $deliveryPlace);
                                }

                                if ($buyerEmail) {
                                    $rssItem->setCategory('buyerEmail', $buyerEmail);
                                }

                                if ($appelOffreId) {
                                    $rssItem->setCategory('appelOffreId', $appelOffreId);
                                }

                                $rssItem->setCategory('ClosureDate', $closureDate);
                                if (!empty($cpv)) {
                                    $rssItem->setCategory('cpv', $cpv);
                                }
                                if (!empty($codeOperation)) {
                                    $rssItem->setCategory('codeOperation', $codeOperation);
                                }
                                $nodeLots = $nodeTender[$i]->getElementsByTagName('Lots');
                                if ((is_countable($nodeLots) ? count($nodeLots) : 0) > 0) {
                                    $nodeLot = $nodeLots[0]->getElementsByTagName('Lot');
                                    $nombreLots = is_countable($nodeLot) ? count($nodeLot) : 0;
                                    for ($j = 0; $j < $nombreLots; ++$j) {
                                        if ($nodeLot[$j]) {
                                            $nodeTitleLot = $nodeLot[$j]->getElementsByTagName('Title');
                                            $rssItem->setCategory('Lot', Atexo_Util::atexoHtmlEntitiesDecode($nodeTitleLot[0]->nodeValue));
                                        }
                                    }
                                }
                                // Ajout de l'Item au Feed
                                $rssFeed->appendItem($rssItem);
                            }
                        }
                    }
                }
            } catch (Exception $ex) {
                Prado::log('Flux RSS Erreur de génération '.$ex->getMessage(), TLogger::ERROR, 'Atexo.Cli.RSS');
                $message = Atexo_Message::getGenericEmailFormat('Erreur de generation du flux RSS pour la plate-forme, une exception est survenue : '.$ex->getMessage());
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'Erreur de generation flux RSS', $message, '', '', false, true);
                Atexo_Util::writeFile(Atexo_Config::getParameter('COMMON_TMP').'rss_'.date('Ymd_His').'.xml', $xmlTender);
            }
        }
    }
}
