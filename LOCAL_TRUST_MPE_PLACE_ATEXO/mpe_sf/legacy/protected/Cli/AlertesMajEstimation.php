<?php

namespace Application\Cli;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Mise à jour des alertes metiers.
 *
 * @category Atexo
 */
class Cli_AlertesMajEstimation extends Atexo_Cli
{
    public static function run()
    {
        echo ' début de lancement du script de mise à jour des alerte de delai de publication  '.date('Y-m-d H:i:s');
        try {
            $dateMiseEnligne = '2014-01-01 00:00:00';
            $sql = "SELECT * FROM consultation WHERE date_mise_en_ligne_calcule > '".$dateMiseEnligne."'";
            $statement = Atexo_Db::getLinkCommon(true)->query($sql);
            while ($ligneConsultation = $statement->fetch()) {
                $consultation = new CommonConsultation();
                $consultation->setNew(false);

                foreach ($ligneConsultation as $key => $value) {
                    $setValue = 'set'.Atexo_Util::getFormatMethodeFromChamps($key);
                    if (method_exists($consultation, $setValue)) {
                        $consultation->$setValue($value);
                    }
                }
                echo "\n mise à jour de la consultation dont la reference => ".$consultation->getId();
                $resAlerte = Atexo_AlerteMetier::lancerAlerte('ScriptAfterMajEstimation', $consultation);
            }
            echo "\n ->Done";
        } catch (Exception $e) {
            echo "\n ERREUR => ".$e->getMessage()." \n ".$e->getTraceAsString();
        }
        echo "\n fin de lancement du script de mise à jour des alerte de delai de publication  ".date('Y-m-d H:i:s');
        echo "\n\t____________________________________\n";
    }
}
