<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de suppression des téléchargements en trop.
 *
 * @author adil el kanabi
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_DeleteTelechargements extends Atexo_Cli
{
    public static function run()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $organismes = CommonOrganismePeer::doSelect($c, $connexion);

        if (is_array($organismes)) {
            foreach ($organismes as $organisme) {
                $log = '';
                $c1 = new Criteria();
                $c1->add(CommonTelechargementPeer::ORGANISME, $organisme->getAcronyme());
                $telechargements = CommonTelechargementPeer::doSelect($c1, $connexion);

                foreach ($telechargements as $oneTel) {
                    $telechargement[$oneTel->getOrganisme().'+'.$oneTel->getConsultationId().'+'.$oneTel->getDatetelechargement()][] = $oneTel->getId();
                }

                foreach ($telechargement as $key => $ids) {
                    if (is_array($ids) && count($ids) > 1) {
                        $data = explode('+', $key);
                        $log .= '-- '.count($ids).' téléchargement ont été éffectués le '.Atexo_Util::iso2frnDateTime($data[2]).' sur la consultation '.$data[1]." pour cet organisme \r\n";
                        $log .= "\t|\r\n";
                        $log .= "\t|__ ids des téléchargements trouvés : \r\n";
                        foreach ($ids as $id) {
                            $log .= "\t\t|__ ".$id."\r\n";
                        }
                        $idNonSupprime = array_shift($ids);

                        $log .= "\t|\r\n";
                        $log .= "\t|__ id non supprimé : ".$idNonSupprime." \r\n";

                        $log .= "\t|\r\n";
                        $log .= "\t|__ ids supprimés : \r\n";

                        foreach ($ids as $id) {
                            echo $organisme->getAcronyme().'->'.$id."\r\n";
                            $c = new Criteria();
                            $c->add(CommonTelechargementPeer::ID, $id);
                            $c->add(CommonTelechargementPeer::ORGANISME, $organisme->getAcronyme());
                            CommonTelechargementPeer::doDelete($c, $connexion);
                            $log .= "\t\t|__ ".$id." .... Supprimé \r\n";
                        }

                        $log .= "\r\n";
                    }
                }

                if ($log) {
                    Atexo_Util::write_file('/tmp/log_suppession_telechargement_organisme_'.$organisme->getAcronyme().'.log', $log);
                }
            }
        }
    }
}
