<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Cron pour exporter une table.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @category Atexo
 */
class Cli_ExportTable extends Atexo_Cli
{
    public static function run()
    {
        $arguments = $_SERVER['argv'];
        //print_r($arguments); exit;
        if ((is_countable($arguments) ? count($arguments) : 0) < 4) {
            echo "Merci d'ajouter des params dans l'ordre : \r\n";
            echo "\t-> 1- le nom de la table \r\n";
            echo "\t-> 2- le chemin du repertoire de destination (/ a la fin) \r\n";
            echo "\t-> 3- nom du fichier de destination (si rien n'est mis alors le fiche se nomme 'nom de la table'_date('Y_m_d_h_i_s').csv \r\n";
            exit;
        }
        if (is_array($arguments)) {
            $table = $arguments['2'];
            $fileDestination = $arguments['3'];
            $nom_fichier = $arguments['4'];

            if (!is_dir($fileDestination)) {
                system('mkdir '.escapeshellarg($fileDestination));
                echo "\r\n-> répertoire suivant créé   ".$fileDestination.'....';
            }

            self::exporterDonneesTablesVersCsv($table, $fileDestination, $nom_fichier);
        }
    }

    public static function exporterDonneesTablesVersCsv($table, $fileDestination, $nom_fichier = '')
    {
        $entete = '';
        $separateur = '|';
        $content = '';

        $statment = Atexo_Db::getLinkCommon();

        $nameFile = $nom_fichier.'.csv';

        if ('' == $nom_fichier) {
            $nameFile = $table.'_'.date('Y_m_d_h_i_s').'.csv';
        }

        $sqlEntete = "SHOW COLUMNS FROM $table";
        $dataEntete = $statment->query($sqlEntete, PDO::FETCH_ASSOC);
        foreach ($dataEntete as $resEntete) {
            $entete .= $resEntete['Field'].$separateur;
            $champs[] = $resEntete['Field'];
        }
        $entete .= "\r\n";
        $content .= $entete;
        $sql = "SELECT * FROM $table";
        $data = $statment->query($sql, PDO::FETCH_ASSOC);
        foreach ($data as $res) {
            $ligne = '';
            foreach ($champs as $oneChamp) {
                $ligne .= self::enleverRetourAlaLigne($res[$oneChamp]).$separateur;
            }
            echo '.';
            $ligne .= "\r\n";
            $content .= $ligne;
        }
        Atexo_Util::write_file($fileDestination.$nameFile, $content, 'w');
        echo "\r\n ===> path file : ".$fileDestination.$nameFile."\r\n";
    }

    private static function enleverRetourAlaLigne($mot)
    {
        $mot = str_replace("\n", '', $mot);
        $mot = str_replace("\r\n", '', $mot);
        $mot = str_replace("\r", '', $mot);

        return $mot;
    }
}
