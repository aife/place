<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_Alertes;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Annonce;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de suivi des annonces au boamp.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Cli_SuiviBoamp extends Atexo_Cli
{
    public static $_connexionCom;
    public static ?string $_logFilePathBoampSuivi = null;
    public static ?string $_destinataireStatutPu = null;
    public static ?string $_destinataireStatutRg = null;
    public static ?string $_destinataireStatutRx = null;
    public static ?string $_destinataireStatutRe = null;
    public static ?string $_annonceACorriger = null;

    public static function run()
    {
        self::$_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::$_logFilePathBoampSuivi = Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP_SUIVI');
        self::$_destinataireStatutPu = Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU');
        self::$_destinataireStatutRg = Atexo_Config::getParameter('DESTINATAIRE_STATUT_RG');
        self::$_destinataireStatutRx = Atexo_Config::getParameter('DESTINATAIRE_STATUT_RX');
        self::$_destinataireStatutRe = Atexo_Config::getParameter('DESTINATAIRE_STATUT_RE');
        self::$_annonceACorriger = Atexo_Config::getParameter('ANNONCE_A_CORRIGER');
        self::doSuiviAnnonce();
    }

    /**
     * //envoi des notifications aux agents abonnés à l'alerte publication boamp.
       // Retirer la ligne "date limite de remise de plis" dans le courriel de l'alerte dans les cas suivants :
       //** JOUE_03 - Avis d'attribution
       //** BOAMP4 - Avis d'attribution
       //** BOAMP7 - Résultat de MAPA
       //** JOUE_05 - Secteurs Spéciaux
       //** JOUE_06 - Avis d'attribution - Secteurs Spéciaux
     */
    public static function doSuiviAnnonce()
    {
        $entitys = (new Atexo_Organismes())->retrieveAllEntity();
        foreach ($entitys as $oneEntity) {
            $entityAcronyme = $oneEntity->getAcronyme();
            $comptesBoamp = (new Atexo_Publicite_Boamp())->retrieveAllAcheteurPublic($entityAcronyme);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            foreach ($comptesBoamp as $oneCompte) {
                try {
                    $suiviXml = self::webGetBoampSuiviInfo($oneCompte);
                    if ('' != $suiviXml) {
                        $xmlObject = simplexml_load_string($suiviXml);
                        foreach ($xmlObject as $oneObect) {
                            $idAnnEmmetteur = (string) $oneObect['ID_ANN'];
                            //$idAnnonceEmetteur a la forme $id_boamp table annonce dans base organisme "-" ref de la consultation "-" reference utilisateur;
                            //Attention : lors de l'envoi au BOAMP on a limite a 30 caracteres l'idAnnonce car sinon refus du BOAMP
                            $idAnnonce = explode('-', $idAnnEmmetteur);
                            $etatAnnonce = (string) $oneObect['ETAT'];
                            $idJo = (string) $oneObect['ID_JO'];
                            //$nameFile est unique et est reference dans la base commune et organisme
                            $nameFile = (string) $oneObect['FICHIER'];
                            $lienPdf = (string) $oneObect->INFOS_PUB['URL_PDF'];
                            $lienBoamp = (string) $oneObect->INFOS_PUB['URL'];
                            $datePub = (string) $oneObect->INFOS_PUB['DATE_PUB'];
                            $parType = (string) $oneObect->INFOS_PUB['PAR_TYPE'];
                            $parution = (string) $oneObect->INFOS_PUB['PARUTION'];
                            $numAnnpar = (string) $oneObect->INFOS_PUB['NUM_ANN_PAR'];
                            $infoRejet = (string) $oneObect->INFOS_REJET;
                            //traitement des erreurs
                            $infoErreurs = '';
                            $xmlErreurs = $oneObect->INFOS_REGLES;
                            foreach ($xmlErreurs as $oneErreur) {
                                $infoErreurs .= (string) $oneErreur->ERRNUM.'-'.(string) $oneErreur->ERRTEXT."\n";
                                unset($oneErreur);
                            }
                            unset($xmlErreurs);
                            $annonce = (new Atexo_Publicite_Destinataire())->retrieveAnnonceByFileName($nameFile, $entityAcronyme);
                            $commonAnnonce = (new Atexo_Publicite_Annonce())->retreiveCommonAnnoncebyFileName($nameFile, $entityAcronyme);
                            if ($annonce) {
                                if ($etatAnnonce != $annonce->getStatutDestinataire()) {
                                    $annonce->setStatutDestinataire($etatAnnonce);
                                    $annonce->setLienPdf($lienPdf);
                                    $annonce->setLienBoamp($lienBoamp);
                                    $annonce->setDatepub($datePub);
                                    $annonce->setIdJo($idJo);
                                    $annonce->setParution($parution);
                                    $annonce->setNumAnn($numAnnpar);
                                    $annonce->setTypeBoamp($parType);
                                    $annonce->setDateMaj(date('Y-m-d H:i:s'));
                                    if ('' != $infoErreurs) {
                                        $annonce->setAnnError($infoErreurs);
                                    } else {
                                        $annonce->setAnnError($infoRejet);
                                    }//enregistrement de la date de publication dans la table annonce de la base commune
                                    if ($commonAnnonce) {
                                        $commonAnnonce->setDatepub($datePub);
                                        $commonAnnonce->save(self::$_connexionCom);
                                    }
                                    $annonce->save($connexionCom);
                                    // calcul et enregistrement de la date de mise en ligne de la consultation lors de la publication
                                    if ($etatAnnonce == self::$_destinataireStatutPu) {
                                        $CommonConsultation = CommonConsultationPeer::retrieveByPK($annonce->getConsultationId(), self::$_connexionCom);
                                        $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne($CommonConsultation);
                                        $CommonConsultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
                                        $CommonConsultation->save(self::$_connexionCom);
                                        $withDateFin = true;
                                        if (in_array($annonce->getIdTypeXml(), explode('#', Atexo_Config::getParameter('IDENTIFIANTS_AVIS_ATTRIBUTION_BOAMP')))) {
                                            $withDateFin = false;
                                        }
                                        (new Atexo_Agent_Alertes())->sendAlertePublication('BOAMP', $CommonConsultation, $withDateFin);
                                    }
                                    if (($etatAnnonce == self::$_destinataireStatutRg)
                                     || ($etatAnnonce == self::$_destinataireStatutRx)
                                     || ($etatAnnonce == self::$_destinataireStatutRe)) {
                                        $formXml = CommonReferentielFormXmlPeer::retrieveByPK($annonce->getIdFormXml(), Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
                                        if ($formXml) {
                                            $formXml->setStatut(self::$_annonceACorriger);
                                            $formXml->save($connexionCom);
                                        }
                                        //envoi des notifications aux agents abonnés à l'alerte echec publication boamp
                                        $CommonConsultation = CommonConsultationPeer::retrieveByPK($annonce->getConsultationId(), self::$_connexionCom);
                                        (new Atexo_Agent_Alertes())->sendAlerteEchecPublication('BOAMP', $CommonConsultation);
                                    }
                                }
                            }
                            unset($oneObect);
                            unset($idAnnEmmetteur);
                            unset($idAnnonce);
                            unset($etatAnnonce);
                            unset($idJo);
                            unset($nameFile);
                            unset($lienPdf);
                            unset($lienBoamp);
                            unset($datePub);
                            unset($parType);
                            unset($parution);
                            unset($numAnnpar);
                            unset($infoRejet);
                            unset($infoErreurs);
                            unset($xmlErreurs);
                            unset($annonce);
                            unset($commonAnnonce);
                            unset($dateMiseEnligneCalcule);
                            unset($CommonConsultation);
                            unset($formXml);
                        }
                        unset($xmlObject);
                    }
                    unset($oneCompte);
                    unset($suiviXml);
                } catch (Exception $e) {
                    Prado::log('Flux XML BOAMP attendu incorrect '.$e, TLogger::ERROR, 'Atexo.Cli.SuiviBoamp');
                }
            }
            unset($oneEntity);
            unset($comptesBoamp);
            unset($entityAcronyme);
        }
        unset($entitys);
        exit;
    }

    /**
     * téléchargement du suivi.
     */
    public static function webGetBoampSuiviInfo($compteBoamp)
    {
        $log = null;
        $url = (new Atexo_Publicite_Boamp())->getBoampSuiviUrl($compteBoamp).'?type=complet';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $compteBoamp->getBoampLogin().':'.$compteBoamp->getBoampPassword());

        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $resp = curl_exec($ch);

        if ('<HTML' == substr($resp, 0, 5)) {
            return '';
        }

        $resp = str_replace('&lt;', '<', $resp);
        $resp = str_replace('&gt;', '>', $resp);
        $resp = str_replace(']]>', ' ', $resp);
        $resp = str_replace('<![CDATA[', ' ', $resp);

        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\nTraitement du compte BOAMP ".$compteBoamp->getBoampLogin().' '.$compteBoamp->getBoampPassword().'<br/>';
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= $resp;
        $filePath = self::$_logFilePathBoampSuivi.'RetourBoamp_'.$compteBoamp->getBoampLogin().date('YmdHisu').'.log';
        Atexo_Util::writeLogFile(self::$_logFilePathBoampSuivi, $filePath, $log);
        unset($log);
        unset($filePath);
        unset($url);
        unset($ch);

        return $resp;
    }
}
