<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;

require_once __DIR__.'/MPERequirements.php';
class Cli_CheckAndInitConfig extends Atexo_Cli
{
    public static function run()
    {
        $mpeRequirements = new MPERequirements();

        $fixedRequirements = $mpeRequirements->getFixedRequirements();
        $fixedRecommendations = $mpeRequirements->getFixedRecommendations();
        $majorProblems = $mpeRequirements->getFailedRequirements();
        $minorProblems = $mpeRequirements->getFailedRecommendations();

        if (count($majorProblems)) {
            self::display('Problèmes majeurs KO');
            self::display('Des problèmes majeurs ont été détectés et *doivent* être fixé avant de poursuivre :');
            foreach ($majorProblems as $problem) {
                self::displayError($problem->getHelpHtml());
            }
        }

        /* GCH : Mis en stand by
        if (count($minorProblems)):
            self::display('Recommendations');
            $message =(count($majorProblems))?'En outre, pour':'Pour';
            $message .= ' améliorer votre expérience MPE, il est recommandé que vous corrigiez les points suivants:';
            self::display($message);
            foreach ($minorProblems as $problem):
                self::display($problem->getHelpHtml());
            endforeach;
        endif;

        if ($mpeRequirements->hasPhpIniConfigIssue()):
            if ($mpeRequirements->getPhpIniConfigPath()):
                self::display('Les modifications dans le fichier *php.ini* doivent se faire ici "'.$mpeRequirements->getPhpIniConfigPath().'".');
            else:
                self::display('To change settings, create a "php.ini".');
            endif;
        endif;
        */

        if (!count($majorProblems) && !count($minorProblems)) {
            self::display('Votre configuration semble bonne pour exécuter MPE.');
        }
    }
}
