<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SearchLucene;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Réindexe les consultations dans Lucene.
 *
 * @category Atexo
 */
class Cli_Lucene extends Atexo_Cli
{
    /**
     * Force une réindexation de toutes les consultations.
     */
    public static bool $full = false;

    /**
     * Efface toute la base Lucene avant de réindexer.
     */
    public static bool $clean = false;

    /**
     * Lance la réindexation des consultations dans le moteur Lucene
     * Paramètres en CLI :
     *    -c   : Clean - Efface toute la base Lucene avant réindexation (false par defaut)
     *    -f   : Full  - Indexe toutes les consultations et pas seulements les récentes (false par defaut)
     *    -r   : Reference : Indexe uniquement une référence (format : -r "REF:ORGA".
     */
    public static function run()
    {
        Prado::log("Suppression de l'index Lucene ".Atexo_Config::getParameter('DIR_INDEX_LUCENE'), TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
        $reference = null;
        // gestion des paramètres passés au script
        try {
            $options = new \Zend_Console_Getopt('-fcr:');
            if (isset($options->f)) {
                echo "option 'f' OK".PHP_EOL;
                self::$full = true;
            }
            if (isset($options->c)) {
                echo "option 'c' OK".PHP_EOL;
                self::$clean = true;
            }
            if (isset($options->r)) {
                echo "option 'r' OK".PHP_EOL;
                if (strpos($options->r, ':') > 0) {
                    $split = explode(':', $options->r);
                    $reference = $split[0];
                // $organisme = $split[1];
                } else {
                    self::displayError('Le format pour le param -r doit être de la forme : REFERENCE:ORGANISME');
                }
            }
        } catch (\Zend_Console_Getopt_Exception $e) {
            self::displayError($e->getMessage().PHP_EOL.$e->getUsageMessage());
        }

        /*$options = getopt('-fcr:');
        if (isset($options['f'])){
             self::$full = true;
        }
        if (isset($options['c'])){
            self::$clean = true;
        }
        if (isset($options['r'])) {
            if(strpos($options['r'], ':') > 0) {
                $split = split(':', $options['r']);
                $reference = $split[0];
               // $organisme = $split[1];
            } else {
                self::displayError("Le format pour le param -r doit être de la forme : REFERENCE:ORGANISME");
            }
        }*/

        // Suppression initiale de la base Lucene avant la réindexation
        if (true === self::$clean) {
            Prado::log("Suppression de l'index Lucene ".Atexo_Config::getParameter('DIR_INDEX_LUCENE'), TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
            if ($handle = opendir(Atexo_Config::getParameter('DIR_INDEX_LUCENE'))) {
                while (false !== ($file = readdir($handle))) {
                    if ('.' != $file && '..' != $file) {
                        unlink(Atexo_Config::getParameter('DIR_INDEX_LUCENE').DIRECTORY_SEPARATOR.$file);
                    }
                }
                closedir($handle);
            }
            if (is_dir(Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL'))) {
                if ($handle = opendir(Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL'))) {
                    while (false !== ($file = readdir($handle))) {
                        if ('.' != $file && '..' != $file) {
                            unlink(Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL').DIRECTORY_SEPARATOR.$file);
                        }
                    }
                    closedir($handle);
                }
            } else {
                mkdir(Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL'));
                $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
                shell_exec("chown -R {$user['name']} ".Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL'));
            }
            $organismes = (new Atexo_Organismes())->retrieveAllEntity();
            if (is_array($organismes)) {
                foreach ($organismes as $oneOrg) {
                    if (!is_dir(Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme())) {
                        mkdir(Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme());
                        mkdir(Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme().'/files');
                        mkdir(Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme().'/images');
                        $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
                        shell_exec("chown -R {$user['name']} ".Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme());
                    }
                    $pathIndexOrg = Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme().'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_ARCHIVE');
                    if (is_dir($pathIndexOrg)) {
                        if ($handle = opendir($pathIndexOrg)) {
                            while (false !== ($file = readdir($handle))) {
                                if ('.' != $file && '..' != $file) {
                                    unlink($pathIndexOrg.DIRECTORY_SEPARATOR.$file);
                                }
                            }
                            closedir($handle);
                        }
                    } else {
                        mkdir($pathIndexOrg);
                        $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
                        shell_exec("chown -R {$user['name']} ".$pathIndexOrg);
                    }
                    $pathIndexOrgAgent = Atexo_Config::getParameter('BASE_ROOT_DIR').$oneOrg->getAcronyme().'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_AGENT');
                    if (is_dir($pathIndexOrgAgent)) {
                        if ($handle = opendir($pathIndexOrgAgent)) {
                            while (false !== ($file = readdir($handle))) {
                                if ('.' != $file && '..' != $file) {
                                    unlink($pathIndexOrgAgent.DIRECTORY_SEPARATOR.$file);
                                }
                            }
                            closedir($handle);
                        }
                    } else {
                        mkdir($pathIndexOrgAgent);
                        $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
                        shell_exec("chown -R {$user['name']} ".$pathIndexOrgAgent);
                    }
                }
            }
        }
        self::generateLuceneAgent();
        self::generateLucenePortail();
        self::generateLuceneArchive();
    }

    public static function generateLucenePortail()
    {
        // indexation unique ou Full reindexation ou seulement les consultations récentes
        Prado::log('Début indexation totale Lucene (Portail)', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
        $c = new Criteria();
        $cton1 = $c->getNewCriterion(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE, null, Criteria::ISNULL);
        $cton2 = $c->getNewCriterion(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE, '0000-00-00 00:00:00', Criteria::LIKE);
        $cton3 = $c->getNewCriterion(CommonConsultationPeer::DATE_MISE_EN_LIGNE_CALCULE, date('Y-m-d').' 00:00', Criteria::LESS_THAN);
        $cton1->addOr($cton2);
        $cton1->addOr($cton3);

        // add to Criteria
        $c->add($cton1);

        $c->add(CommonConsultationPeer::DATEFIN, date('Y-m-d').' 00:00', Criteria::GREATER_THAN);
        Prado::log('Début indexation Lucene ( > 2008-11-01 seront indexées) ', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');

        // On force le tri en descendant pour etre sur que les consultations recentes soient indexes en premier
        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);

        $commonConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultations = CommonConsultationPeer::doSelect($c, $commonConnection);
        Prado::log(count($consultations).' consultations à indexer', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
        $pathIndex = Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL');
        self::generateIndex($consultations, $pathIndex);
    }

    public static function generateLuceneArchive()
    {
        // indexation unique ou Full reindexation ou seulement les consultations récentes
        Prado::log('Début indexation totale Lucene (Archive)', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
        $organismes = (new Atexo_Organismes())->retrieveAllEntity();
        if (is_array($organismes)) {
            foreach ($organismes as $oneOrg) {
                $organisme = $oneOrg->getAcronyme();
                $c = new Criteria();
                $c->add(CommonConsultationPeer::ID_ETAT_CONSULTATION, Atexo_Config::getParameter('STATUS_DECISION'), Criteria::GREATER_EQUAL);
                $c->add(CommonConsultationPeer::ORGANISME, $organisme, Criteria::EQUAL);
                Prado::log('Début indexation totale Lucene ', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');

                Prado::log('Début indexation Lucene ( > 2008-11-01 seront indexées) ', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');

                // On force le tri en descendant pour etre sur que les consultations recentes soient indexes en premier
                $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);

                $comConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $consultations = CommonConsultationPeer::doSelect($c, $comConnection);
                Prado::log(count($consultations).' consultations à indexer', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
                $pathIndex = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_ARCHIVE');
                echo "$pathIndex \r\n";
                self::generateIndex($consultations, $pathIndex, $organisme);
            }
        }
    }

    public static function generateLuceneAgent()
    {
        // indexation unique ou Full reindexation ou seulement les consultations récentes
        $organismes = (new Atexo_Organismes())->retrieveAllEntity();
        Prado::log('Début indexation totale Lucene (Agent)', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
        if (is_array($organismes)) {
            foreach ($organismes as $oneOrg) {
                $organisme = $oneOrg->getAcronyme();
                $c = new Criteria();
                $c->add(CommonConsultationPeer::ID_ETAT_CONSULTATION, Atexo_Config::getParameter('STATUS_DECISION'), Criteria::LESS_EQUAL);
                $c->add(CommonConsultationPeer::ORGANISME, $organisme, Criteria::EQUAL);
                // On force le tri en descendant pour etre sur que les consultations recentes soient indexes en premier
                $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);

                $comConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $consultations = CommonConsultationPeer::doSelect($c, $comConnection);
                Prado::log(" $organisme : ".count($consultations).' consultations à indexer', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
                echo " $organisme : ".count($consultations)." consultations à indexer\r\n";
                $pathIndex = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_AGENT');
                self::generateIndex($consultations, $pathIndex, $organisme);
            }
        }
    }

    private static function getCommonLot($organisme, $consultationId, $connection)
    {
        $arrayLot = [];
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
        $commonCategorieLots = CommonCategorieLotPeer::doSelect($c, $connection);
        if (is_array($commonCategorieLots)) {
            foreach ($commonCategorieLots as $lot) {
                if ($lot instanceof CommonCategorieLot) {
                    $arrayLot[] = $lot;
                }
            }
        }

        return $arrayLot;
    }

    private static function generateIndex($consultations, $pathIndex, $organisme = null)
    {
        $searchLucene = new Atexo_Consultation_SearchLucene($pathIndex);
        $commonConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (is_array($consultations)) {
            foreach ($consultations as $oneCons) {
                if ($oneCons instanceof CommonConsultation) {
                    $consultation = CommonConsultationPeer::retrieveByPK($oneCons->getId(), $commonConnection);
                } else {
                    $consultation = $oneCons;
                }
                if ($consultation instanceof CommonConsultation) {
                    $arrayComlots = self::getCommonLot($consultation->getOrganisme(), $consultation->getId(), $commonConnection);
                    if (!is_array($arrayComlots)) {
                        $arrayComlots = [];
                    }
                    // Si on a fait un clean, on a donc un index vide, on peut donc passer false en dernier param (deleteIfExists) pour gagner en perf
                    $searchLucene->addConsultation($consultation, $arrayComlots, '', (true === self::$clean ? false : true));
                } else {
                    echo "#$organisme#".$oneCons->getId()."# \r\n";
                }
            }

            // Optimize
            Prado::log('Optimize index Lucene ', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
            $searchLucene->optimizeIndex();
            $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
            shell_exec("chown -R {$user['name']} ".$pathIndex);
        }
        Prado::log('Fin indexation Lucene ('.(is_countable($consultations) ? count($consultations) : 0).' consultations) ', TLogger::INFO, 'Atexo.Cli.Cron.Lucene');
    }
}
