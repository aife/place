<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Mise à jour des denominations adapte.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2015
 */
class Cli_MajDenominationAdapte extends Atexo_Cli
{
    public static function run()
    {
        $log = null;
        $ex = null;
        $log .= ' Début de mise à jour des denomination adapte '.date('Y-m-d H:i:s');
        try {
            $consultations = CommonConsultationQuery::create()->findByDenominationAdapte(null);
            if ($consultations) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                foreach ($consultations as $consultation) {
                    if ($consultation instanceof CommonConsultation) {
                        $log .= "\n Mise à jour de la consultation dans la reference est => ".$consultation->getId();
                        $denominationOrg = (new Atexo_Consultation())->getOrganismeDenominationByService($consultation->getOrganisme(), $consultation->getServiceId());
                        $consultation->setOrgDenomination($denominationOrg);
                        $log .= "\n Dénomination adapte => ".strtoupper(Atexo_Util::formatterChaine($denominationOrg));
                        $consultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($denominationOrg)));
                        $consultation->save($connexion);
                    }
                }
            }
        } catch (Exception $e) {
            $log .= "\n Error : ".$ex->getMessage().'   '.$e->getTraceAsString();
        }
        $log .= "\n Fin  de mise à jour des denomination adapte ".date('Y-m-d H:i:s');
        $log .= "\n ->Done";
        $log .= "\n\t____________________________________\n";
        echo $log;
        $filePath = 'majDenominationAdapte_'.date('Ymd').'.log';
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'logs/'.$filePath, $log, 'a');
    }
}
