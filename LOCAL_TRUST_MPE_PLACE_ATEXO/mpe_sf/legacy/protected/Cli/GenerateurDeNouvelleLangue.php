<?php

namespace Application\Cli;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonLanguePeer;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Exception;

/**
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * Génère une nouvelle langue dans la plateforme.
 * Crée dans la base de données les champs correspondants à la nouvelle langue.
 *
 * @category Atexo
 */
class Cli_GenerateurDeNouvelleLangue extends Atexo_Cli
{
    public static function run()
    {
        echo "Veuillez saisir le signe de la langue ('fr' pour français, 'en' pour l'anglais, 'it' pour l'italien, ...):  ";
        $langue = trim(fgets(STDIN));

        if ($langue) {
            //Si la langue existe deja en base, dans la table langue, on ne fait rien
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $critere = new Criteria();
            $critere->Add(CommonLanguePeer::LANGUE, $langue);
            if (CommonLanguePeer::doSelectOne($critere, $connexionCom)) {
                echo "\n\t            $langue existe deja en Base dans la table langue           \r\n";

                return;
            }

            $scriptCOMMON = self::getScriptAjoutLangue();

            //Execution des requetes de la base Commune
            echo "\n MISE A JOUR DE LA BD COMMUNE : ";
            $log = '';
            try {
                foreach ($scriptCOMMON as $query) {
                    if (Atexo_Db::getLinkCommon()->query($query)) {
                        echo '.';
                    } else {
                        $message = " BD COMMUNE : ERROR when executing query : $query ";
                        $log .= $message."\r\n";
                    }
                }

                Propel::log($log, Propel::LOG_ERR);
                echo "\n\n";
            } catch (Exception $ex) {
                Propel::log($log, Propel::LOG_ERR);
                Propel::log($ex->getMessage(), Propel::LOG_ERR);
            }
        }
    }

    /**
     * Construit un tableau de script pour l'ajout d'une nouvelle langue dans la plateforme.
     */
    public static function getScriptAjoutLangue()
    {
        $langue = null;
        $scriptCOMMON = [];
        //Ajout de l'italien dans les langues
        $scriptCOMMON[] = "INSERT INTO `Langue` (`id_langue` ,`langue` ,`active` ,`defaut` ,`theme_specifique` ,`obligatoire_pour_publication_consultation`)VALUES (NULL , '$langue', '0', '0', '0', '0');";

        //Saisie des scripts : Base de données commune
        //Table Agrement
        $scriptCOMMON[] = " ALTER TABLE `Agrement` ADD `libelle_$langue` VARCHAR( 255 ) NULL ;";
        //Table Qualification
        $scriptCOMMON[] = " ALTER TABLE `Qualification` ADD `libelle_$langue` VARCHAR( 255 ) NULL;";
        //Table organisme
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `denomination_org_$langue` VARCHAR( 100 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `description_org_$langue` MEDIUMTEXT NOT NULL  default '';";
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `adresse_$langue` VARCHAR( 100 ) NOT NULL  default '';";
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `ville_$langue` VARCHAR( 100 ) NOT NULL  default '';";
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `adresse2_$langue` VARCHAR( 100 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `Organisme` ADD `pays_$langue` VARCHAR( 150 ) NOT NULL default '' ;";
        self::getScriptAjoutLangueGeoLocation($scriptCOMMON);
        self::getScriptAjoutLangueConsultationLot($scriptCOMMON);
        //Table SousCategorie
        $scriptCOMMON[] = " ALTER TABLE `SousCategorie` ADD `libelle_$langue` varchar(250) default NULL ;";
        //Table Societes_Exclues
        $scriptCOMMON[] = " ALTER TABLE `Societes_Exclues` ADD `raison_sociale_$langue` VARCHAR( 255 ) NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `Societes_Exclues` ADD `motif_$langue` VARCHAR( 255 ) NULL ;";
        //Table TypeAvis
        $scriptCOMMON[] = " ALTER TABLE `TypeAvis` ADD `intitule_avis_$langue` VARCHAR( 100 ) NULL ;";
        //Table TypeProcedure
        $scriptCOMMON[] = " ALTER TABLE `TypeProcedure` ADD `libelle_type_procedure_$langue` VARCHAR( 100 ) NULL ;";
        //Table ValeurReferentiel
        $scriptCOMMON[] = " ALTER TABLE `ValeurReferentiel` ADD `libelle_valeur_referentiel_$langue` TEXT  NOT NULL DEFAULT '';";
        //Table Faq_Entreprise
        $scriptCOMMON[] = " ALTER TABLE `Faq_Entreprise` ADD `question_$langue` text NOT NULL DEFAULT '' ;";
        $scriptCOMMON[] = " ALTER TABLE `Faq_Entreprise` ADD `reponse_$langue` text NOT NULL DEFAULT '' ;";
        //table Justificatifs
        $scriptCOMMON[] = " ALTER TABLE `Justificatifs` ADD `nom_$langue` VARCHAR( 100 ) NULL AFTER `nom` ; ";
        //table DocumentsAttaches
        $scriptCOMMON[] = " ALTER TABLE `DocumentsAttaches` ADD `nom_document_$langue` VARCHAR( 100 ) NOT NULL ; ";
        //table formejuridique
        $scriptCOMMON[] = " ALTER TABLE `formejuridique` ADD `libelle_formejuridique_$langue` VARCHAR( 255 ) NOT NULL ; ";
        //table Service_Mertier_Profils
        $scriptCOMMON[] = " ALTER TABLE `Service_Mertier_Profils` ADD `libelle_$langue` VARCHAR( 255 ) NULL ; ";
        //table 'CategorieINSEE'
        $scriptCOMMON[] = " ALTER TABLE `CategorieINSEE` ADD `libelle_$langue` VARCHAR( 100 ) NOT NULL ;";
        //Table Service
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `libelle_$langue` TEXT NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `adresse_$langue` VARCHAR( 100 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `adresse_suite_$langue` VARCHAR( 100 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `ville_$langue` VARCHAR( 100 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `pays_$langue` VARCHAR( 150 ) NOT NULL DEFAULT 'NULL';";
        $scriptCOMMON[] = " ALTER TABLE `Service` ADD `chemin_complet_$langue` VARCHAR( 255 ) NULL DEFAULT NULL ;";
        //Table TypeProcedureOrganisme
        $scriptCOMMON[] = " ALTER TABLE `TypeProcedureOrganisme` ADD `libelle_type_procedure_$langue` VARCHAR( 100 ) NULL ;";
        //Table visite_lieux
        $scriptCOMMON[] = " ALTER TABLE `visite_lieux` ADD `adresse_$langue` VARCHAR( 255 ) NULL ;";
        //table ReferentielTypeXml
        $scriptCOMMON[] = " ALTER TABLE `ReferentielTypeXml` ADD `libelle_type_$langue` VARCHAR( 100 ) NOT NULL AFTER `libelle_type_fr` ; ";
        //table 'EchangeTypeAR'
        $scriptCOMMON[] = " ALTER TABLE `EchangeTypeAR` ADD `libelle_$langue` VARCHAR( 100 ) NOT NULL ; ";
        //table 'EchangeFormat'
        $scriptCOMMON[] = " ALTER TABLE `EchangeFormat` ADD `libelle_$langue` VARCHAR( 100 ) NOT NULL ; ";
        //table 'DecisionPassationMarcheAVenir'
        $scriptCOMMON[] = " ALTER TABLE `DecisionPassationMarcheAVenir` ADD `libelle_$langue` VARCHAR( 100 ) NULL ; ";
        //table 'DecisionPassationConsultation'
        $scriptCOMMON[] = " ALTER TABLE `DecisionPassationConsultation` ADD `libelle_$langue` VARCHAR( 100 ) NULL ; ";
        //table 'AvisCao'
        $scriptCOMMON[] = " ALTER TABLE `AvisCao` ADD `libelle_$langue` VARCHAR( 100 ) NULL ; ";
        //table 'TypeAvenant'
        $scriptCOMMON[] = " ALTER TABLE `TypeAvenant` ADD `libelle_$langue` VARCHAR( 100 ) NULL ; ";
        //table 'nature_acte_juridique'
        $scriptCOMMON[] = " ALTER TABLE `nature_acte_juridique` ADD `libelle_$langue` VARCHAR( 50 ) NULL ; ";
        //table 'mode_execution_contrat'
        $scriptCOMMON[] = " ALTER TABLE `mode_execution_contrat` ADD `libelle_$langue` VARCHAR( 255 ) NULL ; ";
        //table 'ValeurReferentiel'
        $scriptCOMMON[] = " ALTER TABLE `ValeurReferentielOrg` ADD `libelle_valeur_referentiel_$langue` TEXT  NOT NULL DEFAULT '' ; ";
        //Table gestion_adresses
        $scriptCOMMON[] = " ALTER TABLE `gestion_adresses` ADD `adresse_depot_offres_$langue` varchar(250) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `gestion_adresses` ADD `adresse_retrais_dossiers_$langue` varchar(250) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `gestion_adresses` ADD `lieu_ouverture_plis_$langue` varchar(250) NOT NULL default '' ;";

        return $scriptCOMMON;
    }

    /**
     * Construit un tableau de script pour l'ajout d'une nouvelle langue dans les géo-localisations.
     *
     * @param array $scriptCOMMON: tableau des scripts passé par référence
     */
    public static function getScriptAjoutLangueGeoLocation(&$scriptCOMMON)
    {
        $langue = null;
        //Table Geolocalisation N0
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN0` ADD `denomination_$langue` VARCHAR( 40 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN0` ADD `libelle_selectionner_$langue` VARCHAR( 40 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN0` ADD `libelle_tous_$langue` VARCHAR( 40 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN0` ADD `libelle_Aucun_$langue` VARCHAR( 40 ) NOT NULL default '';";
        //Table Geolocalisation N1
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN1` ADD `denomination1_$langue` VARCHAR( 100 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN1` ADD `denomination2_$langue` VARCHAR( 100 ) NOT NULL default '';";
        //Table Geolocalisation N2
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN2` ADD `denomination1_$langue` VARCHAR( 50 ) NOT NULL default '';";
        $scriptCOMMON[] = " ALTER TABLE `GeolocalisationN2` ADD `denomination2_$langue` VARCHAR( 30 ) NOT NULL default '';";
    }

    /**
     * Construit un tableau de script pour l'ajout d'une nouvelle langue pour les tables "CategorieLot", "consultation" et "CategorieConsultation".
     *
     * @param array $scriptCOMMON: tableau des scripts passé par référence
     */
    public static function getScriptAjoutLangueConsultationLot(&$scriptCOMMON)
    {
        $langue = null;
        //Table CategorieLot
        $scriptCOMMON[] = " ALTER TABLE `CategorieLot` ADD `description_detail_$langue` LONGTEXT NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `CategorieLot` ADD `description_$langue` LONGTEXT NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `CategorieLot` ADD `add_echantillion_$langue` VARCHAR( 255 ) NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `CategorieLot` ADD `add_reunion_$langue` VARCHAR( 255 ) NULL ;";
        //Table CategorieConsultation
        $scriptCOMMON[] = " ALTER TABLE `CategorieConsultation` ADD `libelle_$langue` VARCHAR( 100 ) NULL ;";
        //Table consultation
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `intitule_$langue` LONGTEXT NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `objet_$langue` LONGTEXT NULL ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `accessibilite_$langue` ENUM( '0', '1' ) NOT NULL DEFAULT '0' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `adresse_depot_offres_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `lieu_ouverture_plis_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `adresse_retrais_dossiers_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `pieces_dossier_admin_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `pieces_dossier_tech_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `pieces_dossier_additif_$langue` VARCHAR( 255 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `detail_consultation_$langue` text ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `add_echantillion_$langue` VARCHAR( 250 ) NOT NULL default '' ;";
        $scriptCOMMON[] = " ALTER TABLE `consultation` ADD `add_reunion_$langue` VARCHAR( 250 ) NOT NULL default '' ;";
    }
}
