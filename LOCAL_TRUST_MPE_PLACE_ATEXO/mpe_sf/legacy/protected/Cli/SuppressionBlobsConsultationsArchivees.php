<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_SuppressionBlobsConsultationsArchivees;
use Prado\Prado;
use Prado\Util\TLogger;

class Cli_SuppressionBlobsConsultationsArchivees extends Atexo_Cli
{
    public static function run()
    {
        $_GET = [];

        Prado::log(
            'Début de la suppression des blobs sur consultations archivées ',
            TLogger::INFO,
            'Atexo.Cli.SuppressionBlobsConsultationsArchivees'
        );
        $suppBlobsConsultationsArchovees = new Atexo_SuppressionBlobsConsultationsArchivees();
        $arguments = $_SERVER['argv'];

        if (is_array($arguments)) {
            for ($i = 2; $i < $_SERVER['argc']; ++$i) {
                $argument = explode('=', $arguments[$i]);
                $_GET[$argument[0]] = $argument[1] ?: true;
            }
        }

        $suppBlobsConsultationsArchovees->run();
    }
}
