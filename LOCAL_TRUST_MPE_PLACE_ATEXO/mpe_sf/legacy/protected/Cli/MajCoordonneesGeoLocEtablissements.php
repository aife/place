<?php

namespace Application\Cli;

use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Exception;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Cron de mise a jour des coordonnees geographiques des etablissements.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-roadmap
 *
 * @copyright Atexo 2015
 */
class Cli_MajCoordonneesGeoLocEtablissements extends Atexo_Cli
{
    public static string $cacheGeocodingFile = '/logs/cacheGeocodingFile.bkp';

    public static function run()
    {
        try {
            self::loggerInfos("\n\n ===> Debut lancement du cron de mise a jour des coordonnees geolocalisation des etablissements <=== \n");
            //Recuperation de la liste des etablissements
            $etablissementQuery = new CommonTEtablissementQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $listeEtablissements = $etablissementQuery->recupererListeEtablissementsPourMajCoordonneesGeo();
            if ($listeEtablissements instanceof PropelObjectCollection && !empty($listeEtablissements)) {
                self::loggerInfos(count($listeEtablissements).' Etablissement(s) trouve(s)');
                foreach ($listeEtablissements as $etablissement) {
                    if ($etablissement instanceof CommonTEtablissement) {
                        self::loggerInfos('Etablissement id = '.$etablissement->getIdEtablissement());
                        //Recuperation des coordonnees geolocalisation de l'etablissement
                        $url = self::getUrl($etablissement->getVille(), $etablissement->getCodePostal(), $etablissement->getPays());
                        if ($etablissement->getVille() || $etablissement->getCodePostal() || $etablissement->getPays()) {
                            $jsonCoordonneesGeoEtab = self::recupererCoordonneesGeoViaNominatim($etablissement, $url);
                        } else {
                            self::loggerInfos("\tURL incorrect : ".$url);
                        }
                        //Parser le resultat
                        $coordonneesGeoEtab = self::parserResultat($jsonCoordonneesGeoEtab);
                        //Mise a jour de l'etablissement
                        if ($coordonneesGeoEtab['lat'] && $coordonneesGeoEtab['long']) {
                            self::majCoordonneesGeolocEtablissement($etablissement, $coordonneesGeoEtab);
                        }
                    }
                }
            }
            self::loggerInfos("\n\n ===> Fin lancement du cron de mise a jour des coordonnees geolocalisation des etablissements <=== \n");
        } catch (Exception $e) {
            self::loggerErreur('Error: '.$e->getMessage());
        }
    }

    /**
     * Permet de recuperer les coordonnees geolocalisation de l'etablissement.
     *
     * @param CommonTEtablissement $etablissement :  etablissement
     * @param string               $url           : url du ws
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function recupererCoordonneesGeoViaNominatim(CommonTEtablissement $etablissement, $url, $reload = false)
    {
        try {
            $fileCacheEncoding = realpath(Atexo_Config::getParameter('BASE_ROOT_DIR')).self::$cacheGeocodingFile;
            self::loggerInfos("\tFichier de cache : $fileCacheEncoding");
            $contentCacheGeocodingFile = null;
            if (file_exists($fileCacheEncoding)) {
                $contentCacheGeocodingFile = unserialize(file_get_contents($fileCacheEncoding));
            }
            $contentCacheGeocoding = is_array($contentCacheGeocodingFile) ? $contentCacheGeocodingFile : [];
            $index = base64_encode($url);
            if (empty($contentCacheGeocoding[$index])) {
                self::loggerInfos("\tUSE CACHE : FALSE");
                sleep(random_int(0.100, 0.800));
                $client = new Client();
                $client->setUri($url);
                $client->setMethod(Request::METHOD_GET);
                $resultat = $client->send();

                self::loggerInfos("\tURL ws NOMINATIM : ".$url);
                self::loggerInfos("\tRetour du ws NOMINATIM : ".$resultat);
                if (!self::retourWsValide($resultat)) {
                    $url = self::getUrl($etablissement->getVille(), null, $etablissement->getPays());
                    $indexReload = base64_encode($url);
                    if ($etablissement->getVille() && !$reload) {
                        $resultat = self::recupererCoordonneesGeoViaNominatim($etablissement, $url, true);
                        $contentCacheGeocoding[$indexReload] = $resultat;
                    } else {
                        self::loggerInfos("\tURL incorrect : ".$url);
                    }
                } else {
                    $contentCacheGeocoding[$index] = $resultat;
                }
            } else {
                self::loggerInfos("\tUSE CACHE : TRUE");
                $resultat = $contentCacheGeocoding[$index];
            }
            file_put_contents($fileCacheEncoding, serialize($contentCacheGeocoding));

            return $resultat;
        } catch (Exception $e) {
            self::loggerInfos("\tURL ws NOMINATIM : ".$url);
            self::loggerErreur("\t Erreur lors du retour du ws NOMINATIM [id etablissement= ".$etablissement->getIdEtablissement().'], [code postal= '.$etablissement->getCodePostal().'], [ville= '.$etablissement->getVille().'], [Erreur= '.$e->getMessage().']');
        }
    }

    /**
     * Permet de parser le retour du ws NOMINATIM pour recuperer la latitude et la longitude.
     *
     * @param string $jsonResultat : le retour du ws en encode en json
     *
     * @return array : tableau unidimensionnel contenant la latitude et la longitude
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function parserResultat($jsonResultat)
    {
        try {
            $resultat = json_decode($jsonResultat, null, 512, JSON_THROW_ON_ERROR);
            $coordonneesGeoloc = [];
            if (!empty($resultat) && is_object($resultat[0])) {
                $coordonneesGeoloc['lat'] = $resultat[0]->lat;
                $coordonneesGeoloc['long'] = $resultat[0]->lon;
                self::loggerInfos("\t Parse des donnees effectue avec succes ");
            }

            return $coordonneesGeoloc;
        } catch (Exception $e) {
            self::loggerErreur('Erreur lors du parse du json: '.$e->getMessage());
        }
    }

    /**
     * Permet de mettre a jour les champ "lat" et "long" de l'etablissement.
     *
     * @param CommonTEtablissement $etablissement      : objet etablissement
     * @param array                $coordonneesGeoEtab : tableau contenant les coordonnees geolocalisation de l'etablissement
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function majCoordonneesGeolocEtablissement(CommonTEtablissement $etablissement, array $coordonneesGeoEtab)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $etablissement->setLat($coordonneesGeoEtab['lat']);
            $etablissement->setLong($coordonneesGeoEtab['long']);
            $etablissement->save($connexion);
            self::loggerInfos("\t Etablissement mis a jour avec succes ");
        } catch (Exception $e) {
            self::loggerErreur("Erreur lors de la mise a jour de l'etablissement [id = ".$etablissement->getIdEtablissement().'] [Erreur : '.$e->getMessage().']');
        }
    }

    /**
     * Permet de logger les infos.
     *
     * @param string $infos
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function loggerInfos($infos)
    {
        echo $infos.PHP_EOL;
        $logger = Atexo_LoggerManager::getLogger('app');
        $logger->info($infos);
    }

    /**
     * Permet de logger les erreurs.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function loggerErreur($erreur)
    {
        echo $erreur.PHP_EOL;
        $logger = Atexo_LoggerManager::getLogger('app');
        $logger->error($erreur);
    }

    /**
     * Permet de construire l'url du ws.
     *
     * @param string|null $city       : ville
     * @param string|null $postalcode : code postal
     * @param string      $country    : pays
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function getUrl($city = null, $postalcode = null, $country = null)
    {
        $url = Atexo_Config::getParameter('GEOLOC_SEARCH_URL');
        $urlParam = [];
        if (null != $city) {
            $urlParam[] = 'city='.urlencode(preg_replace('#cedex#i', '', $city));
        }
        if (null != $country) {
            $urlParam[] = 'country='.urlencode($country);
        }
        if (null != $postalcode) {
            $urlParam[] = 'postalcode='.urlencode($postalcode);
        }
        $urlParam[] = 'limit=1';
        $urlParam[] = 'format=json';
        $url .= '?'.implode('&', $urlParam);

        return $url;
    }

    /**
     * Permet de valider le retour du ws.
     *
     * @param string $jsonResultat
     *
     * @return bool : true si valide, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function retourWsValide($jsonResultat)
    {
        try {
            $resultat = json_decode($jsonResultat, null, 512, JSON_THROW_ON_ERROR);
            if (!empty($resultat) && is_object($resultat[0]) && $resultat[0]->lat && $resultat[0]->lon) {
                self::loggerInfos("\t Validation du retour du ws: OK");

                return true;
            }
            self::loggerInfos("\t Validation du retour du ws: KO");

            return false;
        } catch (Exception $e) {
            self::loggerErreur('Erreur lors de la validation du retour du ws: '.$e->getMessage());
        }
    }
}
