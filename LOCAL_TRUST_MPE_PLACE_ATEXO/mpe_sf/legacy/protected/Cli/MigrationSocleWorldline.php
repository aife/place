<?php

namespace Application\Cli;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\InterfaceWL\Atexo_InterfaceWL_Entreprises;
use Application\Service\Atexo\InterfaceWL\Atexo_InterfaceWL_Organismes;
use Application\Service\Atexo\InterfaceWL\Atexo_InterfaceWL_Services;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Script de migration des donnees du socle pour Worldline.
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @since 4.6.4
 *
 * @version $Id: MigrationSocleWorldline.php,v 1.1.2.4 2015-01-26 00:10:09 gchevret Exp $
 *
 * @copyright Atexo 2014
 */
class Cli_MigrationSocleWorldline extends Atexo_Cli
{
    public static function log($message)
    {
        fwrite(STDOUT, date('Y-m-d H:i:s')."\t".self::convert(memory_get_usage(true))."\t".$message."\n");
        Prado::log($message, TLogger::INFO, 'Atexo.Cli.Cron.MigrationSocleWorldline');
    }

    public static function convert($size)
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

        return @round($size / 1024 ** ($i = floor(log($size, 1024))), 2).' '.$unit[$i];
    }

    public static function run()
    {
        // Debut du traitement
        self::log('Script de migration des donnees du socle pour Worldline');

        $arguments = $_SERVER['argv'];

        if (3 != (is_countable($arguments) ? count($arguments) : 0)) {
            $buffer = 'Nombre de parametres donne incorrect'."\n".'Usage :'."\n";
            $buffer .= 'Initialisation des champs "id_initial" : ./mpe/protected/bin/mpe MigrationSocleWorldline -i'."\n";
            $buffer .= 'Export des donnees socles : ./mpe/protected/bin/mpe MigrationSocleWorldline -o';
            self::log($buffer);
        } else {
            if ('-i' == $arguments[2]) {
                //Initialisation des champs "id_initial"
                self::runInitMigration();
            } elseif ('-o' == $arguments[2]) {
                //Export des donnes socle
                self::runExport();
            } else {
                $buffer = 'Parametre non reconnu'."\n".'Usage :'."\n";
                $buffer .= 'Initialisation des champs "id_initial" : ./mpe/protected/bin/mpe MigrationSocleWorldline -i'."\n";
                $buffer .= 'Export des donnees socles : ./mpe/protected/bin/mpe MigrationSocleWorldline -o';
                self::log($buffer);
            }
        }

        self::log('Execution du script terminee');
        exit;
    }

    public static function runInitMigration()
    {
        // Debut du traitement
        self::log('Debut du traitement : initialisation des champs "id_initial" pour la migration du socle');

//         try {

        $db = Atexo_Db::getLinkCommon(true);
        //Desactivation des contraintes de cles etrangeres
        $db->query('SET FOREIGN_KEY_CHECKS = 0');

        // Mise a jour des organismes
        self::log('Mise a jour des organismes');
        self::initOrganismesIds($db);
        self::log('Fin de la mise a jour des organismes');

        // Mise a jour des services
        self::log('Mise a jour des services');
        self::initServicesIds($db);
        self::log('Fin de la mise a jour des services');

        // Mise a jour des agents
        self::log('Mise a jour des agents');
        self::initAgentsIds($db);
        self::log('Fin de la mise a jour des agents');

        // Mise a jour des entreprises
        self::log('Mise a jour des entreprises');
        self::initEntreprisesIds($db);
        self::log('Fin de la mise a jour des entreprises');

        // Mise a jour des établissements
        self::log('Mise a jour des établissements');
        self::initEtablissementsIds($db);
        self::log('Fin de la mise a jour des établissements');

        // Mise a jour des inscrits
        self::log('Mise a jour des inscrits');
        self::initInscritsIds($db);
        self::log('Fin de la mise a jour des inscrits');

        //Desactivation des contraintes de cles etrangeres
        $db->query('SET FOREIGN_KEY_CHECKS = 1');

        //Liberation espace memoire
        unset($db);

        // Fin de traitement
        self::log('Fin du traitement : Ok ;)');

//         } catch(Exception $e){

//             self::log('Erreur :'.$e);
//             self::log('Fin du traitement : Ko x(');
//             exit;

//         }
    }

    public static function runExport()
    {
        // Debut du traitement
        self::log('Debut du traitement : Export des donnees socle pour Worldline');

//         try {

        //Initialisation : verification de l'existence du repertoire de sortie
        self::initRepertoireExport();

        $db = Atexo_Db::getLinkCommon(true);
        $report = [];
        $pathXmlsSoclePPP = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP');

        //Export des organismes
        self::exportOrganismes($db, $report, $pathXmlsSoclePPP);

        //Export des services
        self::exportServices($db, $report, $pathXmlsSoclePPP);

        //Export des agent
        self::exportAgents($db, $report, $pathXmlsSoclePPP);

        //Export des entreprises
        self::exportEntreprises($db, $report, $pathXmlsSoclePPP);

        //Export des établissements
        self::exportEtablissements($db, $report, $pathXmlsSoclePPP);

        //Export des inscrits entreprises
        self::exportInscrits($db, $report, $pathXmlsSoclePPP);

        //Generation du rapport d'extraction
        self::generateRapportExtractionCSV($report);

        //Liberation espace memoire
        unset($db);
        unset($report);

        // Fin de traitement
        self::log('Fin du traitement : Ok ;)');

//         } catch (Exception $e){

//             self::log('Erreur :'.$e);
//             self::log('Fin du traitement : Ko x(');
//             exit;

//         }
    }

    public static function getElements(&$db, &$array, $query)
    {
        //Execution de la requete
        $st = $db->prepare($query);
        $st->execute([]);

        self::log('Requete executee :'."\n".$query);

        //Recuperation des resultats de la requete
        $array = $st->fetchAll(PDO::FETCH_ASSOC);
        $st->closeCursor();

        //Liberation espace memoire
        unset($st);
    }

    public static function initOrganismesIds(&$db)
    {
        //Recuperation de l'ensemble des organismes
        $_s_query = 'SELECT `id` FROM `Organisme` WHERE id_initial = 0 ORDER BY `id` ASC';

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '1';
        $id = self::getNextIdInitial($db, 'Organisme', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_organisme = $_t_array[$i]['id'];

            //Calcul de l'identifiant
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                $socle_id = $type_id.$socle_id;

                //Requete de mise a jour de l'enregistrement
                $_u_query = 'UPDATE `Organisme` SET `id_initial` = '.$socle_id.' WHERE `id` = '.$id_organisme;
                $st = $db->query($_u_query);
                $st->closeCursor();
            }
        }

        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initServicesIds(&$db)
    {
        //Recuperation de l'ensemble des services
        //$_s_query = 'SELECT `id`,`organisme` FROM `Service` ORDER BY `organisme` ASC, `id` ASC';

        $_s_query = <<<EOF
        SELECT
	s.`id`,
	s.`organisme` ,
	affs.service_parent_id
FROM `Service` s
LEFT JOIN AffiliationService affs ON s.id=affs.service_id AND s.organisme=affs.organisme
WHERE id_initial = 0
ORDER BY s.`organisme` ASC, affs.service_parent_id ASC, s.`id` ASC
EOF;

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '5';
        $id = self::getNextIdInitial($db, 'Service', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_service = $_t_array[$i]['id'];
            $organisme = $_t_array[$i]['organisme'];

            //Calcul de l'identifiant
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                $socle_id = $type_id.$socle_id;

                //Requete de mise a jour de l'enregistrement
                $_u_query = 'UPDATE `Service` SET `id_initial` = '.$socle_id.' WHERE `id` = '.$id_service.' AND `organisme` = "'.$organisme.'"';
                $st = $db->query($_u_query);
                $st->closeCursor();

                self::log('Requete executee :'."\n".$_u_query);

                //Liberation espace memoire
                unset($st);
            }
        }

        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initAgentsIds(&$db)
    {
        //Recuperation de l'ensemble des agents
        $_s_query = <<<EOL
SELECT `Agent`.`id`, `Agent`.`organisme`, `Agent`.`service_id`
FROM `Agent`
WHERE id_initial = 0
ORDER BY `Agent`.`organisme` ASC, `Agent`.`id` ASC;
EOL;

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '3';
        $id = self::getNextIdInitial($db, 'Agent', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_agent = $_t_array[$i]['id'];
            $organisme = $_t_array[$i]['organisme'];

            //Calcul de l'identifiant socle
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                //Calcul de l'identifiant
                $socle_id = $type_id.$socle_id;

                //Mise a jour de l'enregistrement
                $_u_query = 'UPDATE `Agent` SET `id_initial` = '.$socle_id.' WHERE `id` = '.$id_agent.' AND `organisme` = "'.$organisme.'"';
                $st = $db->query($_u_query);
                $st->closeCursor();

                self::log('Requete executee :'."\n".$_u_query);

                //Liberation espace memoire
                unset($st);
            }
        }

        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initEntreprisesIds(&$db)
    {
        //Recuperation de l'ensemble des entreprises
        $_s_query = 'SELECT `id` FROM `Entreprise` WHERE id_initial = 0 ORDER BY `id` ASC';

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '2';
        $id = self::getNextIdInitial($db, 'Entreprise', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_entreprise = $_t_array[$i]['id'];

            //Calcul de l'identifiant socle
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                //Calcul de l'identifiant
                $socle_id = $type_id.$socle_id;

                //Requete de mise a jour de l'enregistrement
                $_u_query = 'UPDATE `Entreprise` SET `id_initial` = '.$socle_id.' WHERE `id` = '.$id_entreprise;
                $st = $db->query($_u_query);
                $st->closeCursor();

                self::log('Requete executee :'."\n".$_u_query);

                //Liberation espace memoire
                unset($st);
            }
        }
        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initEtablissementsIds(&$db)
    {
        //Recuperation de l'ensemble des entreprises
        $_s_query = 'SELECT `id_etablissement` FROM `t_etablissement` WHERE id_initial = 0 ORDER BY `id_etablissement` ASC';

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '6';
        $id = self::getNextIdInitial($db, 't_etablissement', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_etablissement = $_t_array[$i]['id_etablissement'];

            //Calcul de l'identifiant socle
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                //Calcul de l'identifiant
                $socle_id = $type_id.$socle_id;

                //Requete de mise a jour de l'enregistrement
                $_u_query = 'UPDATE `t_etablissement` SET `id_initial` = '.$socle_id.' WHERE `id_etablissement` = '.$id_etablissement;
                $st = $db->query($_u_query);
                $st->closeCursor();

                self::log('Requete executee :'."\n".$_u_query);

                //Liberation espace memoire
                unset($st);
            }
        }

        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initInscritsIds(&$db)
    {
        //Recuperation de l'ensemble des inscrits des entreprises
        $_s_query = <<<EOL
SELECT `Inscrit`.`id`, `Entreprise`.`id_initial`
FROM `Inscrit`
LEFT JOIN `Entreprise`
ON (`Inscrit`.`entreprise_id` = `Entreprise`.`id`)
WHERE `Inscrit`.id_initial = 0
ORDER BY `Inscrit`.`id` ASC;
EOL;

        //Recuperation des resultats de la requete
        $_t_array = [];
        self::getElements($db, $_t_array, $_s_query);
        $n = is_countable($_t_array) ? count($_t_array) : 0;

        //Initialisation des valeurs pour l'identification
        $type_id = '4';
        $id = self::getNextIdInitial($db, 'Inscrit', $type_id) + 1;

        for ($i = 0; $i < $n; ++$i) {
            //Recuperation de l'identifiant de l'enregistrement
            $id_inscrit = $_t_array[$i]['id'];
            $id_socle_entreprise = $_t_array[$i]['id_initial'];

            //Calcul de l'identifiant socle
            $socle_id = $id++;
            $socle_id = str_pad($socle_id, 8, '0', STR_PAD_LEFT);

            if (8 == strlen($socle_id)) {
                //Calcul de l'identifiant
                $socle_id = $type_id.$socle_id;

                if ($id_socle_entreprise) {
                    $_u_query = 'UPDATE `Inscrit` SET `id_initial` = '.$socle_id.' WHERE `id` = '.$id_inscrit;
                    $st = $db->query($_u_query);
                    $st->closeCursor();

                    self::log('Requete executee :'."\n".$_u_query);

                    //Liberation espace memoire
                    unset($st);
                }
            }
        }

        //Liberation espace memoire
        unset($_t_array);
    }

    public static function initRepertoireExport()
    {
        self::log('Initialisation : verification de l\'existence du repertoire XML');

        //Verification de l'existence du repertoire, creation le cas echeant
        $pathXmlsSoclePPP = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP');

        if (!is_dir($pathXmlsSoclePPP)) {
            try {
                system('mkdir '.escapeshellarg($pathXmlsSoclePPP));
                self::log('Initialisation : creation du repertoire XML');
                self::log('Le repertoire de sortie est : '.$pathXmlsSoclePPP);
            } catch (Exception $e) {
                self::log('Erreur : '.$e->getMessage());
                self::log('Fin du traitement : Ko x(');
                exit;
            }
        } else {
            self::log('Le repertoire de sortie est : '.$pathXmlsSoclePPP);
        }
    }

    public static function exportOrganismes(&$db, &$report, &$pathXmlsSoclePPP)
    {
        $contentXmlOrganims = null;
        self::log('Debut du traitement d\'export des organismes');

        //Recuperation des organismes
        $_t_elements = [];
        $_s_query = <<<EOL
SELECT `id_initial`, `acronyme`, `siren`, `complement`,
`type_article_org`, `denomination_org`, `categorie_insee`, `description_org`,
`adresse`, `cp`, `ville`, `adresse2`,
`email`, `url`, `active`
FROM `Organisme` ORDER BY `id` ASC;
EOL;
        self::getElements($db, $_t_elements, $_s_query);

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Organismes();
        $bufferXML = $interface->getXmlResponse($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'organisms.xml', $bufferXML);
            self::log('Le fichier XML des organisms a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'organisms.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'organisms.xml', $contentXmlOrganims);
                self::log('Le fichier XML des organisms a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'organisms.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_organisms_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'organisms_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => 'Entites publiques', '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des organismes');
    }

    public static function updateServicesParentIds(&$db, &$_t_elements)
    {
        self::log('Debut du traitement de mise a jour des affiliations services');

        $_t_id_initial = [];

        foreach ($_t_elements as $key => &$obj) {
            //Test de l'affiliation du service a un autre du meme organisme
            if ($obj['parentId']) {
                //Recuperation des valeurs d'interet
                $p_id = $obj['parentId'];
                $p_id_initial = $p_id;
                $organisme = $obj['acronymOrganism'];

                //Verification de la presence de ce parent dans le tableau temporaire
                if (!array_key_exists($organisme.'#'.$p_id, $_t_id_initial)) {
                    $query = 'SELECT `id_initial` FROM `Service` where `id` = '.$p_id.' and organisme = "'.$organisme.'"';

                    //Execution de la requete
                    $st = $db->prepare($query);
                    $st->execute([]);

                    self::log('Requete executee :'."\n".$query);

                    //Recuperation des resultats de la requete
                    $res = $st->fetch(PDO::FETCH_ASSOC);
                    $st->closeCursor();

                    //Recuperation du resultat et ajout dans le tableau pour les prochaines recherches
                    if ($res['id_initial']) {
                        $p_id_initial = $res['id_initial'];
                        $_t_id_initial[$organisme.'#'.$p_id] = $p_id_initial;
                    } else {
                        $p_id_initial = 0;
                        $_t_id_initial[$organisme.'#'.$p_id] = 0;
                    }

                    //Liberation espace memoire
                    unset($st);
                    unset($res);
                } else {
                    //Recuperation de l'identifiant par lecture du tableau
                    $p_id_initial = $_t_id_initial[$organisme.'#'.$p_id];
                }

                //Mise a jour de l'identifiant du service parent
                self::log('Mise a jour de l\'affiliation service parent = '.$p_id.' - '.$p_id_initial.' - '.$organisme);
                $obj['parentId'] = $p_id_initial;
            }
        }

        //Liberation espace memoire
        unset($_t_id_initial);

        self::log('Fin du traitement de mise a jour des affiliations services');
    }

    public static function exportServices(&$db, &$report, &$pathXmlsSoclePPP)
    {
        self::log('Debut du traitement d\'export des services');

        //Recuperation des services
        $_t_elements = [];

        /*
         * Attention : Appel a la libraire Atexo_EntityPurchase pour la recuperation des services avec les affiliations
         */
        if (Atexo_Config::getParameter('TEST_SOCLE_PPP')) { // Juste pour les testes Unitaires
            $_t_elements = Atexo_EntityPurchase::getAllServicesAndPoles(Atexo_Config::getParameter('ORGANISME_TEST_SOCLE_PPP'));
        } else {
            $_t_elements = (new Atexo_EntityPurchase())->getAllServicesPlateForme();
        }

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Mise a jour des identifiants pour les affiliations poles/services
        self::updateServicesParentIds($db, $_t_elements);

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Services();
        $bufferXML = $interface->getXmlServices($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'entities.xml', $bufferXML);
            self::log('Le fichier XML des entites a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'entities.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'entities.xml', $bufferXML);
                self::log('Le fichier XML des entites a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'entities.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_entities_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'entities_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => "Entites d'achat", '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des services');
    }

    public static function updateAgentOrganismeId(&$db, &$obj, &$_t_organismes)
    {
        //Recuperation des valeurs d'interet
        $organisme = $obj['organisme'];

        //Recherche de l'id initial du service
        if (!array_key_exists($organisme, $_t_organismes)) {
            $query = 'SELECT `id_initial` FROM `Organisme` where `acronyme` = "'.$organisme.'"';

            //Execution de la requete
            $st = $db->prepare($query);
            $st->execute([]);

            self::log('Requete executee :'."\n".$query);

            //Recuperation des resultats de la requete
            $res = $st->fetch(PDO::FETCH_ASSOC);
            $st->closeCursor();

            //Recuperation du resultat et ajout dans le tableau pour les prochaines recherches
            if ($res['id_initial']) {
                $id_initial = $res['id_initial'];
                $_t_organismes[$organisme] = $id_initial;
            } else {
                $id_initial = 0;
                $_t_organismes[$organisme] = 0;
            }

            //Liberation espace memoire
            unset($st);
            unset($res);
        } else {
            //Recuperation de l'identifiant par lecture du tableau
            $id_initial = $_t_organismes[$organisme];
        }

        //Mise a jour de l'id initial du service dans l'objet
        $obj['organisme_id'] = $id_initial;
        self::log('Mise a jour de l\'identifiant de l\'organisme de l\'agent = '.$id_initial.' - '.$organisme);

        //Liberation espace memoire
        unset($id_initial);
        unset($organisme);
    }

    public static function updateAgentServiceId(&$db, &$obj, &$_t_services)
    {
        //Recuperation des valeurs d'interet
        $s_id = $obj['service_id'];
        $s_id_initial = $s_id;
        $organisme = $obj['organisme'];

        //Recherche de l'id initial du service
        if (!array_key_exists($organisme.'#'.$s_id, $_t_services)) {
            $query = 'SELECT `id_initial` FROM `Service` where `id` = '.$s_id.' and organisme = "'.$organisme.'"';

            //Execution de la requete
            $st = $db->prepare($query);
            $st->execute([]);

            self::log('Requete executee :'."\n".$query);

            //Recuperation des resultats de la requete
            $res = $st->fetch(PDO::FETCH_ASSOC);
            $st->closeCursor();

            //Recuperation du resultat et ajout dans le tableau pour les prochaines recherches
            if ($res['id_initial']) {
                $s_id_initial = $res['id_initial'];
                $_t_services[$organisme.'#'.$s_id] = $s_id_initial;
            } else {
                $s_id_initial = 0;
                $_t_services[$organisme.'#'.$s_id] = 0;
            }

            //Liberation espace memoire
            unset($st);
            unset($res);
        } else {
            //Recuperation de l'identifiant par lecture du tableau
            $s_id_initial = $_t_services[$organisme.'#'.$s_id];
        }

        //Mise a jour de l'id initial du service dans l'objet
        $obj['service_id'] = $s_id_initial;
        self::log('Mise a jour de l\'identifiant du service de l\'agent = '.$s_id.' - '.$s_id_initial.' - '.$organisme);

        //Liberation espace memoire
        unset($s_id);
        unset($s_id_initial);
        unset($organisme);
    }

    public static function updateAgentsRefIds(&$db, &$_t_elements)
    {
        self::log('Debut du traitement de mise a jour des identifiants services et organismes des agents');

        $_t_services = [];
        $_t_organismes = [];

        //Parcours du tableau d'agents
        foreach ($_t_elements as $key => &$obj) {
            //Mise a jour de l'identifiant de l'organisme de l'agent
            if ($obj['organisme']) {
                self::updateAgentOrganismeId($db, $obj, $_t_organismes);

                //Mise a jour de l'identifiant du service de l'agent
                if ($obj['service_id']) {
                    self::updateAgentServiceId($db, $obj, $_t_services);
                }
            }
        }

        //Liberation espace memoire
        unset($_t_services);
        unset($_t_organismes);

        self::log('Fin du traitement de mise a jour des identifiants services et organismes des agents');
    }

    public static function exportAgents(&$db, &$report, &$pathXmlsSoclePPP)
    {
        self::log('Debut du traitement d\'export des agents');

        //Recuperation des agents
        $_t_elements = [];
        $_s_query = <<<EOL
SELECT `Agent`.`id`, `Agent`.`id_initial`,
`Agent`.`nom`, `Agent`.`prenom`, `Agent`.`login`,
`Agent`.`service_id`, `Agent`.`organisme`,
`Agent`.`nom_fonction`, `Agent`.`elu`, `Agent`.`type_comm`,
`Agent`.`tentatives_mdp`, `Agent`.`password`,
`Agent`.`num_tel`, `Agent`.`num_fax`, `Agent`.`email`
FROM `Agent`
ORDER BY `Agent`.`id` ASC
EOL;
        self::getElements($db, $_t_elements, $_s_query);

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Mise a jour des identifiants organismes et services
        self::updateAgentsRefIds($db, $_t_elements);

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Services();
        $bufferXML = $interface->getXmlAgents($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'agents.xml', $bufferXML);
            self::log('Le fichier XML des agents a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'agents.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'agents.xml', $bufferXML);
                self::log('Le fichier XML des agents a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'agents.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_agents_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'agents_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => 'Agents', '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des agents');
    }

    public static function exportEntreprises(&$db, &$report, &$pathXmlsSoclePPP)
    {
        self::log('Debut du traitement d\'export des entreprises');

        //Recuperation des agents
        $_t_elements = [];
        $_s_query = <<<EOL
SELECT `Entreprise`.`id`, `Entreprise`.`id_initial`,
`Entreprise`.`nom`, `Entreprise`.`siren`, `Entreprise`.`nicsiege`,
`Entreprise`.`email`, `Entreprise`.`telephone`, `Entreprise`.`fax`, `Entreprise`.`site_internet`,
`Entreprise`.`adresse`, `Entreprise`.`codepostal`, `Entreprise`.`villeadresse`, `Entreprise`.`paysadresse`,
`Entreprise`.`formejuridique`, `Entreprise`.`codeape`, `Entreprise`.`description_activite`,
`Entreprise`.`paysenregistrement`, `Entreprise`.`sirenetranger`
FROM `Entreprise`
ORDER BY `Entreprise`.`id` ASC
EOL;
        self::getElements($db, $_t_elements, $_s_query);

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Entreprises();
        $bufferXML = $interface->getXmlEntreprises($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'companies.xml', $bufferXML);
            self::log('Le fichier XML des entreprises a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'companies.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'companies.xml', $bufferXML);
                self::log('Le fichier XML des entreprises a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'companies.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_companies_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'companies_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => 'Entreprises', '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des entreprises');
    }

    public static function exportEtablissements(&$db, &$report, &$pathXmlsSoclePPP)
    {
        self::log('Debut du traitement d\'export des établissements');

        //Recuperation des agents
        $_t_elements = [];
        $_s_query = <<<EOL
SELECT `t_etablissement`.`id_etablissement`, `t_etablissement`.`id_initial`, `Entreprise`.`id_initial` as `entreprise_id`,
`t_etablissement`.`code_etablissement`, `t_etablissement`.`est_siege`,
`t_etablissement`.`adresse`, `t_etablissement`.`adresse2`, `t_etablissement`.`code_postal`, `t_etablissement`.`ville`,
`t_etablissement`.`pays`, `t_etablissement`.`saisie_manuelle`, `t_etablissement`.`statut_actif`, `t_etablissement`.`tva_intracommunautaire`
FROM `t_etablissement`
INNER JOIN `Entreprise` ON (`t_etablissement`.`id_entreprise` = `Entreprise`.`id`)
ORDER BY `t_etablissement`.`id_etablissement` ASC
EOL;
        self::getElements($db, $_t_elements, $_s_query);

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Entreprises();
        $bufferXML = $interface->getXmlEtablissements($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'establishments.xml', $bufferXML);
            self::log('Le fichier XML des établissements a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'establishments.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'establishments.xml', $bufferXML);
                self::log('Le fichier XML des établissements a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'establishments.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_establishments_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'establishments_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => 'Etablissements', '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des établissements');
    }

    public static function exportInscrits(&$db, &$report, &$pathXmlsSoclePPP)
    {
        self::log('Debut du traitement d\'export des inscrits entreprises');

        //Recuperation des agents
        $_t_elements = [];
        $_s_query = <<<EOL
SELECT `Inscrit`.`id`, `Inscrit`.`id_initial`, `Entreprise`.`id_initial` as `entreprise_id`, `t_etablissement`.`id_initial` as `etablissement_id`, `Inscrit`.`siret`,
`Inscrit`.`nom`, `Inscrit`.`prenom`,`Inscrit`.`login`, `Inscrit`.`mdp`,
`Inscrit`.`profil`, `Inscrit`.`bloque`,
`Inscrit`.`email`, `Inscrit`.`telephone`, `Inscrit`.`fax`,
`Inscrit`.`adresse`, `Inscrit`.`adresse2`, `Inscrit`.`codepostal`, `Inscrit`.`ville`, `Inscrit`.`pays`
FROM `Inscrit` 
INNER JOIN `Entreprise` ON (`Inscrit`.`entreprise_id` = `Entreprise`.`id`)
INNER JOIN `t_etablissement` ON (`Inscrit`.`id_etablissement` = `t_etablissement`.`id_etablissement`)
ORDER BY `Inscrit`.`id` ASC
EOL;
        self::getElements($db, $_t_elements, $_s_query);

        //Comptage pour le rapport d'extraction
        $b_count = is_countable($_t_elements) ? count($_t_elements) : 0;
        $a_count = 0;

        //Recuperation du flux XML correspondant
        $interface = new Atexo_InterfaceWL_Entreprises();
        $bufferXML = $interface->getXmlInscrits($_t_elements, $a_count);

        if (Atexo_Config::getParameter('NON_VALIDATION_XML_SOCLE_PPP')) {
            $succes = 'Succes';
            Atexo_Util::write_file($pathXmlsSoclePPP.'employees.xml', $bufferXML);
            self::log('Le fichier XML des employes a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'employees.xml');
        } else {
            if (Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), $returnMessageErreur = false)) {
                $succes = 'Succes';
                Atexo_Util::write_file($pathXmlsSoclePPP.'employees.xml', $bufferXML);
                self::log('Le fichier XML des employes a bien ete cree dans le repertoire : '.$pathXmlsSoclePPP.'employees.xml');
            } else {
                $succes = 'Echec';
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($bufferXML, Atexo_Config::getParameter('XSD_SOCLE_PPP'), true));
                Atexo_Util::write_file($pathXmlsSoclePPP.'Erreur_employees_'.date('Ymd_H-i-s').'.xml', $xmlRetour);
                Atexo_Util::write_file($pathXmlsSoclePPP.'employees_non_valide.xml', $bufferXML);
            }
        }

        //Sauvegarde des comptages pour le rapport d'extraction
        $report[] = ['1' => 'Operateurs economiques', '2' => $b_count, '3' => $a_count, '4' => $succes];

        //Liberation espace memoire
        unset($_t_elements);
        unset($bufferXML);
        unset($interface);

        self::log('Fin du traitement d\'export des inscrits entreprises');
    }

    /**
     * Genere le rapport d'extraction des donnees du socle au format CSV.
     *
     * @param array $data : liste des donnees a inserer dans le fichier CSV
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.4
     *
     * @copyright Atexo 2014
     */
    public function generateRapportExtractionCSV(&$data)
    {
        $file = Atexo_Config::getParameter('PATH_XMLS_SOCLE_PPP').'PPP-rapport-extraction-reprise-donnees-socle.csv';

        $header = ['1' => 'Type de donnees',
                         '2' => 'Cardinalite avant extraction',
                         '3' => 'Cardinalite apres extraction',
                         '4' => "Verification de l'extraction",
                         '5' => 'Commentaires', ];

        array_unshift($data, $header);
        $handle = fopen($file, 'w');

        foreach ($data as $fields) {
            fputcsv($handle, $fields);
        }

        fclose($handle);
    }

    /**
     * Methode qui permet de calculer la prochaine valeur de id_initial pour la table passée en paramètre
     * Elle regarde en BD pour la table en question la valeur la plus grande et la retourne sans le prefix $type_id.
     *
     * @param &$db pointeur vers la base de données
     * @param $tableName nom de la table
     * @param $type_id prefix utilise pour tous les identifiants initiaux de la table
     *
     * @return int max_id_initial
     */
    private function getNextIdInitial(&$db, $tableName, $type_id)
    {
        $maxIdInitial = 0;
        $query = 'SELECT max(id_initial)  as max_id_initial FROM '.$tableName;
        $st = $db->query($query);

        while ($row = $st->fetch(PDO::FETCH_ASSOC)) {
            $maxIdInitial = ltrim(ltrim($row['max_id_initial'], $type_id), '0');
        }

        self::log('Requete executee :'."\n".$query."\n");
        self::log('Valeur maximale id_initial pour table '.$tableName.' :'.$maxIdInitial."\n");

        //Liberation espace memoire
        unset($st);

        return $maxIdInitial;
    }
}
