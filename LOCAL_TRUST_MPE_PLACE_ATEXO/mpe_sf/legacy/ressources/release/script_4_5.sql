INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(210, 'Liste des actions tracées côte entreprises');

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(177, 210, 'Authentification à la plateforme', 'Authentification à la plateforme', '', '', '', '', '', '', 'DESCRIPTION1', ''),
(178, 210, 'Accès à la fonction de test de configuration du poste (passé avec succès)', 'Accès à la fonction de test de configuration du poste (passé avec succès)', '', '', '', '', '', '', 'DESCRIPTION2', ''),
(179, 210, 'Accès à la fonction de test de configuration du poste (en échec)', 'Accès à la fonction de test de configuration du poste (en échec)', '', '', '', '', '', '', 'DESCRIPTION3', ''),
(180, 210, 'Accès au détail de la consultation', 'Accès au détail de la consultation', '', '', '', '', '', '', 'DESCRIPTION4', ''),
(188, 210, 'Accès au formulaire de demande de téléchargement du dossier de consultation', 'Accès au formulaire de demande de téléchargement du dossier de consultation', '', '', '', '', '', '', 'DESCRIPTION5', ''),
(189, 210, 'Validation du formulaire de demande de téléchargement du dossier de consultation', 'Validation du formulaire de demande de téléchargement du dossier de consultation', '', '', '', '', '', '', 'DESCRIPTION6', ''),
(190, 210, 'Téléchargement du dossier de consultation', 'Téléchargement du dossier de consultation', '', '', '', '', '', '', 'DESCRIPTION7', ''),
(191, 210, 'Téléchargement du règlement de la consultation', 'Téléchargement du règlement de la consultation', '', '', '', '', '', '', 'DESCRIPTION8', ''),
(192, 210, 'Accès à la fonction « Poser une question » de la consultation', 'Accès à la fonction « Poser une question » de la consultation', '', '', '', '', '', '', 'DESCRIPTION9', ''),
(193, 210, 'Validation du formulaire d''identification préalable à la question', 'Validation du formulaire d''identification préalable à la question', '', '', '', '', '', '', 'DESCRIPTION10', ''),
(194, 210, 'Envoi de la question sur la consultation', 'Envoi de la question sur la consultation', '', '', '', '', '', '', 'DESCRIPTION11', ''),
(195, 210, 'Accès au formulaire de réponse de la consultation', 'Accès au formulaire de réponse de la consultation', '', '', '', '', '', '', 'DESCRIPTION12', ''),
(196, 210, 'Ajout d''une ou plusieurs pièces libres au dossier [_typeEnveloppeLot_]', 'Ajout d''une ou plusieurs pièces libres au dossier [_typeEnveloppeLot_]', '', '', '', '', '', '', 'DESCRIPTION13', ''),
(198, 210, 'Ajout du "DC3 - Acte d''engagement" au dossier [_typeEnveloppeLot_]', 'Ajout du "DC3 - Acte d''engagement" au dossier [_typeEnveloppeLot_]', '', '', '', '', '', '', 'DESCRIPTION14', ''),
(199, 210, 'Retrait d''une pièce libre du dossier [_typeEnveloppeLot_]', 'Retrait d''une pièce libre du dossier [_typeEnveloppeLot_]', '', '', '', '', '', '', 'DESCRIPTION15', ''),
(200, 210, 'Retrait du "DC3 - Acte d''engagement" du dossier [_typeEnveloppeLot_]', 'Retrait du "DC3 - Acte d''engagement" du dossier [_typeEnveloppeLot_]', '', '', '', '', '', '', 'DESCRIPTION16', ''),
(201, 210, 'Signature d''une ou plusieurs pièces du formulaire de réponse de la consultation', 'Signature d''une ou plusieurs pièces du formulaire de réponse de la consultation', '', '', '', '', '', '', 'DESCRIPTION17', ''),
(202, 210, 'Validation du formulaire de réponse de la consultation', 'Validation du formulaire de réponse de la consultation', '', '', '', '', '', '', 'DESCRIPTION18', ''),
(203, 210, 'Annulation du formulaire de réponse de la consultation', 'Annulation du formulaire de réponse de la consultation', '', '', '', '', '', '', 'DESCRIPTION19', ''),
(204, 210, 'Affichage de la page de confirmation du dépôt de la réponse', 'Affichage de la page de confirmation du dépôt de la réponse', '', '', '', '', '', '', 'DESCRIPTION20', ''),
(205, 210, 'Erreur lors de la signature d''une pièce du formulaire de réponse de la consultation', 'Erreur lors de la signature d''une pièce du formulaire de réponse de la consultation\r\n', '', '', '', '', '', '', 'DESCRIPTION21', ''),
(208, 210, 'Erreur de chargement de l''applet cyptographique', 'Erreur de chargement de l''applet cyptographique', '', '', '', '', '', '', 'DESCRIPTION22', ''),
(209, 210, 'Erreur lors de l''ajout d''une pièce au formulaire de réponse de la consultation', 'Erreur lors de l''ajout d''une pièce au formulaire de réponse de la consultation', '', '', '', '', '', '', 'DESCRIPTION23', ''),
(210, 210, 'Erreur technique rencontrée lors du dépôt de la réponse électronique', 'Erreur technique rencontrée lors du dépôt de la réponse électronique', '', '', '', '', '', '', 'DESCRIPTION24', '');

UPDATE `t_edition_formulaire` SET `statut_generation` = '3' WHERE `path` IS NOT NULL AND `type` =1;

INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(25, 'Rejeté, problème de tiers fournisseur ') ;

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(1, 25, 'Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_N_EXISTE_PAS', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(2, 25, 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_NON_ETENDU', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(3, 25, 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_BLOQUE', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(4, 25, 'Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème', 'Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème', '', '', '', '', '', '', 'TIER_ERRONE', '');


INSERT INTO `Chorus_Code_Calcul_Interets` (`id` ,`libelle` ,`code`)VALUES ('1', 'Taux BCE (B1)', 'B1'); 
INSERT INTO `Chorus_Code_Calcul_Interets` (`id` ,`libelle` ,`code`)VALUES ('2', 'Taux BCE Majoré (B2)', 'B2');
INSERT INTO `Chorus_Code_Calcul_Interets` (`id` ,`libelle` ,`code`)VALUES ('3', 'Taux légal (L1)', 'L1'); 
INSERT INTO `Chorus_Code_Calcul_Interets` (`id` ,`libelle` ,`code`)VALUES ('4', 'Taux légal Majoré (L2)', 'L2');
INSERT INTO `Chorus_Code_Calcul_Interets` (`id` ,`libelle` ,`code`)VALUES ('5', 'Taux zéro (T0 IM)', 'T0');

INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('1', 'Paiement immédiat', 'Z000');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('2', 'Paiement à 5 jours', 'Z005');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('3', 'Paiement à 15 jours', 'Z015');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('4', 'Paiement à 30 jours', 'Z030');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('5', 'Paiement à 35 jours', 'Z035');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('6', 'Paiement à 45 jours', 'Z045');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('7', 'Paiement à 50 jours', 'Z050');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('8', 'Paiement à 60 jours', 'Z060');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('9', 'Paiement le 08 du mois suivant', 'ZF08');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('10', 'Paiement le 15 du mois suivant', 'ZF15');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('11', 'Paiement le dernier jour du mois suivant', 'ZFMS');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('12', 'Paiement le 25 du mois', 'ZG25');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('13', 'RNF - Facture interne (Fournisseur)', 'ZINT');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('14', 'Paiement PME', 'ZPME');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('15', 'Titre d''un débiteur privé', 'ZR11');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('16', 'TP arrêt recouvrement par retenue', 'ZR15');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('17', 'Débiteur public ou para public', 'ZR20');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('18', 'Etat', 'ZR21');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('19', 'Administration ou collectivité locale', 'ZR22');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('20', 'Organisme ou établissement Public', 'ZR23');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('21', 'Apurement par compensation', 'ZR30');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('22', 'Retenues sur paie', 'ZR31');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('23', 'Retenues sur une dépense', 'ZR32');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('24', 'Retenues sur pension', 'ZR33');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('25', 'Titre émis par un correspondant', 'ZR40');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('26', 'Taxe parafiscale', 'ZR42');
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES ('27', 'Titre aide juridictionnelle', 'ZR44');
