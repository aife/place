INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES
(25, 'Rejeté, problème de tiers fournisseur ') ;

INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(1, 25, 'Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants n''existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_N_EXISTE_PAS', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(2, 25, 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander l''extension dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_NON_ETENDU', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(3, 25, 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants', 'Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l''organisation d''achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants', '', '', '', '', '', '', 'TIER_BLOQUE', '');
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
(4, 25, 'Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème', 'Le ou les tiers fournisseurs suivants n''ont pu être vérifiés dans CHORUS. Veuillez contacter l''assistance téléphonique de PLACE afin d''identifier la nature du problème', '', '', '', '', '', '', 'TIER_ERRONE', '');




DELETE FROM `ValeurReferentiel` WHERE `ValeurReferentiel`.`id_referentiel` = 161; 

INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_2` ,`libelle_valeur_referentiel_it`) VALUES (1 , '161', 'Consultations', 'Consultations', '', '', '', '', '', '', '1;2;9;22;13;14;16;15;18;24;25', '');
INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_2` ,`libelle_valeur_referentiel_it`) VALUES (2 , '161', 'DCE', 'DCE', '', '', '', '', '', '', '5;17;6;7;8;23', '');
INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_2` ,`libelle_valeur_referentiel_it`) VALUES (3 , '161', 'Publicités', 'Publicités', '', '', '', '', '', '', '3;4', '');
INSERT INTO `ValeurReferentiel` (`id` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_2` ,`libelle_valeur_referentiel_it`) VALUES (4 , '161', 'Réponses électroniques', 'Réponses électroniques', '', '', '', '', '', '', '10;11;12;19;20;21', '');


-- MWA
INSERT INTO `t_flux_rss` (`id`, `tender_xml`, `nom_fichier`, `libelle`, `module`, `afficher_flux_rss`) VALUES (11, '<?xml version="1.0" encoding="UTF-8"?>\r\n<Transaction xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd" Version="1.0" TimeStamp="2001-12-17T09:30:47-05:00">\r\n <Request>\r\n  <Tender mps="1" enCours="1"/>\r\n </Request>\r\n</Transaction>', 'rss-MPS.xml', NULL, NULL, '0');




-- A exécuter avec le maj_organisme

INSERT INTO `ReferentielOrg` (`id_referentiel` ,`organisme` ,`libelle_referentiel`)VALUES (158 , '##ACCRONYME_ORG##', 'Type opération');

INSERT INTO `ValeurReferentielOrg` (`id` ,`organisme` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_valeur_referentiel_it`)
VALUES (NULL , '##ACCRONYME_ORG##', '158', 'Opération', 'Opération', '', '', '', '', '', '', '');

INSERT INTO `ValeurReferentielOrg` (`id` ,`organisme` ,`id_referentiel` ,`libelle_valeur_referentiel` ,`libelle_valeur_referentiel_fr` ,`libelle_valeur_referentiel_en` ,`libelle_valeur_referentiel_es` ,`libelle_valeur_referentiel_su` ,`libelle_valeur_referentiel_du` ,`libelle_valeur_referentiel_cz` ,`libelle_valeur_referentiel_ar` ,`libelle_valeur_referentiel_it`)
VALUES (NULL , '##ACCRONYME_ORG##', '158', 'Unité fonctionnelle', 'Unité fonctionnelle', '', '', '', '', '', '', '');