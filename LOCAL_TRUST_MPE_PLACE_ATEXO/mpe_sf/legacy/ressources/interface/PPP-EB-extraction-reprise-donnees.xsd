<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
	<xs:element name="Transaction">
		<xs:annotation>
			<xs:documentation>
			Cet élément encapsule la réponse qui est véhiculée dans le message.
			</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:choice>
				<xs:element name="Response">
					<xs:annotation>
						<xs:documentation>
							Cet élément encapsule la réponse qui est véhiculée dans le message.
							A chaque élément dans la réponse correspond un élément dans la requête.
						</xs:documentation>
					</xs:annotation>
					<xs:complexType>
						<xs:choice>
							<xs:element name="Organisms" minOccurs="0">
								<xs:annotation>
									<xs:documentation>
											Cet élément contient les informations relatives à un organisme. La description des attributs est la suivante:
											-- organismId : identifiant unique de l'organisme au niveau de la plateforme e-Bourgogne
											-- article : les valeurs possibles sont : Le, La, L' ou espace. Permet d'identifier le genre du nom de l'organisme
											-- acronym : trigramme de l'organisme (unique par organisme)
											-- denomination: libellé de de l'organisme
											-- categorieJuridique : valeur du code correspond à la catégorie juridique de l'organisme (table de référence INSEE)
											-- organismeTest : indique si cet organisme est marqué comme un organisme de test. 1 si vrai, 0 sinon.
											-- description : description de l'organisme
											-- siren : siren de l'organisme
											-- nicCode : code NIC de l'organisme, siren + nicCode = siret
											-- address: adresse de l'organisme
											-- postalCode : code postal de l'adresse de l'organisme
											-- city : ville de l'adresse de l'organisme
											-- email: adresse éléctronique de l'organisme
											-- urlSite : URL du site de l'organisme
											-- grandLogo : contenu binaire encodé en base 64 du grand logo de l'organisme
											-- petitLogo : contenu binaire encodé en base 64 du grand logo de l'organisme
									</xs:documentation>
								</xs:annotation>
								<xs:complexType>
									<xs:sequence>
										<xs:element name="Organism" minOccurs="0" maxOccurs="unbounded">
											<xs:complexType>
												<xs:attribute name="organismId" type="xs:int" use="required"/>
												<xs:attribute name="article" type="xs:int" use="required"/>
												<xs:attribute name="acronym" type="xs:string" use="required"/>
												<xs:attribute name="denomination" type="xs:string" use="required"/>
												<xs:attribute name="categorieJuridique" type="xs:string" use="required"/>
												<xs:attribute name="organismeTest" type="xs:boolean" use="required"/>
												<xs:attribute name="description" type="xs:string" use="optional"/>
												<xs:attribute name="siren" type="xs:int" use="optional"/>
												<xs:attribute name="nicCode" type="xs:int" use="optional"/>
												<xs:attribute name="address" type="xs:string" use="optional"/>
												<xs:attribute name="postalCode" type="xs:string" use="optional"/>
												<xs:attribute name="city" type="xs:string" use="optional"/>
												<xs:attribute name="email" type="xs:string" use="optional"/>
												<xs:attribute name="urlSite" type="xs:string" use="optional"/>
												<xs:attribute name="grandLogo" type="xs:base64Binary" use="optional"/>
												<xs:attribute name="petitLogo" type="xs:base64Binary" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="Entities" minOccurs="0">
								<xs:annotation>
									<xs:documentation>
											Cette réponse encapsule les résultats correspondant à la requête. Cet élément n'existe pas dans le flux XML de retour si aucun résultat n'est trouvé.
									</xs:documentation>
								</xs:annotation>
								<xs:complexType>
									<xs:sequence>
										<xs:element name="Entity" minOccurs="0" maxOccurs="unbounded">
											<xs:annotation>
												<xs:documentation>
														Cet élément contient les informations relatives à une entité d'achat. La description des attributs est la suivante:
														-- id : identifiant unique de l'entité d'achat au niveau de la plateforme e-Bourgogne
														-- parentId : identifiant unique de l'entité d'achat parente à laquelle elle se rattache. Il vaut 0 pour les entités d'achats directement rattachées au niveau organisme
														-- title : libellé de l'entité d'achat
														-- sigle : sigle de l'entité d'achat
														-- acronymOrganism : acronyme de l'organisme auquel se rattache l'entité d'achat
														-- organismId : identifiant de l'organisme auquel se rattache l'entité d'achat												
														-- address: adresse de l'entité d'achat
														-- postalCode : code postal de l'adresse l'entité d'achat
														-- city : ville de l'adresse de l'entité d'achat
														-- phone : numéro de téléphone de l'entité d'achat
														-- fax : fax de l'entité d'achat
														-- country : code pays de l'adresse de l'entité d'achat
														-- email: adresse éléctronique de l'entité d'achat
														-- siren : siren de l'entité d'achat
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:attribute name="id" type="xs:int" use="required"/>
												<xs:attribute name="parentId" type="xs:int" use="required"/>
												<xs:attribute name="title" type="xs:string" use="required"/>
												<xs:attribute name="sigle" type="xs:string" use="required"/>
												<xs:attribute name="acronymOrganism" type="xs:string" use="required"/>
												<xs:attribute name="organismId" type="xs:int" use="required"/>
												<xs:attribute name="address" type="xs:string" use="optional"/>
												<xs:attribute name="postalCode" type="xs:string" use="optional"/>
												<xs:attribute name="city" type="xs:string" use="optional"/>
												<xs:attribute name="phone" type="xs:string" use="optional"/>
												<xs:attribute name="fax" type="xs:string" use="optional"/>
												<xs:attribute name="country" type="xs:string" use="optional"/>
												<xs:attribute name="email" type="xs:string" use="optional"/>
												<xs:attribute name="siren" type="xs:int" use="optional"/>
											</xs:complexType>
										</xs:element>
										<xs:element name="Agent" minOccurs="0" maxOccurs="unbounded">
											<xs:annotation>
												<xs:documentation>
													Cet élément contient les informations relatives à un acheteur public associé à une entité d'achat. La description des attributs est la suivante:
													-- id : identifiant unique de l'agent
													-- entityId: identifiant unique de l'entité d'achat au niveau de la plateforme e-Bourgogne auquel se rattache l'agent
													-- organismId : identifiant unique de l'organisme au niveau de la plateforme e-Bourgogne auquel se rattache l'agent
													-- acronymOrganism : acronyme de l'organisme auquel se rattache l'agent
													-- login : identifiant de connexion de l'agent au niveau de la plateforme e-Bourgogne
													-- password : mot de passe de l'agent au niveau de la plateforme e-Bourgogne encodé en SHA1
													-- email: adresse éléctronique de l'agent
													-- firstname : prénom de l'agent
													-- lastname : nom de l'agent
													-- typeCommunicationCAOPrefEmail : indique si le mode de communication préféré pour la CAO est le courriel mèl. 1 si vrai, 0 sinon (dans ce cas la préférence va au courrier postal).
													-- compteActif : indique si le compte de l'agent est actif ou non
													-- elu : indique si l'agent est un élu. 1 si vrai, 0 sinon
													-- phone : numéro de téléphone de l'agent
													-- fax : fax de l'agent
													-- jobFunction : fonction de l'agent
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:sequence>
													<xs:element name="Applications">
														<xs:annotation>
															<xs:documentation>
																Cet élément liste les applications auxquelles l'acheteur a accès avec le profil correspondant.																										-- applicationId : identifiant unique de l'application
																-- profilId: identifiant du profil avec lequel l'agent accède à cette application
															</xs:documentation>
														</xs:annotation>
														<xs:complexType>
															<xs:sequence minOccurs="0" maxOccurs="unbounded">
																<xs:element name="Application">
																	<xs:complexType>
																		<xs:attribute name="applicationId" type="xs:int" use="required"/>
																		<xs:attribute name="profilId" type="xs:int" use="required"/>
																	</xs:complexType>
																</xs:element>
															</xs:sequence>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="id" type="xs:int" use="required"/>
												<xs:attribute name="entityId" type="xs:int" use="required"/>
												<xs:attribute name="organismId" type="xs:int" use="required"/>
												<xs:attribute name="acronymOrganism" type="xs:string" use="required"/>
												<xs:attribute name="login" type="xs:string" use="required"/>
												<xs:attribute name="password" type="xs:string" use="required"/>
												<xs:attribute name="email" type="xs:string" use="required"/>
												<xs:attribute name="firstname" type="xs:string" use="required"/>
												<xs:attribute name="lastname" type="xs:string" use="required"/>
												<xs:attribute name="elu" type="xs:boolean" use="required"/>
												<xs:attribute name="typeCommunicationCAOPrefEmail" type="xs:boolean" use="required"/>
												<xs:attribute name="compteActif" type="xs:boolean" use="required"/>
												<xs:attribute name="phone" type="xs:string" use="optional"/>
												<xs:attribute name="fax" type="xs:string" use="optional"/>
												<xs:attribute name="jobFunction" type="xs:string" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="Companies" minOccurs="0">
								<xs:annotation>
									<xs:documentation>
											Cette réponse encapsule les résultats correspondant à la requête. Cet élément n'existe pas dans le flux XML de retour si aucun résultat n'est trouvé.
									</xs:documentation>
								</xs:annotation>
								<xs:complexType>
									<xs:sequence>
										<xs:element name="Company" minOccurs="0" maxOccurs="unbounded">
											<xs:annotation>
												<xs:documentation>
														Cet élément contient les informations relatives à une entreprise. La description des attributs est la suivante:
															-- id : idenitifiant unique de l'entreprise
															-- name : dénomination de l'entreprise
															-- siren : numéro de siren de l'entreprise
															-- nicCode : code NIC de l'établissement du siege de l'entreprise auquel se rattache l'opérateur économique (code à 5 chiffres), siren + nicCode = siret
															-- address: adresse de l'entreprise
															-- postalCode : code postal de l'adresse de l'entreprise
															-- city : ville de l'adresse de l'entreprise
															-- country : code pays de l'adresse de l'entreprise
															-- paysEnregistrementFrance : vrai si le pays d'enregistrement est FRANCE, 0 si pays enreigstement ETRANGER
															-- email: adresse éléctronique de l'entreprise
															-- legalStatus : statut de l'entreprise (SA, SARL, etc.)
															-- apeNafNaceCode : code NAF/NACE/APE saisit dans le compte de l'entreprise (sur 4 caractères)
															-- phone : numéro de téléphone de l'entreprise
															-- fax : fax de l'entreprise
															-- nationalId : identifiant national de l'entreprise si étrangère, dans ce cas siren et nicCode non renseignés.
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:sequence>
													<xs:element name="Managers" minOccurs="0">
														<xs:annotation>
															<xs:documentation>
																Cet élément contient la liste des représentants de l'entreprise. Si aucun représentant n'est renseigné dans le compte de l'entreprise alors il n'est pas valorisé.
															</xs:documentation>
														</xs:annotation>
														<xs:complexType>
															<xs:sequence>
																<xs:element ref="Manager" maxOccurs="unbounded"/>
															</xs:sequence>
														</xs:complexType>
													</xs:element>
													<xs:element name="WebSite" minOccurs="0">
														<xs:annotation>
															<xs:documentation>
																Cet élément contient l'URL du site Web saisit dans le compte entreprise. S'il n'est pas renseigné dans le compte entreprise alors il n'est pas valorisé.
															</xs:documentation>
														</xs:annotation>
													</xs:element>
													<xs:element name="ActivityDescription" minOccurs="0">
														<xs:annotation>
															<xs:documentation>
																Cet élément contient la description de l'activité saisit dans le compte entreprise. S'il n'est pas renseigné dans le compte entreprise alors il n'est pas valorisé.
															</xs:documentation>
														</xs:annotation>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="id" type="xs:int" use="required"/>
												<xs:attribute name="name" type="xs:string" use="required"/>
												<xs:attribute name="siren" type="xs:int" use="optional"/>
												<xs:attribute name="nicCode" type="xs:int" use="optional"/>
												<xs:attribute name="address" type="xs:string" use="required"/>
												<xs:attribute name="postalCode" type="xs:string" use="required"/>
												<xs:attribute name="city" type="xs:string" use="required"/>
												<xs:attribute name="country" type="xs:string" use="required"/>
												<xs:attribute name="paysEnregistrementFrance" type="xs:boolean" use="required"/>
												<xs:attribute name="legalStatus" type="xs:NMTOKEN" use="required"/>
												<xs:attribute name="apeNafNaceCode" type="xs:string" use="optional"/>
												<xs:attribute name="phone" type="xs:string" use="optional"/>
												<xs:attribute name="fax" type="xs:string" use="optional"/>
												<xs:attribute name="nationalId" type="xs:string" use="optional"/>
											</xs:complexType>
										</xs:element>
										<xs:element name="Establishment" minOccurs="0" maxOccurs="unbounded">
											<xs:annotation>
												<xs:documentation>
													Cet élément contient la description d'un établissement rattaché à une entreprise. La description des attributs est la suivante:
													-- id : identifiant unique de l'établissement
													-- companyId: identifiant unique de l'entreprise auquel se rattache l'établissement
													-- nicCode : code NIC de l'établissement (code à 5 chiffres), siren + nicCode = siret (valeur possible vide)
													-- estSiege : indique si l'établissement est le siège, valeur possible 1 si siège, 0 sinon
													-- address: adresse de l'établissement (valeur possible vide)
													-- postalCode : code postal de l'adresse de l'établissement (valeur possible vide)
													-- city : ville de l'adresse de l'établissement (valeur possible vide)
													-- country : code pays de l'adresse de l'établissement (valeur possible vide)
													-- saisieManuelle : indique si l'établissement a été rajouté manuellement et non synchronisé via API, valeur possible 1 si saisit manuellement, 0 sinon
													-- statutActif : indique si l'établissement est encore actif, valeur possible 1 si actif, 0 sinon
													-- tvaIntracommunautaire : numéro de TVA intracommunautaire (valeur possible vide)
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:attribute name="id" type="xs:int" use="required"/>
												<xs:attribute name="companyId" type="xs:int" use="required"/>
												<xs:attribute name="nicCode" type="xs:int" use="required"/>
												<xs:attribute name="estSiege" type="xs:boolean" use="required"/>
												<xs:attribute name="address" type="xs:string" use="required"/>
												<xs:attribute name="postalCode" type="xs:string" use="required"/>
												<xs:attribute name="city" type="xs:string" use="required"/>
												<xs:attribute name="country" type="xs:string" use="required"/>
												<xs:attribute name="saisieManuelle" type="xs:boolean" use="required"/>
												<xs:attribute name="statutActif" type="xs:boolean" use="required"/>
												<xs:attribute name="tvaIntracommunautaire" type="xs:string" use="required"/>
											</xs:complexType>
										</xs:element>
										<xs:element name="Employee" minOccurs="0" maxOccurs="unbounded">
											<xs:annotation>
												<xs:documentation>
													Cet élément contient la description d'un opérateur économique rattaché à une entreprise. La description des attributs est la suivante:
														-- id : identifiant unique de l'opérateur économique
														-- companyId: identifiant unique de l'entreprise auquel se rattache l'opérateur économique
														-- establishmentId : identifiant unique de l'établissement auquel se rattache l'opérateur économique
														-- login : identifiant de connexion de l'opérateur économique
														-- password : mot de passe de l'opérateur économique au niveau de la plateforme e-Bourgogne encodé en SHA1
														-- email: adresse éléctronique de l'opérateur économique
														-- nicCode : code NIC de l'établissement auquel se rattache l'opérateur économique (code à 5 chiffres), siren + nicCode = siret
														-- title : civilité de l'inscrit (M., Mme ou Mlle)
														-- firstname : prénom de l'opérateur économique
														-- lastname : nom de l'opérateur économique
														-- profilJEB : code du profil de l'inscrit dans JEB (US = utilisateur simple, AD=administrateur)
														-- profilSDM: code du profil de l'inscrit dans la SDM (US = utilisateur simple, AD=administrateur)
														-- phone : numéro de téléphone de l'établissement de rattachement de l'opérateur économique
														-- address: adresse de l'établissement de rattachement l'opérateur économique
														-- postalCode : code postal de l'adresse de l'établissement de rattachement l'opérateur économique
														-- city : ville de l'adresse de l'établissement de rattachement l'opérateur économique
														-- country : code pays de l'adresse de l'établissement de rattachement l'opérateur économique
														-- fax : fax de de l'établissement de rattachement l'opérateur économique
														-- jobFunction : fonction de l'inscrit dans l'établissement
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:attribute name="id" type="xs:int" use="required"/>
												<xs:attribute name="companyId" type="xs:int" use="required"/>
												<xs:attribute name="establishmentId" type="xs:int" use="required"/>
												<xs:attribute name="login" type="xs:string" use="required"/>
												<xs:attribute name="password" type="xs:string" use="required"/>
												<xs:attribute name="email" type="xs:string" use="required"/>
												<xs:attribute name="nicCode" type="xs:int" use="optional"/>
												<xs:attribute name="title" type="xs:string" use="required"/>
												<xs:attribute name="compteActif" type="xs:boolean" use="required"/>
												<xs:attribute name="firstname" type="xs:string" use="required"/>
												<xs:attribute name="lastname" type="xs:string" use="required"/>
												<xs:attribute name="profilSDM" type="xs:string" use="required"/>
												<xs:attribute name="phone" type="xs:string" use="required"/>
												<xs:attribute name="address" type="xs:string" use="required"/>
												<xs:attribute name="postalCode" type="xs:string" use="required"/>
												<xs:attribute name="city" type="xs:string" use="required"/>
												<xs:attribute name="country" type="xs:string" use="required"/>
												<xs:attribute name="fax" type="xs:string" use="optional"/>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:choice>
					</xs:complexType>
				</xs:element>
			</xs:choice>
			<xs:attribute name="Version" type="xs:decimal" use="required" fixed="1.0"/>
			<xs:attribute name="TimeStamp" type="xs:dateTime" use="required"/>
		</xs:complexType>
	</xs:element>
	<xs:element name="Manager">
		<xs:annotation>
			<xs:documentation>
				Cet élément contient les informations relatives à un representant de l'entreprise. La description des attributs est la suivante:
					-- firstname : prénom
					-- lastname : nom
					-- jobFunction : description de la fonction dans l'entreprise					
			</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:attribute name="firstname" type="xs:string" use="required"/>
			<xs:attribute name="lastname" type="xs:string" use="required"/>
			<xs:attribute name="jobFunction" type="xs:string" use="required"/>
		</xs:complexType>
	</xs:element>
</xs:schema>
