<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:x="fr:gouv:ae:archive:draft:standard_echange_v0.2">
<xsl:output method='html' version='1.0' encoding='UTF-8' indent='yes'/>

<xsl:template match="/">
	<html>
		<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Archive</title>
		<link rel="stylesheet" href="styles/styles.css" type="text/css" />
		<script language="JavaScript" type="text/JavaScript" src="styles/xmlTree.js"></script>
	</head>
		<body>
				<xsl:apply-templates />
		</body>
  </html>
</xsl:template>


<xsl:template match="x:ArchiveTransfer">
	<div class="page">
		<h1><xsl:value-of select="x:Comment"></xsl:value-of></h1>
		<div class="reference"><strong>R�f�rence de l'archive : </strong><xsl:value-of select="x:TransferIdentifier"/></div>
		<div class="contacts">
		
		
			<table class="tableau" cellspacing="5">
				<tr>
					<td class="emetteur">
						<!--Debut Bloc emetteur-->
						<div>
							<div class="top"></div>
							<h2>Emetteur</h2>
							<xsl:for-each select="x:TransferringAgency">
								<!--Description -->	
								<xsl:if test="normalize-space(x:Description)">
									<div><xsl:value-of select="x:Description"/></div>
								</xsl:if>						
								<!--Contact -->	
								<h3>Contact</h3>
								<xsl:if test="normalize-space(x:Contact/x:PersonName)">
								  <div><xsl:value-of select="x:Contact/x:PersonName"/></div>
								</xsl:if>						
								<xsl:if test="normalize-space(x:Contact/x:DepartmentName)">
								  <div><xsl:value-of select="x:Contact/x:DepartmentName"/></div>
								</xsl:if>						
								<xsl:if test="normalize-space(x:Contact/x:Responsibility)">
									<div><xsl:value-of select="x:Contact/x:Responsibility"/></div>
								</xsl:if>	
								<!--Adresse-->	
								
								<div>
									<xsl:if test="normalize-space(x:Address/x:BuildingNumber)">
										<xsl:value-of select="x:Address/x:BuildingNumber"/>,
									</xsl:if>						
									<xsl:if test="normalize-space(x:Address/x:StreetName)">
										<xsl:value-of select="x:Address/x:StreetName"/>
									</xsl:if>
								</div>
								<div>	
									<xsl:if test="normalize-space(x:Address/x:Postcode)">
										<xsl:value-of select="x:Address/x:Postcode"/><span class="hori-spacer"></span> 
									</xsl:if>
									<xsl:if test="normalize-space(x:Address/x:CityName)">
										<xsl:value-of select="x:Address/x:CityName"/><span class="hori-spacer"></span> 
									</xsl:if>
									<xsl:if test="normalize-space(x:Address/x:Country)">
										<xsl:value-of select="x:Address/x:Country"/>
									</xsl:if>						
								</div>
								<!--Tel Courriel-->	
								<div>	
									<xsl:for-each select="x:Contact/x:Communication">
										<xsl:if test="normalize-space(x:Channel)">
											<div>
												<span class="capitalize"><xsl:value-of select="x:Channel"/> : </span> 
												<xsl:if test="normalize-space(x:CompleteNumber)">
													<xsl:value-of select="x:CompleteNumber"/>
												</xsl:if>						
												<xsl:if test="normalize-space(x:URI)">
													<xsl:value-of select="x:URI"/>
												</xsl:if>						
											</div> 
										</xsl:if>
									</xsl:for-each>	
								</div>
														
							</xsl:for-each>	
							<div class="bottom"></div>			
						</div>
						<!--Fin Bloc emetteur-->
					</td>
					<td class="recepteur">
						<!--Debut Bloc recepteur-->
						<div>
							<div class="top"></div>
							<h2>R�cepteur</h2>
							<xsl:for-each select="x:ArchivalAgency">
								<!--Description -->	
								<xsl:if test="normalize-space(x:Description)">
									<div><xsl:value-of select="x:Description"/></div>
								</xsl:if>	
								<div class="spacer-small"></div>
								<xsl:if test="normalize-space(x:Name)">
									<div><xsl:value-of select="x:LegalClassification"/></div>
								</xsl:if>	
								<xsl:if test="normalize-space(x:Name)">
									<div><xsl:value-of select="x:Name"/></div>
								</xsl:if>	
								
								<!--Contact -->	
								<h3>Contact</h3>
								<xsl:if test="normalize-space(x:Contact/x:PersonName)">
								  <div><xsl:value-of select="x:Contact/x:PersonName"/></div>
								</xsl:if>						
								<xsl:if test="normalize-space(x:Contact/x:DepartmentName)">
								  <div><xsl:value-of select="x:Contact/x:DepartmentName"/></div>
								</xsl:if>						
								<xsl:if test="normalize-space(x:Contact/x:Responsibility)">
									<div><xsl:value-of select="x:Contact/x:Responsibility"/></div>
								</xsl:if>	
								<!--Adresse-->	
								
								<div>
									<xsl:if test="normalize-space(x:Address/x:BuildingNumber)">
										<xsl:value-of select="x:Address/x:BuildingNumber"/>,
									</xsl:if>						
									<xsl:if test="normalize-space(x:Address/x:StreetName)">
										<xsl:value-of select="x:Address/x:StreetName"/>
									</xsl:if>
								</div>
								<div>	
									<xsl:if test="normalize-space(x:Address/x:Postcode)">
										<xsl:value-of select="x:Address/x:Postcode"/><span class="hori-spacer"></span> 
									</xsl:if>
									<xsl:if test="normalize-space(x:Address/x:CityName)">
										<xsl:value-of select="x:Address/x:CityName"/><span class="hori-spacer"></span> 
									</xsl:if>
									<xsl:if test="normalize-space(x:Address/x:Country)">
										<xsl:value-of select="x:Address/x:Country"/>
									</xsl:if>						
								</div>
								<!--Tel Courriel-->	
								<div>	
									<xsl:for-each select="x:Contact/x:Communication">
										<xsl:if test="normalize-space(x:Channel)">
											<div>
												<span class="capitalize"><xsl:value-of select="x:Channel"/> : </span> 
												<xsl:if test="normalize-space(x:CompleteNumber)">
													<xsl:value-of select="x:CompleteNumber"/>
												</xsl:if>						
												<xsl:if test="normalize-space(x:URI)">
													<xsl:value-of select="x:URI"/>
												</xsl:if>						
											</div> 
										</xsl:if>
									</xsl:for-each>	
								</div>
							</xsl:for-each>	
							<div class="bottom"></div>			
						</div>
					<!--Fin Bloc recepteur-->
					</td>
				</tr>
			</table>
			<div class="breaker"></div>
		</div>

		<div class="bloc">
			<fieldset>
				<legend>Meta-donn�es</legend>
					<div class="line">
						<span class="intitule-200">R�f�rence de la consultation :</span>
						<div class="content">
							<xsl:for-each select="x:Contains/x:Name">
								<xsl:value-of select="."/>
							</xsl:for-each>
						</div>
					</div>
				
					<div class="line">
						<span class="intitule-200">Objet de la consultation :</span>
						<div class="content">
							<xsl:for-each select="x:Contains/x:ContentDescription/x:Description">
								<xsl:value-of select="."/>
							</xsl:for-each>
						</div>
					</div>
					<div class="spacer"></div>
					
					<!--Debut Mots cles-->
					<div class="line">
						<span class="intitule">Mots cl�s :</span>
						<div class="content">
							<xsl:for-each select="x:Contains/x:ContentDescription/x:ContentDescriptive/x:KeywordReference">
								<div class="content">
									<xsl:if test="@schemeName!=''">
										<xsl:value-of select="@schemeName"/>,
									</xsl:if>
									<xsl:if test="normalize-space(.)">
										<xsl:value-of select="."/>,
									</xsl:if>
									<xsl:if test="normalize-space(../x:KeywordContent)">
										<xsl:value-of select="../x:KeywordContent"/>
									</xsl:if>
								</div>
							</xsl:for-each>
						</div>
					</div>
					<!--Fin Mots cles-->
			</fieldset>
		</div>
		<xsl:apply-templates select="x:Contains" />
	</div>
</xsl:template>


<xsl:template match="x:Contains">
	<div class="bloc">
		<fieldset>
			<legend>Arborescence de l'archive</legend>
			<div id="arbo">
				<div class="root">
					<xsl:call-template name="fichier"></xsl:call-template>
					<xsl:variable name="blocId"></xsl:variable>
					<div class="folder" id="{$blocId}" style="display:block;">
						<xsl:value-of select="$blocId"></xsl:value-of>
						<xsl:apply-templates select="x:Contains"/>
					</div>
				</div>
			</div>
		</fieldset>
	</div>
</xsl:template>




<xsl:template match="x:Contains/x:Contains">

	<!--Test a faire sur les docs externes ici-->
	<xsl:variable name="docxExternes"><xsl:value-of select="x:Name"/></xsl:variable>

	<xsl:choose>
		<!--Si docs externes-->
		<xsl:when test="$docxExternes='Documents_Externes'">
			<div class="branch">
			<!--<xsl:value-of select="$docxExternes"/>-->
				<xsl:call-template name="fichier"></xsl:call-template>			
				<div class="folder" style="display:none;">
					<div class="docs-externes"><xsl:apply-templates select="x:Document" /></div>
				</div>
			</div>
		</xsl:when>
		<!--Sinon-->
		<xsl:otherwise>
			<div class="branch">
				<xsl:call-template name="fichier"></xsl:call-template>			
				<div class="folder" style="display:none;">
					<xsl:apply-templates select="x:Contains" />
				</div>
				<xsl:apply-templates select="x:Document" />
			</div>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>








<!--Template fichier Telechargeable-->
<xsl:template match="x:Document">
	<div class="branch">
			<xsl:call-template name="document"></xsl:call-template>
			<div class="breaker"></div>
	</div>
</xsl:template>


<!--Template  fichier -->
<xsl:template name="fichier">
	<div class="branch">
		<xsl:apply-templates select="x:DescriptionLevel"/>
	</div>
</xsl:template>

<!--Template Nom fichier -->
<xsl:template match="x:Name">
	<!--<xsl:variable name="docxExternes"><xsl:value-of select="."/></xsl:variable>
	<xsl:if test="$docxExternes='Documents_Externes'">
		<div class="nom-fichier">
		
		hg
		</div>

	</xsl:if>-->

	<div class="nom-fichier">
		<xsl:value-of select="."/>
	</div>
	
	
	
</xsl:template>





<xsl:template match="x:ContentDescription">
	<xsl:value-of select="x:Description"/>
</xsl:template>


<!--Template Nom fichier sans le chemin complet-->

<!--Template Chemin complet du fichier-->
<xsl:template name="filePath">
	<xsl:call-template name="filename"></xsl:call-template>
</xsl:template>


<!--Template Nom fichier sans le chemin complet-->
<xsl:template name="filename">
  <xsl:param name="path"><xsl:value-of select="@filename"/></xsl:param>
  <xsl:choose>
    <xsl:when test="contains($path, '/')">
      <xsl:call-template name="filename">
        <xsl:with-param name="path" select="substring-after($path, '/')"/>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="$path"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>


<!--Chemin complet du document-->
<xsl:template name="docPath">
	<xsl:variable name="urlDoc"><xsl:value-of select="./x:Attachment/@filename"/></xsl:variable>
	<xsl:value-of select="substring($urlDoc,2)"/>
</xsl:template>


<!--Template Document-->
<xsl:template name="document">
	<span class="trigger">
		<xsl:variable name="title"><xsl:apply-templates select="x:Description"/></xsl:variable>
		<xsl:variable name="url"><xsl:apply-templates select="x:Attachment"/></xsl:variable>
		<xsl:variable name="fullPath"><xsl:call-template name="docPath" /></xsl:variable>
		<span class="doc">
			<img alt="" src="styles/doc.gif" />
			<span title="{$title}">
				<a href="{$fullPath}" target="_blank"><xsl:value-of select="$url"/></a>
			</span>
		</span>
	</span>
</xsl:template>


<!--Template URL-->
<xsl:template match="x:Description">
	<xsl:value-of select="."/>
</xsl:template>

<!--Template URL-->
<xsl:template match="x:Attachment">
		<xsl:call-template name="filePath"></xsl:call-template>
</xsl:template>


<!--Template Icone fichier-->
<xsl:template match="x:DescriptionLevel">

	<xsl:variable name="type"><xsl:value-of select="."/></xsl:variable>
	<xsl:variable name="title"><xsl:apply-templates select="../x:ContentDescription"/></xsl:variable>
	
	<xsl:variable name="url">
		<!--<xsl:apply-templates select="../Document"/>-->
		<xsl:call-template name="filename" />
	</xsl:variable>
	
	<xsl:variable name="fileCompletePath">
	  <xsl:call-template name="filePath" />
	</xsl:variable>

		<xsl:if test="$type='file'">
			<span class="trigger" onclick="toggle(this)">
				<img src="styles/closed.gif" alt=""/>
				<span title="{$title}">
				
				
				
				
					<xsl:apply-templates select="../x:Name"/>
				</span>
			</span>
		</xsl:if>
		<xsl:if test="$type='recordgrp'">
			<span class="trigger" onclick="toggle(this)">
				<img src="styles/open.gif" alt=""/>
				<span title="{$title}">
					<xsl:apply-templates select="../x:Name"/>
				</span>
			</span>
		</xsl:if>
		<div class="breaker"></div>
		
</xsl:template>

</xsl:stylesheet>