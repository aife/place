#/bin/bash
# Generation AC + certif marches + ts (horodatage) + smime (email signés)
# Auteur : Laurent CHARTRAIN
#
# -d : specifie le tmp_ca (dossier où seront stockés tous les certificats)
# -n : specifie le nom de la PF
# -e : Email pour le certificat SMIME
# 

tmp_dir=tmp_ca
nom="MPE"
email="mpe@atexo.com"

#### Ne rien toucher en dessous
while getopts  "d:n:e:" flag; do
  case "$flag" in
    n) nom=$OPTARG;;
    d) tmp_dir=$OPTARG;;
    e) email=$OPTARG;;
  esac
done

mkdir -p ${tmp_dir}
mkdir -p ${tmp_dir}/certs
mkdir -p ${tmp_dir}/reqs
mkdir -p ${tmp_dir}/keys

echo "00000001" > ${tmp_dir}/serial

### AC ROOT
echo -e "\n --> Generation AC Racine..."
openssl req -nodes -newkey rsa:2048 -x509 -days 3650 -keyout ${tmp_dir}/keys/root_ca_pkey.pem -out ${tmp_dir}/certs/root_ca.crt -subj "/C=FR/O=$nom/OU=MARCHES PUBLICS ELECTRONIQUES/CN=$nom ROOT CA/"

### MARCHES
echo -e "\n --> Generation CSR pour le certificat Marches..."
openssl req -nodes -newkey rsa:2048                  -keyout ${tmp_dir}/keys/marches_ca_pkey.pem -out ${tmp_dir}/reqs/marches_ca.csr -subj "/C=FR/O=$nom/OU=MARCHES PUBLICS ELECTRONIQUES/CN=$nom MPE" 

cat <<@eof >${tmp_dir}/extfile
extensions = extend
[extend]
basicConstraints=CA:TRUE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
@eof

echo -e "\n --> Signature du certificat marches par l'ac racine..."
openssl x509 -req -in ${tmp_dir}/reqs/marches_ca.csr -out ${tmp_dir}/certs/marches_ca.crt -days 3650 -CA ${tmp_dir}/certs/root_ca.crt -CAkey ${tmp_dir}/keys/root_ca_pkey.pem -CAserial ${tmp_dir}/serial -extfile ${tmp_dir}/extfile

### HORODATAGE
echo -e "\n --> Generation CSR pour le certificat Horodatage..."
openssl req -nodes -newkey rsa:2048                  -keyout ${tmp_dir}/keys/ts_pkey.pem -out ${tmp_dir}/reqs/ts.csr -subj "/C=FR/O=$nom/OU=MARCHES PUBLICS ELECTRONIQUES/CN=$nom MPE TIMESTAMPING" 

cat <<@eof >${tmp_dir}/extfile
extensions = extend
[extend]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer
extendedKeyUsage=critical,timeStamping
@eof

echo -e "\n --> Signature du certificat horodatage par l'ac racine..."
openssl x509 -req -in ${tmp_dir}/reqs/ts.csr -out ${tmp_dir}/certs/ts.crt -days 3650 -CA ${tmp_dir}/certs/root_ca.crt -CAkey ${tmp_dir}/keys/root_ca_pkey.pem -CAserial ${tmp_dir}/serial -extfile ${tmp_dir}/extfile

### SMIME
cat <<@eof >${tmp_dir}/extfile
extensions = extend
[extend]
basicConstraints=CA:FALSE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid,issuer:always
keyUsage=nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage=clientAuth,emailProtection
nsCertType = email
@eof

echo -e "\n --> Generation CSR pour le certificat S/MIME..."
openssl req -nodes -newkey rsa:2048 -keyout ${tmp_dir}/keys/s_mime_pkey.pem -out ${tmp_dir}/reqs/smime.csr -subj "/C=FR/O=$nom/OU=MARCHES PUBLICS ELECTRONIQUES/CN=$nom/emailAddress=$email"
echo -e "\n --> Signature du certificat S/MIME par l'ac racine..."
openssl x509 -req -in ${tmp_dir}/reqs/smime.csr -out ${tmp_dir}/certs/s_mime.crt -days 3650 -CA ${tmp_dir}/certs/root_ca.crt -CAkey ${tmp_dir}/keys/root_ca_pkey.pem -CAserial ${tmp_dir}/serial -extfile ${tmp_dir}/extfile

rm -f ${tmp_dir}/extfile

echo
