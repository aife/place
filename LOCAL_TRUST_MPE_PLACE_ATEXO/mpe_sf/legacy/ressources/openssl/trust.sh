#!/bin/sh
OPENSSL_PATH=`which openssl`
if [ "$?" != 0 ]; then echo "openssl not found, exiting...";exit 1;fi;
MAKE_PATH=`which make >/dev/null 2>&1`
if [ "$?" != 0 ]; then
        for file in `ls *.crt | grep -v Makefile`; do echo $file;ln -sfn $file `openssl x509 -hash -noout -in $file`.0; done
        for file in `ls *.crl | grep -v Makefile`; do echo $file;ln -sfn $file `openssl crl -hash -noout -in $file`.0; done
else
        make -f Makefile.crt
        make -f Makefile.crl
fi
