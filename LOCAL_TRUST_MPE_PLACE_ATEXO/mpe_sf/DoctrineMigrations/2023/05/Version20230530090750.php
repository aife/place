<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230530090751 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22735 - [TNCP] Mise à jour du statut de la publicité';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE AnnonceBoamp CHANGE ann_xml ann_xml LONGBLOB DEFAULT NULL, CHANGE ann_pdf ann_pdf LONGBLOB DEFAULT NULL,'
            . ' CHANGE ann_form_values ann_form_values LONGBLOB DEFAULT NULL, CHANGE ann_error ann_error LONGTEXT DEFAULT NULL,  CHANGE parution parution LONGTEXT DEFAULT NULL,'
            . '  CHANGE erreurs erreurs LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE AnnonceBoamp CHANGE ann_xml ann_xml LONGBLOB NOT NULL, CHANGE ann_pdf ann_pdf LONGBLOB NOT NULL,'
            . ' CHANGE ann_form_values ann_form_values LONGBLOB NOT NULL, CHANGE ann_error ann_error LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`,'
            . '  CHANGE parution parution MEDIUMTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`,'
            . '  CHANGE erreurs erreurs MEDIUMTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`');
    }
}
