<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230526093651 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22660 - Nouvelle habilitation de rattachement à un service pour un invité permanent et reprise de données automatiques';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE HabilitationAgent 
            ADD COLUMN IF NOT EXISTS rattachement_service TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE HabilitationProfil 
            ADD COLUMN IF NOT EXISTS rattachement_service TINYINT(1) NOT NULL DEFAULT 0
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE HabilitationAgent DROP IF EXISTS rattachement_service');
        $this->addSql('ALTER TABLE HabilitationProfil DROP IF EXISTS rattachement_service');
    }
}
