<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230530140115 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22691 - Ajout d'un champs data contenant les informations des offres supprimés";
    }

    public function up(Schema $schema): void
    {
        // Modification collation
        $this->addSql("ALTER TABLE historique_purge charset = utf8;");
        $this->addSql("ALTER TABLE historique_purge MODIFY type varchar(255) charset utf8 null;");
        $this->addSql("ALTER TABLE historique_purge MODIFY description varchar(255) charset utf8 null;");

        // Add new fields
        $this->addSql("ALTER TABLE `historique_purge` ADD COLUMN IF NOT EXISTS `data` TEXT NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE historique_purge MODIFY data text charset utf8 null;");

        $this->addSql(
            'ALTER TABLE `historique_purge`
                ADD COLUMN IF NOT EXISTS `id_fichier_enveloppe` INT NULL DEFAULT NULL
            ;'
        );
        $this->addSql(
            'ALTER TABLE `historique_purge`
                ADD COLUMN IF NOT EXISTS `id_bloc_fichier_enveloppe` INT NULL DEFAULT NULL
            ;'
        );
        $this->addSql(
            'ALTER TABLE `historique_purge` 
                ADD COLUMN IF NOT EXISTS `organisme` VARCHAR(30) charset utf8 NULL DEFAULT NULL;'
        );

        $this->addSql('
            ALTER TABLE blocFichierEnveloppe DROP FOREIGN KEY IF EXISTS FK_blocFichierEnveloppe_historiquePurge;
        ');
        $this->addSql('ALTER TABLE `blocFichierEnveloppe` DROP IF EXISTS `historique_purge_id`;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `blocFichierEnveloppe`
                ADD COLUMN IF NOT EXISTS `historique_purge_id` INT NULL DEFAULT NULL AFTER `id_blob_dechiffre`,
                ADD CONSTRAINT `FK_blocFichierEnveloppe_historiquePurge` 
                FOREIGN KEY IF NOT EXISTS (`historique_purge_id`) REFERENCES `historique_purge`(`id`) ON DELETE SET NULL
            ;'
        );
        $this->addSql('ALTER TABLE `historique_purge` DROP COLUMN IF EXISTS `data`;');
        $this->addSql('ALTER TABLE `historique_purge` DROP COLUMN IF EXISTS `id_fichier_enveloppe`;');
        $this->addSql('ALTER TABLE `historique_purge` DROP COLUMN IF EXISTS `organisme`;');
        $this->addSql('ALTER TABLE `historique_purge` DROP COLUMN IF EXISTS `id_bloc_fichier_enveloppe`;');
    }
}
