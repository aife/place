<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231026180103 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23905 - [FTV] WS retournant les champs de fusion v2';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `champs_fusion_redac` ADD IF NOT EXISTS `type_fusion` VARCHAR(255)');
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.organisme'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.directionService'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.objet'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.reference'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.intitule'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'date' 
                WHERE `champ_fusion` = 'consultation.dateLimiteRemisePlis'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.lot'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.candidat.contact'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.candidat.entreprise'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.candidat.etablissement'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.candidat.raisonSociale'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.candidat.role'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'date' 
                WHERE `champ_fusion` = 'consultation.candidat.dateDepot'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.donneesComplementaires.ccag'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.donneesComplementaires.tranches'"
        );
        $this->addSql(
            "UPDATE `champs_fusion_redac` 
                SET `type_fusion` = 'string' 
                WHERE `champ_fusion` = 'consultation.donneesComplementaires.formePrix'"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `champs_fusion_redac` DROP COLUMN IF EXISTS `type_fusion`');
    }
}
