<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231016094842 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23912 - Ajout de valeurs référentielles pour les types de prix';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`) VALUES
                (4, 22, 'Prix fermes et révisables', 'Prix fermes et révisables', '', '', '', '', '', '', 'PRIX_FERME_REVISABLE', ''),
                (5, 22, 'Prix fermes et actualisables', 'Prix fermes et actualisables', '', '', '', '', '', '', 'PRIX_FERME_ACTUALISABLES', '')
	        ;"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            "DELETE FROM `ValeurReferentiel` WHERE `libelle_2` IN ('PRIX_FERME_REVISABLE', 'PRIX_FERME_ACTUALISABLES');"
        );
    }
}
