<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231010084612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23881 - [MPE][Espace Documentaire] - Impossible de rééditer un document pré-rempli après avoir fermé l onglet (OnlyOffice)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT IGNORE INTO Agent (login, email, nom, prenom, password, technique) 
                VALUES (
                        \'agent_docgen\',
                        \'agent_docgen@atexo.com\',
                        \'agent_docgen\',
                        \'agent_docgen\',
                        \'' . sha1(base64_encode((string)rand())) . '\',
                        1
                        )'
        );

    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM Agent WHERE login = \'agent_docgen\'');
    }
}
