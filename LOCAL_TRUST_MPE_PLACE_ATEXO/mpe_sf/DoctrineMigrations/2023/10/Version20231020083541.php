<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231020083541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23912 - Inverser les deux dernières variations de valeurs référentielles pour les types de prix';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "UPDATE `ValeurReferentiel` 
                SET `libelle_valeur_referentiel` = 'Prix actualisables et révisables',
                    `libelle_valeur_referentiel_fr` = 'Prix actualisables et révisables',
                    `libelle_2` = 'PRIX_ACTUALISABLE_REVISABLE' 
                WHERE `id` = 5 AND `id_referentiel` = 22"
        );

        $this->addSql(
            "UPDATE `ValeurReferentiel` 
                SET `libelle_valeur_referentiel` = 'Prix fermes et actualisables',
                    `libelle_valeur_referentiel_fr` = 'Prix fermes et actualisables',
                    `libelle_2` = 'PRIX_FERME_ACTUALISABLES' 
                WHERE `id` = 4 AND `id_referentiel` = 22"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            "UPDATE `ValeurReferentiel` 
                SET `libelle_valeur_referentiel` = 'Prix actualisables et révisables',
                    `libelle_valeur_referentiel_fr` = 'Prix actualisables et révisables',
                    `libelle_2` = 'PRIX_ACTUALISABLE_REVISABLE' 
                WHERE `id` = 4 AND `id_referentiel` = 22"
        );

        $this->addSql(
            "UPDATE `ValeurReferentiel` 
                SET `libelle_valeur_referentiel` = 'Prix fermes et actualisables',
                    `libelle_valeur_referentiel_fr` = 'Prix fermes et actualisables',
                    `libelle_2` = 'PRIX_FERME_ACTUALISABLES' 
                WHERE `id` = 5 AND `id_referentiel` = 22"
        );
    }
}
