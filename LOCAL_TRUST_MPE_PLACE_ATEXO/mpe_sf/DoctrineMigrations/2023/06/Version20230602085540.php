<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230602085540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22439 - [Place][MPE] Alerte transmise aux agents supprimés';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            UPDATE Agent SET
                 login=CONCAT(COALESCE(id,"NULL"), "_", login),
                 email=CONCAT(COALESCE(id,"NULL"), "_", email),
                 tentatives_mdp=3,
                 actif="0",      
                 deleted_at=NOW(),    
                 password="delete",
                 alerte_reponse_electronique="0", 
                 alerte_cloture_consultation="0", 
                 alerte_reception_message="0", 
                 alerte_publication_boamp="0", 
                 alerte_echec_publication_boamp="0", 
                 alerte_creation_modification_agent="0", 
                 alerte_question_entreprise="0", 
                 alerte_validation_consultation="0", 
                 alerte_chorus="0", 
                 alerte_mes_consultations="0", 
                 alerte_consultations_mon_entite="0", 
                 alerte_consultations_des_entites_dependantes="0", 
                 alerte_consultations_mes_entites_transverses="0"
            where 	old_service_id = 10000
            and deleted_at is not null
        ;');

    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
