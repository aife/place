<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230605145848 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22690 - Script de suppression de l'historique du DCE";
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `historique_purge`
                ADD COLUMN IF NOT EXISTS `id_dce` INT NULL DEFAULT NULL
            ;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `historique_purge` DROP COLUMN IF EXISTS `id_dce`;');
    }
}
