<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230622080340 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22168 - Nouvelle habilitation de duplication d'une consultation et reprise de données automatiques";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
                        ALTER TABLE HabilitationAgent 
                        ADD COLUMN IF NOT EXISTS `duplication_consultations` BOOLEAN NOT NULL DEFAULT 0');

        $this->addSql('
                        ALTER TABLE HabilitationProfil 
                        ADD COLUMN IF NOT EXISTS `duplication_consultations` BOOLEAN NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `duplication_consultations`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `duplication_consultations`;");
    }
}
