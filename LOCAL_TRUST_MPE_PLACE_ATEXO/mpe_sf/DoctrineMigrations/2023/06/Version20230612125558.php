<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230612125558 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22735 - [TNCP] Mise à jour du statut de la publicité';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi ADD annonce_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE interface_suivi ADD CONSTRAINT FK_CD66CF808805AB2F FOREIGN KEY (annonce_id) REFERENCES AnnonceBoamp (id_boamp)');
        $this->addSql('CREATE INDEX IDX_CD66CF808805AB2F ON interface_suivi (annonce_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi DROP FOREIGN KEY FK_CD66CF808805AB2F');
        $this->addSql('DROP INDEX IDX_CD66CF808805AB2F ON interface_suivi');
        $this->addSql('ALTER TABLE interface_suivi DROP annonce_id');
    }
}
