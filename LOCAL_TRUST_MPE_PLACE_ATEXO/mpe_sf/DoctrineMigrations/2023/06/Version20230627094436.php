<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230627094436 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23059 - Delete Api Entreprise Attestation AGEFIPH';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
                            UPDATE `t_document_type` SET `synchro_actif` = '0', `afficher_type_doc` = '0'
                            WHERE `code` = 'attestations_agefiph';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("
                            UPDATE `t_document_type` SET `synchro_actif` = '1', `afficher_type_doc` = '1'
                            WHERE `code` = 'attestations_agefiph';");
    }
}
