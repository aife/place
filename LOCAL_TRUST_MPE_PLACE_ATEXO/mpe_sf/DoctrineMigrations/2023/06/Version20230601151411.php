<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230601151411 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22662 - Affichage de l'entité d'achat qui opère la consultation sur l'espace fournisseurs";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_plateforme` ADD COLUMN IF NOT EXISTS `afficher_rattachement_service` TINYINT(1) NOT NULL DEFAULT 0;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` DROP `afficher_rattachement_service`');
    }
}
