<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230629130416 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22157 - Ajout de l'autoincrement dans la table T_Traduction";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE T_Traduction MODIFY id_libelle INT AUTO_INCREMENT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE T_Traduction MODIFY id_libelle INT');
    }
}
