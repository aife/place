<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230606100628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22695 - Passer en mode refrence avec UUID pour la ressource object-media ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS media_uuid (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\', media_id INT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_6A2CA10CEA9FDD75 (media_id), PRIMARY KEY(uuid)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE media_uuid ADD CONSTRAINT FK_6A2CA10CEA9FDD75 FOREIGN KEY (media_id) REFERENCES blobOrganisme_file (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS media_uuid');
    }
}
