<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231114152636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24237 - Rendre activable comme un module la partie administration des documents modèles';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
                        ALTER TABLE configuration_organisme 
                        ADD COLUMN IF NOT EXISTS `module_administration_document` BOOLEAN NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` DROP IF EXISTS `module_administration_document`;");
    }
}
