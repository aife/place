<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Ramsey\Uuid\Uuid;

final class Version20231115154419 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23692 - Modifier l id consultation de l url de la nouvelle onglet SF identité par un UUID stocké en BD ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\'');
        $this->addSql('UPDATE consultation set uuid=UUID()');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_964685A6D17F50A6 ON consultation (uuid)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_964685A6D17F50A6 ON consultation');
        $this->addSql('ALTER TABLE consultation  DROP uuid');
    }
}
