<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231103140258 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24007 - Ajout des nouvelles fonctionnalités en base de données';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE referentiel_consultation_clauses_n1 ADD COLUMN IF NOT EXISTS tooltip VARCHAR(255) DEFAULT NULL, ADD COLUMN IF NOT EXISTS actif BOOLEAN DEFAULT TRUE");
        $this->addSql("ALTER TABLE referentiel_consultation_clauses_n2 ADD COLUMN IF NOT EXISTS tooltip VARCHAR(255) DEFAULT NULL, ADD COLUMN IF NOT EXISTS actif BOOLEAN DEFAULT TRUE, ADD COLUMN IF NOT EXISTS limitation INT DEFAULT NULL");
        $this->addSql("ALTER TABLE referentiel_consultation_clauses_n3 ADD COLUMN IF NOT EXISTS tooltip VARCHAR(255) DEFAULT NULL, ADD COLUMN IF NOT EXISTS actif BOOLEAN DEFAULT TRUE");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `referentiel_consultation_clauses_n1` DROP `tooltip`, DROP `actif`');
        $this->addSql('ALTER TABLE `referentiel_consultation_clauses_n2` DROP `tooltip`, DROP `actif`, DROP `limitation`');
        $this->addSql('ALTER TABLE `referentiel_consultation_clauses_n3` DROP `tooltip`, DROP `actif`');
    }
}
