<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231121100309 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22859 - [FTV] Habilitations - Scindement de l\'habilitation "gérer les contrats"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE HabilitationAgent CHANGE gerer_contrat modifier_contrat enum(\'0\',\'1\') NOT NULL DEFAULT \'0\', ADD supprimer_contrat TINYINT(1) NOT NULL;');
        $this->addSql('ALTER TABLE HabilitationProfil CHANGE gerer_contrat modifier_contrat enum(\'0\',\'1\') NOT NULL DEFAULT \'0\', ADD supprimer_contrat TINYINT(1) NOT NULL;');
        $this->addSql('UPDATE HabilitationAgent SET supprimer_contrat = 1 WHERE modifier_contrat =\'1\';');
        $this->addSql('UPDATE HabilitationProfil SET supprimer_contrat = 1 WHERE modifier_contrat =\'1\';');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE HabilitationAgent CHANGE modifier_contrat gerer_contrat enum(\'0\',\'1\') NOT NULL DEFAULT \'0\', DROP supprimer_contrat;');
        $this->addSql('ALTER TABLE HabilitationProfil CHANGE modifier_contrat gerer_contrat enum(\'0\',\'1\') NOT NULL DEFAULT \'0\', DROP supprimer_contrat;');
    }
}
