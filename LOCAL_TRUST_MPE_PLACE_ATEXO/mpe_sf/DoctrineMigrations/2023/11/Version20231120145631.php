<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231120145631 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22585 - [FTV] Ajout de données dans consultation et modification du POST consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD besoin_recurrent TINYINT(1) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation DROP besoin_recurrent');
    }
}
