<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231122161544 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23988 - [Habilitations] Modification des habilitations \'Module EXEC\'';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE HabilitationAgent DROP IF EXISTS exec_voir_contrats');
        $this->addSql('ALTER TABLE HabilitationProfil DROP IF EXISTS exec_voir_contrats');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE HabilitationAgent ADD IF NOT EXISTS exec_voir_contrats VARCHAR(255) CHARACTER SET utf8 DEFAULT \'0\' NOT NULL COLLATE `utf8_general_ci` COMMENT \'Permet d\'\'accéder au module EXEC et visualiser ses contrats\'');
        $this->addSql('ALTER TABLE HabilitationProfil ADD IF NOT EXISTS exec_voir_contrats VARCHAR(255) CHARACTER SET utf8 DEFAULT \'0\' NOT NULL COLLATE `utf8_general_ci` COMMENT \'Permet d\'\'accéder au module EXEC et visualiser ses contrats\'');
    }
}
