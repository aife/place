<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231103085339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24102 Creation des tables et migration pour la clause niveau 4';
    }

    public function up(Schema $schema): void
    {
        //referentiel_consultation_clauses_n4
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS referentiel_consultation_clauses_n4 
                (id INT AUTO_INCREMENT NOT NULL, 
                label VARCHAR(255) NOT NULL, 
                slug VARCHAR(255) DEFAULT NULL, 
                tooltip VARCHAR(255) DEFAULT NULL, 
                type_champ VARCHAR(255) NOT NULL, 
                actif BOOLEAN NOT NULL DEFAULT false, 
                clause_n3_id INT NOT NULL, 
                CONSTRAINT FK_RefClause FOREIGN KEY (clause_n3_id) REFERENCES referentiel_consultation_clauses_n3(id), 
                PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE `referentiel_consultation_clauses_n4` ADD INDEX (`clause_n3_id`)');

        //consultation_clauses_n4
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS consultation_clauses_n4 
                (id INT AUTO_INCREMENT NOT NULL, 
                valeur VARCHAR(255) DEFAULT NULL, 
                clause_n3_id INT NOT NULL,
                referentiel_clause_n4_id INT NOT NULL,
                CONSTRAINT FK_RefConsClause FOREIGN KEY (referentiel_clause_n4_id) 
                                            REFERENCES referentiel_consultation_clauses_n4(id),
                CONSTRAINT FK_ConsClause FOREIGN KEY (clause_n3_id) REFERENCES consultation_clauses_n3(id),
                PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql('ALTER TABLE `consultation_clauses_n4` ADD INDEX (`clause_n3_id`)');
        $this->addSql('ALTER TABLE `consultation_clauses_n4` ADD INDEX (`referentiel_clause_n4_id`)');

        $this->addSql("INSERT INTO `referentiel_consultation_clauses_n4` 
                           (`id`, `label`, `slug`, `tooltip`, `actif`, `clause_n3_id`, `type_champ`) 
                           VALUES (NULL, 
                                   'DEFINE_NOMBRE_HEURE_INSERTION', 
                                   'heureInsertionActiviteEconomique', 
                                   '', 
                                   '0', 
                                   (SELECT id FROM `referentiel_consultation_clauses_n3` 
                                              WHERE slug = 'insertionActiviteEconomique'), 
                                   'int')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE consultation_clauses_n4');
        $this->addSql('DROP TABLE referentiel_consultation_clauses_n4');
    }
}
