<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230316131026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-21868 - Ajout d'une liste référentiel dans l'écran création d'un échange";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `echange_doc` 
                ADD IF NOT EXISTS referentiel_sous_type_parapheur_id INT DEFAULT NULL
        ");
        $this->addSql("
            CREATE INDEX IF NOT EXISTS IDX_referentiel_sous_type_parapheur_id 
                ON `echange_doc` (`referentiel_sous_type_parapheur_id`);
        ");
        $this->addSql("
            ALTER TABLE echange_doc
                ADD CONSTRAINT FK_ECHANGEDOC_PARAPHEUR FOREIGN KEY IF NOT EXISTS (referentiel_sous_type_parapheur_id)
                REFERENCES referentiel_sous_type_parapheur (id)
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `echange_doc` DROP FOREIGN KEY IF EXISTS FK_ECHANGEDOC_PARAPHEUR;");
        $this->addSql("DROP INDEX IF EXISTS IDX_referentiel_sous_type_parapheur_id on echange_doc;");
        $this->addSql("ALTER TABLE `echange_doc` DROP IF EXISTS `referentiel_sous_type_parapheur_id`;");
    }
}
