<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230314162526 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-21867 - Ajout d'une table référentiel (referentiel_sous_type_parapheur)";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS `referentiel_sous_type_parapheur` (
                `id` INT NOT NULL AUTO_INCREMENT,
                `code` VARCHAR(100) NOT NULL,
                `label` VARCHAR(255) NOT NULL,
                PRIMARY KEY (`id`)
            ) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
            ;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS `referentiel_sous_type_parapheur`;');
    }
}
