<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230317110155 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21849 - Nouvelle habilitation ECO et SIP et reprise de données automatiques';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE `HabilitationAgent` 
            ADD COLUMN IF NOT EXISTS `gestion_validation_eco` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationProfil`
            ADD COLUMN IF NOT EXISTS `gestion_validation_eco` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationAgent` 
            ADD COLUMN IF NOT EXISTS `gestion_validation_sip` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationProfil`
            ADD COLUMN IF NOT EXISTS `gestion_validation_sip` TINYINT(1) NOT NULL DEFAULT 0
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP IF EXISTS `gestion_validation_eco`');
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP IF EXISTS `gestion_validation_eco`');
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP IF EXISTS `gestion_validation_sip`');
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP IF EXISTS `gestion_validation_sip`');
    }
}
