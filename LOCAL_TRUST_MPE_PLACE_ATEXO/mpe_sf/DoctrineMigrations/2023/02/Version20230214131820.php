<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230214131820 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-1021 - [PLACE][CHORUS] - Ajout du bouton : Statuts Chorus';
    }

    public function up(Schema $schema): void
    {
        $value = (bool)getenv('SPECIFIQUE_PLACE') ? 1 : 0;
        $this->addSql(
            "INSERT INTO `configuration_client` (`parameter`, `value`, `updated_at`) VALUES
('ACTIVATION_CHORUS_POUR_ARCHIVES', '" . $value . "', NOW());"
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            "DELETE FROM  `configuration_client` WHERE `parameter` like 'ACTIVATION_CHORUS_POUR_ARCHIVES';"
        );
    }
}
