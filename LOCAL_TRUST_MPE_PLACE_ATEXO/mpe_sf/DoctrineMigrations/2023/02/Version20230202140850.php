<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230202140850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21618 - Aller chercher dynamiquement les référentiels des clauses dans la migration des clauses';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug`= 'conditionsExecutions' WHERE `label`= 'DEFINE_CONDITIONS_EXECUTION_ARTICLE_14';");
    }

    public function down(Schema $schema): void
    {
    }
}
