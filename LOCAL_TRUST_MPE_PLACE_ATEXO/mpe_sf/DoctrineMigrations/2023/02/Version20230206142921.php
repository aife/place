<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230206142921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-686 - [PLACE] [MPE] [Création d une consultation] Mauvais jour des opérations de maintenance';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("UPDATE `configuration_messages_traduction` SET `target` = 'Les opérations de maintenance de la plateforme ont généralement lieu le mardi soir entre 19h et 20h. Pendant ces quelques heures, la plateforme est indisponible, les opérateurs économiques ne peuvent pas déposer d’offre. Ainsi, les dates limites de remise des plis intervenant le mardi après 17h ou très tôt le mardi matin ne sont pas recommandées.' WHERE `configuration_messages_traduction`.`source` = 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' and `configuration_messages_traduction`.`langue_id` = 1;");
    }

    public function down(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("UPDATE `configuration_messages_traduction` SET `target` = 'Les opérations de maintenance de la plateforme ont généralement lieu le mercredi soir entre 19h et 22h. Pendant ces quelques heures, la plateforme est indisponible, les opérateurs économiques ne peuvent pas déposer d’offre.	Ainsi, les dates limites de remise des plis intervenant le mercredi après 17h ou très tôt le jeudi matin ne sont pas recommandées.' WHERE `configuration_messages_traduction`.`source` = 'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' and `configuration_messages_traduction`.`langue_id` = 1;");
    }
}
