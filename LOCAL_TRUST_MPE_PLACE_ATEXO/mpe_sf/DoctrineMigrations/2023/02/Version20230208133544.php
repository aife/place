<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230208133544 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-995 - [PLACE] RdR 2023-00.01.00 - Reliquats CGU';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        // ----- Agent -----
        $this->addSql('DELETE FROM `configuration_messages_traduction` WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION_AGENT" and `langue_id` = 1;');
        $this->addSql("INSERT INTO `configuration_messages_traduction` (`source`, `target`, `langue_id`) 
            VALUES (
                'TEXT_APPROBATION_APPLICATION_AGENT',
                'Les termes des présentes conditions générales d\'utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées aux fonctionnalités, de l\'évolution de la réglementation ou pour tout autre motif jugé nécessaire. Il appartient à l\'utilisateur de s\'informer des conditions générales d\'utilisation de PLACE en vigueur.',
                '1'
                )
            "
        );

        $this->addSql(
            "UPDATE `configuration_messages_traduction` 
                SET `target` = 'Le profil d’acheteur dénommé « Plate-forme des achats de l\'Etat » (ci-après « PLACE ») est un site du Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique, édité par la Direction des Achats de l\'Etat (DAE) et mis en œuvre par l\'Agence pour l\'informatique financière de l\'Etat (AIFE).<br/><br/>
Il constitue le profil d’acheteur des marchés publics de l\'Etat et permet aux agents publics de créer notamment des consultations et de les mettre en ligne à disposition des opérateurs économiques.<br/><br/>
Cette page a pour objet de définir, d\'une part, les conditions dans lesquelles le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique met le site à la disposition des utilisateurs, et d\'autre part, la manière dont l\'utilisateur accède au profil d’acheteur et à ses fonctionnalités.'
                WHERE `source` = 'TEXT_CONDITION_UTILISATION' AND `langue_id` = '1';
        ");

        $this->addSql(
            "UPDATE `configuration_messages_traduction` 
                SET `target` = 'L\'accès à PLACE est ouvert à tout opérateur économique. Son utilisation est gratuite.
<br/><br/>
Lors de l\'inscription, le nouvel utilisateur est invité à renseigner son identifiant national (Ex : SIRET, N° d\'enregistrement national, DUNS Number, numéro local, etc.) et ses coordonnées professionnelles. Le premier utilisateur de l’entreprise est enregistré comme administrateur.
<br/><br/>
L\'utilisateur choisit un identifiant et un mot de passe qu’il doit conserver car ils lui seront indispensables pour accéder à son compte professionnel sur PLACE.
<br/><br/>
Le mot de passe doit être choisi par l\'utilisateur de façon qu\'il ne puisse pas être deviné par un tiers en respectant les règles de sécurité précisées sur la page de création de compte. L\'utilisateur s\'engage à en préserver la confidentialité. L\'utilisateur s\'engage à avertir immédiatement l’assistance en ligne (accessible depuis chaque page via la languette située à droite de l’écran) de toute utilisation non autorisée de ses informations professionnelles, le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique ne pouvant être tenu pour responsable des dommages éventuellement causés par l\'utilisation des codes d\'accès par une personne non autorisée.
<br/><br/>
L\'utilisateur de PLACE doit fournir une adresse électronique valide lors de son inscription pour recevoir les communications des acheteurs publics et les alertes de la plateforme.
<br/><br/>
Le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique se réserve le droit de résilier tout compte de PLACE faisant l\'objet d\'une utilisation illicite ou frauduleuse avérée. <br/><br/>Afin de se désinscrire, l\'utilisateur doit contacter le support PLACE en réalisant une demande d’assistance en ligne (accessible depuis chaque page via la languette située à droite de l’écran), pour préciser le compte utilisateur ou le compte entreprise qu\'il souhaite supprimer, et joindre un extrait du registre pertinent tel un extrait K, K bis, D 1 ou un document équivalent délivré par l’autorité judiciaire ou administrative du pays d’origine ou d’établissement de l’utilisateur à sa demande.
'
                WHERE `source` = 'TEXT_UTILISATION_MESSAGERIE_SECURISEE' AND `langue_id` = '1';
        ");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`source`, `target`, `langue_id`) 
            VALUES (
                'LISTE_DES_CERTIFICATS_RGS',
                    'PLACE est disponible 7 jours sur 7, 24h sur 24h. Le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique se réserve le droit de faire évoluer, de modifier ou de suspendre, sans préavis les fonctionnalités pour des raisons de maintenance ou pour tout autre motif jugé nécessaire.
<br/><br/>
En cas d’indisponibilité planifiée, un message mentionnant la durée de l’interruption du service est affiché sur la page d’accueil en amont de l’intervention.
',
                '1'
                )
            "
        );

        $this->addSql(
            "UPDATE `configuration_messages_traduction` 
                SET `target` = 'PLACE est disponible 7 jours sur 7, 24h sur 24h. Le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique se réserve le droit de faire évoluer, de modifier ou de suspendre, sans préavis les fonctionnalités pour des raisons de maintenance ou pour tout autre motif jugé nécessaire.
<br/><br/>
En cas d’indisponibilité planifiée, un message mentionnant la durée de l’interruption du service est affiché sur la page d’accueil en amont de l’intervention.
'
                WHERE `source` = 'LISTE_DES_CERTIFICATS_RGS' AND `langue_id` = '1';
        ");

        $this->addSql(
            "UPDATE `configuration_messages_traduction` 
                SET `target` = '<h3>Engagements et responsabilité</h3>

La sécurité des données collectées auprès de l\'utilisateur est garantie par le profil d’acheteur, les solutions adéquates ont été mises en œuvre pour empêcher qu\'elles soient modifiées, endommagées ou que des tiers non autorisés y aient accès.
<br/><br/>
L\'utilisateur de PLACE s\'engage à fournir des informations exactes, à jour et complètes. Dans l\'hypothèse où l\'utilisateur ne s\'acquitterait pas de cet engagement, le Ministère de l\'Économie, des Finances et de la Souveraineté industrielle et numérique se réserve le droit de suspendre ou de résilier l’accès aux fonctionnalités. D’éventuelles actions en responsabilité pénale et civile pourraient être engagées à son encontre.
<br/><br/>
Il est rappelé que toute personne procédant à une fausse déclaration pour elle-même ou pour autrui s\'expose, notamment, aux sanctions prévues à l\'article 441-1 du code pénal, prévoyant des peines pouvant aller jusqu\'à trois ans d\'emprisonnement et 45 000 euros d\'amende.
<br/><br/>
Conformément aux dispositions de l\'article L112-9 du code des relations entre le public et l\'administration, les présentes conditions générales s\'imposent à tout utilisateur de PLACE.

<h3>Juridiction compétente</h3>

Les présentes conditions sont soumises au droit français. En cas de litige, et si un accord amiable n\'a pu être conclu, les tribunaux français sont compétents.

<h3>Mentions légales</h3>

Les mentions légales sont disponibles <a href=\'https://mpe-docker.local-trust.com/entreprise/footer/info-site\'>ici</a>.'
                WHERE `source` = 'TEXT_DIFFICULTE_CERTIFICAT_ELECTRONIQUE' AND `langue_id` = '1';
        ");


        // ----- Entreprise -----
        $this->addSql('DELETE FROM `configuration_messages_traduction` WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION" and `langue_id` = 1;');

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
            VALUES (
                    NULL,
                    'TEXT_APPROBATION_APPLICATION',
                    'Les termes des présentes conditions générales d\'utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées aux fonctionnalités, de l\'évolution de la réglementation ou pour tout autre motif jugé nécessaire. Il appartient à l\'utilisateur de s\'informer des conditions générales d\'utilisation de la plateforme PLACE en vigueur. <br/><br/> L\'utilisation de PLACE nécessite de disposer d\'un environnement informatique compatible et de se conformer aux prérequis techniques.
<h3>Avertissement et recommandation aux entreprises</h3>
    Les questions ou demandes d’informations complémentaires relatives à une consultation peuvent être posées via le profil d’acheteur ou, le cas échéant, à une adresse mail mentionnée par l’acheteur dans les documents de la consultation.
<br/><br/>
Afin de conserver une trace des échanges, il est recommandé de recourir au service de messagerie du profil d’acheteur. Si un opérateur économique, après avoir pris connaissance de l’ensemble des documents de la consultation, pose une question et que la réponse a un impact sur la remise des candidatures ou des offres, la réponse est communiquée à tous les opérateurs économiques ayant téléchargé les documents de consultation avec authentification, et sera visible de tous les opérateurs économiques n’ayant pas encore téléchargé les documents.
<br/><br/>
Les plis doivent être reçus dans les délais fixés dans l’avis de publicité ou dans les autres documents de la consultation.
<br/><br/>
L’opérateur économique doit donc prévoir le temps nécessaire pour que son pli soit reçu dans le délai fixé par l’acheteur, surtout si les fichiers sont volumineux et/ou si le réseau a un faible débit. Il lui est ainsi conseillé de déposer ses plis au minimum quatre heures avant la date limite de remise des plis afin de garantir son envoi dématérialisé et lui permettre de remédier à d’éventuels problèmes techniques.
<br/><br/>
Par ailleurs, les opérateurs économiques sont alertés que la plateforme déconnecte automatiquement l’utilisateur en cas d’inactivité supérieure à trente minutes.
<br/><br/>
Attention, les plis, dont le téléchargement a commencé avant la date et l’heure limite mais s’est achevé hors délai sont éliminés par l’acheteur.
<br/><br/>
L’opérateur économique est invité à remettre une copie de sauvegarde dans les conditions fixées dans le règlement de la consultation afin d’anticiper toute difficulté de dernière minute. En effet, en cas de réception d’un pli électronique hors délai, l’acheteur est tenu d’ouvrir la copie de sauvegarde régulièrement transmise.
<h3>Certificat électronique</h3>
    Un certificat électronique permet de prouver son identité et de signer électroniquement les offres avec la même valeur juridique qu\'une signature manuscrite.
<br/><br/>
L’annexe 12 du code de la commande publique relatif à la signature électronique des contrats de la commande publique prévoit que le certificat doit être qualifié conformément au règlement n° 910/2014 du parlement européen et du conseil du 23 juillet 2014 sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur et abrogeant la directive 1999/93/CE dit « eIDAS ».
<br/><br/>
Pour l\'obtention d\'un certificat électronique, la liste des prestataires de services de confiance qualifiés délivrant des certificats de signatures conformes à la réglementation est accessible sur la page des autorités de certification européennes.
<h3>Assistance en ligne</h3>
Un service de support est à votre disposition de 9h00 à 19h00 les jours ouvrés pour répondre à vos questions ou vous assister en cas de difficultés (consulter la page dédiée). L’assistance en ligne est accessible depuis chaque page via la languette située à droite de l’écran.', 
                    '1'
                    );
        ");


    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM `configuration_messages_traduction` WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION" and `langue_id` = 1;');
        $this->addSql('DELETE FROM `configuration_messages_traduction` WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION_AGENT" and `langue_id` = 1;');
    }

}
