<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231206200412 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24070 - Ajout d\'un nouveau mode d\'authentification keycloak-atexo (configuration platefrome)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_plateforme ADD authenticate_agent_by_internal_keycloak TINYINT(1) DEFAULT 1 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_plateforme  DROP authenticate_agent_by_internal_keycloak');
    }
}
