<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20231205133355 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24394 - [AMPA] Affichage du menu déroulant "Type de procédure" KO et contenu du menu KO';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` ADD INDEX IF NOT EXISTS `type_procedure_organisme_organisme_index` (`organisme`);');
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` ADD INDEX IF NOT EXISTS `type_procedure_organisme_id_type_procedure_portail_index` (`id_type_procedure_portail`);');
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` ADD INDEX IF NOT EXISTS `type_procedure_organisme_libelle_type_procedure_index` (`libelle_type_procedure`);');

        $this->addSql('ALTER TABLE `t_type_contrat_et_procedure` ADD INDEX IF NOT EXISTS `t_type_contrat_et_procedure_id_type_contrat_index` (`id_type_contrat`);');
        $this->addSql('ALTER TABLE `t_type_contrat_et_procedure` ADD INDEX IF NOT EXISTS `t_type_contrat_et_procedure_id_type_procedure_index` (`id_type_procedure`);');
        $this->addSql('ALTER TABLE `t_type_contrat_et_procedure` ADD INDEX IF NOT EXISTS `t_type_contrat_et_procedure_organisme_index` (`organisme`);');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP INDEX IF EXISTS `type_procedure_organisme_organisme_index` on `Type_Procedure_Organisme`;');
        $this->addSql('DROP INDEX IF EXISTS `type_procedure_organisme_id_type_procedure_portail_index` on `Type_Procedure_Organisme`;');
        $this->addSql('DROP INDEX IF EXISTS `type_procedure_organisme_libelle_type_procedure_index` on `Type_Procedure_Organisme`;');

        $this->addSql('DROP INDEX IF EXISTS `t_type_contrat_et_procedure_id_type_contrat_index` on `t_type_contrat_et_procedure`;');
        $this->addSql('DROP INDEX IF EXISTS `t_type_contrat_et_procedure_id_type_procedure_index` on `t_type_contrat_et_procedure`;');
        $this->addSql('DROP INDEX IF EXISTS `t_type_contrat_et_procedure_organisme_index` on `t_type_contrat_et_procedure`;');
    }
}
