<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231214150856 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-24075 - Récupération de la liste contrats EXEC lors de la préparation d'un échange documentaire";
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `echange_doc` ADD COLUMN IF NOT EXISTS `uuid_externe_exec` VARCHAR(50) NULL;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `echange_doc` DROP IF EXISTS `uuid_externe_exec`;');
    }
}
