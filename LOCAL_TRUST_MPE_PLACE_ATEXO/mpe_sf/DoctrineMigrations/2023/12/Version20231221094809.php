<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231221094809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23825 - Echanges Chorus : Récupérer les infos du contrat dans EXEC';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `Chorus_echange` ADD COLUMN IF NOT EXISTS `uuid_externe_exec` VARCHAR(50) NULL;'
        );
        $this->addSql(
            'ALTER TABLE `t_donnees_consultation` ADD COLUMN IF NOT EXISTS `uuid_externe_exec` VARCHAR(50) NULL;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Chorus_echange` DROP IF EXISTS `uuid_externe_exec`;');
        $this->addSql('ALTER TABLE `t_donnees_consultation` DROP IF EXISTS `uuid_externe_exec`;');
    }
}
