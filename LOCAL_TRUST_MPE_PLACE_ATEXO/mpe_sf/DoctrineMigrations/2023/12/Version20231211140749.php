<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * MPE-24454 - [PLACE] Interface B1 - renommage d'action "MODIFICATION_CONSULTATION"
 */
final class Version20231211140749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            UPDATE `interface_suivi`
            SET `action` = REPLACE(`action`, "MODIFICATION_CONSULTATION", "MODIFICATION_DATE_LIMITE_REMISE_PLI");
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('
            UPDATE `interface_suivi` 
            SET `action` = REPLACE(`action`, "MODIFICATION_DATE_LIMITE_REMISE_PLI", "MODIFICATION_CONSULTATION");
        ');
    }
}
