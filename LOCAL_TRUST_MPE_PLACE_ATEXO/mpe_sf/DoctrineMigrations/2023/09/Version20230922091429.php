<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230922091429 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-23300 - [SIMAP] Champs en trop dans la création d'une consultation";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `configuration_plateforme` 
                ADD COLUMN IF NOT EXISTS `conf_publicite_francaise` TINYINT(1) NOT NULL DEFAULT 1
                COMMENT 'Configure l’affichage des blocs de données lié à la publicité, lors de la création d’une consultation : DC/Condition de participation, DC/valeur estimé, DC/favoris, DC/adresse et correspondant, Identification / Groupement de commandes;'
        ;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` DROP `conf_publicite_francaise`');
    }
}
