<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230922120942 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23579 - [FTV] Gestion des habilitations "Administration de documents modèles"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
                        ALTER TABLE HabilitationAgent 
                        ADD COLUMN IF NOT EXISTS `administration_documents_modeles` BOOLEAN NOT NULL DEFAULT 0');

        $this->addSql('
                        ALTER TABLE HabilitationProfil 
                        ADD COLUMN IF NOT EXISTS `administration_documents_modeles` BOOLEAN NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `administration_documents_modeles`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `administration_documents_modeles`;");
    }
}
