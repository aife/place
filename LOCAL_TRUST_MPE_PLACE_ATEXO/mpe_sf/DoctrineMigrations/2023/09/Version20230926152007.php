<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230926152007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23589  [FTV] Identification de l\'acheteur du projet d\'achat de Recensement dans le profil acheteur côté MPE';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD IF NOT EXISTS agent_technique_createur INT DEFAULT NULL');
        $this->addSql('ALTER TABLE consultation ADD CONSTRAINT FK_Consultation_AgentTechniqueCreateur FOREIGN KEY IF NOT EXISTS (agent_technique_createur) REFERENCES Agent (id)');
        $this->addSql('CREATE INDEX IF NOT EXISTS IDX_agent_technique_createur ON consultation (agent_technique_createur)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation DROP FOREIGN KEY IF EXISTS FK_Consultation_AgentTechniqueCreateur');
        $this->addSql('DROP INDEX IF EXISTS IDX_agent_technique_createur ON consultation');
        $this->addSql('ALTER TABLE consultation DROP COLUMN IF EXISTS agent_technique_createur');
    }
}
