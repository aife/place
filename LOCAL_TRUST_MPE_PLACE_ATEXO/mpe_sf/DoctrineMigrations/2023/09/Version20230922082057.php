<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230922082057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23610 - WS retournant les champs de fusion';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS `champs_fusion_redac` (
                `id` SERIAL PRIMARY KEY,
                `libelle` VARCHAR(255) NOT NULL,
                `champ_fusion` VARCHAR(255) NOT NULL,
                `actif` BOOLEAN DEFAULT TRUE NOT NULL,
                created_at  DATETIME NOT NULL,
                updated_at DATETIME NULL
            ) DEFAULT CHARACTER SET UTF8 COLLATE `utf8_general_ci` ENGINE = InnoDB
            ;
        ");

        $this->addSql('TRUNCATE TABLE `champs_fusion_redac`');
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Organisme de la consultation', 'consultation.organisme', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Service direction de la consultation', 'consultation.directionService', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Objet de la consultation', 'consultation.objet', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Réference de la consultation', 'consultation.reference', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Intitulé de la consultation', 'consultation.intitule', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Date limite de remise des plis de la consultation', 'consultation.dateLimiteRemisePlis', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Lot(s) de la consultation', 'consultation.lot', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Contact de l''offre : Prénom', 'candidat.contact.prenom', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Contact de l''offre : Nom', 'candidat.contact.nom', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Contact de l''offre : Email', 'candidat.contact.email', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Contact de l''offre : Télécopie', 'candidat.contact.telecopie', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Contact de l''offre : Téléphone', 'candidat.contact.telephone', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Entreprise de l''offre : Rue', 'candidat.entreprise.rue', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Entreprise de l''offre : Code postal', 'candidat.entreprise.codePostal', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Entreprise de l''offre : Ville', 'candidat.entreprise.ville', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Entreprise de l''offre : Pays', 'candidat.entreprise.pays', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Etablissement de l''offre : Siret', 'candidat.etablissement.siret', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Etablissement de l''offre : Rue', 'candidat.etablissement.rue', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Etablissement de l''offre : Code postal', 'candidat.etablissement.codePostal', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Etablissement de l''offre : Ville', 'candidat.etablissement.ville', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Etablissement de l''offre : Pays', 'candidat.etablissement.pays', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Raison sociale de l''offre', 'candidat.raisonSociale', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Role', 'candidat.role', NOW());");

        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Date et heure des dépôts', 'candidat.dateDepot', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Durée maximale du marché : DUrée', 'donneesComplementaires.dureeMarche.dureeValue', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Durée maximale du marché: Type de la durée', 'donneesComplementaires.dureeMarche.dureeType', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Durée maximale du marché: Description libre', 'donneesComplementaires.dureeMarche.descriptionLibre', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Délai de validité des offres', 'donneesComplementaires.delaiValiditeOffres', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Nombre de candidats admis à présenter une offre : Nombre fixe', 'donneesComplementaires.nombreCandidats.nombreFixe', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Nombre de candidats admis à présenter une offre : Nombre minimum', 'donneesComplementaires.nombreCandidats.nombreMin', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Nombre de candidats admis à présenter une offre : Nombre maximum', 'donneesComplementaires.nombreCandidats.nombreMax', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('CCAG de référence', 'donneesComplementaires.ccag', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Variante autorisée', 'donneesComplementaires.varianteAutorisee', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Variante exigée', 'donneesComplementaires.varianteExigee', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Possibilité d’attribution sans négociation ', 'consultation.donneesComplementaires.attributionSansNegociation', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Reconduction et modalités de reconduction : Nombre', 'donneesComplementaires.reconduction.nombre', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Reconduction et modalités de reconduction : Modalités', 'donneesComplementaires.reconduction.modalite', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Prestations supplémentaires éventuelles : Activation', 'donneesComplementaires.prestationsSupplementaires.enabled', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Prestations supplémentaires éventuelles : Description', 'donneesComplementaires.prestationsSupplementaires.description', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Tranches', 'donneesComplementaires.tranches', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Forme de prix et variation du prix', 'donneesComplementaires.formePrix', NOW());");
        $this->addSql("INSERT INTO `champs_fusion_redac` (`libelle`, `champ_fusion`, `created_at`) VALUES ('Critères d’attribution', 'donneesComplementaires.criteres', NOW());");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS `champs_fusion_redac`;');
    }
}
