<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230920091809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22228 - Habilitation Recensement et reprise de données automatiques';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE `HabilitationAgent` 
            ADD COLUMN IF NOT EXISTS `recensement_invalider_projet_achat` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationProfil`
            ADD COLUMN IF NOT EXISTS `recensement_invalider_projet_achat` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationAgent` 
            ADD COLUMN IF NOT EXISTS `recensement_annuler_projet_achat` TINYINT(1) NOT NULL DEFAULT 0
        ');
        $this->addSql('
            ALTER TABLE `HabilitationProfil`
            ADD COLUMN IF NOT EXISTS `recensement_annuler_projet_achat` TINYINT(1) NOT NULL DEFAULT 0
        ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP IF EXISTS `recensement_invalider_projet_achat`');
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP IF EXISTS `recensement_invalider_projet_achat`');
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP IF EXISTS `recensement_annuler_projet_achat`');
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP IF EXISTS `recensement_annuler_projet_achat`');
    }
}
