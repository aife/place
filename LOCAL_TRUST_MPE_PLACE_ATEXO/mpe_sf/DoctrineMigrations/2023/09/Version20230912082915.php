<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230912082915 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23643 - Supression du champs offset non utilisé qui provoque une erreur avec la version de mariadb';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Organisme DROP COLUMN `offset`;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Organisme ADD `offset` CHAR(3) DEFAULT '0' NOT NULL;");
    }
}
