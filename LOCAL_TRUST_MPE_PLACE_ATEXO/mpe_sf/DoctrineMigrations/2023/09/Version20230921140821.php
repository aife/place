<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230921140821 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23717 - [SIMAP REC eForm] Module de publicité - Suppression publier un format libre';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `configuration_plateforme` 
                ADD COLUMN IF NOT EXISTS `publication_format_libre` TINYINT(1) NOT NULL DEFAULT 1;
");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` DROP `publication_format_libre`');
    }
}
