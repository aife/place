<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230801132855 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22938 - Enrichissement de l\'API Post consultations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD IF NOT EXISTS  referentiel_achat_id INT DEFAULT NULL, ADD CONSTRAINT FK_964685A64CA8C785 FOREIGN KEY (referentiel_achat_id) REFERENCES referentiel_achat (id)');
        $this->addSql('CREATE INDEX IDX_964685A64CA8C785 ON consultation (referentiel_achat_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation DROP FOREIGN KEY IF EXISTS FK_964685A64CA8C785, DROP COLUMN IF EXISTS referentiel_achat_id ');
        $this->addSql('DROP INDEX IDX_964685A64CA8C785 ON consultation');
    }
}
