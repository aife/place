<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230802084122 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23118 - Gestion des habilitations Recensement des Besoins (RdB)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
                        ALTER TABLE HabilitationAgent 
                        ADD COLUMN IF NOT EXISTS `projet_achat_lancement_sourcing` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `projet_achat_invalidation` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `projet_achat_annulation` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `lancement_procedure` BOOLEAN NOT NULL DEFAULT 0');


        $this->addSql('
                        ALTER TABLE HabilitationProfil 
                        ADD COLUMN IF NOT EXISTS `projet_achat_lancement_sourcing` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `projet_achat_invalidation` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `projet_achat_annulation` BOOLEAN NOT NULL DEFAULT 0,
                        ADD COLUMN IF NOT EXISTS `lancement_procedure` BOOLEAN NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `projet_achat_lancement_sourcing`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `projet_achat_lancement_sourcing`;");

        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `projet_achat_invalidation`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `projet_achat_invalidation`;");

        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `projet_achat_annulation`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `projet_achat_annulation`;");

        $this->addSql("ALTER TABLE `HabilitationAgent` DROP IF EXISTS `lancement_procedure`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP IF EXISTS `lancement_procedure`;");
    }
}
