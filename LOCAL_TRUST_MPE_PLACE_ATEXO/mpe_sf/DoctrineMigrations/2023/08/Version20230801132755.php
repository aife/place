<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230801132755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22938 - Enrichissement de l\'API Post consultations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS referentiel_achat (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(30) NOT NULL, libelle VARCHAR(255) NOT NULL, recurrence TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS referentiel_achat');
    }
}
