<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230411131657 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22318 - Ajout en BDD d'un nouveau champ par organisme indiquant la configuration PUB";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN IF NOT EXISTS `pub_tncp` BOOLEAN NOT NULL DEFAULT 0, 
    ADD COLUMN IF NOT EXISTS `pub_mol` BOOLEAN NOT NULL DEFAULT 1, 
    ADD COLUMN IF NOT EXISTS `pub_jal_fr` BOOLEAN NOT NULL DEFAULT 1, 
    ADD COLUMN IF NOT EXISTS `pub_jal_lux` BOOLEAN NOT NULL DEFAULT 0, 
    ADD COLUMN IF NOT EXISTS `pub_joue` BOOLEAN NOT NULL DEFAULT 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `pub_tncp`');
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `pub_mol`');
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `pub_jal_fr`');
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `pub_jal_lux`');
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `pub_joue`');
    }
}
