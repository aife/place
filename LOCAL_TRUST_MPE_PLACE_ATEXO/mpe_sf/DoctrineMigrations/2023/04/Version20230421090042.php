<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230421090042 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22286 - Nettoyage de paramétrage de l'ancien  SGMAP";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationFiscaleWebService' WHERE `code` = 'attestation_fiscale';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationSocialeWebService' WHERE `code` = 'attestation_sociale_mp';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationSocialeWebService' WHERE `code` = 'attestation_sociale_vg';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\CarteProfessionnelleWebService' WHERE `code` = 'carte_professionnelle';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\DocumentWebService' WHERE `code` = 'certificat_cotisation';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\CertificatQualibatWebService' WHERE `code` = 'certificat_qualibat';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationCotisationRetraiteWebService' WHERE `code` = 'attestation_cotisation_retraite';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationCertificatQualificationWebService' WHERE `code` = 'certificats_qualifications';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\CotisationEmployeurWebService' WHERE `code` = 'cotisation_msa';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceApiGouvEntreprise\\\AttestationAgefiphWebService' WHERE `code` = 'attestations_agefiph';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationFiscaleWebService' WHERE `code` = 'attestation_fiscale';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationSocialeWebService' WHERE `code` = 'attestation_sociale_mp';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationSocialeWebService' WHERE `code` = 'attestation_sociale_vg';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\CarteProfessionnelleWebService' WHERE `code` = 'carte_professionnelle';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\DocumentWebService' WHERE `code` = 'certificat_cotisation';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\CertificatQualibatWebService' WHERE `code` = 'certificat_qualibat';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationCotisationRetraiteWebService' WHERE `code` = 'attestation_cotisation_retraite';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationCertificatQualificationWebService' WHERE `code` = 'certificats_qualifications';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\CotisationEmployeurWebService' WHERE `code` = 'cotisation_msa';");
        $this->addSql("UPDATE `t_document_type` SET `class_name` = 'Application\\\Service\\\Atexo\\\WebServiceSGMAP\\\AttestationAgefiphWebService' WHERE `code` = 'attestations_agefiph';");
    }
}
