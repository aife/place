<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230418122800 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22317 - Envoi des informations au concentrateur';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT IGNORE INTO Agent (login, email, nom, prenom, password, technique) 
                VALUES (
                        \'agent_concentrateur\',
                        \'agent_concentrateur@atexo.com\',
                        \'agent_concentrateur\',
                        \'agent_concentrateur\',
                        \'' . sha1(base64_encode((string)rand())) . '\', 1
                        )');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM Agent WHERE login = \'agent_concentrateur\'');
    }
}
