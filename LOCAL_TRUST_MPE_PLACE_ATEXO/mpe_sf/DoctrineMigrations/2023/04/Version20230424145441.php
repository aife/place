<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20230424145441 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21622 -GrandEst - La partie "Dossier Volumineux" 
        n\'est pas traduit en anglais/allemand + affichage du bouton "Ajouter un dossier" KO';
    }

    public function up(Schema $schema): void
    {
        /**
         * Traduction Allemand
         */
        $sql = 'select * FROM `Langue` where langue ="du"';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        if (0 < $stmt->rowCount()) {
            $row = $stmt->fetch();
            $idLanguage = $row['id_langue'];
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TITLE', 'Meine hochgeladenen großen Dateien', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_INFO', 'Von dieser Seite aus können Sie große Ordner versenden, die an Ihre Antworten sowie an Ihren Austausch im sicheren Nachrichtensystem angehängt werden können. Wenn Sie einen großen Ordner hinzufügen, wird der gesamte Inhalt (Ordner und Dateien) des von Ihnen ausgewählten lokalen Verzeichnisses an die Plattform gesendet.', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_LIBELLE', 'Mein Etikett', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_IDENTIFIANT', 'Eindeutige ID', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_TAILLE', 'Größe', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_DATE', 'Datum der Hinzufügung (Upload)', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_ACTION', 'Aktion/Status', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'INFO_STATUT_DOSSIER_VOLUMINEUX', 'Bei der Auswahl einer großen Datei in den verschiedenen Funktionen der Plattform. Es werden ', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_LINK', 'Einen großen Ordner hinzufügen', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_EMPTY', 'Sie haben keine umfangreichen Ordner.', '".$idLanguage."');"
            );


            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_DOSSIER_VOLUMINEUX', 'Herunterladen des umfangreichen Dossiers', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_JOURNAL_DE_LOGS_DOSSIER_VOLUMINEUX', 'Herunterladen des Logbuchs dieses umfangreichen Ordners', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_ANNEXES_TECHNIQUES_DCE', 'Herunterladen der technischen Anhänge zum DCE', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE', '<div class=\"alert alert-info bloc-message form-bloc-conf msg-info\">
                                <i class=\"fa fa-info-circle m-r-1 hide-agent\"></i>
                               Das Herunterladen eines großen Ordners erfordert die Verwendung eines Dienstprogramms, das auf Ihren Rechner heruntergeladen werden muss.
                                <div class=\"m-l-3\">
                                   Eine Anleitung zur Installation und Verwendung des Dienstprogramms zum Herunterladen großer Ordner wird auf dieser
                                    <a target=\"_blank\" href=\"/?page=Commun.EntrepriseGuideUtilitaireTelechargementDV\">
                                      Seite bereitgestellt.
                                    </a>.
                                </div>
                            </div>', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TEXT_TELECHARGEMENT_FICHIER_ATEXO', 'Laden Sie die .envol-Datei herunter, die mit dem Dienstprogramm zum Herunterladen großer Ordner geöffnet werden soll.', '".$idLanguage."');"
            );
        }

        /**
         * Traduction Anglais
         */
        $sql = 'select * FROM `Langue` where langue ="en"';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        if (0 < $stmt->rowCount()) {
            $row = $stmt->fetch();
            $idLanguage = $row['id_langue'];
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TITLE', 'My uploaded large files', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_INFO', 'From this page, you can send large folders that can be attached to your answers, as well as to your exchanges in the secure messaging system. When adding a large folder, the entire content (folders and files) of the local directory you have selected will be sent to the platform.', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_LIBELLE', 'My label', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_IDENTIFIANT', 'Unique ID', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_TAILLE', 'Size', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_DATE', 'Date of addition (upload)', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_ACTION', 'Action / status', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'INFO_STATUT_DOSSIER_VOLUMINEUX', 'When choosing a large file in the various features of the platform. Only the active large folders will be proposed to you', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_LINK', 'Add a large folder', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DOSSIER_VOLUMINEUX_GESTION_TABLE_EMPTY', 'You do not have any large files.', '".$idLanguage."');"
            );


            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_DOSSIER_VOLUMINEUX', 'Download the large file', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_JOURNAL_DE_LOGS_DOSSIER_VOLUMINEUX', 'Download the log of this large file', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TELECHARGER_ANNEXES_TECHNIQUES_DCE', 'Download the technical annexes to the DCE', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE', '<div class=\"alert alert-info bloc-message form-bloc-conf msg-info\">
                                <i class=\"fa fa-info-circle m-r-1 hide-agent\"></i>
                                Downloading a large file requires the use of a utility to be downloaded on your computer.
                                <div class=\"m-l-3\">
                                    A guide for installing and using the large file download utility is available on 
                                    <a target=\"_blank\" href=\"/?page=Commun.EntrepriseGuideUtilitaireTelechargementDV\">
                                       this page 
                                    </a>.
                                </div>
                            </div>', '".$idLanguage."');"
            );
            $this->addSql(
                "INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TEXT_TELECHARGEMENT_FICHIER_ATEXO', 'Download the .envol file to open with the large file download utility', '".$idLanguage."');"
            );
        }
    }

    public function down(Schema $schema): void
    {
        $sql = 'select * FROM `Langue` where langue ="du"';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        if (0 < $stmt->rowCount()) {
            $row = $stmt->fetch();
            $idLanguage = $row['id_langue'];
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TITLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_INFO" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_LIBELLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_IDENTIFIANT" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_TAILLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_DATE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_ACTION" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "INFO_STATUT_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_LINK" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );



            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );

            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_JOURNAL_DE_LOGS_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_ANNEXES_TECHNIQUES_DCE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TEXT_TELECHARGEMENT_FICHIER_ATEXO" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
        }

        $sql = 'select * FROM `Langue` where langue ="en"';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        if (0 < $stmt->rowCount()) {
            $row = $stmt->fetch();
            $idLanguage = $row['id_langue'];
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TITLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_INFO" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_LIBELLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_IDENTIFIANT" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_TAILLE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_DATE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_TABLE_ACTION" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "INFO_STATUT_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "DOSSIER_VOLUMINEUX_GESTION_LINK" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );

            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );

            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_JOURNAL_DE_LOGS_DOSSIER_VOLUMINEUX" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TELECHARGER_ANNEXES_TECHNIQUES_DCE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
            $this->addSql(
                'DELETE FROM configuration_messages_traduction 
       WHERE `configuration_messages_traduction`.`source` = "TEXT_TELECHARGEMENT_FICHIER_ATEXO" 
         and  `configuration_messages_traduction`.`langue_id` = "'.$idLanguage.'";'
            );
        }
    }
}
