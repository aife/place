<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230407100850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22305 - [PLACE][Contrat] Entité éligible par Service non disponible';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `invite_permanent_contrat` CHANGE COLUMN `service` `old_service` INT(11) NULL, 
    ADD COLUMN `service` BIGINT UNSIGNED NULL;'
        );
        // traitement des entrées avec old_id
        $this->addSql(
            'UPDATE invite_permanent_contrat t, Service s
SET t.service=s.id 
WHERE 
    t.old_service = s.old_id 
    AND t.organisme = s.organisme 
    AND s.old_id IS NOT NULL 
   ;'
        );
        // traitement des entrées avec id
        $this->addSql(
            'UPDATE invite_permanent_contrat t, Service s
SET t.service=s.id 
WHERE 
    t.old_service = s.id 
    AND t.organisme = s.organisme 
    ;'
        );
    }

    public function down(Schema $schema): void
    {
    }
}
