<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230420123225 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-20992 - Creation d'un service SF pour centraliser les appel à ProxySGMAP";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Entreprise ADD COLUMN IF NOT EXISTS update_date_api_gouv DATETIME DEFAULT NULL");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Entreprise` DROP `update_date_api_gouv`');
    }
}
