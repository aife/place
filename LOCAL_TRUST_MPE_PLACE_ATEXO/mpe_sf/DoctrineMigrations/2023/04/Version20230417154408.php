<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230417154408 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22287 - Refacto EntrepriseAPIService pour le pointer vers le nouveau WS API V3 ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/dgfip/unites_legales/{siren}/attestation_fiscale' WHERE `code` = 'attestation_fiscale';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/urssaf/unites_legales/{siren}/attestation_vigilance' WHERE `code` = 'attestation_sociale_mp';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/urssaf/unites_legales/{siren}/attestation_vigilance' WHERE `code` = 'attestation_sociale_vg';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/fntp/unites_legales/{siren}/carte_professionnelle_travaux_publics' WHERE `code` = 'carte_professionnelle';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/cnetp/unites_legales/{siren}/attestation_cotisations_conges_payes_chomage_intemperies' WHERE `code` = 'certificat_cotisation';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/qualibat/etablissements/{siret}/certification_batiment' WHERE `code` = 'certificat_qualibat';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/probtp/etablissements/{siret}/attestation_cotisations_retraite' WHERE `code` = 'attestation_cotisation_retraite';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/opqibi/unites_legales/{siren}/certification_ingenierie' WHERE `code` = 'certificats_qualifications';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/msa/etablissements/{siret}/conformite_cotisations' WHERE `code` = 'cotisation_msa';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/attestations_fiscales_dgfip/' WHERE `code` = 'attestation_fiscale';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/attestations_sociales_acoss/' WHERE `code` = 'attestation_sociale_mp';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/attestations_sociales_acoss/' WHERE `code` = 'attestation_sociale_vg';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/cartes_professionnelles_fntp/' WHERE `code` = 'carte_professionnelle';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/certificats_cnetp/' WHERE `code` = 'certificat_cotisation';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/certificats_qualibat/' WHERE `code` = 'certificat_qualibat';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/attestations_cotisation_retraite_probtp/' WHERE `code` = 'attestation_cotisation_retraite';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/certificats_opqibi/' WHERE `code` = 'certificats_qualifications';");
        $this->addSql("UPDATE `t_document_type` SET `uri` = '/cotisations_msa/' WHERE `code` = 'cotisation_msa';");
    }
}
