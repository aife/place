<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230407152421 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21783 Désactivation de comptes entreprises - CDP';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "CREATE TABLE `tmp_siret_incorrect`
                    (
                        `id`              int(10) unsigned                     NOT NULL AUTO_INCREMENT,
                        `id_inscrit`      int(11)                              NOT NULL,
                        `email`           varchar(256) COLLATE utf8_unicode_ci NOT NULL,
                        `siret`           varchar(256) COLLATE utf8_unicode_ci NOT NULL,
                        `is_send_message` tinyint(1)                           NOT NULL,
                        `send_date`       datetime DEFAULT NULL,
                        `create_date`     datetime                             NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `UNIQ_4703DC41E7927C74` (`email`)
                    ) ENGINE = InnoDB
                      DEFAULT CHARSET = utf8
                      COLLATE = utf8_unicode_ci;"
        );
        $this->addSql('ALTER TABLE `tmp_siret_incorrect` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL;');
        $this->addSql(
            'ALTER TABLE `tmp_siret_incorrect` ADD CONSTRAINT `FK_tmp_siret_incorrect_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;'
        );
        $this->addSql(
            'UPDATE tmp_siret_incorrect SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = tmp_siret_incorrect.old_id_inscrit LIMIT 1) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS `tmp_siret_incorrect`;');
    }
}
