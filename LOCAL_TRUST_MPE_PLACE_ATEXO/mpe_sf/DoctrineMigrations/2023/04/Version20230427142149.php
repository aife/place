<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20230427142149 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22478 - Agent - La liste des mandataire a disparu';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Mandataire_service` 
    CHANGE COLUMN IF EXISTS  `mandataire` `old_mandataire` INT(11) NULL;');

        $this->addSql('ALTER TABLE `Mandataire_service` 
    ADD COLUMN IF NOT EXISTS  `mandataire` BIGINT UNSIGNED NULL;');

        $this->addSql('UPDATE `Mandataire_service` SET mandataire = (
    SELECT id FROM Service WHERE old_id = `Mandataire_service`.old_mandataire 
                             AND organisme = `Mandataire_service`.organisme)
                            WHERE old_mandataire is not null AND old_mandataire != 0;');

    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
