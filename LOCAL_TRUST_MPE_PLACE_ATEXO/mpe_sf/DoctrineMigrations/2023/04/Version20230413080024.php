<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230413080024 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22314 - Remplacement du compte BOAMP par le PIAMP";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE AcheteurPublic ADD COLUMN IF NOT EXISTS `token` TEXT');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `AcheteurPublic` DROP `token`');
    }
}
