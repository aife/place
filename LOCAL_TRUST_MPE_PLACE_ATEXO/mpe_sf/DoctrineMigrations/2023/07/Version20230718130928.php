<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230718130928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-23199 - [PLACE] RdR C2 Pub - Conditionnement sur l'affichage des fonctions MOL";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
                        ALTER TABLE configuration_organisme 
                        ADD COLUMN IF NOT EXISTS `module_mpe_pub` BOOLEAN NOT NULL DEFAULT 1');
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` DROP IF EXISTS `module_mpe_pub`;");
    }
}
