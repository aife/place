<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230718140921 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-23170 - Refacto afficher valeur estimee";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE configuration_plateforme SET afficher_valeur_estimee = '1'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE configuration_plateforme SET afficher_valeur_estimee = '0'");
    }
}
