<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230711123722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-22934 - Suppression d'un contrat id 81746 / 81749 KO";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `modification_contrat` DROP FOREIGN KEY `id_contrat_titulaire_FK`;');
        $this->addSql('ALTER TABLE `modification_contrat` ADD CONSTRAINT `id_contrat_titulaire_FK` 
    FOREIGN KEY (`id_contrat_titulaire`)  REFERENCES `t_contrat_titulaire`(`id_contrat_titulaire`) ON DELETE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `modification_contrat` ADD CONSTRAINT `id_contrat_titulaire_FK` 
    FOREIGN KEY (`id_contrat_titulaire`)  REFERENCES `t_contrat_titulaire`(`id_contrat_titulaire`)');
    }
}
