<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230704085722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-22814 - Consultation passée de statut E à statut OA sans validation + impossible de supprimer la consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi DROP CONSTRAINT FK_CD66CF80B587F0D4');
        $this->addSql('ALTER TABLE interface_suivi ADD CONSTRAINT FK_CD66CF80B587F0D4 
            FOREIGN KEY (id_consultation) REFERENCES consultation (id) ON DELETE CASCADE;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi ADD CONSTRAINT FK_CD66CF80B587F0D4 
            FOREIGN KEY (id_consultation) REFERENCES consultation (id)');
    }
}
