<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230724145551 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-23125 - [Messec] Demande d'acquittement agent selon le type de courrier";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mail_template ADD IF NOT EXISTS reponse_agent_attendue TINYINT(1) NOT NULL');
        $this->addSql('UPDATE mail_template set reponse_agent_attendue = 1 WHERE code=\'DEMANDE_COMPLEMENT\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mail_template DROP IF EXISTS reponse_agent_attendue');
    }
}
