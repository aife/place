<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230130093532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21557 - Amélioration requêtes SQL données essentielles  (tester sur environnement de sécurisation / preprod)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `MarchePublie` ADD INDEX  IF NOT EXISTS `marche_publie_service_id_index` (`service_id`);');
        $this->addSql('ALTER TABLE `t_contrat_titulaire` ADD INDEX IF NOT EXISTS `t_contrat_titulaire_service_id_index` (`service_id`);');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `MarchePublie` DROP INDEX IF NOT EXISTS `marche_publie_service_id_index`;');
        $this->addSql('ALTER TABLE `t_contrat_titulaire` ADD INDEX  IF NOT EXISTS `t_contrat_titulaire_service_id_index`;');
    }
}
