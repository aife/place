<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230105104645 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-21434 Enrichir la table de log des commandes avec l'output de la commande";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE command_monitoring ADD command_output text DEFAULT NULL;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE command_monitoring drop column command_output;');
    }
}
