<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230126124439 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21588 - Transformer la suppression physique en suppression logique de l\'incrit.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
        ALTER TABLE `Inscrit` ADD COLUMN IF NOT EXISTS `deleted_at` DATETIME NULL DEFAULT NULL AFTER `rgpd_communication`, 
            ADD  COLUMN IF NOT EXISTS `deleted` BOOLEAN NULL DEFAULT NULL AFTER `deleted_at`;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Inscrit` DROP `deleted_at`, DROP `deleted`;");
    }
}
