<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230113102616 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20850 - POC monitoring applicatif URL de VIE via Prometheus';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS module_satellite_monitoring (id INT AUTO_INCREMENT NOT NULL, service VARCHAR(255) NOT NULL, json_response LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE module_satellite_monitoring');
    }
}
