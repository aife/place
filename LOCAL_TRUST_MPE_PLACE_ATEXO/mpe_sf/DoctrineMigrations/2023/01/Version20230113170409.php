<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230113170409 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20977 - Mise en place commande nettoyage offre chiffrée';
    }
    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE IF NOT EXISTS historique_purge (
                id INT AUTO_INCREMENT NOT NULL,
                type VARCHAR(50) NOT NULL, 
                description VARCHAR(255) DEFAULT NULL, 
                created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
                updated_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', 
                state TINYINT(1) NOT NULL default 0, 
                deleted TINYINT(1) NOT NULL default 0, 
                PRIMARY KEY(id)
            ) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
            ;'
        );

        $this->addSql(
            'ALTER TABLE `blocFichierEnveloppe`
                ADD COLUMN IF NOT EXISTS `historique_purge_id` INT NULL DEFAULT NULL AFTER `id_blob_dechiffre`,
                ADD CONSTRAINT `FK_blocFichierEnveloppe_historiquePurge` 
                FOREIGN KEY IF NOT EXISTS (`historique_purge_id`) REFERENCES `historique_purge`(`id`) ON DELETE SET NULL
            ;'
        );
    }
    public function down(Schema $schema): void
    {
        $this->addSql('
            ALTER TABLE blocFichierEnveloppe DROP FOREIGN KEY IF EXISTS FK_blocFichierEnveloppe_historiquePurge;
        ');
        $this->addSql('ALTER TABLE `blocFichierEnveloppe` DROP IF EXISTS `historique_purge_id`;');
        $this->addSql('DROP TABLE IF EXISTS historique_purge');
    }
}
