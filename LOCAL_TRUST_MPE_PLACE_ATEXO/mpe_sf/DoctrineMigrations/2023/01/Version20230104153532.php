<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Service\AtexoMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230104153532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21398 - Finir la supervision et la procedure de mise en production de la nouvelles commande d\'alerte entreprise. ';
    }

    public function up(Schema $schema): void
    {
        AtexoMigration::insertInMessengerMessage(
            $this->connection->createQueryBuilder(),
            'mpe:hash-admin-plain-passwords-to-argon2',
            []
        );

        AtexoMigration::insertInMessengerMessage(
            $this->connection->createQueryBuilder(),
            'mpe:redressement-xml-criteria',
            []
        );

        AtexoMigration::insertInMessengerMessage(
            $this->connection->createQueryBuilder(),
            'mpe:search-criteria:transform',
            []
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM messenger_messages 
        WHERE body LIKE \'%mpe:hash-admin-plain-passwords-to-argon2%\' and delivered_at is null;');
        $this->addSql('DELETE FROM messenger_messages 
        WHERE body LIKE \'%mpe:redressement-xml-criteria%\' and delivered_at is null;');
        $this->addSql('DELETE FROM messenger_messages 
        WHERE body LIKE \'%mpe:search-criteria:transform%\' and delivered_at is null;');
    }
}
