<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230124130629 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19356 - Suppression de tables "backup_*" dans la base de données MPE';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS backup_t_CAO_Ordre_Du_Jour;');
        $this->addSql('DROP TABLE IF EXISTS backup_t_CAO_Ordre_Du_Jour_Intervenant;');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les tables supprimées ne peuvent pas être recréées.'
        );
    }
}
