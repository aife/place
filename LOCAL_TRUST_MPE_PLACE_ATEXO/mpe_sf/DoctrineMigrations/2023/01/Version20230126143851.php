<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230126143851 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21585 - [RDR-2023-00.01.00] [ARNIA] Message non afficher avant de répondre à la consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE configuration_messages_traduction SET target = 'Pour votre réponse :
<ol>
    <li>Chaque document ne doit pas dépasser 1Go</li>
    <li>Privilégiez le dépôt de documents depuis votre poste de travail directement</li>
    <li>Pour les réponses comprennant de très nombreuses pièces, privilégiez les dossiers compressés (.zip, .rar, .7zip, ...)</li>
</ol>' WHERE source = 'INFORMATIONS_IMPORTANTE_TAILLE_DEPOT' ;");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
