<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * MPE-26199 - [PLACE] [OUBLI] Les habilitations par types de procédures par profil ne sont pas modifiables
 */
final class Version20240821124926 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-26199 - [PLACE] Les habilitations par types de procédures par profil ne sont pas modifiables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
            CREATE TABLE IF NOT EXISTS habilitation_profil_habilitation
            (
                id INT AUTO_INCREMENT NOT NULL,
                habilitation_profil_id INT NOT NULL,
                habilitation_id INT NOT NULL,
                PRIMARY KEY(id)
            ) ENGINE = InnoDB
         ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS habilitation_profil_habilitation');
    }
}
