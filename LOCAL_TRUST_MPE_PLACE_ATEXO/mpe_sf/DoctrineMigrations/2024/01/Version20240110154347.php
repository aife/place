<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240110154347 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-23835 - Lors de la création d un marché subséquent à accord cadre ou d un marché spécifique, le champ numéro de contrat n est pas recherchable.';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD IF NOT EXISTS contrat_exec_uuid VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation DROP COLUMN IF EXISTS contrat_exec_uuid');
    }
}
