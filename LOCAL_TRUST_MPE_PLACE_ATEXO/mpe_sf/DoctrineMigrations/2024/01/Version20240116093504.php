<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240116093504 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24629 - mettre une valeur par défaut pour le champ uuid de la table t_contrat_titulaire';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('update t_contrat_titulaire set uuid=uuid() where uuid is null');
        $this->addSql('ALTER TABLE t_contrat_titulaire MODIFY COLUMN uuid VARCHAR(36) DEFAULT uuid()');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_contrat_titulaire MODIFY COLUMN uuid VARCHAR(36) DEFAULT null');
    }
}
