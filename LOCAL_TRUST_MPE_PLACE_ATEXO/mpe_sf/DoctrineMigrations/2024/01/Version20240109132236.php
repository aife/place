<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240109132236 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24575 - Impossible de créer une consultation (disparition habilitations agent)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE HabilitationAgent SET creer_consultation = '1' WHERE gerer_mapa_inferieur_montant = '1' OR gerer_mapa_superieur_montant = '1';");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les données insérées ne peuvent pas être retrouvées pour une opération inverse.'
        );
    }
}
