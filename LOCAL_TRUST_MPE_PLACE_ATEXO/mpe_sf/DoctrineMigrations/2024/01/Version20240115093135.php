<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240115093135 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24525 - [PLACE] Critères RSE - Clause de niveau 4 : permettre uniquement un nombre entier strictement positif';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `referentiel_consultation_clauses_n4`
            SET `type_champ` = 'unsigned int'
            WHERE `slug` = 'heureInsertionActiviteEconomique' AND `label` = 'DEFINE_NOMBRE_HEURE_INSERTION'
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `referentiel_consultation_clauses_n4`
            SET `type_champ` = 'int'
            WHERE `slug` = 'heureInsertionActiviteEconomique' AND `label` = 'DEFINE_NOMBRE_HEURE_INSERTION'
        ");
    }
}
