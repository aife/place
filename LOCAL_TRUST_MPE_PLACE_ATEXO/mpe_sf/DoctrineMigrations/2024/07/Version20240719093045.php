<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240719093045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-25916 - [PLACE] Modification mentions légales - affichage éditeur ATEXO";
    }

    public function up(Schema $schema): void
    {
        if ((bool) getenv('SPECIFIQUE_PLACE') === true) {
            $sql = <<<SQL
update configuration_client set value = 'Directeur de la DAE</div></div>\n <div class=\'clearfix\'><div class=\'col-md-10\'>\n<div class=\'clearfix\'>\n    <div class=\'col-md-10\'>\n        <strong>Maîtrise d\'oeuvre :</strong>\n        <a class=\'lien-ext\' title=\'Nouvelle Fenêtre\' href=\'http://www.economie.gouv.fr/aife\\' target=\'_blank\'>Agence pour l\'informatique financière de l\'Etat (AIFE)</a>\n    </div>\n</div>\n<br>\n<div class=\'clearfix\'>\n    <div class=\'col-md-10\'>\n        <strong>Conception :</strong>\n        ATEXO a développé depuis 2003 les différents modules logiciels LOCAL TRUST de sa suite Marchés publics, dont le module LOCAL TRUST MPE_PLACE_ATEXO. </div> <div class=\'col-md-10\'> LOCAL TRUST est une marque déposée par ATEXO. ATEXO dispose de la propriété intellectuelle intégrale et exclusive de ce module LOCAL TRUST MPE_PLACE_ATEXO.</div>  <div class=\'col-md-10\'>Depuis le 1er janvier 2024, ce module LOCAL TRUST MPE_PLACE_ATEXO est sous licence libre CeCILL v2.1.</div>\n</div>\n ' where parameter = 'INFOSITE_DIRECTEUR_PUBLICATION';
SQL;
            $this->addSql($sql);
        }
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
