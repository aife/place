<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Enum\Habilitation\HabilitationSlug;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240708140553 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25590 - Ajout des procédures "Créer une consultation" dans la pop-in Habilitations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_habilitation SET active = 1 WHERE slug IN("' . implode(
            '","',
            HabilitationSlug::creerConsultationSection()
        ) .
            '")');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_habilitation SET active = 0 WHERE slug IN("' . implode(
            '","',
            HabilitationSlug::creerConsultationSection()
        ) .
            '")');
    }
}
