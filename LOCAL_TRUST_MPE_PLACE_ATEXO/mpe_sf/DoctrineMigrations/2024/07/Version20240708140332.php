<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240708140332 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25590 - Ajout des procédures "Créer une consultation" dans la pop-in Habilitations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE referentiel_habilitation ADD COLUMN IF NOT EXISTS active BOOLEAN NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE referentiel_habilitation DROP COLUMN active');
    }
}
