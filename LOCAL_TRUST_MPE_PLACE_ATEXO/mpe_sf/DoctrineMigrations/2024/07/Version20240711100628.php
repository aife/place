<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240711100628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25586 - Paramètre d\'alerte en cas d\'invité ponctuel';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Agent` ADD COLUMN IF NOT EXISTS `alerte_consultations_invite_ponctuel` ENUM('0','1') NOT NULL DEFAULT '0';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Agent` DROP COLUMN `alerte_consultations_invite_ponctuel`");
    }
}
