<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Enum\Habilitation\HabilitationSlug;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240709150749 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25665 - Gestion du périmètre d\'intervention sur les consultations et les contrats';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_habilitation SET active = 1 WHERE slug IN("' . implode(
            '","',
            HabilitationSlug::gestionPerimetreSection()
        ) .
        '")');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_habilitation SET active = 0 WHERE slug IN("' . implode(
            '","',
            HabilitationSlug::gestionPerimetreSection()
        ) .
            '")');
    }
}
