<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240708200722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25596 - Ajout des procédures "Créer une suite" dans la pop-in Habilitations';
    }

    private $slugs = [
        'gerer_mapa_inferieur_montant_suite',
        'gerer_mapa_superieur_montant_suite',
        'administrer_procedures_formalisees_suite'
    ];

    public function up(Schema $schema): void
    {
        foreach ($this->slugs as $slug) {
            $this->addSql("INSERT INTO referentiel_habilitation (slug, active) VALUES (:slug, 1);", ['slug' => $slug]);
        }

        $this->addSql('UPDATE referentiel_habilitation SET active = 1 WHERE slug = "creer_suite_consultation"');
    }

    public function down(Schema $schema): void
    {
        foreach ($this->slugs as $slug) {
            $this->addSql("DELETE FROM referentiel_habilitation WHERE slug = :slug", ['slug' => $slug]);
        }
        $this->addSql('UPDATE referentiel_habilitation SET active = 0 WHERE slug = "creer_suite_consultation"');
    }
}
