<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240730105553 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25585 - Paramètre d\'alerte en cas d\'invité ponctuel';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Agent 
                           ADD COLUMN IF NOT EXISTS `alerte_consultations_invite_ponctuel` ENUM('0','1') 
                           NOT NULL DEFAULT '0' 
                           COMMENT 'Permet d''activer les notifications pour les invités ponctuels';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Agent` drop column `alerte_consultations_invite_ponctuel`;');
    }
}
