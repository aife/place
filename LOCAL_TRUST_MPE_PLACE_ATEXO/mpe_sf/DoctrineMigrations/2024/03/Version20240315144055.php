<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240315144055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25137 - Script d initialisation de champs uuid dans la table chorus_echange';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }
        
        $this->addSql("UPDATE Chorus_echange ce SET uuid_externe_exec = (SELECT uuid FROM t_contrat_titulaire con WHERE ce.id_decision = con.id_contrat_titulaire) WHERE ce.uuid_externe_exec is null AND ce.id_decision is not null;");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
