<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240620095359 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-25590 - Ajout des procédures \'Créer une consultation\' dans la pop-in Habilitations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS referentiel_habilitation ' .
            '(id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) ' .
            'DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS habilitation_type_procedure ' .
            '(id INT AUTO_INCREMENT NOT NULL, habilitation_id INT NOT NULL, agent_id INT NOT NULL, ' .
            'organisme varchar (30) DEFAULT NULL, type_procedure INT DEFAULT NULL, ' .
            'INDEX IDX_E05AD7A8389712CD (habilitation_id), INDEX IDX_E05AD7A83414710B (agent_id), ' .
            'INDEX IDX_E05AD7A8FDF25C20 (type_procedure), INDEX IDX_E05AD7A8DD0F4533 (organisme), ' .
            'PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
        $this->addSql(
            'ALTER TABLE habilitation_type_procedure ADD CONSTRAINT FK_E05AD7A8389712CD ' .
            'FOREIGN KEY (habilitation_id) REFERENCES referentiel_habilitation (id)'
        );
        $this->addSql(
            'ALTER TABLE habilitation_type_procedure ADD CONSTRAINT FK_E05AD7A83414710B ' .
            'FOREIGN KEY (agent_id) REFERENCES Agent (id)'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE habilitation_type_procedure DROP FOREIGN KEY FK_E05AD7A8389712CD');
        $this->addSql('DROP TABLE habilitation_type_procedure');
        $this->addSql('DROP TABLE referentiel_habilitation');
    }
}
