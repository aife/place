<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240205140030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'NOCHECK - désactiver le keycloak interne par défaut';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE configuration_plateforme SET authenticate_agent_by_internal_keycloak = 0');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE configuration_plateforme SET authenticate_agent_by_internal_keycloak = 1');
    }
}
