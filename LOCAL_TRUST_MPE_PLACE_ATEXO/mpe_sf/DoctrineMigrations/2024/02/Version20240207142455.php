<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240207142455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24637 - Ajustement de la consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE consultation CHANGE `dateFinLocale` `dateFinLocale` VARCHAR(255) NULL DEFAULT '0000-00-00 00:00:00';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE consultation CHANGE `dateFinLocale` `dateFinLocale` VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00';");
    }
}
