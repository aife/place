<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240202090016 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-24737 - Script to update field dateFinLocale negative value';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `consultation` 
                           SET `dateFinLocale` = '0000-00-00 00:00:00' 
                           WHERE `dateFinLocale` LIKE '-0001-11-30 00:00:00'");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
