<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220328125803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-175 - [PLACE][OE] Inscription nouvelle entreprise - Liste des pays à enrichir';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE `GeolocalisationN2` SET `id_geolocalisationN1` = 233 WHERE `id` = 717");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `GeolocalisationN2` SET `id_geolocalisationN1` = 234 WHERE `id` = 717");
    }
}

