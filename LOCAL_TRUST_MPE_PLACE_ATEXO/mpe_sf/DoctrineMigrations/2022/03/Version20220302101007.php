<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220302101007 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18684 - Changer les type de procedure à 0 pour les consultations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation CHANGE `id_type_procedure` `id_type_procedure` INT(11) NULL DEFAULT NULL');
        $this->addSql('UPDATE consultation set id_type_procedure = null where id_type_procedure = 0');
    }

    public function down(Schema $schema): void
    {
    }
}
