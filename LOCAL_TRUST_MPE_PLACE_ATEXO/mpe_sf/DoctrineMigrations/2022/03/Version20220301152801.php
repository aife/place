<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220301152801 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-478 - [PLACE] [Portail Entreprise] [Page d\'accueil] Modification texte';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }
        $this->addSql("UPDATE `configuration_messages_traduction` SET `target` = 'PLACE est la plateforme de dématérialisation des procédures de marché de l''Etat. Elle permet aux entreprises de consulter et répondre aux consultations émanant des services de l''Etat en administration centrale et en services déconcentrés, des établissements publics de l''Etat et des organismes, des autorités publiques indépendantes, des autorités administratives indépendantes, des groupements d''intérêt public, des groupements d''intérêt économique investis d''une mission de service public d''intérêt national, des organismes de sécurité sociale, de l''Union des caisses nationales de sécurité sociale et de la Caisse des Dépôts et Consignations.' 
WHERE source = 'DEFINE_ENTREPRISE_VENEZ_DECOUVRIR_PORTAIL_MARCHES_PUBLICS' AND langue_id = 1;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `configuration_messages_traduction` SET `target` = 'La PLACE est la plate-forme de dématérialisation des procédures de marché de l''Etat. Elle permet aux entreprises de consulter et répondre aux consultations émanant des services de l''Etat en administration centrale et en services déconcentrés, des établissements publics de l''Etat et des organismes, des autorités publiques indépendantes, des autorités administratives indépendantes, des groupements d''intérêt public, des groupements d''intérêt économique investis d''une mission de service public d''intérêt national, des organismes de sécurité sociale, de l''Union des caisses nationales de sécurité sociale.' 
WHERE source = 'DEFINE_ENTREPRISE_VENEZ_DECOUVRIR_PORTAIL_MARCHES_PUBLICS' AND langue_id = 1;");
    }
}
