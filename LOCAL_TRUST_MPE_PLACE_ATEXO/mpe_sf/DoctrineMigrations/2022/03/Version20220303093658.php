<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220303093658 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-18084 - TERNUM : Les téléchargements groupés d'archives ne fonctionnent pas";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("DELETE FROM `T_Telechargement_Asynchrone_fichier` WHERE 1");
        $this->addSql("DELETE FROM `T_Telechargement_Asynchrone` WHERE 1");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Pas de rattrapage possible de données!');
    }
}
