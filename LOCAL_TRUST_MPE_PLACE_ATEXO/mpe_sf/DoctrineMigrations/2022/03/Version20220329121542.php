<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220329121542 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18822 - Refonte commande alertes Entreprise';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE T_MesRecherches ADD COLUMN IF NOT EXISTS criteria LONGTEXT DEFAULT NULL COMMENT '(DC2Type:json)'");
        $this->addSql("ALTER TABLE T_MesRecherches ADD COLUMN IF NOT EXISTS hashed_criteria VARCHAR(32)");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE T_MesRecherches DROP COLUMN IF EXISTS criteria;');
        $this->addSql('ALTER TABLE T_MesRecherches DROP COLUMN IF EXISTS hashed_criteria;');
    }
}
