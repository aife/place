<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220607143927 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19672 - Supprimer les tables *backup* de la BDD MPE';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $backupTables = [
            'backup_t_CAO_Ordre_Du_Jour',
            'backup_t_CAO_Ordre_Du_Jour_Intervenant',
        ];

        foreach ($backupTables as $backupTable) {
            $this->addSql(
                "DROP TABLE IF EXISTS `$backupTable`;"
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
