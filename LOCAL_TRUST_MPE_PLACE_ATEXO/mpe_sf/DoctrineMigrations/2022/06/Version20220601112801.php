<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20220601112801 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-310 - [MPE] Préventif - La taille du champ de l\'intitulé de consultation 
        n\'est pas limité côté MPE alors qu\'il l\'est côté LT-DUME';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `configuration_client` SET `value` = '255' 
                              WHERE `parameter` = 'NOMBRE_LIMIT_CARACTERE_DESCRIPTION_CONSULTATION';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `configuration_client` SET `value` = '190' 
                              WHERE `parameter` = 'NOMBRE_LIMIT_CARACTERE_DESCRIPTION_CONSULTATION';");
    }
}
