<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220121082104 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-314 - Activer double option BOAMP : Démo / Production';
    }

    public function up(Schema $schema): void
    {
        $sql = 'select * FROM configuration_client where parameter = "PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP"';
        $stmt = $this->connection->prepare($sql);
        $stmt->execute();
        $sql = 'UPDATE `configuration_client` SET `value` = \'3\' WHERE parameter = "PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP"';
        if (0 == $stmt->rowCount()) {
            $sql = "INSERT INTO `configuration_client` (`id`, `parameter`, `value`, `updated_at`) 
                    VALUES (NULL, 'PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP', '3', '" . date('Y-m-d H:i:s') . "')";
        }
        $this->addSql($sql);
    }
}
