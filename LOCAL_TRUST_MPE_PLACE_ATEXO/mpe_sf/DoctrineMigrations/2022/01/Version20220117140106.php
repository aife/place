<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220117140106 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18216 - Création du fichier de migration [Module Publicité]';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `t_donnee_complementaire`
            ADD `visite_obligatoire` BOOLEAN NULL AFTER `identification_projet`,
            ADD `visite_description` VARCHAR(100) NULL AFTER `visite_obligatoire`,
            ADD `catalogue_electronique` INT(1) NULL AFTER `visite_description`,
            ADD `attribution_sans_negociation` BOOLEAN NULL AFTER `catalogue_electronique`;
        ");

        $this->addSql("CREATE TABLE `technique_achat` (
            `id_technique_achat` INT NOT NULL AUTO_INCREMENT ,
            `libelle` VARCHAR(100) NOT NULL ,
            `code` VARCHAR(50) NOT NULL ,
            PRIMARY KEY (`id_technique_achat`)) ENGINE = InnoDB;
        ");

        $this->addSql("ALTER TABLE `t_type_contrat`
            ADD `technique_achat` INT NULL AFTER `id_externe`,
            ADD CONSTRAINT `technique_achat_type_contrat` FOREIGN KEY (`technique_achat`) 
            REFERENCES `technique_achat`(`id_technique_achat`) ON UPDATE CASCADE;
        ");

        $this->addSql("ALTER TABLE `consultation`
            ADD `groupement` BOOLEAN NULL AFTER `service_associe_id`;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `t_donnee_complementaire` DROP COLUMN IF EXISTS `visite_obligatoire`;");
        $this->addSql("ALTER TABLE `t_donnee_complementaire` DROP COLUMN IF EXISTS `visite_description`;");
        $this->addSql("ALTER TABLE `t_donnee_complementaire` DROP COLUMN IF EXISTS `catalogue_electronique`;");
        $this->addSql("ALTER TABLE `t_donnee_complementaire` DROP COLUMN IF EXISTS `attribution_sans_negociation`;");

        $this->addSql("ALTER TABLE `t_type_contrat` DROP FOREIGN KEY IF EXISTS technique_achat_type_contrat;");
        $this->addSql("ALTER TABLE `t_type_contrat` DROP INDEX IF EXISTS `technique_achat_type_contrat`;");
        $this->addSql("ALTER TABLE `t_type_contrat` DROP COLUMN IF EXISTS `technique_achat`;");
        $this->addSql("DROP TABLE `technique_achat`;");

        $this->addSql("ALTER TABLE `consultation` DROP COLUMN IF EXISTS `groupement`;");
    }
}
