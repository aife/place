<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220128122932 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18351 - Supprimer les tables *backup* de la BDD MPE';
    }

    public function up(Schema $schema): void
    {
        $backupTables = [
            'backup_Agent_Commission',
            'backup_Commission',
            'backup_Etape_CAO',
            'backup_Intervenant_Externe',
            'backup_Intervenant_Externe_Commission',
            'backup_Intervenant_Ordre_Du_Jour',
            'backup_Ordre_Du_Jour',
            'backup_Ordre_Du_Jour_2',
        ];

        foreach ($backupTables as $backupTable) {
            $this->addSql(
                "DROP TABLE IF EXISTS `$backupTable`;"
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
