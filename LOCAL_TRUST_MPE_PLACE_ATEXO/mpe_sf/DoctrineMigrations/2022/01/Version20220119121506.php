<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220119121506 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18260 - Correction données champ service_validation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `consultation` ' .
            'CHANGE COLUMN `service_validation` `old_service_validation` INT(11) NOT NULL DEFAULT 0, ' .
            'ADD COLUMN `service_validation` BIGINT UNSIGNED NULL;'
        );
        $this->addSql(
            'UPDATE `consultation` SET service_validation = ' .
            '(SELECT id FROM Service WHERE old_id = consultation.old_service_validation ' .
            'and organisme = consultation.organisme) ' .
            'WHERE old_service_id is not null AND old_service_id > 0;'
        );
        $this->addSql(
            'ALTER TABLE `consultation` ADD CONSTRAINT FK_SERVICE_VALIDATION_ID FOREIGN KEY ' .
            '(service_validation) REFERENCES `Service` (id)'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` DROP FOREIGN KEY FK_SERVICE_VALIDATION_ID;");
        $this->addSql(
            'ALTER TABLE `consultation` ' .
            'DROP COLUMN `service_validation`, ' .
            'CHANGE COLUMN `old_service_validation` `service_validation` INT(11) NOT NULL DEFAULT 0;'
        );
    }
}
