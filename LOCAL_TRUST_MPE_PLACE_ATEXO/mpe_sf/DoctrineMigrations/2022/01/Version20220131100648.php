<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220131100648 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-447 - [Place] : Impossible d\'accéder aux consultations - Erreur 500';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("Update t_dume_numero dn set dn.blob_id_xml =
                           ( SELECT b.id FROM `t_dume_contexte` dc, blobOrganisme_file b
                           WHERE dn.blob_id_xml = b.old_id and dc.organisme = b.organisme
                           AND dc.id = dn.id_dume_contexte)
                           WHERE dn.blob_id_xml is NOT NULL");

    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
