<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220124181737 extends AbstractMigration
{
    private $typeContratTechniqueAchat = [
        "TYPE_CONTRAT_MARCHE" => "Sans objet",
        "TYPE_CONTRAT_AC_BON_COMMANDE_MONO_ATTRIBUTAIRE" => "Accord-cadre",
        "TYPE_CONTRAT_MARCHE_PUBLIC_MULTI_ATTRIBUTAIRES" => "Sans objet",
        "TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE" => "Accord-cadre",
        "TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE" => "Accord-cadre",
        "TYPE_CONTRAT_MARCHE_SUBSEQUENT" => "Accord-cadre",
        "TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC"	=> "Sans objet",
        "TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE"	=> "Sans objet",
        "TYPE_CONTRAT_PARTENARIAT_D_INNOVATION"	=> "Sans objet",
        "TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE" => "Système d'acquisition dynamique (SAD)",
        "TYPE_CONTRAT_MARCHE_SPECIFIQUE" => "Système d'acquisition dynamique (SAD)",
        "TYPE_CONTRAT_MARCHE_DE_CONCEPTION_REALISATION"	=> "Sans objet",
        "TYPE_CONTRAT_AUTRE" => "Sans objet",
        "TYPE_CONTRAT_MARCHE_SECTORIEL" => "Sans objet",
        "TYPE_CONTRAT_MARCHE_GLOBAL_PERFORMANCE" => "Sans objet",
    ];

    public function getDescription(): string
    {
        return 'MPE-18078 - [Module publicité] Ajout champ Technique achat Ecran Données complémentaires';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `technique_achat`
            (`id_technique_achat`, `libelle`, `code`) VALUES
            (NULL, 'Sans objet', ''),
            (NULL, 'Accord-cadre', ''),
            (NULL, 'Système d\'acquisition dynamique (SAD)', '');"
        );

        foreach ($this->typeContratTechniqueAchat as $typeContrat => $techniqueAchat) {
            $this->addSql("
                UPDATE t_type_contrat SET technique_achat = 
                    (SELECT id_technique_achat FROM technique_achat WHERE libelle = '".addslashes($techniqueAchat)."') 
                WHERE `libelle_type_contrat` = '".$typeContrat."';"
            );
        }

        $this->addSql("ALTER TABLE `consultation` ADD `autre_technique_achat` VARCHAR(255) NULL AFTER `service_validation`;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE t_type_contrat SET technique_achat = NULL;");

        $this->addSql("
            DELETE FROM `technique_achat` WHERE `libelle` = 'Sans objet';
            DELETE FROM `technique_achat` WHERE `libelle` = 'Accord-cadre';
            DELETE FROM `technique_achat` WHERE `libelle` = 'Système d\'acquisition dynamique (SAD)';
        ");

        $this->addSql("ALTER TABLE `consultation` DROP COLUMN IF EXISTS `autre_technique_achat`;");
    }
}
