<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220125145010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18066 - Ajout du champ “prestation supplémentaires éventuelles” dans les Données complémentaire';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `t_donnee_complementaire`
            ADD IF NOT EXISTS `variante_prestations_supp_eventuelles` BOOLEAN NOT NULL DEFAULT FALSE;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `t_donnee_complementaire` 
            DROP COLUMN IF EXISTS `variante_prestations_supp_eventuelles`;
        ");
    }
}
