<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220113145702 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17336 - Soft delete sur l\'entité Agent';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE Agent ADD deleted_at DATETIME DEFAULT NULL after actif;');
        $this->addSql('UPDATE Agent SET deleted_at = NOW() WHERE old_service_id = 10000;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE Agent drop column deleted_at;');
    }
}
