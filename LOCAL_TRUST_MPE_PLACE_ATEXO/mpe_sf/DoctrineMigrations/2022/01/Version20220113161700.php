<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220113161700 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18193 - Création Consultation simplifiée KO';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `consultation` CHANGE `reference_consultation_init` `reference_consultation_init` 
            VARCHAR(250) NULL DEFAULT '';
        ");
        $this->addSql("
            ALTER TABLE `consultation` CHANGE `service_validation` `service_validation` INT(11) NULL DEFAULT '0';
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `consultation` CHANGE `reference_consultation_init` `reference_consultation_init` 
            VARCHAR(250) NOT NULL DEFAULT '';
        ");
        $this->addSql("
            ALTER TABLE `consultation` CHANGE `service_validation` `service_validation` INT(11) NOT NULL DEFAULT '0';
        ");
    }
}
