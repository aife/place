<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220712080341 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19968 - MERGE PLACE - Ajout de la colonne numero_projet_achat en BD';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` ADD COLUMN IF NOT EXISTS `numero_projet_achat` VARCHAR(255) DEFAULT NULL");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` DROP `numero_projet_achat`");

    }
}
