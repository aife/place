<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220712160509 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19792 - [Merge PLACE] Rendre modulaire l\'évolution "Je donne mon avis"';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("ALTER TABLE `configuration_plateforme` ADD COLUMN IF NOT EXISTS `donner_avis_depot_entreprise` BOOLEAN NOT NULL DEFAULT '0' COMMENT 'Si 1, sur la page de confirmation d’un dépôt entreprise, un bouton permet de rediriger vers un site afin de donner son avis';");

        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $this->addSql("UPDATE `configuration_plateforme` SET `donner_avis_depot_entreprise` = '1';");
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("ALTER TABLE `configuration_plateforme` DROP `donner_avis_depot_entreprise`");
    }
}
