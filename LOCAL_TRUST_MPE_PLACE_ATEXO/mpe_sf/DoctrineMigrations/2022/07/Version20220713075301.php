<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220713075301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-19796 - [Merge PLACE] Rendre modulaire l'évolution Recueil du consentement RGPD'";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_plateforme` ADD COLUMN IF NOT EXISTS `recueil_consentement_rgpd` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Si 1, un recueil de consentement RGPD est réalisé auprès des entreprises et des agents, au moment de leur inscription et périodiquement à la connexion.';");

        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $this->addSql("UPDATE `configuration_plateforme` SET `recueil_consentement_rgpd` = 1;");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` DROP `recueil_consentement_rgpd`');
    }
}
