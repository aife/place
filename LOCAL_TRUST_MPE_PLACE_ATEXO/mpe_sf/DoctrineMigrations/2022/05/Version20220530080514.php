<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220530080514 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-318 - [MPE] ARCADE - La taille du champ poids_archive dans la table archive_arcade est trop petit';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `archive_arcade` CHANGE `poids_archive` `poids_archive` 
    BIGINT UNSIGNED NULL DEFAULT NULL;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `archive_arcade` CHANGE `poids_archive` `poids_archive` 
    INT(11) NULL DEFAULT NULL;");
    }
}
