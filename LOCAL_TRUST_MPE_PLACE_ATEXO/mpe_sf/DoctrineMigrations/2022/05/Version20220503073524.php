<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220503073524 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19327 - Ajouter une nouvelle colonne dans la table echange_doc_application_client';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE echange_doc_application_client ADD envoi_auto_archivage TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Si = 1, alors un échange est automatiquement créé lorsque la consultation est archivée' AFTER multi_docs_principaux;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `echange_doc_application_client` DROP `envoi_auto_archivage`');
    }
}
