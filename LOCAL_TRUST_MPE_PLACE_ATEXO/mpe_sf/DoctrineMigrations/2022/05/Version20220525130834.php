<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220525130834 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19602 - Logger les commandes Symfony en BDD';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS command_monitoring(
                `id` SERIAL PRIMARY KEY,
                name VARCHAR(50) not null,
                params text,
                `start_date` datetime NOT NULL,
                `end_date` datetime,
                status VARCHAR(15) NOT NULL,
                logs VARCHAR(500)
        ) collate = utf8_unicode_ci;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE command_monitoring');
    }
}
