<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220505103322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-572 [PLACE] - WS qui permet de renvoyé le ZIP';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
                           VALUES 
                           ('/api/{version}/echanges-chorus/download-zip/{idEchangeChorus}', 'GET', 'Dowload les pièce de l\'échange + l\'acte modificatif', '1');
                           ");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM web_service WHERE route_ws='/api/{version}/echanges-chorus/download-zip/{idEchangeChorus}';");
    }
}
