<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220505103254 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-266 - [PLACE] Guides d'utilisation - Calcul automatique du poids erroné";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE `configuration_messages_traduction` SET `target` = "Guide d\'utilisation (zip 4.5 Mo)" WHERE `source` = "TEXT_GUIDE_UTILISATION_ECO" AND `langue_id` = 1 ');
        $this->addSql('UPDATE `configuration_messages_traduction` SET `target` = "Guide d\'utilisation à destination des Agents publics (zip 9,2 Mo)" WHERE `source` = "TEXT_GUIDE_UTILISATION_AGENT" AND `langue_id` = 1 ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE `configuration_messages_traduction` SET `target` = "Guide d\'utilisation (zip 3.5 Mo)" WHERE `source` = "TEXT_GUIDE_UTILISATION_ECO" AND `langue_id` = 1 ');
        $this->addSql('UPDATE `configuration_messages_traduction` SET `target` = "Guide d\'utilisation à destination des Agents publics (zip 7,9 Mo)" WHERE `source` = "TEXT_GUIDE_UTILISATION_AGENT" AND `langue_id` = 1 ');
    }
}
