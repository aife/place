<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220502161143 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19328 - Insérer une nouvelle entrée  dans la table  echange_doc_application';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO echange_doc_application(code, libelle, flux_actes,primo_signature, envoi_dic)VALUES ('PASTELL_V2_DDM', 'Pas- tell v2.0 - Flux Dossier de marché','0', '0', '1');");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM echange_doc_application WHERE code=\'PASTELL_V2_DDM\';');
    }
}
