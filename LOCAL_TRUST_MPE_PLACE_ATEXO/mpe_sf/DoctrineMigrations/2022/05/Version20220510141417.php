<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220510141417 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19334 - Enrichissement du suivi des échanges documentaires';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("insert into echange_doc_type_piece_standard (code, libelle) values ('DAC', 'Dossier d''Archive de la Consultation (généré automatiquement)');");
    }

    public function down(Schema $schema): void
    {
    }
}
