<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220503080301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19329 - Inserer une nouvelle entrée dans la table echange_doc_application_client';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT INTO echange_doc_application_client (echange_doc_application_id, code, libelle, actif,
cheminement_signature, cheminement_ged, cheminement_sae, cheminement_tdt, classification_1,
classification_2, classification_3, classification_4, classification_5, multi_docs_principaux, envoi_auto_archivage)
SELECT id, 'PASTELL_V2_DDM_01','Pastell – Flux dossier de marché (modèle)', '1', '0', '0', '0','0', NULL, NULL, NULL, NULL, NULL, '0', '0'
FROM echange_doc_application
WHERE code LIKE 'PASTELL_V2_DDM'");

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
