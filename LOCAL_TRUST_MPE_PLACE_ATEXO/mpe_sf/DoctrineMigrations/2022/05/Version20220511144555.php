<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220511144555 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19334 - Enrichissement du suivi des échanges documentaires';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('create table lock_keys(
                                key_id         varchar(64)  not null primary key,
                                key_token      varchar(44)  not null,
                                key_expiration int unsigned not null
                      ) collate = utf8_unicode_ci;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE lock_keys');
    }
}
