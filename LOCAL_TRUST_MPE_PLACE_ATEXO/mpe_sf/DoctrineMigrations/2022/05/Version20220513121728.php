<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20220513121728 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'EXEC-94 - lever de la restriction du WS au agent technique';
    }

    public function up(Schema $schema) : void
    {
            $this->addSql("UPDATE `web_service` SET `uniquement_technique` = '0' 
                            WHERE `web_service`.`route_ws` = '/api/{version}/echanges-chorus/download-zip/{idEchangeChorus}';");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `web_service` SET `uniquement_technique` = '1' 
                            WHERE `web_service`.`route_ws` = '/api/{version}/echanges-chorus/download-zip/{idEchangeChorus}';");
    }
}
