<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220502125251 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-568 - Redressement de la table invitationconsultationtransverse';
    }

    public function up(Schema $schema): void
    {
        //On met a NULL l'ancien chang de référence de la consultion
        $this->addSql('ALTER TABLE `InvitationConsultationTransverse` 
    CHANGE `reference` `reference` VARCHAR(256) 
    CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;');
        //On fait un premier redressement des données avant l'ajout `ConsultationId`
        $this->addSql('UPDATE `InvitationConsultationTransverse` a 
SET consultation_id = 
    (SELECT id FROM consultation c WHERE a.organisme_emetteur = c.organisme 
                                     AND a.reference = c.reference 
                                     AND a.consultation_id is null)');
        //Ensuite on redresse les nouvelle donnnée après l'ajout `ConsultationId`
        $this->addSql('UPDATE `InvitationConsultationTransverse` a 
SET consultation_id = 
    (SELECT id FROM consultation c WHERE a.organisme_emetteur = c.organisme 
                                     AND a.reference = c.id 
                                     AND a.consultation_id is null)');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.'
        );
    }
}
