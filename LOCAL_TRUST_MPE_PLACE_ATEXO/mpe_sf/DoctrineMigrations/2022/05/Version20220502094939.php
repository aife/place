<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220502094939 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19326 - Ajouter une nouvelle colonne dans la table echange_doc_application';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE echange_doc_application ADD envoi_dic TINYINT(1) NOT NULL DEFAULT '0' COMMENT 'Si = 1, alors le fichierjoint aux échanges liés est leDIC/DAC zippé de la consultation' AFTER primo_signature;");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `echange_doc_application` DROP `envoi_dic`');
    }
}
