<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220428083724 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19126 - [RdR 2022-00.02.00] Widget Messec KO (WS GET consultations en erreur 500)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation  MODIFY type_marche int(11) DEFAULT NULL');
        $this->addSql('UPDATE consultation SET type_marche=null WHERE type_marche not in (select id_type_contrat from t_type_contrat)');
        $this->addSql('ALTER TABLE consultation ADD CONSTRAINT `FK_Type_Marche_Type_Contrat_Id_Type_Contrat` 
    FOREIGN KEY ( type_marche ) REFERENCES t_type_contrat(id_type_contrat) ON UPDATE CASCADE;');

    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
