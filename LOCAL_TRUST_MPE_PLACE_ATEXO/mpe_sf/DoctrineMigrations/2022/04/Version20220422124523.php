<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220422124523 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-18687 - Nouvelle habilitation : Valider un Projet d'achat";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationAgent` ADD COLUMN IF NOT EXISTS `valider_projet_achat` TINYINT(1) NULL DEFAULT \'0\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP IF EXISTS `valider_projet_achat`');
    }
}
