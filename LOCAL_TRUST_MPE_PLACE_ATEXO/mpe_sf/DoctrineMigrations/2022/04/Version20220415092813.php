<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220415092813 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17542 - TERNUM : Disparition des alertes entreprises dans mon compte / mes alertes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE T_MesRecherches SET id_createur = (SELECT id FROM Inscrit WHERE old_id = T_MesRecherches.id_createur LIMIT 1) WHERE id_createur is not null AND type_createur = \'ENTREPRISE\';');
    }

    public function down(Schema $schema): void
    {
    }
}
