<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220411083532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18866 [DCE restreint] Enrichissement du détail d\'une consultation côté agent';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE consultation ADD COLUMN `code_dce_restreint` VARCHAR(15) NULL DEFAULT NULL');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `consultation` DROP `code_dce_restreint`');
    }
}
