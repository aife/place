<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220406090336 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18864 [DCE restreint] Nouveau module organisme';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `dce_restreint` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'Permet de définir un code d’accès lors de la création d’une consultation, nécessaire au téléchargement du DCE côté entreprise\'');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `configuration_organisme` DROP `dce_restreint`');

    }
}
