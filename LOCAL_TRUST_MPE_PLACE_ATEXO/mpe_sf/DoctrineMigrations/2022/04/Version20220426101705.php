<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220426101705 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19150 - [AVIS STANDARD] Conditions de participation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_donnee_complementaire ADD activite_professionel LONGTEXT DEFAULT NULL, ADD economique_financiere LONGTEXT DEFAULT NULL, ADD techniques_professionels LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_donnee_complementaire DROP activite_professionel, DROP economique_financiere, DROP techniques_professionels');

    }
}
