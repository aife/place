<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220415171153 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-19151 - [RdP 2022-00.01.01] TerNum - Pas de validation lors de la création de consultation";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` CHANGE COLUMN `service_validation` `old_service_validation` INT default 0 not null, ADD COLUMN `service_validation` BIGINT UNSIGNED NULL after `id_type_validation`;');
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` ADD CONSTRAINT `FK_Type_Procedure_Organisme_service_validation` 
    FOREIGN KEY ( `service_validation` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `Type_Procedure_Organisme` SET service_validation = (SELECT id FROM Service WHERE old_id = Type_Procedure_Organisme.old_service_validation AND organisme = Type_Procedure_Organisme.organisme) WHERE old_service_validation is not null AND old_service_validation != 0;');

        $this->addSql(
            'ALTER TABLE `consultation` ' .
            'CHANGE COLUMN `service_validation_intermediaire` `old_service_validation_intermediaire` INT(11) NOT NULL DEFAULT 0, ' .
            'ADD COLUMN `service_validation_intermediaire` BIGINT UNSIGNED NULL;'
        );
        $this->addSql(
            'UPDATE `consultation` SET service_validation_intermediaire = ' .
            '(SELECT id FROM Service WHERE old_id = consultation.old_service_validation_intermediaire ' .
            'and organisme = consultation.organisme) ' .
            'WHERE old_service_validation_intermediaire is not null AND old_service_validation_intermediaire > 0;'
        );
        $this->addSql(
            'ALTER TABLE `consultation` ADD CONSTRAINT FK_service_validation_intermediaire_ID FOREIGN KEY ' .
            '(service_validation_intermediaire) REFERENCES `Service` (id)'
        );

        $this->addSql('ALTER TABLE `Newsletter` CHANGE COLUMN `id_service_redacteur` `old_id_service_redacteur` INT not null, ADD COLUMN `id_service_redacteur` BIGINT UNSIGNED NULL after `corps`;');
        $this->addSql('ALTER TABLE `Newsletter` ADD CONSTRAINT `FK_Newsletter_id_service_redacteur` 
    FOREIGN KEY ( `id_service_redacteur` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `Newsletter` SET id_service_redacteur = (SELECT id FROM Service WHERE old_id = Newsletter.old_id_service_redacteur AND organisme = Newsletter.organisme) WHERE old_id_service_redacteur is not null AND old_id_service_redacteur != 0;');

        $this->addSql('ALTER TABLE `Newsletter` CHANGE COLUMN `id_service_destinataire` `old_id_service_destinataire` varchar(11) null, ADD COLUMN `id_service_destinataire` BIGINT UNSIGNED NULL after `date_envoi`;');
        $this->addSql('ALTER TABLE `Newsletter` ADD CONSTRAINT `FK_Newsletter_id_service_destinataire` 
    FOREIGN KEY ( `id_service_destinataire` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `Newsletter` SET id_service_destinataire = (SELECT id FROM Service WHERE old_id = Newsletter.old_id_service_destinataire AND organisme = Newsletter.organisme) WHERE old_id_service_destinataire is not null AND old_id_service_destinataire != 0;');

        $this->addSql('ALTER TABLE `t_fusionner_services` CHANGE COLUMN `id_service_source` `old_id_service_source` INT null, ADD COLUMN `id_service_source` BIGINT UNSIGNED NULL after `id`;');
        $this->addSql('ALTER TABLE `t_fusionner_services` ADD CONSTRAINT `FK_t_fusionner_services_id_service_source` 
    FOREIGN KEY ( `id_service_source` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `t_fusionner_services` SET id_service_source = (SELECT id FROM Service WHERE old_id = t_fusionner_services.old_id_service_source AND organisme = t_fusionner_services.organisme) WHERE old_id_service_source is not null AND old_id_service_source != 0;');

        $this->addSql('ALTER TABLE `t_fusionner_services` CHANGE COLUMN `id_service_cible` `old_id_service_cible` INT null, ADD COLUMN `id_service_cible` BIGINT UNSIGNED NULL after `id_service_source`;');
        $this->addSql('ALTER TABLE `t_fusionner_services` ADD CONSTRAINT `FK_t_fusionner_services_id_service_cible` 
    FOREIGN KEY ( `id_service_cible` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `t_fusionner_services` SET id_service_cible = (SELECT id FROM Service WHERE old_id = t_fusionner_services.old_id_service_cible AND organisme = t_fusionner_services.organisme) WHERE old_id_service_cible is not null AND old_id_service_cible != 0;');

        $this->addSql('drop index if exists id_service_agent on T_Telechargement_Asynchrone;');
        $this->addSql('ALTER TABLE `T_Telechargement_Asynchrone` CHANGE COLUMN `id_service_agent` `old_id_service_agent` int default 0 not null;');
        $this->addSql('ALTER TABLE `T_Telechargement_Asynchrone` ADD COLUMN `id_service_agent` BIGINT UNSIGNED NULL after `email_agent`;');
        $this->addSql('ALTER TABLE `T_Telechargement_Asynchrone` ADD CONSTRAINT `FK_T_Telechargement_Asynchrone_id_service_agent` FOREIGN KEY ( `id_service_agent` ) 
    REFERENCES `Service` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE `T_Telechargement_Asynchrone` SET id_service_agent = (SELECT id FROM Service WHERE old_id = T_Telechargement_Asynchrone.old_id_service_agent AND organisme = T_Telechargement_Asynchrone.organisme_agent) WHERE old_id_service_agent is not null AND old_id_service_agent != 0;');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Pas de rattrapage possible de données!');
    }
}
