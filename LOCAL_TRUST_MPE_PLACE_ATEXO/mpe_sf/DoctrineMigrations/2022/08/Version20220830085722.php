<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220830085722 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20111 - Création Table interface_suivi pour faire le suivi des publications du TNCP dans MPE';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE interface_suivi (uuid CHAR(36) NOT NULL COMMENT \'(DC2Type:uuid)\',
         id_consultation INT DEFAULT NULL, interface VARCHAR(30) NOT NULL, action VARCHAR(30) NOT NULL,
          statut INT NOT NULL, message LONGTEXT DEFAULT NULL, id_objet_destination VARCHAR(30) DEFAULT NULL,
           date_creation DATETIME NOT NULL, date_modification DATETIME NOT NULL,
            INDEX IDX_CD66CF80B587F0D4 (id_consultation),
             PRIMARY KEY(uuid)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE interface_suivi ADD CONSTRAINT FK_CD66CF80B587F0D4 
            FOREIGN KEY (id_consultation) REFERENCES consultation (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE interface_suivi');
    }
}
