<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220818140005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20176 Externalisation et enrichissement des conditionnements d\'activation liés à Mon Assistant Marchés Publics';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `activer_mon_assistant_marches_publics` BOOLEAN NOT NULL DEFAULT 1 COMMENT \'Si 1, les fonctions intégrant MAMP l utiliseront pour fonctionner. Si 0, les anciens utilitaires JNLP, ActiveX... seront utilisés.\'');
        if ((bool) getenv('SPECIFIQUE_PLACE') === true) {
            $this->addSql('UPDATE configuration_organisme SET `activer_mon_assistant_marches_publics` = 0');
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `activer_mon_assistant_marches_publics`');
    }
}
