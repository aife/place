<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220822130633 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-20185 - [Merge PLACE] Activation et conditionnements modules d'autoformation";
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "ALTER TABLE `configuration_plateforme` 
            ADD COLUMN IF NOT EXISTS `modules_autoformation` TINYINT(1) NOT NULL DEFAULT 0
            COMMENT 'Si 1, deux champs permettent aux entreprises, au moment d’un dépôt,
                 de saisir quelle part de la valeur de l’offre présentée est composée
                 de produits fabriqués en France et en UE';"
        );


        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $this->addSql("UPDATE `configuration_plateforme` SET `modules_autoformation` = 1;");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_plateforme` DROP COLUMN IF EXISTS `modules_autoformation`;");
    }
}
