<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221130124636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-762 - [PLACE] RdR 2023-00.01.00 - Nouvel onglet/fonctionnalité publicité à masquer";
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql('update `configuration_plateforme` set publicite = "0";');
    }

    public function down(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql('update `configuration_plateforme` set publicite = "1";');
    }
}
