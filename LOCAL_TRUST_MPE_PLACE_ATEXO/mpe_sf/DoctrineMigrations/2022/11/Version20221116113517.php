<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221116113517 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21053 - Mise à jour des dépendances Composer : retour à Doctrine 2';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_access_token CHANGE data data LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_access_token CHANGE data data LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci` COMMENT \'(DC2Type:json_array)\'');
    }
}
