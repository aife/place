<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221117084652 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20899 - [Consult. Simpl.] Ajout d\'un module organisme';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `consultation_simplifiee` BOOLEAN 
    NOT NULL DEFAULT \'1\' COMMENT \'Si 1, la création de consultations simplifiées est possible. Si 0, le lien n’est pas affiché dans le menu.\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `consultation_simplifiee`');
    }
}
