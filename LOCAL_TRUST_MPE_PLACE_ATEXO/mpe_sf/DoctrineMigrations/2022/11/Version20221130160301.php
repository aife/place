<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221130160301 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD module_tncp TINYINT(1) NOT NULL DEFAULT 0 ');

        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $this->addSql('UPDATE `configuration_organisme` SET `module_tncp` = 1;');
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme  DROP module_tncp');
    }
}
