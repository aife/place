<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221124110013 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-21070 - [Typage JO2024] Typage JO2024 lors de la création d'une consultation";
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE IF NOT EXISTS `consultation_tags`(
                `id` SERIAL PRIMARY KEY,
                `consultation_id` INT NOT NULL,
                `tag_code` VARCHAR(255) NOT NULL,
                CONSTRAINT `consultation_id_tags_fk` FOREIGN KEY (`consultation_id`) REFERENCES `consultation` (`id`) 
                    ON DELETE CASCADE ON UPDATE CASCADE
                ) ENGINE = InnoDB;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS `consultation_tags`');
    }
}
