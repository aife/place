<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221121094323 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-21069 - [Typage JO2024] Création d'un nouveau module organisme";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
        ALTER TABLE configuration_organisme
        ADD COLUMN `typage_jo2024` BOOLEAN NOT NULL DEFAULT 0 
COMMENT \'Si 1, il est possible de typer une consultation comme étant en lien avec les JO2024, lors de sa création.\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `typage_jo2024`');
    }
}
