<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221123150750 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21153 - Ajouter l\'agent agent_tncp via doctrine migration';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT IGNORE INTO Agent (login, email, nom, prenom, password) 
                VALUES (
                        \'agent_tncp\',
                        \'agent_tncp@atexo.com\',
                        \'agent_tncp\',
                        \'agent_tncp\',
                        \'' . sha1(base64_encode((string)rand())) . '\')');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM Agent WHERE login = \'agent_tncp\'');
    }
}
