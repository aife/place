<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220906155312 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20240 Creation de commande SF de reprise de données pour les clauses social ';
    }

    public function up(Schema $schema): void
    {
        // ajout de l'insert de migration:tiers car elle a manqué sur certaines integ!
        $message = addslashes('O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:\\"\\0Symfony'
            . '\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component\\\\Messenger'
            . '\\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp'
            . '\\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\";s:21:'
            . '\\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message'
            . '\\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message\\\\CommandeMessenger'
            . '\\0commande\\";s:19:\\"mpe:migration:consultation:clauses\\";s:37:\\"\\0App\\\\Message\\\\CommandeMessenger\\0params'
            . '\\";a:0:{}}}');

        $sql = 'INSERT INTO messenger_messages 
        (body, headers, queue_name, created_at, available_at, delivered_at) 
        SELECT DISTINCT \'' . $message . '\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null ;';
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
    }
}
