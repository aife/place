<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220914151213 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-20368 - Modification de schéma de données pour l'ajout de la colonne contrat_id dans la table clauses_n1";
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation_clauses_n1 ADD COLUMN IF NOT EXISTS contrat_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE consultation_clauses_n1 ADD CONSTRAINT FK_977AD86F1823061F FOREIGN KEY (contrat_id) REFERENCES t_contrat_titulaire (id_contrat_titulaire)');
        $this->addSql('CREATE INDEX IF NOT EXISTS IDX_977AD86F1823061F ON consultation_clauses_n1 (contrat_id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation_clauses_n1 DROP FOREIGN KEY FK_977AD86F1823061F');
        $this->addSql('DROP INDEX IDX_977AD86F1823061F ON consultation_clauses_n1');
        $this->addSql('ALTER TABLE consultation_clauses_n1 DROP contrat_id');
    }
}
