<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220901095637 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20173 Modification modèle de données sauvegarde clauses sociales';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE consultation_clauses_n1 (id INT AUTO_INCREMENT NOT NULL, consultation_id INT DEFAULT NULL, lot_id INT DEFAULT NULL, referentiel_clause_n1_id INT NOT NULL, INDEX IDX_977AD86F62FF6CDF (consultation_id), INDEX IDX_977AD86FA8CBA5F7 (lot_id), INDEX IDX_977AD86F4BBFFC96 (referentiel_clause_n1_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consultation_clauses_n2 (id INT AUTO_INCREMENT NOT NULL, clause_n1_id INT NOT NULL, referentiel_clause_n2_id INT NOT NULL, INDEX IDX_E7389D5BDA9065 (clause_n1_id), INDEX IDX_E7389D5590A5378 (referentiel_clause_n2_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE consultation_clauses_n3 (id INT AUTO_INCREMENT NOT NULL, clause_n2_id INT NOT NULL, referentiel_clause_n3_id INT NOT NULL, INDEX IDX_7974B943196F3F8B (clause_n2_id), INDEX IDX_7974B943E1B6341D (referentiel_clause_n3_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referentiel_consultation_clauses_n1 (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referentiel_consultation_clauses_n2 (id INT AUTO_INCREMENT NOT NULL, clause_n1_id INT NOT NULL, label VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, INDEX IDX_EAE06A40BDA9065 (clause_n1_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id INT NOT NULL, clauses_n3_id INT NOT NULL, INDEX IDX_1C5CF9E2847212C0 (clauses_n2_id), INDEX IDX_1C5CF9E23CCE75A5 (clauses_n3_id), PRIMARY KEY(clauses_n2_id, clauses_n3_id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referentiel_consultation_clauses_n3 (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE consultation_clauses_n1 ADD CONSTRAINT FK_977AD86F62FF6CDF FOREIGN KEY (consultation_id) REFERENCES consultation (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n1 ADD CONSTRAINT FK_977AD86FA8CBA5F7 FOREIGN KEY (lot_id) REFERENCES CategorieLot (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n1 ADD CONSTRAINT FK_977AD86F4BBFFC96 FOREIGN KEY (referentiel_clause_n1_id) REFERENCES referentiel_consultation_clauses_n1 (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n2 ADD CONSTRAINT FK_E7389D5BDA9065 FOREIGN KEY (clause_n1_id) REFERENCES consultation_clauses_n1 (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n2 ADD CONSTRAINT FK_E7389D5590A5378 FOREIGN KEY (referentiel_clause_n2_id) REFERENCES referentiel_consultation_clauses_n2 (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n3 ADD CONSTRAINT FK_7974B943196F3F8B FOREIGN KEY (clause_n2_id) REFERENCES consultation_clauses_n2 (id)');
        $this->addSql('ALTER TABLE consultation_clauses_n3 ADD CONSTRAINT FK_7974B943E1B6341D FOREIGN KEY (referentiel_clause_n3_id) REFERENCES referentiel_consultation_clauses_n3 (id)');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2 ADD CONSTRAINT FK_EAE06A40BDA9065 FOREIGN KEY (clause_n1_id) REFERENCES referentiel_consultation_clauses_n1 (id)');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2_clauses_n3 ADD CONSTRAINT FK_1C5CF9E2847212C0 
    FOREIGN KEY (clauses_n2_id) REFERENCES referentiel_consultation_clauses_n2 (id);');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2_clauses_n3 ADD CONSTRAINT FK_1C5CF9E23CCE75A5 
    FOREIGN KEY (clauses_n3_id) REFERENCES referentiel_consultation_clauses_n3 (id);');
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n1 (label, slug) VALUES ('CLAUSES_SOCIALES', null)");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n1 (label, slug) VALUES ('CLAUSES_ENVIRONNEMENTALES', null)");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_SOCIALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_SOCIALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_SOCIALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_TEXT_MARCHE_RESERVE', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_SOCIALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_TEXT_MARCHE_INSERTION', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_SOCIALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_SPECIFICATIONS_TECHNIQUES_ARTICLE_6', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_ENVIRONNEMENTALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_CONDITIONS_EXECUTION_ARTICLE_14', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_ENVIRONNEMENTALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2 (clause_n1_id, label, slug) SELECT id, 'DEFINE_CRITERE_SELECTION_ARTICLE_53.1', null from referentiel_consultation_clauses_n1 where label = 'CLAUSES_ENVIRONNEMENTALES'");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('INSERTION_ACTIVITE_ECONOMIQUE', 'insertionActiviteEconomique')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('CLAUSE_SOCIALE_FORMATION_SCOLAIRE', 'clauseSocialeFormationScolaire')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('LUTTE_CONTRE_DISCRIMATIONS', 'lutteContreDiscriminations')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('COMMERCE_EQUITABLE', 'commerceEquitable')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('ACHATS_ETHIQUES_TRACABILITE_SOCIALE', 'achatsEthiquesTracabiliteSociale')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('AUTRE_CLAUSE_SOCIALE', 'autreClauseSociale')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('DEFINE_TEXT_MARCHE_RESERVE_ATELIER_PROTEGE', 'clauseSocialeReserveAtelierProtege')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('DEFINE_TEXT_A_SIAE', 'clauseSocialeSIAE')");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n3 (label, slug) VALUES ('DEFINE_TEXT_A_ESS', 'clauseSocialeEESS')");

        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'INSERTION_ACTIVITE_ECONOMIQUE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'CLAUSE_SOCIALE_FORMATION_SCOLAIRE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'LUTTE_CONTRE_DISCRIMATIONS';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'COMMERCE_EQUITABLE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'ACHATS_ETHIQUES_TRACABILITE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14' and referentiel_consultation_clauses_n3.label = 'AUTRE_CLAUSE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'INSERTION_ACTIVITE_ECONOMIQUE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'CLAUSE_SOCIALE_FORMATION_SCOLAIRE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'LUTTE_CONTRE_DISCRIMATIONS';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'COMMERCE_EQUITABLE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'ACHATS_ETHIQUES_TRACABILITE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE' and referentiel_consultation_clauses_n3.label = 'AUTRE_CLAUSE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'INSERTION_ACTIVITE_ECONOMIQUE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'CLAUSE_SOCIALE_FORMATION_SCOLAIRE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'LUTTE_CONTRE_DISCRIMATIONS';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'COMMERCE_EQUITABLE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'ACHATS_ETHIQUES_TRACABILITE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53' and referentiel_consultation_clauses_n3.label = 'AUTRE_CLAUSE_SOCIALE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_MARCHE_RESERVE' and referentiel_consultation_clauses_n3.label = 'DEFINE_TEXT_MARCHE_RESERVE_ATELIER_PROTEGE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_MARCHE_RESERVE' and referentiel_consultation_clauses_n3.label = 'DEFINE_TEXT_A_SIAE';");
        $this->addSql("INSERT INTO referentiel_consultation_clauses_n2_clauses_n3 (clauses_n2_id, clauses_n3_id) 
                        SELECT referentiel_consultation_clauses_n2.id, referentiel_consultation_clauses_n3.id from referentiel_consultation_clauses_n2, referentiel_consultation_clauses_n3
                        where referentiel_consultation_clauses_n2.label = 'DEFINE_TEXT_MARCHE_RESERVE' and referentiel_consultation_clauses_n3.label = 'DEFINE_TEXT_A_ESS';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation_clauses_n2 DROP FOREIGN KEY FK_E7389D5BDA9065');
        $this->addSql('ALTER TABLE consultation_clauses_n3 DROP FOREIGN KEY FK_7974B943196F3F8B');
        $this->addSql('ALTER TABLE consultation_clauses_n1 DROP FOREIGN KEY FK_977AD86F4BBFFC96');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2 DROP FOREIGN KEY FK_EAE06A40BDA9065');
        $this->addSql('ALTER TABLE consultation_clauses_n2 DROP FOREIGN KEY FK_E7389D5590A5378');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2_clauses_n3 DROP FOREIGN KEY FK_1C5CF9E2847212C0');
        $this->addSql('ALTER TABLE consultation_clauses_n3 DROP FOREIGN KEY FK_7974B943E1B6341D');
        $this->addSql('ALTER TABLE referentiel_consultation_clauses_n2_clauses_n3 DROP FOREIGN KEY FK_1C5CF9E23CCE75A5');
        $this->addSql('DROP TABLE consultation_clauses_n1');
        $this->addSql('DROP TABLE consultation_clauses_n2');
        $this->addSql('DROP TABLE consultation_clauses_n3');
        $this->addSql('DROP TABLE referentiel_consultation_clauses_n1');
        $this->addSql('DROP TABLE referentiel_consultation_clauses_n2');
        $this->addSql('DROP TABLE referentiel_consultation_clauses_n2_clauses_n3');
        $this->addSql('DROP TABLE referentiel_consultation_clauses_n3');
    }
}
