<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Entity\Consultation\ClausesN1;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220908091853 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20104 - Enrichissement Webservice Récupération consultation (method GET)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            sprintf(
                "UPDATE `referentiel_consultation_clauses_n1` SET `slug` = '%s' WHERE `LABEL` = 'CLAUSES_SOCIALES';"
                , ClausesN1::CLAUSE_SOCIALES
            )
        );
        $this->addSql(
            sprintf(
                "UPDATE `referentiel_consultation_clauses_n1` SET `slug` = '%s' WHERE `LABEL` = 'CLAUSES_ENVIRONNEMENTALES';"
                , ClausesN1::CLAUSES_ENVIRONNEMENTALES
            )
        );

        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'conditionExecution' WHERE `LABEL` = 'DEFINE_TEXT_CLAUSE_SOCIALE_ARTICLE_14';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'specificationTechnique' WHERE `LABEL` = 'DEFINE_TEXT_CLAUSE_SPECIFICATION_TECHNIQUE';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'critereAttributionMarche' WHERE `LABEL` = 'DEFINE_TEXT_CRITERE_RELATIF_INSERTION_ARTICLE_53';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'marcheReserve' WHERE `LABEL` = 'DEFINE_TEXT_MARCHE_RESERVE';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'insertion' WHERE `LABEL` = 'DEFINE_TEXT_MARCHE_INSERTION';");

        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'specificationsTechniques' WHERE `LABEL` = 'DEFINE_SPECIFICATIONS_TECHNIQUES_ARTICLE_6';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'conditionExecution' WHERE `LABEL` = 'DEFINE_CONDITIONS_EXECUTION_ARTICLE_14';");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = 'criteresSelections' WHERE `LABEL` = 'DEFINE_CRITERE_SELECTION_ARTICLE_53.1';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `referentiel_consultation_clauses_n1` SET `slug` = null;");
        $this->addSql("UPDATE `referentiel_consultation_clauses_n2` SET `slug` = null;");
    }
}
