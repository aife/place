<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220908153238 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20151 - Message envoyé de Apache Flink  vers MPE à travers KAFKA (confirmation de publication)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi ADD id INT AUTO_INCREMENT NOT NULL, ADD message_details LONGTEXT DEFAULT NULL, CHANGE statut statut INT NOT NULL, DROP PRIMARY KEY, ADD PRIMARY KEY (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi MODIFY id INT NOT NULL');
        $this->addSql('ALTER TABLE interface_suivi DROP PRIMARY KEY');
        $this->addSql('ALTER TABLE interface_suivi DROP id, DROP message_details, CHANGE statut statut VARCHAR(30) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
        $this->addSql('ALTER TABLE interface_suivi ADD PRIMARY KEY (uuid)');
    }
}
