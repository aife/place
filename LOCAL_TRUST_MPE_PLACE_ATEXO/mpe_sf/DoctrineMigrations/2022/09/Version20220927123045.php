<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220927123045 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-20196 - Ajout d'un lien + image dans la page « Nos Partenaires » vers le portail APPACH";
    }

    public function up(Schema $schema): void
    {
        if ((bool) getenv('SPECIFIQUE_PLACE') === true) {
            $this->addSql("INSERT INTO `Partenaire` (`initials`, `desc_partenaire`, `desc_partenaire_fr`, `desc_partenaire_en`, `desc_partenaire_es`, `desc_partenaire_it`, `desc_partenaire_ar`, `desc_partenaire_su`, `lien_img`, `lien_externe`, `title`) VALUES ('APProch', 'Portail permettant aux entreprises d’identifier les projets d’achats de l’État et de leurs établissements publics', 'Portail permettant aux entreprises d’identifier les projets d’achats de l’État et de leurs établissements publics', NULL, NULL, NULL, NULL, NULL, 'logo-approch.jpg', 'https://projets-achats.marches-publics.gouv.fr/', 'Accéder au site APProch (lien externe)');");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM Partenaire WHERE initials = 'APProch';");
    }
}
