<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220912090819 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20374 - Enrichissement de TNCP suivi';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi ADD etape VARCHAR(30) DEFAULT NULL, CHANGE interface flux VARCHAR(30) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE interface_suivi DROP etape, CHANGE flux interface VARCHAR(30) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');
    }
}
