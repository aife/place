<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220901143827 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-20197 - Ajout d'une nouvelle entrée dans le menu de navigation horizontal vers le portail APPACH ";
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
        'CREATE TABLE IF NOT EXISTS `liens_menu_entreprise_personnalise`(
                `id` INT AUTO_INCREMENT NOT NULL,
                `label` VARCHAR(100) NOT NULL,
                `url` VARCHAR(255) NOT NULL,
                `display_order` INT NOT NULL,
                PRIMARY KEY(`id`)) 
            DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS `liens_menu_entreprise_personnalise`');
    }
}
