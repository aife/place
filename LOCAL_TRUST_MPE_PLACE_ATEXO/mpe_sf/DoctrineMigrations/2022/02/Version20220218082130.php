<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220218082130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'PLACE-461 - Redressement des archives';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation_archive` 
DROP INDEX  IF EXISTS `unicite_consultation`;");

        $this->addSql('ALTER TABLE `consultation_archive` 
    ADD COLUMN `consultation_id` INT(11)  ;');

        $this->addSql("Update consultation_archive ca set ca.consultation_id = 
    (SELECT c.id FROM consultation c WHERE ca.`consultation_ref` = c.reference 
                                       and ca.`organisme` = c.`organisme` 
                                       and c.reference is not null
        )");

        $this->addSql("Update consultation_archive ca set ca.consultation_id = 
    (SELECT c.id FROM consultation c WHERE ca.`consultation_ref` = c.id 
                                       and ca.`organisme` = c.`organisme` 
                                       and c.reference is not null
        ) where ca.consultation_id is null;");

        $this->addSql("Update consultation_archive ca set ca.consultation_id = 
    (SELECT c.id FROM consultation c WHERE ca.`consultation_ref` = c.id 
                                       and ca.`organisme` = c.`organisme` 
                                       and c.reference is null
        ) where ca.consultation_id is null");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.'
        );
    }
}
