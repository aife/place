<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20220223102528 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MES-2377 - [TerNum][MER] Déploiement v2022-00.02.00-RC2';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('UPDATE CategorieLot set categorie = LEFT(categorie, 1) WHERE categorie LIKE "%.%";');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
