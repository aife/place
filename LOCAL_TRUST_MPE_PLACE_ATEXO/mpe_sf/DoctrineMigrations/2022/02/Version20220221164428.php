<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220221164428 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18364 - Web service référentiels nature prestation (categorieConsultation)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `CategorieConsultation` 
            ADD `id_externe` VARCHAR(10) NOT NULL COMMENT 'code externe utilisé dans le cadre des WS Athena';
        ");

        $this->addSql("UPDATE `CategorieConsultation` SET `id_externe`='TV' WHERE `libelle` = 'Travaux';");
        $this->addSql("UPDATE `CategorieConsultation` SET `id_externe`='FNT' WHERE `libelle` = 'Fournitures';");
        $this->addSql("UPDATE `CategorieConsultation` SET `id_externe`='SV' WHERE `libelle` = 'Services';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `CategorieConsultation` DROP `id_externe`;");
    }
}
