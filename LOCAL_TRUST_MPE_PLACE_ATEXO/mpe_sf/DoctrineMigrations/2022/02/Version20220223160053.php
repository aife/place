<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220223160053 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18359 - Web service création consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE consultation CHANGE titre titre LONGTEXT DEFAULT NULL, CHANGE resume resume LONGTEXT DEFAULT NULL'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE consultation CHANGE titre titre LONGTEXT NOT NULL, CHANGE resume resume LONGTEXT NOT NULL'
        );
    }
}
