<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220223102628 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18613 - Athena WS GET/POST/PUT/DELETE - Ajout FK Constraint entité Lot et CategorieConsultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `CategorieLot` 
            ADD CONSTRAINT `FK_CategorieLot_CategorieConsultation` FOREIGN KEY (`categorie`) 
            REFERENCES `CategorieConsultation`(`id`)  ON UPDATE CASCADE;
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `CategorieLot` DROP FOREIGN KEY FK_CategorieLot_CategorieConsultation;");
    }
}
