<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220228153952 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18674 - Activation/désactivation Module BI';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `module_BI` BOOLEAN NOT NULL DEFAULT \'1\' COMMENT \'Active le module BI\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `module_BI`');
    }
}
