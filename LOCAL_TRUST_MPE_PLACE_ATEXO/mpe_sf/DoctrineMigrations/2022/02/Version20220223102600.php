<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220223102600 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-19809 - Exception lors des migrations Doctrine : FK_CategorieLot_CategorieConsultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'ALTER TABLE `consultation` CHANGE `categorie` `categorie` VARCHAR(30) NULL;'
        );
        $this->addSql(
            'ALTER TABLE `CategorieLot` CHANGE `categorie` `categorie` VARCHAR(30) NULL;'
        );

        $this->addSql(
            'update consultation set categorie = null
                    where categorie not in (select id from CategorieConsultation);'
        );
        $this->addSql(
            'update CategorieLot set categorie = null
                    where categorie not in (select id from CategorieConsultation);'
        );
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.'
        );
    }
}
