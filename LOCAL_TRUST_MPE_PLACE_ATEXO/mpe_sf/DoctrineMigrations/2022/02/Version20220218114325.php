<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220218114325 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18579 - RDR - Erreur appel api gateway sur synchroAttestations - FormulaireReponseConsultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `uri`= REPLACE(`uri`, '/v2/', '/');");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `t_document_type` SET `uri`= CONCAT('/v2', `uri`) WHERE `uri` is not null;");
    }
}
