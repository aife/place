<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220204143448 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18458 - Amelioration perf tableau de bord agent';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'update consultation set reference_consultation_init = null '
            . 'where reference_consultation_init not in (select id from consultation)'
        );
        $this->addSql(
            'ALTER TABLE consultation '
            . 'CHANGE `reference_consultation_init` `reference_consultation_init` INT(11) NULL DEFAULT NULL, '
            . 'ADD CONSTRAINT FK_reference_consultation_init '
            . 'FOREIGN KEY IF NOT EXISTS (reference_consultation_init) REFERENCES consultation (id)'
        );

        $sqlParts = [];

        for ($i = 1; $i < 40; $i++) {
            $sqlParts[] = "DROP INDEX IF EXISTS `DATE_FIN_UNIX_${i}`";
        }
        $this->addSql(
            'ALTER TABLE `consultation` '
            . implode(', ', $sqlParts)
        );
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
