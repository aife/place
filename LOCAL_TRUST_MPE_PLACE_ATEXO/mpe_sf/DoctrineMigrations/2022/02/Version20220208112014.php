<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220208112014 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18026 - Amélioration du WS contrat';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `web_service` (`id`, `route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
            VALUES (NULL, '/api/{version}/contrats/{id}.{format}', 'GET', 'Get a contract', '1')
        ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `web_service` WHERE `web_service`.`route_ws` = '/api/{version}/contrats/{id}.{format}'");
    }
}
