<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220210154940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-18072 - [Module publicité] Ajout procédure ouverte / restreinte Ecran Identification';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` ADD `procedure_ouverte` BOOLEAN NULL AFTER `autre_technique_achat`;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` DROP IF EXISTS `procedure_ouverte`;");
    }
}
