<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221010100101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20336 - [Merge PLACE] Conditionnement du comportement des boutons de gestion des contrats dans MPE';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            ALTER TABLE `configuration_organisme`
            ADD COLUMN IF NOT EXISTS `gestion_contrat_dans_exec` TINYINT NOT NULL DEFAULT '0'
            COMMENT 'Si 1, la gestion des contrats notifiés (modification, consultation, avenants…) se fait uniquement dans EXEC. Si 0, la gestion des contrats notifiés peut se faire dans MPE.';
        ");
        if ((bool) getenv('SPECIFIQUE_PLACE') === true) {
            $this->addSql("UPDATE configuration_organisme SET `gestion_contrat_dans_exec` = '1'");
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` DROP IF EXISTS `gestion_contrat_dans_exec`");
    }
}
