<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221011094009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-20537 - Ajout les clauses sociales dans les données essentielles / WS';
    }

    public function up(Schema $schema): void
    {
        // Cette migration ne sert à rien, mais deja éxécuté en prod, donc on la laisse
    }

    public function down(Schema $schema): void
    {
    }
}
