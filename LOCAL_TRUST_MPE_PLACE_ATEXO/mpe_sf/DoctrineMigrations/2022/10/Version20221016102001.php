<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221016102001 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-13853 SIMAP - Le no d'enregistrement national est trop court";
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`, `updated_at`) VALUES
('NOMBRE_MAX_CARACTERES_ID_ENTREPRISE_ETRANGERE', '9', NOW());");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM `configuration_client` 
WHERE `configuration_client`.`parameter` = 'NOMBRE_MAX_CARACTERES_ID_ENTREPRISE_ETRANGERE' ;");
    }
}
