<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221227141939 extends AbstractMigration
{

    public function getDescription(): string
    {
        return 'MPE-21182 - Lancement de la commande mpe:hash-admin-plain-passwords-to-argon2'
        . 'via une doctrine migration';
    }

    public function up(Schema $schema): void
    {
        //$serviceAdministrator = $this->container->get('service.administrator');
        //$serviceAdministrator->convertPasswordToArgon();
    }

    public function down(Schema $schema): void
    {
        // not allowed
    }
}
