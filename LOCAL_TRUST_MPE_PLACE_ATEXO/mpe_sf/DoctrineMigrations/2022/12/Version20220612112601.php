<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220612112601 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-19945 [FTV] VSR MS1 - MPE PROD : Lenteurs lors de l'accès à la page des Accords-cadres et SAD";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("create index  IF NOT EXISTS `numero_contrat` on t_contrat_titulaire (`numero_contrat`);");
        $this->addSql("create index  IF NOT EXISTS `lien_AC_SAD` on t_contrat_titulaire (`lien_AC_SAD`);");
        $this->addSql("create index  IF NOT EXISTS `organisme` on t_contrat_titulaire (`organisme`);");
        $this->addSql("create index  IF NOT EXISTS `service_id` on t_contrat_titulaire (`service_id`);");
        $this->addSql("create index  IF NOT EXISTS `statut_contrat` on t_contrat_titulaire (`statut_contrat`);");
        $this->addSql("create index  IF NOT EXISTS `categorie` on t_contrat_titulaire (`categorie`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale_ateliers_proteges` 
            on t_contrat_titulaire (`clause_sociale_ateliers_proteges`);");
        $this->addSql("create index  IF NOT EXISTS `clause_env_criteres_select` 
        on t_contrat_titulaire (`clause_env_criteres_select`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale` on t_contrat_titulaire (`clause_sociale`);");
        $this->addSql("create index  IF NOT EXISTS `clause_env_specs_techniques` 
        on t_contrat_titulaire (`clause_env_specs_techniques`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale_insertion` on t_contrat_titulaire (`clause_sociale_insertion`);");
        $this->addSql("create index  IF NOT EXISTS `clause_environnementale` on t_contrat_titulaire (`clause_environnementale`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale_siae` on t_contrat_titulaire (`clause_sociale_siae`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale_condition_execution` 
        on t_contrat_titulaire (`clause_sociale_condition_execution`);");
        $this->addSql("create index  IF NOT EXISTS `clause_sociale_ess` on t_contrat_titulaire (`clause_sociale_ess`);");
        $this->addSql("create index  IF NOT EXISTS `clause_env_cond_execution` on t_contrat_titulaire (`clause_env_cond_execution`);");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("drop index `numero_contrat` on t_contrat_titulaire;");
        $this->addSql("drop index `lien_AC_SAD` on t_contrat_titulaire;");
        $this->addSql("drop index `organisme` on t_contrat_titulaire ;");
        $this->addSql("drop index `service_id` on t_contrat_titulaire;");
        $this->addSql("drop index `statut_contrat` on t_contrat_titulaire;");
        $this->addSql("drop index `categorie` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale_ateliers_proteges` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_env_criteres_select` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_env_specs_techniques` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale_insertion` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_environnementale` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale_siae` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale_condition_execution` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_sociale_ess` on t_contrat_titulaire;");
        $this->addSql("drop index `clause_env_cond_execution` on t_contrat_titulaire;");
    }
}
