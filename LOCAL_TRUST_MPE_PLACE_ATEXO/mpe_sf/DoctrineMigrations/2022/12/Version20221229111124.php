<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221229111124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'update agent_tncp';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `Agent` SET technique = 1 where login = 'agent_tncp';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `Agent` SET technique = 0 where login = 'agent_tncp';");
    }
}
