<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Entity\HabilitationAdministrateur;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221221144024 extends AbstractMigration
{
    private array $administrateurs;

    public function getDescription(): string
    {
        return "MPE-896 - [PLACE] RdR 2023-00.01.00 - Page d'historisation TNCP B1 - Différentes habilitations d'accès";
    }

    public function preUp(Schema $schema): void
    {
        $stmt = $this->connection->prepare('SELECT id FROM Administrateur;');
        $this->administrateurs = $stmt->executeQuery()->fetchFirstColumn();
    }

    public function up(Schema $schema): void
    {
        // Création des tables de gestion d'habilitation
        $this->addSql('
            CREATE TABLE IF NOT EXISTS habilitation_administrateur (
                id INT AUTO_INCREMENT NOT NULL,
                habilitation VARCHAR(150) NOT NULL,
                PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
        ');
        $this->addSql('
            CREATE TABLE IF NOT EXISTS habilitation_administrateur_administrateur (
                habilitation_administrateur_id INT NOT NULL,
                administrateur_id INT NOT NULL,
                INDEX IDX_5D2932891B6F64D1 (habilitation_administrateur_id),
                INDEX IDX_5D2932897EE5403C (administrateur_id),
                PRIMARY KEY(habilitation_administrateur_id, administrateur_id)) 
                DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB
        ');
        $this->addSql('
            ALTER TABLE habilitation_administrateur_administrateur 
                ADD CONSTRAINT FK_5D2932891B6F64D1 FOREIGN KEY IF NOT EXISTS (habilitation_administrateur_id) 
                    REFERENCES habilitation_administrateur (id) ON DELETE CASCADE
        ');
        $this->addSql('
            ALTER TABLE habilitation_administrateur_administrateur 
                ADD CONSTRAINT FK_5D2932897EE5403C FOREIGN KEY IF NOT EXISTS (administrateur_id) 
                    REFERENCES Administrateur (id) ON DELETE CASCADE
        ');


        // Habilitation administrateur pour n'afficher que l'onglet interface suivi TNCP
        $this->addSql("
            INSERT INTO `habilitation_administrateur` (`id`, `habilitation`)
            VALUES (1, '" . HabilitationAdministrateur::ACCESS_ADMINISTRATION_PANELS . "')
        ");

        // Habilitation administrateur pour avoir les droits d'écriture sur l'interface TNCP
        $this->addSql("
            INSERT INTO `habilitation_administrateur` (`id`, `habilitation`)
            VALUES (2, '" . HabilitationAdministrateur::ACCESS_INTERFACE_SUIVI_WRITE . "')
        ");


        // Rattrapage de données pour que les utilisateurs actuels puissent avoir accès aux onglets de configuration
        foreach ($this->administrateurs as $administrateur) {
            // Habilitation administration general
            $this->addSql(
                "INSERT INTO habilitation_administrateur_administrateur (habilitation_administrateur_id, administrateur_id) 
                    VALUES (
                            (SELECT id FROM habilitation_administrateur WHERE habilitation = '".HabilitationAdministrateur::ACCESS_ADMINISTRATION_PANELS."' )
                            , '" . $administrateur . "'
                            )
                    ;"
            );

            // Habilitation droits écriture interface tncp
            $this->addSql(
                "INSERT INTO habilitation_administrateur_administrateur (habilitation_administrateur_id, administrateur_id) 
                    VALUES (
                            (SELECT id FROM habilitation_administrateur WHERE habilitation = '".HabilitationAdministrateur::ACCESS_INTERFACE_SUIVI_WRITE."' )
                            , '" . $administrateur . "'
                            )
                    ;"
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE IF EXISTS habilitation_administrateur_administrateur DROP FOREIGN KEY IF EXISTS FK_5D2932891B6F64D1');
        $this->addSql('TRUNCATE TABLE habilitation_administrateur_administrateur');
        $this->addSql('DROP TABLE IF EXISTS habilitation_administrateur_administrateur');
        $this->addSql('DROP TABLE IF EXISTS habilitation_administrateur');
    }
}
