<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221209151714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21278 - Clause N3 se baser sur le Slug et non le Label API V0 + 
        renommage du label LUTTE_CONTRE_DISCRIMATIONS par LUTTE_CONTRE_DISCRIMINATIONS';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_consultation_clauses_n3 
set `label` = \'LUTTE_CONTRE_DISCRIMINATIONS\' WHERE `label` = \'LUTTE_CONTRE_DISCRIMATIONS\';');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE referentiel_consultation_clauses_n3 
set `label` = \'LUTTE_CONTRE_DISCRIMATIONS\' WHERE `label` = \'LUTTE_CONTRE_DISCRIMINATIONS\';');
    }
}
