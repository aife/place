<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221216155244 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-21326 - [RdR 2023-00.01] [PLACE] Erreur migrations Doctrine EnchereEntreprisePmi et EnchereOffre';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` DROP FOREIGN KEY IF EXISTS EnchereEntreprisePmi_ibfk_1;');
        $this->addSql('ALTER TABLE `EnchereOffre` DROP FOREIGN KEY IF EXISTS EnchereOffre_ibfk_1;');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            "Ces clés étrangères n'existent pas forcément sur tous les schémas,
            donc on ne sait pas si on doit les recréer en cas d'échec de la migration..."
        );
    }
}
