<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20221209075854 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Correction de date sur les tables Agent, Inscrit et Historique_suppression_agent';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE Agent 
                           SET 
                               date_creation = REPLACE(date_creation, '-', ':'),
                               date_modification = REPLACE(date_modification, '-', ':'),
                               deleted_at = REPLACE(deleted_at, '-', ':')
                           WHERE
                                   date_creation LIKE '%-%-%'
                                       or
                                   date_modification LIKE '%-%-%'
                                       or
                                   deleted_at LIKE '%-%-%'
                            ;");

        $this->addSql(" UPDATE Inscrit
                            SET 
                                date_creation = REPLACE(date_creation, '-', ':'), 
                                date_modification = REPLACE(date_modification, '-', ':') 
                            WHERE 
                                date_creation LIKE '%-%-%' 
                               or
                                date_modification LIKE '%-%-%'
                            ;");

        $this->addSql(" UPDATE Historique_suppression_agent 
                            SET 
                                date_suppression = REPLACE(date_suppression, '-', ':') 
                            WHERE 
                                date_suppression LIKE '%-%-%'
                            ;");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
