<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221229085255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-833 - [PLACE] RdR 2023-00.01.00 - Régression graphique CGU';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql('INSERT INTO `configuration_messages_traduction` ( `source`, `target`, `langue_id`) 
VALUES ("TEXT_APPROBATION_APPLICATION_AGENT","Les termes des présentes conditions générales d\'utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées aux fonctionnalités, de l\'évolution de la réglementation ou pour tout autre motif jugé nécessaire. Il appartient à l\'utilisateur de s\'informer des conditions générales d\'utilisation de la plateforme PLACE en vigueur. <br/<br/> L\'utilisation de PLACE nécessite de disposer d\'un environnement informatique compatible et de se conformer aux <a href=\'/agent/footer/prerequis-techniques\'>prérequis techniques</a>.
<h3>Avertissement et recommandation aux entreprises</h3>
<p>    Les questions ou demandes d’informations complémentaires relatives à une consultation peuvent être posées via le profil d’acheteur ou, le cas échéant, à une adresse mail mentionnée par l’acheteur dans les documents de la consultation.
<br/><br/>
Afin de conserver une trace des échanges, il est recommandé de recourir au service de messagerie du profil d’acheteur. Si un opérateur économique, après avoir pris connaissance de l’ensemble des documents de la consultation, pose une question et que la réponse a un impact sur la remise des candidatures ou des offres, la réponse est communiquée à tous les opérateurs économiques ayant téléchargé les documents de consultation avec authentification, et sera visible de tous les opérateurs économiques n’ayant pas encore téléchargé les documents.
<br/><br/>
Les plis doivent être reçus dans les délais fixés dans l’avis de publicité ou dans les autres documents de la consultation.
<br/><br/>
L’opérateur économique doit donc prévoir le temps nécessaire pour que son pli soit reçu dans le délai fixé par l’acheteur, surtout si les fichiers sont volumineux et/ou si le réseau a un faible débit. Il lui est ainsi conseillé de déposer ses plis au minimum quatre heures avant la date limite de remise des plis afin de garantir son envoi dématérialisé et lui permettre de remédier à d’éventuels problèmes techniques.
<br/><br/>
Par ailleurs, les opérateurs économiques sont alertés que la plateforme déconnecte automatiquement l’utilisateur en cas d’inactivité supérieure à trente minutes.
<br/><br/>
Attention, les plis, dont le téléchargement a commencé avant la date et l’heure limite mais s’est achevé hors délai sont éliminés par l’acheteur.
<br/><br/>
L’opérateur économique est invité à remettre une copie de sauvegarde dans les conditions fixées dans le règlement de la consultation afin d’anticiper toute difficulté de dernière minute. En effet, en cas de réception d’un pli électronique hors délai, l’acheteur est tenu d’ouvrir la copie de sauvegarde régulièrement transmise.
</p>
<h3>Certificat électronique</h3>
<p>
    Un certificat électronique permet de prouver son identité et de signer électroniquement les offres avec la même valeur juridique qu\'une signature manuscrite.
<br/><br/>
L’annexe 12 du code de la commande publique relatif à la signature électronique des contrats de la commande publique prévoit que le certificat doit être qualifié conformément au règlement n° 910/2014 du parlement européen et du conseil du 23 juillet 2014 sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur et abrogeant la directive 1999/93/CE dit « eIDAS ».
<br/><br/>
Pour l\'obtention d\'un certificat électronique, la liste des prestataires de services de confiance qualifiés délivrant des certificats de signatures conformes à la réglementation est accessible sur la page des <a href=\'/?page=entreprise.ListeAcRGS&calledFrom=entreprise\'>autorités de certification européennes</a>.
</p>
<h3>Assistance en ligne</h3>
<p>Un service de support est à votre disposition de 9h00 à 19h00 les jours ouvrés pour répondre à vos questions ou vous assister en cas de difficultés (consulter la page dédiée). L’assistance en ligne est accessible depuis chaque page via la languette située à droite de l’écran.</p>", 1)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM `configuration_messages_traduction` WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION_AGENT" and `langue_id` = 1;');
    }
}
