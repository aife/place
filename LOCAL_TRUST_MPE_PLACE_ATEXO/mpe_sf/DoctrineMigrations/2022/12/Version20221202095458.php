<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221202095458 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-832 - [PLACE] RdR 2023-00.01.00 - RechercheAvancéeArchive - Recherche d'AC/SAD à masquer";
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool)getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool)getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql(
            'update configuration_plateforme '
            . 'set affichage_recherche_avancee_agent_ac_sad_transversaux = "0";'
        );
    }

    public function down(Schema $schema): void
    {
        $this->warnIf(true !== (bool)getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool)getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql(
            'update configuration_plateforme '
            . 'set affichage_recherche_avancee_agent_ac_sad_transversaux = "1";'
        );
    }
}
