<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324142422 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.02.00 MPE-11809';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS echange_doc_application_client (id INT AUTO_INCREMENT NOT NULL, echange_doc_application_id INT DEFAULT NULL, code VARCHAR(255) NOT NULL, libelle VARCHAR(255) NOT NULL, actif TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_echange_doc_application_id (echange_doc_application_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS echange_doc (id INT AUTO_INCREMENT NOT NULL, echange_doc_application_client_id INT DEFAULT NULL, objet VARCHAR(100) NOT NULL, description VARCHAR(500) NOT NULL,consultation_id INT DEFAULT NULL, agent_id INT DEFAULT NULL, statut VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, INDEX IDX_echange_doc_application_client_id (echange_doc_application_client_id), INDEX IDX_consultation_id (consultation_id), INDEX IDX_agent_id (agent_id),  PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS echange_doc_application (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(255) NOT NULL, libelle VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS echange_doc_application_client_organisme (id INT AUTO_INCREMENT NOT NULL, echange_doc_application_client_id INT DEFAULT NULL, organisme_id INT DEFAULT NULL, INDEX IDX_echange_doc_application_client_id (echange_doc_application_client_id), INDEX IDX_organisme_id (organisme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS echange_doc_blob (id INT AUTO_INCREMENT NOT NULL, echange_doc_id INT DEFAULT NULL, blob_organisme_id INT DEFAULT NULL, chemin LONGTEXT DEFAULT NULL, INDEX IDX_echange_doc_id (echange_doc_id), INDEX IDX_blob_organisme_id (blob_organisme_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE echange_doc_application_client ADD CONSTRAINT FK_1E7B3AAF4A7843DC FOREIGN KEY IF NOT EXISTS (echange_doc_application_id) REFERENCES echange_doc_application (id)');
        $this->addSql('ALTER TABLE echange_doc ADD CONSTRAINT FK_FEEA7C99561BA9BE FOREIGN KEY IF NOT EXISTS (echange_doc_application_client_id) REFERENCES echange_doc_application_client (id)');
        $this->addSql('ALTER TABLE echange_doc ADD CONSTRAINT FK_FEEA7C9962FF6CDF FOREIGN KEY IF NOT EXISTS (consultation_id) REFERENCES consultation (id)');
        $this->addSql('ALTER TABLE echange_doc ADD CONSTRAINT FK_FEEA7C993414710B FOREIGN KEY IF NOT EXISTS (agent_id) REFERENCES Agent (id)');
        $this->addSql('ALTER TABLE echange_doc_application_client_organisme ADD CONSTRAINT FK_B504956B561BA9BE FOREIGN KEY IF NOT EXISTS (echange_doc_application_client_id) REFERENCES echange_doc_application_client (id)');
        $this->addSql('ALTER TABLE echange_doc_application_client_organisme ADD CONSTRAINT FK_B504956B5DDD38F5 FOREIGN KEY IF NOT EXISTS (organisme_id) REFERENCES Organisme (id)');
        $this->addSql('ALTER TABLE echange_doc_blob ADD CONSTRAINT FK_3EEF16D757D67B2D FOREIGN KEY IF NOT EXISTS (echange_doc_id) REFERENCES echange_doc (id)');
        $this->addSql('ALTER TABLE echange_doc_blob ADD CONSTRAINT FK_3EEF16D783A2B91F FOREIGN KEY IF NOT EXISTS (blob_organisme_id) REFERENCES blobOrganisme_file (id)');

        $this->addSql("INSERT INTO `echange_doc_application` (`id`, `code`, `libelle`) VALUES ('1', 'EXPOSITION_WS', 'Exposition simple via Web Service')");
        $this->addSql("INSERT INTO `echange_doc_application_client` (`id`, `echange_doc_application_id`, `code`, `libelle`, `actif`) VALUES ('1', '1', 'EXPOSITION_WS_1', 'Exposition par Web Service', '1')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE echange_doc DROP FOREIGN KEY FK_FEEA7C99561BA9BE');
        $this->addSql('ALTER TABLE echange_doc DROP FOREIGN KEY FK_FEEA7C9962FF6CDF');
        $this->addSql('ALTER TABLE echange_doc DROP FOREIGN KEY FK_FEEA7C993414710B');
        $this->addSql('ALTER TABLE echange_doc_application_client_organisme DROP FOREIGN KEY FK_B504956B561BA9BE');
        $this->addSql('ALTER TABLE echange_doc_application_client_organisme DROP FOREIGN KEY FK_B504956B5DDD38F5');
        $this->addSql('ALTER TABLE echange_doc_blob DROP FOREIGN KEY FK_3EEF16D757D67B2D');
        $this->addSql('ALTER TABLE echange_doc_blob DROP FOREIGN KEY FK_3EEF16D783A2B91F');
        $this->addSql('ALTER TABLE echange_doc_application_client DROP FOREIGN KEY FK_1E7B3AAF4A7843DC');
        $this->addSql('DROP TABLE echange_doc_application_client');
        $this->addSql('DROP TABLE echange_doc');
        $this->addSql('DROP TABLE echange_doc_application');
        $this->addSql('DROP TABLE echange_doc_application_client_organisme');
        $this->addSql('DROP TABLE echange_doc_blob');
    }
}
