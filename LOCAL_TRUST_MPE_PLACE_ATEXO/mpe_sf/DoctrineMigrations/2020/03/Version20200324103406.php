<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324103406 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 MPE-11808';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE HabilitationAgent ADD COLUMN `acces_echange_documentaire` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'habilitation a l échange des documents\'');
        $this->addSql('ALTER TABLE HabilitationProfil ADD COLUMN `acces_echange_documentaire` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'habilitation a l échange des documents\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `HabilitationAgent` DROP `acces_echange_documentaire`');
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP `acces_echange_documentaire`');
    }
}
