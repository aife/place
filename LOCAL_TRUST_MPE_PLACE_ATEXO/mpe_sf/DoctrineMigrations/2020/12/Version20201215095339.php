<?php

declare(strict_types=1);

namespace App\Migrations;

use Application\Service\Atexo\WebServiceApiGouvEntreprise\AttestationAgefiphWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\AttestationCertificatQualificationWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\AttestationCotisationRetraiteWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\AttestationFiscaleWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\AttestationSocialeWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\CarteProfessionnelleWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\CertificatQualibatWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\CotisationEmployeurWebService;
use Application\Service\Atexo\WebServiceApiGouvEntreprise\DocumentWebService;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201215095339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-14510 - update className';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(CertificatQualibatWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%CertificatQualibatWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(AttestationFiscaleWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%AttestationFiscaleWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(AttestationSocialeWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%AttestationSocialeWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(CarteProfessionnelleWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%CarteProfessionnelleWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(DocumentWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%DocumentWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(AttestationCotisationRetraiteWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%AttestationCotisationRetraiteWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(AttestationCertificatQualificationWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%AttestationCertificatQualificationWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(CotisationEmployeurWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%CotisationEmployeurWebService%'");

        $this->addSql("UPDATE `t_document_type` SET `class_name` = '" . addslashes(AttestationAgefiphWebService::class) . "' 
WHERE `t_document_type`.`class_name` like '%AttestationAgefiphWebService%'");
    }
}
