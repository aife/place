<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201208162834 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14341 - Outils de formation - lien non fonctionnel';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `configuration_client` WHERE `parameter` = 'URL_DOCS_FORMATION'");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_FORMATION', 'formation')");
    }
}
