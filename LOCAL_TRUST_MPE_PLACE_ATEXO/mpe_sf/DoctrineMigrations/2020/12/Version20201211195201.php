<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201211195201 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14423 - [Entreprise] Guide utilisateur non disponible';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `configuration_client` WHERE `parameter` = 'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE'");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE', 'guide_utilisateur')");
    }
}
