<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Plate-forme virtuelle : association des alertes, retraits, questions et dépôts
 */
final class Version20201208172620 extends AbstractMigration
{
    private $tables = [
        'T_MesRecherches',
        'Offres',
        'questions_dce',
        'Telechargement',
    ];

    public function getDescription(): string
    {
        return 'MPE-14030 : Plate-forme virtuelle : association des alertes, retraits, questions et dépôts';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        foreach ($this->tables as $table) {
            $this->addSql(
                'ALTER TABLE ' . $table
                . ' ADD COLUMN `plateforme_virtuelle_id` INT(11) UNSIGNED DEFAULT NULL,'
                . ' ADD CONSTRAINT `fk_plateforme_virtuelle_' . strtolower($table) . '`'
                . ' FOREIGN KEY (plateforme_virtuelle_id) REFERENCES plateforme_virtuelle(id)'
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        foreach ($this->tables as $table) {
            $this->addSql(
                'ALTER TABLE ' . $table
                . ' DROP FOREIGN KEY `fk_plateforme_virtuelle_' . strtolower($table) . '`,'
                . ' DROP COLUMN `plateforme_virtuelle_id`'
            );
        }
    }
}
