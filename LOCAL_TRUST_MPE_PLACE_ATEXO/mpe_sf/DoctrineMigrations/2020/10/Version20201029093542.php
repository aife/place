<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Renommage de la route du ws modification contrat
 * Ajout de l'habilitation agent pour la modification du contrat
 */
final class Version20201029093542 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Renommage de la route du ws modification contrat";
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $query = <<<EOF
            update `web_service` set route_ws = '/api/{version}/contrats/modifications.{format}' 
            WHERE route_ws = '/api/{version}/contrats/modification.{format}';
        EOF;

        $this->addSql($query);

        // ajout de l'insert de migration:tiers car elle a manqué sur certaines integ!
        $message = addslashes('O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:\\"\\0Symfony'
            . '\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component\\\\Messenger'
            . '\\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp'
            . '\\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\";s:21:'
            . '\\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message'
            . '\\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message\\\\CommandeMessenger'
            . '\\0commande\\";s:19:\\"mpe:migration:tiers\\";s:37:\\"\\0App\\\\Message\\\\CommandeMessenger\\0params'
            . '\\";a:0:{}}}');

        $sql = 'INSERT INTO messenger_messages 
        (body, headers, queue_name, created_at, available_at, delivered_at) 
        SELECT DISTINCT \'' . $message . '\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null ;';
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/contrats/modifications.{format}' 
and `method_ws` =  'POST'");
    }
}
