<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajout de la colonne code NUT dans la table GeolocalisationN2 pour la publicité
 */
final class Version20201020184714 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE GeolocalisationN2 ADD IF NOT EXISTS code_nuts VARCHAR(10) NULL");

        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR101' where denomination2 = '75'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR102' where denomination2 = '77'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR103' where denomination2 = '78'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR104' where denomination2 = '91'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR105' where denomination2 = '92'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR106' where denomination2 = '93'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR107' where denomination2 = '94'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FR108' where denomination2 = '95'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB01' where denomination2 = '18'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB02' where denomination2 = '28'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB03' where denomination2 = '36'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB04' where denomination2 = '37'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB05' where denomination2 = '41'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRB06' where denomination2 = '45'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC11' where denomination2 = '21'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC12' where denomination2 = '58'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC13' where denomination2 = '71'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC14' where denomination2 = '89'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC21' where denomination2 = '25'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC22' where denomination2 = '39'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC23' where denomination2 = '70'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRC24' where denomination2 = '90'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRD11' where denomination2 = '14'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRD12' where denomination2 = '50'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRD13' where denomination2 = '61'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRD21' where denomination2 = '27'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRD22' where denomination2 = '76'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRE11' where denomination2 = '59'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRE12' where denomination2 = '62'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRE21' where denomination2 = '02'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRE22' where denomination2 = '60'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRE23' where denomination2 = '80'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF11' where denomination2 = '67'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF12' where denomination2 = '68'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF21' where denomination2 = '08'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF22' where denomination2 = '10'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF23' where denomination2 = '51'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF24' where denomination2 = '52'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF31' where denomination2 = '54'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF32' where denomination2 = '55'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF33' where denomination2 = '57'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRF34' where denomination2 = '88'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRG01' where denomination2 = '44'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRG02' where denomination2 = '49'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRG03' where denomination2 = '53'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRG04' where denomination2 = '72'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRG05' where denomination2 = '85'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRH01' where denomination2 = '22'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRH02' where denomination2 = '29'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRH03' where denomination2 = '35'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRH04' where denomination2 = '56'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI11' where denomination2 = '24'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI12' where denomination2 = '33'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI13' where denomination2 = '40'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI14' where denomination2 = '47'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI15' where denomination2 = '64'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI21' where denomination2 = '19'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI22' where denomination2 = '23'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI23' where denomination2 = '87'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI31' where denomination2 = '16'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI32' where denomination2 = '17'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI33' where denomination2 = '79'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRI34' where denomination2 = '86'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ11' where denomination2 = '11'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ12' where denomination2 = '30'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ13' where denomination2 = '34'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ14' where denomination2 = '48'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ15' where denomination2 = '66'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ21' where denomination2 = '09'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ22' where denomination2 = '12'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ23' where denomination2 = '31'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ24' where denomination2 = '32'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ25' where denomination2 = '46'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ26' where denomination2 = '65'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ27' where denomination2 = '81'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRJ28' where denomination2 = '82'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK11' where denomination2 = '03'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK12' where denomination2 = '15'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK13' where denomination2 = '43'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK14' where denomination2 = '63'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK21' where denomination2 = '01'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK22' where denomination2 = '07'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK23' where denomination2 = '26'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK24' where denomination2 = '38'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK25' where denomination2 = '42'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK26' where denomination2 = '69'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK27' where denomination2 = '73'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRK28' where denomination2 = '74'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL01' where denomination2 = '04'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL02' where denomination2 = '05'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL03' where denomination2 = '06'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL04' where denomination2 = '13'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL05' where denomination2 = '83'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRL06' where denomination2 = '84'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRM01' where denomination2 = '2A'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRM02' where denomination2 = '2B'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY10' where denomination2 = '971'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY20' where denomination2 = '972'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY30' where denomination2 = '973'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY40' where denomination2 = '974'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY50' where denomination2 = '976'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRZZZ' where denomination2 = '975'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY10' where denomination2 = 'GP'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY2' where denomination2 = 'MQ'");
        $this->addSql("update GeolocalisationN2 set code_nuts = 'FRY4' where denomination2 = 'RE'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE GeolocalisationN2 DROP IF EXISTS code_nuts ");
    }
}
