<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ajout de la route du ws de modifcation de contrat dansla table web_service
 * Ajout de l'habilitation agent pour la modification du contrat
 */
final class Version20201025122827 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "Ajout de la sécurité de la modification d'un contrat par un agent";
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT INTO `web_service` (`id`, `route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) 
VALUES (NULL, '/api/{version}/contrats/modification.{format}', 'POST', 'Ajoute un avenant au contrat', '0')");
        $this->addSql('ALTER TABLE `HabilitationAgent` ADD IF NOT EXISTS `exec_modification_contrat` 
INT NOT NULL DEFAULT 0;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/contrats/modification.{format}' 
and `method_ws` =  'POST'");
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP IF EXISTS `exec_modification_contrat`;');
    }
}
