<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20201006095646 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13705 - update de la colonne reference_consultation_init';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        
        $this->addSql("UPDATE consultation c1
        join consultation c2  on c1.reference_consultation_init = c2.reference
        and c1.organisme = c2.organisme
        SET c1.reference_consultation_init = c2.id");
    }

    public function down(Schema $schema): void
    {
    }
}
