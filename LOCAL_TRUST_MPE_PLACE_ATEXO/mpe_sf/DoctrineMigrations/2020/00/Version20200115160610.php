<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115160610 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-12111';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/statistiques/interfaces.{format}\', \'GET\', \'GET Statistique interface\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/statistiques/interfaces.{format}\', \'POST\', \'POST Statistique interface\');');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'GET Statistique interface\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'POST Statistique interface\';');
    }
}