<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115160200 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-10439';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/authentification/connexion/\', \'GET\', \'generation token (authentification)\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/annonceservice/create\', \'POST\', \'création annonce service\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/annonceservice/update\', \'POST\', \'update annonce service\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/annonceservice/get/\', \'GET\', \'get annonce service\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/consultation/create\', \'POST\', \'création consultation\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/consultation/update\', \'POST\', \'update consultation\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/consultation/get/\', \'GET\', \'get consultation\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/tableaudebord\', \'POST\', \'tableaubord\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/questions/\', \'GET\', \'get questions\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/question/\', \'GET\', \'get question\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/retraitdces/\', \'GET\', \'get retrait DCES\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/retraitdce/\', \'GET\', \'get retrait DCE\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/depots/\', \'GET\', \'get depots\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/depot/\', \'GET\', \'get depot\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/depot/update\', \'POST\', \'update depots\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/pli/\', \'GET\', \'get pli\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/pli/\', \'POST\', \'post pli\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/contactsBySiret/get/\', \'GET\', \'get contacts par Siret\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/contactsByEntrepriseId/get/\', \'GET\', \'get contacts par id Entreprise\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/entrepriseByRaisonSociale/get/\', \'GET\', \'get entreprise par Raison Sociale\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/entrepriseBySiren/get/\', \'GET\', \'get entreprise par Siret\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/archive/listeid/\', \'GET\', \'get archive liste\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/archive/fichier/\', \'GET\', \'get archive fichier\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/organismes\', \'GET\', \'get organismes\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/organisme/\', \'GET\', \'get organisme\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/document/\', \'GET\', \'get document\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/document/\', \'POST\', \'post document\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/depot/admissibilite/\', \'GET\', \'get depot admissibilite\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/depot/decision/\', \'GET\', \'get depot decision\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/offre/admissibilite/\', \'GET\', \'get offre admissibilite\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/offre/decision/\', \'GET\', \'get offre decision\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/echanges/\', \'GET\', \'get registre echanges\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/registre/echange/\', \'GET\', \'get registre echange\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/fichier/\', \'GET\', \'get fichier\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/document/synchro/\', \'GET\', \'get document synchro\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/\', \'GET\', \'get WS\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/\', \'POST\', \'post WS\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api.php/ws/\', \'PUT\', \'put WS\');');

        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/referentiels/procedureContrats\', \'GET\', \'get procedure Contrats\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/referentiels\', \'GET\', \'get referentiels\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/agents.{format}\', \'GET\', \'get agents\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/agents.{format}\', \'POST\', \'post agents\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/agents.{format}\', \'PUT\', \'put agents\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/agents.{format}/habilitations\', \'GET\', \'get agents habilitations\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/contacts.{format}\', \'GET\', \'get contacts\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/donnees-essentielles/contrat/format-etendu\', \'POST\', \'post donnees-essentielles format-etendu\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/donnees-essentielles/contrat/format-pivot\', \'POST\', \'post donnees-essentielles format-pivot\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/entreprises.{format}\', \'GET\', \'get entreprises\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/etablissements.{format}\', \'GET\', \'get etablissements\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/monitoring/jmeter/hash/{scenario}.{format}\', \'GET\', \'get monitoring jmeter hash\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/monitoring/jmeter/{scenario}\', \'GET\', \'get monitoring jmeter\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/organismes.{format}\', \'GET\', \'get organismes (sous sf)\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/services.{format}\', \'GET\', \'get services\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/services.{format}\', \'PATCH\', \'PATCH services\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/services.{format}\', \'POST\', \'POST services\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/services.{format}\', \'PUT\', \'PUT services\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/tableau-de-bord/consultations-en-cours.{format}\', \'GET\', \'get tableau-de-bord consultations-en-cours\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/tableau-de-bord/contrats.{format}\', \'GET\', \'get tableau-de-bord contrats\');');
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/tableau-de-bord/questions.{format}\', \'GET\', \'get tableau-de-bord questions\');');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'generation token (authentification)\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'création annonce service\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'update annonce service\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get annonce service\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'création consultation\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'update consultation\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get consultation\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'tableaubord\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get questions\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get question\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get retrait DCES\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get retrait DCE\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get depots\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get depot\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'update depots\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get pli\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post pli\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get contacts par Siret\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get contacts par id Entreprise\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get entreprise par Raison Sociale\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get entreprise par Siret\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get archive liste\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get archive fichier\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get organismes\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get organisme\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get document\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post document\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get depot admissibilite\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get depot decision\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get offre admissibilite\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get offre decision\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get registre echanges\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get registre echange\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get fichier\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get document synchro\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get WS\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post WS\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'put WS\';');

        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get procedure Contrats\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get referentiels\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get agents\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post agents\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'put agents\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get agents habilitations\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get contacts\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post donnees-essentielles format-etendu\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'post donnees-essentielles format-pivot\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get entreprises\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get etablissements\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get monitoring jmeter hash\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get monitoring jmeter\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get organismes (sous sf)\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get services\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'PATCH services\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'POST services\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'PUT services\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get tableau-de-bord consultations-en-cours\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get tableau-de-bord contrats\';');
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'get tableau-de-bord questions\';');
    }
}