<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000029 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update data';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('UPDATE blob_file SET statut_synchro = 1;');
        $this->addSql('UPDATE blobOrganisme_file SET statut_synchro = 1;');

        # FMA MPE-10439
        # Partie prado
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/authentification/connexion/', 'GET', 'generation token (authentification)');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/annonceservice/create', 'POST', 'création annonce service');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/annonceservice/update', 'POST', 'update annonce service');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/annonceservice/get/', 'GET', 'get annonce service');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/consultation/create', 'POST', 'création consultation');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/consultation/update', 'POST', 'update consultation');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/consultation/get/', 'GET', 'get consultation');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/tableaudebord', 'POST', 'tableaubord');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/questions/', 'GET', 'get questions');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/question/', 'GET', 'get question');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/retraitdces/', 'GET', 'get retrait DCES');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/retraitdce/', 'GET', 'get retrait DCE');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/depots/', 'GET', 'get depots');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/depot/', 'GET', 'get depot');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/depot/update', 'POST', 'update depots')");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/pli/', 'GET', 'get pli');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/pli/', 'POST', 'post pli');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/contactsBySiret/get/', 'GET', 'get contacts par Siret');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/contactsByEntrepriseId/get/', 'GET', 'get contacts par id Entreprise');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/entrepriseByRaisonSociale/get/', 'GET', 'get entreprise par Raison Sociale');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/entrepriseBySiren/get/', 'GET', 'get entreprise par Siret');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/archive/listeid/', 'GET', 'get archive liste');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/archive/fichier/', 'GET', 'get archive fichier');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/organismes', 'GET', 'get organismes');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/organisme/', 'GET', 'get organisme');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/document/', 'GET', 'get document');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/document/', 'POST', 'post document');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/depot/admissibilite/', 'GET', 'get depot admissibilite');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/depot/decision/', 'GET', 'get depot decision');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/offre/admissibilite/', 'GET', 'get offre admissibilite');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) *
            VALUES ( '/api.php/ws/offre/decision/', 'GET', 'get offre decision');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/echanges/', 'GET', 'get registre echanges');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/registre/echange/', 'GET', 'get registre echange');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/fichier/', 'GET', 'get fichier');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/document/synchro/', 'GET', 'get document synchro');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/', 'GET', 'get WS');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/', 'POST', 'post WS');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api.php/ws/', 'PUT', 'put WS');");

        # Partie SF
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/referentiels/procedureContrats', 'GET', 'get procedure Contrats');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/referentiels', 'GET', 'get referentiels');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/agents.{format}', 'GET', 'get agents');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/agents.{format}', 'POST', 'post agents');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/agents.{format}', 'PUT', 'put agents');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/agents.{format}/habilitations', 'GET', 'get agents habilitations');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/contacts.{format}', 'GET', 'get contacts');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/contrats.{format}', 'GET', 'get contrats');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/donnees-essentielles/contrat/format-etendu', 'POST', 'post donnees-essentielles format-etendu');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/donnees-essentielles/contrat/format-pivot', 'POST', 'post donnees-essentielles format-pivot');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/entreprises.{format}', 'GET', 'get entreprises');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/etablissements.{format}', 'GET', 'get etablissements');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/monitoring/jmeter/hash/{scenario}.{format}', 'GET', 'get monitoring jmeter hash');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/monitoring/jmeter/{scenario}', 'GET', 'get monitoring jmeter');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/organismes.{format}', 'GET', 'get organismes (sous sf)');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/services.{format}', 'GET', 'get services');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/services.{format}', 'PATCH', 'PATCH services');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/services.{format}', 'POST', 'POST services');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/services.{format}', 'PUT', 'PUT services');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/tableau-de-bord/consultations-en-cours.{format}', 'GET', 'get tableau-de-bord consultations-en-cours');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/tableau-de-bord/contrats.{format}', 'GET', 'get tableau-de-bord contrats');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/tableau-de-bord/questions.{format}', 'GET', 'get tableau-de-bord questions');");

        # FMA MPE-12111
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/statistiques/interfaces.{format}', 'GET', 'GET Statistique interface');");
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
            VALUES ( '/api/{version}/statistiques/interfaces.{format}', 'POST', 'POST Statistique interface');");

        # FMA MPE-11502
        $message = addslashes('O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:\\"\\0Symfony'
            . '\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component\\\\Messenger'
            . '\\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp'
            . '\\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\";s:21:'
            . '\\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message'
            . '\\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message\\\\CommandeMessenger'
            . '\\0commande\\";s:19:\\"mpe:migration:tiers\\";s:37:\\"\\0App\\\\Message\\\\CommandeMessenger\\0params'
            . '\\";a:0:{}}}');

        $sql = 'INSERT INTO messenger_messages 
        (body, headers, queue_name, created_at, available_at, delivered_at) 
        SELECT DISTINCT \'' . $message . '\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null 
        FROM jms_jobs as J where NOT EXISTS
        (SELECT 1 from  jms_jobs as jm where jm.command like  \'mpe:migration:tiers\');';
        $this->addSql($sql);

        $this->addSql(
            "INSERT IGNORE INTO `Tiers` 
            (`login`, `password`, `denomination`, `fonctionnalite`, `organisme`) 
            SELECT DISTINCT  'mpe_oversight', '59is6onej32w', 'mpe_oversight', '4', NULL 
            FROM Tiers where NOT EXISTS (SELECT 1 from  Tiers where Tiers.login like 'mpe_oversight');"
        );

        $message = addslashes('O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:\\"\\0Symfony'
            . '\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component\\\\Messenger'
            . '\\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp'
            . '\\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\";s:21:'
            . '\\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message'
            . '\\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message\\\\CommandeMessenger'
            . '\\0commande\\";s:19:\\"mpe:migration:tiers\\";s:37:\\"\\0App\\\\Message\\\\CommandeMessenger\\0params'
            . '\\";a:0:{}}}');

        $sql = 'INSERT INTO messenger_messages 
        (body, headers, queue_name, created_at, available_at, delivered_at) 
        SELECT DISTINCT \'' . $message . '\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null 
        FROM jms_jobs as J where NOT EXISTS
        (SELECT 1 from  jms_jobs as jm where jm.command like  \'mpe:migration:tiers\' AND state = \'pending\');';
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
