<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115170555 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-11653';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `t_etablissement` ADD COLUMN IF NOT EXISTS `etat_administratif` CHAR(1) NULL DEFAULT NULL COMMENT \'etat administratif : A => active, F => fermé\';');
        $this->addSql('ALTER TABLE `t_etablissement` ADD COLUMN IF NOT EXISTS `date_fermeture` DATETIME NULL COMMENT \'timestamp de la date de fermeture\';');
        $this->addSql('ALTER TABLE `Entreprise` ADD COLUMN IF NOT EXISTS `etat_administratif` CHAR(1) NULL DEFAULT NULL COMMENT \'etat administratif : A => active, C => Cessée\';');
        $this->addSql('ALTER TABLE `Entreprise` ADD COLUMN IF NOT EXISTS `date_cessation` DATETIME NULL DEFAULT NULL COMMENT \'date de cessation\';');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE t_etablissement DROP etat_administratif');
        $this->addSql('ALTER TABLE t_etablissement DROP date_fermeture');
        $this->addSql('ALTER TABLE Entreprise DROP etat_administratif');
        $this->addSql('ALTER TABLE Entreprise DROP date_cessation');
    }
}