<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000026 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initialisation';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `Entreprise`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT \'0\';');
        $this->addSql('ALTER TABLE `Inscrit`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT \'0\';');
        $this->addSql('ALTER TABLE `t_etablissement`	ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT \'0\';');
        $this->addSql('ALTER TABLE `Organisme` ADD COLUMN `id_externe` VARCHAR(50) NOT NULL DEFAULT \'0\';');
        $this->addSql('ALTER TABLE `CertificatChiffrement` CHANGE `consultation_ref` `consultation_ref` INT(11) NULL DEFAULT \'0\';');

        # FMA MPE-10676
        $this->addSql('ALTER TABLE `configuration_plateforme` ADD COLUMN `messagerie_v2` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'active la messagerie sécurisé v2\' AFTER `masquer_atexo_sign`;');
        $this->addSql('ALTER TABLE `consultation` ADD COLUMN `version_messagerie` INT(11) NOT NULL DEFAULT \'1\' COMMENT \'version de la messagerie\' AFTER `attestation_consultation`;');

        # FMA MPE-10648
        $this->addSql('CREATE TABLE IF NOT EXISTS `mail_type_group` (
    id INT AUTO_INCREMENT NOT NULL,
    label VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
)
ENGINE = InnoDB;');

        $this->addSql('CREATE TABLE IF NOT EXISTS `mail_type` (
    id INT AUTO_INCREMENT NOT NULL,
    mail_type_group_id INT NOT NULL,
    label VARCHAR(255) NOT NULL,
    code VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    CONSTRAINT mail_type_mail_type_group_id FOREIGN KEY (mail_type_group_id) REFERENCES mail_type_group (id)
)
ENGINE = InnoDB;');

        $this->addSql('CREATE TABLE IF NOT EXISTS `mail_template` (
    id INT AUTO_INCREMENT NOT NULL,
    mail_type_id INT NOT NULL,
    code VARCHAR(255) NOT NULL,
    titre VARCHAR(255) NOT NULL,
    corps LONGTEXT NOT NULL,
    ordre_affichage INT NULL,
    envoi_modalite VARCHAR(255) NOT NULL DEFAULT \'AVEC_AR\' COMMENT \'AVEC_AR | SANS_AR\',
    envoi_modalite_figee BOOLEAN NOT NULL DEFAULT \'0\',
    reponse_attendue BOOLEAN NOT NULL DEFAULT \'0\',
    reponse_attendue_figee BOOLEAN NOT NULL DEFAULT \'0\',
    PRIMARY KEY(id),
    CONSTRAINT mail_template_mail_type_id FOREIGN KEY (mail_type_id) REFERENCES mail_type (id)
)
ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE `mail_template` CHANGE COLUMN `titre` `objet` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;');
        $this->addSql('ALTER TABLE `mail_type` ADD UNIQUE (code);');
        $this->addSql('ALTER TABLE `mail_template` ADD UNIQUE (code);');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
