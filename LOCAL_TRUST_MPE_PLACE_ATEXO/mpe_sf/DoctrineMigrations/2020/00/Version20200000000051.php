<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Exception;
use Application\Service\Atexo\Atexo_ConfigClientBuilder;
use App\Entity\Langue;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Finder\Finder;

/**
 * Gestion des tables configuration_client et configuration_messages_traduction
 */
final class Version202000000000511 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    protected $languages;

    public function getDescription(): string
    {
        return 'Gestion des tables configuration_client et configuration_messages_traduction';
    }

    public function preUp(Schema $schema): void
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $this->languages = $em->getRepository(Langue::class)->findAll();
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->createConfigurationClientTable();
        $this->createConfigurationMessagesTraductionTable();
    }

    protected function createConfigurationClientTable()
    {
        // On vérifie s'il y a déjà des enregistrements dans la table
        $sql = 'select * FROM configuration_client';
        $stmt = $this->connection->prepare($sql);
        try {
            $stmt->execute();

            if (0 < $stmt->rowCount()) {
                $this->write(
                    'La table "configuration_client" existe et contient déjà des enregistrements.'
                );
                return;
            }
        } catch (Exception $exception) {
            $this->write(
                'La table "configuration_client" n\'existe pas, on la crée.'
            );

            $sqlInit = "CREATE TABLE `configuration_client` ( `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , `parameter` VARCHAR(300) NOT NULL , `value` TEXT NOT NULL,`updated_at` DATETIME NULL ) ENGINE = InnoDB;";
            $this->addSql($sqlInit);
        }

        $specificParametersFile = __DIR__ . '/../../../../config/application.php';

        if (!file_exists($specificParametersFile) || !is_readable($specificParametersFile)) {
            $this->write(
                'Le fichier de configuration n\'a pas été trouvé ou il n\'est pas possible de le lire : '
                . PHP_EOL . $specificParametersFile
                . PHP_EOL . 'On ne pré-remplit donc pas la table "configuration_client".' . PHP_EOL
            );
            return;
        }

        $fileContent = file_get_contents($specificParametersFile);
        $fileContent = str_replace(
            'use Application\Service\Atexo\Atexo_ConfigClientBuilder as Atexo_Config;',
            '',
            $fileContent
        );
        $fileContent = str_replace(
            '<?php',
            '<?php' . PHP_EOL
            . 'use Application\Service\Atexo\Atexo_ConfigClientBuilder as Atexo_Config;',
            $fileContent
        );

        $speFileSyntaxOk = (boolean)token_get_all($fileContent);
        $this->abortIf(!$speFileSyntaxOk, $specificParametersFile . ' file : syntax error.');

        file_put_contents($specificParametersFile, $fileContent);

        ob_start();
        include $specificParametersFile;
        $specificParameters = Atexo_ConfigClientBuilder::getParameters();
        ob_get_clean();

        $sql = 'INSERT INTO `configuration_client` (parameter, value, updated_at) VALUES (:key, :value, NOW())';

        foreach ($specificParameters as $key => $value) {
            $this->addSql($sql, ['key' => $key, 'value' => $value]);
        }
    }

    protected function createConfigurationMessagesTraductionTable()
    {
        // On vérifie s'il y a déjà des enregistrements dans la table
        $sql = 'select * FROM configuration_messages_traduction';
        $stmt = $this->connection->prepare($sql);
        try {
            $stmt->execute();

            if (0 < $stmt->rowCount()) {
                $this->write(
                    'La table "configuration_messages_traduction" existe et contient déjà des enregistrements.'
                );
                return;
            }
        } catch (Exception $exception) {
            $this->write(
                'La table "configuration_messages_traduction" n\'existe pas, on la crée.'
            );

            $sqlInit = <<<EOT
                CREATE TABLE IF NOT EXISTS `configuration_messages_traduction` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `source` varchar(300) NOT NULL,
                  `target` text NOT NULL,
                  `langue_id` int(11) NOT NULL,
                  PRIMARY KEY (`id`),
                  KEY `fk_languages_langue_id` (`langue_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
                EOT;

            $this->addSql($sqlInit);
            $this->addSql(
                <<<EOT
                ALTER TABLE `configuration_messages_traduction`
                  ADD CONSTRAINT `fk_languages_langue_id` FOREIGN KEY (`langue_id`) REFERENCES `Langue` (`id_langue`);
                EOT
            );
        }

        $finder = new Finder();
        try {
            $languageFiles = $finder->files()
                ->in(__DIR__ . '/../../../../config/messages/')
                ->name('messages.*.xml');
        } catch (Exception $exception) {
            $languageFiles = [];
            $this->write(
                'Répertoire de recherche des fichiers de langues :' . PHP_EOL
                . __DIR__ . '/../../../../config/messages/messages.*.xml'
            );
        }

        if (0 === sizeof($languageFiles)) {
            $this->write(
                'Aucun fichier de traduction n\'a été trouvé dans la configuration du client.'
                . PHP_EOL . 'On ne pré-remplit donc pas la table "configuration_messages_traduction".' . PHP_EOL
            );
            return;
        }

        $sql = 'INSERT INTO `configuration_messages_traduction` (`source`, `target`, `langue_id`)'
            . ' VALUES (:source, :target, :langue_id)';

        $languages = $this->languages;

        foreach ($languageFiles as $languageFile) {
            preg_match('/messages\.([a-z]{2})\.xml/', $languageFile->getFilename(), $matches);

            $xmlContent = file_get_contents($languageFile->getPathname());
            $sourceNodes = (new Crawler($xmlContent))->filter('source');
            $targetNodes = (new Crawler($xmlContent))->filter('target');

            /** @var Langue $language */
            foreach ($languages as $language) {
                if ($language->getLangue() === $matches[1]) {
                    $langue_id = $language->getId();
                    break;
                }
            }

            if (! isset($langue_id)) {
                continue;
            }

            $i = 0;

            foreach ($sourceNodes as $domElement) {
                $source = $domElement->textContent;
                $target = $targetNodes->getNode($i++)->textContent;
                $this->addSql($sql, ['source' => $source, 'target' => $target, 'langue_id' => $langue_id]);
            }
        }
    }

    public function down(Schema $schema): void
    {
        // On ne supprime pas les tables car on ne sait pas si elles existaient avant.
    }
}
