<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115160000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-11197';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = \'Consortium representative\' WHERE `ValeurReferentiel`.`id` = 1 AND `ValeurReferentiel`.`id_referentiel` = 13;');
        $this->addSql('UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = \'Co-contractor with joint and several liability\' WHERE `ValeurReferentiel`.`id` = 2 AND `ValeurReferentiel`.`id_referentiel` = 13;');
        $this->addSql('UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = \'Co-contractor with joint liability only \' WHERE `ValeurReferentiel`.`id` = 3 AND `ValeurReferentiel`.`id_referentiel` = 13;');
        $this->addSql('UPDATE `ValeurReferentiel` SET `libelle_valeur_referentiel_en` = \'Subcontractor\' WHERE `ValeurReferentiel`.`id` = 4 AND `ValeurReferentiel`.`id_referentiel` = 13');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

    }

}