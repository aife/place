<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115144225 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-10436';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS `web_service` (
                id INT AUTO_INCREMENT NOT NULL,
                route_ws VARCHAR(255) NOT NULL,
                method_ws VARCHAR(255) NOT NULL,
                nom_ws varchar(255) NOT NULL,
                PRIMARY KEY(id))
            ENGINE = InnoDB;'
        );

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE web_service');
    }

}