<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000027 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initialisation -- MPE-11566';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE fichierEnveloppe DROP FOREIGN KEY IF EXISTS FK_fichierEnveloppe_blobOrganismeFile;');
        $this->addSql('ALTER TABLE Offres DROP FOREIGN KEY IF EXISTS FK_Offres_blobOrganismeFile;');
        $this->addSql('ALTER TABLE Offres DROP FOREIGN KEY IF EXISTS offres_id_blob_xml_reponse;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
