<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000025 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Migration des ID, FK ect.';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        # On commence par supprimer toutes les clés étrangères pour pouvoir manipuler la clé primaire
        # CLE ETRANGERE VISIBLE SEULEMENT EN INTEGRATION. DONC ON FAIT QUAND MEME AU CAS OU ..
        $this->addSql('ALTER TABLE `Complement` DROP FOREIGN KEY IF EXISTS `Complement_ibfk_1`, DROP FOREIGN KEY IF EXISTS `Complement_ibfk_2`;');
        $this->addSql('ALTER TABLE `CategorieLot` DROP FOREIGN KEY IF EXISTS `CategorieLot_ibfk_2`, DROP FOREIGN KEY IF EXISTS `CategorieLot_ibfk_3`;');
        $this->addSql('ALTER TABLE `DATEFIN` DROP FOREIGN KEY IF EXISTS `DATEFIN_ibfk_1`, DROP FOREIGN KEY IF EXISTS `DATEFIN_ibfk_2`;');
        $this->addSql('ALTER TABLE `DCE` DROP FOREIGN KEY IF EXISTS `DCE_ibfk_1`, DROP FOREIGN KEY IF EXISTS `DCE_ibfk_2`;');
        $this->addSql('ALTER TABLE `InterneConsultation` DROP FOREIGN KEY IF EXISTS `InterneConsultation_ibfk_1`, DROP FOREIGN KEY IF EXISTS `InterneConsultation_ibfk_2`;');
        $this->addSql('ALTER TABLE `InterneConsultationSuiviSeul` DROP FOREIGN KEY IF EXISTS `InterneConsultationSuiviSeul_ibfk_1`, DROP FOREIGN KEY IF EXISTS `InterneConsultationSuiviSeul_ibfk_2`;');
        $this->addSql('ALTER TABLE `Offres` DROP FOREIGN KEY IF EXISTS `Offres_ibfk_1`, DROP FOREIGN KEY IF EXISTS `Offres_ibfk_2`;');
        $this->addSql('ALTER TABLE `Offre_papier` DROP FOREIGN KEY IF EXISTS `Offre_papier_ibfk_1`, DROP FOREIGN KEY IF EXISTS `Offre_papier_ibfk_2`;');
        $this->addSql('ALTER TABLE `Retrait_Papier` DROP FOREIGN KEY IF EXISTS `Retrait_Papier_ibfk_1`, DROP FOREIGN KEY IF EXISTS `Retrait_Papier_ibfk_2`;');
        $this->addSql('ALTER TABLE `RG` DROP FOREIGN KEY IF EXISTS `RG_ibfk_1`, DROP FOREIGN KEY IF EXISTS `RG_ibfk_2`;');
        $this->addSql('ALTER TABLE `TelechargementAnonyme` DROP FOREIGN KEY IF EXISTS `TelechargementAnonyme_ibfk_1`;');
        $this->addSql('ALTER TABLE `consultation_document_cfe` DROP FOREIGN KEY IF EXISTS `consultation_document_cfe_ibfk_1`, DROP FOREIGN KEY IF EXISTS `consultation_document_cfe_ibfk_2`;');
        $this->addSql('ALTER TABLE `Pieces_DCE` DROP FOREIGN KEY IF EXISTS `Pieces_DCE_ibfk_1`, DROP FOREIGN KEY IF EXISTS `Pieces_DCE_ibfk_2`;');
        $this->addSql('ALTER TABLE `visite_lieux` DROP FOREIGN KEY IF EXISTS `visite_lieux_constraint_2`;');

        # ALTER TABLE AVIS DROP FOREIGN KEY AVIS_consultation;
        $this->addSql('ALTER TABLE `AVIS` DROP FOREIGN KEY IF EXISTS `AVIS_consultation`;');
        $this->addSql('ALTER TABLE `AnnonceBoamp` DROP FOREIGN KEY IF EXISTS `AnnonceBoamp_consultation`;');
        $this->addSql('ALTER TABLE `Annonce` DROP FOREIGN KEY IF EXISTS `Annonce_consultation`;');
        $this->addSql('ALTER TABLE `Avis_Pub` DROP FOREIGN KEY IF EXISTS `Avis_Pub_consultation`;');
        $this->addSql('ALTER TABLE `Complement` DROP FOREIGN KEY IF EXISTS `Complement_consultation`;');
        $this->addSql('ALTER TABLE `DATEFIN` DROP FOREIGN KEY IF EXISTS `DATEFIN_consultation`;');
        $this->addSql('ALTER TABLE `DCE` DROP FOREIGN KEY IF EXISTS `DCE_consultation`;');
        $this->addSql('ALTER TABLE `DocumentExterne` DROP FOREIGN KEY IF EXISTS `DOC_EX_consultation`;');
        $this->addSql('ALTER TABLE `Helios_piece_publicite` DROP FOREIGN KEY IF EXISTS `Helios_piece_publicite_Consultation`;');
        $this->addSql('ALTER TABLE `Helios_pv_consultation` DROP FOREIGN KEY IF EXISTS `Helios_pv_consultation_Consultation`;');
        $this->addSql('ALTER TABLE `Helios_rapport_prefet` DROP FOREIGN KEY IF EXISTS `Helios_rapport_prefet_Consultation`;');
        $this->addSql('ALTER TABLE `Helios_tableau_ar` DROP FOREIGN KEY IF EXISTS `Helios_tableau_ar_Consultation`;');
        $this->addSql('ALTER TABLE `CategorieLot` DROP FOREIGN KEY IF EXISTS `CategorieLot_consultation`;');
        $this->addSql('ALTER TABLE `InterneConsultation` DROP FOREIGN KEY IF EXISTS `InterneConsultation_consultation`;');
        $this->addSql('ALTER TABLE `InterneConsultationSuiviSeul` DROP FOREIGN KEY IF EXISTS `InterneConsultationSuiviSeul_consultation`;');
        $this->addSql('ALTER TABLE `Offres` DROP FOREIGN KEY IF EXISTS `Offres_consultation`;');
        $this->addSql('ALTER TABLE `Offre_papier` DROP FOREIGN KEY IF EXISTS `Offre_papier_consultation`;');
        $this->addSql('ALTER TABLE `QuestionDCE` DROP FOREIGN KEY IF EXISTS `QuestionDCE_consultation`;');
        $this->addSql('ALTER TABLE `Retrait_Papier` DROP FOREIGN KEY IF EXISTS `Retrait_Papier_consultation`;');
        $this->addSql('ALTER TABLE `RG` DROP FOREIGN KEY IF EXISTS `RG_consultation`;');
        $this->addSql('ALTER TABLE `Telechargement` DROP FOREIGN KEY IF EXISTS `Telechargement_consultation`;');
        $this->addSql('ALTER TABLE `TelechargementAnonyme` DROP FOREIGN KEY IF EXISTS `TelechargementAnonyme_consultation`;');
        $this->addSql('ALTER TABLE `t_param_dossier_formulaire` DROP FOREIGN KEY IF EXISTS `consultation_t_param_dossier_form_consultation_ref_fk`;');
        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` DROP FOREIGN KEY IF EXISTS `consultation_t_reponse_elec_form_consultation_ref_fk`;');
        $this->addSql('ALTER TABLE `Helios_teletransmission` DROP FOREIGN KEY IF EXISTS `Helios_teletransmission_Consultation`;');
        $this->addSql('ALTER TABLE `t_calendrier` DROP FOREIGN KEY IF EXISTS `T_CALENDRIER_ID_CONSULTATION_FK`;');
        $this->addSql('ALTER TABLE `visite_lieux` DROP FOREIGN KEY IF EXISTS `visite_lieux_consultation`;');


        $this->addSql('ALTER TABLE `consultation_favoris` DROP FOREIGN KEY IF EXISTS `consultation_favoris_id_consultation_FK`;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` DROP FOREIGN KEY IF EXISTS `PanierEntrepriseRef_ConsultationRef`;');
        $this->addSql('ALTER TABLE `t_dume_contexte` DROP FOREIGN KEY IF EXISTS `FK_t_dume_contexte_consultation`;');
        $this->addSql('ALTER TABLE `t_liste_lots_candidature` DROP FOREIGN KEY IF EXISTS `FK_t_liste_lots_candidature_consultation`;');
        $this->addSql('ALTER TABLE `t_annonce_consultation` DROP FOREIGN KEY IF EXISTS `fk_tAnnonceConsultation_Consultation`;');
        $this->addSql('ALTER TABLE `t_candidature` DROP FOREIGN KEY IF EXISTS `FK_t_candidature_consultation`;');
        $this->addSql('ALTER TABLE `ConsultationHistoriqueEtat` DROP FOREIGN KEY IF EXISTS `ConsultationHistoriqueEtat`;');
        $this->addSql('ALTER TABLE `consultation` DROP PRIMARY KEY, CHANGE COLUMN `reference` `reference` INT(11) NULL, ADD COLUMN `id` INT(11) NOT NULL AUTO_INCREMENT FIRST , AUTO_INCREMENT=1500000, ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE consultation ADD KEY reference_organisme (reference,organisme);');

        # Création des ID FK
        $this->addSql('ALTER TABLE `AVIS` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `AnnonceBoamp` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Annonce` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Avis_Pub` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Complement` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `DATEFIN` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `DCE` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `DocumentExterne` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Helios_piece_publicite` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Helios_pv_consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Helios_rapport_prefet` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Helios_tableau_ar` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `CategorieLot` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `CategorieLot` DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE `CategorieLot` ADD COLUMN `id` INT(11)  PRIMARY KEY AUTO_INCREMENT FIRST;');
        $this->addSql('ALTER TABLE `InterneConsultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `InterneConsultationSuiviSeul` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Offres` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Offre_papier` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `QuestionDCE` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `questions_dce` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Retrait_Papier` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `RG` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Telechargement` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `TelechargementAnonyme` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_param_dossier_formulaire` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Helios_teletransmission` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_calendrier` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `visite_lieux` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_numero_reponse` ADD COLUMN `consultation_id` INT(11);');

        # Nullable l'ancien attribut car plus alimenté (pourles tables qui n'ont pas de default value)
        $this->addSql('ALTER TABLE `AVIS` MODIFY `consultation_ref` varchar(255) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Annonce` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Avis_Pub` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `CategorieLot` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `QuestionDCE` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Telechargement` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `TelechargementAnonyme` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_param_dossier_formulaire` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_calendrier` MODIFY `REFERENCE` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_numero_reponse` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE AVIS a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Annonce a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE AnnonceBoamp a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Avis_Pub a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Complement a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE DATEFIN a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE DCE a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE DocumentExterne a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.refConsultation = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Helios_piece_publicite a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Helios_pv_consultation a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Helios_rapport_prefet a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Helios_tableau_ar a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE CategorieLot a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE InterneConsultation a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE InterneConsultationSuiviSeul a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');

        $years = [
            ['year' => '2021'],
            ['year' => '2020'],
            ['year' => '2019'],
            ['year' => '2018'],
            ['year' => '2017'],
            ['year' => '2016'],
            ['year' => '2015'],
            ['year' => '2014'],
            ['year' => '2013'],
            ['year' => '2012'],
            ['year' => '2011'],
            ['year' => '2010'],
            ['year' => '2009'],
            ['year' => '2008'],
            ['year' => '2007']
        ];
        foreach ($years as $year) {
            $this->connection->beginTransaction();
            $this->addSql('UPDATE Offres a INNER JOIN consultation c ON a.organisme = c.organisme AND a.consultation_ref = c.reference 
    SET a.consultation_id = c.id where a.consultation_ref != 0 AND year(a.untrusteddate)=:year and a.consultation_id is null;', $year);
            $this->connection->commit();
        }

        $this->connection->beginTransaction();
        $this->addSql('UPDATE Offres a INNER JOIN consultation c ON a.organisme = c.organisme AND a.consultation_ref = c.reference 
    SET a.consultation_id = c.id where a.consultation_ref != 0  and a.consultation_id is null;');
        $this->connection->commit();


        $this->addSql('UPDATE Offre_papier a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE QuestionDCE a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE questions_dce a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Retrait_Papier a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE RG a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Telechargement a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE TelechargementAnonyme a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE t_param_dossier_formulaire a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE t_reponse_elec_formulaire a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE Helios_teletransmission a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE t_calendrier a SET consultation_id = (SELECT id FROM consultation c WHERE a.ORGANISME = c.organisme AND a.REFERENCE = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE visite_lieux a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.reference = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE t_numero_reponse a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference) where consultation_id IS NULL;');
        $this->addSql('UPDATE t_numero_reponse a SET id = consultation_id where consultation_id IS NOT NULL;');
        //$this->addSql('UPDATE `consultation_favoris` cf SET id_consultation = (SELECT id FROM consultation c WHERE  cf.id_consultation = c.reference) where consultation_id IS NULL;');

        # Update des ID FK
        $this->addSql('ALTER TABLE  `AVIS` ADD CONSTRAINT `FK_AVIS_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Annonce` ADD CONSTRAINT `FK_Annonce_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `AnnonceBoamp` ADD CONSTRAINT `FK_AnnonceBoamp_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Avis_Pub` ADD CONSTRAINT `FK_AvisPub_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Complement` ADD CONSTRAINT `FK_Complement_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `DATEFIN` ADD CONSTRAINT `FK_DATEFIN_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `DCE` ADD CONSTRAINT `FK_DCE_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `DocumentExterne` ADD CONSTRAINT `FK_DOC_EX_consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Helios_piece_publicite` ADD CONSTRAINT `FK_Helios_piece_publicite_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Helios_pv_consultation` ADD CONSTRAINT `FK_Helios_pv_consultation_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Helios_rapport_prefet` ADD CONSTRAINT `FK_Helios_rapport_prefet_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Helios_tableau_ar` ADD CONSTRAINT `FK_Helios_tableau_ar_Consultation` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `CategorieLot` ADD CONSTRAINT `FK_CategorieLot_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `CategorieLot` ADD UNIQUE `unique_categorielot`(`consultation_id`, `lot`);');
        $this->addSql('ALTER TABLE  `InterneConsultation` ADD CONSTRAINT `FK_InterneConsultation_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `InterneConsultationSuiviSeul` ADD CONSTRAINT `FK_InterneConsultationSuiviSeul_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `Offres` ADD CONSTRAINT `FK_Offres_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `Offre_papier` ADD CONSTRAINT `FK_Offre_papier_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `QuestionDCE` ADD CONSTRAINT `FK_QuestionDCE_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `Retrait_Papier` ADD CONSTRAINT `FK_Retrait_Papier_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `RG` ADD CONSTRAINT `FK_RG_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `Telechargement` ADD CONSTRAINT `FK_Telechargement_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `TelechargementAnonyme` ADD CONSTRAINT `FK_TelechargementAnonyme_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `t_param_dossier_formulaire` ADD CONSTRAINT `FK_consultation_t_param_dossier_form_consultation_ref_fk` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `t_reponse_elec_formulaire` ADD CONSTRAINT `FK_consultation_t_reponse_elec_form_consultation_ref_fk` FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`);');
        $this->addSql('ALTER TABLE  `Helios_teletransmission` ADD CONSTRAINT `FK_Helios_teletransmission_Consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `t_calendrier` ADD CONSTRAINT `FK_T_CALENDRIER_ID_CONSULTATION_FK` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE  `visite_lieux` ADD CONSTRAINT `FK_visite_lieux_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `consultation_favoris` ADD CONSTRAINT `consultation_favoris_id_consultation_FK` 
    FOREIGN KEY (`id_consultation`) REFERENCES `consultation` (`id`)  ON DELETE CASCADE;');

        # Autre fichier qui utilise consultation REF
        # consultation_ref
        $this->addSql('ALTER TABLE `AnnonceJAL` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Annonce_Press` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `CertificatChiffrement` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `CertificatChiffrement` DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE `CertificatChiffrement` ADD COLUMN `id` INT(11)  PRIMARY KEY AUTO_INCREMENT FIRST;');
        $this->addSql('ALTER TABLE `ConsultationFormulaire` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `ConsultationHistoriqueEtat` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Criteres_Evaluation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `DecisionLot` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Pieces_Mise_Disposition` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `ReferentielFormXml` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `decisionEnveloppe` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `resultat_analyse` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `resultat_analyse_decision` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_cons_lot_contrat` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_decision_selection_entreprise` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_document` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `ConsultationFormulaire` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Criteres_Evaluation` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Pieces_Mise_Disposition` MODIFY `consultation_ref` INT(11) DEFAULT NULL;');

        $this->addSql('UPDATE `AnnonceJAL` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `Annonce_Press` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `CertificatChiffrement` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('ALTER TABLE `CertificatChiffrement` ADD UNIQUE `certificatchiffrement`(`consultation_id`, `type_env`,`sous_pli`,`index_certificat`,`organisme`);');
        $this->addSql('UPDATE `ConsultationFormulaire` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `ConsultationHistoriqueEtat` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `Criteres_Evaluation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `DecisionLot` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `Pieces_Mise_Disposition` a SET consultation_id = (SELECT id FROM consultation c WHERE a.org = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `ReferentielFormXml` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `decisionEnveloppe` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `resultat_analyse` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `resultat_analyse_decision` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `t_cons_lot_contrat` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `t_decision_selection_entreprise` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');
        $this->addSql('UPDATE `t_document` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.consultation_ref = c.reference);');

        # ref_consultation
        # La FOREIKEY EXIST EN INTEGRATION
        $this->addSql('ALTER TABLE `consultation_document_cfe` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `consultation_document_cfe` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `consultation_document_cfe` a SET consultation_id = (SELECT id FROM consultation c WHERE a.ref_consultation = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `consultation_document_cfe` ADD CONSTRAINT `consultation_document_cfe_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `Pieces_DCE` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Pieces_DCE` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `Pieces_DCE` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme_consultation = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `Pieces_DCE` ADD CONSTRAINT `Pieces_DCE_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Panier_Entreprise` DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` CHANGE COLUMN `ref_consultation` `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` ADD COLUMN `id` INT(11)  PRIMARY KEY AUTO_INCREMENT FIRST;');
        $this->addSql('UPDATE `Panier_Entreprise` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `Panier_Entreprise` ADD CONSTRAINT `PanierEntrepriseRef_ConsultationId` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` ADD UNIQUE `panierentrepriseunique`(`organisme`,`consultation_id`,`id_entreprise`,`id_inscrit`);');
        $this->addSql('ALTER TABLE `t_dume_contexte` DROP INDEX FK_t_dume_contexte_consultation;');
        $this->addSql('ALTER TABLE `t_dume_contexte` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_dume_contexte` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `t_dume_contexte` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `t_dume_contexte` ADD CONSTRAINT `FK_t_dume_contexte_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `t_liste_lots_candidature` DROP INDEX FK_t_liste_lots_candidature_consultation;');
        $this->addSql('ALTER TABLE `t_liste_lots_candidature` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_liste_lots_candidature` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `t_liste_lots_candidature` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `t_liste_lots_candidature` ADD CONSTRAINT `FK_t_liste_lots_candidature_consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `t_annonce_consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_annonce_consultation` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `t_annonce_consultation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `t_annonce_consultation` ADD CONSTRAINT `fk_tAnnonceConsultation_Consultation` 
    FOREIGN KEY (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('ALTER TABLE `t_candidature` DROP INDEX FK_t_candidature_consultation;');
        $this->addSql('ALTER TABLE `t_candidature` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_candidature` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `t_candidature` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('ALTER TABLE  `t_candidature` 
    ADD CONSTRAINT `FK_t_candidature_consultation` FOREIGN KEY (`consultation_id`) 
        REFERENCES  `consultation` (`id`) ON UPDATE CASCADE;');

        # Pas de FK
        $this->addSql('ALTER TABLE `Echange` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `backup_Ordre_Du_Jour` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `backup_Ordre_Du_Jour_2` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `historiques_consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `trace_operations_inscrit` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_CAO_Commission_Consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_candidature_mps` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_CAO_Commission_Consultation` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_candidature_mps` MODIFY `ref_consultation` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `Echange` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `backup_Ordre_Du_Jour` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `backup_Ordre_Du_Jour_2` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `historiques_consultation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `trace_operations_inscrit` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `t_CAO_Commission_Consultation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');
        $this->addSql('UPDATE `t_candidature_mps` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.ref_consultation = c.reference);');

        # refConsultation
        $this->addSql('ALTER TABLE `EncherePmi` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Parametrage_Enchere` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('UPDATE `EncherePmi` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.refConsultation = c.reference);');
        $this->addSql('UPDATE `Parametrage_Enchere` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.refConsultation = c.reference);');

        # reference
        $this->addSql('ALTER TABLE `Referentiel_Consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `InvitationConsultationTransverse` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `ModeleFormulaire` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Passation_consultation` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `t_espace_collaboratif` ADD COLUMN `consultation_id` INT(11);');
        $this->addSql('ALTER TABLE `Referentiel_Consultation` MODIFY `reference` INT(100) DEFAULT NULL;');
        $this->addSql('ALTER TABLE `ModeleFormulaire` MODIFY `reference` varchar(255)  DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_espace_collaboratif` MODIFY `reference` INT(11) DEFAULT NULL;');
        $this->addSql('UPDATE `Referentiel_Consultation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.reference = c.reference);');
        $this->addSql('UPDATE `InvitationConsultationTransverse` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme_emetteur = c.organisme AND a.reference = c.reference);');
        $this->addSql('UPDATE `ModeleFormulaire` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.reference = c.reference);');
        $this->addSql('UPDATE `Passation_consultation` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.reference = c.reference);');
        $this->addSql('UPDATE `t_espace_collaboratif` a SET consultation_id = (SELECT id FROM consultation c WHERE a.organisme = c.organisme AND a.reference = c.reference);');
        $this->addSql('ALTER TABLE AVIS MODIFY COLUMN consultation_ref varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE blobOrganisme_file MODIFY COLUMN dossier varchar(256) NULL DEFAULT NULL COMMENT \'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.\';');
        $this->addSql('ALTER TABLE blobOrganisme_file MODIFY COLUMN hash varchar(256) NULL DEFAULT \'ND\' COMMENT \'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit\';');
        $this->addSql('ALTER TABLE blob_file MODIFY COLUMN dossier varchar(256) NULL DEFAULT NULL COMMENT \'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.\';');
        $this->addSql('ALTER TABLE blob_file MODIFY COLUMN hash varchar(256) NULL DEFAULT \'ND\' COMMENT \'Empreinte du blob, SHA1 du fichier / ND : Valeur par défaut pour non définit\';');
        $this->addSql('ALTER TABLE Chorus_echange MODIFY COLUMN dume_acheteur_items varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE Chorus_echange MODIFY COLUMN dume_oe_items varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation MODIFY COLUMN source_externe varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive MODIFY COLUMN chemin_fichier varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_atlas MODIFY COLUMN doc_id varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_atlas MODIFY COLUMN comp_id varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_atlas MODIFY COLUMN organisme varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_bloc MODIFY COLUMN doc_id varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_bloc MODIFY COLUMN chemin_fichier varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_bloc MODIFY COLUMN erreur varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_bloc MODIFY COLUMN comp_id varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE consultation_archive_fichier MODIFY COLUMN chemin_fichier varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE donnees_annuelles_concession_tarif MODIFY COLUMN intitule_tarif varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE dossier_volumineux MODIFY COLUMN uuid_technique varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE ModeleFormulaire MODIFY COLUMN reference varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE modification_contrat MODIFY COLUMN montant varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE Organisme MODIFY COLUMN pf_url varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE type_contrat_concession_pivot MODIFY COLUMN libelle varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE type_procedure_concession_pivot MODIFY COLUMN libelle varchar(256) NOT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE Type_Procedure_Organisme MODIFY COLUMN id_externe varchar(256) NULL DEFAULT \'\' COMMENT \'Id unique sauf pour les MAPA\';');
        $this->addSql('ALTER TABLE t_contrat_titulaire MODIFY COLUMN procedure_passation_pivot varchar(256) NULL DEFAULT NULL COMMENT \'\';');
        $this->addSql('ALTER TABLE t_type_contrat MODIFY COLUMN id_externe varchar(256) NULL DEFAULT \'\' COMMENT \'Id unique sauf pour les MAPA\';');

        # FIX MPE-9835
        $this->addSql('ALTER TABLE `Offres` ADD COLUMN IF NOT EXISTS `verification_signature` VARCHAR(50) NOT NULL DEFAULT \'\';');
        $this->addSql('ALTER TABLE `autre_piece_consultation` ADD CONSTRAINT autre_piece_consultation_consultation_id_fk FOREIGN KEY (consultation_id) REFERENCES consultation (id);');
        $this->addSql('ALTER TABLE `autre_piece_consultation` ADD CONSTRAINT autre_piece_consultation_blob_id_fk FOREIGN KEY (blob_id) REFERENCES blobOrganisme_file (id);');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
