<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115154025 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-10442';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `web_service` ADD COLUMN IF NOT EXISTS `uniquement_technique` BOOLEAN NOT NULL DEFAULT TRUE COMMENT \'Définit si l accès du Ws n est qu accessible que par un agent technique\';');
        $this->addSql('CREATE TABLE IF NOT EXISTS `agent_technique_token` (
            id INT AUTO_INCREMENT NOT NULL,
                agent_id int(11) NOT NULL,
                token varchar(255) NOT NULL,
                create_at datetime NOT NULL,
                update_at datetime NOT NULL,
                date_expiration datetime NOT NULL,
                PRIMARY KEY(id),
                CONSTRAINT `agent_technique_token_agent_id_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE = InnoDB;'
        );
        $this->addSql('ALTER TABLE `web_service` ADD CONSTRAINT web_service_route_ws_method_ws UNIQUE IF NOT EXISTS (route_ws, method_ws);');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE web_service DROP uniquement_technique');
        $this->addSql('DROP TABLE agent_technique_token');
        $this->addSql('ALTER TABLE web_service DROP INDEX web_service_route_ws_method_ws');
    }
}