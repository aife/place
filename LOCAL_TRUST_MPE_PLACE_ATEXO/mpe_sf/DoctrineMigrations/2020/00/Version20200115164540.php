<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200115164540 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-11047';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `blobOrganisme_file` ADD COLUMN IF NOT EXISTS `extension` VARCHAR(50) NOT NULL DEFAULT \'\' COMMENT \'extension du fichier après -0 (.dce, .brouillon, .rc ...)\';');
        $this->addSql('ALTER TABLE `blob_file` ADD COLUMN IF NOT EXISTS `extension` VARCHAR(50) NOT NULL DEFAULT \'\' COMMENT \'extension du fichier après -0 (.dce, .brouillon, .rc ...)\';');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blobOrganisme_file DROP extension');
        $this->addSql('ALTER TABLE blob_file DROP extension');
    }
}
