<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000030 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Update data';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        # AME MPE-10955
        $this->addSql(
            'UPDATE `ValeurReferentiel` 
            SET `libelle_valeur_referentiel` = \'Préparation de la page de [_nom_page_] \', 
                `libelle_valeur_referentiel_fr` = \'Préparation de la page de [_nom_page_] \' 
            WHERE libelle_valeur_referentiel like \'%Prépartion de la page de [_nom_page_]%\';'
        );

        # MKH MPE-12606
        $this->addSql(
            'UPDATE blobOrganisme_file set chemin=CONCAT(organisme,\'/files/xml_reponse/\') 
                          where dossier =\'xml_reponse\' and chemin is null;'
        );

        # FMA MPE-11702
        $this->addSql(
            'INSERT INTO blobOrganisme_file(`old_id`, `organisme`, `name`, `deletion_datetime`, `chemin`, 
                               `dossier`, `statut_synchro`, `hash`, `id`) 
                SELECT NULL, \'DELETE\', \'DELETE\', \'2020-02-19 00:00:00\', \'DELETE\', \'DELETE\', \'0\',
                       \'ND\', MAX(`old_id`) + 10 FROM blobOrganisme_file;'
        );
        $this->addSql(
            'INSERT INTO blob_file(`old_id`, `name`, `deletion_datetime`, `chemin`, `dossier`,
                      `statut_synchro`, `hash`, `id`) 
            SELECT NULL, \'DELETE\', \'2020-02-19 00:00:00\', \'DELETE\', \'DELETE\', \'0\', \'ND\',
                   MAX(`old_id`) + 10 FROM blob_file;'
        );

        $this->addSql(
            'INSERT INTO `Tiers` (`login`, `password`, `denomination`, `fonctionnalite`, `organisme`)
            SELECT DISTINCT  \'mpe_oversight\', \'759ec8276c2e77b\', \'mpe_oversight\', \'4\', NULL 
            FROM Tiers where NOT EXISTS (SELECT 1 from  Tiers where Tiers.login like \'mpe_oversight\');'
        );

        # MPE-12326
        $this->addSql("UPDATE Inscrit SET id_externe=id_initial WHERE id_initial !=0;");

        # MPE-12511
        $this->addSql("update `Entreprise` set id_externe=id_initial WHERE  `id_initial`!=0 and id_externe=0;");
        $this->addSql("update `t_etablissement` set id_externe=id_initial WHERE  `id_initial`!=0 and id_externe=0;");
        $this->addSql("UPDATE `Organisme` set id_externe=id_initial WHERE `id_initial`!=0 and id_externe=0;");

        # MPE-12640
        $this->addSql("UPDATE `configuration_organisme` SET `espace_documentaire` = '1';");

        $this->addSql(
            'INSERT INTO messenger_messages (body, headers, queue_name, created_at, available_at, delivered_at) 
            SELECT DISTINCT \'O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:\\"\\0Symfony
            \\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component\\\\Messenger
            \\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp
            \\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp\\0busName\\";s:21:
            \\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0message
            \\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message\\\\CommandeMessenger
            \\0commande\\";s:19:\\"mpe:migration:referenceConsultationInit\\";s:37:\\"\\0App\\\\Message
            \\\\CommandeMessenger\\0params\\";a:0:{}}}\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null
            FROM jms_jobs as J where NOT EXISTS 
            (SELECT 1 from  jms_jobs as jm where jm.command like \'mpe:migration:referenceConsultationInit\' );'
        );

        $this->addSql("UPDATE HabilitationAgent SET espace_documentaire_consultation = 1 WHERE ouvrir_offre_en_ligne = '1';");

        # FLO MPE-11055
        $this->addSql("INSERT INTO `mail_type_group` (`id`, `label`) VALUES
(1, 'MessecV2'),
(2, 'MPE');");

        $this->addSql("INSERT INTO `mail_type` (`id`, `mail_type_group_id`, `label`, `code`) VALUES
(1, 1, 'Courrier libre', 'COURRIER_LIBRE'),
(2, 1, 'Invitation à concourir', 'INVITATION_CONCOURIR'),
(3, 1, 'Information de modification de la consultation', 'MODIFICATION_CONSULTATION'),
(4, 1, 'Réponse à une question', 'REPONSE_QUESTION'),
(5, 1, 'Demande de complément', 'DEMANDE_COMPLEMENT'),
(6, 1, 'Courrier d\'attribution', 'COURRIER_ATTRIBUTION'),
(7, 1, 'Courrier de notification', 'COURRIER_NOTIFICATION'),
(8, 1, 'Courrier de rejet', 'COURRIER_REJET'),
(9, 1, 'Information d\'annulation de la consultation', 'COURRIER_ANNULATION');");
// phpcs:disable
        $this->addSql("INSERT INTO `mail_template` (`id`, `mail_type_id`, `code`, `objet`, `corps`, `ordre_affichage`, `envoi_modalite`, `envoi_modalite_figee`, `reponse_attendue`, `reponse_attendue_figee`) VALUES
(1, 1, 'COURRIER_LIBRE', 'Courrier libre', '<p>Bonjour,</p>\n<p><strong>TEXTE COURRIER LIBRE</strong></p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.</p>\n<p>La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 1, 'AVEC_AR', 0, 0, 0),
(2, 2, 'INVITATION_CONCOURIR', 'Invitation à concourir', '<p>Bonjour,</p>\n<p>Vous &ecirc;tes invit&eacute; &agrave; concourir pour la consultation cit&eacute;e en r&eacute;f&eacute;rence.</p>\n<p>&nbsp;</p>', 2, 'AVEC_AR', 0, 0, 0),
(3, 3, 'MODIFICATION_CONSULTATION', 'Information de modification de la consultation', '<p>Bonjour,</p>\r\n<p>La consultation cit&eacute;e en r&eacute;f&eacute;rence a &eacute;t&eacute; modifi&eacute;e.</p>\r\n<p>Les &eacute;l&eacute;ments modifi&eacute;s sont : <strong>A PRECISER AU CAS PAR CAS</strong> Merci de votre int&eacute;r&ecirc;t pour cette consultation. <br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 3, 'AVEC_AR', 0, 0, 0),
(4, 4, 'REPONSE_QUESTION', 'Réponse à une question', '<p>Bonjour,</p>\r\n<p><strong>R&Eacute;PONSE &Agrave; LA QUESTION</strong></p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 4, 'AVEC_AR', 0, 0, 0),
(5, 5, 'DEMANDE_COMPLEMENT', 'Demande de complément', '<p>Bonjour,</p>\n<p>Nous vous remercions d\'avoir r&eacute;pondu &agrave; la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Apr&egrave;s analyse, il vous est demand&eacute; d\'apporter les pr&eacute;cisions suivantes : <strong>A PRECISER AU CAS PAR CAS</strong></p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 5, 'AVEC_AR', 1, 1, 1),
(6, 6, 'COURRIER_ATTRIBUTION', 'Courrier d\'attribution', '<p>Bonjour,</p>\n<p>J&rsquo;ai l&rsquo;honneur de vous informer que vous avez &eacute;t&eacute; d&eacute;sign&eacute; attributaire de la consultation cit&eacute;e en r&eacute;f&eacute;rence.</p>\n<p>Je vous informe que le contrat ne sera valablement form&eacute; qu&rsquo;apr&egrave;s sa signature par un repr&eacute;sentant habilit&eacute;, et ne produira ses effets qu&rsquo;au terme de la notification dudit march&eacute;.</p>\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 6, 'AVEC_AR', 1, 0, 0),
(7, 7, 'COURRIER_NOTIFICATION', 'Courrier de notification', '<p>Bonjour,</p>\r\n<p>Votre entreprise a &eacute;t&eacute; d&eacute;clar&eacute;e attributaire de la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Veuillez-trouver &agrave; titre de notification le march&eacute; sign&eacute; par un repr&eacute;sentant habilit&eacute;.<br />L\'Accus&eacute; de r&eacute;ception de ce message vaut notification officielle du march&eacute;.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 7, 'AVEC_AR', 1, 0, 0),
(8, 8, 'COURRIER_REJET', 'Courrier de rejet', '<p>Bonjour,</p>\r\n<p>Nous vous remercions d\'avoir r&eacute;pondu &agrave; la consultation cit&eacute;e en r&eacute;f&eacute;rence.<br />Nous sommes toutefois au regret de vous annoncer que votre r&eacute;ponse n\'a pas &eacute;t&eacute; retenue.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 8, 'AVEC_AR', 0, 0, 0),
(9, 9, 'COURRIER_ANNULATION', 'Information d\'annulation de la consultation', '<p>Bonjour,</p>\r\n<p>La consultation cit&eacute;e en r&eacute;f&eacute;rence a &eacute;t&eacute; annul&eacute;e.</p>\r\n<p>Merci de votre int&eacute;r&ecirc;t pour cette consultation.<br />La plate-forme de d&eacute;mat&eacute;rialisation des march&eacute;s publics</p>', 9, 'AVEC_AR', 0, 0, 0);");
// phpcs:enable
        # MPE-12400
        $this->addSql(
            "UPDATE `configuration_plateforme` SET `messagerie_v2` = '1' 
                  WHERE `configuration_plateforme`.`id_auto` = 1;"
        );
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
