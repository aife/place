<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ceci est un script d'initialisation permettant de contourner le problème
 * ansible lorsque l'on n'a pas encore de script à migrer.
 */
final class Version20200000000000 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('-- "Doctrine migrations initialisation"');
        $this->addSql('update fichierEnveloppe set id_blob = null where `id_blob` = 0;');
        $this->addSql("INSERT  INTO blobOrganisme_file(id, name , Organisme, statut_synchro, chemin) 
SELECT id_blob, nom_fichier, Organisme, '1', 'ERROR' FROM `fichierEnveloppe` fe  
WHERE NOT EXISTS ( SELECT b.* FROM blobOrganisme_file b WHERE fe.id_blob = b.id and b.Organisme = fe.Organisme )
  and `id_blob` is not null;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('-- "Doctrine migrations initialisation"');
    }
}
