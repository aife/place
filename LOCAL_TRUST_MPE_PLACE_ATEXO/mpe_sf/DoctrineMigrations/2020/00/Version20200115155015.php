<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115155015 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-11575';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS `agent_technique_association` (`id` int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,`id_agent` int(11) NOT NULL,`organisme` varchar(50) DEFAULT NULL,`id_service` int(11) NOT NULL DEFAULT 0) ENGINE=InnoDB;');
        $this->addSql('ALTER TABLE `agent_technique_association` ADD UNIQUE KEY IF NOT EXISTS `association_unique` (`id_agent`,`organisme`,`id_service`);');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE agent_technique_association');
    }
}