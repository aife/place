<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200000000028 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Initialisation -- MPE-11566';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE `configuration_organisme` CHANGE `espace_documentaire` `espace_documentaire` TINYINT(1) NOT NULL DEFAULT \'1\';');
        $this->addSql('ALTER TABLE `configuration_plateforme` CHANGE `messagerie_v2` `messagerie_v2` TINYINT(1) NOT NULL DEFAULT \'1\' COMMENT \'active la messagerie sécurisé v2\';');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
    }
}
