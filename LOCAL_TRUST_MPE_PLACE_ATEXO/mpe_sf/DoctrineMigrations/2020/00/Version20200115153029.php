<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115153029 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-10437';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS `habilitation_agent_ws` (
                id INT AUTO_INCREMENT NOT NULL,
                agent_id int(11) NOT NULL,
                web_service_id int(11) NOT NULL,
                PRIMARY KEY(id),
                CONSTRAINT `habilitation_agent_ws_id_agent_fk` FOREIGN KEY (`agent_id`) REFERENCES `Agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `habilitation_agent_ws_id_web_service_fk` FOREIGN KEY (`web_service_id`) REFERENCES `web_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            )
            ENGINE = InnoDB;'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE habilitation_agent_ws');
    }
}