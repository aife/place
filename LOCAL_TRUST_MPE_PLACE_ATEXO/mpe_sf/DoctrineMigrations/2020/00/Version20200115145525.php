<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

class Version20200115145525 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-11066';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS `archive_arcade` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `annee` int(11) NOT NULL,
          `num_semaine` int(11) NOT NULL,
          `poids_archive` int(11) DEFAULT NULL,
          `chemin_fichier` varchar(255) DEFAULT NULL,
          `date_envoi_debut` DATETIME NULL,
          `date_envoi_fin` DATETIME NULL,
          `status_transmission` tinyint(1) DEFAULT \'0\' NOT NULL,
          `erreur` varchar(255) DEFAULT NULL,
           PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;');
        $this->addSql('ALTER TABLE `archive_arcade` ADD UNIQUE IF NOT EXISTS `IDX_ANNNEE_SEMAINE` (`annee`, `num_semaine`);');
        $this->addSql('CREATE TABLE IF NOT EXISTS `consultation_archive_arcade` (
           `consultation_archive_id` int(11) NOT NULL,
           `archive_arcade_id` int(11) DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
        $this->addSql('ALTER TABLE `consultation_archive_arcade`
            ADD PRIMARY KEY IF NOT EXISTS (`consultation_archive_id`),
            ADD KEY IF NOT EXISTS `archive_arcade_id` (`archive_arcade_id`);'
        );
        $this->addSql('ALTER TABLE `consultation_archive_arcade`
            ADD CONSTRAINT `FK_archive_arcade_id` FOREIGN KEY IF NOT EXISTS (`archive_arcade_id`) REFERENCES `archive_arcade` (`id`),
            ADD CONSTRAINT `FK_consultation_archive_id` FOREIGN KEY IF NOT EXISTS (`consultation_archive_id`) REFERENCES `consultation_archive` (`id`);'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE archive_arcade');
        $this->addSql('DROP TABLE consultation_archive_arcade');
    }
}