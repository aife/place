<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Ceci est un script d'initialisation permettant de contourner le problème
 * ansible lorsque l'on n'a pas encore de script à migrer.
 */
final class Version20200000000010 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Reprise';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        # ----------- Reprise structure -----------------
        # LEZ MPE-5820
        $this->addSql("ALTER TABLE `ProcedureEquivalence`	ADD COLUMN IF NOT EXISTS `afficher_code_cpv` VARCHAR(2) NULL DEFAULT '1';");
        $this->addSql("ALTER TABLE `ProcedureEquivalence`	ADD COLUMN IF NOT EXISTS `code_cpv_obligatoire` VARCHAR(2) NULL DEFAULT '1' ;");

        # AME MPE-7433
        $this->addSql("ALTER TABLE `configuration_plateforme`	ADD COLUMN IF NOT EXISTS `dossier_volumineux` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet d''activer l''upload et download des fichiers volumineux';");

        # LEZ MPE-6123
        $this->addSql("ALTER TABLE `Agent` CHANGE COLUMN `id_initial` `id_externe` INT(11) NOT NULL DEFAULT '0';");
        $this->addSql("ALTER TABLE `Agent`	CHANGE COLUMN `id_externe` `id_externe` VARCHAR(50) NOT NULL DEFAULT '0';");

        # FLO MPE-5827
        $this->addSql("ALTER TABLE `configuration_plateforme` ADD `afficher_valeur_estimee` ENUM('0','1') NOT NULL DEFAULT '0';");

        $this->addSql("ALTER TABLE `blob_file` ADD COLUMN  IF NOT EXISTS  `chemin` TEXT NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `blobOrganisme_file` ADD COLUMN  IF NOT EXISTS  `chemin` TEXT NULL DEFAULT NULL;");

        # FMA-MPE-7465
        $this->addSql("CREATE TABLE IF NOT EXISTS `dossier_volumineux` ( `id` INT NOT NULL AUTO_INCREMENT , `uuid` VARCHAR(30) NOT NULL , `nom` VARCHAR(30) NOT NULL , `taille` BIGINT UNSIGNED NOT NULL , `date_creation` DATETIME NOT NULL , `statut` BOOLEAN NOT NULL , `id_agent` INT NULL , `id_inscrit` INT NULL , `id_blob` INT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");

        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT IF NOT EXISTS `id_agent_dv_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT IF NOT EXISTS `id_inscrit_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT IF NOT EXISTS `id_blob_FK` FOREIGN KEY (`id_blob`) REFERENCES `blob_file` (`id`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD UNIQUE(`uuid`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `taille` `taille` BIGINT(20) UNSIGNED NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `statut` `statut` TINYINT(1) NOT NULL DEFAULT '0';");


        $this->addSql("ALTER TABLE `consultation` ADD COLUMN IF NOT EXISTS `id_dossier_volumineux` INT NULL;");
        $this->addSql("ALTER TABLE `consultation` ADD CONSTRAINT IF NOT EXISTS `id_dossier_volumineux_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`);");

        $this->addSql("ALTER TABLE `Enveloppe` ADD COLUMN IF NOT EXISTS `id_dossier_volumineux` INT NULL;");
        $this->addSql("ALTER TABLE `Enveloppe` ADD CONSTRAINT IF NOT EXISTS `id_dv_FK` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`);");

        $this->addSql("ALTER TABLE `dossier_volumineux` ADD COLUMN IF NOT EXISTS  `uuid_tus` VARCHAR(255) NOT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `uuid_tus` `uuid_technique` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD UNIQUE(`uuid_technique`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `uuid` `uuid_reference` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP INDEX `uuid`, ADD UNIQUE `uuid_reference` (`uuid_reference`) USING BTREE;");

        $this->addSql("ALTER TABLE `dossier_volumineux` DROP INDEX `id_agent_dv_FK`, ADD INDEX `dossier_volumineux_agent_FK` (`id_agent`) USING BTREE;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP INDEX `id_inscrit_FK`, ADD INDEX `dossier_volumineux_inscrit_FK` (`id_inscrit`) USING BTREE;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP INDEX `id_blob_FK`, ADD INDEX `dossier_volumineux_blob_FK` (`id_blob`) USING BTREE;");

        # FMA-MPE-7575
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `statut` `statut` VARCHAR(30) NOT NULL DEFAULT 'init' COMMENT 'valauer possible : init, complet, incomplet';");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD COLUMN IF NOT EXISTS `actif` BOOLEAN NULL DEFAULT FALSE;");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD COLUMN IF NOT EXISTS `id_blob_logfile` INT NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT IF NOT EXISTS `dossier_volumineux_id_blob_logfile_FK` FOREIGN KEY (`id_blob_logfile`) REFERENCES `blob_file` (`id`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY `id_blob_FK`;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP INDEX `dossier_volumineux_blob_FK`;");
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `id_blob` `id_blob_descripteur` INT(11) NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT IF NOT EXISTS `dossier_volumineux_id_blob_descripteur_FK` FOREIGN KEY (`id_blob_descripteur`) REFERENCES `blob_file` (`id`);");

        # FLO-MPE-7436
        $this->addSql("ALTER TABLE `dossier_volumineux` CHANGE `nom` `nom` VARCHAR(30) NULL DEFAULT NULL;");

        # MKH-MPE-7448
        $this->addSql("ALTER TABLE `Echange` ADD COLUMN IF NOT EXISTS `id_dossier_volumineux` INT NULL;");

        # FMA-MPE-7575
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD COLUMN  IF NOT EXISTS  `date_modification` DATETIME NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `AVIS` ADD COLUMN  IF NOT EXISTS  `chemin` TEXT NULL DEFAULT NULL;");

        # mkh mpe-7581
        $this->addSql("ALTER TABLE `configuration_plateforme`  ADD COLUMN IF NOT EXISTS  `entreprise_mots_de_passe_historises` INT(2) NOT NULL DEFAULT 0 AFTER `interface_dume`;");
        $this->addSql("ALTER TABLE `configuration_plateforme`  ADD COLUMN IF NOT EXISTS  `entreprise_duree_vie_mot_de_passe` INT(2) NOT NULL DEFAULT 0 AFTER `interface_dume`;");

        # mkh mpe-7582
        $this->addSql("CREATE TABLE IF NOT EXISTS `historisation_mot_de_passe` ( `id` INT NOT NULL AUTO_INCREMENT , `ancien_mot_de_passe` VARCHAR(64) NOT NULL  ,  `date_modification` DATETIME NOT NULL , `id_inscrit` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        $this->addSql("ALTER TABLE `historisation_mot_de_passe` ADD CONSTRAINT IF NOT EXISTS `id_inscrit_histo_mdp_FK` FOREIGN KEY (`id_inscrit`) REFERENCES `Inscrit` (`id`);");

        # AME MPE-6313
        $this->addSql("ALTER TABLE `configuration_plateforme`  ADD COLUMN IF NOT EXISTS  `case_attestation_consultation` ENUM('0','1') NOT NULL DEFAULT '0' COMMENT 'Permet activer une case à cocher dans le formulaire de la consultation afin d''attester que le besoin n''est pas déjà couvert par un marché national ou local';");
        $this->addSql("ALTER TABLE `consultation`  ADD COLUMN IF NOT EXISTS  `attestation_consultation` CHAR(2) NOT NULL DEFAULT '0' COMMENT 'Permet d''attester que le besoin n''est pas déjà couvert par un marché national ou local';");

        # MKH MPE-7937
        $this->addSql("ALTER TABLE `Service` CHANGE `id_initial` `id_externe` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0';");

        # NKN MPE-8192
        $this->addSql("UPDATE Inscrit i left JOIN t_etablissement e ON e.id_etablissement = i.id_etablissement SET i.id_etablissement = NULL WHERE e.id_etablissement IS NULL;");

        # NKN MPE-8224
        $this->addSql("ALTER TABLE `Echange` ADD CONSTRAINT IF NOT EXISTS `echange_dossier_volumineux_fk` FOREIGN KEY (`id_dossier_volumineux`) REFERENCES `dossier_volumineux` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;");

        $this->addSql("CREATE TABLE `consultation_favoris` ( `id` INT NOT NULL AUTO_INCREMENT , `id_consultation` INT NOT NULL , `id_agent` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;");
        $this->addSql("ALTER TABLE `consultation_favoris` ADD CONSTRAINT `consultation_favoris_id_consultation_FK` FOREIGN KEY (`id_consultation`) REFERENCES `consultation` (`reference`)  ON DELETE CASCADE;");
        $this->addSql("ALTER TABLE `consultation_favoris` ADD CONSTRAINT `consultation_favoris_id_agent_FK` FOREIGN KEY (`id_agent`) REFERENCES `Agent` (`id`) ON DELETE CASCADE;");


        # mwa MPE-7615
        $this->addSql("ALTER TABLE `HabilitationAgent`  ADD COLUMN IF NOT EXISTS  `invite_permanent_entite_dependante` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `suivre_consultation_pole`, ADD `invite_permanent_mon_entite` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_entite_dependante`, ADD `invite_permanent_transverse` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_mon_entite`;");
        $this->addSql("ALTER TABLE `HabilitationProfil`  ADD COLUMN IF NOT EXISTS  `invite_permanent_entite_dependante` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `suivre_consultation_pole`, ADD `invite_permanent_mon_entite` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_entite_dependante`, ADD `invite_permanent_transverse` ENUM('0','1') NOT NULL DEFAULT '0' AFTER `invite_permanent_mon_entite`;");

        # mwa MPE-7617
        $this->addSql("CREATE TABLE  IF NOT EXISTS invite_permanent_transverse (
        `id` INT NOT NULL AUTO_INCREMENT ,
`service_id` INT NOT NULL ,
`agent_id` INT NOT NULL ,
PRIMARY KEY (`id`),
INDEX `invitePermanentTransverse_service_idx` (`service_id`),
INDEX `invitePermanentTransverse_agent_idx` (`agent_id`)
) ENGINE = InnoDB;");

        $this->addSql("ALTER TABLE invite_permanent_transverse
ADD CONSTRAINT `t_invite_permanent_transverse_service_id_FK` FOREIGN KEY (`service_id`)
REFERENCES `Service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");

        $this->addSql("ALTER TABLE invite_permanent_transverse
ADD CONSTRAINT `t_invite_permanent_transverse_agent_id_FK` FOREIGN KEY (`agent_id`)
REFERENCES `Agent` (`id`) ON DELETE CASCADE  ON UPDATE CASCADE;");

        # FMA-7617
        $this->addSql("ALTER TABLE invite_permanent_transverse ADD COLUMN IF NOT EXISTS  `acronyme` VARCHAR(30) NOT NULL;");
        $this->addSql("ALTER TABLE invite_permanent_transverse CHANGE `service_id` `service_id` INT(11) NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE invite_permanent_transverse 
    ADD CONSTRAINT `invite_permanent_transverse_organisme_acronyme_FK` 
        FOREIGN KEY (`acronyme`) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;");

        $this->addSql("ALTER TABLE `Agent`  ADD COLUMN IF NOT EXISTS  `alerte_mes_consultations` ENUM('0','1') NOT NULL DEFAULT '1';");
        $this->addSql("ALTER TABLE `Agent`  ADD COLUMN IF NOT EXISTS  `alerte_consultations_mon_entite` ENUM('0','1') NOT NULL DEFAULT '1';");
        $this->addSql("ALTER TABLE `Agent`  ADD COLUMN IF NOT EXISTS  `alerte_consultations_des_entites_dependantes` ENUM('0','1') NOT NULL DEFAULT '1';");
        $this->addSql("ALTER TABLE `Agent`  ADD COLUMN IF NOT EXISTS  `alerte_consultations_mes_entites_transverses` ENUM('0','1') NOT NULL DEFAULT '1';");

        # NBS MPE-9641
        $this->addSql("ALTER TABLE Inscrit ALTER COLUMN type_hash SET DEFAULT NULL;");

        # on garde les deux lignes car le champ a déjà été créé sur certaines PF (intégration)
        # NBS-MPE-10040
        $this->addSql("ALTER TABLE Enveloppe  ADD COLUMN IF NOT EXISTS  date_debut_dechiffrement VARCHAR(20) NOT NULL DEFAULT '0000-00-00 00:00:00';");

        # NBS MPE-9829
        $this->addSql("ALTER TABLE Enveloppe MODIFY COLUMN date_debut_dechiffrement DATETIME NULL;");

        # FMA MPE-10364
        $this->addSql("ALTER TABLE ProcedureEquivalence  ADD COLUMN IF NOT EXISTS  donnees_complementaire_non VARCHAR(2) NOT NULL DEFAULT '+0';");
        $this->addSql("ALTER TABLE ProcedureEquivalence  ADD COLUMN IF NOT EXISTS  donnees_complementaire_oui VARCHAR(2) NOT NULL DEFAULT '+1';");


        # FMA MPE-10083
        $this->addSql("ALTER TABLE `configuration_organisme`  ADD COLUMN IF NOT EXISTS  `espace_documentaire` BOOLEAN NOT NULL DEFAULT FALSE;");

        # FMA MPE-10085
        $this->addSql("ALTER TABLE `HabilitationAgent`  ADD COLUMN IF NOT EXISTS  `espace_documentaire_consultation` BOOLEAN NOT NULL DEFAULT FALSE;");

        # FMA MPE-10120
        $this->addSql("CREATE TABLE IF NOT EXISTS `autre_piece_consultation` (
            id INT AUTO_INCREMENT NOT NULL,
            blob_id INT NOT NULL,
            consultation_id INT NOT NULL,
            created_at  DATETIME NOT NULL,
            updated_at DATETIME NULL,
            PRIMARY KEY(id)
        ) ENGINE = InnoDB;");



        # FMA MPE-10083
        $this->addSql("ALTER TABLE `configuration_organisme`  ADD COLUMN IF NOT EXISTS  `espace_documentaire` BOOLEAN NOT NULL DEFAULT FALSE;");

        # FMA MPE-10085
        $this->addSql("ALTER TABLE `HabilitationAgent`  ADD COLUMN IF NOT EXISTS  `espace_documentaire_consultation` BOOLEAN NOT NULL DEFAULT FALSE;");

        # FMA MPE-10120
        $this->addSql("CREATE TABLE IF NOT EXISTS `autre_piece_consultation` (
    id INT AUTO_INCREMENT NOT NULL,
    blob_id INT NOT NULL,
    consultation_id INT NOT NULL,
    created_at  DATETIME NOT NULL,
    updated_at DATETIME NULL,
    PRIMARY KEY(id)
) ENGINE = InnoDB;");


        $this->addSql("ALTER TABLE `Offres`  ADD COLUMN IF NOT EXISTS  `id_blob_xml_reponse` INT(11) NULL DEFAULT NULL;");

        $this->addSql("ALTER TABLE `blobOrganisme_file`  ADD COLUMN IF NOT EXISTS  `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';");
        $this->addSql("ALTER TABLE `blob_file`  ADD COLUMN IF NOT EXISTS  `dossier` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom du dossier où le blob sera stocké. Si vide le stockage se fait à la racine du dossier.';");



        # Suppression des tables blobOrganisme et blob
        # la bdd doit être arretée et la plateforme également

        # ATTENTION AVANT DE FAIRE LE RESTE VERIFIER QUE LES DEUX ID ONT LA MEME VALEUR
        # SELECT max(id) as max_file FROM `blobOrganisme` WHERE 1 UNION ALL SELECT max(id) as max_file FROM `blobOrganisme_file` WHERE 1;


        $this->addSql("ALTER TABLE `DocumentExterne` DROP FOREIGN KEY IF EXISTS `DOC_EX_Blob`;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY IF EXISTS `dossier_volumineux_id_blob_descripteur_FK`;");
        $this->addSql("ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY IF EXISTS `dossier_volumineux_id_blob_logfile_FK`;");
        $this->addSql("ALTER TABLE `Offres` DROP FOREIGN KEY IF EXISTS `offres_id_blob_xml_reponse`;");

        # table `blobOrganisme_file`
        # rendre l''id auto incremental

        $this->addSql("ALTER TABLE `blobOrganisme_file` DROP PRIMARY KEY;");
        $this->addSql("ALTER TABLE `blobOrganisme_file` CHANGE COLUMN `id` `old_id` int(11) NULL DEFAULT NULL;");
        $this->addSql("ALTER TABLE `blobOrganisme_file`  ADD COLUMN IF NOT EXISTS  `id` INT(11) PRIMARY KEY AUTO_INCREMENT;");
        $this->addSql("ALTER TABLE `blobOrganisme_file` ADD INDEX `blob_organisme_file_old_id` (`old_id`);");

        $this->addSql("ALTER TABLE `blob_file` DROP PRIMARY KEY;");
        $this->addSql("ALTER TABLE `blob_file` CHANGE COLUMN `id` `old_id` int(11) NULL DEFAULT NULL;;");
        $this->addSql("ALTER TABLE `blob_file`  ADD COLUMN IF NOT EXISTS  `id` INT(11) PRIMARY KEY AUTO_INCREMENT;");
        $this->addSql("ALTER TABLE `blob_file` ADD INDEX `blob_file_old_id` (`old_id`);");

        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_descripteur_FK` FOREIGN KEY IF NOT EXISTS (`id_blob_descripteur`) REFERENCES `blob_file` (`id`);");
        $this->addSql("ALTER TABLE `dossier_volumineux` ADD CONSTRAINT `dossier_volumineux_id_blob_logfile_FK` FOREIGN KEY IF NOT EXISTS (`id_blob_logfile`) REFERENCES `blob_file` (`id`);");

        # ajout clé externe vers blobOrganisme_file
        $this->addSql("UPDATE `DocumentExterne` d SET `idBlob` =
                           (SELECT id FROM blobOrganisme_file b WHERE d.idBlob = b.old_id and d.organisme = b.organisme)
                           WHERE d.idBlob IS NOT NULL;");

        $this->addSql("ALTER TABLE `DocumentExterne` ADD CONSTRAINT `DOC_EX_Blob` 
    FOREIGN KEY (`idBlob`) REFERENCES `blobOrganisme_file` (`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE;"
        );

        $this->addSql("ALTER TABLE blobOrganisme_file DROP revision;");
        $this->addSql("ALTER TABLE blob_file DROP revision;");

        $this->addSql("DROP TABLE `blobOrganisme`;");
        $this->addSql("DROP TABLE `blob`;");

        # FMA-10327
        $this->addSql("ALTER TABLE `consultation_favoris` ADD UNIQUE `consultation_favoris_id_consultation_id_agent_unique` (`id_consultation`, `id_agent`);");


        # ------------ Data ---------------
        #MPE-7432
        $this->addSql("INSERT INTO `Tiers` (`id_tiers`, `login`, `password`, `denomination`, `fonctionnalite`, `organisme`) VALUES (NULL, 'loggerMPE', LEFT(REPLACE(UUID(), '-', ''), 50), 'loggerMPE', '5', NULL);");

        #MPE-7447
        $this->addSql("INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`, `valeur_sub`, `ordre_affichage`) VALUES (NULL, '210', 'Téléchargement des annexes techniques du DCE', 'Téléchargement des annexes techniques du DCE', '', '', '', '', '', '', 'ANNEXES1', '', '', '0');");

        # mwa MPE-7611
        # mise à jour de habilitations Agent
        $this->addSql("UPDATE HabilitationAgent SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';");
        $this->addSql("UPDATE HabilitationAgent SET invite_permanent_entite_dependante='1' WHERE suivre_consultation_pole='1' AND HabilitationAgent.id_agent IN (SELECT Agent.id FROM Agent, Organisme, configuration_organisme WHERE Agent.organisme=Organisme.acronyme AND Organisme.acronyme=configuration_organisme.organisme AND configuration_organisme.organisation_centralisee='1');");

        # mise à jour des Profils hors PF PLACE
        $this->addSql("UPDATE HabilitationProfil SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';");
        $this->addSql("UPDATE HabilitationProfil SET invite_permanent_entite_dependante='1' WHERE suivre_consultation_pole='1';");

        # mise à jour des Profils pour PF PLACE
        $this->addSql("UPDATE HabilitationProfil SET invite_permanent_mon_entite='1' WHERE suivre_consultation_pole='1';");

        # SLE MPE-11245
        $this->addSql("ALTER TABLE ProcedureEquivalence ALTER donnees_complementaire_non SET DEFAULT '+1';");
        $this->addSql("ALTER TABLE ProcedureEquivalence ALTER donnees_complementaire_oui SET DEFAULT '+0';");
        $this->addSql("UPDATE ProcedureEquivalence SET donnees_complementaire_non = '+1', donnees_complementaire_oui = '+0';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql('');
    }
}
