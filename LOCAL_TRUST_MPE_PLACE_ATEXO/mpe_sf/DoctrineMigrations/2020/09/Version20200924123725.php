<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=0);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200924123725
 * @package Application\Migrations
 */
final class Version20200924123725 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13535 - Enregistrement de la PFV sur la table consultation coté agent ';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `consultation` ADD `plateforme_virtuelle_id` INT UNSIGNED NULL DEFAULT NULL");
        $this->addSql("ALTER TABLE `consultation` ADD CONSTRAINT `fk_plateforme_virtuelle_consultation_id` 
 FOREIGN KEY ( `plateforme_virtuelle_id` ) REFERENCES `plateforme_virtuelle` (`id`) ON DELETE NO ACTION;");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `consultation` DROP FOREIGN KEY IF EXISTS fk_plateforme_virtuelle_consultation_id;");
        $this->addSql("ALTER TABLE `consultation` DROP `plateforme_virtuelle_id`;");
    }
}
