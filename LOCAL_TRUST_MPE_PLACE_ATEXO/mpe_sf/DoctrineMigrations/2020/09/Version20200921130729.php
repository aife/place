<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200921130729
 * @package Application\Migrations
 */
final class Version20200921130729 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-13478 - Paramétrage des procédures';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `ProcedureEquivalence` ADD `signature_autoriser` CHAR(2) NOT NULL DEFAULT '+0';");



    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `ProcedureEquivalence` DROP `signature_autoriser`;");

    }
}
