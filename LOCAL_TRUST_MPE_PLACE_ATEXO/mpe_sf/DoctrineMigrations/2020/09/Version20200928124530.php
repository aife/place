<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20200928124530 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-13534 - Gestion de la charte PFV coté mail.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `plateforme_virtuelle` ADD `protocole` VARCHAR(5) NOT NULL DEFAULT 'https'");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` ADD `no_reply` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Email de surcharge du \"nepasrepondre@domaine.xx\"'");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` ADD `footer_mail` TEXT NULL DEFAULT NULL COMMENT 'text qui surcharge le paramètre applicatif : \"PLACE_MARCHE_PUBLIC_INTERMINISTERE\"'");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` ADD `from_pf_name` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nom de la plate-forme dans le From du mail'");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE `plateforme_virtuelle` DROP `protocole`;");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` DROP `no_reply`;");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` DROP `footer_mail`;");
        $this->addSql("ALTER TABLE `plateforme_virtuelle` DROP `from_pf_name`;");
    }
}
