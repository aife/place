<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

final class Version20200917113504 extends AbstractMigration  implements ContainerAwareInterface
{
    /** @var ContainerInterface */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getDescription() : string
    {
        return 'MPE-12967';
    }

    public function up(Schema $schema) : void
    {
        $heureClotureDefaut = $this->container->getParameter('HEURE_CLOTURE_DEFAUT');

        $query = "SELECT value from configuration_client where parameter = 'HEURE_CLOTURE_DEFAUT'";
        $confClient = $this->connection->prepare($query);
        $confClient->execute();

        $result = $confClient->fetch();

        if(is_array($result)){
            $heureClotureDefaut = $result['value'];
        }
        $this->addSql("UPDATE `HabilitationProfil` SET `administrer_organisme` = 1 where id = 1;");
        $this->addSql("UPDATE `configuration_organisme` SET `heure_limite_de_remise_de_plis_par_defaut` = '".$heureClotureDefaut."'");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `HabilitationProfil` SET `administrer_organisme` = 0 where id = 1;");
        $this->addSql("UPDATE `configuration_organisme` SET `heure_limite_de_remise_de_plis_par_defaut` = '';");
    }
}
