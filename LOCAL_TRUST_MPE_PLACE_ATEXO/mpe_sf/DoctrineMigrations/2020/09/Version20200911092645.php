<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200911092645 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13345';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `HabilitationAgent` ADD `administrer_organisme` INT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE `HabilitationProfil` ADD `administrer_organisme` INT NOT NULL DEFAULT 0;');
        $this->addSql('ALTER TABLE `configuration_organisme` ADD `heure_limite_de_remise_de_plis_par_defaut` VARCHAR(10) DEFAULT NULL;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `HabilitationProfil` DROP `administrer_organisme`;');
        $this->addSql('ALTER TABLE `HabilitationAgent` DROP `administrer_organisme`; ');
        $this->addSql('ALTER TABLE configuration_organisme DROP heure_limite_de_remise_de_plis_par_defaut;');
    }
}
