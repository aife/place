<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200820121304 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Suppression d\'un doublon et redécaler correctement les ID';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE echange_doc_blob DROP FOREIGN KEY FK_ECHANGE_TYPE_PIECE_ACTES');
        $this->addSql("TRUNCATE TABLE `echange_doc_type_piece_actes`");
        $this->addSql("INSERT INTO `echange_doc_type_piece_actes` (`id`, `code`, `libelle`, `type_nature`, 
`code_classification`) VALUES 
(1, '11_AE', 'Acte d\'engagement', 'CC', '1.1'),
(2, '99_AU', 'Autre document', 'AU', '9.9'),
(3, '11_AC', 'Avis d\'appel public à concurrence', 'CC', '1.1'),
(4, '11_AV', 'Avis du jury de concours', 'CC', '1.1'),
(5, '11_BP', 'Bordereau des prix', 'CC', '1.1'),
(6, '11_AP', 'Cahier des clauses administratives particulières', 'CC', '1.1'),
(7, '11_TP', 'Cahier des clauses techniques particulières', 'CC', '1.1'),
(8, '10_DE', 'Délibération autorisant à passer le contrat', 'CC', '1.0'),
(9, '99_DC', 'Document contractuel', 'CC', '9.9'),
(10, '99_SE', 'Fichier de signature électronique', 'CC', '9.9'),
(11, '11_IN', 'Invitation des candidats à soumissionner', 'CC', '1.1'),
(12, '10_MT', 'Mémoire technique', 'CC', '1.0'),
(13, '10_AV', 'Modification du contrat', 'CC', '1.0'),
(14, '11_PV', 'Procès verbal de la commission d\'appel d\'offre ou du jury', 'CC', '1.1'),
(15, '11_RA', 'Rapport de la commission d\'appel d\'offre', 'CC', '1.1'),
(16, '11_RP', 'Rapport de présentation de l\'acheteur', 'CC', '1.1'),
(17, '11_JU', 'Rapport justifiant le choix du marché, les modalités et la procédure de passation', 'CC', '1.1'),
(18, '10_RD', 'Registre des dépôts des offres', 'CC', '1.0'),
(19, '10_RC', 'Règlement de la consultation', 'CC', '1.0'),
(20, '10_AT', 'Renseignements, attestations et déclarations fournies par l\'attributaire', 'CC', '1.0')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("TRUNCATE TABLE `echange_doc_type_piece_actes`");
        $this->addSql('ALTER TABLE echange_doc_blob ADD CONSTRAINT FK_ECHANGE_TYPE_PIECE_ACTES FOREIGN KEY
                           (echange_type_piece_actes_id) REFERENCES echange_doc_type_piece_actes (id)');
        $this->addSql("INSERT INTO `echange_doc_type_piece_actes` (`id`, `code`, `libelle`, `type_nature`, 
`code_classification`) VALUES
(1, '11_AE', 'Acte d\'engagement', 'CC', '1.1'),
(2, '99_AU', 'Autre document', 'AU', '9.9'),
(3, '11_AC', 'Avis d\'appel public à concurrence', 'CC', '1.1'),
(4, '11_AV', 'Avis du jury de concours', 'CC', '1.1'),
(5, '11_BP', 'Bordereau des prix', 'CC', '1.1'),
(6, '11_AP', 'Cahier des clauses administratives particulières', 'CC', '1.1'),
(7, '11_TP', 'Cahier des clauses techniques particulières', 'CC', '1.1'),
(8, '10_DE', 'Délibération autorisant à passer le contrat', 'CC', '1.0'),
(9, '99_DC', 'Document contractuel', 'CC', '9.9'),
(10, '99_SE', 'Fichier de signature électronique', 'AU', '9.9'),
(11, '99_SE', 'Fichier de signature électronique', 'CC', '9.9'),
(12, '11_IN', 'Invitation des candidats à soumissionner', 'CC', '1.1'),
(13, '10_MT', 'Mémoire technique', 'CC', '1.0'),
(14, '10_AV', 'Modification du contrat', 'CC', '1.0'),
(15, '11_PV', 'Procès verbal de la commission d\'appel d\'offre ou du jury', 'CC', '1.1'),
(16, '11_RA', 'Rapport de la commission d\'appel d\'offre', 'CC', '1.1'),
(17, '11_RP', 'Rapport de présentation de l\'acheteur', 'CC', '1.1'),
(18, '11_JU', 'Rapport justifiant le choix du marché, les modalités et la procédure de passation', 'CC', '1.1'),
(19, '10_RD', 'Registre des dépôts des offres', 'CC', '1.0'),
(20, '10_RC', 'Règlement de la consultation', 'CC', '1.0'),
(21, '10_AT', 'Renseignements, attestations et déclarations fournies par l\'attributaire', 'CC', '1.0')");
    }
}
