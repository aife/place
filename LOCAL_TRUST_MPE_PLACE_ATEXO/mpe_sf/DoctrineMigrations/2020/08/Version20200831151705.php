<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200831151705 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-12761 : [Mégalis] - Suppression d'une annonce de consultation KO";
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `AVIS` DROP FOREIGN KEY IF EXISTS `FK_AVIS_consultation`; ");
        $this->addSql("ALTER TABLE `Annonce` DROP FOREIGN KEY IF EXISTS `FK_Annonce_consultation`; ");
        $this->addSql("ALTER TABLE `AnnonceBoamp` DROP FOREIGN KEY IF EXISTS `FK_AnnonceBoamp_consultation`; ");
        $this->addSql("ALTER TABLE `Avis_Pub` DROP FOREIGN KEY IF EXISTS `FK_AvisPub_consultation`; ");
        $this->addSql("ALTER TABLE `Complement` DROP FOREIGN KEY IF EXISTS `FK_Complement_consultation`; ");
        $this->addSql("ALTER TABLE `DATEFIN` DROP FOREIGN KEY IF EXISTS `FK_DATEFIN_consultation`; ");
        $this->addSql("ALTER TABLE `DCE` DROP FOREIGN KEY IF EXISTS `FK_DCE_consultation`; ");
        $this->addSql("ALTER TABLE `DocumentExterne` DROP FOREIGN KEY IF EXISTS `FK_DOC_EX_consultation`; ");

        $this->addSql("ALTER TABLE  `AVIS` ADD CONSTRAINT `FK_AVIS_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `Annonce` ADD CONSTRAINT `FK_Annonce_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `AnnonceBoamp` ADD CONSTRAINT `FK_AnnonceBoamp_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `Avis_Pub` ADD CONSTRAINT `FK_AvisPub_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `Complement` ADD CONSTRAINT `FK_Complement_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `DATEFIN` ADD CONSTRAINT `FK_DATEFIN_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `DCE` ADD CONSTRAINT `FK_DCE_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql("ALTER TABLE  `DocumentExterne` ADD CONSTRAINT `FK_DOC_EX_consultation` FOREIGN KEY IF NOT EXISTS (`consultation_id`) REFERENCES  `consultation` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    public function down(Schema $schema): void
    {
    // nothing todo
    }
}
