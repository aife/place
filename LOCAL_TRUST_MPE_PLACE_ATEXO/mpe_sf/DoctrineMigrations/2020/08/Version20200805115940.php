<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200805115940 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-13220';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        // PASTELL - Flux PDM dans echange_doc_application
        $this->addSql("INSERT IGNORE INTO echange_doc_application (id, code, libelle, flux_actes, primo_signature) VALUES ('2', 'PASTELL_V2_PDM', 'Pastell v2.0 – Flux Pièce de marché', '0', '1');");

        // PASTELL - Flux ACTES dans echange_doc_application
        $this->addSql("INSERT IGNORE INTO echange_doc_application (id, code, libelle, flux_actes, primo_signature) VALUES ('3', 'PASTELL_V2_ACTES', 'Pastell v2.0 – Flux ACTES', '1', '0');");

        // PASTELL - Modèle de flux PDM dans echange_doc_application_client
        $this->addSql("INSERT IGNORE INTO echange_doc_application_client (id, echange_doc_application_id, code, libelle, actif, cheminement_signature, cheminement_ged, cheminement_sae, cheminement_tdt, classification_1, classification_2, classification_3, classification_4, classification_5) VALUES (NULL, '2', 'PASTELL_V2_PDM_01', 'Pastell – Flux pièces de marché (Modèle)', '1', '1', '1', '1', '0', 'SOUS_TYPE_PARAPHEUR', 'EO', NULL, NULL, NULL);");

        // PASTELL - Modèle de flux ACTES dans echange_doc_application_client
        $this->addSql("INSERT IGNORE INTO echange_doc_application_client (id, echange_doc_application_id, code, libelle, actif, cheminement_signature, cheminement_ged, cheminement_sae, cheminement_tdt, classification_1, classification_2, classification_3, classification_4, classification_5) VALUES (NULL, '3', 'PASTELL_V2_ACTES_01', 'Pastell – Flux ACTES (Modèle)', '1', '1', '1', '1', '1', 'SOUS_TYPE_PARAPHEUR', NULL, 'CC', '1.1', NULL);");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
