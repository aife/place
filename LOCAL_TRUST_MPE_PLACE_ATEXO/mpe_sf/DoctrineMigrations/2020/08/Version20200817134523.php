<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200817134523 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Ajout d\'une colonne temporaire \'original_mdp\' ' ;
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Administrateur ADD original_mdp VARCHAR(40) DEFAULT NULL");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE Administrateur DROP original_mdp");
    }
}
