<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200805125211 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-13260';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('ALTER TABLE `HabilitationProfil` ADD `espace_documentaire_consultation` BOOLEAN NOT NULL DEFAULT FALSE COMMENT \'habilitation a l espace documentaire\';');
        $this->addSql('UPDATE HabilitationProfil SET espace_documentaire_consultation = 1 WHERE id in (SELECT id FROM `HabilitationProfil` WHERE ouvrir_offre_en_ligne = \'1\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
