<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200824094636 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Modification de la methode get create download en post create download';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `web_service` SET method_ws = 'POST', `nom_ws` = 'post create download' 
WHERE `route_ws` = '/api/{version}/espace-documentaire/create-download/{idConsultation}.{format}'");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `web_service` SET method_ws = 'GET', `nom_ws` = 'get create download' 
WHERE `route_ws` = '/api/{version}/espace-documentaire/create-download/{idConsultation}.{format}'");
    }
}
