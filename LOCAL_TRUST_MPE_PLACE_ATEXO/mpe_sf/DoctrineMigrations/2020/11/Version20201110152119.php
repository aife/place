<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201110152119 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13919 - Ajouter des données dans la table document_template';
    }
    public function up(Schema $schema): void
    {
        $sql = 'INSERT INTO `document_template` (document, nom, nom_afficher) VALUES (:document, :nom, :nom_afficher)';
        foreach ($this->getData() as $data) {
            $this->addSql(
                $sql,
                ['document' => $data['document'], 'nom' => $data['nom'], 'nom_afficher' => $data['nom_afficher']]
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('TRUNCATE TABLE `document_template`');
    }

    /**
     * @return array
     */
    private function getData()
    {
        $files = [
            [
                'document' => 'OUV3',
                'nom_afficher' => 'OUV3 - Rapport d’analyse des candidatures',
                'nom' => 'RAC'
            ],
            [
                'document' => 'OUV4',
                'nom_afficher' => 'OUV4 - PV commission d’appel d’offres - Admission des candidatures',
                'nom' => 'PV_CAO_admission'
            ],
            [
                'document' => 'OUV5',
                'nom_afficher' => 'OUV5 - Admission des candidatures',
                'nom' => 'Admission_candidatures'
            ],
            [
                'document' => 'OUV6',
                'nom_afficher' => 'OUV6 - Demande de précisions ou de compléments sur l’offre',
                'nom' => 'Demande_precisions'
            ],
            [
                'document' => 'OUV7',
                'nom_afficher' => 'OUV7 - Réponse à la demande de précisions ou de compléments sur l’offre',
                'nom' => 'Reponse_precisions'
            ],
            [
                'document' => 'OUV8',
                'nom_afficher' => 'OUV8 - Rapport d’analyse des offres',
                'nom' => 'RAO'
            ],
            [
                'document' => 'OUV9',
                'nom_afficher' => 'OUV9 - PV de la commission d’appel d’offres - Décision d’attribution',
                'nom' => 'PV_CAO_attribution'
            ],
            [
                'document' => 'OUV10',
                'nom_afficher' => 'OUV10 - Décision d’attribution',
                'nom' => 'decision_attribution'
            ],
            [
                'document' => 'OUV11',
                'nom_afficher' => 'OUV11 - Mise au point',
                'nom' => 'mise_au_point'
            ],
            [
                'document' => 'NOTI1',
                'nom_afficher' => 'NOTI1 - Information au titulaire retenu',
                'nom' => 'information_titulaire'
            ],
            [
                'document' => 'NOTI3',
                'nom_afficher' => 'NOTI3 - Notification de rejet de candidature ou d’offre',
                'nom' => 'notification_rejet'
            ],
            [
                'document' => 'NOTI4',
                'nom_afficher' => 'NOTI4 - Rapport de présentation d’une consultation',
                'nom' => 'rapport_presentation'
            ],
            [
                'document' => 'NOTI5',
                'nom_afficher' => 'NOTI5 - Notification du marché public',
                'nom' => 'notification_titulaire'
            ],
            [
                'document' => 'NOTI7',
                'nom_afficher' => 'NOTI7 - Garantie à première demande',
                'nom' => 'garantie'
            ],
            [
                'document' => 'NOTI8',
                'nom_afficher' => 'NOTI8 - Caution personnelle et solidaire',
                'nom' => 'caution'
            ]
        ];
        return $files;
    }
}
