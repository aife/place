<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Envoi de la publicité à la validation de la consultation
 */
final class Version20201127174331 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-14236: envoi de la publicité à la validation de la consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation ADD is_envoi_publicite_validation TINYINT(1) DEFAULT \'0\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE consultation DROP is_envoi_publicite_validation');
    }
}
