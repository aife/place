<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201103062905 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13972 - Ajout de la configuration pour la ressource';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`, `updated_at`) VALUES
('URL_CENTRALE_REFERENTIELS', 'https://ressources.local-trust.com/', NOW()),
('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX', 'telechargements-rec/mon-assistant-marche-public-rec.AppImage', NOW()),
('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS', 'telechargements-rec/mon-assistant-marche-public-rec.dmg', NOW()),
('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS', 'telechargements-rec/mon-assistant-marche-public-rec.exe', NOW())");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("DELETE FROM `configuration_client` 
WHERE `configuration_client`.`parameter` in ('URL_CENTRALE_REFERENTIELS', 'LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX', 
'LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS','LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS');");
    }
}
