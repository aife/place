<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201104135051 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13973';
    }

    public function up(Schema $schema): void
    {

        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_OUTILS', 'docs/outils/deprecated/atexo')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_MODELE_IMPORT_LOTS', 'docs/modeles/atexo/Import Lots - Modele - v2019-1.4.0.xlsx')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE', 'docs/guides_utilisateurs/atexo/2020-00.01/GuideUtilisateurEntreprise.zip')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_GUIDE_AUTOFORMATION_ENTREPRISE', 'docs/guides_utilisateurs/atexo/2020-00.01/FilmsAutoformationEntreprise.zip')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_GUIDE', 'docs/guides_utilisateurs/atexo/2020-00.01/')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_FORMATION', 'formation')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES ('URL_DOCS_AUTOFORMATION', 'autoformation')");
        $this->addSql("INSERT INTO `configuration_client` (`parameter`, `value`) VALUES('URL_DOCS', 'https://ressources.local-trust.com/')");


    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_OUTILS'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_MODELE_IMPORT_LOTS'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_GUIDE_AUTOFORMATION_ENTREPRISE'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_GUIDE'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_FORMATION'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS_AUTOFORMATION'");
        $this->addSql("DELETE FROM  `configuration_client` WHERE `parameter` like 'URL_DOCS'");

    }
}
