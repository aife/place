<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201126201304 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14256 - Téléchargement du template de lot';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("UPDATE `configuration_client` SET `value`='/docs/modeles/Import Lots - Modele - v2019-1.4.0.xlsx' WHERE `parameter` = 'URL_DOCS_MODELE_IMPORT_LOTS'");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("UPDATE `configuration_client` SET `value`='docs/modeles/atexo/Import Lots - Modele - v2019-1.4.0.xlsx' WHERE `parameter` = 'URL_DOCS_MODELE_IMPORT_LOTS'");

    }
}
