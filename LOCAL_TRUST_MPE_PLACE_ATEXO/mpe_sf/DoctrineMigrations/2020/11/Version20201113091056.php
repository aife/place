<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201113091056 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-14087: Ajouter Token to entity PieceGenereConsultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `piece_genere_consultation` ADD `token` VARCHAR(500) NULL;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `piece_genere_consultation` DROP `token`;');
    }
}
