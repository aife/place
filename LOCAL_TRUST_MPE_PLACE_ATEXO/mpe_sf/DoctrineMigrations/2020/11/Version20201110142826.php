<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201110142826 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-13919- Création de latble document_template et piece_genere_consultation ';
    }
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('CREATE TABLE IF NOT EXISTS document_template (id INT AUTO_INCREMENT NOT NULL, document VARCHAR(50) NOT NULL, nom VARCHAR(50) NOT NULL, nom_afficher VARCHAR(100) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS piece_genere_consultation (id INT AUTO_INCREMENT NOT NULL, blob_id int(11) NOT NULL, consultation_id int(11) NOT NULL, created_at datetime NOT NULL, updated_at datetime DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE piece_genere_consultation ADD CONSTRAINT `genere_piece_consultation_consultation_id_fk` FOREIGN KEY(`consultation_id`) REFERENCES consultation(id)');
        $this->addSql('ALTER TABLE piece_genere_consultation ADD CONSTRAINT `genere_piece_consultation_blob_id_fk` FOREIGN KEY(`blob_id`) REFERENCES blobOrganisme_file(id)');
    }
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('DROP TABLE document_template');
        $this->addSql('DROP TABLE piece_genere_consultation');
    }
}
