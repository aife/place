<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706071457 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.02.00 - MPE-12859';
    }

    public function up(Schema $schema) : void
    {
        $this->addSQL('ALTER TABLE `Organisme` ADD `id_entite` INT NULL DEFAULT NULL AFTER `id_externe`;');
        $this->addSQL('ALTER TABLE `Service` ADD `id_entite` INT NULL DEFAULT NULL AFTER `synchronisation_exec`;');

    }

    public function down(Schema $schema) : void
    {
        $this->addSQl('alter table Organisme drop id_entite');
        $this->addSQl('alter table Service drop id_entite');
    }
}