<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200716190807 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Echange Documentaire - MPE-12705';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addsql('CREATE TABLE if not exists  echange_doc_type_piece_standard (
                                id INT AUTO_INCREMENT NOT NULL, 
                                code VARCHAR(20) NOT NULL, 
                                libelle VARCHAR(255) NOT NULL,
                                PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');

        $this->addsql('CREATE TABLE if not exists  echange_doc_type_piece_actes (
                                id INT AUTO_INCREMENT NOT NULL, 
                                code VARCHAR(20) NOT NULL, 
                                libelle VARCHAR(255) NOT NULL,
                                type_nature VARCHAR(20) NOT NULL, 
                                code_classification VARCHAR(20) NOT NULL,
                                PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addsql('DROP Table IF EXISTS echange_doc_type_piece_standard');
        $this->addsql('DROP Table IF EXISTS echange_doc_type_piece_actes');
    }
}
