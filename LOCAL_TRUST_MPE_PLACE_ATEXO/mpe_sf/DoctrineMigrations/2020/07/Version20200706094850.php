<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706094850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-12708';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE echange_doc_application_client ADD classification_1 varchar(255) DEFAULT null");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD classification_2 varchar(255) DEFAULT null");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD classification_3 varchar(255) DEFAULT null");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD classification_4 varchar(255) DEFAULT null");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD classification_5 varchar(255) DEFAULT null");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE echange_doc_application_client DROP classification_1");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP classification_2");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP classification_3");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP classification_4");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP classification_5");
    }
}
