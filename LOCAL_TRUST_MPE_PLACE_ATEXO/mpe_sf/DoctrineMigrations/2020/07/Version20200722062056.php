<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200722062056 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Echange Doc - MPE-12706';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE echange_doc ADD id_contrat_titulaire int(11) DEFAULT NULL");
        $this->addSql('ALTER TABLE echange_doc ADD CONSTRAINT FK_ECHANGE_DOC_CONTRAT_TITULAIRE FOREIGN KEY 
                           (id_contrat_titulaire) REFERENCES t_contrat_titulaire (id_contrat_titulaire)');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE echange_doc DROP FOREIGN KEY FK_ECHANGE_DOC_CONTRAT_TITULAIRE');
        $this->addSql("ALTER TABLE echange_doc DROP id_contrat_titulaire");
    }
}
