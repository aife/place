<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706081900 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-12709';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE echange_doc_application_client ADD cheminement_signature BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD cheminement_ged BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD cheminement_sae BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc_application_client ADD cheminement_tdt BOOLEAN DEFAULT false");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE echange_doc_application_client DROP cheminement_signature");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP cheminement_ged");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP cheminement_sae");
        $this->addSql("ALTER TABLE echange_doc_application_client DROP cheminement_tdt");
    }
}
