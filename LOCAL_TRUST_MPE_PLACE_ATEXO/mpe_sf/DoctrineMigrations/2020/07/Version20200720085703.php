<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200720085703 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Echange doc - MPE-12706';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE echange_doc ADD cheminement_signature BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc ADD cheminement_ged BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc ADD cheminement_sae BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc ADD cheminement_tdt BOOLEAN DEFAULT false");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE echange_doc DROP cheminement_signature");
        $this->addSql("ALTER TABLE echange_doc DROP cheminement_ged");
        $this->addSql("ALTER TABLE echange_doc DROP cheminement_sae");
        $this->addSql("ALTER TABLE echange_doc DROP cheminement_tdt");

    }
}
