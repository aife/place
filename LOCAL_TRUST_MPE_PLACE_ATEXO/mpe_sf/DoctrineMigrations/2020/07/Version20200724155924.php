<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200724155924 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE echange_doc_blob ADD echange_type_piece_actes_id int(11) DEFAULT NULL");
        $this->addSql("ALTER TABLE echange_doc_blob ADD echange_type_piece_standard_id int(11) DEFAULT NULL");
        $this->addSql('ALTER TABLE echange_doc_blob ADD CONSTRAINT FK_ECHANGE_TYPE_PIECE_STANDARD FOREIGN KEY 
                           (echange_type_piece_standard_id) REFERENCES echange_doc_type_piece_standard (id)');
        $this->addSql('ALTER TABLE echange_doc_blob ADD CONSTRAINT FK_ECHANGE_TYPE_PIECE_ACTES FOREIGN KEY 
                           (echange_type_piece_actes_id) REFERENCES echange_doc_type_piece_actes (id)');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE echange_doc_blob DROP FOREIGN KEY FK_ECHANGE_TYPE_PIECE_ACTES');
        $this->addSql('ALTER TABLE echange_doc_blob DROP FOREIGN KEY FK_ECHANGE_TYPE_PIECE_STANDARD');
        $this->addSql("ALTER TABLE echange_doc_blob DROP echange_type_piece_actes_id");
        $this->addSql("ALTER TABLE echange_doc_blob DROP echange_type_piece_actes_id");
    }
}
