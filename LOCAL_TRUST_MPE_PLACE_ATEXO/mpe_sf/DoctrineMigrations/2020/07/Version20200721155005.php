<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200721155005 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MULTI-URL MPE-13120';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("
CREATE TABLE IF NOT EXISTS `plateforme_virtuelle` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `domain` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `code_design` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;");

        $this->addSql("
CREATE TABLE IF NOT EXISTS `plateforme_virtuelle_organisme` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `plateforme_id` int(11) UNSIGNED NOT NULL,
  `organisme_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plateforme_id_fk` (`plateforme_id`),
  KEY `organisme_id_fk` (`organisme_id`)
) ENGINE=InnoDB;");


        $this->addSql("
ALTER TABLE `plateforme_virtuelle_organisme`
  ADD CONSTRAINT `organisme_id_fk` FOREIGN KEY IF NOT EXISTS (`organisme_id`) REFERENCES `Organisme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `plateforme_id_fk` FOREIGN KEY IF NOT EXISTS (`plateforme_id`) REFERENCES `plateforme_virtuelle` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
");

    }

    public function down(Schema $schema): void
    {
        $this->addSql("DROP TABLE plateforme_virtuelle");
        $this->addSql("DROP TABLE plateforme_virtuelle_organisme");

    }
}
