<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200723082825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.02.00 - MPE-12705';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("INSERT IGNORE INTO `echange_doc_type_piece_standard` (`id`, `code`, `libelle`) VALUES
(1, 'ARN', 'Accusé de Réception de Notification'),
(2, 'AE', 'Acte d\'Engagement'),
(3, 'AN', 'Annexes'),
(4, 'AL', 'Annonces Légales'),
(5, 'AU', 'Autres'),
(6, 'APD', 'Avant Projet Détaillé'),
(7, 'APS', 'Avant Projet Sommaire'),
(8, 'AC', 'Avis d\'appel à la Concurrence'),
(9, 'BPU', 'Bordereau des Prix Unitaires'),
(10, 'CCAP', 'Cahier des Clauses Administratives Particulières'),
(11, 'CCTP', 'Cahier des Clauses Techniques Particulières'),
(12, 'CA', 'Courrier d\'Attribution'),
(13, 'DPGF', 'Décomposition du Prix Global et Forfaitaire'),
(14, 'DQE', 'Détail Quantitatif Estimatif'),
(15, 'DR', 'Dossier de Réponse'),
(16, 'EC', 'Échange en cours de Consultation'),
(17, 'E', 'Étude'),
(18, 'LR', 'Lettre de Rejet'),
(19, 'LC', 'Liste des Candidatures'),
(20, 'P', 'Programme'),
(21, 'A0', 'Rapport d\'Analyse des Offres'),
(22, 'CAO', 'Rapport de Commission d\'Appel d\'Offres'),
(23, 'COP', 'Rapport de Commission d\'Ouverture des Plis'),
(24, 'RP', 'Rapport de Présentation'),
(25, 'RDP', 'Récépissé de dépôt de pli'),
(26, 'RC', 'Règlement de la Consultation')");

        $this->addSql("INSERT IGNORE INTO `echange_doc_type_piece_actes` (`id`, `code`, `libelle`, `type_nature`, `code_classification`) VALUES
(1, '11_AE', 'Acte d\'engagement', 'CC', '1.1'),
(2, '99_AU', 'Autre document', 'AU', '9.9'),
(3, '11_AC', 'Avis d\'appel public à concurrence', 'CC', '1.1'),
(4, '11_AV', 'Avis du jury de concours', 'CC', '1.1'),
(5, '11_BP', 'Bordereau des prix', 'CC', '1.1'),
(6, '11_AP', 'Cahier des clauses administratives particulières', 'CC', '1.1'),
(7, '11_TP', 'Cahier des clauses techniques particulières', 'CC', '1.1'),
(8, '10_DE', 'Délibération autorisant à passer le contrat', 'CC', '1.0'),
(9, '99_DC', 'Document contractuel', 'CC', '9.9'),
(10, '99_SE', 'Fichier de signature électronique', 'AU', '9.9'),
(11, '99_SE', 'Fichier de signature électronique', 'CC', '9.9'),
(12, '11_IN', 'Invitation des candidats à soumissionner', 'CC', '1.1'),
(13, '10_MT', 'Mémoire technique', 'CC', '1.0'),
(14, '10_AV', 'Modification du contrat', 'CC', '1.0'),
(15, '11_PV', 'Procès verbal de la commission d\'appel d\'offre ou du jury', 'CC', '1.1'),
(16, '11_RA', 'Rapport de la commission d\'appel d\'offre', 'CC', '1.1'),
(17, '11_RP', 'Rapport de présentation de l\'acheteur', 'CC', '1.1'),
(18, '11_JU', 'Rapport justifiant le choix du marché, les modalités et la procédure de passation', 'CC', '1.1'),
(19, '10_RD', 'Registre des dépôts des offres', 'CC', '1.0'),
(20, '10_RC', 'Règlement de la consultation', 'CC', '1.0'),
(21, '10_AT', 'Renseignements, attestations et déclarations fournies par l\'attributaire', 'CC', '1.0')");


    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("TRUNCATE echange_doc_type_piece_actes");
        $this->addSql("TRUNCATE echange_doc_type_piece_standard");
    }
}
