<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200706150553 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-12708';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("ALTER TABLE echange_doc_application ADD flux_actes BOOLEAN DEFAULT false");
        $this->addSql("ALTER TABLE echange_doc_application ADD primo_signature BOOLEAN DEFAULT false");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql("ALTER TABLE echange_doc_application DROP flux_actes");
        $this->addSql("ALTER TABLE echange_doc_application DROP primo_signature");
    }
}
