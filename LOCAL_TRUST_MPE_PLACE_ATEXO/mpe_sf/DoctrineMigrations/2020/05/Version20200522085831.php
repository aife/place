<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20200522085831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.01.00 - MPE-12572';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("UPDATE `mail_template` SET `reponse_attendue` = '1' WHERE `mail_template`.`id` = 6");
        $this->addSql("UPDATE `mail_template` SET `envoi_modalite_figee` = '1' WHERE `mail_template`.`id` = 8");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("UPDATE `mail_template` SET `reponse_attendue` = '0' WHERE `mail_template`.`id` = 6");
        $this->addSql("UPDATE `mail_template` SET `envoi_modalite_figee` = '0' WHERE `mail_template`.`id` = 8");
    }
}
