<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);
namespace App\Migrations;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504125257 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 - MPE-12471';
    }

    /**
     * @param $queryCount
     * @param $query
     * @param int $offset
     * @throws DBALException
     */
    public function doQueries($queryCount, $query, $offset = 100)
    {
        $res = $this->connection->prepare($queryCount);
        $res->execute();
        $max = $res->fetch();
        $maxTable = (int)$max['MAX'];
        $i = 1;
        do {
            $limit = ['min' => $i, 'max' => $i + $offset];
            $this->addSql($query, $limit);
            $i = $i + $offset;
        } while ($i <= $maxTable);
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("alter table Offres ADD KEY IF NOT EXISTS offres_id_pdf_echange_accuse_20210607 (id_pdf_echange_accuse);");
        $this->addSql("alter table blocFichierEnveloppe ADD KEY IF NOT EXISTS blocFichierEnveloppe_id_blob_chiffre_20210607 (id_blob_chiffre);");


        $this->addSql("ANALYZE TABLE blobOrganisme_file;");
        $this->addSql("ANALYZE TABLE Offres;");

        $queryCount = "SELECT Max(id_fichier) AS MAX FROM fichierEnveloppe;";
        $query = "UPDATE `fichierEnveloppe` f INNER JOIN `blobOrganisme_file` b ON f.id_blob = b.old_id and f.organisme = b.organisme SET f.id_blob = b.id WHERE f.id_blob IS NOT NULL AND f.id_fichier >=  :min  AND f.id_fichier < :max ;";
        $this->doQueries($queryCount, $query);
        $queryCount = "SELECT Max(id) AS MAX FROM Offres;";
        $query = "UPDATE `Offres` o INNER JOIN `blobOrganisme_file` b ON o.id_pdf_echange_accuse = b.old_id and o.organisme=b.organisme SET o.id_pdf_echange_accuse = b.id WHERE o.id_pdf_echange_accuse IS NOT NULL AND o.id >=  :min  AND o.id < :max ;";
        $this->doQueries($queryCount, $query);
        $query = "UPDATE `Offres` o INNER JOIN `blobOrganisme_file` b ON o.id_blob_horodatage_hash = b.old_id  and o.organisme=b.organisme SET  o.id_blob_horodatage_hash = b.id WHERE  o.id_blob_horodatage_hash IS NOT NULL AND o.id >=  :min  AND o.id < :max ;";
        $this->doQueries($queryCount, $query);
        $queryCount = "SELECT Max(id) AS MAX FROM DCE;";
        $query = "UPDATE DCE d INNER JOIN `blobOrganisme_file` b ON d.dce = b.old_id and d.organisme=b.organisme SET d.`dce` = b.id WHERE d.dce IS NOT NULL AND d.id >=  :min  AND d.id < :max ;";
        $this->doQueries($queryCount, $query);
        $queryCount = "SELECT Max(id_fichier) AS MAX FROM blocFichierEnveloppe;";
        $query = "UPDATE blocFichierEnveloppe f INNER JOIN `blobOrganisme_file` b ON f.id_blob_chiffre = b.old_id and f.organisme=b.organisme SET f.id_blob_chiffre = b.id WHERE f.id_blob_chiffre IS NOT NULL AND f.id_fichier >=  :min  AND f.id_fichier < :max ;";
        $this->doQueries($queryCount, $query);

        $this->addSql("UPDATE dossier_volumineux d INNER JOIN blob_file b ON d.id_blob_descripteur = b.old_id SET d.id_blob_descripteur = b.id WHERE d.id_blob_descripteur IS NOT NULL");
        $this->addSql("UPDATE dossier_volumineux d INNER JOIN blob_file b ON d.id_blob_logfile = b.old_id SET d.id_blob_logfile = b.id WHERE d.id_blob_logfile IS NOT NULL");
        $this->addSql("UPDATE DocumentExterne d INNER JOIN blobOrganisme_file b ON d.idBlob = b.old_id and d.organisme=b.organisme SET d.idBlob = b.id WHERE d.idBlob IS NOT NULL");
        $this->addSql("UPDATE Complement c INNER JOIN blobOrganisme_file b ON c.complement = b.old_id and c.organisme=b.organisme SET c.`complement` = b.id WHERE c.complement IS NOT NULL");
        $this->addSql("UPDATE RG r INNER JOIN blobOrganisme_file b ON r.rg = b.old_id and r.organisme=b.organisme SET r.`rg` = b.id WHERE r.`rg` IS NOT NULL");
        $this->addSql("UPDATE questions_dce q INNER JOIN blobOrganisme_file b ON q.id_fichier = b.old_id and q.organisme=b.organisme SET q.`id_fichier` = b.id WHERE q.id_fichier IS NOT NULL");
        $this->addSql("UPDATE Autres_Pieces_Mise_Disposition a INNER JOIN blobOrganisme_file b ON a.id_blob = b.old_id and a.org=b.organisme SET a.`id_blob` = b.id WHERE a.id_blob IS NOT NULL");
        $this->addSql("UPDATE EchangePieceJointe e INNER JOIN blobOrganisme_file b ON e.piece = b.old_id  and e.organisme=b.organisme SET e.piece = b.id WHERE e.piece IS NOT NULL");
        $this->addSql("UPDATE `Annonce_Press_PieceJointe` a INNER JOIN blobOrganisme_file b ON a.piece = b.old_id and a.organisme=b.organisme SET a.piece = b.id WHERE a.piece IS NOT NULL");
        $this->addSql("UPDATE `AVIS` a INNER JOIN blobOrganisme_file b ON a.avis = b.old_id and a.organisme=b.organisme SET a.avis = b.id WHERE a.avis IS NOT NULL");
        $this->addSql("UPDATE `t_document` d INNER JOIN blobOrganisme_file b ON d.id_fichier = b.old_id and d.organisme=b.organisme SET d.id_fichier = b.id WHERE d.id_fichier IS NOT NULL");

        $this->addSql("UPDATE `t_entreprise_document_version` e SET id_blob =
                           (SELECT id FROM blob_file b WHERE e.id_blob = b.old_id OR (e.id_blob = b.id AND b.old_id is null))
                           WHERE e.id_blob IS NOT NULL;");
        $this->addSql("UPDATE `blocFichierEnveloppeTemporaire` f SET `id_blob` =
                           ( SELECT id FROM blobOrganisme_file b
                           WHERE f.id_blob = b.old_id  and f.organisme=b.organisme)
                           WHERE f.id_blob IS NOT NULL;");
        $this->addSql("UPDATE `Chorus_Fiche_Navette` c SET id_document =
                           (SELECT id FROM blobOrganisme_file b WHERE (c.id_document = b.old_id OR (c.id_document = b.id AND b.old_id is null)) AND c.organisme = b.organisme)
                           WHERE c.id_document IS NOT NULL;");
        $this->addSql("UPDATE `decisionEnveloppe` d SET `id_blob_pieces_notification` =
                           (SELECT id FROM blobOrganisme_file b WHERE (d.id_blob_pieces_notification = b.old_id OR (d.id_blob_pieces_notification = b.id AND b.old_id is null)) AND d.organisme = b.organisme)
                           WHERE d.id_blob_pieces_notification != '';");
        $this->addSql("UPDATE `decisionEnveloppe` d SET `fichier_joint` =
                           (SELECT id FROM blobOrganisme_file b WHERE (d.fichier_joint = b.old_id OR (d.fichier_joint = b.id AND b.old_id is null)) AND d.organisme=b.organisme)
                           WHERE d.fichier_joint IS NOT NULL;");
        $this->addSql("UPDATE `Societes_Exclues` s SET `id_blob` =
                           (SELECT id FROM blobOrganisme_file b WHERE s.id_blob = b.old_id OR (s.id_blob = b.id AND b.old_id is null))
                           WHERE s.id_blob IS NOT NULL;");
        $this->addSql("UPDATE `fichiers_liste_marches` f SET `fichier`=
                           (SELECT id FROM blobOrganisme_file b WHERE (f.fichier = b.old_id OR (f.fichier = b.id AND b.old_id is null)) and f.organisme = b.organisme)
                           WHERE f.fichier IS NOT NULL;");
        $this->addSql("UPDATE `programme_previsionnel` p SET `fichier` =
                           (SELECT id FROM blobOrganisme_file b WHERE (p.fichier = b.old_id OR (p.fichier = b.id AND b.old_id is null)) AND p.organisme = b.organisme)
                           WHERE p.fichier IS NOT NULL;");
        $this->addSql("UPDATE `t_synthese_rapport_audit` s SET `fichier` =
                           (SELECT id FROM blobOrganisme_file b WHERE (s.fichier = b.old_id OR (s.fichier = b.id AND b.old_id is null)) AND s.organisme = b.organisme)
                           WHERE s.fichier IS NOT NULL;");
        $this->addSql("UPDATE `AdresseFacturationJal` a SET `id_blob_logo` =
                           (SELECT id FROM blobOrganisme_file b WHERE (a.id_blob_logo = b.old_id OR (a.id_blob_logo = b.id AND b.old_id is null)) AND a.organisme = b.organisme)
                           WHERE a.id_blob_logo IS NOT NULL;");
        $this->addSql("UPDATE `Chorus_pj` c SET `fichier` =
                           (SELECT id FROM blobOrganisme_file b WHERE (c.fichier = b.old_id OR (c.fichier = b.id AND b.old_id is null)) AND c.organisme = b.organisme)
                           WHERE c.fichier IS NOT NULL;");
        $this->addSql("UPDATE `AnnonceJALPieceJointe` a SET piece =
                           (SELECT id FROM blobOrganisme_file b WHERE (a.piece = b.old_id OR (a.piece = b.id AND b.old_id is null)) AND a.organisme = b.organisme)
                           WHERE a.piece IS NOT NULL;");
        $this->addSql("UPDATE `t_candidature_mps` c SET id_blob =
                           (SELECT id FROM blobOrganisme_file b WHERE (c.id_blob = b.old_id OR (c.id_blob = b.id AND b.old_id is null)) AND c.organisme = b.organisme)
                           WHERE c.id_blob IS NOT NULL;");
        $this->addSql("UPDATE `T_Illustration_Fond` i SET `id_blob_image` =
                           (SELECT id from blob_file b where i.id_blob_image = b.old_id OR (i.id_blob_image = b.id AND b.old_id is null))
                           WHERE i.id_blob_image IS NOT NULL;");
        $this->addSql("UPDATE `Entreprise` e SET documents_commerciaux =
                           (SELECT id FROM blob_file b WHERE e.documents_commerciaux = b.old_id OR (e.documents_commerciaux = b.id AND b.old_id is null))
                           WHERE e.documents_commerciaux IS NOT NULL;");
        $this->addSql("UPDATE `Justificatifs` j SET justificatif =
                           ( SELECT id FROM blob_file b WHERE j.justificatif = b.old_id OR (j.justificatif = b.id AND b.old_id is null))
                           WHERE j.justificatif IS NOT NULL;");
        $this->addSql("UPDATE t_dume_numero dn set dn.blob_id =
                           (SELECT b.id FROM `t_dume_contexte` dc, blobOrganisme_file b WHERE (dn.blob_id = b.old_id OR (dn.blob_id = b.id AND b.old_id is null)) 
                                                                                          AND dc.organisme = b.organisme AND dc.id = dn.id_dume_contexte)
                           WHERE dn.blob_id IS NOT NULL;");

        $this->addSql(
            "ALTER TABLE Offres ADD CONSTRAINT FK_Offres_blobOrganismeFile 
    FOREIGN KEY IF NOT EXISTS  (id_blob_horodatage_hash) REFERENCES blobOrganisme_file (id);"
        );

        /**
         * Mise a jour des id_blob_xml_reponse par rapport au nouveau ID pour pouvoir faire un FK
         */
        $query = "UPDATE `Offres` o INNER JOIN `blobOrganisme_file` b ON o.id_blob_xml_reponse = b.old_id  
    and o.organisme=b.organisme SET  o.id_blob_xml_reponse = b.id 
WHERE  o.id_blob_xml_reponse IS NOT NULL AND o.id >=  :min  AND o.id < :max ;";
        $this->doQueries($queryCount, $query);

        # table fichierEnveloppe
        # ajout clé externe vers la table blobOrganisme_file
        $this->addSql(
            'ALTER TABLE fichierEnveloppe ADD CONSTRAINT FK_fichierEnveloppe_blobOrganismeFile 
    FOREIGN KEY IF NOT EXISTS (id_blob) REFERENCES blobOrganisme_file (id) ON DELETE NO ACTION;'
        );

        $this->addSql(
            'ALTER TABLE Offres ADD CONSTRAINT FK_Offres_blobOrganismeFile 
    FOREIGN KEY IF NOT EXISTS  (id_blob_horodatage_hash) REFERENCES blobOrganisme_file (id) ON DELETE NO ACTION;'
        );

        $this->addSql(
            'ALTER TABLE `Offres` ADD CONSTRAINT `offres_id_blob_xml_reponse`
    FOREIGN KEY IF NOT EXISTS  ( `id_blob_xml_reponse` ) REFERENCES `blobOrganisme_file` (`id`) ON DELETE NO ACTION;'
        );
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Les données modifiées ne peuvent pas être retrouvées pour une opération inverse.');
    }
}
