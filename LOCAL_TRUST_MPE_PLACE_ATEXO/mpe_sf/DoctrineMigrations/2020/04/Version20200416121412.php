<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416121412 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 MPE-1848';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql(
            "INSERT INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) 
VALUES ( '/api/{version}/echange-documentaire/echange.{format}', 'GET', 'get échanges documentaires')"
        );
        $this->addSql('ALTER TABLE echange_doc_blob ADD poids int(11) DEFAULT NULL ');
        $this->addSql('ALTER TABLE echange_doc_blob ADD checksum char(32) DEFAULT NULL ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` 
where `route_ws` = '/api/{version}/echange-documentaire/echange.{format}' 
AND `method_ws` = 'GET'");
        $this->addSql('ALTER TABLE echange_doc_blob DROP poids');
        $this->addSql('ALTER TABLE echange_doc_blob DROP checksum');
    }
}
