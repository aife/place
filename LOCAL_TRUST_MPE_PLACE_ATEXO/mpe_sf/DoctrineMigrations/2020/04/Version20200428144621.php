<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200428144621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 MPE-11850';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) 
                           VALUES ('/api/{version}/echange-documentaire/update-statut-echange.{format}', 
                           'PUT', 'update statut échange', '0');
                      ");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` 
                           where `route_ws` = '/api/{version}/echange-documentaire/update-statut-echange.{format}' 
                          AND `method_ws` = 'PUT'");
    }
}
