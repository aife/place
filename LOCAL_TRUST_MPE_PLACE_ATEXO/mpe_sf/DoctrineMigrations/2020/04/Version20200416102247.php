<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416102247 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.02.00 MPE-12337';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('CREATE TABLE echange_doc_historique (id INT AUTO_INCREMENT NOT NULL, echange_doc_id INT DEFAULT NULL,  agent_id INT DEFAULT NULL, statut VARCHAR(50) NOT NULL, created_at datetime NOT NULL, message_fonctionnel varchar(500) DEFAULT NULL, message_technique varchar(1000) DEFAULT NULL , INDEX IDX_echange_doc_historique_id (id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE echange_doc_historique ADD CONSTRAINT FK_ECHANGE_DOC_HISTORIQUE_AGENT FOREIGN KEY (agent_id) REFERENCES Agent (id)');
        $this->addSql('ALTER TABLE echange_doc_historique ADD CONSTRAINT FK_ECHANGE_DOC_HISTORIQUE_ENCHANGE_DOC FOREIGN KEY (echange_doc_id) REFERENCES echange_doc (id)');
        $this->addSql('ALTER TABLE echange_doc MODIFY statut varchar(50)');
        $this->addSql('ALTER TABLE echange_doc ADD updated_at datetime DEFAULT NULL ');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE echange_doc_historique DROP FOREIGN KEY FK_ECHANGE_DOC_HISTORIQUE_AGENT');
        $this->addSql('ALTER TABLE echange_doc_historique DROP FOREIGN KEY FK_ECHANGE_DOC_HISTORIQUE_ENCHANGE_DOC');
        $this->addSql('DROP TABLE echange_doc_historique');
        $this->addSql('ALTER TABLE echange_doc MODIFY statut varchar(255)');
        $this->addSql('ALTER TABLE echange_doc DROP updated_at');
    }
}
