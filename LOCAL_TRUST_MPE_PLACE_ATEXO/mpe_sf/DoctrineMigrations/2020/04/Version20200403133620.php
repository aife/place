<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200403133620 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Version 2020-00.02.00 MPE-12251';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/entreprises.{format}', 'POST', 'post entreprises')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/entreprises.{format}', 'PUT', 'put entreprises')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/rc/{idConsultation}.{format}', 'GET', 'get espacedocumentaire rc',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/dce/autres-pieces/{idConsultation}.{format}', 'GET', 'get espacedocumentaire dce autres pieces',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/plis/{idConsultation}.{format}', 'GET', 'get espacedocumentaire plis',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/dce/{idConsultation}.{format}', 'GET', 'get dce',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/dume/dume-acheteur/{idConsultation}.{format}', 'GET', 'get dume acheteur',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/pieces-du-depots/{idOffre}/{idConsultation}.{format}', 'GET', 'get dume enveloppe',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/publicite/{idConsultation}.{format}', 'GET', 'get publicite',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}', 'GET', 'get autres pieces consultation',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}', 'POST', 'post autres pieces consultation',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}/{blobId}.{format}', 'DELETE', 'delete autres pieces consultation',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/plis-attributaires/{idConsultation}.{format}', 'GET', 'get plis attributaires',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/pieces-plis-attributaires/{idOffre}/{idConsultation}.{format}', 'GET', 'get pieces plis attributaires',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/espace-documentaire/create-download/{idConsultation}.{format}', 'GET', 'get create download',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/etablissements.{format}', 'POST', 'post etablissements')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/etablissements.{format}', 'PUT', 'put etablissements')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/inscrits.{format}', 'GET', 'get inscrits')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/inscrits.{format}', 'POST', 'post inscrits')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/inscrits.{format}', 'PUT', 'put inscrits')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES ( '/api/{version}/messagerie/templates.{format}', 'GET', 'get messagerie template',0)");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/organismes.{format}', 'POST', 'post organismes')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/organismes.{format}', 'PUT', 'put organismes')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/dossier-volumineux/upload/logfile/{uuidTechnique}/{HASHMD5}/{ticket}', 'POST', 'post logfile envol')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/dossier-volumineux/upload/descripteurfile/{uuidTechnique}/{HASHMD5}/{ticket}', 'POST', 'post descripteur envol')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/dossier-volumineux/upload/metadata/{uuidTechnique}/{ticket}', 'POST', 'post metadata envol')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/dossier-volumineux/upload/', 'POST', 'post update envol')");
        $this->addSql("INSERT IGNORE INTO  `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( '/api/{version}/dossier-volumineux/event/{uuidReference}', 'POST', 'post event envol')");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/entreprises.{format}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/entreprises.{format}' AND `method_ws` = 'PUT'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/rc/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/dce/autres-pieces/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/plis/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/dce/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/dume/dume-acheteur/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/pieces-du-depots/{idOffre}/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/publicite/{idConsultation}.{format}' AND `method_ws` = 'GET' ");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}.{format}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/autres-pieces-consultation/{idConsultation}/{blobId}.{format}' AND `method_ws` = 'DELETE'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/plis-attributaires/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/pieces-plis-attributaires/{idOffre}/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/espace-documentaire/create-download/{idConsultation}.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/etablissements.{format}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/etablissements.{format}' AND `method_ws` = 'PUT'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/inscrits.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/inscrits.{format}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/inscrits.{format}' AND `method_ws` = 'PUT'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/messagerie/templates.{format}' AND `method_ws` = 'GET'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/organismes.{format}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/organismes.{format}' AND `method_ws` = 'PUT' ");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/dossier-volumineux/upload/logfile/{uuidTechnique}/{HASHMD5}/{ticket}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/dossier-volumineux/upload/descripteurfile/{uuidTechnique}/{HASHMD5}/{ticket}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/dossier-volumineux/upload/metadata/{uuidTechnique}/{ticket}' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/dossier-volumineux/upload/' AND `method_ws` = 'POST'");
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/dossier-volumineux/event/{uuidReference}' AND `method_ws` = 'POST'");
        


    }
}
