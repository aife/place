<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200422131435 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Version 2020-00.02.00 MPE-11849';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`)
                           VALUES ( '/api/{version}/echange-documentaire/document.{format}', 'GET', 'get document')");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` 
                           where `route_ws` = '/api/{version}/echange-documentaire/document.{format}' 
                          AND `method_ws` = 'GET'");
    }
}
