<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210520091055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16290 - [RdR 2021-00.04.00] Retours sur la modification de la gestion des infos de contact de l\'agent';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE t_publicite_favoris ADD IF NOT EXISTS acheteur_telephone varchar(20) null;');
        $this->addSql('ALTER TABLE t_publicite_favoris ADD IF NOT EXISTS acheteur_email varchar(100) null;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `t_publicite_favoris` DROP `acheteur_telephone`;');
        $this->addSql('ALTER TABLE `t_publicite_favoris` DROP acheteur_email;');
    }
}
