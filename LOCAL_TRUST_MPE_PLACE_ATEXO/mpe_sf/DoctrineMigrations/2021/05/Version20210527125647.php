<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210527125647 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-15645 - Générer une ONR avec les informations issues de l\'analyse des offres';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE piece_genere_consultation DROP CONSTRAINT genere_piece_consultation_blob_id_fk');
        $this->addSql('ALTER TABLE piece_genere_consultation ADD CONSTRAINT `genere_piece_consultation_blob_id_fk` FOREIGN KEY(`blob_id`) REFERENCES blobOrganisme_file(id) ON DELETE CASCADE ON UPDATE CASCADE');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE piece_genere_consultation DROP CONSTRAINT genere_piece_consultation_blob_id_fk');
        $this->addSql('ALTER TABLE piece_genere_consultation ADD CONSTRAINT `genere_piece_consultation_blob_id_fk` FOREIGN KEY(`blob_id`) REFERENCES blobOrganisme_file(id)');
    }
}
