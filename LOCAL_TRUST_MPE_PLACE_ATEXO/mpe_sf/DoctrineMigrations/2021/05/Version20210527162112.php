<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210527162112 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16399 - Ajout relation parent/enfant echange doc blob principal/annexes';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `echange_doc_blob` ADD `doc_blob_principal_id` INT(11) NULL DEFAULT NULL COMMENT 'Id echange doc principal';");
        $this->addSql("ALTER TABLE `echange_doc_blob` ADD CONSTRAINT `FK_ECHANGE_DOC_BLOB_PRINCIPAL` FOREIGN KEY (`doc_blob_principal_id`) REFERENCES `echange_doc_blob`(`id`);");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `echange_doc_blob` DROP FOREIGN KEY FK_ECHANGE_DOC_BLOB_PRINCIPAL;");
        $this->addSql("ALTER TABLE `echange_doc_blob` DROP `doc_blob_principal_id`;");
    }
}
