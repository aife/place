<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210507144053
 * @package App\Migrations
 */
final class Version20210507144053 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15835 - [PLACE] RdR 2021-02.10.00 - PKCS#7 - Le message dans la pop-in pré-dépôt n\'est pas celui de la SFD';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("UPDATE configuration_messages_traduction SET target = REPLACE(REPLACE(target, '2 .P', '2. P'), 'comprennant', 'comportant') WHERE source = 'INFORMATIONS_IMPORTANTE_TAILLE_DEPOT' ;");
    }
}
