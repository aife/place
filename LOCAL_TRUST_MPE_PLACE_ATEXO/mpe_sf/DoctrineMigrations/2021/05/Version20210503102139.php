<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503102139 extends AbstractMigration
{
    public function getDescription(): string
    {
        return "MPE-16095 - [Annexes fin.] Espace doc - Ajout de l'annexe financière comme pièce disponible à la génération";
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            'mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("INSERT INTO `document_template` (document, nom, nom_afficher) VALUES ('AF', 'annexe_financiere' , 'AF - Annexe financière (DPGF - BPU - DQE)')");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            'mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("DELETE FROM `document_template` where `document` = 'AF'");
    }
}
