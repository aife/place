<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503085233 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16097 - [Annexes fin.] Paramétrage types proc. - Gestion de la pièce typée Annexe financière au niveau de l\'enveloppe d\'offre';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `consultation` ADD `annexe_financiere` TINYINT(1) NULL DEFAULT \'0\' AFTER `is_envoi_publicite_validation`;');
        $this->addSql('ALTER TABLE `ProcedureEquivalence` ADD `annexe_financiere` 	char(2) NOT NULL AFTER `signature_autoriser`;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `consultation` DROP `annexe_financiere`;');
        $this->addSql('ALTER TABLE `ProcedureEquivalence` DROP `annexe_financiere`;');
    }
}
