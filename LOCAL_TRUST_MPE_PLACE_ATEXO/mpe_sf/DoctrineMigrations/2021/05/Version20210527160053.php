<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210527160053 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16231 - Modification de la page "Mentions légales"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
            VALUES (NULL, 'TEXT_DPO', '[Renseigner les informations de contact du délégué à la protection des données]', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
            VALUES (NULL, 'TEXT_SITE_PROPRIETE', 'Atexo', '1')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `configuration_messages_traduction` 
            WHERE `configuration_messages_traduction`.`source` IN ('TEXT_DPO', 'TEXT_SITE_PROPRIETE')");
    }
}
