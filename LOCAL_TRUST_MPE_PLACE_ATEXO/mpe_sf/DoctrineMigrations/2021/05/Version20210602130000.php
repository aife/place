<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210602130000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16481 - Oauth2 Openid Keycloak';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('Alter table configuration_plateforme ADD COLUMN IF NOT EXISTS authenticate_agent_openid_keycloak varchar(1) DEFAULT 0;');
        $this->addSql('Alter table configuration_plateforme ADD COLUMN IF NOT EXISTS authenticate_inscrit_openid_keycloak varchar(1) DEFAULT 0;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('ALTER TABLE configuration_plateforme DROP COLUMN authenticate_agent_openid_keycloak;');
        $this->addSql('ALTER TABLE configuration_plateforme DROP COLUMN authenticate_inscrit_openid_keycloak;');
    }
}
