<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210503141255 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16100 - [Annexes fin.] Réponse entreprise - Gestion de la pièce typée "Annexe financière"';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'DEFINE_ANNEXE_FINANCIER', 'Annexe financière (DPGF, BPU/DQE, au format .xlsx)', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'WARNING_PIECE_NON_RENSEIGNER', 'Attention : au moins une pièce n\'a pas été renseignée dans le champ dédié à cet effet :', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'ACTE_ENGAGEMENT_TEXT', 'Acte d\'engagement', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'ANNEXE_FINANCIERE_TEXT', 'Annexe financière', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'COMPLETUDE_REPONSE_TEXT', 'Assurez-vous de la complétude de votre réponse avant de la soumettre.', '1')");

        $this->addSql("INSERT INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'PIECE_NON_RENSEIGNER', 'Pièce non renseignée', '1')");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `configuration_messages_traduction` 
WHERE `configuration_messages_traduction`.`source` 
          in ('DEFINE_ANNEXE_FINANCIER', 'WARNING_PIECE_NON_RENSEIGNER', 'ACTE_ENGAGEMENT_TEXT', 'COMPLETUDE_REPONSE_TEXT', 'PIECE_NON_RENSEIGNER')");
    }
}
