<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210520143424 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16077 - Evolution configuration de la publicité';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE `ProcedureEquivalence` SET `autoriser_publicite` = '1'");
    }
}
