<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210519170031
 * @package App\Migrations
 */
final class Version20210519170031 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15949 - CD01 - ALPI-RECIA: impossible de créer un compte boamp en production';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE `configuration_client` SET `parameter` = 'PARAMETRAGE_PLATE_FORM_COMPTE_BOAMP' WHERE `configuration_client`.`parameter` = 'PARAMETRAGE_PLATE-FORM_COMPTE_BOAMP';");
    }
}
