<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210531134451 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16418 - Accès page de dépôt KO - Annexe financière non setté dans les consultations simplifiées';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `consultation` CHANGE `annexe_financiere` `annexe_financiere` TINYINT(1) NULL DEFAULT '0';");
        $this->addSql("UPDATE `consultation` SET `annexe_financiere` = 0 WHERE `annexe_financiere` IS NULL");
    }
}
