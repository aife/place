<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210505143322 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16178 -  [PLACE] RdR 2021-02.01.00 - Caractère "Entité Chorus" non pris en compte à la création d\'une entité';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql('UPDATE `configuration_messages_traduction` SET `target` = "Les termes des présentes conditions générales d\'utilisation peuvent être amendés à tout moment, sans préavis, en fonction des modifications apportées aux fonctionnalités, de l\'évolution de la réglementation ou pour tout autre motif jugé nécessaire. Il appartient à l\'utilisateur de s\'informer des conditions générales d\'utilisation de la plateforme PLACE en vigueur. <br/<br/> L\'utilisation de PLACE nécessite de disposer d\'un environnement informatique compatible et de se conformer aux <a href=\'/?page=entreprise.PrerequisTechniques&calledFrom=entreprise\'>prérequis techniques</a>.
<h3>Avertissement et recommandation aux entreprises</h3>
    Les questions ou demandes d’informations complémentaires relatives à une consultation peuvent être posées via le profil d’acheteur ou, le cas échéant, à une adresse mail mentionnée par l’acheteur dans les documents de la consultation.
<br/><br/>
Afin de conserver une trace des échanges, il est recommandé de recourir au service de messagerie du profil d’acheteur. Si un opérateur économique, après avoir pris connaissance de l’ensemble des documents de la consultation, pose une question et que la réponse a un impact sur la remise des candidatures ou des offres, la réponse est communiquée à tous les opérateurs économiques ayant téléchargé les documents de consultation avec authentification, et sera visible de tous les opérateurs économiques n’ayant pas encore téléchargé les documents.
<br/><br/>
Les plis doivent être reçus dans les délais fixés dans l’avis de publicité ou dans les autres documents de la consultation.
<br/><br/>
L’opérateur économique doit donc prévoir le temps nécessaire pour que son pli soit reçu dans le délai fixé par l’acheteur, surtout si les fichiers sont volumineux et/ou si le réseau a un faible débit. Il lui est ainsi conseillé de déposer ses plis au minimum quatre heures avant la date limite de remise des plis afin de garantir son envoi dématérialisé et lui permettre de remédier à d’éventuels problèmes techniques.
<br/><br/>
Par ailleurs, les opérateurs économiques sont alertés que la plateforme déconnecte automatiquement l’utilisateur en cas d’inactivité supérieure à trente minutes.
<br/><br/>
Attention, les plis, dont le téléchargement a commencé avant la date et l’heure limite mais s’est achevé hors délai sont éliminés par l’acheteur.
<br/><br/>
L’opérateur économique est invité à remettre une copie de sauvegarde dans les conditions fixées dans le règlement de la consultation afin d’anticiper toute difficulté de dernière minute. En effet, en cas de réception d’un pli électronique hors délai, l’acheteur est tenu d’ouvrir la copie de sauvegarde régulièrement transmise.
<h3>Certificat électronique</h3>
    Un certificat électronique permet de prouver son identité et de signer électroniquement les offres avec la même valeur juridique qu\'une signature manuscrite.
<br/><br/>
L’annexe 12 du code de la commande publique relatif à la signature électronique des contrats de la commande publique prévoit que le certificat doit être qualifié conformément au règlement n° 910/2014 du parlement européen et du conseil du 23 juillet 2014 sur l’identification électronique et les services de confiance pour les transactions électroniques au sein du marché intérieur et abrogeant la directive 1999/93/CE dit « eIDAS ».
<br/><br/>
Pour l\'obtention d\'un certificat électronique, la liste des prestataires de services de confiance qualifiés délivrant des certificats de signatures conformes à la réglementation est accessible sur la page des <a href=\'/?page=entreprise.ListeAcRGS&calledFrom=entreprise\'>autorités de certification européennes</a>.
<h3>Assistance en ligne</h3>
Un service de support est à votre disposition de 9h00 à 19h00 les jours ouvrés pour répondre à vos questions ou vous assister en cas de difficultés (consulter la page dédiée). L’assistance en ligne est accessible depuis chaque page via la languette située à droite de l’écran."
        WHERE `configuration_messages_traduction`.`source` = "TEXT_APPROBATION_APPLICATION" and `langue_id` = 1;');

        $this->addSql('UPDATE `configuration_messages_traduction` 
SET `target` = "<h3>Engagements et responsabilité</h3> La sécurité des données collectées auprès de l\'utilisateur est garantie par le profil d’acheteur, 
les solutions adéquates ont été mises en œuvre pour empêcher qu\'elles soient modifiées, endommagées ou que des tiers non autorisés y aient accès. 
<br/><br/> L\'utilisateur de PLACE s\'engage à fournir des informations exactes, à jour et complètes. 
Dans l\'hypothèse où l\'utilisateur ne s\'acquitterait pas de cet engagement, le Ministère de l’Action et des Comptes publics se réserve le droit de 
suspendre ou de résilier l’accès aux fonctionnalités. D’éventuelles actions en responsabilité pénale et civile pourraient être engagées à son encontre. 
<br/><br/> Il est rappelé que toute personne procédant à une fausse déclaration pour elle-même ou pour autrui s\'expose, notamment, aux sanctions prévues 
à l\'article 441-1 du code pénal, prévoyant des peines pouvant aller jusqu\'à trois ans d\'emprisonnement et 45 000 euros d\'amende. 
<br/><br/> Conformément aux dispositions de l\'article L112-9 du code des relations entre le public et l\'administration, les présentes conditions 
générales s\'imposent à tout utilisateur de PLACE. <h3>Juridiction compétente</h3> Les présentes conditions sont soumises au droit français. 
En cas de litige, et si un accord amiable n\'a pu être conclu, les tribunaux français sont compétents. <h3>Mentions légales</h3> 
Les mentions légales sont disponibles <a href=\'/entreprise/footer/info-site\'>ici</a>."
WHERE `configuration_messages_traduction`.`source` = "TEXT_DIFFICULTE_CERTIFICAT_ELECTRONIQUE" and `langue_id` = 1');

        $this->addSql('UPDATE `configuration_messages_traduction` 
SET `target` = "Les informations relatives à vos données à caractère personnel sont disponibles sur la page 
<a href=\'/entreprise/footer/protection-donnees\'>Protection des données à caractère personnel</a>" 
WHERE `configuration_messages_traduction`.`source` = "TEXT_AVERTISSEMENT_RECOMMANDATION" and `langue_id` = 1');

    }
}
