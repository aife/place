<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210521145322 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16217 -  [Recens./Prog./Strat] Intégration des 11 nouvelles habilitations';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationProfil` 
    ADD `besoin_unitaire_consultation` TINYINT(1) NULL DEFAULT \'0\', 
    ADD `besoin_unitaire_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `demande_achat_consultation` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `demande_achat_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `projet_achat_consultation` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `projet_achat_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_opportunite` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_achat` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_budget` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `strategie_achat_gestion` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `recensement_programmation_administration` TINYINT(1) NULL DEFAULT \'0\'
    ');
        $this->addSql('ALTER TABLE `HabilitationAgent` 
    ADD `besoin_unitaire_consultation` TINYINT(1) NULL DEFAULT \'0\', 
    ADD `besoin_unitaire_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `demande_achat_consultation` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `demande_achat_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `projet_achat_consultation` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `projet_achat_creation_modification` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_opportunite` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_achat` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `validation_budget` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `strategie_achat_gestion` TINYINT(1) NULL DEFAULT \'0\',  
    ADD `recensement_programmation_administration` TINYINT(1) NULL DEFAULT \'0\'
    ');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `HabilitationProfil` 
    DROP `besoin_unitaire_consultation`,
    DROP `besoin_unitaire_creation_modification` ,
    DROP `demande_achat_consultation`,
    DROP `demande_achat_creation_modification` , 
    DROP `projet_achat_consultation` ,
    DROP `projet_achat_creation_modification` ,
    DROP `validation_opportunite` ,
    DROP `validation_achat`, 
    DROP `validation_budget` , 
    DROP `strategie_achat_gestion` , 
    DROP `recensement_programmation_administration` 
;');

        $this->addSql('ALTER TABLE `HabilitationAgent` 
    DROP `besoin_unitaire_consultation`,
    DROP `besoin_unitaire_creation_modification` ,
    DROP `demande_achat_consultation`,
    DROP `demande_achat_creation_modification` , 
    DROP `projet_achat_consultation` ,
    DROP `projet_achat_creation_modification` ,
    DROP `validation_opportunite` ,
    DROP `validation_achat`, 
    DROP `validation_budget` , 
    DROP `strategie_achat_gestion` , 
    DROP `recensement_programmation_administration` 
;');
    }
}
