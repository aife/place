<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210511103906 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16208 - Génération du doc NOTI 6 KO';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("UPDATE `document_template` SET `nom` = 'NOTI6' WHERE `document` = 'NOTI6'");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("UPDATE `document_template` SET `nom` = 'cessibilite_creances' WHERE `document` = 'NOTI6'");
    }
}
