<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210504153528 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15602 - [Annexes fin.] Analyse et classement - Import manuel d\'une annexe financière';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("CREATE TABLE IF NOT EXISTS `annexe_financiere`( 
            `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
            `id_consultation` int(11) DEFAULT NULL, 
            `id_agent` int(11) DEFAULT NULL, 
            `id_blob` int(11) DEFAULT NULL, 
            `entreprise` VARCHAR (11) NOT NULL, 
            `numero_lot` int(11) DEFAULT NULL, 
            `numero_pli` int(11) DEFAULT NULL, 
            `date_remise` datetime DEFAULT NULL, 
            `created_at` datetime DEFAULT NULL, 
            `updated_at` datetime DEFAULT NULL, 
            PRIMARY KEY(`id`) 
            ); 
        ");

        $this->addSql('ALTER TABLE annexe_financiere ADD CONSTRAINT FK_ANNEXE_FINANCIERE_AGENT FOREIGN KEY (id_agent) REFERENCES Agent (id)');
        $this->addSql('ALTER TABLE annexe_financiere ADD CONSTRAINT FK_FK_ANNEXE_FINANCIERE_CONSULTATION FOREIGN KEY (id_consultation) REFERENCES consultation (id)');
        $this->addSql('ALTER TABLE annexe_financiere ADD CONSTRAINT FK_FK_ANNEXE_FINANCIERE_BLOB_ORGANISME_FILE FOREIGN KEY (id_blob) REFERENCES blobOrganisme_file (id)');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DROP TABLE annexe_financiere');

    }
}
