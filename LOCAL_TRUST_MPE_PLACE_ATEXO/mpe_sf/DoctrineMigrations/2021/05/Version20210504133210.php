<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210504133210 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15990 - Ajout d\'une valeur référentiel dans référentiel CCAG';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO ValeurReferentiel (id,id_referentiel,libelle_valeur_referentiel, libelle_valeur_referentiel_fr,ordre_affichage) VALUES (8,20,'CCAG-MOE','CCAG-MOE',0)");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM ValeurReferentiel where id = 8;");
    }
}
