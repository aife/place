<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210521135934 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16216 - [Recens./Prog./Strat] Nouveau module organisme';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` 
    ADD `module_recensement_programmation` TINYINT(1) NULL DEFAULT \'0\' 
    COMMENT "Active le module de recensement, programmation, et stratégie d\'achat";');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `module_recensement_programmation`;');
    }
}
