<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210616141154 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16309 - [ENVOL - Module et habilit.] Création d\'une nouvelle habilitation Agent';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE HabilitationAgent ha SET gestion_envol = 
            (
                SELECT module_envol
                FROM configuration_organisme co WHERE organisme = (
                    SELECT organisme FROM Agent a
                                     WHERE id = ha.id_agent
                )
            )'
        );

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('UPDATE HabilitationAgent SET gestion_envol = 0');
    }
}
