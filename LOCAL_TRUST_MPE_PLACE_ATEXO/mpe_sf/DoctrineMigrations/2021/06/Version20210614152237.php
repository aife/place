<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210614152237 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16309 - [ENVOL - Module et habilit.] Création d\'une nouvelle habilitation Agent';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE HabilitationAgent ADD COLUMN gestion_envol BOOLEAN NOT NULL DEFAULT 0 COMMENT \'Importer et joindre des dossiers volumineux (module ENVOL)\';');
        $this->addSql('ALTER TABLE HabilitationProfil ADD COLUMN gestion_envol BOOLEAN NOT NULL DEFAULT 0 COMMENT \'Importer et joindre des dossiers volumineux (module ENVOL)\';');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE HabilitationAgent DROP COLUMN gestion_envol;');
        $this->addSql('ALTER TABLE HabilitationProfil DROP COLUMN gestion_envol;');
    }
}
