<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210628065838 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16561 - Activation automatique de la pub simplifiée';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE configuration_plateforme set publicite = "1;"');
        $this->addSql('UPDATE `configuration_client` SET `value`= "1" WHERE `parameter` = "CONCENTRATEUR_V2";');
        $this->addSql('UPDATE configuration_organisme set donnees_complementaires = "1", Donnees_Redac = "1"');
        $this->addSql('UPDATE ProcedureEquivalence set autoriser_publicite = "1";');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('UPDATE configuration_plateforme set publicite = "0";');
        $this->addSql('UPDATE `configuration_client` SET `value`= "0" WHERE `parameter` = "CONCENTRATEUR_V2";');
        $this->addSql('UPDATE configuration_organisme set donnees_complementaires = "0", Donnees_Redac = "0"');
        $this->addSql('UPDATE ProcedureEquivalence set autoriser_publicite = "0";');
    }
}
