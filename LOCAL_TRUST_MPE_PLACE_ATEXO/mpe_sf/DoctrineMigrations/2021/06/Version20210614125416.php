<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210614125416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16306 - [ENVOL - Module et habilit.] Remplacement du module plateforme en module organisme';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE configuration_organisme SET module_envol = IF ( (SELECT dossier_volumineux FROM configuration_plateforme AS dv) = '1', 1, 0 )");
        $this->addSql('ALTER TABLE configuration_plateforme DROP COLUMN dossier_volumineux;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_plateforme ADD COLUMN dossier_volumineux BOOLEAN NOT NULL DEFAULT 0 COMMENT \'Permet d\'activer l\'upload et download des fichiers volumineux\';');
    }
}
