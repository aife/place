<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210617160025 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16312 - [ENVOL - Agent] Enrichissement du paramétrage des types de procédure';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE ProcedureEquivalence ADD autorisation_dossiers_volumineux_envol_non VARCHAR(2) NOT NULL DEFAULT \'+1\'');
        $this->addSql('ALTER TABLE ProcedureEquivalence ADD autorisation_dossiers_volumineux_envol_oui VARCHAR(2) NOT NULL DEFAULT \'+0\'');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE ProcedureEquivalence DROP COLUMN autorisation_dossiers_volumineux_envol_non;');
        $this->addSql('ALTER TABLE ProcedureEquivalence DROP COLUMN autorisation_dossiers_volumineux_envol_oui;');
    }
}
