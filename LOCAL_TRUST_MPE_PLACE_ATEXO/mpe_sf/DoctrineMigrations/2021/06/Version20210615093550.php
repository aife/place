<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210615093550 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16095 - [Annexes fin.] Espace doc - Ajout de l\'annexe financière comme pièce disponible à la génération';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $sql = 'UPDATE `document_template` SET nom = :nom WHERE document = :document';
        foreach ($this->getData() as $data) {
            $this->addSql(
                $sql,
                ['document' => $data['document'], 'nom' => $data['nom']]
            );
        }
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $sql = 'UPDATE `document_template` SET nom = :nom WHERE document = :document';
        foreach ($this->getData() as $data) {
            $this->addSql(
                $sql,
                ['document' => $data['document'], 'nom' => $data['document']]
            );
        }
    }

    /**
     * @return array
     */
    private function getData(): array
    {
        return [
            [
                'document' => 'OUV3',
                'nom' => 'OUV3.docx'
            ],
            [
                'document' => 'OUV4',
                'nom' => 'OUV4.docx'
            ],
            [
                'document' => 'OUV5',
                'nom' => 'OUV5.docx'
            ],
            [
                'document' => 'OUV6',
                'nom' => 'OUV6.docx'
            ],
            [
                'document' => 'OUV7',
                'nom' => 'OUV7.docx'
            ],
            [
                'document' => 'OUV8',
                'nom' => 'OUV8.docx'
            ],
            [
                'document' => 'OUV9',
                'nom' => 'OUV9.docx'
            ],
            [
                'document' => 'OUV10',
                'nom' => 'OUV10.docx'
            ],
            [
                'document' => 'OUV11',
                'nom' => 'OUV11.docx'
            ],
            [
                'document' => 'NOTI1',
                'nom' => 'NOTI1.docx'
            ],
            [
                'document' => 'NOTI3',
                'nom' => 'NOTI3.docx'
            ],
            [
                'document' => 'NOTI4',
                'nom' => 'NOTI4.docx'
            ],
            [
                'document' => 'NOTI5',
                'nom' => 'NOTI5.docx'
            ],
            [
                'document' => 'NOTI6',
                'nom' => 'NOTI6.docx'
            ],
            [
                'document' => 'NOTI7',
                'nom' => 'NOTI7.docx'
            ],
            [
                'document' => 'NOTI8',
                'nom' => 'NOTI8.docx'
            ],
            [
                'document' => 'AF',
                'nom' => 'annexe_financiere.xlsx'
            ]
        ];
    }
}
