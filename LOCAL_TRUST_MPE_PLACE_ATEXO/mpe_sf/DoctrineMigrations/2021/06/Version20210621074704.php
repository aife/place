<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210621074704 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16466 - [ENVOL - Tech] Reprise de données sur les consultations existantes';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            '
            UPDATE consultation c SET envol_activation = 
            (
                SELECT module_envol
                FROM configuration_organisme co WHERE co.organisme = c.organisme
            )
            '
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('UPDATE consultation SET envol_activation = 0');
    }
}
