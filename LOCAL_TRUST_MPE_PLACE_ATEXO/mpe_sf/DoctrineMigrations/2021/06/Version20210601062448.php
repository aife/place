<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

final class Version20210601062448 extends AbstractMigration implements ContainerAwareInterface
{

    use ContainerAwareTrait;

    /**
     * @var []Organisme
     */
    private $organismes;

    /**
     * @var []TypeContrat
     */
    private $typeContrats;

    private $idProcedureMax;

    public function getDescription(): string
    {
        return 'MPE-16232 - [Procédure simplifiée] Type de procédure par défaut';
    }

    public function preUp(Schema $schema): void
    {
        $increment = 200;

        $stmt = $this->connection->prepare('select acronyme from Organisme;');
        $this->organismes = $stmt->executeQuery()->fetchFirstColumn();

        $stmt = $this->connection->prepare('select id_type_contrat from t_type_contrat;');
        $this->typeContrats = $stmt->executeQuery()->fetchFirstColumn();

        $stmt = $this->connection->prepare('select max(id_type_procedure) as maxId FROM ProcedureEquivalence');
        $max = $stmt->executeQuery()->fetchOne();
        $this->idProcedureMax = (int)$max + $increment;
    }

    public function up(Schema $schema): void
    {
        foreach ($this->organismes as $organismeAcronyme) {
            $this->addSql(
                "INSERT INTO Type_Procedure_Organisme (id_type_procedure, organisme, libelle_type_procedure, abbreviation, type_boamp, id_type_procedure_portail, categorie_procedure, delai_alerte, id_type_validation, service_validation, mapa, activer_mapa, libelle_type_procedure_fr, libelle_type_procedure_en, libelle_type_procedure_es, libelle_type_procedure_su, libelle_type_procedure_du, libelle_type_procedure_cz, libelle_type_procedure_ar, id_montant_mapa, code_recensement, depouillable_phase_consultation, consultation_transverse, tag_Boamp, ao, mn, dc, autre, sad, accord_cadre, pn, tag_name_mesure_avancement, abreviation_interface, libelle_type_procedure_it, publicite_types_form_xml, tag_name_chorus, equivalent_opoce, equivalent_boamp, ordre_affichage, id_externe, procedure_simplifie) 
VALUES (" . $this->idProcedureMax . ", '" . $organismeAcronyme . "', 'Procédure simplifiée', 'P-SIMP', 0, 3, 0, 0, 2, 0, '1', '1', '', '', '', '', '', '', NULL, 1, '9', '0', '0', '<ProcedureAdaptee/>', '0', '0', '0', '0', '0', '0', '0', '4', 'MAI', NULL, '', '9', '', '', 0, 'PA-INF', 1);"
            );

            $this->addSql(
                "INSERT INTO ProcedureEquivalence (`id_type_procedure`, organisme, elec_resp, no_elec_resp, cipher_enabled, cipher_disabled, signature_enabled, signature_disabled, env_candidature, env_offre, env_anonymat, envoi_complet, envoi_differe, procedure_publicite, procedure_restreinte_candidature, procedure_restreinte_offre, envoi_mail_par_mpe, no_envoi_mail_par_mpe, mise_en_ligne1, mise_en_ligne2, mise_en_ligne3, mise_en_ligne4, env_offre_type_unique, env_offre_type_multiple, no_fichier_annonce, fichier_importe, fichier_boamp, reglement_cons, dossier_dce, partial_dce_download, service, constitution_dossier_reponse, env_offre_type_unique2, env_offre_type_multiple2, gestion_envois_postaux, tireur_plan_non, tireur_plan_oui, tireur_plan_papier, tireur_plan_cdrom, tireur_plan_nom, tirage_descriptif, delai_date_limite_remise_pli, signature_propre, procedure_restreinte, ouverture_simultanee, type_decision_a_renseigner, type_decision_attribution_marche, type_decision_declaration_sans_suite, type_decision_declaration_infructueux, type_decision_selection_entreprise, type_decision_attribution_accord_cadre, type_decision_admission_sad, type_decision_autre, env_offre_technique, env_offre_technique_type_unique, env_offre_technique_type_multiple, rep_obligatoire, no_rep_obligatoire, autre_piece_cons, resp_elec_autre_plateforme, mise_en_ligne_entite_coordinatrice, autoriser_publicite, poursuite_date_limite_remise_pli, delai_poursuite_affichage, delai_poursuivre_affichage_unite, mode_ouverture_dossier, mode_ouverture_reponse, marche_public_simplifie, dume_demande, type_procedure_dume, type_formulaire_dume_standard, type_formulaire_dume_simplifie, afficher_code_cpv, code_cpv_obligatoire, donnees_complementaire_non, donnees_complementaire_oui, signature_autoriser, annexe_financiere) VALUES
('" . $this->idProcedureMax . "', '" . $organismeAcronyme . "', '-1', '-0', '-0', '-1', '-0', '-1', '-0', '-1', '-0', '-1', '-0', '+0', '+0', '+0', '+0', '+0', '-0', '-1', '-0', '-0', '-1', '-0', '+1', '+0', '+0', '-0', '+1', '-1', '+1', '-0', '1', '+0', '-1', '+1', '+0', '-0', '-0', '+1', '+1', '-0', '-0', '+1', '', '+0', '+0', '+0', '+0', '+0', '-0', '-0', '+0', '-0', '-0', '-0', '-0', '+1', '-0', '-0', '-0', '0', '-1', '4', 'HOUR', '-1', '-0', '-0', '-0', '-0', '-0', '-1', '0', '0', '-1', '-0', '-0', '-0');"
            );

            foreach ($this->typeContrats as $typeContratId) {
                $this->addSql(
                    "INSERT INTO `t_type_contrat_et_procedure`(`id_type_contrat`, `id_type_procedure`, `organisme`) 
VALUES ('" . $typeContratId . "','" . $this->idProcedureMax . "','" . $organismeAcronyme . "');"
                );
            }
            $this->idProcedureMax += 1;
        }
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException(
            'Les données insérées ne peuvent pas être retrouvées pour une opération inverse.'
        );
    }
}
