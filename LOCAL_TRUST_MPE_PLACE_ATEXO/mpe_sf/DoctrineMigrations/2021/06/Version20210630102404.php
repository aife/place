<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210630102404 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16646 - Correction de la gestion des propriétés liées aux documents sur le serveur de ressources';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_OUTILS';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_MODELE_IMPORT_LOTS';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_GUIDE_AUTOFORMATION_ENTREPRISE';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_GUIDE';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_FORMATION';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_AUTOFORMATION';");
        $this->addSql("DELETE FROM configuration_client WHERE parameter = 'URL_DOCS_AUTOFORMATION_MPS';");
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
