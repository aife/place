<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210607072904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16362 - Personnalisation des modèles de documents';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
            CREATE TABLE IF NOT EXISTS `document_template_surcharge` (
                `id` INT(11) NOT NULL AUTO_INCREMENT 
                , `document_template_id` INT(11) NOT NULL
                , `nom_afficher` VARCHAR(100) NOT NULL 
                , `organisme` VARCHAR(30) NULL
                , `priority` INT(11) NOT NULL COMMENT '1:Client, 2:Organisme' 
                , PRIMARY KEY (`id`)
            ) ENGINE = InnoDB;
        ");

        $this->addSql("ALTER TABLE `document_template_surcharge` ADD KEY IF NOT EXISTS `template_surcharge_organisme_idx` (`organisme`), ADD KEY IF NOT EXISTS `template_surcharge_document_idx` (`document_template_id`)");

        $this->addSql("ALTER TABLE `document_template_surcharge` ADD CONSTRAINT `FK_DOCUMENT_TEMPLATE_SURCHARGE_DOCUMENT` FOREIGN KEY (`document_template_id`) REFERENCES `document_template`(`id`);");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `document_template_surcharge` DROP FOREIGN KEY FK_DOCUMENT_TEMPLATE_SURCHARGE_ORGANISME;");
        $this->addSql("ALTER TABLE `document_template_surcharge` DROP FOREIGN KEY FK_DOCUMENT_TEMPLATE_SURCHARGE_DOCUMENT;");
        $this->addSql("DROP TABLE `document_template_surcharge`");
    }
}
