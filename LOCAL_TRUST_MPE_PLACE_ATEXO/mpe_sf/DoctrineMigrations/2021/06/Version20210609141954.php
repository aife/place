<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210609141954 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16483 - [Publication des données essentielles] Choix de la source de données';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `MarchePublie` ADD `dataSource` INT(2) NOT NULL DEFAULT 0 COMMENT '0:Ce profil acheteur + data.gouv.fr, 1:Ce profil acheteur, 2:data.gouv.fr';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `MarchePublie` DROP `dataSource`;");
    }
}
