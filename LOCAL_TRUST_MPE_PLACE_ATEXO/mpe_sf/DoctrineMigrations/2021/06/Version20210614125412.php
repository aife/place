<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210614125412 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16306 - [ENVOL - Module et habilit.] Remplacement du module plateforme en module organisme';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN module_envol BOOLEAN NOT NULL DEFAULT 0 COMMENT \'Permet d’activer le module ENVOL\';');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_organisme DROP COLUMN module_envol;');
    }
}
