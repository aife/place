<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210601133928 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16457 - Paramétrage par défaut des types de procédure existants pour les annexes financieres';
    }

    public function up(Schema $schema): void
    {
        // Add default value to annexe_financiere field
        $this->addSql("ALTER TABLE `ProcedureEquivalence` CHANGE `annexe_financiere` `annexe_financiere` CHAR(2) NOT NULL DEFAULT '-0';");

        // Set annexe_financiere empty values to default
        $this->addSql("UPDATE `ProcedureEquivalence` SET `annexe_financiere`='-0' WHERE `annexe_financiere`='';");

        // Default settings for existing procedures when enveloppe d'offre is enabled
        $this->addSql("UPDATE `ProcedureEquivalence` SET `annexe_financiere`='+1' WHERE `env_offre` like '%1';");
    }

    public function down(Schema $schema): void
    {
        // Rollback default value
        $this->addSql("ALTER TABLE `ProcedureEquivalence` CHANGE `annexe_financiere` `annexe_financiere` CHAR(2) NOT NULL;");

        // Rollback default settings for existing procedures when enveloppe d'offre is enabled
        $this->addSql("UPDATE `ProcedureEquivalence` SET `annexe_financiere`='-0' WHERE `env_offre` like '%1';");
    }
}
