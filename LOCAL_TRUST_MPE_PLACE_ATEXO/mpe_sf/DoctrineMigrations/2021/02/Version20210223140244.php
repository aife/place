<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210223140244 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15180 - PLACE - RGPD - Popin entreprise';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Inscrit ADD date_validation_rgpd Datetime DEFAULT NULL');
        $this->addSql('ALTER TABLE Inscrit ADD rgpd_communication_place Boolean DEFAULT 0');
        $this->addSql('ALTER TABLE Inscrit ADD rgpd_enquete Boolean DEFAULT 0');
        $this->addSql('ALTER TABLE Inscrit ADD rgpd_communication Boolean DEFAULT 0');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Inscrit DROP date_validation_rgpd');
        $this->addSql('ALTER TABLE Inscrit DROP rgpd_communication_place');
        $this->addSql('ALTER TABLE Inscrit DROP rgpd_enquete');
        $this->addSql('ALTER TABLE Inscrit DROP rgpd_communication');

    }
}
