<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210225161938 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15188 - PLACE - RGPD - Enrichissement de la page Mon compte côté agent';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Agent ADD date_validation_rgpd Datetime DEFAULT NULL');
        $this->addSql('ALTER TABLE Agent ADD rgpd_communication_place Boolean DEFAULT 0');
        $this->addSql('ALTER TABLE Agent ADD rgpd_enquete Boolean DEFAULT 0');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Agent DROP date_validation_rgpd');
        $this->addSql('ALTER TABLE Inscrit DROP rgpd_communication_place');
        $this->addSql('ALTER TABLE Inscrit DROP rgpd_enquete');

    }
}
