<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210205122617 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15110 - [RdR 2021-00.01.00] Modification URL appelée pour récupérer le modèle "Import de lots"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("update `configuration_client` SET value = '/docs/modeles/atexo/Import_Lots_Modele_v2021.xlsx' where parameter = 'URL_DOCS_MODELE_IMPORT_LOTS';");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("update `configuration_client` SET value = 'docs/modeles/atexo/Import Lots - Modele - v2019-1.4.0.xlsx' where parameter = 'URL_DOCS_MODELE_IMPORT_LOTS';");
    }
}
