<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210209064427 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14976 - [PLACE] Désactivation par défaut de la MESSEC V2';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }
        $this->addSql('UPDATE configuration_plateforme SET messagerie_v2 = 0;');
    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException('La configuration est figée est PLACE.');
    }
}
