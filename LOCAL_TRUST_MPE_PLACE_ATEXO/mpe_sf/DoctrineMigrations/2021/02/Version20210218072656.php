<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210218072656
 * @package App\Migrations
 */
final class Version20210218072656 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15079 - [RdR 2021-00.02.00] Page "autorités de certifications européennes" KO';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT IGNORE INTO `configuration_client` ( `parameter`, `value`, `updated_at`) 
SELECT * FROM ( SELECT 'LISTE_AC_RGS_TSL_FR', 'http://www.ssi.gouv.fr/eidas/TL-FR.xml', '2021-02-18 00:00:00') AS tmp 
WHERE NOT EXISTS (SELECT * FROM `configuration_client` WHERE `parameter`='LISTE_AC_RGS_TSL_FR') LIMIT 1");
    }
}
