<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210202133943 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_organisme MODIFY saisie_manuelle_id_externe BOOLEAN NOT NULL DEFAULT 0 COMMENT \'Permet d’activer la saisie manuelle de l’ID externe des agents et des services\'');

    }

    public function down(Schema $schema) : void
    {
        //this is only to add a comment on the column

    }
}
