<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210212132227 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14973 - ';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT IGNORE INTO `configuration_messages_traduction` (`id`, `source`, `target`, `langue_id`) 
VALUES (NULL, 'TEXT_ALERTES_CONSULTATIONS', 'Alertes liées aux consultations suivies par moi-même (celles pour lesquelles je suis mentionné(e) comme \"Invité\")', '1');");
     }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM `configuration_messages_traduction` 
WHERE `configuration_messages_traduction`.`source` = 'TEXT_ALERTES_CONSULTATIONS';");

    }
}
