<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210215150749 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15112';
    }
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `module_sourcing` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'active le module sourcing\'');
    }
    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `module_sourcing`');
    }
}