<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210913074126 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-17155 - [RdR 2021-00.05.00] Synchro socle admin agent : suppression d\'un utilisateur sur le socle KO';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("INSERT INTO `web_service` (`id`, `route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`) VALUES (NULL, '/api/{version}/agents.{format}', 'DELETE', 'Désactive un agent', '1')");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("DELETE FROM `web_service` where `route_ws` = '/api/{version}/agents.{format}' 
and `method_ws` =  'DELETE'");
    }
}
