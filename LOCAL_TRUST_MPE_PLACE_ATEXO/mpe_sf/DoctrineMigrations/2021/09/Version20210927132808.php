<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210927132808 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-17262 - Faire fonctionner le module CAO';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` ADD `cao` TINYINT NOT NULL DEFAULT '0';");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` DROP `cao`");

    }
}
