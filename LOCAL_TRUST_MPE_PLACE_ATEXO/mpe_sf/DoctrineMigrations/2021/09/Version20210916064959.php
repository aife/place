<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210916064959 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'PLACE-208 : 
        [PLACE] RdR 2021-02.01.02 - Le chemin du guide utilisateur Entreprise n\'est pas variabilisé';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("INSERT IGNORE INTO `configuration_client` (`parameter`, `value`, `updated_at`) 
SELECT DISTINCT  'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE', '/docs/guide-esr-2017/place/GuideUtilisateurEntreprise.zip', CURRENT_DATE
FROM configuration_client as c where NOT EXISTS (SELECT 1 from  configuration_client as cc where cc.parameter like 'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE');");

    }
}
