<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210921131705 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16414 - Revert ajout champ date publication joue';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `ProcedureEquivalence` DROP IF EXISTS `date_publication_joue`");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `ProcedureEquivalence` ADD COLUMN IF NOT EXISTS `date_publication_joue` VARCHAR(2) NOT NULL DEFAULT '+0';");
    }
}
