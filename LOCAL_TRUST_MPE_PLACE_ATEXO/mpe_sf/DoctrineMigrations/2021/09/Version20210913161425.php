<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210913161425 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16894 - Maximilien : description activité entreprise, impossible d ajouter un code CPV';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `Lt_Referentiel` SET path_config = REPLACE(path_config, '/ressources/referentiels-new', '/docs/referentiels/atexo');");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `Lt_Referentiel` SET path_config = REPLACE(path_config, '/docs/referentiels/atexo', '/ressources/referentiels-new');");
    }
}
