<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210915090434 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17197 - [RdR 2021-00.05.00] Le DIC est incomplet';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO `configuration_client` (parameter, value, updated_at) SELECT 'STRUCTURE_ARCHIVE', 1, NOW() FROM DUAL WHERE NOT EXISTS( SELECT 1 FROM `configuration_client` WHERE parameter = 'STRUCTURE_ARCHIVE' ) LIMIT 1;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `configuration_client` WHERE parameter = 'STRUCTURE_ARCHIVE';");
    }
}
