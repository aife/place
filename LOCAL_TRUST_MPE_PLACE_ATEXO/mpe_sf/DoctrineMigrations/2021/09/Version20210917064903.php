<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210917064903 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-166';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("UPDATE configuration_messages_traduction SET `target` =
        'WARNING: One or more files exceed the authorized size limit (1GB) on this consultation and therefore were not added to your answer. <br/><b>List of affected files: </b>'
        where  `langue_id`='2' and `source` = 'ERREUR_LORS_DE_LA_VALIDATION_DU_DEPOT_D_UNE_REPONSE_ELECTRONIQUE_ERROR_MESSAGE_FIRST';");
    }

}
