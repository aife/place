<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210903122206 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17102 - [RdR 2021-00.05.00] Désactivation de la publicité simplifié';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `configuration_plateforme` 
SET `publicite` = '1' 
WHERE `configuration_plateforme`.`id_auto` = 1;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE `configuration_plateforme` 
SET `publicite` = '0' 
WHERE `configuration_plateforme`.`id_auto` = 1;");
    }
}
