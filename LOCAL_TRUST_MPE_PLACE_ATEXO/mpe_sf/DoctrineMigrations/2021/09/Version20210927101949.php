<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210927101949 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("CREATE TABLE `Commission` (
  `id` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lieu` varchar(200) NOT NULL DEFAULT '',
  `salle` varchar(100) NOT NULL DEFAULT '',
  `type` varchar(200) NOT NULL DEFAULT '',
  `libelle` varchar(250) DEFAULT NULL,
  `ordre_du_jour` longblob DEFAULT NULL,
  `invitations_zip` longblob DEFAULT NULL,
  `status_cao` int(11) NOT NULL,
  PRIMARY KEY (id,organisme)
) ");

        $this->addSql("CREATE TABLE `Intervenant_Ordre_Du_Jour` (
  `id` int(11) NOT NULL,
  `organisme` varchar(30) NOT NULL,
  `id_ordre_du_jour` int(11) NOT NULL DEFAULT 0,
  `id_intervenant` int(11) NOT NULL DEFAULT 0,
  `id_agent` int(11) NOT NULL DEFAULT 0,
  `convocation` longblob DEFAULT NULL,
  `convoc_send` char(1) NOT NULL DEFAULT '0',
  `nom_convoc` varchar(255) DEFAULT NULL,
  `contenu_envoi` text DEFAULT NULL,
  `nom_fichier_envoye` varchar(255) DEFAULT NULL,
  `fichier_envoye` longblob DEFAULT NULL,
  `date_envoi` datetime DEFAULT NULL,
  `type_voix` varchar(50) NOT NULL,
  PRIMARY KEY (id,organisme)
)");

        $this->addSql("ALTER TABLE `Intervenant_Ordre_Du_Jour`
  ADD KEY `id_ordre_du_jour` (`id_ordre_du_jour`);");
}

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
