<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211202105605 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17542 - Validation du schéma de données Doctrine';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `EnchereOffre` DROP FOREIGN KEY IF EXISTS EnchereOffre_idEnchere;');


        //Refactor Table Administrateur
        $this->addSql('ALTER TABLE `Administrateur` drop column IF EXISTS `original_mdp`, DROP FOREIGN KEY IF EXISTS Administrateur_ibfk_1, CHANGE original_login original_login VARCHAR(100) NOT NULL, CHANGE login login VARCHAR(100) NOT NULL, CHANGE nom nom VARCHAR(100) NOT NULL, CHANGE prenom prenom VARCHAR(100) NOT NULL, CHANGE email email VARCHAR(100) NOT NULL, CHANGE admin_general admin_general CHAR(1) NOT NULL, CHANGE tentatives_mdp tentatives_mdp INT NOT NULL');
        $this->addSql('ALTER TABLE Administrateur ADD CONSTRAINT FK_FF8F2A30DD0F4533 FOREIGN KEY (organisme) REFERENCES Organisme (acronyme)');


        //Refactor Table JAL and delete composite primary key
        $this->addSql('ALTER TABLE `DestinataireAnnonceJAL` DROP FOREIGN KEY IF EXISTS destinataireAnnonceJAL_jal_id;');
        $this->addSql('ALTER TABLE `DestinataireAnnonceJAL` DROP FOREIGN KEY IF EXISTS destinataireAnnonceJAL_jal_id_org;');

        $this->addSql('ALTER TABLE `JAL` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from JAL;');
        $this->addSql('ALTER TABLE JAL ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');
        $this->addSql('UPDATE JAL SET id = (id + @max);');
        $this->addSql('ALTER TABLE JAL CHANGE email_ar email_ar VARCHAR(100) NOT NULL, CHANGE telecopie telecopie VARCHAR(20) NOT NULL;');

        $this->addSql('ALTER TABLE `DestinataireAnnonceJAL` CHANGE COLUMN `idJAL` `old_jal_id` INT(11) NULL, ADD COLUMN `idJAL` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_destinataireAnnonceJAL_jal_id` 
 FOREIGN KEY ( `idJAL` ) REFERENCES `JAL` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('UPDATE DestinataireAnnonceJAL SET idJAL = (SELECT id FROM JAL WHERE old_id = DestinataireAnnonceJAL.old_jal_id AND organisme = DestinataireAnnonceJAL.organisme) WHERE old_jal_id is not null AND old_jal_id != 0;');

        $this->addSql('ALTER TABLE `Destinataire_Annonce_Press` CHANGE COLUMN `id_jal` `old_jal_id` INT(11) NULL, ADD COLUMN `id_jal` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Destinataire_Annonce_Press SET id_jal = (SELECT id FROM JAL WHERE old_id = Destinataire_Annonce_Press.old_jal_id AND organisme = Destinataire_Annonce_Press.organisme) WHERE old_jal_id is not null AND old_jal_id != 0;');


        //Refactor Table Echange and delete composite primary key
        $this->addSql('ALTER TABLE `EchangeDestinataire` DROP FOREIGN KEY IF EXISTS EchangeDestinataire_ibfk_1;');
        $this->addSql('ALTER TABLE `EchangePieceJointe` DROP FOREIGN KEY IF EXISTS EchangePieceJointe_ibfk_1;');

        $this->addSql('ALTER TABLE `Echange` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from Echange;');
        $this->addSql('ALTER TABLE Echange ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');
        $this->addSql('UPDATE Echange SET id = (id + @max);');

        $this->addSql('ALTER TABLE `EchangeDestinataire` CHANGE COLUMN `id_echange` `old_id_echange` INT(11) NULL, ADD COLUMN `id_echange` BIGINT UNSIGNED NULL, ADD CONSTRAINT `EchangeDestinataire_ibfk_1` 
 FOREIGN KEY ( `id_echange` ) REFERENCES `Echange` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('UPDATE EchangeDestinataire SET id_echange = (SELECT id FROM Echange WHERE old_id = EchangeDestinataire.old_id_echange AND organisme = EchangeDestinataire.organisme) WHERE old_id_echange is not null AND old_id_echange != 0;');

        $this->addSql('ALTER TABLE `EchangePieceJointe` CHANGE COLUMN `id_message` `old_id_message` INT(11) NULL, ADD COLUMN `id_message` BIGINT UNSIGNED NULL, ADD CONSTRAINT `EchangePieceJointe_ibfk_1` 
 FOREIGN KEY ( `id_message` ) REFERENCES `Echange` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
        $this->addSql('UPDATE EchangePieceJointe SET id_message = (SELECT id FROM Echange WHERE old_id = EchangePieceJointe.old_id_message AND organisme = EchangePieceJointe.organisme) WHERE old_id_message is not null AND old_id_message != 0;');

        $this->addSql('ALTER TABLE `Relation_Echange` CHANGE COLUMN `id_echange` `old_id_echange` INT(11) NULL, ADD COLUMN `id_echange` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Relation_Echange SET id_echange = (SELECT id FROM Echange WHERE old_id = Relation_Echange.old_id_echange AND organisme = Relation_Echange.organisme) WHERE old_id_echange is not null AND old_id_echange != 0;');
        $this->addSql('ALTER TABLE Relation_Echange ADD CONSTRAINT FK_748EB32F6BEA4ACF FOREIGN KEY (id_echange) REFERENCES Echange (id)');

        $this->addSql('ALTER TABLE `destinataire_centrale_pub` CHANGE COLUMN `id_echange` `old_id_echange` INT(11) NULL, ADD COLUMN `id_echange` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE destinataire_centrale_pub SET id_echange = (SELECT id FROM Echange WHERE old_id = destinataire_centrale_pub.old_id_echange AND organisme = destinataire_centrale_pub.organisme) WHERE old_id_echange is not null AND old_id_echange != 0;');

        $this->addSql('ALTER TABLE `DestinataireAnnonceJAL` CHANGE COLUMN `id_echange` `old_id_echange` INT(11) NULL, ADD COLUMN `id_echange` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE DestinataireAnnonceJAL SET id_echange = (SELECT id FROM Echange WHERE old_id = DestinataireAnnonceJAL.old_id_echange AND organisme = DestinataireAnnonceJAL.organisme) WHERE old_id_echange is not null AND old_id_echange != 0;');

        $this->addSql('ALTER TABLE Echange DROP FOREIGN KEY IF EXISTS echange_dossier_volumineux_fk');
        $this->addSql('CREATE INDEX IDX_7ACADA23B6709BD0 ON Echange (id_dossier_volumineux)');
        $this->addSql('ALTER TABLE Echange ADD CONSTRAINT echange_dossier_volumineux_fk FOREIGN KEY (id_dossier_volumineux) REFERENCES dossier_volumineux (id) ON UPDATE NO ACTION ON DELETE NO ACTION, CHANGE organisme organisme VARCHAR(30) DEFAULT NULL, ADD CONSTRAINT FK_7ACADA23DD0F4533 FOREIGN KEY (organisme) REFERENCES Organisme (acronyme)');


        //Refactor Table EnchereOffre and delete composite primary key
        $this->addSql('ALTER TABLE `EnchereOffreReference` DROP FOREIGN KEY IF EXISTS EnchereOffreReference_EnchereOffre_idEnchereOffre;');
        $this->addSql('ALTER TABLE `EnchereOffreReference` DROP FOREIGN KEY IF EXISTS EnchereOffreReference_EnchereOffre;');

        $this->addSql('ALTER TABLE `EnchereOffre` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from EnchereOffre;');
        $this->addSql('ALTER TABLE EnchereOffre ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');
        $this->addSql('UPDATE EnchereOffre SET id = (id + @max);');

        $this->addSql('ALTER TABLE `EnchereOffreReference` CHANGE COLUMN `idEnchereOffre` `old_id_enchere_offre` INT(11) NULL, ADD COLUMN `id_enchere_offre` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereOffreReference_EnchereOffre` 
 FOREIGN KEY ( `id_enchere_offre` ) REFERENCES `EnchereOffre` (`id`)  ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereOffreReference SET id_enchere_offre = (SELECT id FROM EnchereOffre WHERE old_id = EnchereOffreReference.old_id_enchere_offre AND organisme = EnchereOffreReference.organisme) WHERE old_id_enchere_offre is not null AND old_id_enchere_offre != 0;');


        //Refactor Table EncherePmi and delete composite primary key
        $this->addSql('ALTER TABLE `EnchereTranchesBaremeNETC` DROP FOREIGN KEY IF EXISTS EnchereTranchesBaremeNETC_EncherePmi;');
        $this->addSql('ALTER TABLE `EnchereTranchesBaremeNETC` DROP FOREIGN KEY IF EXISTS EnchereTranchesBaremeNETC_EncherePmi_idReference;');
        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` DROP FOREIGN KEY IF EXISTS EnchereEntreprisePmi_EncherePmi_IdTech;');
        $this->addSql('ALTER TABLE `EnchereReference` DROP FOREIGN KEY IF EXISTS EnchereReference_EncherePmi_idEnchere;');
        $this->addSql('ALTER TABLE `EnchereReference` DROP FOREIGN KEY IF EXISTS EnchereReference_EncherePmi_idEnchere_org;');
        $this->addSql('ALTER TABLE `EnchereOffre` DROP FOREIGN KEY IF EXISTS EnchereOffre_EncherePmi;');

        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` DROP FOREIGN KEY IF EXISTS EnchereEntreprisePmi_ibfk_1;');
        $this->addSql('ALTER TABLE `EncherePmi` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from EncherePmi;');
        $this->addSql('ALTER TABLE EncherePmi ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');
        $this->addSql('UPDATE EncherePmi SET id = (id + @max);');

        $this->addSql('DROP INDEX IF EXISTS EnchereOffre_EncherePmi ON EnchereOffre');
        $this->addSql('DROP INDEX IF EXISTS idEnchere ON EnchereOffre');
        $this->addSql('ALTER TABLE `EnchereOffre` CHANGE COLUMN `idEnchere` `old_id_enchere` INT(11) NULL, ADD COLUMN `idEnchere` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereOffre_EncherePmi` 
 FOREIGN KEY ( `idEnchere` ) REFERENCES `EncherePmi` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereOffre SET idEnchere = (SELECT id FROM EncherePmi WHERE old_id = EnchereOffre.old_id_enchere AND organisme = EnchereOffre.organisme) WHERE old_id_enchere is not null AND old_id_enchere != 0;');

        $this->addSql('DROP INDEX IF EXISTS EnchereReference_EncherePmi_idEnchere_org ON EnchereReference;');
        $this->addSql('ALTER TABLE `EnchereReference` CHANGE COLUMN `idEnchere` `old_id_enchere` INT(11) NULL, ADD COLUMN `idEnchere` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereReference_EncherePmi_idEnchere` 
 FOREIGN KEY ( `idEnchere` ) REFERENCES `EncherePmi` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereReference SET idEnchere = (SELECT id FROM EncherePmi WHERE old_id = EnchereReference.old_id_enchere AND organisme = EnchereReference.organisme) WHERE old_id_enchere is not null AND old_id_enchere != 0;');

        $this->addSql('DROP INDEX IF EXISTS EnchereEntreprisePmi_EncherePmi_IdTech ON EnchereEntreprisePmi;');
        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` CHANGE COLUMN `idEnchere` `old_id_enchere` INT(11) NULL, ADD COLUMN `idEnchere` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereEntreprisePmi_EncherePmi_idEnchere` 
 FOREIGN KEY ( `idEnchere` ) REFERENCES `EncherePmi` (`id`)  ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereEntreprisePmi SET idEnchere = (SELECT id FROM EncherePmi WHERE old_id = EnchereEntreprisePmi.old_id_enchere AND organisme = EnchereEntreprisePmi.organisme) WHERE old_id_enchere is not null AND old_id_enchere != 0;');

        $this->addSql('DROP INDEX IF EXISTS EnchereTranchesBaremeNETC_EncherePmi ON EnchereTranchesBaremeNETC;');
        $this->addSql('ALTER TABLE `EnchereTranchesBaremeNETC` CHANGE COLUMN `idEnchere` `old_id_enchere` INT(11) NULL, ADD COLUMN `idEnchere` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereTranchesBaremeNETC_EncherePmi_idEnchere` 
 FOREIGN KEY ( `idEnchere` ) REFERENCES `EncherePmi` (`id`)  ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereTranchesBaremeNETC SET idEnchere = (SELECT id FROM EncherePmi WHERE old_id = EnchereTranchesBaremeNETC.old_id_enchere AND organisme = EnchereTranchesBaremeNETC.organisme) WHERE old_id_enchere is not null AND old_id_enchere != 0;');

        $this->addSql('ALTER TABLE EncherePmi DROP FOREIGN KEY IF EXISTS FK_enchere_pmi_service;');
        $this->addSql('ALTER TABLE EncherePmi CHANGE organisme organisme VARCHAR(30) NOT NULL, ADD CONSTRAINT FK_B6D793AFED5CA9E6 FOREIGN KEY (service_id) REFERENCES Service (id);');


        //Refactor Table EnchereEntreprisePmi and delete composite primary key
        $this->addSql('ALTER TABLE `EnchereOffre` DROP FOREIGN KEY IF EXISTS EnchereOffre_EnchereEntreprisePmi;');
        $this->addSql('ALTER TABLE `EnchereValeursInitiales` DROP FOREIGN KEY IF EXISTS EnchereValeursInitiales_EnchereEntreprisePmi;');

        $this->addSql('ALTER TABLE `EnchereOffre` DROP FOREIGN KEY IF EXISTS EnchereOffre_ibfk_1;');

        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from EnchereEntreprisePmi;');
        $this->addSql('ALTER TABLE EnchereEntreprisePmi ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');
        $this->addSql('UPDATE EnchereEntreprisePmi SET id = (id + @max);');

        $this->addSql('DROP INDEX IF EXISTS EnchereOffre_EnchereEntreprisePmi ON EnchereOffre;');
        $this->addSql('ALTER TABLE `EnchereOffre` CHANGE COLUMN `idEnchereEntreprise` `old_id_enchere_entreprise` INT(11) NULL, ADD COLUMN `idEnchereEntreprise` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereOffre_EnchereEntreprisePmi_id` 
 FOREIGN KEY ( `idEnchereEntreprise` ) REFERENCES `EnchereEntreprisePmi` (`id`)  ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereOffre SET idEnchereEntreprise = (SELECT id FROM EnchereEntreprisePmi WHERE old_id = EnchereOffre.old_id_enchere_entreprise AND organisme = EnchereOffre.organisme) WHERE old_id_enchere_entreprise is not null AND old_id_enchere_entreprise != 0;');

        $this->addSql('ALTER TABLE `EnchereValeursInitiales` CHANGE COLUMN `idEnchereEntreprise` `old_id_enchere_entreprise` INT(11) NULL, ADD COLUMN `idEnchereEntreprise` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_EnchereOffre_EnchereValeursInitiales_id` 
 FOREIGN KEY ( `idEnchereEntreprise` ) REFERENCES `EnchereEntreprisePmi` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE EnchereValeursInitiales SET idEnchereEntreprise = (SELECT id FROM EnchereEntreprisePmi WHERE old_id = EnchereValeursInitiales.old_id_enchere_entreprise AND organisme = EnchereValeursInitiales.organisme) WHERE old_id_enchere_entreprise is not null AND old_id_enchere_entreprise != 0;');

        $this->addSql('ALTER TABLE EnchereEntreprisePmi DROP FOREIGN KEY IF EXISTS FK_EnchereEntreprisePmi_EncherePmi_idEnchere');
        $this->addSql('ALTER TABLE EnchereEntreprisePmi CHANGE organisme organisme VARCHAR(30) NOT NULL, CHANGE nom nom VARCHAR(256) NOT NULL, CHANGE email email VARCHAR(256) NOT NULL, CHANGE mdp mdp VARCHAR(256) NOT NULL, ADD CONSTRAINT FK_9A77EC7D2868ECFD FOREIGN KEY (idEnchere) REFERENCES EncherePmi (id)');

        $this->addSql('DROP INDEX IF EXISTS idEnchereEntreprise ON EnchereOffre');
        $this->addSql('ALTER TABLE EnchereOffre DROP FOREIGN KEY IF EXISTS FK_EnchereOffre_EnchereEntreprisePmi_id, DROP FOREIGN KEY IF EXISTS FK_EnchereOffre_EncherePmi, CHANGE organisme organisme VARCHAR(30) NOT NULL');
        $this->addSql('ALTER TABLE EnchereOffre ADD CONSTRAINT FK_1F7C883C84A84093 FOREIGN KEY (idEnchereEntreprise) REFERENCES EnchereEntreprisePmi (id), ADD CONSTRAINT FK_1F7C883C2868ECFD FOREIGN KEY (idEnchere) REFERENCES EncherePmi (id)');


        //Refactor Table Inscrit and delete composite primary key
        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` DROP FOREIGN KEY IF EXISTS Inscrit_t_reponse_elec_form_id_inscrit_fk;');
        $this->addSql('ALTER TABLE `historisation_mot_de_passe` DROP FOREIGN KEY IF EXISTS id_inscrit_histo_mdp_FK;');
        $this->addSql('ALTER TABLE `Certificats_Entreprises` DROP FOREIGN KEY IF EXISTS Certificats_Entreprises_ibfk_2;');
        $this->addSql('ALTER TABLE `dossier_volumineux` DROP FOREIGN KEY IF EXISTS id_inscrit_FK;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` DROP FOREIGN KEY IF EXISTS PanierEntrepriseIdInscrit_InscritId;');

        $this->addSql('ALTER TABLE `Inscrit` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from Inscrit;');
        $this->addSql('ALTER TABLE Inscrit ADD CONSTRAINT old_primary_key UNIQUE (old_id, entreprise_id);');
        $this->addSql('UPDATE Inscrit SET id = (id + @max);');

        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_t_reponse_elec_formulaire_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE t_reponse_elec_formulaire SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = t_reponse_elec_formulaire.old_id_inscrit AND entreprise_id = t_reponse_elec_formulaire.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `historisation_mot_de_passe` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_historisation_mot_de_passe_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE historisation_mot_de_passe SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = historisation_mot_de_passe.old_id_inscrit LIMIT 1) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `Certificats_Entreprises` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_certificats_entreprises_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE Certificats_Entreprises SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = Certificats_Entreprises.old_id_inscrit AND entreprise_id = Certificats_Entreprises.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `dossier_volumineux` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_dossier_volumineux_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE dossier_volumineux SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = dossier_volumineux.old_id_inscrit LIMIT 1) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `Panier_Entreprise` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_panier_entreprise_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE Panier_Entreprise SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = Panier_Entreprise.old_id_inscrit AND entreprise_id = Panier_Entreprise.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `Alerte` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_alerte_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE Alerte SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = Alerte.old_id_inscrit LIMIT 1) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `EchangeDestinataire` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_echange_destinataire_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE EchangeDestinataire SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = EchangeDestinataire.old_id_inscrit AND entreprise_id = EchangeDestinataire.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `Offres` CHANGE COLUMN `inscrit_id` `old_inscrit_id` INT(11) NULL, ADD COLUMN `inscrit_id` BIGINT UNSIGNED NULL, 
    ADD CONSTRAINT `FK_offres_inscrit_id` 
 FOREIGN KEY ( `inscrit_id` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE Offres SET inscrit_id = (SELECT id FROM Inscrit WHERE old_id = Offres.old_inscrit_id AND entreprise_id = Offres.entreprise_id) WHERE old_inscrit_id is not null AND old_inscrit_id != 0;');

        $this->addSql('ALTER TABLE `questions_dce` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_questions_dce_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE questions_dce SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = questions_dce.old_id_inscrit AND entreprise_id = questions_dce.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `QuestionDCE` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_question_dce_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE QuestionDCE SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = QuestionDCE.old_id_inscrit AND entreprise_id = QuestionDCE.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `ReponseInscritFormulaireConsultation` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_reponse_inscrit_formulaire_consultation_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE ReponseInscritFormulaireConsultation SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = ReponseInscritFormulaireConsultation.old_id_inscrit AND entreprise_id = ReponseInscritFormulaireConsultation.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `sso_entreprise` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_sso_entreprise_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE sso_entreprise SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = sso_entreprise.old_id_inscrit LIMIT 1) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `Telechargement` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_telechargement_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE Telechargement SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = Telechargement.old_id_inscrit AND entreprise_id = Telechargement.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `trace_operations_inscrit` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_trace_operations_inscrit_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE trace_operations_inscrit SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = trace_operations_inscrit.old_id_inscrit AND entreprise_id = trace_operations_inscrit.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `t_candidature` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_t_candidature_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE t_candidature SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = t_candidature.old_id_inscrit AND entreprise_id = t_candidature.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `t_candidature_mps` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_t_candidature_mps_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE t_candidature_mps SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = t_candidature_mps.old_id_inscrit AND entreprise_id = t_candidature_mps.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `t_contact_contrat` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_t_contact_contrat_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE');
        $this->addSql('UPDATE t_contact_contrat SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = t_contact_contrat.old_id_inscrit AND entreprise_id = t_contact_contrat.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE `t_liste_lots_candidature` CHANGE COLUMN `id_inscrit` `old_id_inscrit` INT(11) NULL, ADD COLUMN `id_inscrit` BIGINT UNSIGNED NULL, ADD CONSTRAINT `FK_t_liste_lots_candidature_inscrit_id` 
 FOREIGN KEY ( `id_inscrit` ) REFERENCES `Inscrit` (`id`) ON UPDATE CASCADE;');
        $this->addSql('UPDATE t_liste_lots_candidature SET id_inscrit = (SELECT id FROM Inscrit WHERE old_id = t_liste_lots_candidature.old_id_inscrit AND entreprise_id = t_liste_lots_candidature.id_entreprise) WHERE old_id_inscrit is not null AND old_id_inscrit != 0;');

        $this->addSql('ALTER TABLE Inscrit DROP FOREIGN KEY IF EXISTS Inscrit_ibfk_1, CHANGE entreprise_id entreprise_id INT NOT NULL, CHANGE id_etablissement id_etablissement INT DEFAULT NULL, CHANGE nom nom VARCHAR(30) NOT NULL, CHANGE prenom prenom VARCHAR(30) NOT NULL, CHANGE adresse adresse VARCHAR(80) NOT NULL, CHANGE codepostal codepostal VARCHAR(20) NOT NULL, CHANGE ville ville VARCHAR(50) NOT NULL, CHANGE pays pays VARCHAR(50) NOT NULL, CHANGE email email VARCHAR(100) NOT NULL, CHANGE telephone telephone VARCHAR(20) NOT NULL, CHANGE periode periode SMALLINT DEFAULT 0 NOT NULL, CHANGE fax fax VARCHAR(20) NOT NULL');
        $this->addSql('ALTER TABLE Inscrit ADD CONSTRAINT FK_5DC29AF9A4AEAFEA FOREIGN KEY (entreprise_id) REFERENCES Entreprise (id)');
        $this->addSql('UPDATE Inscrit SET id_etablissement = NULL where id_etablissement not in (select id_etablissement from t_etablissement);');
        $this->addSql('ALTER TABLE Inscrit ADD CONSTRAINT FK_5DC29AF99ED58849 FOREIGN KEY (id_etablissement) REFERENCES t_etablissement (id_etablissement)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
