<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211210082417 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-392 - [PLACE] Ajout de la création d\'indexes';
    }

    public function up(Schema $schema): void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("DROP INDEX IF EXISTS echange_organisme_idx on echange;");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on echange (consultation_id, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS piece_idx on EchangePieceJointe (piece, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consulation_idx on historiques_consultation (consultation_id, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS untrusted_date_idx on offres (untrusteddate);");
        $this->addSql("CREATE INDEX IF NOT EXISTS uuid_idx on t_contrat_titulaire (uuid);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on questions_dce (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_numero_reponse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on annoncejal (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on annonce_press (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on certificatchiffrement (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on consultationformulaire (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on consultationhistoriqueetat (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on criteres_evaluation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on decisionlot (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on pieces_mise_disposition (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on  referentielformxml (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on decisionenveloppe (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on resultat_analyse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on resultat_analyse_decision (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_cons_lot_contrat (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_decision_selection_entreprise (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_document (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on backup_ordre_du_jour (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on backup_ordre_du_jour_2 (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on trace_operations_inscrit (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_cao_commission_consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_candidature_mps (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on encherepmi (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on referentiel_consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on invitationconsultationtransverse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on modeleformulaire (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on passation_consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_espace_collaboratif (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on categorielot (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on consultation_favoris (id_consultation);");

    }

    public function down(Schema $schema): void
    {
        $this->addSql("CREATE INDEX IF NOT EXISTS echange_organisme_idx on echange (id, organisme);");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on echange;");
        $this->addSql("DROP INDEX IF EXISTS piece_idx on EchangePieceJointe;");
        $this->addSql("DROP INDEX IF EXISTS consulation_idx on historiques_consultation;");
        $this->addSql("DROP INDEX IF EXISTS untrusted_date_idx on offres;");
        $this->addSql("DROP INDEX IF EXISTS uuid_idx on t_contrat_titulaire;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on questions_dce;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_numero_reponse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on annoncejal");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on annonce_press;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on certificatchiffrement");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on consultationformulaire");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on consultationhistoriqueetat;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on criteres_evaluation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on decisionlot;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on pieces_mise_disposition;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on  referentielformxml;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on decisionenveloppe;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on resultat_analyse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on resultat_analyse_decision;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_cons_lot_contrat;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_decision_selection_entreprise;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_document;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on backup_ordre_du_jour;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on backup_ordre_du_jour_2;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on trace_operations_inscrit;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_cao_commission_consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_candidature_mps;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on encherepmi;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on referentiel_consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on invitationconsultationtransverse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on modeleformulaire;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on passation_consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_espace_collaboratif;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on categorielot;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on consultation_favoris;");

    }
}
