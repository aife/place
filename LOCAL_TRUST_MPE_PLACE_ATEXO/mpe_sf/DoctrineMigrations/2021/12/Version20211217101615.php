<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211217101615 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17986 - Mise en place de API Platform';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'CREATE TABLE refresh_tokens (id INT AUTO_INCREMENT NOT NULL, refresh_token VARCHAR(128) NOT NULL, ' .
            'username VARCHAR(255) NOT NULL, valid DATETIME NOT NULL, ' .
            'UNIQUE INDEX UNIQ_9BACE7E1C74F2195 (refresh_token), ' .
            'PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB'
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE refresh_tokens');
    }
}
