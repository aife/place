<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211208083746 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-372 - [MPE] RdR 2021-02.02.00 - Activer la MESSEC V2 par défaut lors de l\'installation de la version MPE 2021-02.02.00';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE configuration_plateforme SET messagerie_v2 = '1';");
    }

    public function down(Schema $schema) : void
    {
    }
}
