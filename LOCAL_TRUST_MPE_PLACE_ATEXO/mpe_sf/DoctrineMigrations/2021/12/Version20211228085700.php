<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211228085700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-415 - [PLACE] MPE - La valeur en configuration client PF_URL_MESSAGERIE 
        n\'est pas interprétée après redémarrage des services';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("INSERT INTO `configuration_client` (`id`, `parameter`, `value`, `updated_at`) 
        VALUES (NULL, 'PF_ACTIVE_MULTI_DOMAINE', 'false', '2021-12-28 00:00:00')");

    }

    public function down(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("DELETE FROM `configuration_client` 
WHERE `configuration_client`.`parameter`='PF_ACTIVE_MULTI_DOMAINE'");
    }
}
