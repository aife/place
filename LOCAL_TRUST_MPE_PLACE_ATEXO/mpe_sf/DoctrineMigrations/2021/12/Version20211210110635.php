<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211210110635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17978 - [Index][BDD] Ajouter les index sur les tables ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Echange (consultation_id, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS piece_idx on EchangePieceJointe (piece, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consulation_idx on historiques_consultation (consultation_id, organisme);");
        $this->addSql("CREATE INDEX IF NOT EXISTS untrusted_date_idx on Offres (untrusteddate);");
        $this->addSql("CREATE INDEX IF NOT EXISTS uuid_idx on t_contrat_titulaire(uuid);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on questions_dce (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_numero_reponse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on AnnonceJAL (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Annonce_Press (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on CertificatChiffrement (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on CertificatChiffrement (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on ConsultationHistoriqueEtat (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Criteres_Evaluation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on DecisionLot (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Pieces_Mise_Disposition (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on  ReferentielFormXml (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on decisionEnveloppe (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on resultat_analyse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on resultat_analyse_decision (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_cons_lot_contrat (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_decision_selection_entreprise (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_document (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on trace_operations_inscrit (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on  t_CAO_Commission_Consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_candidature_mps (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on EncherePmi (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Referentiel_Consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on InvitationConsultationTransverse (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on ModeleFormulaire (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on Passation_consultation (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on t_espace_collaboratif (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on CategorieLot (consultation_id);");
        $this->addSql("CREATE INDEX IF NOT EXISTS consultation_idx on consultation_favoris (id_consultation);");

    }

    public function down(Schema $schema): void
    {
        $this->addSql("CREATE INDEX IF NOT EXISTS echange_organisme_idx on echange (id, organisme);");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on echange;");
        $this->addSql("DROP INDEX IF EXISTS piece_idx on EchangePieceJointe;");
        $this->addSql("DROP INDEX IF EXISTS consulation_idx on historiques_consultation;");
        $this->addSql("DROP INDEX IF EXISTS untrusted_date_idx on Offres;");
        $this->addSql("DROP INDEX IF EXISTS uuid_idx on t_contrat_titulaire;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on questions_dce;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_numero_reponse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on AnnonceJAL");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on Annonce_Press;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on CertificatChiffrement");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on CertificatChiffrement");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on ConsultationHistoriqueEtat;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on Criteres_Evaluation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on DecisionLot;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on Pieces_Mise_Disposition;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on  ReferentielFormXml;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on decisionEnveloppe;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on resultat_analyse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on resultat_analyse_decision;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_cons_lot_contrat;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_decision_selection_entreprise;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_document;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on trace_operations_inscrit;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on  t_CAO_Commission_Consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_candidature_mps;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on EncherePmi;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on Referentiel_Consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on InvitationConsultationTransverse;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on ModeleFormulaire;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on Passation_consultation;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on t_espace_collaboratif;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on CategorieLot;");
        $this->addSql("DROP INDEX IF EXISTS consultation_idx on consultation_favoris;");

    }
}
