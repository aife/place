<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210701123957 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16604 - Add fileName field to add new custom document template';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `document_template_surcharge` ADD COLUMN IF NOT EXISTS `file_name` VARCHAR(100) NOT NULL;");
        $this->addSql("ALTER TABLE `document_template` ADD COLUMN IF NOT EXISTS `is_custom` BOOLEAN NOT NULL DEFAULT 0;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `document_template` DROP `is_custom`;");
        $this->addSql("ALTER TABLE `document_template_surcharge` DROP `file_name`;");
    }
}
