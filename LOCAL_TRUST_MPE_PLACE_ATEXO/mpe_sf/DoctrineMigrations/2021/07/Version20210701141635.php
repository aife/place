<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210701141635 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-166 - [PLACE] Traduction anglaise absente - Cas d\'un dépôt avec limitation à 1 Go';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('INFORMATIONS_IMPORTANTE_TAILLE_DEPOT', 
        'For your answer : <br />&nbsp;&nbsp;&nbsp;&nbsp;1. Each document should not exceed 1GB<br/>&nbsp;&nbsp;&nbsp;&nbsp;2 .Prefer documents from your workstation.<br />&nbsp;&nbsp;&nbsp;&nbsp;3. For responses comprising a large number of documents &nbsp;&nbsp;&nbsp;&nbsp;compressed files (.zip, .rar, .7zip, ...)', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('DEFINE_MESSAGE_SE_CONFORMER_REGLEMENT_CONSULTATION_POUR_CONNAITRE_LISTE_PIECES_A_TRANSMETTRE', 
        'Please comply with the Download Notice for the list of documents to be sent.', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('FICHIER_DEPASSE_TAILLE', 
        'The size of the file submited is greater than that authorized on the platform (1GB). ', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('ERREUR_LORS_DE_LA_VALIDATION_DU_DEPOT_D_UNE_REPONSE_ELECTRONIQUE', 
        'Error while validating the submitting of an electronic response', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('ERREUR_LORS_DE_LA_VALIDATION_DU_DEPOT_D_UNE_REPONSE_ELECTRONIQUE_ERROR_MESSAGE_FIRST', 
        '<![CDATA[WARNING: One or more files exceed the authorized size limit (1GB) on this consultation and therefore were not added to your answer. <br/><b>List of affected files: </b>]]>', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('ERREUR_LORS_DE_LA_VALIDATION_DU_DEPOT_D_UNE_REPONSE_ELECTRONIQUE_ERROR_MESSAGE_SECOND', 
        'In the case of compressed files (.zip for example) whose size is greater than 1 GB, prefer splitting into several compressed files of size less than 1 GB.', 
        '2');");

        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('ERREUR_LORS_DE_LA_VALIDATION_DU_DEPOT_D_UNE_REPONSE_ELECTRONIQUE_ERROR_MESSAGE_THIRD', 
        'Please remove any errored files before re-validating your answer. ', 
        '2');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
