<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210719090346 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16725 - [Pub] Configuration publicité JAL';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `ProcedureEquivalence` ADD COLUMN `autoriser_publicite_non` VARCHAR(2) NOT NULL DEFAULT '+0';");
        $this->addSql("ALTER TABLE `ProcedureEquivalence` ADD COLUMN `autoriser_publicite_oui` VARCHAR(2) NOT NULL DEFAULT '+1';");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `ProcedureEquivalence` DROP `autoriser_publicite_non`;");
        $this->addSql("ALTER TABLE `ProcedureEquivalence` DROP `autoriser_publicite_oui`;");
    }
}
