<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210712122533 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16555 - [Analyse des offres] Rendre modulaire l\'accès à la page d\'analyse des offres ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` ADD `analyse_offres` TINYINT NOT NULL DEFAULT '1' ;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `configuration_organisme` DROP `analyse_offres`;");
    }
}
