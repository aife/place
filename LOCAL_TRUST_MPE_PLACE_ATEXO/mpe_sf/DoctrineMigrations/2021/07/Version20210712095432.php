<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210712095432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-16770';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE configuration_organisme ADD COLUMN `module_bi_premium` BOOLEAN NOT NULL DEFAULT \'0\' COMMENT \'Permet d’activer le mode premium du module BI\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` DROP `module_bi_premium`');
    }
}
