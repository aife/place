<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210422144223 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE−15428: [Refonte JNLP] Gestion des bi-clés - Intégration du bloc de statut et modification du lien d\'ajout d\'un bi-clé';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/cryptographie/mpeMock/dual-key\', \'POST\', \'POST_MAMP\');');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('DELETE FROM web_service WHERE nom_ws=\'POST_MAMP\';');
    }
}
