<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210429081852 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15618 - [PLACE] RdR 2021-01.00.00 - Footer - Entreprise en anglais : page "Personal data" vide';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $text = "<!--Debut Bloc Conditions utilisation-->
	<h3 id=\'rubrique_1_paragraphe_3\'>Personal data</h3>
	<p>Lexicon :</p>
	<p><strong>Processing of personal data</strong> is an operation or an organized set of operations carried out on personal data (collection, structuring, storage, modification, communication...).</p>
	<p><strong>Personal data</strong> is information that makes it possible to identify a human being (natural person), either directly (e.g. his name/first name), or indirectly (e.g. his telephone number, his contract number, his pseudonym).</br>
		The data subject is the person who can be identified by the data used in the processing of personal data.</p>
	<p>The <strong>data controller</strong> is the person who decides how the processing of personal data will be implemented, in particular by determining the purposes and means of the processing.</p>
	<p>The <strong>processor</strong> is, where appropriate, the one who carries out operations on the data on behalf of the data controller. In particular, it has the technical and organizational guarantees enabling it to process the personal data entrusted to it in accordance with the regulations.</p>
	<p>The Ministry of Action and Public Accounts processes and stores the following personal data of the users of the buyer profile : 
	<ul class=\'liste-actions\'>
		<li>Identity (2 to 5 years).</li>
		<li>Connection data (2 to 5 years).</li>
		<li>Other: recordings of user calls within the support, application traces (6 to 12 months).</li>
	</ul></p>
	<p>These data are mandatory to use the functionalities of PLACE and any false or irregular declaration engages the responsibility of the users concerned. Failure to fill in the mandatory fields of this information means that the platform cannot be used.</br>
		Only personal data that is strictly useful is collected and processed and is not kept beyond the time necessary for the operations for which it was collected</p>
	<p>In the context of the use of PLACE, the Ministry of Action and Public Accounts implements a processing of personal data on the basis of Article 6.1 c) of Regulation No. 2016/679 of 27 April 2016 on the protection of individuals with regard to the processing of personal data and on the free movement of such data, and repealing Directive 95/46/EC known as the General Regulation on Data Protection (RGPD), and after obtaining the consent of users (only for the sending of satisfaction surveys).</p>
	<p>Personal data may be communicated by the Ministry of Action and Public Accounts to sub-contractors, as defined in the GDPP. As such, the sub-contractors concerned have undertaken in the context of their relationship with the Ministry to comply with the RGPD. The Ministry of Action and Public Accounts has endeavoured to select subcontractors with the necessary guarantees.</p>
	<p>These data are treated confidentially and are only communicated to public officials (buyers), application manager (operation, maintainer), external auditors and controllers (financial jurisdictions and control bodies, QA-SSI) and the Ministry\'s personal data protection officer (DPD)</p>
	<p>This personal data is used by the Ministry to enable the dematerialization of public procurement procedures The Ministry of Action and Public Accounts takes, in view of the nature of personal data and the risks involved in processing, appropriate technical, physical and organizational measures to ensure the protection of personal data and prevent it from being lost, altered, destroyed or accidentally or unlawfully accessed by unauthorized third parties In accordance with the provisions of the RGPD, the users concerned have dedicated rights with respect to their personal data, in particular: a right of information, a right of access, a right of rectification, a right of deletion, a right of limitation. These rights may be exercised by contacting the Data Protection Officer (DPO) at :
	<ul>
		<li><a href=\'mailto:le-delegue-a-la-protection-des-donnees-personnelles@finances.gouv.fr\'>le-delegue-a-la-protection-des-donnees-personnelles@finances.gouv.fr</a></li>
	</ul>
	</p>
	<p>As well as directly with the data controller:
	<ul class=\'liste-actions\'>
		<li>By email : <a href=\'mailto:place.dae@finances.gouv.fr\'>place.dae@finances.gouv.fr</a></li>
		<li>By mail : Direction des Achats de l’État
			</br><span style=\'margin-left:83px\'>59 Boulevard Vincent Auriol</span>
			</br><span style=\'margin-left:83px\'>75013 Paris</span>
		</li>
	</ul>
	</p>
	<p>To facilitate the process, each user concerned, when sending a request to exercise rights, is invited to :
	<ul class=\'liste-actions\'>
		<li>Indicate which right(s) they wish to exercise,</li>
		<li>Mention his surname / first name / contact details to which he would like to receive answers,</li>
		<li>Attach a copy of your ID card.</li>
	</ul>
	</p>
<p>For more information, the CNIL has created a section dedicated to the understanding of rights: <a target=\'_blank\' href=\'https://www.cnil.fr/en/home\'>www.cnil.fr</a></p>";

        $this->addSql("update `configuration_messages_traduction` 
set  `target` = '".$text."' 
where source = 'TEXT_PROTECTION_DONNEES_A_CARACTERE_PERSONNEL' 
and `langue_id` = '2';");
    }

}
