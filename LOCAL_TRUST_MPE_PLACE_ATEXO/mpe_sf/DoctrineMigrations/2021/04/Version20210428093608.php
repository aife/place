<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210428093608 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-16074 - [PLACE] RdR 2021-02.01.00 - Entreprise (page "Mentions légales) - retrait de libellés';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("UPDATE `configuration_client` 
SET `value` = '&lt;p&gt;&lt;strong&gt;Ministère des finances et des comptes publics&lt;/strong&gt;&lt;br /&gt;' 
WHERE `configuration_client`.`parameter` = 'INFOSITE';");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `configuration_client` 
SET `value` = '&lt;p&gt;&lt;strong&gt;Ministère des finances et des comptes publics&lt;/strong&gt;&lt;br /&gt;
			Direction des achats de l\'Etat (DAE)&lt;br /&gt;
			59, boulevard Vincent Auriol &lt;br /&gt;
			75703 Paris Cedex 13&lt;br /&gt;
			Téléphone  +33 1 44 97 34 53  &lt;br /&gt;
			Télécopie  +33 1 44 97 07 32 &lt;/p&gt;' 
WHERE `configuration_client`.`parameter` = 'INFOSITE';");
    }
}
