<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210408145803 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15524 - Add multi main documents in echange doc application client';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `echange_doc_application_client` ADD IF NOT EXISTS `multi_docs_principaux` TINYINT(1) NOT NULL DEFAULT '0'");
        ///$this->addSql("INSERT INTO `echange_doc_application_client` (id, echange_doc_application_id, code, libelle, actif, cheminement_signature, cheminement_ged, cheminement_sae, cheminement_tdt, classification_1, classification_2, classification_3, classification_4, classification_5, multi_docs_principaux) VALUES (NULL, '2', 'PASTELL_V2_PDM_MULTIDOCS_01', 'Pastell – Flux pièces de marché multi-documents (Modèle)', '1', '1', '1', '1', '0', 'SOUS_TYPE_PARAPHEUR', 'EO', NULL, NULL, NULL, '1');");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `echange_doc_application_client` DROP IF EXISTS `multi_docs_principaux`;");
        $this->addSql("DELETE FROM `echange_doc_application_client` WHERE `code` = 'PASTELL_V2_PDM_MULTIDOCS_01'");
    }
}
