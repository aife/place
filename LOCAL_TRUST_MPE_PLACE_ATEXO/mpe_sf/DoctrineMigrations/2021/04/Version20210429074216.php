<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210429074216 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15835 - [PLACE] RdR 2021-02.10.00 - PKCS#7 - Le message dans la pop-in pré-dépôt n\'est pas celui de la SFD';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO configuration_messages_traduction (source, target, langue_id) 
VALUES ('INFORMATIONS_IMPORTANTE_TAILLE_DEPOT', 'Pour votre réponse : <br />&nbsp;&nbsp;&nbsp;&nbsp;1. Chaque document ne doit pas dépasser 1Go<br/>&nbsp;&nbsp;&nbsp;&nbsp;2 .Privilégiez le dépôt de documents depuis votre poste de travail directement<br />&nbsp;&nbsp;&nbsp;&nbsp;3. Pour les réponses comprennant de très nombreuses pièces, privilégiez les dossiers &nbsp;&nbsp;&nbsp;&nbsp;compressés (.zip, .rar, .7zip, ...)', '1')");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM configuration_messages_traduction WHERE source = 'INFORMATIONS_IMPORTANTE_TAILLE_DEPOT' AND langue_id = 1 ;"
        );

    }
}
