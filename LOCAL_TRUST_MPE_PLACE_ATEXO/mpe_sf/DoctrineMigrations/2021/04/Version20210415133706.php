<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210415133706 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15906 - Add description for unicite_mail_agent field';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `configuration_plateforme` CHANGE `unicite_mail_agent` `unicite_mail_agent` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Permet de désactiver le contrôle d’unicité sur le mail des agent';");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `configuration_plateforme` CHANGE `unicite_mail_agent` `unicite_mail_agent` TINYINT(1) NOT NULL DEFAULT '1';");
    }
}
