<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210401165537
 * @package App\Migrations
 */
final class Version20210413133637 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15768 - AR non envoyé sur la plateforme HM pour le Ville de Saint Tropez';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE configuration_client SET parameter=REPLACE(parameter, '-', '_') WHERE parameter LIKE 'PF_URL_AGENT_%';");
    }

    public function down(Schema $schema) : void
    {

    }
}
