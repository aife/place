<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210427140052 extends AbstractMigration
{

    public function getDescription() : string
    {
        return 'MPE-16054 - Ajout du nouvel Agent technique';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO `Agent` (`login`, `organisme`, `service_id`, `type_comm`,`actif`, `password`, `technique`) VALUES ('mamp', NULL, 0, '2', '1', '".uniqid().date('Ymdhis')."','1') ");
        $this->addSql("INSERT IGNORE INTO `habilitation_agent_ws` (`agent_id`, `web_service_id`) select a.id, ws.id from Agent a, web_service ws where a.login = 'mamp' AND nom_ws= 'POST_MAMP'");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM `habilitation_agent_ws` where `agent_id` = (select a.id from Agent a where a.login= 'mamp') AND `web_service_id`= (select ws.id from web_service ws where ws.nom_ws='POST_MAMP')");
        $this->addSql("DELETE FROM Agent where login = 'mamp'");
    }

}