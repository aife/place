<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210406103258
 * @package App\Migrations
 */
final class Version20210406103258 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15387 - [Pub] Gestion des informations de contact de l\'agent';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `t_consultation_compte_pub` ADD IF NOT EXISTS `telephone` VARCHAR(255) NULL DEFAULT NULL;');
        $this->addSql('ALTER TABLE `t_consultation_compte_pub` ADD IF NOT EXISTS `email` VARCHAR(255) NULL DEFAULT NULL;');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `t_consultation_compte_pub` DROP IF EXISTS `telephone`;');
        $this->addSql('ALTER TABLE `t_consultation_compte_pub` DROP IF EXISTS `email`;');
    }
}
