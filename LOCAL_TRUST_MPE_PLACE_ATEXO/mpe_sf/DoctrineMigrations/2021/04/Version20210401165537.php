<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20210401165537
 * @package App\Migrations
 */
final class Version20210401165537 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15699 - ALPI PROD : Erreur lors de l\'ouverture de 4 plis sur 6 ref MAPA2021-02';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE `Offres` o INNER JOIN `blobOrganisme_file` b ON o.id_blob_xml_reponse = b.old_id and o.organisme=b.organisme SET o.id_blob_xml_reponse = b.id WHERE o.id_blob_xml_reponse IS NOT NULL AND o.statut_offres!=\'99\' AND b.old_id IS NOT NULL ;');
    }

    public function down(Schema $schema) : void
    {

    }
}
