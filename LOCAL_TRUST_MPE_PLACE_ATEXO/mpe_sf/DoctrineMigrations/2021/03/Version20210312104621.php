<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210312104621 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-14664 : [PLACE - PKCS#7] Ajout d\'un contrôle sur la taille des fichiers déposés en réponse à une consultation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` 
                           ADD COLUMN `controle_taille_depot` integer NULL DEFAULT 1000 
                           COMMENT \'Fixe la taille des fichiers du dépôt en MO\';');
        $this->addSql('ALTER TABLE `consultation` 
                           ADD COLUMN `controle_taille_depot` integer NULL 
                           COMMENT \'Fixe la taille des fichiers du dépôt en MO\';');

    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `configuration_plateforme` drop column `controle_taille_depot`;');
        $this->addSql('ALTER TABLE `consultation` drop column `controle_taille_depot`;');
    }
}
