<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210323111906 extends AbstractMigration
{
    public function getDescription() : string
    {
        return "MPE-15489 : CDL - Erreur 500 à l'appel de WS";
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`) VALUES ( \'/api/{version}/contrats.{format}\', \'GET\', \'get contrats\');');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DELETE FROM web_service WHERE route_ws=\'/api/{version}/contrats.{format}\';');

    }
}
