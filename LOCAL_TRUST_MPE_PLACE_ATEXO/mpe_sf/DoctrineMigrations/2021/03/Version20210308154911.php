<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210308154911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'PLACE - RGPD - Apparition de la Popin après création d’un compte agent';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE Agent ALTER rgpd_communication_place SET DEFAULT NULL");
        $this->addSql("ALTER TABLE Agent ALTER rgpd_enquete SET DEFAULT NULL");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_communication_place SET DEFAULT NULL");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_enquete SET DEFAULT NULL");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_communication SET DEFAULT NULL");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE Agent ALTER rgpd_communication_place SET DEFAULT false");
        $this->addSql("ALTER TABLE Agent ALTER rgpd_enquete SET DEFAULT false");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_communication_place SET DEFAULT false");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_communication_place SET DEFAULT false");
        $this->addSql("ALTER TABLE Inscrit ALTER rgpd_communication SET DEFAULT false");

    }
}
