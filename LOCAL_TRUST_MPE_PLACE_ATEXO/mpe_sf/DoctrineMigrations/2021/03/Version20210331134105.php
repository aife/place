<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210331134105 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-15217 - [Menu Agent] Modification de la configuration du module plateforme menu_agent_complet';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('update configuration_plateforme set menu_agent_complet = 0;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('update configuration_plateforme set menu_agent_complet = 1;');
    }
}
