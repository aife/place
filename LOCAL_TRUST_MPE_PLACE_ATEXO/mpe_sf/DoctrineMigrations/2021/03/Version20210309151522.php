<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210309151522 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15192 - PLACE - RGPD - Mise à jour du Footer (entreprise et agent)';
    }

    public function up(Schema $schema) : void
    {
        $text = "
<!--Debut Bloc Conditions utilisation-->
	<h3 id=rubrique_1_paragraphe_3>Protection des données à caractère personnel</h3>
	<p>Lexique :</p>
	<p>Un <strong>traitement de données à caractère personnel</strong> est une opération ou un ensemble organisé d’opérations effectué sur des données à caractère personnel (collecte, structuration, conservation, modification, communication...).</p>
	<p>Une <strong>donnée à caractère personnel</strong> est une information qui permet d’identifier un être humain (personne physique), directement (par exemple son nom/prénom), ou indirectement (par exemple son numéro de téléphone, son numéro de contrat, son pseudonyme).</br>
		La personne concernée est celle qui peut être identifiée par les données utilisées dans le cadre du traitement de données à caractère personnel.</p>
	<p>Le <strong>responsable de traitement</strong> est celui qui décide de la manière dont sera mis en œuvre le traitement des données à caractère personnel, notamment en déterminant les fins et les moyens du traitement.</p>
	<p>Le <strong>sous-traitant</strong> est, le cas échéant, celui qui effectue des opérations sur les données pour le compte du responsable de traitement. Il dispose notamment des garanties techniques et organisationnelles, lui permettant de traiter les données à caractère personnel qui lui sont confiées conformément à la règlementation.</p>
	<p>Le Ministère de l’action et des comptes publics traite et conserve les données à caractère personnel des utilisateurs du profil d’acheteur suivantes :
	<ul class=liste-actions>
		<li>L''identité (2 à 5 ans) ;</li>
		<li>Les données de connexion (2 à 5 ans) ;</li>
		<li>Autres : enregistrements des appels des utilisateurs dans le cadre du support, traces applicatives (6 à 12 mois).</li>
	</ul></p>
	<p>Ces données sont obligatoires pour utiliser les fonctionnalités de PLACE et la constitution d’un annuaire des professionnels ; toute déclaration fausse ou irrégulière engage la responsabilité des utilisateurs concernés. À défaut d’avoir renseigné les champs obligatoires de ces informations, la plateforme ne peut pas être utilisée.</br>
		Seules les données à caractère personnel qui sont strictement utiles sont collectées et traitées et ne sont pas conservées au-delà de la durée nécessaire pour les opérations pour lesquelles elles ont été collectées.</p>
	<p>Dans le cadre de l’utilisation de PLACE, le Ministère de l’action et des compte publics met en œuvre un traitement de données à caractère personnel sur le fondement de l’article 6.1 c) du règlement n° 2016/679 du 27 avril 2016 relatif à la protection des personnes physiques à l''égard du traitement des données à caractère personnel et à la libre circulation de ces données, et abrogeant la directive 95/46/CE dit règlement général sur la protection des données (RGPD) et après recueil du consentement des utilisateurs (notamment pour l’envoi des enquêtes de satisfaction et la constitution d’un annuaire des professionnels).</p>
	<p>Les données à caractère personnel peuvent être communiquées par le Ministère de l’action et des comptes publics à des sous-traitants, au sens du RGPD. À ce titre, les sous-traitants concernés se sont engagés dans le cadre de leur relation avec le Ministère à respecter le RGPD. Le Ministère de l’action et des comptes publics s’est attaché à sélectionner des sous-traitants présentant les garanties nécessaires.</p>
	<p>Ces données sont traitées de manière confidentielle et ne sont communiquées qu’aux agents publics (acheteurs), gestionnaires de l''application (exploitation, mainteneur), auditeurs et contrôleurs externes (juridictions financières et corps de contrôle, AQ-SSI) et au délégué à la protection des données à caractère personnel du ministère (DPD).</p>
	<p>Conformément aux dispositions du RGPD, les utilisateurs concernés disposent de droits dédiés sur leurs données à caractère personnel, notamment : un droit d’information, un droit d’accès, un droit de rectification, un droit d’effacement, un droit de limitation. Ces droits peuvent être exercés auprès du délégué à la protection des données (DPD) :
	<ul>
		<li><a href=mailto:le-delegue-a-la-protection-des-donnees-personnelles@finances.gouv.fr>le-delegue-a-la-protection-des-donnees-personnelles@finances.gouv.fr</a></li>
	</ul>
	</p>
	<p>Ainsi que directement auprès du responsable de traitement :
	<ul class=liste-actions>
		<li>Par email : <a href=mailto:place.dae@finances.gouv.fr>place.dae@finances.gouv.fr</a></li>
		<li>Par courrier : Direction des Achats de l’État
			</br><span style=margin-left:83px>59 Boulevard Vincent Auriol</span>
			</br><span style=margin-left:83px>75013 Paris</span>
		</li>
	</ul>
	</p>
	<p>Pour faciliter les démarches, chaque utilisateur concerné, lors de l’envoi d’une demande d’exercice des droits, est invité à :
	<ul class=liste-actions>
		<li>Indiquer quel(s) droit(s) il souhaite exercer,</li>
		<li>Mentionner ses noms / prénoms / coordonnées auxquels il souhaite recevoir les réponses,</li>
		<li>Joindre une copie de sa pièce d’identité</li>
	</ul>
	</p>
<p>Utilisation de témoins de connexion (« cookies »)</p>
<ul>
<li>Un témoin de connexion, aussi appelé cookie, est une suite d''informations, généralement de petite taille et identifié par un nom, qui peut être transmis à votre navigateur par un site web sur lequel vous vous connectez. Votre navigateur web le conservera pendant une certaine durée, et le renverra au serveur web chaque fois que vous vous y reconnecterez.</li>
</ul>
<p>Témoins de connexions internes utilisés par la plateforme</p>
<ul>
<li>Ces témoins permettent au site de fonctionner de manière nominale pour un utilisateur authentifié. Vous pouvez vous y opposer et les supprimer en utilisant les paramètres de votre navigateur, cependant votre expérience utilisateur risque d’être très dégradée. En effet, la connexion à votre compte personnel et aux fonctionnalités réservées aux utilisateurs connectés vous seront impossibles.</li>
</ul>
<table class=protection-donnees-table>
    <tr>
        <th>Nom du témoin</th>
        <th>Finalité</th>
        <th>Durée de conservation</th>
    </tr>
    <tr>
        <td>PHPSESSID</td>
        <td>Récupère ou définit l’identifiant de session pour la session courante de l’utilisateur</td>
        <td>session</td>
    </tr>
    <tr>
        <td>SERVERID</td>
        <td>Détermine le serveur de l’application sur lequel l’utilisateur est connecté</td>
        <td>session</td>
    </tr>
</table>";
        $this->addSql("insert into `configuration_messages_traduction` values ('', 'TEXT_PROTECTION_DONNEES_A_CARACTERE_PERSONNEL', '".$text."', '1');");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("delete from `configuration_messages_traduction` WHERE `source` LIKE 'TEXT_PROTECTION_DONNEES_A_CARACTERE_PERSONNEL' AND langue_id = '1'");
    }
}
