<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210330071909 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14792 - [PLACE] Evolution T1 2021 - Made in France';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Offres ADD taux_production_france Integer DEFAULT null');
        $this->addSql('ALTER TABLE Offres ADD taux_production_europe Integer DEFAULT null');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Offres DROP taux_production_france');
        $this->addSql('ALTER TABLE Offres DROP taux_production_europe');

    }
}
