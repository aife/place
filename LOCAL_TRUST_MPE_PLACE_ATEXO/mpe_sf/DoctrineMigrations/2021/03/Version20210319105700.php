<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210319105700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15597 - Admin procédure simplifiée MAPA';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('Alter table Type_Procedure_Organisme ADD COLUMN IF NOT EXISTS procedure_simplifie tinyint(1) NOT NULL DEFAULT 0;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('Alter table Type_Procedure_Organisme DROP procedure_simplifie;');
    }
}
