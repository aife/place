<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210301173124 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14937 - Problème caractères spéciaux onglet "calendrier"';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE `t_calendrier_etape` SET LIBELLE = 'Analyse des réponses'  WHERE `LIBELLE` = 'Analyse des rÃ©ponses'");
        $this->addSql("UPDATE `t_calendrier_etape` SET LIBELLE = 'Notification de la décision'  WHERE `LIBELLE` = 'Notification de la dÃ©cision'");
        $this->addSql("UPDATE `t_calendrier_etape` SET LIBELLE = 'Décision / Attribution'  WHERE `LIBELLE` = 'DÃ©cision / Attribution'");

        $this->addSql("UPDATE `t_calendrier_etape_referentiel` SET LIBELLE = 'Analyse des réponses'  WHERE `LIBELLE` = 'Analyse des rÃ©ponses'");
        $this->addSql("UPDATE `t_calendrier_etape_referentiel` SET LIBELLE = 'Notification de la décision'  WHERE `LIBELLE` = 'Notification de la dÃ©cision'");
        $this->addSql("UPDATE `t_calendrier_etape_referentiel` SET LIBELLE = 'Décision / Attribution'  WHERE `LIBELLE` = 'DÃ©cision / Attribution'");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
