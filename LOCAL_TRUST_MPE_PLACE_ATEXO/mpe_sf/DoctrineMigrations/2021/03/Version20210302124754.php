<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210302124754 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-15370 - [AlerteEntreprise] - Error log - lors d\'un envoie de message';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $message = addslashes('O:36:\\"Symfony\\\\Component\\\\Messenger\\\\Envelope\\":2:{s:44:'
            . '\\"\\0Symfony\\\\Component\\\\Messenger\\\\Envelope\\0stamps\\";a:1:{s:46:\\"Symfony\\\\Component'
            . '\\\\Messenger\\\\Stamp\\\\BusNameStamp\\";a:1:{i:0;O:46:\\"Symfony\\\\Component\\\\Messenger'
            . '\\\\Stamp\\\\BusNameStamp\\":1:{s:55:\\"\\0Symfony\\\\Component\\\\Messenger\\\\Stamp\\\\BusNameStamp'
            . '\\0busName\\";s:21:\\"messenger.bus.default\\";}}}s:45:\\"\\0Symfony\\\\Component\\\\Messenger'
            . '\\\\Envelope\\0message\\";O:29:\\"App\\\\Message\\\\CommandeMessenger\\":2:{s:39:\\"\\0App\\\\Message'
            . '\\\\CommandeMessenger\\0commande\\";s:29:\\"mpe:redressement-xml-criteria\\";s:37:\\"\\0App'
            . '\\\\Message\\\\CommandeMessenger\\0params\\";a:0:{}}}');

        $sql = 'INSERT INTO messenger_messages 
        (body, headers, queue_name, created_at, available_at, delivered_at) 
        SELECT DISTINCT \'' . $message . '\', \'[]\', \'default\', CURRENT_DATE, CURRENT_DATE, null;';
        $this->addSql($sql);
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('DELETE FROM messenger_messages
         WHERE body LIKE \'%mpe:redressement-xml-criteria%\' and delivered_at is null;');
    }
}
