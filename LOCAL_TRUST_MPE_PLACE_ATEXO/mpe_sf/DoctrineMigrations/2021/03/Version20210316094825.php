<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210316094825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15457 - [PLACE] - lien Fiche de recensement KO';
    }

    public function up(Schema $schema) : void
    {
       $this->addSql("UPDATE configuration_plateforme SET decision_fiche_recensement = '0'");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE configuration_plateforme SET decision_fiche_recensement = '1'");

    }
}
