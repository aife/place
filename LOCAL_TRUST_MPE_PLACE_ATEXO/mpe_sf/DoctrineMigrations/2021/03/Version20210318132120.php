<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210318132120 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-13935 Génération documentaire : gestion du lotissement (1/2 : IHM)';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD `lot_id` INT UNSIGNED NULL DEFAULT NULL");

        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD CONSTRAINT `genere_piece_consultation_CategorieLot_lot_fk` 
 FOREIGN KEY ( `id` ) REFERENCES CategorieLot(id) ON DELETE NO ACTION;");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql("ALTER TABLE `piece_genere_consultation` DROP `lot_id`");

    }
}
