<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210331125159 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15136 - Ajout d\'un nouveau template pour génération documentaire : le NOTI 6';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("UPDATE `document_template` SET `nom_afficher` = 'NOTI6 - Certificat de cessibilité de créance(s)' WHERE `document` = 'NOTI6'");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("UPDATE `document_template` SET `nom_afficher` = 'NOTI 6 - Certificat de cessibilité de créance(s)' WHERE `document` = 'NOTI6'");
    }
}
