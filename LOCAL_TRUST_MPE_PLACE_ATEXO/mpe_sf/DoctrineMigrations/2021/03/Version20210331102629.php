<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210331102629 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15136 - Ajout d\'un nouveau template pour génération documentaire : le NOTI 6';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("INSERT INTO `document_template` (document, nom, nom_afficher) VALUES ('NOTI6', 'cessibilite_creances' , 'NOTI 6 - Certificat de cessibilité de créance(s)')");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("DELETE FROM `document_template` where `document` = 'NOTI6'");
    }
}
