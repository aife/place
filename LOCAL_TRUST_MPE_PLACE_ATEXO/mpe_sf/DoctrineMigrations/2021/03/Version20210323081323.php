<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210323081323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-13938 Génération documentaire : gestion du soumissionnaire (1/2 : IHM)';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD `entreprise_id` INT NULL DEFAULT NULL");
        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD CONSTRAINT `genere_piece_consultation_entreprise_id_fk` 
FOREIGN KEY (`entreprise_id`) REFERENCES Entreprise(`id`) ON DELETE NO ACTION;");

        $this->addSql('ALTER TABLE `piece_genere_consultation` DROP FOREIGN KEY IF EXISTS genere_piece_consultation_CategorieLot_lot_fk');
        $this->addSql("ALTER TABLE `piece_genere_consultation` DROP IF EXISTS `lot_id`");

        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD `lot_id` INT NULL DEFAULT NULL");
        $this->addSql("ALTER TABLE `piece_genere_consultation` ADD CONSTRAINT `genere_piece_consultation_CategorieLot_lot_fk` 
 FOREIGN KEY ( `lot_id` ) REFERENCES CategorieLot(id) ON DELETE NO ACTION;");
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `piece_genere_consultation` DROP FOREIGN KEY genere_piece_consultation_entreprise_id_fk');
        $this->addSql("ALTER TABLE `piece_genere_consultation` DROP `entreprise_id`");
    }
}
