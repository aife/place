<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210319062412 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15217 - [Menu Agent] Nouveau module plateforme : menu_agent_complet';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('Alter table configuration_plateforme ADD COLUMN menu_agent_complet tinyint(1) DEFAULT 1;');

    }

    public function down(Schema $schema) : void
    {
        $this->addSql('Alter table configuration_plateforme DROP COLUMN menu_agent_complet');

    }
}
