<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210326124136 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-15666 - Add new field unicite_mail_agent in configuration_plateforme entity';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_plateforme ADD COLUMN IF NOT EXISTS unicite_mail_agent tinyint(1) NOT NULL DEFAULT 1;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE configuration_plateforme DROP unicite_mail_agent;');
    }
}
