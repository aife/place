<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211122163641 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17807 - Suppression clé primaire composite sur la table Service';
    }

    public function up(Schema $schema): void
    {
        //Drop previous Foreign Keys
        $this->addSql('ALTER TABLE `invite_permanent_transverse` DROP FOREIGN KEY t_invite_permanent_transverse_service_id_FK;');
        $this->addSql('ALTER TABLE `EncherePmi` DROP FOREIGN KEY EncherePmi_ibfk_1;');
        $this->addSql('ALTER TABLE `Parametrage_Enchere` DROP FOREIGN KEY relation_parametrage_Enchere_Service;');
        $this->addSql('ALTER TABLE `AffiliationService` DROP FOREIGN KEY AffiliationService_Pole;');
        $this->addSql('ALTER TABLE `AffiliationService` DROP FOREIGN KEY AffiliationService_Service;');
        $this->addSql('ALTER TABLE `AffiliationService` DROP FOREIGN KEY IF EXISTS AffiliationService_ibfk_2;');
        $this->addSql('ALTER TABLE `AffiliationService` DROP FOREIGN KEY IF EXISTS AffiliationService_ibfk_3;');

        //Table Service
        $this->addSql('ALTER TABLE `Service` DROP PRIMARY KEY, CHANGE COLUMN `id` `old_id` INT(11) NULL, ADD COLUMN `id` SERIAL PRIMARY KEY;');
        $this->addSql('SELECT @max := GREATEST(count(old_id), max(old_id)) from Service;');
        $this->addSql('UPDATE Service SET id = (id + @max);');
        $this->addSql('ALTER TABLE Service ADD CONSTRAINT old_primary_key UNIQUE (old_id, organisme);');

        //Tables with Foreign Keys
        $this->addSql('ALTER TABLE `invite_permanent_transverse` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('ALTER TABLE `invite_permanent_transverse` ADD CONSTRAINT `FK_invite_permanent_transverse_service` 
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`)  ON UPDATE CASCADE;');
        $this->addSql('UPDATE invite_permanent_transverse SET service_id = (SELECT id FROM Service WHERE old_id = invite_permanent_transverse.old_service_id AND organisme = invite_permanent_transverse.acronyme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `EncherePmi` CHANGE COLUMN `idEntiteeAssociee` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `EncherePmi` ADD CONSTRAINT `FK_enchere_pmi_service` 
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;");
        $this->addSql('UPDATE EncherePmi SET service_id = (SELECT id FROM Service WHERE old_id = EncherePmi.old_service_id AND organisme = EncherePmi.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `Parametrage_Enchere` CHANGE COLUMN `idEntiteeAssociee` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `Parametrage_Enchere` ADD CONSTRAINT `FK_parametrage_enchere_service` 
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;");
        $this->addSql('UPDATE Parametrage_Enchere SET service_id = (SELECT id FROM Service WHERE old_id = Parametrage_Enchere.old_service_id AND organisme = Parametrage_Enchere.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `AffiliationService` DROP PRIMARY KEY, ADD COLUMN `id` SERIAL PRIMARY KEY, CHANGE COLUMN `id_pole` `old_id_pole` INT(11) NULL, ADD COLUMN `service_parent_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `AffiliationService` ADD CONSTRAINT `FK_affiliation_service_parent_id` 
 FOREIGN KEY ( `service_parent_id` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;");
        $this->addSql('ALTER TABLE `AffiliationService` CHANGE COLUMN `id_service` `old_id_service` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `AffiliationService` ADD CONSTRAINT `FK_affiliation_service_id` 
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`)  ON UPDATE CASCADE;");
        $this->addSql('UPDATE AffiliationService SET service_parent_id = (SELECT id FROM Service WHERE old_id = AffiliationService.old_id_pole AND organisme = AffiliationService.organisme);');
        $this->addSql('UPDATE AffiliationService SET service_id = (SELECT id FROM Service WHERE old_id = AffiliationService.old_id_service AND organisme = AffiliationService.organisme);');
        $this->addSql('ALTER TABLE `AffiliationService` MODIFY COLUMN service_id BIGINT UNSIGNED NOT NULL, MODIFY COLUMN service_parent_id BIGINT UNSIGNED NOT NULL;');

        //Tables with Doctrine links but without previous Foreign Keys
        $this->addSql('ALTER TABLE `agent_technique_association` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `agent_technique_association` ADD CONSTRAINT `FK_agent_technique_association_service`
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`)  ON UPDATE CASCADE;");
        $this->addSql('UPDATE agent_technique_association SET service_id = (SELECT id FROM Service WHERE old_id = agent_technique_association.old_service_id AND organisme = agent_technique_association.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `consultation` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `consultation` ADD CONSTRAINT `FK_consultation_service`
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`) ON UPDATE CASCADE;");
        $this->addSql('UPDATE consultation SET service_id = (SELECT id FROM Service WHERE old_id = consultation.old_service_id AND organisme = consultation.organisme) WHERE old_service_id is not null AND old_service_id != 0;');
        $this->addSql('ALTER TABLE `consultation` CHANGE COLUMN `service_associe_id` `old_service_associe_id` INT(11) NULL, ADD COLUMN `service_associe_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE consultation SET service_associe_id = (SELECT id FROM Service WHERE old_id = consultation.old_service_associe_id AND organisme = consultation.organisme) WHERE old_service_associe_id is not null AND old_service_id != 0;');

        //Tables with service field but without Foreign Keys
        $this->addSql('ALTER TABLE `AcheteurPublic` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE AcheteurPublic SET service_id = (SELECT id FROM Service WHERE old_id = AcheteurPublic.old_service_id AND organisme = AcheteurPublic.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `AdresseFacturationJal` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE AdresseFacturationJal SET service_id = (SELECT id FROM Service WHERE old_id = AdresseFacturationJal.old_service_id AND organisme = AdresseFacturationJal.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `Agent` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql("ALTER TABLE `Agent` ADD CONSTRAINT `FK_agent_service`
 FOREIGN KEY ( `service_id` ) REFERENCES `Service` (`id`);");
        $this->addSql('UPDATE Agent SET service_id = (SELECT id FROM Service WHERE old_id = Agent.old_service_id AND organisme = Agent.organisme) WHERE old_service_id is not null AND old_service_id != 0;');

        $this->addSql('ALTER TABLE `CertificatPermanent` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE CertificatPermanent SET service_id = (SELECT id FROM Service WHERE old_id = CertificatPermanent.old_service_id AND organisme = CertificatPermanent.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Chorus_groupement_achat` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Chorus_groupement_achat SET service_id = (SELECT id FROM Service WHERE old_id = Chorus_groupement_achat.old_service_id AND organisme = Chorus_groupement_achat.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Echange` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Echange SET service_id = (SELECT id FROM Service WHERE old_id = Echange.old_service_id AND organisme = Echange.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `gestion_adresses` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE gestion_adresses SET service_id = (SELECT id FROM Service WHERE old_id = gestion_adresses.old_service_id AND organisme = gestion_adresses.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Groupe_Moniteur` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Groupe_Moniteur SET service_id = (SELECT id FROM Service WHERE old_id = Groupe_Moniteur.old_service_id AND organisme = Groupe_Moniteur.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Historique_suppression_agent` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Historique_suppression_agent SET service_id = (SELECT id FROM Service WHERE old_id = Historique_suppression_agent.old_service_id AND organisme = Historique_suppression_agent.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `JAL` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE JAL SET service_id = (SELECT id FROM Service WHERE old_id = JAL.old_service_id AND organisme = JAL.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Mandataire_service` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Mandataire_service SET service_id = (SELECT id FROM Service WHERE old_id = Mandataire_service.old_service_id AND organisme = Mandataire_service.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Marche` CHANGE COLUMN `idService` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Marche SET service_id = (SELECT id FROM Service WHERE old_id = Marche.old_service_id AND organisme = Marche.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `MarchePublie` CHANGE COLUMN `idService` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE MarchePublie SET service_id = (SELECT id FROM Service WHERE old_id = MarchePublie.old_service_id AND organisme = MarchePublie.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Mesure_avancement` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Mesure_avancement SET service_id = (SELECT id FROM Service WHERE old_id = Mesure_avancement.old_service_id AND organisme = Mesure_avancement.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `ModeleFormulaire` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE ModeleFormulaire SET service_id = (SELECT id FROM Service WHERE old_id = ModeleFormulaire.old_service_id AND organisme = ModeleFormulaire.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Numerotation_ref_cons_auto` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Numerotation_ref_cons_auto SET service_id = (SELECT id FROM Service WHERE old_id = Numerotation_ref_cons_auto.old_service_id AND organisme = Numerotation_ref_cons_auto.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Operations` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Operations SET service_id = (SELECT id FROM Service WHERE old_id = Operations.old_service_id AND organisme = Operations.acronyme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql("
CREATE TABLE IF NOT EXISTS `Ordre_Du_Jour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisme` varchar(30) NOT NULL,
  `id_seance` int(11) NOT NULL DEFAULT 0,
  `ref_consultation` varchar(255) DEFAULT NULL,
  `ref_libre` varchar(50) DEFAULT NULL,
  `id_etape` int(11) NOT NULL,
  `intitule_ordre_du_jour` longtext DEFAULT NULL,
  `lots_odj_libre` varchar(100) DEFAULT NULL,
  `id_type_procedure` int(11) DEFAULT NULL,
  `type_procedure_libre` varchar(255) DEFAULT NULL,
  `date_cloture` datetime DEFAULT NULL,
  `type_env` int(1) DEFAULT NULL,
  `sous_pli` int(2) DEFAULT NULL,
  `heure` varchar(5) NOT NULL DEFAULT '00',
  `etape_consultation` varchar(200) NOT NULL DEFAULT '',
  `type_consultation` varchar(200) NOT NULL DEFAULT '',
  `minutes` char(2) NOT NULL DEFAULT '',
  `service` varchar(255) DEFAULT NULL,
  `id_service` int(10) DEFAULT NULL,
  `date_debut` varchar(255) DEFAULT NULL,
  `consultation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`organisme`),
  KEY `id_commission` (`id_seance`),
  KEY `ref_Consultation` (`ref_consultation`)
) ENGINE=InnoDB default CHARACTER SET utf8 COLLATE utf8_general_ci;");

        $this->addSql('ALTER TABLE `Ordre_Du_Jour` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Ordre_Du_Jour SET service_id = (SELECT id FROM Service WHERE old_id = Ordre_Du_Jour.old_service_id AND organisme = Ordre_Du_Jour.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `programme_previsionnel` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE programme_previsionnel SET service_id = (SELECT id FROM Service WHERE old_id = programme_previsionnel.old_service_id AND organisme = programme_previsionnel.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `RPA` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE RPA SET service_id = (SELECT id FROM Service WHERE old_id = RPA.old_service_id AND organisme = RPA.acronymeOrg) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `Societes_Exclues` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE Societes_Exclues SET service_id = (SELECT id FROM Service WHERE old_id = Societes_Exclues.old_service_id AND organisme = Societes_Exclues.organisme_acronyme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `sso_agent` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE sso_agent SET service_id = (SELECT id FROM Service WHERE old_id = sso_agent.old_service_id AND organisme = sso_agent.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `SuiviAcces` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE SuiviAcces SET service_id = (SELECT id FROM Service WHERE old_id = SuiviAcces.old_service_id AND organisme = SuiviAcces.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `TireurPlan` CHANGE COLUMN `id_service` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE TireurPlan SET service_id = (SELECT id FROM Service WHERE old_id = TireurPlan.old_service_id AND organisme = TireurPlan.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_contrat_titulaire` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_contrat_titulaire SET service_id = (SELECT id FROM Service WHERE old_id = t_contrat_titulaire.old_service_id AND organisme = t_contrat_titulaire.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_decision_selection_entreprise` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_decision_selection_entreprise SET service_id = (SELECT id FROM Service WHERE old_id = t_decision_selection_entreprise.old_service_id AND organisme = t_decision_selection_entreprise.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_donnees_consultation` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_donnees_consultation SET service_id = (SELECT id FROM Service WHERE old_id = t_donnees_consultation.old_service_id AND organisme = t_donnees_consultation.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_numerotation_automatique` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_numerotation_automatique SET service_id = (SELECT id FROM Service WHERE old_id = t_numerotation_automatique.old_service_id AND organisme = t_numerotation_automatique.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_publicite_favoris` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_publicite_favoris SET service_id = (SELECT id FROM Service WHERE old_id = t_publicite_favoris.old_service_id AND organisme = t_publicite_favoris.organisme) WHERE old_service_id is not null AND old_service_id > 0;');

        $this->addSql('ALTER TABLE `t_synthese_rapport_audit` CHANGE COLUMN `service_id` `old_service_id` INT(11) NULL, ADD COLUMN `service_id` BIGINT UNSIGNED NULL;');
        $this->addSql('UPDATE t_synthese_rapport_audit SET service_id = (SELECT id FROM Service WHERE old_id = t_synthese_rapport_audit.old_service_id AND organisme = t_synthese_rapport_audit.organisme) WHERE old_service_id is not null AND old_service_id > 0;');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
