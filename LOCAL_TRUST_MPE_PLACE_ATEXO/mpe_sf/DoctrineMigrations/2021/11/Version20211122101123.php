<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211122101123 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17656 - Augmentation du nombre de caractères du champs identifiant';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Inscrit` CHANGE `login` `login` VARCHAR(100) NULL DEFAULT NULL;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Inscrit` CHANGE `login` `login` VARCHAR(50) NULL DEFAULT NULL;");
    }
}
