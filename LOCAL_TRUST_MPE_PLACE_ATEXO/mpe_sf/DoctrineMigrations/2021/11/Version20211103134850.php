<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211103134850 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-315 - Ajouter un WS upload des fichiers échanges chorus';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("
                           INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
                           VALUES 
                           ('/api/{version}/echanges-chorus/upload/{idEchangeChorus}', 'POST', 'Upload fichier échange chorus', '1');
                           ");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM web_service WHERE nom_ws='Upload fichier échange chorus';");
    }
}
