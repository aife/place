<?php

declare(strict_types=1);

namespace App\Migrations;

use App\Service\AtexoMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211103143229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17541 - Mettre en place le callback pour l\'ensemble des flux avec gen-doc/only-office';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('DELETE FROM messenger_messages
         WHERE body LIKE \'%mpe:generation-document:recuperation%\' and delivered_at is null;');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        AtexoMigration::insertInMessengerMessage(
            $this->connection->createQueryBuilder(),
            'mpe:generation-document:recuperation',
            [],
            (new \DateTime('now'))->format('Y-m-d H:i:s')
        );
    }
}
