<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211130090335 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-173 : [PLACE] EXEC 1.1 - MPE - Accès au module EXEC';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("UPDATE `configuration_messages_traduction` 
SET `target` = 'Accéder à la gestion administrative des contrats' 
WHERE `configuration_messages_traduction`.`source` = 'ACCEDER_MODULE_EXEC'");
    }
}
