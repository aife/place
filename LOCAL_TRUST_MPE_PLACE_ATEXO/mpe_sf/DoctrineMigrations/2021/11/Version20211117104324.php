<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211117104324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-323 - [Evo : Autoformation] : Création des tables, entités';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE module_autoformation ADD COLUMN IF NOT EXISTS position integer NOT NULL');
        $this->addSql('ALTER TABLE langue_module_autoformation ADD COLUMN IF NOT EXISTS nom VARCHAR(100) NOT NULL');
        $this->addSql('ALTER TABLE langue_module_autoformation ADD COLUMN IF NOT EXISTS description VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE rubrique DROP COLUMN titre');
        $this->addSql('ALTER TABLE module_autoformation DROP COLUMN titre');
        $this->addSql('ALTER TABLE rubrique ADD COLUMN IF NOT EXISTS is_agent BOOLEAN NOT NULL DEFAULT false');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE module_autoformation DROP COLUMN position');
        $this->addSql('ALTER TABLE langue_module_autoformation DROP COLUMN nom');
        $this->addSql('ALTER TABLE langue_module_autoformation DROP COLUMN description');
        $this->addSql('ALTER TABLE rubrique ADD COLUMN titre VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE module_autoformation ADD COLUMN titre VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE rubrique DROP COLUMN is_agent');
    }
}
