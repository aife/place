<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211122084339 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17812 - [2021-00.06.00-RC3-SNAPSHOT] Erreur 500 création compte Agent';
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `HabilitationProfil` ADD COLUMN IF NOT EXISTS 
    `acces_ws` BOOLEAN NULL DEFAULT FALSE AFTER `exec_voir_contrats_organisme`;");
        $this->addSql("ALTER TABLE `HabilitationProfil` ADD COLUMN IF NOT EXISTS 
    `exec_modification_contrat` INT NULL DEFAULT 0 AFTER `administrer_organisme`;");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP `acces_ws`");
        $this->addSql("ALTER TABLE `HabilitationProfil` DROP `exec_modification_contrat`");
    }
}
