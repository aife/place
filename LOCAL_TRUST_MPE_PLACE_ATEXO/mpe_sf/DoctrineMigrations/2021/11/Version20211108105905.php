<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211108105905 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-321 - [Evo : Autoformation] : Nouvel habilitation : "Administration aux modules d\'autoformation"';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("ALTER TABLE `HabilitationAgent` ADD COLUMN IF NOT EXISTS  `module_autoformation` BOOLEAN NOT NULL DEFAULT FALSE;");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `HabilitationProfil` DROP `module_autoformation`');
    }
}
