<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211108165946 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-323 - [Evo : Autoformation] : Création des tables, entités';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE IF NOT EXISTS langue_module_autoformation (id INT AUTO_INCREMENT NOT NULL, langue_id INT NOT NULL, module_autoformation_id INT DEFAULT NULL, INDEX IDX_3DB2971B2AADBACD (langue_id), INDEX IDX_3DB2971BB04C3E89 (module_autoformation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS langue_rubrique (id INT AUTO_INCREMENT NOT NULL, langue_id INT NOT NULL, rubrique_id INT NOT NULL, nom VARCHAR(255) NOT NULL, INDEX IDX_96C3BBE12AADBACD (langue_id), INDEX IDX_96C3BBE13BD38833 (rubrique_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS module_autoformation (id INT AUTO_INCREMENT NOT NULL, rubrique_id INT NOT NULL, titre VARCHAR(255) NOT NULL, duree INT NOT NULL, path_picture VARCHAR(255) NOT NULL, path_video VARCHAR(255) NOT NULL, url_externe VARCHAR(255) NOT NULL, INDEX IDX_FB7D38883BD38833 (rubrique_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE IF NOT EXISTS rubrique (id INT AUTO_INCREMENT NOT NULL, titre VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE langue_module_autoformation ADD CONSTRAINT FK_3DB2971B2AADBACD FOREIGN KEY IF NOT EXISTS (langue_id) REFERENCES Langue (	id_langue)');
        $this->addSql('ALTER TABLE langue_module_autoformation ADD CONSTRAINT FK_3DB2971BB04C3E89 FOREIGN KEY (module_autoformation_id) REFERENCES module_autoformation (id)');
        $this->addSql('ALTER TABLE langue_rubrique ADD CONSTRAINT FK_96C3BBE12AADBACD FOREIGN KEY IF NOT EXISTS (langue_id) REFERENCES Langue (id_langue)');
        $this->addSql('ALTER TABLE langue_rubrique ADD CONSTRAINT FK_96C3BBE13BD38833 FOREIGN KEY IF NOT EXISTS (rubrique_id) REFERENCES rubrique (id)');
        $this->addSql('ALTER TABLE module_autoformation ADD CONSTRAINT FK_FB7D38883BD38833 FOREIGN KEY IF NOT EXISTS (rubrique_id) REFERENCES rubrique (id)');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE langue_module_autoformation DROP FOREIGN KEY FK_3DB2971B2AADBACD');
        $this->addSql('ALTER TABLE langue_rubrique DROP FOREIGN KEY FK_96C3BBE12AADBACD');
        $this->addSql('ALTER TABLE langue_module_autoformation DROP FOREIGN KEY FK_3DB2971BB04C3E89');
        $this->addSql('ALTER TABLE langue_rubrique DROP FOREIGN KEY FK_96C3BBE13BD38833');
        $this->addSql('ALTER TABLE module_autoformation DROP FOREIGN KEY FK_FB7D38883BD38833');
        $this->addSql('DROP TABLE langue_module_autoformation');
        $this->addSql('DROP TABLE langue_rubrique');
        $this->addSql('DROP TABLE module_autoformation');
        $this->addSql('DROP TABLE rubrique');
    }
}
