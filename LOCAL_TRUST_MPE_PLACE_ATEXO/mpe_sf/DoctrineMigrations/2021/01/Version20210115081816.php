<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210115081816 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14767 : [CNRS] Saisie manuelle de l\'ID externe des Agents et des Services';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` ADD `saisie_manuelle_id_externe` BOOLEAN NOT NULL DEFAULT FALSE;');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` drop column `saisie_manuelle_id_externe`;');
    }
}
