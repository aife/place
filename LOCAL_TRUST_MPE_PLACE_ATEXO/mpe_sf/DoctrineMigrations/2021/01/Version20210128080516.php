<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210128080516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-14661 -  [PLACE - PKCS#7] Modularité de l\'activation par organisme du nouveau mode de chiffrement PKCS#7 sur une consultation MPE';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` 
    ADD COLUMN `cms_actif` BOOLEAN NOT NULL DEFAULT FALSE COMMENT \'Gestion du chiffrement en PKCS#7\';');
        $this->addSql('ALTER TABLE `consultation` 
    ADD COLUMN `cms_actif` BOOLEAN NOT NULL DEFAULT FALSE COMMENT \'Gestion du chiffrement en PKCS#7\';');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE `configuration_organisme` drop column `cms_actif`;');
        $this->addSql('ALTER TABLE `consultation` drop column `cms_actif`;');
    }
}
