<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20210107121710 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-14759 - [Transvers. Contrats] Administration du caractère transverse d’un contrat';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql("CREATE TABLE IF NOT EXISTS `invite_permanent_contrat`( 
    `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT, 
    `organisme` VARCHAR(256) NULL, 
    `service` int(11) DEFAULT NULL, 
    `lot` VARCHAR(256) DEFAULT NULL, 
    `id_consultation` int(11) DEFAULT NULL, 
    `id_contrat_titulaire` int(11) DEFAULT NULL, 
    `date_decision` datetime NULL, 
    `created_at` datetime DEFAULT NULL, 
    `updated_at` datetime DEFAULT NULL, 
    PRIMARY KEY(`id`) ) 
    ENGINE = InnoDB;");
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('DROP TABLE invite_permanent_contrat');
    }
}
