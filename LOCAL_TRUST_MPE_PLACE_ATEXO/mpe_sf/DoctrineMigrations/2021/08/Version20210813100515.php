<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210813100515 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-194 - CCAG MOE';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`, `valeur_sub`, `ordre_affichage`) 
VALUES (NULL, '20', 'CCAG-MOE', 'CCAG-MOE', '', '', '', '', '', '', '', '', '', '0');");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("delete FROM `ValeurReferentiel` where libelle_valeur_referentiel = 'CCAG-MOE' ");
    }
}
