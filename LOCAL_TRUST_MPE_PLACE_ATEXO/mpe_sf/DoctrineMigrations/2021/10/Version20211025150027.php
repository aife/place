<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211025150027 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '[Evo: Brute Force] : Modification de la taille des champs en BDD';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('ALTER TABLE Agent MODIFY COLUMN password varchar(255);');
        $this->addSql('ALTER TABLE Inscrit MODIFY COLUMN mdp varchar(255);');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );
        $this->addSql('ALTER TABLE Agent MODIFY COLUMN password varchar(64);');
        $this->addSql('ALTER TABLE Inscrit MODIFY COLUMN mdp varchar(64);');
    }
}
