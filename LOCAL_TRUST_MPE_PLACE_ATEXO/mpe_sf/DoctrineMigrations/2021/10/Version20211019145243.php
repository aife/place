<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211019145243 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-17512 - Renseignement des lots via import d\'un fichier';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `t_critere_attribution` ADD `lot_id` INT NULL DEFAULT NULL AFTER `id_donnee_complementaire`, ADD INDEX `lot` (`lot_id`);");
        $this->addSql("ALTER TABLE `t_critere_attribution` ADD CONSTRAINT `Lot` FOREIGN KEY (`lot_id`) REFERENCES `CategorieLot`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `t_critere_attribution` DROP CONSTRAINT `Lot`;");
        $this->addSql("ALTER TABLE `t_critere_attribution` DROP COLUMN `lot_id`;");
    }
}
