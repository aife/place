<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211022100000 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'NOCHECK - Mise à jour des charsets de toute la BDD';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('SET foreign_key_checks = 0;');

        $this->addSql('ALTER TABLE `Journaux` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DecisionLot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_lancement_unique_cli` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `InvitationConsultationTransverse` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Chorus_forme_prix` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DATEFIN` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `NewsletterPieceJointe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `fichiers_liste_marches` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `visite_lieux` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_forme_prix` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_cosignature` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_parametrage_formulaire_publicite` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_document` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_type_marche` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `annexe_financiere` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `echange_doc` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_forme_prix_pf_has_ref_variation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `HabilitationProfil` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_lot_technique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Autres_Pieces_Mise_Disposition` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `EnchereValeursInitiales` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AnnonceBoamp` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_param_dossier_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `donnees_annuelles_concession` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `configuration_client` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_groupement_achat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `EnveloppeItemFormulaireConsultationValues` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Agent_Service_Metier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `resultat_analyse_decision` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_MesRecherches` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DateLancementCron` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Parametrage_Enchere_Tranches_Bareme_NETC` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `agent_technique_association` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `blobOrganisme_file` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_teletransmission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Centrale_publication` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Tranche_Article_133` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Complement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_dispositif_annonce` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `type_procedure_concession_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_contrat_titulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `type_contrat_concession_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `ReferentielDestinationFormXml` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `TelechargementAnonyme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `HabilitationAgent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AffiliationService` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AdresseFacturationJal` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_Theme_Graphique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CG76_Domaine` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `InterneConsultationSuiviSeul` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Sso_Tiers` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Historique_Avis_Pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_pv_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Service_Mertier_Profils` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_teletransmission_lot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_CAO_Ordre_De_Passage` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `historiques_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_Illustration_Fond` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `piece_genere_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DCE` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_espace_collaboratif` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CG76_PieceJointe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Enveloppe_papier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `document_serveur_docs` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_donnee_complementaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_echange` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_Code_Calcul_Interets` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `chorus_noms_fichiers` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `HistoriqueNbrConsultationsPubliees` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `InscritHistorique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `MarchePublie` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ModeleFormulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_entreprise_document_version` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_edition_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EchangePieceJointe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TypeAvenant` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `T_trace_operations_inscrit_details` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Telechargement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Annonce_Press_PieceJointe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `RPA` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `gestion_adresses` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_consultation_compte_pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_publicite_favoris` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `donnees_annuelles_concession_tarif` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `echange_doc_type_piece_standard` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `questions_dce` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_CAO_Intervenant_Externe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Fcsp_Mandataire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `historisation_mot_de_passe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_historique_synchronisation_SGMAP` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `invite_permanent_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `GeolocalisationN0` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Organisme_Service_Metier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ValeurReferentielOrg` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_vision_rma_agent_organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DocumentsAttaches` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `mail_type_group` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_critere_attribution` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EchangeTypeAR` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_procedure_equivalence_dume` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CertificatPermanent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Tiers` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EnveloppeCritereEvaluation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Fcsp_Lieu` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_type_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Socle_Habilitation_Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `type_procedure_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Partenaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_enveloppe_dossier_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_Offre_Support_Publicite` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `InterneConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_etablissement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `programme_previsionnel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `api_user` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Referentiel_Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Pieces_Mise_Disposition` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_forme_prix_has_ref_type_prix` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `AvisCao` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Admissibilite_Enveloppe_Lot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `changement_heure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `comptes_agents_associes` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `echanges_interfaces` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_derniers_appels_valides_ws_sgmap_documents` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_CAO_Commission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Referentiel_Entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Service` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `ReponseInscritFormulaireConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `ItemFormulaireConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ProcedureEquivalence` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `trace_operations_inscrit` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Mesure_Type_Procedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `web_service` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Inscrit` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_message_accueil` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Agrement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_document_type` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Echange` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_calendrier_transition_referentiel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_bourse_cotraitance` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `blob_file` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `DecisionPassationMarcheAVenir` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `consultation_archive` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_support_annonce_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_groupement_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `configuration_alerte` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EtatConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Offres` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_pj` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Avis_Pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Criteres_Evaluation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `compte_centrale` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ReferentielFormXml` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_CAO_Seance` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Certificat_Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_notification_agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Annonce` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Parametrage_Enchere_Reference` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Mesure_avancement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_decision_selection_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_type_procedure_dume` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `QuestionDCE` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_archive_bloc` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Guides` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ConsultationHistoriqueEtat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AnnonceJALPieceJointe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Commission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `archive_arcade` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `RG` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `nature_acte_juridique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `ReponseInscritItemFormulaireConsultationValues` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `modification_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_CAO_Seance_Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `echange_doc_application` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `migration_manager` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Referentiel_Consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `resultat_analyse` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `type_contrat_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_dume_numero` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Avis_Membres_CAO` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_membre_groupement_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `EnchereReference` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Type_Avis_Pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_sous_critere_attribution` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Langue` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `echange_doc_blob` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Chorus_Regroupement_Comptable` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `AcheteurPublic` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_flux_rss` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EnchereTranchesBaremeNETC` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `configuration_plateforme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_chorus_fiche_modificative_pj` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `echange_doc_historique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `plateforme_virtuelle` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_acte_juridique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_archive_fichier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_contrat_titulaire_favori` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_complement_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AnnonceJAL` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `DecisionPassationConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `GeolocalisationN2` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TypeProcedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Fcsp_unite` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_Fiche_Navette` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Offre_papier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Passation_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `GeolocalisationN1` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_organisation_achat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `destinataire_centrale_pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TypeAvis` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `type_contrat_mpe_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_CAO_Seance_Intervenant_Externe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `EnchereTrancheBaremeReference` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `autre_piece_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `configuration_organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Parametrage_Enchere` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_preference_support_publication` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `t_information_modification_password` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_calendrier_etape` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `formejuridique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_archive_arcade` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `responsableengagement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `code_retour` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `mail_template` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_archive_atlas` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_numero_sequence` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AVIS` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `StatutEnveloppe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Panier_Entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_referentiel_nace` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TireurPlan` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DocumentExterne` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `JAL` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Numerotation_ref_cons_auto` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_calendrier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ItemCritereEvaluation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `EnveloppeFormulaireConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Qualification` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_chorus_ccag` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_rapport_prefet` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `mode_execution_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_document_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Annonce_Press` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_chorus_tier_rejete` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `document_template_surcharge` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_annonce_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `chorus_numeros_marches` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_CAO_Commission_Intervenant_Externe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Societes_Exclues` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `echange_doc_application_client_organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `t_preference_offre_support_publication` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `EchangeDestinataire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ccag_applicable` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Administrateur` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_Telechargement_Asynchrone` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_piece_publicite` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_type_contrat_et_procedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `DestinataireAnnonceJAL` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `invite_permanent_transverse` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CategorieLot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_tranche` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Mandataire_service` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ReferentielOrg` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `destinataire_mise_disposition` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Type_Avis_Pub_Procedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_Profil_Joue` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Marche` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ItemFormulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `echange_doc_type_piece_actes` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `categories_considerations_sociales` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `EchangeFormat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `type_procedure_mpe_pivot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TypeCommission` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_access_token` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `sso_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_conditions_paiements` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Certificats_Entreprises` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `passation_marche_a_venir` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `sso_agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Chorus_Type_Groupement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Historique_suppression_agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `EnveloppeItemCritereEvaluation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `SuiviAcces` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_reponse_elec_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_candidature` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `CG76_Donnee_Complementaire_Domaine` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Avenant` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `InterfaceTypeProcedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `habilitation_agent_ws` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `alerte_metier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `decisionEnveloppe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_CAO_Seance_Invite` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `blocFichierEnveloppe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Departement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_synthese_rapport_audit` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `document_template` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Operations` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Pieces_DCE` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Prestation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_numero_reponse` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Alerte` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_attestations_offres` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_contact_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `agent_technique_token` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CompteMoniteur` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Entreprise_info_exercice` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `renseignements_boamp` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_dume_contexte` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Referentiel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_type_groupement_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_CAO_Commission_Consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql(
            'ALTER TABLE `t_calendrier_etape_referentiel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Destinataire_Annonce_Press` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_favoris` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Type_Procedure_Organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Enveloppe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `CG76_Donnee_Complementaire_entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_donnees_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EchangeTypeMessage` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_referentiel_certificat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `supervision_interface` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `plateforme_virtuelle_organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Type_Avis_Pub_Organisme` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_type_notification_agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `dossier_volumineux` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `AnnonceMoniteur` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `echange_doc_application_client` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Chorus_type_procedure` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EntrepriseInsee` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Relation_Echange` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Groupe_Moniteur` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_type_annonce_consultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EncherePmi` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `t_forme_prix_pu_has_ref_variation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `SousCategorie` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_support_publication` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `configuration_messages_traduction` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_referentiel_mots_cles` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Service_Mertier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `LieuExecution` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Intervenant_Ordre_Du_Jour` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `fichierEnveloppe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Newsletter` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Contact_Entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Type_support` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `FormXmlDestinataireOpoce` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Lt_Referentiel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ValeurReferentiel` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ConsultationFormulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_calendrier_transition` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_chorus_fiche_modificative` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `mail_type` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Justificatifs` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_dossier_formulaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `blocFichierEnveloppeTemporaire` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `Retrait_Papier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CategorieConsultation` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_candidature_mps` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `TypeDecision` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Referentiel_org_denomination` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_lot_technique_has_tranche` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `ReferentielTypeXml` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CertificatChiffrement` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Destinataire_Pub` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EnchereEntreprisePmi` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_role_juridique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `consultation_document_cfe` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_liste_lots_candidature` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `CategorieINSEE` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Articles` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Faq_Entreprise` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `T_Traduction` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Parametrage_Fiche_Weka` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_cons_lot_contrat` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_fusionner_services` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `T_Telechargement_Asynchrone_fichier` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_numerotation_automatique` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `Helios_tableau_ar` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `EnchereOffreReference` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `EnchereOffre` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql('ALTER TABLE `t_historique_annonce` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');
        $this->addSql(
            'ALTER TABLE `Parametrage_Enchere_Tranche_Bareme_Reference` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;'
        );
        $this->addSql('ALTER TABLE `t_CAO_Commission_Agent` CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;');

        $this->addSql('SET foreign_key_checks = 1;');
    }

    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException('Aucun rattrapage de données possible');
    }
}
