<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211022100116 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-17542 - Validation du schéma de données Doctrine';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` ADD COLUMN `organisme_acronyme` VARCHAR(30);');
        $this->addSql("UPDATE `plateforme_virtuelle_organisme` pvo SET organisme_acronyme = 
    (SELECT acronyme FROM Organisme o WHERE o.id = pvo.organisme_id);");
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` MODIFY `organisme_acronyme` VARCHAR(30) NOT NULL;');
        $this->addSql("ALTER TABLE `plateforme_virtuelle_organisme` ADD CONSTRAINT `organisme_acronyme_fk` 
 FOREIGN KEY ( `organisme_acronyme` ) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` DROP FOREIGN KEY organisme_id_fk;');
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` DROP COLUMN `organisme_id`;');

        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` ADD COLUMN `organisme_acronyme` VARCHAR(30)');
        $this->addSql("UPDATE `echange_doc_application_client_organisme` edaco SET organisme_acronyme = 
    (SELECT acronyme FROM Organisme o WHERE o.id = edaco.organisme_id);");
        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` DROP FOREIGN KEY FK_B504956B5DDD38F5;');
        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` DROP KEY IDX_organisme_id;');
        $this->addSql("ALTER TABLE `echange_doc_application_client_organisme` ADD CONSTRAINT `organisme_acronyme_edaco_fk` 
 FOREIGN KEY ( `organisme_acronyme` ) REFERENCES `Organisme` (`acronyme`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` DROP COLUMN `organisme_id`;');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` ADD COLUMN `organisme_id` INT(11);');
        $this->addSql("UPDATE `plateforme_virtuelle_organisme` pvo SET organisme_id = 
    (SELECT id FROM Organisme o WHERE o.acronyme = pvo.organisme_acronyme);");
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` MODIFY `organisme_id` INT(11) NOT NULL;');
        $this->addSql("ALTER TABLE `plateforme_virtuelle_organisme` ADD CONSTRAINT `organisme_id_fk` 
 FOREIGN KEY ( `organisme_id` ) REFERENCES `Organisme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` DROP FOREIGN KEY organisme_acronyme_fk;');
        $this->addSql('ALTER TABLE `plateforme_virtuelle_organisme` DROP COLUMN `organisme_acronyme`;');

        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` ADD COLUMN `organisme_id` INT(11);');
        $this->addSql("UPDATE `echange_doc_application_client_organisme` edaco SET organisme_id = 
    (SELECT id FROM Organisme o WHERE o.acronyme = edaco.organisme_acronyme);");
        $this->addSql("ALTER TABLE `echange_doc_application_client_organisme` ADD CONSTRAINT `IDX_organisme_id` 
 FOREIGN KEY ( `organisme_id` ) REFERENCES `Organisme` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;");
        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` DROP FOREIGN KEY organisme_acronyme_edaco_fk;');
        $this->addSql('ALTER TABLE `echange_doc_application_client_organisme` DROP COLUMN `organisme_acronyme`;');
    }
}
