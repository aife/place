<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211025162248 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-279 - [EVO : FEN211] : Mise à jour de la table chorus_echange';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `Chorus_echange` ADD `raison_sociale_attributaire` VARCHAR(255) NULL DEFAULT NULL AFTER `type_flux_a_envoyer`, ADD `siret_attributaire` VARCHAR(14) NULL DEFAULT NULL AFTER `raison_sociale_attributaire`, ADD `code_ape` VARCHAR(5) NULL DEFAULT NULL AFTER `siret_attributaire`, ADD `forme_juridique` VARCHAR(255) NULL DEFAULT NULL AFTER `code_ape`, ADD `pme` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '0: Autre, 1:PME' AFTER `forme_juridique`, ADD `pays_territoire` VARCHAR(255) NULL DEFAULT NULL AFTER `pme`, ADD `numero_national_attributaire` VARCHAR(20) NULL DEFAULT NULL AFTER `pays_territoire`;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `Chorus_echange`
                          DROP `raison_sociale_attributaire`,
                          DROP `siret_attributaire`,
                          DROP `code_ape`,
                          DROP `forme_juridique`,
                          DROP `pme`,
                          DROP `pays_territoire`,
                          DROP `numero_national_attributaire`;");
    }
}
