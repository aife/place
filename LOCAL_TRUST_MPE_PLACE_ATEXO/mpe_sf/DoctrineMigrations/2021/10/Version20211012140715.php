<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211012140715 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-248 - Création d\'un WS : Création échange chorus';
    }

    public function up(Schema $schema) : void
    {
        $this->warnIf(true !== (bool) getenv('SPECIFIQUE_PLACE'), 'Migration spécifique Place !');
        if (true !== (bool) getenv('SPECIFIQUE_PLACE')) {
            return;
        }

        $this->addSql("
                           INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
                           VALUES 
                           ('/api/{version}/echanges-chorus.{format}', 'GET', 'Retourner les échanges chorus', '1');
                           ");
        $this->addSql("
                           INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
                           VALUES 
                           ('/api/{version}/echanges-chorus.{format}', 'POST', 'Créer un échange chorus', '1');
                           ");
        $this->addSql("
                           INSERT IGNORE INTO `web_service` (`route_ws`, `method_ws`, `nom_ws`, `uniquement_technique`)
                           VALUES 
                           ('/api/{version}/echanges-chorus.{format}', 'PUT', 'Mettre à jour un échange chorus', '1');
                           ");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("DELETE FROM web_service WHERE nom_ws='Retourner les échanges chorus';");
        $this->addSql("DELETE FROM web_service WHERE nom_ws='Créer un échange chorus';");
        $this->addSql("DELETE FROM web_service WHERE nom_ws='Mettre à jour un échange chorus';");
    }
}
