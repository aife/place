<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211028100111 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-174 - [PLACE] EXEC 1.1 - MPE - Données du contrat';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `t_contrat_titulaire` ADD IF NOT EXISTS `marche_innovant` TINYINT(1) NOT NULL AFTER `uuid`;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `t_contrat_titulaire` DROP COLUMN `marche_innovant`;");
    }
}
