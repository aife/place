<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;


final class Version20211014144411 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-246 - Enrichissement des WS : /api/{version}/contrats.{format}';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `modification_contrat` ADD `date_publication_sn` DATETIME NULL AFTER `statut_publication_sn`, ADD `erreur_sn` VARCHAR(255) NULL AFTER `date_publication_sn`, ADD `date_modification_sn` DATETIME NULL AFTER `erreur_sn`;");
        $this->addSql("ALTER TABLE `t_contrat_titulaire` ADD `date_publication_sn` DATETIME NULL AFTER `statut_publication_sn`, ADD `erreur_sn` text NULL AFTER `date_publication_sn`, ADD `date_modification_sn` DATETIME NULL AFTER `erreur_sn`;");

    }

    public function down(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE `modification_contrat` DROP `date_publication_sn`, DROP `erreur_sn`, DROP `date_modification_sn`;");
        $this->addSql("ALTER TABLE `t_contrat_titulaire` DROP `date_publication_sn`, DROP `erreur_sn`, DROP `date_modification_sn`;");
    }
}
