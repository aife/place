<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211004142815 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-17386 - Migration des mots de passe "superadmin" en Argon2';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Administrateur MODIFY COLUMN mdp varchar(255);');
    }

    public function down(Schema $schema) : void
    {
        $this->addSql('ALTER TABLE Administrateur MODIFY COLUMN mdp varchar(40);');
    }
}
