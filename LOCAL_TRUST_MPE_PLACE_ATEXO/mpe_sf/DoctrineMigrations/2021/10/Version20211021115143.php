<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211021115143 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-252 - Vérification des données des templates exsitant';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p><strong>[Contenu du message à renseigner]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation citée en référence.</p> ' WHERE `mail_template`.`id` = 1;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p> Vous êtes invité à concourir pour la consultation citée en référence.</p><p><br></p><p><br></p><p> Merci de votre intérêt pour cette consultation.</p>' WHERE `mail_template`.`id` = 2;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été modifiée.</p><p><br></p><p>Les éléments modifiés sont : </p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p>' WHERE `mail_template`.`id` = 3;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Veuillez-trouver une réponse concernant la consultation citée en référence.</p> <p><strong>[Réponse à la question posée]</strong></p><p><br></p><p>Cordialement.</p> ' WHERE `mail_template`.`id` = 4;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Nous vous remercions d\'avoir répondu à la consultation citée en référence.</p><p><br></p><p>Après analyse, il vous est demandé d\'apporter les précisions suivantes :</p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p>' WHERE `mail_template`.`id` = 5;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>J’ai l’honneur de vous informer que vous avez été désigné attributaire de la consultation citée en référence.</p><p>Je vous informe que le contrat ne sera valablement formé qu’après sa signature par un représentant habilité, et ne produira ses effets qu’au terme de la notification dudit marché.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p>' WHERE `mail_template`.`id` = 6;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Votre entreprise a été déclarée attributaire de la consultation citée en référence. Veuillez-trouver à titre de notification le marché signé par un représentant habilité. L\'Accusé de réception de ce message vaut notification officielle du marché. </p><p><br></p><p> Merci de votre intérêt pour cette consultation. </p>' WHERE `mail_template`.`id` = 7;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><br><br><p>Nous vous remercions d\'avoir répondu à la consultation citée en référence. Nous sommes toutefois au regret de vous annoncer que votre réponse n\'a pas été retenue. </p><p><br></p><p> Merci de votre intérêt pour cette consultation.</p>' WHERE `mail_template`.`id` = 8;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été annulée. </p><p><br></p><p> Merci de votre intérêt pour cette consultation. </p>' WHERE `mail_template`.`id` = 9;");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p><strong>[Contenu du message à renseigner]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 1;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Vous êtes invité à concourir pour la consultation citée en référence.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 2;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été modifiée.</p><p><br></p><p>Les éléments modifiés sont : </p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 3;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p><strong>[Réponse à la question posée]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 4;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Nous vous remercions d'avoir répondu à la consultation citée en référence.</p><p><br></p><p>Après analyse, il vous est demandé d'apporter les précisions suivantes :</p><p><strong>[À préciser au cas par cas]</strong></p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 5;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>J’ai l’honneur de vous informer que vous avez été désigné attributaire de la consultation citée en référence.</p><p>Je vous informe que le contrat ne sera valablement formé qu’après sa signature par un représentant habilité, et ne produira ses effets qu’au terme de la notification dudit marché.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 6;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Votre entreprise a été déclarée attributaire de la consultation citée en référence.</p><p>Veuillez-trouver à titre de notification le marché signé par un représentant habilité.</p><p><br></p><p>L'Accusé de réception de ce message vaut notification officielle du marché.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 7;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>Nous vous remercions d'avoir répondu à la consultation citée en référence.</p><p>Nous sommes toutefois au regret de vous annoncer que votre réponse n'a pas été retenue.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 8;");
        $this->addSql("UPDATE `mail_template` SET `corps` = '<p>Bonjour,</p><p><br></p><p>La consultation citée en référence a été annulée.</p><p><br></p><p>Merci de votre intérêt pour cette consultation.</p><p>La plate-forme de dématérialisation des marchés publics</p>' WHERE `mail_template`.`id` = 9;");
    }
}
