<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211025095639 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'MPE-291 - [MPE] RdR 2021-02.01.00 - 
        Téléchargement des fichiers en clair lors de l\'ouverture hors ligne';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql("ALTER TABLE fichierEnveloppe 
DROP FOREIGN KEY IF EXISTS FK_fichierEnveloppe_blobOrganismeFile");
    }
}
