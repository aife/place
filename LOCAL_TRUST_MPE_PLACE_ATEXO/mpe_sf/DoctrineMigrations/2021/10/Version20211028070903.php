<?php

declare(strict_types=1);

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20211028070903 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'MPE-312 - [EXEC] : Enrichissement du WS échange chorus';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Chorus_echange` ADD IF NOT EXISTS `date_modification` DATETIME NULL AFTER `numero_national_attributaire`;");
    }

    public function down(Schema $schema): void
    {
        $this->addSql("ALTER TABLE `Chorus_echange` DROP COLUMN `date_modification`;");
    }
}
