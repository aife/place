#!/bin/bash

phpbin=$(which php8.1)
composerbin=$(which composer2)

if [[ $composerbin == '' ]]
then
  composerbin=$(which composer)
fi

"${phpbin}" ${composerbin} install \
  --no-interaction --no-ansi --no-dev --ignore-platform-reqs --no-scripts --optimize-autoloader \
&& APP_ENV=prod "${phpbin}" ./bin/console "assets:install"
