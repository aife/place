<?php

$finder = PhpCsFixer\Finder::create()
    ->in([
             __DIR__.'/legacy/protected/Bin',
             __DIR__.'/legacy/protected/Cli',
             __DIR__.'/legacy/protected/Controls',
             __DIR__.'/legacy/protected/Data',
             __DIR__.'/legacy/protected/Layouts',
             __DIR__.'/legacy/protected/Pages',
             __DIR__.'/legacy/protected/Service',
             __DIR__.'/src',
             __DIR__.'/tests',
             __DIR__.'/../mpe-librairies/atexo-crypto',
             __DIR__.'/../mpe-librairies/atexo-dume',
             __DIR__.'/../bundles',
           ])
    ->exclude('var')
;

$config = new PhpCsFixer\Config();
return $config->setRules([
        '@PSR12' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
