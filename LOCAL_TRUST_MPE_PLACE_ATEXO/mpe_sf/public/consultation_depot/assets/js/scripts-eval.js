// JavaScript Document

  
J(document).ready(function() {  
	J('<style type="text/css"> table.tableau-prix .hidden { display:none; } </style>').appendTo("head");
	//Cache les Critères sur les Prix unitaires et Critères issus du Questionnaire au chargement de la page						   
	J(".readOnlyRow").hide(); 
	
	//Cache/affiche les Critères sur les Prix unitaires et Critères issus du Questionnaire au chargement de la page						   
	J(".toggle-rows").click(function() {  
		J(this).parents('table').find(".readOnlyRow").toggle(J(this).parents('table').find(".readOnlyRow").css('display') == 'none'); 
		J(this).toggleClass('toggle-on');
	});  
  
	//Active/Desactive les champs du critère			   
	J(".activer-layer-criteres").click(function(){ 
		J(this).is(':checked') ? J(this).parents('tr').find('.layer-criteres').removeClass('hidden') : J(this).parents('tr').find('.layer-criteres').addClass('hidden');
	});
	
	//Active/Desactive les prix unitaire 		   
	J("input.prix-unitaire").click(function(){ 
		J(this).is(':checked') ? J(this).parents('tr').find('.quantite').addClass('hidden') : J(this).parents('tr').find('.quantite').removeClass('hidden');
	});

	//Appliquer le taux au tableau entier
	J("#appliquerPartout").click(function(){ 
		var myValue = J(".taux-tva-copy").val();	
		J(this).is(':checked') ? J(".taux-tva").val(myValue) : J(".taux-tva").val('');
	});
 
});



//Edition modèles Qestionnaires et Bordereau de prix//

//Ajout d'un ligne et mise à jour des fleches et numero d'item
function ajoutLigne() {
	var numberRows = J('.tableau-prix tbody tr').length;
	var incrementedItemIndex = parseInt(J('.tableau-prix tr:visible:last .num-item').val()) + 1;
	J('.tableau-prix tr:visible:last').next('tr').removeClass('hidden');
	if (incrementedItemIndex<numberRows) {
		J('.tableau-prix tr:visible:last .saveStatut').val('0');	
		J('.tableau-prix tr:visible:last .num-item').val(incrementedItemIndex);
		updateArrowStatut();	
		tableAlternateColor();	
		initItemNumber();
	}
	updateArrowStatut();
	tableAlternateColor();		
	initItemNumber();
}

J(document).ready(function(){
			   
    updateArrowStatut();					   
    J(".remonter,.descendre,.remonter-off,.descendre-off").click(function(){
        var row = J(this).parents("tr:first");
		if (J(this).is(".remonter")) {
            row.insertBefore(row.prev());
 			tableAlternateColor();
			updateArrowStatut();
			initItemNumber();
		}
		if (J(this).is(".descendre")) {
            row.insertAfter(row.next());
			tableAlternateColor();
			updateArrowStatut();
			initItemNumber();
		} 
       else {
	   }
    });
	
	//Supprime la ligne et met à jour le numero d'item
    J(".tableau-prix .supprimer").click(function(){
        var row = J(this).parents("tr:");
        var lastRow = row.siblings(":last");
		var tableRowLength = J('.tableau-prix tbody tr:visible').length;
		if (tableRowLength>1) {
			row.find('.saveStatut').val('1');
			row.addClass('hidden');
			row.insertAfter(lastRow);
			tableAlternateColor();		
			initItemNumber();
			updateArrowStatut();
		}
		else {
		}
	});
	
});

//*****************************Gestion de l'alternance des couleur de ligne*****************************//
function tableAlternateColor() {
	J('.tableau-prix tbody tr:visible:odd').addClass('on');
	J('.tableau-prix tbody tr:visible:even').removeClass('on');
}

//Gestion de l'affichage des fleche monter/descendre selon position de l'item
function updateArrowStatut() {
	
	J('.tableau-prix tbody tr:visible .remonter-off').removeClass().addClass('remonter').attr('title','Remonter');
	J('.tableau-prix tbody tr:visible .descendre-off').removeClass().addClass('descendre').attr('title','Descendre');
	
	J('.tableau-prix tbody tr:visible:first .remonter').removeClass().removeClass('remonter-off').addClass('remonter-off').attr('title','Remonter (inactif)');
	J('.tableau-prix tbody tr:visible:first .descendre').removeClass().removeClass('descendre-off').addClass('descendre').attr('title','Descendre');
	
	J('.tableau-prix tbody tr:visible:last .descendre-off').removeClass().removeClass('descendre').addClass('descendre').attr('title','Descendre');
	J('.tableau-prix tbody tr:visible:last .remonter-off').removeClass().removeClass('remonter').addClass('remonter').attr('title','Remonter');
	
	J('.tableau-prix tbody tr:visible:last .descendre').removeClass().addClass('descendre-off').attr('title','Descendre (inactif)');
	J('.tableau-prix tbody tr:visible:last .remonter').removeClass().addClass('remonter').attr('title','Remonter');
  
  
  	//Desacactive les fleche si derniere ligne du tableau
	var tableRowLength = J('.tableau-prix tbody tr:visible').length;
	if (tableRowLength<2) {
	  J('.tableau-prix tbody tr .descendre').addClass('descendre-off').attr('title','Descendre (inactif)');
	  J('.tableau-prix tbody tr .remonter').addClass('remonter-off').attr('title','Remonter (inactif)');
	}
	else {
	}
  
}

//Reinitialisation du numero d'item
function initItemNumber() {
	J('.tableau-prix tbody tr:visible').each(function(index) {
		J(this).find('.num-item').val(index+1);
	});
}

//*****************************Fin Edition Qestionnaires et Bordereau de prix*****************************//

