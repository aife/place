$(document).ready(function () {
    // Default page import lot
    $('.fileupload-component-file').hide();
    $('.js-btn-import').attr('disabled','disabled');

    /*$( document ).mousemove(function( event ) {
        var msg = "Handler for .mousemove() called at ";
        msg += event.pageX + ", " + event.pageY;
        $( "#log" ).append( "<div>" + msg + "</div>" );
    });*/

    var mouseX = 0;
    var mouseY = 0;

    document.addEventListener("mousemove", function(e) {
        mouseX = e.clientX;
        mouseY = e.clientY;
    });

    $( window ).unload(function(e) {
            if (mouseY < 50) {
                opener.document.getElementById($('#btn-close-popup-import-lot').data('refresh')).click();
                window.close();
            }
    });


    // click on delete btn, reset the form
    $('.js-btn-delete').click(function(e){
       e.preventDefault();
        $('.js-btn-import').attr('disabled','disabled');
        $('.form-upload-lots')[0].reset();
        $('.fileupload-component-zone').show();
        $('.fileupload-component-file').hide();
        $('.fileupload-component-file .progress-bar').width('0%').attr('aria-valuenow', '0');
        $('.fileupload-component-file [data-binding="valuenow"]').text('0' );
    });

    $('#btn-close-popup-import-lot').click(function(){
        opener.document.getElementById($(this).data('refresh')).click();
        window.close();
    });

    // Drag and Drop fileupload
    $(document).on('dragenter', '.fileupload-component-zone', function (event) {

        $(this).css('border', '3px dashed #90CAF9');
        return false;
    });

    $(document).on('dragover', '.fileupload-component-zone', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).css('border', '3px dashed #1E88E5');
        return false;
    });

    $(document).on('dragleave', '.fileupload-component-zone', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).css('border', '3px dashed #90CAF9');
        return false;
    });

    // Drop the file
    $(document).on('drop', '.fileupload-component-input', function (e) {
        check_file(e);
        return false;
    });

    // Select the file
    $('.fileupload-component-input').change(function (e) {
        check_file(e);
    });

    // Event on drop or select file
    var check_file = function (e) {
        var _file;
        if (e.type=='drop' && e.originalEvent.dataTransfer) {
            if (e.originalEvent.dataTransfer.files.length) {
                // Stop the propagation of the event
                e.preventDefault();
                e.stopPropagation();

                show_file(e.originalEvent.dataTransfer.files[0]);
            }
        }

        if (e.type=='change' && e.target.files[0]) {
            if (e.target.files.length) {
                // Stop the propagation of the event
                e.preventDefault();
                e.stopPropagation();

                show_file(e.target.files[0]);
            }
        }
    };

    // Show nex file select with progress bar
    var show_file = function(_file){
        $('.js-btn-import').removeAttr('disabled');
        $('.fileupload-component-zone').hide();
        $('.fileupload-component-file').show();
        $('.fileupload-component-file [data-binding="name"]').text(_file.name);
        $('.fileupload-component-file [data-binding="size"]').text( (parseInt(_file.size) / 1024).toFixed(2) + ' ko' );
        $('.fileupload-component-file .progress-bar').width('100%').attr('aria-valuenow', '100');
        $('.fileupload-component-file [data-binding="valuenow"]').text('100' );
    };
});
