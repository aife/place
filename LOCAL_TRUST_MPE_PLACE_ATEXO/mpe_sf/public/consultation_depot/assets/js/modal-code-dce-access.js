/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

J( "#target" ).submit(function(event) {
    J('#code-dce_restreint-modal').hide();
    let url = J(this).attr('action');
    let code = J('#code-acces').val();

    if(code.length > 0) {
        window.location.href = url + '&code=' + code;
    }

    event.preventDefault();
});

J( "button.close" ).click(function(event) {
    J('#code-dce_restreint-modal').hide();
});
J( "button.close-btn" ).click(function(event) {
    J('#code-dce_restreint-modal').hide();
});