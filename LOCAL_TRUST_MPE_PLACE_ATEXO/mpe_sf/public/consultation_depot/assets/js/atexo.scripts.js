//----------------------------------Reponse consultation----------------------------------//

function validerFormulaire()
{
    /** Valide si les CGU sont bien coché par l'utilisateur **/
    if (J('#isActeEngagementExists').val() == 0) {
        if (!J("#accepter-conditions-utilisation").is(':checked')) {
            alert(conditionsUtilisationError);
            return false;
        }
    }

    if (consultationAlloti == "1" && interfaceModuleSub != "1") {
        var multiArray = J('#select-lot').val();
        if (!Array.isArray(multiArray) || multiArray.length <= 0) {
            alert(hasSelectedLotError);
            return false;
        }
    }



    /** Verifie si au moins un fichier a été déposé **/
    if (J('.templ-fileupload.template-download.in').length == 0) {
        alert(ajoutFichierReponseError);
        return false;
    }
    return true;
}

function ajouterDonneesFormulaire()
{
    /**
     * Ajout du mail d'AR de l'inscrit au formulaire de réponse
     */
    J('#form_adresse_ar').val(J('#inputEmail').val());

    var $listeFichiers = [];
    /**
     * Recuperation des id de fichiers à conserver
     */
    J('.row-file_name[data-fichier]').each(function () {
        $listeFichiers.push(J(this).attr('data-fichier'));
    });
    J('#form_fichiers').val($listeFichiers);
}

function annulerDepot(langue, code)
{
    trackInscritOperationAnnulation(this, null, 'DESCRIPTION19', 'annuler', false);
    let queryCode = '';
    if (code && code !== 'undefined') {
        queryCode = '&code=' + code;
    }
    window.location = '/index.php?page=Entreprise.EntrepriseDetailConsultation&id='+consulation_ref+'&orgAcronyme='+organisme+ queryCode + '&lang='+langue+'#depot';
    return false;
}

function addZero(i)
{
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function formatIsoDate($dateIso)
{
    var $dateFormatted = addZero($dateIso.getDate())+"/"+addZero($dateIso.getMonth()+1)+"/"+$dateIso.getFullYear()+" "+addZero($dateIso.getHours())+":"+addZero($dateIso.getMinutes())+":"+addZero($dateIso.getSeconds())
    return $dateFormatted;
}

J(document).ready(function () {

    //code a garder : dateBabdeau
    //dateBandeau(time, journeeDeLAnnee);
    J('#ctl0_bandeauEntrepriseWithoutMenu_lienAcceuil').attr("href", "/index.php?page=Entreprise.EntrepriseHome");
    J('.bootstrap [data-toggle=collapse]').on('click', function () {
        if (J(this).hasClass('collapsed')) {
            J(this).find('i.fa').removeClass('fa-plus-circle').addClass('fa-minus-circle');
        } else {
            J(this).find('i.fa').removeClass('fa-minus-circle').addClass('fa-plus-circle');
        }
    });
    if (J('#select-lot')) {
        var oldSelected = J('#select-lot').val()?J('#select-lot').val(): [];
        var loadBlocLot = function () {
            var multiArray =  J('#select-lot').val()?J('#select-lot').val(): [];
            if (Array.isArray(multiArray) && Array.isArray(oldSelected) && oldSelected.length > multiArray.length) {
                for (i=0; i< oldSelected.length; i++) {
                    var item = oldSelected[i];
                    if (J.inArray(item , multiArray) < 0) {
                        if (J('[id^=files_container_lot'+item+'_]').children('div').hasClass('templ-fileupload template-upload')
                            || J('[id^=ae-file-'+item+'-]').children('div').hasClass('templ-fileupload template-upload')
                            || J('[id^=af-file-'+item+'-]').children('div').hasClass('templ-fileupload template-upload')
                            || J('[data-lot=lot-'+item+']').length != 0) {
                            J("#modal-deselection-lot").modal("show");
                            J('#select-lot').val(oldSelected).trigger("chosen:updated").trigger("change");
                            return false;
                        }
                    }
                }
            }
            oldSelected = J('#select-lot').val();
            J('[id^=bloc-lot][data-lot]').hide().find('.collapse').removeClass('in');
            if (Array.isArray(multiArray) && multiArray.length > 0) {
                multiArray.forEach(function (item,index) {
                    J('[id^=bloc-lot][data-lot='+item+']').show().find('.collapse').addClass('in');
                    if (item == 0) {
                        J('[id^=bloc-lot]').show().find('.collapse').addClass('in');
                        var lots = [];
                        var i = 0;
                        J('#select-lot option').each(function () {
                            var elementToSelect = J(this).val();
                            if (elementToSelect != 0) {
                                lots[i++] = elementToSelect;
                            }
                        });
                        J('#select-lot').val(lots).trigger("chosen:updated").trigger("change");
                    }
                });
            }
        };
        J('#select-lot').chosen();
        J('#select-lot').change(function (e) {
            loadBlocLot();
        });
        loadBlocLot();
        if (typeof fromCandidatureMPS !== 'undefined' && fromCandidatureMPS !== "undefined"   && fromCandidatureMPS == true) {
            J('#select_lot_chosen').find('.search-choice-close').removeClass('search-choice-close');
        }
    }

    /*if (navigator.userAgent.indexOf("MSIE 10") > 0) {
     J(".fileupload").bind('mousedown', function (event) {
     J(this).trigger('click')
     });
     }*/

    J('.fileinput-image').bind('click',function (e) {
        J(this).siblings('.fileupload').click();
    });
    J('.annuler-reponse-consultation').click(function () {
        var langue = J(this).data('langue');
        var code = J(this).data('code');
        return annulerDepot(langue, code);
    });
    J(document).on('click','[data-infos-signature]',function (e) {
        var $data = JSON.parse(J(this).attr('data-infos-signature'));
        if (!$data['name']) {
            var $filename = J(this).closest('.template-upload-jeton').find('.name-link').html();
            $data['name']=$filename;
        }
        J('#content-details-signature').empty();
        J('#content-details-signature').html(tmpl("template-popin-details-signature", $data));
        J("#modal-details-signature").modal("show");
    });

    //Modifier l'url du lien de deconnexion
    var disconnectUrl = J("a.deconnexion").attr('href') + "&callFromMpeSf";
    J("a.deconnexion").attr("href", disconnectUrl);

});

J("#valider-reponse-consultation").click(function () {
    if (!J("#accepter-conditions-utilisation").is(':checked')) {
        alert(conditionsUtilisationError);
        return false;
    }

    let acte = false;
    let afi = false;
    if (document.getElementsByClassName('clearfix row-acte').length > 0) {
        acte = true;
    }

    if (document.getElementsByClassName('clearfix row-afi').length > 0) {
        afi = true;
    }

    if (J('#isActeEngagementExists').val() == 1 || J('#isAnnexeFinanciereExists').val() == 1) {
        if ( (document.getElementsByClassName('row row-acte').length === 0 && acte)
            ||  (document.getElementsByClassName('row row-afi').length === 0 && afi)
        ) {
            hideAndShow()
            J('#modal-acte-engagement-non-renseigne').modal("show");
            return false;
        } else {
            deposerMademande();
            return false;
        }
    } else {
        deposerMademande();
        return false;
    }
});

J('#valider-modal-acte-engagement-non-renseigne').click(function () {

    let acte = false;
    let afi = false;
    if (document.getElementsByClassName('clearfix row-acte').length > 0) {
        acte = true;
    }

    if (document.getElementsByClassName('clearfix row-afi').length > 0) {
        afi = true;
    }

    if (J('#isActeEngagementExists').val() == 1  || J('#isAnnexeFinanciereExists').val() == 1) {
        if ( (document.getElementsByClassName('row row-acte').length === 0 && acte)
            ||  (document.getElementsByClassName('row row-afi').length === 0 && afi)) {

            hideAndShow();
            J('#modal-acte-engagement-non-renseigne').modal("show");
            return false;
        } else {
            deposerMademande();
            return false;
        }
    } else {
        deposerMademande();
        return false;
    }
});

J('#valider-modal-acte-engagement-non-renseigne').click(function () {
    J('#modal-acte-engagement-non-renseigne').modal("toggle");
    deposerMademande();
});


function hideAndShow() {
    J('#acte-engagement-absent').hide();
    J('#annexe-financiere-absent').hide();
    if (document.getElementsByClassName('row row-afi').length === 0) {
        J('#annexe-financiere-absent').show();
    }

    if (document.getElementsByClassName('row row-acte').length === 0) {
        J('#acte-engagement-absent').show();
    }
}

function deposerMademande()
{

    /** Vérifier si l'on a des fichiers qui dépasse le poids **/
    if (J('.templ-fileupload').length > 0) {
        var targetDiv = document.getElementById('listeFichiersTailleDepasseLimite');
        let checkErrorLimit = false;
        let checkErrorDoublon = false;

        J('#content-modal-doublon').hide();
        J('#content-modal-taille-fichier-depasse').hide();

        var html = '<ul>';
        var listFileError = [];
        J('.templ-fileupload .container-fluid .row .row-file_name').each(function (index, el) {
            if (J(el).children('.error-area').length) {
                if (J(el).children('.error-area').attr('attr-error') == 1) {
                    checkErrorDoublon = true
                    html += '<li>' + J(el).children('.name-link').html() + '</li>';
                    listFileError.push(J(el).children('.name-link').html());
                } else {
                    checkErrorLimit = true;
                    html += '<li>' + J(el).children('.name-link').html() + '</li>';
                    listFileError.push(J(el).children('.name-link').html());
                }
            }
        })

        if (checkErrorDoublon == true) {
            J('#content-modal-doublon').show();
        }

        if (checkErrorLimit == true) {
            J('#content-modal-taille-fichier-depasse').show();
        }

        html += '</ul>';
        targetDiv.innerHTML = html;
        if (listFileError.length > 0) {
            J("#modal-taille-fichier-depasse").modal("show");
            return false
        }
    }

    if (nbFile==0) {
        ajouterDonneesFormulaire();
        if (validerFormulaire() == true) {
            //Afficher loader
            J('#bloc-loader').show();
            //Valider les fichiers ajoutes sur le formulaire
            var datasFormulaire = new FormData();
            datasFormulaire.append('reponse[offre_id]', J('#form_offre_id').val());
            datasFormulaire.append('reponse[fichiers]', J('#form_fichiers').val());
            datasFormulaire.append('reponse[uid_response]', J('#form_uid_response').val());
            J.ajax({
                type: "POST",
                url: Routing.generate('atexo_consultation_depot_enregistrer_valider_fichiers', {'id': consulation_ref}),
                processData: false,
                contentType: false,
                data: datasFormulaire,
                success: function (data) {
                    if (data.hasOwnProperty('infoFiles')) {
                        console.log("SUCCES: validation des fichiers OK");
                        if (data['infoFiles']['statutFileDepot'] == 'KO') {
                            console.log("Affichage de la popin de confirmation: " + data['infoFiles']);
                            J('#bloc-loader').hide();
                            trackInscritOperation(this, null, 'DESCRIPTION33');
                            afficherPopInConfirmationDepotReponse(data['infoFiles']);
                            return false;
                        }
                        //Validation de la reponse
                        return validerReponseConsultation();
                    } else if (data.hasOwnProperty('errors')) {
                        console.log("ERROR: validation des fichiers KO");
                        J('#bloc-loader').hide();
                        //TODO afficher messages d'erreur
                        return false;
                    }
                },
                error: function (jqXHR) {
                    console.log("ERROR: validation des fichiers KO");
                    switch (jqXHR.status) {
                        case 401:
                            var redirectUrl = Routing.generate('mpe_non_auth');
                            window.location.replace(redirectUrl);
                            break;
                        case 403: // (Invalid CSRF token for example)
                            // Reload page from server
                            window.location.reload(true);
                    }
                    J('#bloc-loader').hide();
                    //TODO afficher messages d'erreur
                    return false;
                }
            });
        }
    } else {
        if (!J("#accepter-conditions-utilisation").is(':checked')) {
            alert(conditionsUtilisationError);
        } else {
            J('.atx-upload-modal').modal('show');
        }
    }
    return false;
}
// Validation anticipée
J("#activer-depot-automatique").click(function (event) {
    validationAnticipee = 1;

    if (nbFile!=0) {
        depotAutomatique = 1;
        pageTransmissionAffichee = 1;
    } else {
        J('#modal-confirmation-depot-reponse-electronique').modal('hide');
        return validerReponseConsultation();
    }
});
// Fin validation anticipée
J("#valider-popin-confirmation-reponse-consultation").click(function () {
    J('#modal-confirmation-depot-reponse-electronique').modal('hide');
    return validerReponseConsultation();
});
J("#annuler-popin-confirmation-reponse-consultation").click(function () {
    J('#modal-confirmation-depot-reponse-electronique').modal('hide');
});

function validateInputNumber(input) {
    J('#' + input).on('keypress',function(key) {
        if (
            key.charCode < 48 ||
            key.charCode > 57 ||
            J('#' + input).val().length > 2
        ) {
            return false;
        }
    });
}
function validerReponseConsultation()
{
    console.log("Debut enregistrement de la reponse");
    ajouterDonneesFormulaire();
    //Desactiver le bouton de validation
    J('#valider-reponse-consultation').prop('disabled', true);
    //Afficher loader
    J('#bloc-loader').show();
    var datasFormulaire = new FormData();
    var listeDV = [];
    J('.dv-select').each(function () {
        listeDV[J(this).data("dvlot")]=J(this).val();
        var key=J(this).data("dvlot");
        var value=J(this).val();
        listeDV[key]=value;
    });

    datasFormulaire.append('reponse[offre_id]', J('#form_offre_id').val());
    datasFormulaire.append('reponse[adresse_ar]', J('#form_adresse_ar').val());
    datasFormulaire.append('reponse[fichiers]', J('#form_fichiers').val());
    datasFormulaire.append('reponse[groupement_id]', J('#form_groupement_id').val());
    datasFormulaire.append('reponse[uid_response]', J('#form_uid_response').val());
    datasFormulaire.append('reponse[validationAnticipee]', validationAnticipee);
    datasFormulaire.append('reponse[dossiersVolumineux]', JSON.stringify(listeDV));
    datasFormulaire.append('reponse[tauxProductionFrance]', J('#tauxProductionFrance').val());
    datasFormulaire.append('reponse[tauxProductionEurope]', J('#tauxProductionEUrope').val());

    J.ajax({
        type: "POST",
        url: Routing.generate('atexo_consultation_depot_enregistrer', {'id': consulation_ref}),
        processData: false,
        contentType: false,
        data: datasFormulaire,

        success: function (data) {
            if (data.hasOwnProperty('redirect')) {
                console.log("SUCCES: enregistrement OK");
                console.log("Redirection vers la page: " + data['redirect']);
                window.location.replace(data['redirect']);
            } else if (data.hasOwnProperty('errors')) {
                console.log("ERROR: enregistrement KO");
                J('#bloc-loader').hide();
                J('#valider-reponse-consultation').prop('disabled', false);
                //TODO afficher messages d'erreur
            }
        },
        error: function (jqXHR) {
            console.log("ERROR: validation des fichiers KO");
            switch (jqXHR.status) {
                case 401:
                    var redirectUrl = Routing.generate('mpe_non_auth');
                    window.location.replace(redirectUrl);
                    break;
                case 403: // (Invalid CSRF token for example)
                    // Reload page from server
                    window.location.reload(true);
            }
            J('#bloc-loader').hide();
            //TODO afficher messages d'erreur
            return false;
        }
    });
}

function dateBandeau(value, offset)
{
    date=new Date();
    date.setTime((value/1000 + date.getTimezoneOffset()*60 + new Number(offset)) * 1000);

    var jours=Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi");
    var mois=Array("Janv.", "Fév.", "Mars","Avril","Mai","Juin","Juil.","Août","Sept.", "Oct.", "Nov.", "Déc.");

    nomJour=jours[date.getDay()];
    jour=date.getDate();
    nomMois=mois[date.getMonth()];
    annee=date.getFullYear();
    heures=date.getHours();
    minute=date.getMinutes();
    seconde=date.getSeconds();

    if (seconde<=9) {
        seconde="0"+seconde;
    }
    if (minute<=9) {
        minute="0"+minute;
    }
    if (heures<=9) {
        heures="0"+heures;
    }

    if (document.getElementById("ctl0_bandeauAgent_dateAujourdhui")) {
        document.getElementById("ctl0_bandeauAgent_dateAujourdhui").innerHTML=nomJour+" "+jour+" "+nomMois+" "+annee+" "+heures+":"+minute;
    } else if (document.getElementById("ctl0_bandeauEntreprise_dateAujourdhui")) {
        document.getElementById("ctl0_bandeauEntreprise_dateAujourdhui").innerHTML=nomJour+" "+jour+" "+nomMois+" "+annee+" "+heures+":"+minute;
    } else if (document.getElementById("ctl0_bandeauEntrepriseWithoutMenu_dateAujourdhui")) {
        document.getElementById("ctl0_bandeauEntrepriseWithoutMenu_dateAujourdhui").innerHTML=nomJour+" "+jour+" "+nomMois+" "+annee+" "+heures+":"+minute;
    }


    //code à garder
    //time=eval(value+"+1000");
    //setTimeout("dateBandeau('"+time+"', '"+offset+"')",1000);
}

function afficherPopInConfirmationDepotReponse(data)
{

    if(data['infoTaille'] == undefined){
        J('#content-popin-confirmation-depot-reponse-electronique').empty();
        J('#content-popin-confirmation-depot-reponse-electronique').html(tmpl("template-popin-confirmation-depot-reponse-electronique", data));
        J("#modal-confirmation-depot-reponse-electronique").modal("show");
    }else{
        var targetDiv = document.getElementById('listeFichiersTailleDepasseLimite');
        var html = '<ul>';
        for (let i = 0; i < data['infoTaille']['infoTailleDepasse'].length; i++){
            html += '<li>'+ data.infoTaille.infoTailleDepasse[i]+'</li>';
        }
        html += '</ul>';
        targetDiv.innerHTML  = html;
        J("#modal-taille-fichier-depasse").modal("show");
    }
}

function trackUserOperation(element, data, cleDescription, action, asynchrone, detailFileOrigin)
{
    var datasFormulaire = new FormData();

    var dateTracking = new Date(ServerDate());
    var dateTrackingTimestampMillisecond = ServerDate.now();
    if (dateTracking == undefined || !dateTracking instanceof Date) {
        dateTracking = new Date();
        dateTrackingTimestampMillisecond = dateTracking.now();
    }

    if (typeof asynchrone == "undefined" || asynchrone == null || asynchrone == '') {
        asynchrone = true;
    }
    if (typeof data != "undefined" && data != null && typeof data.files != "undefined" && data.files != null) {
        var files = '';
        J.each(data.files, function (index, file) {
            files += file.name + ' (' + FileConvertSize(file.size) + ')';
        });
        if (action == 'ADD') {
            console.log('Fichier(s) ajoute(s) par l\'utilisateur : ' + files);
        }
        datasFormulaire.append('dataTracking[fichiers]', files);
    }
    if (typeof cleDescription != "undefined" && cleDescription != null && cleDescription != '') {
        datasFormulaire.append('dataTracking[cleDescription]', cleDescription);
    }
    if (typeof action != "undefined" && action != null && action != '') {
        datasFormulaire.append('dataTracking[action]', action);
    }
    if (typeof detailFileOrigin != "undefined" ) {
        datasFormulaire.append('dataTracking[fichierSource]', detailFileOrigin['name'] + ' (' + detailFileOrigin['size'] + ')');
    }

    datasFormulaire.append('dataTracking[dateDebutAction]', dateTracking.getFullYear() + "-" + addZero(dateTracking.getMonth() + 1) + "-" + dateTracking.getDate() + " " + addZero(dateTracking.getHours()) + ":" + addZero(dateTracking.getMinutes()) + ":" + addZero(dateTracking.getSeconds()));
    datasFormulaire.append('dataTracking[debutActionTimestampMillisecond]', dateTrackingTimestampMillisecond);

    if (typeof element != "undefined") {
        datasFormulaire.append('dataTracking[typeFichier]', J(element).closest('form').find('input[name=type_fichier]').val());
        datasFormulaire.append('dataTracking[typeEnveloppe]', J(element).closest('form').find('input[name=type_enveloppe]').val());
        datasFormulaire.append('dataTracking[uid_response]', J('#form_uid_response').val());
        datasFormulaire.append('dataTracking[offre_id]', J('#form_offre_id').val());
    }

    J.ajax({
        type: "POST",
        url: Routing.generate('traking_inscrit_operation', {'consultation_id': consulation_ref, 'consultation_org': organisme}),
        async: true,
        processData: false,
        contentType: false,
        data: datasFormulaire,
        success: function (data) {
            if (data.hasOwnProperty('dataTracking')) {
                console.log("Tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] [Files = " + data['dataTracking']['fichiers'] + "] : SUCCES");
            } else if (data.hasOwnProperty('errors')) {
                console.log("Error tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] : " + data['errors']);
            }
        },
        error: function (error) {
            console.log("Error tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] : " + error);
        }
    });
}

function FileConvertSize(aSize)
{
    aSize = Math.abs(parseInt(aSize, 10));
    var def = [[1, 'octets'], [1024, 'ko'], [1024*1024, 'Mo'], [1024*1024*1024, 'Go'], [1024*1024*1024*1024, 'To']];
    for (var i=0; i<def.length; i++) {
        if (aSize<def[i][0]) {
            return (aSize/def[i-1][0]).toFixed(2)+' '+def[i-1][1];
        }
    }
}

function trackInscritOperation(element, data, cleDescription, action, asynchrone, detailFileOrigin)
{
    eventUserTracking.initEvent('UserOperationTrackingEvent', true, true);
    eventUserTracking.elementTracking        = element;
    eventUserTracking.cleDescriptionTracking = cleDescription;
    eventUserTracking.dataTracking           = data;
    eventUserTracking.actionTracking         = action;
    eventUserTracking.asyncTracking          = asynchrone;
    eventUserTracking.detailFileOriginTracking = detailFileOrigin;
    document.dispatchEvent(eventUserTracking);
}

function trackInscritOperationAnnulation(element, data, cleDescription, action, asynchrone, detailFileOrigin)
{
    eventUserTracking.initEvent('UserOperationTrackingEventAnnulation', true, true);
    eventUserTracking.elementTracking        = element;
    eventUserTracking.cleDescriptionTracking = cleDescription;
    eventUserTracking.dataTracking           = data;
    eventUserTracking.actionTracking         = action;
    eventUserTracking.asyncTracking          = asynchrone;
    eventUserTracking.detailFileOriginTracking = detailFileOrigin;
    document.dispatchEvent(eventUserTracking);
}

function trackUserOperationAnnulation(element, data, cleDescription, action, asynchrone, detailFileOrigin)
{
    var datasFormulaire = new FormData();

    var dateTracking = new Date(ServerDate());
    var dateTrackingTimestampMillisecond = ServerDate.now();
    if (dateTracking == undefined || !dateTracking instanceof Date) {
        dateTracking = new Date();
        dateTrackingTimestampMillisecond = dateTracking.now();
    }

    if (typeof asynchrone == "undefined" || asynchrone == null || asynchrone == '') {
        asynchrone = true;
    }
    if (typeof data != "undefined" && data != null && typeof data.files != "undefined" && data.files != null) {
        var files = '';
        J.each(data.files, function (index, file) {
            files += file.name + ' (' + FileConvertSize(file.size) + ')';
        });
        if (action == 'ADD') {
            console.log('Fichier(s) ajoute(s) par l\'utilisateur : ' + files);
        }
        datasFormulaire.append('dataTracking[fichiers]', files);
    }
    if (typeof cleDescription != "undefined" && cleDescription != null && cleDescription != '') {
        datasFormulaire.append('dataTracking[cleDescription]', cleDescription);
    }
    if (typeof action != "undefined" && action != null && action != '') {
        datasFormulaire.append('dataTracking[action]', action);
    }
    if (typeof detailFileOrigin != "undefined" ) {
        datasFormulaire.append('dataTracking[fichierSource]', detailFileOrigin['name'] + ' (' + detailFileOrigin['size'] + ')');
    }

    datasFormulaire.append('dataTracking[dateDebutAction]', dateTracking.getFullYear() + "-" + addZero(dateTracking.getMonth() + 1) + "-" + dateTracking.getDate() + " " + addZero(dateTracking.getHours()) + ":" + addZero(dateTracking.getMinutes()) + ":" + addZero(dateTracking.getSeconds()));
    datasFormulaire.append('dataTracking[debutActionTimestampMillisecond]', dateTrackingTimestampMillisecond);

    if (typeof element != "undefined") {
        datasFormulaire.append('dataTracking[typeFichier]', J(element).closest('form').find('input[name=type_fichier]').val());
        datasFormulaire.append('dataTracking[typeEnveloppe]', J(element).closest('form').find('input[name=type_enveloppe]').val());
        datasFormulaire.append('dataTracking[uid_response]', J('#form_uid_response').val());
        datasFormulaire.append('dataTracking[offre_id]', J('#form_offre_id').val());
    }

    J.ajax({
        type: "POST",
        url: Routing.generate('traking_inscrit_operation', {'consultation_id': consulation_ref, 'consultation_org': organisme}),
        async: asynchrone,
        processData: false,
        contentType: false,
        data: datasFormulaire,
        success: function (data) {
            if (data.hasOwnProperty('dataTracking')) {
                console.log("Tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] [Files = " + data['dataTracking']['fichiers'] + "] : SUCCES");
            } else if (data.hasOwnProperty('errors')) {
                console.log("Error tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] : " + data['errors']);
            }
        },
        error: function (error) {
            console.log("Error tracking user operation [cleDescription = " + cleDescription + "] [action = " + action + "] : " + error);
        }
    });
}


var goBackAfterSaveDume = false;

function saveAndBackToDepot(msgChampsDumeInvalide, goBack)
{
    if (J("#saveDume").is(":disabled")) {
        J("#error_message").addClass("alert alert-danger");
        J("#error_message").append('<i class="fa fa-exclamation-circle"></i>'+msgChampsDumeInvalide);
    } else {
        goBackAfterSaveDume = goBack;
        J("#error_message").removeClass("alert alert-danger");
        J("#error_message").html('');
        J("#saveDume").trigger('click');
    }
}
function afterSaveDumeOeSuccess()
{
    if (goBackAfterSaveDume == true) {
        J.ajax(Routing.generate('atexo_consultation_verifier_validation_Dume'),{
            method: "POST",
            data: {
                idCandidature: J("#idCandidature").val()
            }
        }).done(function (result) {
            var res = jQuery.parseJSON(result);
            J("#error_message_validation").hide();
            if (res.urlRedirection) {
                window.location = res.urlRedirection;
            } else if (res.afficherModal) {
                J("#modalDumeNotValid").modal("show");
            } else {
                J("#error_message_validation").show();
            }
        }).fail(function (result) {
            J("#error_message_validation").show();
        });
    }
}

function afterSaveDumeOeError(message)
{
    goBackAfterSaveDume = false;
}
function verifieSession()
{
    J.ajax(Routing.generate('atexo_consultation_depot_ping'), {
        method: "GET",
    }).done(function (result) {
        try {
            var json = JSON.parse(result);
            if (json.status == "KO") {
                window.location = Routing.generate('mpe_non_auth');
            }
        } catch (e) {
           // window.location = Routing.generate('mpe_non_auth');
        }
    });
}
var verifySession;
function pingSessionDepot()
{
    verifySession = setInterval(verifieSession, timePing);
}
function stopPingSessionDepot()
{
    clearInterval(verifySession);
}
