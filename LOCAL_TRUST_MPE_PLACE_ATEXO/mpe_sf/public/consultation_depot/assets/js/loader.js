// JavaScript Document

<!--


//////////////////////////////////////Debut Loader a l'interieur de la page//////////////////////////////////

/*Checks IE 6.0*/
var name = navigator.appName ;
var version = navigator.appVersion;

if (name == 'Microsoft Internet Explorer') {
	id = version.indexOf('MSIE');
	version = version.substring(id+5,id+9);
}


function showLoader() {
	
	el = document.getElementById("pageLoader");
	if(!el) {
		var body = document.getElementsByTagName("body")[0];
		var myLoader = document.createElement("div");
		myLoader.setAttribute('id','pageLoader');

		var pageHeight = document.body.offsetHeight;
		var pageWidth = document.body.offsetWidth;
		myLoader.innerHTML = '<div><img src="./bundles/atexoconsultationdepot/assets/images/ajax-loader.gif" alt="" title="Veuillez patienter" /><p>Veuillez patienter...</p></div>';
		if (version == '6.0;') {
			myLoader.innerHTML = '<div><iframe src="blank.html" scrolling="no" frameborder="0" id="loaderIframe"></iframe><img src="./bundles/atexoconsultationdepot/assets/images/ajax-loader.gif" alt="" title="Veuillez patienter..." /><p>Veuillez patienter...</p></div>';
		}

		/*Positionnement vertical du Div container par rapport a la taille de la fenetre*/
		var windowHeight = 0;
		if (typeof(window.innerHeight) == 'number') {
			windowHeight= window.innerHeight;
		}
		else {
			if (document.documentElement && document.documentElement.clientHeight) {
				windowHeight= document.documentElement.clientHeight;
			}
			else {
				if (document.body && document.body.clientHeight) {
					windowHeight = document.body.clientHeight;
				}
			}
		}
		var newHeight = Math.round(windowHeight/2) ;
	
		var windowScroll = document.documentElement.scrollTop;
		myLoader.style.top = ((windowHeight/2)+windowScroll-70)+'px';

		myLoader.style.left = (pageWidth/2)-80+'px';
		body.appendChild(myLoader);	
		
	}
	else {
		document.body.removeChild(el);
	}
}	

function showModalLoader() {
	
	el = document.getElementById("pageLoader");
	if(!el) {
		var body = document.getElementsByTagName("body")[0];
		var myLoader = document.createElement("div");
		myLoader.setAttribute('id','pageLoader');
		
		var pageHeight = document.body.offsetHeight;
		var pageWidth = document.body.offsetWidth;
		//myLoader.innerHTML = '<div><img src="./themes/images/ajax-loader.gif" alt="" title="Veuillez patienter" /><p>Veuillez patienter...</p></div>';
		myLoader.innerHTML = '<div><img src="./bundles/atexoconsultationdepot/assets/images/ajax-loader.gif" alt="" title="Veuillez patienter" /><p>Veuillez patienter...</p></div>';

		body.appendChild(myLoader);	
		
		J("#pageLoader").dialog({
			modal: true,
			resizable: false,
			dialogClass: 'modal-form'
		});	
		
	}
	else {
		J("#pageLoader").remove();	
	}
}	

//////////////////////////////////////Fin Loader a l'interieur de la page//////////////////////////////////


//-->

