/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
/*J(document).bind("dragover", function(e) {
 e.preventDefault();
 return false;
 });*/

/* global $, window */

var uuid = function () {
    var uuid = "", i, random;
    for (i = 0; i < 32; i++) {
        random = Math.random() * 16 | 0;

        if (i == 8 || i == 12 || i == 16 || i == 20) {
            uuid += "-"
        }
        uuid += (i == 12 ? 4 : (i == 16 ? (random & 3 | 8) : random)).toString(16);
    }
    return uuid;
}
var dynamicUploadActionMasking = function () {
    if (pageTransmissionAffichee == 0) {
        return 0;
    }
    J(".atx-is-highlighted .fileupload-body").each(function (index) {
        if (J(this).css("display") != "none") {
            J(".atx-is-highlighted .row-file_actions").each(function (index) {
                J(this).css("display", "none");
            });
            J(".atx-is-highlighted .row-jeton").each(function (index) {
                J(this).css("display", "none");
            });
        }
    });
}

var dynamicUploadActionDisplaying = function () {
    J(".atx-is-highlighted .fileupload-body").each(function (index) {
        if (J(this).css("display") != "none") {
            J(".atx-is-highlighted .row-file_actions").each(function (index) {
                J(this).css("display", "inline");
            });
            J(".atx-is-highlighted .row-jeton").each(function (index) {
                J(this).css("display", "block");
            });
        }
    });
}

var UPLOAD_FILE_TYPE = (function () {
    var private = {
        'ACTE': 'acte',
        'AFI': 'afi',
        'SIG': 'SIG'
    };
    return {
        get: function (name) {
            return private[name];
        }
    };
})();

var nbFile = 0;

var initUploadAtexo = function (initializedIds) {

    (function (J) {
        'use strict';

        var initDragLeave = function ($this) {
            $this.on('dragleave', function (e) {
                // dragleave callback implementation
                J(this).find('.drag-zone').removeClass('drag-over');
            });
        };
        var reloadButton = J('<a/>')
            .addClass('btn btn-primary reload')
            .on('click', function () {
                var $this = J(this),
                    data = $this.data();
                $this
                    .off('click')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.context.find('.progress').addClass('active');
                var reloadElement = data.context.find('div.actions > a.reload');
                var cancelElement = data.context.find('div.actions > a.cancel');
                data.context.find('div.error-area').text('');
                reloadElement.hide();
                cancelElement.show();
                data.submit().always(function () {
                    $this.remove();
                });
            });
        reloadButton.prop('title', 'Renvoyer');
        reloadButton.append('<i class="fa fa-refresh" aria-hidden="true"></i>');
        J('.form-file-upload').each(function () {

            var $thisComponentFileUpload = J(this);
            var $thisComponentFileUploadData = $thisComponentFileUpload.data('fileupload');
            if (typeof($thisComponentFileUploadData) == "undefined") {
                // Initialize the jQuery File Upload widget:
                $thisComponentFileUpload.fileupload({
                    // Uncomment the following to send cross-domain cookies:
                    autoUpload: true,
                    sequentialUploads: false,
                    minFileSize: 1,
                    //maxFileSize: 2048,
                    dropZone: J(this),
                    filesContainer: $thisComponentFileUpload.closest('.component-file-upload').find(' > .fileupload-body > .fileupload-table > .files'),
                    async: true,
                    messages: {
                        maxNumberOfFiles: 'Nombre maximal de fichier dépassé',
                        acceptFileTypes: 'Type de fichier non autorisé',
                        maxFileSize: 'Fichier trop volumineux',
                        minFileSize: 'Fichier est trop petit'
                    }
                }).bind('fileuploadsubmit', function (e, data) {

                    if(data.doublon || data.maxFileSizeSize){
                        data.jqXHR.abort();
                    }
                    e.stopImmediatePropagation();

                }).bind('fileuploadadd', function (e, data) {
                    data.doublon = false;
                    data.maxFileSizeSize = false;
                    var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                    var hiddenContent = (J('#names_all_files').val());
                    var listeFiles = new Array();
                    var names_all_files  = {};
                    if(hiddenContent){
                        names_all_files = JSON.parse(hiddenContent);
                        listeFiles = names_all_files[indexEnveloppe];
                        if(listeFiles){
                            jQuery.each(listeFiles, function (index, value) {
                                if(value ==data.files[0].name){
                                    data.doublon = true;
                                    data.files[0].doublon =true;
                                }
                            });
                        }else{
                            listeFiles = new Array();
                        }
                    }

                    if(data.files[0].size > maxSizeUpload && maxSizeUpload > 0) {
                        data.maxFileSizeSize = true;
                        data.files[0].tailleDepasse =true;
                    }

                    listeFiles.push(data.files[0].name);
                    names_all_files[indexEnveloppe] = listeFiles;
                    J('#names_all_files').val(JSON.stringify(names_all_files));

                    if(!data.doublon || !data.maxFileSizeSize ){
                        trackInscritOperation(this, data, null, 'ADD');
                        var that = J(this).data('fileupload') || J(this).data('blueimp-fileupload');
                        if (data.files && data.files.length >= that.options.maxNumberOfFiles) {
                            J(this).closest('form').hide();
                        } else {
                            J(this).closest('form').show();
                        }
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploadstart', function (e) {
                }).bind('fileuploaddone', function (e, data) {
                    if(!data.doublon || !data.maxFileSizeSize) {
                        var that = J(this).data('fileupload') || J(this).data('blueimp-fileupload');
                        if (data.files && data.files.length >= that.options.maxNumberOfFiles) {
                            J(this).closest('form').hide();
                        } else {
                            J(this).closest('form').show();
                        }
                        J(this).find('.drag-zone').removeClass('drag-over');

                        // Callback for successful upload requests
                        if (J(this).closest('form').find('input[name=enveloppe_id]').val() == '') {
                            J(this).closest('form').find('input[name=enveloppe_id]').val(data.result.files[0].enveloppe);
                        }
                        var files_collections = JSON.parse(J(this).closest('form').find('input[name=files_collections]').val());
                        files_collections.push(data.result.files[0]);
                        J(this).closest('form').find('input[name=files_collections]').val(JSON.stringify(files_collections));
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploaddragover', function (e) {
                    //J(this).find('.fileupload-buttonbar.drag-zone').addClass('drag-over');
                }).bind('fileuploadsend', function (e, data) {
                    if(!data.doublon || !data.maxFileSizeSize) {
                        // La ligne suivante permet de regrouper le fichier et son jeton dans une enveloppe DOM (span) comme etant des frères (siblings)
                        J(data.context).wrap('<span role="Couple-fichier-jeton" class="siblings"></span>');
                        if (jetonForm.trim() !== '') {
                            var form = J(data.form);
                            var fileName = data.files[0].name;
                            var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                            if (form.data('type') !== UPLOAD_FILE_TYPE.get('SIG') && fileNameExt.toLowerCase() != "pdf") {
                                var idTokenPresentation = uuid();
                                J(data.context).after(jetonForm);
                                var presentationDiv = J(data.context).closest('span.siblings').find('div.fileupload-table').children('div')[0];
                                var uploadTokenForm = J(data.context).closest('span.siblings').find('form.form-jeton-upload');
                                J(presentationDiv).attr('id', 'jeton-file-' + idTokenPresentation);
                                J(uploadTokenForm).attr('data-files-container', "#jeton-file-" + idTokenPresentation);
                                var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                                initUploadJetonAtexo(indexEnveloppe,initializedIds);

                            }
                        }
                        e.stopImmediatePropagation();
                    }
                }).bind('fileuploadsent', function (e, data) {//Voir https://github.com/blueimp/jQuery-File-Upload/wiki/Options#sent
                    if(!data.doublon || !data.maxFileSizeSize) {
                        trackInscritOperation(this, data, 'DESCRIPTION34');
                        if (nbFile == 0) {
                            pingSessionDepot();
                        }
                        nbFile++;
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploadfinished', function (e,data) {
                    if(data.jqXHR != undefined && data.jqXHR.status!=500) {
                        (nbFile > 0) ? nbFile-- : 0;
                        if (nbFile == 0) {
                            trackInscritOperation(this, null, 'DESCRIPTION38');
                            // La validation automatique du dépot, impliquée par le concept de la validation anticipée
                            stopPingSessionDepot();
                            dynamicUploadActionMasking();
                            // La validation automatique du dépot, impliquée par le concept de la validation anticipée
                            if (depotAutomatique == 1) {
                                return validerReponseConsultation();
                            }
                        }
                        dynamicUploadActionMasking();
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploadcompleted', function (e, data) {
                    if(!data.doublon || !data.maxFileSizeSize) {
                        trackInscritOperation(this, data, 'DESCRIPTION35');
                        var currentFile = data.result.files[0];
                        var fileName = currentFile.name;
                        var fileNameExt = fileName.substr(fileName.lastIndexOf('.') + 1);
                        if (fileNameExt.toLowerCase() == "pdf" && (currentFile.infos_certificat && (currentFile.infos_certificat.signatureValide == null || currentFile.infos_certificat.emetteur == null || currentFile.infos_certificat.periodiciteValide == null))) {
                            var idTokenPresentation = uuid();
                            J(data.context).after(jetonForm);
                            var presentationDiv = J(data.context).closest('span.siblings').find('div.fileupload-table').children('div')[0];
                            var uploadTokenForm = J(data.context).closest('span.siblings').find('form.form-jeton-upload');
                            J(presentationDiv).attr('id', 'jeton-file-' + idTokenPresentation);
                            J(uploadTokenForm).attr('data-files-container', "#jeton-file-" + idTokenPresentation)
                            var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                            initUploadJetonAtexo(indexEnveloppe,initializedIds);
                        }

                        var wrapper = J(data.context).closest('span.siblings');
                        var currentTokenForm = wrapper.find('form.form-jeton-upload');

                        currentTokenForm.find("input:hidden[name='type_enveloppe']").val(currentFile.type_enveloppe);
                        currentTokenForm.find("input:hidden[name='enveloppe']").val('env_lot-' + currentFile.numlot + '_' + currentFile.acronyme_enveloppe);
                        currentTokenForm.find("input:hidden[name='offre_id']").val(currentFile.offre);
                        currentTokenForm.find("input:hidden[name='fichier']").val(currentFile.fichier);
                        var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                        currentTokenForm.find("input:hidden[name='info_index_enveloppe']").val(indexEnveloppe);
                        currentTokenForm.attr('data-fichier-source', currentFile.fichier);
                        var element = wrapper.children('div').last();
                        if (element.hasClass('template-container-upload-jeton') && element.data('token-size') <= jetonSignatureMaxFileSize) {
                            var startBtn = element.find('a.start-sending-token');
                            J(startBtn).trigger("click");
                            //J( startBtn).click();
                        }
                        e.stopImmediatePropagation();
                    }
                }).bind('fileuploadfail', function (e, data) {


                    var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                    var hiddenContent = (J('#names_all_files').val());
                    var listeFiles = new Array();
                    var names_all_files  = {};
                    if(hiddenContent){
                        names_all_files = JSON.parse(hiddenContent);
                        listeFiles = names_all_files[indexEnveloppe];
                        if(listeFiles){
                            var nameMyFile = data.files[0].name;
                            var index = listeFiles.indexOf(nameMyFile);
                            listeFiles.splice(index,1);

                            names_all_files[indexEnveloppe] = listeFiles;
                            J('#names_all_files').val(JSON.stringify(names_all_files));
                        }
                    }

                    depotAutomatique = 0;
                    validationAnticipee = 0;
                    var target = J(data.context).context;
                    var condition1 = J(this).attr('data-type') === UPLOAD_FILE_TYPE.get('ACTE');
                    var condition3 = J(this).attr('data-type') === UPLOAD_FILE_TYPE.get('AFI');
                    var condition2 = data.jqXHR == undefined || !data.jqXHR.readyState;
                    if ((condition1 && condition2) || (condition3 && condition2)) {
                        J(this).show();
                    }

                    if(data.jqXHR != undefined && data.jqXHR.status!=500) {
                        trackInscritOperation(this, data, null, 'INTERRUPTION');
                        J(data.context).closest('span.siblings').remove();

                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploadfailed', function (e, data) {
                    var that = J(this).data('fileupload') || J(this).data('blueimp-fileupload');
                    J.each(data.files, function (index, file) {
                        var node = J('<div class="actions">');
                        if (!index) {
                            node
                                .append(reloadButton.clone(true).data(data));
                            J(data.context).closest('span.siblings').remove();

                        }
                        var cancelElement = data.context.find('div.actions > a.cancel');
                        cancelElement.hide();
                    });
                    e.stopImmediatePropagation();
                    //depotAutomatique = 0;
                }).bind('fileuploaddestroy', function (e, data) {
                    var wrapper = J(data.context).closest('span.siblings');
                    var tokenRow = J(wrapper).children('div.template-container-upload-jeton');
                    var deleteTokenBtn = tokenRow.find('a.delete');
                    var cancelTokenBtn = tokenRow.find('a.cancel');
                    var nameMyFile = '';
                    var listefilesCollections = JSON.parse(J(this).closest('form').find('input[name=files_collections]').val());
                    jQuery.each(listefilesCollections, function (index, value) {
                        if(value.deleteUrl == data.url){
                            nameMyFile = value.name;
                        }
                    });
                    if(nameMyFile){
                        var hidden = J(data.context).closest('span.siblings').find('input[name=pere_deleted]');
                        J(hidden).val(1);
                        var indexEnveloppe = (J(this).closest('form').find('input[name=info_index_enveloppe]').val());
                        var hiddenContent = (J('#names_all_files').val());
                        var listeFiles = new Array();
                        var names_all_files  = {};
                        if(hiddenContent){
                            names_all_files = JSON.parse(hiddenContent);
                            listeFiles = names_all_files[indexEnveloppe];
                            if(listeFiles){
                                listeFiles = jQuery.grep(listeFiles, function(value) {
                                    return value != nameMyFile;
                                });
                                //listeFiles.push(data.files[0].name);

                                names_all_files[indexEnveloppe] = listeFiles;
                                J('#names_all_files').val(JSON.stringify(names_all_files));
                            }
                        }
                        J(wrapper).hide();
                    }
                    var condition1 = J(this).attr('data-type') === UPLOAD_FILE_TYPE.get('ACTE');
                    var condition3 = J(this).attr('data-type') === UPLOAD_FILE_TYPE.get('AFI');
                    var condition2 = data.jqXHR == undefined || !data.jqXHR.readyState;

                    if ((condition1 && condition2) || (condition3 && condition2)) {
                        J(this).show();
                    }
                    if (deleteTokenBtn.length) {
                        deleteTokenBtn.get(0).click();
                    } else if (cancelTokenBtn.length) {
                        cancelTokenBtn.get(0).click();
                    } else {
                        J(wrapper).remove();
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploaddestroyed', function (e, data) {
                });
                initDragLeave($thisComponentFileUpload);

                // Enable iframe cross-domain access via redirect option:
                $thisComponentFileUpload.fileupload(
                    'option',
                    'redirect',
                    window.location.href.replace(
                        /\/[^\/]*$/,
                        '/cors/result.html?%s'
                    )
                );

            }
        });


    })(J);
};
initUploadAtexo([]);

J(document).bind('dragover', function (e) {
    var dropZone = J('.drag-zone'),
        foundDropzone,
        timeout = window.dropZoneTimeout;
    if (!timeout) {
        dropZone.addClass('in');
    }
    else {
        clearTimeout(timeout);
    }
    var found = false,
        node = e.target;

    do {

        if (J(node).hasClass('drag-zone')) {
            found = true;
            foundDropzone = J(node);
            break;
        }

        node = node.parentNode;

    } while (node != null);

    dropZone.removeClass('in hover');

    if (found) {
        foundDropzone.addClass('hover');
    }

    window.dropZoneTimeout = setTimeout(function () {
        window.dropZoneTimeout = null;
        dropZone.removeClass('in hover');
    }, 100);
});