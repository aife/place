/*
 * jQuery File Upload Plugin JS Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
/*J(document).bind("dragover", function(e) {
 e.preventDefault();
 return false;
 });*/
/* global $, window */
var initUploadJetonAtexo = function (indexEnveloppInit, initializedIds) {


    (function (J) {
        'use strict';
        J('.form-jeton-upload').each(function () {
            var $thisComponentFileUpload = J(this);
            if (!initializedIds.includes($thisComponentFileUpload.data('filesContainer'))) {
                // Initialize the jQuery File Upload widget:
                $thisComponentFileUpload.fileupload({
                    // Uncomment the following to send cross-domain cookies:
                    autoUpload: false,
                    sequentialUploads: true,
                    replaceFileInput: false,
                    minFileSize: 1,
                    maxFileSize: jetonSignatureMaxFileSize,
                    acceptFileTypes: /(\.|\/)(xml|p7s)$/i,
                    dropZone: J(this),
                    filesContainer: $thisComponentFileUpload.closest('.component-file-upload').find(' > .fileupload-body > .fileupload-table > .files'),
                    messages: {
                        maxNumberOfFiles: 'Nombre maximal de fichier dépassé',
                        acceptFileTypes: 'Seuls les fichiers xml et p7s sont acceptés.',
                        maxFileSize: 'Fichier trop volumineux (' + (jetonSignatureMaxFileSize / 1048576).toFixed(0) + ' Mo max.)',
                        minFileSize: 'Fichier est trop petit'
                    }
                }).bind('fileuploadadd', function (e, data) {
                    data.doublon = false;
                    if (indexEnveloppInit) {
                        var indexEnveloppe = indexEnveloppInit;
                    } else {
                        var indexEnveloppe = J(data.form).find('input[name=info_index_enveloppe]').val();
                    }
                    var hiddenContent = J('#names_all_files').val();
                    var listeFiles = new Array();
                    var names_all_files = {};
                    if (hiddenContent) {
                        names_all_files = JSON.parse(hiddenContent);
                        listeFiles = names_all_files[indexEnveloppe];
                        if (listeFiles) {
                            jQuery.each(listeFiles, function (index, value) {
                                if (value == data.files[0].name) {
                                    data.doublon = true;
                                    data.files[0].doublon = true;
                                }
                            });
                        } else {
                            listeFiles = new Array();
                        }
                    }

                    listeFiles.push(data.files[0].name);
                    names_all_files[indexEnveloppe] = listeFiles;
                    J('#names_all_files').val(JSON.stringify(names_all_files));

                    if (!data.doublon) {
                        trackInscritOperation(this, data, null, 'ADD');
                        var tokenFormContext = J(data.form).closest('div.template-container-upload-jeton');
                        tokenFormContext.attr('data-token-size', data.files[0].size);
                        /*----- Bloc vérification pour IE -----*/
                        /*---- Il permet de forcer le vidage du div du chargement des templates d'upload et de download ----*/
                        var el = tokenFormContext.find('div[id^="jeton-file-"]').children();
                        el.length > 0 ? el.remove() : '';
                        /*----- Fin bloc vérification pour IE -----*/
                        var form = J(data.form);
                        var div = form.closest('div.fileupload-buttonbar');
                        div.hide();
                        var fileDeleteUrl = J(data.form).closest('span.siblings').children('div.template-download').find('a.delete').attr('data-url');
                        if (J(data.form).closest('span.siblings').find('div').hasClass('template-download') && fileDeleteUrl != undefined) {
                            $thisComponentFileUpload.fileupload({
                                autoUpload: true,
                            });
                        }
                    }
                    e.stopImmediatePropagation();
                }).bind('fileuploadstart', function (e) {
                }).bind('fileuploadsubmit', function (e) {
                    e.stopImmediatePropagation();
                    $thisComponentFileUpload.fileupload({
                        autoUpload: false,
                    });
                }).bind('fileuploaddone', function (e, data) {
                    e.stopImmediatePropagation();
                    var files_collections = JSON.parse(J(data.form).find('input[name=files_collections]').val());
                    files_collections.push(data.result.files[0]);
                    J(data.form).find('input[name=files_collections]').val(JSON.stringify(files_collections));
                }).bind('fileuploaddragover', function (e) {
                    e.stopImmediatePropagation();
                }).bind('fileuploadsent', function (e, data) {//Voir https://github.com/blueimp/jQuery-File-Upload/wiki/Options#sent
                    trackInscritOperation(this, data, 'DESCRIPTION34');
                    e.stopImmediatePropagation();
                    nbFile++;
                }).bind('fileuploadfinished', function (e, data) {
                    console.log("**** fileuploadfinished **** ");
                    if (data.jqXHR != undefined && data.jqXHR.status != 500) {
                        e.stopImmediatePropagation();
                        (nbFile > 0) ? nbFile-- : 0;
                        if (nbFile == 0) {
                            trackInscritOperation(this, null, 'DESCRIPTION38');
                            dynamicUploadActionMasking();
                            if (depotAutomatique == 1) {
                                return validerReponseConsultation();
                            }
                        }
                    }
                }).bind('fileuploadfail', function (e, data) {

                    if (indexEnveloppInit) {
                        var indexEnveloppe = indexEnveloppInit;
                    } else {
                        var indexEnveloppe = J(data.form).find('input[name=info_index_enveloppe]').val();
                    }
                    var hiddenContent = (J('#names_all_files').val());
                    var listeFiles = new Array();
                    var names_all_files = {};
                    if (hiddenContent) {
                        names_all_files = JSON.parse(hiddenContent);
                        listeFiles = names_all_files[indexEnveloppe];
                        if (listeFiles) {
                            var nameMyFile = data.files[0].name;
                            var index = listeFiles.indexOf(nameMyFile);
                            listeFiles.splice(index, 1);

                            names_all_files[indexEnveloppe] = listeFiles;
                            J('#names_all_files').val(JSON.stringify(names_all_files));
                        }
                    }

                    e.stopImmediatePropagation();
                    depotAutomatique = 0;
                    validationAnticipee = 0;
                    var target = J(data.context).context;
                    var condition1 = J(this).attr('data-type') === UPLOAD_FILE_TYPE.get('SIG');
                    var condition2 = data.jqXHR == undefined || !data.jqXHR.readyState;
                    var deleteFile = J(this).closest('form').find('input[name=pere_deleted]').val();
                    if (deleteFile == 1) {
                        J(this).closest('span.siblings').remove();
                    } else if (condition1 && condition2) {
                        J(this).closest('div.fileupload-buttonbar').show();
                    }

                    if (data.jqXHR != undefined && data.jqXHR.status != 500) {
                        trackInscritOperation(this, data, null, 'INTERRUPTION');
                        J(this).trigger('reset');

                    }
                }).bind('fileuploadfailed', function (e, data) {

                }).bind('fileuploaddestroy', function (e, data) {
                    e.stopImmediatePropagation();
                    // Ne re afficher le formulaire d'uplod de jeton que si ligne (row upload/download) du fichier principal est bien présenter au niveau du DOM.
                    var priPrinted = J(data.context).closest('span.siblings').children('div.piece-libre').length;
                    var aePrinted = J(data.context).closest('span.siblings').children('div.acte-engagement').length;
                    var aiPrinted = J(data.context).closest('span.siblings').children('div.afi-engagement').length;
                    var nameMyFile = '';
                    var listefilesCollections = JSON.parse(J(this).closest('form').find('input[name=files_collections]').val());
                    jQuery.each(listefilesCollections, function (index, value) {
                        if (value.deleteUrl == data.url) {
                            nameMyFile = value.name;
                        }
                    });
                    if (nameMyFile) {
                        var deleteFile = J(this).closest('form').find('input[name=pere_deleted]').val();
                        if (deleteFile == 1) {
                            J(this).closest('span.siblings').remove();
                        } else if (priPrinted || aePrinted || aiPrinted) {
                            J(this).closest('div.fileupload-buttonbar').show();
                        } else {
                            J(this).closest('div.fileupload-buttonbar').hide();
                        }
                        if (indexEnveloppInit) {
                            var indexEnveloppe = indexEnveloppInit;
                        } else {
                            var indexEnveloppe = J(data.form).find('input[name=info_index_enveloppe]').val();
                        }
                        var hiddenContent = (J('#names_all_files').val());
                        var listeFiles = new Array();
                        var names_all_files = {};
                        if (hiddenContent) {
                            names_all_files = JSON.parse(hiddenContent);
                            listeFiles = names_all_files[indexEnveloppe];
                            if (listeFiles) {
                                listeFiles = jQuery.grep(listeFiles, function (value) {
                                    return value != nameMyFile;
                                });
                                //listeFiles.push(data.files[0].name);

                                names_all_files[indexEnveloppe] = listeFiles;
                                J('#names_all_files').val(JSON.stringify(names_all_files));
                            }
                        }
                    }
                    J(this).trigger('reset');
                }).bind('fileuploaddestroyed', function (e, data) {

                }).bind('fileuploadcompleted', function (e, data) {
                    var wrapper = J(data.context).closest('span.siblings');
                    var currentTokenForm = wrapper.find('form.form-jeton-upload');
                    var detailFileOrigin = [];
                    detailFileOrigin['name'] = J("[data-fichier=" + currentTokenForm.attr('data-fichier-source') + "]").find('span.name-link').html();
                    detailFileOrigin['size'] = J("[data-fichier=" + currentTokenForm.attr('data-fichier-source') + "]").find('span.size').html();
                    trackInscritOperation(this, data, 'DESCRIPTION36', null, true, detailFileOrigin);
                    e.stopImmediatePropagation();
                });

                // Enable iframe cross-domain access via redirect option:
                $thisComponentFileUpload.fileupload(
                    'option',
                    'redirect',
                    window.location.href.replace(
                        /\/[^\/]*$/,
                        '/cors/result.html?%s'
                    )
                );
                initializedIds.push($thisComponentFileUpload.data('filesContainer'))
            }

        });


    })(J);
};
