J(document).ready(function () {

    //Feedback message appear with fadIn
    J(".bloc-message:visible").hide().fadeIn(1200);

    //Display alt value as a title on mouseover
    J('img').hover(function () {
        if (J(this).attr('alt') != '') {
            var title = J(this).attr('alt');
            J(this).attr('title', title);
        }
    }, function () {
        J(this).removeAttr('title');
    });

    //Display tootltip for abbr
    J("abbr.info-suite").hover(function () {
        J(this).data('title', J(this).attr("title"));
        J(this).attr("title", "");
    }, function () {
        J(this).attr("title", J(this).data('title'));
    });


    J('#ficheConseil').hide().fadeIn(800);

    J(".btn-toggle").click(function () {
        J(".fiche-content").slideToggle(200);
        J("#ficheConseil").toggleClass("collapsed").toggleClass("expanded");
    })

    //Fonction toggle detail
    J(".detail-toggle-all").click(function () {
        if (J(this).hasClass("btn-closed")) {
            J(this).toggleClass("btn-closed");
            J(".detail-toggle").removeClass("btn-closed");
            J(".ligne-detail-lot").removeClass("expanded").addClass("collapsed");

        } else {
            J(this).addClass("btn-closed");
            J(".detail-toggle").addClass("btn-closed");
            J(".ligne-detail-lot").removeClass("collapsed").addClass("expanded");
        }
    });

    J(".detail-toggle").click(function () {
        if (J(this).hasClass("btn-closed")) {
            J(this).toggleClass("btn-closed");
            J(this).parents('tr').next(".ligne-detail-lot").removeClass("expanded").addClass("collapsed");
        } else {
            J(this).addClass("btn-closed");
            J(this).parents('tr').next(".ligne-detail-lot").removeClass("collapsed").addClass("expanded");
        }
    });

    J(".detail-toggle-one-lot").click(function () {
        if (J(this).hasClass("clicked")) {
            J(this).toggleClass("btn-closed");
            J(this).parents('tr').next(".ligne-detail-one-lot").removeClass("expanded").addClass("collapsed");
        } else {
            J(this).addClass("btn-closed");
            J(this).parents('tr').next(".ligne-detail-one-lot").removeClass("collapsed").addClass("expanded");
        }
    });


    J('.expandTogglePanels').click(function () {
        J('.panel').css('display', 'block');
        J('.title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
    });
    J('.collapseTogglePanels').click(function () {
        J('.panel').css('display', 'none');
        J('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
    });
    J(".definir-forme-prix").click(function () {
        J(".modal-form-prix").dialog({
            modal: true,
            resizable: false,
            closeText: 'Fermer',
            title: '<h2>' + this.title + '</h2>',
            dialogClass: 'modal-form popup-small2',
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    J(".modal-form-prix").dialog('close');
                })
            }
        });
    });
    J(".saisir-appartenance-tranches").click(function () {
        J(".modal-appartenance-tranches").dialog({
            modal: true,
            resizable: false,
            closeText: 'Fermer',
            title: '<h2>' + this.title + '</h2>',
            dialogClass: 'modal-form popup-small2',
            open: function () {
                jQuery('.ui-widget-overlay').bind('click', function () {
                    J(".modal-appartenance-tranches").dialog('close');
                })
            }
        });
    });

});

//Disable toggle panel onclick
function disableTogglePanels(myInput, panelClass) {
    var panel = J('.' + panelClass);
    if (myInput.checked == true) {
        panel.addClass('panel-off');
        //panel.css('pointer-events','none');
        panel.find('.title-toggle-open').addClass('detail-toggle-inactive');
        panel.find('.title-toggle').addClass('detail-toggle-inactive');
        J('.cover').show();
    }
    else {
        return false;
    }
}
//Enable toggle panel onclick
function enableTogglePanels(myInput, panelClass) {
    var panel = J('.' + panelClass);
    if (myInput.checked == true) {
        //panel.css('pointer-events','inherit');
        panel.removeClass('panel-off');
        J('.detail-toggle-inactive').css('class', 'title-toggle-open');
        panel.find('.title-toggle-open').removeClass('detail-toggle-inactive');
        panel.find('.title-toggle').removeClass('detail-toggle-inactive');
        J('.cover').hide();
    }
    else {
        return false;
    }
}

function initFunctions() {

    if (document.getElementById("main-part")) {

        if (document.getElementById("menu") && document.getElementById("main-part").offsetHeight) {

            resetElementHeight();

        }

        else {
        }

    }


    if (document.getElementById("ctl0_CONTENU_PAGE_mainPart")) {


        if (document.getElementById("menu") && document.getElementById("ctl0_CONTENU_PAGE_mainPart").offsetHeight) {

            resetElementHeight();

        }

        else {
        }

    }

    else {
    }

    open_ext_link();

}


function resetElementHeight() {

    if (document.getElementById("main-part")) {

        divHeight = document.getElementById("main-part").offsetHeight;

        leftMenu = document.getElementById("menu").offsetHeight;


        highestElement = Math.max(divHeight, leftMenu);


        if (leftMenu < divHeight) {

            document.getElementById("menu").style.height = highestElement + 'px';

        }

        if (leftMenu == divHeight) {

        }

        if (leftMenu > divHeight) {

            //alert('left plus grand');

            document.getElementById("main-part").style.height = 'auto';

            document.getElementById("menu").style.height = 'auto';

            divHeight = document.getElementById("main-part").offsetHeight;

            leftMenu = document.getElementById("menu").offsetHeight;

            highestElement = Math.max(divHeight, leftMenu);

            document.getElementById("menu").style.height = highestElement + 'px';

        }


    }


    if (document.getElementById("ctl0_CONTENU_PAGE_mainPart")) {


        divHeight = document.getElementById("ctl0_CONTENU_PAGE_mainPart").offsetHeight;

        leftMenu = document.getElementById("menu").offsetHeight;


        highestElement = Math.max(divHeight, leftMenu);


        if (leftMenu < divHeight) {

            document.getElementById("menu").style.height = highestElement + 'px';

        }

        if (leftMenu == divHeight) {

        }

        if (leftMenu > divHeight) {

            //alert('left plus grand');

            document.getElementById("ctl0_CONTENU_PAGE_mainPart").style.height = 'auto';

            document.getElementById("menu").style.height = 'auto';

            divHeight = document.getElementById("ctl0_CONTENU_PAGE_mainPart").offsetHeight;

            leftMenu = document.getElementById("menu").offsetHeight;

            highestElement = Math.max(divHeight, leftMenu);

            document.getElementById("menu").style.height = highestElement + 'px';

        }

    }

    else {
    }

}


function checkHeight() {


    if (leftMenu < divHeight) {

        alert('left plus petit');

        document.getElementById("menu").style.height = divHeight + "px";

        leftMenu = document.getElementById("menu").style.height;

    }

    else if (leftMenu == divHeight) {

        alert('left egal');


        document.getElementById("menu").style.height = 'auto';

        leftMenu = document.getElementById("menu").offsetHeight;

    }

    else if (leftMenu > divHeight) {

        alert('left plus grand');

        //alert('no');

        //alert('hgf');

        divHeight = document.getElementById("menu").offsetHeight;

        document.getElementById("menu").style.height = offsetHeight;

    }

}


/*set left-menu background height*/

window.onload = function () {

    if (document.getElementById("main-part")) {


        if (document.getElementById("menu") && document.getElementById("main-part").offsetHeight) {

            resetElementHeight();

        }

        else {
        }

    }

    if (document.getElementById("ctl0_CONTENU_PAGE_mainPart")) {


        if (document.getElementById("menu") && document.getElementById("ctl0_CONTENU_PAGE_mainPart").offsetHeight) {

            resetElementHeight();

        }

        else {
        }

    }

    else {
    }

    open_ext_link();

}


/*date dans le header*/

setInterval('dispDate()', 1000);

function dispDate() {

    myDate = new Date();

    now = new Date();

    jour = now.getDate();

    mois = now.getMonth() + 1;

    annee = now.getFullYear();

    heure = now.getHours();

    minute = now.getMinutes();


    heure = ((heure < 10) ? "0" + heure : heure);

    minute = ((minute < 10) ? "0" + minute : minute);


    var lettres = ("" + now.toGMTString() + "");


    //On recherche le jour de la semaine

    var joursemaine = lettres.substring(0, 3);

    var js = "";

    var mois2 = "";


    if (joursemaine == "Mon") {

        js = "Lundi";
    }

    if (joursemaine == "Tue") {

        js = "Mardi";
    }

    if (joursemaine == "Wed") {

        js = "Mercredi";
    }

    if (joursemaine == "Thu") {

        js = "Jeudi";
    }

    if (joursemaine == "Fri") {

        js = "Vendredi";
    }

    if (joursemaine == "Sat") {

        js = "Samedi";
    }

    if (joursemaine == "Sun") {

        js = "Dimanche";
    }


    //On recherche le mois

    if (mois == "1") {

        mois2 = "janv.";
    }

    if (mois == "2") {

        mois2 = "f&eacute;.";
    }

    if (mois == "3") {

        mois2 = "mars";
    }

    if (mois == "4") {

        mois2 = "avril";
    }

    if (mois == "5") {

        mois2 = "mai";
    }

    if (mois == "6") {

        mois2 = "juin";
    }

    if (mois == "7") {

        mois2 = "juillet";
    }

    if (mois == "8") {

        mois2 = "ao&ucirc;t";
    }

    if (mois == "9") {

        mois2 = "sept.";
    }

    if (mois == "10") {

        mois2 = "oct.";
    }

    if (mois == "11") {

        mois2 = "nov.";
    }

    if (mois == "12") {

        mois2 = "d&eacute;.";
    }


    var eme = "";

    if (jour == 1) {

        eme = " er";
    }


    if (document.getElementById('displayTime')) {

        document.getElementById('displayTime').innerHTML = (js + ' ' + jour + eme + ' ' + mois2 + ' ' + annee + ' ' + heure + ':' + minute);

    }

}


/*Passer à l'onglet suivant*/

function showNextTab() {


    var myNav = document.getElementById('form-nav');

    for (i = 0; i < myNav.childNodes.length; i++) {

        if (myNav.childNodes[i].className == 'tab-on') {

            var myTab = myNav.childNodes[i].id;

            var myLayer = (myTab.substring(7, 6));

        }

    }


    var myTabPosition = (myTab.substring(7, 6));

    d = myTabPosition * 1;

    var nextTabId = 'onglet' + (d + 1);

    //alert(nextTabId);

    document.getElementById(myTab).className = 'tab';

    document.getElementById(nextTabId).className = 'tab-on';

    myTab = nextTabId;

    //alert('onglet'+'Layer'+d);

    document.getElementById('onglet' + 'Layer' + d).style.display = 'none';

    document.getElementById('onglet' + 'Layer' + (d + 1)).style.display = 'block';


    initFunctions();

}


/*Passer à l'onglet suivant*/

function showNextTabMulti() {


    var myNav = document.getElementById('form-nav-multi');

    for (i = 0; i < myNav.childNodes.length; i++) {

        if (myNav.childNodes[i].className == 'tab-on') {

            var myTab = myNav.childNodes[i].id;

            var myLayer = (myTab.substring(7, 6));

        }

    }


    var myTabPosition = (myTab.substring(7, 6));

    d = myTabPosition * 1;

    var nextTabId = 'onglet' + (d + 1);

    //alert(nextTabId);

    document.getElementById(myTab).className = 'tab';

    document.getElementById(nextTabId).className = 'tab-on';

    myTab = nextTabId;

    //alert('onglet'+'Layer'+d);

    document.getElementById('onglet' + 'Layer' + d).style.display = 'none';

    document.getElementById('onglet' + 'Layer' + (d + 1)).style.display = 'block';


    initFunctions();

}


/*Checks IE 6.0*/

var name = navigator.appName;

var version = navigator.appVersion;


if (name == 'Microsoft Internet Explorer') {

    id = version.indexOf('MSIE');

    version = version.substring(id + 5, id + 9);

}


/*open close panel*/

function togglePanel(myPosition, myPanel) {

    var layer = document.getElementById(myPanel);

    if (layer.style.display == 'none') {

        layer.style.display = 'block';

        myPosition.className = "title-toggle-open";

    }

    else if (layer.style.display == 'block') {

        layer.style.display = 'none';

        myPosition.className = "title-toggle";

    }

    initFunctions();

}


/*Menu gauche rubrique*/

function toggleMenu(myItem) {


    var myToggleItem = document.getElementById(myItem);

    var getParentClass = myToggleItem.parentNode.className;


    if (myToggleItem.style.display == 'block') {

        myToggleItem.style.display = 'none';

        if (getParentClass == 'menu-open menu-on') {

            myToggleItem.parentNode.className = 'menu-open menu-on' + ' toggle-off';

        }

        if (getParentClass == 'menu-open') {

            myToggleItem.parentNode.className = 'menu-open' + ' toggle-off';

        }

        if (getParentClass == 'menu-open toggle-on') {

            myToggleItem.parentNode.className = 'menu-open';

        }

        else {
        }

    }

    else if (myToggleItem.style.display == 'none') {

        myToggleItem.style.display = 'block';

        if (getParentClass == 'menu-open menu-on toggle-off') {

            myToggleItem.parentNode.className = 'menu-open menu-on';

        }

        if (getParentClass == 'menu-open toggle-on') {

            myToggleItem.parentNode.className = 'menu-open';

        }

        if (getParentClass == 'menu-open') {

            myToggleItem.parentNode.className = 'menu-open toggle-on';

        }

        else {
        }

    }

    document.getElementById('menu').style.height = '100%';

    initFunctions();

}


/*sous menu avec fleche*/

function toggleSousMenu(myPosition, myItem) {


    var myToggleItem = document.getElementById(myItem);

    if (myToggleItem.style.display == 'block') {

        myToggleItem.style.display = 'none';

        myPosition.parentNode.className = 'off';

    }

    else if (myToggleItem.style.display == 'none') {

        myToggleItem.style.display = 'block';

        myPosition.parentNode.className = 'on';

    }

    initFunctions();

}


function resetSsmenu(myPosition) {

    var mySsliste = myPosition.parentNode.childNodes;

    for (g = 0; g < mySsliste.length; g++) {

        if (mySsliste[g].tagName == 'UL') {

            var mySilbing = mySsliste[g].childNodes;

            for (a = 0; a < mySilbing.length; a++) {

                if (mySilbing[a].nodeType == 1 && mySilbing[a].tagName == 'LI') {

                    mySilbing[a].className = 'off';

                    //alert(mySilbing[1].tagName);

                    if (navigator.appName == "Microsoft Internet Explorer") {

                        mySilbing[0].className = 'on';

                    }

                    else {

                        mySilbing[1].className = 'on';

                    }


                }

            }

        }

    }

}


/*Show layer according to tabs*/

function showHideLayer(myLayer, myParent, nbOnglet) {

    for (t = 1; t <= nbOnglet; t++) {

        layers = 'ongletLayer' + t;

        document.getElementById(layers).style.display = 'none';

    }

    document.getElementById(myLayer).style.display = 'block';


    var myNav = document.getElementById('form-nav');

    for (i = 0; i < myNav.childNodes.length; i++) {

        if (myNav.childNodes[i].className == 'tab-on') {

            if (myNav.childNodes[i].className == 'tab-inactive') {

                myNav.childNodes[i].className = 'tab-inactive';

            }

            else {

                myNav.childNodes[i].className = 'tab';

            }

        }

    }

    myParent.parentNode.className = 'tab-on';

    initFunctions();

    ongletActive = document.getElementById('ongletActive');

    if (ongletActive) {

        ongletActive.value = myLayer[myLayer.length - 1];

    }

}


/*Show layer according to tabs*/

function showHideLayerMulti(myLayer, myParent, nbOnglet) {

    for (t = 6; t <= nbOnglet; t++) {

        layers = 'ongletLayer' + t;

        document.getElementById(layers).style.display = 'none';

    }

    document.getElementById(myLayer).style.display = 'block';


    var myNav = document.getElementById('form-nav-multi');

    for (i = 0; i < myNav.childNodes.length; i++) {

        if (myNav.childNodes[i].className == 'tab-on') {

            if (myNav.childNodes[i].className == 'tab-inactive') {

                myNav.childNodes[i].className = 'tab-inactive';

            }

            else {

                myNav.childNodes[i].className = 'tab';

            }

        }

    }

    myParent.parentNode.className = 'tab-on';

    initFunctions();

    ongletActive = document.getElementById('ongletActive');

    if (ongletActive) {

        ongletActive.value = myLayer[myLayer.length - 1];

    }

}


function showDiv(myDiv) {

    document.getElementById(myDiv).style.display = 'block';

    initFunctions();


}

function hideDiv(myDiv) {

    document.getElementById(myDiv).style.display = 'none';

    initFunctions();


}


/*onclick, show element1 and hide element2 */

function showHideElement(element1, element2) {

    document.getElementById(element1).style.display = 'block';

    document.getElementById(element2).style.display = 'none';

    initFunctions();

}


/*display panel according to option choice*/

function displayOptionChoice(mySelect) {

    var totalOptions = mySelect.options;

    for (i = 0; i < totalOptions.length; i++) {

        var totalLayers = (totalOptions[i].getAttribute('value'));

        document.getElementById(totalLayers).style.display = 'none';

    }
    if (document.getElementById('PF1') != null) {
        document.getElementById('PF1').style.display = 'none';
    }
    if (document.getElementById('PM1') != null) {
        document.getElementById('PM1').style.display = 'none';
    }
    if (document.getElementById('PU1') != null) {
        document.getElementById('PU1').style.display = 'none';
    }

    document.getElementById(mySelect.options[mySelect.selectedIndex].value).style.display = 'block';
    if (document.getElementById(mySelect.options[mySelect.selectedIndex].value + '1') != null) {
        document.getElementById(mySelect.options[mySelect.selectedIndex].value + '1').style.display = 'block';
    }
    initFunctions();

}

function displayOptionByClass(mySelect) {
    var totalOptions = mySelect.options;
    for (i = 0; i < totalOptions.length; i++) {
        var totalLayers = (totalOptions[i].getAttribute('value'));
        J('.' + totalLayers).hide();
        J('.' + mySelect.options[mySelect.selectedIndex].value).show();
    }
    initFunctions();

}


/*display panel according to 1 option choice*/

function displayOneOptionPanel(mySelect, myIndex, myDiv) {

    if (mySelect.selectedIndex == myIndex) {

        document.getElementById(myDiv).style.display = 'block';

    }

    else {

        document.getElementById(myDiv).style.display = 'none';

    }

    initFunctions();

}


/*hide panel according to 1 option choice*/

function hideOneOptionPanel(mySelect, myIndex, myDiv) {

    if (mySelect.selectedIndex == myIndex) {

        document.getElementById(myDiv).style.display = 'none';

    }

    else {

        document.getElementById(myDiv).style.display = 'block';

    }

    initFunctions();

}


/*If checkbox is checked, displays 1 Div*/

function isCheckedShowDiv(myInput, myDiv) {

    if (myInput.checked == true) {

        document.getElementById(myDiv).style.display = 'block';

    }

    if (myInput.checked == false) {

        document.getElementById(myDiv).style.display = 'none';

    }

    initFunctions();

}


function isCheckedHideDiv(myInput, myDiv) {

    if (myInput.checked == true) {

        document.getElementById(myDiv).style.display = 'none';

    }

    if (myInput.checked == false) {

        document.getElementById(myDiv).style.display = 'block';

    }

    initFunctions();

}


/*If checkbox is checked, displays 1 Div and hide 1 div */

function isChecked2(myInput, myDiv1, myDiv2) {

    if (myInput.checked == true) {

        document.getElementById(myDiv1).style.display = 'block';

        document.getElementById(myDiv2).style.display = 'none';

    }

    if (myInput.checked == false) {

        document.getElementById(myDiv1).style.display = 'none';

        document.getElementById(myDiv2).style.display = 'block';

    }

    initFunctions();

}


/*If checkbox is checked, displays table line*/

function isCheckedShowLine(myInput, myLine) {

    if (myInput.checked == true) {

        document.getElementById(myLine).style.display = '';

    }

    if (myInput.checked == false) {

        document.getElementById(myLine).style.display = 'none';

    }

    initFunctions();

}


function activateSelect(mySelect) {

    document.getElementById(mySelect).disabled = false;

}

function disactivateSelect(mySelect) {

    document.getElementById(mySelect).disabled = true;

}


function disableInput(myCheckbox, myInput) {

    if (myCheckbox.checked == true) {

        document.getElementById(myInput).disabled = true;

    }

    if (myCheckbox.checked == false) {

        document.getElementById(myInput).disabled = false;

    }

}


/*Clear input default value onclick */

function clearOnFocus(e, o) {

    if (o.firstTime) {
        return
    }

    o.firstTime = true

    o.value = "";

}


/*popup*/

var newWin = null;

function popUp(strURL, strScrollbars) {

    /*if (newWin !== null) {

     if(!newWin.closed) {

     newWin.close();

     }

     }*/

    var strOptions = "";

    strOptions = "toolbar=no,menubar=no,scrollbars=" + strScrollbars + ",resizable,location=no";

    newWin = window.open(strURL, 'newWin', strOptions);

    newWin.focus();

}


function popUpOpen(URL, name, specs) {

    newWin = window.open(URL, name, specs);

    newWin.focus();

}


/*Ajuste taille popup si calendrier dépasse*/

function resetPopupHeight(myCalendarPositionTop) {


    var calendarHeight = document.getElementById('calendar').offsetHeight;

    var myPageHeight = document.body.offsetHeight;

    var myPageWidth = document.getElementById('container').offsetWidth;

    var newHeight = myCalendarPositionTop + calendarHeight;


    if (document.getElementById('container')) {

        var pageHeight = document.getElementById('container').offsetHeight;

        var bodyWidth = document.body.offsetWidth;

        if (newHeight > pageHeight) {

            // commenter comme correctif du mantis 21012: Dans l'onglet calendrier lorsqu'on appuie sur l'icone calendrier la page du navigateur rétrécit
            //pf IE
            //window.resizeTo(myPageWidth+50,newHeight+70+myCalendarPositionTop);

            document.body.style.overflow = 'visible';

        }

        else {

            document.body.style.position = 'relative';

            document.body.style.overflow = 'hidden';

        }


    }

}


function popupResize(containerId) {


    if (containerId == null) {

        containerId = "container";

    }

    var pageHeight = document.getElementById(containerId).offsetHeight;

    var pageWidth = document.getElementById(containerId).offsetWidth;


    var name = navigator.appName;

    var version = navigator.appVersion;


    var autoLeft = (screen.availWidth - pageWidth) / 2;

    var autoTop = (screen.availHeight - pageHeight) / 2;


    /*if popup content higher than screen height*/

    if (pageHeight > screen.availHeight) {

        /*if IE*/

        if (name == 'Microsoft Internet Explorer') {

            id = version.indexOf('MSIE');

            version = version.substring(id + 5, id + 9);

            /*if IE 7*/

            if (version == '7.0;') {

                //alert(version);

                window.resizeTo(pageWidth + 50, screen.availHeight - 100);

                window.moveTo(autoLeft - 20, 50);

            }

            /*if other IE version*/

            else {

                window.resizeTo(pageWidth + 50, screen.availHeight - 100);

                window.moveTo(autoLeft, 50);

            }

        }

        /*if other navitor*/

        else {

            window.resizeTo(pageWidth + 50, screen.availHeight - 100);

            window.moveTo(autoLeft, 50);

        }

    }

    /*else if content smaller than screen height */

    else {

        /*if IE*/

        if (name == 'Microsoft Internet Explorer') {

            id = version.indexOf('MSIE');

            version = version.substring(id + 5, id + 9);

            /*if IE 7*/

            if (version == '7.0;') {

                //alert(version);

                window.resizeTo(pageWidth + 50, pageHeight + 100);

                window.moveTo(autoLeft - 20, autoTop);

            }

            /*if other IE version*/

            else {

                window.resizeTo(pageWidth + 50, pageHeight + 80);

                window.moveTo(autoLeft, autoTop);

            }

        }

        /*if other navitor*/

        else {

            window.resizeTo(pageWidth + 50, pageHeight + 100);

            window.moveTo(autoLeft, autoTop);

        }


    }


}


/*Auto position Message alertes*/

function autoPositionPopup() {


    var name = navigator.appName;

    var version = navigator.appVersion;


    var autoLeft = (screen.availWidth) / 2;

    var autoTop = (screen.availHeight) / 2;


    window.moveTo(autoLeft - 200, autoTop - 100);

}


/*popup with set height and width*/

var newWinSetSize = null;

function popUpSetSize(strURL, strWidth, strHeight, strScrollbars) {


    if (newWinSetSize !== null) {

        if (!newWinSetSize.closed) {

            newWinSetSize.close();

        }

    }


    var strOptions = "";

    strOptions = "toolbar=no,menubar=no,scrollbars=" + strScrollbars + ",resizable,location=no,height=" + strHeight + ",width=" + strWidth;

    newWinSetSize = window.open(strURL, 'newWin', strOptions);

    newWinSetSize.focus();

}


//Check/Uncheck All checkboxes


function CheckAll(chk) {

    for (i = 0; i < chk.length; i++)

        chk[i].checked = true;

}


function UnCheckAll(chk) {

    for (i = 0; i < chk.length; i++)

        chk[i].checked = false;

}


function isCheckedCheckAll(myInput, myInputList) {


    if (myInput.checked == true) {

        CheckAll(myInputList);

    }

    if (myInput.checked == false) {

        UnCheckAll(myInputList);

    }

}


/*show or hide collection of input */

function showHideCollection(myInput, collection1, collection2, totalCollection) {

    var myCollectionToShow = collection1 + "_";

    var myCollectionToHide = collection2 + "_";


    if (myInput.checked == true) {

        //show col 2 and hide col 1

        for (i = 1; i < (totalCollection + 1); i++) {

            document.getElementById(myCollectionToShow + [i]).style.display = 'block';

            document.getElementById(myCollectionToHide + [i]).style.display = 'none';

            //alert(myCollectionToShow+[i]);

        }

    }

    if (myInput.checked == false) {

        //show col 1 and hide col 2

        for (i = 1; i < (totalCollection + 1); i++) {

            document.getElementById(myCollectionToHide + [i]).style.display = 'block';

            document.getElementById(myCollectionToShow + [i]).style.display = 'none';

        }

    }

}


/*Check Uncheck collection of radio buttons */

function isCheckedCheckAllRadio(myInput, myVersion, version, totalCollection) {

    var myCollection = myVersion + "_";

    for (i = 1; i < (totalCollection + 1); i++) {

        var myTotalCollectionToCheckId = myCollection + [i] + '_' + version;

        document.getElementById(myTotalCollectionToCheckId).checked = true;

    }

}


/*Onclick swap image*/

var status = false;

function swapImage(myImg, img1, img2) {

    var imgRoot = 'themes/images/';

    if (status == false) {

        myImg.src = imgRoot + img2;

        status = true;

    }

    else if (status == true) {

        myImg.src = imgRoot + img1;

        status = false;

    }

}


/////////////////////////////////////////////Floating div////////////////////////////////////////////////


/*Transparent layer that makes the the popup parent window inactive*/

function overlay(idPopup, idOverlay) {


    var el = document.getElementById(idOverlay);

    el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";


    var myPageHeight = document.body.offsetHeight;

    var myPageWidth = document.body.offsetWidth;

    document.getElementById(idOverlay).style.width = myPageWidth + 'px';

    document.getElementById(idOverlay).style.height = myPageHeight + 'px';


    initFloatingIframe(idPopup);


    if (idPopup != undefined) {

        autoPositionLayer(idPopup);

    }


    if (idOverlay == undefined) {

        idOverlay = "overlay";

    }

}


function autoPositionLayer(id) {



    /*Attention, il faut appeler la fonction 'overlay' avantauto 'PositionLayer' */

    var myLayer = document.getElementById(id);


    /*Positionnement horizontal du Div container par rapport a la taille de la fenetre*/

    var windowWidth = 0;

    if (typeof(window.innerWidth) == 'number') {

        windowWidth = window.innerWidth;

    }

    else {

        if (document.documentElement && document.documentElement.clientWidth) {

            windowWidth = document.documentElement.clientWidth;

        }

        else {

            if (document.body && document.body.clientWidth) {

                windowWidth = document.body.clientWidth;

            }

        }

    }

    var newWidth = Math.round(windowWidth / 2);

    var myContainerWidth = document.getElementById(id).offsetWidth;

    var myContainerWidthPosition = myContainerWidth / 2;

    myLayer.style.left = newWidth - myContainerWidthPosition + 'px';


    /*Positionnement vertical du Div container par rapport a la taille de la fenetre*/

    var windowHeight = 0;

    if (typeof(window.innerHeight) == 'number') {

        windowHeight = window.innerHeight;

    }

    else {

        if (document.documentElement && document.documentElement.clientHeight) {

            windowHeight = document.documentElement.clientHeight;


        }

        else {

            if (document.body && document.body.clientHeight) {

                windowHeight = document.body.clientHeight;

            }

        }

    }

    var newHeight = Math.round(windowHeight / 2);

    var myContainerHeight = document.getElementById(id).offsetHeight;

    var myContainerHeightPosition = myContainerHeight / 2;


    if (version == '6.0;') {

        var windowScroll = document.documentElement.scrollTop;

        myLayer.style.top = ((windowHeight / 2) + windowScroll - myContainerHeightPosition) + 'px';


    }

    else {

        myLayer.style.top = newHeight - myContainerHeightPosition + 'px';

    }


}


/*Reinitialization of Layer position on Window resize event*/

function initLayer() {

    if (document.getElementById('overlay') != "") {

        autoPositionLayer();

        initFloatingIframe();

    }

}


/*Set Floating Iframe size*/

function initFloatingIframe() {

    if (document.getElementById('floatingIframe')) {

        var myIframe = document.getElementById('floatingIframe');

        var myContainerHeight = document.getElementById('container').offsetHeight;

        var myContainerWidth = document.getElementById('container').offsetWidth;

        myIframe.style.height = myContainerHeight + 'px';

        myIframe.style.width = myContainerWidth + 'px';

    }

}


/////////////////////////////////////////////End Floating div////////////////////////////////////////////////


/*show or hide all panels*/

function showAll(myPanel, NbPanels) {

    for (i = 0; i < NbPanels; i++) {

        var panel = myPanel + '-' + i;

        //alert(panel);

        if (document.getElementById(panel).style.display == 'none') {

            document.getElementById(panel).style.display = 'block';

        }

        var mypanelParent = document.getElementById(panel).parentNode;

        for (j = 0; j < mypanelParent.childNodes.length; j++) {

            if (mypanelParent.childNodes[j].className == 'title') {

                var titlechildnodes = mypanelParent.childNodes[j].childNodes;

                for (p = 0; p < titlechildnodes.length; p++) {

                    if (titlechildnodes[p].tagName == 'IMG')

                        titlechildnodes[p].src = 'themes/images/picto-close-panel.gif';

                }

            }

        }

    }

}

function hideAll(myPanel, NbPanels) {

    for (i = 0; i < NbPanels; i++) {

        var panel = myPanel + '-' + i;

        //alert(panel);

        if (document.getElementById(panel).style.display == 'block') {

            document.getElementById(panel).style.display = 'none';

        }

        var mypanelParent = document.getElementById(panel).parentNode;

        for (j = 0; j < mypanelParent.childNodes.length; j++) {

            if (mypanelParent.childNodes[j].className == 'title') {

                var titlechildnodes = mypanelParent.childNodes[j].childNodes;

                for (p = 0; p < titlechildnodes.length; p++) {

                    if (titlechildnodes[p].tagName == 'IMG')

                        titlechildnodes[p].src = 'themes/images/picto-open-panel.gif';

                }

            }

        }

    }

}


//////////////////////////////////////Infos-bulles//////////////////////////////////


var idBulleT;

var chrono = null;

var delai = "500";


function afficheBulle(idBulle, parent) {

    var bulle = document.getElementById(idBulle);

    var offset;

    var exp = new RegExp("^td_", "gi");


    if (chrono != null) {

        clearTimeout(chrono);

        cacheBulleT();

    }


    bulle.style.display = "block";


    var bulleLeft = parent.offsetLeft + 10;

    var bulleTop = parent.offsetTop - bulle.offsetHeight;


    var windowWidth = 0;

    if (typeof(window.innerWidth) == 'number') {

        windowWidth = window.innerWidth;

    }

    else {

        if (document.documentElement && document.documentElement.clientWidth) {

            windowWidth = document.documentElement.clientWidth;

        }

        else {

            if (document.body && document.body.clientWidth) {

                windowWidth = document.body.clientWidth;

            }

        }

    }


    bulle.style.left = bulleLeft + 'px';

    bulle.style.top = bulleTop + 'px';

    if ((bulleLeft + bulle.offsetWidth + 20) > windowWidth) {

        bulle.style.left = (bulleLeft - bulle.offsetWidth) + 'px';


    }


    //check Internet Exporer VErsion


    //create iframe to fix ie6 select z-index bug

    if (version == '6.0;') {


        var myBulleIframe = '<iframe src="blank.html" scrolling="no" frameborder="0" class="info-bulle-Iframe" title="Cadre vide"' + ' id="' + idBulle + '-Iframe"' + '></iframe>';

        var myMessage = bulle.innerHTML;

        bulle.innerHTML = myBulleIframe + myMessage;

        var myBulleHeight = bulle.offsetHeight;

        var myBulleWidth = bulle.offsetWidth;

        myNewID = idBulle + '-Iframe';

        var myNewIframe = document.getElementById(myNewID);


        var parentClass = myNewIframe.parentNode.className;

        var iframeNewClass = parentClass + '-Iframe';

        myNewIframe.className = iframeNewClass;

        myNewIframe.style.height = myBulleHeight;

        myNewIframe.style.width = myBulleWidth;

        //alert(myNewIframe.style.height);

        //alert(myNewIframe.className);

    }


}


function cacheBulleT() {

    document.getElementById(idBulleT).style.display = "none";

    chrono = null;

}


function cacheBulle(idBulle) {

    idBulleT = idBulle;

    chrono = setTimeout("cacheBulleT()", delai);


    //document.getElementById(idBulle).style.display = "none";

}


function mouseOverBulle() {

    clearTimeout(chrono);

    chrono = null;

}


function mouseOutBulle() {

    chrono = setTimeout("cacheBulleT()", delai);

}


//////////////////////////////////////End Infos-bulles//////////////////////////////////


/*disable option in selectbox */

function disableOption(mySelect, myIndex) {

    if (mySelect.selectedIndex == myIndex) {

        mySelect.selectedIndex = 0;

    }

}


/////////////////////////////////////Navigation Liste Entites d'achat//////////////////////////


function openSousCat(myPosition, myPanel) {

    var layer = document.getElementById(myPanel);

    if (layer.style.display == 'none') {

        layer.style.display = 'block';

        myPosition.src = 'themes/images/picto-moins.gif';

    }

    else if (layer.style.display == 'block') {

        layer.style.display = 'none';

        myPosition.src = 'themes/images/picto-plus.gif';

    }

    initFunctions();

}


/////////////////////////////////////End Navigation Liste Entites d'achat//////////////////////////


/*Ouveruture lien externe*/

function open_ext_link() {

    var liens = document.getElementsByTagName('a');

    // On récupère tous les liens (<a>) du document dans une variable (un array), ici liens.

    // Une boucle qui parcourt le tableau (array) liens du début à la fin.

    for (var i = 0; i < liens.length; ++i) {

        // Si les liens ont un nom de class égale à lien-ext, alors on agit.

        if (liens[i].className == 'lien-ext' || liens[i].className == 'lien-ext-left') {

            var actualTitle = liens[i].title;

            liens[i].title = actualTitle + '';

            // Au clique de la souris.

            liens[i].onclick = function () {

                window.open(this.href);

                return false; // On ouvre une nouvelle page ayant pour URL le href du lien cliqué et on inhibe le lien réel.

            };

        }

    }

}


// Au chargement de la page, on appelle la fonction.


//////////////////////////////////////MULTI-LINGUISME////////////////////////////////////////////

/*Display Objet in selected language*/

function showLangueDiv(myObjetlangue, myDiv) {

    var totalLangue = document.getElementById(myObjetlangue);

    for (i = 0; i < totalLangue.childNodes.length; i++) {

        //alert(totalLangue.childNodes[i].nodeType);

        if (totalLangue.childNodes[i].nodeType == 1) {

            var myDivId = totalLangue.childNodes[i].id;

            if (document.getElementById(myDivId)) {

                document.getElementById(myDivId).style.display = 'none';

            }

        }

    }

    document.getElementById(myDiv).style.display = 'block';

}


/*select and highlight flag*/

function selectFlag(myFlag) {

    var myFlagParent = myFlag.parentNode;

    for (i = 0; i < myFlagParent.childNodes.length; i++) {

        if (myFlagParent.childNodes[i].tagName == 'A') {

            myFlagParent.childNodes[i].className = '';

        }


    }

    myFlag.className = 'on';

}

//////////////////////////////////////End MULTI-LINGUISME////////////////////////////////////////////

J(document).ready(function () {
    document.createElement("abbr");
    //J(".hidden").hide();

    //Choix liste detinataires compte ou registre
    J(".tableau-choix-destinataires th.col-compte input[type=checkbox]").click(function () {
        J(this).is(':checked') ? J(this).parents('.tableau-choix-destinataires').find('td.col-compte input[type=checkbox]').attr("checked", 'checked') : J(this).parents('.tableau-choix-destinataires').find('td.col-compte input[type=checkbox]').removeAttr("checked");
    });

    J(".tableau-choix-destinataires th.col-registre input[type=checkbox]").click(function () {
        J(this).is(':checked') ? J(this).parents('.tableau-choix-destinataires').find('td.col-registre input[type=checkbox]').attr("checked", 'checked') : J(this).parents('.tableau-choix-destinataires').find('td.col-registre input[type=checkbox]').removeAttr("checked");
    });

    J(".tableau-choix-destinataires td.check-col input[type=checkbox]").click(function () {
        J(this).is(':checked') ? J(this).parents('tr').find('input[type=checkbox]').attr("checked", 'checked') : J(this).parents('tr').find('input[type=checkbox]').removeAttr("checked");
    });


    // Load Modal dialog on click
    J('.bouton-modal').click(function (e) {
        J(this).next('.modal').modal();
        return false;
    });


    // Affiche lien Partager on click
    J(".bouton-partager").click(function () {
        if (J(this).next('.share-panel').hasClass('visible')) {
            J(this).next('.share-panel').removeClass('visible');
        } else {
            J('.share-panel').removeClass('visible');
            J(this).next('.share-panel').addClass('visible');
        }
    });
    J('html').click(function () {
        J('.share-panel').removeClass('visible');
    });
    J(".bouton-partager").click(function (event) {
        event.stopPropagation();
    });


    maxLength = J(".max-length").attr("maxlength");
    //J(".max-length").after("<div class='remaining'><span id='remainingLengthTempId'>"
    //		  + maxLength + "</span> caract&egrave;res max.</div>");
    J(".max-length").bind("keyup change", function () {
        checkMaxLength(this.id, maxLength);
    })

    //Formattage champ montant
    J(".input-montant").blur(function () {
        J(this).parseNumber({format: "#,###.00", locale: "fr"});
        J(this).formatNumber({format: "#,###.00", locale: "fr"});
    });

    //Formattage input propre
    J('.input-propre').keypress(function (event) {
        if (event.keyCode == 10 || event.keyCode == 13)
            event.preventDefault();
        formatInputPropre();
    });

    formatInputPropre();

    //Check all
    J(".checkall").click(function () {
        J(this).is(':checked') ? J(this).parents("table").find("input[type=checkbox]:not(:disabled)").attr("checked", 'checked') : J(this).parents("table").find("input[type=checkbox]:not(:disabled)").attr("checked", false);
    });

    //Check all pieces reponses
    J(".checkall-pieces-reponses").click(function () {
        J(this).is(':checked') ? J(".tableau-reponse").find("input[type=checkbox]:not(:disabled)").prop("checked", 'checked') : J(".tableau-reponse").find("input[type=checkbox]:not(:disabled)").prop("checked", false);
        J(this).is(':checked') ? J(".checkall-pieces-reponses").prop("checked", 'checked') : J(".checkall-pieces-reponses").prop("checked", false);

    });

    //UnCheck all child checkbox elements
    /*J(".uncheckall").click(function(){
     J(this).is(':checked') ? false : J(this).parents(".line").find("input[type=checkbox]:not(:disabled)").attr("checked",false);
     });*/
    //UnCheck all child checkbox elements
    J(".hideall").click(function () {
        //J(this).is(':checked') ? J(this).parents(".line").find(".panel").removeClass('hidden').addClass('visible') : J(this).parents(".line").find(".panel").removeClass('hidden').addClass('visible')
    });


    //Checkbox is checked  --> show child element, Checkbox is unchecked --> hide and uncheck all child checkboxes
    J(".toggleUncheckall").click(function () {
        if (J(this).is(':checked') == true) {
            J(this).parents(".line:first").find(".panel:first").removeClass('hidden').addClass('visible');
        }
        if (J(this).is(':checked') == false) {
            J(this).parents(".line:first").find(".panel").removeClass('visible').addClass('hidden');
            J(this).parents(".line:first").find("input[type=checkbox]:not(:disabled)").attr("checked", false);
        }
    });
    //Radio is checked  --> hide and uncheck all child checkboxes
    J(".isCheckedHideUncheckall").click(function () {
        if (J(this).is(':checked') == true) {
            J(this).parents(".line:first").find("input[type=checkbox]:not(:disabled)").attr("checked", false);
            J(this).parents(".line:first").find(".panel").removeClass('visible').addClass('hidden');
        }
    });

    //Check all liste group
    J(".checkall-list").click(function () {
        J(this).is(':checked') ? J(this).parents(".check-group").find("input[type=checkbox]:not(:disabled)").attr("checked", 'checked') : J(this).parents(".check-group").find("input[type=checkbox]:not(:disabled)").attr("checked", false);
    });

    //CollapseAll/ToggleAll table detail row
    J(".toggle-details-line .collapse-all").click(function () {
        J(this).parents('.table-bloc-detail').find('.panel-detail').show();
        J(this).parents('.table-bloc-detail').find('.title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
        initFunctions();
        return false;
    });
    J(".toggle-details-line .toggle-all").click(function () {
        J(this).parents('.table-bloc-detail').find('.panel-detail').hide();
        J(this).parents('.table-bloc-detail').find('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
        initFunctions();
        return false;
    });

    //CollapseAll/ToggleAll table AC row
    J(".toggle-ac .collapse-all").click(function () {
        J(this).parents('.table-bloc-detail').find('.panel-detail').show();
        J(this).parents('.table-bloc-detail').find('.line-racine .title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
        initFunctions();
        return false;
    });
    J(".toggle-ac .toggle-all").click(function () {
        J(this).parents('.table-bloc-detail').find('.panel-detail').hide();
        J(this).parents('.table-bloc-detail').find('.panel-detail').nextUntil('.line-racine', 'tr').hide();
        J(this).parents('.table-bloc-detail').find('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
        initFunctions();
        return false;
    });


});

//Formattage champ objet du marche
function formatInputPropre() {

    var listInput = J('.input-propre');

    for (var i = 0; i < listInput.length; i++) {
        var myIds = listInput[i];
        var listInputId = J('#' + listInput[i].id).val();
        var inputVal2 = listInputId.replace(/\t+/g, '');
        var inputPropre = inputVal2.replace(/\n+/g, ' - ');
        J('#' + listInput[i].id).val(inputPropre);
    }
    return false;

}

//Check maxlength
function checkMaxLength(textareaID, maxLength) {
    currentLengthInTextarea = J("#" + textareaID).val().length;
    //J(remainingLengthTempId).text(parseInt(maxLength) - parseInt(currentLengthInTextarea));
    if (currentLengthInTextarea > (maxLength)) {
        // Trim the field current length over the maxlength.
        J(".max-length").val(J(".max-length").val().slice(0, maxLength));
        //J(remainingLengthTempId).text(0);

    }
}

function togglePanelClass(myPosition, myPanel) {
    J('.' + myPanel).toggle();
    J(myPosition).toggleClass('title-toggle-open').toggleClass('title-toggle');
}
function togglePanelDetail(myPosition, myPanel) {
    J('.' + myPanel).toggle();
    J(myPosition).toggleClass('title-toggle-open').toggleClass('title-toggle');
    J('.' + myPanel).next('.panel-detail').hide();
    J('.' + myPanel).find('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
}
//Toggle tableau AC 
function togglePanelAc(myPosition, myPanel) {
    J('.' + myPanel).toggle();
    J(myPosition).toggleClass('title-toggle-open').toggleClass('title-toggle');
    J('.' + myPanel).next('.panel-detail').hide();
    J('.' + myPanel).next('tr').hide();
    J('.' + myPanel).find('.title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
}

function showHidePanelClass(myPosition, myPanel) {
    J('.' + myPanel).toggleClass('hidden');
    J(myPosition).toggleClass('title-toggle-open').toggleClass('title-toggle');
}

function togglePanelOpenAll(myPanel) {
    var panel = J('.' + myPanel);
    panel.show();
}
function togglePanelCloseAll(myPanel) {
    var panel = J('.' + myPanel);
    panel.hide();
}

function showPanel(myPanel) {
    J('#' + myPanel).toggleClass('hidden').toggleClass('visible');
}


function toggleMultiPanel() {
    for (var i = 0; i < arguments.length; i++) {
        var allPanels = arguments[i];
        J('#' + allPanels).toggleClass('hidden');
    }

}

function toggleTableColum() {
    for (var i = 0; i < arguments.length; i++) {
        var allPanels = arguments[i];
        J('#' + allPanels).toggle();
    }
}

/*Function toggle element by Class*/
function toggleDisplay(myInput, myDiv) {
    if (myInput.checked == true) {
        J('.' + myDiv).show();
    }
    if (myInput.checked == false) {
        J('.' + myDiv).hide();
    }
    initFloatingIframe();
}


//Champ autocomplete Entité d'achat
J(document).ready(function () {

    /*J( '#entiteSearch' ).each( function(){
     J(this).attr( 'title', J(this).val() )
     .focus( function(){
     if ( J(this).val() == J(this).attr('title') ) {
     J(this).val( '' );
     }
     } ).blur( function(){
     if ( J(this).val() == '' || J(this).val() == ' ' ) {
     J(this).val( J(this).attr('title') );
     }
     } );
     } );*/

    if (J('input#entiteSearch').length) {


        J('input#entiteSearch').result(function (event, data, formatted) {
            J('#result').html(!data ? "No match!" : "Selected: " + formatted);
        }).blur(function () {
        });

        J(function () {
            function format(entite) {
                return "<div class='title'>" + entite.title + "</div>";
            }

            function title(entite) {
                return entite.title
            }

            J("#entiteSearch").autocomplete(completeResults, {
                width: J("#entiteSearch").outerWidth() - 2,
                max: 10,
                scroll: false,
                dataType: "json",
                minChars: 3,
                matchContains: "word",
                idSubmit: "ctl0_CONTENU_PAGE_AdvancedSearch_lancerRecherche",
                selectFirst: false,
                parse: function (data) {
                    return J.map(data, function (row) {
                        return {
                            data: row,
                            value: row.title,
                            result: J("#entiteSearch").val()
                        }
                    });
                },
                formatItem: function (item) {
                    return format(item);
                }
            }).result(function (e, item) {
                J("#entiteSearch").val(title(item));
                //location.href = link(item);
            });
        });


    }

    /*Placeholder support for old navigator*/
    /*if(!Modernizr.input.placeholder){
     J('[placeholder]').focus(function() {
     var input = J(this);
     if (input.val() == input.attr('placeholder')) {
     input.val('');
     input.removeClass('placeholder');
     }
     }).blur(function() {
     var input = J(this);
     if (input.val() == '' || input.val() == input.attr('placeholder')) {
     input.addClass('placeholder');
     input.val(input.attr('placeholder'));
     }
     }).blur();
     J('[placeholder]').parents('form').submit(function() {
     J(this).find('[placeholder]').each(function() {
     var input = J(this);
     if (input.val() == input.attr('placeholder')) {
     input.val('');
     }
     })
     });
     }*/

});

/*
 * Permet de d'ouvrir le pop-ine en lui passant la class du lien, la taille de la popin (css) et la pop-ine
 */

function openModal(myModal, myClass) {
    J("." + myModal).dialog({
        modal: true,
        resizable: false,
        closeText: 'Fermer',
        dialogClass: 'modal-form popup-small2 ' + myClass,
        open: function () {
            /*jQuery('.ui-widget-overlay').bind('click',function(){
             J("."+myModal).dialog('close');
             });*/
            //Dialog freezes window scrollbar - Chrome fix
            window.setTimeout(function () {
                jQuery(document).unbind('mousedown.dialog-overlay').unbind('mouseup.dialog-overlay');
            }, 100);
        }
    });
}


//////////////////////////////////////User et modules bar////////////////////////////////////////////
J(document).ready(function () {
    /*Rollover Modules*/
    var opts =
        // horizontal Dock with images expanding downwards in the vertical axis...
    {
        align: 'middle'
        // set the maximum minor axis (vertical) image dimension to 48px
        , size: '24'
        // add labels..
        , labels: 'tc'
    };
    if (J("#list-modules").length > 0) {
        J("#list-modules").jqDock(opts);
    }

    /*Bloc user infos*/
    J('#triggerUserInfos').click(function (event) {
        event.stopPropagation();
        J('.panel-user-infos').toggle();
    });
    J(document).click(function () {
        J('.panel-user-infos').hide();
    });

});


//----------------------------------Verification format champs on blur----------------------------------//
J(document).ready(function () {
    if (J('.mask-year').length) {
        J(".mask-year").mask("9999");
    }
});


//----------------------------------Treeview with toggle----------------------------------//
//http://techlister.com/jquery/creating-jquery-tree-view-without-plugin/976/	
J(document).ready(function () {
    J('.treeview ul').hide();
    J('.treeview .arbo').hide();
    J('.treeview .folder').each(function () {
        J(this).prepend("<span class='expander'></span><span class='icon'></span>");
    });
    J('.treeview .root').each(function () {
        J(this).prepend("<span class='expander'></span>");
    });
    J('.treeview li').each(function () {
        if (J(this).children('ul').length > 0) {
            J(this).addClass('parent');
        }
    });
    J('.treeview li > div.root').click(function () {
        J(this).parent().toggleClass('active');
        J(this).next('.arbo').show().children('ul').slideToggle('fast');
    });
    J('.treeview li > span').click(function () {
        J(this).parent().toggleClass('active');
        J(this).parent().children('ul').slideToggle('fast');
    });
    /*J('#all').click( function() {
     J('.treeview li' ).each( function() {
     J( this ).toggleClass( 'active' );
     J( this ).children( 'ul' ).slideToggle( 'fast' );
     });
     return false;
     });*/
});


//----------------------------------Couleur survol lignes tableaux----------------------------------//
J(document).ready(function () {
    J('.table-highlight td.check-col input').click(function () {
        if (J(this).is(':checked') == true) {
            if (J(this).is(':radio')) {
                J(this).parents(".table-highlight").find('tr').removeClass('highlight');
            }
            J(this).parents("tr").addClass('highlight');
        }
        else {
            J(this).parents("tr").removeClass('highlight');
        }
    });

});

//----------------------------------Animation timeline 1ere visite----------------------------------//
J(document).ready(function () {
    J('.timeline-img').click(function () {
        J('.timeline-block').removeClass('active');
        J(this).parents(".timeline-block").addClass('active');
        J('.timeline-block p').slideUp();
        if (J(this).parents(".timeline-block").find('p').is(":hidden")) {
            J(this).parents(".timeline-block").find('p').slideToggle();
        }
    });
    J('.timeline-content').click(function () {
        J('.timeline-block').removeClass('active');
        J(this).parents(".timeline-block").addClass('active');

        J('.timeline-block p').slideUp();
        if (J(this).parents(".timeline-block").find('p').is(":hidden")) {
            J(this).parents(".timeline-block").find('p').slideToggle();
        }
    });
});


//----------------------------------FAQ----------------------------------//
J(document).ready(function () {
    J('.panel-faq .panel').hide();
    J('.panel-faq h3 a').click(function () {
        J('.panel-faq .panel').slideUp();
        J(this).parent().next('.panel:hidden').slideToggle();
        J(this).parent().siblings().find('span').attr('class', 'arrow-right');
        J(this).find('span').toggleClass('arrow-right').toggleClass('arrow-down');
    });

    J('#myTab a').click(function (e) {
        e.preventDefault();
        J(this).tab('show');
    })

});

