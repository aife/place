/**
 * Created by AAB on 11/10/2016.
 */
"use strict";

/**
 * @date 07/09/2016
 * @version v1.1.0
 * @namespace ATX
 * @description
 * This Source Code is the global script for SUB project
 */

var ATXMPE = (function ($) {

    var ATXMPE = {
        settings: {
            modeDev: false
        },
        jquery: {
            component: {
                fileUpload: {


                    settings: {},
                    init: function () {

                        var initDragLeave = function ($this) {
                            $this.on('dragleave', function (e) {
                                // dragleave callback implementation
                                $(this).find('.drag-zone').removeClass('drag-over');
                            });
                        };


                        $('.component-file-upload').each(function () {

                            var $thisComponentFileUpload = $(this);

                            // Initialize the jQuery File Upload widget:
                            $thisComponentFileUpload.fileupload({
                                // Uncomment the following to send cross-domain cookies:
                                //xhrFields: {withCredentials: true},
                                url: 'http://localhost/Design/MPE/server/php/',
                                autoUpload: true,
                                filesContainer: $thisComponentFileUpload.find('> .fileupload-table > .files'),
                                dropZone: $thisComponentFileUpload.find('> .fileupload-buttonbar')
                            }).bind('fileuploaddone', function (e, data) {
                                // Callback for successful upload requests
                            }).bind('fileuploaddragover', function (e) {
                                $(this).find('.drag-zone').addClass('drag-over');
                            });
                            initDragLeave($thisComponentFileUpload);

                            // Enable iframe cross-domain access via redirect option:
                            $thisComponentFileUpload.fileupload(
                                'option',
                                'redirect',
                                window.location.href.replace(
                                    /\/[^\/]*$/,
                                    '/cors/result.html?%s'
                                )
                            );

                            // Load existing files:
                            $thisComponentFileUpload.addClass('fileupload-processing');
                            $.ajax({
                                // Uncomment the following to send cross-domain cookies:
                                //xhrFields: {withCredentials: true},
                                url: $thisComponentFileUpload.fileupload('option', 'url'),
                                dataType: 'json',
                                context: $thisComponentFileUpload[0]
                            }).always(function () {
                                $(this).removeClass('fileupload-processing');
                            }).done(function (result) {
                                $(this).fileupload('option', 'done')
                                    .call(this, $.Event('done'), {result: result});
                            });
                        });
                    }

                }
            }
        }

    };

    ATXMPE.document.ready();

    return ATXMPE;

})(J);