var eventUserTracking = document.createEvent('Event');
document.addEventListener('UserOperationTrackingEvent', trackingUserOperationHandler, false);
document.addEventListener('UserOperationTrackingEventAnnulation', trackingUserOperationHandlerAnnulation, false);

function trackingUserOperationHandler(e)
{
    trackUserOperation(e.elementTracking, e.dataTracking, e.cleDescriptionTracking, e.actionTracking, e.asyncTracking, e.detailFileOriginTracking);
}

function trackingUserOperationHandlerAnnulation(e)
{
    trackUserOperationAnnulation,(e.elementTracking, e.dataTracking, e.cleDescriptionTracking, e.actionTracking, e.asyncTracking, e.detailFileOriginTracking);
}