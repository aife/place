
//----------------------------------Modal Change Div Content in background----------------------------------//
J(document).ready(function(){

    J('.atx-hide-content-on-upload').on("click",function(){

        J('.repondre-consultation .atx-show-modal').modal('hide');
        J('.custom-overlay').css({
            'display' : 'block',
            'background' : 'rgba(0, 0, 0, 0.7)'
        });
        J('.repondre-consultation .atx-is-highlighted').css({
            'position': 'relative',
            'z-index': '1023',
            'background': '#fff'
        }),
        J('body.modal-open').css('overflow','auto');
        J('.repondre-consultation .bloc.atx-highlight-bloc').addClass('atx-is-highlighted');
        J('.repondre-consultation .atx-highlight-bloc .bg-warning').css({
            'background' : '#d9edf7',
            'color' : '#666'
        });
        J('.repondre-consultation .atx-upload-modal').css({
            'z-index' : '999',
            'overflow' : 'hidden !important'
        })
        J('.repondre-consultation .atx-is-highlighted .drag-zone,.repondre-consultation .atx-is-highlighted .atx-btn-circle.cancel, .repondre-consultation .atx-is-highlighted .fileinput-button, .repondre-consultation .atx-is-highlighted .fileinput-button').css('display','none');
        J('.repondre-consultation .atx-upload-content').html('Votre dépôt est en cours. Merci patienter et de ne pas quitter cette page durant le transfert des fichiers. <br> Une fois le transfert des fichiers terminé, vous serez redirigé sur la page de confirmation et recevrez un accusé de réception. <br> En cas d&#39;échec de chargement d une pièce, le dépôt est interrompu. Il faut alors recharger les pièces nécessaires sur la page de dépôt.');
        J('html, body').animate({
            scrollTop: J('.repondre-consultation .atx-is-highlighted').offset().top
        }, 400);
        J('.repondre-consultation .atx-is-uploading-return-btn').css('display','block');
    });
});
//----------------------------------Modal retrun to default button----------------------------------//
J(document).ready(function(){

    J('.atx-btn-return-upload').on("click",function(){
        depotAutomatique = 0;
        pageTransmissionAffichee = 0;
        validationAnticipee = 0;
        J('.custom-overlay').css('display','none');
        J('.repondre-consultation .atx-highlight-bloc .bg-warning').css('background','#fcf8e3');
        J('.repondre-consultation .atx-is-highlighted .bloc .bloc-body .fileupload-body .form-file-upload, .repondre-consultation .atx-is-highlighted .bloc .bloc-body .fileupload-body .row-file_actions, .repondre-consultation .atx-is-highlighted .bloc .fileinput-button').css('display','block');
        J('.repondre-consultation .atx-upload-content').html('Veuillez vous conformer au Règlement de la Consultation pour connaître la liste des pièces à transmettre. <br> Attention les pièces exigeant une signature électronique doivent être signées au préalable. ');
        J('.repondre-consultation .form-group.atx-is-uploading-return-btn').css('display','none');
        dynamicUploadActionDisplaying();
    });
});
