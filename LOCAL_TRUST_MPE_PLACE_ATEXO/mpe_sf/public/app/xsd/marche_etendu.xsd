<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <xs:element name="marches" type="marchesType"/>
    <xs:complexType name="marchesType">
        <xs:sequence>
            <xs:element name="marche" type="marcheType" maxOccurs="unbounded" minOccurs="0"/>
        </xs:sequence>
    </xs:complexType>
    <xs:complexType name="acheteurType">
        <xs:annotation>
            <xs:documentation>
                Champs correspondant à l'acheteur et à l'autorité concédante dans le cadre d'un contrat de concession.
            </xs:documentation>
        </xs:annotation>
        <xs:all>
            <xs:element name="id">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="([0-9]{14,14})"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="nom" type="xs:string"/>
            <xs:element name="idOrganisme" type="xs:integer"/>
            <xs:element name="libelleOrganisme" type="xs:string"/>
            <xs:element name="idEntiteAchat" type="xs:string" minOccurs="0" />
            <xs:element name="libelleEntiteAchat" type="xs:string" minOccurs="0" />
            <xs:element name="accesChorus" type="xs:string" minOccurs="0" />
            <xs:element name="idAgent" type="xs:integer"/>
            <xs:element name="nomAgent" type="xs:string"/>
            <xs:element name="prenomAgent" type="xs:string"/>
            <xs:element name="loginAgent" type="xs:string"/>
            <xs:element name="emailAgent" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:simpleType name="typeContratType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="MARCHE_PUBLIC"/>
            <xs:enumeration value="CONTRAT_DE_CONCESSION"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="consultationType">
        <xs:all>
            <xs:element name="id" type="xs:string"/>
            <xs:element name="numero" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="lotType">
        <xs:all>
            <xs:element name="intituleLot" type="xs:string"/>
            <xs:element name="numeroLot" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="numerosContratType">
        <xs:all>
            <xs:element name="idTechnique" type="xs:string"/>
            <xs:element name="referenceLibre" type="xs:string"/>
            <xs:element name="numeroCourt" type="xs:string"/>
            <xs:element name="numeroLong" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="contratsChapeauxType">
        <xs:all>
            <xs:element name="contratChapeauMultiAttributaires" type="ContratChapeauMultiAttributairesType"/>
            <xs:element name="contratChapeauAcSad" type="ContratChapeauAcSadType"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="contratTransverseType">
        <xs:all>
            <xs:element name="entiteEligible" type="entiteEligibleType"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="entiteEligibleType">
        <xs:all>
            <xs:element name="id" type="xs:string"/>
            <xs:element name="acronyme" type="xs:string"/>
            <xs:element name="denomination" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="ContratChapeauAcSadType">
        <xs:all>
            <xs:element name="idTechniqueContratChapeauAcSad" type="xs:string"/>
            <xs:element name="idContratChapeauAcSad" type="xs:string"/>
            <xs:element name="referenceLibreContratChapeauAcSad" type="xs:string"/>
            <xs:element name="numeroCourtContratChapeauAcSad" type="xs:string"/>
            <xs:element name="numeroLongContratChapeauAcSad" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="ContratChapeauMultiAttributairesType">
        <xs:all>
            <xs:element name="idTechniqueContratChapeauMultiAttributaires" type="xs:string"/>
            <xs:element name="referenceLibreChapeauMultiAttributaires" type="xs:string"/>
            <xs:element name="numeroCourtContratChapeauMultiAttributaires" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="clausesType">
        <xs:all>
            <xs:element name="iri" type="xs:string" maxOccurs="1" minOccurs="0"/>
            <xs:element name="label" type="xs:string" maxOccurs="1" minOccurs="0"/>
            <xs:element name="slug" type="xs:string" maxOccurs="1" minOccurs="0"/>
        </xs:all>
    </xs:complexType>

    <xs:simpleType name="natureType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Marché"/>
            <xs:enumeration value="Marché de partenariat"/>
            <xs:enumeration value="Accord-cadre"/>
            <xs:enumeration value="Marché subséquent"/>
            <!-- Nature spécifique aux contrats de concession -->
            <xs:enumeration value="Concession de travaux"/>
            <xs:enumeration value="Concession de service"/>
            <xs:enumeration value="Concession de service public"/>
            <xs:enumeration value="Délégation de service public"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="procedureType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Procédure adaptée"/>
            <xs:enumeration value="Appel d'offres ouvert"/>
            <xs:enumeration value="Appel d'offres restreint"/>
            <xs:enumeration value="Procédure concurrentielle avec négociation"/>
            <xs:enumeration value="Procédure négociée avec mise en concurrence préalable"/>
            <xs:enumeration value="Marché négocié sans publicité ni mise en concurrence préalable"/>
            <xs:enumeration value="Dialogue compétitif"/>
            <!-- Procédures spécifiques aux contrats de concession -->
            <xs:enumeration value="Procédure négociée ouverte"/>
            <xs:enumeration value="Procédure non négociée ouverte"/>
            <xs:enumeration value="Procédure négociée restreinte"/>
            <xs:enumeration value="Procédure non négociée restreinte"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="typeCodeType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Code postal"/>
            <xs:enumeration value="Code commune"/>
            <xs:enumeration value="Code arrondissement"/>
            <xs:enumeration value="Code canton"/>
            <xs:enumeration value="Code département"/>
            <xs:enumeration value="Code région"/>
            <xs:enumeration value="Code pays"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="lieuExecutionType">
        <xs:all>
            <xs:element name="code" type="xs:string"/>
            <xs:element name="typeCode" type="typeCodeType"/>
            <xs:element name="nom" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:simpleType name="typeIdentifiantType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="SIRET"/>
            <xs:enumeration value="TVA"/>
            <xs:enumeration value="TAHITI"/>
            <xs:enumeration value="RIDET"/>
            <xs:enumeration value="FRWF"/>
            <xs:enumeration value="IREP"/>
            <xs:enumeration value="UE"/>
            <xs:enumeration value="HORS-UE"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:simpleType name="formePrixType">
        <xs:restriction base="xs:string">
            <xs:enumeration value="Ferme"/>
            <xs:enumeration value="Ferme et actualisable"/>
            <xs:enumeration value="Révisable"/>
        </xs:restriction>
    </xs:simpleType>
    <xs:complexType name="titulaireType">
        <xs:all>
            <xs:element name="typeIdentifiant" type="typeIdentifiantType"/>
            <xs:element name="id" type="xs:string"/>
            <xs:element name="denominationSociale" type="xs:string"/>
            <xs:element name="contact" type="contactType"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="contactType">
        <xs:all>
            <xs:element name="id" type="xs:string"/>
            <xs:element name="nom" type="xs:string"/>
            <xs:element name="prenom" type="xs:string"/>
            <xs:element name="email" type="xs:string"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="modificationType">
        <xs:all>
            <xs:element name="objetModification">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:maxLength value="256"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="dateSignatureModification" type="xs:date"/>
            <xs:element name="datePublicationDonneesModification" type="xs:date"/>
            <xs:element name="dureeMois" type="xs:integer" minOccurs="0"/>
            <!-- le champs montant correspond à la valeur globale dans le cadre d'un contrat de concession -->
            <xs:element name="montant" type="xs:decimal" minOccurs="0"/>
            <xs:element name="titulaires" minOccurs="0">
                <xs:annotation>
                    <xs:documentation>
                        Dans le cadre d'un contrat de concession ce champs correspond aux "concessionnaires"
                    </xs:documentation>
                </xs:annotation>
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="titulaire" type="titulaireType" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="tarifType">
        <xs:all>
            <xs:element name="intituleTarif" type="xs:string"/>
            <xs:element name="tarif">
                <xs:simpleType>
                    <xs:restriction base="xs:decimal">
                        <xs:minInclusive value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="donneesAnnuellesType">
        <xs:all>
            <xs:element name="datePublicationDonneesExecution" type="xs:date"/>
            <xs:element name="depensesInvestissement">
                <xs:simpleType>
                    <xs:restriction base="xs:decimal">
                        <xs:minInclusive value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="tarifs">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="tarif" type="tarifType" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="donneesComplementairesType">
        <xs:all>
            <xs:element name="procedurePassation" type="xs:string"/>
            <xs:element name="naturePassation" type="xs:string"/>
            <xs:element name="accordCadreAvecMarcheSubsequent" type="xs:boolean"/>
            <xs:element name="idAccordCadre" minOccurs="0" type="xs:string"/>
            <xs:element name="siretPAAccordCadre" minOccurs="0" type="xs:string"/>
            <xs:element name="codesCPVSec" type="xs:string"/>
            <xs:element name="nbTotalPropositionsRecu" type="xs:integer"/>
            <xs:element name="nbTotalPropositionsDemat" type="xs:integer"/>
            <xs:element name="clauseSociale" type="xs:boolean"/>
            <xs:element name="clauses" type="clausesType"/>
            <xs:element name="clauseEnvironnementale" type="xs:boolean"/>
            <xs:element name="ccagReference" type="xs:string"/>
            <xs:element name="consultation" type="consultationType"/>
            <xs:element name="lot" minOccurs="0" type="lotType"/>
            <xs:element name="numerosContrat" type="numerosContratType"/>
            <xs:element name="contratsChapeaux" type="contratsChapeauxType"/>
        </xs:all>
    </xs:complexType>
    <xs:complexType name="marcheType">
        <xs:all>
            <xs:element name="id">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:minLength value="7"/>
                        <xs:maxLength value="16"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="uuid">
                <xs:annotation>
                    <xs:documentation>
                        uuid du contrat initial
                    </xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="[0-9A-F]{8}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{4}-[0-9A-F]{12}"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="typeContrat" minOccurs="1" type="typeContratType">
                <xs:annotation>
                    <xs:documentation>
                        Les marchés publics et les contrats de concession sont envoyés via le même Web-Service, cet attribut permet de sitinguer un marché public d'un contrat de
                        concession
                    </xs:documentation>
                </xs:annotation>
            </xs:element>
            <xs:element name="acheteur" minOccurs="0" maxOccurs="1" type="acheteurType"/>
            <xs:element name="nature" type="natureType"/>
            <xs:element name="objet">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:maxLength value="256"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="codeCPV">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:pattern value="([0-9]{8,8}(\-[0-9])?)"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="procedure" type="procedureType"/>
            <xs:element name="lieuExecution" type="lieuExecutionType"/>
            <xs:element name="dureeMois">
                <xs:simpleType>
                    <xs:restriction base="xs:integer">
                        <xs:minInclusive value="1"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="dateNotification" type="xs:date"/>
            <!-- Champs additionnels pour les contrats de concession -->
            <xs:element name="dateSignature"  minOccurs="0" type="xs:date"/>
            <xs:element name="dateDebutExecution" minOccurs="0" type="xs:date"/>
            <xs:element name="montantSubventionPublique" minOccurs="0">
                <xs:simpleType>
                    <xs:restriction base="xs:decimal">
                        <xs:minInclusive value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="valeurGlobale" minOccurs="0" type="xs:decimal"/>
            <xs:element name="donneesExecution" minOccurs="0">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="donneesAnnuelles" type="donneesAnnuellesType" minOccurs="0" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <!-- Fin des champs additionnels pour les contrats de concession-->
            <xs:element name="datePublicationDonnees" type="xs:date"/>
            <xs:element name="dateTransmissionDonneesEtalab" type="xs:date"/>
            <xs:element name="montant">
                <xs:annotation>
                    <xs:documentation>
                        ce champs correspond à la valeur globale dans le cadre d'un contrat de concession
                    </xs:documentation>
                </xs:annotation>
                <xs:simpleType>
                    <xs:restriction base="xs:decimal">
                        <xs:minInclusive value="0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:element>
            <xs:element name="formePrix" type="formePrixType"/>
            <xs:element name="titulaires">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="titulaire" type="titulaireType" maxOccurs="unbounded">
                        </xs:element>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="modifications">
                <xs:complexType>
                    <xs:sequence>
                        <xs:element name="modification" type="modificationType" minOccurs="0" maxOccurs="unbounded"/>
                    </xs:sequence>
                </xs:complexType>
            </xs:element>
            <xs:element name="donneesComplementaires" minOccurs="0" maxOccurs="1" type="donneesComplementairesType"/>
            <xs:element name="contratTransverse" minOccurs="0" maxOccurs="1" type="contratTransverseType"/>
        </xs:all>
    </xs:complexType>
</xs:schema>
