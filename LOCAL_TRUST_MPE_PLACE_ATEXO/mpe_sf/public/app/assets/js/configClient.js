function deleteAssociation (url,idAssociation, idAgent) {
    $.ajax(url, {
        method: "DELETE",
            data: {
            "idAssociation": idAssociation,
                "idAgent": idAgent
        },
        success: function (response) {
            $.each(response, function (key, value) {
                if(key == 'url'){
                    window.location.replace(value);
                }
            });
        },
        error: function (err) {
            alert("Problème est survenu lors de la suppression...");
        }
    });
}