J(".switch").each(function() {

    var dataState = J(this).attr('data-state');
    state = false
    if (dataState == "true") {
        state = true
    }
    J(this).bootstrapSwitch('state', state);
})

J(".switch").on('switchChange.bootstrapSwitch', function (e, data) {
    var bootThis = J(this);
    bootThis.bootstrapSwitch('state', !data, true);
    var check = data;
    J.ajax({
        url: J(this).attr('data-href')
    }).done(function (data) {
        bootThis.bootstrapSwitch('state', check, true);
    }).fail(function (data) {
        J('#error').html('<div class="alert alert-danger" role="alert">'+data.responseText+'</div>');
    })
});