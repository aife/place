var sf_lang_switcher = function(lang){
    //window.location.href = location.protocol + '//' + location.host + location.pathname + '?_locale=' + lang;
}
J(document).ready(function () {
    J('.lang-switcher').on('click', function(){
        J.ajax({
            type: "GET",
            url: Routing.generate('switcher-language', {'_locale': J(this).data('lang')}),
            success: function (data) {
                location.reload(true);
            },
            error: function (error) {
            }
        });
    });
});

function language() {
    J('.select_wrap label').html(J('#lang_' + lang).html());

    J('.select_wrap').click(function() {
        J('.select_wrap ul').slideToggle(200);
    });
    J('.select_wrap ul li').click(function() {
        var affichage = J(this).html();
        var valeur = J(this).attr('data-value');
        J('.select_wrap label').html(affichage);
        J('.select_wrap input').val(valeur);
    });
}