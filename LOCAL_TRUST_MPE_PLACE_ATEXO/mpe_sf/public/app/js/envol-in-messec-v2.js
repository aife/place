/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */


function showModalEnvol(uuidReference)
{
    var modal = document.getElementById('modal-envol');
    var btnRetour = document.querySelector('#modal-envol .btn-default');
    var btnClose = document.querySelector('#modal-envol button');
    var linkDownload =  document.getElementById('download');
    linkDownload.href = linkDownload.href.replace('uuidReference', uuidReference);
    modal.classList.add("show", "in");
    btnRetour.onclick = function () {
        modal.classList.remove("show", "in"); }
    btnClose.onclick = function () {
        modal.classList.remove("show", "in"); }
    linkDownload.onclick = function () {
        modal.classList.remove("show", "in"); }
}
