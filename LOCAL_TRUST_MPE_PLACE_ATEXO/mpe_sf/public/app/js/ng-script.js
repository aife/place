/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
//! moment.js
//! version : 2.18.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
        typeof define === 'function' && define.amd ? define(factory) :
            global.moment = factory()
}(this, (function () { 'use strict';

    var hookCallback;

    function hooks () {
        return hookCallback.apply(null, arguments);
    }

// This is done to register the method called with moment()
// without creating circular dependencies.
    function setHookCallback (callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
    }

    function isObject(input) {
        // IE8 will treat undefined and null as object if it wasn't for
        // input != null
        return input != null && Object.prototype.toString.call(input) === '[object Object]';
    }

    function isObjectEmpty(obj) {
        var k;
        for (k in obj) {
            // even if its not own property I'd still call it non-empty
            return false;
        }
        return true;
    }

    function isUndefined(input) {
        return input === void 0;
    }

    function isNumber(input) {
        return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
    }

    function isDate(input) {
        return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
        var res = [], i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function createUTC (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty           : false,
            unusedTokens    : [],
            unusedInput     : [],
            overflow        : -2,
            charsLeftOver   : 0,
            nullInput       : false,
            invalidMonth    : null,
            invalidFormat   : false,
            userInvalidated : false,
            iso             : false,
            parsedDateParts : [],
            meridiem        : null,
            rfc2822         : false,
            weekdayMismatch : false
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function (fun) {
            var t = Object(this);
            var len = t.length >>> 0;

            for (var i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    var some$1 = some;

    function isValid(m) {
        if (m._isValid == null) {
            var flags = getParsingFlags(m);
            var parsedParts = some$1.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            var isNowValid = !isNaN(m._d.getTime()) &&
                flags.overflow < 0 &&
                !flags.empty &&
                !flags.invalidMonth &&
                !flags.invalidWeekday &&
                !flags.nullInput &&
                !flags.invalidFormat &&
                !flags.userInvalidated &&
                (!flags.meridiem || (flags.meridiem && parsedParts));

            if (m._strict) {
                isNowValid = isNowValid &&
                    flags.charsLeftOver === 0 &&
                    flags.unusedTokens.length === 0 &&
                    flags.bigHour === undefined;
            }

            if (Object.isFrozen == null || !Object.isFrozen(m)) {
                m._isValid = isNowValid;
            }
            else {
                return isNowValid;
            }
        }
        return m._isValid;
    }

    function createInvalid (flags) {
        var m = createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        }
        else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

// Plugins that add properties should also add the key here (null value),
// so we can properly clone ourselves.
    var momentProperties = hooks.momentProperties = [];

    function copyConfig(to, from) {
        var i, prop, val;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentProperties.length > 0) {
            for (i = 0; i < momentProperties.length; i++) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    var updateInProgress = false;

// Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        if (!this.isValid()) {
            this._d = new Date(NaN);
        }
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment (obj) {
        return obj instanceof Moment || (obj != null && obj._isAMomentObject != null);
    }

    function absFloor (number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

// compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if ((dontConvert && array1[i] !== array2[i]) ||
                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function warn(msg) {
        if (hooks.suppressDeprecationWarnings === false &&
            (typeof console !==  'undefined') && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                var args = [];
                var arg;
                for (var i = 0; i < arguments.length; i++) {
                    arg = '';
                    if (typeof arguments[i] === 'object') {
                        arg += '\n[' + i + '] ';
                        for (var key in arguments[0]) {
                            arg += key + ': ' + arguments[0][key] + ', ';
                        }
                        arg = arg.slice(0, -2); // Remove trailing comma and space
                    } else {
                        arg = arguments[i];
                    }
                    args.push(arg);
                }
                warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + (new Error()).stack);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    hooks.suppressDeprecationWarnings = false;
    hooks.deprecationHandler = null;

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    function set (config) {
        var prop, i;
        for (i in config) {
            prop = config[i];
            if (isFunction(prop)) {
                this[i] = prop;
            } else {
                this['_' + i] = prop;
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
        // TODO: Remove "ordinalParse" fallback in next major release.
        this._dayOfMonthOrdinalParseLenient = new RegExp(
            (this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) +
            '|' + (/\d{1,2}/).source);
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig), prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (hasOwnProp(parentConfig, prop) &&
                !hasOwnProp(childConfig, prop) &&
                isObject(parentConfig[prop])) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function (obj) {
            var i, res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var keys$1 = keys;

    var defaultCalendar = {
        sameDay : '[Today at] LT',
        nextDay : '[Tomorrow at] LT',
        nextWeek : 'dddd [at] LT',
        lastDay : '[Yesterday at] LT',
        lastWeek : '[Last] dddd [at] LT',
        sameElse : 'L'
    };

    function calendar (key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
        LTS  : 'h:mm:ss A',
        LT   : 'h:mm A',
        L    : 'MM/DD/YYYY',
        LL   : 'MMMM D, YYYY',
        LLL  : 'MMMM D, YYYY h:mm A',
        LLLL : 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat (key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
            return val.slice(1);
        });

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate () {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

    function ordinal (number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future : 'in %s',
        past   : '%s ago',
        s  : 'a few seconds',
        ss : '%d seconds',
        m  : 'a minute',
        mm : '%d minutes',
        h  : 'an hour',
        hh : '%d hours',
        d  : 'a day',
        dd : '%d days',
        M  : 'a month',
        MM : '%d months',
        y  : 'a year',
        yy : '%d years'
    };

    function relativeTime (number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return (isFunction(output)) ?
            output(number, withoutSuffix, string, isFuture) :
            output.replace(/%d/i, number);
    }

    function pastFuture (diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {};

    function addUnitAlias (unit, shorthand) {
        var lowerCase = unit.toLowerCase();
        aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
        return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {};

    function addUnitPriority(unit, priority) {
        priorities[unit] = priority;
    }

    function getPrioritizedUnits(unitsObj) {
        var units = [];
        for (var u in unitsObj) {
            units.push({unit: u, priority: priorities[u]});
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    function makeGetSet (unit, keepTime) {
        return function (value) {
            if (value != null) {
                set$1(this, unit, value);
                hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get(this, unit);
            }
        };
    }

    function get (mom, unit) {
        return mom.isValid() ?
            mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
    }

    function set$1 (mom, unit, value) {
        if (mom.isValid()) {
            mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
        }
    }

// MOMENTS

    function stringGet (units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }


    function stringSet (units, value) {
        if (typeof units === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units);
            for (var i = 0; i < prioritized.length; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (sign ? (forceSign ? '+' : '') : '-') +
            Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

// token:    'M'
// padded:   ['MM', 2]
// ordinal:  'Mo'
// callback: function () { this.month() + 1 }
    function addFormatToken (token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function () {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(func.apply(this, arguments), token);
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens), i, length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '', i;
            for (i = 0; i < length; i++) {
                output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

// format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var match1         = /\d/;            //       0 - 9
    var match2         = /\d\d/;          //      00 - 99
    var match3         = /\d{3}/;         //     000 - 999
    var match4         = /\d{4}/;         //    0000 - 9999
    var match6         = /[+-]?\d{6}/;    // -999999 - 999999
    var match1to2      = /\d\d?/;         //       0 - 99
    var match3to4      = /\d\d\d\d?/;     //     999 - 9999
    var match5to6      = /\d\d\d\d\d\d?/; //   99999 - 999999
    var match1to3      = /\d{1,3}/;       //       0 - 999
    var match1to4      = /\d{1,4}/;       //       0 - 9999
    var match1to6      = /[+-]?\d{1,6}/;  // -999999 - 999999

    var matchUnsigned  = /\d+/;           //       0 - inf
    var matchSigned    = /[+-]?\d+/;      //    -inf - inf

    var matchOffset    = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
    var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

// any word (or two) characters or numbers including two/three word month in arabic.
// includes scottish gaelic two word and hyphenated months
    var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;


    var regexes = {};

    function addRegexToken (token, regex, strictRegex) {
        regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
            return (isStrict && strictRegex) ? strictRegex : regex;
        };
    }

    function getParseRegexForToken (token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

// Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        }));
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken (token, callback) {
        var i, func = callback;
        if (typeof token === 'string') {
            token = [token];
        }
        if (isNumber(callback)) {
            func = function (input, array) {
                array[callback] = toInt(input);
            };
        }
        for (i = 0; i < token.length; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken (token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;
    var WEEK = 7;
    var WEEKDAY = 8;

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function (o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    var indexOf$1 = indexOf;

    function daysInMonth(year, month) {
        return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
    }

// FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

// ALIASES

    addUnitAlias('month', 'M');

// PRIORITY

    addUnitPriority('month', 8);

// PARSING

    addRegexToken('M',    match1to2);
    addRegexToken('MM',   match1to2, match2);
    addRegexToken('MMM',  function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

// LOCALES

    var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths (m, format) {
        if (!m) {
            return isArray(this._months) ? this._months :
                this._months['standalone'];
        }
        return isArray(this._months) ? this._months[m.month()] :
            this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort (m, format) {
        if (!m) {
            return isArray(this._monthsShort) ? this._monthsShort :
                this._monthsShort['standalone'];
        }
        return isArray(this._monthsShort) ? this._monthsShort[m.month()] :
            this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
    }

    function handleStrictParse(monthName, format, strict) {
        var i, ii, mom, llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf$1.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf$1.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf$1.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf$1.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse (monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
                regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

// MOMENTS

    function setMonth (mom, value) {
        var dayOfMonth;

        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (!isNumber(value)) {
                    return mom;
                }
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth (value) {
        if (value != null) {
            setMonth(this, value);
            hooks.updateOffset(this, true);
            return this;
        } else {
            return get(this, 'Month');
        }
    }

    function getDaysInMonth () {
        return daysInMonth(this.year(), this.month());
    }

    var defaultMonthsShortRegex = matchWord;
    function monthsShortRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict ?
                this._monthsShortStrictRegex : this._monthsShortRegex;
        }
    }

    var defaultMonthsRegex = matchWord;
    function monthsRegex (isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict ?
                this._monthsStrictRegex : this._monthsRegex;
        }
    }

    function computeMonthsParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            shortPieces.push(this.monthsShort(mom, ''));
            longPieces.push(this.months(mom, ''));
            mixedPieces.push(this.months(mom, ''));
            mixedPieces.push(this.monthsShort(mom, ''));
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 12; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
        }
        for (i = 0; i < 24; i++) {
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    }

// FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? '' + y : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY',   4],       0, 'year');
    addFormatToken(0, ['YYYYY',  5],       0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

// ALIASES

    addUnitAlias('year', 'y');

// PRIORITIES

    addUnitPriority('year', 1);

// PARSING

    addRegexToken('Y',      matchSigned);
    addRegexToken('YY',     match1to2, match2);
    addRegexToken('YYYY',   match1to4, match4);
    addRegexToken('YYYYY',  match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

// HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
    }

// HOOKS

    hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

// MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear () {
        return isLeapYear(this.year());
    }

    function createDate (y, m, d, h, M, s, ms) {
        // can't just apply() to create a date:
        // https://stackoverflow.com/q/181348
        var date = new Date(y, m, d, h, M, s, ms);

        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
            date.setFullYear(y);
        }
        return date;
    }

    function createUTCDate (y) {
        var date = new Date(Date.UTC.apply(null, arguments));

        // the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
            date.setUTCFullYear(y);
        }
        return date;
    }

// start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
            fwd = 7 + dow - doy,
            // first-week day local weekday -- which local weekday is fwd
            fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

// https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear, resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek, resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

// FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

// ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

// PRIORITIES

    addUnitPriority('week', 5);
    addUnitPriority('isoWeek', 5);

// PARSING

    addRegexToken('w',  match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W',  match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
        week[token.substr(0, 1)] = toInt(input);
    });

// HELPERS

// LOCALES

    function localeWeek (mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow : 0, // Sunday is the first day of the week.
        doy : 6  // The week that contains Jan 1st is the first week of the year.
    };

    function localeFirstDayOfWeek () {
        return this._week.dow;
    }

    function localeFirstDayOfYear () {
        return this._week.doy;
    }

// MOMENTS

    function getSetWeek (input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek (input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

// FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

// ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

// PRIORITY
    addUnitPriority('day', 11);
    addUnitPriority('weekday', 11);
    addUnitPriority('isoWeekday', 11);

// PARSING

    addRegexToken('d',    match1to2);
    addRegexToken('e',    match1to2);
    addRegexToken('E',    match1to2);
    addRegexToken('dd',   function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd',   function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd',   function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

// HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

// LOCALES

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays (m, format) {
        if (!m) {
            return isArray(this._weekdays) ? this._weekdays :
                this._weekdays['standalone'];
        }
        return isArray(this._weekdays) ? this._weekdays[m.day()] :
            this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort (m) {
        return (m) ? this._weekdaysShort[m.day()] : this._weekdaysShort;
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin (m) {
        return (m) ? this._weekdaysMin[m.day()] : this._weekdaysMin;
    }

    function handleStrictParse$1(weekdayName, format, strict) {
        var i, ii, mom, llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf$1.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf$1.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf$1.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf$1.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf$1.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf$1.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf$1.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse (weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return handleStrictParse$1.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
                this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
                this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
            }
            if (!this._weekdaysParse[i]) {
                regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

// MOMENTS

    function getSetDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek (input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    var defaultWeekdaysRegex = matchWord;
    function weekdaysRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict ?
                this._weekdaysStrictRegex : this._weekdaysRegex;
        }
    }

    var defaultWeekdaysShortRegex = matchWord;
    function weekdaysShortRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict ?
                this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
        }
    }

    var defaultWeekdaysMinRegex = matchWord;
    function weekdaysMinRegex (isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict ?
                this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
        }
    }


    function computeWeekdaysParse () {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [], shortPieces = [], longPieces = [], mixedPieces = [],
            i, mom, minp, shortp, longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, 1]).day(i);
            minp = this.weekdaysMin(mom, '');
            shortp = this.weekdaysShort(mom, '');
            longp = this.weekdays(mom, '');
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 7; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
    }

// FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2) +
            zeroFill(this.seconds(), 2);
    });

    function meridiem (token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
        });
    }

    meridiem('a', true);
    meridiem('A', false);

// ALIASES

    addUnitAlias('hour', 'h');

// PRIORITY
    addUnitPriority('hour', 13);

// PARSING

    function matchMeridiem (isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a',  matchMeridiem);
    addRegexToken('A',  matchMeridiem);
    addRegexToken('H',  match1to2);
    addRegexToken('h',  match1to2);
    addRegexToken('k',  match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);
    addRegexToken('kk', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['k', 'kk'], function (input, array, config) {
        var kInput = toInt(input);
        array[HOUR] = kInput === 24 ? 0 : kInput;
    });
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

// LOCALES

    function localeIsPM (input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return ((input + '').toLowerCase().charAt(0) === 'p');
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem (hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }


// MOMENTS

// Setting the hour should keep the time, because the user explicitly
// specified which hour he wants. So trying to maintain the same hour (in
// a new timezone) makes sense. Adding/subtracting hours does not follow
// this rule.
    var getSetHour = makeGetSet('Hours', true);

// months
// week
// weekdays
// meridiem
    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse
    };

// internal storage for locale config files
    var locales = {};
    var localeFamilies = {};
    var globalLocale;

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

// pick the locale from the array
// try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
// substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0, j, next, locale, split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return null;
    }

    function loadLocale(name) {
        var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && (typeof module !== 'undefined') &&
            module && module.exports) {
            try {
                oldLocale = globalLocale._abbr;
                require('./locale/' + name);
                // because defineLocale currently also sets the global locale, we
                // want to undo that for lazy loaded locales
                getSetGlobalLocale(oldLocale);
            } catch (e) { }
        }
        return locales[name];
    }

// This function will load locale and then set the global locale.  If
// no arguments are passed in, it will simply return the current global
// locale key.
    function getSetGlobalLocale (key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = getLocale(key);
            }
            else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale (name, config) {
        if (config !== null) {
            var parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple('defineLocaleOverride',
                    'use moment.updateLocale(localeName, config) to change ' +
                    'an existing locale. moment.defineLocale(localeName, ' +
                    'config) should only be used for creating a new locale ' +
                    'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    if (!localeFamilies[config.parentLocale]) {
                        localeFamilies[config.parentLocale] = [];
                    }
                    localeFamilies[config.parentLocale].push({
                        name: name,
                        config: config
                    });
                    return null;
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            if (localeFamilies[name]) {
                localeFamilies[name].forEach(function (x) {
                    defineLocale(x.name, x.config);
                });
            }

            // backwards compat for now: also set the locale
            // make sure we set the locale AFTER all child locales have been
            // created, so we won't end up with the child locale set.
            getSetGlobalLocale(name);


            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale, parentConfig = baseConfig;
            // MERGE
            if (locales[name] != null) {
                parentConfig = locales[name]._config;
            }
            config = mergeConfigs(parentConfig, config);
            locale = new Locale(config);
            locale.parentLocale = locales[name];
            locales[name] = locale;

            // backwards compat for now: also set the locale
            getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

// returns locale data
    function getLocale (key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function listLocales() {
        return keys$1(locales);
    }

    function checkOverflow (m) {
        var overflow;
        var a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow =
                a[MONTH]       < 0 || a[MONTH]       > 11  ? MONTH :
                    a[DATE]        < 1 || a[DATE]        > daysInMonth(a[YEAR], a[MONTH]) ? DATE :
                        a[HOUR]        < 0 || a[HOUR]        > 24 || (a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0)) ? HOUR :
                            a[MINUTE]      < 0 || a[MINUTE]      > 59  ? MINUTE :
                                a[SECOND]      < 0 || a[SECOND]      > 59  ? SECOND :
                                    a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND :
                                        -1;

            if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

// iso 8601 regex
// 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
    var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

    var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

    var isoDates = [
        ['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/],
        ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/],
        ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/],
        ['GGGG-[W]WW', /\d{4}-W\d\d/, false],
        ['YYYY-DDD', /\d{4}-\d{3}/],
        ['YYYY-MM', /\d{4}-\d\d/, false],
        ['YYYYYYMMDD', /[+-]\d{10}/],
        ['YYYYMMDD', /\d{8}/],
        // YYYYMM is NOT allowed by the standard
        ['GGGG[W]WWE', /\d{4}W\d{3}/],
        ['GGGG[W]WW', /\d{4}W\d{2}/, false],
        ['YYYYDDD', /\d{7}/]
    ];

// iso time formats and regexes
    var isoTimes = [
        ['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/],
        ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/],
        ['HH:mm:ss', /\d\d:\d\d:\d\d/],
        ['HH:mm', /\d\d:\d\d/],
        ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/],
        ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/],
        ['HHmmss', /\d\d\d\d\d\d/],
        ['HHmm', /\d\d\d\d/],
        ['HH', /\d\d/]
    ];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

// date from iso format
    function configFromISO(config) {
        var i, l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime, dateFormat, timeFormat, tzFormat;

        if (match) {
            getParsingFlags(config).iso = true;

            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimes.length; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

// RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
    var basicRfcRegex = /^((?:Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d?\d\s(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(?:\d\d)?\d\d\s)(\d\d:\d\d)(\:\d\d)?(\s(?:UT|GMT|[ECMP][SD]T|[A-IK-Za-ik-z]|[+-]\d{4}))$/;

// date and time from ref 2822 format
    function configFromRFC2822(config) {
        var string, match, dayFormat,
            dateFormat, timeFormat, tzFormat;
        var timezones = {
            ' GMT': ' +0000',
            ' EDT': ' -0400',
            ' EST': ' -0500',
            ' CDT': ' -0500',
            ' CST': ' -0600',
            ' MDT': ' -0600',
            ' MST': ' -0700',
            ' PDT': ' -0700',
            ' PST': ' -0800'
        };
        var military = 'YXWVUTSRQPONZABCDEFGHIKLM';
        var timezone, timezoneIndex;

        string = config._i
            .replace(/\([^\)]*\)|[\n\t]/g, ' ') // Remove comments and folding whitespace
            .replace(/(\s\s+)/g, ' ') // Replace multiple-spaces with a single space
            .replace(/^\s|\s$/g, ''); // Remove leading and trailing spaces
        match = basicRfcRegex.exec(string);

        if (match) {
            dayFormat = match[1] ? 'ddd' + ((match[1].length === 5) ? ', ' : ' ') : '';
            dateFormat = 'D MMM ' + ((match[2].length > 10) ? 'YYYY ' : 'YY ');
            timeFormat = 'HH:mm' + (match[4] ? ':ss' : '');

            // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
            if (match[1]) { // day of week given
                var momentDate = new Date(match[2]);
                var momentDay = ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'][momentDate.getDay()];

                if (match[1].substr(0,3) !== momentDay) {
                    getParsingFlags(config).weekdayMismatch = true;
                    config._isValid = false;
                    return;
                }
            }

            switch (match[5].length) {
                case 2: // military
                    if (timezoneIndex === 0) {
                        timezone = ' +0000';
                    } else {
                        timezoneIndex = military.indexOf(match[5][1].toUpperCase()) - 12;
                        timezone = ((timezoneIndex < 0) ? ' -' : ' +') +
                            (('' + timezoneIndex).replace(/^-?/, '0')).match(/..$/)[0] + '00';
                    }
                    break;
                case 4: // Zone
                    timezone = timezones[match[5]];
                    break;
                default: // UT or +/-9999
                    timezone = timezones[' GMT'];
            }
            match[5] = timezone;
            config._i = match.splice(1).join('');
            tzFormat = ' ZZ';
            config._f = dayFormat + dateFormat + timeFormat + tzFormat;
            configFromStringAndFormat(config);
            getParsingFlags(config).rfc2822 = true;
        } else {
            config._isValid = false;
        }
    }

// date from iso format or fallback
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);

        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        configFromRFC2822(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        // Final attempt, use Input Fallback
        hooks.createFromInputFallback(config);
    }

    hooks.createFromInputFallback = deprecate(
        'value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' +
        'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' +
        'discouraged and will be removed in an upcoming major release. Please refer to ' +
        'http://momentjs.com/guides/#/warnings/js-date/ for more info.',
        function (config) {
            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
        }
    );

// Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(hooks.now());
        if (config._useUTC) {
            return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

// convert an array to a date.
// the array should mirror the parameters below
// note: all values past the year are optional and will default to the lowest possible value.
// [year, month, day , hour, minute, second, millisecond]
    function configFromArray (config) {
        var i, date, input = [], currentDate, yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear != null) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 &&
            config._a[MINUTE] === 0 &&
            config._a[SECOND] === 0 &&
            config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            var curWeek = weekOfYear(createLocal(), dow, doy);

            weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

            // Default to current week.
            week = defaults(w.w, curWeek.week);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from begining of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to begining of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

// constant that refers to the ISO standard
    hooks.ISO_8601 = function () {};

// constant that refers to the RFC 2822 form
    hooks.RFC_2822 = function () {};

// date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === hooks.ISO_8601) {
            configFromISO(config);
            return;
        }
        if (config._f === hooks.RFC_2822) {
            configFromRFC2822(config);
            return;
        }
        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i, parsedInput, tokens, token, skipped,
            stringLength = string.length,
            totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            // console.log('token', token, 'parsedInput', parsedInput,
            //         'regex', getParseRegexForToken(token, config));
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                }
                else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            }
            else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (config._a[HOUR] <= 12 &&
            getParsingFlags(config).bigHour === true &&
            config._a[HOUR] > 0) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }


    function meridiemFixWrap (locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

// date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig,
            bestMoment,

            scoreToBeat,
            i,
            currentScore;

        if (config._f.length === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (!isValid(tempConfig)) {
                continue;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i);
        config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
            return obj && parseInt(obj, 10);
        });

        configFromArray(config);
    }

    function createFromConfig (config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig (config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || getLocale(config._l);

        if (input === null || (format === undefined && input === '')) {
            return createInvalid({nullInput: true});
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isDate(input)) {
            config._d = input;
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (format) {
            configFromStringAndFormat(config);
        }  else {
            configFromInput(config);
        }

        if (!isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (isUndefined(input)) {
            config._d = new Date(hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (isObject(input)) {
            configFromObject(config);
        } else if (isNumber(input)) {
            // from milliseconds
            config._d = new Date(input);
        } else {
            hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC (input, format, locale, strict, isUTC) {
        var c = {};

        if (locale === true || locale === false) {
            strict = locale;
            locale = undefined;
        }

        if ((isObject(input) && isObjectEmpty(input)) ||
            (isArray(input) && input.length === 0)) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function createLocal (input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate(
        'moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other < this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

    var prototypeMax = deprecate(
        'moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/',
        function () {
            var other = createLocal.apply(null, arguments);
            if (this.isValid() && other.isValid()) {
                return other > this ? this : other;
            } else {
                return createInvalid();
            }
        }
    );

// Pick a moment m from moments so that m[fn](other) is true for all
// other. This relies on the function fn to be transitive.
//
// moments should either be an array of moment objects or an array, whose
// first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

// TODO: Use [].sort instead?
    function min () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max () {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function () {
        return Date.now ? Date.now() : +(new Date());
    };

    var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

    function isDurationValid(m) {
        for (var key in m) {
            if (!(ordering.indexOf(key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
                return false;
            }
        }

        var unitHasDecimal = false;
        for (var i = 0; i < ordering.length; ++i) {
            if (m[ordering[i]]) {
                if (unitHasDecimal) {
                    return false; // only allow non-integers for smallest unit
                }
                if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                    unitHasDecimal = true;
                }
            }
        }

        return true;
    }

    function isValid$1() {
        return this._isValid;
    }

    function createInvalid$1() {
        return createDuration(NaN);
    }

    function Duration (duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        this._isValid = isDurationValid(normalizedInput);

        // representation for dateAddRemove
        this._milliseconds = +milliseconds +
            seconds * 1e3 + // 1000
            minutes * 6e4 + // 1000 * 60
            hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days +
            weeks * 7;
        // It is impossible translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months +
            quarters * 3 +
            years * 12;

        this._data = {};

        this._locale = getLocale();

        this._bubble();
    }

    function isDuration (obj) {
        return obj instanceof Duration;
    }

    function absRound (number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

// FORMATTING

    function offset (token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset();
            var sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~(offset) % 60, 2);
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

// PARSING

    addRegexToken('Z',  matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

// HELPERS

// timezone chunker
// '+10:00' > ['10',  '00']
// '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = (string || '').match(matcher);

        if (matches === null) {
            return null;
        }

        var chunk   = matches[matches.length - 1] || [];
        var parts   = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        var minutes = +(parts[1] * 60) + toInt(parts[2]);

        return minutes === 0 ?
            0 :
            parts[0] === '+' ? minutes : -minutes;
    }

// Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            hooks.updateOffset(res, false);
            return res;
        } else {
            return createLocal(input).local();
        }
    }

    function getDateOffset (m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

// HOOKS

// This function will be called whenever a moment is mutated.
// It is intended to keep the offset in sync with the timezone.
    hooks.updateOffset = function () {};

// MOMENTS

// keepLocalTime = true means only change the timezone, without
// affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
// 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
// +0200, so we adjust the time as needed, to be valid.
//
// Keeping the time actually adds/subtracts (one hour)
// from the actual represented time. That is why we call updateOffset
// a second time. In case it wants us to change the offset again
// _changeInProgress == true case, then we have to adjust, because
// there is no such time in the given timezone.
    function getSetOffset (input, keepLocalTime, keepMinutes) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
                if (input === null) {
                    return this;
                }
            } else if (Math.abs(input) < 16 && !keepMinutes) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    addSubtract(this, createDuration(input - offset, 'm'), 1, false);
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone (input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC (keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal (keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset () {
        if (this._tzm != null) {
            this.utcOffset(this._tzm, false, true);
        } else if (typeof this._i === 'string') {
            var tZone = offsetFromString(matchOffset, this._i);
            if (tZone != null) {
                this.utcOffset(tZone);
            }
            else {
                this.utcOffset(0, true);
            }
        }
        return this;
    }

    function hasAlignedHourOffset (input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime () {
        return (
            this.utcOffset() > this.clone().month(0).utcOffset() ||
            this.utcOffset() > this.clone().month(5).utcOffset()
        );
    }

    function isDaylightSavingTimeShifted () {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {};

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
            this._isDSTShifted = this.isValid() &&
                compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal () {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset () {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc () {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

// ASP.NET json date format regex
    var aspNetRegex = /^(\-)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

// from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
// somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
// and further modified to allow for strings containing both week and day
    var isoRegex = /^(-)?P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)W)?(?:(-?[0-9,.]*)D)?(?:T(?:(-?[0-9,.]*)H)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)S)?)?$/;

    function createDuration (input, key) {
        var duration = input,
            // matching against regexp is expensive, do it on demand
            match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms : input._milliseconds,
                d  : input._days,
                M  : input._months
            };
        } else if (isNumber(input)) {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y  : 0,
                d  : toInt(match[DATE])                         * sign,
                h  : toInt(match[HOUR])                         * sign,
                m  : toInt(match[MINUTE])                       * sign,
                s  : toInt(match[SECOND])                       * sign,
                ms : toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
            };
        } else if (!!(match = isoRegex.exec(input))) {
            sign = (match[1] === '-') ? -1 : 1;
            duration = {
                y : parseIso(match[2], sign),
                M : parseIso(match[3], sign),
                w : parseIso(match[4], sign),
                d : parseIso(match[5], sign),
                h : parseIso(match[6], sign),
                m : parseIso(match[7], sign),
                s : parseIso(match[8], sign)
            };
        } else if (duration == null) {// checks for null or undefined
            duration = {};
        } else if (typeof duration === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        return ret;
    }

    createDuration.fn = Duration.prototype;
    createDuration.invalid = createInvalid$1;

    function parseIso (inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = {milliseconds: 0, months: 0};

        res.months = other.month() - base.month() +
            (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return {milliseconds: 0, months: 0};
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

// TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' +
                    'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                tmp = val; val = period; period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = createDuration(val, period);
            addSubtract(this, dur, direction);
            return this;
        };
    }

    function addSubtract (mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (days) {
            set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
        }
        if (months) {
            setMonth(mom, get(mom, 'Month') + months * isAdding);
        }
        if (updateOffset) {
            hooks.updateOffset(mom, days || months);
        }
    }

    var add      = createAdder(1, 'add');
    var subtract = createAdder(-1, 'subtract');

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6 ? 'sameElse' :
            diff < -1 ? 'lastWeek' :
                diff < 0 ? 'lastDay' :
                    diff < 1 ? 'sameDay' :
                        diff < 2 ? 'nextDay' :
                            diff < 7 ? 'nextWeek' : 'sameElse';
    }

    function calendar$1 (time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = hooks.calendarFormat(this, sod) || 'sameElse';

        var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

        return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
    }

    function clone () {
        return new Moment(this);
    }

    function isAfter (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween (from, to, units, inclusivity) {
        inclusivity = inclusivity || '()';
        return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) &&
            (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
    }

    function isSame (input, units) {
        var localInput = isMoment(input) ? input : createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units || 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
        }
    }

    function isSameOrAfter (input, units) {
        return this.isSame(input, units) || this.isAfter(input,units);
    }

    function isSameOrBefore (input, units) {
        return this.isSame(input, units) || this.isBefore(input,units);
    }

    function diff (input, units, asFloat) {
        var that,
            zoneDelta,
            delta, output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        if (units === 'year' || units === 'month' || units === 'quarter') {
            output = monthDiff(this, that);
            if (units === 'quarter') {
                output = output / 3;
            } else if (units === 'year') {
                output = output / 12;
            }
        } else {
            delta = this - that;
            output = units === 'second' ? delta / 1e3 : // 1000
                units === 'minute' ? delta / 6e4 : // 1000 * 60
                    units === 'hour' ? delta / 36e5 : // 1000 * 60 * 60
                        units === 'day' ? (delta - zoneDelta) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
                            units === 'week' ? (delta - zoneDelta) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
                                delta;
        }
        return asFloat ? output : absFloor(output);
    }

    function monthDiff (a, b) {
        // difference in months
        var wholeMonthDiff = ((b.year() - a.year()) * 12) + (b.month() - a.month()),
            // b is in (anchor - 1 month, anchor + 1 month)
            anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2, adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString () {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function toISOString() {
        if (!this.isValid()) {
            return null;
        }
        var m = this.clone().utc();
        if (m.year() < 0 || m.year() > 9999) {
            return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            return this.toDate().toISOString();
        }
        return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }

    /**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */
    function inspect () {
        if (!this.isValid()) {
            return 'moment.invalid(/* ' + this._i + ' */)';
        }
        var func = 'moment';
        var zone = '';
        if (!this.isLocal()) {
            func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
            zone = 'Z';
        }
        var prefix = '[' + func + '("]';
        var year = (0 <= this.year() && this.year() <= 9999) ? 'YYYY' : 'YYYYYY';
        var datetime = '-MM-DD[T]HH:mm:ss.SSS';
        var suffix = zone + '[")]';

        return this.format(prefix + year + datetime + suffix);
    }

    function format (inputString) {
        if (!inputString) {
            inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from (time, withoutSuffix) {
        if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
                createLocal(time).isValid())) {
            return createDuration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow (withoutSuffix) {
        return this.from(createLocal(), withoutSuffix);
    }

    function to (time, withoutSuffix) {
        if (this.isValid() &&
            ((isMoment(time) && time.isValid()) ||
                createLocal(time).isValid())) {
            return createDuration({from: this, to: time}).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow (withoutSuffix) {
        return this.to(createLocal(), withoutSuffix);
    }

// If passed a locale key, it will set the locale for this
// instance.  Otherwise, it will return the locale configuration
// variables for this instance.
    function locale (key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate(
        'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
        function (key) {
            if (key === undefined) {
                return this.localeData();
            } else {
                return this.locale(key);
            }
        }
    );

    function localeData () {
        return this._locale;
    }

    function startOf (units) {
        units = normalizeUnits(units);
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (units) {
            case 'year':
                this.month(0);
            /* falls through */
            case 'quarter':
            case 'month':
                this.date(1);
            /* falls through */
            case 'week':
            case 'isoWeek':
            case 'day':
            case 'date':
                this.hours(0);
            /* falls through */
            case 'hour':
                this.minutes(0);
            /* falls through */
            case 'minute':
                this.seconds(0);
            /* falls through */
            case 'second':
                this.milliseconds(0);
        }

        // weeks are a special case
        if (units === 'week') {
            this.weekday(0);
        }
        if (units === 'isoWeek') {
            this.isoWeekday(1);
        }

        // quarters are also special
        if (units === 'quarter') {
            this.month(Math.floor(this.month() / 3) * 3);
        }

        return this;
    }

    function endOf (units) {
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond') {
            return this;
        }

        // 'date' is an alias for 'day', so it should be considered as such.
        if (units === 'date') {
            units = 'day';
        }

        return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
    }

    function valueOf () {
        return this._d.valueOf() - ((this._offset || 0) * 60000);
    }

    function unix () {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate () {
        return new Date(this.valueOf());
    }

    function toArray () {
        var m = this;
        return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject () {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds()
        };
    }

    function toJSON () {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function isValid$2 () {
        return isValid(this);
    }

    function parsingFlags () {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt () {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict
        };
    }

// FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken (token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg',     'weekYear');
    addWeekYearFormatToken('ggggg',    'weekYear');
    addWeekYearFormatToken('GGGG',  'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

// ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

// PRIORITY

    addUnitPriority('weekYear', 1);
    addUnitPriority('isoWeekYear', 1);


// PARSING

    addRegexToken('G',      matchSigned);
    addRegexToken('g',      matchSigned);
    addRegexToken('GG',     match1to2, match2);
    addRegexToken('gg',     match1to2, match2);
    addRegexToken('GGGG',   match1to4, match4);
    addRegexToken('gggg',   match1to4, match4);
    addRegexToken('GGGGG',  match1to6, match6);
    addRegexToken('ggggg',  match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
        week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = hooks.parseTwoDigitYear(input);
    });

// MOMENTS

    function getSetWeekYear (input) {
        return getSetWeekYearHelper.call(this,
            input,
            this.week(),
            this.weekday(),
            this.localeData()._week.dow,
            this.localeData()._week.doy);
    }

    function getSetISOWeekYear (input) {
        return getSetWeekYearHelper.call(this,
            input, this.isoWeek(), this.isoWeekday(), 1, 4);
    }

    function getISOWeeksInYear () {
        return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear () {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

// FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

// ALIASES

    addUnitAlias('quarter', 'Q');

// PRIORITY

    addUnitPriority('quarter', 7);

// PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

// MOMENTS

    function getSetQuarter (input) {
        return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

// FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

// ALIASES

    addUnitAlias('date', 'D');

// PRIOROITY
    addUnitPriority('date', 9);

// PARSING

    addRegexToken('D',  match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        // TODO: Remove "ordinalParse" fallback in next major release.
        return isStrict ?
            (locale._dayOfMonthOrdinalParse || locale._ordinalParse) :
            locale._dayOfMonthOrdinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0], 10);
    });

// MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

// FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

// ALIASES

    addUnitAlias('dayOfYear', 'DDD');

// PRIORITY
    addUnitPriority('dayOfYear', 4);

// PARSING

    addRegexToken('DDD',  match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

// HELPERS

// MOMENTS

    function getSetDayOfYear (input) {
        var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
        return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
    }

// FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

// ALIASES

    addUnitAlias('minute', 'm');

// PRIORITY

    addUnitPriority('minute', 14);

// PARSING

    addRegexToken('m',  match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

// MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

// FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

// ALIASES

    addUnitAlias('second', 's');

// PRIORITY

    addUnitPriority('second', 15);

// PARSING

    addRegexToken('s',  match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

// MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

// FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });


// ALIASES

    addUnitAlias('millisecond', 'ms');

// PRIORITY

    addUnitPriority('millisecond', 16);

// PARSING

    addRegexToken('S',    match1to3, match1);
    addRegexToken('SS',   match1to3, match2);
    addRegexToken('SSS',  match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }
// MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

// FORMATTING

    addFormatToken('z',  0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

// MOMENTS

    function getZoneAbbr () {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName () {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var proto = Moment.prototype;

    proto.add               = add;
    proto.calendar          = calendar$1;
    proto.clone             = clone;
    proto.diff              = diff;
    proto.endOf             = endOf;
    proto.format            = format;
    proto.from              = from;
    proto.fromNow           = fromNow;
    proto.to                = to;
    proto.toNow             = toNow;
    proto.get               = stringGet;
    proto.invalidAt         = invalidAt;
    proto.isAfter           = isAfter;
    proto.isBefore          = isBefore;
    proto.isBetween         = isBetween;
    proto.isSame            = isSame;
    proto.isSameOrAfter     = isSameOrAfter;
    proto.isSameOrBefore    = isSameOrBefore;
    proto.isValid           = isValid$2;
    proto.lang              = lang;
    proto.locale            = locale;
    proto.localeData        = localeData;
    proto.max               = prototypeMax;
    proto.min               = prototypeMin;
    proto.parsingFlags      = parsingFlags;
    proto.set               = stringSet;
    proto.startOf           = startOf;
    proto.subtract          = subtract;
    proto.toArray           = toArray;
    proto.toObject          = toObject;
    proto.toDate            = toDate;
    proto.toISOString       = toISOString;
    proto.inspect           = inspect;
    proto.toJSON            = toJSON;
    proto.toString          = toString;
    proto.unix              = unix;
    proto.valueOf           = valueOf;
    proto.creationData      = creationData;

// Year
    proto.year       = getSetYear;
    proto.isLeapYear = getIsLeapYear;

// Week Year
    proto.weekYear    = getSetWeekYear;
    proto.isoWeekYear = getSetISOWeekYear;

// Quarter
    proto.quarter = proto.quarters = getSetQuarter;

// Month
    proto.month       = getSetMonth;
    proto.daysInMonth = getDaysInMonth;

// Week
    proto.week           = proto.weeks        = getSetWeek;
    proto.isoWeek        = proto.isoWeeks     = getSetISOWeek;
    proto.weeksInYear    = getWeeksInYear;
    proto.isoWeeksInYear = getISOWeeksInYear;

// Day
    proto.date       = getSetDayOfMonth;
    proto.day        = proto.days             = getSetDayOfWeek;
    proto.weekday    = getSetLocaleDayOfWeek;
    proto.isoWeekday = getSetISODayOfWeek;
    proto.dayOfYear  = getSetDayOfYear;

// Hour
    proto.hour = proto.hours = getSetHour;

// Minute
    proto.minute = proto.minutes = getSetMinute;

// Second
    proto.second = proto.seconds = getSetSecond;

// Millisecond
    proto.millisecond = proto.milliseconds = getSetMillisecond;

// Offset
    proto.utcOffset            = getSetOffset;
    proto.utc                  = setOffsetToUTC;
    proto.local                = setOffsetToLocal;
    proto.parseZone            = setOffsetToParsedOffset;
    proto.hasAlignedHourOffset = hasAlignedHourOffset;
    proto.isDST                = isDaylightSavingTime;
    proto.isLocal              = isLocal;
    proto.isUtcOffset          = isUtcOffset;
    proto.isUtc                = isUtc;
    proto.isUTC                = isUtc;

// Timezone
    proto.zoneAbbr = getZoneAbbr;
    proto.zoneName = getZoneName;

// Deprecations
    proto.dates  = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    proto.years  = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    proto.zone   = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
    proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

    function createUnix (input) {
        return createLocal(input * 1000);
    }

    function createInZone () {
        return createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat (string) {
        return string;
    }

    var proto$1 = Locale.prototype;

    proto$1.calendar        = calendar;
    proto$1.longDateFormat  = longDateFormat;
    proto$1.invalidDate     = invalidDate;
    proto$1.ordinal         = ordinal;
    proto$1.preparse        = preParsePostFormat;
    proto$1.postformat      = preParsePostFormat;
    proto$1.relativeTime    = relativeTime;
    proto$1.pastFuture      = pastFuture;
    proto$1.set             = set;

// Month
    proto$1.months            =        localeMonths;
    proto$1.monthsShort       =        localeMonthsShort;
    proto$1.monthsParse       =        localeMonthsParse;
    proto$1.monthsRegex       = monthsRegex;
    proto$1.monthsShortRegex  = monthsShortRegex;

// Week
    proto$1.week = localeWeek;
    proto$1.firstDayOfYear = localeFirstDayOfYear;
    proto$1.firstDayOfWeek = localeFirstDayOfWeek;

// Day of Week
    proto$1.weekdays       =        localeWeekdays;
    proto$1.weekdaysMin    =        localeWeekdaysMin;
    proto$1.weekdaysShort  =        localeWeekdaysShort;
    proto$1.weekdaysParse  =        localeWeekdaysParse;

    proto$1.weekdaysRegex       =        weekdaysRegex;
    proto$1.weekdaysShortRegex  =        weekdaysShortRegex;
    proto$1.weekdaysMinRegex    =        weekdaysMinRegex;

// Hours
    proto$1.isPM = localeIsPM;
    proto$1.meridiem = localeMeridiem;

    function get$1 (format, index, field, setter) {
        var locale = getLocale();
        var utc = createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl (format, index, field) {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return get$1(format, index, field, 'month');
        }

        var i;
        var out = [];
        for (i = 0; i < 12; i++) {
            out[i] = get$1(format, i, field, 'month');
        }
        return out;
    }

// ()
// (5)
// (fmt, 5)
// (fmt)
// (true)
// (true, 5)
// (true, fmt, 5)
// (true, fmt)
    function listWeekdaysImpl (localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = getLocale(),
            shift = localeSorted ? locale._week.dow : 0;

        if (index != null) {
            return get$1(format, (index + shift) % 7, field, 'day');
        }

        var i;
        var out = [];
        for (i = 0; i < 7; i++) {
            out[i] = get$1(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function listMonths (format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function listMonthsShort (format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function listWeekdays (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function listWeekdaysShort (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function listWeekdaysMin (localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    getSetGlobalLocale('en', {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal : function (number) {
            var b = number % 10,
                output = (toInt(number % 100 / 10) === 1) ? 'th' :
                    (b === 1) ? 'st' :
                        (b === 2) ? 'nd' :
                            (b === 3) ? 'rd' : 'th';
            return number + output;
        }
    });

// Side effect imports
    hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
    hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

    var mathAbs = Math.abs;

    function abs () {
        var data           = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days         = mathAbs(this._days);
        this._months       = mathAbs(this._months);

        data.milliseconds  = mathAbs(data.milliseconds);
        data.seconds       = mathAbs(data.seconds);
        data.minutes       = mathAbs(data.minutes);
        data.hours         = mathAbs(data.hours);
        data.months        = mathAbs(data.months);
        data.years         = mathAbs(data.years);

        return this;
    }

    function addSubtract$1 (duration, input, value, direction) {
        var other = createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days         += direction * other._days;
        duration._months       += direction * other._months;

        return duration._bubble();
    }

// supports only 2.0-style add(1, 's') or add(duration)
    function add$1 (input, value) {
        return addSubtract$1(this, input, value, 1);
    }

// supports only 2.0-style subtract(1, 's') or subtract(duration)
    function subtract$1 (input, value) {
        return addSubtract$1(this, input, value, -1);
    }

    function absCeil (number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble () {
        var milliseconds = this._milliseconds;
        var days         = this._days;
        var months       = this._months;
        var data         = this._data;
        var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!((milliseconds >= 0 && days >= 0 && months >= 0) ||
            (milliseconds <= 0 && days <= 0 && months <= 0))) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds           = absFloor(milliseconds / 1000);
        data.seconds      = seconds % 60;

        minutes           = absFloor(seconds / 60);
        data.minutes      = minutes % 60;

        hours             = absFloor(minutes / 60);
        data.hours        = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days   = days;
        data.months = months;
        data.years  = years;

        return this;
    }

    function daysToMonths (days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays (months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as (units) {
        if (!this.isValid()) {
            return NaN;
        }
        var days;
        var months;
        var milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'year') {
            days   = this._days   + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            return units === 'month' ? months : months / 12;
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week'   : return days / 7     + milliseconds / 6048e5;
                case 'day'    : return days         + milliseconds / 864e5;
                case 'hour'   : return days * 24    + milliseconds / 36e5;
                case 'minute' : return days * 1440  + milliseconds / 6e4;
                case 'second' : return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond': return Math.floor(days * 864e5) + milliseconds;
                default: throw new Error('Unknown unit ' + units);
            }
        }
    }

// TODO: Use this.as('ms')?
    function valueOf$1 () {
        if (!this.isValid()) {
            return NaN;
        }
        return (
            this._milliseconds +
            this._days * 864e5 +
            (this._months % 12) * 2592e6 +
            toInt(this._months / 12) * 31536e6
        );
    }

    function makeAs (alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds      = makeAs('s');
    var asMinutes      = makeAs('m');
    var asHours        = makeAs('h');
    var asDays         = makeAs('d');
    var asWeeks        = makeAs('w');
    var asMonths       = makeAs('M');
    var asYears        = makeAs('y');

    function get$2 (units) {
        units = normalizeUnits(units);
        return this.isValid() ? this[units + 's']() : NaN;
    }

    function makeGetter(name) {
        return function () {
            return this.isValid() ? this._data[name] : NaN;
        };
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds      = makeGetter('seconds');
    var minutes      = makeGetter('minutes');
    var hours        = makeGetter('hours');
    var days         = makeGetter('days');
    var months       = makeGetter('months');
    var years        = makeGetter('years');

    function weeks () {
        return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        ss: 44,         // a few seconds to seconds
        s : 45,         // seconds to minute
        m : 45,         // minutes to hour
        h : 22,         // hours to day
        d : 26,         // days to month
        M : 11          // months to year
    };

// helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime$1 (posNegDuration, withoutSuffix, locale) {
        var duration = createDuration(posNegDuration).abs();
        var seconds  = round(duration.as('s'));
        var minutes  = round(duration.as('m'));
        var hours    = round(duration.as('h'));
        var days     = round(duration.as('d'));
        var months   = round(duration.as('M'));
        var years    = round(duration.as('y'));

        var a = seconds <= thresholds.ss && ['s', seconds]  ||
            seconds < thresholds.s   && ['ss', seconds] ||
            minutes <= 1             && ['m']           ||
            minutes < thresholds.m   && ['mm', minutes] ||
            hours   <= 1             && ['h']           ||
            hours   < thresholds.h   && ['hh', hours]   ||
            days    <= 1             && ['d']           ||
            days    < thresholds.d   && ['dd', days]    ||
            months  <= 1             && ['M']           ||
            months  < thresholds.M   && ['MM', months]  ||
            years   <= 1             && ['y']           || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

// This function allows you to set the rounding function for relative time strings
    function getSetRelativeTimeRounding (roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof(roundingFunction) === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

// This function allows you to set a threshold for relative time strings
    function getSetRelativeTimeThreshold (threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        if (threshold === 's') {
            thresholds.ss = limit - 1;
        }
        return true;
    }

    function humanize (withSuffix) {
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var locale = this.localeData();
        var output = relativeTime$1(this, !withSuffix, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var abs$1 = Math.abs;

    function toISOString$1() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var seconds = abs$1(this._milliseconds) / 1000;
        var days         = abs$1(this._days);
        var months       = abs$1(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes           = absFloor(seconds / 60);
        hours             = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years  = absFloor(months / 12);
        months %= 12;


        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds;
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        return (total < 0 ? '-' : '') +
            'P' +
            (Y ? Y + 'Y' : '') +
            (M ? M + 'M' : '') +
            (D ? D + 'D' : '') +
            ((h || m || s) ? 'T' : '') +
            (h ? h + 'H' : '') +
            (m ? m + 'M' : '') +
            (s ? s + 'S' : '');
    }

    var proto$2 = Duration.prototype;

    proto$2.isValid        = isValid$1;
    proto$2.abs            = abs;
    proto$2.add            = add$1;
    proto$2.subtract       = subtract$1;
    proto$2.as             = as;
    proto$2.asMilliseconds = asMilliseconds;
    proto$2.asSeconds      = asSeconds;
    proto$2.asMinutes      = asMinutes;
    proto$2.asHours        = asHours;
    proto$2.asDays         = asDays;
    proto$2.asWeeks        = asWeeks;
    proto$2.asMonths       = asMonths;
    proto$2.asYears        = asYears;
    proto$2.valueOf        = valueOf$1;
    proto$2._bubble        = bubble;
    proto$2.get            = get$2;
    proto$2.milliseconds   = milliseconds;
    proto$2.seconds        = seconds;
    proto$2.minutes        = minutes;
    proto$2.hours          = hours;
    proto$2.days           = days;
    proto$2.weeks          = weeks;
    proto$2.months         = months;
    proto$2.years          = years;
    proto$2.humanize       = humanize;
    proto$2.toISOString    = toISOString$1;
    proto$2.toString       = toISOString$1;
    proto$2.toJSON         = toISOString$1;
    proto$2.locale         = locale;
    proto$2.localeData     = localeData;

// Deprecations
    proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
    proto$2.lang = lang;

// Side effect imports

// FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

// PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

// Side effect imports


    hooks.version = '2.18.1';

    setHookCallback(createLocal);

    hooks.fn                    = proto;
    hooks.min                   = min;
    hooks.max                   = max;
    hooks.now                   = now;
    hooks.utc                   = createUTC;
    hooks.unix                  = createUnix;
    hooks.months                = listMonths;
    hooks.isDate                = isDate;
    hooks.locale                = getSetGlobalLocale;
    hooks.invalid               = createInvalid;
    hooks.duration              = createDuration;
    hooks.isMoment              = isMoment;
    hooks.weekdays              = listWeekdays;
    hooks.parseZone             = createInZone;
    hooks.localeData            = getLocale;
    hooks.isDuration            = isDuration;
    hooks.monthsShort           = listMonthsShort;
    hooks.weekdaysMin           = listWeekdaysMin;
    hooks.defineLocale          = defineLocale;
    hooks.updateLocale          = updateLocale;
    hooks.locales               = listLocales;
    hooks.weekdaysShort         = listWeekdaysShort;
    hooks.normalizeUnits        = normalizeUnits;
    hooks.relativeTimeRounding = getSetRelativeTimeRounding;
    hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
    hooks.calendarFormat        = getCalendarFormat;
    hooks.prototype             = proto;

    return hooks;

})));
(function () {
    if (typeof(Prototype) !== 'undefined') {
        if (Prototype.BrowserFeatures.ElementExtensions) {
            var disablePrototypeJS = function (method, pluginsToDisable) {
                    var handler = function (event) {
                        event.target[method] = undefined;
                        setTimeout(function () {
                            delete event.target[method];
                        }, 0);
                    };
                    pluginsToDisable.each(function (plugin) {
                        jQuery(window).on(method + '.bs.' + plugin, handler);
                    });
                },
                pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover', 'tab'];
            disablePrototypeJS('show', pluginsToDisable);
            disablePrototypeJS('hide', pluginsToDisable);
        }
    }

})();


(function (window, document, $) {
    var EVENTS = {
        o: $({}),
        init: function () {
            $.each({
                trigger: 'publish',
                on: 'subscribe',
                off: 'unsubscribe'
            }, function (key, val) {
                jQuery[val] = function () {
                    EVENTS.o[key].apply(EVENTS.o, arguments);
                };
            });
        }
    };

    window.EVENTS = EVENTS.init();

})(window, window.document, jQuery);

(function (window, $) {

    var $$ = {
        $: function (obj) {
            $.extend($$, obj);
        },
        ready: function () {


            $.each(this, function (index, obj) {
                if (index !== '$' && index !== 'ready' && index !== 'fn' && index !== 'plugins') {
                    obj.ready();
                }
            });
        }
    };
    $(document).ready(function () {
        $$.ready();
        $('.match-height-container').each(function(){
            // Cache the highest
            var highestBox = 0;
            // Select and loop the elements you want to equalise
            $('.match-height', this).each(function(){
                // If this box is higher than the cached highest then store it
                if($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            });
            // Set the height of all those children to whichever was highest
            $('.match-height',this).height(highestBox);

        });
    });

    window.KTJS = $$;

})(window, jQuery);
(function ($, $$) {

    $$.$({
        tools: {
            obj: function (o, s) {
                s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
                s = s.replace(/^\./, '');           // strip a leading dot
                var a = s.split('.');
                for (var i = 0, n = a.length; i < n; ++i) {
                    var k = a[i];
                    if (k in o) {
                        o = o[k];
                    } else {
                        return;
                    }
                }
                return o;
            },
            ready: function () {
                return true;
            }
        }
    });

})(jQuery, KTJS);
(function ($, $$, W) {

    "use strict";

    /**
     *  extend/callback.js
     */

    // ToDO
    // check documentation

    /**
     * @param
     *  _callback: String // the fn callback
     *
     * @description
     *  This extend function used to get property of obj
     *
     * @used
     *  - Get index
     *  $().callback('index.index2')
     *
     * @example
     *  var obj = {
     *              a : {
     *                 b:{
     *                     c: 2
     *                  }
     *              }
     *          };
     *
     * $.obj(obj, 'a.b.c')
     */


    $.fn.extend({
        callback: function (_callback) {
            var _func = _callback;

            if (typeof _callback === "string") {
                if (typeof W[_callback] === typeof undefined) {
                    _func = $().obj(W, _callback);
                } else {
                    _func = W[_callback];
                }
            }

            return _func;
        }
    });

})(jQuery, KTJS, window);
(function ($, $$, W) {

    "use strict";

    /**
     *  extend/check.js
     */

    /**
     * @param
     *  _param: any // the fn callback
     *
     * @description
     *  This extend function used to check if _param exist and not null
     *
     * @used
     *  - Check _param
     *  $().check([name_of_param])
     *
     * @example
     *  var param1 = '';
     *  var param2 = 'test';
     *
     * $().check(param1) // false
     * $().check(param2) // true
     * $().check(param3) // false
     */


    $.fn.extend({
        check: function (_param) {
            return (typeof _param !== undefined && _param !== '' && _param !== null);
        }
    });

})(jQuery, KTJS, window);
(function ($, $$) {
    /**
     *  extend/data-init.js
     */

    // ToDO
    // ...


    $.fn.extend({
        initPlugin: function (arg) {
            var data = (!arg) ? $(this).data('init-plugin') : arg,
                _data = [],
                regexp_array = /(^\[*)(.[^\[\]]*)(\]?)/g,
                regexp_trim = /(?=\S)[^,]+?(?=\s*(,|$))/g;

            // Test if has plugin to init
            if (data) {

                // Test if data type is array
                if (typeof data === 'object') {
                    _data = arg;
                } else { // Else convert data to array
                    data = regexp_array.exec(data)[2];
                    _data = data.match(regexp_trim);
                }

                if (_data.length) {
                    $.each(_data, function (index, obj) {
                        //console.log(index);
                        //console.log(obj);
                    });
                }

            }
        }
    });

})(jQuery, KTJS);
(function ($, $$) {

    "use strict";

    /**
     *  extend/data-json.js
     */

    // ToDO
    // ...


    /**
     * @param
     *  index: string // specifically index in data
     *
     * @description
     *  This extend function used to get json data defined in tag element
     *
     * @define
     *  <div id="input-test-json" data-json='{"select2":{"maximumSelectionLength":2}}'>...<div>
     *
     * @used
     *  - Get all index
     *  $('#input-test-json').json()
     *
     *  - Get select2 index
     *  $('#input-test-json').json().select2
     *  $('#input-test-json').json('select2')
     *
     * @example
     *  <div id="input-test-json" data-json='{"select2":{"maximumSelectionLength":2}}'>...<div>
     *  $('[data-init-plugin="select2"]').select2(
     *   $('#input-test-json').json().select2
     *  )
     *
     */


    $.fn.extend({

        json: function (index) {
            var data = $(this).data('json'),
                _data = {};

            // Get all data
            if (typeof data === 'object') {
                _data = data;
            } else {
                try {
                    _data = JSON.parse(data);
                } catch (e) {
                    _data = {};
                    console.error("Parsing data error in element :", this);
                }
            }

            // Get specifically index
            if(index && $().obj(_data, index)){
                _data = $().obj(_data, index);
            }

            // return _data
            return _data;
        }
    });

})(jQuery, KTJS);
(function ($, $$) {

    "use strict";

    /**
     *  extend/obj.js
     */


    /**
     * @param
     *  o: Object // the object
     *  s: string // indexes to properties
     *  d: Any // default if not exist
     *
     * @description
     *  This extend function used to get property of obj
     *
     * @used
     *  - Get index
     *  $().obj(obj, 'index.index2')
     *
     * @example
     *  var obj = {
     *              a : {
     *                 b:{
     *                     c: 2
     *                  }
     *              }
     *          };
     *
     * $().obj(obj, 'a.b.c')
     */


    $.fn.extend({
        obj: function (o, s, d) {
            if( typeof(d) == 'undefined' ){
                d = null;
            }
            s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
            s = s.replace(/^\./, '');           // strip a leading dot
            var a = s.split('.');
            for (var i = 0, n = a.length; i < n; ++i) {
                var k = a[i];
                if (k in o) {
                    o = o[k];
                } else {
                    return d;
                }
            }
            return o;
        }
    });

})(jQuery, KTJS);

(function ($, $$) {
    /**
     *
     *    Regular expression for parsing URLs.
     *
     *    Taken from parseUri 1.2 (http://blog.stevenlevithan.com/archives/parseuri).
     *
     **/

    var urlRegEx = /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/;

    /*

     Global object of watched selectors and their array of callstacks.

     */

    var selectorCallStack = {};

    /*

     Currently watched selector. ToDo: Remove this and use a better mechanism for selector tracking.

     */

    var currentSelector = "";

    /**
     *
     *    Section: Internal functions.
     *
     *    All internal private functions.
     *
     *    This section contains all internal functions that perform the grunt work for the parser interface.
     *
     **/

    /**
     *
     *    Function: initialiseElement
     *
     *    Initialise an element for use with the URL parser.
     *
     *    Parameters:
     *
     *    this - The element to initialise. See <this parameter>.
     *
     **/

    var initialiseElement = function () {
        /* Attempt to retreive a href from the element */

        var href = getHref.apply(this);

        /* Was a href found for the element? */

        if (href == "") {
            /* Is the current element the document? */

            if (this.get(0) == $(document).get(0)) {
                /* Use the document href */

                href = document.location.href;
            }
            else if (this.is("[href]")) {
                /* Use the element href attribute */

                href = this.attr("href");
            }
            else if (this.is("[src]")) {
                /* Use the element src attribute */

                href = this.attr("src");
            }

            /* Check the href is not empty so we don't initialise the "data-href" attribute on text elements (although maybe this is not wanted, as an empty href is technically the current page?). */

            if (href != "") {
                /* Sanitise the URL */

                href = sanitiseUrl(href);

                /* Store the URL as a data attribute within the element */

                this.data("href", href);
            }
        }
    };

    /**
     *
     *    Function: initialiseElementText
     *
     *    Initialise an elements text field for use with the URL parser.
     *
     *    Parameters:
     *
     *    this - The element to initialise.
     *
     **/

    var initialiseElementText = function () {
        /* Is the current element not the document, and also does not contain a "href" attribute */

        if (this.get(0) != $(document).get(0) && this.attr("href") == null && this.attr("src") == null) {
            /* Does the element contain anything? */

            if (this.html() != null && this.hasClass("jurlp-span") == false) {
                var urls = [];
                var modifiedHtml = false;
                var match = "";

                /* Regular expression for finding URLs in free text */

                var findUrlRegExp = /((((mailto|spotify|skype)\:([a-zA-Z0-9\.\-\:@\?\=\%]*))|((ftp|git|irc|ircs|irc6|pop|rss|ssh|svn|udp|feed|imap|ldap|nntp|rtmp|sftp|snmp|xmpp|http|https|telnet|ventrilo|webcal|view\-source)\:[\/]{2})?(([a-zA-Z0-9\.\-]+)\:([a-zA-Z0-9\.&;%\$\-]+)*@)*((25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])|localhost|([a-zA-Z0-9\-]+\.)*[a-zA-Z0-9\-]+\.(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|xn--0zwm56d|xn--11b5bs3a9aj6g|xn--80akhbyknj4f|xn--9t4b11yi5a|xn--deba0ad|xn--g6w251d|xn--hgbk6aj7f53bba|xn--hlcj6aya9esc7a|xn--jxalpdlp|xn--kgbechtv|xn--zckzah|[a-zA-Z]{2}))(\:[0-9]+)*(\/($|[a-zA-Z0-9\.\,\?\'\\\+&;%\$\=~_\-]+)?(#\w*)?)*))/i;

                /* Store the elements HTML */

                var html = this.html();

                /* Attempt to locate URLs within the HTML */

                while (match = findUrlRegExp.exec(html)) {
                    /* Replace the URL with a unique ID */

                    html = html.replace(match [0], "$" + urls.length);

                    /* Store the discovered URL */

                    urls.push(match [0]);

                    /* Indicate that the HTML was modified */

                    modifiedHtml = true;
                }

                /* Iterate through all discovered URLs */

                for (var i = 0; i < urls.length; i++) {
                    /* Get the postion of the current URL ID */

                    var pos = html.indexOf("$" + i);

                    /* Get the possible attribute name */

                    var attr = html.substring(pos - 6, pos - 1);

                    /* Does the URL reside within an attribute (i.e. an existing tag) */

                    if (attr == "href=" || attr == " src=" || html.substring(pos - 1, pos) == ">") {
                        /* Replace the ID with the original URL (do not convert this URL to an anchor as it most likely is part of one) */

                        html = html.replace("$" + i, urls [i]);
                    }
                    else {
                        /* Replace the unique ID with an anchor tag */

                        html = html.replace("$" + i, "<a href=\"[url]\" class=\"jurlp-no-watch\">[url]</a>".replace(/\[url\]/g, urls [i]));
                    }
                }

                /* Did we change the HTML at all? */

                if (modifiedHtml != false) {
                    /* Add a class on the parent element to indicate that we have modified it */

                    this.addClass("jurlp-span");

                    /* Update the elements HTML */

                    this.html(html);

                    /* Find and return all newly created anchor tags */

                    return this.find("a[href]").each
                    (
                        function () {
                            /* Get the href attribute for the element */

                            var href = getHref.apply($(this));

                            /* Sanitise the URL and reset the elements href */

                            setHref.apply($(this), [href]);
                        }
                    );
                }
            }
        }

        /* No URLs found */

        return null;
    }

    /**
     *
     *    Function: setAttrUrl
     *
     *    Given an element, and an attribute, set the attribute to the supplied URL, and created a backup of the original URL if not already done.
     *
     *    Note, if the attribute doesn't exist, then it will not be created.
     *
     *    Parameters:
     *
     *    this - The element to set the attribute URL on.
     *
     *    attr - The name of the attribute to set.
     *
     *    url - The value of the attributes URL.
     *
     **/

    var setAttrUrl = function (attr, url) {
        /* Is the attribute present on this element? */

        if (this.is("[" + attr + "]") != false) {
            /* Has a copy of the original attribute been stored? */

            if (this.data("original-" + attr) == null) {
                /* Store a copy of the original attribute */

                this.data("original-" + attr, this.attr(attr));
            }

            /* Update the elements attribute */

            this.attr(attr, url);
        }
    };

    /**
     *
     *    Function: restoreAttrUrl
     *
     *    Given an element, and an attribute, then restore the URL attribute value to its original value.
     *
     *    Parameters:
     *
     *    this - The element to restore the attribute URL on.
     *
     *    attr - The name of the attribute to restore.
     *
     **/

    var restoreAttrUrl = function (attr) {
        /* Was a backup of the original attribute URL made? */

        if (this.data("original-" + attr) != null) {
            /* Restore the attribute URL */

            this.attr(attr, this.data("original-" + attr));

            /* Remove the original URL */

            this.removeData("original-" + attr);
        }
    };

    /**
     *
     *    Function: restoreElement
     *
     *    Destroys any data associated with an element that has previously been initialised for use with the URL parser, and restores the elements "href" or "src" attribute (if any) to its original value.
     *
     *    Parameters:
     *
     *    this - The element to destroy.
     *
     **/

    var restoreElement = function () {
        /* Remove the working href URL */

        this.removeData("href");

        /* Restore the href attribute */

        restoreAttrUrl.apply(this, ["href"]);

        /* Restore the src attribute */

        restoreAttrUrl.apply(this, ["src"]);

        /* Remove any watch attributes */

        this.removeData("jurlp-no-watch");
        this.removeData("is-watched");

        /* clean up selector callstack and unbind */

        methods.unwatch.apply(this);
    }

    /**
     *
     *    Function: getHref
     *
     *    Get the href URL for the element. Prioritises internal objects href, over "data-href", over "href", over "src" attributes.
     *
     *    Parameters:
     *
     *    this - The element to retieve the URL value from.
     *
     **/

    var getHref = function () {
        return this.href || this.data("href") || this.attr("href") || this.attr("src") || "";
    };

    /**
     *
     *    Function: updateHref
     *
     *    Update a segment of the elements href URL.
     *
     *    Parameters:
     *
     *    this - The element to update the URL value on.
     *
     *    segment - The segment to update ("scheme", "host", "port", "path", "query" or "fragment").
     *
     *    value - The new value for the segment.
     *
     **/

    var updateHref = function (segment, value) {
        setHref.apply(this, [setUrlSegment(getHref.apply(this), segment, value)]);
    };

    /**
     *
     *    Function: updateHrefShim
     *
     *    Shim function for reorganising parameters before calling updateHref(). Called via the each callback.
     *
     *    Parameters:
     *
     *    this - The element to update the URL value on.
     *
     *    parameters - Array containing segment and value parameters for updateHref().
     *
     **/

    var updateHrefShim = function (parameters) {
        updateHref.apply(this, [parameters [0], parameters [1]]);
    };

    /**
     *
     *    Function: setHref
     *
     *    Sets the href URL value for an element.
     *
     *    Parameters:
     *
     *    this - The element to set the URL value on.
     *
     *    url - The new url (string) value.
     *
     **/

    var setHref = function (url) {
        /* Ensure the supplied URL is a string */

        if (typeof url == "object") {
            url = objectToUrl(url);
        }

        /* Sanitise the URL - slow and horrible :( */

        url = sanitiseUrl(url);

        if (this.href != null) {
            this.href = url;

            return;
        }

        /* Is the current element the document? */

        if (this.get(0) == $(document).get(0)) {
            /* Current element is the document. Save the href under a data attribute. */

            this.data("href", url);
        }
        else {
            /* Update href URL (if present) */

            setAttrUrl.apply(this, ["href", url]);

            /* Update src URL (if present) */

            setAttrUrl.apply(this, ["src", url]);
        }
    };

    /**
     *
     *    Function: urlToObject
     *
     *    Parse a URL into segments using the DOM. Parses authority information from the URL using parseUri (http://blog.stevenlevithan.com/archives/parseuri).
     *
     *    Parameters:
     *
     *    url - URL String to parse.
     *
     *    Returns:
     *
     *    URL object.
     *
     **/

    var urlToObject = function (url) {
        /* Was a null URL supplied? */

        if (url == null) {
            /* Return an empty object */

            return {scheme: "", user: "", password: "", host: "", port: "", path: "", query: "", fragment: ""};
        }

        var credentials = {user: "", password: ""};

        if (url.substring(0, 2) == "//") {
            url = "http:" + url;
        }

        /* If a URL is supplied, ensure a protocol is specified, otherwise the parser will assume that the supplied host is the path */

        if (url != "" && url.indexOf("://") == -1) {
            url = "http://" + url;
        }

        /* Does the URL contain authority information? */

        if (url.indexOf("@") != -1) {
            /* Parse the URL using regex */

            var urlSegments = url.match(urlRegEx);

            /* Was a username found? */

            if (urlSegments [4]) {
                credentials.user = urlSegments [4];
            }

            /* Was a password found? */

            if (urlSegments [5]) {
                credentials.password = urlSegments [5];
            }
        }

        /* Construct a new anchor element based on the supplied URL */

        var a = document.createElement("a");

        /* Set the anchor href to the URL to parse (let the browser do (most of) the parsing) */

        a.href = url;

        /* Under IE, an anchor element containing a username and password is inaccessible, so we will probe "a.protocol" to test if we have access */

        try {
            /* Check if the element is accessible */

            var accessible = a.protocol;
        }
        catch (err) {
            /* MSIE: A security problem occurred. (cannot access the anchor element) */

            if (err.number == -2146697202) {
                /* IE hack!.. strip the username and password from the URL, and reparse */

                var authority = "";

                /* Were credentials found in the URL? */

                if (credentials.user != "") {
                    /* Build string containing the username and password to strip from the URL */

                    authority += credentials.user;

                    if (credentials.password != "") {
                        authority += ":" + credentials.password;
                    }

                    authority += "@";

                    /* Strip the username and password from the URL and set the anchor href (this could in theory be done for all browsers) */

                    a.href = url.replace(authority, "");
                }
            }
        }

        /* Sanitise the protocol string */

        var protocol = a.protocol;

        if (a.protocol.indexOf("//") == -1) {
            protocol += "//";
        }

        /* Sanitise the path string */

        var pathname = a.pathname;

        if (pathname [0] != "/") {
            pathname = "/" + pathname;
        }

        /* Ensure the port value is a string */

        var port = a.port + "";

        /* Strip of default port numbers if added, and not present in the original URL */

        if (( port == "21" && url.indexOf(":21") == -1 ) || ( port == "80" && url.indexOf(":80") == -1 ) || ( port == "443" && url.indexOf(":443") == -1 ) || port == "0") {
            port = "";
        }

        /* Return the URL object, based on the URL information for the newly created anchor element contained in the DOM */

        return {
            scheme: protocol,
            user: credentials.user,
            password: credentials.password,
            host: a.hostname,
            port: port,
            path: pathname,
            query: a.search,
            fragment: a.hash
        };
    };

    /**
     *
     *    Function: objectToUrl
     *
     *    Convert a URL object to a string.
     *
     *    Parameters:
     *
     *    url - The URL object to convert.
     *
     *    Returns:
     *
     *    URL string.
     *
     **/

    var objectToUrl = function (url) {
        /* Build URL string, starting with the scheme */

        var urlString = url.scheme;

        /* Was a username specified? */

        if (url.user != null && url.user != "") {
            /* Insert the username */

            urlString += url.user;

            /* Was a password also specified? */

            if (url.password != null && url.password != "") {
                urlString += ":" + url.password;
            }

            /* Insert authority/host seperator */

            urlString += "@";
        }

        /* Glue the remainder of the URL together (only including the port if explicitly specified) */

        return urlString + url.host + ( url.port != "" ? ":" + url.port : "" ) + url.path + url.query + url.fragment;
    };

    /**
     *
     *    Function: sanitiseUrl
     *
     *    Sanitise a URL. Creates a fully qualified URL by converting it from a string to a DOM element and back to a string again.
     *
     *    Parameters:
     *
     *    url - The URL to sanitise.
     *
     *    Returns:
     *
     *    The sanitised URL string.
     *
     **/

    var sanitiseUrl = function (url) {
        return uri.parse(url).toString();
    };

    /**
     *
     *    Function: urlObjectToString
     *
     *    Converts a URL object to a string (used to override toString for URL objects).
     *
     *    Parameters:
     *
     *    this - The URL object to convert to a string.
     *
     *    Returns:
     *
     *    The URL string.
     *
     **/

    var urlObjectToString = function () {
        return objectToUrl(this);
    };

    /**
     *
     *    Function: setUrlSegment
     *
     *    Set the value of a segment within a URL string.
     *
     *    Parameters:
     *
     *    url - The URL to modify.
     *
     *    segment - The segment of the URL to modify ("scheme", "host", "port", "path", "query" or "fragment").
     *
     *    value - The new segment value.
     *
     *    Returns:
     *
     *    The URL string containing the update segment.
     *
     **/

    var setUrlSegment = function (url, segment, value) {
        /* Convert the URL to an object */

        var urlObject = uri.parse(url);

        /* Update the URL segment */

        urlObject [segment] = value;

        /* Convert the URL object back to a string */

        return objectToUrl(urlObject);
    };

    /**
     *
     *    Function: getUrlObject
     *
     *    Convert a URL string to an object, if not already. Used to ensure we always work with URL objects where either a string or object can be supplied.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    URL object.
     *
     **/

    var getUrlObject = function (url) {
        /* Is the URL a string? */

        if (typeof url == "string") {
            /* Return the URL string converted to an object */

            return uri.parse(url);
        }

        /* Return the URL object */

        return url;
    };

    /**
     *
     *    Function: getFragmentString
     *
     *    Retrieve the fragment string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The fragment string.
     *
     **/

    var getFragmentString = function (url) {
        return getUrlObject(url).fragment;
    };

    /**
     *
     *    Function: getQueryString
     *
     *    Retrieve the query string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The query string.
     *
     **/

    var getQueryString = function (url) {
        /* Get the query string from the URL object */

        var query = getUrlObject(url).query;

        /* Anything?`*/

        if (query [0] == "?") {
            /* Strip the leading "?" (makes .split() happy later) */

            return query.slice(1);
        }

        /* Return the (probably not very valid?) query */

        return query;
    };

    /**
     *
     *    Function: updateQuery
     *
     *    Update the query string for the elements URL.
     *
     *    Parameters:
     *
     *    this - The element to set the new query string on.
     *
     *    query - New query object.
     *
     **/

    var updateQuery = function (query) {
        var queryObject = {};

        /* Get the query object for the current URL */

        var currentQueryObject = getQueryObject(getHref.apply(this));

        /* Was a string supplied as the parameter? */

        if (typeof query == "string") {
            /* If the first character is a "?", then replace the whole query string */

            if (query [0] == "?") {
                /* Trash the existing query object */

                currentQueryObject = {};

                /* Strip the leading question mark */

                query = query.substring(1);
            }

            /* Convert the supplied query string to an object */

            queryObject = queryStringToObject(query);
        }
        else {
            queryObject = query;
        }

        /* Did the supplied query object contain parameters? */

        if ($.isEmptyObject(queryObject) == false) {
            /* Extend the existing query object with the new query object */

            queryObject = $.extend(currentQueryObject, queryObject);
        }
        else {
            /* An empty query object was supplied, so null the query string */

            currentQueryObject = {};
        }

        /* Convert the query object to a string, and update the URL query string */

        updateHref.apply(this, ["query", queryObjectToString.apply(queryObject)]);
    };

    /**
     *
     *    Function: queryStringToObject
     *
     *    Convert a query string to an object.
     *
     *    Parameters:
     *
     *    query - Query string to convert to an object.
     *
     *    Returns:
     *
     *    The query object.
     *
     **/

    var queryStringToObject = function (query) {
        var object = {};

        /* Was a query string supplied? */

        if (query != "") {
            /* Get all elements of the query string ("&name=value") */

            var elements = query.split("&");

            /* Create the query object */

            for (var i = 0; i < elements.length; i++) {
                /* Retrieve the parameter name and value from the string "name=value" */

                var parameter = elements [i].split("=");

                /* Add the parameter to the query object */

                object [parameter [0]] = parameter [1];
            }
        }

        /* Return the query object */

        return object;
    };

    /**
     *
     *    Function: getQueryObject
     *
     *    Retrieve the query object for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The query object.
     *
     **/

    var getQueryObject = function (url) {
        /* Get the query string from the URL and convert it to an object */

        return queryStringToObject(getQueryString(url));
    };

    /**
     *
     *    Function: queryObjectToString
     *
     *    Query objects toString method.
     *
     *    Parameters:
     *
     *    this - Query object.
     *
     *    Returns:
     *
     *    The query string.
     *
     **/

    var queryObjectToString = function () {
        var string = "";

        /* For each item in the query string */

        for (var i in this) {
            /* Ensure that it contains valid data */

            if (i != "toString" && this [i] != null) {
                string += "&" + i + "=" + this [i];
            }
        }

        /* Anything? */

        if (string [0] == "&") {
            /* Return the query string (replacing the first "&" character with a "?" character. */

            string = "?" + string.slice(1);
        }

        /* Empty query string */

        return string;
    };

    /**
     *
     *    Function: getPathString
     *
     *    Retrieve the path string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The path string.
     *
     **/

    var getPathString = function (url) {
        /* Get the path string from the URL object */

        var path = getUrlObject(url).path;

        /* Ensure the path starts with a leading slash */

        if (path [0] == "/") {
            /* Strip the leading slash from the path */

            return path.slice(1);
        }

        /* Empty or malformed path */

        return path;
    };

    /**
     *
     *    Function: getPathObject
     *
     *    Retrieve the path object for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The path obbject.
     *
     **/

    var getPathObject = function (url) {
        /* Get the path string from the URL object (without leading slash) */

        var pathString = getPathString(url);

        /* Anything? */

        if (pathString != "") {
            /* Create the path object */

            return pathString.split("/");
        }

        /* No path specified */

        return [];
    };

    /**
     *
     *    Function: updatePath
     *
     *    Update the path string for the elements URL.
     *
     *    Parameters:
     *
     *    this - The element to set the new path string on.
     *
     *    path - New path object.
     *
     **/

    var updatePath = function (path) {
        var pathString = "";

        /* Get an array of existing path elements */

        var pathArray = getUrlObject(getHref.apply(this)).path.split("/");

        /* Get an array of new path elements */

        var newPathArray = path.split("/");
        var i = 0;

        /* Was the first character a "/"? */

        if (newPathArray [0] == "") {
            /* Truncate the existing path */

            pathArray = [];

            i++;
        }

        /* Remove the initial empty path element */

        pathArray.splice(0, 1);

        /* Iterate through all new path elements */

        for (var l = newPathArray.length; i < l; i++) {
            /* Remove an old path element? */

            if (newPathArray [i] == "..") {
                /* Remove the element from the path array */

                if (pathArray.length > 0) {
                    pathArray.splice(pathArray.length - 1, 1);
                }
            }
            else if (newPathArray [i] == ".") {
                /* Current directory */
            }
            else {
                /* Include the new path element */

                pathArray.push(newPathArray [i]);
            }
        }

        /* Update the path string */

        updateHref.apply(this, ["path", pathObjectToString.apply(pathArray)]);
    };

    /**
     *
     *    Function: pathObjectToString
     *
     *    Path objects toString method.
     *
     *    Parameters:
     *
     *    this - Path object.
     *
     *    Returns:
     *
     *    The path string.
     *
     **/

    var pathObjectToString = function () {
        /* Are there any path elements? */

        if (this.length > 0) {
            /* Join all elements with a "/", and return the leading path slash again */

            return "/" + this.join("/");
        }

        /* No path */

        return "/";
    };

    /**
     *
     *    Function: getPortString
     *
     *    Retrieve the port string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The port string.
     *
     **/

    var getPortString = function (url) {
        return getUrlObject(url).port;
    };

    /**
     *
     *    Function: getHostString
     *
     *    Retrieve the host string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The host string.
     *
     **/

    var getHostString = function (url) {
        return getUrlObject(url).host;
    };

    /**
     *
     *    Function: getPasswordString
     *
     *    Retrieve the password string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The password string.
     *
     **/

    var getPasswordString = function (url) {
        return getUrlObject(url).password;
    };

    /**
     *
     *    Function: getUserString
     *
     *    Retrieve the user string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The user string.
     *
     **/

    var getUserString = function (url) {
        return getUrlObject(url).user;
    };

    /**
     *
     *    Function: getSchemeString
     *
     *    Retrieve the scheme string for a given URL.
     *
     *    Parameters:
     *
     *    url - URL string or object.
     *
     *    Returns:
     *
     *    The scheme string.
     *
     **/

    var getSchemeString = function (url) {
        return getUrlObject(url).scheme;
    };

    /**
     *
     *    Function: addSelectorCallback
     *
     *    Add a function the selector callstack.
     *
     **/

    var addSelectorCallback = function (element, callback, parameters) {
        if (element.data("is-watched") == true) {
            return;
        }

        if (selectorCallStack [currentSelector]) {
            selectorCallStack [currentSelector].push([callback, parameters]);
        }
    };

    /**
     *
     *    Function: returnEachElement
     *
     *    Apply the callback for each element in this. Used for methods that return elements.
     *
     *    Parameters:
     *
     *    this Array of elements to iterate through.
     *
     *    callback Function to call for each element found.
     *
     *    parameters Callback function parameters (array).
     *
     *    Returns:
     *
     *    Array of elements.
     *
     **/

    var returnEachElement = function (callback, parameters) {
        /* Is this an object, containing an href member? */

        if (this.href != null) {
            /* Issue the callback */

            callback.apply(this, [parameters]);

            /* Return the object (for chaining purposes) */

            return this;
        }

        /* Add this function and parameters to the watch selector callstack (if watched) */

        addSelectorCallback(this, callback, [parameters]);

        /* Return all elements, after applying the callback */

        return this.each
        (
            function () {
                callback.apply($(this), [parameters]);
            }
        );
    };

    /**
     *
     *    Function: returnEachObject
     *
     *    Apply the callback for each element in this, and buffer return codes. Used for methods that return data.
     *
     *    Parameters:
     *
     *    this Array of elements to iterate through.
     *
     *    callback Function to call for each element found.
     *
     *    parameters Callback function parameters (array).
     *
     *    Returns:
     *
     *    Array of return codes.
     *
     **/

    var returnEachObject = function (callback, parameters) {
        if (this.href != null) {
            return callback.apply(this, [parameters]);
        }

        var result = [];

        /* Build and return an array of each elements callback results */

        this.each
        (
            function () {
                result.push(callback.apply($(this), [parameters]));
            }
        );

        return result;
    };

    /**
     *
     *    Function: dispatchGetSetHelper
     *
     *    Dispatch to get or set helper functions depending on the arguments supplied.
     *
     *    If no user arguments are supplied, perform the get, ortherwise perform the set with the user arguments.
     *
     *    Parameters:
     *
     *    getHelper - Get URL data callback.
     *
     *    setHelper - Set URL data callback.
     *
     *    helperArguments - User arguments supplied to the public interface.
     *
     *    Returns:
     *
     *    get/setHelper() return code.
     *
     **/

    var dispatchGetSetHelper = function (getHelper, setHelper, helperArguments) {
        if (helperArguments.length == 0) {
            return getHelper.apply(this)
        }

        return setHelper.apply(this, helperArguments)
    };

    /**
     *
     *    Function: methodDispatcher
     *
     *    Main method dispatcher for the public interface.
     *
     *    Parameters: method
     *
     *    method - The method to perform.
     *
     *    Returns:
     *
     *    Array of method handler results (either elements for set/filter methods, or strings/objects for get methods).
     *
     */

    var methodDispatcher = function (method) {
        /* Is the method name valid */

        if (methods [method] != null) {
            /* Dispatch to the method handler */

            return methods [method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof method == "object" || method == null) {
            /* No method, or an object was supplied, initialise the element(s) */

            return methods.initialise.apply(this, Array.prototype.slice.call(arguments, 1));
        }

        /* Invalid method/parameters */

        return this;
    };

    /**
     *
     *    Section: URI parser interface.
     *
     *    All URI parsing is handled through this interface.
     *
     *    This section contains all parser interfaces utilised by the public parser interface.
     *
     *    ToDo: Extend this interface with the current URL segment parsing logic, and implement a more comprehensive URI parser set.
     *
     *    See http://en.wikipedia.org/wiki/URI_scheme for an overview of URIs.
     *
     **/

    var uri =
        {
            /**
             *
             *    All URI object to string methods.
             *
             **/

            toString: {
                /**
                 *
                 *    Function: uri.toString.http
                 *
                 *    Converts a URI object with an "http" scheme to a string.
                 *
                 **/

                "http": function () {
                    return objectToUrl(this);
                },

                /**
                 *
                 *    Function: uri.toString.mailto
                 *
                 *    Converts a URI object with a "mailto:" scheme to a string.
                 *
                 **/

                "mailto": function () {
                    /* Blank invalid fields */

                    this.password = "";
                    this.path = "";
                    this.port = "";

                    return objectToUrl(this);
                },

                /**
                 *
                 *    Function: uri.toString.javascript
                 *
                 *    Converts a URI object with a "javascript:" scheme to a string.
                 *
                 **/

                "javascript": function () {
                    return "javascript:" + this.javascript;
                },

                /**
                 *
                 *    Function: uri.toString.generic
                 *
                 *    Converts an generic URI object to a string.
                 *
                 **/

                "generic": function () {
                    return this.scheme + this.url;
                }
            },

            /**
             *
             *    All URI string parsers.
             *
             **/

            parsers: {
                /**
                 *
                 *    Function: uri.parsers.http
                 *
                 *    Parse a URI with a "http://" scheme into a URI object.
                 *
                 **/

                "http": function (url) {
                    return $.extend(urlToObject(url), {toString: uri.toString.http});
                },

                /**
                 *
                 *    Function: uri.parsers.mailto
                 *
                 *    Parse a URI with a "mailto:" scheme into a URI object.
                 *
                 **/

                "mailto": function (url) {

                    return $.extend(urlToObject(url.substring(7)), {scheme: "mailto:", toString: uri.toString.mailto});
                },

                /**
                 *
                 *    Function: uri.parsers.javascript
                 *
                 *    Parse a URI with a "javascript:" scheme into a URI object.
                 *
                 **/

                "javascript": function (url) {
                    return $.extend(urlToObject(document.location.href), {
                        javascript: url.substring(11),
                        toString: uri.toString.javascript
                    });
                },

                /**
                 *
                 *    Function: uri.parsers.generic
                 *
                 *    Parses any URI (URIs with a scheme seperator of "://" are parsed as "http://", everything else is treated as unknown..
                 *
                 **/

                "generic": function (scheme, url) {
                    /* Was a "//" specified in the scheme? */

                    if (url.substring(0, 2) == "//") {
                        /* Parse as HTTP URL */

                        return $.extend(urlToObject(url.substring(2)), {
                            scheme: scheme + "://",
                            toString: uri.toString.http
                        });
                    }

                    /* Unknown, store the schem (for filtering purposes), and simply append the remainder of the URI */

                    return {scheme: scheme + ":", url: url, toString: uri.toString.generic};
                }
            },

            /**
             *
             *    Function: uri.parse
             *
             *    Parse a URI string based on scheme.
             *
             **/

            parse: function (uri) {
                /* Only parse strings */

                if (typeof uri != "string") {
                    /* URI object? */

                    return uri;
                }

                /* Try to determine the scheme */

                var pos = uri.indexOf(":");

                /* Was a scheme seperator found? */

                if (pos != -1) {
                    /* Get the scheme name (from the start of the URI, until the first ":" character)*/

                    var scheme = uri.substring(0, pos).toLowerCase();

                    /* Is a handler present for this scheme? */

                    if (this.parsers [scheme] != null) {
                        /* Parse the URI with a specific parser */

                        return this.parsers [scheme](uri);
                    }

                    /* Try to parse the URI generically */

                    return this.parsers.generic(scheme, uri.substring(pos + 1));
                }

                /* Parse the URI with the HTTP parser, for now... */

                return this.parsers.http(uri);
            }
        };

    /**
     *
     *    Section: Helper interface.
     *
     *    All private helper methods.
     *
     *    This section contains all get/set and filter methods utilised by the public interface.
     *
     **/

    var helpers =
        {
            /**
             *
             *    Function: getUrl
             *
             *    Return the elements URL (stored under its "data-href", and/or "href"/"src" attribute).
             *
             **/

            "getUrl": function () {
                return returnEachObject.apply(this, [getHref, null]);
            },

            /**
             *
             *    Function: setUrl
             *
             *    Set the elements URL (stored under it's "data-href", and/or "href"/"src" attribute). Note: This does not change document.location.href for the $(document) element!
             *
             */

            "setUrl": function (url) {
                setHref.apply(this, [url]);
            },

            /**
             *
             *    Function: parseUrl
             *
             *    Return the URL object for the elements "data-href" attribute value.
             *
             **/

            "parseUrl": function () {
                return returnEachObject.apply(this, [function () {
                    return uri.parse(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: getFragment
             *
             *    Get the fragment object from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the fragment object from.
             *
             *    Returns:
             *
             *    The fragment object. Call .toString() on the object to convert it to a string value.
             *
             **/

            "getFragment": function () {
                return returnEachObject.apply(this, [function () {
                    return getFragmentString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setFragment
             *
             *    Set the fragment string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the fragment string on.
             *
             *    fragment - The new fragment string/object.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setFragment": function (fragment) {
                if (fragment [0] != "#") {
                    fragment = "#" + fragment;
                }

                return returnEachElement.apply(this, [updateHrefShim, ["fragment", fragment]]);
            },

            /**
             *
             *    Function: getQuery
             *
             *    Get the query object from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the query object from.
             *
             *    Returns:
             *
             *    The query object. Call .toString() on the object to convert it to a string value.
             *
             **/

            "getQuery": function () {
                return returnEachObject.apply(this, [function () {
                    return $.extend(getQueryObject(getHref.apply(this)), {toString: queryObjectToString});
                }, null]);
            },

            /**
             *
             *    Function: setQuery
             *
             *    Set the query string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the query object on.
             *
             *    query - The new query string represented as an object.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setQuery": function (query) {
                return returnEachElement.apply(this, [updateQuery, query]);
            },

            /**
             *
             *    Function: getPath
             *
             *    Get the path object from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the path object from.
             *
             *    Returns:
             *
             *    The path object. Call .toString() on the object to convert it to a string value.
             *
             **/

            "getPath": function () {
                return returnEachObject.apply(this, [function () {
                    return $.extend(getPathObject(getHref.apply(this)), {toString: pathObjectToString});
                }, null]);
            },

            /**
             *
             *    Function: setPath
             *
             *    Set the path string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the path string on.
             *
             *    path - The new path string/object.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setPath": function (path) {
                return returnEachElement.apply(this, [updatePath, path]);
            },

            /**
             *
             *    Function: getPort
             *
             *    Get the port string from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the port string from.
             *
             *    Returns:
             *
             *    The port string.
             *
             **/

            "getPort": function () {
                return returnEachObject.apply(this, [function () {
                    return getPortString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setPort
             *
             *    Set the port string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the port string on.
             *
             *    port - The new port string.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setPort": function (port) {
                return returnEachElement.apply(this, [updateHrefShim, ["port", port]]);
            },

            /**
             *
             *    Function: getHost
             *
             *    Get the host string from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the host string from.
             *
             *    Returns:
             *
             *    The host string.
             *
             **/

            "getHost": function () {
                return returnEachObject.apply(this, [function () {
                    return getHostString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setHost
             *
             *    Set the host string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the host string on.
             *
             *    host - The new host string.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setHost": function (host) {
                return returnEachElement.apply(this, [updateHrefShim, ["host", host]]);
            },

            /**
             *
             *    Function: getPassword
             *
             *    Get the password string from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the password string from.
             *
             *    Returns:
             *
             *    The password string.
             *
             **/

            "getPassword": function () {
                return returnEachObject.apply(this, [function () {
                    return getPasswordString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setPassword
             *
             *    Set the password string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the password string on.
             *
             *    password - The new password string.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setPassword": function (password) {
                return returnEachElement.apply(this, [updateHrefShim, ["password", password]]);
            },

            /**
             *
             *    Function: getUser
             *
             *    Get the user string from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the user string from.
             *
             *    Returns:
             *
             *    The user string.
             *
             **/

            "getUser": function () {
                return returnEachObject.apply(this, [function () {
                    return getUserString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setUser
             *
             *    Set the user string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the user string on.
             *
             *    user - The new user string.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setUser": function (user) {
                return returnEachElement.apply(this, [updateHrefShim, ["user", user]]);
            },

            /**
             *
             *    Function: getScheme
             *
             *    Get the scheme string from the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to retrieve the scheme string from.
             *
             *    Returns:
             *
             *    The scheme string.
             *
             **/

            "getScheme": function () {
                return returnEachObject.apply(this, [function () {
                    return getSchemeString(getHref.apply(this));
                }, null]);
            },

            /**
             *
             *    Function: setScheme
             *
             *    Set the scheme string for the elements URL.
             *
             *    Parameters:
             *
             *    this - The element to set the scheme string on.
             *
             *    scheme - The new scheme string.
             *
             *    Returns:
             *
             *    Array of elements that were changed.
             *
             **/

            "setScheme": function (scheme) {
                return returnEachElement.apply(this, [updateHrefShim, ["scheme", scheme]]);
            },

            "filters": {
                /**
                 *
                 *    Function: = (equals)
                 *
                 *    Test if the actual value is equal to the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual and user values are equal.
                 *
                 *    false - The actual and user values are not equal.
                 *
                 **/

                "=": function (actualValue, userValue) {
                    if (actualValue == userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: != (not equals)
                 *
                 *    Test if the actual value is equal to the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual and user values are not equal.
                 *
                 *    false - The actual and user values are equal.
                 *
                 **/

                "!=": function (actualValue, userValue) {
                    if (actualValue != userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: < (less than)
                 *
                 *    Test if the actual value is less than the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual value is less than the user supplied value.
                 *
                 *    false - The actual value is greater than or equal to the user supplied value.
                 *
                 **/

                "<": function (actualValue, userValue) {
                    if (actualValue < userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: > (greater than)
                 *
                 *    Test if the actualValue is greater than the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual value is greater than the user supplied value.
                 *
                 *    false - The actual value is less than or equal to the user supplied value.
                 *
                 **/

                ">": function (actualValue, userValue) {
                    if (actualValue > userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: <= (less than or equal to)
                 *
                 *    Test if the actual value is less than or equal to the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual value is less than or equal to the user supplied value.
                 *
                 *    false - The actual value is greater than the user supplied value.
                 *
                 **/

                "<=": function (actualValue, userValue) {
                    if (actualValue <= userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: >= (greater than or equal to)
                 *
                 *    Test if the actual value is greater than or equal to the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual value is greater than or equal to the user supplied value.
                 *
                 *    false - The actual value is less than the user supplied value.
                 *
                 **/

                ">=": function (actualValue, userValue) {
                    if (actualValue >= userValue) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: *= (contains)
                 *
                 *    Test if the actual value contains the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The actual value contains the user supplied value.
                 *
                 *    false - The actual value does not contain the user supplied value.
                 *
                 **/

                "*=": function (actualValue, userValue) {
                    if (actualValue.indexOf(userValue) != -1) {
                        return true;
                    }

                    return false;
                },

                /**
                 *
                 *    Function: ^= (starts with)
                 *
                 *    Test if the start of the actual value matches the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The start of the actual value matches the user supplied value.
                 *
                 *    false - The start of the actual value does not match the user supplied value.
                 *
                 **/

                "^=": function (actualValue, userValue) {
                    if (actualValue.length >= userValue.length) {
                        if (actualValue.substring(0, userValue.length) == userValue) {
                            return true;
                        }
                    }

                    return false;
                },

                /**
                 *
                 *    Function: $= (ends with)
                 *
                 *    Test if the end of the actual value is the same as the user supplied value.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - User supplied value.
                 *
                 *    Returns:
                 *
                 *    true - The end of the actual value matches the user supplied value.
                 *
                 *    false - The end of the actual value does not match the user supplied value.
                 *
                 **/

                "$=": function (actualValue, userValue) {
                    if (actualValue.length >= userValue.length) {
                        if (actualValue.substring(actualValue.length - userValue.length) == userValue) {
                            return true;
                        }
                    }

                    return false;
                },

                /**
                 *
                 *    Function: regex (regular expression)
                 *
                 *    Test if the actual value matches the user supplied regular expression.
                 *
                 *    Parameters:
                 *
                 *    actualValue - Actual value.
                 *
                 *    userValue - Regular expression to apply.
                 *
                 *    Returns:
                 *
                 *    true - The regular expression matches.
                 *
                 *    false - The regular expression does not match.
                 *
                 **/

                "regex": function (actualValue, userValue) {
                    return actualValue.match(userValue);
                }
            }
        };

    /**
     *
     *    Section: Public interface.
     *
     *    All public methods exposed via the JQuery URL parser plugin interface.
     *
     **/

    var methods =
        {
            /**
             *
             *    Function: url
             *
             *    Get/Set the href string for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    url - If present, specifies the new URL object/string to set. Otherwise the function will get the URL.
             *
             *    Returns:
             *
             *    If a URL was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of element URLs.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL
             *    $(document).jurlp("url");
             *
             *    // Parse all URLs in anchor tags
             *    $("a").jurlp("url");
             *
             *    // Update the working URL for the document
             *    $(document).jurlp("url", "http://www.google.com");
             *
             *    // Replace all anchor tags with the google URL!
             *    $("a").jurlp("url", "http://www.google.com");
             *
             *    // Output the documents URL object
             *    console.log($(document).jurlp("url"));
             *
             *    // Output the documents URL string
             *    console.log($(document).jurlp("url").toString());
             *
             *    (end code)
             *
             **/

            "url": function (url) {
                return dispatchGetSetHelper.apply(this, [helpers.parseUrl, helpers.setUrl, arguments]);
            },

            /**
             *
             *    Function: fragment
             *
             *    Get/Set the fragment segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    fragment - If present, specifies the new fragment string to set. Otherwise the function will get the fragment string from each elements URL.
             *
             *    Returns:
             *
             *    If a fragment string was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of URL fragments from each element.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the fragment string
             *    $(document).jurlp("fragment");
             *
             *    // Parse all URLs in anchor tags and retrieve their fragment strings
             *    $("a").jurlp("fragment");
             *
             *    // Set a new fragment for the document
             *    $(document).jurlp("fragment", "elementid");
             *
             *    // Replace the fragment string in all anchor tags with the new element ID
             *    $("a").jurlp("fragment", "elementid");
             *
             *    // Output the documents URL fragment
             *    console.log($(document).jurlp("fragment"));
             *
             *    (end code)
             *
             **/

            "fragment": function (fragment) {
                return dispatchGetSetHelper.apply(this, [helpers.getFragment, helpers.setFragment, arguments]);
            },

            /**
             *
             *    Function: query
             *
             *    Get/Set the query segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    query - If present, specifies the new query object to set. Otherwise the function will get the query object from each elements URL.
             *
             *    Returns:
             *
             *    If a query object was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of URL query objects from each element. Each returned query object can be converted to a string by calling its toString() method.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the query object
             *    $(document).jurlp("query");
             *
             *    // Parse all URLs in anchor tags and retrieve their query object
             *    $("a").jurlp("query");
             *
             *    // Set/update the "new" parameter in the query string for the document
             *    $(document).jurlp("query", {"new":"parameter"});
             *
             *    // Remove the query string for the document
             *    $(document).jurlp("query", {});
             *
             *    // Update the query string in all anchor tags with the new query object.
             *    $("a").jurlp("query", {"new":"parameter"});
             *
             *    // Remove the query string in all anchor tags.
             *    $("a").jurlp("query", {});
             *
             *    // Output the documents URL query object
             *    console.log($(document).jurlp("query"));
             *
             *    // Output the documents URL query string
             *    console.log($(document).jurlp("query").toString());
             *
             *    (end code)
             *
             **/

            "query": function (query) {
                return dispatchGetSetHelper.apply(this, [helpers.getQuery, helpers.setQuery, arguments]);
            },

            /**
             *
             *    Function: path
             *
             *    Get/Set the path segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    path - If present, specifies the new path to set. Otherwise the function will get the path object from each elements URL.
             *
             *    A quick guide to paths:
             *
             *    - Leading slashes (i.e. "/index.html") set the full path.
             *
             *    - No leading slash (or a "./") will append to the existing path.
             *
             *    - You can use "../" to remove elements from the existing path, or the path string you supply (which makes concatinating an existing file path and new path easy, as specifying a leading "../" in the new path will remove the file name segment of the existing path).
             *
             *    Returns:
             *
             *    If a path was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of URL path objects from each element. Each returned path object can be converted to a string by calling its toString() method.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the path object
             *    $(document).jurlp("path");
             *
             *    // Parse all URLs in anchor tags and retrieve their path object
             *    $("a").jurlp("path");
             *
             *    // Set a new path for the document
             *    $(document).jurlp("path", "/index.html");
             *
             *    // Append a path to the document URLs path
             *    $(document).jurlp("path", "./file.name");
             *
             *    // Append a path to the document URLs path, which removes 2 existing path
             *    // elements before appending the new path
             *    $(document).jurlp("path", "../../folder/file.name");
             *
             *    // Update the file name segment of the path in all anchor tags
             *    // with the new file name.
             *    $("a").jurlp("path", "../file.name");
             *
             *    // Remove the path in all anchor tags.
             *    $("a").jurlp("path", "/");
             *
             *    // Output the documents URL path object
             *    console.log($(document).jurlp("path"));
             *
             *    // Output the documents URL path string
             *    console.log($(document).jurlp("path").toString());
             *
             *    (end code)
             *
             **/

            "path": function (path) {
                return dispatchGetSetHelper.apply(this, [helpers.getPath, helpers.setPath, arguments]);
            },

            /**
             *
             *    Function: port
             *
             *    Get/Set the port segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    port - If present, specifies the new port to set. Otherwise the function will get the port string from each elements URL.
             *
             *    Returns:
             *
             *    If a port was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of port strings from each elements URL.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the port
             *    $(document).jurlp("port");
             *
             *    // Parse all URLs in anchor tags and retrieve their port
             *    $("a").jurlp("port");
             *
             *    // Set a new port for the document
             *    $(document).jurlp("port", "8080");
             *
             *    // Replace the port in all anchor tags with the new port number
             *    $("a").jurlp("port", "8080");
             *
             *    // Output the documents URL port
             *    console.log($(document).jurlp("port"));
             *
             *    (end code)
             *
             **/

            "port": function (port) {
                return dispatchGetSetHelper.apply(this, [helpers.getPort, helpers.setPort, arguments]);
            },

            /**
             *
             *    Function: host
             *
             *    Get/Set the host segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    host - If present, specifies the new host name to set. Otherwise the function will get the host name string from each elements URL.
             *
             *    Returns:
             *
             *    If a host name was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of host name strings from each elements URL.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the host name
             *    $(document).jurlp("host");
             *
             *    // Parse all URLs in anchor tags and retrieve their host name
             *    $("a").jurlp("host");
             *
             *    // Set a new host name for the document
             *    $(document).jurlp("host", "www.example.com");
             *
             *    // Replace the host name in all anchor tags with the new host name
             *    $("a").jurlp("host", "www.example.com");
             *
             *    // Output the documents URL host name
             *    console.log($(document).jurlp("host"));
             *
             *    (end code)
             *
             **/

            "host": function (host) {
                return dispatchGetSetHelper.apply(this, [helpers.getHost, helpers.setHost, arguments]);
            },

            /**
             *
             *    Function: password
             *
             *    Get/Set the password segment of the URL for the given element(s).
             *
             *    Note! A password cannot be set on a URL unless a user name has been set first (see <user>).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    password - If present, specifies the new password to set. Otherwise the function will get the password string from each elements URL.
             *
             *    Returns:
             *
             *    If a password was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of password strings from each elements URL.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse all URLs in anchor tags and retrieve their password
             *    $("a").jurlp("password");
             *
             *    // Replace the password in all anchor tags with the new password string
             *    $("a").jurlp("password", "newpassword");
             *
             *    (end code)
             *
             **/

            "password": function (password) {
                return dispatchGetSetHelper.apply(this, [helpers.getPassword, helpers.setPassword, arguments]);
            },

            /**
             *
             *    Function: user
             *
             *    Get/Set the user segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    user - If present, specifies the new username to set. Otherwise the function will get the username string from each elements URL.
             *
             *    Returns:
             *
             *    If a username was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of username strings from each elements URL.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse all URLs in anchor tags and retrieve their username
             *    $("a").jurlp("user");
             *
             *    // Replace the username in all anchor tags with the new username string
             *    $("a").jurlp("username", "newusername");
             *
             *    (end code)
             *
             **/

            "user": function (user) {
                return dispatchGetSetHelper.apply(this, [helpers.getUser, helpers.setUser, arguments]);
            },

            /**
             *
             *    Function: scheme
             *
             *    Get/Set the scheme segment of the URL for the given element(s).
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    scheme - If present, specifies the new scheme. Otherwise the function will get the scheme string from each elements URL.
             *
             *    Returns:
             *
             *    If a scheme string was specified, then this function returns the array of modified elements for chaining purposes, otherwise it returns an array of scheme strings from each elements URL.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Parse the document.location.href URL for the scheme
             *    $(document).jurlp("scheme");
             *
             *    // Parse all URLs in anchor tags and retrieve their scheme
             *    $("a").jurlp("scheme");
             *
             *    // Set a new scheme name for the document
             *    $(document).jurlp("scheme", "https://");
             *
             *    // Replace the scheme in all anchor tags href attributes
             *    $("a").jurlp("scheme", "https://");
             *
             *    // Output the documents URL host name
             *    console.log($(document).jurlp("scheme"));
             *
             *    (end code)
             *
             **/

            "scheme": function (scheme) {
                return dispatchGetSetHelper.apply(this, [helpers.getScheme, helpers.setScheme, arguments]);
            },

            /**
             *
             *    Function: initialise
             *
             *    Initialise the parser for the given element(s). HTML anchor elements or the HTML document element need not be explicitly initialised.
             *
             *    Elements are initialised as follows;
             *
             *    $(document) - Initialise the "data-href" attribute for the document with the value of "document.location.href". The "data-href" attribute will be modified instead of "document.location.href" when modifying this element. See <Parsing document.location.href>.
             *
             *    Elements with "href"/"src" attributes - An attribute named "data-original-href" or "data-original-src" is created to store a copy of the elements original "href"/"src" attribute at the time of initialisation. See <Parsing elements with an href or src attribute>.
             *
             *    All other elements - Parses the element HTML for URLs, wraps any URLs found in an anchor tag, and returns all anchor elements.
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    Returns:
             *
             *    Array of initialised elements (minus the parent container element).
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Not necessary
             *    $(document).jurlp();
             *
             *    // Not necessary
             *    $("a").jurlp();
             *
             *    // Parse the HTML for URLs, and convert all URLs found in the
             *    // text to anchors tags, and return the anchor elements.
             *    $("<div>www.example.com</div>").jurlp();
             *
             *    (end code)
             *
             **/

            "initialise": function () {
                var me = this;

                /* Attempt to initialise the element as a text field. */

                var elements = [];

                elements = initialiseElementText.apply($(this));

                if (elements != null) {
                    /* Initialise and return all created anchor elements, and remove the parent element from the array */

                    return returnEachElement.apply(this.filter(function () {
                        return $(this).get(0) != $(me).get(0);
                    }).add(elements), [initialiseElement]);
                }

                /* Initialise the element directly */

                return returnEachElement.apply(me, [initialiseElement]);
            },

            /**
             *
             *    Function: restore
             *
             *    Removes any parser data associated with the element(s), and sets the href attribute to its original value.
             *
             *    $(document) - Removes the "data-href" attribute.
             *
             *    Elements with "href"/"src" - Restores the "href"/"src" attribute to the "data-original-href/src" attribute value, and removes any other added attributes.
             *
             *    All other elements - Currently there is no way to restore an elements HTML which has been converted by the parser, so consider saving it first if needed!
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    Returns:
             *
             *    Array of elements which were restored for chaining purposes.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Restore the working URL for the document.
             *    $(document).jurlp("restore");
             *
             *    // Restore the URL for all anchor elements.
             *    $("a").jurlp("restore");
             *
             *    (end code)
             *
             **/

            "restore": function () {
                return returnEachElement.apply(this, [restoreElement]);
            },

            /**
             *
             *    Function: goto
             *
             *    Set document.location.href to the supplied elements "href", "src" or "data-href" attribute value.
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Goto the documents URL.
             *    $(document).jurlp("goto");
             *
             *    (end code)
             *
             **/

            "goto": function () {
                document.location.href = getHref.apply(this);
            },

            /**
             *
             *    Function: proxy
             *
             *    Proxy the URL. The elements URL will be replaced with the proxy URL, and the original URL will be encapsulated under the query string using the parameter name specified.
             *
             *    Parameters:
             *
             *    this - See <this parameter>.
             *
             *    url - The proxy URL.
             *
             *    parameter - The name of the query string parameter to encapsulate the original URL in.
             *
             *    Returns:
             *
             *    Array of modified elements for chaining purposes.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Proxy all URLs in anchor tags to www.example.com
             *    // URL becomes "http://www.example.com/?url=<original URL>"
             *    $("a").jurlp("proxy", "http://www.example.com", "url");
             *
             *    (end code)
             *
             **/

            "proxy": function (url, parameter) {
                var elementUrl = getHref.apply(this);
                var query = {};

                setHref.apply(this, [objectToUrl(getUrlObject(url))]);

                query [parameter] = elementUrl;

                helpers.setQuery.apply(this, [query]);
            },

            /**
             *
             *    function: watch
             *
             *    Automatically apply all modifications to new elements added to the DOM that match the selector for the supplied elements. This allows URL filters/modifications that have been applied to existing elements to be propogated to new elements if the page content is being modified (i.e. inserting new anchor tags via AJAX).
             *
             *    Overview:
             *
             *    Watch will monitor the selector of the supplied elements via a DOM node listener to detect when new elements are inserted. For each new element that is inserted, any prior filters or modifications made to URLs with the same selector will be applied, and the watcher will be alerted via a callback.
             *
             *    Note! It is not possible to call watch more than once for the same selector. To do this, try naming the selector differently, i.e. instead of "a", use "a:not(uniqueid)", where "uniqueid" is a nice lengthy descriptive name!
             *
             *    To stop watching for updates on a selecter, use <unwatch>.
             *
             *    Parameters:
             *
             *    this - Array of elements to obtain the selector from. See <this parameter>.
             *
             *    callback - Function to call when elements are found, which is supplied two arguments, the new element that was inserted into the DOM, and the selector that triggered the watch.
             *
             *    Returns:
             *
             *    Array of unmodified elements for chaining purposes.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Add a watch on the first anchor element, and if the host name is
             *    // "www.example.com", set the URL query string to "found=example".
             *    // The "filter" and "query" calls will also be applied to all new elements that
             *    // watch discovers!..
             *    $("a:eq(0)").jurlp("watch", function(element, selector){
             *		// If we get here, the first anchor element has changed to a URL containing
             *		// "www.example.com" and now contains "found=example" in the query string.
             *		// Dump the URL object to prove it!
             *		console.log($(element).jurlp("url"));
             *	}).jurlp("filter", "host", "=", "www.example.com")
             *      .jurlp("query",{"found":"example"});
             *
             *    //    Prepend a new anchor tag to the page. This will trigger the watch on the
             *    //    "a:eq(0)" selector, which will apply all prior calls to this selector,
             *    //    so in this instance:
             *    //    - First perform the filter host, to ensure the host name is "www.example.com".
             *    //    - If the host name matches, update the URL query string with "found=example").
             *    // If the host name does not match, then the query string will not be set.
             *
             *    $("body").prepend ( "<a href='http://www.example.com/'></a>" );
             *
             *    // Stop watching for updates on the "a:eq(0)" selector.
             *    // The "a:eq(0)" selector can now be watched on again.
             *    $("a:eq(0)").jurlp("unwatch");
             *
             *    (end code)
             *
             *    - Watching the "same selector":
             *
             *    (start code)
             *
             *    // As an arbitrary example, we want to modify the query string on all existing
             *    // facebook/twitter URLs, and then watch the "a" selector for all new
             *    // facebook/twitter URLs that appear, and apply the new query string to those too:
             *
             *    // THIS WILL NOT WORK!:
             *    $("a").jurlp("watch").
             *          .jurlp("filter", "host", "=", "www.facebook.com").
             *          .jurlp("query",{"found":"facebook"});
             *
             *    // This call will fail, as the "a" selector is now watched.
             *    $("a").jurlp("watch").
             *          .jurlp("filter", "host", "=", "www.twitter.com")
             *          .jurlp("query",{"found":"twitter"});
             *
             *    // THIS WILL WORK!:
             *    $("a:not(facebook)").jurlp("watch").
             *                        .jurlp("filter", "host", "=", "www.facebook.com")
             *                        .jurlp("query",{"found":"facebook"});
             *
             *    $("a:not(twitter)").jurlp("watch")
             *                       .jurlp("filter", "host", "=", "www.twitter.com")
             *                       .jurlp("query",{"found":"twitter"});
             *
             *    (end code)
             *
             **/

            "watch": function (callback) {
                /* Get the current selector */

                var selector = this.selector;

                /* Has this selector been initialised? */

                if (selectorCallStack [currentSelector] == null) {
                    /* Initialise the selector callstack */

                    selectorCallStack [currentSelector] = [];

                    /* Monitor the DOM for new nodes being inserted */

                    $(document).bind
                    (
                        "DOMNodeInserted",
                        function DOMListener(event) {
                            /* Has the selector been unwatched? */

                            if (selectorCallStack [selector] == null) {
                                /* Remove the DOM listener for the specific selector */

                                $(document).unbind("DOMNodeInserted", DOMListener);

                                return;
                            }

                            /* Does this element belong to the current selector? */

                            var target = $(event.target).filter(selector);

                            if (target.get(0) == null) {
                                /* Or is it a child */

                                target = $(event.target).find(selector);
                            }

                            /* Ensure we have a target to modify, and that we are allowed to watch it (the ".jurlp-no-watch" class is present on elements created in initialiseElementText()). */

                            if (target.length > 0 && target.is(".jurlp-no-watch") == false) {
                                var filtered = false;

                                /* Mark the element as being watched */

                                target.data("is-watched", true);

                                /* Apply the selector callstack for this element */

                                for (var i = 0, l = selectorCallStack [selector].length; i < l; i++) {
                                    /* Run the selector callback to update the element */

                                    var output = selectorCallStack [selector] [i] [0].apply(target, selectorCallStack [selector] [i] [1]);

                                    /* Has the element been filtered out? */

                                    if (output != null && output.length == 0) {
                                        /* This element has been filtered, perform no further modifications */

                                        filtered = true;

                                        break;
                                    }
                                }

                                /* Was the element not filtered out, and a user callback specified? */

                                if (filtered == false && typeof callback == "function") {
                                    /* Call the user callback for each element found in the watch */

                                    target.each(function () {
                                        callback($(this), selector)
                                    });
                                }
                            }
                        }
                    );
                }

                return this;
            },

            /**
             *
             *    Function: unwatch
             *
             *    Removes a watch previously created with <watch>, and prevents modifications being made to new elemenets of the same selector. This will also clear the list of modifications for the selector, and the selector is free to use in a sebsequent call to <watch>.
             *
             *    Parameters:
             *
             *    this - Array of elements to obtain the selector from.
             *
             *    Returns:
             *
             *    Array of unmodified elements for chaining purposes.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Create a watch on the "a" selector
             *    $("a").jurlp("watch");
             *
             *    // Remove the watch on the "a" selector
             *    $("a").jurlp("unwatch");
             *
             *    (end code)
             *
             **/

            "unwatch": function () {
                selectorCallStack [this.selector] = null;
            },

            /**
             *
             *    Function: filter
             *
             *    Filters elements by URL or URL segment.
             *
             *    Parameters:
             *
             *    this - Array of elements to filter. See <this parameter>.
             *
             *    segment - The URL segment to filter on (either "scheme", "host", "port", "path", "query" or "fragment"), or "url" to filter on the full URL. See <URL overview> for more information.
             *
             *    operator - The type of filtering to apply (either "!=", "$=", "*=", "<", "<=", "=", ">", ">=" or "^="). See <Operators> for more information.
             *
             *    value - The value of the item to filter on.
             *
             *    Returns:
             *
             *    Filtered element array for chaining purposes.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Get the URL object for all anchors that match
             *    // the document URLs host name.
             *    $("a").jurlp("filter", "host", "=", $(document).jurlp("host"))
             *          .jurlp("url");
             *
             *    // Get the URL object for all anchors that match
             *    // the document URLs host, and has a path ending in ".php".
             *    $("a").jurlp("filter", "host", "=", $(document).jurlp("host"))
             *          .jurlp("filter", "path", "$=", ".php")
             *          .jurlp("url");
             *
             *    // Get the URL object for all anchors whose query
             *    // string matches the regular expression
             *    $("a").jurlp("filter", "query", "regex", /(\?)/).jurlp("url");
             *
             *    (end code)
             *
             **/

            "filter": function (segment, operator, value) {
                /* Get an arbitrary URL object */

                var url = uri.parse(getHref.apply(this));

                /* Ensure operator is valid */

                if (operator == "==") {
                    operator = "=";
                }

                /* Ensure that the segment is valid, and that a filter method exists */

                if (( segment == "url" || url [segment] != null ) && helpers.filters [operator] != null) {
                    /* Add the filter to the selectors callstack if watched */

                    addSelectorCallback(this, methods.filter, [segment, operator, value]);

                    /* Filter each element */

                    return this.filter
                    (
                        function () {
                            /* Get the current elements href */

                            var url = getHref.apply($(this));

                            var actualValue = "";

                            /* Get the segment value */

                            if (segment != "url") {
                                /* Use a segment of the URL */

                                actualValue = uri.parse(url) [segment];
                            }
                            else {
                                /* Use the full URL */

                                actualValue = url;
                            }

                            /* If we are testing port numbers, then convert the actual and user port string to an integer */

                            if (segment == "port") {
                                actualValue = parseInt(actualValue, 10);

                                value = parseInt(value, 10);
                            }
                            /* Perform the filter check */

                            return helpers.filters [operator].apply($(this), [actualValue, value]);
                        }
                    );
                }

                return this;
            },

            /**
             *
             *    Function: interface
             *
             *    Get the available methods for the parser interface.
             *
             *    Returns:
             *
             *    Array of interface methods.
             *
             *    Examples:
             *
             *    (start code)
             *
             *    // Return all methods exposed by the URL parser interface.
             *    $.fn.jurlp ( "interface" );
             *
             *    (end code)
             *
             **/

            "interface": function () {
                return methods;
            }
        };

    /**
     *
     *    Section: JQuery plugin interface.
     *
     *    Function: $.fn.jurlp
     *
     *    Public interface/method dispatcher for the JQuery URL parser.
     *
     *    See <Public interface> for more information on the available methods.
     *
     *    See <initialise> for more specific information on how elements are initialised by the parser.
     *
     *    Parameters:
     *
     *    this - Element(s) to process. See <this parameter>.
     *
     *    method - See <Public interface> for an overview of available methods and arguments.
     *
     *    arguments - Method arguments.
     *
     *    Returns:
     *
     *    Either an array of elements for chaining purposes, or array of specific values, depending on the method called.
     *
     **/

    $.fn.jurlp = function (method) {
        /* If the current selector isn't part of a filter... */

        if (this.selector.indexOf(".filter") == -1) {
            /* Set the global current selector */

            currentSelector = this.selector;
        }

        /* Ensure all elements are initialised with a "data-href". */

        returnEachElement.apply(this, [initialiseElement]);

        /* Dispatch to the relevant method */

        return methodDispatcher.apply(this, arguments);
    };

    /**
     *
     *
     *    Function: $.jurlp
     *
     *    Returns an interface for directly parsing, manipulating and monitoring the supplied URL.
     *
     *    Parameters:
     *
     *    url - The URL string to provide a URL parser interface for. Defaults to document.location.href if no URL is supplied.
     *
     *    Returns:
     *
     *    The URL parser interface for the given URL.
     *
     *    Members:
     *
     *    href - The URL string.
     *
     *    Methods:
     *
     *    url - See <url>.
     *
     *    scheme - See <scheme>.
     *
     *    user - See <user>.
     *
     *    password - See <password>.
     *
     *    host - See <host>.
     *
     *    port - See <port>.
     *
     *    path - See <path>.
     *
     *    query - See <query>.
     *
     *    fragment - See <fragment>.
     *
     *    proxy - See <proxy>.
     *
     *    goto - See <goto>.
     *
     *    watch - Sets a watch for all "href" and "src" attributes containing the URLs hostname (selector is "[href*="host"],[src*="host"]" where host is this.host()), and returns all elements of the same selector for chaining purposes. See <watch> for more information.
     *
     *    unwatch - Removes a watch created for the current URLs hostname. See <unwatch>.
     *
     *    Examples:
     *
     *    See <Parsing URL strings directly>.
     *
     **/

    $.jurlp = function (url) {
        /* Create an object for manipulating the url, or document.location.href if null. */

        return {
            href: url || document.location.href,
            url: methods.url,
            scheme: methods.scheme,
            user: methods.user,
            password: methods.password,
            host: methods.host,
            port: methods.port,
            path: methods.path,
            query: methods.query,
            fragment: methods.fragment,
            proxy: methods.proxy,
            "goto": methods ["goto"],
            watch: function (callback) {
                /* Get the current host name */

                var host = this.host();

                /* Set a watch on the href or src selectors */

                return $("[href*=\"" + host + "\"],[src*=\"" + host + "\"]").jurlp("watch", callback);
            },
            unwatch: function () {
                /* Get the current host name */

                var host = this.host();

                /* Set a watch on the href or src selectors */

                return $("[href*=\"" + host + "\"],[src*=\"" + host + "\"]").jurlp("unwatch");
            }
        };
    };
})(jQuery, KTJS);
(function ($, $$, W) {

    "use strict";

    /**
     *  extend/validator.js
     */

    // ToDO
    // ...

    $.fn.extend({
        validator: function (_args) {

            var _param = (!_args) ? {} : _args,
                _arg = {
                    errorElement: "em",
                    errorPlacement: function (error, element) {
                        // Add the `help-block` class to the error element
                        error.addClass("help-block");
                        // Add `has-feedback` class to the parent div.form-group
                        // in order to add icons to inputs
                        element.parents(".form-group").addClass("has-feedback");
                        if (element.prop("type") === "checkbox") {
                            error.insertAfter(element.parent("label"));
                        } else {
                            error.insertAfter(element);
                        }
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!element.next("span")[0]) {
                            $("<span class='fa fa-times form-control-feedback'></span>").insertAfter(element);
                        }
                    },
                    success: function (label, element) {
                        // Add the span element, if doesn't exists, and apply the icon classes to it.
                        if (!$(element).next("span")[0]) {
                            $("<span class='fa fa-check form-control-feedback'></span>").insertAfter($(element));
                        }
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).closest(".form-group").addClass("has-error has-feedback").removeClass("has-success");
                        $(element).next("span").addClass("fa-times").removeClass("fa-check");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).closest(".form-group").addClass("has-success has-feedback").removeClass("has-error");
                        $(element).next("span").addClass("fa-check").removeClass("fa-times");
                    }
                };

            $.extend(true, _arg, _param);

            return $(this).validate(_arg);
        }
    });

})(jQuery, KTJS, window);

/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */
;(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined'
    && typeof require === 'function' ? factory(require('../moment')) :
        typeof define === 'function' && define.amd ? define(['../moment'], factory) :
            factory(global.moment)
}(this, (function (moment) { 'use strict';


    var fr = moment.defineLocale('fr', {
        months : 'janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre'.split('_'),
        monthsShort : 'janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.'.split('_'),
        monthsParseExact : true,
        weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
        weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
        weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
        weekdaysParseExact : true,
        longDateFormat : {
            LT : 'HH:mm',
            LTS : 'HH:mm:ss',
            L : 'DD/MM/YYYY',
            LL : 'D MMMM YYYY',
            LLL : 'D MMMM YYYY HH:mm',
            LLLL : 'dddd D MMMM YYYY HH:mm'
        },
        calendar : {
            sameDay : '[Aujourd’hui à] LT',
            nextDay : '[Demain à] LT',
            nextWeek : 'dddd [à] LT',
            lastDay : '[Hier à] LT',
            lastWeek : 'dddd [dernier à] LT',
            sameElse : 'L'
        },
        relativeTime : {
            future : 'dans %s',
            past : 'il y a %s',
            s : 'quelques secondes',
            m : 'une minute',
            mm : '%d minutes',
            h : 'une heure',
            hh : '%d heures',
            d : 'un jour',
            dd : '%d jours',
            M : 'un mois',
            MM : '%d mois',
            y : 'un an',
            yy : '%d ans'
        },
        dayOfMonthOrdinalParse: /\d{1,2}(er|)/,
        ordinal : function (number, period) {
            switch (period) {
                // TODO: Return 'e' when day of month > 1. Move this case inside
                // block for masculine words below.
                // See https://github.com/moment/moment/issues/3375
                case 'D':
                    return number + (number === 1 ? 'er' : '');

                // Words with masculine grammatical gender: mois, trimestre, jour
                default:
                case 'M':
                case 'Q':
                case 'DDD':
                case 'd':
                    return number + (number === 1 ? 'er' : 'e');

                // Words with feminine grammatical gender: semaine
                case 'w':
                case 'W':
                    return number + (number === 1 ? 're' : 'e');
            }
        },
        week : {
            dow : 1, // Monday is the first day of the week.
            doy : 4  // The week that contains Jan 4th is the first week of the year.
        }
    });

    return fr;

})));
!function($){"use strict";function a(a){for(var b=window,c=a.split("."),d=c.pop(),e=0,f=c.length;e<f;e++)b=b[c[e]];return function(){b[d].call(this)}}var b;if(!$.fn.popover)throw new Error("Confirmation requires popover.js");var c=function(a,b){b.trigger="click",this.init(a,b)};c.VERSION="2.4.0",c.KEYMAP={13:"Enter",27:"Escape",39:"ArrowRight",40:"ArrowDown"},c.DEFAULTS=$.extend({},$.fn.popover.Constructor.DEFAULTS,{placement:"top",title:"Are you sure?",popout:!1,singleton:!1,copyAttributes:"href target",buttons:null,onConfirm:$.noop,onCancel:$.noop,btnOkClass:"btn-xs btn-primary",btnOkIcon:"glyphicon glyphicon-ok",btnOkLabel:"Yes",btnCancelClass:"btn-xs btn-default",btnCancelIcon:"glyphicon glyphicon-remove",btnCancelLabel:"No",template:'<div class="popover confirmation"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"><p class="confirmation-content"></p><div class="confirmation-buttons text-center"><div class="btn-group"><a href="#" class="btn" data-apply="confirmation"></a><a href="#" class="btn" data-dismiss="confirmation"></a></div></div></div></div>'}),c.prototype=$.extend({},$.fn.popover.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.init=function(a,b){if($.fn.popover.Constructor.prototype.init.call(this,"confirmation",a,b),(this.options.popout||this.options.singleton)&&!b.rootSelector)throw new Error("The rootSelector option is required to use popout and singleton features since jQuery 3.");this.options._isDelegate=!1,b.selector?this.options._selector=this._options._selector=b.rootSelector+" "+b.selector:b._selector?(this.options._selector=b._selector,this.options._isDelegate=!0):this.options._selector=b.rootSelector;var c=this;this.options.selector?this.$element.on(this.options.trigger,this.options.selector,function(a,b){b||(a.preventDefault(),a.stopPropagation(),a.stopImmediatePropagation())}):(this.options._attributes={},this.options.copyAttributes?"string"==typeof this.options.copyAttributes&&(this.options.copyAttributes=this.options.copyAttributes.split(" ")):this.options.copyAttributes=[],this.options.copyAttributes.forEach(function(a){this.options._attributes[a]=this.$element.attr(a)},this),this.$element.on(this.options.trigger,function(a,b){b||(a.preventDefault(),a.stopPropagation(),a.stopImmediatePropagation())}),this.$element.on("show.bs.confirmation",function(a){c.options.singleton&&$(c.options._selector).not($(this)).filter(function(){return void 0!==$(this).data("bs.confirmation")}).confirmation("hide")})),this.options._isDelegate||(this.eventBody=!1,this.uid=this.$element[0].id||this.getUID("group_"),this.$element.on("shown.bs.confirmation",function(a){c.options.popout&&!c.eventBody&&(c.eventBody=$("body").on("click.bs.confirmation."+c.uid,function(a){$(c.options._selector).is(a.target)||($(c.options._selector).filter(function(){return void 0!==$(this).data("bs.confirmation")}).confirmation("hide"),$("body").off("click.bs."+c.uid),c.eventBody=!1)}))}))},c.prototype.hasContent=function(){return!0},c.prototype.setContent=function(){var a=this,c=this.tip(),d=this.getTitle(),e=this.getContent();if(c.find(".popover-title")[this.options.html?"html":"text"](d),c.find(".confirmation-content").toggle(!!e).children().detach().end()[this.options.html?"string"==typeof e?"html":"append":"text"](e),c.on("click",function(a){a.stopPropagation()}),this.options.buttons){var f=c.find(".confirmation-buttons .btn-group").empty();this.options.buttons.forEach(function(b){f.append($('<a href="#"></a>').addClass(b["class"]||"btn btn-xs btn-default").html(b.label||"").attr(b.attr||{}).prepend($("<i></i>").addClass(b.icon)," ").one("click",function(c){"#"===$(this).attr("href")&&c.preventDefault(),b.onClick&&b.onClick.call(a.$element),b.cancel?(a.getOnCancel.call(a).call(a.$element),a.$element.trigger("canceled.bs.confirmation")):(a.getOnConfirm.call(a).call(a.$element),a.$element.trigger("confirmed.bs.confirmation")),a.inState&&(a.inState.click=!1),a.hide()}))},this)}else c.find('[data-apply="confirmation"]').addClass(this.options.btnOkClass).html(this.options.btnOkLabel).attr(this.options._attributes).prepend($("<i></i>").addClass(this.options.btnOkIcon)," ").off("click").one("click",function(b){"#"===$(this).attr("href")&&b.preventDefault(),a.getOnConfirm.call(a).call(a.$element),a.$element.trigger("confirmed.bs.confirmation"),a.$element.trigger(a.options.trigger,[!0]),a.hide()}),c.find('[data-dismiss="confirmation"]').addClass(this.options.btnCancelClass).html(this.options.btnCancelLabel).prepend($("<i></i>").addClass(this.options.btnCancelIcon)," ").off("click").one("click",function(b){b.preventDefault(),a.getOnCancel.call(a).call(a.$element),a.$element.trigger("canceled.bs.confirmation"),a.inState&&(a.inState.click=!1),a.hide()});c.removeClass("fade top bottom left right in"),c.find(".popover-title").html()||c.find(".popover-title").hide(),b=this,$(window).off("keyup.bs.confirmation").on("keyup.bs.confirmation",this._onKeyup.bind(this))},c.prototype.destroy=function(){b===this&&(b=void 0,$(window).off("keyup.bs.confirmation")),$.fn.popover.Constructor.prototype.destroy.call(this)},c.prototype.hide=function(){b===this&&(b=void 0,$(window).off("keyup.bs.confirmation")),$.fn.popover.Constructor.prototype.hide.call(this)},c.prototype._onKeyup=function(a){if(!this.$tip)return b=void 0,void $(window).off("keyup.bs.confirmation");var d,e=a.key||c.KEYMAP[a.keyCode||a.which],f=this.$tip.find(".confirmation-buttons .btn-group"),g=f.find(".active");switch(e){case"Escape":this.hide();break;case"ArrowRight":d=g.length&&g.next().length?g.next():f.children().first(),g.removeClass("active"),d.addClass("active").focus();break;case"ArrowLeft":d=g.length&&g.prev().length?g.prev():f.children().last(),g.removeClass("active"),d.addClass("active").focus()}},c.prototype.getOnConfirm=function(){return this.$element.attr("data-on-confirm")?a(this.$element.attr("data-on-confirm")):this.options.onConfirm},c.prototype.getOnCancel=function(){return this.$element.attr("data-on-cancel")?a(this.$element.attr("data-on-cancel")):this.options.onCancel};var d=$.fn.confirmation;$.fn.confirmation=function(a){var b="object"==typeof a&&a||{};return b.rootSelector=this.selector||b.rootSelector,this.each(function(){var d=$(this),e=d.data("bs.confirmation");(e||"destroy"!=a)&&(e||d.data("bs.confirmation",e=new c(this,b)),"string"==typeof a&&(e[a](),"hide"==a&&e.inState&&(e.inState.click=!1)))})},$.fn.confirmation.Constructor=c,$.fn.confirmation.noConflict=function(){return $.fn.confirmation=d,this}}(jQuery);
/*! jQuery Validation Plugin - v1.17.0 - 7/29/2017
 * https://jqueryvalidation.org/
 * Copyright (c) 2017 Jörn Zaefferer; Licensed MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):"object"==typeof module&&module.exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){a.extend(a.fn,{validate:function(b){if(!this.length)return void(b&&b.debug&&window.console&&console.warn("Nothing selected, can't validate, returning nothing."));var c=a.data(this[0],"validator");return c?c:(this.attr("novalidate","novalidate"),c=new a.validator(b,this[0]),a.data(this[0],"validator",c),c.settings.onsubmit&&(this.on("click.validate",":submit",function(b){c.submitButton=b.currentTarget,a(this).hasClass("cancel")&&(c.cancelSubmit=!0),void 0!==a(this).attr("formnovalidate")&&(c.cancelSubmit=!0)}),this.on("submit.validate",function(b){function d(){var d,e;return c.submitButton&&(c.settings.submitHandler||c.formSubmitted)&&(d=a("<input type='hidden'/>").attr("name",c.submitButton.name).val(a(c.submitButton).val()).appendTo(c.currentForm)),!c.settings.submitHandler||(e=c.settings.submitHandler.call(c,c.currentForm,b),d&&d.remove(),void 0!==e&&e)}return c.settings.debug&&b.preventDefault(),c.cancelSubmit?(c.cancelSubmit=!1,d()):c.form()?c.pendingRequest?(c.formSubmitted=!0,!1):d():(c.focusInvalid(),!1)})),c)},valid:function(){var b,c,d;return a(this[0]).is("form")?b=this.validate().form():(d=[],b=!0,c=a(this[0].form).validate(),this.each(function(){b=c.element(this)&&b,b||(d=d.concat(c.errorList))}),c.errorList=d),b},rules:function(b,c){var d,e,f,g,h,i,j=this[0];if(null!=j&&(!j.form&&j.hasAttribute("contenteditable")&&(j.form=this.closest("form")[0],j.name=this.attr("name")),null!=j.form)){if(b)switch(d=a.data(j.form,"validator").settings,e=d.rules,f=a.validator.staticRules(j),b){case"add":a.extend(f,a.validator.normalizeRule(c)),delete f.messages,e[j.name]=f,c.messages&&(d.messages[j.name]=a.extend(d.messages[j.name],c.messages));break;case"remove":return c?(i={},a.each(c.split(/\s/),function(a,b){i[b]=f[b],delete f[b]}),i):(delete e[j.name],f)}return g=a.validator.normalizeRules(a.extend({},a.validator.classRules(j),a.validator.attributeRules(j),a.validator.dataRules(j),a.validator.staticRules(j)),j),g.required&&(h=g.required,delete g.required,g=a.extend({required:h},g)),g.remote&&(h=g.remote,delete g.remote,g=a.extend(g,{remote:h})),g}}}),a.extend(a.expr.pseudos||a.expr[":"],{blank:function(b){return!a.trim(""+a(b).val())},filled:function(b){var c=a(b).val();return null!==c&&!!a.trim(""+c)},unchecked:function(b){return!a(b).prop("checked")}}),a.validator=function(b,c){this.settings=a.extend(!0,{},a.validator.defaults,b),this.currentForm=c,this.init()},a.validator.format=function(b,c){return 1===arguments.length?function(){var c=a.makeArray(arguments);return c.unshift(b),a.validator.format.apply(this,c)}:void 0===c?b:(arguments.length>2&&c.constructor!==Array&&(c=a.makeArray(arguments).slice(1)),c.constructor!==Array&&(c=[c]),a.each(c,function(a,c){b=b.replace(new RegExp("\\{"+a+"\\}","g"),function(){return c})}),b)},a.extend(a.validator,{defaults:{messages:{},groups:{},rules:{},errorClass:"error",pendingClass:"pending",validClass:"valid",errorElement:"label",focusCleanup:!1,focusInvalid:!0,errorContainer:a([]),errorLabelContainer:a([]),onsubmit:!0,ignore:":hidden",ignoreTitle:!1,onfocusin:function(a){this.lastActive=a,this.settings.focusCleanup&&(this.settings.unhighlight&&this.settings.unhighlight.call(this,a,this.settings.errorClass,this.settings.validClass),this.hideThese(this.errorsFor(a)))},onfocusout:function(a){this.checkable(a)||!(a.name in this.submitted)&&this.optional(a)||this.element(a)},onkeyup:function(b,c){var d=[16,17,18,20,35,36,37,38,39,40,45,144,225];9===c.which&&""===this.elementValue(b)||a.inArray(c.keyCode,d)!==-1||(b.name in this.submitted||b.name in this.invalid)&&this.element(b)},onclick:function(a){a.name in this.submitted?this.element(a):a.parentNode.name in this.submitted&&this.element(a.parentNode)},highlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).addClass(c).removeClass(d):a(b).addClass(c).removeClass(d)},unhighlight:function(b,c,d){"radio"===b.type?this.findByName(b.name).removeClass(c).addClass(d):a(b).removeClass(c).addClass(d)}},setDefaults:function(b){a.extend(a.validator.defaults,b)},messages:{required:"This field is required.",remote:"Please fix this field.",email:"Please enter a valid email address.",url:"Please enter a valid URL.",date:"Please enter a valid date.",dateISO:"Please enter a valid date (ISO).",number:"Please enter a valid number.",digits:"Please enter only digits.",equalTo:"Please enter the same value again.",maxlength:a.validator.format("Please enter no more than {0} characters."),minlength:a.validator.format("Please enter at least {0} characters."),rangelength:a.validator.format("Please enter a value between {0} and {1} characters long."),range:a.validator.format("Please enter a value between {0} and {1}."),max:a.validator.format("Please enter a value less than or equal to {0}."),min:a.validator.format("Please enter a value greater than or equal to {0}."),step:a.validator.format("Please enter a multiple of {0}.")},autoCreateRanges:!1,prototype:{init:function(){function b(b){!this.form&&this.hasAttribute("contenteditable")&&(this.form=a(this).closest("form")[0],this.name=a(this).attr("name"));var c=a.data(this.form,"validator"),d="on"+b.type.replace(/^validate/,""),e=c.settings;e[d]&&!a(this).is(e.ignore)&&e[d].call(c,this,b)}this.labelContainer=a(this.settings.errorLabelContainer),this.errorContext=this.labelContainer.length&&this.labelContainer||a(this.currentForm),this.containers=a(this.settings.errorContainer).add(this.settings.errorLabelContainer),this.submitted={},this.valueCache={},this.pendingRequest=0,this.pending={},this.invalid={},this.reset();var c,d=this.groups={};a.each(this.settings.groups,function(b,c){"string"==typeof c&&(c=c.split(/\s/)),a.each(c,function(a,c){d[c]=b})}),c=this.settings.rules,a.each(c,function(b,d){c[b]=a.validator.normalizeRule(d)}),a(this.currentForm).on("focusin.validate focusout.validate keyup.validate",":text, [type='password'], [type='file'], select, textarea, [type='number'], [type='search'], [type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'], [type='radio'], [type='checkbox'], [contenteditable], [type='button']",b).on("click.validate","select, option, [type='radio'], [type='checkbox']",b),this.settings.invalidHandler&&a(this.currentForm).on("invalid-form.validate",this.settings.invalidHandler)},form:function(){return this.checkForm(),a.extend(this.submitted,this.errorMap),this.invalid=a.extend({},this.errorMap),this.valid()||a(this.currentForm).triggerHandler("invalid-form",[this]),this.showErrors(),this.valid()},checkForm:function(){this.prepareForm();for(var a=0,b=this.currentElements=this.elements();b[a];a++)this.check(b[a]);return this.valid()},element:function(b){var c,d,e=this.clean(b),f=this.validationTargetFor(e),g=this,h=!0;return void 0===f?delete this.invalid[e.name]:(this.prepareElement(f),this.currentElements=a(f),d=this.groups[f.name],d&&a.each(this.groups,function(a,b){b===d&&a!==f.name&&(e=g.validationTargetFor(g.clean(g.findByName(a))),e&&e.name in g.invalid&&(g.currentElements.push(e),h=g.check(e)&&h))}),c=this.check(f)!==!1,h=h&&c,c?this.invalid[f.name]=!1:this.invalid[f.name]=!0,this.numberOfInvalids()||(this.toHide=this.toHide.add(this.containers)),this.showErrors(),a(b).attr("aria-invalid",!c)),h},showErrors:function(b){if(b){var c=this;a.extend(this.errorMap,b),this.errorList=a.map(this.errorMap,function(a,b){return{message:a,element:c.findByName(b)[0]}}),this.successList=a.grep(this.successList,function(a){return!(a.name in b)})}this.settings.showErrors?this.settings.showErrors.call(this,this.errorMap,this.errorList):this.defaultShowErrors()},resetForm:function(){a.fn.resetForm&&a(this.currentForm).resetForm(),this.invalid={},this.submitted={},this.prepareForm(),this.hideErrors();var b=this.elements().removeData("previousValue").removeAttr("aria-invalid");this.resetElements(b)},resetElements:function(a){var b;if(this.settings.unhighlight)for(b=0;a[b];b++)this.settings.unhighlight.call(this,a[b],this.settings.errorClass,""),this.findByName(a[b].name).removeClass(this.settings.validClass);else a.removeClass(this.settings.errorClass).removeClass(this.settings.validClass)},numberOfInvalids:function(){return this.objectLength(this.invalid)},objectLength:function(a){var b,c=0;for(b in a)void 0!==a[b]&&null!==a[b]&&a[b]!==!1&&c++;return c},hideErrors:function(){this.hideThese(this.toHide)},hideThese:function(a){a.not(this.containers).text(""),this.addWrapper(a).hide()},valid:function(){return 0===this.size()},size:function(){return this.errorList.length},focusInvalid:function(){if(this.settings.focusInvalid)try{a(this.findLastActive()||this.errorList.length&&this.errorList[0].element||[]).filter(":visible").focus().trigger("focusin")}catch(b){}},findLastActive:function(){var b=this.lastActive;return b&&1===a.grep(this.errorList,function(a){return a.element.name===b.name}).length&&b},elements:function(){var b=this,c={};return a(this.currentForm).find("input, select, textarea, [contenteditable]").not(":submit, :reset, :image, :disabled").not(this.settings.ignore).filter(function(){var d=this.name||a(this).attr("name");return!d&&b.settings.debug&&window.console&&console.error("%o has no name assigned",this),this.hasAttribute("contenteditable")&&(this.form=a(this).closest("form")[0],this.name=d),!(d in c||!b.objectLength(a(this).rules()))&&(c[d]=!0,!0)})},clean:function(b){return a(b)[0]},errors:function(){var b=this.settings.errorClass.split(" ").join(".");return a(this.settings.errorElement+"."+b,this.errorContext)},resetInternals:function(){this.successList=[],this.errorList=[],this.errorMap={},this.toShow=a([]),this.toHide=a([])},reset:function(){this.resetInternals(),this.currentElements=a([])},prepareForm:function(){this.reset(),this.toHide=this.errors().add(this.containers)},prepareElement:function(a){this.reset(),this.toHide=this.errorsFor(a)},elementValue:function(b){var c,d,e=a(b),f=b.type;return"radio"===f||"checkbox"===f?this.findByName(b.name).filter(":checked").val():"number"===f&&"undefined"!=typeof b.validity?b.validity.badInput?"NaN":e.val():(c=b.hasAttribute("contenteditable")?e.text():e.val(),"file"===f?"C:\\fakepath\\"===c.substr(0,12)?c.substr(12):(d=c.lastIndexOf("/"),d>=0?c.substr(d+1):(d=c.lastIndexOf("\\"),d>=0?c.substr(d+1):c)):"string"==typeof c?c.replace(/\r/g,""):c)},check:function(b){b=this.validationTargetFor(this.clean(b));var c,d,e,f,g=a(b).rules(),h=a.map(g,function(a,b){return b}).length,i=!1,j=this.elementValue(b);if("function"==typeof g.normalizer?f=g.normalizer:"function"==typeof this.settings.normalizer&&(f=this.settings.normalizer),f){if(j=f.call(b,j),"string"!=typeof j)throw new TypeError("The normalizer should return a string value.");delete g.normalizer}for(d in g){e={method:d,parameters:g[d]};try{if(c=a.validator.methods[d].call(this,j,b,e.parameters),"dependency-mismatch"===c&&1===h){i=!0;continue}if(i=!1,"pending"===c)return void(this.toHide=this.toHide.not(this.errorsFor(b)));if(!c)return this.formatAndAdd(b,e),!1}catch(k){throw this.settings.debug&&window.console&&console.log("Exception occurred when checking element "+b.id+", check the '"+e.method+"' method.",k),k instanceof TypeError&&(k.message+=".  Exception occurred when checking element "+b.id+", check the '"+e.method+"' method."),k}}if(!i)return this.objectLength(g)&&this.successList.push(b),!0},customDataMessage:function(b,c){return a(b).data("msg"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase())||a(b).data("msg")},customMessage:function(a,b){var c=this.settings.messages[a];return c&&(c.constructor===String?c:c[b])},findDefined:function(){for(var a=0;a<arguments.length;a++)if(void 0!==arguments[a])return arguments[a]},defaultMessage:function(b,c){"string"==typeof c&&(c={method:c});var d=this.findDefined(this.customMessage(b.name,c.method),this.customDataMessage(b,c.method),!this.settings.ignoreTitle&&b.title||void 0,a.validator.messages[c.method],"<strong>Warning: No message defined for "+b.name+"</strong>"),e=/\$?\{(\d+)\}/g;return"function"==typeof d?d=d.call(this,c.parameters,b):e.test(d)&&(d=a.validator.format(d.replace(e,"{$1}"),c.parameters)),d},formatAndAdd:function(a,b){var c=this.defaultMessage(a,b);this.errorList.push({message:c,element:a,method:b.method}),this.errorMap[a.name]=c,this.submitted[a.name]=c},addWrapper:function(a){return this.settings.wrapper&&(a=a.add(a.parent(this.settings.wrapper))),a},defaultShowErrors:function(){var a,b,c;for(a=0;this.errorList[a];a++)c=this.errorList[a],this.settings.highlight&&this.settings.highlight.call(this,c.element,this.settings.errorClass,this.settings.validClass),this.showLabel(c.element,c.message);if(this.errorList.length&&(this.toShow=this.toShow.add(this.containers)),this.settings.success)for(a=0;this.successList[a];a++)this.showLabel(this.successList[a]);if(this.settings.unhighlight)for(a=0,b=this.validElements();b[a];a++)this.settings.unhighlight.call(this,b[a],this.settings.errorClass,this.settings.validClass);this.toHide=this.toHide.not(this.toShow),this.hideErrors(),this.addWrapper(this.toShow).show()},validElements:function(){return this.currentElements.not(this.invalidElements())},invalidElements:function(){return a(this.errorList).map(function(){return this.element})},showLabel:function(b,c){var d,e,f,g,h=this.errorsFor(b),i=this.idOrName(b),j=a(b).attr("aria-describedby");h.length?(h.removeClass(this.settings.validClass).addClass(this.settings.errorClass),h.html(c)):(h=a("<"+this.settings.errorElement+">").attr("id",i+"-error").addClass(this.settings.errorClass).html(c||""),d=h,this.settings.wrapper&&(d=h.hide().show().wrap("<"+this.settings.wrapper+"/>").parent()),this.labelContainer.length?this.labelContainer.append(d):this.settings.errorPlacement?this.settings.errorPlacement.call(this,d,a(b)):d.insertAfter(b),h.is("label")?h.attr("for",i):0===h.parents("label[for='"+this.escapeCssMeta(i)+"']").length&&(f=h.attr("id"),j?j.match(new RegExp("\\b"+this.escapeCssMeta(f)+"\\b"))||(j+=" "+f):j=f,a(b).attr("aria-describedby",j),e=this.groups[b.name],e&&(g=this,a.each(g.groups,function(b,c){c===e&&a("[name='"+g.escapeCssMeta(b)+"']",g.currentForm).attr("aria-describedby",h.attr("id"))})))),!c&&this.settings.success&&(h.text(""),"string"==typeof this.settings.success?h.addClass(this.settings.success):this.settings.success(h,b)),this.toShow=this.toShow.add(h)},errorsFor:function(b){var c=this.escapeCssMeta(this.idOrName(b)),d=a(b).attr("aria-describedby"),e="label[for='"+c+"'], label[for='"+c+"'] *";return d&&(e=e+", #"+this.escapeCssMeta(d).replace(/\s+/g,", #")),this.errors().filter(e)},escapeCssMeta:function(a){return a.replace(/([\\!"#$%&'()*+,.\/:;<=>?@\[\]^`{|}~])/g,"\\$1")},idOrName:function(a){return this.groups[a.name]||(this.checkable(a)?a.name:a.id||a.name)},validationTargetFor:function(b){return this.checkable(b)&&(b=this.findByName(b.name)),a(b).not(this.settings.ignore)[0]},checkable:function(a){return/radio|checkbox/i.test(a.type)},findByName:function(b){return a(this.currentForm).find("[name='"+this.escapeCssMeta(b)+"']")},getLength:function(b,c){switch(c.nodeName.toLowerCase()){case"select":return a("option:selected",c).length;case"input":if(this.checkable(c))return this.findByName(c.name).filter(":checked").length}return b.length},depend:function(a,b){return!this.dependTypes[typeof a]||this.dependTypes[typeof a](a,b)},dependTypes:{"boolean":function(a){return a},string:function(b,c){return!!a(b,c.form).length},"function":function(a,b){return a(b)}},optional:function(b){var c=this.elementValue(b);return!a.validator.methods.required.call(this,c,b)&&"dependency-mismatch"},startRequest:function(b){this.pending[b.name]||(this.pendingRequest++,a(b).addClass(this.settings.pendingClass),this.pending[b.name]=!0)},stopRequest:function(b,c){this.pendingRequest--,this.pendingRequest<0&&(this.pendingRequest=0),delete this.pending[b.name],a(b).removeClass(this.settings.pendingClass),c&&0===this.pendingRequest&&this.formSubmitted&&this.form()?(a(this.currentForm).submit(),this.submitButton&&a("input:hidden[name='"+this.submitButton.name+"']",this.currentForm).remove(),this.formSubmitted=!1):!c&&0===this.pendingRequest&&this.formSubmitted&&(a(this.currentForm).triggerHandler("invalid-form",[this]),this.formSubmitted=!1)},previousValue:function(b,c){return c="string"==typeof c&&c||"remote",a.data(b,"previousValue")||a.data(b,"previousValue",{old:null,valid:!0,message:this.defaultMessage(b,{method:c})})},destroy:function(){this.resetForm(),a(this.currentForm).off(".validate").removeData("validator").find(".validate-equalTo-blur").off(".validate-equalTo").removeClass("validate-equalTo-blur")}},classRuleSettings:{required:{required:!0},email:{email:!0},url:{url:!0},date:{date:!0},dateISO:{dateISO:!0},number:{number:!0},digits:{digits:!0},creditcard:{creditcard:!0}},addClassRules:function(b,c){b.constructor===String?this.classRuleSettings[b]=c:a.extend(this.classRuleSettings,b)},classRules:function(b){var c={},d=a(b).attr("class");return d&&a.each(d.split(" "),function(){this in a.validator.classRuleSettings&&a.extend(c,a.validator.classRuleSettings[this])}),c},normalizeAttributeRule:function(a,b,c,d){/min|max|step/.test(c)&&(null===b||/number|range|text/.test(b))&&(d=Number(d),isNaN(d)&&(d=void 0)),d||0===d?a[c]=d:b===c&&"range"!==b&&(a[c]=!0)},attributeRules:function(b){var c,d,e={},f=a(b),g=b.getAttribute("type");for(c in a.validator.methods)"required"===c?(d=b.getAttribute(c),""===d&&(d=!0),d=!!d):d=f.attr(c),this.normalizeAttributeRule(e,g,c,d);return e.maxlength&&/-1|2147483647|524288/.test(e.maxlength)&&delete e.maxlength,e},dataRules:function(b){var c,d,e={},f=a(b),g=b.getAttribute("type");for(c in a.validator.methods)d=f.data("rule"+c.charAt(0).toUpperCase()+c.substring(1).toLowerCase()),this.normalizeAttributeRule(e,g,c,d);return e},staticRules:function(b){var c={},d=a.data(b.form,"validator");return d.settings.rules&&(c=a.validator.normalizeRule(d.settings.rules[b.name])||{}),c},normalizeRules:function(b,c){return a.each(b,function(d,e){if(e===!1)return void delete b[d];if(e.param||e.depends){var f=!0;switch(typeof e.depends){case"string":f=!!a(e.depends,c.form).length;break;case"function":f=e.depends.call(c,c)}f?b[d]=void 0===e.param||e.param:(a.data(c.form,"validator").resetElements(a(c)),delete b[d])}}),a.each(b,function(d,e){b[d]=a.isFunction(e)&&"normalizer"!==d?e(c):e}),a.each(["minlength","maxlength"],function(){b[this]&&(b[this]=Number(b[this]))}),a.each(["rangelength","range"],function(){var c;b[this]&&(a.isArray(b[this])?b[this]=[Number(b[this][0]),Number(b[this][1])]:"string"==typeof b[this]&&(c=b[this].replace(/[\[\]]/g,"").split(/[\s,]+/),b[this]=[Number(c[0]),Number(c[1])]))}),a.validator.autoCreateRanges&&(null!=b.min&&null!=b.max&&(b.range=[b.min,b.max],delete b.min,delete b.max),null!=b.minlength&&null!=b.maxlength&&(b.rangelength=[b.minlength,b.maxlength],delete b.minlength,delete b.maxlength)),b},normalizeRule:function(b){if("string"==typeof b){var c={};a.each(b.split(/\s/),function(){c[this]=!0}),b=c}return b},addMethod:function(b,c,d){a.validator.methods[b]=c,a.validator.messages[b]=void 0!==d?d:a.validator.messages[b],c.length<3&&a.validator.addClassRules(b,a.validator.normalizeRule(b))},methods:{required:function(b,c,d){if(!this.depend(d,c))return"dependency-mismatch";if("select"===c.nodeName.toLowerCase()){var e=a(c).val();return e&&e.length>0}return this.checkable(c)?this.getLength(b,c)>0:b.length>0},email:function(a,b){return this.optional(b)||/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(a)},url:function(a,b){return this.optional(b)||/^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[\/?#]\S*)?$/i.test(a)},date:function(a,b){return this.optional(b)||!/Invalid|NaN/.test(new Date(a).toString())},dateISO:function(a,b){return this.optional(b)||/^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(a)},number:function(a,b){return this.optional(b)||/^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(a)},digits:function(a,b){return this.optional(b)||/^\d+$/.test(a)},minlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d},maxlength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e<=d},rangelength:function(b,c,d){var e=a.isArray(b)?b.length:this.getLength(b,c);return this.optional(c)||e>=d[0]&&e<=d[1]},min:function(a,b,c){return this.optional(b)||a>=c},max:function(a,b,c){return this.optional(b)||a<=c},range:function(a,b,c){return this.optional(b)||a>=c[0]&&a<=c[1]},step:function(b,c,d){var e,f=a(c).attr("type"),g="Step attribute on input type "+f+" is not supported.",h=["text","number","range"],i=new RegExp("\\b"+f+"\\b"),j=f&&!i.test(h.join()),k=function(a){var b=(""+a).match(/(?:\.(\d+))?$/);return b&&b[1]?b[1].length:0},l=function(a){return Math.round(a*Math.pow(10,e))},m=!0;if(j)throw new Error(g);return e=k(d),(k(b)>e||l(b)%l(d)!==0)&&(m=!1),this.optional(c)||m},equalTo:function(b,c,d){var e=a(d);return this.settings.onfocusout&&e.not(".validate-equalTo-blur").length&&e.addClass("validate-equalTo-blur").on("blur.validate-equalTo",function(){a(c).valid()}),b===e.val()},remote:function(b,c,d,e){if(this.optional(c))return"dependency-mismatch";e="string"==typeof e&&e||"remote";var f,g,h,i=this.previousValue(c,e);return this.settings.messages[c.name]||(this.settings.messages[c.name]={}),i.originalMessage=i.originalMessage||this.settings.messages[c.name][e],this.settings.messages[c.name][e]=i.message,d="string"==typeof d&&{url:d}||d,h=a.param(a.extend({data:b},d.data)),i.old===h?i.valid:(i.old=h,f=this,this.startRequest(c),g={},g[c.name]=b,a.ajax(a.extend(!0,{mode:"abort",port:"validate"+c.name,dataType:"json",data:g,context:f.currentForm,success:function(a){var d,g,h,j=a===!0||"true"===a;f.settings.messages[c.name][e]=i.originalMessage,j?(h=f.formSubmitted,f.resetInternals(),f.toHide=f.errorsFor(c),f.formSubmitted=h,f.successList.push(c),f.invalid[c.name]=!1,f.showErrors()):(d={},g=a||f.defaultMessage(c,{method:e,parameters:b}),d[c.name]=i.message=g,f.invalid[c.name]=!0,f.showErrors(d)),i.valid=j,f.stopRequest(c,j)}},d)),"pending")}}});var b,c={};return a.ajaxPrefilter?a.ajaxPrefilter(function(a,b,d){var e=a.port;"abort"===a.mode&&(c[e]&&c[e].abort(),c[e]=d)}):(b=a.ajax,a.ajax=function(d){var e=("mode"in d?d:a.ajaxSettings).mode,f=("port"in d?d:a.ajaxSettings).port;return"abort"===e?(c[f]&&c[f].abort(),c[f]=b.apply(this,arguments),c[f]):b.apply(this,arguments)}),a});
/*! jQuery Validation Plugin - v1.17.0 - 7/29/2017
 * https://jqueryvalidation.org/
 * Copyright (c) 2017 Jörn Zaefferer; Licensed MIT */
!function(a){"function"==typeof define&&define.amd?define(["jquery","./jquery.validate.min"],a):"object"==typeof module&&module.exports?module.exports=a(require("jquery")):a(jQuery)}(function(a){return function(){function b(a){return a.replace(/<.[^<>]*?>/g," ").replace(/&nbsp;|&#160;/gi," ").replace(/[.(),;:!?%#$'\"_+=\/\-“”’]*/g,"")}a.validator.addMethod("maxWords",function(a,c,d){return this.optional(c)||b(a).match(/\b\w+\b/g).length<=d},a.validator.format("Please enter {0} words or less.")),a.validator.addMethod("minWords",function(a,c,d){return this.optional(c)||b(a).match(/\b\w+\b/g).length>=d},a.validator.format("Please enter at least {0} words.")),a.validator.addMethod("rangeWords",function(a,c,d){var e=b(a),f=/\b\w+\b/g;return this.optional(c)||e.match(f).length>=d[0]&&e.match(f).length<=d[1]},a.validator.format("Please enter between {0} and {1} words."))}(),a.validator.addMethod("accept",function(b,c,d){var e,f,g,h="string"==typeof d?d.replace(/\s/g,""):"image/*",i=this.optional(c);if(i)return i;if("file"===a(c).attr("type")&&(h=h.replace(/[\-\[\]\/\{\}\(\)\+\?\.\\\^\$\|]/g,"\\$&").replace(/,/g,"|").replace(/\/\*/g,"/.*"),c.files&&c.files.length))for(g=new RegExp(".?("+h+")$","i"),e=0;e<c.files.length;e++)if(f=c.files[e],!f.type.match(g))return!1;return!0},a.validator.format("Please enter a value with a valid mimetype.")),a.validator.addMethod("alphanumeric",function(a,b){return this.optional(b)||/^\w+$/i.test(a)},"Letters, numbers, and underscores only please"),a.validator.addMethod("bankaccountNL",function(a,b){if(this.optional(b))return!0;if(!/^[0-9]{9}|([0-9]{2} ){3}[0-9]{3}$/.test(a))return!1;var c,d,e,f=a.replace(/ /g,""),g=0,h=f.length;for(c=0;c<h;c++)d=h-c,e=f.substring(c,c+1),g+=d*e;return g%11===0},"Please specify a valid bank account number"),a.validator.addMethod("bankorgiroaccountNL",function(b,c){return this.optional(c)||a.validator.methods.bankaccountNL.call(this,b,c)||a.validator.methods.giroaccountNL.call(this,b,c)},"Please specify a valid bank or giro account number"),a.validator.addMethod("bic",function(a,b){return this.optional(b)||/^([A-Z]{6}[A-Z2-9][A-NP-Z1-9])(X{3}|[A-WY-Z0-9][A-Z0-9]{2})?$/.test(a.toUpperCase())},"Please specify a valid BIC code"),a.validator.addMethod("cifES",function(a,b){"use strict";function c(a){return a%2===0}if(this.optional(b))return!0;var d,e,f,g,h=new RegExp(/^([ABCDEFGHJKLMNPQRSUVW])(\d{7})([0-9A-J])$/gi),i=a.substring(0,1),j=a.substring(1,8),k=a.substring(8,9),l=0,m=0,n=0;if(9!==a.length||!h.test(a))return!1;for(d=0;d<j.length;d++)e=parseInt(j[d],10),c(d)?(e*=2,n+=e<10?e:e-9):m+=e;return l=m+n,f=(10-l.toString().substr(-1)).toString(),f=parseInt(f,10)>9?"0":f,g="JABCDEFGHI".substr(f,1).toString(),i.match(/[ABEH]/)?k===f:i.match(/[KPQS]/)?k===g:k===f||k===g},"Please specify a valid CIF number."),a.validator.addMethod("cpfBR",function(a){if(a=a.replace(/([~!@#$%^&*()_+=`{}\[\]\-|\\:;'<>,.\/? ])+/g,""),11!==a.length)return!1;var b,c,d,e,f=0;if(b=parseInt(a.substring(9,10),10),c=parseInt(a.substring(10,11),10),d=function(a,b){var c=10*a%11;return 10!==c&&11!==c||(c=0),c===b},""===a||"00000000000"===a||"11111111111"===a||"22222222222"===a||"33333333333"===a||"44444444444"===a||"55555555555"===a||"66666666666"===a||"77777777777"===a||"88888888888"===a||"99999999999"===a)return!1;for(e=1;e<=9;e++)f+=parseInt(a.substring(e-1,e),10)*(11-e);if(d(f,b)){for(f=0,e=1;e<=10;e++)f+=parseInt(a.substring(e-1,e),10)*(12-e);return d(f,c)}return!1},"Please specify a valid CPF number"),a.validator.addMethod("creditcard",function(a,b){if(this.optional(b))return"dependency-mismatch";if(/[^0-9 \-]+/.test(a))return!1;var c,d,e=0,f=0,g=!1;if(a=a.replace(/\D/g,""),a.length<13||a.length>19)return!1;for(c=a.length-1;c>=0;c--)d=a.charAt(c),f=parseInt(d,10),g&&(f*=2)>9&&(f-=9),e+=f,g=!g;return e%10===0},"Please enter a valid credit card number."),a.validator.addMethod("creditcardtypes",function(a,b,c){if(/[^0-9\-]+/.test(a))return!1;a=a.replace(/\D/g,"");var d=0;return c.mastercard&&(d|=1),c.visa&&(d|=2),c.amex&&(d|=4),c.dinersclub&&(d|=8),c.enroute&&(d|=16),c.discover&&(d|=32),c.jcb&&(d|=64),c.unknown&&(d|=128),c.all&&(d=255),1&d&&/^(5[12345])/.test(a)?16===a.length:2&d&&/^(4)/.test(a)?16===a.length:4&d&&/^(3[47])/.test(a)?15===a.length:8&d&&/^(3(0[012345]|[68]))/.test(a)?14===a.length:16&d&&/^(2(014|149))/.test(a)?15===a.length:32&d&&/^(6011)/.test(a)?16===a.length:64&d&&/^(3)/.test(a)?16===a.length:64&d&&/^(2131|1800)/.test(a)?15===a.length:!!(128&d)},"Please enter a valid credit card number."),a.validator.addMethod("currency",function(a,b,c){var d,e="string"==typeof c,f=e?c:c[0],g=!!e||c[1];return f=f.replace(/,/g,""),f=g?f+"]":f+"]?",d="^["+f+"([1-9]{1}[0-9]{0,2}(\\,[0-9]{3})*(\\.[0-9]{0,2})?|[1-9]{1}[0-9]{0,}(\\.[0-9]{0,2})?|0(\\.[0-9]{0,2})?|(\\.[0-9]{1,2})?)$",d=new RegExp(d),this.optional(b)||d.test(a)},"Please specify a valid currency"),a.validator.addMethod("dateFA",function(a,b){return this.optional(b)||/^[1-4]\d{3}\/((0?[1-6]\/((3[0-1])|([1-2][0-9])|(0?[1-9])))|((1[0-2]|(0?[7-9]))\/(30|([1-2][0-9])|(0?[1-9]))))$/.test(a)},a.validator.messages.date),a.validator.addMethod("dateITA",function(a,b){var c,d,e,f,g,h=!1,i=/^\d{1,2}\/\d{1,2}\/\d{4}$/;return i.test(a)?(c=a.split("/"),d=parseInt(c[0],10),e=parseInt(c[1],10),f=parseInt(c[2],10),g=new Date(Date.UTC(f,e-1,d,12,0,0,0)),h=g.getUTCFullYear()===f&&g.getUTCMonth()===e-1&&g.getUTCDate()===d):h=!1,this.optional(b)||h},a.validator.messages.date),a.validator.addMethod("dateNL",function(a,b){return this.optional(b)||/^(0?[1-9]|[12]\d|3[01])[\.\/\-](0?[1-9]|1[012])[\.\/\-]([12]\d)?(\d\d)$/.test(a)},a.validator.messages.date),a.validator.addMethod("extension",function(a,b,c){return c="string"==typeof c?c.replace(/,/g,"|"):"png|jpe?g|gif",this.optional(b)||a.match(new RegExp("\\.("+c+")$","i"))},a.validator.format("Please enter a value with a valid extension.")),a.validator.addMethod("giroaccountNL",function(a,b){return this.optional(b)||/^[0-9]{1,7}$/.test(a)},"Please specify a valid giro account number"),a.validator.addMethod("iban",function(a,b){if(this.optional(b))return!0;var c,d,e,f,g,h,i,j,k,l=a.replace(/ /g,"").toUpperCase(),m="",n=!0,o="",p="",q=5;if(l.length<q)return!1;if(c=l.substring(0,2),h={AL:"\\d{8}[\\dA-Z]{16}",AD:"\\d{8}[\\dA-Z]{12}",AT:"\\d{16}",AZ:"[\\dA-Z]{4}\\d{20}",BE:"\\d{12}",BH:"[A-Z]{4}[\\dA-Z]{14}",BA:"\\d{16}",BR:"\\d{23}[A-Z][\\dA-Z]",BG:"[A-Z]{4}\\d{6}[\\dA-Z]{8}",CR:"\\d{17}",HR:"\\d{17}",CY:"\\d{8}[\\dA-Z]{16}",CZ:"\\d{20}",DK:"\\d{14}",DO:"[A-Z]{4}\\d{20}",EE:"\\d{16}",FO:"\\d{14}",FI:"\\d{14}",FR:"\\d{10}[\\dA-Z]{11}\\d{2}",GE:"[\\dA-Z]{2}\\d{16}",DE:"\\d{18}",GI:"[A-Z]{4}[\\dA-Z]{15}",GR:"\\d{7}[\\dA-Z]{16}",GL:"\\d{14}",GT:"[\\dA-Z]{4}[\\dA-Z]{20}",HU:"\\d{24}",IS:"\\d{22}",IE:"[\\dA-Z]{4}\\d{14}",IL:"\\d{19}",IT:"[A-Z]\\d{10}[\\dA-Z]{12}",KZ:"\\d{3}[\\dA-Z]{13}",KW:"[A-Z]{4}[\\dA-Z]{22}",LV:"[A-Z]{4}[\\dA-Z]{13}",LB:"\\d{4}[\\dA-Z]{20}",LI:"\\d{5}[\\dA-Z]{12}",LT:"\\d{16}",LU:"\\d{3}[\\dA-Z]{13}",MK:"\\d{3}[\\dA-Z]{10}\\d{2}",MT:"[A-Z]{4}\\d{5}[\\dA-Z]{18}",MR:"\\d{23}",MU:"[A-Z]{4}\\d{19}[A-Z]{3}",MC:"\\d{10}[\\dA-Z]{11}\\d{2}",MD:"[\\dA-Z]{2}\\d{18}",ME:"\\d{18}",NL:"[A-Z]{4}\\d{10}",NO:"\\d{11}",PK:"[\\dA-Z]{4}\\d{16}",PS:"[\\dA-Z]{4}\\d{21}",PL:"\\d{24}",PT:"\\d{21}",RO:"[A-Z]{4}[\\dA-Z]{16}",SM:"[A-Z]\\d{10}[\\dA-Z]{12}",SA:"\\d{2}[\\dA-Z]{18}",RS:"\\d{18}",SK:"\\d{20}",SI:"\\d{15}",ES:"\\d{20}",SE:"\\d{20}",CH:"\\d{5}[\\dA-Z]{12}",TN:"\\d{20}",TR:"\\d{5}[\\dA-Z]{17}",AE:"\\d{3}\\d{16}",GB:"[A-Z]{4}\\d{14}",VG:"[\\dA-Z]{4}\\d{16}"},g=h[c],"undefined"!=typeof g&&(i=new RegExp("^[A-Z]{2}\\d{2}"+g+"$",""),!i.test(l)))return!1;for(d=l.substring(4,l.length)+l.substring(0,4),j=0;j<d.length;j++)e=d.charAt(j),"0"!==e&&(n=!1),n||(m+="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(e));for(k=0;k<m.length;k++)f=m.charAt(k),p=""+o+f,o=p%97;return 1===o},"Please specify a valid IBAN"),a.validator.addMethod("integer",function(a,b){return this.optional(b)||/^-?\d+$/.test(a)},"A positive or negative non-decimal number please"),a.validator.addMethod("ipv4",function(a,b){return this.optional(b)||/^(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)\.(25[0-5]|2[0-4]\d|[01]?\d\d?)$/i.test(a)},"Please enter a valid IP v4 address."),a.validator.addMethod("ipv6",function(a,b){return this.optional(b)||/^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test(a)},"Please enter a valid IP v6 address."),a.validator.addMethod("lettersonly",function(a,b){return this.optional(b)||/^[a-z]+$/i.test(a)},"Letters only please"),a.validator.addMethod("letterswithbasicpunc",function(a,b){return this.optional(b)||/^[a-z\-.,()'"\s]+$/i.test(a)},"Letters or punctuation only please"),a.validator.addMethod("mobileNL",function(a,b){return this.optional(b)||/^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)6((\s|\s?\-\s?)?[0-9]){8}$/.test(a)},"Please specify a valid mobile number"),a.validator.addMethod("mobileUK",function(a,b){return a=a.replace(/\(|\)|\s+|-/g,""),this.optional(b)||a.length>9&&a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)7(?:[1345789]\d{2}|624)\s?\d{3}\s?\d{3})$/)},"Please specify a valid mobile number"),a.validator.addMethod("netmask",function(a,b){return this.optional(b)||/^(254|252|248|240|224|192|128)\.0\.0\.0|255\.(254|252|248|240|224|192|128|0)\.0\.0|255\.255\.(254|252|248|240|224|192|128|0)\.0|255\.255\.255\.(254|252|248|240|224|192|128|0)/i.test(a)},"Please enter a valid netmask."),a.validator.addMethod("nieES",function(a,b){"use strict";if(this.optional(b))return!0;var c,d=new RegExp(/^[MXYZ]{1}[0-9]{7,8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/gi),e="TRWAGMYFPDXBNJZSQVHLCKET",f=a.substr(a.length-1).toUpperCase();return a=a.toString().toUpperCase(),!(a.length>10||a.length<9||!d.test(a))&&(a=a.replace(/^[X]/,"0").replace(/^[Y]/,"1").replace(/^[Z]/,"2"),c=9===a.length?a.substr(0,8):a.substr(0,9),e.charAt(parseInt(c,10)%23)===f)},"Please specify a valid NIE number."),a.validator.addMethod("nifES",function(a,b){"use strict";return!!this.optional(b)||(a=a.toUpperCase(),!!a.match("((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)")&&(/^[0-9]{8}[A-Z]{1}$/.test(a)?"TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.substring(8,0)%23)===a.charAt(8):!!/^[KLM]{1}/.test(a)&&a[8]==="TRWAGMYFPDXBNJZSQVHLCKE".charAt(a.substring(8,1)%23)))},"Please specify a valid NIF number."),a.validator.addMethod("nipPL",function(a){"use strict";if(a=a.replace(/[^0-9]/g,""),10!==a.length)return!1;for(var b=[6,5,7,2,3,4,5,6,7],c=0,d=0;d<9;d++)c+=b[d]*a[d];var e=c%11,f=10===e?0:e;return f===parseInt(a[9],10)},"Please specify a valid NIP number."),a.validator.addMethod("notEqualTo",function(b,c,d){return this.optional(c)||!a.validator.methods.equalTo.call(this,b,c,d)},"Please enter a different value, values must not be the same."),a.validator.addMethod("nowhitespace",function(a,b){return this.optional(b)||/^\S+$/i.test(a)},"No white space please"),a.validator.addMethod("pattern",function(a,b,c){return!!this.optional(b)||("string"==typeof c&&(c=new RegExp("^(?:"+c+")$")),c.test(a))},"Invalid format."),a.validator.addMethod("phoneNL",function(a,b){return this.optional(b)||/^((\+|00(\s|\s?\-\s?)?)31(\s|\s?\-\s?)?(\(0\)[\-\s]?)?|0)[1-9]((\s|\s?\-\s?)?[0-9]){8}$/.test(a)},"Please specify a valid phone number."),a.validator.addMethod("phonesUK",function(a,b){return a=a.replace(/\(|\)|\s+|-/g,""),this.optional(b)||a.length>9&&a.match(/^(?:(?:(?:00\s?|\+)44\s?|0)(?:1\d{8,9}|[23]\d{9}|7(?:[1345789]\d{8}|624\d{6})))$/)},"Please specify a valid uk phone number"),a.validator.addMethod("phoneUK",function(a,b){return a=a.replace(/\(|\)|\s+|-/g,""),this.optional(b)||a.length>9&&a.match(/^(?:(?:(?:00\s?|\+)44\s?)|(?:\(?0))(?:\d{2}\)?\s?\d{4}\s?\d{4}|\d{3}\)?\s?\d{3}\s?\d{3,4}|\d{4}\)?\s?(?:\d{5}|\d{3}\s?\d{3})|\d{5}\)?\s?\d{4,5})$/)},"Please specify a valid phone number"),a.validator.addMethod("phoneUS",function(a,b){return a=a.replace(/\s+/g,""),this.optional(b)||a.length>9&&a.match(/^(\+?1-?)?(\([2-9]([02-9]\d|1[02-9])\)|[2-9]([02-9]\d|1[02-9]))-?[2-9]([02-9]\d|1[02-9])-?\d{4}$/)},"Please specify a valid phone number"),a.validator.addMethod("postalcodeBR",function(a,b){return this.optional(b)||/^\d{2}.\d{3}-\d{3}?$|^\d{5}-?\d{3}?$/.test(a)},"Informe um CEP válido."),a.validator.addMethod("postalCodeCA",function(a,b){return this.optional(b)||/^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ] *\d[ABCEGHJKLMNPRSTVWXYZ]\d$/i.test(a)},"Please specify a valid postal code"),a.validator.addMethod("postalcodeIT",function(a,b){return this.optional(b)||/^\d{5}$/.test(a)},"Please specify a valid postal code"),a.validator.addMethod("postalcodeNL",function(a,b){return this.optional(b)||/^[1-9][0-9]{3}\s?[a-zA-Z]{2}$/.test(a)},"Please specify a valid postal code"),a.validator.addMethod("postcodeUK",function(a,b){return this.optional(b)||/^((([A-PR-UWYZ][0-9])|([A-PR-UWYZ][0-9][0-9])|([A-PR-UWYZ][A-HK-Y][0-9])|([A-PR-UWYZ][A-HK-Y][0-9][0-9])|([A-PR-UWYZ][0-9][A-HJKSTUW])|([A-PR-UWYZ][A-HK-Y][0-9][ABEHMNPRVWXY]))\s?([0-9][ABD-HJLNP-UW-Z]{2})|(GIR)\s?(0AA))$/i.test(a)},"Please specify a valid UK postcode"),a.validator.addMethod("require_from_group",function(b,c,d){var e=a(d[1],c.form),f=e.eq(0),g=f.data("valid_req_grp")?f.data("valid_req_grp"):a.extend({},this),h=e.filter(function(){return g.elementValue(this)}).length>=d[0];return f.data("valid_req_grp",g),a(c).data("being_validated")||(e.data("being_validated",!0),e.each(function(){g.element(this)}),e.data("being_validated",!1)),h},a.validator.format("Please fill at least {0} of these fields.")),a.validator.addMethod("skip_or_fill_minimum",function(b,c,d){var e=a(d[1],c.form),f=e.eq(0),g=f.data("valid_skip")?f.data("valid_skip"):a.extend({},this),h=e.filter(function(){return g.elementValue(this)}).length,i=0===h||h>=d[0];return f.data("valid_skip",g),a(c).data("being_validated")||(e.data("being_validated",!0),e.each(function(){g.element(this)}),e.data("being_validated",!1)),i},a.validator.format("Please either skip these fields or fill at least {0} of them.")),a.validator.addMethod("stateUS",function(a,b,c){var d,e="undefined"==typeof c,f=!e&&"undefined"!=typeof c.caseSensitive&&c.caseSensitive,g=!e&&"undefined"!=typeof c.includeTerritories&&c.includeTerritories,h=!e&&"undefined"!=typeof c.includeMilitary&&c.includeMilitary;return d=g||h?g&&h?"^(A[AEKLPRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$":g?"^(A[KLRSZ]|C[AOT]|D[CE]|FL|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEINOPST]|N[CDEHJMVY]|O[HKR]|P[AR]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY])$":"^(A[AEKLPRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$":"^(A[KLRZ]|C[AOT]|D[CE]|FL|GA|HI|I[ADLN]|K[SY]|LA|M[ADEINOST]|N[CDEHJMVY]|O[HKR]|PA|RI|S[CD]|T[NX]|UT|V[AT]|W[AIVY])$",d=f?new RegExp(d):new RegExp(d,"i"),this.optional(b)||d.test(a)},"Please specify a valid state"),a.validator.addMethod("strippedminlength",function(b,c,d){return a(b).text().length>=d},a.validator.format("Please enter at least {0} characters")),a.validator.addMethod("time",function(a,b){return this.optional(b)||/^([01]\d|2[0-3]|[0-9])(:[0-5]\d){1,2}$/.test(a)},"Please enter a valid time, between 00:00 and 23:59"),a.validator.addMethod("time12h",function(a,b){return this.optional(b)||/^((0?[1-9]|1[012])(:[0-5]\d){1,2}(\ ?[AP]M))$/i.test(a)},"Please enter a valid time in 12-hour am/pm format"),a.validator.addMethod("url2",function(a,b){return this.optional(b)||/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a)},a.validator.messages.url),a.validator.addMethod("vinUS",function(a){if(17!==a.length)return!1;var b,c,d,e,f,g,h=["A","B","C","D","E","F","G","H","J","K","L","M","N","P","R","S","T","U","V","W","X","Y","Z"],i=[1,2,3,4,5,6,7,8,1,2,3,4,5,7,9,2,3,4,5,6,7,8,9],j=[8,7,6,5,4,3,2,10,0,9,8,7,6,5,4,3,2],k=0;for(b=0;b<17;b++){if(e=j[b],d=a.slice(b,b+1),8===b&&(g=d),isNaN(d)){for(c=0;c<h.length;c++)if(d.toUpperCase()===h[c]){d=i[c],d*=e,isNaN(g)&&8===c&&(g=h[c]);break}}else d*=e;k+=d}return f=k%11,10===f&&(f="X"),f===g},"The specified vehicle identification number (VIN) is invalid."),a.validator.addMethod("zipcodeUS",function(a,b){return this.optional(b)||/^\d{5}(-\d{4})?$/.test(a)},"The specified US ZIP Code is invalid"),a.validator.addMethod("ziprange",function(a,b){return this.optional(b)||/^90[2-5]\d\{2\}-\d{4}$/.test(a)},"Your ZIP-code must be in the range 902xx-xxxx to 905xx-xxxx"),a});
/*
 * Translated default messages for the jQuery validation plugin.
 * Locale: FR (French; français)
 */
(function ($) {
    $.extend($.validator.messages, {
        required: "Ce champ est obligatoire.",
        remote: "Veuillez corriger ce champ.",
        email: "Veuillez fournir une adresse électronique valide.",
        url: "Veuillez fournir une adresse URL valide.",
        date: "Veuillez fournir une date valide.",
        dateISO: "Veuillez fournir une date valide (ISO).",
        number: "Veuillez fournir un numéro valide.",
        digits: "Veuillez fournir seulement des chiffres.",
        creditcard: "Veuillez fournir un numéro de carte de crédit valide.",
        equalTo: "Veuillez fournir encore la même valeur.",
        notEqualTo: "Veuillez fournir une valeur différente, les valeurs ne doivent pas être identiques.",
        extension: "Veuillez fournir une valeur avec une extension valide.",
        maxlength: $.validator.format("Veuillez fournir au plus {0} caractères."),
        minlength: $.validator.format("Veuillez fournir au moins {0} caractères."),
        rangelength: $.validator.format("Veuillez fournir une valeur qui contient entre {0} et {1} caractères."),
        range: $.validator.format("Veuillez fournir une valeur entre {0} et {1}."),
        max: $.validator.format("Veuillez fournir une valeur inférieure ou égale à {0}."),
        min: $.validator.format("Veuillez fournir une valeur supérieure ou égale à {0}."),
        step: $.validator.format("Veuillez fournir une valeur multiple de {0}."),
        maxWords: $.validator.format("Veuillez fournir au plus {0} mots."),
        minWords: $.validator.format("Veuillez fournir au moins {0} mots."),
        rangeWords: $.validator.format("Veuillez fournir entre {0} et {1} mots."),
        letterswithbasicpunc: "Veuillez fournir seulement des lettres et des signes de ponctuation.",
        alphanumeric: "Veuillez fournir seulement des lettres, nombres, espaces et soulignages.",
        lettersonly: "Veuillez fournir seulement des lettres.",
        nowhitespace: "Veuillez ne pas inscrire d'espaces blancs.",
        ziprange: "Veuillez fournir un code postal entre 902xx-xxxx et 905-xx-xxxx.",
        integer: "Veuillez fournir un nombre non décimal qui est positif ou négatif.",
        vinUS: "Veuillez fournir un numéro d'identification du véhicule (VIN).",
        dateITA: "Veuillez fournir une date valide.",
        time: "Veuillez fournir une heure valide entre 00:00 et 23:59.",
        phoneUS: "Veuillez fournir un numéro de téléphone valide.",
        phoneUK: "Veuillez fournir un numéro de téléphone valide.",
        mobileUK: "Veuillez fournir un numéro de téléphone mobile valide.",
        strippedminlength: $.validator.format("Veuillez fournir au moins {0} caractères."),
        email2: "Veuillez fournir une adresse électronique valide.",
        url2: "Veuillez fournir une adresse URL valide.",
        creditcardtypes: "Veuillez fournir un numéro de carte de crédit valide.",
        ipv4: "Veuillez fournir une adresse IP v4 valide.",
        ipv6: "Veuillez fournir une adresse IP v6 valide.",
        require_from_group: "Veuillez fournir au moins {0} de ces champs.",
        nifES: "Veuillez fournir un numéro NIF valide.",
        nieES: "Veuillez fournir un numéro NIE valide.",
        cifES: "Veuillez fournir un numéro CIF valide.",
        postalCodeCA: "Veuillez fournir un code postal valide."
    });
})(J);
/* Project: Bootstrap Growl = v3.1.3 | Description: Turns standard Bootstrap alerts into "Growl-like" notifications. | Author: Mouse0270 aka Robert McIntosh | License: MIT License | Website: https://github.com/mouse0270/bootstrap-growl */
!function(t){"function"==typeof define&&define.amd?define(["jquery"],t):t("object"==typeof exports?require("jquery"):jQuery)}(function(t){function e(e,i,n){var i={content:{message:"object"==typeof i?i.message:i,title:i.title?i.title:"",icon:i.icon?i.icon:"",url:i.url?i.url:"#",target:i.target?i.target:"-"}};n=t.extend(!0,{},i,n),this.settings=t.extend(!0,{},s,n),this._defaults=s,"-"==this.settings.content.target&&(this.settings.content.target=this.settings.url_target),this.animations={start:"webkitAnimationStart oanimationstart MSAnimationStart animationstart",end:"webkitAnimationEnd oanimationend MSAnimationEnd animationend"},"number"==typeof this.settings.offset&&(this.settings.offset={x:this.settings.offset,y:this.settings.offset}),this.init()}var s={element:"body",position:null,type:"info",allow_dismiss:!0,newest_on_top:!1,showProgressbar:!1,placement:{from:"top",align:"right"},offset:20,spacing:10,z_index:1031,delay:5e3,timer:1e3,url_target:"_blank",mouse_over:null,animate:{enter:"animated fadeInDown",exit:"animated fadeOutUp"},onShow:null,onShown:null,onClose:null,onClosed:null,icon_type:"class",template:'<div data-notify="container" class="col-xs-11 col-sm-4 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">&times;</button><span data-notify="icon"></span> <span data-notify="title">{1}</span> <span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><a href="{3}" target="{4}" data-notify="url"></a></div>'};String.format=function(){for(var t=arguments[0],e=1;e<arguments.length;e++)t=t.replace(RegExp("\\{"+(e-1)+"\\}","gm"),arguments[e]);return t},t.extend(e.prototype,{init:function(){var t=this;this.buildNotify(),this.settings.content.icon&&this.setIcon(),"#"!=this.settings.content.url&&this.styleURL(),this.placement(),this.bind(),this.notify={$ele:this.$ele,update:function(e,s){var i={};"string"==typeof e?i[e]=s:i=e;for(var e in i)switch(e){case"type":this.$ele.removeClass("alert-"+t.settings.type),this.$ele.find('[data-notify="progressbar"] > .progress-bar').removeClass("progress-bar-"+t.settings.type),t.settings.type=i[e],this.$ele.addClass("alert-"+i[e]).find('[data-notify="progressbar"] > .progress-bar').addClass("progress-bar-"+i[e]);break;case"icon":var n=this.$ele.find('[data-notify="icon"]');"class"==t.settings.icon_type.toLowerCase()?n.removeClass(t.settings.content.icon).addClass(i[e]):(n.is("img")||n.find("img"),n.attr("src",i[e]));break;case"progress":var a=t.settings.delay-t.settings.delay*(i[e]/100);this.$ele.data("notify-delay",a),this.$ele.find('[data-notify="progressbar"] > div').attr("aria-valuenow",i[e]).css("width",i[e]+"%");break;case"url":this.$ele.find('[data-notify="url"]').attr("href",i[e]);break;case"target":this.$ele.find('[data-notify="url"]').attr("target",i[e]);break;default:this.$ele.find('[data-notify="'+e+'"]').html(i[e])}var o=this.$ele.outerHeight()+parseInt(t.settings.spacing)+parseInt(t.settings.offset.y);t.reposition(o)},close:function(){t.close()}}},buildNotify:function(){var e=this.settings.content;this.$ele=t(String.format(this.settings.template,this.settings.type,e.title,e.message,e.url,e.target)),this.$ele.attr("data-notify-position",this.settings.placement.from+"-"+this.settings.placement.align),this.settings.allow_dismiss||this.$ele.find('[data-notify="dismiss"]').css("display","none"),(this.settings.delay<=0&&!this.settings.showProgressbar||!this.settings.showProgressbar)&&this.$ele.find('[data-notify="progressbar"]').remove()},setIcon:function(){"class"==this.settings.icon_type.toLowerCase()?this.$ele.find('[data-notify="icon"]').addClass(this.settings.content.icon):this.$ele.find('[data-notify="icon"]').is("img")?this.$ele.find('[data-notify="icon"]').attr("src",this.settings.content.icon):this.$ele.find('[data-notify="icon"]').append('<img src="'+this.settings.content.icon+'" alt="Notify Icon" />')},styleURL:function(){this.$ele.find('[data-notify="url"]').css({backgroundImage:"url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)",height:"100%",left:"0px",position:"absolute",top:"0px",width:"100%",zIndex:this.settings.z_index+1}),this.$ele.find('[data-notify="dismiss"]').css({position:"absolute",right:"10px",top:"5px",zIndex:this.settings.z_index+2})},placement:function(){var e=this,s=this.settings.offset.y,i={display:"inline-block",margin:"0px auto",position:this.settings.position?this.settings.position:"body"===this.settings.element?"fixed":"absolute",transition:"all .5s ease-in-out",zIndex:this.settings.z_index},n=!1,a=this.settings;switch(t('[data-notify-position="'+this.settings.placement.from+"-"+this.settings.placement.align+'"]:not([data-closing="true"])').each(function(){return s=Math.max(s,parseInt(t(this).css(a.placement.from))+parseInt(t(this).outerHeight())+parseInt(a.spacing))}),1==this.settings.newest_on_top&&(s=this.settings.offset.y),i[this.settings.placement.from]=s+"px",this.settings.placement.align){case"left":case"right":i[this.settings.placement.align]=this.settings.offset.x+"px";break;case"center":i.left=0,i.right=0}this.$ele.css(i).addClass(this.settings.animate.enter),t.each(Array("webkit","moz","o","ms",""),function(t,s){e.$ele[0].style[s+"AnimationIterationCount"]=1}),t(this.settings.element).append(this.$ele),1==this.settings.newest_on_top&&(s=parseInt(s)+parseInt(this.settings.spacing)+this.$ele.outerHeight(),this.reposition(s)),t.isFunction(e.settings.onShow)&&e.settings.onShow.call(this.$ele),this.$ele.one(this.animations.start,function(){n=!0}).one(this.animations.end,function(){t.isFunction(e.settings.onShown)&&e.settings.onShown.call(this)}),setTimeout(function(){n||t.isFunction(e.settings.onShown)&&e.settings.onShown.call(this)},600)},bind:function(){var e=this;if(this.$ele.find('[data-notify="dismiss"]').on("click",function(){e.close()}),this.$ele.mouseover(function(){t(this).data("data-hover","true")}).mouseout(function(){t(this).data("data-hover","false")}),this.$ele.data("data-hover","false"),this.settings.delay>0){e.$ele.data("notify-delay",e.settings.delay);var s=setInterval(function(){var t=parseInt(e.$ele.data("notify-delay"))-e.settings.timer;if("false"===e.$ele.data("data-hover")&&"pause"==e.settings.mouse_over||"pause"!=e.settings.mouse_over){var i=(e.settings.delay-t)/e.settings.delay*100;e.$ele.data("notify-delay",t),e.$ele.find('[data-notify="progressbar"] > div').attr("aria-valuenow",i).css("width",i+"%")}t<=-e.settings.timer&&(clearInterval(s),e.close())},e.settings.timer)}},close:function(){var e=this,s=parseInt(this.$ele.css(this.settings.placement.from)),i=!1;this.$ele.data("closing","true").addClass(this.settings.animate.exit),e.reposition(s),t.isFunction(e.settings.onClose)&&e.settings.onClose.call(this.$ele),this.$ele.one(this.animations.start,function(){i=!0}).one(this.animations.end,function(){t(this).remove(),t.isFunction(e.settings.onClosed)&&e.settings.onClosed.call(this)}),setTimeout(function(){i||(e.$ele.remove(),e.settings.onClosed&&e.settings.onClosed(e.$ele))},600)},reposition:function(e){var s=this,i='[data-notify-position="'+this.settings.placement.from+"-"+this.settings.placement.align+'"]:not([data-closing="true"])',n=this.$ele.nextAll(i);1==this.settings.newest_on_top&&(n=this.$ele.prevAll(i)),n.each(function(){t(this).css(s.settings.placement.from,e),e=parseInt(e)+parseInt(s.settings.spacing)+t(this).outerHeight()})}}),t.notify=function(t,s){var i=new e(this,t,s);return i.notify},t.notifyDefaults=function(e){return s=t.extend(!0,{},s,e)},t.notifyClose=function(e){"undefined"==typeof e||"all"==e?t("[data-notify]").find('[data-notify="dismiss"]').trigger("click"):t('[data-notify-position="'+e+'"]').find('[data-notify="dismiss"]').trigger("click")}});
/**
 * jQuery Internationalization library
 *
 * Copyright (C) 2012 Santhosh Thottingal
 *
 * jquery.i18n is dual licensed GPLv2 or later and MIT. You don't have to do
 * anything special to choose one license or the other and you don't have to
 * notify anyone which license you are using. You are free to use
 * UniversalLanguageSelector in commercial projects as long as the copyright
 * header is left intact. See files GPL-LICENSE and MIT-LICENSE for details.
 *
 * @licence GNU General Public Licence 2.0 or later
 * @licence MIT License
 */

( function ( $ ) {
    'use strict';

    var nav, I18N,
        slice = Array.prototype.slice;
    /**
     * @constructor
     * @param {Object} options
     */
    I18N = function ( options ) {
        // Load defaults
        this.options = $.extend( {}, I18N.defaults, options );

        this.parser = this.options.parser;
        this.locale = this.options.locale;
        this.messageStore = this.options.messageStore;
        this.languages = {};

        this.init();
    };

    I18N.prototype = {
        /**
         * Initialize by loading locales and setting up
         * String.prototype.toLocaleString and String.locale.
         */
        init: function () {
            var i18n = this;

            // Set locale of String environment
            String.locale = i18n.locale;

            // Override String.localeString method
            String.prototype.toLocaleString = function () {
                var localeParts, localePartIndex, value, locale, fallbackIndex,
                    tryingLocale, message;

                value = this.valueOf();
                locale = i18n.locale;
                fallbackIndex = 0;

                while ( locale ) {
                    // Iterate through locales starting at most-specific until
                    // localization is found. As in fi-Latn-FI, fi-Latn and fi.
                    localeParts = locale.split( '-' );
                    localePartIndex = localeParts.length;

                    do {
                        tryingLocale = localeParts.slice( 0, localePartIndex ).join( '-' );
                        message = i18n.messageStore.get( tryingLocale, value );

                        if ( message ) {
                            return message;
                        }

                        localePartIndex--;
                    } while ( localePartIndex );

                    if ( locale === 'en' ) {
                        break;
                    }

                    locale = ( $.i18n.fallbacks[i18n.locale] && $.i18n.fallbacks[i18n.locale][fallbackIndex] ) ||
                        i18n.options.fallbackLocale;
                    $.i18n.log( 'Trying fallback locale for ' + i18n.locale + ': ' + locale );

                    fallbackIndex++;
                }

                // key not found
                return '';
            };
        },

        /*
         * Destroy the i18n instance.
         */
        destroy: function () {
            $.removeData( document, 'i18n' );
        },

        /**
         * General message loading API This can take a URL string for
         * the json formatted messages. Example:
         * <code>load('path/to/all_localizations.json');</code>
         *
         * To load a localization file for a locale:
         * <code>
         * load('path/to/de-messages.json', 'de' );
         * </code>
         *
         * To load a localization file from a directory:
         * <code>
         * load('path/to/i18n/directory', 'de' );
         * </code>
         * The above method has the advantage of fallback resolution.
         * ie, it will automatically load the fallback locales for de.
         * For most usecases, this is the recommended method.
         * It is optional to have trailing slash at end.
         *
         * A data object containing message key- message translation mappings
         * can also be passed. Example:
         * <code>
         * load( { 'hello' : 'Hello' }, optionalLocale );
         * </code>
         *
         * A source map containing key-value pair of languagename and locations
         * can also be passed. Example:
         * <code>
         * load( {
         * bn: 'i18n/bn.json',
         * he: 'i18n/he.json',
         * en: 'i18n/en.json'
         * } )
         * </code>
         *
         * If the data argument is null/undefined/false,
         * all cached messages for the i18n instance will get reset.
         *
         * @param {String|Object} source
         * @param {String} locale Language tag
         * @returns {jQuery.Promise}
         */
        load: function ( source, locale ) {
            var fallbackLocales, locIndex, fallbackLocale, sourceMap = {};
            if ( !source && !locale ) {
                source = 'i18n/' + $.i18n().locale + '.json';
                locale = $.i18n().locale;
            }
            if ( typeof source === 'string'	&&
                source.split( '.' ).pop() !== 'json'
            ) {
                // Load specified locale then check for fallbacks when directory is specified in load()
                sourceMap[locale] = source + '/' + locale + '.json';
                fallbackLocales = ( $.i18n.fallbacks[locale] || [] )
                    .concat( this.options.fallbackLocale );
                for ( locIndex in fallbackLocales ) {
                    fallbackLocale = fallbackLocales[locIndex];
                    sourceMap[fallbackLocale] = source + '/' + fallbackLocale + '.json';
                }
                return this.load( sourceMap );
            } else {
                return this.messageStore.load( source, locale );
            }

        },

        /**
         * Does parameter and magic word substitution.
         *
         * @param {string} key Message key
         * @param {Array} parameters Message parameters
         * @return {string}
         */
        parse: function ( key, parameters ) {
            var message = key.toLocaleString();
            // FIXME: This changes the state of the I18N object,
            // should probably not change the 'this.parser' but just
            // pass it to the parser.
            this.parser.language = $.i18n.languages[$.i18n().locale] || $.i18n.languages['default'];
            if ( message === '' ) {
                message = key;
            }
            return this.parser.parse( message, parameters );
        }
    };

    /**
     * Process a message from the $.I18N instance
     * for the current document, stored in jQuery.data(document).
     *
     * @param {string} key Key of the message.
     * @param {string} param1 [param...] Variadic list of parameters for {key}.
     * @return {string|$.I18N} Parsed message, or if no key was given
     * the instance of $.I18N is returned.
     */
    $.i18n = function ( key, param1 ) {
        var parameters,
            i18n = $.data( document, 'i18n' ),
            options = typeof key === 'object' && key;

        // If the locale option for this call is different then the setup so far,
        // update it automatically. This doesn't just change the context for this
        // call but for all future call as well.
        // If there is no i18n setup yet, don't do this. It will be taken care of
        // by the `new I18N` construction below.
        // NOTE: It should only change language for this one call.
        // Then cache instances of I18N somewhere.
        if ( options && options.locale && i18n && i18n.locale !== options.locale ) {
            String.locale = i18n.locale = options.locale;
        }

        if ( !i18n ) {
            i18n = new I18N( options );
            $.data( document, 'i18n', i18n );
        }

        if ( typeof key === 'string' ) {
            if ( param1 !== undefined ) {
                parameters = slice.call( arguments, 1 );
            } else {
                parameters = [];
            }

            return i18n.parse( key, parameters );
        } else {
            // FIXME: remove this feature/bug.
            return i18n;
        }
    };

    $.fn.i18n = function () {
        var i18n = $.data( document, 'i18n' );

        if ( !i18n ) {
            i18n = new I18N();
            $.data( document, 'i18n', i18n );
        }
        String.locale = i18n.locale;
        return this.each( function () {
            var $this = $( this ),
                messageKey = $this.data( 'i18n' );

            if ( messageKey ) {
                $this.text( i18n.parse( messageKey ) );
            } else {
                $this.find( '[data-i18n]' ).i18n();
            }
        } );
    };

    String.locale = String.locale || $( 'html' ).attr( 'lang' );

    if ( !String.locale ) {
        if ( typeof window.navigator !== undefined ) {
            nav = window.navigator;
            String.locale = nav.language || nav.userLanguage || '';
        } else {
            String.locale = '';
        }
    }

    $.i18n.languages = {};
    $.i18n.messageStore = $.i18n.messageStore || {};
    $.i18n.parser = {
        // The default parser only handles variable substitution
        parse: function ( message, parameters ) {
            return message.replace( /\$(\d+)/g, function ( str, match ) {
                var index = parseInt( match, 10 ) - 1;
                return parameters[index] !== undefined ? parameters[index] : '$' + match;
            } );
        },
        emitter: {}
    };
    $.i18n.fallbacks = {};
    $.i18n.debug = false;
    $.i18n.log = function ( /* arguments */ ) {
        if ( window.console && $.i18n.debug ) {
            window.console.log.apply( window.console, arguments );
        }
    };
    /* Static members */
    I18N.defaults = {
        locale: String.locale,
        fallbackLocale: 'en',
        parser: $.i18n.parser,
        messageStore: $.i18n.messageStore
    };

    // Expose constructor
    $.i18n.constructor = I18N;
}( jQuery ) );

(function ($, $$) {

    "use strict";

    /**
     *  widgets/profile.js
     */

    // ToDO
    // ...

    $$.$({
        widgets_loader: {
            options: {
                selector: 'kt-loader-backdrop'
            },
            show: function () {
                if (!$('#' + this.options.selector).length) {
                    $('body').append($('<div/>', {
                        id: this.options.selector,
                        class: this.options.selector
                    }));
                }else{
                    this.hide();
                    console.log(this.hide());
                }
            },
            hide: function () {
                $('body').find('#' + this.options.selector).remove();
            },
            ready: function () {
            }
        }

    });

})(J, KTJS);
(function ($, $$) {

    "use strict";

    /**
     *  widgets/profile.js
     */

    // ToDO
    // ...

    $$.$({
        widgets_notify: {
            init: function () {
                $.notifyDefaults({
                    element: 'body',
                    type: 'success',
                    delay: 5000,
                    placement: {
                        from: "bottom",
                        align: "right"
                    },
                    animate: {
                        enter: 'animated bounceInUp',
                        exit: 'animated bounceOutDown'
                    }
                });
            },
            ready: function () {
                this.init();
            }
        }

    });

})(J, KTJS);
(function ($, $$) {

    "use strict";

    /**
     *  widgets/profile.js
     */

    $$.$({
        widgets_profile: {
            status: function () {
                $('.kt-profile').each(function () {
                    var $this = $(this);
                    $this.find('[data-status-value]').click(function (event) {
                        event.preventDefault();
                        $this.find('[data-status]').attr('data-status', $(this).data('status-value'));
                        return true;
                    });
                });
            },
            initial: function () {
                $('.kt-profile').each(function () {
                    var _profile = $(this).json('profile');
                    var firstname = _profile.name.substring(0, 1);
                    var lastname = _profile.lastname.substring(0, 1);
                    $(".img-circle").html(firstname + lastname);
                });
            },
            ready: function () {
                this.initial();
                this.status();
            }
        }

    });

})(J, KTJS);
(function ($, $$, W) {

    "use strict";

    /**
     *  plugin/ajaxmodal.js
     */

    // ToDO
    // ...

    $$.$({
        ajaxModal: {

            fn: function (_target, _ajax_modal, _args) {
                $$.widgets_loader.show();
                $.ajax({
                    url: _target,
                    success: function (data) {
                        $('body').append(data);
                        $$.widgets_loader.hide();
                        $('.kt-ajaxmodal')
                            .modal('show')
                            .on('hidden.bs.modal', function (e) {
                                $('.kt-ajaxmodal').remove();
                            });

                        _args.response = data;

                        if (_ajax_modal && $().check(_ajax_modal.callback)) {
                            var callbacks = $.Callbacks("once");
                            callbacks.add($().callback(_ajax_modal.callback));
                            callbacks.fire(_args);
                        }

                    },
                    error: function () {
                        $$.widgets_loader.hide();
                    }
                });
            },

            scope: function($_scope){
                $_scope = (typeof $_scope === typeof undefined)? '' : $_scope;
                $( $_scope + ' [data-toggle="ajaxmodal"]').each(function () {
                    var $this = $(this),
                        _target = (typeof $this.data('target') !== typeof undefined) ? $this.data('target') : (typeof $this.attr('href') !== typeof undefined) ? $this.attr('href') : null,
                        _ajax_modal = $(this).json('ajaxmodal'),
                        _args = $.extend($(this).json(), {$elem: $this});

                    if (_target) {
                        $this.click(function (event) {
                            event.preventDefault();
                            $$.ajaxModal.fn(_target, _ajax_modal, _args);
                        });
                    }
                });
            },

            init: function () {
                this.scope();


                $(document).ajaxSuccess(function () {
                    //console.log("Triggered ajaxSuccess handler.");
                });

                $(document).ajaxError(function () {
                    $.notify({
                            icon: 'fa fa-times',
                            message: 'Ajax Error !'
                        },
                        {
                            type: 'danger'
                        });
                });
            },

            ready: function () {
                this.init();
            }
        }

    });

})(J, KTJS, window);
(function ($, $$) {
    /**
     *  plugin/apc.js
     */

    // ToDO
    // ...

    $$.$({
        apc: {
            options: {
                active: false
            },
            clean: function () {
                var _location_href = window.location.href,
                    _redirect_href = _location_href;
                _redirect_href += (_location_href.indexOf('?') !== -1) ? '&cleanAPC=clean' : '?&cleanAPC=clean';

                $.get(_redirect_href, function (data) {
                    window.location.reload();
                });
            },
            init: function () {
                if (this.options.active) {
                    var _btnCleanApc = '<a href="#" class="btn btn-danger btn-clean-apc"><i class="fa fa-refresh"></i> </a>';
                    $('.navbar.horizonatal-menu').append(_btnCleanApc);

                    this.action();
                }
                return true;
            },
            action: function () {
                var _this = this,
                    $btnCleanApc = $('.navbar.horizonatal-menu').find('.btn-clean-apc');

                $btnCleanApc.click(function (event) {
                    event.preventDefault();
                    $btnCleanApc.find('.fa-refresh').addClass('fa-spin');
                    _this.clean();
                    return false;
                });

                $(document).keydown(function (event) {
                    if (event.keyCode == 82 && event.ctrlKey) {
                        event.preventDefault();
                        $btnCleanApc.find('.fa-refresh').addClass('fa-spin');
                        _this.clean();
                    }
                });
            },
            ready: function () {
                this.init();
            }
        }

    });

})(jQuery, KTJS);


/**
 * bootbox.js [master branch]
 *
 * http://bootboxjs.com/license.txt
 */

// @see https://github.com/makeusabrew/bootbox/issues/180
// @see https://github.com/makeusabrew/bootbox/issues/186
(function (root, factory) {

    "use strict";
    if (typeof define === "function" && define.amd) {
        // AMD. Register as an anonymous module.
        define(["jquery"], factory);
    } else if (typeof exports === "object") {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.

        if (typeof $ === "undefined") {
            module.exports = factory(require("jquery"));
        } else {
            module.exports = factory($); // jshint ignore:line
        }

    } else {
        // Browser globals (root is window)
        root.bootbox = factory(root.jQuery);
    }

}(this, function init($, undefined) {

    "use strict";

    // the base DOM structure needed to create a modal
    var templates = {
        dialog:
            "<div class='bootbox modal' tabindex='-1' role='dialog' aria-hidden='true'>" +
            "<div class='modal-dialog'>" +
            "<div class='modal-content'>" +
            "<div class='modal-body'><div class='bootbox-body'></div></div>" +
            "</div>" +
            "</div>" +
            "</div>",
        header:
            "<div class='modal-header'>" +
            "<h4 class='modal-title'></h4>" +
            "</div>",
        footer:
            "<div class='modal-footer'></div>",
        closeButton:
            "<button type='button' class='bootbox-close-button close' aria-hidden='true'>&times;</button>",
        form:
            "<form class='bootbox-form'></form>",
        inputs: {
            text:
                "<input class='bootbox-input bootbox-input-text form-control' autocomplete=off type=text />",
            textarea:
                "<textarea class='bootbox-input bootbox-input-textarea form-control'></textarea>",
            email:
                "<input class='bootbox-input bootbox-input-email form-control' autocomplete='off' type='email' />",
            select:
                "<select class='bootbox-input bootbox-input-select form-control'></select>",
            checkbox:
                "<div class='checkbox'><label><input class='bootbox-input bootbox-input-checkbox' type='checkbox' /></label></div>",
            date:
                "<input class='bootbox-input bootbox-input-date form-control' autocomplete=off type='date' />",
            time:
                "<input class='bootbox-input bootbox-input-time form-control' autocomplete=off type='time' />",
            number:
                "<input class='bootbox-input bootbox-input-number form-control' autocomplete=off type='number' />",
            password:
                "<input class='bootbox-input bootbox-input-password form-control' autocomplete='off' type='password' />"
        }
    };

    var defaults = {
        // default language
        locale: "en",
        // show backdrop or not. Default to static so user has to interact with dialog
        backdrop: "static",
        // animate the modal in/out
        animate: true,
        // additional class string applied to the top level dialog
        className: null,
        // whether or not to include a close button
        closeButton: true,
        // show the dialog immediately by default
        show: true,
        // dialog container
        container: "body"
    };

    // our public object; augmented after our private API
    var exports = {};

    /**
     * @private
     */
    function _t(key) {
        var locale = locales[defaults.locale];
        return locale ? locale[key] : locales.en[key];
    }

    function processCallback(e, dialog, callback) {
        e.stopPropagation();
        e.preventDefault();

        // by default we assume a callback will get rid of the dialog,
        // although it is given the opportunity to override this

        // so, if the callback can be invoked and it *explicitly returns false*
        // then we'll set a flag to keep the dialog active...
        var preserveDialog = $.isFunction(callback) && callback.call(dialog, e) === false;

        // ... otherwise we'll bin it
        if (!preserveDialog) {
            dialog.modal("hide");
        }
    }

    // Bootstrap 3.x supports back to IE8 on Windows (http://getbootstrap.com/getting-started/#support)
    // so unfortunately we can't just get away with assuming Object.keys exists
    function getKeyLength(obj) {
        if (Object.keys) {
            return Object.keys(obj).length;
        }

        var k, t = 0;
        for (k in obj) {
            t ++;
        }
        return t;
    }

    // tiny wrapper function around jQuery.each; just adds index as the third parameter
    function each(collection, iterator) {
        var index = 0;
        $.each(collection, function(key, value) {
            iterator(key, value, index++);
        });
    }

    /**
     * Filter and tidy up any user supplied parameters to this dialog.
     * Also looks for any shorthands used and ensures that the options
     * which are returned are all normalized properly
     */
    function sanitize(options) {
        var buttons;
        var total;

        if (typeof options !== "object") {
            throw new Error("Please supply an object of options");
        }

        if (!options.message) {
            throw new Error("Please specify a message");
        }

        // make sure any supplied options take precedence over defaults
        options = $.extend({}, defaults, options);

        // no buttons is still a valid dialog but it's cleaner  toalways have
        // a buttons object to iterate over, even if it's empty
        if (!options.buttons) {
            options.buttons = {};
        }

        buttons = options.buttons;

        total = getKeyLength(buttons);

        each(buttons, function(key, button, index) {
            var isLast = index === total-1;

            if ($.isFunction(button)) {
                // short form, assume value is our callback. Since button
                // isn't an object it isn't a reference either so re-assign it
                button = buttons[key] = {
                    callback: button
                };
            }

            // before any further checks make sure by now button is the correct type
            if ($.type(button) !== "object") {
                throw new Error("button with key " + key + " must be an object");
            }

            if (!button.label) {
                // the lack of an explicit label means we'll assume the key is good enough
                button.label = key;
            }

            if (!button.className) {
                if (total <= 2 && isLast) {
                    // always add a primary to the main option in a one or two-button dialog
                    button.className = "btn-primary";
                } else {
                    button.className = "btn-default";
                }
            }
        });

        return options;
    }

    /**
     * map a flexible set of arguments into a single returned object
     * if args.length is already one just return it, otherwise
     * use the properties argument to map the unnamed args to
     * object properties
     * so in the latter case:
     * mapArguments(["foo", $.noop], ["message", "callback"])
     * -> { message: "foo", callback: $.noop }
     */
    function mapArguments(args, properties) {
        var argn = args.length;
        var options = {};

        if (argn < 1 || argn > 2) {
            throw new Error("Invalid argument length");
        }

        if (argn === 2 || typeof args[0] === "string") {
            options[properties[0]] = args[0];
            options[properties[1]] = args[1];
        } else {
            options = args[0];
        }

        return options;
    }

    /**
     * merge a set of default dialog options with user supplied arguments
     */
    function mergeArguments(defaults, args, properties) {
        return $.extend(
            // deep merge
            true,
            // ensure the target is an empty, unreferenced object
            {},
            // the base options object for this type of dialog (often just buttons)
            defaults,
            // args could be an object or array; if it's an array properties will
            // map it to a proper options object
            mapArguments(
                args,
                properties
            )
        );
    }

    /**
     * this entry-level method makes heavy use of composition to take a simple
     * range of inputs and return valid options suitable for passing to bootbox.dialog
     */
    function mergeDialogOptions(className, labels, properties, args) {
        //  build up a base set of dialog properties
        var baseOptions = {
            className: "bootbox-" + className,
            buttons: createLabels.apply(null, labels)
        };

        // ensure the buttons properties generated, *after* merging
        // with user args are still valid against the supplied labels
        return validateButtons(
            // merge the generated base properties with user supplied arguments
            mergeArguments(
                baseOptions,
                args,
                // if args.length > 1, properties specify how each arg maps to an object key
                properties
            ),
            labels
        );
    }

    /**
     * from a given list of arguments return a suitable object of button labels
     * all this does is normalise the given labels and translate them where possible
     * e.g. "ok", "confirm" -> { ok: "OK", cancel: "Annuleren" }
     */
    function createLabels() {
        var buttons = {};

        for (var i = 0, j = arguments.length; i < j; i++) {
            var argument = arguments[i];
            var key = argument.toLowerCase();
            var value = argument.toUpperCase();

            buttons[key] = {
                label: _t(value)
            };
        }

        return buttons;
    }

    function validateButtons(options, buttons) {
        var allowedButtons = {};
        each(buttons, function(key, value) {
            allowedButtons[value] = true;
        });

        each(options.buttons, function(key) {
            if (allowedButtons[key] === undefined) {
                throw new Error("button key " + key + " is not allowed (options are " + buttons.join("\n") + ")");
            }
        });

        return options;
    }

    exports.alert = function() {
        var options;

        options = mergeDialogOptions("alert", ["ok"], ["message", "callback"], arguments);

        // @TODO: can this move inside exports.dialog when we're iterating over each
        // button and checking its button.callback value instead?
        if (options.callback && !$.isFunction(options.callback)) {
            throw new Error("alert requires callback property to be a function when provided");
        }

        /**
         * override the ok and escape callback to make sure they just invoke
         * the single user-supplied one (if provided)
         */
        options.buttons.ok.callback = options.onEscape = function() {
            if ($.isFunction(options.callback)) {
                return options.callback.call(this);
            }
            return true;
        };

        return exports.dialog(options);
    };

    exports.confirm = function() {
        var options;

        options = mergeDialogOptions("confirm", ["cancel", "confirm"], ["message", "callback"], arguments);

        // confirm specific validation; they don't make sense without a callback so make
        // sure it's present
        if (!$.isFunction(options.callback)) {
            throw new Error("confirm requires a callback");
        }

        /**
         * overrides; undo anything the user tried to set they shouldn't have
         */
        options.buttons.cancel.callback = options.onEscape = function() {
            return options.callback.call(this, false);
        };

        options.buttons.confirm.callback = function() {
            return options.callback.call(this, true);
        };

        return exports.dialog(options);
    };

    exports.prompt = function() {
        var options;
        var defaults;
        var dialog;
        var form;
        var input;
        var shouldShow;
        var inputOptions;

        // we have to create our form first otherwise
        // its value is undefined when gearing up our options
        // @TODO this could be solved by allowing message to
        // be a function instead...
        form = $(templates.form);

        // prompt defaults are more complex than others in that
        // users can override more defaults
        // @TODO I don't like that prompt has to do a lot of heavy
        // lifting which mergeDialogOptions can *almost* support already
        // just because of 'value' and 'inputType' - can we refactor?
        defaults = {
            className: "bootbox-prompt",
            buttons: createLabels("cancel", "confirm"),
            value: "",
            inputType: "text"
        };

        options = validateButtons(
            mergeArguments(defaults, arguments, ["title", "callback"]),
            ["cancel", "confirm"]
        );

        // capture the user's show value; we always set this to false before
        // spawning the dialog to give us a chance to attach some handlers to
        // it, but we need to make sure we respect a preference not to show it
        shouldShow = (options.show === undefined) ? true : options.show;

        /**
         * overrides; undo anything the user tried to set they shouldn't have
         */
        options.message = form;

        options.buttons.cancel.callback = options.onEscape = function() {
            return options.callback.call(this, null);
        };

        options.buttons.confirm.callback = function() {
            var value;

            if (options.inputType === "checkbox") {
                value = input.find("input:checked").map(function() {
                    return $(this).val();
                }).get();
            } else {
                value = input.val();
            }

            return options.callback.call(this, value);
        };

        options.show = false;

        // prompt specific validation
        if (!options.title) {
            throw new Error("prompt requires a title");
        }

        if (!$.isFunction(options.callback)) {
            throw new Error("prompt requires a callback");
        }

        if (!templates.inputs[options.inputType]) {
            throw new Error("invalid prompt type");
        }

        // create the input based on the supplied type
        input = $(templates.inputs[options.inputType]);

        switch (options.inputType) {
            case "text":
            case "textarea":
            case "email":
            case "date":
            case "time":
            case "number":
            case "password":
                input.val(options.value);
                break;

            case "select":
                var groups = {};
                inputOptions = options.inputOptions || [];

                if (!$.isArray(inputOptions)) {
                    throw new Error("Please pass an array of input options");
                }

                if (!inputOptions.length) {
                    throw new Error("prompt with select requires options");
                }

                each(inputOptions, function(_, option) {

                    // assume the element to attach to is the input...
                    var elem = input;

                    if (option.value === undefined || option.text === undefined) {
                        throw new Error("each option needs a `value` and a `text` property");
                    }

                    // ... but override that element if this option sits in a group

                    if (option.group) {
                        // initialise group if necessary
                        if (!groups[option.group]) {
                            groups[option.group] = $("<optgroup/>").attr("label", option.group);
                        }

                        elem = groups[option.group];
                    }

                    elem.append("<option value='" + option.value + "'>" + option.text + "</option>");
                });

                each(groups, function(_, group) {
                    input.append(group);
                });

                // safe to set a select's value as per a normal input
                input.val(options.value);
                break;

            case "checkbox":
                var values   = $.isArray(options.value) ? options.value : [options.value];
                inputOptions = options.inputOptions || [];

                if (!inputOptions.length) {
                    throw new Error("prompt with checkbox requires options");
                }

                if (!inputOptions[0].value || !inputOptions[0].text) {
                    throw new Error("each option needs a `value` and a `text` property");
                }

                // checkboxes have to nest within a containing element, so
                // they break the rules a bit and we end up re-assigning
                // our 'input' element to this container instead
                input = $("<div/>");

                each(inputOptions, function(_, option) {
                    var checkbox = $(templates.inputs[options.inputType]);

                    checkbox.find("input").attr("value", option.value);
                    checkbox.find("label").append(option.text);

                    // we've ensured values is an array so we can always iterate over it
                    each(values, function(_, value) {
                        if (value === option.value) {
                            checkbox.find("input").prop("checked", true);
                        }
                    });

                    input.append(checkbox);
                });
                break;
        }

        // @TODO provide an attributes option instead
        // and simply map that as keys: vals
        if (options.placeholder) {
            input.attr("placeholder", options.placeholder);
        }

        if (options.pattern) {
            input.attr("pattern", options.pattern);
        }

        if (options.maxlength) {
            input.attr("maxlength", options.maxlength);
        }

        // now place it in our form
        form.append(input);

        form.on("submit", function(e) {
            e.preventDefault();
            // Fix for SammyJS (or similar JS routing library) hijacking the form post.
            e.stopPropagation();
            // @TODO can we actually click *the* button object instead?
            // e.g. buttons.confirm.click() or similar
            dialog.find(".btn-primary").click();
        });

        dialog = exports.dialog(options);

        // clear the existing handler focusing the submit button...
        dialog.off("shown.bs.modal");

        // ...and replace it with one focusing our input, if possible
        dialog.on("shown.bs.modal", function() {
            // need the closure here since input isn't
            // an object otherwise
            input.focus();
        });

        if (shouldShow === true) {
            dialog.modal("show");
        }

        return dialog;
    };

    exports.dialog = function(options) {
        options = sanitize(options);

        var dialog = $(templates.dialog);
        var innerDialog = dialog.find(".modal-dialog");
        var body = dialog.find(".modal-body");
        var buttons = options.buttons;
        var buttonStr = "";
        var callbacks = {
            onEscape: options.onEscape
        };

        if ($.fn.modal === undefined) {
            throw new Error(
                "$.fn.modal is not defined; please double check you have included " +
                "the Bootstrap JavaScript library. See http://getbootstrap.com/javascript/ " +
                "for more details."
            );
        }

        each(buttons, function(key, button) {

            // @TODO I don't like this string appending to itself; bit dirty. Needs reworking
            // can we just build up button elements instead? slower but neater. Then button
            // can just become a template too
            buttonStr += "<button data-bb-handler='" + key + "' type='button' class='btn " + button.className + "'>" + button.label + "</button>";
            callbacks[key] = button.callback;
        });

        body.find(".bootbox-body").html(options.message);

        if (options.animate === true) {
            dialog.addClass("fade");
        }

        if (options.className) {
            dialog.addClass(options.className);
        }

        if (options.size === "large") {
            innerDialog.addClass("modal-lg");
        } else if (options.size === "small") {
            innerDialog.addClass("modal-sm");
        }

        if (options.title) {
            body.before(templates.header);
        }

        if (options.closeButton) {
            var closeButton = $(templates.closeButton);

            if (options.title) {
                dialog.find(".modal-header").prepend(closeButton);
            } else {
                closeButton.css("margin-top", "-2px").prependTo(body);
            }
        }

        if (options.title) {
            dialog.find(".modal-title").html(options.title);
        }

        if (buttonStr.length) {
            body.after(templates.footer);
            dialog.find(".modal-footer").html(buttonStr);
        }


        /**
         * Bootstrap event listeners; these handle extra
         * setup & teardown required after the underlying
         * modal has performed certain actions
         */

        // make sure we unbind any listeners once the dialog has definitively been dismissed
        dialog.one("hide.bs.modal", function() {
            dialog.off("escape.close.bb");
            dialog.off("click");
        });

        dialog.one("hidden.bs.modal", function(e) {
            // ensure we don't accidentally intercept hidden events triggered
            // by children of the current dialog. We shouldn't anymore now BS
            // namespaces its events; but still worth doing
            if (e.target === this) {
                dialog.remove();
            }
        });

        /*
         dialog.on("show.bs.modal", function() {
         // sadly this doesn't work; show is called *just* before
         // the backdrop is added so we'd need a setTimeout hack or
         // otherwise... leaving in as would be nice
         if (options.backdrop) {
         dialog.next(".modal-backdrop").addClass("bootbox-backdrop");
         }
         });
         */

        dialog.one("shown.bs.modal", function() {
            dialog.find(".btn-primary:first").focus();
        });

        /**
         * Bootbox event listeners; used to decouple some
         * behaviours from their respective triggers
         */

        if (options.backdrop !== "static") {
            // A boolean true/false according to the Bootstrap docs
            // should show a dialog the user can dismiss by clicking on
            // the background.
            // We always only ever pass static/false to the actual
            // $.modal function because with `true` we can't trap
            // this event (the .modal-backdrop swallows it)
            // However, we still want to sort of respect true
            // and invoke the escape mechanism instead
            dialog.on("click.dismiss.bs.modal", function(e) {
                // @NOTE: the target varies in >= 3.3.x releases since the modal backdrop
                // moved *inside* the outer dialog rather than *alongside* it
                if (dialog.children(".modal-backdrop").length) {
                    e.currentTarget = dialog.children(".modal-backdrop").get(0);
                }

                if (e.target !== e.currentTarget) {
                    return;
                }

                dialog.trigger("escape.close.bb");
            });
        }

        dialog.on("escape.close.bb", function(e) {
            // the if statement looks redundant but it isn't; without it
            // if we *didn't* have an onEscape handler then processCallback
            // would automatically dismiss the dialog
            if (callbacks.onEscape) {
                processCallback(e, dialog, callbacks.onEscape);
            }
        });

        /**
         * Standard jQuery event listeners; used to handle user
         * interaction with our dialog
         */

        dialog.on("click", ".modal-footer button", function(e) {
            var callbackKey = $(this).data("bb-handler");

            processCallback(e, dialog, callbacks[callbackKey]);
        });

        dialog.on("click", ".bootbox-close-button", function(e) {
            // onEscape might be falsy but that's fine; the fact is
            // if the user has managed to click the close button we
            // have to close the dialog, callback or not
            processCallback(e, dialog, callbacks.onEscape);
        });

        dialog.on("keyup", function(e) {
            if (e.which === 27) {
                dialog.trigger("escape.close.bb");
            }
        });

        // the remainder of this method simply deals with adding our
        // dialogent to the DOM, augmenting it with Bootstrap's modal
        // functionality and then giving the resulting object back
        // to our caller

        $(options.container).append(dialog);

        dialog.modal({
            backdrop: options.backdrop ? "static": false,
            keyboard: false,
            show: false
        });

        if (options.show) {
            dialog.modal("show");
        }

        // @TODO should we return the raw element here or should
        // we wrap it in an object on which we can expose some neater
        // methods, e.g. var d = bootbox.alert(); d.hide(); instead
        // of d.modal("hide");

        /*
         function BBDialog(elem) {
         this.elem = elem;
         }

         BBDialog.prototype = {
         hide: function() {
         return this.elem.modal("hide");
         },
         show: function() {
         return this.elem.modal("show");
         }
         };
         */

        return dialog;

    };

    exports.setDefaults = function() {
        var values = {};

        if (arguments.length === 2) {
            // allow passing of single key/value...
            values[arguments[0]] = arguments[1];
        } else {
            // ... and as an object too
            values = arguments[0];
        }

        $.extend(defaults, values);
    };

    exports.hideAll = function() {
        $(".bootbox").modal("hide");

        return exports;
    };


    /**
     * standard locales. Please add more according to ISO 639-1 standard. Multiple language variants are
     * unlikely to be required. If this gets too large it can be split out into separate JS files.
     */
    var locales = {
        ar : {
            OK      : "موافق",
            CANCEL  : "الغاء",
            CONFIRM : "تأكيد"
        },
        bg_BG : {
            OK      : "Ок",
            CANCEL  : "Отказ",
            CONFIRM : "Потвърждавам"
        },
        br : {
            OK      : "OK",
            CANCEL  : "Cancelar",
            CONFIRM : "Sim"
        },
        cs : {
            OK      : "OK",
            CANCEL  : "Zrušit",
            CONFIRM : "Potvrdit"
        },
        da : {
            OK      : "OK",
            CANCEL  : "Annuller",
            CONFIRM : "Accepter"
        },
        de : {
            OK      : "OK",
            CANCEL  : "Abbrechen",
            CONFIRM : "Akzeptieren"
        },
        el : {
            OK      : "Εντάξει",
            CANCEL  : "Ακύρωση",
            CONFIRM : "Επιβεβαίωση"
        },
        en : {
            OK      : "OK",
            CANCEL  : "Cancel",
            CONFIRM : "OK"
        },
        es : {
            OK      : "OK",
            CANCEL  : "Cancelar",
            CONFIRM : "Aceptar"
        },
        eu : {
            OK      : "OK",
            CANCEL  : "Ezeztatu",
            CONFIRM : "Onartu"
        },
        et : {
            OK      : "OK",
            CANCEL  : "Katkesta",
            CONFIRM : "OK"
        },
        fa : {
            OK      : "قبول",
            CANCEL  : "لغو",
            CONFIRM : "تایید"
        },
        fi : {
            OK      : "OK",
            CANCEL  : "Peruuta",
            CONFIRM : "OK"
        },
        fr : {
            OK      : "OK",
            CANCEL  : "Annuler",
            CONFIRM : "Confirmer"
        },
        he : {
            OK      : "אישור",
            CANCEL  : "ביטול",
            CONFIRM : "אישור"
        },
        hu : {
            OK      : "OK",
            CANCEL  : "Mégsem",
            CONFIRM : "Megerősít"
        },
        hr : {
            OK      : "OK",
            CANCEL  : "Odustani",
            CONFIRM : "Potvrdi"
        },
        id : {
            OK      : "OK",
            CANCEL  : "Batal",
            CONFIRM : "OK"
        },
        it : {
            OK      : "OK",
            CANCEL  : "Annulla",
            CONFIRM : "Conferma"
        },
        ja : {
            OK      : "OK",
            CANCEL  : "キャンセル",
            CONFIRM : "確認"
        },
        lt : {
            OK      : "Gerai",
            CANCEL  : "Atšaukti",
            CONFIRM : "Patvirtinti"
        },
        lv : {
            OK      : "Labi",
            CANCEL  : "Atcelt",
            CONFIRM : "Apstiprināt"
        },
        nl : {
            OK      : "OK",
            CANCEL  : "Annuleren",
            CONFIRM : "Accepteren"
        },
        no : {
            OK      : "OK",
            CANCEL  : "Avbryt",
            CONFIRM : "OK"
        },
        pl : {
            OK      : "OK",
            CANCEL  : "Anuluj",
            CONFIRM : "Potwierdź"
        },
        pt : {
            OK      : "OK",
            CANCEL  : "Cancelar",
            CONFIRM : "Confirmar"
        },
        ru : {
            OK      : "OK",
            CANCEL  : "Отмена",
            CONFIRM : "Применить"
        },
        sk : {
            OK      : "OK",
            CANCEL  : "Zrušiť",
            CONFIRM : "Potvrdiť"
        },
        sl : {
            OK : "OK",
            CANCEL : "Prekliči",
            CONFIRM : "Potrdi"
        },
        sq : {
            OK : "OK",
            CANCEL : "Anulo",
            CONFIRM : "Prano"
        },
        sv : {
            OK      : "OK",
            CANCEL  : "Avbryt",
            CONFIRM : "OK"
        },
        th : {
            OK      : "ตกลง",
            CANCEL  : "ยกเลิก",
            CONFIRM : "ยืนยัน"
        },
        tr : {
            OK      : "Tamam",
            CANCEL  : "İptal",
            CONFIRM : "Onayla"
        },
        zh_CN : {
            OK      : "OK",
            CANCEL  : "取消",
            CONFIRM : "确认"
        },
        zh_TW : {
            OK      : "OK",
            CANCEL  : "取消",
            CONFIRM : "確認"
        }
    };

    exports.addLocale = function(name, values) {
        $.each(["OK", "CANCEL", "CONFIRM"], function(_, v) {
            if (!values[v]) {
                throw new Error("Please supply a translation for '" + v + "'");
            }
        });

        locales[name] = {
            OK: values.OK,
            CANCEL: values.CANCEL,
            CONFIRM: values.CONFIRM
        };

        return exports;
    };

    exports.removeLocale = function(name) {
        delete locales[name];

        return exports;
    };

    exports.setLocale = function(name) {
        return exports.setDefaults("locale", name);
    };

    exports.init = function(_$) {
        return init(_$ || $);
    };

    return exports;
}));
(function ($, $$) {

    "use strict";

    /**
     *  plugin/bootstrap.js
     */

    // ToDO
    // ...

    $$.$({
        bootstrap: {

            noConflict: function () {
                $('[data-toggle="tooltip"]').tooltip({
                    template : '<div class="tooltip"></div></div>'
                });

                $('[data-toggle="popover"]').popover({
                    html: true,
                    content: function () {
                        var _content = $(this).attr('title');
                        if ($(this).data('target')) {
                            _content = $($(this).data('target')).html();
                        }
                        return _content;
                    }
                });

                // 6.2.3  Pour chaque lien composite ayant un titre de lien (attribut title), le contenu de cet attribut est-il pertinent ?
                $('[data-toggle="popover"]').each(function(){
                    $(this).attr('title', $(this).attr('data-original-title'));
                });



                $('body').on('click', function (e) {
                    $('[data-toggle="popover"]').each(function () {
                        //the 'is' for buttons that trigger popups
                        //the 'has' for icons within a button that triggers a popup
                        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                            $(this).popover('hide');
                        }
                    });
                });


                // To add title in CodeCpv input
                // I don't have access to chosen component generator
                window.setTimeout(function(){
                    $('#chozen_container__0').attr('title', $('#PAGE_AdvancedSearch_referentielCPV_panelCpv_label_cpv').text());
                    $('#chozen_container__0_chzn .search-field input[type=text]').attr('title', $('#PAGE_AdvancedSearch_referentielCPV_panelCpv_label_cpv').text());
                }, 1000);



            },
            ready: function () {
                this.noConflict();
            }
        }

    });

})(J, KTJS);
(function ($, $$) {
    /**
     *  plugin/datetimepicker.js
     */

    // ToDO
    // ...

})(jQuery, KTJS);


(function ($, $$) {
    /**
     *  plugin/footer.js
     */

    // ToDO
    // ...

    $$.$({
        footer: {
            fn1: function () {
                console.log('FN footer !');
            },
            ready: function () {
                //console.log(this);
                //this.fn1();
                //this.fn1();
            }
        }

    });

})(jQuery, KTJS);


(function ($, $$) {
    /**
     *  plugin/form.js
     */

    // ToDO
    // ...

    $$.$({
        forms: {
            ready: function () {

            }
        }
    });

})(jQuery, KTJS);


(function ($, $$) {
    /**
     *  plugin/header.js
     */

    // ToDO
    // ...

    $$.$({
        header: {
            affix: function () {

                $(window).scroll(function () {

                    $('.hero-section-affix').affix({
                        offset: {
                            top: 180
                        }
                    })
                        .on('affix.bs.affix', function () {
                            $('body').addClass('atx-hero-section-affix');
                        })
                        .on('affix-top.bs.affix', function () {
                            $('body').removeClass('atx-hero-section-affix');
                        });

                });

            },
            ready: function () {
                //console.log(this);
                this.affix();
            }
        }
    });

})(jQuery, KTJS);
/**
 * Created by sarah.bo on 15/06/2017.
 */
(function ($, $$) {
    /**
     *  plugin/home.js
     */

    // ToDO
    // ...

    $$.$({
        home: {
            flip: function () {

                // Comment
                $('.card').hover(function () {
                    $(this).toggleClass('flipped');
                });

            },
            ready: function () {
                //console.log(this);
                this.flip();
            }
        }
    });

})(jQuery, KTJS);
(function ($, $$) {
    /**
     *  plugin/initPlugin.js
     */

    // ToDO
    // ...

    $$.$({
        initPlugin: {
            plugins: {
                list: []
            },
            init: function ($scope, arg) {
                $('[data-init-plugin]').each(function () {
                    $(this).initPlugin();
                });
            },
            ready: function () {
                this.init();
            }
        }
    });

})(jQuery, KTJS);
(function ($, $$) {
    /**
     *  plugin/scrollbar.js
     */

    // ToDO
    // ...

    $$.$({
        scrollbar: {
            initPlugin: function ($scope, arg) {
                var _arg = (!arg)? {} : arg;
                $scope.scrollbar(_arg);
            },
            ready: function () {
                return true;
            }
        }
    });

})(jQuery, KTJS);
(function ($, $$) {

    "use strict";

    $$.$({
        pages_accueil_entreprise: {
            modal: function () {
                window.onload = function () {
                    if (localStorage.length === 0) {
                        $('#modal-info').modal('show');
                        localStorage.setItem("hasCodeRunBefore", true);
                    } else {
                        $('#information').css('display', 'block');
                    }
                    $("#modal-info").on("hidden.bs.modal", function () {
                        $('#information').css('display', 'block');
                    });
                };
            },
            message_accueil: function () {
                var bodyHeight = 40;
                var _ma_alert_body_height = $('.alert-msg-accueil .alert-body-content').height();
                if (_ma_alert_body_height <= bodyHeight) {
                    bodyHeight = 15;
                }

                if (_ma_alert_body_height > bodyHeight) {


                    //$('.alert-msg-accueil').addClass('in').attr('aria-expanded', true);

                    $('.alert-msg-accueil .alert-body-content').height(_ma_alert_body_height / 2);
                    $('.alert-msg-accueil .btn-circle-toggle').click(function (event) {
                        event.preventDefault();
                        $(this).blur();
                        if ($('.alert-msg-accueil').hasClass('in')) {
                            $('.alert-msg-accueil').removeClass('in').attr('aria-expanded', false);
                            $('.alert-msg-accueil .alert-body-content').height(bodyHeight);
                        } else {
                            $('.alert-msg-accueil').addClass('in').attr('aria-expanded', true);
                            $('.alert-msg-accueil .alert-body-content').height('auto');
                        }
                    });
                }
            },
            map: function () {
                if (typeof onInitMap !== 'undefined' && $.isFunction(onInitMap)) {
                    $('body').append('<div class="tooltip top" role="map-tooltip" id="id-map-tooltip" style="opacity: 1; -webkit-transition: all .01s ease-in; -moz-transition: all .01s ease-in; -o-transition: all .01s ease-in; transition: all .01s ease-in;"> <div class="tooltip-arrow" style="-webkit-transition: all .01s ease-in; -moz-transition: all .01s ease-in; -o-transition: all .01s ease-in; transition: all .01s ease-in;"></div> <div class="tooltip-inner" style="-webkit-transition: all .01s ease-in; -moz-transition: all .01s ease-in; -o-transition: all .01s ease-in; transition: all .01s ease-in; border-style: solid; border-color: #000; border-width: 1px;"></div> </div>');
                    $('body').find('#id-map-tooltip').hide();
                    onInitMap();

                    var MESSAGE_CARTE = $('#paper').data('tite');
                    $('#paper svg').attr('role', 'img').attr('title', MESSAGE_CARTE).attr('aria-label', MESSAGE_CARTE);

                }
            },
            ready: function () {
                //this.modal();
                this.message_accueil();
                this.map();
            }
        }

    });

})(jQuery, KTJS);



(function ($, $$) {

    "use strict";

    /**
     *  pages/entreprise/AdvancedSearch.js
     */

    // ToDO
    // ...

    $$.$({
        pages_entreprise_advancedsearch: {

            // Togle type of search [Recherche avancee | Recherche d une procedure restreinte]
            toggle_search: function () {
                var url = $.jurlp(document.URL);

                if (url.query().type && url.query().type === 'restreinte') {
                    $('.content-advancedSearch a[aria-controls=restreinte]').tab('show');
                }
            },

            search: {
                callback: {
                    add_lieu_execution: function () {
                        console.log('lieu d execution ');
                    }
                }

            },

            consultation: {
                callback: {
                    add: function (_args) {
                        _args.$elem.parent().remove();
                        return true;
                    },
                    delete: function (_args) {
                        $$.ajaxModal.scope('#' +
                            'modalGestionPanier ');
                    },
                    deleteConfirmation: function (_args) {
                        var _response;
                        if (_args.data.target.indexOf("=") !== -1) {
                            $(_args.data.target.replace(/=/gi, '\\=')).hide('slow');
                        } else {
                            $(_args.data.target).hide('slow');
                        }
                        $('#modalGestionPanier').modal('hide');
                        $.notify({
                            icon: 'fa fa-check',
                            message: 'La consultation a été supprimée !'
                        });
                    },
                    addToBasket: function (_args) {
                        $(_args.data.btnAddBasket).removeAttr('href').click(function (e) {
                            e.preventDefault();
                        }).off('click').find('.btn').addClass('disabled');
                    }
                },

                init: function () {

                }
            },

            ready: function () {
                this.toggle_search();
                this.consultation.init();
            }
        }

    });

})(jQuery, KTJS);


(function ($, $$) {

    "use strict";

    /**
     *  pages/entreprise/common.js
     */

    // ToDO
    // Add message i18n

    $$.$({
        pages_entreprise_common: {

            common: {

                init: function () {

                    // IsConnected
                    $('[href=isConnected]').click(function (event) {
                        event.preventDefault();

                        var $this = $(this),
                            _bootbox = $(this).json('bootbox'),
                            _data = $(this).json('data'),
                            _dialogIsConnected = null;

                        _dialogIsConnected = bootbox.confirm({
                            message: '<div class="' + _bootbox.class + '">' + atob(_bootbox.message) + '</div>',
                            className: _bootbox.class_name,
                            closeButton: false,
                            buttons: {
                                confirm: {
                                    label: atob(_bootbox.btn_confirm),
                                    className: 'btn-primary'
                                },
                                cancel: {
                                    label: atob(_bootbox.btn_cancel),
                                    className: 'btn-default'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    //$(_data.target).hide('slow');
                                    document.location.href = "?page=Entreprise.EntrepriseHome&goto=&page=entreprise.EntrepriseAccueilAuthentifie&inscription";
                                    return true;
                                }

                            }
                        });

                        return false;


                    });

                    $('.button-quickSearch').click(function (event) {
                        var _input_val = $('.input-quickSearch').val();
                        if (!$().check(_input_val)) {
                            event.preventDefault();
                            return false;
                        }
                        return true;
                    });

                },
                openModal: function (modal_selector) {
                    $(modal_selector).modal();
                },
                closeModal: function (modal_selector) {
                    $(modal_selector).modal(('hide'));
                },
                initScrollToTop: function () {
                    // ===== Scroll to Top ====
                    var testScroll = true;


                    if ($(window).scrollTop() >= 50) {
                        $('#js-footer_go_top').removeClass('out').addClass('in');
                    }

                    $(window).scroll(function () {
                        if (testScroll) {
                            if ($(this).scrollTop() >= 50) {        // If page is scrolled more than 50px
                                $('#js-footer_go_top').removeClass('out').addClass('in');
                                $('#js-btn-go_to_top').removeClass('out').addClass('in');    // Fade in the arrow
                            } else {
                                $('#js-btn-go_to_top').removeClass('in').addClass('out');   // Else fade out the arrow
                                $('#js-footer_go_top').removeClass('in').addClass('out');
                            }
                        }
                    });
                    $('#js-btn-go_to_top').click(function () {
                        $('#js-btn-go_to_top').removeClass('in').addClass('out');
                        $('#js-btn-go_to_top').tooltip('hide');
                        testScroll = false;
                        $('body,html').animate({
                            scrollTop: 0                       // Scroll to top of body
                        }, 500, function () {
                            $('#js-btn-go_to_top').addClass('animated');
                            $('#js-footer_go_top').removeClass('in').addClass('out');
                            testScroll = true;
                        });
                    });
                }


            },
            ready: function () {
                this.common.init();
                this.common.initScrollToTop();
            }
        }

    });

})(jQuery, KTJS);

var offsetHeaderTop = J('header').height();

J('#horizonatal-menu').attr('data-offset-top', offsetHeaderTop)

// Navbar fix scrollable height on mobile
J(".navbar-collapse").css({ maxHeight: J(window).height() - J(".navbar-header").height() + "px" });
// Toggle the dropdown mobile version
J(".horizonatal-menu .mobile-search").click(function(e){
    e.stopPropagation();
    J(".atx-popover-markup-login-usr").toggleClass("open");
});


(function ($, $$) {

    "use strict";

    /**
     *  pages/entreprise/common.js
     */

    // ToDO
    // Not used

    $$.$({
        pages_entreprise_gestion_recherche: {

            alertes: {

                events: function () {

                }


            },
            ready: function () {
                this.alertes.events();
            }
        }

    });

})(jQuery, KTJS);


(function ($, $$) {

    "use strict";

    /**
     *  plugin/footer.js
     */

    // ToDO
    // Add message i18n

    $$.$({
        pages_entreprise_inscription_user: {
            etablissement: {
                callback: {
                    update: function (_arg) {

                        var $form = $('#modalEtablissement .form');

                        $.validator.setDefaults({
                            submitHandler: function () {

                                $.ajax({
                                    url: $form.attr('action'),
                                    data: $form.serialize(),
                                    success: function (data) {
                                        if ($().check(data)) {

                                            var _data_parse = JSON.parse(data);

                                            $('.kt-ajaxmodal')
                                                .modal('hide')
                                                .on('hidden.bs.modal', function () {

                                                    $.notify({
                                                        icon: 'fa fa-check',
                                                        message: 'La modification de vos donn&eacute;es a &eacute;t&eacute; effectu&eacute;e avec succ&egrave;s.'
                                                    });

                                                    //$('#row-etablissement-' + _data_parse.IdEtablissement).addClass('animated flash');
                                                    $(_arg.data.target).addClass('animated flash');

                                                    $.each(_data_parse, function (key, val) {
                                                        $('[data-binding=' + key + ']', _arg.data.target).text(val);
                                                    });
                                                });

                                        }
                                    }
                                });

                            }
                        });

                        $form.validator({
                            rules: {
                                codeEtablissement: "required",
                                EtablissementAddress1: "required",
                                cpEtablissement: "required",
                                villeEtablissement: "required",
                                paysEtablissement: "required"

                            },
                            messages: {
                                codeEtablissement: $.validator.messages.required,
                                EtablissementAddress1: $.validator.messages.required,
                                cpEtablissement: $.validator.messages.required,
                                villeEtablissement: $.validator.messages.required,
                                paysEtablissement: $.validator.messages.required
                            }
                        });


                    },
                    add: function(){
                        var $form = $('#modalEtablissement .form');

                        $.validator.setDefaults({
                            submitHandler: function () {

                                $.ajax({
                                    url: $form.attr('action'),
                                    data: $form.serialize(),
                                    success: function (data) {
                                        if ($().check(data)) {

                                            var _data_parse = JSON.parse(data);

                                            $('.kt-ajaxmodal')
                                                .modal('hide')
                                                .on('hidden.bs.modal', function () {

                                                    $.notify({
                                                        icon: 'fa fa-check',
                                                        message: 'La modification de vos donn&eacute;es a &eacute;t&eacute; effectu&eacute;e avec succ&egrave;s.'
                                                    });

                                                    $('#row-etablissement-' + _data_parse.IdEtablissement).addClass('animated flash');

                                                    $.each(_data_parse, function (key, val) {
                                                        $('[data-binding=' + key + ']').text(val);
                                                    });


                                                });

                                        }
                                    }
                                });

                            }
                        });

                        $form.validator({
                            rules: {
                                codeEtablissement: "required",
                                EtablissementAddress1: "required",
                                cpEtablissement: "required",
                                villeEtablissement: "required",
                                paysEtablissement: "required"

                            },
                            messages: {
                                codeEtablissement: $.validator.messages.required,
                                EtablissementAddress1: $.validator.messages.required,
                                cpEtablissement: $.validator.messages.required,
                                villeEtablissement: $.validator.messages.required,
                                paysEtablissement: $.validator.messages.required
                            }
                        });
                    }

                },
                init: function () {
                    // Delete etablissement
                    $('.table-etablissements-btn-delete-action').click(function () {

                        var $this = $(this),
                            _bootbox = $(this).json('bootbox'),
                            _data = $(this).json('data'),
                            _dialogDownload = null;

                        _dialogDownload = bootbox.confirm({
                            message: '<div class="' + _bootbox.class + '">' + _bootbox.message + '</div>',
                            className: _bootbox.class_name,
                            closeButton: false,
                            buttons: {
                                confirm: {
                                    label: _bootbox.btn_confirm,
                                    className: 'btn-primary btn-sm'
                                },
                                cancel: {
                                    label: _bootbox.btn_cancel,
                                    className: 'btn-default btn-sm'
                                }
                            },
                            callback: function (result) {
                                if (result) {
                                    $(_data.target).hide('slow');
                                }

                            }
                        });

                    });
                }
            },

            ready: function () {
                this.etablissement.init();
            }
        }

    });

})(jQuery, KTJS);

