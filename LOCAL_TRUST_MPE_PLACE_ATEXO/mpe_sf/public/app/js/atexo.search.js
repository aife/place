/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function selectAllLocalisationsItems(idListLocalisation) {
    var selectBox = document.getElementById(idListLocalisation);
    var i = selectBox.options.length;

    while (i--) {
        selectBox.options[i].selected = "selected";
    }
    
    document.getElementsByName('lancerRecherche')[0].submit();
}