/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

function showModalAssistance() {
    J("#modal-assistant-marche-public").modal("show");
}
