function addCoTraitant(msgError){
    //showModalLoader();
    var siren = J("#siren_coTraitant").val();
    var siret = J("#siret_coTraitant").val();
    if(siren != '' && siret != '') {
        if (isSiretValide(siret,siren)) {
            J('#bloc-loader').show();
            var formData = new FormData();
            formData.append('siren', siren);
            formData.append('siret', siret);
            formData.append('uid_response', J('#form_uid_response').val());
            formData.append('refConsultation', consulation_ref);

            if(J('#groupementSolidaire').is(":checked")){
                formData.append('idTypeGroupement', J('#groupementSolidaire').val());
            }else if(J('#groupementConjointNonSolidaire').is(":checked")){
                formData.append('idTypeGroupement', J('#groupementConjointNonSolidaire').val());
            }else if(J('#groupementConjointSolidaire').is(":checked")){
                formData.append('idTypeGroupement', J('#groupementConjointSolidaire').val());
            }
            if(doPostDataToRoute(formData, 'atexo_groupement_ajout_cotraitant')){
                J("#siren_coTraitant").val('');
                J("#siret_coTraitant").val('');
                document.getElementById('imgError').style.display = 'none';
                document.getElementById('panelMessage').style.display = 'none';
                document.getElementById('labelMessage').innerHTML = '';
            } else {
                document.getElementById('imgError').style.display = '';
                document.getElementById('panelMessage').style.display = '';
                document.getElementById('labelMessage').innerHTML = msgError;
            }

        } else {
            document.getElementById('imgError').style.display = '';
            document.getElementById('panelMessage').style.display = '';
            document.getElementById('labelMessage').innerHTML = msgError;
        }
    }else{
        document.getElementById('imgError').style.display = '';
        document.getElementById('panelMessage').style.display = '';
        document.getElementById('labelMessage').innerHTML = msgError;
    }
    //showModalLoader();
    return false;
}

function addSousTraitant(idParent){
    //showModalLoader();
    var siren = J("#siren"+idParent).val();
    var siret = J("#siret"+idParent).val();
    if(siren != '' && siret != '') {
        if (isSiretValide(siret,siren)) {
            J('#bloc-loader').show();
            var formData = new FormData();
            formData.append('siren', siren);
            formData.append('siret', siret);
            formData.append('idParent', idParent);
            formData.append('id', 0);
            formData.append('uid_response', J('#form_uid_response').val());
            formData.append('refConsultation', consulation_ref);
            if (doPostDataToRoute(formData, 'atexo_groupement_ajouter_soustraitant')) {
                document.getElementById('imgError' + idParent).style.display = 'none';
                document.getElementById('labelSiretInvalide' + idParent).style.display = 'none';
                document.getElementById('labelSiretObligatoire' + idParent).style.display = 'none';
            } else {
                document.getElementById('imgError' + idParent).style.display = '';
                document.getElementById('labelSiretInvalide' + idParent).style.display = '';
                document.getElementById('labelSiretObligatoire' + idParent).style.display = 'none';
            }
        } else {
            document.getElementById('labelSiretInvalide'+idParent).style.display = '';
            document.getElementById('labelSiretObligatoire'+idParent).style.display = 'none';
            document.getElementById('imgError'+idParent).style.display = '';
        }
    }else{
        document.getElementById('labelSiretObligatoire'+idParent).style.display = '';
        document.getElementById('labelSiretInvalide'+idParent).style.display = 'none';
        document.getElementById('imgError'+idParent).style.display = '';
    }
    //showModalLoader();
    return false;
}

function addLigneSousTraitant(event, idParent, idCotraitant){
    var sousMembre = document.getElementById("groupementBlocSousTraitant_" + idParent);
    var data = {
        "idParent": idParent,
        "sousMembre": sousMembre
    };

    if (sousMembre == null) {
        J(event).parents("tr").after(tmpl("template-new-sous-traitant", data));
    } else {
        J("#groupementBlocCoTraitant_" + idCotraitant).after(tmpl("template-new-sous-traitant", data));
    }

    J(event).prop('disabled', true);

    return false;
}

function supprimerSousTraitant(id,idParent){
    //showModalLoader();
    var formData = new FormData();
    formData.append('id', id);
    formData.append('idParent', idParent);
    formData.append('type', 'soustraitant');
    formData.append('uid_response', J('#form_uid_response').val());
    formData.append('refConsultation', consulation_ref);
    doPostDataToRoute(formData, 'atexo_groupement_supprimer_membre' );
    //showModalLoader();
    return false;
}

function supprimerCoTraitant(id){
    //showModalLoader();
    var formData = new FormData();
    formData.append('id', id);
    formData.append('type', 'cotraitant');
    formData.append('uid_response', J('#form_uid_response').val());
    formData.append('refConsultation', consulation_ref);
    doPostDataToRoute(formData, 'atexo_groupement_supprimer_membre');
    //showModalLoader();
    return false;
}

function renseignerDumeModal(idMembreGroupement, numeroSN) {
    J('#modal-renseigner-dume').modal('show');
    J('#idMembreGroupement').val(idMembreGroupement);
    J('#num_dume').val(numeroSN);

    return false;
}

function validerRefDume(reference) {
    var idMembreGroupement = J('#idMembreGroupement')[0].value;
    J.ajax(Routing.generate('atexo_groupement_valider_reference_dume'),{
        method: "POST",
        data: {
            refDume: reference,
            idMembreGroupement: idMembreGroupement
        }
    }).done(function(result) {
        var res = jQuery.parseJSON(result);

        if (res.error === 1) {
            J("#erreur-cotraitant-dume").show();
            J("#erreur-cotraitant-dume").text(res.message);
        } else {
            if (res.div && res.view) {
                J('#' + res.div).html(res.view);
            }

            J("#erreur-cotraitant-dume").hide();
            J('#modal-renseigner-dume').modal('hide');
            J('.modal-backdrop').remove();
            J('body').removeClass('modal-open');
        }
        return false;
    });
}

function doPostDataToRoute(formData, route){
    var succes = false;
    succes = J.ajax(Routing.generate(route),{
        type: "POST",
        data: formData,
        processData: false,
        contentType: false
    }).done(function (result) {
        var res = jQuery.parseJSON(result);
        if (res.div && res.view) {
            J('#'+res.div).replaceWith(res.view);
            succes = true;
            J("#siren_coTraitant").val('');
            J("#siret_coTraitant").val('');
            if(!J('.icon-sous-traitant')[0]) {
                J('#infoGroupementPanel').hide();
            }
        } else {
            succes = false;
        }
        if(res.cacheDiv){
            J('#'+res.cacheDiv).hide();
        }
        J('#bloc-loader').hide();
        return succes;
    }).fail(function(result){
        J('#bloc-loader').hide();
    });

    return succes;
}

function isSiretValide(siret,siren) {
    var parameter = siren.concat(siret);
    var estValide;

    if (siret == 0 && siren == 0) {
        estValide = false;
    }

    if ((parameter.length != 14) || (isNaN(parameter)) || (parameter == 0)) {
        estValide = false;
    }else if(siren=='356000000'){
        estValide = true;
    }   else {
        // Donc le SIRET est un numérique à 14 chiffres
        // Les 9 premiers chiffres sont ceux du SIREN (ou RCS), les 4 suivants
        // correspondent au numéro d'établissement
        // et enfin le dernier chiffre est une clef de LUHN.
        var somme = 0;
        var tmp;
        for (var cpt = 0; cpt < parameter.length; cpt++) {
            if ((cpt % 2) == 0) { // Les positions impaires : 1er, 3è, 5è, etc...
                tmp = parameter.charAt(cpt) * 2; // On le multiplie par 2
                if (tmp > 9)
                    tmp -= 9;	// Si le résultat est supérieur à 9, on lui soustrait 9
            }
            else
                tmp = parameter.charAt(cpt);
            somme += parseInt(tmp);
        }
        if ((somme % 10) == 0)
            estValide = true; // Si la somme est un multiple de 10 alors le SIRET est valide
        else
            estValide = false;
    }
    return estValide;
}

function changeTypeGroupement(check, libelle, solidaire){
    if(document.getElementById('libelleTpeGroupement')) {
        document.getElementById('libelleTpeGroupement').innerHTML = libelle;
    }
    if(solidaire) {
        J('.type_mandataire').addClass("solidaire");
    }else{
        J('.type_mandataire').removeClass("solidaire");
    }
    var formData = new FormData();
    formData.append('idTypeGroupement', check.value);
    formData.append('uid_response', J('#form_uid_response').val());
    formData.append('refConsultation', consulation_ref);
    doPostDataToRoute(formData,'atexo_groupement_change_type');
}

function creerGroupement(value){
    J.ajax(Routing.generate('atexo_groupement_oui'),{
        method: "POST",
        data: { groupementOui: value, uid_response: J('#form_uid_response').val(), refConsultation: consulation_ref}
    }).done(function (data) {
        J('#idGroupeEntreprise').val(data)
    })
}

function declarerGroupement(value){
    J.ajax(Routing.generate('atexo_groupement_declarer'),{
        method: "POST",
        data: { declarerGroupement: value, uid_response: J('#form_uid_response').val(), refConsultation: consulation_ref}
    })
}