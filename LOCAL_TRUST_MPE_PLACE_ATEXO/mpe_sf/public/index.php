<?php

use App\Kernel;
use Application\Service\Atexo\Atexo_Util;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require dirname(__DIR__) . '/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->usePutenv(true);
$dotenv->load(__DIR__ . '/../.env');

if ($trustedProxies = $_SERVER['TRUSTED_PROXIES'] ?? false) {
    Request::setTrustedProxies(
        explode(',', $trustedProxies),
        Request::HEADER_X_FORWARDED_ALL ^ Request::HEADER_X_FORWARDED_HOST
    );
}

$kernel = new kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();

// On sette le Kernel entier pour avoir accès à tous les services SF
// car on ne peut pas utiliser la DI côté Prado.
Atexo_Util::setSfKernel($kernel);

$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
