-- MySQL dump 10.13  Distrib 5.5.55, for Linux (x86_64)
--
-- Host: localhost    Database: place_test_MPE_DB
-- ------------------------------------------------------
-- Server version	5.5.55-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

USE `place_test_unitaire_MPE_DB`;

--
-- Dumping data for table `AVIS`
--

LOCK TABLES `AVIS` WRITE;
/*!40000 ALTER TABLE `AVIS` DISABLE KEYS */;
/*!40000 ALTER TABLE `AVIS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AcheteurPublic`
--

LOCK TABLES `AcheteurPublic` WRITE;
/*!40000 ALTER TABLE `AcheteurPublic` DISABLE KEYS */;
/*!40000 ALTER TABLE `AcheteurPublic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Administrateur`
--

LOCK TABLES `Administrateur` WRITE;
/*!40000 ALTER TABLE `Administrateur` DISABLE KEYS */;
INSERT INTO `Administrateur` (`id`, `original_login`, `login`, `certificat`, `mdp`, `nom`, `prenom`, `email`, `organisme`, `admin_general`, `tentatives_mdp`) VALUES (1,'a1a','a1a','','ATX32008','Nom','Prenom','email','a1a','0',0),(2,'a2z','a2z','','F836A9BA','Nom','Prenom','email','a2z','0',0),(3,'e3r','e3r','','5ECFF512','Nom','Prenom','email','e3r','0',0),(4,'a1t','a1t','','F78DEDFG','Nom','Prenom','email','a1t','0',0),(5,'t5y','t5y','','F59D993B','Nom','Prenom','email',NULL,'0',0),(6,'u6i','u6i','','B37F18E1','Nom','Prenom','email',NULL,'0',0),(7,'o8p','o8p','','128BB460','Nom','Prenom','email',NULL,'0',0),(8,'q1s','q1s','','23029148','Nom','Prenom','email',NULL,'0',0),(9,'d2f','d2f','','7E6089B4','Nom','Prenom','email',NULL,'0',0),(10,'g3h','g3h','','C33EB20C','Nom','Prenom','email',NULL,'0',0),(11,'j4k','j4k','','4880CF1F','Nom','Prenom','email',NULL,'0',0),(12,'m5w','m5w','','211B364A','Nom','Prenom','email',NULL,'0',0),(13,'x6c','x6c','','5ABD3710','Nom','Prenom','email',NULL,'0',0),(14,'v5b','v5b','','A4F67F3C','Nom','Prenom','email',NULL,'0',0),(15,'n2a','n2a','','7CC4BE4D','Nom','Prenom','email',NULL,'0',0),(16,'z5e','z5e','','F583CCE1','Nom','Prenom','email',NULL,'0',0),(17,'r8t','r8t','','69FA10BC','Nom','Prenom','email',NULL,'0',0),(18,'y6u','y6u','','5A2C9690','Nom','Prenom','email',NULL,'0',0),(19,'i9o','i9o','','255E39B3','Nom','Prenom','email',NULL,'0',0),(20,'p2q','p2q','','F47B15D8','Nom','Prenom','email',NULL,'0',0),(21,'s3d','s3d','','FC20DED2','Nom','Prenom','email',NULL,'0',0),(22,'f4g','f4g','','80F16829','Nom','Prenom','email',NULL,'0',0),(23,'h5j','h5j','','E8721360','Nom','Prenom','email',NULL,'0',0);
/*!40000 ALTER TABLE `Administrateur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Admissibilite_Enveloppe_Lot`
--

LOCK TABLES `Admissibilite_Enveloppe_Lot` WRITE;
/*!40000 ALTER TABLE `Admissibilite_Enveloppe_Lot` DISABLE KEYS */;
/*!40000 ALTER TABLE `Admissibilite_Enveloppe_Lot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Admissibilite_Enveloppe_papier_Lot`
--

LOCK TABLES `Admissibilite_Enveloppe_papier_Lot` WRITE;
/*!40000 ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` DISABLE KEYS */;
/*!40000 ALTER TABLE `Admissibilite_Enveloppe_papier_Lot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AdresseFacturationJal`
--

LOCK TABLES `AdresseFacturationJal` WRITE;
/*!40000 ALTER TABLE `AdresseFacturationJal` DISABLE KEYS */;
/*!40000 ALTER TABLE `AdresseFacturationJal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AffiliationService`
--

LOCK TABLES `AffiliationService` WRITE;
/*!40000 ALTER TABLE `AffiliationService` DISABLE KEYS */;
/*!40000 ALTER TABLE `AffiliationService` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Agent`
--

LOCK TABLES `Agent` WRITE;
/*!40000 ALTER TABLE `Agent` DISABLE KEYS */;
INSERT INTO `Agent` (`id`, `login`, `email`, `mdp`, `certificat`, `nom`, `prenom`, `tentatives_mdp`, `organisme`, `service_id`, `RECEVOIR_MAIL`, `elu`, `nom_fonction`, `num_tel`, `num_fax`, `type_comm`, `adr_postale`, `civilite`, `alerte_reponse_electronique`, `alerte_cloture_consultation`, `alerte_reception_message`, `alerte_publication_boamp`, `alerte_echec_publication_boamp`, `alerte_creation_modification_agent`, `date_creation`, `date_modification`, `id_externe`, `id_profil_socle_externe`, `lieu_execution`, `alerte_question_entreprise`, `actif`, `codes_nuts`, `num_certificat`, `alerte_validation_consultation`, `alerte_chorus`, `password`, `code_theme`, `date_connexion`, `type_hash`) VALUES (1,'hyperadmin','suivi.mpe@atexo.com','',NULL,'Admin','Hyper',0,'a1a',0,'0','0','',NULL,NULL,'2','','','0','0','0','0','0','0','2019-03-14 17:11:49','2019-03-14 17:11:49','0',0,',,241,','0','1',NULL,'','0','0','15cb90385e95f227c2eff99e5627a848f507cb83','0','2019-03-14 18:18:01','SHA1'),(2,'admin_a1a','suivi.mpe@atexo.com','',NULL,'A1A','Admin',0,'a1a',0,'0','0','',NULL,NULL,'2','','','0','0','0','0','0','0','2019-03-14 17:11:49','2019-03-14 17:11:49','0',0,NULL,'0','1',NULL,'','0','0','6aa05c2f87159cc315b7b67c7bda82bfef54f0e8','0',NULL,'SHA1'),(3,'admin_a2z','suivi.mpe@atexo.com','',NULL,'A2Z','Admin',0,'a2z',0,'0','0','',NULL,NULL,'2','','','0','0','0','0','0','0','2019-03-14 17:11:51','2019-03-14 17:11:51','0',0,NULL,'0','1',NULL,'','0','0','028ad1e9ac333e51d30a98fbb30f719f75b78e31','0',NULL,'SHA1'),(4,'admin_e3r','suivi.mpe@atexo.com','',NULL,'E3R','Admin',0,'e3r',0,'0','0','',NULL,NULL,'2','','','0','0','0','0','0','0','2019-03-14 17:11:52','2019-03-14 17:11:52','0',0,NULL,'0','1',NULL,'','0','0','ca11aa925ede16667ad888ca5dc8f2e8d4aab513','0',NULL,'SHA1'),(5,'admin_a1t','suivi.mpe@atexo.com','',NULL,'A1T','Admin',0,'a1t',0,'0','0','',NULL,NULL,'2','','','0','0','0','0','0','0','2019-03-14 17:11:53','2019-03-14 17:11:53','0',0,NULL,'0','1',NULL,'','0','0','88a63960592ae362bb72ba349790e6330947a63c','0',NULL,'SHA1'),(6,'flo','florian.martin@atexo.com','',NULL,'MARTIN','florian',0,'a1a',1,'0','0','','','','2','','','1','1','1','1','1','0','2019-03-14 17:39:30','2019-03-14 17:39:30','0',0,NULL,'1','1',NULL,'','0','0','3872bef8381cb6231616e85f8ec9b0306c06491a','0','2019-03-14 17:55:14','SHA1');
/*!40000 ALTER TABLE `Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Agent_Commission`
--

LOCK TABLES `Agent_Commission` WRITE;
/*!40000 ALTER TABLE `Agent_Commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `Agent_Commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Agent_Service_Metier`
--

LOCK TABLES `Agent_Service_Metier` WRITE;
/*!40000 ALTER TABLE `Agent_Service_Metier` DISABLE KEYS */;
INSERT INTO `Agent_Service_Metier` (`id_agent`, `id_service_metier`, `id_profil_service`, `date_creation`, `date_modification`) VALUES (1,1,1,NULL,NULL),(2,1,1,NULL,NULL),(3,1,1,NULL,NULL),(4,1,1,NULL,NULL),(5,1,1,NULL,NULL),(6,1,4,'2019-03-14 17:39:30','2019-03-14 17:39:30');
/*!40000 ALTER TABLE `Agent_Service_Metier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Agrement`
--

LOCK TABLES `Agrement` WRITE;
/*!40000 ALTER TABLE `Agrement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Agrement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Alerte`
--

LOCK TABLES `Alerte` WRITE;
/*!40000 ALTER TABLE `Alerte` DISABLE KEYS */;
/*!40000 ALTER TABLE `Alerte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Annonce`
--

LOCK TABLES `Annonce` WRITE;
/*!40000 ALTER TABLE `Annonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `Annonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AnnonceBoamp`
--

LOCK TABLES `AnnonceBoamp` WRITE;
/*!40000 ALTER TABLE `AnnonceBoamp` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnonceBoamp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AnnonceJAL`
--

LOCK TABLES `AnnonceJAL` WRITE;
/*!40000 ALTER TABLE `AnnonceJAL` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnonceJAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AnnonceJALPieceJointe`
--

LOCK TABLES `AnnonceJALPieceJointe` WRITE;
/*!40000 ALTER TABLE `AnnonceJALPieceJointe` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnonceJALPieceJointe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AnnonceMoniteur`
--

LOCK TABLES `AnnonceMoniteur` WRITE;
/*!40000 ALTER TABLE `AnnonceMoniteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnonceMoniteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Annonce_Press`
--

LOCK TABLES `Annonce_Press` WRITE;
/*!40000 ALTER TABLE `Annonce_Press` DISABLE KEYS */;
/*!40000 ALTER TABLE `Annonce_Press` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Annonce_Press_PieceJointe`
--

LOCK TABLES `Annonce_Press_PieceJointe` WRITE;
/*!40000 ALTER TABLE `Annonce_Press_PieceJointe` DISABLE KEYS */;
/*!40000 ALTER TABLE `Annonce_Press_PieceJointe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Articles`
--

LOCK TABLES `Articles` WRITE;
/*!40000 ALTER TABLE `Articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Autres_Pieces_Mise_Disposition`
--

LOCK TABLES `Autres_Pieces_Mise_Disposition` WRITE;
/*!40000 ALTER TABLE `Autres_Pieces_Mise_Disposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `Autres_Pieces_Mise_Disposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Avenant`
--

LOCK TABLES `Avenant` WRITE;
/*!40000 ALTER TABLE `Avenant` DISABLE KEYS */;
/*!40000 ALTER TABLE `Avenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `AvisCao`
--

LOCK TABLES `AvisCao` WRITE;
/*!40000 ALTER TABLE `AvisCao` DISABLE KEYS */;
INSERT INTO `AvisCao` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'Avis favorable',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Avis défavorable',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Sans suite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `AvisCao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Avis_Membres_CAO`
--

LOCK TABLES `Avis_Membres_CAO` WRITE;
/*!40000 ALTER TABLE `Avis_Membres_CAO` DISABLE KEYS */;
/*!40000 ALTER TABLE `Avis_Membres_CAO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Avis_Pub`
--

LOCK TABLES `Avis_Pub` WRITE;
/*!40000 ALTER TABLE `Avis_Pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `Avis_Pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CG76_Domaine`
--

LOCK TABLES `CG76_Domaine` WRITE;
/*!40000 ALTER TABLE `CG76_Domaine` DISABLE KEYS */;
INSERT INTO `CG76_Domaine` (`Id`, `libelle`, `Parent`) VALUES (1,'Repères et outils fondamentaux prof.',0),(2,'Management',0),(3,'Affaires juridiques',0),(4,'Gestion des ressources humaines',0),(5,'Formation professionnelle et recherche',0),(6,'Finances et gestion financière',0),(7,'Informatique et systèmes d\'information',0),(8,'Europe et international',0),(9,'Citoyenneté et population',0),(10,'Social et santé',0),(11,'Enfance, éducation, jeunesse',0),(12,'Restauration',0),(13,'Culture, archives et documentation',0),(14,'Environnement',0),(15,'Hygiène et sécurité',0),(16,'Génie technique',0),(17,'Développement local',0),(18,'Sport',0),(19,'Positionnement',0),(20,'Tech. expression communication et relat.',1),(21,'Observation du milieu professionnel',1),(22,'Tech. administ.d\'organis° et secrétariat',1),(23,'Connaissance de l\'environnement territorial',1),(24,'Savoirs de base intégration vie profess',1),(25,'Management stratégique',2),(26,'Management organisationnel',2),(27,'Management des équipes et des personnes',2),(28,'Approche général du management',2),(29,'Modes de gestion des services publics',3),(30,'Achat public, Marchés pub et contrats',3),(31,'Prévention juridique et contentieux',3),(32,'Approche générale du droit',3),(33,'Assurances',3),(34,'Prévention juridique et contentieux',3),(35,'Approche générale du droit',3),(36,'Gestion administrative et statutaire',4),(37,'Politiques de GRH',4),(38,'Conditions de travail',4),(39,'Relations sociales',4),(40,'Fonction formation',4),(41,'Approche générale de la GRH',4),(42,'Connais. Acteurs disp polit emploi form',5),(43,'Contrôle évaluation org form recherche',5),(44,'Instruc organis gest form prof et rech',5),(45,'Procédure budgétaire et comptable',6),(46,'Gestion et stratégie financière',6),(47,'Fiscalité et dotation de l\'Etat',6),(48,'Approche générale des finances',6),(49,'Stratégie de communication',7),(50,'Techniques et outils de communication',7),(51,'Appr géné de la communication institut',7),(52,'Architecture et admin des syst d\'infos',8),(53,'Réseaux et télécommunication',8),(54,'Techniques informatiques',8),(55,'Bureautique et utilisation outils inform',8),(56,'Connaissances des institutions',9),(57,'Politiques régionales',9),(58,'Coopération décentralisée',9),(59,'Politiques sectorielles',9),(60,'Démographie',10),(61,'état civil',10),(62,'élections',10),(63,'gestion funéraire',10),(64,'affaires générales',10),(65,'Connais acteurs des dispos et polit',11),(66,'Protection de l\'enfance et de l\'adolesc.',11),(67,'Insertion et développement social',11),(68,'Accueil et accompagnement des pers âgées',11),(69,'Prévention et promotion de la santé',11),(70,'Méthodologie du travail social',11),(71,'Appr. gén. de l\'act. soc. et de la santé',11),(72,'Accueil et accompagnement des pers handicapées',11),(73,'Connais. acteurs dispositifs, politiques',12),(74,'Accueil de l\'enfance et de l\'adolescence',12),(75,'Animation enfance, jeunesse',12),(76,'Organisation et production',13),(77,'Hygiène et sécurité alimentaire',13),(78,'Distribution et service',13),(79,'Equilibre nutritionnel',13),(80,'Approche génér. de la restauration',13),(81,'Connais. act dispositifs, polit act cult',14),(82,'Gestion et enrichissement des fonds',14),(83,'Diffusion, promot, protection juridique',14),(84,'Protect dév du patri architec et archéo',14),(85,'Enseignement artistique',14),(86,'Programmation et techniques du spectacle',14),(87,'Tech de documentat et d\'information',14),(88,'Approche générale de l\'action culturelle',14),(89,'Polit géné d\'environ et gest des ress.',15),(90,'Résorption des nuisances et pollutions',15),(91,'Production d\'eau potable',15),(92,'Traitement des eaux usées',15),(93,'Biologie, hygiène alim et santé animale',15),(94,'Collecte, trait et gestion des déchets',15),(95,'Connaissance des act.,des dispo.des pol.',15),(96,'Maintenance des moyens techniques',15),(97,'Prév et opér d\'incendie et de secours',16),(98,'Sûreté et sécurité dans la ville',16),(99,'Prév et gest des risques tech et nat.',16),(100,'Prévention et protection du public',16),(101,'Sécurité des agents au travail',16),(102,'Responsabilité du personnel d?encadrement ',16),(103,'Plan de prévention entreprises extérieures intervenantes',16),(104,'Travail sur écran',16),(105,'Fonction achat des équipements dans une logique d?ergonomie',16),(106,'Formation des ACMO',16),(107,'Formation des membres du CHS',16),(108,'Nuisances sonores',16),(109,'Equipement de protection individuelle (EPI)',16),(110,'Document unique',16),(111,'Prévention et Secours Civiques de niveau ?PSC1-',16),(112,'SST',16),(113,'SSIAP 1',16),(114,'Utilisation du CMSI et du SSI',16),(115,'Initiation à la lutte contre l?incendie',16),(116,'Habilitations électriques',16),(117,'CACES',16),(118,'Signalisation temporaire des zones d?intervention',16),(119,'Manutention de ponts roulants',16),(120,'Risque routier',16),(121,'FCOS',16),(122,'Permis Poids lourd',16),(123,'Soudure',16),(124,'Utilisation de la débroussailleuse',16),(125,'Utilisation de la tronçonneuse',16),(126,'Manutentions manuelles et mécaniques de charges',16),(127,'Montage et démontage des échafaudages, scènes, podiums',16),(128,'Utilisation des produits phyto sanitaires',16),(129,'Gestion des déchets industriels et ménagers',16),(130,'Maîtrise du risque infectieux en milieu médico social',16),(131,'Nettoyage et désinfection des locaux',16),(132,'Fonction achat des produits d?entretien',16),(133,'Gestion du stress',16),(134,'Gestion des phénomènes agressifs',16),(135,'HACCP',16),(136,'Hygiène des aliments',16),(137,'Architecture et construction',17),(138,'Infrastructures et réseaux',17),(139,'Espaces verts et paysage',17),(140,'Maint des bâtiments tt corps d\'état',17),(141,'Maîtrise de l\'énergie',17),(142,'Maintenance des moyens techniques',17),(143,'Connais. acteurs disp politiq dév territ',18),(144,'Aménag, urbanisme et act foncière',18),(145,'Politique de l\'habitat logement social',18),(146,'Action économique',18),(147,'Développement touristique',18),(148,'Déplacements et transports',18),(149,'Connais. acteurs disp politiq sportives',19),(150,'Enseignement des prat sportives',19),(151,'Entretien des matériels sportifs',19),(152,'Orientation formative, posit évaluation',20),(153,'Remise à niveau',20),(154,'Acquisition de connaissances',20),(155,'Méthodo et entraînement aux épreuves',20),(156,'Sensibilisation générale',21),(157,'Marchés publics et développement durable',21),(158,'Energies et approche environnementale',21),(159,'Eco labels et certifications',21);
/*!40000 ALTER TABLE `CG76_Domaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CG76_Donnee_Complementaire_Domaine`
--

LOCK TABLES `CG76_Donnee_Complementaire_Domaine` WRITE;
/*!40000 ALTER TABLE `CG76_Donnee_Complementaire_Domaine` DISABLE KEYS */;
/*!40000 ALTER TABLE `CG76_Donnee_Complementaire_Domaine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CG76_Donnee_Complementaire_entreprise`
--

LOCK TABLES `CG76_Donnee_Complementaire_entreprise` WRITE;
/*!40000 ALTER TABLE `CG76_Donnee_Complementaire_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `CG76_Donnee_Complementaire_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CG76_PieceJointe`
--

LOCK TABLES `CG76_PieceJointe` WRITE;
/*!40000 ALTER TABLE `CG76_PieceJointe` DISABLE KEYS */;
/*!40000 ALTER TABLE `CG76_PieceJointe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CategorieConsultation`
--

LOCK TABLES `CategorieConsultation` WRITE;
/*!40000 ALTER TABLE `CategorieConsultation` DISABLE KEYS */;
INSERT INTO `CategorieConsultation` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `id_categorie_ANM`, `libelle_it`, `code`, `code_dume`) VALUES ('1','Travaux','Travaux','Works','Obras',NULL,NULL,NULL,NULL,'1','Lavori',NULL,'01'),('2','Fournitures','Fournitures','Supplies','Suministros',NULL,NULL,NULL,NULL,'2','Forniture',NULL,'02'),('3','Services','Services','Services','Servicios',NULL,NULL,NULL,NULL,'3','Servizi',NULL,'03');
/*!40000 ALTER TABLE `CategorieConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CategorieINSEE`
--

LOCK TABLES `CategorieINSEE` WRITE;
/*!40000 ALTER TABLE `CategorieINSEE` DISABLE KEYS */;
INSERT INTO `CategorieINSEE` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`, `code`) VALUES ('4','Personne morale de droit public soumise au droit commercial','','','','','','','','',NULL),('4.1','Établissement public ou régie à caractère industriel ou commercial','','','','','','','','','09'),('4.1.10','Établissement public national à caractère industriel ou commercial doté d\'un comptable public','','','','','','','','','09'),('4.1.20','Établissement public national à caractère industriel ou commercial non doté d\'un comptable public','','','','','','','','','09'),('4.1.30','Exploitant public','','','','','','','','','09'),('4.1.40','Établissement public local à caractère industriel ou commercial','','','','','','','','','09'),('4.1.50','Régie d\'une collectivité locale à caractère industriel ou commercial','','','','','','','','','09'),('4.1.60','Institution Banque de France','','','','','','','','','09'),('7','Personne morale et organisme soumis au droit administratif','','','','','','','','',NULL),('7.1','Administration de l\'état','','','','','','','','','04'),('7.1.11','Autorité constitutionnelle','','','','','','','','','04'),('7.1.12','Autorité administrative indépendante','','','','','','','','','04'),('7.1.13','Ministère','','','','','','','','','04'),('7.1.20','Service central d\'un ministère','','','','','','','','','04'),('7.1.50','Service du ministère de la Défense','','','','','','','','','04'),('7.1.60','Service déconcentré à compétence nation. D\'un ministère (hors Défense)','','','','','','','','','04'),('7.1.71','Service déconcentré de l\'État à compétence (inter) régionale','','','','','','','','','04'),('7.1.72','Service déconcentré de l\'État à compétence (inter) départementale','','','','','','','','','04'),('7.1.79','(Autre) Service déconcentré de l\'État à compétence territoriale','','','','','','','','','04'),('7.1.90','Ecole nationale non dotée de la personnalité morale','','','','','','','','','09'),('7.2','Collectivité territoriale','','','','','','','','','07'),('7.2.10','Commune','','','','','','','','','08'),('7.2.20','Département','','','','','','','','','06'),('7.2.25','Territoire d\'Outre-mer','','','','','','','','','05'),('7.2.29','(Autre) Collectivité territoriale','','','','','','','','','07'),('7.2.30','Région','','','','','','','','','05'),('7.3','Établissement public administratif','','','','','','','','','05'),('7.3.12','Commune associée','','','','','','','','','08'),('7.3.13','Section de commune','','','','','','','','','08'),('7.3.14','Ensemble urbain','','','','','','','','','09'),('7.3.21','Association syndicale autorisée','','','','','','','','','09'),('7.3.22','Association foncière urbaine','','','','','','','','','09'),('7.3.23','Association foncière de remembrement','','','','','','','','','09'),('7.3.31','Établissement public local d\'enseignement','','','','','','','','','09'),('7.3.41','Secteur de commune','','','','','','','','','09'),('7.3.42','District urbain','','','','','','','','','09'),('7.3.43','Communauté urbaine','','','','','','','','','09'),('7.3.45','Syndicat intercommunal à vocation multiple (SIVOM)','','','','','','','','','09'),('7.3.46','Communauté de communes','','','','','','','','','09'),('7.3.47','Communauté de villes','','','','','','','','','09'),('7.3.48','Communauté d\'agglomération','','','','','','','','','09'),('7.3.49','Autre établissement public local de coopération non spécialisé ou entente','','','','','','','','','09'),('7.3.51','Institution interdépartemental ou entente','','','','','','','','','09'),('7.3.52','Institution interrégionale ou entente','','','','','','','','','09'),('7.3.53','Syndicat intercommunal à vocation unique (SIVU)','','','','','','','','','09'),('7.3.54','Syndicat mixte communal','','','','','','','','','09'),('7.3.55','Autre syndicat mixte','','','','','','','','','09'),('7.3.56','Commission syndicale pour la gestion des biens indivis des communes','','','','','','','','','09'),('7.3.61','Centre communal d\'action sociale','','','','','','','','','09'),('7.3.62','Caisse des écoles','','','','','','','','','09'),('7.3.63','Caisse de crédit municipal','','','','','','','','','09'),('7.3.64','Établissement d\'hospitalisation','','','','','','','','','09'),('7.3.65','Syndicat inter hospitalier','','','','','','','','','09'),('7.3.66','Établissement public local social et médico-social','','','','','','','','','09'),('7.3.71','Office public d\'habitation à loyer modéré (OPHLM)','','','','','','','','','09'),('7.3.72','Service départemental d\'incendie','','','','','','','','','09'),('7.3.73','Établissement public local culturel','','','','','','','','','09'),('7.3.79','(Autre) Établissement public administratif local','','','','','','','','','09'),('7.3.81','Organisme consulaire','','','','','','','','','09'),('7.3.82','Établissement public national ayant fonction d\'administration centrale','','','','','','','','','09'),('7.3.83','Établissement public national à caractère scientifique culturel et professionnel','','','','','','','','','09'),('7.3.84','Autre établissement public national d\'enseignement','','','','','','','','','09'),('7.3.85','Autre établissement public national administratif à compétence territoriale limitée','','','','','','','','','09'),('7.3.89','Établissement public national à caractère administratif','','','','','','','','','09'),('7.4','Autre personne morale de droit public administratif','','','','','','','','','09'),('7.4.10','Groupement d\'intérêt public (GIP)','','','','','','','','','09'),('7.4.30','Établissement public des cultes d\'Alsace-Lorraine','','','','','','','','','09'),('7.4.50','Cercle et foyer dans les armées','','','','','','','','','09'),('7.4.90','Autre personne morale de droit administratif','','','','','','','','','09');
/*!40000 ALTER TABLE `CategorieINSEE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CategorieLot`
--

LOCK TABLES `CategorieLot` WRITE;
/*!40000 ALTER TABLE `CategorieLot` DISABLE KEYS */;
/*!40000 ALTER TABLE `CategorieLot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Centrale_publication`
--

LOCK TABLES `Centrale_publication` WRITE;
/*!40000 ALTER TABLE `Centrale_publication` DISABLE KEYS */;
INSERT INTO `Centrale_publication` (`id`, `organisme`, `nom`, `mail`, `fax`, `information`) VALUES (1,'a1a','MEDIALEX','technique@atexo.com',NULL,NULL),(2,'a2z','MEDIALEX','technique@atexo.com',NULL,NULL),(3,'e3r','MEDIALEX','technique@atexo.com',NULL,NULL),(4,'a1t','MEDIALEX','technique@atexo.com',NULL,NULL);
/*!40000 ALTER TABLE `Centrale_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CertificatChiffrement`
--

LOCK TABLES `CertificatChiffrement` WRITE;
/*!40000 ALTER TABLE `CertificatChiffrement` DISABLE KEYS */;
/*!40000 ALTER TABLE `CertificatChiffrement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CertificatPermanent`
--

LOCK TABLES `CertificatPermanent` WRITE;
/*!40000 ALTER TABLE `CertificatPermanent` DISABLE KEYS */;
/*!40000 ALTER TABLE `CertificatPermanent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Certificat_Agent`
--

LOCK TABLES `Certificat_Agent` WRITE;
/*!40000 ALTER TABLE `Certificat_Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Certificat_Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Certificats_Entreprises`
--

LOCK TABLES `Certificats_Entreprises` WRITE;
/*!40000 ALTER TABLE `Certificats_Entreprises` DISABLE KEYS */;
/*!40000 ALTER TABLE `Certificats_Entreprises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_Code_Calcul_Interets`
--

LOCK TABLES `Chorus_Code_Calcul_Interets` WRITE;
/*!40000 ALTER TABLE `Chorus_Code_Calcul_Interets` DISABLE KEYS */;
INSERT INTO `Chorus_Code_Calcul_Interets` (`id`, `libelle`, `code`) VALUES (1,'Taux BCE (B1)','B1'),(2,'Taux BCE Majoré (B2)','B2'),(3,'Taux légal (L1)','L1'),(4,'Taux légal Majoré (L2)','L2'),(5,'Taux zéro (T0 IM)','T0');
/*!40000 ALTER TABLE `Chorus_Code_Calcul_Interets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_Fiche_Navette`
--

LOCK TABLES `Chorus_Fiche_Navette` WRITE;
/*!40000 ALTER TABLE `Chorus_Fiche_Navette` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_Fiche_Navette` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_Regroupement_Comptable`
--

LOCK TABLES `Chorus_Regroupement_Comptable` WRITE;
/*!40000 ALTER TABLE `Chorus_Regroupement_Comptable` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_Regroupement_Comptable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_Type_Groupement`
--

LOCK TABLES `Chorus_Type_Groupement` WRITE;
/*!40000 ALTER TABLE `Chorus_Type_Groupement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_Type_Groupement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_acte_juridique`
--

LOCK TABLES `Chorus_acte_juridique` WRITE;
/*!40000 ALTER TABLE `Chorus_acte_juridique` DISABLE KEYS */;
INSERT INTO `Chorus_acte_juridique` (`id`, `libelle`, `code`) VALUES (1,'1 - Contrat Initial','001'),(2,'2 - Contrat Complémentaire','002'),(3,'3 - Contrat sur la Base d\'un Accord Cadre','003'),(4,'4 - Marché de Définition','004'),(5,'5 - Autre','005');
/*!40000 ALTER TABLE `Chorus_acte_juridique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_conditions_paiements`
--

LOCK TABLES `Chorus_conditions_paiements` WRITE;
/*!40000 ALTER TABLE `Chorus_conditions_paiements` DISABLE KEYS */;
INSERT INTO `Chorus_conditions_paiements` (`id`, `libelle`, `code`) VALUES (1,'Paiement immédiat','Z000'),(2,'Paiement à 5 jours','Z005'),(3,'Paiement à 15 jours','Z015'),(4,'Paiement à 30 jours','Z030'),(5,'Paiement à 35 jours','Z035'),(6,'Paiement à 45 jours','Z045'),(7,'Paiement à 50 jours','Z050'),(8,'Paiement à 60 jours','Z060'),(9,'Paiement le 08 du mois suivant','ZF08'),(10,'Paiement le 15 du mois suivant','ZF15'),(11,'Paiement le dernier jour du mois suivant','ZFMS'),(12,'Paiement le 25 du mois','ZG25'),(13,'RNF - Facture interne (Fournisseur)','ZINT'),(14,'Paiement PME','ZPME'),(15,'Titre d\'un débiteur privé','ZR11'),(16,'TP arrêt recouvrement par retenue','ZR15'),(17,'Débiteur public ou para public','ZR20'),(18,'Etat','ZR21'),(19,'Administration ou collectivité locale','ZR22'),(20,'Organisme ou établissement Public','ZR23'),(21,'Apurement par compensation','ZR30'),(22,'Retenues sur paie','ZR31'),(23,'Retenues sur une dépense','ZR32'),(24,'Retenues sur pension','ZR33'),(25,'Titre émis par un correspondant','ZR40'),(26,'Taxe parafiscale','ZR42'),(27,'Titre aide juridictionnelle','ZR44');
/*!40000 ALTER TABLE `Chorus_conditions_paiements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_echange`
--

LOCK TABLES `Chorus_echange` WRITE;
/*!40000 ALTER TABLE `Chorus_echange` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_forme_prix`
--

LOCK TABLES `Chorus_forme_prix` WRITE;
/*!40000 ALTER TABLE `Chorus_forme_prix` DISABLE KEYS */;
INSERT INTO `Chorus_forme_prix` (`id`, `libelle`, `code`) VALUES (1,'1 - Prix Fermes','001'),(2,'2 - Prix Fermes Actualisables','002'),(3,'3 - Prix Révisables','003');
/*!40000 ALTER TABLE `Chorus_forme_prix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_groupement_achat`
--

LOCK TABLES `Chorus_groupement_achat` WRITE;
/*!40000 ALTER TABLE `Chorus_groupement_achat` DISABLE KEYS */;
INSERT INTO `Chorus_groupement_achat` (`id`, `organisme`, `id_oa`, `libelle`, `code`, `actif`, `id_service`) VALUES (1,'',1,'GA 01D','01D',1,0),(2,'',1,'GA 2','GA2',1,0),(3,'',2,'GA 3','GA3',1,0),(4,'',2,'GA 4','GA4',1,0),(5,'',3,'GA 5','GA5',1,0),(6,'',3,'GA 6','GA6',1,0);
/*!40000 ALTER TABLE `Chorus_groupement_achat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_numero_sequence`
--

LOCK TABLES `Chorus_numero_sequence` WRITE;
/*!40000 ALTER TABLE `Chorus_numero_sequence` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_numero_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_organisation_achat`
--

LOCK TABLES `Chorus_organisation_achat` WRITE;
/*!40000 ALTER TABLE `Chorus_organisation_achat` DISABLE KEYS */;
INSERT INTO `Chorus_organisation_achat` (`id`, `organisme`, `libelle`, `code`, `actif`, `flux_fen111`, `flux_fen211`) VALUES (1,'','OA Justice','C001',1,'1','1'),(2,'','OA 2','C002',1,'1','1'),(3,'','OA 3','C003',1,'1','1'),(4,'','OA 4','C004',1,'1','1');
/*!40000 ALTER TABLE `Chorus_organisation_achat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_pj`
--

LOCK TABLES `Chorus_pj` WRITE;
/*!40000 ALTER TABLE `Chorus_pj` DISABLE KEYS */;
/*!40000 ALTER TABLE `Chorus_pj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_type_marche`
--

LOCK TABLES `Chorus_type_marche` WRITE;
/*!40000 ALTER TABLE `Chorus_type_marche` DISABLE KEYS */;
INSERT INTO `Chorus_type_marche` (`id`, `libelle`, `code`) VALUES (1,'Subventions','ZSUB'),(2,'Marché à BDC','ZMBC'),(3,'MAPA à BDC','ZMPC'),(4,'MAPA à tranches','ZMPT'),(5,'MAPA unique','ZMPU'),(6,'MAPA mixte','ZMPX'),(7,'Marché à tranches','ZMT'),(8,'Marché unique','ZMU'),(9,'Marché mixte','ZMX'),(10,'ZCTR - Autres contrats','ZCTR');
/*!40000 ALTER TABLE `Chorus_type_marche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Chorus_type_procedure`
--

LOCK TABLES `Chorus_type_procedure` WRITE;
/*!40000 ALTER TABLE `Chorus_type_procedure` DISABLE KEYS */;
INSERT INTO `Chorus_type_procedure` (`id`, `libelle`, `code`) VALUES (1,'Appel d\'offres ouvert','001'),(2,'Appel d\'offres restreint','002'),(3,'Procédure négociée après publicité préalable et mise en concurrence','003'),(4,'Procédure négociée sans publicité préalable et sans mise en concurrence','004'),(5,'Procédure spécifique à certains marchés de la défense','005'),(6,'Dialogue compétitif','006'),(7,'Concours','007'),(8,'Système d\'acquisition dynamique','008'),(9,'Procédure adaptée (\"MAPA\")','009'),(10,'Autre','010');
/*!40000 ALTER TABLE `Chorus_type_procedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Complement`
--

LOCK TABLES `Complement` WRITE;
/*!40000 ALTER TABLE `Complement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Complement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CompteMoniteur`
--

LOCK TABLES `CompteMoniteur` WRITE;
/*!40000 ALTER TABLE `CompteMoniteur` DISABLE KEYS */;
INSERT INTO `CompteMoniteur` (`id`, `organisme`, `moniteur_login`, `moniteur_password`, `moniteur_mail`, `moniteur_target`) VALUES (1,'a1a','Atexo','mdp08eT@','suivi.mpe@atexo.com','1'),(2,'a1a','Atexo','@texo','dev.pmi@atexo.biz','0'),(3,'a2z','Atexo','mdp08eT@','suivi.mpe@atexo.com','1'),(4,'a2z','Atexo','@texo','dev.pmi@atexo.biz','0'),(5,'e3r','Atexo','mdp08eT@','suivi.mpe@atexo.com','1'),(6,'e3r','Atexo','@texo','dev.pmi@atexo.biz','0'),(7,'a1t','Atexo','mdp08eT@','suivi.mpe@atexo.com','1'),(8,'a1t','Atexo','@texo','dev.pmi@atexo.biz','0');
/*!40000 ALTER TABLE `CompteMoniteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ConsultationFormulaire`
--

LOCK TABLES `ConsultationFormulaire` WRITE;
/*!40000 ALTER TABLE `ConsultationFormulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `ConsultationFormulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ConsultationHistoriqueEtat`
--

LOCK TABLES `ConsultationHistoriqueEtat` WRITE;
/*!40000 ALTER TABLE `ConsultationHistoriqueEtat` DISABLE KEYS */;
/*!40000 ALTER TABLE `ConsultationHistoriqueEtat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Contact_Entreprise`
--

LOCK TABLES `Contact_Entreprise` WRITE;
/*!40000 ALTER TABLE `Contact_Entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `Contact_Entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Contrat`
--

LOCK TABLES `Contrat` WRITE;
/*!40000 ALTER TABLE `Contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `Contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Criteres_Evaluation`
--

LOCK TABLES `Criteres_Evaluation` WRITE;
/*!40000 ALTER TABLE `Criteres_Evaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Criteres_Evaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DATEFIN`
--

LOCK TABLES `DATEFIN` WRITE;
/*!40000 ALTER TABLE `DATEFIN` DISABLE KEYS */;
/*!40000 ALTER TABLE `DATEFIN` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DCE`
--

LOCK TABLES `DCE` WRITE;
/*!40000 ALTER TABLE `DCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `DCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DateLancementCron`
--

LOCK TABLES `DateLancementCron` WRITE;
/*!40000 ALTER TABLE `DateLancementCron` DISABLE KEYS */;
/*!40000 ALTER TABLE `DateLancementCron` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DecisionLot`
--

LOCK TABLES `DecisionLot` WRITE;
/*!40000 ALTER TABLE `DecisionLot` DISABLE KEYS */;
/*!40000 ALTER TABLE `DecisionLot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DecisionPassationConsultation`
--

LOCK TABLES `DecisionPassationConsultation` WRITE;
/*!40000 ALTER TABLE `DecisionPassationConsultation` DISABLE KEYS */;
INSERT INTO `DecisionPassationConsultation` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'Poursuite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Infructueux',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Sans suite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `DecisionPassationConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DecisionPassationMarcheAVenir`
--

LOCK TABLES `DecisionPassationMarcheAVenir` WRITE;
/*!40000 ALTER TABLE `DecisionPassationMarcheAVenir` DISABLE KEYS */;
INSERT INTO `DecisionPassationMarcheAVenir` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'Attribué',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Infructueux',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'Sans suite',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `DecisionPassationMarcheAVenir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Departement`
--

LOCK TABLES `Departement` WRITE;
/*!40000 ALTER TABLE `Departement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Departement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DestinataireAnnonceJAL`
--

LOCK TABLES `DestinataireAnnonceJAL` WRITE;
/*!40000 ALTER TABLE `DestinataireAnnonceJAL` DISABLE KEYS */;
/*!40000 ALTER TABLE `DestinataireAnnonceJAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Destinataire_Annonce_Press`
--

LOCK TABLES `Destinataire_Annonce_Press` WRITE;
/*!40000 ALTER TABLE `Destinataire_Annonce_Press` DISABLE KEYS */;
/*!40000 ALTER TABLE `Destinataire_Annonce_Press` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Destinataire_Pub`
--

LOCK TABLES `Destinataire_Pub` WRITE;
/*!40000 ALTER TABLE `Destinataire_Pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `Destinataire_Pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DocumentExterne`
--

LOCK TABLES `DocumentExterne` WRITE;
/*!40000 ALTER TABLE `DocumentExterne` DISABLE KEYS */;
/*!40000 ALTER TABLE `DocumentExterne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `DocumentsAttaches`
--

LOCK TABLES `DocumentsAttaches` WRITE;
/*!40000 ALTER TABLE `DocumentsAttaches` DISABLE KEYS */;
INSERT INTO `DocumentsAttaches` (`id_document`, `nom_document`, `nom_document_fr`, `nom_document_es`, `nom_document_en`, `nom_document_su`, `nom_document_du`, `nom_document_cz`, `nom_document_ar`, `nom_document_it`) VALUES (1,'Certificats fiscaux 3666 volets 1 à 4','','','','','','','',''),(2,'Certificats sociaux','','','','','','','',''),(3,'K-BIS','','','','','','','',''),(4,'NOTI2','','','','','','','',''),(5,'Attestation d\'assurance','','','','','','','',''),(6,'Autre','','','','','','','','');
/*!40000 ALTER TABLE `DocumentsAttaches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Echange`
--

LOCK TABLES `Echange` WRITE;
/*!40000 ALTER TABLE `Echange` DISABLE KEYS */;
/*!40000 ALTER TABLE `Echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EchangeDestinataire`
--

LOCK TABLES `EchangeDestinataire` WRITE;
/*!40000 ALTER TABLE `EchangeDestinataire` DISABLE KEYS */;
/*!40000 ALTER TABLE `EchangeDestinataire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EchangeFormat`
--

LOCK TABLES `EchangeFormat` WRITE;
/*!40000 ALTER TABLE `EchangeFormat` DISABLE KEYS */;
INSERT INTO `EchangeFormat` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'Echange plate-forme','','','','','','','',''),(2,'Courriel hors plate-forme','','','','','','','',''),(3,'Courrier recommandé','','','','','','','',''),(4,'Courrier simple','','','','','','','',''),(5,'Télécopie','','','','','','','',''),(6,'Autre','','','','','','','','');
/*!40000 ALTER TABLE `EchangeFormat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EchangePieceJointe`
--

LOCK TABLES `EchangePieceJointe` WRITE;
/*!40000 ALTER TABLE `EchangePieceJointe` DISABLE KEYS */;
/*!40000 ALTER TABLE `EchangePieceJointe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EchangeTypeAR`
--

LOCK TABLES `EchangeTypeAR` WRITE;
/*!40000 ALTER TABLE `EchangeTypeAR` DISABLE KEYS */;
INSERT INTO `EchangeTypeAR` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'Sans objet','','','','','','','',''),(2,'Non retiré','','','','','','','',''),(3,'Date et heure','','','','','','','','');
/*!40000 ALTER TABLE `EchangeTypeAR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EchangeTypeMessage`
--

LOCK TABLES `EchangeTypeMessage` WRITE;
/*!40000 ALTER TABLE `EchangeTypeMessage` DISABLE KEYS */;
INSERT INTO `EchangeTypeMessage` (`id`, `code`, `corps`) VALUES (1,'DEFINE_COURRIER_NOTIFICATION','Votre entreprise a été déclarée attributaire de la consultation citée en référence.\nVeuillez-trouver à titre de notification le marché signé par le Représentant du Pouvoir Adjudicateur.\nL\'Accusé de réception de ce message vaut notification officielle du marché.\n'),(2,'DEFINE_COURRIER_REJET','Nous vous remercions d\'avoir répondu à la consultation citée en référence.\nNous sommes toutefois au regret de vous annoncer que votre réponse n\'a pas été retenue par le Représentant du Pouvoir Adjudicateur.\nCordialement,\n'),(3,'DEFINE_AVERTISSEMENT_MODIFICATION_CONSULTATION','La consultation citée en référence a été modifiée.\nLes éléments modifiés sont : [à préciser au cas par cas]\nCordialement,\n'),(4,'DEFINE_COURRIER_LIBRE','\nTEXTE COURRIER LIBRE\n'),(5,'DEFINE_COURRIER_DEMANDE_COMPLEMENT','Nous vous remercions d\'avoir répondu à la consultation citée en référence.\nAprès analyse, il vous est demandé d\'apporter les précisions suivantes : [à préciser au cas par cas].\nLa réponse à ces questions peut se faire via l\'application, à partir de la page d\'accès à cette demande de complément.\nIl est nécessaire de disposer d\'un Compte entreprise sur l\'application pour accéder à cette réponse.\nCordialement,'),(6,'DEFINE_COURRIER_REPONSE_A_QUESTION','TEXTE REPONSE A QUESTION\r\n'),(7,'DEFINE_AVERTISSEMENT_ANNULATION_CONSULTATION','La consultation citée en référence a été annulée. Cordialement,');
/*!40000 ALTER TABLE `EchangeTypeMessage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereEntreprisePmi`
--

LOCK TABLES `EnchereEntreprisePmi` WRITE;
/*!40000 ALTER TABLE `EnchereEntreprisePmi` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereEntreprisePmi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereOffre`
--

LOCK TABLES `EnchereOffre` WRITE;
/*!40000 ALTER TABLE `EnchereOffre` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereOffre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereOffreReference`
--

LOCK TABLES `EnchereOffreReference` WRITE;
/*!40000 ALTER TABLE `EnchereOffreReference` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereOffreReference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EncherePmi`
--

LOCK TABLES `EncherePmi` WRITE;
/*!40000 ALTER TABLE `EncherePmi` DISABLE KEYS */;
/*!40000 ALTER TABLE `EncherePmi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereReference`
--

LOCK TABLES `EnchereReference` WRITE;
/*!40000 ALTER TABLE `EnchereReference` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereReference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereTrancheBaremeReference`
--

LOCK TABLES `EnchereTrancheBaremeReference` WRITE;
/*!40000 ALTER TABLE `EnchereTrancheBaremeReference` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereTrancheBaremeReference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereTranchesBaremeNETC`
--

LOCK TABLES `EnchereTranchesBaremeNETC` WRITE;
/*!40000 ALTER TABLE `EnchereTranchesBaremeNETC` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereTranchesBaremeNETC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnchereValeursInitiales`
--

LOCK TABLES `EnchereValeursInitiales` WRITE;
/*!40000 ALTER TABLE `EnchereValeursInitiales` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnchereValeursInitiales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Entreprise`
--

LOCK TABLES `Entreprise` WRITE;
/*!40000 ALTER TABLE `Entreprise` DISABLE KEYS */;
INSERT INTO `Entreprise` (`id`, `admin_id`, `siren`, `repmetiers`, `nom`, `adresse`, `codepostal`, `villeadresse`, `paysadresse`, `email`, `taille`, `formejuridique`, `villeenregistrement`, `motifNonIndNum`, `ordreProfOuAgrement`, `dateConstSociete`, `nomOrgInscription`, `adrOrgInscription`, `dateConstAssoc`, `dateConstAssocEtrangere`, `nomPersonnePublique`, `nationalite`, `redressement`, `paysenregistrement`, `sirenEtranger`, `numAssoEtrangere`, `debutExerciceGlob1`, `finExerciceGlob1`, `debutExerciceGlob2`, `finExerciceGlob2`, `debutExerciceGlob3`, `finExerciceGlob3`, `ventesGlob1`, `ventesGlob2`, `ventesGlob3`, `biensGlob1`, `biensGlob2`, `biensGlob3`, `servicesGlob1`, `servicesGlob2`, `servicesGlob3`, `totalGlob1`, `totalGlob2`, `totalGlob3`, `codeape`, `libelle_ape`, `origine_compte`, `telephone`, `fax`, `site_internet`, `description_activite`, `activite_domaine_defense`, `annee_cloture_exercice1`, `annee_cloture_exercice2`, `annee_cloture_exercice3`, `effectif_moyen1`, `effectif_moyen2`, `effectif_moyen3`, `effectif_encadrement1`, `effectif_encadrement2`, `effectif_encadrement3`, `pme1`, `pme2`, `pme3`, `adresse2`, `nicSiege`, `acronyme_pays`, `date_creation`, `date_modification`, `id_initial`, `region`, `province`, `telephone2`, `telephone3`, `cnss`, `rc_num`, `rc_ville`, `domaines_activites`, `num_tax`, `documents_commerciaux`, `intitule_documents_commerciaux`, `taille_documents_commerciaux`, `qualification`, `agrement`, `moyens_technique`, `moyens_humains`, `compte_actif`, `capital_social`, `ifu`, `id_agent_createur`, `nom_agent`, `prenom_agent`, `adresses_electroniques`, `visible_bourse`, `type_collaboration`, `entreprise_EA`, `entreprise_SIAE`, `saisie_manuelle`, `created_from_decision`, `id_code_effectif`, `categorie_entreprise`) VALUES (1,0,'444914568','','SOCIETEST','231, rue Saint-Honoré','75001','Paris','France',NULL,NULL,'SA',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FR','',NULL,'','','','','','','','','','','','','','','','','','','721Z',NULL,NULL,'','','','','','','','',0,0,0,0,0,0,NULL,NULL,NULL,'','00019','FR','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'','',0,'','',NULL,'0',NULL,'0','0','0','0',NULL,NULL),(2,0,'313955866','','ENTREDEMO','6 Pl d\'Alleray','75015','PARIS 15','France',NULL,NULL,'SARL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FR','',NULL,'','','','','','','','','','','','','','','','','','','642C',NULL,NULL,'','','','','','','','',0,0,0,0,0,0,NULL,NULL,NULL,'','00014','FR','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'','',0,'','',NULL,'0',NULL,'0','0','0','0',NULL,NULL),(3,0,'451491609','','COMPANESSAI','1 Place Carpeaux','92800','PUTEAUX','France',NULL,NULL,'SARL',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'FR','',NULL,'','','','','','','','','','','','','','','','','','','748H',NULL,NULL,'','','','','','','','',0,0,0,0,0,0,NULL,NULL,NULL,'','00017','FR','','',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'','',0,'','',NULL,'0',NULL,'0','0','0','0',NULL,NULL);
/*!40000 ALTER TABLE `Entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EntrepriseInsee`
--

LOCK TABLES `EntrepriseInsee` WRITE;
/*!40000 ALTER TABLE `EntrepriseInsee` DISABLE KEYS */;
/*!40000 ALTER TABLE `EntrepriseInsee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Entreprise_info_exercice`
--

LOCK TABLES `Entreprise_info_exercice` WRITE;
/*!40000 ALTER TABLE `Entreprise_info_exercice` DISABLE KEYS */;
/*!40000 ALTER TABLE `Entreprise_info_exercice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Enveloppe`
--

LOCK TABLES `Enveloppe` WRITE;
/*!40000 ALTER TABLE `Enveloppe` DISABLE KEYS */;
/*!40000 ALTER TABLE `Enveloppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnveloppeCritereEvaluation`
--

LOCK TABLES `EnveloppeCritereEvaluation` WRITE;
/*!40000 ALTER TABLE `EnveloppeCritereEvaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnveloppeCritereEvaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnveloppeFormulaireConsultation`
--

LOCK TABLES `EnveloppeFormulaireConsultation` WRITE;
/*!40000 ALTER TABLE `EnveloppeFormulaireConsultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnveloppeFormulaireConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnveloppeItemCritereEvaluation`
--

LOCK TABLES `EnveloppeItemCritereEvaluation` WRITE;
/*!40000 ALTER TABLE `EnveloppeItemCritereEvaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnveloppeItemCritereEvaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EnveloppeItemFormulaireConsultationValues`
--

LOCK TABLES `EnveloppeItemFormulaireConsultationValues` WRITE;
/*!40000 ALTER TABLE `EnveloppeItemFormulaireConsultationValues` DISABLE KEYS */;
/*!40000 ALTER TABLE `EnveloppeItemFormulaireConsultationValues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Enveloppe_papier`
--

LOCK TABLES `Enveloppe_papier` WRITE;
/*!40000 ALTER TABLE `Enveloppe_papier` DISABLE KEYS */;
/*!40000 ALTER TABLE `Enveloppe_papier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Etape_cao`
--

LOCK TABLES `Etape_cao` WRITE;
/*!40000 ALTER TABLE `Etape_cao` DISABLE KEYS */;
/*!40000 ALTER TABLE `Etape_cao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EtatConsultation`
--

LOCK TABLES `EtatConsultation` WRITE;
/*!40000 ALTER TABLE `EtatConsultation` DISABLE KEYS */;
INSERT INTO `EtatConsultation` (`id_etat`, `code_etat`, `abreviation_etat`, `visible`) VALUES (1,'DEFINE_PREPARATION','P',0),(2,'DEFINE_CONSULTATION','C',0),(3,'DEFINE_OUVERTURE_ANALYSE','OA',1),(4,'DEFINE_DECISION','D',1),(5,'DEFINE_A_ARCHIVER','AA',1),(6,'DEFINE_TEXT_ARCHIVE_REALISEE','AR',1);
/*!40000 ALTER TABLE `EtatConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Faq_Entreprise`
--

LOCK TABLES `Faq_Entreprise` WRITE;
/*!40000 ALTER TABLE `Faq_Entreprise` DISABLE KEYS */;
INSERT INTO `Faq_Entreprise` (`id`, `question`, `question_fr`, `question_en`, `question_es`, `reponse`, `reponse_fr`, `reponse_en`, `reponse_es`, `question_it`, `reponse_it`, `theme`, `ordre`) VALUES (1,'Que faire si l\'applet Java ne se charge pas ?','Que faire si l\'applet Java ne se charge pas ?','How to proceed if the Java applet does not load?','¿Qué pasa si el applet de Java no se carga?','<p>Vérifier que votre poste est conforme aux \"Conditions d\'utilisation\" du site qui présente les pré-requis d\'installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>','<p>Vérifier que votre poste est conforme aux \"Conditions d\'utilisation\" du site qui présente les pré-requis d\'installation et de configuration des postes de travail (ex : version de la machine java, fichiers à installer, etc.).</p>','<p>Make sure your equipment and system meet all the requirements defined in the \"Terms of use\" section (e.g.: Java version, file download, etc.).</p>','<p>Compruebe que su puesto está en conformidad con \"las condiciones de uso\" del sitio que presenta los requisitos previos de instalación y configuración de los puestos de trabajo (por ejemplo, la versión de la máquina para instalar los archivos java, etc.).</p>','','','',0),(2,'Que dois-je faire lorsque lors de la remise d\'une réponse, le message suivant s\'affiche \"Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation?\" ?','Que dois-je faire lorsque lors de la remise d\'une réponse, le message suivant s\'affiche \"Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation?\" ?','How to proceed if I get the following message while submitting a bid \"Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation\"?','¿Qué debo hacer cuando al momento de presentar una respuesta, el siguiente mensaje \"Les fichiers d\'autorisation de chiffrement cryptographique sont absents de votre poste, veuillez vous conférer aux conditions d\'utilisation\" ?','<p>Les \"Conditions d\'utilisation\" indiquent la marche à suivre pour installer ces fichiers manquants.</p>','<p>Les \"Conditions d\'utilisation\" indiquent la marche à suivre pour installer ces fichiers manquants.</p>','<p>You will find the procedure to follow in the \"Terms of use\" section in order to install the missing files.</p>','<p>Las \"Condiciones de utilización\" muestran cómo instalar estos archivos que faltan.</p>','','','',0),(3,'Que faire si j\'ai oublié mon identifiant ou mon mot de passe de connexion ?','Que faire si j\'ai oublié mon identifiant ou mon mot de passe de connexion ?','How to proceed if I have lost my ID and password?','¿Qué sucede si olvido mi nombre de usuario o mi contraseña de inicio de sesión?','<p>La fonction \"Mot de passe oublié\" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>','<p>La fonction \"Mot de passe oublié\" vous permet de recevoir votre identifiant et votre nouveau mot de passe.</p>','<p>Click the \"Forgotten password\" link and fill in your e-mail address to retrieve your ID and password.</p>','<p>La función \"¿Olvidaste tu contraseña?\" le permite recibir su login y contraseña.</p>','','','',0),(4,'Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?','Que faire si je ne retrouve pas une consultation restreinte sur la PMI ?','I cannot find a restricted tender on the PMI website. What can I do?','¿Qué sucede si no se encuentra un procedimiento restringido?','<p>Il est nécessaire d\'utiliser le moteur de recherche avancée avec la référence et le code d\'accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l\'assistance téléphonique.</p>','<p>Il est nécessaire d\'utiliser le moteur de recherche avancée avec la référence et le code d\'accès transmis par le pouvoir adjudicateur. Vous aurez ainsi accès aux données de la consultation restreinte. En cas de difficulté, contactez l\'assistance téléphonique.</p>','<p>Use the advanced search page to search the reference and the access code provided by the public entity.<br />You will then have access to the restricted tender.<br />If you encounter any problem, please contact our hotline.</p>','<p>Es necesario utilizar la búsqueda avanzada con la referencia y el código de acceso transmitido por la autoridad contratante.<br />Usted tendrá acceso a los datos del procedimiento restringido.<br />En caso de dificultad, póngase en contacto con la línea directa.</p>','','','',0),(5,'Est ce que l\'entreprise reçoit un AR quand elle pose une question via la plate-forme ?','Est ce que l\'entreprise reçoit un AR quand elle pose une question via la plate-forme ?','Does the company receive an AR (acknowledgement of receipt) when submitting a question on the platform?','¿La empresa recibe un AR cuando ella hace una pregunta a través de la plataforma?','<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>','<p>Non. Seuls les dépôts de plis donnent lieu à un accusé de réception.</p>','<p>No. An acknowledgement of receipt is sent to those submitting a bid only.</p>','<p>No. Sólo los depósitos de pliegues dan lugar a un recibo.</p>','','','',0),(6,'Pourquoi je ne retrouve pas une consultation publique sur le portail ?','Pourquoi je ne retrouve pas une consultation publique sur le portail ?','Why can not I find a public tender on the portal?','¿Por qué no puedo encontrar una consulta pública en el portal?','<p>Veuillez vérifier que la date limite de remise des plis n\'est pas déjà passée. Si ce n\'est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n\'a pas été retirée par le Pouvoir adjudicateur.</p>','<p>Veuillez vérifier que la date limite de remise des plis n\'est pas déjà passée. Si ce n\'est pas le cas, vérifiez votre requête ou assurez-vous que la consultation n\'a pas été retirée par le Pouvoir adjudicateur.</p>','<p>Please make sure the closing date has not passed. Otherwise, make sure the tender has not been removed by the public entity.</p>','<p>Por favor, verifique que la fecha límite para la presentación de los pliegues no ha pasado. Si este no es el caso, compruebe su solicitud o asegúrese de que la consulta no ha sido retirada por la Autoridad contratante.</p>','','','',0),(7,'Pourquoi n\'ai-je pas reçu d\'accusé de réception par courriel après mon dépôt de pli ?','Pourquoi n\'ai-je pas reçu d\'accusé de réception par courriel après mon dépôt de pli ?','Why haven\'t I received any AR by mail after I submitted my envelop?','¿Por qué no recibo un acuse por correo después de la presentación de mi pliego?','<p>L\'application vous envoie un AR par courriel lorsque l\'écran de confirmation de bonne réception de votre envoi s\'affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>','<p>L\'application vous envoie un AR par courriel lorsque l\'écran de confirmation de bonne réception de votre envoi s\'affiche.\r\nVérifier que votre adresse électronique a été correctement saisie dans votre compte sur la plate-forme, ou que ce courriel ne se trouve pas dans vos courriels indésirables.</p>','<p>You will receive an AR by mail once a confirmation message is displayed on screen. <br />Make sure the email address has been correctly entered in your account or check your spam folder.</p>','<p>La aplicación le envía un AR por correo electrónico cuando la pantalla de confirmación de recibo de su envío aparece. <br />Compruebe que su dirección de correo electrónico está escrito correctamente en su cuenta en la plataforma, o que el correo electrónico no está en su correo basura.</p>','','','',0),(8,'Les enveloppes de candidature ou d\'offre peuvent-elles être envoyées au format PDF?','Les enveloppes de candidature ou d\'offre peuvent-elles être envoyées au format PDF?','Can the application /bid envelops be sent in a PDF format?','¿Los sobres de candidatura u ofertas pueden ser enviados en formato PDF?','<p>Non. Seul le format ZIP est accepté.</p>','<p>Non. Seul le format ZIP est accepté.</p>','<p>No. ZIP is the only format allowed.</p>','<p>No. Sólo el formato ZIP es aceptado.</p>','','','',0);
/*!40000 ALTER TABLE `Faq_Entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Fcsp_Lieu`
--

LOCK TABLES `Fcsp_Lieu` WRITE;
/*!40000 ALTER TABLE `Fcsp_Lieu` DISABLE KEYS */;
/*!40000 ALTER TABLE `Fcsp_Lieu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Fcsp_Mandataire`
--

LOCK TABLES `Fcsp_Mandataire` WRITE;
/*!40000 ALTER TABLE `Fcsp_Mandataire` DISABLE KEYS */;
INSERT INTO `Fcsp_Mandataire` (`id_auto`, `id`, `organisme`, `libelle`) VALUES (1,1,'','Mandataire UL - SOGEFI'),(2,2,'','Mandataire UO - SIPARC'),(3,3,'','Mandataire UO - RIALI');
/*!40000 ALTER TABLE `Fcsp_Mandataire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Fcsp_unite`
--

LOCK TABLES `Fcsp_unite` WRITE;
/*!40000 ALTER TABLE `Fcsp_unite` DISABLE KEYS */;
/*!40000 ALTER TABLE `Fcsp_unite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `FormXmlDestinataireOpoce`
--

LOCK TABLES `FormXmlDestinataireOpoce` WRITE;
/*!40000 ALTER TABLE `FormXmlDestinataireOpoce` DISABLE KEYS */;
/*!40000 ALTER TABLE `FormXmlDestinataireOpoce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GeolocalisationN0`
--

LOCK TABLES `GeolocalisationN0` WRITE;
/*!40000 ALTER TABLE `GeolocalisationN0` DISABLE KEYS */;
INSERT INTO `GeolocalisationN0` (`id`, `denomination`, `denomination_fr`, `denomination_en`, `denomination_es`, `type`, `actif`, `libelle_selectionner`, `libelle_selectionner_fr`, `libelle_selectionner_en`, `libelle_selectionner_es`, `libelle_tous`, `libelle_tous_fr`, `libelle_tous_en`, `libelle_tous_es`, `libelle_Aucun`, `libelle_Aucun_fr`, `libelle_Aucun_en`, `libelle_Aucun_es`, `denomination_ar`, `libelle_selectionner_ar`, `libelle_tous_ar`, `libelle_Aucun_ar`, `denomination_su`, `libelle_selectionner_su`, `libelle_tous_su`, `libelle_Aucun_su`, `denomination_du`, `libelle_selectionner_du`, `libelle_tous_du`, `libelle_Aucun_du`, `denomination_cz`, `libelle_selectionner_cz`, `libelle_tous_cz`, `libelle_Aucun_cz`, `denomination_it`, `libelle_selectionner_it`, `libelle_tous_it`, `libelle_Aucun_it`) VALUES (1,'France métropolitaine','France métropolitaine','Metropolitan France','Francia Metropolitana',2,'1','Sélectionner par département(s)','Sélectionner par département(s)','Select by department(s)','Seleccionar por Departamento(s)','Tous les départements','Tous les départements','All departments','Todos los Departamentos','Aucun département','Aucun département','No Department','Ningún Departamento','','','','','','','','','','','','','','','','','','','',''),(2,'France - Collectivité(s) d\'Outre-Mer','France - Collectivité(s) d\'Outre-Mer','France - overseas community','Francia - Colectividade(s) de Ultra-Mar',0,'0','Sélectionner par collectivité(s)','Sélectionner par collectivité(s)','Select by community','Seleccionar por Colectividade(s)','Toutes les collectivités','Toutes les collectivités','All community','Todas las Colectividades','Aucune collectivité','Aucune collectivité','No community','Ninguna Colectividad','','','','','','','','','','','','','','','','','','','',''),(3,'Autre(s) pays','Autre(s) pays','Others countries','Otro(s) pais(es)',1,'1','Sélectionner par pays','Sélectionner par pays','Select by country','Seleccionar por pais(es)','Tous les pays','Tous les pays','All countries','Todos los pais(es)','Aucun pays','Aucun pays','No countries','Ningún pais','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `GeolocalisationN0` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GeolocalisationN1`
--

LOCK TABLES `GeolocalisationN1` WRITE;
/*!40000 ALTER TABLE `GeolocalisationN1` DISABLE KEYS */;
INSERT INTO `GeolocalisationN1` (`id`, `id_geolocalisationN0`, `denomination1`, `denomination2`, `denomination1_ar`, `denomination2_ar`, `denomination1_fr`, `denomination2_fr`, `denomination1_en`, `denomination2_en`, `denomination1_es`, `denomination2_es`, `denomination1_su`, `denomination2_su`, `denomination1_du`, `denomination2_du`, `denomination1_cz`, `denomination2_cz`, `denomination1_it`, `denomination2_it`) VALUES (40,1,'Alsace','','','','','','','','','','','','','','','','',''),(41,1,'Aquitaine','','','','','','','','','','','','','','','','',''),(42,1,'Auvergne','','','','','','','','','','','','','','','','',''),(43,1,'Basse-Normandie','','','','','','','','','','','','','','','','',''),(44,1,'Bourgogne','','','','','','','','','','','','','','','','',''),(45,1,'Bretagne','','','','','','','','','','','','','','','','',''),(46,1,'Centre','','','','','','','','','','','','','','','','',''),(48,1,'Corse','','','','','','','','','','','','','','','','',''),(49,1,'Franche-Comté','','','','','','','','','','','','','','','','',''),(50,1,'Haute-Normandie','','','','','','','','','','','','','','','','',''),(51,1,'Champagne-Ardenne','','','','','','','','','','','','','','','','',''),(52,1,'Ile-de-France','','','','','','','','','','','','','','','','',''),(53,1,'Languedoc-Roussillon','','','','','','','','','','','','','','','','',''),(54,1,'Limousin','','','','','','','','','','','','','','','','',''),(55,1,'Lorraine','','','','','','','','','','','','','','','','',''),(56,1,'Midi-Pyrénées','','','','','','','','','','','','','','','','',''),(57,1,'Nord-Pas-de-Calais','','','','','','','','','','','','','','','','',''),(58,1,'Pays-de-la-Loire','','','','','','','','','','','','','','','','',''),(59,1,'Picardie','','','','','','','','','','','','','','','','',''),(60,1,'Poitou-Charentes','','','','','','','','','','','','','','','','',''),(61,1,'Provence-Alpes Côte d\'Azur','','','','','','','','','','','','','','','','',''),(62,1,'Rhône-Alpes','','','','','','','','','','','','','','','','',''),(233,2,'Départements d\'Outre-Mer','','','','','','','','','','','','','','','','',''),(234,3,'Pays','','','','','','','','','','','','','','','','',''),(235,3,'Pays','','','','','','','','','','','','','','','','',''),(236,3,'Pays','','','','','','','','','','','','','','','','',''),(237,3,'Pays','','','','','','','','','','','','','','','','',''),(238,3,'Pays','','','','','','','','','','','','','','','','',''),(239,3,'Pays','','','','','','','','','','','','','','','','',''),(240,3,'Pays','','','','','','','','','','','','','','','','',''),(241,3,'Pays','','','','','','','','','','','','','','','','',''),(242,3,'Pays','','','','','','','','','','','','','','','','',''),(243,3,'Pays','','','','','','','','','','','','','','','','',''),(244,3,'Pays','','','','','','','','','','','','','','','','',''),(245,3,'Pays','','','','','','','','','','','','','','','','',''),(246,3,'Pays','','','','','','','','','','','','','','','','',''),(247,3,'Pays','','','','','','','','','','','','','','','','',''),(248,3,'Pays','','','','','','','','','','','','','','','','',''),(249,3,'Pays','','','','','','','','','','','','','','','','',''),(250,3,'Pays','','','','','','','','','','','','','','','','',''),(251,3,'Pays','','','','','','','','','','','','','','','','',''),(252,3,'Pays','','','','','','','','','','','','','','','','',''),(253,3,'Pays','','','','','','','','','','','','','','','','',''),(254,3,'Pays','','','','','','','','','','','','','','','','',''),(255,3,'Pays','','','','','','','','','','','','','','','','',''),(256,3,'Pays','','','','','','','','','','','','','','','','',''),(257,3,'Pays','','','','','','','','','','','','','','','','',''),(258,3,'Pays','','','','','','','','','','','','','','','','',''),(259,3,'Pays','','','','','','','','','','','','','','','','',''),(260,3,'Pays','','','','','','','','','','','','','','','','',''),(261,3,'Pays','','','','','','','','','','','','','','','','',''),(262,3,'Pays','','','','','','','','','','','','','','','','',''),(263,3,'Pays','','','','','','','','','','','','','','','','',''),(264,3,'Pays','','','','','','','','','','','','','','','','',''),(265,3,'Pays','','','','','','','','','','','','','','','','',''),(266,3,'Pays','','','','','','','','','','','','','','','','',''),(267,3,'Pays','','','','','','','','','','','','','','','','',''),(268,3,'Pays','','','','','','','','','','','','','','','','',''),(269,3,'Pays','','','','','','','','','','','','','','','','',''),(270,3,'Pays','','','','','','','','','','','','','','','','',''),(271,3,'Pays','','','','','','','','','','','','','','','','',''),(272,3,'Pays','','','','','','','','','','','','','','','','',''),(273,3,'Pays','','','','','','','','','','','','','','','','',''),(274,3,'Pays','','','','','','','','','','','','','','','','',''),(275,3,'Pays','','','','','','','','','','','','','','','','',''),(276,3,'Pays','','','','','','','','','','','','','','','','',''),(277,3,'Pays','','','','','','','','','','','','','','','','',''),(278,3,'Pays','','','','','','','','','','','','','','','','',''),(279,3,'Pays','','','','','','','','','','','','','','','','',''),(280,3,'Pays','','','','','','','','','','','','','','','','',''),(281,3,'Pays','','','','','','','','','','','','','','','','',''),(282,3,'Pays','','','','','','','','','','','','','','','','',''),(283,3,'Pays','','','','','','','','','','','','','','','','',''),(284,3,'Pays','','','','','','','','','','','','','','','','',''),(285,3,'Pays','','','','','','','','','','','','','','','','',''),(286,3,'Pays','','','','','','','','','','','','','','','','',''),(287,3,'Pays','','','','','','','','','','','','','','','','',''),(288,3,'Pays','','','','','','','','','','','','','','','','',''),(289,3,'Pays','','','','','','','','','','','','','','','','',''),(290,3,'Pays','','','','','','','','','','','','','','','','',''),(291,3,'Pays','','','','','','','','','','','','','','','','',''),(292,3,'Pays','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `GeolocalisationN1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GeolocalisationN2`
--

LOCK TABLES `GeolocalisationN2` WRITE;
/*!40000 ALTER TABLE `GeolocalisationN2` DISABLE KEYS */;
INSERT INTO `GeolocalisationN2` (`id`, `id_geolocalisationN1`, `denomination1`, `denomination2`, `valeur_avec_sous_categorie`, `denomination1_ar`, `denomination2_ar`, `denomination1_fr`, `denomination2_fr`, `denomination1_en`, `denomination2_en`, `denomination1_es`, `denomination2_es`, `denomination1_su`, `denomination2_su`, `denomination1_du`, `denomination2_du`, `denomination1_cz`, `denomination2_cz`, `denomination1_it`, `denomination2_it`, `code_interface`, `valeur_sub`, `type_lieu`) VALUES (214,40,'(67) Bas-Rhin','67','1','','','','','','','','','','','','','','','','','FR-67','066','1'),(215,40,'(68) Haut-Rhin','68','1','','','','','','','','','','','','','','','','','FR-68','067','1'),(216,41,'(24) Dordogne','24','1','','','','','','','','','','','','','','','','','FR-24','023','1'),(217,41,'(33) Gironde','33','1','','','','','','','','','','','','','','','','','FR-33','032','1'),(218,41,'(40) Landes','40','1','','','','','','','','','','','','','','','','','FR-40','039','1'),(219,41,'(47) Lot-et-Garonne','47','1','','','','','','','','','','','','','','','','','FR-47','046','1'),(220,41,'(64) Pyrénées-Atlantiques','64','1','','','','','','','','','','','','','','','','','FR-64','063','1'),(221,42,'(03) Allier','03','1','','','','','','','','','','','','','','','','','FR-03','003','1'),(222,42,'(15) Cantal','15','1','','','','','','','','','','','','','','','','','FR-15','015','1'),(223,42,'(43) Haute-Loire','43','1','','','','','','','','','','','','','','','','','FR-43','042','1'),(224,42,'(63) Puy-de-Dôme','63','1','','','','','','','','','','','','','','','','','FR-63','062','1'),(225,43,'(14) Calvados','14','1','','','','','','','','','','','','','','','','','FR-14','014','1'),(226,43,'(50) Manche','50','1','','','','','','','','','','','','','','','','','FR-50','049','1'),(227,43,'(61) Orne','61','1','','','','','','','','','','','','','','','','','FR-61','060','1'),(228,44,'(21) Côte-d\'Or','21','1','','','','','','','','','','','','','','','','','FR-21','020','1'),(229,44,'(58) Nièvre','58','1','','','','','','','','','','','','','','','','','FR-58','057','1'),(230,44,'(71) Saône-et-Loire','71','1','','','','','','','','','','','','','','','','','FR-71','070','1'),(231,44,'(89) Yonne','89','1','','','','','','','','','','','','','','','','','FR-89','088','1'),(232,45,'(22) Côtes-d\'Armor','22','1','','','','','','','','','','','','','','','','','FR-22','021','1'),(233,45,'(29) Finistère','29','1','','','','','','','','','','','','','','','','','FR-29','028','1'),(234,45,'(35) Ille-et-Vilaine','35','1','','','','','','','','','','','','','','','','','FR-35','034','1'),(235,45,'(56) Morbihan','56','1','','','','','','','','','','','','','','','','','FR-56','055','1'),(236,46,'(18) Cher','18','1','','','','','','','','','','','','','','','','','FR-18','018','1'),(237,46,'(28) Eure-et-Loir','28','1','','','','','','','','','','','','','','','','','FR-28','027','1'),(238,46,'(36) Indre','36','1','','','','','','','','','','','','','','','','','FR-36','035','1'),(239,46,'(37) Indre-et-Loire','37','1','','','','','','','','','','','','','','','','','FR-37','036','1'),(240,46,'(41) Loir-et-Cher','41','1','','','','','','','','','','','','','','','','','FR-41','040','1'),(241,46,'(45) Loiret','45','1','','','','','','','','','','','','','','','','','FR-45','044','1'),(242,48,'(2A) Corse-du-Sud','2A','1','','','','','','','','','','','','','','','','','FR-2A','095','1'),(243,48,'(2B) Haute-Corse','2B','1','','','','','','','','','','','','','','','','','FR-2B','096','1'),(244,49,'(25) Doubs','25','1','','','','','','','','','','','','','','','','','FR-25','024','1'),(245,49,'(39) Jura','39','1','','','','','','','','','','','','','','','','','FR-39','038','1'),(246,49,'(70) Haute-Saône','70','1','','','','','','','','','','','','','','','','','FR-70','069','1'),(247,49,'(90) Territoire-de-Belfort','90','1','','','','','','','','','','','','','','','','','FR-90','089','1'),(248,50,'(27) Eure','27','1','','','','','','','','','','','','','','','','','FR-27','026','1'),(249,50,'(76) Seine-Maritime','76','1','','','','','','','','','','','','','','','','','FR-76','075','1'),(250,51,'(08) Ardennes','08','1','','','','','','','','','','','','','','','','','FR-08','008','1'),(251,51,'(10) Aube','10','1','','','','','','','','','','','','','','','','','FR-10','010','1'),(252,51,'(51) Marne','51','1','','','','','','','','','','','','','','','','','FR-51','050','1'),(253,51,'(52) Haute-Marne','52','1','','','','','','','','','','','','','','','','','FR-52','051','1'),(254,52,'(75) Paris','75','1','','','','','','','','','','','','','','','','','FR-75','074','1'),(255,52,'(77) Seine-et-Marne','77','1','','','','','','','','','','','','','','','','','FR-77','076','1'),(256,52,'(78) Yvelines','78','1','','','','','','','','','','','','','','','','','FR-78','077','1'),(257,52,'(91) Essonne','91','1','','','','','','','','','','','','','','','','','FR-91','090','1'),(258,52,'(92) Hauts-de-Seine','92','1','','','','','','','','','','','','','','','','','FR-92','091','1'),(259,52,'(93) Seine-Saint-Denis','93','1','','','','','','','','','','','','','','','','','FR-93','092','1'),(260,52,'(94) Val-de-Marne','94','1','','','','','','','','','','','','','','','','','FR-94','093','1'),(261,52,'(95) Val-d\'Oise','95','1','','','','','','','','','','','','','','','','','FR-95','094','1'),(262,53,'(11) Aude','11','1','','','','','','','','','','','','','','','','','FR-11','011','1'),(263,53,'(30) Gard','30','1','','','','','','','','','','','','','','','','','FR-30','029','1'),(264,53,'(34) Hérault','34','1','','','','','','','','','','','','','','','','','FR-34','033','1'),(265,53,'(48) Lozère','48','1','','','','','','','','','','','','','','','','','FR-48','047','1'),(266,53,'(66) Pyrénées-Orientales','66','1','','','','','','','','','','','','','','','','','FR-66','065','1'),(267,54,'(19) Corrèze','19','1','','','','','','','','','','','','','','','','','FR-19','019','1'),(268,54,'(23) Creuse','23','1','','','','','','','','','','','','','','','','','FR-23','022','1'),(269,54,'(87) Haute-Vienne','87','1','','','','','','','','','','','','','','','','','FR-87','086','1'),(270,55,'(54) Meurthe-et-Moselle','54','1','','','','','','','','','','','','','','','','','FR-54','053','1'),(271,55,'(55) Meuse','55','1','','','','','','','','','','','','','','','','','FR-55','054','1'),(272,55,'(57) Moselle','57','1','','','','','','','','','','','','','','','','','FR-57','056','1'),(273,55,'(88) Vosges','88','1','','','','','','','','','','','','','','','','','FR-88','087','1'),(274,56,'(09) Ariège','09','1','','','','','','','','','','','','','','','','','FR-09','009','1'),(275,56,'(12) Aveyron','12','1','','','','','','','','','','','','','','','','','FR-12','012','1'),(276,56,'(31) Haute-Garonne','31','1','','','','','','','','','','','','','','','','','FR-31','030','1'),(277,56,'(32) Gers','32','1','','','','','','','','','','','','','','','','','FR-32','031','1'),(278,56,'(46) Lot','46','1','','','','','','','','','','','','','','','','','FR-46','045','1'),(279,56,'(65) Hautes-Pyrénées','65','1','','','','','','','','','','','','','','','','','FR-65','064','1'),(280,56,'(81) Tarn','81','1','','','','','','','','','','','','','','','','','FR-81','080','1'),(281,56,'(82) Tarn-et-Garonne','82','1','','','','','','','','','','','','','','','','','FR-82','081','1'),(282,57,'(59) Nord','59','1','','','','','','','','','','','','','','','','','FR-59','058','1'),(283,57,'(62) Pas-de-Calais','62','1','','','','','','','','','','','','','','','','','FR-62','061','1'),(284,58,'(44) Loire-Atlantique','44','1','','','','','','','','','','','','','','','','','FR-44','043','1'),(285,58,'(49) Maine-et-Loire','49','1','','','','','','','','','','','','','','','','','FR-49','048','1'),(286,58,'(53) Mayenne','53','1','','','','','','','','','','','','','','','','','FR-53','052','1'),(287,58,'(72) Sarthe','72','1','','','','','','','','','','','','','','','','','FR-72','071','1'),(288,58,'(85) Vendée','85','1','','','','','','','','','','','','','','','','','FR-85','084','1'),(289,59,'(02) Aisne','02','1','','','','','','','','','','','','','','','','','FR-02','002','1'),(290,59,'(60) Oise','60','1','','','','','','','','','','','','','','','','','FR-60','059','1'),(291,59,'(80) Somme','80','1','','','','','','','','','','','','','','','','','FR-80','079','1'),(292,60,'(16) Charente','16','1','','','','','','','','','','','','','','','','','FR-16','016','1'),(293,60,'(17) Charente-Maritime','17','1','','','','','','','','','','','','','','','','','FR-17','017','1'),(294,60,'(79) Deux-Sèvres','79','1','','','','','','','','','','','','','','','','','FR-79','078','1'),(295,60,'(86) Vienne','86','1','','','','','','','','','','','','','','','','','FR-86','085','1'),(296,61,'(04) Alpes-de-Haute-Provence','04','1','','','','','','','','','','','','','','','','','FR-04','004','1'),(297,61,'(05) Hautes-Alpes','05','1','','','','','','','','','','','','','','','','','FR-05','005','1'),(298,61,'(06) Alpes-Maritimes','06','1','','','','','','','','','','','','','','','','','FR-06','006','1'),(299,61,'(13) Bouches-du-Rhône','13','1','','','','','','','','','','','','','','','','','FR-13','013','1'),(300,61,'(83) Var','83','1','','','','','','','','','','','','','','','','','FR-83','082','1'),(301,61,'(84) Vaucluse','84','1','','','','','','','','','','','','','','','','','FR-84','083','1'),(302,62,'(01) Ain','01','1','','','','','','','','','','','','','','','','','FR-01','001','1'),(303,62,'(07) Ardèche','07','1','','','','','','','','','','','','','','','','','FR-07','007','1'),(304,62,'(26) Drôme','26','1','','','','','','','','','','','','','','','','','FR-26','025','1'),(305,62,'(38) Isère','38','1','','','','','','','','','','','','','','','','','FR-38','037','1'),(306,62,'(42) Loire','42','1','','','','','','','','','','','','','','','','','FR-42','041','1'),(307,62,'(69) Rhône','69','1','','','','','','','','','','','','','','','','','FR-69','068','1'),(308,62,'(73) Savoie','73','1','','','','','','','','','','','','','','','','','FR-73','072','1'),(309,62,'(74) Haute-Savoie','74','1','','','','','','','','','','','','','','','','','FR-74','073','1'),(713,233,'GUADELOUPE','GP','1','','','','','','','','','','','','','','','','','FR-GP','097',''),(714,233,'GUYANE FRANÇAISE','GF','1','','','','','','','','','','','','','','','','','FR-GF','099',''),(715,233,'MARTINIQUE','MQ','1','','','','','','','','','','','','','','','','','FR-MQ','098',''),(716,233,'MAYOTTE','YT','1','','','','','','','','','','','','','','','','','FR-YT','20B',''),(717,233,'NOUVELLE-CALÉDONIE','NC','1','','','','','','','','','','','','','','','','','NC','',''),(718,233,'POLYNÉSIE FRANÇAISE','PF','1','','','','','','','','','','','','','','','','','PF','',''),(719,233,'RÉUNION','RE','1','','','','','','','','','','','','','','','','','FR-RE','20A',''),(720,233,'SAINT-BARTHÉLEMY','BL','1','','','','','','','','','','','','','','','','','FR-BL','',''),(721,233,'SAINT-MARTIN','MF','1','','','','','','','','','','','','','','','','','FR-MF','',''),(722,233,'SAINT-PIERRE-ET-MIQUELON','PM','1','','','','','','','','','','','','','','','','','FR-PM','',''),(723,233,'TERRES AUSTRALES FRANÇAISES','TF','1','','','','','','','','','','','','','','','','','FR-TF','',''),(724,233,'WALLIS ET FUTUNA','WF','1','','','','','','','','','','','','','','','','','FR-WF','',''),(6235,234,'AFGHANISTAN','AF','1','','','','','','','','','','','','','','','','','AF','',''),(6236,234,'AFRIQUE DU SUD','ZA','1','','','','','','','','','','','','','','','','','ZA','',''),(6237,234,'ÂLAND, ÎLES','AX','1','','','','','','','','','','','','','','','','','AX','',''),(6238,234,'ALBANIE','AL','1','','','','','','','','','','','','','','','','','AL','',''),(6239,234,'ALGÉRIE','DZ','1','','','','','','','','','','','','','','','','','DZ','',''),(6240,234,'ALLEMAGNE','DE','1','','','','','','','','','','','','','','','','','DE','',''),(6241,234,'ANDORRE','AD','1','','','','','','','','','','','','','','','','','AD','',''),(6242,234,'ANGOLA','AO','1','','','','','','','','','','','','','','','','','AO','',''),(6243,234,'ANGUILLA','AI','1','','','','','','','','','','','','','','','','','AI','',''),(6244,234,'ANTARCTIQUE','AQ','1','','','','','','','','','','','','','','','','','AQ','',''),(6245,234,'ANTIGUA-ET-BARBUDA','AG','1','','','','','','','','','','','','','','','','','AG','',''),(6246,234,'ANTILLES NÉERLANDAISES','AN','1','','','','','','','','','','','','','','','','','AN','',''),(6247,234,'ARABIE SAOUDITE','SA','1','','','','','','','','','','','','','','','','','SA','',''),(6248,234,'ARGENTINE','AR','1','','','','','','','','','','','','','','','','','AR','',''),(6249,234,'ARMÉNIE','AM','1','','','','','','','','','','','','','','','','','AM','',''),(6250,234,'ARUBA','AW','1','','','','','','','','','','','','','','','','','AW','',''),(6251,234,'AUSTRALIE','AU','1','','','','','','','','','','','','','','','','','AU','',''),(6252,234,'AUTRICHE','AT','1','','','','','','','','','','','','','','','','','AT','',''),(6253,234,'AZERBAÏDJAN','AZ','1','','','','','','','','','','','','','','','','','AZ','',''),(6254,234,'BAHAMAS','BS','1','','','','','','','','','','','','','','','','','BS','',''),(6255,234,'BAHREÏN','BH','1','','','','','','','','','','','','','','','','','BH','',''),(6256,234,'BANGLADESH','BD','1','','','','','','','','','','','','','','','','','BD','',''),(6257,234,'BARBADE','BB','1','','','','','','','','','','','','','','','','','BB','',''),(6258,234,'BÉLARUS','BY','1','','','','','','','','','','','','','','','','','BY','',''),(6259,234,'BELGIQUE','BE','1','','','','','','','','','','','','','','','','','BE','',''),(6260,234,'BELIZE','BZ','1','','','','','','','','','','','','','','','','','BZ','',''),(6261,234,'BÉNIN','BJ','1','','','','','','','','','','','','','','','','','BJ','',''),(6262,234,'BERMUDES','BM','1','','','','','','','','','','','','','','','','','BM','',''),(6263,234,'BHOUTAN','BT','1','','','','','','','','','','','','','','','','','BT','',''),(6264,234,'BOLIVIE','BO','1','','','','','','','','','','','','','','','','','BO','',''),(6265,234,'BOSNIE-HERZÉGOVINE','BA','1','','','','','','','','','','','','','','','','','BA','',''),(6266,234,'BOTSWANA','BW','1','','','','','','','','','','','','','','','','','BW','',''),(6267,234,'BOUVET, ÎLE','BV','1','','','','','','','','','','','','','','','','','BV','',''),(6268,234,'BRÉSIL','BR','1','','','','','','','','','','','','','','','','','BR','',''),(6269,234,'BRUNÉI DARUSSALAM','BN','1','','','','','','','','','','','','','','','','','BN','',''),(6270,234,'BULGARIE','BG','1','','','','','','','','','','','','','','','','','BG','',''),(6271,234,'BURKINA FASO','BF','1','','','','','','','','','','','','','','','','','BF','',''),(6272,234,'BURUNDI','BI','1','','','','','','','','','','','','','','','','','BI','',''),(6273,234,'CAÏMANES, ÎLES','KY','1','','','','','','','','','','','','','','','','','KY','',''),(6274,234,'CAMBODGE','KH','1','','','','','','','','','','','','','','','','','KH','',''),(6275,234,'CAMEROUN','CM','1','','','','','','','','','','','','','','','','','CM','',''),(6276,234,'CANADA','CA','1','','','','','','','','','','','','','','','','','CA','',''),(6277,234,'CAP-VERT','CV','1','','','','','','','','','','','','','','','','','CV','',''),(6278,234,'CENTRAFRICAINE, RÉPUBLIQUE','CF','1','','','','','','','','','','','','','','','','','CF','',''),(6279,234,'CHILI','CL','1','','','','','','','','','','','','','','','','','CL','',''),(6280,234,'CHINE','CN','1','','','','','','','','','','','','','','','','','CN','',''),(6281,234,'CHRISTMAS, ÎLE','CX','1','','','','','','','','','','','','','','','','','CX','',''),(6282,234,'CHYPRE','CY','1','','','','','','','','','','','','','','','','','CY','',''),(6283,234,'COCOS (KEELING), ÎLES','CC','1','','','','','','','','','','','','','','','','','CC','',''),(6284,234,'COLOMBIE','CO','1','','','','','','','','','','','','','','','','','CO','',''),(6285,234,'COMORES','KM','1','','','','','','','','','','','','','','','','','KM','',''),(6286,234,'CONGO','CG','1','','','','','','','','','','','','','','','','','CG','',''),(6287,234,'CONGO, LA RÉPUBLIQUE DÉMOCRATIQUE DU','CD','1','','','','','','','','','','','','','','','','','CD','',''),(6288,234,'COOK, ÎLES','CK','1','','','','','','','','','','','','','','','','','CK','',''),(6289,234,'CORÉE, RÉPUBLIQUE DE','KR','1','','','','','','','','','','','','','','','','','KR','',''),(6290,234,'CORÉE, RÉPUBLIQUE POPULAIRE DÉMOCRATIQUE DE','KP','1','','','','','','','','','','','','','','','','','KP','',''),(6291,234,'COSTA RICA','CR','1','','','','','','','','','','','','','','','','','CR','',''),(6292,234,'CÔTE D\'IVOIRE','CI','1','','','','','','','','','','','','','','','','','CI','',''),(6293,234,'CROATIE','HR','1','','','','','','','','','','','','','','','','','HR','',''),(6294,234,'CUBA','CU','1','','','','','','','','','','','','','','','','','CU','',''),(6295,234,'DANEMARK','DK','1','','','','','','','','','','','','','','','','','DK','',''),(6296,234,'DJIBOUTI','DJ','1','','','','','','','','','','','','','','','','','DJ','',''),(6297,234,'DOMINICAINE, RÉPUBLIQUE','DO','1','','','','','','','','','','','','','','','','','DO','',''),(6298,234,'DOMINIQUE','DM','1','','','','','','','','','','','','','','','','','DM','',''),(6299,234,'ÉGYPTE','EG','1','','','','','','','','','','','','','','','','','EG','',''),(6300,234,'EL SALVADOR','SV','1','','','','','','','','','','','','','','','','','SV','',''),(6301,234,'ÉMIRATS ARABES UNIS','AE','1','','','','','','','','','','','','','','','','','AE','',''),(6302,234,'ÉQUATEUR','EC','1','','','','','','','','','','','','','','','','','EC','',''),(6303,234,'ÉRYTHRÉE','ER','1','','','','','','','','','','','','','','','','','ER','',''),(6304,234,'ESPAGNE','ES','1','','','','','','','','','','','','','','','','','ES','',''),(6305,234,'ESTONIE','EE','1','','','','','','','','','','','','','','','','','EE','',''),(6306,234,'ÉTATS-UNIS','US','1','','','','','','','','','','','','','','','','','US','',''),(6307,234,'ÉTHIOPIE','ET','1','','','','','','','','','','','','','','','','','ET','',''),(6308,234,'FALKLAND, ÎLES (MALVINAS)','FK','1','','','','','','','','','','','','','','','','','FK','',''),(6309,234,'FÉROÉ, ÎLES','FO','1','','','','','','','','','','','','','','','','','FO','',''),(6310,234,'FIDJI','FJ','1','','','','','','','','','','','','','','','','','FJ','',''),(6311,234,'FINLANDE','FI','1','','','','','','','','','','','','','','','','','FI','',''),(6313,234,'GABON','GA','1','','','','','','','','','','','','','','','','','GA','',''),(6314,234,'GAMBIE','GM','1','','','','','','','','','','','','','','','','','GM','',''),(6315,234,'GÉORGIE','GE','1','','','','','','','','','','','','','','','','','GE','',''),(6316,234,'GÉORGIE DU SUD ET LES ÎLES SANDWICH DU SUD','GS','1','','','','','','','','','','','','','','','','','GS','',''),(6317,234,'GHANA','GH','1','','','','','','','','','','','','','','','','','GH','',''),(6318,234,'GIBRALTAR','GI','1','','','','','','','','','','','','','','','','','GI','',''),(6319,234,'GRÈCE','GR','1','','','','','','','','','','','','','','','','','GR','',''),(6320,234,'GRENADE','GD','1','','','','','','','','','','','','','','','','','GD','',''),(6321,234,'GROENLAND','GL','1','','','','','','','','','','','','','','','','','GL','',''),(6322,234,'GUAM','GU','1','','','','','','','','','','','','','','','','','GU','',''),(6323,234,'GUATEMALA','GT','1','','','','','','','','','','','','','','','','','GT','',''),(6324,234,'GUERNESEY','GG','1','','','','','','','','','','','','','','','','','GG','',''),(6325,234,'GUINÉE','GN','1','','','','','','','','','','','','','','','','','GN','',''),(6326,234,'GUINÉE-BISSAU','GW','1','','','','','','','','','','','','','','','','','GW','',''),(6327,234,'GUINÉE ÉQUATORIALE','GQ','1','','','','','','','','','','','','','','','','','GQ','',''),(6328,234,'GUYANA','GY','1','','','','','','','','','','','','','','','','','GY','',''),(6329,234,'HAÏTI','HT','1','','','','','','','','','','','','','','','','','HT','',''),(6330,234,'HEARD, ÎLE ET MCDONALD, ÎLES','HM','1','','','','','','','','','','','','','','','','','HM','',''),(6331,234,'HONDURAS','HN','1','','','','','','','','','','','','','','','','','HN','',''),(6332,234,'HONG-KONG','HK','1','','','','','','','','','','','','','','','','','HK','',''),(6333,234,'HONGRIE','HU','1','','','','','','','','','','','','','','','','','HU','',''),(6334,234,'ÎLE DE MAN','IM','1','','','','','','','','','','','','','','','','','IM','',''),(6335,234,'ÎLES MINEURES ÉLOIGNÉES DES ÉTATS-UNIS','UM','1','','','','','','','','','','','','','','','','','UM','',''),(6336,234,'ÎLES VIERGES BRITANNIQUES','VG','1','','','','','','','','','','','','','','','','','VG','',''),(6337,234,'ÎLES VIERGES DES ÉTATS-UNIS','VI','1','','','','','','','','','','','','','','','','','VI','',''),(6338,234,'INDE','IN','1','','','','','','','','','','','','','','','','','IN','',''),(6339,234,'INDONÉSIE','ID','1','','','','','','','','','','','','','','','','','ID','',''),(6340,234,'IRAN, RÉPUBLIQUE ISLAMIQUE D\'','IR','1','','','','','','','','','','','','','','','','','IR','',''),(6341,234,'IRAQ','IQ','1','','','','','','','','','','','','','','','','','IQ','',''),(6342,234,'IRLANDE','IE','1','','','','','','','','','','','','','','','','','IE','',''),(6343,234,'ISLANDE','IS','1','','','','','','','','','','','','','','','','','IS','',''),(6344,234,'ISRAËL','IL','1','','','','','','','','','','','','','','','','','IL','',''),(6345,234,'ITALIE','IT','1','','','','','','','','','','','','','','','','','IT','',''),(6346,234,'JAMAÏQUE','JM','1','','','','','','','','','','','','','','','','','JM','',''),(6347,234,'JAPON','JP','1','','','','','','','','','','','','','','','','','JP','',''),(6348,234,'JERSEY','JE','1','','','','','','','','','','','','','','','','','JE','',''),(6349,234,'JORDANIE','JO','1','','','','','','','','','','','','','','','','','JO','',''),(6350,234,'KAZAKHSTAN','KZ','1','','','','','','','','','','','','','','','','','KZ','',''),(6351,234,'KENYA','KE','1','','','','','','','','','','','','','','','','','KE','',''),(6352,234,'KIRGHIZISTAN','KG','1','','','','','','','','','','','','','','','','','KG','',''),(6353,234,'KIRIBATI','KI','1','','','','','','','','','','','','','','','','','KI','',''),(6354,234,'KOWEÏT','KW','1','','','','','','','','','','','','','','','','','KW','',''),(6355,234,'LAO, RÉPUBLIQUE DÉMOCRATIQUE POPULAIRE','LA','1','','','','','','','','','','','','','','','','','LA','',''),(6356,234,'LESOTHO','LS','1','','','','','','','','','','','','','','','','','LS','',''),(6357,234,'LETTONIE','LV','1','','','','','','','','','','','','','','','','','LV','',''),(6358,234,'LIBAN','LB','1','','','','','','','','','','','','','','','','','LB','',''),(6359,234,'LIBÉRIA','LR','1','','','','','','','','','','','','','','','','','LR','',''),(6360,234,'LIBYENNE, JAMAHIRIYA ARABE','LY','1','','','','','','','','','','','','','','','','','LY','',''),(6361,234,'LIECHTENSTEIN','LI','1','','','','','','','','','','','','','','','','','LI','',''),(6362,234,'LITUANIE','LT','1','','','','','','','','','','','','','','','','','LT','',''),(6363,234,'LUXEMBOURG','LU','1','','','','','','','','','','','','','','','','','LU','',''),(6364,234,'MACAO','MO','1','','','','','','','','','','','','','','','','','MO','',''),(6365,234,'MACÉDOINE, L\'EX-RÉPUBLIQUE YOUGOSLAVE DE','MK','1','','','','','','','','','','','','','','','','','MK','',''),(6366,234,'MADAGASCAR','MG','1','','','','','','','','','','','','','','','','','MG','',''),(6367,234,'MALAISIE','MY','1','','','','','','','','','','','','','','','','','MY','',''),(6368,234,'MALAWI','MW','1','','','','','','','','','','','','','','','','','MW','',''),(6369,234,'MALDIVES','MV','1','','','','','','','','','','','','','','','','','MV','',''),(6370,234,'MALI','ML','1','','','','','','','','','','','','','','','','','ML','',''),(6371,234,'MALTE','MT','1','','','','','','','','','','','','','','','','','MT','',''),(6372,234,'MARIANNES DU NORD, ÎLES','MP','1','','','','','','','','','','','','','','','','','MP','',''),(6373,234,'MAROC','MA','1','','','','','','','','','','','','','','','','','MA','',''),(6374,234,'MARSHALL, ÎLES','MH','1','','','','','','','','','','','','','','','','','MH','',''),(6375,234,'MAURICE','MU','1','','','','','','','','','','','','','','','','','MU','',''),(6376,234,'MAURITANIE','MR','1','','','','','','','','','','','','','','','','','MR','',''),(6377,234,'MEXIQUE','MX','1','','','','','','','','','','','','','','','','','MX','',''),(6378,234,'MICRONÉSIE, ÉTATS FÉDÉRÉS DE','FM','1','','','','','','','','','','','','','','','','','FM','',''),(6379,234,'MOLDOVA, RÉPUBLIQUE DE','MD','1','','','','','','','','','','','','','','','','','MD','',''),(6380,234,'MONACO','MC','1','','','','','','','','','','','','','','','','','MC','',''),(6381,234,'MONGOLIE','MN','1','','','','','','','','','','','','','','','','','MN','',''),(6382,234,'MONTÉNÉGRO','ME','1','','','','','','','','','','','','','','','','','ME','',''),(6383,234,'MONTSERRAT','MS','1','','','','','','','','','','','','','','','','','MS','',''),(6384,234,'MOZAMBIQUE','MZ','1','','','','','','','','','','','','','','','','','MZ','',''),(6385,234,'MYANMAR','MM','1','','','','','','','','','','','','','','','','','MM','',''),(6386,234,'NAMIBIE','NA','1','','','','','','','','','','','','','','','','','NA','',''),(6387,234,'NAURU','NR','1','','','','','','','','','','','','','','','','','NR','',''),(6388,234,'NÉPAL','NP','1','','','','','','','','','','','','','','','','','NP','',''),(6389,234,'NICARAGUA','NI','1','','','','','','','','','','','','','','','','','NI','',''),(6390,234,'NIGER','NE','1','','','','','','','','','','','','','','','','','NE','',''),(6391,234,'NIGÉRIA','NG','1','','','','','','','','','','','','','','','','','NG','',''),(6392,234,'NIUÉ','NU','1','','','','','','','','','','','','','','','','','NU','',''),(6393,234,'NORFOLK, ÎLE','NF','1','','','','','','','','','','','','','','','','','NF','',''),(6394,234,'NORVÈGE','NO','1','','','','','','','','','','','','','','','','','NO','',''),(6395,234,'NOUVELLE-ZÉLANDE','NZ','1','','','','','','','','','','','','','','','','','NZ','',''),(6396,234,'OCÉAN INDIEN, TERRITOIRE BRITANNIQUE DE L\'','IO','1','','','','','','','','','','','','','','','','','IO','',''),(6397,234,'OMAN','OM','1','','','','','','','','','','','','','','','','','OM','',''),(6398,234,'OUGANDA','UG','1','','','','','','','','','','','','','','','','','UG','',''),(6399,234,'OUZBÉKISTAN','UZ','1','','','','','','','','','','','','','','','','','UZ','',''),(6400,234,'PAKISTAN','PK','1','','','','','','','','','','','','','','','','','PK','',''),(6401,234,'PALAOS','PW','1','','','','','','','','','','','','','','','','','PW','',''),(6402,234,'PALESTINIEN OCCUPÉ, TERRITOIRE','PS','1','','','','','','','','','','','','','','','','','PS','',''),(6403,234,'PANAMA','PA','1','','','','','','','','','','','','','','','','','PA','',''),(6404,234,'PAPOUASIE-NOUVELLE-GUINÉE','PG','1','','','','','','','','','','','','','','','','','PG','',''),(6405,234,'PARAGUAY','PY','1','','','','','','','','','','','','','','','','','PY','',''),(6406,234,'PAYS-BAS','NL','1','','','','','','','','','','','','','','','','','NL','',''),(6407,234,'PÉROU','PE','1','','','','','','','','','','','','','','','','','PE','',''),(6408,234,'PHILIPPINES','PH','1','','','','','','','','','','','','','','','','','PH','',''),(6409,234,'PITCAIRN','PN','1','','','','','','','','','','','','','','','','','PN','',''),(6410,234,'POLOGNE','PL','1','','','','','','','','','','','','','','','','','PL','',''),(6411,234,'PORTO RICO','PR','1','','','','','','','','','','','','','','','','','PR','',''),(6412,234,'PORTUGAL','PT','1','','','','','','','','','','','','','','','','','PT','',''),(6413,234,'QATAR','QA','1','','','','','','','','','','','','','','','','','QA','',''),(6414,234,'ROUMANIE','RO','1','','','','','','','','','','','','','','','','','RO','',''),(6415,234,'ROYAUME-UNI','GB','1','','','','','','','','','','','','','','','','','GB','',''),(6416,234,'RUSSIE, FÉDÉRATION DE','RU','1','','','','','','','','','','','','','','','','','RU','',''),(6417,234,'RWANDA','RW','1','','','','','','','','','','','','','','','','','RW','',''),(6418,234,'SAHARA OCCIDENTAL','EH','1','','','','','','','','','','','','','','','','','EH','',''),(6419,234,'SAINTE-HÉLÈNE','SH','1','','','','','','','','','','','','','','','','','SH','',''),(6420,234,'SAINTE-LUCIE','LC','1','','','','','','','','','','','','','','','','','LC','',''),(6421,234,'SAINT-KITTS-ET-NEVIS','KN','1','','','','','','','','','','','','','','','','','KN','',''),(6422,234,'SAINT-MARIN','SM','1','','','','','','','','','','','','','','','','','SM','',''),(6423,234,'SAINT-SIÈGE (ÉTAT DE LA CITÉ DU VATICAN)','VA','1','','','','','','','','','','','','','','','','','VA','',''),(6424,234,'SAINT-VINCENT-ET-LES GRENADINES','VC','1','','','','','','','','','','','','','','','','','VC','',''),(6425,234,'SALOMON, ÎLES','SB','1','','','','','','','','','','','','','','','','','SB','',''),(6426,234,'SAMOA','WS','1','','','','','','','','','','','','','','','','','WS','',''),(6427,234,'SAMOA AMÉRICAINES','AS','1','','','','','','','','','','','','','','','','','AS','',''),(6428,234,'SAO TOMÉ-ET-PRINCIPE','ST','1','','','','','','','','','','','','','','','','','ST','',''),(6429,234,'SÉNÉGAL','SN','1','','','','','','','','','','','','','','','','','SN','',''),(6430,234,'SERBIE','RS','1','','','','','','','','','','','','','','','','','RS','',''),(6431,234,'SEYCHELLES','SC','1','','','','','','','','','','','','','','','','','SC','',''),(6432,234,'SIERRA LEONE','SL','1','','','','','','','','','','','','','','','','','SL','',''),(6433,234,'SINGAPOUR','SG','1','','','','','','','','','','','','','','','','','SG','',''),(6434,234,'SLOVAQUIE','SK','1','','','','','','','','','','','','','','','','','SK','',''),(6435,234,'SLOVÉNIE','SI','1','','','','','','','','','','','','','','','','','SI','',''),(6436,234,'SOMALIE','SO','1','','','','','','','','','','','','','','','','','SO','',''),(6437,234,'SOUDAN','SD','1','','','','','','','','','','','','','','','','','SD','',''),(6438,234,'SRI LANKA','LK','1','','','','','','','','','','','','','','','','','LK','',''),(6439,234,'SUÈDE','SE','1','','','','','','','','','','','','','','','','','SE','',''),(6440,234,'SUISSE','CH','1','','','','','','','','','','','','','','','','','CH','',''),(6441,234,'SURINAME','SR','1','','','','','','','','','','','','','','','','','SR','',''),(6442,234,'SVALBARD ET ÎLE JAN MAYEN','SJ','1','','','','','','','','','','','','','','','','','SJ','',''),(6443,234,'SWAZILAND','SZ','1','','','','','','','','','','','','','','','','','SZ','',''),(6444,234,'SYRIENNE, RÉPUBLIQUE ARABE','SY','1','','','','','','','','','','','','','','','','','SY','',''),(6445,234,'TADJIKISTAN','TJ','1','','','','','','','','','','','','','','','','','TJ','',''),(6446,234,'TAÏWAN, PROVINCE DE CHINE','TW','1','','','','','','','','','','','','','','','','','TW','',''),(6447,234,'TANZANIE, RÉPUBLIQUE-UNIE DE','TZ','1','','','','','','','','','','','','','','','','','TZ','',''),(6448,234,'TCHAD','TD','1','','','','','','','','','','','','','','','','','TD','',''),(6449,234,'TCHÈQUE, RÉPUBLIQUE','CZ','1','','','','','','','','','','','','','','','','','CZ','',''),(6450,234,'THAÏLANDE','TH','1','','','','','','','','','','','','','','','','','TH','',''),(6451,234,'TIMOR-LESTE','TL','1','','','','','','','','','','','','','','','','','TL','',''),(6452,234,'TOGO','TG','1','','','','','','','','','','','','','','','','','TG','',''),(6453,234,'TOKELAU','TK','1','','','','','','','','','','','','','','','','','TK','',''),(6454,234,'TONGA','TO','1','','','','','','','','','','','','','','','','','TO','',''),(6455,234,'TRINITÉ-ET-TOBAGO','TT','1','','','','','','','','','','','','','','','','','TT','',''),(6456,234,'TUNISIE','TN','1','','','','','','','','','','','','','','','','','TN','',''),(6457,234,'TURKMÉNISTAN','TM','1','','','','','','','','','','','','','','','','','TM','',''),(6458,234,'TURKS ET CAÏQUES, ÎLES','TC','1','','','','','','','','','','','','','','','','','TC','',''),(6459,234,'TURQUIE','TR','1','','','','','','','','','','','','','','','','','TR','',''),(6460,234,'TUVALU','TV','1','','','','','','','','','','','','','','','','','TV','',''),(6461,234,'UKRAINE','UA','1','','','','','','','','','','','','','','','','','UA','',''),(6462,234,'URUGUAY','UY','1','','','','','','','','','','','','','','','','','UY','',''),(6463,234,'VANUATU','VU','1','','','','','','','','','','','','','','','','','VU','',''),(6464,234,'VENEZUELA','VE','1','','','','','','','','','','','','','','','','','VE','',''),(6465,234,'VIET NAM','VN','1','','','','','','','','','','','','','','','','','VN','',''),(6466,234,'YÉMEN','YE','1','','','','','','','','','','','','','','','','','YE','',''),(6467,234,'ZAMBIE','ZM','1','','','','','','','','','','','','','','','','','ZM','',''),(6468,234,'ZIMBABWE','ZW','1','','','','','','','','','','','','','','','','','ZW','',''),(6469,234,'FRANCE','FR','1','','','','','','','','','','','','','','','','','FR','','');
/*!40000 ALTER TABLE `GeolocalisationN2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Groupe_Moniteur`
--

LOCK TABLES `Groupe_Moniteur` WRITE;
/*!40000 ALTER TABLE `Groupe_Moniteur` DISABLE KEYS */;
/*!40000 ALTER TABLE `Groupe_Moniteur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Guides`
--

LOCK TABLES `Guides` WRITE;
/*!40000 ALTER TABLE `Guides` DISABLE KEYS */;
/*!40000 ALTER TABLE `Guides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `HabilitationAgent`
--

LOCK TABLES `HabilitationAgent` WRITE;
/*!40000 ALTER TABLE `HabilitationAgent` DISABLE KEYS */;
INSERT INTO `HabilitationAgent` (`id_agent`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_anonymat_en_ligne`, `ouvrir_offre_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`, `statistiques_QoS`, `ouvrir_anonymat_a_distance`, `gestion_compte_jal`, `gestion_centrale_pub`, `Gestion_Compte_Groupe_Moniteur`, `ouvrir_offre_technique_en_ligne`, `ouvrir_offre_technique_a_distance`, `activation_compte_entreprise`, `importer_enveloppe`, `suivi_seul_registre_depots_papier`, `suivi_seul_registre_retraits_papier`, `suivi_seul_registre_questions_papier`, `suivi_seul_registre_depots_electronique`, `suivi_seul_registre_retraits_electronique`, `suivi_seul_registre_questions_electronique`, `modifier_consultation_mapa_inferieur_montant_apres_validation`, `modifier_consultation_mapa_superieur_montant_apres_validation`, `modifier_consultation_procedures_formalisees_apres_validation`, `gerer_les_entreprises`, `portee_societes_exclues`, `portee_societes_exclues_tous_organismes`, `modifier_societes_exclues`, `supprimer_societes_exclues`, `resultat_analyse`, `gerer_adresses_service`, `gerer_mon_service`, `download_archives`, `creer_annonce_extrait_pv`, `creer_annonce_rapport_achevement`, `gestion_certificats_agent`, `creer_avis_programme_previsionnel`, `annuler_consultation`, `envoyer_publicite`, `liste_marches_notifies`, `suivre_message`, `envoyer_message`, `suivi_flux_chorus_transversal`, `gestion_mandataire`, `gerer_newsletter`, `gestion_modeles_formulaire`, `gestion_adresses_facturation_jal`, `administrer_adresses_facturation_jal`, `redaction_documents_redac`, `validation_documents_redac`, `gestion_mise_disposition_pieces_marche`, `annuaire_acheteur`, `reprendre_integralement_article`, `administrer_clauses`, `valider_clauses`, `administrer_canevas`, `valider_canevas`, `administrer_clauses_entite_achats`, `generer_pieces_format_odt`, `publier_version_clausier_editeur`, `administrer_clauses_editeur`, `valider_clauses_editeur`, `administrer_canevas_editeur`, `valider_canevas_editeur`, `decision_suivi_seul`, `ouvrir_candidature_hors_ligne`, `ouvrir_offre_hors_ligne`, `ouvrir_offre_technique_hors_ligne`, `ouvrir_anonymat_hors_ligne`, `espace_collaboratif_gestionnaire`, `espace_collaboratif_contributeur`, `gerer_organismes`, `gerer_associations_agents`, `module_redaction_uniquement`, `historique_navigation_inscrits`, `telecharger_accords_cadres`, `creer_annonce_decision_resiliation`, `creer_annonce_synthese_rapport_audit`, `gerer_operations`, `telecharger_siret_acheteur`, `gerer_reouvertures_modification`, `acceder_tous_telechargements`, `creer_contrat`, `gerer_contrat`, `consulter_contrat`, `gerer_newsletter_redac`, `profil_rma`, `affectation_vision_rma`, `gerer_gabarit_editeur`, `gerer_gabarit`, `gerer_gabarit_entite_achats`, `gerer_gabarit_agent`, `gerer_messages_accueil`, `gerer_OA_GA`, `deplacer_service`, `activer_version_clausier`) VALUES (1,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(2,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(3,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(4,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(5,'1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(6,'0','0','0','1','1','0','1','0','0','0','0','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','1','0','0','0','0','0','0','0','1','0','0','1','1','1','1','1','1','1','0','1','1','1','1','0','1','1','1','1','1','1','1','1','1','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0');
/*!40000 ALTER TABLE `HabilitationAgent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `HabilitationProfil`
--

LOCK TABLES `HabilitationProfil` WRITE;
/*!40000 ALTER TABLE `HabilitationProfil` DISABLE KEYS */;
INSERT INTO `HabilitationProfil` (`id`, `libelle`, `gestion_agent_pole`, `gestion_fournisseurs_envois_postaux`, `gestion_bi_cles`, `creer_consultation`, `modifier_consultation`, `valider_consultation`, `publier_consultation`, `suivre_consultation`, `suivre_consultation_pole`, `supprimer_enveloppe`, `supprimer_consultation`, `depouiller_candidature`, `depouiller_offre`, `messagerie_securisee`, `acces_registre_depots_papier`, `acces_registre_retraits_papier`, `acces_registre_questions_papier`, `gerer_encheres`, `suivre_encheres`, `suivi_entreprise`, `envoi_boamp`, `acces_classement_lot`, `connecteur_sis`, `connecteur_marco`, `repondre_aux_questions`, `appel_projet_formation`, `utiliser_client_CAO`, `notification_boamp`, `administrer_compte`, `gestion_mapa`, `gestion_type_validation`, `approuver_consultation`, `administrer_procedure`, `restreindre_creation`, `creer_liste_marches`, `gestion_commissions`, `suivi_seul_consultation`, `attribution_marche`, `fiche_recensement`, `declarer_infructueux`, `declarer_sans_suite`, `creer_consultation_transverse`, `ouvrir_candidature_en_ligne`, `ouvrir_candidature_a_distance`, `refuser_enveloppe`, `gerer_admissibilite`, `restaurer_enveloppe`, `ouvrir_offre_en_ligne`, `ouvrir_anonymat_en_ligne`, `gestion_compte_boamp`, `gestion_agents`, `gestion_habilitations`, `gerer_mapa_inferieur_montant`, `gerer_mapa_superieur_montant`, `modifier_consultation_avant_validation`, `modifier_consultation_apres_validation`, `acces_reponses`, `telechargement_groupe_anticipe_plis_chiffres`, `telechargement_unitaire_plis_chiffres`, `ouvrir_offre_a_distance`, `creer_annonce_information`, `saisie_marches`, `validation_marches`, `publication_marches`, `gerer_statistiques_metier`, `gerer_archives`, `administrer_procedures_formalisees`, `creer_annonce_attribution`, `acces_registre_retraits_electronique`, `acces_registre_questions_electronique`, `acces_registre_depots_electronique`, `validation_simple`, `validation_intermediaire`, `validation_finale`, `creer_suite_consultation`, `hyper_admin`, `droit_gestion_services`, `suivi_acces`, `statistiques_site`, `statistiques_QoS`, `ouvrir_anonymat_a_distance`, `gestion_compte_jal`, `gestion_centrale_pub`, `Gestion_Compte_Groupe_Moniteur`, `ouvrir_offre_technique_en_ligne`, `ouvrir_offre_technique_a_distance`, `activation_compte_entreprise`, `importer_enveloppe`, `suivi_seul_registre_depots_papier`, `suivi_seul_registre_retraits_papier`, `suivi_seul_registre_questions_papier`, `suivi_seul_registre_depots_electronique`, `suivi_seul_registre_retraits_electronique`, `suivi_seul_registre_questions_electronique`, `modifier_consultation_mapa_inferieur_montant_apres_validation`, `modifier_consultation_mapa_superieur_montant_apres_validation`, `modifier_consultation_procedures_formalisees_apres_validation`, `gerer_les_entreprises`, `portee_societes_exclues`, `portee_societes_exclues_tous_organismes`, `modifier_societes_exclues`, `supprimer_societes_exclues`, `resultat_analyse`, `gerer_adresses_service`, `gerer_mon_service`, `download_archives`, `creer_annonce_extrait_pv`, `creer_annonce_rapport_achevement`, `gestion_certificats_agent`, `creer_avis_programme_previsionnel`, `annuler_consultation`, `envoyer_publicite`, `liste_marches_notifies`, `suivre_message`, `envoyer_message`, `suivi_flux_chorus_transversal`, `gestion_mandataire`, `gerer_newsletter`, `gestion_modeles_formulaire`, `gestion_adresses_facturation_jal`, `administrer_adresses_facturation_jal`, `redaction_documents_redac`, `validation_documents_redac`, `gestion_mise_disposition_pieces_marche`, `annuaire_acheteur`, `reprendre_integralement_article`, `administrer_clauses`, `valider_clauses`, `administrer_canevas`, `valider_canevas`, `administrer_clauses_entite_achats`, `generer_pieces_format_odt`, `publier_version_clausier_editeur`, `administrer_clauses_editeur`, `valider_clauses_editeur`, `administrer_canevas_editeur`, `valider_canevas_editeur`, `decision_suivi_seul`, `ouvrir_candidature_hors_ligne`, `ouvrir_offre_hors_ligne`, `ouvrir_offre_technique_hors_ligne`, `ouvrir_anonymat_hors_ligne`, `espace_collaboratif_gestionnaire`, `espace_collaboratif_contributeur`, `gerer_organismes`, `gerer_associations_agents`, `module_redaction_uniquement`, `historique_navigation_inscrits`, `telecharger_accords_cadres`, `creer_annonce_decision_resiliation`, `creer_annonce_synthese_rapport_audit`, `gerer_operations`, `telecharger_siret_acheteur`, `gerer_reouvertures_modification`, `acceder_tous_telechargements`, `creer_contrat`, `gerer_contrat`, `consulter_contrat`, `gerer_newsletter_redac`, `profil_rma`, `affectation_vision_rma`, `gerer_gabarit_editeur`, `gerer_gabarit`, `gerer_gabarit_entite_achats`, `gerer_gabarit_agent`, `gerer_messages_accueil`, `gerer_OA_GA`, `deplacer_service`, `activer_version_clausier`) VALUES (1,'Administrateur/Acheteur','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0'),(4,'Acheteur','0','0','0','1','1','0','1','0','0','0','0','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','1','0','0','0','0','0','0','0','1','0','0','1','1','1','1','1','1','1','0','1','1','1','1','0','1','1','1','1','1','1','1','1','1','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0'),(6,'Acheteur < 90 kEUR','0','0','0','1','1','0','1','0','0','0','0','0','0','0','1','1','1','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','1','0','0','1','0','1','1','1','0','0','0','1','1','1','1','0','1','0','1','1','1','1','1','1','1','1','0','0','0','0','0','0','1','0','1','0','0','0','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0'),(12,'Administrateur/Acheteur','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','1','0','1','0','1','0','1','1','1','1','0','0','0','1','0','0','0','1','1','1','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0'),(13,'Administrateur Service','0','0','0','1','1','0','1','0','0','0','0','0','0','0','1','1','1','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','1','1','0','1','0','1','1','1','0','0','0','1','1','1','1','0','1','0','1','1','1','1','1','1','1','1','0','0','0','0','0','0','1','0','1','0','0','0','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0');
/*!40000 ALTER TABLE `HabilitationProfil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_cosignature`
--

LOCK TABLES `Helios_cosignature` WRITE;
/*!40000 ALTER TABLE `Helios_cosignature` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_cosignature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_piece_publicite`
--

LOCK TABLES `Helios_piece_publicite` WRITE;
/*!40000 ALTER TABLE `Helios_piece_publicite` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_piece_publicite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_pv_consultation`
--

LOCK TABLES `Helios_pv_consultation` WRITE;
/*!40000 ALTER TABLE `Helios_pv_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_pv_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_rapport_prefet`
--

LOCK TABLES `Helios_rapport_prefet` WRITE;
/*!40000 ALTER TABLE `Helios_rapport_prefet` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_rapport_prefet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_tableau_ar`
--

LOCK TABLES `Helios_tableau_ar` WRITE;
/*!40000 ALTER TABLE `Helios_tableau_ar` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_tableau_ar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_teletransmission`
--

LOCK TABLES `Helios_teletransmission` WRITE;
/*!40000 ALTER TABLE `Helios_teletransmission` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_teletransmission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Helios_teletransmission_lot`
--

LOCK TABLES `Helios_teletransmission_lot` WRITE;
/*!40000 ALTER TABLE `Helios_teletransmission_lot` DISABLE KEYS */;
/*!40000 ALTER TABLE `Helios_teletransmission_lot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `HistoriqueNbrConsultationsPubliees`
--

LOCK TABLES `HistoriqueNbrConsultationsPubliees` WRITE;
/*!40000 ALTER TABLE `HistoriqueNbrConsultationsPubliees` DISABLE KEYS */;
/*!40000 ALTER TABLE `HistoriqueNbrConsultationsPubliees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Historique_Avis_Pub`
--

LOCK TABLES `Historique_Avis_Pub` WRITE;
/*!40000 ALTER TABLE `Historique_Avis_Pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `Historique_Avis_Pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Historique_suppression_agent`
--

LOCK TABLES `Historique_suppression_agent` WRITE;
/*!40000 ALTER TABLE `Historique_suppression_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Historique_suppression_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Inscrit`
--

LOCK TABLES `Inscrit` WRITE;
/*!40000 ALTER TABLE `Inscrit` DISABLE KEYS */;
INSERT INTO `Inscrit` (`id`, `entreprise_id`, `id_etablissement`, `login`, `mdp`, `num_cert`, `cert`, `civilite`, `nom`, `prenom`, `adresse`, `codepostal`, `ville`, `pays`, `email`, `telephone`, `categorie`, `motstitreresume`, `periode`, `siret`, `fax`, `code_cpv`, `id_langue`, `profil`, `adresse2`, `bloque`, `id_initial`, `inscrit_annuaire_defense`, `date_creation`, `date_modification`, `tentatives_mdp`, `uid`, `type_hash`) VALUES (1,1,0,'mp','91f6e575920d2d503d8597338fd1637d580f4243',NULL,NULL,0,'PASSET','Marc','231, rue Saint-Honoré','75001','Paris','France','mp@atexo.com','0153430500',NULL,NULL,0,'00019','',NULL,NULL,2,'','0',0,'0','','',0,NULL,'SHA1'),(2,1,0,'cc','9f107db7187c84fd1836accd91a272f496866a30',NULL,NULL,0,'CIVETE','Charles','231, rue Saint-Honoré','75001','Paris','France','cc@atexo.com','0153430500',NULL,NULL,0,'00019','',NULL,NULL,1,'','0',0,'0','','2011-01-04 12:47:10',3,NULL,'SHA1'),(4,2,0,'mp2','44609257f13936e49292cbd87c6d7fd6079ff095',NULL,NULL,0,'POLLET','Michel','6 Pl d\'Alleray','75015','PARIS 15','France','mp2@atexo.com','0153430544',NULL,NULL,0,'00014','',NULL,NULL,2,'','0',0,'0','','',0,NULL,'SHA1'),(5,2,0,'cc2','1e090fd026721c03e6b552e951234fa45f17c2c8',NULL,NULL,0,'CHARON','Clément','6 Pl d\'Alleray','75015','PARIS 15','France','cc2@atexo.com','0153430550',NULL,NULL,0,'00014','',NULL,NULL,1,'','0',0,'0','','',0,NULL,'SHA1'),(6,3,0,'mp3','1bc0264d5bf8869899640171b693092d8c4899f8',NULL,NULL,0,'POSTIER','Maxime','1 Place Carpeaux','92800','PUTEAUX','France','mp3@atexo.com','0153430540',NULL,NULL,0,'00017','',NULL,NULL,2,'','0',0,'0','','',0,NULL,'SHA1'),(7,3,0,'cc3','26aca93edcc6da566cbdcc5b9de15a277289618e',NULL,NULL,0,'CARLET','Christian','1 Place Carpeaux','92800','PUTEAUX','France','cc3@atexo.com','014354069',NULL,NULL,0,'00017','',NULL,NULL,1,'','0',0,'0','','',0,NULL,'SHA1'),(8,1,0,'alerte.v3','b7124ab3f60eec664940f58cd11d876ae911c9e0',NULL,NULL,0,'ALERTE V3','alertev3','231, rue Saint-Honoré','58768','Paris','France','alerte.v3@atexo.com','0153430540',NULL,NULL,0,'00019','',NULL,NULL,1,'Escalier A - 3éme étage','0',0,'0','','',0,NULL,'SHA1');
/*!40000 ALTER TABLE `Inscrit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InscritHistorique`
--

LOCK TABLES `InscritHistorique` WRITE;
/*!40000 ALTER TABLE `InscritHistorique` DISABLE KEYS */;
/*!40000 ALTER TABLE `InscritHistorique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InterfaceTypeProcedure`
--

LOCK TABLES `InterfaceTypeProcedure` WRITE;
/*!40000 ALTER TABLE `InterfaceTypeProcedure` DISABLE KEYS */;
INSERT INTO `InterfaceTypeProcedure` (`id`, `type_procedure_sis`, `type_procedure_marco`, `type_procedure`) VALUES (1,'ACI',NULL,'ATR'),(2,'ACR',NULL,'ATR'),(3,'AOCR',NULL,'ATR'),(4,'AOO',NULL,'ATR'),(5,'AOO2',NULL,'AOO'),(6,'AOR',NULL,'ARC'),(7,'COO',NULL,'COO'),(8,'COR',NULL,'CRC'),(9,'MNAC',NULL,'MNE'),(10,'MNAP',NULL,'MNE'),(11,'MNSC',NULL,'MNE'),(12,'MOCN',NULL,'CRC'),(13,'MOSC',NULL,'MNE'),(14,'MSF',NULL,'ATR'),(15,'MSFA',NULL,'ACA'),(16,'MSFB',NULL,'ACS'),(17,'MSFC',NULL,'MAI'),(18,'MSFR',NULL,'ATR'),(19,'PDC',NULL,'DCC'),(20,NULL,'3','AOO'),(21,NULL,'4','ARC'),(22,NULL,'5','CRC'),(23,NULL,'6','DCC'),(24,NULL,'7','MNE'),(25,NULL,'8','MNE'),(26,NULL,'10','ATR'),(27,NULL,'12','COO'),(28,NULL,'13','MNE'),(29,NULL,'14','MNE'),(30,NULL,'16','ATR'),(31,NULL,'17','COO'),(32,NULL,'18','AOO'),(33,NULL,'19','ARC'),(34,NULL,'20','MNE'),(35,NULL,'21','MAI'),(36,NULL,'22','MAI');
/*!40000 ALTER TABLE `InterfaceTypeProcedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InterneConsultation`
--

LOCK TABLES `InterneConsultation` WRITE;
/*!40000 ALTER TABLE `InterneConsultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `InterneConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InterneConsultationSuiviSeul`
--

LOCK TABLES `InterneConsultationSuiviSeul` WRITE;
/*!40000 ALTER TABLE `InterneConsultationSuiviSeul` DISABLE KEYS */;
/*!40000 ALTER TABLE `InterneConsultationSuiviSeul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InvitationConsultationTransverse`
--

LOCK TABLES `InvitationConsultationTransverse` WRITE;
/*!40000 ALTER TABLE `InvitationConsultationTransverse` DISABLE KEYS */;
/*!40000 ALTER TABLE `InvitationConsultationTransverse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ItemCritereEvaluation`
--

LOCK TABLES `ItemCritereEvaluation` WRITE;
/*!40000 ALTER TABLE `ItemCritereEvaluation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ItemCritereEvaluation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ItemFormulaire`
--

LOCK TABLES `ItemFormulaire` WRITE;
/*!40000 ALTER TABLE `ItemFormulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `ItemFormulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ItemFormulaireConsultation`
--

LOCK TABLES `ItemFormulaireConsultation` WRITE;
/*!40000 ALTER TABLE `ItemFormulaireConsultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ItemFormulaireConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `JAL`
--

LOCK TABLES `JAL` WRITE;
/*!40000 ALTER TABLE `JAL` DISABLE KEYS */;
/*!40000 ALTER TABLE `JAL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Journaux`
--

LOCK TABLES `Journaux` WRITE;
/*!40000 ALTER TABLE `Journaux` DISABLE KEYS */;
INSERT INTO `Journaux` (`ID_JOURNAL`, `ORGANISME`, `ID_CENTRALE`, `NOM_JOURNAL`) VALUES (1,'a1a',1,'Ouest France'),(1,'a1t',1,'Ouest France'),(1,'a2z',1,'Ouest France'),(1,'e3r',1,'Ouest France'),(2,'a1a',1,'Le maine'),(2,'a1t',1,'Le maine'),(2,'a2z',1,'Le maine'),(2,'e3r',1,'Le maine'),(3,'a1a',1,'Presse Océan'),(3,'a1t',1,'Presse Océan'),(3,'a2z',1,'Presse Océan'),(3,'e3r',1,'Presse Océan'),(4,'a1a',1,'La Presse de la Manche'),(4,'a1t',1,'La Presse de la Manche'),(4,'a2z',1,'La Presse de la Manche'),(4,'e3r',1,'La Presse de la Manche');
/*!40000 ALTER TABLE `Journaux` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Justificatifs`
--

LOCK TABLES `Justificatifs` WRITE;
/*!40000 ALTER TABLE `Justificatifs` DISABLE KEYS */;
/*!40000 ALTER TABLE `Justificatifs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Langue`
--

LOCK TABLES `Langue` WRITE;
/*!40000 ALTER TABLE `Langue` DISABLE KEYS */;
INSERT INTO `Langue` (`id_langue`, `langue`, `active`, `defaut`, `theme_specifique`, `obligatoire_pour_publication_consultation`) VALUES (1,'fr','1','1','0','0'),(2,'en','1','0','0','0'),(3,'es','1','0','0','0'),(4,'cz','0','0','0','0'),(5,'du','0','0','0','0'),(6,'su','0','0','0','0'),(7,'it','0','0','0','0');
/*!40000 ALTER TABLE `Langue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `LieuExecution`
--

LOCK TABLES `LieuExecution` WRITE;
/*!40000 ALTER TABLE `LieuExecution` DISABLE KEYS */;
/*!40000 ALTER TABLE `LieuExecution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Lt_Referentiel`
--

LOCK TABLES `Lt_Referentiel` WRITE;
/*!40000 ALTER TABLE `Lt_Referentiel` DISABLE KEYS */;
/*!40000 ALTER TABLE `Lt_Referentiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Mandataire_service`
--

LOCK TABLES `Mandataire_service` WRITE;
/*!40000 ALTER TABLE `Mandataire_service` DISABLE KEYS */;
/*!40000 ALTER TABLE `Mandataire_service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Marche`
--

LOCK TABLES `Marche` WRITE;
/*!40000 ALTER TABLE `Marche` DISABLE KEYS */;
/*!40000 ALTER TABLE `Marche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `MarchePublie`
--

LOCK TABLES `MarchePublie` WRITE;
/*!40000 ALTER TABLE `MarchePublie` DISABLE KEYS */;
INSERT INTO `MarchePublie` (`id`, `organisme`, `numeroMarcheAnnee`, `idService`, `isPubliee`, `isImportee`, `newVersion`) VALUES (1,'a1a',2019,0,'1','0','0'),(2,'a2z',2019,0,'1','0','0'),(3,'e3r',2019,0,'1','0','0'),(4,'a1t',2019,0,'1','0','0'),(5,'a1a',2019,1,'1','0','0');
/*!40000 ALTER TABLE `MarchePublie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Mesure_Type_Procedure`
--

LOCK TABLES `Mesure_Type_Procedure` WRITE;
/*!40000 ALTER TABLE `Mesure_Type_Procedure` DISABLE KEYS */;
INSERT INTO `Mesure_Type_Procedure` (`id_tag_name`, `organisme`, `libelle_tag_name`) VALUES (1,'a1a','Appel d\'offres ouvert'),(2,'a1a','Appel d\'offres restreint'),(3,'a1a','Dialogue compétitif'),(4,'a1a','Marché à  procédure adaptée'),(5,'a1a','Marché négocié avec publicité préalable et mise en concurrence'),(6,'a1a','Concours'),(7,'a1a','Procédure spécifique à certains marchés de défense nationale'),(8,'a1a','Accord cadre'),(9,'a1a','Système d\'acquisition dynamique'),(10,'a1a','Marché subséquent à accord cadre'),(11,'a1a','Marché spécifique sur système acquisition dynamique'),(12,'a2z','Appel d\'offres ouvert'),(13,'a2z','Appel d\'offres restreint'),(14,'a2z','Dialogue compétitif'),(15,'a2z','Marché à  procédure adaptée'),(16,'a2z','Marché négocié avec publicité préalable et mise en concurrence'),(17,'a2z','Concours'),(18,'a2z','Procédure spécifique à certains marchés de défense nationale'),(19,'a2z','Accord cadre'),(20,'a2z','Système d\'acquisition dynamique'),(21,'a2z','Marché subséquent à accord cadre'),(22,'a2z','Marché spécifique sur système acquisition dynamique'),(23,'e3r','Appel d\'offres ouvert'),(24,'e3r','Appel d\'offres restreint'),(25,'e3r','Dialogue compétitif'),(26,'e3r','Marché à  procédure adaptée'),(27,'e3r','Marché négocié avec publicité préalable et mise en concurrence'),(28,'e3r','Concours'),(29,'e3r','Procédure spécifique à certains marchés de défense nationale'),(30,'e3r','Accord cadre'),(31,'e3r','Système d\'acquisition dynamique'),(32,'e3r','Marché subséquent à accord cadre'),(33,'e3r','Marché spécifique sur système acquisition dynamique'),(34,'a1t','Appel d\'offres ouvert'),(35,'a1t','Appel d\'offres restreint'),(36,'a1t','Dialogue compétitif'),(37,'a1t','Marché à  procédure adaptée'),(38,'a1t','Marché négocié avec publicité préalable et mise en concurrence'),(39,'a1t','Concours'),(40,'a1t','Procédure spécifique à certains marchés de défense nationale'),(41,'a1t','Accord cadre'),(42,'a1t','Système d\'acquisition dynamique'),(43,'a1t','Marché subséquent à accord cadre'),(44,'a1t','Marché spécifique sur système acquisition dynamique');
/*!40000 ALTER TABLE `Mesure_Type_Procedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Mesure_avancement`
--

LOCK TABLES `Mesure_avancement` WRITE;
/*!40000 ALTER TABLE `Mesure_avancement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Mesure_avancement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ModeleFormulaire`
--

LOCK TABLES `ModeleFormulaire` WRITE;
/*!40000 ALTER TABLE `ModeleFormulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `ModeleFormulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Newsletter`
--

LOCK TABLES `Newsletter` WRITE;
/*!40000 ALTER TABLE `Newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `Newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `NewsletterPieceJointe`
--

LOCK TABLES `NewsletterPieceJointe` WRITE;
/*!40000 ALTER TABLE `NewsletterPieceJointe` DISABLE KEYS */;
/*!40000 ALTER TABLE `NewsletterPieceJointe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Numerotation_ref_cons_auto`
--

LOCK TABLES `Numerotation_ref_cons_auto` WRITE;
/*!40000 ALTER TABLE `Numerotation_ref_cons_auto` DISABLE KEYS */;
/*!40000 ALTER TABLE `Numerotation_ref_cons_auto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Offre_papier`
--

LOCK TABLES `Offre_papier` WRITE;
/*!40000 ALTER TABLE `Offre_papier` DISABLE KEYS */;
/*!40000 ALTER TABLE `Offre_papier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Offres`
--

LOCK TABLES `Offres` WRITE;
/*!40000 ALTER TABLE `Offres` DISABLE KEYS */;
/*!40000 ALTER TABLE `Offres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Operations`
--

LOCK TABLES `Operations` WRITE;
/*!40000 ALTER TABLE `Operations` DISABLE KEYS */;
/*!40000 ALTER TABLE `Operations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Ordre_Du_Jour`
--

LOCK TABLES `Ordre_Du_Jour` WRITE;
/*!40000 ALTER TABLE `Ordre_Du_Jour` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ordre_Du_Jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Organisme`
--

LOCK TABLES `Organisme` WRITE;
/*!40000 ALTER TABLE `Organisme` DISABLE KEYS */;
INSERT INTO `Organisme` (`id`, `acronyme`, `type_article_org`, `denomination_org`, `categorie_insee`, `description_org`, `adresse`, `cp`, `ville`, `email`, `url`, `id_attrib_file`, `attrib_file`, `date_creation`, `active`, `id_client_ANM`, `status`, `signataire_cao`, `offset`, `sigle`, `adresse2`, `tel`, `telecopie`, `pays`, `affichage_entite`, `id_initial`, `denomination_org_ar`, `description_org_ar`, `adresse_ar`, `ville_ar`, `adresse2_ar`, `pays_ar`, `denomination_org_fr`, `description_org_fr`, `adresse_fr`, `ville_fr`, `adresse2_fr`, `pays_fr`, `denomination_org_es`, `description_org_es`, `adresse_es`, `ville_es`, `adresse2_es`, `pays_es`, `denomination_org_en`, `description_org_en`, `adresse_en`, `ville_en`, `adresse2_en`, `pays_en`, `denomination_org_su`, `description_org_su`, `adresse_su`, `ville_su`, `adresse2_su`, `pays_su`, `denomination_org_du`, `description_org_du`, `adresse_du`, `ville_du`, `adresse2_du`, `pays_du`, `denomination_org_cz`, `description_org_cz`, `adresse_cz`, `ville_cz`, `adresse2_cz`, `pays_cz`, `denomination_org_it`, `description_org_it`, `adresse_it`, `ville_it`, `adresse2_it`, `pays_it`, `siren`, `complement`, `moniteur_provenance`, `code_acces_logiciel`, `decalage_horaire`, `lieu_residence`, `activation_fuseau_horaire`, `alerte`, `ordre`, `URL_INTERFACE_ANM`, `sous_type_organisme`) VALUES (1,'a1a',3,'Organisme de paramétrage de référence','7',NULL,'','','','','',NULL,'','2019-03-14 17:11:49','0','0','0',NULL,'0','OPR','','','',NULL,'',0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'0','0',0,NULL,2),(2,'a2z',3,'Organisme de test ATEXO','7',NULL,'','','','','',NULL,'','2019-03-14 17:11:49','0','0','0',NULL,'0','ATEXO','','','',NULL,'',0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'0','0',0,NULL,2),(3,'e3r',3,'Organisme de Formation','7',NULL,'','','','','',NULL,'','2019-03-14 17:11:51','0','0','0',NULL,'0','FORM','','','',NULL,'',0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'0','0',0,NULL,2),(4,'a1t',3,'Organisme de test pour les entreprises','7',NULL,'','','','','',NULL,'','2019-03-14 17:11:52','0','0','0',NULL,'0','TEST','','','',NULL,'',0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',NULL,NULL,NULL,NULL,'0','0',0,NULL,2);
/*!40000 ALTER TABLE `Organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Organisme_Service_Metier`
--

LOCK TABLES `Organisme_Service_Metier` WRITE;
/*!40000 ALTER TABLE `Organisme_Service_Metier` DISABLE KEYS */;
INSERT INTO `Organisme_Service_Metier` (`id_auto`, `organisme`, `id_service_metier`) VALUES (1,'a1a',1),(2,'a2z',1),(3,'e3r',1),(4,'a1t',1);
/*!40000 ALTER TABLE `Organisme_Service_Metier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Panier_Entreprise`
--

LOCK TABLES `Panier_Entreprise` WRITE;
/*!40000 ALTER TABLE `Panier_Entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `Panier_Entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Parametrage_Enchere`
--

LOCK TABLES `Parametrage_Enchere` WRITE;
/*!40000 ALTER TABLE `Parametrage_Enchere` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parametrage_Enchere` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Parametrage_Enchere_Reference`
--

LOCK TABLES `Parametrage_Enchere_Reference` WRITE;
/*!40000 ALTER TABLE `Parametrage_Enchere_Reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parametrage_Enchere_Reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Parametrage_Enchere_Tranche_Bareme_Reference`
--

LOCK TABLES `Parametrage_Enchere_Tranche_Bareme_Reference` WRITE;
/*!40000 ALTER TABLE `Parametrage_Enchere_Tranche_Bareme_Reference` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parametrage_Enchere_Tranche_Bareme_Reference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Parametrage_Enchere_Tranches_Bareme_NETC`
--

LOCK TABLES `Parametrage_Enchere_Tranches_Bareme_NETC` WRITE;
/*!40000 ALTER TABLE `Parametrage_Enchere_Tranches_Bareme_NETC` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parametrage_Enchere_Tranches_Bareme_NETC` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Parametrage_Fiche_Weka`
--

LOCK TABLES `Parametrage_Fiche_Weka` WRITE;
/*!40000 ALTER TABLE `Parametrage_Fiche_Weka` DISABLE KEYS */;
/*!40000 ALTER TABLE `Parametrage_Fiche_Weka` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Partenaire`
--

LOCK TABLES `Partenaire` WRITE;
/*!40000 ALTER TABLE `Partenaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `Partenaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Passation_consultation`
--

LOCK TABLES `Passation_consultation` WRITE;
/*!40000 ALTER TABLE `Passation_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Passation_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Pieces_DCE`
--

LOCK TABLES `Pieces_DCE` WRITE;
/*!40000 ALTER TABLE `Pieces_DCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `Pieces_DCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Pieces_Mise_Disposition`
--

LOCK TABLES `Pieces_Mise_Disposition` WRITE;
/*!40000 ALTER TABLE `Pieces_Mise_Disposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `Pieces_Mise_Disposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Prestation`
--

LOCK TABLES `Prestation` WRITE;
/*!40000 ALTER TABLE `Prestation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Prestation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ProcedureEquivalence`
--

LOCK TABLES `ProcedureEquivalence` WRITE;
/*!40000 ALTER TABLE `ProcedureEquivalence` DISABLE KEYS */;
INSERT INTO `ProcedureEquivalence` (`id_type_procedure`, `organisme`, `elec_resp`, `no_elec_resp`, `cipher_enabled`, `cipher_disabled`, `signature_enabled`, `signature_disabled`, `env_candidature`, `env_offre`, `env_anonymat`, `envoi_complet`, `envoi_differe`, `procedure_publicite`, `procedure_restreinte_candidature`, `procedure_restreinte_offre`, `envoi_mail_par_mpe`, `no_envoi_mail_par_mpe`, `mise_en_ligne1`, `mise_en_ligne2`, `mise_en_ligne3`, `mise_en_ligne4`, `env_offre_type_unique`, `env_offre_type_multiple`, `no_fichier_annonce`, `fichier_importe`, `fichier_boamp`, `reglement_cons`, `dossier_dce`, `partial_dce_download`, `service`, `constitution_dossier_reponse`, `env_offre_type_unique2`, `env_offre_type_multiple2`, `gestion_envois_postaux`, `tireur_plan_non`, `tireur_plan_oui`, `tireur_plan_papier`, `tireur_plan_cdrom`, `tireur_plan_nom`, `tirage_descriptif`, `delai_date_limite_remise_pli`, `signature_propre`, `procedure_restreinte`, `ouverture_simultanee`, `type_decision_a_renseigner`, `type_decision_attribution_marche`, `type_decision_declaration_sans_suite`, `type_decision_declaration_infructueux`, `type_decision_selection_entreprise`, `type_decision_attribution_accord_cadre`, `type_decision_admission_sad`, `type_decision_autre`, `env_offre_technique`, `env_offre_technique_type_unique`, `env_offre_technique_type_multiple`, `rep_obligatoire`, `no_rep_obligatoire`, `autre_piece_cons`, `resp_elec_autre_plateforme`, `mise_en_ligne_entite_coordinatrice`, `autoriser_publicite`, `poursuite_date_limite_remise_pli`, `delai_poursuite_affichage`, `delai_poursuivre_affichage_unite`, `mode_ouverture_dossier`, `mode_ouverture_reponse`, `marche_public_simplifie`, `dume_demande`, `type_procedure_dume`, `type_formulaire_dume_standard`, `type_formulaire_dume_simplifie`, `afficher_code_cpv`, `code_cpv_obligatoire`) VALUES (1,'a1a','+1','+0','1','0','1','0','+1','+1','+0','-1','-1','1','0','0','0','0','+0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+0','','','+0','+0','+0','+0','+1','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1,'a1t','+1','+0','1','0','1','0','+1','+1','+0','-1','-1','1','0','0','0','0','+0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+0','','','+0','+0','+0','+0','+1','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1,'a2z','+1','+0','1','0','1','0','+1','+1','+0','-1','-1','1','0','0','0','0','+0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+0','','','+0','+0','+0','+0','+1','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1,'e3r','+1','+0','1','0','1','0','+1','+1','+0','-1','-1','1','0','0','0','0','+0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+0','','','+0','+0','+0','+0','+1','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(2,'a1a','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(2,'a1t','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(2,'a2z','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(2,'e3r','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(3,'a1a','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(3,'a1t','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(3,'a2z','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(3,'e3r','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(4,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(4,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(4,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(4,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(5,'a1a','+1','+0','+1','+0','+1','+0','+1','+1','+1','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(5,'a1t','+1','+0','+1','+0','+1','+0','+1','+1','+1','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(5,'a2z','+1','+0','+1','+0','+1','+0','+1','+1','+1','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(5,'e3r','+1','+0','+1','+0','+1','+0','+1','+1','+1','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(6,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(6,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(6,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(6,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(7,'a1a','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(7,'a1t','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(7,'a2z','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(7,'e3r','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','+1','+0','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(8,'a1a','+1','+0','+1','+0','+1','+0','+1','+1','1','-1','-1','1','+0','+0','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(8,'a1t','+1','+0','+1','+0','+1','+0','+1','+1','1','-1','-1','1','+0','+0','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(8,'a2z','+1','+0','+1','+0','+1','+0','+1','+1','1','-1','-1','1','+0','+0','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(8,'e3r','+1','+0','+1','+0','+1','+0','+1','+1','1','-1','-1','1','+0','+0','+0','+0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','+0','+0','+0','-0','-0','-0','+0','+1','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(9,'a1a','+1','+0','+1','+0','+1','+0','1','0','0','-1','-1','1','+1','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(9,'a1t','+1','+0','+1','+0','+1','+0','1','0','0','-1','-1','1','+1','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(9,'a2z','+1','+0','+1','+0','+1','+0','1','0','0','-1','-1','1','+1','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(9,'e3r','+1','+0','+1','+0','+1','+0','1','0','0','-1','-1','1','+1','+0','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(10,'a1a','+1','+0','+1','+0','+1','+0','-0','1','1','-1','-1','0','+0','+1','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(10,'a1t','+1','+0','+1','+0','+1','+0','-0','1','1','-1','-1','0','+0','+1','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(10,'a2z','+1','+0','+1','+0','+1','+0','-0','1','1','-1','-1','0','+0','+1','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(10,'e3r','+1','+0','+1','+0','+1','+0','-0','1','1','-1','-1','0','+0','+1','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(11,'a1a','1','0','+1','+0','+1','+0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(11,'a1t','1','0','+1','+0','+1','+0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(11,'a2z','1','0','+1','+0','+1','+0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(11,'e3r','1','0','+1','+0','+1','+0','1','0','-0','-1','-1','1','1','0','+0','+0','-0','+1','+0','+0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(12,'a1a','+1','+0','+1','+0','+1','+0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','-0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(12,'a1t','+1','+0','+1','+0','+1','+0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','-0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(12,'a2z','+1','+0','+1','+0','+1','+0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','-0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(12,'e3r','+1','+0','+1','+0','+1','+0','0','1','-0','-1','-1','0','0','1','+0','+0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','-0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(17,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+0','+0','+1','+0','+0','-0','-0','-0','-0','+1','+0','-1','-1','-1','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(17,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+0','+0','+1','+0','+0','-0','-0','-0','-0','+1','+0','-1','-1','-1','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(17,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+0','+0','+1','+0','+0','-0','-0','-0','-0','+1','+0','-1','-1','-1','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(17,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','+0','+0','+1','+0','+0','-0','-0','-0','-0','+1','+0','-1','-1','-1','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(18,'a1a','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','+0','-0','+0','-0','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(18,'a1t','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','+0','-0','+0','-0','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(18,'a2z','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','+0','-0','+0','-0','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(18,'e3r','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','+1','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','0','1','','1','+1','+0','-0','-0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','+0','-0','+0','-0','','','+0','+0','+0','-0','+1','0','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(30,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','1','+0','+0','1','0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(30,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','1','+0','+0','1','0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(30,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','1','+0','+0','1','0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(30,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','1','+0','+0','1','0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(31,'a1a','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','-0','+0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(31,'a1t','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','-0','+0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(31,'a2z','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','-0','+0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(31,'e3r','+1','+0','+1','+0','+1','+0','+1','+1','-0','-1','-1','1','+0','+0','0','0','-0','-0','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','0','','+0','-0','+0','+0','-0','-0','+0','+0','-0','-0','-0','+0','+0','+0','','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(32,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(32,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(32,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(32,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-1','0','0','1','+0','+1','-0','-0','-0','-0','+1','+0','+1','+0','+0','+0','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','-0','+0','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','','','+0','+0','+0','-0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(33,'a1a','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-0','+1','+0','+0','+0','+0','-0','-0','-0','-0','1','-0','+1','+0','+0','+0','+1','+1','+1','-0','1','','-1','+1','+0','-0','-0','+1','+1','-0','-0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','+0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(33,'a1t','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-0','+1','+0','+0','+0','+0','-0','-0','-0','-0','1','-0','+1','+0','+0','+0','+1','+1','+1','-0','1','','-1','+1','+0','-0','-0','+1','+1','-0','-0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','+0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(33,'a2z','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-0','+1','+0','+0','+0','+0','-0','-0','-0','-0','1','-0','+1','+0','+0','+0','+1','+1','+1','-0','1','','-1','+1','+0','-0','-0','+1','+1','-0','-0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','+0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(33,'e3r','+1','+0','+1','+0','+1','+0','+0','+1','-0','-1','-0','+1','+0','+0','+0','+0','-0','-0','-0','-0','1','-0','+1','+0','+0','+0','+1','+1','+1','-0','1','','-1','+1','+0','-0','-0','+1','+1','-0','-0','+0','','+0','+0','+0','+0','+0','-0','-0','+0','-0','','','+0','+0','+0','+0','+1','1','-1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1000,'a1a','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1000,'a1t','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1000,'a2z','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1000,'e3r','1','0','1','0','1','0','1','0','-0','-1','-1','1','1','0','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','-0','0','','+0','-0','+0','+0','+0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1004,'a1a','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+1','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1004,'a1t','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+1','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1004,'a2z','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+1','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1'),(1004,'e3r','1','0','1','0','1','0','0','1','-0','-1','-1','0','0','1','+0','+0','+0','+1','-0','-0','+1','+0','+1','+0','+0','+1','+1','+1','+1','+0','1','','+1','+1','+0','+0','+0','+1','+1','+0','+1','+1','','+0','+0','+0','+0','-0','-0','-0','+0','-0','-0','-0','+0','+0','+0','-0','-0','1','+1','','DAY','+1','+0','-0','-0','-0','+1','+0','1','1');
/*!40000 ALTER TABLE `ProcedureEquivalence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Qualification`
--

LOCK TABLES `Qualification` WRITE;
/*!40000 ALTER TABLE `Qualification` DISABLE KEYS */;
/*!40000 ALTER TABLE `Qualification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `QuestionDCE`
--

LOCK TABLES `QuestionDCE` WRITE;
/*!40000 ALTER TABLE `QuestionDCE` DISABLE KEYS */;
/*!40000 ALTER TABLE `QuestionDCE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `RG`
--

LOCK TABLES `RG` WRITE;
/*!40000 ALTER TABLE `RG` DISABLE KEYS */;
/*!40000 ALTER TABLE `RG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `RPA`
--

LOCK TABLES `RPA` WRITE;
/*!40000 ALTER TABLE `RPA` DISABLE KEYS */;
/*!40000 ALTER TABLE `RPA` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Referentiel`
--

LOCK TABLES `Referentiel` WRITE;
/*!40000 ALTER TABLE `Referentiel` DISABLE KEYS */;
INSERT INTO `Referentiel` (`id_referentiel`, `libelle_referentiel`) VALUES (4,'modèle avis appel d\'offre avec sélection'),(5,'modèle avis concours'),(6,'Type de pouvoir adjudicateur'),(7,' Principale(s) activité(s) du pouvoir adjudicateu'),(8,' Principale(s) activité(s) de l\'entité adjudicatrice'),(13,'Type de collaboration'),(14,'types de documents pour la Mise à disposition des pièces de marchés'),(15,'Forme juridique du groupement attributaire'),(16,'Durée  ou délai du marché'),(18,'Durée  ou délai du marché'),(19,'Critère(s) d\'attribution'),(20,'CCAG de référence'),(21,'Unité durée marché'),(22,'Variation du prix'),(23,'Type de Prix'),(24,'Unité de prix'),(25,'Rejeté, problème de tiers fournisseur '),(26,'Les tranches effectis'),(27,'Droits de propriété intellectuelle'),(157,'Etapes de l\'ordre du jour de la séance'),(161,'Les catégories des statistiques sur la plateforme'),(184,'Civilité'),(185,'Statut de la séance de commission'),(210,'Liste des actions tracées côte entreprises'),(486,'Justification'),(487,'Justification npn publication marché'),(488,'Type de modification');
/*!40000 ALTER TABLE `Referentiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReferentielDestinationFormXml`
--

LOCK TABLES `ReferentielDestinationFormXml` WRITE;
/*!40000 ALTER TABLE `ReferentielDestinationFormXml` DISABLE KEYS */;
INSERT INTO `ReferentielDestinationFormXml` (`id`, `organisme`, `destinataire`) VALUES (1,'a1a','BOAMP seul'),(1,'a1t','BOAMP seul'),(1,'a2z','BOAMP seul'),(1,'e3r','BOAMP seul'),(2,'a1a','BOAMP et JOUE'),(2,'a1t','BOAMP et JOUE'),(2,'a2z','BOAMP et JOUE'),(2,'e3r','BOAMP et JOUE'),(3,'a1a','Groupe Moniteur'),(3,'a1t','Groupe Moniteur'),(3,'a2z','Groupe Moniteur'),(3,'e3r','Groupe Moniteur');
/*!40000 ALTER TABLE `ReferentielDestinationFormXml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReferentielFormXml`
--

LOCK TABLES `ReferentielFormXml` WRITE;
/*!40000 ALTER TABLE `ReferentielFormXml` DISABLE KEYS */;
/*!40000 ALTER TABLE `ReferentielFormXml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReferentielOrg`
--

LOCK TABLES `ReferentielOrg` WRITE;
/*!40000 ALTER TABLE `ReferentielOrg` DISABLE KEYS */;
INSERT INTO `ReferentielOrg` (`id_referentiel`, `organisme`, `libelle_referentiel`) VALUES (1,'a1a','Type organismes'),(1,'a1t','Type organismes'),(1,'a2z','Type organismes'),(1,'e3r','Type organismes'),(2,'a1a','Tranche budgétaire'),(2,'a1t','Tranche budgétaire'),(2,'a2z','Tranche budgétaire'),(2,'e3r','Tranche budgétaire'),(3,'a1a','pme pmi'),(3,'a1t','pme pmi'),(3,'a2z','pme pmi'),(3,'e3r','pme pmi'),(15,'a1a','Type contrat'),(15,'a1t','Type contrat'),(15,'a2z','Type contrat'),(15,'e3r','Type contrat'),(158,'a1a','Type opération'),(158,'a1t','Type opération'),(158,'a2z','Type opération'),(158,'e3r','Type opération');
/*!40000 ALTER TABLE `ReferentielOrg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReferentielTypeXml`
--

LOCK TABLES `ReferentielTypeXml` WRITE;
/*!40000 ALTER TABLE `ReferentielTypeXml` DISABLE KEYS */;
INSERT INTO `ReferentielTypeXml` (`id`, `organisme`, `id_destinataire`, `libelle_type`, `libelle_type_fr`, `libelle_type_en`, `libelle_type_es`, `libelle_type_su`, `libelle_type_du`, `libelle_type_cz`, `libelle_type_ar`, `libelle_type_it`, `id_avis_marche`) VALUES (1,'a1a','1#3','XML 1.9 BOAMP3 - Avis de marché National','','','','','','','','',NULL),(1,'a1t','1#3','XML 1.9 BOAMP3 - Avis de marché National','','','','','','','','',NULL),(1,'a2z','1#3','XML 1.9 BOAMP3 - Avis de marché National','','','','','','','','',NULL),(1,'e3r','1#3','XML 1.9 BOAMP3 - Avis de marché National','','','','','','','','',NULL),(2,'a1a','1#3','XML 1.9 BOAMP6 - Avis de MAPA','','','','','','','','',NULL),(2,'a1t','1#3','XML 1.9 BOAMP6 - Avis de MAPA','','','','','','','','',NULL),(2,'a2z','1#3','XML 1.9 BOAMP6 - Avis de MAPA','','','','','','','','',NULL),(2,'e3r','1#3','XML 1.9 BOAMP6 - Avis de MAPA','','','','','','','','',NULL),(3,'a1a','2#3','XML 1.9 JOUE_02 - Avis de marché','','','','','','','','',NULL),(3,'a1t','2#3','XML 1.9 JOUE_02 - Avis de marché','','','','','','','','',NULL),(3,'a2z','2#3','XML 1.9 JOUE_02 - Avis de marché','','','','','','','','',NULL),(3,'e3r','2#3','XML 1.9 JOUE_02 - Avis de marché','','','','','','','','',NULL),(4,'a1a','2#3','XML 1.9 JOUE_03 - Avis d\'attribution','','','','','','','','',NULL),(4,'a1t','2#3','XML 1.9 JOUE_03 - Avis d\'attribution','','','','','','','','',NULL),(4,'a2z','2#3','XML 1.9 JOUE_03 - Avis d\'attribution','','','','','','','','',NULL),(4,'e3r','2#3','XML 1.9 JOUE_03 - Avis d\'attribution','','','','','','','','',NULL),(5,'a1a','1#3','XML 1.9 BOAMP4 - Avis d\'attribution','','','','','','','','',NULL),(5,'a1t','1#3','XML 1.9 BOAMP4 - Avis d\'attribution','','','','','','','','',NULL),(5,'a2z','1#3','XML 1.9 BOAMP4 - Avis d\'attribution','','','','','','','','',NULL),(5,'e3r','1#3','XML 1.9 BOAMP4 - Avis d\'attribution','','','','','','','','',NULL),(6,'a1a','1#3','XML 1.9 BOAMP2 - Avis rectificatif','','','','','','','','',NULL),(6,'a1t','1#3','XML 1.9 BOAMP2 - Avis rectificatif','','','','','','','','',NULL),(6,'a2z','1#3','XML 1.9 BOAMP2 - Avis rectificatif','','','','','','','','',NULL),(6,'e3r','1#3','XML 1.9 BOAMP2 - Avis rectificatif','','','','','','','','',NULL),(7,'a1a','1#3','XML 1.9 BOAMP7 - Résultat de MAPA','','','','','','','','',NULL),(7,'a1t','1#3','XML 1.9 BOAMP7 - Résultat de MAPA','','','','','','','','',NULL),(7,'a2z','1#3','XML 1.9 BOAMP7 - Résultat de MAPA','','','','','','','','',NULL),(7,'e3r','1#3','XML 1.9 BOAMP7 - Résultat de MAPA','','','','','','','','',NULL),(8,'a1a','2#3','XML 1.9 JOUE_12 - Avis Concours','','','','','','','','',NULL),(8,'a1t','2#3','XML 1.9 JOUE_12 - Avis Concours','','','','','','','','',NULL),(8,'a2z','2#3','XML 1.9 JOUE_12 - Avis Concours','','','','','','','','',NULL),(8,'e3r','2#3','XML 1.9 JOUE_12 - Avis Concours','','','','','','','','',NULL),(9,'a1a','2#3','XML 1.9 JOUE_14 - Formulaire rectificatif / annulation','','','','','','','','',NULL),(9,'a1t','2#3','XML 1.9 JOUE_14 - Formulaire rectificatif / annulation','','','','','','','','',NULL),(9,'a2z','2#3','XML 1.9 JOUE_14 - Formulaire rectificatif / annulation','','','','','','','','',NULL),(9,'e3r','2#3','XML 1.9 JOUE_14 - Formulaire rectificatif / annulation','','','','','','','','',NULL),(10,'a1a','2#3','XML 1.9 JOUE_05 - Secteurs Spéciaux','','','','','','','','',NULL),(10,'a1t','2#3','XML 1.9 JOUE_05 - Secteurs Spéciaux','','','','','','','','',NULL),(10,'a2z','2#3','XML 1.9 JOUE_05 - Secteurs Spéciaux','','','','','','','','',NULL),(10,'e3r','2#3','XML 1.9 JOUE_05 - Secteurs Spéciaux','','','','','','','','',NULL),(11,'a1a','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(11,'a1t','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(11,'a2z','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(11,'e3r','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(12,'a1a','2','XML 1.9 JOUE_13 - Résultat Concours','','','','','','','','',NULL),(12,'a1t','2','XML 1.9 JOUE_13 - Résultat Concours','','','','','','','','',NULL),(12,'a2z','2','XML 1.9 JOUE_13 - Résultat Concours','','','','','','','','',NULL),(12,'e3r','2','XML 1.9 JOUE_13 - Résultat Concours','','','','','','','','',NULL),(13,'a1a','2','XML 1.9 JOUE_10 - Concession de travaux publics','','','','','','','','',NULL),(13,'a1t','2','XML 1.9 JOUE_10 - Concession de travaux publics','','','','','','','','',NULL),(13,'a2z','2','XML 1.9 JOUE_10 - Concession de travaux publics','','','','','','','','',NULL),(13,'e3r','2','XML 1.9 JOUE_10 - Concession de travaux publics','','','','','','','','',NULL),(14,'a1a','2','XML 1.9 JOUE_01T - Avis de préinformation - Travaux','','','','','','','','',NULL),(14,'a1t','2','XML 1.9 JOUE_01T - Avis de préinformation - Travaux','','','','','','','','',NULL),(14,'a2z','2','XML 1.9 JOUE_01T - Avis de préinformation - Travaux','','','','','','','','',NULL),(14,'e3r','2','XML 1.9 JOUE_01T - Avis de préinformation - Travaux','','','','','','','','',NULL),(15,'a1a','2','XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services','','','','','','','','',NULL),(15,'a1t','2','XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services','','','','','','','','',NULL),(15,'a2z','2','XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services','','','','','','','','',NULL),(15,'e3r','2','XML 1.9 JOUE_01FS - Avis de préinformation - Fournitures / Services','','','','','','','','',NULL),(16,'a1a','1','XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification','','','','','','','','',NULL),(16,'a1t','1','XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification','','','','','','','','',NULL),(16,'a2z','1','XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification','','','','','','','','',NULL),(16,'e3r','1','XML 1.9 BOAMP3DEF - Formulaire Défense - Système de qualification','','','','','','','','',NULL),(17,'a1a','2#3','XML 1.9 JOUE_06 -  Avis d\'attribution - Secteurs Spéciaux','','','','','','','','',NULL),(17,'a1t','2#3','XML 1.9 JOUE_06 -  Avis d\'attribution - Secteurs Spéciaux','','','','','','','','',NULL),(17,'a2z','2#3','XML 1.9 JOUE_06 -  Avis d\'attribution - Secteurs Spéciaux','','','','','','','','',NULL),(17,'e3r','2#3','XML 1.9 JOUE_06 -  Avis d\'attribution - Secteurs Spéciaux','','','','','','','','',NULL),(18,'a1a','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(18,'a1t','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(18,'a2z','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL),(18,'e3r','2','XML 1.9 JOUE15 - Avis en cas de transparence ex ante volontaire','','','','','','','','',NULL);
/*!40000 ALTER TABLE `ReferentielTypeXml` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Referentiel_Agent`
--

LOCK TABLES `Referentiel_Agent` WRITE;
/*!40000 ALTER TABLE `Referentiel_Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `Referentiel_Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Referentiel_Consultation`
--

LOCK TABLES `Referentiel_Consultation` WRITE;
/*!40000 ALTER TABLE `Referentiel_Consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `Referentiel_Consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Referentiel_Entreprise`
--

LOCK TABLES `Referentiel_Entreprise` WRITE;
/*!40000 ALTER TABLE `Referentiel_Entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `Referentiel_Entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Referentiel_org_denomination`
--

LOCK TABLES `Referentiel_org_denomination` WRITE;
/*!40000 ALTER TABLE `Referentiel_org_denomination` DISABLE KEYS */;
/*!40000 ALTER TABLE `Referentiel_org_denomination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Relation_Echange`
--

LOCK TABLES `Relation_Echange` WRITE;
/*!40000 ALTER TABLE `Relation_Echange` DISABLE KEYS */;
/*!40000 ALTER TABLE `Relation_Echange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReponseInscritFormulaireConsultation`
--

LOCK TABLES `ReponseInscritFormulaireConsultation` WRITE;
/*!40000 ALTER TABLE `ReponseInscritFormulaireConsultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ReponseInscritFormulaireConsultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ReponseInscritItemFormulaireConsultationValues`
--

LOCK TABLES `ReponseInscritItemFormulaireConsultationValues` WRITE;
/*!40000 ALTER TABLE `ReponseInscritItemFormulaireConsultationValues` DISABLE KEYS */;
/*!40000 ALTER TABLE `ReponseInscritItemFormulaireConsultationValues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Retrait_Papier`
--

LOCK TABLES `Retrait_Papier` WRITE;
/*!40000 ALTER TABLE `Retrait_Papier` DISABLE KEYS */;
/*!40000 ALTER TABLE `Retrait_Papier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Service`
--

LOCK TABLES `Service` WRITE;
/*!40000 ALTER TABLE `Service` DISABLE KEYS */;
INSERT INTO `Service` (`id`, `organisme`, `type_service`, `libelle`, `sigle`, `adresse`, `adresse_suite`, `cp`, `ville`, `telephone`, `fax`, `mail`, `pays`, `id_initial`, `date_creation`, `date_modification`, `siren`, `complement`, `libelle_ar`, `adresse_ar`, `adresse_suite_ar`, `ville_ar`, `pays_ar`, `libelle_fr`, `adresse_fr`, `adresse_suite_fr`, `ville_fr`, `pays_fr`, `libelle_es`, `adresse_es`, `adresse_suite_es`, `ville_es`, `pays_es`, `libelle_en`, `adresse_en`, `adresse_suite_en`, `ville_en`, `pays_en`, `libelle_su`, `adresse_su`, `adresse_suite_su`, `ville_su`, `pays_su`, `libelle_du`, `adresse_du`, `adresse_suite_du`, `ville_du`, `pays_du`, `libelle_cz`, `adresse_cz`, `adresse_suite_cz`, `ville_cz`, `pays_cz`, `libelle_it`, `adresse_it`, `adresse_suite_it`, `ville_it`, `pays_it`, `chemin_complet`, `chemin_complet_fr`, `chemin_complet_en`, `chemin_complet_es`, `chemin_complet_su`, `chemin_complet_du`, `chemin_complet_cz`, `chemin_complet_ar`, `chemin_complet_it`, `nom_service_archiveur`, `identifiant_service_archiveur`, `affichage_service`, `activation_fuseau_horaire`, `decalage_horaire`, `lieu_residence`, `alerte`, `acces_chorus`, `forme_juridique`, `forme_juridique_code`) VALUES (1,'a1a','2','MYPRM','PRM','128 RUE LA BOETIE','','75008','PARIS 8','0102030405','0102030405','florian.mart@yopmail.com','FRANCE',0,'2019-03-14 17:38:05','2019-03-14 17:38:05','831670377','00017','','','','','NULL','','','','','NULL','','','','','NULL','','','','','NULL','','','','','NULL','','','','','NULL','','','','','','','','','','NULL','OPR / PRM - MYPRM','','','','','','','OPR / PRM - ','','prm','prm','1','0',NULL,NULL,'0','0','Société par actions simplifiée à associé unique ou société par actions simplifiée unipersonnelle ','5720');
/*!40000 ALTER TABLE `Service` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Service_Mertier`
--

LOCK TABLES `Service_Mertier` WRITE;
/*!40000 ALTER TABLE `Service_Mertier` DISABLE KEYS */;
INSERT INTO `Service_Mertier` (`id`, `sigle`, `denomination`, `url_acces`, `logo`, `url_deconnexion`, `ordre`) VALUES (1,'MPE','MARCHES_PUBLICS_ELECTRONIQUES','?page=Agent.AccueilAgentAuthentifie','','',0);
/*!40000 ALTER TABLE `Service_Mertier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Service_Mertier_Profils`
--

LOCK TABLES `Service_Mertier_Profils` WRITE;
/*!40000 ALTER TABLE `Service_Mertier_Profils` DISABLE KEYS */;
INSERT INTO `Service_Mertier_Profils` (`id_auto`, `id_interne`, `id_service_metier`, `id_externe`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`, `show_profile_for_hyperadmin_only`) VALUES (1,1,1,1,'Administrateur/Acheteur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1'),(2,4,1,4,'Acheteur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(3,6,1,6,'Acheteur < 90 kEUR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(4,12,1,12,'Administrateur/Acheteur',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0'),(5,13,1,13,'Administrateur Service',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
/*!40000 ALTER TABLE `Service_Mertier_Profils` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Societes_Exclues`
--

LOCK TABLES `Societes_Exclues` WRITE;
/*!40000 ALTER TABLE `Societes_Exclues` DISABLE KEYS */;
/*!40000 ALTER TABLE `Societes_Exclues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Socle_Habilitation_Agent`
--

LOCK TABLES `Socle_Habilitation_Agent` WRITE;
/*!40000 ALTER TABLE `Socle_Habilitation_Agent` DISABLE KEYS */;
INSERT INTO `Socle_Habilitation_Agent` (`id_agent`, `gestion_agent_pole_socle`, `gestion_agents_socle`, `droit_gestion_services_socle`) VALUES (1,'1','1','1'),(2,'1','1','1'),(3,'1','1','1'),(4,'1','1','1'),(5,'1','1','1');
/*!40000 ALTER TABLE `Socle_Habilitation_Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `SousCategorie`
--

LOCK TABLES `SousCategorie` WRITE;
/*!40000 ALTER TABLE `SousCategorie` DISABLE KEYS */;
/*!40000 ALTER TABLE `SousCategorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Sso_Tiers`
--

LOCK TABLES `Sso_Tiers` WRITE;
/*!40000 ALTER TABLE `Sso_Tiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Sso_Tiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `StatutCommission`
--

LOCK TABLES `StatutCommission` WRITE;
/*!40000 ALTER TABLE `StatutCommission` DISABLE KEYS */;
/*!40000 ALTER TABLE `StatutCommission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `StatutEnveloppe`
--

LOCK TABLES `StatutEnveloppe` WRITE;
/*!40000 ALTER TABLE `StatutEnveloppe` DISABLE KEYS */;
INSERT INTO `StatutEnveloppe` (`id_statut`, `description`) VALUES (1,'FERMEE'),(2,'DEFINE_OUVERTE_EN_LIGNE'),(3,'DEFINE_OUVERTE_HORS_LIGNE'),(4,'DEFINE_REFUSEE'),(5,'OUVERTE'),(6,'DEFINE_SUPPRIMEE'),(7,'DEFINE_OUVERTE_A_DISTANCE'),(8,'DEFINE_EN_COURS_CHIFFREMENT'),(9,'DEFINE_EN_COURS_DECHIFFREMENT'),(10,'DEFINE_EN_ATTENTE_CHIFFREMENT'),(11,'DEFINE_EN_COURS_FERMETURE'),(12,'DEFINE_EN_ATTENTE_FERMETURE');
/*!40000 ALTER TABLE `StatutEnveloppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `SuiviAcces`
--

LOCK TABLES `SuiviAcces` WRITE;
/*!40000 ALTER TABLE `SuiviAcces` DISABLE KEYS */;
INSERT INTO `SuiviAcces` (`id_auto`, `id_agent`, `date_acces`, `id_service`, `nom`, `prenom`, `email`, `organisme`) VALUES (1,1,'2019-03-14 17:33',0,'Admin','Hyper','suivi.mpe@atexo.com','a1a'),(2,6,'2019-03-14 17:39',1,'MARTIN','florian','florian.martin@atexo.com','a1a'),(3,1,'2019-03-14 17:49',0,'Admin','Hyper','suivi.mpe@atexo.com','a1a'),(4,6,'2019-03-14 17:55',1,'MARTIN','florian','florian.martin@atexo.com','a1a'),(5,1,'2019-03-14 17:56',0,'Admin','Hyper','suivi.mpe@atexo.com','a1a'),(6,1,'2019-03-14 18:05',0,'Admin','Hyper','suivi.mpe@atexo.com','a1a'),(7,1,'2019-03-14 18:18',0,'Admin','Hyper','suivi.mpe@atexo.com','a1a');
/*!40000 ALTER TABLE `SuiviAcces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Illustration_Fond`
--

LOCK TABLES `T_Illustration_Fond` WRITE;
/*!40000 ALTER TABLE `T_Illustration_Fond` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Illustration_Fond` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_MesRecherches`
--

LOCK TABLES `T_MesRecherches` WRITE;
/*!40000 ALTER TABLE `T_MesRecherches` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_MesRecherches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Profil_Joue`
--

LOCK TABLES `T_Profil_Joue` WRITE;
/*!40000 ALTER TABLE `T_Profil_Joue` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Profil_Joue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Recherche_Sauvegardee`
--

LOCK TABLES `T_Recherche_Sauvegardee` WRITE;
/*!40000 ALTER TABLE `T_Recherche_Sauvegardee` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Recherche_Sauvegardee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Telechargement_Asynchrone`
--

LOCK TABLES `T_Telechargement_Asynchrone` WRITE;
/*!40000 ALTER TABLE `T_Telechargement_Asynchrone` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Telechargement_Asynchrone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Telechargement_Asynchrone_fichier`
--

LOCK TABLES `T_Telechargement_Asynchrone_fichier` WRITE;
/*!40000 ALTER TABLE `T_Telechargement_Asynchrone_fichier` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Telechargement_Asynchrone_fichier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Theme_Graphique`
--

LOCK TABLES `T_Theme_Graphique` WRITE;
/*!40000 ALTER TABLE `T_Theme_Graphique` DISABLE KEYS */;
/*!40000 ALTER TABLE `T_Theme_Graphique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_Traduction`
--

LOCK TABLES `T_Traduction` WRITE;
/*!40000 ALTER TABLE `T_Traduction` DISABLE KEYS */;
INSERT INTO `T_Traduction` (`langue`, `id_libelle`, `libelle`) VALUES ('fr',1,'PRM-FLO-01'),('fr',2,'PRM-FLO-01'),('fr',3,'REF-FLO-0101'),('fr',4,'REF-FLO-0101');
/*!40000 ALTER TABLE `T_Traduction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `T_trace_operations_inscrit_details`
--

LOCK TABLES `T_trace_operations_inscrit_details` WRITE;
/*!40000 ALTER TABLE `T_trace_operations_inscrit_details` DISABLE KEYS */;
INSERT INTO `T_trace_operations_inscrit_details` (`id`, `id_trace`, `date_debut_action`, `nom_action`, `details`, `date_fin_action`, `id_description`, `afficher`, `descripton`, `log_applet`, `lien_download`, `infos_browser`, `date_debut_action_client`, `id_offre`, `debut_action_millisecond`) VALUES (1,1,'2019-03-14 17:32:32','page=Entreprise.EntrepriseHome&goto=','-','2019-03-14 17:32:32',0,'1','','',NULL,NULL,NULL,NULL,1552581152368),(2,1,'2019-03-14 17:32:32','page=Entreprise.EntrepriseAccueilAuthentifie','-','2019-03-14 17:32:32',0,'1','','',NULL,NULL,NULL,NULL,1552581152974);
/*!40000 ALTER TABLE `T_trace_operations_inscrit_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Telechargement`
--

LOCK TABLES `Telechargement` WRITE;
/*!40000 ALTER TABLE `Telechargement` DISABLE KEYS */;
/*!40000 ALTER TABLE `Telechargement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TelechargementAnonyme`
--

LOCK TABLES `TelechargementAnonyme` WRITE;
/*!40000 ALTER TABLE `TelechargementAnonyme` DISABLE KEYS */;
/*!40000 ALTER TABLE `TelechargementAnonyme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Tiers`
--

LOCK TABLES `Tiers` WRITE;
/*!40000 ALTER TABLE `Tiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Tiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TireurPlan`
--

LOCK TABLES `TireurPlan` WRITE;
/*!40000 ALTER TABLE `TireurPlan` DISABLE KEYS */;
/*!40000 ALTER TABLE `TireurPlan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Traduction`
--

LOCK TABLES `Traduction` WRITE;
/*!40000 ALTER TABLE `Traduction` DISABLE KEYS */;
/*!40000 ALTER TABLE `Traduction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Tranche_Article_133`
--

LOCK TABLES `Tranche_Article_133` WRITE;
/*!40000 ALTER TABLE `Tranche_Article_133` DISABLE KEYS */;
INSERT INTO `Tranche_Article_133` (`id`, `acronyme_org`, `millesime`, `Libelle_tranche_budgetaire`, `borne_inf`, `borne_sup`) VALUES (1,'a1a','2010','0 à 3 999,99 HT','0','3 999,99'),(2,'a1a','2010','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(3,'a1a','2010','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(4,'a1a','2010','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(5,'a1a','2010','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(6,'a1a','2010','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(7,'a1a','2010','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(8,'a1a','2010','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(9,'a1a','2010','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(10,'a1a','2010','4 845 000 HT et plus.','4845000',''),(11,'a1a','2011','0 à 3 999,99 HT','0','3 999,99'),(12,'a1a','2011','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(13,'a1a','2011','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(14,'a1a','2011','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(15,'a1a','2011','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(16,'a1a','2011','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(17,'a1a','2011','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(18,'a1a','2011','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(19,'a1a','2011','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(20,'a1a','2011','4 845 000 HT et plus.','4845000',''),(21,'a1a','2012','0 à 3 999,99 HT','0','3 999,99'),(22,'a1a','2012','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(23,'a1a','2012','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(24,'a1a','2012','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(25,'a1a','2012','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(26,'a1a','2012','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(27,'a1a','2012','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(28,'a1a','2012','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(29,'a1a','2012','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(30,'a1a','2012','4 845 000 HT et plus.','4845000',''),(31,'a1a','2013','0 à 3 999,99 HT','0','3 999,99'),(32,'a1a','2013','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(33,'a1a','2013','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(34,'a1a','2013','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(35,'a1a','2013','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(36,'a1a','2013','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(37,'a1a','2013','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(38,'a1a','2013','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(39,'a1a','2013','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(40,'a1a','2013','4 845 000 HT et plus.','4845000',''),(41,'a1a','2014','0 à 3 999,99 HT','0','3 999,99'),(42,'a1a','2014','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(43,'a1a','2014','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(44,'a1a','2014','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(45,'a1a','2014','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(46,'a1a','2014','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(47,'a1a','2014','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(48,'a1a','2014','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(49,'a1a','2014','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(50,'a1a','2014','4 845 000 HT et plus.','4845000',''),(51,'a1a','2015','0 à 3 999,99 HT','0','3 999,99'),(52,'a1a','2015','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(53,'a1a','2015','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(54,'a1a','2015','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(55,'a1a','2015','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(56,'a1a','2015','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(57,'a1a','2015','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(58,'a1a','2015','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(59,'a1a','2015','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(60,'a1a','2015','4 845 000 HT et plus.','4845000',''),(121,'a2z','2010','0 à 3 999,99 HT','0','3 999,99'),(122,'a2z','2010','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(123,'a2z','2010','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(124,'a2z','2010','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(125,'a2z','2010','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(126,'a2z','2010','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(127,'a2z','2010','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(128,'a2z','2010','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(129,'a2z','2010','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(130,'a2z','2010','4 845 000 HT et plus.','4845000',''),(131,'a2z','2011','0 à 3 999,99 HT','0','3 999,99'),(132,'a2z','2011','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(133,'a2z','2011','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(134,'a2z','2011','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(135,'a2z','2011','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(136,'a2z','2011','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(137,'a2z','2011','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(138,'a2z','2011','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(139,'a2z','2011','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(140,'a2z','2011','4 845 000 HT et plus.','4845000',''),(141,'a2z','2012','0 à 3 999,99 HT','0','3 999,99'),(142,'a2z','2012','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(143,'a2z','2012','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(144,'a2z','2012','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(145,'a2z','2012','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(146,'a2z','2012','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(147,'a2z','2012','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(148,'a2z','2012','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(149,'a2z','2012','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(150,'a2z','2012','4 845 000 HT et plus.','4845000',''),(151,'a2z','2013','0 à 3 999,99 HT','0','3 999,99'),(152,'a2z','2013','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(153,'a2z','2013','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(154,'a2z','2013','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(155,'a2z','2013','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(156,'a2z','2013','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(157,'a2z','2013','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(158,'a2z','2013','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(159,'a2z','2013','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(160,'a2z','2013','4 845 000 HT et plus.','4845000',''),(161,'a2z','2014','0 à 3 999,99 HT','0','3 999,99'),(162,'a2z','2014','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(163,'a2z','2014','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(164,'a2z','2014','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(165,'a2z','2014','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(166,'a2z','2014','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(167,'a2z','2014','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(168,'a2z','2014','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(169,'a2z','2014','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(170,'a2z','2014','4 845 000 HT et plus.','4845000',''),(171,'a2z','2015','0 à 3 999,99 HT','0','3 999,99'),(172,'a2z','2015','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(173,'a2z','2015','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(174,'a2z','2015','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(175,'a2z','2015','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(176,'a2z','2015','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(177,'a2z','2015','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(178,'a2z','2015','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(179,'a2z','2015','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(180,'a2z','2015','4 845 000 HT et plus.','4845000',''),(241,'e3r','2010','0 à 3 999,99 HT','0','3 999,99'),(242,'e3r','2010','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(243,'e3r','2010','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(244,'e3r','2010','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(245,'e3r','2010','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(246,'e3r','2010','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(247,'e3r','2010','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(248,'e3r','2010','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(249,'e3r','2010','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(250,'e3r','2010','4 845 000 HT et plus.','4845000',''),(251,'e3r','2011','0 à 3 999,99 HT','0','3 999,99'),(252,'e3r','2011','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(253,'e3r','2011','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(254,'e3r','2011','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(255,'e3r','2011','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(256,'e3r','2011','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(257,'e3r','2011','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(258,'e3r','2011','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(259,'e3r','2011','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(260,'e3r','2011','4 845 000 HT et plus.','4845000',''),(261,'e3r','2012','0 à 3 999,99 HT','0','3 999,99'),(262,'e3r','2012','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(263,'e3r','2012','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(264,'e3r','2012','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(265,'e3r','2012','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(266,'e3r','2012','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(267,'e3r','2012','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(268,'e3r','2012','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(269,'e3r','2012','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(270,'e3r','2012','4 845 000 HT et plus.','4845000',''),(271,'e3r','2013','0 à 3 999,99 HT','0','3 999,99'),(272,'e3r','2013','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(273,'e3r','2013','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(274,'e3r','2013','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(275,'e3r','2013','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(276,'e3r','2013','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(277,'e3r','2013','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(278,'e3r','2013','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(279,'e3r','2013','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(280,'e3r','2013','4 845 000 HT et plus.','4845000',''),(281,'e3r','2014','0 à 3 999,99 HT','0','3 999,99'),(282,'e3r','2014','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(283,'e3r','2014','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(284,'e3r','2014','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(285,'e3r','2014','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(286,'e3r','2014','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(287,'e3r','2014','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(288,'e3r','2014','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(289,'e3r','2014','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(290,'e3r','2014','4 845 000 HT et plus.','4845000',''),(291,'e3r','2015','0 à 3 999,99 HT','0','3 999,99'),(292,'e3r','2015','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(293,'e3r','2015','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(294,'e3r','2015','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(295,'e3r','2015','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(296,'e3r','2015','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(297,'e3r','2015','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(298,'e3r','2015','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(299,'e3r','2015','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(300,'e3r','2015','4 845 000 HT et plus.','4845000',''),(361,'a1t','2010','0 à 3 999,99 HT','0','3 999,99'),(362,'a1t','2010','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(363,'a1t','2010','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(364,'a1t','2010','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(365,'a1t','2010','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(366,'a1t','2010','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(367,'a1t','2010','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(368,'a1t','2010','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(369,'a1t','2010','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(370,'a1t','2010','4 845 000 HT et plus.','4845000',''),(371,'a1t','2011','0 à 3 999,99 HT','0','3 999,99'),(372,'a1t','2011','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(373,'a1t','2011','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(374,'a1t','2011','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(375,'a1t','2011','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(376,'a1t','2011','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(377,'a1t','2011','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(378,'a1t','2011','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(379,'a1t','2011','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(380,'a1t','2011','4 845 000 HT et plus.','4845000',''),(381,'a1t','2012','0 à 3 999,99 HT','0','3 999,99'),(382,'a1t','2012','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(383,'a1t','2012','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(384,'a1t','2012','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(385,'a1t','2012','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(386,'a1t','2012','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(387,'a1t','2012','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(388,'a1t','2012','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(389,'a1t','2012','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(390,'a1t','2012','4 845 000 HT et plus.','4845000',''),(391,'a1t','2013','0 à 3 999,99 HT','0','3 999,99'),(392,'a1t','2013','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(393,'a1t','2013','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(394,'a1t','2013','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(395,'a1t','2013','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(396,'a1t','2013','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(397,'a1t','2013','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(398,'a1t','2013','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(399,'a1t','2013','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(400,'a1t','2013','4 845 000 HT et plus.','4845000',''),(401,'a1t','2014','0 à 3 999,99 HT','0','3 999,99'),(402,'a1t','2014','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(403,'a1t','2014','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(404,'a1t','2014','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(405,'a1t','2014','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(406,'a1t','2014','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(407,'a1t','2014','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(408,'a1t','2014','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(409,'a1t','2014','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(410,'a1t','2014','4 845 000 HT et plus.','4845000',''),(411,'a1t','2015','0 à 3 999,99 HT','0','3 999,99'),(412,'a1t','2015','4 000 HT à 19 999,99 HT','4000',' 19 999,99 '),(413,'a1t','2015','20 000 HT à 49 999,99 HT','20000',' 49 999,99 '),(414,'a1t','2015','50 000 HT à 89 999,99 HT','50000',' 89 999,99 '),(415,'a1t','2015','90 000 HT à 124 999,99 HT','90000',' 124 999,99'),(416,'a1t','2015','125 000 HT à 192 999,99 HT','125000',' 192 999,99'),(417,'a1t','2015','193 000 HT à 999 999,99 HT','193000',' 999 999,99'),(418,'a1t','2015','1 000 000 HT à 2 999 999,99 HT','1000000',' 2 999 999,99 '),(419,'a1t','2015','3 000 000 HT à 4 844 999,99 HT','3000000',' 4 844 999,99'),(420,'a1t','2015','4 845 000 HT et plus.','4845000','');
/*!40000 ALTER TABLE `Tranche_Article_133` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TypeAvenant`
--

LOCK TABLES `TypeAvenant` WRITE;
/*!40000 ALTER TABLE `TypeAvenant` DISABLE KEYS */;
INSERT INTO `TypeAvenant` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'1-Sans incidence financière',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2-Avenant augmentant le montant initial',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'3-avenant diminuant le montant initial',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `TypeAvenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TypeAvis`
--

LOCK TABLES `TypeAvis` WRITE;
/*!40000 ALTER TABLE `TypeAvis` DISABLE KEYS */;
INSERT INTO `TypeAvis` (`id`, `intitule_avis`, `intitule_avis_fr`, `intitule_avis_en`, `intitule_avis_es`, `intitule_avis_su`, `intitule_avis_du`, `intitule_avis_cz`, `intitule_avis_ar`, `abbreviation`, `id_type_avis_ANM`, `intitule_avis_it`) VALUES (2,'Annonce d\'information','Annonce d\'information','en Annonce d\'information ','es Annonce d\'information ',NULL,NULL,NULL,NULL,'API',1,NULL),(3,'Annonce de consultation','Annonce de consultation','en Annonce de consultation ','es Annonce de consultation',NULL,NULL,NULL,NULL,'AAPC',2,NULL),(4,'Annonce d\'attribution','Annonce d\'attribution','en Annonce d\'attribution ','es Annonce d\'attribution ',NULL,NULL,NULL,NULL,'AA',3,NULL),(8,'Annonce de décision de résiliation','Annonce de décision de résiliation',NULL,NULL,NULL,NULL,NULL,'??????? ???? ???????','ADR',0,NULL),(9,'Projet d\'achat','Projet d\'achat','en Projet d\'achat ','es Projet d\'achat ',NULL,NULL,NULL,NULL,'APA',9,'');
/*!40000 ALTER TABLE `TypeAvis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TypeCommission`
--

LOCK TABLES `TypeCommission` WRITE;
/*!40000 ALTER TABLE `TypeCommission` DISABLE KEYS */;
INSERT INTO `TypeCommission` (`id`, `organisme`, `libelle`) VALUES (1,'a1a','Commission d\'ouverture des plis'),(1,'a1t','Commission d\'ouverture des plis'),(1,'a2z','Commission d\'ouverture des plis'),(1,'e3r','Commission d\'ouverture des plis'),(2,'a1a','Commission d\'appel d\'offres'),(2,'a1t','Commission d\'appel d\'offres'),(2,'a2z','Commission d\'appel d\'offres'),(2,'e3r','Commission d\'appel d\'offres'),(3,'a1a','Commission - Autre'),(3,'a1t','Commission - Autre'),(3,'a2z','Commission - Autre'),(3,'e3r','Commission - Autre'),(4,'a1a','Jury - Phase 1'),(4,'a1t','Jury - Phase 1'),(4,'a2z','Jury - Phase 1'),(4,'e3r','Jury - Phase 1'),(5,'a1a','Jury - Phase 2'),(5,'a1t','Jury - Phase 2'),(5,'a2z','Jury - Phase 2'),(5,'e3r','Jury - Phase 2'),(6,'a1a','Jury - Autre'),(6,'a1t','Jury - Autre'),(6,'a2z','Jury - Autre'),(6,'e3r','Jury - Autre');
/*!40000 ALTER TABLE `TypeCommission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TypeDecision`
--

LOCK TABLES `TypeDecision` WRITE;
/*!40000 ALTER TABLE `TypeDecision` DISABLE KEYS */;
INSERT INTO `TypeDecision` (`id_type_decision`, `code_type_decision`) VALUES (2,'DECISION_ATTRIBUTION_MARCHE'),(3,'DECISION_DECLARATION_SANS_SUITE'),(4,'DECISION_DECLARATION_INFRUCTUEUX'),(5,'DECISION_SELECTION_ENTREPRISES'),(6,'DECISION_ATTRIBUTION_DE_ACCORD_CADRE'),(7,'DECISION_ADMISSION_SAD'),(8,'DECISION_AUTRE'),(9,'DECISION_A_RENSEIGNER');
/*!40000 ALTER TABLE `TypeDecision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `TypeProcedure`
--

LOCK TABLES `TypeProcedure` WRITE;
/*!40000 ALTER TABLE `TypeProcedure` DISABLE KEYS */;
INSERT INTO `TypeProcedure` (`id_type_procedure`, `libelle_type_procedure`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `abbreviation`, `type_boamp`, `categorie_procedure`, `id_type_procedure_ANM`, `delai_alerte`, `mapa`, `consultation_transverse`, `code_recensement`, `abbreviation_portail_ANM`, `id_modele`, `libelle_type_procedure_it`, `value_binding_sub`) VALUES (1,'Appel d\'offres ouvert',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AOO',0,1,1,0,'0','0',NULL,'AOO',0,NULL,'01_AOO'),(2,'Appel d\'offres restreint',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AOR',0,2,2,0,'0','0',NULL,'AOR',0,NULL,'02_AOR'),(3,'Procédure adaptée',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MAPA',0,3,7,0,'1','0',NULL,'MAPA',0,NULL,'03_MAPA'),(4,'Concours ouvert',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'COUV',0,4,3,0,'1','0',NULL,'COUV',0,NULL,'04_Concours'),(5,'Concours restreint',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'CRES',0,5,4,0,'0','0',NULL,'CRES',0,NULL,'05_ConcoursRestreint'),(7,'Dialogue compétitif',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'DCO',0,7,6,0,'0','0',NULL,'DCO',0,NULL,'06_DialogueCompetitif'),(8,'Marché négocié',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MN',0,8,5,0,'0','0',NULL,'MN',0,NULL,'08_MarcheNegocie'),(15,'Procédure autre',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AUT',0,15,8,0,'0','0',NULL,'AUT',0,NULL,'12_Autre'),(30,'Accord-Cadre - 1 Sélection des Attributaires (obsolète)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AC-PA',1,1,1,0,'0','0',NULL,'AC-PA',0,NULL,'13_AccordCadreSelection'),(31,'Procédure de passation des marchés subséquents',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'AC-MS',2,2,3,0,'0','0',NULL,'AC-MS',0,NULL,'14_MarcheSubsequent'),(32,'Système d\'Acquisition Dynamique',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SAD-A',8,4,8,0,'0','0',NULL,'SAD-A',0,NULL,'15_SAD'),(33,'Procédure de passation des marchés spécifiques',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'SAD-MS',8,4,8,0,'0','0',NULL,'SAD-MS',0,NULL,'16_MarcheSpecifique'),(1000,'Procédure concurrentielle avec négociations',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'PCN',0,1,1,0,'0','0',NULL,'PCN',0,NULL,'18_PCN');
/*!40000 ALTER TABLE `TypeProcedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Type_Avis_Pub`
--

LOCK TABLES `Type_Avis_Pub` WRITE;
/*!40000 ALTER TABLE `Type_Avis_Pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `Type_Avis_Pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Type_Avis_Pub_Organisme`
--

LOCK TABLES `Type_Avis_Pub_Organisme` WRITE;
/*!40000 ALTER TABLE `Type_Avis_Pub_Organisme` DISABLE KEYS */;
/*!40000 ALTER TABLE `Type_Avis_Pub_Organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Type_Avis_Pub_Procedure`
--

LOCK TABLES `Type_Avis_Pub_Procedure` WRITE;
/*!40000 ALTER TABLE `Type_Avis_Pub_Procedure` DISABLE KEYS */;
/*!40000 ALTER TABLE `Type_Avis_Pub_Procedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Type_Procedure_Organisme`
--

LOCK TABLES `Type_Procedure_Organisme` WRITE;
/*!40000 ALTER TABLE `Type_Procedure_Organisme` DISABLE KEYS */;
INSERT INTO `Type_Procedure_Organisme` (`id_type_procedure`, `organisme`, `libelle_type_procedure`, `abbreviation`, `type_boamp`, `id_type_procedure_portail`, `categorie_procedure`, `delai_alerte`, `id_type_validation`, `service_validation`, `mapa`, `activer_mapa`, `libelle_type_procedure_fr`, `libelle_type_procedure_en`, `libelle_type_procedure_es`, `libelle_type_procedure_su`, `libelle_type_procedure_du`, `libelle_type_procedure_cz`, `libelle_type_procedure_ar`, `id_montant_mapa`, `code_recensement`, `depouillable_phase_consultation`, `consultation_transverse`, `tag_Boamp`, `ao`, `mn`, `dc`, `autre`, `sad`, `accord_cadre`, `pn`, `tag_name_mesure_avancement`, `abreviation_interface`, `libelle_type_procedure_it`, `publicite_types_form_xml`, `tag_name_chorus`, `equivalent_opoce`, `equivalent_boamp`, `ordre_affichage`, `id_externe`) VALUES (1,'a1a','Appel d\'offres ouvert','AOO',1,1,1,0,2,0,'0','1','Appel d\'offres ouvert','Open procedure','','','','','',0,'01','0','0','<AppelOuvert/>','1','0','0','0','0','0','0','1','AOO','Procedura apperta','','1','OPEN','',0,''),(1,'a1t','Appel d\'offres ouvert','AOO',1,1,1,0,2,0,'0','1','Appel d\'offres ouvert','Open procedure','','','','','',0,'01','0','0','<AppelOuvert/>','1','0','0','0','0','0','0','1','AOO','Procedura apperta','','1','OPEN','',0,''),(1,'a2z','Appel d\'offres ouvert','AOO',1,1,1,0,2,0,'0','1','Appel d\'offres ouvert','Open procedure','','','','','',0,'01','0','0','<AppelOuvert/>','1','0','0','0','0','0','0','1','AOO','Procedura apperta','','1','OPEN','',0,''),(1,'e3r','Appel d\'offres ouvert','AOO',1,1,1,0,2,0,'0','1','Appel d\'offres ouvert','Open procedure','','','','','',0,'01','0','0','<AppelOuvert/>','1','0','0','0','0','0','0','1','AOO','Procedura apperta','','1','OPEN','',0,''),(2,'a1a','Appel d\'offres restreint - Candidature','AOR-C',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Candidature','Restricted procedure - preselection','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARC','Procedura ristretta - qualificazione','','10','RESTRICTED','',0,''),(2,'a1t','Appel d\'offres restreint - Candidature','AOR-C',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Candidature','Restricted procedure - preselection','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARC','Procedura ristretta - qualificazione','','10','RESTRICTED','',0,''),(2,'a2z','Appel d\'offres restreint - Candidature','AOR-C',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Candidature','Restricted procedure - preselection','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARC','Procedura ristretta - qualificazione','','10','RESTRICTED','',0,''),(2,'e3r','Appel d\'offres restreint - Candidature','AOR-C',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Candidature','Restricted procedure - preselection','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARC','Procedura ristretta - qualificazione','','10','RESTRICTED','',0,''),(3,'a1a','Appel d\'offres restreint - Offre','AOR-O',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Offre','Restricted procedure - bid','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARO','Procedura ristretta - offerta','','2','RESTRICTED','',0,''),(3,'a1t','Appel d\'offres restreint - Offre','AOR-O',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Offre','Restricted procedure - bid','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARO','Procedura ristretta - offerta','','2','RESTRICTED','',0,''),(3,'a2z','Appel d\'offres restreint - Offre','AOR-O',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Offre','Restricted procedure - bid','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARO','Procedura ristretta - offerta','','2','RESTRICTED','',0,''),(3,'e3r','Appel d\'offres restreint - Offre','AOR-O',2,2,2,0,2,0,'0','1','Appel d\'offres restreint - Offre','Restricted procedure - bid','','','','','',0,'02','0','0','<AppelRestreint/>','1','0','0','0','0','0','0','2','ARO','Procedura ristretta - offerta','','2','RESTRICTED','',0,''),(4,'a1a','Procédure adaptée < 90 k EUR HT','PA-INF',23,3,3,0,2,0,'1','1','Procédure adaptée < 90 k EUR HT','Procedure below thresholds < 90 k EUR','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAI','Procedura sotto soglia < 90 k EUR','','9','','',0,''),(4,'a1t','Procédure adaptée < 90 k EUR HT','PA-INF',23,3,3,0,2,0,'1','1','Procédure adaptée < 90 k EUR HT','Procedure below thresholds < 90 k EUR','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAI','Procedura sotto soglia < 90 k EUR','','9','','',0,''),(4,'a2z','Procédure adaptée < 90 k EUR HT','PA-INF',23,3,3,0,2,0,'1','1','Procédure adaptée < 90 k EUR HT','Procedure below thresholds < 90 k EUR','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAI','Procedura sotto soglia < 90 k EUR','','9','','',0,''),(4,'e3r','Procédure adaptée < 90 k EUR HT','PA-INF',23,3,3,0,2,0,'1','1','Procédure adaptée < 90 k EUR HT','Procedure below thresholds < 90 k EUR','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAI','Procedura sotto soglia < 90 k EUR','','9','','',0,''),(5,'a1a','Procédure adaptée >  90 k EUR HT','PA-SUP',23,3,3,0,2,0,'1','1','Procédure adaptée >  90 k EUR HT','Procedure below thresholds >  90 k EUR','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAS','Procedura sotto soglia >  90 k EUR','','9','','',0,''),(5,'a1t','Procédure adaptée >  90 k EUR HT','PA-SUP',23,3,3,0,2,0,'1','1','Procédure adaptée >  90 k EUR HT','Procedure below thresholds >  90 k EUR','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAS','Procedura sotto soglia >  90 k EUR','','9','','',0,''),(5,'a2z','Procédure adaptée >  90 k EUR HT','PA-SUP',23,3,3,0,2,0,'1','1','Procédure adaptée >  90 k EUR HT','Procedure below thresholds >  90 k EUR','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAS','Procedura sotto soglia >  90 k EUR','','9','','',0,''),(5,'e3r','Procédure adaptée >  90 k EUR HT','PA-SUP',23,3,3,0,2,0,'1','1','Procédure adaptée >  90 k EUR HT','Procedure below thresholds >  90 k EUR','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','MAS','Procedura sotto soglia >  90 k EUR','','9','','',0,''),(6,'a1a','Procédure adaptée spécifique article 30 < 90 k EUR HT','PA-A30-INF',23,3,3,0,2,0,'1','1','','','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSI','','','9','','',0,''),(6,'a1t','Procédure adaptée spécifique article 30 < 90 k EUR HT','PA-A30-INF',23,3,3,0,2,0,'1','1','','','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSI','','','9','','',0,''),(6,'a2z','Procédure adaptée spécifique article 30 < 90 k EUR HT','PA-A30-INF',23,3,3,0,2,0,'1','1','','','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSI','','','9','','',0,''),(6,'e3r','Procédure adaptée spécifique article 30 < 90 k EUR HT','PA-A30-INF',23,3,3,0,2,0,'1','1','','','','','','','',1,'09','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSI','','','9','','',0,''),(7,'a1a','Procédure adaptée spécifique article 30 >  90 k EUR HT','PA-A30-SUP',23,3,0,0,2,0,'1','1','','','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSS','','','9','','',0,''),(7,'a1t','Procédure adaptée spécifique article 30 >  90 k EUR HT','PA-A30-SUP',23,3,0,0,2,0,'1','1','','','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSS','','','9','','',0,''),(7,'a2z','Procédure adaptée spécifique article 30 >  90 k EUR HT','PA-A30-SUP',23,3,0,0,2,0,'1','1','','','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSS','','','9','','',0,''),(7,'e3r','Procédure adaptée spécifique article 30 >  90 k EUR HT','PA-A30-SUP',23,3,0,0,2,0,'1','1','','','','','','','',2,'','0','0','<ProcedureAdaptee/>','0','0','0','0','0','0','0','4','PSS','','','9','','',0,''),(8,'a1a','Concours ouvert','CO',7,4,4,0,2,0,'0','1','Concours ouvert','Open design contest','','','','','',0,'07','0','0','<ConcoursOuvert/>','0','0','0','1','0','0','0','6','COO','Concorso apperto','','7','OPEN','',0,''),(8,'a1t','Concours ouvert','CO',7,4,4,0,2,0,'0','1','Concours ouvert','Open design contest','','','','','',0,'07','0','0','<ConcoursOuvert/>','0','0','0','1','0','0','0','6','COO','Concorso apperto','','7','OPEN','',0,''),(8,'a2z','Concours ouvert','CO',7,4,4,0,2,0,'0','1','Concours ouvert','Open design contest','','','','','',0,'07','0','0','<ConcoursOuvert/>','0','0','0','1','0','0','0','6','COO','Concorso apperto','','7','OPEN','',0,''),(8,'e3r','Concours ouvert','CO',7,4,4,0,2,0,'0','1','Concours ouvert','Open design contest','','','','','',0,'07','0','0','<ConcoursOuvert/>','0','0','0','1','0','0','0','6','COO','Concorso apperto','','7','OPEN','',0,''),(9,'a1a','Concours restreint - Candidature','CR-C',8,5,4,0,2,0,'0','1','Concours restreint - Candidature','Restricted design contest - preselection','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRC','Concorso ristretto - qualificazione','','10','RESTRICTED','',0,''),(9,'a1t','Concours restreint - Candidature','CR-C',8,5,4,0,2,0,'0','1','Concours restreint - Candidature','Restricted design contest - preselection','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRC','Concorso ristretto - qualificazione','','10','RESTRICTED','',0,''),(9,'a2z','Concours restreint - Candidature','CR-C',8,5,4,0,2,0,'0','1','Concours restreint - Candidature','Restricted design contest - preselection','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRC','Concorso ristretto - qualificazione','','10','RESTRICTED','',0,''),(9,'e3r','Concours restreint - Candidature','CR-C',8,5,4,0,2,0,'0','1','Concours restreint - Candidature','Restricted design contest - preselection','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRC','Concorso ristretto - qualificazione','','10','RESTRICTED','',0,''),(10,'a1a','Concours restreint - Offre','CR-O',8,5,4,0,2,0,'0','1','Concours restreint - Offre','Restricted design contest - bid','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRO','Concorso ristretto - offerta','','7','RESTRICTED','',0,''),(10,'a1t','Concours restreint - Offre','CR-O',8,5,4,0,2,0,'0','1','Concours restreint - Offre','Restricted design contest - bid','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRO','Concorso ristretto - offerta','','7','RESTRICTED','',0,''),(10,'a2z','Concours restreint - Offre','CR-O',8,5,4,0,2,0,'0','1','Concours restreint - Offre','Restricted design contest - bid','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRO','Concorso ristretto - offerta','','7','RESTRICTED','',0,''),(10,'e3r','Concours restreint - Offre','CR-O',8,5,4,0,2,0,'0','1','Concours restreint - Offre','Restricted design contest - bid','','','','','',0,'07','0','0','<ConcoursRestreint/>','0','0','0','1','0','0','0','6','CRO','Concorso ristretto - offerta','','7','RESTRICTED','',0,''),(11,'a1a','Dialogue compétitif - Candidature','DC-C',22,7,4,0,2,0,'0','1','Dialogue compétitif - Candidature','Competitive dialogue - preselection','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCC','Dialogo competitivo - qualificazione','','10','COMPETITIVE_DIALOGUE','',0,''),(11,'a1t','Dialogue compétitif - Candidature','DC-C',22,7,4,0,2,0,'0','1','Dialogue compétitif - Candidature','Competitive dialogue - preselection','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCC','Dialogo competitivo - qualificazione','','10','COMPETITIVE_DIALOGUE','',0,''),(11,'a2z','Dialogue compétitif - Candidature','DC-C',22,7,4,0,2,0,'0','1','Dialogue compétitif - Candidature','Competitive dialogue - preselection','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCC','Dialogo competitivo - qualificazione','','10','COMPETITIVE_DIALOGUE','',0,''),(11,'e3r','Dialogue compétitif - Candidature','DC-C',22,7,4,0,2,0,'0','1','Dialogue compétitif - Candidature','Competitive dialogue - preselection','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCC','Dialogo competitivo - qualificazione','','10','COMPETITIVE_DIALOGUE','',0,''),(12,'a1a','Dialogue compétitif - Offre','DC-O',22,7,4,0,2,0,'0','1','Dialogue compétitif - Offre','Competitive dialogue - bid','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCO','Dialogo competitivo - offerta','','6','COMPETITIVE_DIALOGUE','',0,''),(12,'a1t','Dialogue compétitif - Offre','DC-O',22,7,4,0,2,0,'0','1','Dialogue compétitif - Offre','Competitive dialogue - bid','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCO','Dialogo competitivo - offerta','','6','COMPETITIVE_DIALOGUE','',0,''),(12,'a2z','Dialogue compétitif - Offre','DC-O',22,7,4,0,2,0,'0','1','Dialogue compétitif - Offre','Competitive dialogue - bid','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCO','Dialogo competitivo - offerta','','6','COMPETITIVE_DIALOGUE','',0,''),(12,'e3r','Dialogue compétitif - Offre','DC-O',22,7,4,0,2,0,'0','1','Dialogue compétitif - Offre','Competitive dialogue - bid','','','','','',0,'06','0','0','<DialogueCompetitif/>','0','0','1','0','0','0','0','3','DCO','Dialogo competitivo - offerta','','6','COMPETITIVE_DIALOGUE','',0,''),(17,'a1a','Procédure Autre','AUT',24,15,4,0,2,0,'0','1','Procédure Autre','Other procedure','','','','','',0,'10','0','0','<Autres/>','0','0','0','1','0','0','0','','ATR','Altra procedura','','10','','',0,''),(17,'a1t','Procédure Autre','AUT',24,15,4,0,2,0,'0','1','Procédure Autre','Other procedure','','','','','',0,'10','0','0','<Autres/>','0','0','0','1','0','0','0','','ATR','Altra procedura','','10','','',0,''),(17,'a2z','Procédure Autre','AUT',24,15,4,0,2,0,'0','1','Procédure Autre','Other procedure','','','','','',0,'10','0','0','<Autres/>','0','0','0','1','0','0','0','','ATR','Altra procedura','','10','','',0,''),(17,'e3r','Procédure Autre','AUT',24,15,4,0,2,0,'0','1','Procédure Autre','Other procedure','','','','','',0,'10','0','0','<Autres/>','0','0','0','1','0','0','0','','ATR','Altra procedura','','10','','',0,''),(18,'a1a','Marché négocié','MN',18,8,4,0,2,0,'0','1','Marché négocié','Negociated procedure','','','','','',0,'10','0','0','<MarcheNegocie/>','0','1','0','0','0','0','0','5','MNE','Procedura negoziata','','4','NEGOTIATED','',0,''),(18,'a1t','Marché négocié','MN',18,8,4,0,2,0,'0','1','Marché négocié','Negociated procedure','','','','','',0,'10','0','0','<MarcheNegocie/>','0','1','0','0','0','0','0','5','MNE','Procedura negoziata','','4','NEGOTIATED','',0,''),(18,'a2z','Marché négocié','MN',18,8,4,0,2,0,'0','1','Marché négocié','Negociated procedure','','','','','',0,'10','0','0','<MarcheNegocie/>','0','1','0','0','0','0','0','5','MNE','Procedura negoziata','','4','NEGOTIATED','',0,''),(18,'e3r','Marché négocié','MN',18,8,4,0,2,0,'0','1','Marché négocié','Negociated procedure','','','','','',0,'10','0','0','<MarcheNegocie/>','0','1','0','0','0','0','0','5','MNE','Procedura negoziata','','4','NEGOTIATED','',0,''),(30,'a1a','Accord-Cadre - 1 Sélection des Attributaires','AC-SA',1,30,1,0,2,0,'0','1','Accord-Cadre - 1 Sélection des Attributaires','Framework agreement - 1 Selection of suppliers','','','','','',0,'10','0','1','<Autres/>','0','0','0','0','0','1','0','8','ACA','Accordo quadro - 1 Qualificazione dei titolare','','10','','',0,''),(30,'a1t','Accord-Cadre - 1 Sélection des Attributaires','AC-SA',1,30,1,0,2,0,'0','1','Accord-Cadre - 1 Sélection des Attributaires','Framework agreement - 1 Selection of suppliers','','','','','',0,'10','0','1','<Autres/>','0','0','0','0','0','1','0','8','ACA','Accordo quadro - 1 Qualificazione dei titolare','','10','','',0,''),(30,'a2z','Accord-Cadre - 1 Sélection des Attributaires','AC-SA',1,30,1,0,2,0,'0','1','Accord-Cadre - 1 Sélection des Attributaires','Framework agreement - 1 Selection of suppliers','','','','','',0,'10','0','1','<Autres/>','0','0','0','0','0','1','0','8','ACA','Accordo quadro - 1 Qualificazione dei titolare','','10','','',0,''),(30,'e3r','Accord-Cadre - 1 Sélection des Attributaires','AC-SA',1,30,1,0,2,0,'0','1','Accord-Cadre - 1 Sélection des Attributaires','Framework agreement - 1 Selection of suppliers','','','','','',0,'10','0','1','<Autres/>','0','0','0','0','0','1','0','8','ACA','Accordo quadro - 1 Qualificazione dei titolare','','10','','',0,''),(31,'a1a','Accord-Cadre - 2 Marché Subséquent','AC-MS',2,31,2,0,2,0,'0','1','Accord-Cadre - 2 Marché Subséquent','Framework agreement - 2 Individual contract','','','','','',0,'10','0','0','<Autres/>','0','0','0','0','0','1','0','10','ACS','Accordo quadro - 2 Apalto','','10','','',0,''),(31,'a1t','Accord-Cadre - 2 Marché Subséquent','AC-MS',2,31,2,0,2,0,'0','1','Accord-Cadre - 2 Marché Subséquent','Framework agreement - 2 Individual contract','','','','','',0,'10','0','0','<Autres/>','0','0','0','0','0','1','0','10','ACS','Accordo quadro - 2 Apalto','','10','','',0,''),(31,'a2z','Accord-Cadre - 2 Marché Subséquent','AC-MS',2,31,2,0,2,0,'0','1','Accord-Cadre - 2 Marché Subséquent','Framework agreement - 2 Individual contract','','','','','',0,'10','0','0','<Autres/>','0','0','0','0','0','1','0','10','ACS','Accordo quadro - 2 Apalto','','10','','',0,''),(31,'e3r','Accord-Cadre - 2 Marché Subséquent','AC-MS',2,31,2,0,2,0,'0','1','Accord-Cadre - 2 Marché Subséquent','Framework agreement - 2 Individual contract','','','','','',0,'10','0','0','<Autres/>','0','0','0','0','0','1','0','10','ACS','Accordo quadro - 2 Apalto','','10','','',0,''),(32,'a1a','Système d\'Acquisition Dynamique - Admission','SAD-A',8,32,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Admission','Dynamic purchasing system - Admission','','','','','',0,'08','1','1','<Autres/>','0','0','0','0','1','0','0','9','SAA','Sistema dinamico di acquisizione - Ammissione','','10','','',0,''),(32,'a1t','Système d\'Acquisition Dynamique - Admission','SAD-A',8,32,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Admission','Dynamic purchasing system - Admission','','','','','',0,'08','1','1','<Autres/>','0','0','0','0','1','0','0','9','SAA','Sistema dinamico di acquisizione - Ammissione','','10','','',0,''),(32,'a2z','Système d\'Acquisition Dynamique - Admission','SAD-A',8,32,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Admission','Dynamic purchasing system - Admission','','','','','',0,'08','1','1','<Autres/>','0','0','0','0','1','0','0','9','SAA','Sistema dinamico di acquisizione - Ammissione','','10','','',0,''),(32,'e3r','Système d\'Acquisition Dynamique - Admission','SAD-A',8,32,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Admission','Dynamic purchasing system - Admission','','','','','',0,'08','1','1','<Autres/>','0','0','0','0','1','0','0','9','SAA','Sistema dinamico di acquisizione - Ammissione','','10','','',0,''),(33,'a1a','Système d\'Acquisition Dynamique - Marché Spécifique','SAD-MS',8,33,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract','','','','','',0,'08','0','0','<Autres/>','0','0','0','0','1','0','0','11','SAM','Sistema dinamico di acquisizione - Appalto','','8','','',0,''),(33,'a1t','Système d\'Acquisition Dynamique - Marché Spécifique','SAD-MS',8,33,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract','','','','','',0,'08','0','0','<Autres/>','0','0','0','0','1','0','0','11','SAM','Sistema dinamico di acquisizione - Appalto','','8','','',0,''),(33,'a2z','Système d\'Acquisition Dynamique - Marché Spécifique','SAD-MS',8,33,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract','','','','','',0,'08','0','0','<Autres/>','0','0','0','0','1','0','0','11','SAM','Sistema dinamico di acquisizione - Appalto','','8','','',0,''),(33,'e3r','Système d\'Acquisition Dynamique - Marché Spécifique','SAD-MS',8,33,4,0,2,0,'0','1','Système d\'Acquisition Dynamique - Marché Spécifique','Dynamicpurchasing system - Specific contract','','','','','',0,'08','0','0','<Autres/>','0','0','0','0','1','0','0','11','SAM','Sistema dinamico di acquisizione - Appalto','','8','','',0,''),(1000,'a1a','Procédure concurrentielle avec négociation - Candidature','PCN-C',1,1000,4,0,2,0,'0','1','','','','','','',NULL,0,NULL,'0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1000,'a1t','Procédure concurrentielle avec négociation - Candidature','PCN-C',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1000,'a2z','Procédure concurrentielle avec négociation - Candidature','PCN-C',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1000,'e3r','Procédure concurrentielle avec négociation - Candidature','PCN-C',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1004,'a1a','Procédure concurrentielle avec négociation - Offre','PCN-O',1,1000,4,0,2,0,'0','1','','','','','','',NULL,0,NULL,'0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1004,'a1t','Procédure concurrentielle avec négociation - Offre','PCN-O',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1004,'a2z','Procédure concurrentielle avec négociation - Offre','PCN-O',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,''),(1004,'e3r','Procédure concurrentielle avec négociation - Offre','PCN-O',1,1000,4,0,2,0,'0','1','','','','','','','',0,'','0','0','','1','0','0','0','0','1','0','','PCN','','','1','','',0,'');
/*!40000 ALTER TABLE `Type_Procedure_Organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Type_support`
--

LOCK TABLES `Type_support` WRITE;
/*!40000 ALTER TABLE `Type_support` DISABLE KEYS */;
/*!40000 ALTER TABLE `Type_support` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ValeurReferentiel`
--

LOCK TABLES `ValeurReferentiel` WRITE;
/*!40000 ALTER TABLE `ValeurReferentiel` DISABLE KEYS */;
INSERT INTO `ValeurReferentiel` (`id`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_2`, `libelle_valeur_referentiel_it`, `valeur_sub`, `ordre_affichage`) VALUES (1,13,'Mandataire de groupement','Mandataire de groupement','','','','','','','','','',0),(1,16,'Solidaire','Solidaire','','','','','','','SOLIDAIRE','','',0),(1,18,'Durée ferme','Durée ferme','','','','','','','DUREE_MARCHEE','','',0),(1,19,'Critères énoncés ci après (par ordre de priorité décroissante)','Critères énoncés ci après (par ordre de priorité décroissante)','','','','','','','layerDefinitionCriteres_1','','',4),(1,20,'CCAG-FCS','CCAG-FCS','','','','','','','','','',0),(1,22,'Prix révisables','Prix révisables','','','','','','','PRIX_REVISABLES','','',0),(1,23,'Prix sur catalogues','Prix sur catalogues','','','','','','','PRIX_CATALOGUE','','',0),(1,24,'EUR HT','EUR HT','','','','','','','','','',0),(1,25,'Le ou les tiers fournisseurs suivants n\'existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants','Le ou les tiers fournisseurs suivants n\'existent pas dans CHORUS.Veuillez contacter votre CSP pour demander la création dans CHORUS du ou des tiers fournisseurs suivants','','','','','','','TIER_N_EXISTE_PAS','','',0),(1,26,'Non renseigné','Non renseigné','','','','','','','','','',0),(1,27,'OPTION-A','OPTION-A','','','','','','','','','',0),(1,157,'Ouverture','Ouverture','','','','','','','','','',0),(1,161,'Consultations','Consultations','','','','','','','0;1;2;24;25;9;18;26','','',0),(1,184,'M.','M.','','','','','','','','','',0),(1,185,'En attente d\'édition','En attente d\'édition','','','','','','','','','',0),(1,486,'Confidentiel','Confidentiel','','','','','','','TEXTE_JUSTIFICATION_1','','',0),(1,487,'Marché confidentiel','Marché confidentiel','','','','','','','TEXTE_JUSTIFICATION_MARCHE_NON_PUBLIE_1','','',0),(1,488,'Acte Spécial (hors sous-traitance)','Acte Spécial (hors sous-traitance)','','','','','','','','','',0),(2,13,'Co-traitant solidaire','Co-traitant solidaire','','','','','','','','','',0),(2,16,'Conjoint','Conjoint','','','','','','','CONJOINT','','',0),(2,19,'Critères énoncés ci-après avec leur pondération','Critères énoncés ci-après avec leur pondération','','','','','','','layerDefinitionCriteres_2','','',3),(2,20,'CCAG-MI','CCAG-MI','','','','','','','','','',0),(2,22,'Prix actualisables','Prix actualisables','','','','','','','PRIX_ACTUALISABLES','','',0),(2,23,'Bordereau de prix','Bordereau de prix','','','','','','','BORDEREAU_PRIX','','',0),(2,24,'EUR TTC','EUR TTC','','','','','','','','','',0),(2,25,'Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l\'organisation d\'achat sélectionnée. Veuillez contacter votre CSP pour demander l\'extension dans CHORUS du ou des tiers fournisseurs suivants','Le ou les tiers fournisseurs suivants existent dans CHORUS mais ne sont pas étendus sur l\'organisation d\'achat sélectionnée. Veuillez contacter votre CSP pour demander l\'extension dans CHORUS du ou des tiers fournisseurs suivants','','','','','','','TIER_NON_ETENDU','','',0),(2,26,'Unités non employeuses (pas de salarié au cours de l\'année de référence et pas d\'effectif au 31/12).','Unités non employeuses (pas de salarié au cours de l\'année de référence et pas d\'effectif au 31/12).','','','','','','','NN','','',0),(2,27,'OPTION-B','OPTION-B','','','','','','','','','',0),(2,157,'Sélection des candidatures','Sélection des candidatures','','','','','','','','','',0),(2,161,'DCE','DCE','','','','','','','0;5;17;6;7;8;23','','',0),(2,184,'Mme','Mme','','','','','','','','','',0),(2,185,'Envoyé','Envoyé','','','','','','','','','',0),(2,486,'Marché dont le montant peut trahir le secret industriel ou professionnel','Marché dont le montant peut trahir le secret industriel ou professionnel','','','','','','','TEXTE_JUSTIFICATION_2','','',0),(2,487,'Marché inférieur à 25 000 EUR','Marché inférieur à 25 000 EUR','','','','','','','TEXTE_JUSTIFICATION_MARCHE_NON_PUBLIE_2','','',0),(2,488,'Acte Spécial sous-traitance','Acte Spécial sous-traitance','','','','','','','','','',0),(3,13,'Co-traitant conjoint','Co-traitant conjoint','','','','','','','','','',0),(3,16,'Au choix','Au choix','','','','','','','AU_CHOIX','','',0),(3,18,'Description libre de la durée du marché','Description libre de la durée du marché','','','','','','','DESCRIPTION_LIBRE','','',0),(3,19,'Critère unique de prix','Critère unique de prix','','','','','','','layerDefinitionVide','','',2),(3,20,'CCAG-Tx','CCAG-Tx','','','','','','','','','',0),(3,22,'Prix fermes','Prix fermes','','','','','','','PRIX_FERMES','','',0),(3,23,'Autre','Autre','','','','','','','AUTRE','','',0),(3,24,'Dossier(s)','Dossier(s)','','','','','','','','','',0),(3,25,'Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l\'organisation d\'achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants','Le ou les tiers fournisseurs suivants existent dans CHORUS mais sont bloqués sur l\'organisation d\'achat sélectionnée. Veuillez contacter votre CSP pour demander le déblocage dans CHORUS du ou des tiers fournisseurs suivants','','','','','','','TIER_BLOQUE','','',0),(3,26,'0 salarié (n\'ayant pas d\'effectif au 31/12 mais ayant employé des salariés au cours de l\'année de référence)','0 salarié (n\'ayant pas d\'effectif au 31/12 mais ayant employé des salariés au cours de l\'année de référence)','','','','','','','00','','',0),(3,27,'OPTION-DEROG','OPTION-DEROG','','','','','','','','','',0),(3,157,'Présentation de DCE','Présentation de DCE','','','','','','','','','',0),(3,161,'Publicités','Publicités','','','','','','','0;3;4','','',0),(3,185,'A archiver','A archiver','','','','','','','','','',0),(3,486,'Marché ayant attrait à la Défense','Marché ayant attrait à la Défense','','','','','','','AUTRE','','',0),(3,487,'Marché dont le montant peut trahir le secret industriel ou professionnel','Marché dont le montant peut trahir le secret industriel ou professionnel','','','','','','','TEXTE_JUSTIFICATION_MARCHE_NON_PUBLIE_3','','',0),(3,488,'Avenant sans incidence financière','Avenant sans incidence financière','','','','','','','','','',0),(4,13,'Sous-traitant','Sous-traitant','','','','','','','','','',0),(4,19,'Critères énoncés dans les documents de la consultation','Critères énoncés dans les documents de la consultation','','','','','','','layerDefinitionVide_2','','',5),(4,20,'CCAG-PI','CCAG-PI','','','','','','','','','',0),(4,24,'Jour(s)','Jour(s)','','','','','','','','','',0),(4,25,'Le ou les tiers fournisseurs suivants n\'ont pu être vérifiés dans CHORUS. Veuillez contacter l\'assistance téléphonique de PLACE afin d\'identifier la nature du problème','Le ou les tiers fournisseurs suivants n\'ont pu être vérifiés dans CHORUS. Veuillez contacter l\'assistance téléphonique de PLACE afin d\'identifier la nature du problème','','','','','','','TIER_ERRONE','','',0),(4,26,'1 ou 2 salariés','1 ou 2 salariés','','','','','','','01','','',0),(4,157,'Attribution','Attribution','','','','','','','','','',0),(4,161,'Réponses électroniques','Réponses électroniques','','','','','','','0;10;11;12;19;20;21','','',0),(4,185,'Archivé','Archivé','','','','','','','','','',0),(4,486,'Marché inférieur à 25 000 EUR','Marché inférieur à 25 000 EUR','','','','','','','TEXTE_JUSTIFICATION_4','','',0),(4,487,'Marché ayant attrait à la Défense','Marché ayant attrait à la Défense','','','','','','','TEXTE_JUSTIFICATION_MARCHE_NON_PUBLIE_4','','',0),(4,488,'Avenant augmentant le montant initial','Avenant augmentant le montant initial','','','','','','','','','',0),(5,19,'Critère unique de coût','Critère unique de coût','','','','','','','layerDefinitionVide_3','','',1),(5,20,'CCAG-TIC','CCAG-TIC','','','','','','','','','',0),(5,26,'3 à 5 salariés','3 à 5 salariés','','','','','','','02','','',0),(5,157,'Avenant','Avenant','','','','','','','','','',0),(5,161,'Contrats','Contrats','','','','','','','27;28;29;30;31','','',0),(5,488,'Avenant diminuant le montant initial','Avenant diminuant le montant initial','','','','','','','','','',0),(6,20,'CAC','CAC','','','','','','','','','',0),(6,26,'6 à 9 salariés','6 à 9 salariés','','','','','','','03','','',0),(6,157,'Avis','Avis','','','','','','','','','',0),(6,161,'Interface PLACE-CHORUS','Interface PLACE-CHORUS','','','','','','','31;32;33;34;35','','',0),(6,488,'Clôture','Clôture','','','','','','','','','',0),(7,20,'Pas de CCAG de référence','Pas de CCAG de référence','','','','','','','','','',0),(7,26,'10 à 19 salariés','10 à 19 salariés','','','','','','','11','','',0),(7,157,'Négociation','Négociation','','','','','','','','','',0),(7,488,'Décision d\'affermissement','Décision d\'affermissement','','','','','','','','','',0),(8,26,'20 à 49 salariés','20 à 49 salariés','','','','','','','12','','',0),(8,157,'Autre','Autre','','','','','','','','','',0),(8,488,'Décision de poursuivre','Décision de poursuivre','','','','','','','','','',0),(9,26,'50 à 99 salariés','50 à 99 salariés','','','','','','','21','','',0),(9,488,'Décision de prolongation de délai','Décision de prolongation de délai','','','','','','','','','',0),(10,26,'100 à 199 salariés','100 à 199 salariés','','','','','','','22','','',0),(10,488,'Décision de reconduction','Décision de reconduction','','','','','','','','','',0),(11,26,'200 à 249 salariés','200 à 249 salariés','','','','','','','31','','',0),(11,488,'Décision de résiliation','Décision de résiliation','','','','','','','','','',0),(12,26,'250 à 499 salariés','250 à 499 salariés','','','','','','','32','','',0),(12,488,'Décision de sursis d\'exécution','Décision de sursis d\'exécution','','','','','','','','','',0),(13,26,'500 à 999 salariés','500 à 999 salariés','','','','','','','41','','',0),(13,488,'Réouverture','Réouverture','','','','','','','','','',0),(14,26,'1 000 à 1 999 salariés','1 000 à 1 999 salariés','','','','','','','42','','',0),(14,488,'Révision de prix','Révision de prix','','','','','','','','','',0),(15,26,'2 000 à 4 999 salariés','2 000 à 4 999 salariés','','','','','','','51','','',0),(15,488,'Annulation de retenue de garantie','Annulation de retenue de garantie','','','','','','','','','',0),(16,26,'5 000 à 9 999 salariés','5 000 à 9 999 salariés','','','','','','','52','','',0),(16,488,'Autre','Autre','','','','','','','','','',0),(17,26,'10 000 salariés et plus','10 000 salariés et plus','','','','','','','53','','',0),(17,488,'Changement de dénomination sociale','Changement de dénomination sociale','','','','','','','','','',0),(30,4,'à l\'appel d\'offres avec présélection','','','','','','','','01','','',0),(33,5,'au concours','','','','','','','','01','','',0),(34,5,'dossier du concours','','','','','','','','02','','',0),(35,5,'des articles 51 et 53','','','','','','','','03','','',0),(36,6,'Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales','','','','','','','','','','',0),(37,6,'Agence/office national(e) ou fédéral(e)','','','','','','','','','','',0),(38,6,'Collectivité territoriale','','','','','','','','','','',0),(39,6,'Agence/office régional(e) ou local(e)','','','','','','','','','','',0),(40,6,'Organisme de droit public','','','','','','','','','','',0),(41,6,'Institution/agence européenne ou organisation européenne','','','','','','','','','','',0),(42,6,'Autre','','','','','','','','','','',0),(43,7,'Services généraux des administrations publiques','','','','','','','','<servicesGenerauxAdmPub/>','','',0),(44,7,'Défense','','','','','','','','<defense/>','','',0),(45,7,'Ordre et sécurité publique\r\n','','','','','','','','<ordreSecuritePub/>','','',0),(46,7,'Environnement\r\n','','','','','','','','<environnement/>','','',0),(47,7,'Affaires économiques et financières','','','','','','','','<affairesEcoFi/>','','',0),(48,7,'Santé','','','','','','','','<sante/>','','',0),(49,8,'Production, transport et distribution de gaz ou de chaleur','','','','','','','','<gazChaleur/>','','',0),(50,8,'Electricités','','','','','','','','<electricites/>','','',0),(51,8,'Prospection et extraction de pétrole et de gaz\r\n','','','','','','','','<gazPetrole/>','','',0),(52,8,'Prospection et extraction de charbon ou autres combustibles\r\n','','','','','','','','<charbon/>','','',0),(53,8,'Eau','','','','','','','','<eau/>','','',0),(54,8,'Services postaux','','','','','','','','<servicesPostaux/>','','',0),(55,8,'Services de chemin de fer','','','','','','','','<servicesCheminFer/>','','',0),(56,8,'Services de chemin de fer urbains, de tramway ou d\'autobus\r\n','','','','','','','','<servicesCheminFerUrbains/>','','',0),(57,8,'Activités portuaires\r\n','','','','','','','','<activitesPortuaires/>','','',0),(58,8,'Activités aéroportuaires','','','','','','','','<activitesAeroportuaires/>','','',0),(59,6,'Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales','','','','','','','','','','',0),(60,6,'Agence/office national(e) ou fédéral(e)','','','','','','','','','','',0),(61,6,'Collectivité territoriale','','','','','','','','','','',0),(62,6,'Agence/office régional(e) ou local(e)','','','','','','','','','','',0),(63,6,'Organisme de droit public','','','','','','','','','','',0),(64,6,'Institution/agence européenne ou organisation européenne','','','','','','','','','','',0),(65,6,'Autre','','','','','','','','','','',0),(66,7,'Services généraux des administrations publiques','','','','','','','','<servicesGenerauxAdmPub/>','','',0),(67,7,'Défense','','','','','','','','<defense/>','','',0),(68,7,'Ordre et sécurité publique\r\n','','','','','','','','<ordreSecuritePub/>','','',0),(69,7,'Environnement\r\n','','','','','','','','<environnement/>','','',0),(70,7,'Affaires économiques et financières','','','','','','','','<affairesEcoFi/>','','',0),(71,7,'Santé','','','','','','','','<sante/>','','',0),(72,8,'Production, transport et distribution de gaz ou de chaleur','','','','','','','','<gazChaleur/>','','',0),(73,8,'Electricités','','','','','','','','<electricites/>','','',0),(74,8,'Prospection et extraction de pétrole et de gaz\r\n','','','','','','','','<gazPetrole/>','','',0),(75,8,'Prospection et extraction de charbon ou autres combustibles\r\n','','','','','','','','<charbon/>','','',0),(76,8,'Eau','','','','','','','','<eau/>','','',0),(77,8,'Services postaux','','','','','','','','<servicesPostaux/>','','',0),(78,8,'Services de chemin de fer','','','','','','','','<servicesCheminFer/>','','',0),(79,8,'Services de chemin de fer urbains, de tramway ou d\'autobus\r\n','','','','','','','','<servicesCheminFerUrbains/>','','',0),(80,8,'Activités portuaires\r\n','','','','','','','','<activitesPortuaires/>','','',0),(81,8,'Activités aéroportuaires','','','','','','','','<activitesAeroportuaires/>','','',0),(82,6,'Ministère ou toute autre autorité nationale ou fédérale, y compris leurs subdivisions régionales ou locales','','','','','','','','','','',0),(83,6,'Agence/office national(e) ou fédéral(e)','','','','','','','','','','',0),(84,6,'Collectivité territoriale','','','','','','','','','','',0),(85,6,'Agence/office régional(e) ou local(e)','','','','','','','','','','',0),(86,6,'Organisme de droit public','','','','','','','','','','',0),(87,6,'Institution/agence européenne ou organisation européenne','','','','','','','','','','',0),(88,6,'Autre','','','','','','','','','','',0),(89,7,'Services généraux des administrations publiques','','','','','','','','<servicesGenerauxAdmPub/>','','',0),(90,7,'Défense','','','','','','','','<defense/>','','',0),(91,7,'Ordre et sécurité publique\r\n','','','','','','','','<ordreSecuritePub/>','','',0),(92,7,'Environnement\r\n','','','','','','','','<environnement/>','','',0),(93,7,'Affaires économiques et financières','','','','','','','','<affairesEcoFi/>','','',0),(94,7,'Santé','','','','','','','','<sante/>','','',0),(95,8,'Production, transport et distribution de gaz ou de chaleur','','','','','','','','<gazChaleur/>','','',0),(96,8,'Electricités','','','','','','','','<electricites/>','','',0),(97,8,'Prospection et extraction de pétrole et de gaz\r\n','','','','','','','','<gazPetrole/>','','',0),(98,8,'Prospection et extraction de charbon ou autres combustibles\r\n','','','','','','','','<charbon/>','','',0),(99,8,'Eau','','','','','','','','<eau/>','','',0),(100,8,'Services postaux','','','','','','','','<servicesPostaux/>','','',0),(101,8,'Services de chemin de fer','','','','','','','','<servicesCheminFer/>','','',0),(102,8,'Services de chemin de fer urbains, de tramway ou d\'autobus\r\n','','','','','','','','<servicesCheminFerUrbains/>','','',0),(103,8,'Activités portuaires\r\n','','','','','','','','<activitesPortuaires/>','','',0),(104,8,'Activités aéroportuaires','','','','','','','','<activitesAeroportuaires/>','','',0),(105,14,'RC - Règlement de consultation','RC - Règlement de consultation','','','','','','','','','',0),(106,14,'DC1 - Lettre de candidature','DC1 - Lettre de candidature','','','','','','','','','',0),(107,14,'DC2 - Déclaration du candidat ','DC2 - Déclaration du candidat ','','','','','','','','','',0),(108,14,'DC4 - Déclaration de sous-traitance','DC4 - Déclaration de sous-traitance','','','','','','','','','',0),(109,14,'BPU - Bordereau des prix unitaires','BPU - Bordereau des prix unitaires','','','','','','','','','',0),(110,14,'CCAP - Cahier des clauses administratives particulières','CCAP - Cahier des clauses administratives particulières','','','','','','','','','',0),(111,14,'CCTP - Cahier des clauses techniques particulières','CCTP - Cahier des clauses techniques particulières','','','','','','','','','',0),(112,14,'CCAG - Cahier des clauses administratives générales','CCAG - Cahier des clauses administratives générales','','','','','','','','','',0),(113,14,'CCTG - Cahier des clauses techniques générales','CCTG - Cahier des clauses techniques générales','','','','','','','','','',0),(114,14,'DPGF - Décomposition du prix global et forfaitaire','DPGF - Décomposition du prix global et forfaitaire','','','','','','','','','',0),(115,14,'DQE - Détail quantitatif estimatif','DQE - Détail quantitatif estimatif','','','','','','','','','',0),(116,14,'Autres pièces','Autres pièces','','','','','','','','','',0),(175,21,'En mois','En mois','','','','','','','dureeMois','','',0),(176,21,'En jours','En jours','','','','','','','dureeJours','','',0),(178,210,'Accès à la fonction de test de configuration du poste (passé avec succès)','Accès à la fonction de test de configuration du poste (passé avec succès)','','','','','','','DESCRIPTION2','','',0),(179,210,'Accès à la fonction de test de configuration du poste (en échec)','Accès à la fonction de test de configuration du poste (en échec)','','','','','','','DESCRIPTION3','','',0),(180,210,'Accès au détail de la consultation \"[__ref_utilisateur__]\"','Accès au détail de la consultation \"[__ref_utilisateur__]\"','','','','','','','DESCRIPTION4','','',0),(188,210,'Accès au formulaire de demande de téléchargement du dossier de consultation','Accès au formulaire de demande de téléchargement du dossier de consultation','','','','','','','DESCRIPTION5','','',0),(189,210,'Validation du formulaire de demande de téléchargement du dossier de consultation','Validation du formulaire de demande de téléchargement du dossier de consultation','','','','','','','DESCRIPTION6','','',0),(190,210,'Téléchargement (download) du dossier de consultation','Téléchargement (download) du dossier de consultation','','','','','','','DESCRIPTION7','','',0),(191,210,'Téléchargement (download) du règlement de la consultation','Téléchargement (download) du règlement de la consultation','','','','','','','DESCRIPTION8','','',0),(192,210,'Accès à la fonction « Poser une question » de la consultation','Accès à la fonction « Poser une question » de la consultation','','','','','','','DESCRIPTION9','','',0),(193,210,'Validation du formulaire d\'identification préalable à la question','Validation du formulaire d\'identification préalable à la question','','','','','','','DESCRIPTION10','','',0),(194,210,'Envoi de la question sur la consultation','Envoi de la question sur la consultation','','','','','','','DESCRIPTION11','','',0),(195,210,'Accès au formulaire de réponse de la consultation \"[__ref_utilisateur__]\"','Accès au formulaire de réponse de la consultation \"[__ref_utilisateur__]\"','','','','','','','DESCRIPTION12','','',0),(196,210,'Ajout (upload) d\'une pièce libre au dossier [_typeEnveloppeLot_]','Ajout (upload) d\'une pièce libre au dossier [_typeEnveloppeLot_]','','','','','','','DESCRIPTION13','','',0),(198,210,'Ajout (upload) du \"DC3 - Acte d\'engagement\" au dossier [_typeEnveloppeLot_]','Ajout (upload) du \"DC3 - Acte d\'engagement\" au dossier [_typeEnveloppeLot_]','','','','','','','DESCRIPTION14','','',0),(199,210,'Retrait (download) d\'une pièce libre du dossier [_typeEnveloppeLot_]','Retrait (download) d\'une pièce libre du dossier [_typeEnveloppeLot_]','','','','','','','DESCRIPTION15','','',0),(200,210,'Retrait (download) du \"DC3 - Acte d\'engagement\" du dossier [_typeEnveloppeLot_]','Retrait (download) du \"DC3 - Acte d\'engagement\" du dossier [_typeEnveloppeLot_]','','','','','','','DESCRIPTION16','','',0),(201,210,'Signature d\'une pièce du formulaire de réponse de la consultation','Signature d\'une pièce du formulaire de réponse de la consultation','','','','','','','DESCRIPTION17','','',0),(202,210,'Validation du formulaire de réponse de la consultation avec : ','Validation du formulaire de réponse de la consultation avec : ','','','','','','','DESCRIPTION18','','',0),(203,210,'Annulation du formulaire de réponse de la consultation','Annulation du formulaire de réponse de la consultation','','','','','','','DESCRIPTION19','','',0),(204,210,'Affichage de la page de confirmation du dépôt de la réponse et envoi d\'un courriel de confirmation à l\'utilisateur','Affichage de la page de confirmation du dépôt de la réponse et envoi d\'un courriel de confirmation à l\'utilisateur','','','','','','','DESCRIPTION20','','',0),(205,210,'Erreur lors de la signature d\'une pièce du formulaire de réponse de la consultation','Erreur lors de la signature d\'une pièce du formulaire de réponse de la consultation\r\n','','','','','','','DESCRIPTION21','','',0),(208,210,'Erreur de chargement de l\'applet cyptographique','Erreur de chargement de l\'applet cyptographique','','','','','','','DESCRIPTION22','','',0),(209,210,'Erreur lors de l\'\'ajout (upload) d\'une pièce au formulaire de réponse de la consultation','Erreur lors de l\'\'ajout (upload) d\'une pièce au formulaire de réponse de la consultation','','','','','','','DESCRIPTION23','','',0),(210,210,'Erreur technique rencontrée lors du dépôt de la réponse électronique','Erreur technique rencontrée lors du dépôt de la réponse électronique','','','','','','','DESCRIPTION24','','',0),(406,210,'Présentation à l\'utilisateur d\'une alerte lui indiquant un problème avec les signatures de ses documents','Présentation à l\'utilisateur d\'une alerte lui indiquant un problème avec les signatures de ses documents','','','','','','','DESCRIPTION25','','',0),(407,210,'Ajout (upload) d\'une pièce libre au dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','Ajout (upload) d\'une pièce libre au dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION26','','',0),(408,210,'Ajout (upload) du \"DC3 - Acte d\'engagement\" du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','Ajout (upload) du \"DC3 - Acte d\'engagement\" du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION27','','',0),(409,210,'Ajout (upload) d\'un jeton de signature au dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','Ajout (upload)d\'un jeton de signature au dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION28','','',0),(410,210,'Suppression d\'une pièce libre du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','Suppression d\'une pièce libre du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION29','','',0),(411,210,'Suppression du \"DC3 - Acte d\'engagement\" du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','Suppression du \"DC3 - Acte d\'engagement\" du dossier [_type_enveloppe_] du formulaire de réponse de la consultation : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION30','','',0),(412,210,'Suppression du jeton de signature [_fichier_signature_taille_] associé à la pièce  : [_liste_fichiers_tailles_]','Suppression du jeton de signature [_fichier_signature_taille_] associé à la pièce  : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION31','','',0),(413,210,'Validation du formulaire de réponse de la consultation avec : [_liste_fichiers_ajoutes_]','Validation du formulaire de réponse de la consultation avec : [_liste_fichiers_ajoutes_]','','','','','','','DESCRIPTION32','','',0),(414,210,'Présentation à l\'utilisateur d\'une alerte lui indiquant un problème avec les signatures de ses documents','Présentation à l\'utilisateur d\'une alerte lui indiquant un problème avec les signatures de ses documents','','','','','','','DESCRIPTION33','','',0),(415,210,'Début de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','Début de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION34','','',0),(416,210,'Fin de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','Fin de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION35','','',0),(417,210,'Fin de chargement (upload) du jeton de signature [_fichier_signature_taille_] associé au fichier [_liste_fichiers_tailles_]','Fin de chargement (upload) du jeton de signature [_fichier_signature_taille_] associé au fichier [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION36','','',0),(418,210,'Interruption de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','Interruption de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]','','','','','','','DESCRIPTION37','','',0),(419,210,'Réception du dernier fichier (dernier octet)','Réception du dernier fichier (dernier octet)','','','','','','','DESCRIPTION38','','',0),(420,210,'Prépartion de la page de [_nom_page_]','Prépartion de la page de [_nom_page_] ','','','','','','','DESCRIPTION39','','',0),(421,210,'Affichage de la page échec dépôt','Affichage de la page échec dépôt ','','','','','','','DESCRIPTION40','','',0),(422,210,'Validation anticipée','Validation anticipée ','','','','','','','DESCRIPTION41','','',0),(423,210,'Suppression d\'un fichier  suite exception [_liste_fichiers_tailles_]','Suppression d\'un fichier  suite exception [_liste_fichiers_tailles_] ','','','','','','','DESCRIPTION42','','',0),(424,210,'Suppression d\'un fichier par la fonction supprimerFichiersNonValides [_liste_fichiers_tailles_]','Suppression d\'un fichier par la fonction supprimerFichiersNonValides [_liste_fichiers_tailles_] ','','','','','','','DESCRIPTION43','','',0),(425,210,'Téléchargement (download) du DUME Acheteur','Téléchargement (download) du DUME Acheteur ','','','','','','','DESCRIPTION44','','',0);
/*!40000 ALTER TABLE `ValeurReferentiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ValeurReferentielOrg`
--

LOCK TABLES `ValeurReferentielOrg` WRITE;
/*!40000 ALTER TABLE `ValeurReferentielOrg` DISABLE KEYS */;
INSERT INTO `ValeurReferentielOrg` (`id`, `organisme`, `id_referentiel`, `libelle_valeur_referentiel`, `libelle_valeur_referentiel_fr`, `libelle_valeur_referentiel_en`, `libelle_valeur_referentiel_es`, `libelle_valeur_referentiel_su`, `libelle_valeur_referentiel_du`, `libelle_valeur_referentiel_cz`, `libelle_valeur_referentiel_ar`, `libelle_valeur_referentiel_it`, `valeur_sub`) VALUES (1,'a1a',1,'Etat','','','','','','','','','01'),(1,'a1t',1,'Etat','','','','','','','','','01'),(1,'a2z',1,'Etat','','','','','','','','','01'),(1,'e3r',1,'Etat','','','','','','','','','01'),(1,'t5y',1,'Etat','','','','','','','','','01'),(2,'a1a',1,'Région','','','','','','','','','02'),(2,'a1t',1,'Région','','','','','','','','','02'),(2,'a2z',1,'Région','','','','','','','','','02'),(2,'e3r',1,'Région','','','','','','','','','02'),(2,'t5y',1,'Région','','','','','','','','','02'),(3,'a1a',1,'Département','','','','','','','','','03'),(3,'a1t',1,'Département','','','','','','','','','03'),(3,'a2z',1,'Département','','','','','','','','','03'),(3,'e3r',1,'Département','','','','','','','','','03'),(3,'t5y',1,'Département','','','','','','','','','03'),(4,'a1a',1,'Commune','','','','','','','','','04'),(4,'a1t',1,'Commune','','','','','','','','','04'),(4,'a2z',1,'Commune','','','','','','','','','04'),(4,'e3r',1,'Commune','','','','','','','','','04'),(4,'t5y',1,'Commune','','','','','','','','','04'),(5,'a1a',1,'Etablissement public national','','','','','','','','','05'),(5,'a1t',1,'Etablissement public national','','','','','','','','','05'),(5,'a2z',1,'Etablissement public national','','','','','','','','','05'),(5,'e3r',1,'Etablissement public national','','','','','','','','','05'),(5,'t5y',1,'Etablissement public national','','','','','','','','','05'),(6,'a1a',1,'Etablissement public territorial','','','','','','','','','06'),(6,'a1t',1,'Etablissement public territorial','','','','','','','','','06'),(6,'a2z',1,'Etablissement public territorial','','','','','','','','','06'),(6,'e3r',1,'Etablissement public territorial','','','','','','','','','06'),(6,'t5y',1,'Etablissement public territorial','','','','','','','','','06'),(7,'a1a',1,'Autre','','','','','','','','','08'),(7,'a1t',1,'Autre','','','','','','','','','08'),(7,'a2z',1,'Autre','','','','','','','','','08'),(7,'e3r',1,'Autre','','','','','','','','','08'),(7,'t5y',1,'Autre','','','','','','','','','08'),(9,'a1a',3,'Non renseigné','','','','','','','','',''),(9,'a1t',3,'Non renseigné','','','','','','','','',''),(9,'a2z',3,'Non renseigné','','','','','','','','',''),(9,'e3r',3,'Non renseigné','','','','','','','','',''),(9,'t5y',3,'Non renseigné','','','','','','','','',''),(10,'a1a',3,'Oui','','','','','','','','',''),(10,'a1t',3,'Oui','','','','','','','','',''),(10,'a2z',3,'Oui','','','','','','','','',''),(10,'e3r',3,'Oui','','','','','','','','',''),(10,'t5y',3,'Oui','','','','','','','','',''),(11,'a1a',3,'Non','','','','','','','','',''),(11,'a1t',3,'Non','','','','','','','','',''),(11,'a2z',3,'Non','','','','','','','','',''),(11,'e3r',3,'Non','','','','','','','','',''),(11,'t5y',3,'Non','','','','','','','','',''),(12,'a1a',2,'4 000 HT à 19 999,99 HT','','','','','','','','',''),(12,'a1t',2,'4 000 HT à 19 999,99 HT','','','','','','','','',''),(12,'a2z',2,'4 000 HT à 19 999,99 HT','','','','','','','','',''),(12,'e3r',2,'4 000 HT à 19 999,99 HT','','','','','','','','',''),(12,'t5y',2,'4 000 HT à 19 999,99 HT','','','','','','','','',''),(13,'a1a',2,'20 000 HT à 49 999,99 HT','','','','','','','','',''),(13,'a1t',2,'20 000 HT à 49 999,99 HT','','','','','','','','',''),(13,'a2z',2,'20 000 HT à 49 999,99 HT','','','','','','','','',''),(13,'e3r',2,'20 000 HT à 49 999,99 HT','','','','','','','','',''),(13,'t5y',2,'20 000 HT à 49 999,99 HT','','','','','','','','',''),(14,'a1a',2,'50 000 HT à 89 999,99 HT','','','','','','','','',''),(14,'a1t',2,'50 000 HT à 89 999,99 HT','','','','','','','','',''),(14,'a2z',2,'50 000 HT à 89 999,99 HT','','','','','','','','',''),(14,'e3r',2,'50 000 HT à 89 999,99 HT','','','','','','','','',''),(14,'t5y',2,'50 000 HT à 89 999,99 HT','','','','','','','','',''),(15,'a1a',2,'90 000 - 124 999.99','90 000 HT à 124 999,99 HT','90 000 - 124 999.99','','','','','','90 000 - 124 999.99',''),(15,'a1t',2,'90 000 - 124 999.99','90 000 HT à 124 999,99 HT','90 000 - 124 999.99','','','','','','90 000 - 124 999.99',''),(15,'a2z',2,'90 000 - 124 999.99','90 000 HT à 124 999,99 HT','90 000 - 124 999.99','','','','','','90 000 - 124 999.99',''),(15,'e3r',2,'90 000 - 124 999.99','90 000 HT à 124 999,99 HT','90 000 - 124 999.99','','','','','','90 000 - 124 999.99',''),(15,'t5y',2,'90 000 - 124 999.99','90 000 HT à 124 999,99 HT','90 000 - 124 999.99','','','','','','90 000 - 124 999.99',''),(16,'a1a',2,'125 000 - 192 999.99','125 000 HT à 192 999,99 HT','125 000 - 192 999.99','','','','','','125 000 - 192 999.99',''),(16,'a1t',2,'125 000 - 192 999.99','125 000 HT à 192 999,99 HT','125 000 - 192 999.99','','','','','','125 000 - 192 999.99',''),(16,'a2z',2,'125 000 - 192 999.99','125 000 HT à 192 999,99 HT','125 000 - 192 999.99','','','','','','125 000 - 192 999.99',''),(16,'e3r',2,'125 000 - 192 999.99','125 000 HT à 192 999,99 HT','125 000 - 192 999.99','','','','','','125 000 - 192 999.99',''),(16,'t5y',2,'125 000 - 192 999.99','125 000 HT à 192 999,99 HT','125 000 - 192 999.99','','','','','','125 000 - 192 999.99',''),(17,'a1a',2,'193 000 - 999 999.99','193 000 HT à 999 999,99 HT','193 000 - 999 999.99','','','','','','193 000 - 999 999.99',''),(17,'a1t',2,'193 000 - 999 999.99','193 000 HT à 999 999,99 HT','193 000 - 999 999.99','','','','','','193 000 - 999 999.99',''),(17,'a2z',2,'193 000 - 999 999.99','193 000 HT à 999 999,99 HT','193 000 - 999 999.99','','','','','','193 000 - 999 999.99',''),(17,'e3r',2,'193 000 - 999 999.99','193 000 HT à 999 999,99 HT','193 000 - 999 999.99','','','','','','193 000 - 999 999.99',''),(17,'t5y',2,'193 000 - 999 999.99','193 000 HT à 999 999,99 HT','193 000 - 999 999.99','','','','','','193 000 - 999 999.99',''),(18,'a1a',2,'1 000 000 HT à 2 999 999,99 HT','','','','','','','','',''),(18,'a1t',2,'1 000 000 HT à 2 999 999,99 HT','','','','','','','','',''),(18,'a2z',2,'1 000 000 HT à 2 999 999,99 HT','','','','','','','','',''),(18,'e3r',2,'1 000 000 HT à 2 999 999,99 HT','','','','','','','','',''),(18,'t5y',2,'1 000 000 HT à 2 999 999,99 HT','','','','','','','','',''),(19,'a1a',2,'3 000 000 - 4 844 999.99','3 000 000 HT à 4 844 999,99 HT','3 000 000 - 4 844 999.99','','','','','','3 000 000 - 4 844 999.99',''),(19,'a1t',2,'3 000 000 - 4 844 999.99','3 000 000 HT à 4 844 999,99 HT','3 000 000 - 4 844 999.99','','','','','','3 000 000 - 4 844 999.99',''),(19,'a2z',2,'3 000 000 - 4 844 999.99','3 000 000 HT à 4 844 999,99 HT','3 000 000 - 4 844 999.99','','','','','','3 000 000 - 4 844 999.99',''),(19,'e3r',2,'3 000 000 - 4 844 999.99','3 000 000 HT à 4 844 999,99 HT','3 000 000 - 4 844 999.99','','','','','','3 000 000 - 4 844 999.99',''),(19,'t5y',2,'3 000 000 - 4 844 999.99','3 000 000 HT à 4 844 999,99 HT','3 000 000 - 4 844 999.99','','','','','','3 000 000 - 4 844 999.99',''),(20,'a1a',2,'4 845 000 - ...','4 845 000 HT et plus.','4 845 000 - ...','','','','','','4 845 000 - ...',''),(20,'a1t',2,'4 845 000 - ...','4 845 000 HT et plus.','4 845 000 - ...','','','','','','4 845 000 - ...',''),(20,'a2z',2,'4 845 000 - ...','4 845 000 HT et plus.','4 845 000 - ...','','','','','','4 845 000 - ...',''),(20,'e3r',2,'4 845 000 - ...','4 845 000 HT et plus.','4 845 000 - ...','','','','','','4 845 000 - ...',''),(20,'t5y',2,'4 845 000 - ...','4 845 000 HT et plus.','4 845 000 - ...','','','','','','4 845 000 - ...',''),(25,'a1a',15,'Marché public','Marché public','Marché public','','','','','','',''),(25,'a1t',15,'Marché public','Marché public','Marché public','','','','','','',''),(25,'a2z',15,'Marché public','Marché public','Marché public','','','','','','',''),(25,'e3r',15,'Marché public','Marché public','Marché public','','','','','','',''),(26,'a1a',15,'Accord cadre','Accord cadre','Accord cadre','','','','','','',''),(26,'a1t',15,'Accord cadre','Accord cadre','Accord cadre','','','','','','',''),(26,'a2z',15,'Accord cadre','Accord cadre','Accord cadre','','','','','','',''),(26,'e3r',15,'Accord cadre','Accord cadre','Accord cadre','','','','','','',''),(27,'a1a',15,'Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','','','','','','',''),(27,'a1t',15,'Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','','','','','','',''),(27,'a2z',15,'Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','','','','','','',''),(27,'e3r',15,'Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','Système d\'Acquisition Dynamique','','','','','','',''),(28,'a1a',15,'Concession / DSP','Concession / DSP','Concession / DSP','','','','','','',''),(28,'a1t',15,'Concession / DSP','Concession / DSP','Concession / DSP','','','','','','',''),(28,'a2z',15,'Concession / DSP','Concession / DSP','Concession / DSP','','','','','','',''),(28,'e3r',15,'Concession / DSP','Concession / DSP','Concession / DSP','','','','','','',''),(31,'a1a',15,'Marché subséquent à accord-cadre','Marché subséquent à accord-cadre','(en) Marché subséquent à accord-cadre','','','','','','',''),(31,'a1t',15,'Marché subséquent à accord-cadre','Marché subséquent à accord-cadre','(en) Marché subséquent à accord-cadre','','','','','','',''),(31,'a2z',15,'Marché subséquent à accord-cadre','Marché subséquent à accord-cadre','(en) Marché subséquent à accord-cadre','','','','','','',''),(31,'e3r',15,'Marché subséquent à accord-cadre','Marché subséquent à accord-cadre','(en) Marché subséquent à accord-cadre','','','','','','',''),(32,'a1a',15,'Marché de partenariat','Marché de partenariat','(en) Marché de partenariat','','','','','','',''),(32,'a1t',15,'Marché de partenariat','Marché de partenariat','(en) Marché de partenariat','','','','','','',''),(32,'a2z',15,'Marché de partenariat','Marché de partenariat','(en) Marché de partenariat','','','','','','',''),(32,'e3r',15,'Marché de partenariat','Marché de partenariat','(en) Marché de partenariat','','','','','','',''),(33,'a1a',15,'Partenariat d\'innovation','Partenariat d\'innovation','(en) Partenariat d\'innovation','','','','','','',''),(33,'a1t',15,'Partenariat d\'innovation','Partenariat d\'innovation','(en) Partenariat d\'innovation','','','','','','',''),(33,'a2z',15,'Partenariat d\'innovation','Partenariat d\'innovation','(en) Partenariat d\'innovation','','','','','','',''),(33,'e3r',15,'Partenariat d\'innovation','Partenariat d\'innovation','(en) Partenariat d\'innovation','','','','','','',''),(34,'a1a',15,'Marché spécifique (suite SAD)','Marché spécifique (suite SAD)','(en) Marché spécifique (suite SAD)','','','','','','',''),(34,'a1t',15,'Marché spécifique (suite SAD)','Marché spécifique (suite SAD)','(en) Marché spécifique (suite SAD)','','','','','','',''),(34,'a2z',15,'Marché spécifique (suite SAD)','Marché spécifique (suite SAD)','(en) Marché spécifique (suite SAD)','','','','','','',''),(34,'e3r',15,'Marché spécifique (suite SAD)','Marché spécifique (suite SAD)','(en) Marché spécifique (suite SAD)','','','','','','',''),(35,'a1a',15,'Marché public de conception réalisation','Marché public de conception réalisation','(en) Marché public de conception réalisation','','','','','','',''),(35,'a1t',15,'Marché public de conception réalisation','Marché public de conception réalisation','(en) Marché public de conception réalisation','','','','','','',''),(35,'a2z',15,'Marché public de conception réalisation','Marché public de conception réalisation','(en) Marché public de conception réalisation','','','','','','',''),(35,'e3r',15,'Marché public de conception réalisation','Marché public de conception réalisation','(en) Marché public de conception réalisation','','','','','','',''),(36,'a1a',15,'Autre','Autre','(en) Autre','','','','','','',''),(36,'a1t',15,'Autre','Autre','(en) Autre','','','','','','',''),(36,'a2z',15,'Autre','Autre','(en) Autre','','','','','','',''),(36,'e3r',15,'Autre','Autre','(en) Autre','','','','','','',''),(37,'a1a',158,'Opération','Opération','','','','','','','',''),(38,'a1a',158,'Unité fonctionnelle','Unité fonctionnelle','','','','','','','',''),(39,'a1t',158,'Opération','Opération','','','','','','','',''),(40,'a1t',158,'Unité fonctionnelle','Unité fonctionnelle','','','','','','','',''),(41,'a2z',158,'Opération','Opération','','','','','','','',''),(42,'a2z',158,'Unité fonctionnelle','Unité fonctionnelle','','','','','','','',''),(43,'e3r',158,'Opération','Opération','','','','','','','',''),(44,'e3r',158,'Unité fonctionnelle','Unité fonctionnelle','','','','','','','','');
/*!40000 ALTER TABLE `ValeurReferentielOrg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alerte_metier`
--

LOCK TABLES `alerte_metier` WRITE;
/*!40000 ALTER TABLE `alerte_metier` DISABLE KEYS */;
/*!40000 ALTER TABLE `alerte_metier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `api_user`
--

LOCK TABLES `api_user` WRITE;
/*!40000 ALTER TABLE `api_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `api_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Agent_Commission`
--

LOCK TABLES `backup_Agent_Commission` WRITE;
/*!40000 ALTER TABLE `backup_Agent_Commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Agent_Commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Commission`
--

LOCK TABLES `backup_Commission` WRITE;
/*!40000 ALTER TABLE `backup_Commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Etape_CAO`
--

LOCK TABLES `backup_Etape_CAO` WRITE;
/*!40000 ALTER TABLE `backup_Etape_CAO` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Etape_CAO` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Intervenant_Externe`
--

LOCK TABLES `backup_Intervenant_Externe` WRITE;
/*!40000 ALTER TABLE `backup_Intervenant_Externe` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Intervenant_Externe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Intervenant_Externe_Commission`
--

LOCK TABLES `backup_Intervenant_Externe_Commission` WRITE;
/*!40000 ALTER TABLE `backup_Intervenant_Externe_Commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Intervenant_Externe_Commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Intervenant_Ordre_Du_Jour`
--

LOCK TABLES `backup_Intervenant_Ordre_Du_Jour` WRITE;
/*!40000 ALTER TABLE `backup_Intervenant_Ordre_Du_Jour` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Intervenant_Ordre_Du_Jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Ordre_Du_Jour`
--

LOCK TABLES `backup_Ordre_Du_Jour` WRITE;
/*!40000 ALTER TABLE `backup_Ordre_Du_Jour` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Ordre_Du_Jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_Ordre_Du_Jour_2`
--

LOCK TABLES `backup_Ordre_Du_Jour_2` WRITE;
/*!40000 ALTER TABLE `backup_Ordre_Du_Jour_2` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_Ordre_Du_Jour_2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_t_CAO_Ordre_Du_Jour`
--

LOCK TABLES `backup_t_CAO_Ordre_Du_Jour` WRITE;
/*!40000 ALTER TABLE `backup_t_CAO_Ordre_Du_Jour` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_t_CAO_Ordre_Du_Jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `backup_t_CAO_Ordre_Du_Jour_Intervenant`
--

LOCK TABLES `backup_t_CAO_Ordre_Du_Jour_Intervenant` WRITE;
/*!40000 ALTER TABLE `backup_t_CAO_Ordre_Du_Jour_Intervenant` DISABLE KEYS */;
/*!40000 ALTER TABLE `backup_t_CAO_Ordre_Du_Jour_Intervenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blob`
--

LOCK TABLES `blob` WRITE;
/*!40000 ALTER TABLE `blob` DISABLE KEYS */;
/*!40000 ALTER TABLE `blob` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blobOrganisme`
--

LOCK TABLES `blobOrganisme` WRITE;
/*!40000 ALTER TABLE `blobOrganisme` DISABLE KEYS */;
/*!40000 ALTER TABLE `blobOrganisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blobOrganisme_file`
--

LOCK TABLES `blobOrganisme_file` WRITE;
/*!40000 ALTER TABLE `blobOrganisme_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `blobOrganisme_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blob_file`
--

LOCK TABLES `blob_file` WRITE;
/*!40000 ALTER TABLE `blob_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `blob_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blocFichierEnveloppe`
--

LOCK TABLES `blocFichierEnveloppe` WRITE;
/*!40000 ALTER TABLE `blocFichierEnveloppe` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocFichierEnveloppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `blocFichierEnveloppeTemporaire`
--

LOCK TABLES `blocFichierEnveloppeTemporaire` WRITE;
/*!40000 ALTER TABLE `blocFichierEnveloppeTemporaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `blocFichierEnveloppeTemporaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `categories_considerations_sociales`
--

LOCK TABLES `categories_considerations_sociales` WRITE;
/*!40000 ALTER TABLE `categories_considerations_sociales` DISABLE KEYS */;
INSERT INTO `categories_considerations_sociales` (`id`, `label`, `code`) VALUES (1,'Insertion par l\'activité économique','insertionActiviteEconomique'),(2,'Clause sociale de formation sous statut scolaire au bénéfice de jeunes en situation de décrochage scolaire','clauseSocialeFormationScolaire'),(3,'Lutte contre les discriminations : Egalité femmes/hommes, etc.','lutteContreDiscriminations'),(4,'Commerce équitable','commerceEquitable'),(5,'Achats éthiques, traçabilité sociale des services/fournitures, etc.','achatsEthiquesTracabiliteSociale'),(6,'Autre(s) clause(s) sociale(s)','autreClauseSociale');
/*!40000 ALTER TABLE `categories_considerations_sociales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ccag_applicable`
--

LOCK TABLES `ccag_applicable` WRITE;
/*!40000 ALTER TABLE `ccag_applicable` DISABLE KEYS */;
INSERT INTO `ccag_applicable` (`id_auto`, `id`, `organisme`, `libelle`) VALUES (1,1,'','FCS'),(2,2,'','PI'),(3,3,'','T');
/*!40000 ALTER TABLE `ccag_applicable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `changement_heure`
--

LOCK TABLES `changement_heure` WRITE;
/*!40000 ALTER TABLE `changement_heure` DISABLE KEYS */;
/*!40000 ALTER TABLE `changement_heure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `chorus_noms_fichiers`
--

LOCK TABLES `chorus_noms_fichiers` WRITE;
/*!40000 ALTER TABLE `chorus_noms_fichiers` DISABLE KEYS */;
/*!40000 ALTER TABLE `chorus_noms_fichiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `chorus_numeros_marches`
--

LOCK TABLES `chorus_numeros_marches` WRITE;
/*!40000 ALTER TABLE `chorus_numeros_marches` DISABLE KEYS */;
/*!40000 ALTER TABLE `chorus_numeros_marches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `code_retour`
--

LOCK TABLES `code_retour` WRITE;
/*!40000 ALTER TABLE `code_retour` DISABLE KEYS */;
INSERT INTO `code_retour` (`code`, `description`) VALUES (0,'Ce code est retourné quand la tâche ou l’interface (dans le cas où l’interface ne retourne rien) sais exécuté sans erreur.'),(1,'Ce code est retourné dans le cas d’erreur non définie.'),(2,'Ce code est retourné lors d’un mauvais usage de la commande (mauvais paramètre, etc. )'),(3,'Ce code est retourné lorsqu’une ressource n’est pas trouvé'),(10,'Ce code est retourné lorsqu’un service n’arrive pas à écrire sur le systèmes.'),(11,'Certains fichiers/dossiers sont  introuvables'),(20,'Ce code est retourné lorsqu’un service distant  ne réponde pas (timeout, ex : apache : 504)'),(21,'Ce code est retourné lorsqu’un service distant nous retourne une erreur ( ex : code apache 500 ou 503)'),(30,'L’application a rencontré un problème.'),(126,'Ce code est retourné lorsque la commande ou un service distant n’a pas les droits d’exécution ou que l’utilisateur n’ai pas autorisé à l’exécuter.'),(127,'Ce code est retourné lorsque la commande ou un service distant  n’existe pas (exemple erreur apache : 404)'),(128,'Ce code est retourné lorsque la commande ou un service distant  ne retourne pas le bon résultat attendu.');
/*!40000 ALTER TABLE `code_retour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `compte_centrale`
--

LOCK TABLES `compte_centrale` WRITE;
/*!40000 ALTER TABLE `compte_centrale` DISABLE KEYS */;
/*!40000 ALTER TABLE `compte_centrale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `comptes_agents_associes`
--

LOCK TABLES `comptes_agents_associes` WRITE;
/*!40000 ALTER TABLE `comptes_agents_associes` DISABLE KEYS */;
/*!40000 ALTER TABLE `comptes_agents_associes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `configuration_alerte`
--

LOCK TABLES `configuration_alerte` WRITE;
/*!40000 ALTER TABLE `configuration_alerte` DISABLE KEYS */;
/*!40000 ALTER TABLE `configuration_alerte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `configuration_organisme`
--

LOCK TABLES `configuration_organisme` WRITE;
/*!40000 ALTER TABLE `configuration_organisme` DISABLE KEYS */;
INSERT INTO `configuration_organisme` (`organisme`, `encheres`, `consultation_pj_autres_pieces_telechargeables`, `no_activex`, `gestion_mapa`, `article_133_upload_fichier`, `centrale_publication`, `organisation_centralisee`, `presence_elu`, `traduire_consultation`, `suivi_passation`, `numerotation_ref_cons`, `pmi_lien_portail_defense_agent`, `interface_archive_arcade_pmi`, `desarchivage_consultation`, `alimentation_automatique_liste_invites`, `interface_chorus_pmi`, `archivage_consultation_sur_pf`, `autoriser_modification_apres_phase_consultation`, `importer_enveloppe`, `export_marches_notifies`, `acces_agents_cfe_bd_fournisseur`, `acces_agents_cfe_ouverture_analyse`, `utiliser_parametrage_encheres`, `verifier_compte_boamp`, `gestion_mandataire`, `four_eyes`, `interface_module_rsem`, `ARCHIVAGE_CONSULTATION_SAE_EXTERNE_ENVOI_ARCHIVE`, `ARCHIVAGE_CONSULTATION_SAE_EXTERNE_TELECHARGEMENT_ARCHIVE`, `agent_verification_certificat_peppol`, `fuseau_horaire`, `fiche_weka`, `mise_disposition_pieces_marche`, `base_dce`, `avis_membres_commision`, `Donnees_Redac`, `Personnaliser_Affichage_Theme_Et_Illustration`, `type_contrat`, `entite_adjudicatrice`, `calendrier_de_la_consultation`, `donnees_complementaires`, `espace_collaboratif`, `historique_navigation_inscrits`, `Identification_contrat`, `extraction_accords_cadres`, `gestion_operations`, `extraction_siret_acheteur`, `marche_public_simplifie`, `recherches_favorites_agent`, `profil_rma`, `filtre_contrat_ac_sad`, `affichage_nom_service_pere`, `mode_applet`, `marche_defense`) VALUES ('a1a','1','1','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','0','0','1','0','1','0','0','1','1','0','0','1','0','0','0','0','1','1','1','1','1','1','0','1','0','0','0','1','1','0','0','1','1','0','0');
/*!40000 ALTER TABLE `configuration_organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `configuration_plateforme`
--

LOCK TABLES `configuration_plateforme` WRITE;
/*!40000 ALTER TABLE `configuration_plateforme` DISABLE KEYS */;
INSERT INTO `configuration_plateforme` (`id_auto`, `code_cpv`, `multi_linguisme_entreprise`, `gestion_fournisseurs_docs_mes_sous_services`, `authenticate_inscrit_by_cert`, `authenticate_inscrit_by_login`, `base_qualifiee_entreprise_insee`, `gestion_boamp_mes_sous_services`, `gestion_bi_cle_mes_sous_services`, `nom_entreprise_toujours_visible`, `gestion_jal_mes_sous_services`, `choix_langue_affichage_consultation`, `compte_entreprise_donnees_complementaires`, `annuaire_entites_achat_visible_par_entreprise`, `affichage_recherche_avancee_agent_ac_sad_transversaux`, `encheres_entreprise`, `socle_interne`, `module_certificat`, `socle_externe_agent`, `afficher_image_organisme`, `socle_externe_entreprise`, `portail_defense_entreprise`, `compte_entreprise_province`, `compte_entreprise_telephone3`, `compte_entreprise_tax_prof`, `compte_entreprise_rcville`, `compte_entreprise_declaration_honneur`, `compte_entreprise_qualification`, `compte_entreprise_moyens_techniques`, `compte_entreprise_prestations_realisees`, `compte_entreprise_chiffre_affaire_production_biens_services`, `enveloppe_offre_technique`, `compte_inscrit_choix_profil`, `procedure_adaptee`, `compte_entreprise_siren`, `compte_entreprise_activation_inscription_par_agent`, `menu_entreprise_consultations_en_cours`, `compte_entreprise_capital_social`, `mail_activation_compte_inscrit_entreprise`, `decision_date_notification`, `decision_pmi_pme`, `decision_nature_prestations`, `decision_objet_marche`, `decision_note`, `decision_fiche_recensement`, `registre_papier_mail_obligatoire`, `menu_entreprise_indicateurs_cles`, `ajout_rpa_champ_email`, `ajout_rpa_champ_telephone`, `ajout_rpa_champ_fax`, `entreprise_poser_question_sans_pj`, `url_demarche_agent`, `url_demarche_entreprise`, `siret_detail_entite_achat`, `presence_elu`, `gerer_mon_service`, `depouillement_enveloppe_depend_RAT_enveloppe_precedente`, `consultation_adresse_retrais_dossiers`, `consultation_adresse_depot_offres`, `consultation_caution_provisoire`, `consultation_lieu_ouverture_plis`, `consultation_qualification`, `consultation_agrement`, `consultation_echantillons_demandes`, `consultation_reunion`, `consultation_visite_des_lieux`, `consultation_prix_acquisition`, `resultat_analyse_avant_decision`, `creation_inscrit_par_ates`, `consultation_variantes_autorisees`, `recherche_avancee_par_type_org`, `menu_agent_societes_exclues`, `recherche_avancee_par_domaines_activite`, `recherche_avancee_par_qualification`, `recherche_avancee_par_agrement`, `contact_administratif_dans_detail_consultation_cote_entreprise`, `consultation_pieces_dossiers`, `gerer_adresses_service`, `traduire_annonces`, `afficher_bloc_actions_dans_details_annonces`, `autoriser_une_seule_reponse_principale_par_entreprise`, `generation_avis`, `passation_appliquer_donnees_ensemble_lots`, `autre_annonce_extrait_pv`, `autre_annonce_rapport_achevement`, `ajout_fichier_joint_autre_annonce`, `consultation_mode_passation`, `compte_entreprise_identifiant_unique`, `gerer_certificats_agent`, `autre_annonce_programme_previsionnel`, `annuler_consultation`, `cfe_entreprise_accessible_par_agent`, `compte_entreprise_code_nace_referentiel`, `code_nut_lt_referentiel`, `lieux_execution`, `compte_entreprise_domaine_activite_lt_referentiel`, `consultation_domaines_activites_lt_referentiel`, `compte_entreprise_agrement_lt_referentiel`, `compte_entreprise_qualification_lt_referentiel`, `reponse_pas_a_pas`, `agent_controle_format_mot_de_passe`, `entreprise_validation_email_inscription`, `telecharger_dce_avec_authentification`, `authentification_basic`, `reglement_consultation`, `annonces_marches`, `cfe_date_fin_validite_obligatoire`, `associer_documents_cfe_consultation`, `compte_entreprise_region`, `compte_entreprise_telephone2`, `compte_entreprise_cnss`, `compte_entreprise_rcnum`, `compte_entreprise_domaine_activite`, `compte_inscrit_code_nic`, `compte_entreprise_code_ape`, `compte_entreprise_documents_commerciaux`, `compte_entreprise_agrement`, `compte_entreprise_moyens_humains`, `compte_entreprise_activite_domaine_defense`, `compte_entreprise_donnees_financieres`, `enveloppe_anonymat`, `publicite_format_xml`, `article_133_generation_pf`, `entreprise_repondre_consultation_apres_cloture`, `telechargement_outil_verif_horodatage`, `affichage_code_cpv`, `consultation_domaines_activites`, `statistiques_mesure_demat`, `publication_procure`, `menu_entreprise_toutes_les_consultations`, `compte_entreprise_cp_obligatoire`, `annuler_depot`, `traduire_entite_achat_arabe`, `traduire_organisme_arabe`, `decision_cp`, `decision_tranche_budgetaire`, `decision_classement`, `decision_afficher_detail_candidat_par_defaut`, `article_133_upload_fichier`, `multi_linguisme_agent`, `compte_entreprise_ifu`, `gestion_organisme_par_agent`, `utiliser_lucene`, `utiliser_page_html_lieux_execution`, `prado_validateur_format_date`, `prado_validateur_format_email`, `socle_externe_ppp`, `validation_format_champs_stricte`, `poser_question_necessite_authentification`, `autoriser_modif_profil_inscrit_ates`, `unicite_reference_consultation`, `registre_papier_rcnum_rcville_obligatoires`, `registre_papier_adresse_cp_ville_obligatoires`, `telecharger_dce_sans_identification`, `gestion_entreprise_par_agent`, `autoriser_caracteres_speciaux_dans_reference`, `inscription_libre_entreprise`, `afficher_code_service`, `authenticate_agent_by_login`, `authenticate_agent_by_cert`, `generer_acte_dengagement`, `entreprise_controle_format_mot_de_passe`, `autre_annonce_information`, `creer_autre_annonce`, `consultation_clause`, `panier_entreprise`, `regle_mise_en_ligne_par_entite_coordinatrice`, `gestion_newsletter`, `publicite_opoce`, `gestion_modeles_formulaire`, `gestion_adresses_facturation_JAL`, `publicite_marches_en_ligne`, `parametrage_publicite_par_type_procedure`, `export_decision`, `lieu_ouverture_plis_obligatoire`, `dossier_additif`, `type_marche`, `type_prestation`, `afficher_tjr_bloc_caracteristique_reponse`, `alerte_metier`, `bourse_a_la_sous_traitance`, `partager_consultation`, `annuaire_acheteurs_publics`, `entreprise_actions_groupees`, `publier_guides`, `recherche_auto_completion`, `statut_compte_entreprise`, `gestion_organismes`, `accueil_entreprise_personnalise`, `interface_module_sub`, `authentification_agent_multi_organismes`, `lieux_execution_carte`, `surcharge_referentiels`, `Mode_Restriction_RGS`, `autre_annonce_decision_resiliation`, `autre_annonce_synthese_rapport_audit`, `fiche_weka`, `generation_automatique_mdp_agent`, `generation_automatique_mdp_inscrit`, `liste_ac_rgs`, `liste_cons_org`, `marche_public_simplifie_entreprise`, `archive_par_lot`, `recherches_favorites`, `documents_reference`, `synchronisation_SGMAP`, `donnees_candidat`, `autoriser_creation_entreprise_etrangere`, `bourse_cotraitance`, `ac_sad_transversaux`, `web_service_par_silo`, `groupement`, `notifications_agent`, `publicite`, `interface_dume`, `entreprise_duree_vie_mot_de_passe`, `entreprise_mots_de_passe_historises`, `plateforme_editeur`, `donnees_essentielles_suivi_sn`, `afficher_valeur_estimee`, `case_attestation_consultation`) VALUES (1,'1','0','1','0','1','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','1','1','0','0','0','0','1','1','1','1','0','1','0','0','0','0','0','0','0','0','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','1','0','0','0','0','0','1','0','0','0','1','0','1','0','0','0','0','0','0','1','1','0','0','0','1','0','1','1','1','1','0','1','0','1','1','1','1','0','0','0','1','1','0','0','1','0','0','0','1','0','1','1','0','0','0','1','0','0','0','1','0','0','1','0','1','0','0','0','1','1','0','1','0','1','0','0','0','1','1','0','0','0','1','0','0','0','0','0','0','0','0','0','0','0','0','0','1','1','1','0','0','0','0','0','0','0','0','0','0','0','0','0','0','1','0','0','0','1','1','0','1',0,0,'1','0','0','0');
/*!40000 ALTER TABLE `configuration_plateforme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `consultation`
--

LOCK TABLES `consultation` WRITE;
/*!40000 ALTER TABLE `consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `consultation_document_cfe`
--

LOCK TABLES `consultation_document_cfe` WRITE;
/*!40000 ALTER TABLE `consultation_document_cfe` DISABLE KEYS */;
/*!40000 ALTER TABLE `consultation_document_cfe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `decisionEnveloppe`
--

LOCK TABLES `decisionEnveloppe` WRITE;
/*!40000 ALTER TABLE `decisionEnveloppe` DISABLE KEYS */;
/*!40000 ALTER TABLE `decisionEnveloppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `destinataire_centrale_pub`
--

LOCK TABLES `destinataire_centrale_pub` WRITE;
/*!40000 ALTER TABLE `destinataire_centrale_pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `destinataire_centrale_pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `destinataire_mise_disposition`
--

LOCK TABLES `destinataire_mise_disposition` WRITE;
/*!40000 ALTER TABLE `destinataire_mise_disposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `destinataire_mise_disposition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `document_serveur_docs`
--

LOCK TABLES `document_serveur_docs` WRITE;
/*!40000 ALTER TABLE `document_serveur_docs` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_serveur_docs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `donnees_annuelles_concession`
--

LOCK TABLES `donnees_annuelles_concession` WRITE;
/*!40000 ALTER TABLE `donnees_annuelles_concession` DISABLE KEYS */;
/*!40000 ALTER TABLE `donnees_annuelles_concession` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `donnees_annuelles_concession_tarif`
--

LOCK TABLES `donnees_annuelles_concession_tarif` WRITE;
/*!40000 ALTER TABLE `donnees_annuelles_concession_tarif` DISABLE KEYS */;
/*!40000 ALTER TABLE `donnees_annuelles_concession_tarif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `echanges_interfaces`
--

LOCK TABLES `echanges_interfaces` WRITE;
/*!40000 ALTER TABLE `echanges_interfaces` DISABLE KEYS */;
INSERT INTO `echanges_interfaces` (`id`, `code_retour`, `service`, `nom_batch`, `variables_entree`, `resultat`, `type_flux`, `poids`, `information_metier`, `nb_flux`, `debut_execution`, `fin_execution`, `id_echanges_interfaces`, `nom_interface`) VALUES (1,0,'SGMAP','getEntrepriseBySiren','a:2:{s:5:\"siren\";s:9:\"831670377\";s:17:\"withEtablissement\";b:0;}',NULL,'WebService',0,'a:2:{s:5:\"siren\";s:9:\"831670377\";s:17:\"withEtablissement\";b:0;}',1,'2019-03-14 17:37:09','2019-03-14 17:37:10',NULL,NULL),(2,0,'SGMAP','preRemplirEntrepriseVo','a:1:{s:7:\"reponse\";a:3:{s:10:\"entreprise\";a:20:{s:5:\"siren\";s:9:\"831670377\";s:14:\"capital_social\";i:65000;s:29:\"numero_tva_intracommunautaire\";s:13:\"FR35831670377\";s:15:\"forme_juridique\";s:105:\"Société par actions simplifiée à associé unique ou société par actions simplifiée unipersonnelle \";s:20:\"forme_juridique_code\";s:4:\"5720\";s:14:\"nom_commercial\";s:0:\"\";s:20:\"procedure_collective\";b:0;s:8:\"enseigne\";N;s:22:\"libelle_naf_entreprise\";s:44:\"Édition de logiciels système et de réseau\";s:14:\"naf_entreprise\";s:5:\"5829A\";s:14:\"raison_sociale\";s:5:\"MYPRM\";s:18:\"siret_siege_social\";s:14:\"83167037700017\";s:24:\"code_effectif_entreprise\";s:2:\"00\";s:13:\"date_creation\";i:1500933600;s:3:\"nom\";N;s:6:\"prenom\";N;s:14:\"date_radiation\";N;s:20:\"categorie_entreprise\";N;s:35:\"tranche_effectif_salarie_entreprise\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:19:\"mandataires_sociaux\";a:2:{i:0;a:9:{s:3:\"nom\";s:6:\"BESSIS\";s:6:\"prenom\";s:22:\"RICHARD ANDRE JOSSELIN\";s:8:\"fonction\";s:9:\"PRESIDENT\";s:14:\"date_naissance\";s:10:\"1973-03-17\";s:24:\"date_naissance_timestamp\";i:101170800;s:9:\"dirigeant\";b:1;s:14:\"raison_sociale\";s:0:\"\";s:11:\"identifiant\";s:0:\"\";s:4:\"type\";s:2:\"PP\";}i:1;a:9:{s:3:\"nom\";s:9:\"BAVENCOFF\";s:6:\"prenom\";s:4:\"LOIC\";s:8:\"fonction\";s:17:\"DIRECTEUR GENERAL\";s:14:\"date_naissance\";s:10:\"1973-07-05\";s:24:\"date_naissance_timestamp\";i:110674800;s:9:\"dirigeant\";b:1;s:14:\"raison_sociale\";s:0:\"\";s:11:\"identifiant\";s:0:\"\";s:4:\"type\";s:2:\"PP\";}}}s:19:\"etablissement_siege\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',NULL,'WebService',0,'a:1:{s:7:\"reponse\";a:3:{s:10:\"entreprise\";a:20:{s:5:\"siren\";s:9:\"831670377\";s:14:\"capital_social\";i:65000;s:29:\"numero_tva_intracommunautaire\";s:13:\"FR35831670377\";s:15:\"forme_juridique\";s:105:\"Société par actions simplifiée à associé unique ou société par actions simplifiée unipersonnelle \";s:20:\"forme_juridique_code\";s:4:\"5720\";s:14:\"nom_commercial\";s:0:\"\";s:20:\"procedure_collective\";b:0;s:8:\"enseigne\";N;s:22:\"libelle_naf_entreprise\";s:44:\"Édition de logiciels système et de réseau\";s:14:\"naf_entreprise\";s:5:\"5829A\";s:14:\"raison_sociale\";s:5:\"MYPRM\";s:18:\"siret_siege_social\";s:14:\"83167037700017\";s:24:\"code_effectif_entreprise\";s:2:\"00\";s:13:\"date_creation\";i:1500933600;s:3:\"nom\";N;s:6:\"prenom\";N;s:14:\"date_radiation\";N;s:20:\"categorie_entreprise\";N;s:35:\"tranche_effectif_salarie_entreprise\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:19:\"mandataires_sociaux\";a:2:{i:0;a:9:{s:3:\"nom\";s:6:\"BESSIS\";s:6:\"prenom\";s:22:\"RICHARD ANDRE JOSSELIN\";s:8:\"fonction\";s:9:\"PRESIDENT\";s:14:\"date_naissance\";s:10:\"1973-03-17\";s:24:\"date_naissance_timestamp\";i:101170800;s:9:\"dirigeant\";b:1;s:14:\"raison_sociale\";s:0:\"\";s:11:\"identifiant\";s:0:\"\";s:4:\"type\";s:2:\"PP\";}i:1;a:9:{s:3:\"nom\";s:9:\"BAVENCOFF\";s:6:\"prenom\";s:4:\"LOIC\";s:8:\"fonction\";s:17:\"DIRECTEUR GENERAL\";s:14:\"date_naissance\";s:10:\"1973-07-05\";s:24:\"date_naissance_timestamp\";i:110674800;s:9:\"dirigeant\";b:1;s:14:\"raison_sociale\";s:0:\"\";s:11:\"identifiant\";s:0:\"\";s:4:\"type\";s:2:\"PP\";}}}s:19:\"etablissement_siege\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',1,'2019-03-14 17:37:10','2019-03-14 17:37:10',NULL,NULL),(3,0,'SGMAP','getEtablissementBySiret','a:1:{s:5:\"siret\";s:14:\"83167037700017\";}',NULL,'WebService',0,'a:1:{s:5:\"siret\";s:14:\"83167037700017\";}',1,'2019-03-14 17:37:10','2019-03-14 17:37:11',NULL,NULL),(4,0,'SGMAP','preRemplirEtablissementVo','a:1:{s:7:\"reponse\";a:2:{s:13:\"etablissement\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',NULL,'WebService',0,'a:1:{s:7:\"reponse\";a:2:{s:13:\"etablissement\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',1,'2019-03-14 17:37:11','2019-03-14 17:37:11',NULL,NULL),(5,0,'SGMAP','getEtablissementBySiret','a:1:{s:5:\"siret\";s:14:\"83167037700017\";}',NULL,'WebService',0,'a:1:{s:5:\"siret\";s:14:\"83167037700017\";}',1,'2019-03-14 17:37:11','2019-03-14 17:37:11',NULL,NULL),(6,0,'SGMAP','preRemplirEtablissementVo','a:1:{s:7:\"reponse\";a:2:{s:13:\"etablissement\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',NULL,'WebService',0,'a:1:{s:7:\"reponse\";a:2:{s:13:\"etablissement\";a:13:{s:12:\"siege_social\";b:1;s:5:\"siret\";s:14:\"83167037700017\";s:3:\"naf\";s:5:\"5829A\";s:11:\"libelle_naf\";s:44:\"Édition de logiciels système et de réseau\";s:16:\"date_mise_a_jour\";i:1504735200;s:38:\"tranche_effectif_salarie_etablissement\";a:5:{s:2:\"de\";i:0;s:1:\"a\";i:0;s:4:\"code\";s:2:\"00\";s:14:\"date_reference\";s:4:\"2017\";s:8:\"intitule\";s:10:\"0 salarié\";}s:27:\"date_creation_etablissement\";i:1500933600;s:19:\"region_implantation\";a:2:{s:4:\"code\";s:2:\"11\";s:5:\"value\";s:14:\"Île-de-France\";}s:20:\"commune_implantation\";a:2:{s:4:\"code\";s:5:\"75108\";s:5:\"value\";s:7:\"PARIS 8\";}s:17:\"pays_implantation\";a:2:{s:4:\"code\";N;s:5:\"value\";N;}s:26:\"diffusable_commercialement\";b:1;s:8:\"enseigne\";N;s:7:\"adresse\";a:15:{s:2:\"l1\";s:5:\"MYPRM\";s:2:\"l2\";N;s:2:\"l3\";N;s:2:\"l4\";s:17:\"128 RUE LA BOETIE\";s:2:\"l5\";N;s:2:\"l6\";s:11:\"75008 PARIS\";s:2:\"l7\";s:6:\"FRANCE\";s:11:\"numero_voie\";s:3:\"128\";s:9:\"type_voie\";s:3:\"RUE\";s:8:\"nom_voie\";s:9:\"LA BOETIE\";s:18:\"complement_adresse\";N;s:11:\"code_postal\";s:5:\"75008\";s:8:\"localite\";s:7:\"PARIS 8\";s:19:\"code_insee_localite\";s:5:\"75108\";s:5:\"cedex\";N;}}s:13:\"gateway_error\";b:0;}}',1,'2019-03-14 17:37:11','2019-03-14 17:37:11',NULL,NULL);
/*!40000 ALTER TABLE `echanges_interfaces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fichierEnveloppe`
--

LOCK TABLES `fichierEnveloppe` WRITE;
/*!40000 ALTER TABLE `fichierEnveloppe` DISABLE KEYS */;
/*!40000 ALTER TABLE `fichierEnveloppe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fichiers_liste_marches`
--

LOCK TABLES `fichiers_liste_marches` WRITE;
/*!40000 ALTER TABLE `fichiers_liste_marches` DISABLE KEYS */;
/*!40000 ALTER TABLE `fichiers_liste_marches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `formejuridique`
--

LOCK TABLES `formejuridique` WRITE;
/*!40000 ALTER TABLE `formejuridique` DISABLE KEYS */;
INSERT INTO `formejuridique` (`formejuridique`, `ordre`, `libelle_formejuridique`, `libelle_formejuridique_fr`, `libelle_formejuridique_en`, `libelle_formejuridique_es`, `libelle_formejuridique_su`, `libelle_formejuridique_du`, `libelle_formejuridique_cz`, `libelle_formejuridique_ar`, `libelle_formejuridique_it`) VALUES ('Autre',NULL,'','','','','','','','',''),('EURL',NULL,'','','','','','','','',''),('GIE',NULL,'','','','','','','','',''),('SA',NULL,'','','','','','','','',''),('SARL',NULL,'','','','','','','','',''),('SAS',NULL,'','','','','','','','',''),('SASU',NULL,'','','','','','','','',''),('SNC',NULL,'','','','','','','','','');
/*!40000 ALTER TABLE `formejuridique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gestion_adresses`
--

LOCK TABLES `gestion_adresses` WRITE;
/*!40000 ALTER TABLE `gestion_adresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `gestion_adresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `historiques_consultation`
--

LOCK TABLES `historiques_consultation` WRITE;
/*!40000 ALTER TABLE `historiques_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `historiques_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `historisation_mot_de_passe`
--

LOCK TABLES `historisation_mot_de_passe` WRITE;
/*!40000 ALTER TABLE `historisation_mot_de_passe` DISABLE KEYS */;
/*!40000 ALTER TABLE `historisation_mot_de_passe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jms_cron_jobs`
--

LOCK TABLES `jms_cron_jobs` WRITE;
/*!40000 ALTER TABLE `jms_cron_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jms_cron_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jms_job_dependencies`
--

LOCK TABLES `jms_job_dependencies` WRITE;
/*!40000 ALTER TABLE `jms_job_dependencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `jms_job_dependencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jms_job_related_entities`
--

LOCK TABLES `jms_job_related_entities` WRITE;
/*!40000 ALTER TABLE `jms_job_related_entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `jms_job_related_entities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jms_job_statistics`
--

LOCK TABLES `jms_job_statistics` WRITE;
/*!40000 ALTER TABLE `jms_job_statistics` DISABLE KEYS */;
/*!40000 ALTER TABLE `jms_job_statistics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jms_jobs`
--

LOCK TABLES `jms_jobs` WRITE;
/*!40000 ALTER TABLE `jms_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jms_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `mode_execution_contrat`
--

LOCK TABLES `mode_execution_contrat` WRITE;
/*!40000 ALTER TABLE `mode_execution_contrat` DISABLE KEYS */;
INSERT INTO `mode_execution_contrat` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'1 - Ordinaire (Sans tranche, exclusivement à prix forfaitaire)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2 - A bons de commande (Sans tranche, exclusivement à bons de commande)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'3 - A Tranches (Avec TF et TC, exclusivement à prix forfaitaire)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'4 - A tranches et bons de commande (Avec TF et TC, en partie à bons de commande)',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'5 - A phases',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'6 - Autres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `mode_execution_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `modification_contrat`
--

LOCK TABLES `modification_contrat` WRITE;
/*!40000 ALTER TABLE `modification_contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `modification_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `nature_acte_juridique`
--

LOCK TABLES `nature_acte_juridique` WRITE;
/*!40000 ALTER TABLE `nature_acte_juridique` DISABLE KEYS */;
INSERT INTO `nature_acte_juridique` (`id`, `libelle`, `libelle_fr`, `libelle_en`, `libelle_es`, `libelle_su`, `libelle_du`, `libelle_cz`, `libelle_ar`, `libelle_it`) VALUES (1,'1-Contrat initial',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2-Contrat complémentaire',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'3-Contrat sur la base d\'un accord cadre',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'4-Marché de définition',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(5,'5-Autre',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `nature_acte_juridique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `passation_marche_a_venir`
--

LOCK TABLES `passation_marche_a_venir` WRITE;
/*!40000 ALTER TABLE `passation_marche_a_venir` DISABLE KEYS */;
/*!40000 ALTER TABLE `passation_marche_a_venir` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `programme_previsionnel`
--

LOCK TABLES `programme_previsionnel` WRITE;
/*!40000 ALTER TABLE `programme_previsionnel` DISABLE KEYS */;
/*!40000 ALTER TABLE `programme_previsionnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `questions_dce`
--

LOCK TABLES `questions_dce` WRITE;
/*!40000 ALTER TABLE `questions_dce` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions_dce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `renseignements_boamp`
--

LOCK TABLES `renseignements_boamp` WRITE;
/*!40000 ALTER TABLE `renseignements_boamp` DISABLE KEYS */;
/*!40000 ALTER TABLE `renseignements_boamp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `responsableengagement`
--

LOCK TABLES `responsableengagement` WRITE;
/*!40000 ALTER TABLE `responsableengagement` DISABLE KEYS */;
/*!40000 ALTER TABLE `responsableengagement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `resultat_analyse`
--

LOCK TABLES `resultat_analyse` WRITE;
/*!40000 ALTER TABLE `resultat_analyse` DISABLE KEYS */;
/*!40000 ALTER TABLE `resultat_analyse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `resultat_analyse_decision`
--

LOCK TABLES `resultat_analyse_decision` WRITE;
/*!40000 ALTER TABLE `resultat_analyse_decision` DISABLE KEYS */;
/*!40000 ALTER TABLE `resultat_analyse_decision` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sso_agent`
--

LOCK TABLES `sso_agent` WRITE;
/*!40000 ALTER TABLE `sso_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `sso_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sso_entreprise`
--

LOCK TABLES `sso_entreprise` WRITE;
/*!40000 ALTER TABLE `sso_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `sso_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `supervision_interface`
--

LOCK TABLES `supervision_interface` WRITE;
/*!40000 ALTER TABLE `supervision_interface` DISABLE KEYS */;
/*!40000 ALTER TABLE `supervision_interface` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Commission`
--

LOCK TABLES `t_CAO_Commission` WRITE;
/*!40000 ALTER TABLE `t_CAO_Commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Commission_Agent`
--

LOCK TABLES `t_CAO_Commission_Agent` WRITE;
/*!40000 ALTER TABLE `t_CAO_Commission_Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Commission_Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Commission_Consultation`
--

LOCK TABLES `t_CAO_Commission_Consultation` WRITE;
/*!40000 ALTER TABLE `t_CAO_Commission_Consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Commission_Consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Commission_Intervenant_Externe`
--

LOCK TABLES `t_CAO_Commission_Intervenant_Externe` WRITE;
/*!40000 ALTER TABLE `t_CAO_Commission_Intervenant_Externe` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Commission_Intervenant_Externe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Intervenant_Externe`
--

LOCK TABLES `t_CAO_Intervenant_Externe` WRITE;
/*!40000 ALTER TABLE `t_CAO_Intervenant_Externe` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Intervenant_Externe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Ordre_De_Passage`
--

LOCK TABLES `t_CAO_Ordre_De_Passage` WRITE;
/*!40000 ALTER TABLE `t_CAO_Ordre_De_Passage` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Ordre_De_Passage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Ordre_Du_Jour`
--

LOCK TABLES `t_CAO_Ordre_Du_Jour` WRITE;
/*!40000 ALTER TABLE `t_CAO_Ordre_Du_Jour` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Ordre_Du_Jour` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Ordre_Du_Jour_Intervenant`
--

LOCK TABLES `t_CAO_Ordre_Du_Jour_Intervenant` WRITE;
/*!40000 ALTER TABLE `t_CAO_Ordre_Du_Jour_Intervenant` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Ordre_Du_Jour_Intervenant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Seance`
--

LOCK TABLES `t_CAO_Seance` WRITE;
/*!40000 ALTER TABLE `t_CAO_Seance` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Seance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Seance_Agent`
--

LOCK TABLES `t_CAO_Seance_Agent` WRITE;
/*!40000 ALTER TABLE `t_CAO_Seance_Agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Seance_Agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Seance_Intervenant_Externe`
--

LOCK TABLES `t_CAO_Seance_Intervenant_Externe` WRITE;
/*!40000 ALTER TABLE `t_CAO_Seance_Intervenant_Externe` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Seance_Intervenant_Externe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_CAO_Seance_Invite`
--

LOCK TABLES `t_CAO_Seance_Invite` WRITE;
/*!40000 ALTER TABLE `t_CAO_Seance_Invite` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_CAO_Seance_Invite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_Offre_Support_Publicite`
--

LOCK TABLES `t_Offre_Support_Publicite` WRITE;
/*!40000 ALTER TABLE `t_Offre_Support_Publicite` DISABLE KEYS */;
INSERT INTO `t_Offre_Support_Publicite` (`id`, `libelle_offre`, `id_support`, `actif`, `logo`, `mapa`, `montant_inf`, `montant_max`, `code`, `ordre`) VALUES (8,'Formulaire JOUE 2-FR (Avis de marché)',3,'1','NULL',2,0,0,'',NULL),(9,'Avis initial - Marché > 90K&euro;',1,'1','NULL',2,90000,0,'',NULL),(10,'Formulaire JOUE 2-FR (Avis de marché)',6,'1','NULL',2,0,0,'',NULL),(11,'Formulaire MAPA BOAMP',1,'1','NULL',2,0,0,'',NULL),(12,'Formulaire National Standard',1,'1','NULL',2,0,0,'',NULL),(13,'Formulaire JOUE 5-FR (Avis de marché - Secteurs spéciaux)',3,'1','NULL',2,0,0,'',NULL),(14,'Formulaire JOUE 12-FR (Avis de concours)',3,'1','NULL',2,0,0,'',NULL),(15,'Formulaire JOUE 21-FR (Avis de marché - Services sociaux et autres services spécifiques)',3,'1','NULL',2,0,0,'',NULL),(17,'Formulaire JOUE 24-FR (Avis de concession) pour publication européenne',3,'1','NULL',2,0,0,'',NULL),(18,'Formulaire JOUE 5-FR (Avis de marché - Secteurs spéciaux)',6,'1','NULL',2,0,0,'',NULL),(19,'Formulaire JOUE 12-FR (Avis de concours)',6,'1','NULL',2,0,0,'',NULL),(20,'Formulaire JOUE 21-FR (Avis de marché - Services sociaux et autres services spécifiques)',6,'1','NULL',2,0,0,'',NULL),(21,'Formulaire JOUE 24-FR (Avis de concession) pour publication européenne',6,'1','NULL',2,0,0,'',NULL),(24,'Avis initial - Marché > 90K&euro;',4,'1',NULL,2,90000,0,'',NULL),(30,'Avis initial - Marché < 90K&euro;',4,'1',NULL,2,0,90000,'',NULL),(31,'Avis initial - Marché > 90K&euro; (8 UP)',7,'1','NULL',2,90000,0,'',NULL),(32,'Avis initial - Marché < 90K&euro; (8 UP)',7,'1','NULL',2,0,90000,'',NULL),(33,'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)',2,'1',NULL,1,0,90000,'17_LESECHOS',0),(34,'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)',2,'1',NULL,2,90000,0,'18_LESECHOS',0),(35,'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)',2,'1',NULL,0,0,0,'19_LESECHOS',3),(36,'Avis de marché < 90 K€ - 1 dpt - avis résumé (300 € ou 5 UP)',5,'1',NULL,1,0,90000,'22_LEPARISIEN',0),(37,'Avis de marché >= 90 K€ - 1 dpt (550 € ou 10 UP)',5,'1',NULL,2,90000,0,'23_LEPARISIEN',0),(38,'Avis de DSP / Concessions - 1 dpt  (990 € ou 20 UP)',5,'1',NULL,0,0,0,'24_LEPARISIEN',4),(40,'Avis de pré-information (1UP, cf tarif pack)',8,'1',NULL,0,0,0,'28_EMARCHEPUBLIC',3),(41,'Avis de marché < 90 K&euro;   (1UP, cf tarif pack)',8,'1',NULL,1,0,90000,'29_EMARCHEPUBLIC',0),(42,'Avis de marché >= 90 K€  (8 UP, cf tarif pack)',8,'1',NULL,2,90000,0,'30_EMARCHEPUBLIC',0),(43,'Autre avis - Les Echos (sur devis)',2,'1','NULL',2,0,0,'35_LESECHOS',0),(44,'Autre avis - Le Parisien (sur devis)',5,'1','NULL',2,0,0,'36_LEPARISIEN',0),(45,'Autre avis - E-marchespublics.com (8 UP, cf tarif pack)',8,'1',NULL,2,0,0,'37_EMARCHEPUBLIC',0),(46,'Avis de marché >=  90 K€ - Les Echos + site E-marchespublics (20 UP, cf tarif pack)',18,'1',NULL,2,90000,0,'31_LESECHOSBIMEDIA',0),(47,'Avis de marché < 90 K€ - Les Echos (avis résumé) + E-marchespublics.com  (6 UP, cf tarif pack)',18,'1',NULL,2,0,90000,'32_LESECHOSBIMEDIA',0),(48,'Avis de marché >= 90 K€ - Le Parisien + E-marchespublics.com (18 UP, cf tarif pack)',19,'1',NULL,2,90000,0,'33_LEPARISIENBIMEDIA',0),(49,'Avis de marché < 90 K€ - Le Parisien (avis résumé) + E-marchespublics.com  (6 UP, cf tarif pack)',19,'1',NULL,2,0,90000,'34_LEPARISIENBIMEDIA',0),(50,'Avis résumé au MM (70/2 cols maxi) - 1 unité (par lieu d\'exécution sélectionné)',9,'1','NULL',2,0,90000,'SO_Inf90_avisResume',4),(51,'Texte intégral - SUR DEVIS',9,'1','NULL',2,0,90000,'SO_Inf90_texteIntegralDevis',5),(52,'Avis intégral au MM (160/2 cols maxi) - 6 unités (par lieu d\'exécution sélectionné)',9,'1','NULL',2,90000,0,'SO_Sup90_avisIntegral',1),(53,'Avis résumé au MM (80/2 cols maxi) - 3 unités (par lieu d\'exécution sélectionné)',9,'1','NULL',2,90000,0,'SO_Sup90_avisResume',2),(54,'Avis à la ligne - SUR DEVIS',9,'1','NULL',2,90000,0,'SO_Sup90_ligneDevis',3),(55,'Avis intégral au MM (160/2 cols maxi) - 6 unités (par lieu d\'exécution sélectionné)',10,'1',NULL,2,90000,0,'CL_Sup90_avisIntegral',1),(56,'Avis résumé au MM (80/2 cols maxi) - 3 unités (par lieu d\'exécution sélectionné)',10,'1',NULL,2,90000,0,'CL_Sup90_avisResume',2),(57,'Avis au mm - SUR DEVIS',10,'1',NULL,2,90000,0,'CL_Sup90_ligneDevis',3),(58,'Avis résumé au MM  (70/2 cols maxi) - 1 unité (par lieu d\'exécution sélectionné)',10,'1',NULL,2,0,90000,'CL_Inf90_avisResume',4),(59,'Texte intégral - SUR DEVIS',10,'1',NULL,2,0,90000,'CL_Inf90_texteIntegralDevis',5),(60,'Avis intégral au MM (160/2 cols maxi) - 9 unités (par lieu d\'exécution sélectionné)',11,'1',NULL,2,90000,0,'CL_SO_Sup90_avisIntegral',1),(61,'Avis résumé au MM (80/2 cols maxi) - 5 unités (par lieu d\'exécution sélectionné)',11,'1',NULL,2,90000,0,'CL_SO_Sup90_avisResume',2),(62,'Avis à la ligne - SUR DEVIS',11,'1',NULL,2,90000,0,'CL_SO_Sup90_ligneDevis',3),(63,'Avis résumé au MM (70/2 cols maxi) - 2 unités (par lieu d\'exécution sélectionné)',11,'1',NULL,2,0,90000,'CL_SO_Inf90_avisResume',4),(64,'Texte intégral - SUR DEVIS',11,'1',NULL,2,0,90000,'CL_SO_Inf90_texteIntegralDevis',5),(65,'Avis intégral au MM (160/2 cols maxi) - 9 unités (par lieu d\'exécution sélectionné)',12,'1',NULL,2,90000,0,'SO_DL_Sup90_avisIntegral',1),(66,'Avis résumé au MM (80/2 cols maxi) - 5 unités (par lieu d\'exécution sélectionné)',12,'1',NULL,2,90000,0,'SO_DL_Sup90_avisResume',2),(67,'Avis à la ligne - SUR DEVIS',12,'1',NULL,2,90000,0,'SO_DL_Sup90_ligneDevis',3),(68,'Avis résumé au MM (70/2 cols maxi) - 2 unités (par lieu d\'exécution sélectionné)',12,'1',NULL,2,0,90000,'SO_DL_Inf90_avisResume',4),(69,'Texte intégral - SUR DEVIS',12,'1',NULL,2,0,90000,'SO_DL_Inf90_texteIntegralDevis',5),(70,'Avis intégral au MM (160/2 cols maxi) - 11 unités (par lieu d\'exécution sélectionné)',13,'1',NULL,2,90000,0,'SO_RP_EP_Sup90_avisIntegral',1),(71,'Avis résumé au MM (80/2 cols maxi) - 8 unités (par lieu d\'exécution sélectionné)',13,'1',NULL,2,90000,0,'SO_RP_EP_Sup90_avisResume',2),(72,'Avis à la ligne - SUR DEVIS',13,'1',NULL,2,90000,0,'SO_RP_EP_Sup90_ligneDevis',3),(73,'Avis résumé au MM (70/2 cols maxi) - 3 unités (par lieu d\'exécution sélectionné)',13,'1',NULL,2,0,90000,'SO_RP_EP_Inf90_avisResume',4),(74,'Texte intégral - SUR DEVIS',13,'1',NULL,2,0,90000,'SO_RP_EP_Inf90_texteIntegralDevis',5),(75,'Avis intégral au MM (160/2 cols maxi) - 7 unités (par lieu d\'exécution sélectionné)',14,'1',NULL,2,90000,0,'LM_Sup90_avisIntegral',1),(76,'Avis résumé au MM (80/2 cols maxi) - 4 unités (par lieu d\'exécution sélectionné)',14,'1',NULL,2,90000,0,'LM_Sup90_avisResume',2),(77,'Avis à la ligne - SUR DEVIS',14,'1',NULL,2,90000,0,'LM_Sup90_ligneDevis',3),(78,'Avis résumé au MM (70/2 cols maxi) - 2 unités (par lieu d\'exécution sélectionné)',14,'1',NULL,2,0,90000,'LM_Inf90_avisResume',4),(79,'Texte intégral - SUR DEVIS',14,'1',NULL,2,0,90000,'LM_Inf90_texteIntegralDevis',5),(80,'Avis intégral au MM (160/2 cols maxi) - 7 unités (par lieu d\'exécution sélectionné)',15,'1',NULL,2,90000,0,'LNR_Sup90_avisIntegral',1),(81,'Avis résumé au MM (80/2 cols maxi) - 4 unités (par lieu d\'exécution sélectionné)',15,'1',NULL,2,90000,0,'LNR_Sup90_avisResume',2),(82,'Avis à la ligne - SUR DEVIS',15,'1',NULL,2,90000,0,'LNR_Sup90_ligneDevis',3),(83,'Avis résumé au MM (70/2 cols maxi) - 2 unités (par lieu d\'exécution sélectionné)',15,'1',NULL,2,0,90000,'LNR_Inf90_avisResume',4),(84,'Avis intégral au MM (160/2 cols maxi) - 7 unités (par lieu d\'exécution sélectionné)',16,'1',NULL,2,90000,0,'LPC_Sup90_avisIntegral',1),(85,'Avis résumé au MM (80/2 cols maxi) - 4 unités (par lieu d\'exécution sélectionné)',16,'1',NULL,2,90000,0,'LPC_Sup90_avisResume',2),(86,'Avis à la ligne - SUR DEVIS',16,'1',NULL,2,90000,0,'LPC_Sup90_ligneDevis',3),(87,'Avis résumé au MM (70/2 cols maxi) - 2 unités (par lieu d\'exécution sélectionné)',16,'1',NULL,2,0,90000,'LPC_Inf90_avisResume',4),(88,'Annonce marché < 90 K&euro; - Print + web (300&euro;)',17,'1','NULL',2,0,90000,'JBTP_Inf90_annonceCompPrintWeb',1),(89,'Annonce marché >= 90 K&euro; - Print + web (500&euro;)',17,'1','NULL',2,90000,0,'JBTP_Sup90_annonceCompPrintWeb',2),(91,'< 90 K&euro; = 2 UP',20,'1','NULL',2,0,90000,'LAL_Inf90_avisResume',1),(92,'> 90 K&euro; = 6 UP',20,'1','NULL',2,90000,0,'LAL_Sup90_avisIntegral',2);
/*!40000 ALTER TABLE `t_Offre_Support_Publicite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_access_token`
--

LOCK TABLES `t_access_token` WRITE;
/*!40000 ALTER TABLE `t_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_access_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_annonce_consultation`
--

LOCK TABLES `t_annonce_consultation` WRITE;
/*!40000 ALTER TABLE `t_annonce_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_annonce_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_attestations_offres`
--

LOCK TABLES `t_attestations_offres` WRITE;
/*!40000 ALTER TABLE `t_attestations_offres` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_attestations_offres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_bourse_cotraitance`
--

LOCK TABLES `t_bourse_cotraitance` WRITE;
/*!40000 ALTER TABLE `t_bourse_cotraitance` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_bourse_cotraitance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_calendrier`
--

LOCK TABLES `t_calendrier` WRITE;
/*!40000 ALTER TABLE `t_calendrier` DISABLE KEYS */;
INSERT INTO `t_calendrier` (`ID_CALENDRIER`, `REFERENCE`, `ORGANISME`) VALUES (1,2,'a1a');
/*!40000 ALTER TABLE `t_calendrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_calendrier_etape`
--

LOCK TABLES `t_calendrier_etape` WRITE;
/*!40000 ALTER TABLE `t_calendrier_etape` DISABLE KEYS */;
INSERT INTO `t_calendrier_etape` (`ID_CALENDRIER_ETAPE`, `ID_CALENDRIER`, `CODE`, `LIBELLE`, `DATE_INITIALE`, `DATE_PREVUE`, `POSITION`, `LIBRE`, `DATE_REELLE_CONFIRMEE`) VALUES (1,1,'E1','Date de lancement',NULL,NULL,0,0,'0'),(2,1,'E2','Date limite de remise des plis',NULL,NULL,1,0,'0'),(3,1,'E3','Ouverture des plis',NULL,NULL,2,0,'0'),(4,1,'E4','Analyse des réponses',NULL,NULL,3,0,'0'),(5,1,'E5','Décision / Attribution',NULL,NULL,4,0,'0'),(6,1,'E6','Information des soumissionnaires non retenus',NULL,NULL,5,0,'0'),(7,1,'E7','Notification de la décision',NULL,NULL,6,0,'0'),(8,1,'E8','Avis d\'attribution',NULL,NULL,7,0,'0');
/*!40000 ALTER TABLE `t_calendrier_etape` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_calendrier_etape_referentiel`
--

LOCK TABLES `t_calendrier_etape_referentiel` WRITE;
/*!40000 ALTER TABLE `t_calendrier_etape_referentiel` DISABLE KEYS */;
INSERT INTO `t_calendrier_etape_referentiel` (`ID_ETAPE_CALENDRIER_REFERENTIEL`, `ID_TYPE_PROCEDURE`, `ORGANISME`, `CODE`, `LIBELLE`, `POSITION`) VALUES (101,1,'a1a','E1','Date de lancement',0),(101,1,'a1t','E1','Date de lancement',0),(101,1,'a2z','E1','Date de lancement',0),(101,1,'e3r','E1','Date de lancement',0),(101,2,'a1a','E1','Date de lancement',0),(101,2,'a1t','E1','Date de lancement',0),(101,2,'a2z','E1','Date de lancement',0),(101,2,'e3r','E1','Date de lancement',0),(101,3,'a1a','E1','Date de lancement',0),(101,3,'a1t','E1','Date de lancement',0),(101,3,'a2z','E1','Date de lancement',0),(101,3,'e3r','E1','Date de lancement',0),(101,4,'a1a','E1','Date de lancement',0),(101,4,'a1t','E1','Date de lancement',0),(101,4,'a2z','E1','Date de lancement',0),(101,4,'e3r','E1','Date de lancement',0),(101,5,'a1a','E1','Date de lancement',0),(101,5,'a1t','E1','Date de lancement',0),(101,5,'a2z','E1','Date de lancement',0),(101,5,'e3r','E1','Date de lancement',0),(101,6,'a1a','E1','Date de lancement',0),(101,6,'a1t','E1','Date de lancement',0),(101,6,'a2z','E1','Date de lancement',0),(101,6,'e3r','E1','Date de lancement',0),(101,7,'a1a','E1','Date de lancement',0),(101,7,'a1t','E1','Date de lancement',0),(101,7,'a2z','E1','Date de lancement',0),(101,7,'e3r','E1','Date de lancement',0),(101,8,'a1a','E1','Date de lancement',0),(101,8,'a1t','E1','Date de lancement',0),(101,8,'a2z','E1','Date de lancement',0),(101,8,'e3r','E1','Date de lancement',0),(101,10,'a1a','E1','Date de lancement',0),(101,10,'a1t','E1','Date de lancement',0),(101,10,'a2z','E1','Date de lancement',0),(101,10,'e3r','E1','Date de lancement',0),(101,11,'a1a','E1','Date de lancement',0),(101,11,'a1t','E1','Date de lancement',0),(101,11,'a2z','E1','Date de lancement',0),(101,11,'e3r','E1','Date de lancement',0),(101,12,'a1a','E1','Date de lancement',0),(101,12,'a1t','E1','Date de lancement',0),(101,12,'a2z','E1','Date de lancement',0),(101,12,'e3r','E1','Date de lancement',0),(101,17,'a1a','E1','Date de lancement',0),(101,17,'a1t','E1','Date de lancement',0),(101,17,'a2z','E1','Date de lancement',0),(101,17,'e3r','E1','Date de lancement',0),(101,18,'a1a','E1','Date de lancement',0),(101,18,'a1t','E1','Date de lancement',0),(101,18,'a2z','E1','Date de lancement',0),(101,18,'e3r','E1','Date de lancement',0),(101,30,'a1a','E1','Date de lancement',0),(101,30,'a1t','E1','Date de lancement',0),(101,30,'a2z','E1','Date de lancement',0),(101,30,'e3r','E1','Date de lancement',0),(101,31,'a1a','E1','Date de lancement',0),(101,31,'a1t','E1','Date de lancement',0),(101,31,'a2z','E1','Date de lancement',0),(101,31,'e3r','E1','Date de lancement',0),(101,32,'a1a','E1','Date de lancement',0),(101,32,'a1t','E1','Date de lancement',0),(101,32,'a2z','E1','Date de lancement',0),(101,32,'e3r','E1','Date de lancement',0),(101,33,'a1a','E1','Date de lancement',0),(101,33,'a1t','E1','Date de lancement',0),(101,33,'a2z','E1','Date de lancement',0),(101,33,'e3r','E1','Date de lancement',0),(101,1000,'a1a','E1','Date de lancement',0),(101,1000,'a1t','E1','Date de lancement',0),(101,1000,'a2z','E1','Date de lancement',0),(101,1000,'e3r','E1','Date de lancement',0),(101,1004,'a1a','E1','Date de lancement',0),(101,1004,'a1t','E1','Date de lancement',0),(101,1004,'a2z','E1','Date de lancement',0),(101,1004,'e3r','E1','Date de lancement',0),(102,1,'a1a','E2','Date limite de remise des plis',1),(102,1,'a1t','E2','Date limite de remise des plis',1),(102,1,'a2z','E2','Date limite de remise des plis',1),(102,1,'e3r','E2','Date limite de remise des plis',1),(102,2,'a1a','E2','Date limite de remise des plis',1),(102,2,'a1t','E2','Date limite de remise des plis',1),(102,2,'a2z','E2','Date limite de remise des plis',1),(102,2,'e3r','E2','Date limite de remise des plis',1),(102,3,'a1a','E2','Date limite de remise des plis',1),(102,3,'a1t','E2','Date limite de remise des plis',1),(102,3,'a2z','E2','Date limite de remise des plis',1),(102,3,'e3r','E2','Date limite de remise des plis',1),(102,4,'a1a','E2','Date limite de remise des plis',1),(102,4,'a1t','E2','Date limite de remise des plis',1),(102,4,'a2z','E2','Date limite de remise des plis',1),(102,4,'e3r','E2','Date limite de remise des plis',1),(102,5,'a1a','E2','Date limite de remise des plis',1),(102,5,'a1t','E2','Date limite de remise des plis',1),(102,5,'a2z','E2','Date limite de remise des plis',1),(102,5,'e3r','E2','Date limite de remise des plis',1),(102,6,'a1a','E2','Date limite de remise des plis',1),(102,6,'a1t','E2','Date limite de remise des plis',1),(102,6,'a2z','E2','Date limite de remise des plis',1),(102,6,'e3r','E2','Date limite de remise des plis',1),(102,7,'a1a','E2','Date limite de remise des plis',1),(102,7,'a1t','E2','Date limite de remise des plis',1),(102,7,'a2z','E2','Date limite de remise des plis',1),(102,7,'e3r','E2','Date limite de remise des plis',1),(102,8,'a1a','E2','Date limite de remise des plis',1),(102,8,'a1t','E2','Date limite de remise des plis',1),(102,8,'a2z','E2','Date limite de remise des plis',1),(102,8,'e3r','E2','Date limite de remise des plis',1),(102,10,'a1a','E2','Date limite de remise des plis',1),(102,10,'a1t','E2','Date limite de remise des plis',1),(102,10,'a2z','E2','Date limite de remise des plis',1),(102,10,'e3r','E2','Date limite de remise des plis',1),(102,11,'a1a','E2','Date limite de remise des plis',1),(102,11,'a1t','E2','Date limite de remise des plis',1),(102,11,'a2z','E2','Date limite de remise des plis',1),(102,11,'e3r','E2','Date limite de remise des plis',1),(102,12,'a1a','E2','Date limite de remise des plis',1),(102,12,'a1t','E2','Date limite de remise des plis',1),(102,12,'a2z','E2','Date limite de remise des plis',1),(102,12,'e3r','E2','Date limite de remise des plis',1),(102,17,'a1a','E2','Date limite de remise des plis',1),(102,17,'a1t','E2','Date limite de remise des plis',1),(102,17,'a2z','E2','Date limite de remise des plis',1),(102,17,'e3r','E2','Date limite de remise des plis',1),(102,18,'a1a','E2','Date limite de remise des plis',1),(102,18,'a1t','E2','Date limite de remise des plis',1),(102,18,'a2z','E2','Date limite de remise des plis',1),(102,18,'e3r','E2','Date limite de remise des plis',1),(102,30,'a1a','E2','Date limite de remise des plis',1),(102,30,'a1t','E2','Date limite de remise des plis',1),(102,30,'a2z','E2','Date limite de remise des plis',1),(102,30,'e3r','E2','Date limite de remise des plis',1),(102,31,'a1a','E2','Date limite de remise des plis',1),(102,31,'a1t','E2','Date limite de remise des plis',1),(102,31,'a2z','E2','Date limite de remise des plis',1),(102,31,'e3r','E2','Date limite de remise des plis',1),(102,32,'a1a','E2','Date limite de remise des plis',1),(102,32,'a1t','E2','Date limite de remise des plis',1),(102,32,'a2z','E2','Date limite de remise des plis',1),(102,32,'e3r','E2','Date limite de remise des plis',1),(102,33,'a1a','E2','Date limite de remise des plis',1),(102,33,'a1t','E2','Date limite de remise des plis',1),(102,33,'a2z','E2','Date limite de remise des plis',1),(102,33,'e3r','E2','Date limite de remise des plis',1),(102,1000,'a1a','E2','Date limite de remise des plis',1),(102,1000,'a1t','E2','Date limite de remise des plis',1),(102,1000,'a2z','E2','Date limite de remise des plis',1),(102,1000,'e3r','E2','Date limite de remise des plis',1),(102,1004,'a1a','E2','Date limite de remise des plis',1),(102,1004,'a1t','E2','Date limite de remise des plis',1),(102,1004,'a2z','E2','Date limite de remise des plis',1),(102,1004,'e3r','E2','Date limite de remise des plis',1),(103,1,'a1a','E3','Ouverture des plis',2),(103,1,'a1t','E3','Ouverture des plis',2),(103,1,'a2z','E3','Ouverture des plis',2),(103,1,'e3r','E3','Ouverture des plis',2),(103,2,'a1a','E3','Ouverture des plis',2),(103,2,'a1t','E3','Ouverture des plis',2),(103,2,'a2z','E3','Ouverture des plis',2),(103,2,'e3r','E3','Ouverture des plis',2),(103,3,'a1a','E3','Ouverture des plis',2),(103,3,'a1t','E3','Ouverture des plis',2),(103,3,'a2z','E3','Ouverture des plis',2),(103,3,'e3r','E3','Ouverture des plis',2),(103,4,'a1a','E3','Ouverture des plis',2),(103,4,'a1t','E3','Ouverture des plis',2),(103,4,'a2z','E3','Ouverture des plis',2),(103,4,'e3r','E3','Ouverture des plis',2),(103,5,'a1a','E3','Ouverture des plis',2),(103,5,'a1t','E3','Ouverture des plis',2),(103,5,'a2z','E3','Ouverture des plis',2),(103,5,'e3r','E3','Ouverture des plis',2),(103,6,'a1a','E3','Ouverture des plis',2),(103,6,'a1t','E3','Ouverture des plis',2),(103,6,'a2z','E3','Ouverture des plis',2),(103,6,'e3r','E3','Ouverture des plis',2),(103,7,'a1a','E3','Ouverture des plis',2),(103,7,'a1t','E3','Ouverture des plis',2),(103,7,'a2z','E3','Ouverture des plis',2),(103,7,'e3r','E3','Ouverture des plis',2),(103,8,'a1a','E3','Ouverture des plis',2),(103,8,'a1t','E3','Ouverture des plis',2),(103,8,'a2z','E3','Ouverture des plis',2),(103,8,'e3r','E3','Ouverture des plis',2),(103,10,'a1a','E3','Ouverture des plis',2),(103,10,'a1t','E3','Ouverture des plis',2),(103,10,'a2z','E3','Ouverture des plis',2),(103,10,'e3r','E3','Ouverture des plis',2),(103,11,'a1a','E3','Ouverture des plis',2),(103,11,'a1t','E3','Ouverture des plis',2),(103,11,'a2z','E3','Ouverture des plis',2),(103,11,'e3r','E3','Ouverture des plis',2),(103,12,'a1a','E3','Ouverture des plis',2),(103,12,'a1t','E3','Ouverture des plis',2),(103,12,'a2z','E3','Ouverture des plis',2),(103,12,'e3r','E3','Ouverture des plis',2),(103,17,'a1a','E3','Ouverture des plis',2),(103,17,'a1t','E3','Ouverture des plis',2),(103,17,'a2z','E3','Ouverture des plis',2),(103,17,'e3r','E3','Ouverture des plis',2),(103,18,'a1a','E3','Ouverture des plis',2),(103,18,'a1t','E3','Ouverture des plis',2),(103,18,'a2z','E3','Ouverture des plis',2),(103,18,'e3r','E3','Ouverture des plis',2),(103,30,'a1a','E3','Ouverture des plis',2),(103,30,'a1t','E3','Ouverture des plis',2),(103,30,'a2z','E3','Ouverture des plis',2),(103,30,'e3r','E3','Ouverture des plis',2),(103,31,'a1a','E3','Ouverture des plis',2),(103,31,'a1t','E3','Ouverture des plis',2),(103,31,'a2z','E3','Ouverture des plis',2),(103,31,'e3r','E3','Ouverture des plis',2),(103,32,'a1a','E3','Ouverture des plis',2),(103,32,'a1t','E3','Ouverture des plis',2),(103,32,'a2z','E3','Ouverture des plis',2),(103,32,'e3r','E3','Ouverture des plis',2),(103,33,'a1a','E3','Ouverture des plis',2),(103,33,'a1t','E3','Ouverture des plis',2),(103,33,'a2z','E3','Ouverture des plis',2),(103,33,'e3r','E3','Ouverture des plis',2),(103,1000,'a1a','E3','Ouverture des plis',2),(103,1000,'a1t','E3','Ouverture des plis',2),(103,1000,'a2z','E3','Ouverture des plis',2),(103,1000,'e3r','E3','Ouverture des plis',2),(103,1004,'a1a','E3','Ouverture des plis',2),(103,1004,'a1t','E3','Ouverture des plis',2),(103,1004,'a2z','E3','Ouverture des plis',2),(103,1004,'e3r','E3','Ouverture des plis',2),(104,1,'a1a','E4','Analyse des réponses',3),(104,1,'a1t','E4','Analyse des réponses',3),(104,1,'a2z','E4','Analyse des réponses',3),(104,1,'e3r','E4','Analyse des réponses',3),(104,2,'a1a','E4','Analyse des réponses',3),(104,2,'a1t','E4','Analyse des réponses',3),(104,2,'a2z','E4','Analyse des réponses',3),(104,2,'e3r','E4','Analyse des réponses',3),(104,3,'a1a','E4','Analyse des réponses',3),(104,3,'a1t','E4','Analyse des réponses',3),(104,3,'a2z','E4','Analyse des réponses',3),(104,3,'e3r','E4','Analyse des réponses',3),(104,4,'a1a','E4','Analyse des réponses',3),(104,4,'a1t','E4','Analyse des réponses',3),(104,4,'a2z','E4','Analyse des réponses',3),(104,4,'e3r','E4','Analyse des réponses',3),(104,5,'a1a','E4','Analyse des réponses',3),(104,5,'a1t','E4','Analyse des réponses',3),(104,5,'a2z','E4','Analyse des réponses',3),(104,5,'e3r','E4','Analyse des réponses',3),(104,6,'a1a','E4','Analyse des réponses',3),(104,6,'a1t','E4','Analyse des réponses',3),(104,6,'a2z','E4','Analyse des réponses',3),(104,6,'e3r','E4','Analyse des réponses',3),(104,7,'a1a','E4','Analyse des réponses',3),(104,7,'a1t','E4','Analyse des réponses',3),(104,7,'a2z','E4','Analyse des réponses',3),(104,7,'e3r','E4','Analyse des réponses',3),(104,8,'a1a','E4','Analyse des réponses',3),(104,8,'a1t','E4','Analyse des réponses',3),(104,8,'a2z','E4','Analyse des réponses',3),(104,8,'e3r','E4','Analyse des réponses',3),(104,10,'a1a','E4','Analyse des réponses',3),(104,10,'a1t','E4','Analyse des réponses',3),(104,10,'a2z','E4','Analyse des réponses',3),(104,10,'e3r','E4','Analyse des réponses',3),(104,11,'a1a','E4','Analyse des réponses',3),(104,11,'a1t','E4','Analyse des réponses',3),(104,11,'a2z','E4','Analyse des réponses',3),(104,11,'e3r','E4','Analyse des réponses',3),(104,12,'a1a','E4','Analyse des réponses',3),(104,12,'a1t','E4','Analyse des réponses',3),(104,12,'a2z','E4','Analyse des réponses',3),(104,12,'e3r','E4','Analyse des réponses',3),(104,17,'a1a','E4','Analyse des réponses',3),(104,17,'a1t','E4','Analyse des réponses',3),(104,17,'a2z','E4','Analyse des réponses',3),(104,17,'e3r','E4','Analyse des réponses',3),(104,18,'a1a','E4','Analyse des réponses',3),(104,18,'a1t','E4','Analyse des réponses',3),(104,18,'a2z','E4','Analyse des réponses',3),(104,18,'e3r','E4','Analyse des réponses',3),(104,30,'a1a','E4','Analyse des réponses',3),(104,30,'a1t','E4','Analyse des réponses',3),(104,30,'a2z','E4','Analyse des réponses',3),(104,30,'e3r','E4','Analyse des réponses',3),(104,31,'a1a','E4','Analyse des réponses',3),(104,31,'a1t','E4','Analyse des réponses',3),(104,31,'a2z','E4','Analyse des réponses',3),(104,31,'e3r','E4','Analyse des réponses',3),(104,32,'a1a','E4','Analyse des réponses',3),(104,32,'a1t','E4','Analyse des réponses',3),(104,32,'a2z','E4','Analyse des réponses',3),(104,32,'e3r','E4','Analyse des réponses',3),(104,33,'a1a','E4','Analyse des réponses',3),(104,33,'a1t','E4','Analyse des réponses',3),(104,33,'a2z','E4','Analyse des réponses',3),(104,33,'e3r','E4','Analyse des réponses',3),(104,1000,'a1a','E4','Analyse des réponses',3),(104,1000,'a1t','E4','Analyse des réponses',3),(104,1000,'a2z','E4','Analyse des réponses',3),(104,1000,'e3r','E4','Analyse des réponses',3),(104,1004,'a1a','E4','Analyse des réponses',3),(104,1004,'a1t','E4','Analyse des réponses',3),(104,1004,'a2z','E4','Analyse des réponses',3),(104,1004,'e3r','E4','Analyse des réponses',3),(105,1,'a1a','E5','Décision / Attribution',4),(105,1,'a1t','E5','Décision / Attribution',4),(105,1,'a2z','E5','Décision / Attribution',4),(105,1,'e3r','E5','Décision / Attribution',4),(105,2,'a1a','E5','Décision / Attribution',4),(105,2,'a1t','E5','Décision / Attribution',4),(105,2,'a2z','E5','Décision / Attribution',4),(105,2,'e3r','E5','Décision / Attribution',4),(105,3,'a1a','E5','Décision / Attribution',4),(105,3,'a1t','E5','Décision / Attribution',4),(105,3,'a2z','E5','Décision / Attribution',4),(105,3,'e3r','E5','Décision / Attribution',4),(105,4,'a1a','E5','Décision / Attribution',4),(105,4,'a1t','E5','Décision / Attribution',4),(105,4,'a2z','E5','Décision / Attribution',4),(105,4,'e3r','E5','Décision / Attribution',4),(105,5,'a1a','E5','Décision / Attribution',4),(105,5,'a1t','E5','Décision / Attribution',4),(105,5,'a2z','E5','Décision / Attribution',4),(105,5,'e3r','E5','Décision / Attribution',4),(105,6,'a1a','E5','Décision / Attribution',4),(105,6,'a1t','E5','Décision / Attribution',4),(105,6,'a2z','E5','Décision / Attribution',4),(105,6,'e3r','E5','Décision / Attribution',4),(105,7,'a1a','E5','Décision / Attribution',4),(105,7,'a1t','E5','Décision / Attribution',4),(105,7,'a2z','E5','Décision / Attribution',4),(105,7,'e3r','E5','Décision / Attribution',4),(105,8,'a1a','E5','Décision / Attribution',4),(105,8,'a1t','E5','Décision / Attribution',4),(105,8,'a2z','E5','Décision / Attribution',4),(105,8,'e3r','E5','Décision / Attribution',4),(105,10,'a1a','E5','Décision / Attribution',4),(105,10,'a1t','E5','Décision / Attribution',4),(105,10,'a2z','E5','Décision / Attribution',4),(105,10,'e3r','E5','Décision / Attribution',4),(105,11,'a1a','E5','Décision / Attribution',4),(105,11,'a1t','E5','Décision / Attribution',4),(105,11,'a2z','E5','Décision / Attribution',4),(105,11,'e3r','E5','Décision / Attribution',4),(105,12,'a1a','E5','Décision / Attribution',4),(105,12,'a1t','E5','Décision / Attribution',4),(105,12,'a2z','E5','Décision / Attribution',4),(105,12,'e3r','E5','Décision / Attribution',4),(105,17,'a1a','E5','Décision / Attribution',4),(105,17,'a1t','E5','Décision / Attribution',4),(105,17,'a2z','E5','Décision / Attribution',4),(105,17,'e3r','E5','Décision / Attribution',4),(105,18,'a1a','E5','Décision / Attribution',4),(105,18,'a1t','E5','Décision / Attribution',4),(105,18,'a2z','E5','Décision / Attribution',4),(105,18,'e3r','E5','Décision / Attribution',4),(105,30,'a1a','E5','Décision / Attribution',4),(105,30,'a1t','E5','Décision / Attribution',4),(105,30,'a2z','E5','Décision / Attribution',4),(105,30,'e3r','E5','Décision / Attribution',4),(105,31,'a1a','E5','Décision / Attribution',4),(105,31,'a1t','E5','Décision / Attribution',4),(105,31,'a2z','E5','Décision / Attribution',4),(105,31,'e3r','E5','Décision / Attribution',4),(105,32,'a1a','E5','Décision / Attribution',4),(105,32,'a1t','E5','Décision / Attribution',4),(105,32,'a2z','E5','Décision / Attribution',4),(105,32,'e3r','E5','Décision / Attribution',4),(105,33,'a1a','E5','Décision / Attribution',4),(105,33,'a1t','E5','Décision / Attribution',4),(105,33,'a2z','E5','Décision / Attribution',4),(105,33,'e3r','E5','Décision / Attribution',4),(105,1000,'a1a','E5','Décision / Attribution',4),(105,1000,'a1t','E5','Décision / Attribution',4),(105,1000,'a2z','E5','Décision / Attribution',4),(105,1000,'e3r','E5','Décision / Attribution',4),(105,1004,'a1a','E5','Décision / Attribution',4),(105,1004,'a1t','E5','Décision / Attribution',4),(105,1004,'a2z','E5','Décision / Attribution',4),(105,1004,'e3r','E5','Décision / Attribution',4),(106,1,'a1a','E6','Information des soumissionnaires non retenus',5),(106,1,'a1t','E6','Information des soumissionnaires non retenus',5),(106,1,'a2z','E6','Information des soumissionnaires non retenus',5),(106,1,'e3r','E6','Information des soumissionnaires non retenus',5),(106,2,'a1a','E6','Information des soumissionnaires non retenus',5),(106,2,'a1t','E6','Information des soumissionnaires non retenus',5),(106,2,'a2z','E6','Information des soumissionnaires non retenus',5),(106,2,'e3r','E6','Information des soumissionnaires non retenus',5),(106,3,'a1a','E6','Information des soumissionnaires non retenus',5),(106,3,'a1t','E6','Information des soumissionnaires non retenus',5),(106,3,'a2z','E6','Information des soumissionnaires non retenus',5),(106,3,'e3r','E6','Information des soumissionnaires non retenus',5),(106,4,'a1a','E6','Information des soumissionnaires non retenus',5),(106,4,'a1t','E6','Information des soumissionnaires non retenus',5),(106,4,'a2z','E6','Information des soumissionnaires non retenus',5),(106,4,'e3r','E6','Information des soumissionnaires non retenus',5),(106,5,'a1a','E6','Information des soumissionnaires non retenus',5),(106,5,'a1t','E6','Information des soumissionnaires non retenus',5),(106,5,'a2z','E6','Information des soumissionnaires non retenus',5),(106,5,'e3r','E6','Information des soumissionnaires non retenus',5),(106,6,'a1a','E6','Information des soumissionnaires non retenus',5),(106,6,'a1t','E6','Information des soumissionnaires non retenus',5),(106,6,'a2z','E6','Information des soumissionnaires non retenus',5),(106,6,'e3r','E6','Information des soumissionnaires non retenus',5),(106,7,'a1a','E6','Information des soumissionnaires non retenus',5),(106,7,'a1t','E6','Information des soumissionnaires non retenus',5),(106,7,'a2z','E6','Information des soumissionnaires non retenus',5),(106,7,'e3r','E6','Information des soumissionnaires non retenus',5),(106,8,'a1a','E6','Information des soumissionnaires non retenus',5),(106,8,'a1t','E6','Information des soumissionnaires non retenus',5),(106,8,'a2z','E6','Information des soumissionnaires non retenus',5),(106,8,'e3r','E6','Information des soumissionnaires non retenus',5),(106,10,'a1a','E6','Information des soumissionnaires non retenus',5),(106,10,'a1t','E6','Information des soumissionnaires non retenus',5),(106,10,'a2z','E6','Information des soumissionnaires non retenus',5),(106,10,'e3r','E6','Information des soumissionnaires non retenus',5),(106,11,'a1a','E6','Information des soumissionnaires non retenus',5),(106,11,'a1t','E6','Information des soumissionnaires non retenus',5),(106,11,'a2z','E6','Information des soumissionnaires non retenus',5),(106,11,'e3r','E6','Information des soumissionnaires non retenus',5),(106,12,'a1a','E6','Information des soumissionnaires non retenus',5),(106,12,'a1t','E6','Information des soumissionnaires non retenus',5),(106,12,'a2z','E6','Information des soumissionnaires non retenus',5),(106,12,'e3r','E6','Information des soumissionnaires non retenus',5),(106,17,'a1a','E6','Information des soumissionnaires non retenus',5),(106,17,'a1t','E6','Information des soumissionnaires non retenus',5),(106,17,'a2z','E6','Information des soumissionnaires non retenus',5),(106,17,'e3r','E6','Information des soumissionnaires non retenus',5),(106,18,'a1a','E6','Information des soumissionnaires non retenus',5),(106,18,'a1t','E6','Information des soumissionnaires non retenus',5),(106,18,'a2z','E6','Information des soumissionnaires non retenus',5),(106,18,'e3r','E6','Information des soumissionnaires non retenus',5),(106,30,'a1a','E6','Information des soumissionnaires non retenus',5),(106,30,'a1t','E6','Information des soumissionnaires non retenus',5),(106,30,'a2z','E6','Information des soumissionnaires non retenus',5),(106,30,'e3r','E6','Information des soumissionnaires non retenus',5),(106,31,'a1a','E6','Information des soumissionnaires non retenus',5),(106,31,'a1t','E6','Information des soumissionnaires non retenus',5),(106,31,'a2z','E6','Information des soumissionnaires non retenus',5),(106,31,'e3r','E6','Information des soumissionnaires non retenus',5),(106,32,'a1a','E6','Information des soumissionnaires non retenus',5),(106,32,'a1t','E6','Information des soumissionnaires non retenus',5),(106,32,'a2z','E6','Information des soumissionnaires non retenus',5),(106,32,'e3r','E6','Information des soumissionnaires non retenus',5),(106,33,'a1a','E6','Information des soumissionnaires non retenus',5),(106,33,'a1t','E6','Information des soumissionnaires non retenus',5),(106,33,'a2z','E6','Information des soumissionnaires non retenus',5),(106,33,'e3r','E6','Information des soumissionnaires non retenus',5),(106,1000,'a1a','E6','Information des soumissionnaires non retenus',5),(106,1000,'a1t','E6','Information des soumissionnaires non retenus',5),(106,1000,'a2z','E6','Information des soumissionnaires non retenus',5),(106,1000,'e3r','E6','Information des soumissionnaires non retenus',5),(106,1004,'a1a','E6','Information des soumissionnaires non retenus',5),(106,1004,'a1t','E6','Information des soumissionnaires non retenus',5),(106,1004,'a2z','E6','Information des soumissionnaires non retenus',5),(106,1004,'e3r','E6','Information des soumissionnaires non retenus',5),(107,1,'a1a','E7','Notification de la décision',6),(107,1,'a1t','E7','Notification de la décision',6),(107,1,'a2z','E7','Notification de la décision',6),(107,1,'e3r','E7','Notification de la décision',6),(107,2,'a1a','E7','Notification de la décision',6),(107,2,'a1t','E7','Notification de la décision',6),(107,2,'a2z','E7','Notification de la décision',6),(107,2,'e3r','E7','Notification de la décision',6),(107,3,'a1a','E7','Notification de la décision',6),(107,3,'a1t','E7','Notification de la décision',6),(107,3,'a2z','E7','Notification de la décision',6),(107,3,'e3r','E7','Notification de la décision',6),(107,4,'a1a','E7','Notification de la décision',6),(107,4,'a1t','E7','Notification de la décision',6),(107,4,'a2z','E7','Notification de la décision',6),(107,4,'e3r','E7','Notification de la décision',6),(107,5,'a1a','E7','Notification de la décision',6),(107,5,'a1t','E7','Notification de la décision',6),(107,5,'a2z','E7','Notification de la décision',6),(107,5,'e3r','E7','Notification de la décision',6),(107,6,'a1a','E7','Notification de la décision',6),(107,6,'a1t','E7','Notification de la décision',6),(107,6,'a2z','E7','Notification de la décision',6),(107,6,'e3r','E7','Notification de la décision',6),(107,7,'a1a','E7','Notification de la décision',6),(107,7,'a1t','E7','Notification de la décision',6),(107,7,'a2z','E7','Notification de la décision',6),(107,7,'e3r','E7','Notification de la décision',6),(107,8,'a1a','E7','Notification de la décision',6),(107,8,'a1t','E7','Notification de la décision',6),(107,8,'a2z','E7','Notification de la décision',6),(107,8,'e3r','E7','Notification de la décision',6),(107,10,'a1a','E7','Notification de la décision',6),(107,10,'a1t','E7','Notification de la décision',6),(107,10,'a2z','E7','Notification de la décision',6),(107,10,'e3r','E7','Notification de la décision',6),(107,11,'a1a','E7','Notification de la décision',6),(107,11,'a1t','E7','Notification de la décision',6),(107,11,'a2z','E7','Notification de la décision',6),(107,11,'e3r','E7','Notification de la décision',6),(107,12,'a1a','E7','Notification de la décision',6),(107,12,'a1t','E7','Notification de la décision',6),(107,12,'a2z','E7','Notification de la décision',6),(107,12,'e3r','E7','Notification de la décision',6),(107,17,'a1a','E7','Notification de la décision',6),(107,17,'a1t','E7','Notification de la décision',6),(107,17,'a2z','E7','Notification de la décision',6),(107,17,'e3r','E7','Notification de la décision',6),(107,18,'a1a','E7','Notification de la décision',6),(107,18,'a1t','E7','Notification de la décision',6),(107,18,'a2z','E7','Notification de la décision',6),(107,18,'e3r','E7','Notification de la décision',6),(107,30,'a1a','E7','Notification de la décision',6),(107,30,'a1t','E7','Notification de la décision',6),(107,30,'a2z','E7','Notification de la décision',6),(107,30,'e3r','E7','Notification de la décision',6),(107,31,'a1a','E7','Notification de la décision',6),(107,31,'a1t','E7','Notification de la décision',6),(107,31,'a2z','E7','Notification de la décision',6),(107,31,'e3r','E7','Notification de la décision',6),(107,32,'a1a','E7','Notification de la décision',6),(107,32,'a1t','E7','Notification de la décision',6),(107,32,'a2z','E7','Notification de la décision',6),(107,32,'e3r','E7','Notification de la décision',6),(107,33,'a1a','E7','Notification de la décision',6),(107,33,'a1t','E7','Notification de la décision',6),(107,33,'a2z','E7','Notification de la décision',6),(107,33,'e3r','E7','Notification de la décision',6),(107,1000,'a1a','E7','Notification de la décision',6),(107,1000,'a1t','E7','Notification de la décision',6),(107,1000,'a2z','E7','Notification de la décision',6),(107,1000,'e3r','E7','Notification de la décision',6),(107,1004,'a1a','E7','Notification de la décision',6),(107,1004,'a1t','E7','Notification de la décision',6),(107,1004,'a2z','E7','Notification de la décision',6),(107,1004,'e3r','E7','Notification de la décision',6),(108,1,'a1a','E8','Avis d\'attribution',7),(108,1,'a1t','E8','Avis d\'attribution',7),(108,1,'a2z','E8','Avis d\'attribution',7),(108,1,'e3r','E8','Avis d\'attribution',7),(108,2,'a1a','E8','Avis d\'attribution',7),(108,2,'a1t','E8','Avis d\'attribution',7),(108,2,'a2z','E8','Avis d\'attribution',7),(108,2,'e3r','E8','Avis d\'attribution',7),(108,3,'a1a','E8','Avis d\'attribution',7),(108,3,'a1t','E8','Avis d\'attribution',7),(108,3,'a2z','E8','Avis d\'attribution',7),(108,3,'e3r','E8','Avis d\'attribution',7),(108,4,'a1a','E8','Avis d\'attribution',7),(108,4,'a1t','E8','Avis d\'attribution',7),(108,4,'a2z','E8','Avis d\'attribution',7),(108,4,'e3r','E8','Avis d\'attribution',7),(108,5,'a1a','E8','Avis d\'attribution',7),(108,5,'a1t','E8','Avis d\'attribution',7),(108,5,'a2z','E8','Avis d\'attribution',7),(108,5,'e3r','E8','Avis d\'attribution',7),(108,6,'a1a','E8','Avis d\'attribution',7),(108,6,'a1t','E8','Avis d\'attribution',7),(108,6,'a2z','E8','Avis d\'attribution',7),(108,6,'e3r','E8','Avis d\'attribution',7),(108,7,'a1a','E8','Avis d\'attribution',7),(108,7,'a1t','E8','Avis d\'attribution',7),(108,7,'a2z','E8','Avis d\'attribution',7),(108,7,'e3r','E8','Avis d\'attribution',7),(108,8,'a1a','E8','Avis d\'attribution',7),(108,8,'a1t','E8','Avis d\'attribution',7),(108,8,'a2z','E8','Avis d\'attribution',7),(108,8,'e3r','E8','Avis d\'attribution',7),(108,10,'a1a','E8','Avis d\'attribution',7),(108,10,'a1t','E8','Avis d\'attribution',7),(108,10,'a2z','E8','Avis d\'attribution',7),(108,10,'e3r','E8','Avis d\'attribution',7),(108,11,'a1a','E8','Avis d\'attribution',7),(108,11,'a1t','E8','Avis d\'attribution',7),(108,11,'a2z','E8','Avis d\'attribution',7),(108,11,'e3r','E8','Avis d\'attribution',7),(108,12,'a1a','E8','Avis d\'attribution',7),(108,12,'a1t','E8','Avis d\'attribution',7),(108,12,'a2z','E8','Avis d\'attribution',7),(108,12,'e3r','E8','Avis d\'attribution',7),(108,17,'a1a','E8','Avis d\'attribution',7),(108,17,'a1t','E8','Avis d\'attribution',7),(108,17,'a2z','E8','Avis d\'attribution',7),(108,17,'e3r','E8','Avis d\'attribution',7),(108,18,'a1a','E8','Avis d\'attribution',7),(108,18,'a1t','E8','Avis d\'attribution',7),(108,18,'a2z','E8','Avis d\'attribution',7),(108,18,'e3r','E8','Avis d\'attribution',7),(108,30,'a1a','E8','Avis d\'attribution',7),(108,30,'a1t','E8','Avis d\'attribution',7),(108,30,'a2z','E8','Avis d\'attribution',7),(108,30,'e3r','E8','Avis d\'attribution',7),(108,31,'a1a','E8','Avis d\'attribution',7),(108,31,'a1t','E8','Avis d\'attribution',7),(108,31,'a2z','E8','Avis d\'attribution',7),(108,31,'e3r','E8','Avis d\'attribution',7),(108,32,'a1a','E8','Avis d\'attribution',7),(108,32,'a1t','E8','Avis d\'attribution',7),(108,32,'a2z','E8','Avis d\'attribution',7),(108,32,'e3r','E8','Avis d\'attribution',7),(108,33,'a1a','E8','Avis d\'attribution',7),(108,33,'a1t','E8','Avis d\'attribution',7),(108,33,'a2z','E8','Avis d\'attribution',7),(108,33,'e3r','E8','Avis d\'attribution',7),(108,1000,'a1a','E8','Avis d\'attribution',7),(108,1000,'a1t','E8','Avis d\'attribution',7),(108,1000,'a2z','E8','Avis d\'attribution',7),(108,1000,'e3r','E8','Avis d\'attribution',7),(108,1004,'a1a','E8','Avis d\'attribution',7),(108,1004,'a1t','E8','Avis d\'attribution',7),(108,1004,'a2z','E8','Avis d\'attribution',7),(108,1004,'e3r','E8','Avis d\'attribution',7),(181,9,'a1a','E1','Date de lancement',0),(181,9,'a1t','E1','Date de lancement',0),(181,9,'a2z','E1','Date de lancement',0),(181,9,'e3r','E1','Date de lancement',0),(182,9,'a1a','E2','Date limite de remise des plis',1),(182,9,'a1t','E2','Date limite de remise des plis',1),(182,9,'a2z','E2','Date limite de remise des plis',1),(182,9,'e3r','E2','Date limite de remise des plis',1),(183,9,'a1a','E3','Ouverture des plis',2),(183,9,'a1t','E3','Ouverture des plis',2),(183,9,'a2z','E3','Ouverture des plis',2),(183,9,'e3r','E3','Ouverture des plis',2),(184,9,'a1a','E4','Analyse des réponses',3),(184,9,'a1t','E4','Analyse des réponses',3),(184,9,'a2z','E4','Analyse des réponses',3),(184,9,'e3r','E4','Analyse des réponses',3),(185,9,'a1a','E5','Décision / Attribution',4),(185,9,'a1t','E5','Décision / Attribution',4),(185,9,'a2z','E5','Décision / Attribution',4),(185,9,'e3r','E5','Décision / Attribution',4),(186,9,'a1a','E6','Information des soumissionnaires non retenus',5),(186,9,'a1t','E6','Information des soumissionnaires non retenus',5),(186,9,'a2z','E6','Information des soumissionnaires non retenus',5),(186,9,'e3r','E6','Information des soumissionnaires non retenus',5),(187,9,'a1a','E7','Notification de la décision',6),(187,9,'a1t','E7','Notification de la décision',6),(187,9,'a2z','E7','Notification de la décision',6),(187,9,'e3r','E7','Notification de la décision',6),(188,9,'a1a','E8','Avis d\'attribution',7),(188,9,'a1t','E8','Avis d\'attribution',7),(188,9,'a2z','E8','Avis d\'attribution',7),(188,9,'e3r','E8','Avis d\'attribution',7);
/*!40000 ALTER TABLE `t_calendrier_etape_referentiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_calendrier_transition`
--

LOCK TABLES `t_calendrier_transition` WRITE;
/*!40000 ALTER TABLE `t_calendrier_transition` DISABLE KEYS */;
INSERT INTO `t_calendrier_transition` (`ID_CALENDRIER_TRANSITION`, `ID_ETAPE_SOURCE`, `ID_ETAPE_CIBLE`, `VALEUR_FIXE`, `VALEUR_VARIABLE_INITIALE`, `VALEUR_VARIABLE_REELLE`) VALUES (1,2,1,-40,-5,0),(2,2,3,60,15,0),(3,3,4,0,5,0),(4,4,5,60,0,0),(5,5,6,0,12,0),(6,6,7,0,0,0),(7,7,8,0,-7,0);
/*!40000 ALTER TABLE `t_calendrier_transition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_calendrier_transition_referentiel`
--

LOCK TABLES `t_calendrier_transition_referentiel` WRITE;
/*!40000 ALTER TABLE `t_calendrier_transition_referentiel` DISABLE KEYS */;
INSERT INTO `t_calendrier_transition_referentiel` (`ID_TRANSITION_CALENDRIER_REFERENTIEL`, `ID_TYPE_PROCEDURE`, `ORGANISME`, `ID_CALENDRIER_ETAPE_REFERENTIEL_SOURCE`, `ID_CALENDRIER_ETAPE_REFERENTIEL_CIBLE`, `VALEUR_FIXE`, `VALEUR_VARIABLE`) VALUES (101,1,'a1a',102,101,-40,-5),(101,1,'a1t',102,101,-40,-5),(101,1,'a2z',102,101,-40,-5),(101,1,'e3r',102,101,-40,-5),(101,2,'a1a',102,101,-40,-5),(101,2,'a1t',102,101,-40,-5),(101,2,'a2z',102,101,-40,-5),(101,2,'e3r',102,101,-40,-5),(101,3,'a1a',102,101,-40,-5),(101,3,'a1t',102,101,-40,-5),(101,3,'a2z',102,101,-40,-5),(101,3,'e3r',102,101,-40,-5),(101,4,'a1a',102,101,-40,-5),(101,4,'a1t',102,101,-40,-5),(101,4,'a2z',102,101,-40,-5),(101,4,'e3r',102,101,-40,-5),(101,5,'a1a',102,101,-40,-5),(101,5,'a1t',102,101,-40,-5),(101,5,'a2z',102,101,-40,-5),(101,5,'e3r',102,101,-40,-5),(101,6,'a1a',102,101,-40,-5),(101,6,'a1t',102,101,-40,-5),(101,6,'a2z',102,101,-40,-5),(101,6,'e3r',102,101,-40,-5),(101,7,'a1a',102,101,-40,-5),(101,7,'a1t',102,101,-40,-5),(101,7,'a2z',102,101,-40,-5),(101,7,'e3r',102,101,-40,-5),(101,8,'a1a',102,101,-40,-5),(101,8,'a1t',102,101,-40,-5),(101,8,'a2z',102,101,-40,-5),(101,8,'e3r',102,101,-40,-5),(101,10,'a1a',102,101,-40,-5),(101,10,'a1t',102,101,-40,-5),(101,10,'a2z',102,101,-40,-5),(101,10,'e3r',102,101,-40,-5),(101,11,'a1a',102,101,-40,-5),(101,11,'a1t',102,101,-40,-5),(101,11,'a2z',102,101,-40,-5),(101,11,'e3r',102,101,-40,-5),(101,12,'a1a',102,101,-40,-5),(101,12,'a1t',102,101,-40,-5),(101,12,'a2z',102,101,-40,-5),(101,12,'e3r',102,101,-40,-5),(101,17,'a1a',102,101,-40,-5),(101,17,'a1t',102,101,-40,-5),(101,17,'a2z',102,101,-40,-5),(101,17,'e3r',102,101,-40,-5),(101,18,'a1a',102,101,-40,-5),(101,18,'a1t',102,101,-40,-5),(101,18,'a2z',102,101,-40,-5),(101,18,'e3r',102,101,-40,-5),(101,30,'a1a',102,101,-40,-5),(101,30,'a1t',102,101,-40,-5),(101,30,'a2z',102,101,-40,-5),(101,30,'e3r',102,101,-40,-5),(101,31,'a1a',102,101,-40,-5),(101,31,'a1t',102,101,-40,-5),(101,31,'a2z',102,101,-40,-5),(101,31,'e3r',102,101,-40,-5),(101,32,'a1a',102,101,-40,-5),(101,32,'a1t',102,101,-40,-5),(101,32,'a2z',102,101,-40,-5),(101,32,'e3r',102,101,-40,-5),(101,33,'a1a',102,101,-40,-5),(101,33,'a1t',102,101,-40,-5),(101,33,'a2z',102,101,-40,-5),(101,33,'e3r',102,101,-40,-5),(101,1000,'a1a',102,101,-40,-5),(101,1000,'a1t',102,101,-40,-5),(101,1000,'a2z',102,101,-40,-5),(101,1000,'e3r',102,101,-40,-5),(101,1004,'a1a',102,101,-40,-5),(101,1004,'a1t',102,101,-40,-5),(101,1004,'a2z',102,101,-40,-5),(101,1004,'e3r',102,101,-40,-5),(102,1,'a1a',102,103,60,15),(102,1,'a1t',102,103,60,15),(102,1,'a2z',102,103,60,15),(102,1,'e3r',102,103,60,15),(102,2,'a1a',102,103,60,15),(102,2,'a1t',102,103,60,15),(102,2,'a2z',102,103,60,15),(102,2,'e3r',102,103,60,15),(102,3,'a1a',102,103,60,15),(102,3,'a1t',102,103,60,15),(102,3,'a2z',102,103,60,15),(102,3,'e3r',102,103,60,15),(102,4,'a1a',102,103,60,15),(102,4,'a1t',102,103,60,15),(102,4,'a2z',102,103,60,15),(102,4,'e3r',102,103,60,15),(102,5,'a1a',102,103,60,15),(102,5,'a1t',102,103,60,15),(102,5,'a2z',102,103,60,15),(102,5,'e3r',102,103,60,15),(102,6,'a1a',102,103,60,15),(102,6,'a1t',102,103,60,15),(102,6,'a2z',102,103,60,15),(102,6,'e3r',102,103,60,15),(102,7,'a1a',102,103,60,15),(102,7,'a1t',102,103,60,15),(102,7,'a2z',102,103,60,15),(102,7,'e3r',102,103,60,15),(102,8,'a1a',102,103,60,15),(102,8,'a1t',102,103,60,15),(102,8,'a2z',102,103,60,15),(102,8,'e3r',102,103,60,15),(102,10,'a1a',102,103,60,15),(102,10,'a1t',102,103,60,15),(102,10,'a2z',102,103,60,15),(102,10,'e3r',102,103,60,15),(102,11,'a1a',102,103,60,15),(102,11,'a1t',102,103,60,15),(102,11,'a2z',102,103,60,15),(102,11,'e3r',102,103,60,15),(102,12,'a1a',102,103,60,15),(102,12,'a1t',102,103,60,15),(102,12,'a2z',102,103,60,15),(102,12,'e3r',102,103,60,15),(102,17,'a1a',102,103,60,15),(102,17,'a1t',102,103,60,15),(102,17,'a2z',102,103,60,15),(102,17,'e3r',102,103,60,15),(102,18,'a1a',102,103,60,15),(102,18,'a1t',102,103,60,15),(102,18,'a2z',102,103,60,15),(102,18,'e3r',102,103,60,15),(102,30,'a1a',102,103,60,15),(102,30,'a1t',102,103,60,15),(102,30,'a2z',102,103,60,15),(102,30,'e3r',102,103,60,15),(102,31,'a1a',102,103,60,15),(102,31,'a1t',102,103,60,15),(102,31,'a2z',102,103,60,15),(102,31,'e3r',102,103,60,15),(102,32,'a1a',102,103,60,15),(102,32,'a1t',102,103,60,15),(102,32,'a2z',102,103,60,15),(102,32,'e3r',102,103,60,15),(102,33,'a1a',102,103,60,15),(102,33,'a1t',102,103,60,15),(102,33,'a2z',102,103,60,15),(102,33,'e3r',102,103,60,15),(102,1000,'a1a',102,103,60,15),(102,1000,'a1t',102,103,60,15),(102,1000,'a2z',102,103,60,15),(102,1000,'e3r',102,103,60,15),(102,1004,'a1a',102,103,60,15),(102,1004,'a1t',102,103,60,15),(102,1004,'a2z',102,103,60,15),(102,1004,'e3r',102,103,60,15),(103,1,'a1a',103,104,0,5),(103,1,'a1t',103,104,0,5),(103,1,'a2z',103,104,0,5),(103,1,'e3r',103,104,0,5),(103,2,'a1a',103,104,0,5),(103,2,'a1t',103,104,0,5),(103,2,'a2z',103,104,0,5),(103,2,'e3r',103,104,0,5),(103,3,'a1a',103,104,0,5),(103,3,'a1t',103,104,0,5),(103,3,'a2z',103,104,0,5),(103,3,'e3r',103,104,0,5),(103,4,'a1a',103,104,0,5),(103,4,'a1t',103,104,0,5),(103,4,'a2z',103,104,0,5),(103,4,'e3r',103,104,0,5),(103,5,'a1a',103,104,0,5),(103,5,'a1t',103,104,0,5),(103,5,'a2z',103,104,0,5),(103,5,'e3r',103,104,0,5),(103,6,'a1a',103,104,0,5),(103,6,'a1t',103,104,0,5),(103,6,'a2z',103,104,0,5),(103,6,'e3r',103,104,0,5),(103,7,'a1a',103,104,0,5),(103,7,'a1t',103,104,0,5),(103,7,'a2z',103,104,0,5),(103,7,'e3r',103,104,0,5),(103,8,'a1a',103,104,0,5),(103,8,'a1t',103,104,0,5),(103,8,'a2z',103,104,0,5),(103,8,'e3r',103,104,0,5),(103,10,'a1a',103,104,0,5),(103,10,'a1t',103,104,0,5),(103,10,'a2z',103,104,0,5),(103,10,'e3r',103,104,0,5),(103,11,'a1a',103,104,0,5),(103,11,'a1t',103,104,0,5),(103,11,'a2z',103,104,0,5),(103,11,'e3r',103,104,0,5),(103,12,'a1a',103,104,0,5),(103,12,'a1t',103,104,0,5),(103,12,'a2z',103,104,0,5),(103,12,'e3r',103,104,0,5),(103,17,'a1a',103,104,0,5),(103,17,'a1t',103,104,0,5),(103,17,'a2z',103,104,0,5),(103,17,'e3r',103,104,0,5),(103,18,'a1a',103,104,0,5),(103,18,'a1t',103,104,0,5),(103,18,'a2z',103,104,0,5),(103,18,'e3r',103,104,0,5),(103,30,'a1a',103,104,0,5),(103,30,'a1t',103,104,0,5),(103,30,'a2z',103,104,0,5),(103,30,'e3r',103,104,0,5),(103,31,'a1a',103,104,0,5),(103,31,'a1t',103,104,0,5),(103,31,'a2z',103,104,0,5),(103,31,'e3r',103,104,0,5),(103,32,'a1a',103,104,0,5),(103,32,'a1t',103,104,0,5),(103,32,'a2z',103,104,0,5),(103,32,'e3r',103,104,0,5),(103,33,'a1a',103,104,0,5),(103,33,'a1t',103,104,0,5),(103,33,'a2z',103,104,0,5),(103,33,'e3r',103,104,0,5),(103,1000,'a1a',103,104,0,5),(103,1000,'a1t',103,104,0,5),(103,1000,'a2z',103,104,0,5),(103,1000,'e3r',103,104,0,5),(103,1004,'a1a',103,104,0,5),(103,1004,'a1t',103,104,0,5),(103,1004,'a2z',103,104,0,5),(103,1004,'e3r',103,104,0,5),(104,1,'a1a',104,105,60,0),(104,1,'a1t',104,105,60,0),(104,1,'a2z',104,105,60,0),(104,1,'e3r',104,105,60,0),(104,2,'a1a',104,105,60,0),(104,2,'a1t',104,105,60,0),(104,2,'a2z',104,105,60,0),(104,2,'e3r',104,105,60,0),(104,3,'a1a',104,105,60,0),(104,3,'a1t',104,105,60,0),(104,3,'a2z',104,105,60,0),(104,3,'e3r',104,105,60,0),(104,4,'a1a',104,105,60,0),(104,4,'a1t',104,105,60,0),(104,4,'a2z',104,105,60,0),(104,4,'e3r',104,105,60,0),(104,5,'a1a',104,105,60,0),(104,5,'a1t',104,105,60,0),(104,5,'a2z',104,105,60,0),(104,5,'e3r',104,105,60,0),(104,6,'a1a',104,105,60,0),(104,6,'a1t',104,105,60,0),(104,6,'a2z',104,105,60,0),(104,6,'e3r',104,105,60,0),(104,7,'a1a',104,105,60,0),(104,7,'a1t',104,105,60,0),(104,7,'a2z',104,105,60,0),(104,7,'e3r',104,105,60,0),(104,8,'a1a',104,105,60,0),(104,8,'a1t',104,105,60,0),(104,8,'a2z',104,105,60,0),(104,8,'e3r',104,105,60,0),(104,10,'a1a',104,105,60,0),(104,10,'a1t',104,105,60,0),(104,10,'a2z',104,105,60,0),(104,10,'e3r',104,105,60,0),(104,11,'a1a',104,105,60,0),(104,11,'a1t',104,105,60,0),(104,11,'a2z',104,105,60,0),(104,11,'e3r',104,105,60,0),(104,12,'a1a',104,105,60,0),(104,12,'a1t',104,105,60,0),(104,12,'a2z',104,105,60,0),(104,12,'e3r',104,105,60,0),(104,17,'a1a',104,105,60,0),(104,17,'a1t',104,105,60,0),(104,17,'a2z',104,105,60,0),(104,17,'e3r',104,105,60,0),(104,18,'a1a',104,105,60,0),(104,18,'a1t',104,105,60,0),(104,18,'a2z',104,105,60,0),(104,18,'e3r',104,105,60,0),(104,30,'a1a',104,105,60,0),(104,30,'a1t',104,105,60,0),(104,30,'a2z',104,105,60,0),(104,30,'e3r',104,105,60,0),(104,31,'a1a',104,105,60,0),(104,31,'a1t',104,105,60,0),(104,31,'a2z',104,105,60,0),(104,31,'e3r',104,105,60,0),(104,32,'a1a',104,105,60,0),(104,32,'a1t',104,105,60,0),(104,32,'a2z',104,105,60,0),(104,32,'e3r',104,105,60,0),(104,33,'a1a',104,105,60,0),(104,33,'a1t',104,105,60,0),(104,33,'a2z',104,105,60,0),(104,33,'e3r',104,105,60,0),(104,1000,'a1a',104,105,60,0),(104,1000,'a1t',104,105,60,0),(104,1000,'a2z',104,105,60,0),(104,1000,'e3r',104,105,60,0),(104,1004,'a1a',104,105,60,0),(104,1004,'a1t',104,105,60,0),(104,1004,'a2z',104,105,60,0),(104,1004,'e3r',104,105,60,0),(105,1,'a1a',105,106,0,12),(105,1,'a1t',105,106,0,12),(105,1,'a2z',105,106,0,12),(105,1,'e3r',105,106,0,12),(105,2,'a1a',105,106,0,12),(105,2,'a1t',105,106,0,12),(105,2,'a2z',105,106,0,12),(105,2,'e3r',105,106,0,12),(105,3,'a1a',105,106,0,12),(105,3,'a1t',105,106,0,12),(105,3,'a2z',105,106,0,12),(105,3,'e3r',105,106,0,12),(105,4,'a1a',105,106,0,12),(105,4,'a1t',105,106,0,12),(105,4,'a2z',105,106,0,12),(105,4,'e3r',105,106,0,12),(105,5,'a1a',105,106,0,12),(105,5,'a1t',105,106,0,12),(105,5,'a2z',105,106,0,12),(105,5,'e3r',105,106,0,12),(105,6,'a1a',105,106,0,12),(105,6,'a1t',105,106,0,12),(105,6,'a2z',105,106,0,12),(105,6,'e3r',105,106,0,12),(105,7,'a1a',105,106,0,12),(105,7,'a1t',105,106,0,12),(105,7,'a2z',105,106,0,12),(105,7,'e3r',105,106,0,12),(105,8,'a1a',105,106,0,12),(105,8,'a1t',105,106,0,12),(105,8,'a2z',105,106,0,12),(105,8,'e3r',105,106,0,12),(105,10,'a1a',105,106,0,12),(105,10,'a1t',105,106,0,12),(105,10,'a2z',105,106,0,12),(105,10,'e3r',105,106,0,12),(105,11,'a1a',105,106,0,12),(105,11,'a1t',105,106,0,12),(105,11,'a2z',105,106,0,12),(105,11,'e3r',105,106,0,12),(105,12,'a1a',105,106,0,12),(105,12,'a1t',105,106,0,12),(105,12,'a2z',105,106,0,12),(105,12,'e3r',105,106,0,12),(105,17,'a1a',105,106,0,12),(105,17,'a1t',105,106,0,12),(105,17,'a2z',105,106,0,12),(105,17,'e3r',105,106,0,12),(105,18,'a1a',105,106,0,12),(105,18,'a1t',105,106,0,12),(105,18,'a2z',105,106,0,12),(105,18,'e3r',105,106,0,12),(105,30,'a1a',105,106,0,12),(105,30,'a1t',105,106,0,12),(105,30,'a2z',105,106,0,12),(105,30,'e3r',105,106,0,12),(105,31,'a1a',105,106,0,12),(105,31,'a1t',105,106,0,12),(105,31,'a2z',105,106,0,12),(105,31,'e3r',105,106,0,12),(105,32,'a1a',105,106,0,12),(105,32,'a1t',105,106,0,12),(105,32,'a2z',105,106,0,12),(105,32,'e3r',105,106,0,12),(105,33,'a1a',105,106,0,12),(105,33,'a1t',105,106,0,12),(105,33,'a2z',105,106,0,12),(105,33,'e3r',105,106,0,12),(105,1000,'a1a',105,106,0,12),(105,1000,'a1t',105,106,0,12),(105,1000,'a2z',105,106,0,12),(105,1000,'e3r',105,106,0,12),(105,1004,'a1a',105,106,0,12),(105,1004,'a1t',105,106,0,12),(105,1004,'a2z',105,106,0,12),(105,1004,'e3r',105,106,0,12),(106,1,'a1a',106,107,0,0),(106,1,'a1t',106,107,0,0),(106,1,'a2z',106,107,0,0),(106,1,'e3r',106,107,0,0),(106,2,'a1a',106,107,0,0),(106,2,'a1t',106,107,0,0),(106,2,'a2z',106,107,0,0),(106,2,'e3r',106,107,0,0),(106,3,'a1a',106,107,0,0),(106,3,'a1t',106,107,0,0),(106,3,'a2z',106,107,0,0),(106,3,'e3r',106,107,0,0),(106,4,'a1a',106,107,0,0),(106,4,'a1t',106,107,0,0),(106,4,'a2z',106,107,0,0),(106,4,'e3r',106,107,0,0),(106,5,'a1a',106,107,0,0),(106,5,'a1t',106,107,0,0),(106,5,'a2z',106,107,0,0),(106,5,'e3r',106,107,0,0),(106,6,'a1a',106,107,0,0),(106,6,'a1t',106,107,0,0),(106,6,'a2z',106,107,0,0),(106,6,'e3r',106,107,0,0),(106,7,'a1a',106,107,0,0),(106,7,'a1t',106,107,0,0),(106,7,'a2z',106,107,0,0),(106,7,'e3r',106,107,0,0),(106,8,'a1a',106,107,0,0),(106,8,'a1t',106,107,0,0),(106,8,'a2z',106,107,0,0),(106,8,'e3r',106,107,0,0),(106,10,'a1a',106,107,0,0),(106,10,'a1t',106,107,0,0),(106,10,'a2z',106,107,0,0),(106,10,'e3r',106,107,0,0),(106,11,'a1a',106,107,0,0),(106,11,'a1t',106,107,0,0),(106,11,'a2z',106,107,0,0),(106,11,'e3r',106,107,0,0),(106,12,'a1a',106,107,0,0),(106,12,'a1t',106,107,0,0),(106,12,'a2z',106,107,0,0),(106,12,'e3r',106,107,0,0),(106,17,'a1a',106,107,0,0),(106,17,'a1t',106,107,0,0),(106,17,'a2z',106,107,0,0),(106,17,'e3r',106,107,0,0),(106,18,'a1a',106,107,0,0),(106,18,'a1t',106,107,0,0),(106,18,'a2z',106,107,0,0),(106,18,'e3r',106,107,0,0),(106,30,'a1a',106,107,0,0),(106,30,'a1t',106,107,0,0),(106,30,'a2z',106,107,0,0),(106,30,'e3r',106,107,0,0),(106,31,'a1a',106,107,0,0),(106,31,'a1t',106,107,0,0),(106,31,'a2z',106,107,0,0),(106,31,'e3r',106,107,0,0),(106,32,'a1a',106,107,0,0),(106,32,'a1t',106,107,0,0),(106,32,'a2z',106,107,0,0),(106,32,'e3r',106,107,0,0),(106,33,'a1a',106,107,0,0),(106,33,'a1t',106,107,0,0),(106,33,'a2z',106,107,0,0),(106,33,'e3r',106,107,0,0),(106,1000,'a1a',106,107,0,0),(106,1000,'a1t',106,107,0,0),(106,1000,'a2z',106,107,0,0),(106,1000,'e3r',106,107,0,0),(106,1004,'a1a',106,107,0,0),(106,1004,'a1t',106,107,0,0),(106,1004,'a2z',106,107,0,0),(106,1004,'e3r',106,107,0,0),(107,1,'a1a',107,108,0,-7),(107,1,'a1t',107,108,0,-7),(107,1,'a2z',107,108,0,-7),(107,1,'e3r',107,108,0,-7),(107,2,'a1a',107,108,0,-7),(107,2,'a1t',107,108,0,-7),(107,2,'a2z',107,108,0,-7),(107,2,'e3r',107,108,0,-7),(107,3,'a1a',107,108,0,-7),(107,3,'a1t',107,108,0,-7),(107,3,'a2z',107,108,0,-7),(107,3,'e3r',107,108,0,-7),(107,4,'a1a',107,108,0,-7),(107,4,'a1t',107,108,0,-7),(107,4,'a2z',107,108,0,-7),(107,4,'e3r',107,108,0,-7),(107,5,'a1a',107,108,0,-7),(107,5,'a1t',107,108,0,-7),(107,5,'a2z',107,108,0,-7),(107,5,'e3r',107,108,0,-7),(107,6,'a1a',107,108,0,-7),(107,6,'a1t',107,108,0,-7),(107,6,'a2z',107,108,0,-7),(107,6,'e3r',107,108,0,-7),(107,7,'a1a',107,108,0,-7),(107,7,'a1t',107,108,0,-7),(107,7,'a2z',107,108,0,-7),(107,7,'e3r',107,108,0,-7),(107,8,'a1a',107,108,0,-7),(107,8,'a1t',107,108,0,-7),(107,8,'a2z',107,108,0,-7),(107,8,'e3r',107,108,0,-7),(107,10,'a1a',107,108,0,-7),(107,10,'a1t',107,108,0,-7),(107,10,'a2z',107,108,0,-7),(107,10,'e3r',107,108,0,-7),(107,11,'a1a',107,108,0,-7),(107,11,'a1t',107,108,0,-7),(107,11,'a2z',107,108,0,-7),(107,11,'e3r',107,108,0,-7),(107,12,'a1a',107,108,0,-7),(107,12,'a1t',107,108,0,-7),(107,12,'a2z',107,108,0,-7),(107,12,'e3r',107,108,0,-7),(107,17,'a1a',107,108,0,-7),(107,17,'a1t',107,108,0,-7),(107,17,'a2z',107,108,0,-7),(107,17,'e3r',107,108,0,-7),(107,18,'a1a',107,108,0,-7),(107,18,'a1t',107,108,0,-7),(107,18,'a2z',107,108,0,-7),(107,18,'e3r',107,108,0,-7),(107,30,'a1a',107,108,0,-7),(107,30,'a1t',107,108,0,-7),(107,30,'a2z',107,108,0,-7),(107,30,'e3r',107,108,0,-7),(107,31,'a1a',107,108,0,-7),(107,31,'a1t',107,108,0,-7),(107,31,'a2z',107,108,0,-7),(107,31,'e3r',107,108,0,-7),(107,32,'a1a',107,108,0,-7),(107,32,'a1t',107,108,0,-7),(107,32,'a2z',107,108,0,-7),(107,32,'e3r',107,108,0,-7),(107,33,'a1a',107,108,0,-7),(107,33,'a1t',107,108,0,-7),(107,33,'a2z',107,108,0,-7),(107,33,'e3r',107,108,0,-7),(107,1000,'a1a',107,108,0,-7),(107,1000,'a1t',107,108,0,-7),(107,1000,'a2z',107,108,0,-7),(107,1000,'e3r',107,108,0,-7),(107,1004,'a1a',107,108,0,-7),(107,1004,'a1t',107,108,0,-7),(107,1004,'a2z',107,108,0,-7),(107,1004,'e3r',107,108,0,-7),(181,9,'a1a',182,181,-40,-5),(181,9,'a1t',182,181,-40,-5),(181,9,'a2z',182,181,-40,-5),(181,9,'e3r',182,181,-40,-5),(182,9,'a1a',182,183,60,15),(182,9,'a1t',182,183,60,15),(182,9,'a2z',182,183,60,15),(182,9,'e3r',182,183,60,15),(183,9,'a1a',183,184,0,5),(183,9,'a1t',183,184,0,5),(183,9,'a2z',183,184,0,5),(183,9,'e3r',183,184,0,5),(184,9,'a1a',184,185,60,0),(184,9,'a1t',184,185,60,0),(184,9,'a2z',184,185,60,0),(184,9,'e3r',184,185,60,0),(185,9,'a1a',185,186,0,12),(185,9,'a1t',185,186,0,12),(185,9,'a2z',185,186,0,12),(185,9,'e3r',185,186,0,12),(186,9,'a1a',186,187,0,0),(186,9,'a1t',186,187,0,0),(186,9,'a2z',186,187,0,0),(186,9,'e3r',186,187,0,0),(187,9,'a1a',187,188,0,-7),(187,9,'a1t',187,188,0,-7),(187,9,'a2z',187,188,0,-7),(187,9,'e3r',187,188,0,-7);
/*!40000 ALTER TABLE `t_calendrier_transition_referentiel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_candidature`
--

LOCK TABLES `t_candidature` WRITE;
/*!40000 ALTER TABLE `t_candidature` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_candidature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_candidature_mps`
--

LOCK TABLES `t_candidature_mps` WRITE;
/*!40000 ALTER TABLE `t_candidature_mps` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_candidature_mps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_chorus_ccag`
--

LOCK TABLES `t_chorus_ccag` WRITE;
/*!40000 ALTER TABLE `t_chorus_ccag` DISABLE KEYS */;
INSERT INTO `t_chorus_ccag` (`id_ccag`, `reference_ccag_chorus`, `libelle_ccag`, `reference_ccag_orme`) VALUES (1,'000','Pas de CCAG de référence',7),(2,'001','CCAG Fournitures courantes et prestations de services',1),(3,'002','CCAG Prestations intellectuelles',4),(4,'004','CCAG Techniques de l\'information et de la communication',5),(5,'005','CCAG Marchés publics industriels',2),(6,'006','CCAG Travaux',3);
/*!40000 ALTER TABLE `t_chorus_ccag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_chorus_fiche_modificative`
--

LOCK TABLES `t_chorus_fiche_modificative` WRITE;
/*!40000 ALTER TABLE `t_chorus_fiche_modificative` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_chorus_fiche_modificative` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_chorus_fiche_modificative_pj`
--

LOCK TABLES `t_chorus_fiche_modificative_pj` WRITE;
/*!40000 ALTER TABLE `t_chorus_fiche_modificative_pj` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_chorus_fiche_modificative_pj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_chorus_tier_rejete`
--

LOCK TABLES `t_chorus_tier_rejete` WRITE;
/*!40000 ALTER TABLE `t_chorus_tier_rejete` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_chorus_tier_rejete` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_complement_formulaire`
--

LOCK TABLES `t_complement_formulaire` WRITE;
/*!40000 ALTER TABLE `t_complement_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_complement_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_cons_lot_contrat`
--

LOCK TABLES `t_cons_lot_contrat` WRITE;
/*!40000 ALTER TABLE `t_cons_lot_contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_cons_lot_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_consultation_compte_pub`
--

LOCK TABLES `t_consultation_compte_pub` WRITE;
/*!40000 ALTER TABLE `t_consultation_compte_pub` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_consultation_compte_pub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_contact_contrat`
--

LOCK TABLES `t_contact_contrat` WRITE;
/*!40000 ALTER TABLE `t_contact_contrat` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_contact_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_contrat_titulaire`
--

LOCK TABLES `t_contrat_titulaire` WRITE;
/*!40000 ALTER TABLE `t_contrat_titulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_contrat_titulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_contrat_titulaire_favori`
--

LOCK TABLES `t_contrat_titulaire_favori` WRITE;
/*!40000 ALTER TABLE `t_contrat_titulaire_favori` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_contrat_titulaire_favori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_critere_attribution`
--

LOCK TABLES `t_critere_attribution` WRITE;
/*!40000 ALTER TABLE `t_critere_attribution` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_critere_attribution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_decision_selection_entreprise`
--

LOCK TABLES `t_decision_selection_entreprise` WRITE;
/*!40000 ALTER TABLE `t_decision_selection_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_decision_selection_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_derniers_appels_valides_ws_sgmap_documents`
--

LOCK TABLES `t_derniers_appels_valides_ws_sgmap_documents` WRITE;
/*!40000 ALTER TABLE `t_derniers_appels_valides_ws_sgmap_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_derniers_appels_valides_ws_sgmap_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_dispositif_annonce`
--

LOCK TABLES `t_dispositif_annonce` WRITE;
/*!40000 ALTER TABLE `t_dispositif_annonce` DISABLE KEYS */;
INSERT INTO `t_dispositif_annonce` (`id`, `libelle`, `id_externe`, `type`, `visible`, `id_dispositif_rectificatif`, `id_dispositif_annulation`, `sigle`, `id_type_avis_rectificatif`, `id_type_avis_annulation`, `libelle_formulaire_propose`) VALUES (1,'Avis d\'appel public à la concurrence JOUE-2  (formulaire européen)',1,2,'1',15,15,'JOUE-2',5,6,'BOAMP_PROPOSTION_JOUE2'),(2,'Avis d\'appel public à la concurrence FNS-1 (formulaire standard)',2,1,'1',12,11,'FNS-1',7,8,'BOAMP_PROPOSTION_FNS1'),(3,'Avis d\'appel public à la concurrence MAPA-1 (formulaire MAPA)',3,3,'1',13,14,'MAPA-1',9,10,'BOAMP_PROPOSTION_MAPA1'),(4,'Procédure DSP - Avis de convention DSP-1',4,4,'1',NULL,NULL,'DSP-1',11,12,''),(5,'Avis d\'appel public à la concurrence - Secteurs spéciaux JOUE-5 (formulaire européen)',5,2,'1',15,15,'JOUE-5',5,6,''),(6,'Résultat de marché FNS-3 (formulaire standard)',6,1,'1',12,11,'FNS-3',7,8,''),(7,'Avis d\'appel public à la concurrence - Concours  JOUE-12 (formulaire européen)',7,2,'1',15,15,'JOUE-12',5,6,''),(8,'Résultat de marché MAPA-3 (MAPA)',8,3,'1',13,14,'MAPA-3',9,10,''),(9,'Avis de pré-information JOUE-1 (formulaire européen)',9,2,'1',15,15,'JOUE-1',5,6,''),(10,'Avis d\'attribution JOUE-3 (formulaire européen)',10,2,'1',15,15,'JOUE-3',5,6,''),(11,'Formulaire FNS - Avis d\'annulation',11,1,'0',NULL,NULL,'FNS-A',7,8,''),(12,'Formulaire FNS - Avis rectificatif',12,1,'0',NULL,NULL,'FNS-R',7,8,''),(13,'Formulaire MAPA - Avis rectificatif',13,3,'0',NULL,NULL,'MAPA-R',NULL,NULL,''),(14,'Formulaire MAPA - Avis d\'annulation',14,3,'0',NULL,NULL,'MAPA-A',NULL,NULL,''),(15,'Formulaire JOUE - 14 - Avis rectificatif / d\'annulation',15,2,'0',NULL,NULL,'JOUE-14',NULL,NULL,''),(16,'Avis d\'attribution - Secteurs spéciaux JOUE-6 (formulaire européen)',16,2,'1',15,15,'JOUE-6',5,6,''),(17,'Avis d\'attribution - Résultat de concours JOUE-13 (formulaire européen)',17,2,'1',15,15,'JOUE-13',5,6,''),(18,'Procédure DSP - Avis d\'attribution DSP-3',19,4,'1',13,14,'DSP-3',9,10,''),(19,'Avis d\'appel public à la concurrence - Secteurs Défense et Sécurité FNS-4 (formulaire standard)',18,1,'1',12,11,'FNS-4',7,8,''),(24,'Avis de marché - les Echos',22,2,'1',NULL,NULL,'JOUE-2-E',NULL,NULL,'');
/*!40000 ALTER TABLE `t_dispositif_annonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_document`
--

LOCK TABLES `t_document` WRITE;
/*!40000 ALTER TABLE `t_document` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_document` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_document_entreprise`
--

LOCK TABLES `t_document_entreprise` WRITE;
/*!40000 ALTER TABLE `t_document_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_document_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_document_type`
--

LOCK TABLES `t_document_type` WRITE;
/*!40000 ALTER TABLE `t_document_type` DISABLE KEYS */;
INSERT INTO `t_document_type` (`id_type_document`, `nom_type_document`, `code`, `type_doc_entreprise_etablissement`, `uri`, `params_uri`, `class_name`, `nature_document`, `type_retour_ws`, `synchro_actif`, `message_desactivation_synchro`, `afficher_type_doc`) VALUES (1,'Attestation de régularité fiscale','attestation_fiscale','1','/v2/attestations_fiscales_dgfip/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationFiscaleWebService','1','1','1','','1'),(2,'Attestation de marché public URSSAF','attestation_sociale_mp','1','/v2/attestations_sociales_acoss/','type_attestation=AMP_UR, context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationSocialeWebService','1','1','0','ATTESTAtION_MARCHE_URSSAF_NON_PRODUITE','1'),(3,'Attestation de vigilance URSSAF','attestation_sociale_vg','1','/v2/attestations_sociales_acoss/','type_attestation=AVG_UR, context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationSocialeWebService','1','1','1','','1'),(4,'Carte professionnelle FNTP','carte_professionnelle','1','/v2/cartes_professionnelles_fntp/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\CarteProfessionnelleWebService','2','1','1','','1'),(5,'Attestation CNETP','certificat_cotisation','1','/v2/certificats_cnetp/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\DocumentWebService','2','1','1','','1'),(6,'Certificat Qualibat','certificat_qualibat','2','/v2/certificats_qualibat/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\CertificatQualibatWebService','2','1','1','','1'),(7,'Attestation de cotisation retraite ProBT','attestation_cotisation_retraite','2','/v2/attestations_cotisation_retraite_probtp/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationCotisationRetraiteWebService','2','1','1','','1'),(8,'Certificat de qualifications OPQIBI','certificats_qualifications','1','/v2/certificats_opqibi/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationCertificatQualificationWebService','2','2','1','','1'),(9,'Cotisation employeur MSA ','cotisation_msa','2','/v2/cotisations_msa/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\CotisationEmployeurWebService','2','2','1','','1'),(10,'Bordereau d\'archive',NULL,NULL,NULL,'','','2','1','0','','0'),(11,'Attestation AGEFIPH','attestations_agefiph','2','/v2/attestations_agefiph/','context=MPS, recipient={__recipient__}, object={__object__}','\\Atexo\\WebServiceApiGouvEntreprise\\AttestationAgefiphWebService','2','2','1','','1');
/*!40000 ALTER TABLE `t_document_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_donnee_complementaire`
--

LOCK TABLES `t_donnee_complementaire` WRITE;
/*!40000 ALTER TABLE `t_donnee_complementaire` DISABLE KEYS */;
INSERT INTO `t_donnee_complementaire` (`id_donnee_complementaire`, `id_tranche_type_prix`, `id_forme_prix`, `id_ccag_reference`, `reconductible`, `nombre_reconductions`, `modalites_reconduction`, `variantes_autorisees`, `variante_exigee`, `variantes_techniques_obligatoires`, `variantes_techniques_description`, `decomposition_lots_techniques`, `id_duree_delai_description`, `estimation_pf_att_pressenti`, `estimation_bc_min_att_pressenti`, `estimation_bc_max_att_pressenti`, `estimation_pf_tab_ouv_offre`, `estimation_dqe_tab_ouv_offre`, `avis_attribution_present`, `estimation_pf_preinscription`, `estimation_bc_min_preinscription`, `estimation_bc_max_preinscription`, `estimation_date_valeur_preinscription`, `lieu_execution`, `duree_marche`, `duree_date_debut`, `duree_date_fin`, `duree_description`, `id_choix_mois_jour`, `id_unite`, `id_nb_candidats_admis`, `nombre_candidats_fixe`, `nombre_candidats_min`, `nombre_candidats_max`, `delai_validite_offres`, `phase_successive`, `id_groupement_attributaire`, `id_critere_attribution`, `type_prestation`, `delai_partiel`, `adresse_retrais_dossiers`, `adresse_depot_offres`, `lieu_ouverture_plis`, `pieces_dossier_admin`, `id_tr_pieces_dossier_admin`, `pieces_dossier_tech`, `id_tr_pieces_dossier_tech`, `pieces_dossier_additif`, `id_tr_pieces_dossier_additif`, `id_tr_adresse_retrais_dossiers`, `id_tr_adresse_depot_offres`, `id_tr_lieu_ouverture_plis`, `caution_provisoire`, `prix_aquisition_plans`, `add_echantillon`, `id_tr_add_echantillon`, `date_limite_echantillon`, `add_reunion`, `date_reunion`, `id_tr_add_reunion`, `reunion`, `visites_lieux`, `echantillon`, `variantes`, `variante_calcule`, `criteres_identiques`, `justification_non_alloti`, `id_ccag_dpi`, `montant_marche`, `mots_cles`, `procedure_accord_marches_publics_omc`, `cautionnement_regime_financier`, `modalites_financement_regime_financier`, `publication_montant_estimation`, `valeur_montant_estimation_publiee`, `projet_finance_fonds_union_europeenne`, `identification_projet`) VALUES (1,NULL,NULL,NULL,0,NULL,NULL,0,0,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,0,NULL,0,0,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0','0',NULL,'0','0','',NULL,0,NULL,NULL,'','','0',NULL,'0','');
/*!40000 ALTER TABLE `t_donnee_complementaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_donnees_consultation`
--

LOCK TABLES `t_donnees_consultation` WRITE;
/*!40000 ALTER TABLE `t_donnees_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_donnees_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_dossier_formulaire`
--

LOCK TABLES `t_dossier_formulaire` WRITE;
/*!40000 ALTER TABLE `t_dossier_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_dossier_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_dume_contexte`
--

LOCK TABLES `t_dume_contexte` WRITE;
/*!40000 ALTER TABLE `t_dume_contexte` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_dume_contexte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_dume_numero`
--

LOCK TABLES `t_dume_numero` WRITE;
/*!40000 ALTER TABLE `t_dume_numero` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_dume_numero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_edition_formulaire`
--

LOCK TABLES `t_edition_formulaire` WRITE;
/*!40000 ALTER TABLE `t_edition_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_edition_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_entreprise_document_version`
--

LOCK TABLES `t_entreprise_document_version` WRITE;
/*!40000 ALTER TABLE `t_entreprise_document_version` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_entreprise_document_version` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_enveloppe_dossier_formulaire`
--

LOCK TABLES `t_enveloppe_dossier_formulaire` WRITE;
/*!40000 ALTER TABLE `t_enveloppe_dossier_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_enveloppe_dossier_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_espace_collaboratif`
--

LOCK TABLES `t_espace_collaboratif` WRITE;
/*!40000 ALTER TABLE `t_espace_collaboratif` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_espace_collaboratif` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_etablissement`
--

LOCK TABLES `t_etablissement` WRITE;
/*!40000 ALTER TABLE `t_etablissement` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_etablissement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_flux_rss`
--

LOCK TABLES `t_flux_rss` WRITE;
/*!40000 ALTER TABLE `t_flux_rss` DISABLE KEYS */;
INSERT INTO `t_flux_rss` (`id`, `tender_xml`, `nom_fichier`, `libelle`, `module`, `afficher_flux_rss`) VALUES (1,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender category=\"Travaux\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssTr.xml','LISTE_AVIS_EN_LIGNE_TRAVAUX',NULL,'1'),(2,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender category=\"Services\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssSr.xml','LISTE_AVIS_EN_LIGNE_SERVICES',NULL,'1'),(3,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender atelierProtege=\"1\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssAP.xml','LISTE_AVIS_EN_LIGNE_ATELIER_PROTEGE','ConsultationClause','1'),(4,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssOnline.xml','LISTE_AVIS_EN_LIGNE',NULL,'1'),(5,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender category=\"Fournitures\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssFn.xml','LISTE_AVIS_EN_LIGNE_FOURNITURES',NULL,'1'),(6,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender clauseSociale=\"1\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssCS.xml','LISTE_AVIS_EN_LIGNE_AVEC_CLAUSES_SOCIALES','ConsultationClause','1'),(7,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender clauseEnv=\"1\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rssCE.xml','LISTE_AVIS_EN_LIGNE_AVEC_CLAUSES_ENVIRONNEMENTALES','ConsultationClause','1'),(8,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender type=\"AAPC\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rss.xml',NULL,NULL,'0'),(9,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender type=\"AA\" enCours=\"1\"/> \r\n </Request>\r\n</Transaction>','rss-AA.xml',NULL,NULL,'0'),(10,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n   <Tender type=\"API\" enCours=\"1\"/> \r\n </Request>\r\n</Transaction>','rss-API.xml',NULL,NULL,'0'),(11,'<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<Transaction xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"C:\\projets\\mpe-3.0\\mpe\\ressources\\interface\\interfaces-Defense.xsd\" Version=\"1.0\" TimeStamp=\"2001-12-17T09:30:47-05:00\">\r\n <Request>\r\n  <Tender mps=\"1\" enCours=\"1\"/>\r\n </Request>\r\n</Transaction>','rss-MPS.xml','LISTE_AVIS_EN_LIGNE_AVEC_MARCHE_PUBLIC_SIMPLIFIE',NULL,'1');
/*!40000 ALTER TABLE `t_flux_rss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_forme_prix`
--

LOCK TABLES `t_forme_prix` WRITE;
/*!40000 ALTER TABLE `t_forme_prix` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_forme_prix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_forme_prix_has_ref_type_prix`
--

LOCK TABLES `t_forme_prix_has_ref_type_prix` WRITE;
/*!40000 ALTER TABLE `t_forme_prix_has_ref_type_prix` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_forme_prix_has_ref_type_prix` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_forme_prix_pf_has_ref_variation`
--

LOCK TABLES `t_forme_prix_pf_has_ref_variation` WRITE;
/*!40000 ALTER TABLE `t_forme_prix_pf_has_ref_variation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_forme_prix_pf_has_ref_variation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_forme_prix_pu_has_ref_variation`
--

LOCK TABLES `t_forme_prix_pu_has_ref_variation` WRITE;
/*!40000 ALTER TABLE `t_forme_prix_pu_has_ref_variation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_forme_prix_pu_has_ref_variation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_fusionner_services`
--

LOCK TABLES `t_fusionner_services` WRITE;
/*!40000 ALTER TABLE `t_fusionner_services` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_fusionner_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_groupement_entreprise`
--

LOCK TABLES `t_groupement_entreprise` WRITE;
/*!40000 ALTER TABLE `t_groupement_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_groupement_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_historique_annonce`
--

LOCK TABLES `t_historique_annonce` WRITE;
/*!40000 ALTER TABLE `t_historique_annonce` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_historique_annonce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_historique_synchronisation_SGMAP`
--

LOCK TABLES `t_historique_synchronisation_SGMAP` WRITE;
/*!40000 ALTER TABLE `t_historique_synchronisation_SGMAP` DISABLE KEYS */;
INSERT INTO `t_historique_synchronisation_SGMAP` (`id_historique`, `id_objet`, `type_objet`, `code`, `jeton`, `date_creation`) VALUES (1,NULL,'SERVICE','831670377','{\"entreprise\":{\"siren\":\"831670377\",\"capital_social\":65000,\"numero_tva_intracommunautaire\":\"FR35831670377\",\"forme_juridique\":\"Soci\\u00e9t\\u00e9 par actions simplifi\\u00e9e \\u00e0 associ\\u00e9 unique ou soci\\u00e9t\\u00e9 par actions simplifi\\u00e9e unipersonnelle \",\"forme_juridique_code\":\"5720\",\"nom_commercial\":\"\",\"procedure_collective\":false,\"enseigne\":null,\"libelle_naf_entreprise\":\"\\u00c9dition de logiciels syst\\u00e8me et de r\\u00e9seau\",\"naf_entreprise\":\"5829A\",\"raison_sociale\":\"MYPRM\",\"siret_siege_social\":\"83167037700017\",\"code_effectif_entreprise\":\"00\",\"date_creation\":1500933600,\"nom\":null,\"prenom\":null,\"date_radiation\":null,\"categorie_entreprise\":null,\"tranche_effectif_salarie_entreprise\":{\"de\":0,\"a\":0,\"code\":\"00\",\"date_reference\":\"2017\",\"intitule\":\"0 salari\\u00e9\"},\"mandataires_sociaux\":[{\"nom\":\"BESSIS\",\"prenom\":\"RICHARD ANDRE JOSSELIN\",\"fonction\":\"PRESIDENT\",\"date_naissance\":\"1973-03-17\",\"date_naissance_timestamp\":101170800,\"dirigeant\":true,\"raison_sociale\":\"\",\"identifiant\":\"\",\"type\":\"PP\"},{\"nom\":\"BAVENCOFF\",\"prenom\":\"LOIC\",\"fonction\":\"DIRECTEUR GENERAL\",\"date_naissance\":\"1973-07-05\",\"date_naissance_timestamp\":110674800,\"dirigeant\":true,\"raison_sociale\":\"\",\"identifiant\":\"\",\"type\":\"PP\"}]},\"etablissement_siege\":{\"siege_social\":true,\"siret\":\"83167037700017\",\"naf\":\"5829A\",\"libelle_naf\":\"\\u00c9dition de logiciels syst\\u00e8me et de r\\u00e9seau\",\"date_mise_a_jour\":1504735200,\"tranche_effectif_salarie_etablissement\":{\"de\":0,\"a\":0,\"code\":\"00\",\"date_reference\":\"2017\",\"intitule\":\"0 salari\\u00e9\"},\"date_creation_etablissement\":1500933600,\"region_implantation\":{\"code\":\"11\",\"value\":\"\\u00cele-de-France\"},\"commune_implantation\":{\"code\":\"75108\",\"value\":\"PARIS 8\"},\"pays_implantation\":{\"code\":null,\"value\":null},\"diffusable_commercialement\":true,\"enseigne\":null,\"adresse\":{\"l1\":\"MYPRM\",\"l2\":null,\"l3\":null,\"l4\":\"128 RUE LA BOETIE\",\"l5\":null,\"l6\":\"75008 PARIS\",\"l7\":\"FRANCE\",\"numero_voie\":\"128\",\"type_voie\":\"RUE\",\"nom_voie\":\"LA BOETIE\",\"complement_adresse\":null,\"code_postal\":\"75008\",\"localite\":\"PARIS 8\",\"code_insee_localite\":\"75108\",\"cedex\":null}},\"gateway_error\":false}','2019-03-14 17:37:10'),(2,NULL,'SERVICE','83167037700017','{\"etablissement\":{\"siege_social\":true,\"siret\":\"83167037700017\",\"naf\":\"5829A\",\"libelle_naf\":\"\\u00c9dition de logiciels syst\\u00e8me et de r\\u00e9seau\",\"date_mise_a_jour\":1504735200,\"tranche_effectif_salarie_etablissement\":{\"de\":0,\"a\":0,\"code\":\"00\",\"date_reference\":\"2017\",\"intitule\":\"0 salari\\u00e9\"},\"date_creation_etablissement\":1500933600,\"region_implantation\":{\"code\":\"11\",\"value\":\"\\u00cele-de-France\"},\"commune_implantation\":{\"code\":\"75108\",\"value\":\"PARIS 8\"},\"pays_implantation\":{\"code\":null,\"value\":null},\"diffusable_commercialement\":true,\"enseigne\":null,\"adresse\":{\"l1\":\"MYPRM\",\"l2\":null,\"l3\":null,\"l4\":\"128 RUE LA BOETIE\",\"l5\":null,\"l6\":\"75008 PARIS\",\"l7\":\"FRANCE\",\"numero_voie\":\"128\",\"type_voie\":\"RUE\",\"nom_voie\":\"LA BOETIE\",\"complement_adresse\":null,\"code_postal\":\"75008\",\"localite\":\"PARIS 8\",\"code_insee_localite\":\"75108\",\"cedex\":null}},\"gateway_error\":false}','2019-03-14 17:37:11'),(3,NULL,'ETABLISSEMENT','83167037700017','{\"etablissement\":{\"siege_social\":true,\"siret\":\"83167037700017\",\"naf\":\"5829A\",\"libelle_naf\":\"\\u00c9dition de logiciels syst\\u00e8me et de r\\u00e9seau\",\"date_mise_a_jour\":1504735200,\"tranche_effectif_salarie_etablissement\":{\"de\":0,\"a\":0,\"code\":\"00\",\"date_reference\":\"2017\",\"intitule\":\"0 salari\\u00e9\"},\"date_creation_etablissement\":1500933600,\"region_implantation\":{\"code\":\"11\",\"value\":\"\\u00cele-de-France\"},\"commune_implantation\":{\"code\":\"75108\",\"value\":\"PARIS 8\"},\"pays_implantation\":{\"code\":null,\"value\":null},\"diffusable_commercialement\":true,\"enseigne\":null,\"adresse\":{\"l1\":\"MYPRM\",\"l2\":null,\"l3\":null,\"l4\":\"128 RUE LA BOETIE\",\"l5\":null,\"l6\":\"75008 PARIS\",\"l7\":\"FRANCE\",\"numero_voie\":\"128\",\"type_voie\":\"RUE\",\"nom_voie\":\"LA BOETIE\",\"complement_adresse\":null,\"code_postal\":\"75008\",\"localite\":\"PARIS 8\",\"code_insee_localite\":\"75108\",\"cedex\":null}},\"gateway_error\":false}','2019-03-14 17:37:11');
/*!40000 ALTER TABLE `t_historique_synchronisation_SGMAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_information_modification_password`
--

LOCK TABLES `t_information_modification_password` WRITE;
/*!40000 ALTER TABLE `t_information_modification_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_information_modification_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_lancement_unique_cli`
--

LOCK TABLES `t_lancement_unique_cli` WRITE;
/*!40000 ALTER TABLE `t_lancement_unique_cli` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_lancement_unique_cli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_liste_lots_candidature`
--

LOCK TABLES `t_liste_lots_candidature` WRITE;
/*!40000 ALTER TABLE `t_liste_lots_candidature` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_liste_lots_candidature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_lot_technique`
--

LOCK TABLES `t_lot_technique` WRITE;
/*!40000 ALTER TABLE `t_lot_technique` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_lot_technique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_lot_technique_has_tranche`
--

LOCK TABLES `t_lot_technique_has_tranche` WRITE;
/*!40000 ALTER TABLE `t_lot_technique_has_tranche` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_lot_technique_has_tranche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_membre_groupement_entreprise`
--

LOCK TABLES `t_membre_groupement_entreprise` WRITE;
/*!40000 ALTER TABLE `t_membre_groupement_entreprise` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_membre_groupement_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_message_accueil`
--

LOCK TABLES `t_message_accueil` WRITE;
/*!40000 ALTER TABLE `t_message_accueil` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_message_accueil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_notification_agent`
--

LOCK TABLES `t_notification_agent` WRITE;
/*!40000 ALTER TABLE `t_notification_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_notification_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_numero_reponse`
--

LOCK TABLES `t_numero_reponse` WRITE;
/*!40000 ALTER TABLE `t_numero_reponse` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_numero_reponse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_numerotation_automatique`
--

LOCK TABLES `t_numerotation_automatique` WRITE;
/*!40000 ALTER TABLE `t_numerotation_automatique` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_numerotation_automatique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_param_dossier_formulaire`
--

LOCK TABLES `t_param_dossier_formulaire` WRITE;
/*!40000 ALTER TABLE `t_param_dossier_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_param_dossier_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_parametrage_formulaire_publicite`
--

LOCK TABLES `t_parametrage_formulaire_publicite` WRITE;
/*!40000 ALTER TABLE `t_parametrage_formulaire_publicite` DISABLE KEYS */;
INSERT INTO `t_parametrage_formulaire_publicite` (`id`, `id_support`, `id_type_annonce`, `ids_formulaire`) VALUES (5,1,2,'2,3,6,8,19'),(6,2,2,'2,3,6,8,19'),(7,4,2,'2,3,6,8,19'),(8,3,2,'(NULL)'),(9,3,3,'4,18'),(10,1,3,'4,18'),(11,2,3,'4,18'),(12,4,3,'4,18'),(17,1,13,NULL),(18,2,13,NULL),(19,4,13,NULL),(20,3,13,'1,5,7,9,10,16,17');
/*!40000 ALTER TABLE `t_parametrage_formulaire_publicite` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_preference_offre_support_publication`
--

LOCK TABLES `t_preference_offre_support_publication` WRITE;
/*!40000 ALTER TABLE `t_preference_offre_support_publication` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preference_offre_support_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_preference_support_publication`
--

LOCK TABLES `t_preference_support_publication` WRITE;
/*!40000 ALTER TABLE `t_preference_support_publication` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preference_support_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_procedure_equivalence_dume`
--

LOCK TABLES `t_procedure_equivalence_dume` WRITE;
/*!40000 ALTER TABLE `t_procedure_equivalence_dume` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_procedure_equivalence_dume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_publicite_favoris`
--

LOCK TABLES `t_publicite_favoris` WRITE;
/*!40000 ALTER TABLE `t_publicite_favoris` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_publicite_favoris` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_referentiel_certificat`
--

LOCK TABLES `t_referentiel_certificat` WRITE;
/*!40000 ALTER TABLE `t_referentiel_certificat` DISABLE KEYS */;
INSERT INTO `t_referentiel_certificat` (`id`, `nom_referentiel_certificat`, `statut_referentiel_certificat`, `nom_referentiel_fonctionnel`, `message`) VALUES (1,'atx',0,'AC de test ATEXO',NULL),(2,'europe_fr',0,'',NULL),(3,'lux',0,'',NULL),(4,'place',0,'Certificats de l\'Etat',NULL),(5,'prisv1',2,'PRIS V1',NULL),(6,'rgs',0,'RGS',NULL),(7,'ATEXO',0,'AC de test ATEXO',NULL),(8,'PLACE',0,'Certificats de l\'Etat',NULL),(9,'PRISV1',2,'PRIS V1',NULL),(10,'RGS1E',0,'RGS 1 Etoile',NULL),(11,'RGS2E',0,'RGS 2 Etoiles',NULL),(12,'RGS3E',0,'RGS 3 Etoiles',NULL),(13,'TSL-AT',0,'TSL-AT',NULL),(14,'TSL-BE',0,'TSL-BE',NULL),(15,'TSL-BG',0,'TSL-BG',NULL),(16,'TSL-CY',0,'TSL-CY',NULL),(17,'TSL-CZ',0,'TSL-CZ',NULL),(18,'TSL-DE',0,'TSL-DE',NULL),(19,'TSL-DK',0,'TSL-DK',NULL),(20,'TSL-EE',0,'TSL-EE',NULL),(21,'TSL-EL',0,'TSL-EL',NULL),(22,'TSL-ES',0,'TSL-ES',NULL),(23,'TSL-FI',0,'TSL-FI',NULL),(24,'TSL-FR',0,'TSL-FR',NULL),(25,'TSL-HR',0,'TSL-HR',NULL),(26,'TSL-HU',0,'TSL-HU',NULL),(27,'TSL-IE',0,'TSL-IE',NULL),(28,'TSL-IT',0,'TSL-IT',NULL),(29,'TSL-LI',0,'TSL-LI',NULL),(30,'TSL-LT',0,'TSL-LT',NULL),(31,'TSL-LU',0,'TSL-LU',NULL),(32,'TSL-LV',0,'TSL-LV',NULL),(33,'TSL-MT',0,'TSL-MT',NULL),(34,'TSL-NL',0,'TSL-NL',NULL),(35,'TSL-NO',0,'TSL-NO',NULL),(36,'TSL-PL',0,'TSL-PL',NULL),(37,'TSL-PT',0,'TSL-PT',NULL),(38,'TSL-RO',0,'TSL-RO',NULL),(39,'TSL-SE',0,'TSL-SE',NULL),(40,'TSL-SI',0,'TSL-SI',NULL),(41,'TSL-SK',0,'TSL-SK',NULL),(42,'TSL-UK',0,'TSL-UK',NULL);
/*!40000 ALTER TABLE `t_referentiel_certificat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_referentiel_mots_cles`
--

LOCK TABLES `t_referentiel_mots_cles` WRITE;
/*!40000 ALTER TABLE `t_referentiel_mots_cles` DISABLE KEYS */;
INSERT INTO `t_referentiel_mots_cles` (`id`, `libelle`, `valeur_sub`) VALUES (1,'Abonnement','001-002'),(2,'Abris','002-001'),(3,'Acier','003-003'),(4,'Aéronautique','004-004'),(5,'Aire d\'accueil','005-430'),(6,'Aire de jeux','006-400'),(7,'Alarme','007-005'),(8,'Alimentation en eau potable','008-006'),(9,'Aliments pour animaux','009-007'),(10,'Amiante (désamiantage)','010-024'),(11,'Amiante (diagnostic)','011-431'),(12,'Analyses médicales, biologiques','012-008'),(13,'Animaux','013-009'),(14,'Antenne','014-010'),(15,'Antipollution (équipement)','015-432'),(16,'Appareil élévateur','016-011'),(17,'Armement','017-012'),(18,'Arrosage','018-013'),(19,'Articles à usage unique','019-015'),(20,'Articles de bureau et articles scolaires','020-016'),(21,'Articles funéraires, cercueil','021-401'),(22,'Ascenseur','022-017'),(23,'Assainissement','023-018'),(24,'Assistance à maîtrise d\'ouvrage','024-433'),(27,'Assistance juridique','025-434'),(28,'Assistance technique','026-019'),(29,'Assurance','027-020'),(30,'Audiovisuel','028-022'),(31,'Audit','029-021'),(32,'Autocommutateur','030-023'),(33,'Automate','031-402'),(34,'Automatisation','032-014'),(35,'Bâche','033-435'),(36,'Banc d\'essais','034-029'),(37,'Bardage','035-030'),(38,'Barquettes alimentaires','036-032'),(39,'Barrage','037-031'),(40,'Bassin, étang','038-028'),(41,'Bâtiment','039-033'),(45,'Bâtiments modulaires','040-027'),(46,'Benne','041-034'),(47,'Berge','042-035'),(48,'Blanchissage','043-036'),(49,'Blanchisserie (matériel)','044-037'),(50,'Bois','045-038'),(51,'Boisson','046-039'),(52,'Boîtes aux lettres','047-041'),(53,'Busage','048-040'),(54,'Câblage','050-047'),(55,'Canalisations','051-048'),(56,'Candélabre','052-049'),(57,'Caoutchouc','053-050'),(58,'Captage','054-051'),(59,'Carburant, hydrocarbure','055-052'),(60,'Carrelage','056-053'),(61,'Carte magnétique, carte à puce','057-403'),(62,'Carton','058-054'),(63,'Cd, DVD','059-045'),(64,'Chambre froide','061-056'),(65,'Charpente','062-057'),(66,'Chaudronnerie','063-058'),(67,'Chauffage (exploitation, entretien)','064-059'),(68,'Chauffage (matériel)','065-060'),(69,'Chauffage (travaux)','066-061'),(70,'Chaussures ou articles chaussants','067-079'),(71,'Chèque cadeau','068-436'),(72,'Cimetière','069-437'),(73,'Climatisation','070-062'),(74,'Clocher','071-080'),(75,'Cloison, faux plafond','072-063'),(76,'Clôture','073-064'),(77,'Colis','074-065'),(78,'Collecte sélective','075-082'),(79,'Combustible','076-438'),(80,'Composteur','077-439'),(81,'Compteur d\'eau','078-440'),(82,'Conditionnement','080-067'),(83,'Consommable informatique','081-068'),(84,'Consommables médicaux','082-069'),(85,'Construction navale','083-070'),(86,'Conteneur, poubelles','084-071'),(87,'Contrôle d\'accès','085-081'),(88,'Contrôle technique','086-072'),(89,'Couches','087-073'),(90,'Couverture','088-074'),(91,'Crédit-bail','089-075'),(92,'Cuir','090-076'),(93,'Cuisine (équipement)','091-077'),(94,'Curage','092-078'),(95,'Décharge','093-084'),(96,'Déchets non ménagers (prestations)','094-404'),(97,'Déchetterie (exploitation)','095-405'),(98,'Déchetterie (travaux)','096-096'),(101,'Déclencheur d\'avalanche,explosif','097-083'),(102,'Décoration (travaux)','098-085'),(103,'Délégation de service public','099-406'),(104,'Dématérialisation','100-097'),(105,'Déménagement','101-086'),(106,'Démolition','102-087'),(107,'Déneigement','103-098'),(108,'Denrées alimentaires','104-088'),(109,'Dépollution','105-442'),(110,'Dératisation, désinsectisation','106-089'),(111,'Détection incendie','107-090'),(112,'Diététique','108-407'),(113,'Digue','109-091'),(114,'Dragage','111-093'),(115,'Drainage','112-094'),(116,'Droguerie','113-095'),(117,'Echafaudage','114-101'),(118,'Eclairage public','115-102'),(119,'Ecluse','116-103'),(120,'Edition','117-104'),(125,'Electricité, gaz (fourniture)','118-443'),(126,'Electricité (travaux)','119-105'),(127,'Electromécanique','121-107'),(128,'Electroménager','122-108'),(129,'Enneigement artificiel','123-109'),(130,'Enrochements','124-110'),(131,'Eolienne','125-444'),(132,'Equarrissage','126-408'),(133,'Equipement de piscine','127-120'),(138,'Equipement hydraulique','128-111'),(139,'Equipement industriel','129-112'),(140,'Equipement muséographique','130-409'),(141,'Equipement sportif','131-113'),(142,'Equipement thermique','132-114'),(143,'Equipements spécialisés','133-115'),(144,'Espaces verts','134-116'),(145,'Etanchéité','135-117'),(150,'Etude','136-118'),(151,'Evénementiel','137-445'),(152,'Expertise comptable','138-446'),(153,'Extincteur','139-119'),(154,'Falaise','140-125'),(155,'Ferronnerie','141-126'),(159,'Feu d\'artifice','142-447'),(160,'Fibre optique','143-133'),(161,'Film plastique','145-448'),(162,'Fluides médicaux','146-128'),(163,'Fondations spéciales','147-129'),(164,'Fonderie','148-130'),(165,'Fontaine','149-449'),(166,'Fontainerie','150-131'),(167,'Forage','151-132'),(168,'Formation','152-410'),(169,'Fouille archéologique','153-450'),(170,'Fourrière','154-451'),(171,'Gardiennage','155-138'),(175,'Gaz divers','156-139'),(181,'Gaz médicaux','157-140'),(182,'Génie civil','158-141'),(183,'Géothermie','159-452'),(184,'Glissières de sécurité','160-142'),(185,'Gouttières','161-145'),(186,'Gros oeuvre','162-144'),(187,'Groupe électrogène','163-143'),(188,'Habillement','164-150'),(194,'Horodateur','165-151'),(195,'Hygiène (articles)','169-153'),(196,'Illumination','170-170'),(197,'Implants','171-159'),(198,'Impression','172-160'),(199,'Imprimés','173-161'),(200,'Informatique (assistance)','174-453'),(201,'Informatique (maintenance serveurs et réseaux)','175-454'),(202,'Informatique (matériel)','176-162'),(203,'Informatique (prestations de services)','177-163'),(204,'Ingénierie','178-455'),(205,'Insignes','179-164'),(206,'Installation portuaire','180-165'),(207,'Instruments de musique','181-166'),(208,'Interphone','183-167'),(209,'Irrigation','184-168'),(210,'Isolation','185-169'),(211,'Jeux, jouets','186-175'),(212,'Laboratoire (fourniture)','187-181'),(213,'Laboratoire (matériel)','188-188'),(214,'Literie','190-183'),(215,'Livres','191-184'),(216,'Location (matériels)','193-185'),(217,'Logiciel','194-186'),(218,'Lubrifiant','195-187'),(220,'Machines-outils','197-194'),(221,'Maçonnerie','198-195'),(222,'Maintenance','199-196'),(223,'Maîtrise d\'oeuvre','200-197'),(224,'Manutention','201-237'),(225,'Matériaux de construction','202-198'),(226,'Matériaux routiers','203-199'),(227,'Matériel agricole, horticole','204-200'),(228,'Matériel de bureau','205-207'),(229,'Matériel de manutention','206-208'),(230,'Matériel de précision','207-215'),(231,'Matériel de puériculture','208-458'),(232,'Matériel de secours et d\'incendie','209-217'),(233,'Matériel de sécurité','210-209'),(234,'Matériel de stérilisation','211-218'),(235,'Matériel de téléphonie','212-459'),(236,'Matériel de transmission','213-210'),(237,'Matériel de travaux publics','214-220'),(239,'Matériel dentaire','215-201'),(240,'Matériel didactique','216-202'),(241,'Matériel d\'imprimerie','217-211'),(247,'Matériel électrique','218-203'),(248,'Matériel électronique','219-204'),(249,'Matériel électronique médical','220-205'),(250,'Matériel ferroviaire','221-206'),(251,'Matériel médical','222-212'),(252,'Matériel phonique','223-213'),(253,'Matériel photographique et cinématographique','224-214'),(254,'Matériel robinetterie','225-460'),(255,'Matériel scénique','226-216'),(260,'Mécanique','227-221'),(261,'Menuiserie','228-222'),(262,'Métallerie','229-223'),(263,'Métallurgie','230-224'),(264,'Métaux précieux','231-225'),(265,'Minéraux divers','235-228'),(266,'Miroiterie','236-229'),(267,'Mise à disposition de personnel','237-461'),(268,'Mission d\'ordonnancement, pilotage, coordination (OPC)','239-462'),(269,'Mobilier','240-230'),(270,'Mobilier urbain','241-231'),(271,'Monuments historiques','242-236'),(272,'Mur antibruit','243-232'),(273,'Mur de soutènement','244-233'),(274,'Nettoyage de locaux','245-239'),(275,'Nettoyage urbain','246-240'),(276,'Non-tissés','247-241'),(277,'Numérisation','248-463'),(278,'Objet publicitaire','249-464'),(279,'Occultation','250-414'),(280,'Oeuvre d\'art','251-247'),(281,'Onduleur','252-465'),(282,'Optique','253-248'),(283,'Ordures ménagères (prestations)','254-249'),(284,'Orgue','255-250'),(285,'Outillage','256-251'),(286,'Ouvrage d\'art','257-252'),(287,'Ouvrage d\'infrastructure','258-253'),(288,'Ouvrage hydraulique','259-255'),(289,'Ouvrage métallique','260-254'),(291,'Palplanches','261-260'),(297,'Panneaux','262-261'),(298,'Papeterie','263-262'),(299,'Paratonnerre','264-411'),(300,'Passe à poissons','265-287'),(301,'Passerelle','266-286'),(302,'Peinture (fourniture)','267-263'),(303,'Peinture (travaux)','268-264'),(304,'Photovoltaïque','269-476'),(305,'Pièces détachées','270-266'),(306,'Piscine','271-265'),(307,'Plage','272-289'),(308,'Plancher technique','273-267'),(309,'Plastique','274-268'),(310,'Plâtrerie','275-269'),(311,'Plomberie (travaux)','276-270'),(312,'Pneumatiques','277-271'),(313,'Pompes','278-272'),(318,'Porte automatique, portail','279-412'),(319,'Prestation hotelière','282-466'),(320,'Prestations de services','283-467'),(321,'Produits chimiques','284-275'),(322,'Produits de l\'agriculture','285-276'),(323,'Produits de récupération','286-277'),(324,'Produits d\'entretien','287-278'),(325,'Produits des industries diverses','288-279'),(326,'Produits pharmaceutiques','289-280'),(327,'Produits radioactifs','290-282'),(328,'Produits travail des métaux','291-281'),(329,'Progiciel','292-283'),(330,'Protection, chutes de pierres','293-288'),(331,'Prothèses','294-284'),(332,'Publicité, Communication','295-285'),(333,'Quincaillerie (articles)','296-291'),(334,'Radar','297-297'),(335,'Radiologie (matériel)','298-313'),(336,'Radiologie (produits)','299-298'),(337,'Ramonage','300-413'),(338,'Ravalement','301-299'),(339,'Rayonnage','302-300'),(340,'Réactifs','303-301'),(341,'Reliure','304-302'),(342,'Remembrement (travaux connexes)','305-303'),(343,'Repas, traiteur','306-304'),(344,'Reprographie','307-305'),(345,'Réseaux divers','308-306'),(346,'Réservoir','309-307'),(347,'Restauration collective','310-468'),(348,'Revêtements de sols','311-308'),(349,'Revêtements muraux','312-309'),(350,'Rivières, ruisseaux','313-310'),(351,'Routage','315-312'),(352,'Sacs','316-318'),(353,'Sanitaire','317-319'),(354,'Sécurité incendie','318-332'),(357,'Sel de déneigement','319-320'),(358,'Serre','320-469'),(359,'Serrurerie','321-321'),(360,'Signalétique','322-333'),(361,'Signalisation','323-322'),(362,'Site internet','324-470'),(363,'Sondage (enquête)','325-471'),(364,'Sondage (travaux)','326-323'),(365,'Sonorisation','327-324'),(366,'Spectacle','328-472'),(367,'Stand','329-325'),(372,'Station de pompage','330-326'),(400,'Station de refoulement','331-334'),(401,'Station de relevage','332-327'),(402,'Station d\'épuration (exploitation)','333-335'),(403,'Station d\'épuration (travaux)','334-328'),(404,'Stimulateurs cardiaques','335-329'),(405,'Surgelés','336-330'),(406,'Surveillance','337-331'),(407,'Tabac','338-336'),(408,'Télécabines, télésièges, téléskis','339-337'),(409,'Télécommunications','340-338'),(410,'Télégestion','341-352'),(411,'Télésurveillance','343-415'),(412,'Télévision','344-340'),(413,'Tente','345-473'),(414,'Terrain de sport','346-342'),(415,'Terrasse','347-354'),(416,'Terrassement','348-341'),(417,'Textile','349-343'),(418,'Titres de transport','350-417'),(419,'Titres restaurant','351-416'),(420,'Topographie','352-344'),(421,'Tous corps d\'état','353-345'),(422,'Transformateur','354-346'),(423,'Transport','355-347'),(424,'Travaux agricoles','356-348'),(425,'Travaux dans l\'eau','357-349'),(426,'Travaux de viabilité','358-353'),(427,'Travaux forestiers','359-350'),(428,'Travaux sous-marins','360-351'),(429,'Vaisselle','361-357'),(430,'Végétaux','362-358'),(431,'Véhicules (location)','363-474'),(432,'Véhicules (acquisition)','365-475'),(433,'Ventilation','366-360'),(434,'Vidéo, cinéma','368-362'),(435,'Vitraux','369-418'),(436,'Vitrerie','370-363'),(437,'Voies ferrées','371-364'),(438,'Voirie','372-365'),(439,'Voyage','374-367'),(440,'Zinguerie','375-372');
/*!40000 ALTER TABLE `t_referentiel_mots_cles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_referentiel_nace`
--

LOCK TABLES `t_referentiel_nace` WRITE;
/*!40000 ALTER TABLE `t_referentiel_nace` DISABLE KEYS */;
INSERT INTO `t_referentiel_nace` (`id`, `code_nace_5`, `libelle_activite_detaillee`, `code_nace_2`, `libelle_activite_general`) VALUES (1,'0111Z','Culture de céréales (à l\'exception du riz), de légumineuses et de graines oléagineuses','01','Culture et production animale, chasse et services annexes'),(2,'0112Z','Culture du riz','01','Culture et production animale, chasse et services annexes'),(3,'0113Z','Culture de légumes, de melons, de racines et de tubercules','01','Culture et production animale, chasse et services annexes'),(4,'0114Z','Culture de la canne à sucre','01','Culture et production animale, chasse et services annexes'),(5,'0115Z','Culture du tabac','01','Culture et production animale, chasse et services annexes'),(6,'0116Z','Culture de plantes à fibres','01','Culture et production animale, chasse et services annexes'),(7,'0119Z','Autres cultures non permanentes','01','Culture et production animale, chasse et services annexes'),(8,'0121Z','Culture de la vigne','01','Culture et production animale, chasse et services annexes'),(9,'0122Z','Culture de fruits tropicaux et subtropicaux','01','Culture et production animale, chasse et services annexes'),(10,'0123Z','Culture d\'agrumes','01','Culture et production animale, chasse et services annexes'),(11,'0124Z','Culture de fruits à pépins et à noyau','01','Culture et production animale, chasse et services annexes'),(12,'0125Z','Culture d\'autres fruits d\'arbres ou d\'arbustes et de fruits à coque','01','Culture et production animale, chasse et services annexes'),(13,'0126Z','Culture de fruits oléagineux','01','Culture et production animale, chasse et services annexes'),(14,'0127Z','Culture de plantes à boissons','01','Culture et production animale, chasse et services annexes'),(15,'0128Z','Culture de plantes à épices, aromatiques, médicinales et pharmaceutiques','01','Culture et production animale, chasse et services annexes'),(16,'0129Z','Autres cultures permanentes','01','Culture et production animale, chasse et services annexes'),(17,'0130Z','Reproduction de plantes','01','Culture et production animale, chasse et services annexes'),(18,'0141Z','Élevage de vaches laitières','01','Culture et production animale, chasse et services annexes'),(19,'0142Z','Élevage d\'autres bovins et de buffles','01','Culture et production animale, chasse et services annexes'),(20,'0143Z','Élevage de chevaux et d\'autres équidés','01','Culture et production animale, chasse et services annexes'),(21,'0144Z','Élevage de chameaux et d\'autres camélidés','01','Culture et production animale, chasse et services annexes'),(22,'0145Z','Élevage d\'ovins et de caprins','01','Culture et production animale, chasse et services annexes'),(23,'0146Z','Élevage de porcins','01','Culture et production animale, chasse et services annexes'),(24,'0147Z','Élevage de volailles','01','Culture et production animale, chasse et services annexes'),(25,'0149Z','Élevage d\'autres animaux','01','Culture et production animale, chasse et services annexes'),(26,'0150Z','Culture et élevage associés','01','Culture et production animale, chasse et services annexes'),(27,'0161Z','Activités de soutien aux cultures','01','Culture et production animale, chasse et services annexes'),(28,'0162Z','Activités de soutien à la production animale','01','Culture et production animale, chasse et services annexes'),(29,'0163Z','Traitement primaire des récoltes','01','Culture et production animale, chasse et services annexes'),(30,'0164Z','Traitement des semences','01','Culture et production animale, chasse et services annexes'),(31,'0170Z','Chasse, piégeage et services annexes','01','Culture et production animale, chasse et services annexes'),(32,'0210Z','Sylviculture et autres activités forestières','02','Sylviculture et exploitation forestière'),(33,'0220Z','Exploitation forestière','02','Sylviculture et exploitation forestière'),(34,'0230Z','Récolte de produits forestiers non ligneux poussant à l\'état sauvage','02','Sylviculture et exploitation forestière'),(35,'0240Z','Services de soutien à l\'exploitation forestière','02','Sylviculture et exploitation forestière'),(36,'0311Z','Pêche en mer','03','Pêche et aquaculture'),(37,'0312Z','Pêche en eau douce','03','Pêche et aquaculture'),(38,'0321Z','Aquaculture en mer','03','Pêche et aquaculture'),(39,'0322Z','Aquaculture en eau douce','03','Pêche et aquaculture'),(40,'0510Z','Extraction de houille','05','Extraction de houille et de lignite'),(41,'0520Z','Extraction de lignite','05','Extraction de houille et de lignite'),(42,'0610Z','Extraction de pétrole brut','06','Extraction d\'hydrocarbures'),(43,'0620Z','Extraction de gaz naturel','06','Extraction d\'hydrocarbures'),(44,'0710Z','Extraction de minerais de fer','07','Extraction de minerais métalliques'),(45,'0721Z','Extraction de minerais d\'uranium et de thorium','07','Extraction de minerais métalliques'),(46,'0729Z','Extraction d\'autres minerais de métaux non ferreux','07','Extraction de minerais métalliques'),(47,'0811Z','Extraction de pierres ornementales et de construction, de calcaire industriel, de gypse, de craie et d\'ardoise','08','Autres industries extractives'),(48,'0812Z','Exploitation de gravières et sablières, extraction d\'argiles et de kaolin','08','Autres industries extractives'),(49,'0891Z','Extraction des minéraux chimiques et d\'engrais minéraux','08','Autres industries extractives'),(50,'0892Z','Extraction de tourbe','08','Autres industries extractives'),(51,'0893Z','Production de sel','08','Autres industries extractives'),(52,'0899Z','Autres activités extractives n.c.a.','08','Autres industries extractives'),(53,'0910Z','Activités de soutien à l\'extraction d\'hydrocarbures','09','Services de soutien aux industries extractives'),(54,'0990Z','Activités de soutien aux autres industries extractives','09','Services de soutien aux industries extractives'),(55,'1011Z','Transformation et conservation de la viande de boucherie','10','Industries alimentaires'),(56,'1012Z','Transformation et conservation de la viande de volaille','10','Industries alimentaires'),(57,'1013A','Préparation industrielle de produits à base de viande','10','Industries alimentaires'),(58,'1013B','Charcuterie','10','Industries alimentaires'),(59,'1020Z','Transformation et conservation de poisson, de crustacés et de mollusques','10','Industries alimentaires'),(60,'1031Z','Transformation et conservation de pommes de terre','10','Industries alimentaires'),(61,'1032Z','Préparation de jus de fruits et légumes','10','Industries alimentaires'),(62,'1039A','Autre transformation et conservation de légumes','10','Industries alimentaires'),(63,'1039B','Transformation et conservation de fruits','10','Industries alimentaires'),(64,'1041A','Fabrication d\'huiles et graisses brutes','10','Industries alimentaires'),(65,'1041B','Fabrication d\'huiles et graisses raffinées','10','Industries alimentaires'),(66,'1042Z','Fabrication de margarine et graisses comestibles similaires','10','Industries alimentaires'),(67,'1051A','Fabrication de lait liquide et de produits frais','10','Industries alimentaires'),(68,'1051B','Fabrication de beurre','10','Industries alimentaires'),(69,'1051C','Fabrication de fromage','10','Industries alimentaires'),(70,'1051D','Fabrication d\'autres produits laitiers','10','Industries alimentaires'),(71,'1052Z','Fabrication de glaces et sorbets','10','Industries alimentaires'),(72,'1061A','Meunerie','10','Industries alimentaires'),(73,'1061B','Autres activités du travail des grains','10','Industries alimentaires'),(74,'1062Z','Fabrication de produits amylacés','10','Industries alimentaires'),(75,'1071A','Fabrication industrielle de pain et de pâtisserie fraîche','10','Industries alimentaires'),(76,'1071B','Cuisson de produits de boulangerie','10','Industries alimentaires'),(77,'1071C','Boulangerie et boulangerie-pâtisserie','10','Industries alimentaires'),(78,'1071D','Pâtisserie','10','Industries alimentaires'),(79,'1072Z','Fabrication de biscuits, biscottes et pâtisseries de conservation','10','Industries alimentaires'),(80,'1073Z','Fabrication de pâtes alimentaires','10','Industries alimentaires'),(81,'1081Z','Fabrication de sucre','10','Industries alimentaires'),(82,'1082Z','Fabrication de cacao, chocolat et de produits de confiserie','10','Industries alimentaires'),(83,'1083Z','Transformation du thé et du café','10','Industries alimentaires'),(84,'1084Z','Fabrication de condiments et assaisonnements','10','Industries alimentaires'),(85,'1085Z','Fabrication de plats préparés','10','Industries alimentaires'),(86,'1086Z','Fabrication d\'aliments homogénéisés et diététiques','10','Industries alimentaires'),(87,'1089Z','Fabrication d\'autres produits alimentaires n.c.a.','10','Industries alimentaires'),(88,'1091Z','Fabrication d\'aliments pour animaux de ferme','10','Industries alimentaires'),(89,'1092Z','Fabrication d\'aliments pour animaux de compagnie','10','Industries alimentaires'),(90,'1101Z','Production de boissons alcooliques distillées','11','Fabrication de boissons'),(91,'1102A','Fabrication de vins effervescents','11','Fabrication de boissons'),(92,'1102B','Vinification','11','Fabrication de boissons'),(93,'1103Z','Fabrication de cidre et de vins de fruits','11','Fabrication de boissons'),(94,'1104Z','Production d\'autres boissons fermentées non distillées','11','Fabrication de boissons'),(95,'1105Z','Fabrication de bière','11','Fabrication de boissons'),(96,'1106Z','Fabrication de malt','11','Fabrication de boissons'),(97,'1107A','Industrie des eaux de table','11','Fabrication de boissons'),(98,'1107B','Production de boissons rafraîchissantes','11','Fabrication de boissons'),(99,'1200Z','Fabrication de produits à base de tabac','12','Fabrication de produits à base de tabac'),(100,'1310Z','Préparation de fibres textiles et filature','13','Fabrication de textiles'),(101,'1320Z','Tissage','13','Fabrication de textiles'),(102,'1330Z','Ennoblissement textile','13','Fabrication de textiles'),(103,'1391Z','Fabrication d\'étoffes à mailles','13','Fabrication de textiles'),(104,'1392Z','Fabrication d\'articles textiles, sauf habillement','13','Fabrication de textiles'),(105,'1393Z','Fabrication de tapis et moquettes','13','Fabrication de textiles'),(106,'1394Z','Fabrication de ficelles, cordes et filets','13','Fabrication de textiles'),(107,'1395Z','Fabrication de non-tissés, sauf habillement','13','Fabrication de textiles'),(108,'1396Z','Fabrication d\'autres textiles techniques et industriels','13','Fabrication de textiles'),(109,'1399Z','Fabrication d\'autres textiles n.c.a.','13','Fabrication de textiles'),(110,'1411Z','Fabrication de vêtements en cuir','14','Industrie de l\'habillement'),(111,'1412Z','Fabrication de vêtements de travail','14','Industrie de l\'habillement'),(112,'1413Z','Fabrication de vêtements de dessus','14','Industrie de l\'habillement'),(113,'1414Z','Fabrication de vêtements de dessous','14','Industrie de l\'habillement'),(114,'1419Z','Fabrication d\'autres vêtements et accessoires','14','Industrie de l\'habillement'),(115,'1420Z','Fabrication d\'articles en fourrure','14','Industrie de l\'habillement'),(116,'1431Z','Fabrication d\'articles chaussants à mailles','14','Industrie de l\'habillement'),(117,'1439Z','Fabrication d\'autres articles à mailles','14','Industrie de l\'habillement'),(118,'1511Z','Apprêt et tannage des cuirs ; préparation et teinture des fourrures','15','Industrie du cuir et de la chaussure'),(119,'1512Z','Fabrication d\'articles de voyage, de maroquinerie et de sellerie','15','Industrie du cuir et de la chaussure'),(120,'1520Z','Fabrication de chaussures','15','Industrie du cuir et de la chaussure'),(121,'1610A','Sciage et rabotage du bois, hors imprégnation','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(122,'1610B','Imprégnation du bois','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(123,'1621Z','Fabrication de placage et de panneaux de bois','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(124,'1622Z','Fabrication de parquets assemblés','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(125,'1623Z','Fabrication de charpentes et d\'autres menuiseries','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(126,'1624Z','Fabrication d\'emballages en bois','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(127,'1629Z','Fabrication d\'objets divers en bois ; fabrication d\'objets en liège, vannerie et sparterie','16','Travail du bois et fabrication d\'articles en bois et en liège, à l\'exception des meubles ; fabrication d\'articles en vannerie et sparterie'),(128,'1711Z','Fabrication de pâte à papier','17','Industrie du papier et du carton'),(129,'1712Z','Fabrication de papier et de carton','17','Industrie du papier et du carton'),(130,'1721A','Fabrication de carton ondulé','17','Industrie du papier et du carton'),(131,'1721B','Fabrication de cartonnages','17','Industrie du papier et du carton'),(132,'1721C','Fabrication d\'emballages en papier','17','Industrie du papier et du carton'),(133,'1722Z','Fabrication d\'articles en papier à usage sanitaire ou domestique','17','Industrie du papier et du carton'),(134,'1723Z','Fabrication d\'articles de papeterie','17','Industrie du papier et du carton'),(135,'1724Z','Fabrication de papiers peints','17','Industrie du papier et du carton'),(136,'1729Z','Fabrication d\'autres articles en papier ou en carton','17','Industrie du papier et du carton'),(137,'1811Z','Imprimerie de journaux','18','Imprimerie et reproduction d\'enregistrements'),(138,'1812Z','Autre imprimerie (labeur)','18','Imprimerie et reproduction d\'enregistrements'),(139,'1813Z','Activités de pré-presse','18','Imprimerie et reproduction d\'enregistrements'),(140,'1814Z','Reliure et activités connexes','18','Imprimerie et reproduction d\'enregistrements'),(141,'1820Z','Reproduction d\'enregistrements','18','Imprimerie et reproduction d\'enregistrements'),(142,'1910Z','Cokéfaction','19','Cokéfaction et raffinage'),(143,'1920Z','Raffinage du pétrole','19','Cokéfaction et raffinage'),(144,'2011Z','Fabrication de gaz industriels','20','Industrie chimique'),(145,'2012Z','Fabrication de colorants et de pigments','20','Industrie chimique'),(146,'2013A','Enrichissement et retraitement de matières nucléaires','20','Industrie chimique'),(147,'2013B','Fabrication d\'autres produits chimiques inorganiques de base n.c.a.','20','Industrie chimique'),(148,'2014Z','Fabrication d\'autres produits chimiques organiques de base','20','Industrie chimique'),(149,'2015Z','Fabrication de produits azotés et d\'engrais','20','Industrie chimique'),(150,'2016Z','Fabrication de matières plastiques de base','20','Industrie chimique'),(151,'2017Z','Fabrication de caoutchouc synthétique','20','Industrie chimique'),(152,'2020Z','Fabrication de pesticides et d\'autres produits agrochimiques','20','Industrie chimique'),(153,'2030Z','Fabrication de peintures, vernis, encres et mastics','20','Industrie chimique'),(154,'2041Z','Fabrication de savons, détergents et produits d\'entretien','20','Industrie chimique'),(155,'2042Z','Fabrication de parfums et de produits pour la toilette','20','Industrie chimique'),(156,'2051Z','Fabrication de produits explosifs','20','Industrie chimique'),(157,'2052Z','Fabrication de colles','20','Industrie chimique'),(158,'2053Z','Fabrication d\'huiles essentielles','20','Industrie chimique'),(159,'2059Z','Fabrication d\'autres produits chimiques n.c.a.','20','Industrie chimique'),(160,'2060Z','Fabrication de fibres artificielles ou synthétiques','20','Industrie chimique'),(161,'2110Z','Fabrication de produits pharmaceutiques de base','21','Industrie pharmaceutique'),(162,'2120Z','Fabrication de préparations pharmaceutiques','21','Industrie pharmaceutique'),(163,'2211Z','Fabrication et rechapage de pneumatiques','22','Fabrication de produits en caoutchouc et en plastique'),(164,'2219Z','Fabrication d\'autres articles en caoutchouc','22','Fabrication de produits en caoutchouc et en plastique'),(165,'2221Z','Fabrication de plaques, feuilles, tubes et profilés en matières plastiques','22','Fabrication de produits en caoutchouc et en plastique'),(166,'2222Z','Fabrication d\'emballages en matières plastiques','22','Fabrication de produits en caoutchouc et en plastique'),(167,'2223Z','Fabrication d\'éléments en matières plastiques pour la construction','22','Fabrication de produits en caoutchouc et en plastique'),(168,'2229A','Fabrication de pièces techniques à base de matières plastiques','22','Fabrication de produits en caoutchouc et en plastique'),(169,'2229B','Fabrication de produits de consommation courante en matières plastiques','22','Fabrication de produits en caoutchouc et en plastique'),(170,'2311Z','Fabrication de verre plat','23','Fabrication d\'autres produits minéraux non métalliques'),(171,'2312Z','Façonnage et transformation du verre plat','23','Fabrication d\'autres produits minéraux non métalliques'),(172,'2313Z','Fabrication de verre creux','23','Fabrication d\'autres produits minéraux non métalliques'),(173,'2314Z','Fabrication de fibres de verre','23','Fabrication d\'autres produits minéraux non métalliques'),(174,'2319Z','Fabrication et façonnage d\'autres articles en verre, y compris verre technique','23','Fabrication d\'autres produits minéraux non métalliques'),(175,'2320Z','Fabrication de produits réfractaires','23','Fabrication d\'autres produits minéraux non métalliques'),(176,'2331Z','Fabrication de carreaux en céramique','23','Fabrication d\'autres produits minéraux non métalliques'),(177,'2332Z','Fabrication de briques, tuiles et produits de construction, en terre cuite','23','Fabrication d\'autres produits minéraux non métalliques'),(178,'2341Z','Fabrication d\'articles céramiques à usage domestique ou ornemental','23','Fabrication d\'autres produits minéraux non métalliques'),(179,'2342Z','Fabrication d\'appareils sanitaires en céramique','23','Fabrication d\'autres produits minéraux non métalliques'),(180,'2343Z','Fabrication d\'isolateurs et pièces isolantes en céramique','23','Fabrication d\'autres produits minéraux non métalliques'),(181,'2344Z','Fabrication d\'autres produits céramiques à usage technique','23','Fabrication d\'autres produits minéraux non métalliques'),(182,'2349Z','Fabrication d\'autres produits céramiques','23','Fabrication d\'autres produits minéraux non métalliques'),(183,'2351Z','Fabrication de ciment','23','Fabrication d\'autres produits minéraux non métalliques'),(184,'2352Z','Fabrication de chaux et plâtre','23','Fabrication d\'autres produits minéraux non métalliques'),(185,'2361Z','Fabrication d\'éléments en béton pour la construction','23','Fabrication d\'autres produits minéraux non métalliques'),(186,'2362Z','Fabrication d\'éléments en plâtre pour la construction','23','Fabrication d\'autres produits minéraux non métalliques'),(187,'2363Z','Fabrication de béton prêt à l\'emploi','23','Fabrication d\'autres produits minéraux non métalliques'),(188,'2364Z','Fabrication de mortiers et bétons secs','23','Fabrication d\'autres produits minéraux non métalliques'),(189,'2365Z','Fabrication d\'ouvrages en fibre-ciment','23','Fabrication d\'autres produits minéraux non métalliques'),(190,'2369Z','Fabrication d\'autres ouvrages en béton, en ciment ou en plâtre','23','Fabrication d\'autres produits minéraux non métalliques'),(191,'2370Z','Taille, façonnage et finissage de pierres','23','Fabrication d\'autres produits minéraux non métalliques'),(192,'2391Z','Fabrication de produits abrasifs','23','Fabrication d\'autres produits minéraux non métalliques'),(193,'2399Z','Fabrication d\'autres produits minéraux non métalliques n.c.a.','23','Fabrication d\'autres produits minéraux non métalliques'),(194,'2410Z','Sidérurgie','24','Métallurgie'),(195,'2420Z','Fabrication de tubes, tuyaux, profilés creux et accessoires correspondants en acier','24','Métallurgie'),(196,'2431Z','Étirage à froid de barres','24','Métallurgie'),(197,'2432Z','Laminage à froid de feuillards','24','Métallurgie'),(198,'2433Z','Profilage à froid par formage ou pliage','24','Métallurgie'),(199,'2434Z','Tréfilage à froid','24','Métallurgie'),(200,'2441Z','Production de métaux précieux','24','Métallurgie'),(201,'2442Z','Métallurgie de l\'aluminium','24','Métallurgie'),(202,'2443Z','Métallurgie du plomb, du zinc ou de l\'étain','24','Métallurgie'),(203,'2444Z','Métallurgie du cuivre','24','Métallurgie'),(204,'2445Z','Métallurgie des autres métaux non ferreux','24','Métallurgie'),(205,'2446Z','Élaboration et transformation de matières nucléaires','24','Métallurgie'),(206,'2451Z','Fonderie de fonte','24','Métallurgie'),(207,'2452Z','Fonderie d\'acier','24','Métallurgie'),(208,'2453Z','Fonderie de métaux légers','24','Métallurgie'),(209,'2454Z','Fonderie d\'autres métaux non ferreux','24','Métallurgie'),(210,'2511Z','Fabrication de structures métalliques et de parties de structures','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(211,'2512Z','Fabrication de portes et fenêtres en métal','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(212,'2521Z','Fabrication de radiateurs et de chaudières pour le chauffage central','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(213,'2529Z','Fabrication d\'autres réservoirs, citernes et conteneurs métalliques','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(214,'2530Z','Fabrication de générateurs de vapeur, à l\'exception des chaudières pour le chauffage central','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(215,'2540Z','Fabrication d\'armes et de munitions','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(216,'2550A','Forge, estampage, matriçage ; métallurgie des poudres','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(217,'2550B','Découpage, emboutissage','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(218,'2561Z','Traitement et revêtement des métaux','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(219,'2562A','Décolletage','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(220,'2562B','Mécanique industrielle','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(221,'2571Z','Fabrication de coutellerie','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(222,'2572Z','Fabrication de serrures et de ferrures','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(223,'2573A','Fabrication de moules et modèles','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(224,'2573B','Fabrication d\'autres outillages','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(225,'2591Z','Fabrication de fûts et emballages métalliques similaires','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(226,'2592Z','Fabrication d\'emballages métalliques légers','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(227,'2593Z','Fabrication d\'articles en fils métalliques, de chaînes et de ressorts','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(228,'2594Z','Fabrication de vis et de boulons','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(229,'2599A','Fabrication d\'articles métalliques ménagers','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(230,'2599B','Fabrication d\'autres articles métalliques','25','Fabrication de produits métalliques, à l\'exception des machines et des équipements'),(231,'2611Z','Fabrication de composants électroniques','26','Fabrication de produits informatiques, électroniques et optiques'),(232,'2612Z','Fabrication de cartes électroniques assemblées','26','Fabrication de produits informatiques, électroniques et optiques'),(233,'2620Z','Fabrication d\'ordinateurs et d\'équipements périphériques','26','Fabrication de produits informatiques, électroniques et optiques'),(234,'2630Z','Fabrication d\'équipements de communication','26','Fabrication de produits informatiques, électroniques et optiques'),(235,'2640Z','Fabrication de produits électroniques grand public','26','Fabrication de produits informatiques, électroniques et optiques'),(236,'2651A','Fabrication d\'équipements d\'aide à la navigation','26','Fabrication de produits informatiques, électroniques et optiques'),(237,'2651B','Fabrication d\'instrumentation scientifique et technique','26','Fabrication de produits informatiques, électroniques et optiques'),(238,'2652Z','Horlogerie','26','Fabrication de produits informatiques, électroniques et optiques'),(239,'2660Z','Fabrication d\'équipements d\'irradiation médicale, d\'équipements électromédicaux et électrothérapeutiques','26','Fabrication de produits informatiques, électroniques et optiques'),(240,'2670Z','Fabrication de matériels optique et photographique','26','Fabrication de produits informatiques, électroniques et optiques'),(241,'2680Z','Fabrication de supports magnétiques et optiques','26','Fabrication de produits informatiques, électroniques et optiques'),(242,'2711Z','Fabrication de moteurs, génératrices et transformateurs électriques','27','Fabrication d\'équipements électriques'),(243,'2712Z','Fabrication de matériel de distribution et de commande électrique','27','Fabrication d\'équipements électriques'),(244,'2720Z','Fabrication de piles et d\'accumulateurs électriques','27','Fabrication d\'équipements électriques'),(245,'2731Z','Fabrication de câbles de fibres optiques','27','Fabrication d\'équipements électriques'),(246,'2732Z','Fabrication d\'autres fils et câbles électroniques ou électriques','27','Fabrication d\'équipements électriques'),(247,'2733Z','Fabrication de matériel d\'installation électrique','27','Fabrication d\'équipements électriques'),(248,'2740Z','Fabrication d\'appareils d\'éclairage électrique','27','Fabrication d\'équipements électriques'),(249,'2751Z','Fabrication d\'appareils électroménagers','27','Fabrication d\'équipements électriques'),(250,'2752Z','Fabrication d\'appareils ménagers non électriques','27','Fabrication d\'équipements électriques'),(251,'2790Z','Fabrication d\'autres matériels électriques','27','Fabrication d\'équipements électriques'),(252,'2811Z','Fabrication de moteurs et turbines, à l\'exception des moteurs d\'avions et de véhicules','28','Fabrication de machines et équipements n.c.a.'),(253,'2812Z','Fabrication d\'équipements hydrauliques et pneumatiques','28','Fabrication de machines et équipements n.c.a.'),(254,'2813Z','Fabrication d\'autres pompes et compresseurs','28','Fabrication de machines et équipements n.c.a.'),(255,'2814Z','Fabrication d\'autres articles de robinetterie','28','Fabrication de machines et équipements n.c.a.'),(256,'2815Z','Fabrication d\'engrenages et d\'organes mécaniques de transmission','28','Fabrication de machines et équipements n.c.a.'),(257,'2821Z','Fabrication de fours et brûleurs','28','Fabrication de machines et équipements n.c.a.'),(258,'2822Z','Fabrication de matériel de levage et de manutention','28','Fabrication de machines et équipements n.c.a.'),(259,'2823Z','Fabrication de machines et d\'équipements de bureau (à l\'exception des ordinateurs et équipements périphériques)','28','Fabrication de machines et équipements n.c.a.'),(260,'2824Z','Fabrication d\'outillage portatif à moteur incorporé','28','Fabrication de machines et équipements n.c.a.'),(261,'2825Z','Fabrication d\'équipements aérauliques et frigorifiques industriels','28','Fabrication de machines et équipements n.c.a.'),(262,'2829A','Fabrication d\'équipements d\'emballage, de conditionnement et de pesage','28','Fabrication de machines et équipements n.c.a.'),(263,'2829B','Fabrication d\'autres machines d\'usage général','28','Fabrication de machines et équipements n.c.a.'),(264,'2830Z','Fabrication de machines agricoles et forestières','28','Fabrication de machines et équipements n.c.a.'),(265,'2841Z','Fabrication de machines-outils pour le travail des métaux','28','Fabrication de machines et équipements n.c.a.'),(266,'2849Z','Fabrication d\'autres machines-outils','28','Fabrication de machines et équipements n.c.a.'),(267,'2891Z','Fabrication de machines pour la métallurgie','28','Fabrication de machines et équipements n.c.a.'),(268,'2892Z','Fabrication de machines pour l\'extraction ou la construction','28','Fabrication de machines et équipements n.c.a.'),(269,'2893Z','Fabrication de machines pour l\'industrie agro-alimentaire','28','Fabrication de machines et équipements n.c.a.'),(270,'2894Z','Fabrication de machines pour les industries textiles','28','Fabrication de machines et équipements n.c.a.'),(271,'2895Z','Fabrication de machines pour les industries du papier et du carton','28','Fabrication de machines et équipements n.c.a.'),(272,'2896Z','Fabrication de machines pour le travail du caoutchouc ou des plastiques','28','Fabrication de machines et équipements n.c.a.'),(273,'2899A','Fabrication de machines d\'imprimerie','28','Fabrication de machines et équipements n.c.a.'),(274,'2899B','Fabrication d\'autres machines spécialisées','28','Fabrication de machines et équipements n.c.a.'),(275,'2910Z','Construction de véhicules automobiles','29','Industrie automobile'),(276,'2920Z','Fabrication de carrosseries et remorques','29','Industrie automobile'),(277,'2931Z','Fabrication d\'équipements électriques et électroniques automobiles','29','Industrie automobile'),(278,'2932Z','Fabrication d\'autres équipements automobiles','29','Industrie automobile'),(279,'3011Z','Construction de navires et de structures flottantes','30','Fabrication d\'autres matériels de transport'),(280,'3012Z','Construction de bateaux de plaisance','30','Fabrication d\'autres matériels de transport'),(281,'3020Z','Construction de locomotives et d\'autre matériel ferroviaire roulant','30','Fabrication d\'autres matériels de transport'),(282,'3030Z','Construction aéronautique et spatiale','30','Fabrication d\'autres matériels de transport'),(283,'3040Z','Construction de véhicules militaires de combat','30','Fabrication d\'autres matériels de transport'),(284,'3091Z','Fabrication de motocycles','30','Fabrication d\'autres matériels de transport'),(285,'3092Z','Fabrication de bicyclettes et de véhicules pour invalides','30','Fabrication d\'autres matériels de transport'),(286,'3099Z','Fabrication d\'autres équipements de transport n.c.a.','30','Fabrication d\'autres matériels de transport'),(287,'3101Z','Fabrication de meubles de bureau et de magasin','31','Fabrication de meubles'),(288,'3102Z','Fabrication de meubles de cuisine','31','Fabrication de meubles'),(289,'3103Z','Fabrication de matelas','31','Fabrication de meubles'),(290,'3109A','Fabrication de sièges d\'ameublement d\'intérieur','31','Fabrication de meubles'),(291,'3109B','Fabrication d\'autres meubles et industries connexes de l\'ameublement','31','Fabrication de meubles'),(292,'3211Z','Frappe de monnaie','32','Autres industries manufacturières'),(293,'3212Z','Fabrication d\'articles de joaillerie et bijouterie','32','Autres industries manufacturières'),(294,'3213Z','Fabrication d\'articles de bijouterie fantaisie et articles similaires','32','Autres industries manufacturières'),(295,'3220Z','Fabrication d\'instruments de musique','32','Autres industries manufacturières'),(296,'3230Z','Fabrication d\'articles de sport','32','Autres industries manufacturières'),(297,'3240Z','Fabrication de jeux et jouets','32','Autres industries manufacturières'),(298,'3250A','Fabrication de matériel médico-chirurgical et dentaire','32','Autres industries manufacturières'),(299,'3250B','Fabrication de lunettes','32','Autres industries manufacturières'),(300,'3291Z','Fabrication d\'articles de brosserie','32','Autres industries manufacturières'),(301,'3299Z','Autres activités manufacturières n.c.a.','32','Autres industries manufacturières'),(302,'3311Z','Réparation d\'ouvrages en métaux','33','Réparation et installation de machines et d\'équipements'),(303,'3312Z','Réparation de machines et équipements mécaniques','33','Réparation et installation de machines et d\'équipements'),(304,'3313Z','Réparation de matériels électroniques et optiques','33','Réparation et installation de machines et d\'équipements'),(305,'3314Z','Réparation d\'équipements électriques','33','Réparation et installation de machines et d\'équipements'),(306,'3315Z','Réparation et maintenance navale','33','Réparation et installation de machines et d\'équipements'),(307,'3316Z','Réparation et maintenance d\'aéronefs et d\'engins spatiaux','33','Réparation et installation de machines et d\'équipements'),(308,'3317Z','Réparation et maintenance d\'autres équipements de transport','33','Réparation et installation de machines et d\'équipements'),(309,'3319Z','Réparation d\'autres équipements','33','Réparation et installation de machines et d\'équipements'),(310,'3320A','Installation de structures métalliques, chaudronnées et de tuyauterie','33','Réparation et installation de machines et d\'équipements'),(311,'3320B','Installation de machines et équipements mécaniques','33','Réparation et installation de machines et d\'équipements'),(312,'3320C','Conception d\'ensemble et assemblage sur site industriel d\'équipements de contrôle des processus industriels','33','Réparation et installation de machines et d\'équipements'),(313,'3320D','Installation d\'équipements électriques, de matériels électroniques et optiques ou d\'autres matériels','33','Réparation et installation de machines et d\'équipements'),(314,'3511Z','Production d\'électricité','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(315,'3512Z','Transport d\'électricité','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(316,'3513Z','Distribution d\'électricité','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(317,'3514Z','Commerce d\'électricité','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(318,'3521Z','Production de combustibles gazeux','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(319,'3522Z','Distribution de combustibles gazeux par conduites','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(320,'3523Z','Commerce de combustibles gazeux par conduites','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(321,'3530Z','Production et distribution de vapeur et d\'air conditionné','35','Production et distribution d\'électricité, de gaz, de vapeur et d\'air conditionné'),(322,'3600Z','Captage, traitement et distribution d\'eau','36','Captage, traitement et distribution d\'eau'),(323,'3700Z','Collecte et traitement des eaux usées','37','Collecte et traitement des eaux usées'),(324,'3811Z','Collecte des déchets non dangereux','38','Collecte, traitement et élimination des déchets ; récupération'),(325,'3812Z','Collecte des déchets dangereux','38','Collecte, traitement et élimination des déchets ; récupération'),(326,'3821Z','Traitement et élimination des déchets non dangereux','38','Collecte, traitement et élimination des déchets ; récupération'),(327,'3822Z','Traitement et élimination des déchets dangereux','38','Collecte, traitement et élimination des déchets ; récupération'),(328,'3831Z','Démantèlement d\'épaves','38','Collecte, traitement et élimination des déchets ; récupération'),(329,'3832Z','Récupération de déchets triés','38','Collecte, traitement et élimination des déchets ; récupération'),(330,'3900Z','Dépollution et autres services de gestion des déchets','39','Dépollution et autres services de gestion des déchets'),(331,'4110A','Promotion immobilière de logements','41','Construction de bâtiments'),(332,'4110B','Promotion immobilière de bureaux','41','Construction de bâtiments'),(333,'4110C','Promotion immobilière d\'autres bâtiments','41','Construction de bâtiments'),(334,'4110D','Supports juridiques de programmes','41','Construction de bâtiments'),(335,'4120A','Construction de maisons individuelles','41','Construction de bâtiments'),(336,'4120B','Construction d\'autres bâtiments','41','Construction de bâtiments'),(337,'4211Z','Construction de routes et autoroutes','42','Génie civil'),(338,'4212Z','Construction de voies ferrées de surface et souterraines','42','Génie civil'),(339,'4213A','Construction d\'ouvrages d\'art','42','Génie civil'),(340,'4213B','Construction et entretien de tunnels','42','Génie civil'),(341,'4221Z','Construction de réseaux pour fluides','42','Génie civil'),(342,'4222Z','Construction de réseaux électriques et de télécommunications','42','Génie civil'),(343,'4291Z','Construction d\'ouvrages maritimes et fluviaux','42','Génie civil'),(344,'4299Z','Construction d\'autres ouvrages de génie civil n.c.a.','42','Génie civil'),(345,'4311Z','Travaux de démolition','43','Travaux de construction spécialisés'),(346,'4312A','Travaux de terrassement courants et travaux préparatoires','43','Travaux de construction spécialisés'),(347,'4312B','Travaux de terrassement spécialisés ou de grande masse','43','Travaux de construction spécialisés'),(348,'4313Z','Forages et sondages','43','Travaux de construction spécialisés'),(349,'4321A','Travaux d\'installation électrique dans tous locaux','43','Travaux de construction spécialisés'),(350,'4321B','Travaux d\'installation électrique sur la voie publique','43','Travaux de construction spécialisés'),(351,'4322A','Travaux d\'installation d\'eau et de gaz en tous locaux','43','Travaux de construction spécialisés'),(352,'4322B','Travaux d\'installation d\'équipements thermiques et de climatisation','43','Travaux de construction spécialisés'),(353,'4329A','Travaux d\'isolation','43','Travaux de construction spécialisés'),(354,'4329B','Autres travaux d\'installation n.c.a.','43','Travaux de construction spécialisés'),(355,'4331Z','Travaux de plâtrerie','43','Travaux de construction spécialisés'),(356,'4332A','Travaux de menuiserie bois et PVC','43','Travaux de construction spécialisés'),(357,'4332B','Travaux de menuiserie métallique et serrurerie','43','Travaux de construction spécialisés'),(358,'4332C','Agencement de lieux de vente','43','Travaux de construction spécialisés'),(359,'4333Z','Travaux de revêtement des sols et des murs','43','Travaux de construction spécialisés'),(360,'4334Z','Travaux de peinture et vitrerie','43','Travaux de construction spécialisés'),(361,'4339Z','Autres travaux de finition','43','Travaux de construction spécialisés'),(362,'4391A','Travaux de charpente','43','Travaux de construction spécialisés'),(363,'4391B','Travaux de couverture par éléments','43','Travaux de construction spécialisés'),(364,'4399A','Travaux d\'étanchéification','43','Travaux de construction spécialisés'),(365,'4399B','Travaux de montage de structures métalliques','43','Travaux de construction spécialisés'),(366,'4399C','Travaux de maçonnerie générale et gros ?uvre de bâtiment','43','Travaux de construction spécialisés'),(367,'4399D','Autres travaux spécialisés de construction','43','Travaux de construction spécialisés'),(368,'4399E','Location avec opérateur de matériel de construction','43','Travaux de construction spécialisés'),(369,'4511Z','Commerce de voitures et de véhicules automobiles légers','45','Commerce et réparation d\'automobiles et de motocycles'),(370,'4519Z','Commerce d\'autres véhicules automobiles','45','Commerce et réparation d\'automobiles et de motocycles'),(371,'4520A','Entretien et réparation de véhicules automobiles légers','45','Commerce et réparation d\'automobiles et de motocycles'),(372,'4520B','Entretien et réparation d\'autres véhicules automobiles','45','Commerce et réparation d\'automobiles et de motocycles'),(373,'4531Z','Commerce de gros d\'équipements automobiles','45','Commerce et réparation d\'automobiles et de motocycles'),(374,'4532Z','Commerce de détail d\'équipements automobiles','45','Commerce et réparation d\'automobiles et de motocycles'),(375,'4540Z','Commerce et réparation de motocycles','45','Commerce et réparation d\'automobiles et de motocycles'),(376,'4611Z','Intermédiaires du commerce en matières premières agricoles, animaux vivants, matières premières textiles et produits semi-finis','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(377,'4612A','Centrales d\'achat de carburant','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(378,'4612B','Autres intermédiaires du commerce en combustibles, métaux, minéraux et produits chimiques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(379,'4613Z','Intermédiaires du commerce en bois et matériaux de construction','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(380,'4614Z','Intermédiaires du commerce en machines, équipements industriels, navires et avions','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(381,'4615Z','Intermédiaires du commerce en meubles, articles de ménage et quincaillerie','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(382,'4616Z','Intermédiaires du commerce en textiles, habillement, fourrures, chaussures et articles en cuir','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(383,'4617A','Centrales d\'achat alimentaires','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(384,'4617B','Autres intermédiaires du commerce en denrées, boissons et tabac','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(385,'4618Z','Intermédiaires spécialisés dans le commerce d\'autres produits spécifiques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(386,'4619A','Centrales d\'achat non alimentaires','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(387,'4619B','Autres intermédiaires du commerce en produits divers','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(388,'4621Z','Commerce de gros (commerce interentreprises) de céréales, de tabac non manufacturé, de semences et d\'aliments pour le bétail','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(389,'4622Z','Commerce de gros (commerce interentreprises) de fleurs et plantes','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(390,'4623Z','Commerce de gros (commerce interentreprises) d\'animaux vivants','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(391,'4624Z','Commerce de gros (commerce interentreprises) de cuirs et peaux','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(392,'4631Z','Commerce de gros (commerce interentreprises) de fruits et légumes','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(393,'4632A','Commerce de gros (commerce interentreprises) de viandes de boucherie','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(394,'4632B','Commerce de gros (commerce interentreprises) de produits à base de viande','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(395,'4632C','Commerce de gros (commerce interentreprises) de volailles et gibier','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(396,'4633Z','Commerce de gros (commerce interentreprises) de produits laitiers, ?ufs, huiles et matières grasses comestibles','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(397,'4634Z','Commerce de gros (commerce interentreprises) de boissons','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(398,'4635Z','Commerce de gros (commerce interentreprises) de produits à base de tabac','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(399,'4636Z','Commerce de gros (commerce interentreprises) de sucre, chocolat et confiserie','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(400,'4637Z','Commerce de gros (commerce interentreprises) de café, thé, cacao et épices','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(401,'4638A','Commerce de gros (commerce interentreprises) de poissons, crustacés et mollusques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(402,'4638B','Commerce de gros (commerce interentreprises) alimentaire spécialisé divers','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(403,'4639A','Commerce de gros (commerce interentreprises) de produits surgelés','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(404,'4639B','Commerce de gros (commerce interentreprises) alimentaire non spécialisé','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(405,'4641Z','Commerce de gros (commerce interentreprises) de textiles','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(406,'4642Z','Commerce de gros (commerce interentreprises) d\'habillement et de chaussures','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(407,'4643Z','Commerce de gros (commerce interentreprises) d\'appareils électroménagers','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(408,'4644Z','Commerce de gros (commerce interentreprises) de vaisselle, verrerie et produits d\'entretien','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(409,'4645Z','Commerce de gros (commerce interentreprises) de parfumerie et de produits de beauté','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(410,'4646Z','Commerce de gros (commerce interentreprises) de produits pharmaceutiques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(411,'4647Z','Commerce de gros (commerce interentreprises) de meubles, de tapis et d\'appareils d\'éclairage','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(412,'4648Z','Commerce de gros (commerce interentreprises) d\'articles d\'horlogerie et de bijouterie','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(413,'4649Z','Commerce de gros (commerce interentreprises) d\'autres biens domestiques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(414,'4651Z','Commerce de gros (commerce interentreprises) d\'ordinateurs, d\'équipements informatiques périphériques et de logiciels','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(415,'4652Z','Commerce de gros (commerce interentreprises) de composants et d\'équipements électroniques et de télécommunication','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(416,'4661Z','Commerce de gros (commerce interentreprises) de matériel agricole','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(417,'4662Z','Commerce de gros (commerce interentreprises) de machines-outils','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(418,'4663Z','Commerce de gros (commerce interentreprises) de machines pour l\'extraction, la construction et le génie civil','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(419,'4664Z','Commerce de gros (commerce interentreprises) de machines pour l\'industrie textile et l\'habillement','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(420,'4665Z','Commerce de gros (commerce interentreprises) de mobilier de bureau','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(421,'4666Z','Commerce de gros (commerce interentreprises) d\'autres machines et équipements de bureau','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(422,'4669A','Commerce de gros (commerce interentreprises) de matériel électrique','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(423,'4669B','Commerce de gros (commerce interentreprises) de fournitures et équipements industriels divers','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(424,'4669C','Commerce de gros (commerce interentreprises) de fournitures et équipements divers pour le commerce et les services','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(425,'4671Z','Commerce de gros (commerce interentreprises) de combustibles et de produits annexes','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(426,'4672Z','Commerce de gros (commerce interentreprises) de minerais et métaux','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(427,'4673A','Commerce de gros (commerce interentreprises) de bois et de matériaux de construction','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(428,'4673B','Commerce de gros (commerce interentreprises) d\'appareils sanitaires et de produits de décoration','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(429,'4674A','Commerce de gros (commerce interentreprises) de quincaillerie','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(430,'4674B','Commerce de gros (commerce interentreprises) de fournitures pour la plomberie et le chauffage','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(431,'4675Z','Commerce de gros (commerce interentreprises) de produits chimiques','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(432,'4676Z','Commerce de gros (commerce interentreprises) d\'autres produits intermédiaires','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(433,'4677Z','Commerce de gros (commerce interentreprises) de déchets et débris','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(434,'4690Z','Commerce de gros (commerce interentreprises) non spécialisé','46','Commerce de gros, à l\'exception des automobiles et des motocycles'),(435,'4711A','Commerce de détail de produits surgelés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(436,'4711B','Commerce d\'alimentation générale','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(437,'4711C','Supérettes','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(438,'4711D','Supermarchés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(439,'4711E','Magasins multi-commerces','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(440,'4711F','Hypermarchés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(441,'4719A','Grands magasins','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(442,'4719B','Autres commerces de détail en magasin non spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(443,'4721Z','Commerce de détail de fruits et légumes en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(444,'4722Z','Commerce de détail de viandes et de produits à base de viande en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(445,'4723Z','Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(446,'4724Z','Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(447,'4725Z','Commerce de détail de boissons en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(448,'4726Z','Commerce de détail de produits à base de tabac en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(449,'4729Z','Autres commerces de détail alimentaires en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(450,'4730Z','Commerce de détail de carburants en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(451,'4741Z','Commerce de détail d\'ordinateurs, d\'unités périphériques et de logiciels en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(452,'4742Z','Commerce de détail de matériels de télécommunication en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(453,'4743Z','Commerce de détail de matériels audio et vidéo en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(454,'4751Z','Commerce de détail de textiles en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(455,'4752A','Commerce de détail de quincaillerie, peintures et verres en petites surfaces (moins de 400 m²)','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(456,'4752B','Commerce de détail de quincaillerie, peintures et verres en grandes surfaces (400 m² et plus)','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(457,'4753Z','Commerce de détail de tapis, moquettes et revêtements de murs et de sols en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(458,'4754Z','Commerce de détail d\'appareils électroménagers en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(459,'4759A','Commerce de détail de meubles','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(460,'4759B','Commerce de détail d\'autres équipements du foyer','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(461,'4761Z','Commerce de détail de livres en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(462,'4762Z','Commerce de détail de journaux et papeterie en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(463,'4763Z','Commerce de détail d\'enregistrements musicaux et vidéo en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(464,'4764Z','Commerce de détail d\'articles de sport en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(465,'4765Z','Commerce de détail de jeux et jouets en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(466,'4771Z','Commerce de détail d\'habillement en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(467,'4772A','Commerce de détail de la chaussure','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(468,'4772B','Commerce de détail de maroquinerie et d\'articles de voyage','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(469,'4773Z','Commerce de détail de produits pharmaceutiques en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(470,'4774Z','Commerce de détail d\'articles médicaux et orthopédiques en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(471,'4775Z','Commerce de détail de parfumerie et de produits de beauté en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(472,'4776Z','Commerce de détail de fleurs, plantes, graines, engrais, animaux de compagnie et aliments pour ces animaux en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(473,'4777Z','Commerce de détail d\'articles d\'horlogerie et de bijouterie en magasin spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(474,'4778A','Commerces de détail d\'optique','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(475,'4778B','Commerces de détail de charbons et combustibles','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(476,'4778C','Autres commerces de détail spécialisés divers','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(477,'4779Z','Commerce de détail de biens d\'occasion en magasin','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(478,'4781Z','Commerce de détail alimentaire sur éventaires et marchés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(479,'4782Z','Commerce de détail de textiles, d\'habillement et de chaussures sur éventaires et marchés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(480,'4789Z','Autres commerces de détail sur éventaires et marchés','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(481,'4791A','Vente à distance sur catalogue général','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(482,'4791B','Vente à distance sur catalogue spécialisé','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(483,'4799A','Vente à domicile','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(484,'4799B','Vente par automates et autres commerces de détail hors magasin, éventaires ou marchés n.c.a.','47','Commerce de détail, à l\'exception des automobiles et des motocycles'),(485,'4910Z','Transport ferroviaire interurbain de voyageurs','49','Transports terrestres et transport par conduites'),(486,'4920Z','Transports ferroviaires de fret','49','Transports terrestres et transport par conduites'),(487,'4931Z','Transports urbains et suburbains de voyageurs','49','Transports terrestres et transport par conduites'),(488,'4932Z','Transports de voyageurs par taxis','49','Transports terrestres et transport par conduites'),(489,'4939A','Transports routiers réguliers de voyageurs','49','Transports terrestres et transport par conduites'),(490,'4939B','Autres transports routiers de voyageurs','49','Transports terrestres et transport par conduites'),(491,'4939C','Téléphériques et remontées mécaniques','49','Transports terrestres et transport par conduites'),(492,'4941A','Transports routiers de fret interurbains','49','Transports terrestres et transport par conduites'),(493,'4941B','Transports routiers de fret de proximité','49','Transports terrestres et transport par conduites'),(494,'4941C','Location de camions avec chauffeur','49','Transports terrestres et transport par conduites'),(495,'4942Z','Services de déménagement','49','Transports terrestres et transport par conduites'),(496,'4950Z','Transports par conduites','49','Transports terrestres et transport par conduites'),(497,'5010Z','Transports maritimes et côtiers de passagers','50','Transports par eau'),(498,'5020Z','Transports maritimes et côtiers de fret','50','Transports par eau'),(499,'5030Z','Transports fluviaux de passagers','50','Transports par eau'),(500,'5040Z','Transports fluviaux de fret','50','Transports par eau'),(501,'5110Z','Transports aériens de passagers','51','Transports aériens'),(502,'5121Z','Transports aériens de fret','51','Transports aériens'),(503,'5122Z','Transports spatiaux','51','Transports aériens'),(504,'5210A','Entreposage et stockage frigorifique','52','Entreposage et services auxiliaires des transports'),(505,'5210B','Entreposage et stockage non frigorifique','52','Entreposage et services auxiliaires des transports'),(506,'5221Z','Services auxiliaires des transports terrestres','52','Entreposage et services auxiliaires des transports'),(507,'5222Z','Services auxiliaires des transports par eau','52','Entreposage et services auxiliaires des transports'),(508,'5223Z','Services auxiliaires des transports aériens','52','Entreposage et services auxiliaires des transports'),(509,'5224A','Manutention portuaire','52','Entreposage et services auxiliaires des transports'),(510,'5224B','Manutention non portuaire','52','Entreposage et services auxiliaires des transports'),(511,'5229A','Messagerie, fret express','52','Entreposage et services auxiliaires des transports'),(512,'5229B','Affrètement et organisation des transports','52','Entreposage et services auxiliaires des transports'),(513,'5310Z','Activités de poste dans le cadre d\'une obligation de service universel','53','Activités de poste et de courrier'),(514,'5320Z','Autres activités de poste et de courrier','53','Activités de poste et de courrier'),(515,'5510Z','Hôtels et hébergement similaire','55','Hébergement'),(516,'5520Z','Hébergement touristique et autre hébergement de courte durée','55','Hébergement'),(517,'5530Z','Terrains de camping et parcs pour caravanes ou véhicules de loisirs','55','Hébergement'),(518,'5590Z','Autres hébergements','55','Hébergement'),(519,'5610A','Restauration traditionnelle','56','Restauration'),(520,'5610B','Cafétérias et autres libres-services','56','Restauration'),(521,'5610C','Restauration de type rapide','56','Restauration'),(522,'5621Z','Services des traiteurs','56','Restauration'),(523,'5629A','Restauration collective sous contrat','56','Restauration'),(524,'5629B','Autres services de restauration n.c.a.','56','Restauration'),(525,'5630Z','Débits de boissons','56','Restauration'),(526,'5811Z','Édition de livres','58','Édition'),(527,'5812Z','Édition de répertoires et de fichiers d\'adresses','58','Édition'),(528,'5813Z','Édition de journaux','58','Édition'),(529,'5814Z','Édition de revues et périodiques','58','Édition'),(530,'5819Z','Autres activités d\'édition','58','Édition'),(531,'5821Z','Édition de jeux électroniques','58','Édition'),(532,'5829A','Édition de logiciels système et de réseau','58','Édition'),(533,'5829B','Édition de logiciels outils de développement et de langages','58','Édition'),(534,'5829C','Édition de logiciels applicatifs','58','Édition'),(535,'5911A','Production de films et de programmes pour la télévision','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(536,'5911B','Production de films institutionnels et publicitaires','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(537,'5911C','Production de films pour le cinéma','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(538,'5912Z','Post-production de films cinématographiques, de vidéo et de programmes de télévision','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(539,'5913A','Distribution de films cinématographiques','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(540,'5913B','Édition et distribution vidéo','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(541,'5914Z','Projection de films cinématographiques','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(542,'5920Z','Enregistrement sonore et édition musicale','59','Production de films cinématographiques, de vidéo et de programmes de télévision ; enregistrement sonore et édition musicale'),(543,'6010Z','Édition et diffusion de programmes radio','60','Programmation et diffusion'),(544,'6020A','Édition de chaînes généralistes','60','Programmation et diffusion'),(545,'6020B','Édition de chaînes thématiques','60','Programmation et diffusion'),(546,'6110Z','Télécommunications filaires','61','Télécommunications'),(547,'6120Z','Télécommunications sans fil','61','Télécommunications'),(548,'6130Z','Télécommunications par satellite','61','Télécommunications'),(549,'6190Z','Autres activités de télécommunication','61','Télécommunications'),(550,'6201Z','Programmation informatique','62','Programmation, conseil et autres activités informatiques'),(551,'6202A','Conseil en systèmes et logiciels informatiques','62','Programmation, conseil et autres activités informatiques'),(552,'6202B','Tierce maintenance de systèmes et d\'applications informatiques','62','Programmation, conseil et autres activités informatiques'),(553,'6203Z','Gestion d\'installations informatiques','62','Programmation, conseil et autres activités informatiques'),(554,'6209Z','Autres activités informatiques','62','Programmation, conseil et autres activités informatiques'),(555,'6311Z','Traitement de données, hébergement et activités connexes','63','Services d\'information'),(556,'6312Z','Portails Internet','63','Services d\'information'),(557,'6391Z','Activités des agences de presse','63','Services d\'information'),(558,'6399Z','Autres services d\'information n.c.a.','63','Services d\'information'),(559,'6411Z','Activités de banque centrale','64','Activités des services financiers, hors assurance et caisses de retraite'),(560,'6419Z','Autres intermédiations monétaires','64','Activités des services financiers, hors assurance et caisses de retraite'),(561,'6420Z','Activités des sociétés holding','64','Activités des services financiers, hors assurance et caisses de retraite'),(562,'6430Z','Fonds de placement et entités financières similaires','64','Activités des services financiers, hors assurance et caisses de retraite'),(563,'6491Z','Crédit-bail','64','Activités des services financiers, hors assurance et caisses de retraite'),(564,'6492Z','Autre distribution de crédit','64','Activités des services financiers, hors assurance et caisses de retraite'),(565,'6499Z','Autres activités des services financiers, hors assurance et caisses de retraite, n.c.a.','64','Activités des services financiers, hors assurance et caisses de retraite'),(566,'6511Z','Assurance vie','65','Assurance'),(567,'6512Z','Autres assurances','65','Assurance'),(568,'6520Z','Réassurance','65','Assurance'),(569,'6530Z','Caisses de retraite','65','Assurance'),(570,'6611Z','Administration de marchés financiers','66','Activités auxiliaires de services financiers et d\'assurance'),(571,'6612Z','Courtage de valeurs mobilières et de marchandises','66','Activités auxiliaires de services financiers et d\'assurance'),(572,'6619A','Supports juridiques de gestion de patrimoine mobilier','66','Activités auxiliaires de services financiers et d\'assurance'),(573,'6619B','Autres activités auxiliaires de services financiers, hors assurance et caisses de retraite, n.c.a.','66','Activités auxiliaires de services financiers et d\'assurance'),(574,'6621Z','Évaluation des risques et dommages','66','Activités auxiliaires de services financiers et d\'assurance'),(575,'6622Z','Activités des agents et courtiers d\'assurances','66','Activités auxiliaires de services financiers et d\'assurance'),(576,'6629Z','Autres activités auxiliaires d\'assurance et de caisses de retraite','66','Activités auxiliaires de services financiers et d\'assurance'),(577,'6630Z','Gestion de fonds','66','Activités auxiliaires de services financiers et d\'assurance'),(578,'6810Z','Activités des marchands de biens immobiliers','68','Activités immobilières'),(579,'6820A','Location de logements','68','Activités immobilières'),(580,'6820B','Location de terrains et d\'autres biens immobiliers','68','Activités immobilières'),(581,'6831Z','Agences immobilières','68','Activités immobilières'),(582,'6832A','Administration d\'immeubles et autres biens immobiliers','68','Activités immobilières'),(583,'6832B','Supports juridiques de gestion de patrimoine immobilier','68','Activités immobilières'),(584,'6910Z','Activités juridiques','69','Activités juridiques et comptables'),(585,'6920Z','Activités comptables','69','Activités juridiques et comptables'),(586,'7010Z','Activités des sièges sociaux','70','Activités des sièges sociaux ; conseil de gestion'),(587,'7021Z','Conseil en relations publiques et communication','70','Activités des sièges sociaux ; conseil de gestion'),(588,'7022Z','Conseil pour les affaires et autres conseils de gestion','70','Activités des sièges sociaux ; conseil de gestion'),(589,'7111Z','Activités d\'architecture','71','Activités d\'architecture et d\'ingénierie ; activités de contrôle et analyses techniques'),(590,'7112A','Activité des géomètres','71','Activités d\'architecture et d\'ingénierie ; activités de contrôle et analyses techniques'),(591,'7112B','Ingénierie, études techniques','71','Activités d\'architecture et d\'ingénierie ; activités de contrôle et analyses techniques'),(592,'7120A','Contrôle technique automobile','71','Activités d\'architecture et d\'ingénierie ; activités de contrôle et analyses techniques'),(593,'7120B','Analyses, essais et inspections techniques','71','Activités d\'architecture et d\'ingénierie ; activités de contrôle et analyses techniques'),(594,'7211Z','Recherche-développement en biotechnologie','72','Recherche-développement scientifique'),(595,'7219Z','Recherche-développement en autres sciences physiques et naturelles','72','Recherche-développement scientifique'),(596,'7220Z','Recherche-développement en sciences humaines et sociales','72','Recherche-développement scientifique'),(597,'7311Z','Activités des agences de publicité','73','Publicité et études de marché'),(598,'7312Z','Régie publicitaire de médias','73','Publicité et études de marché'),(599,'7320Z','Études de marché et sondages','73','Publicité et études de marché'),(600,'7410Z','Activités spécialisées de design','74','Autres activités spécialisées, scientifiques et techniques'),(601,'7420Z','Activités photographiques','74','Autres activités spécialisées, scientifiques et techniques'),(602,'7430Z','Traduction et interprétation','74','Autres activités spécialisées, scientifiques et techniques'),(603,'7490A','Activité des économistes de la construction','74','Autres activités spécialisées, scientifiques et techniques'),(604,'7490B','Activités spécialisées, scientifiques et techniques diverses','74','Autres activités spécialisées, scientifiques et techniques'),(605,'7500Z','Activités vétérinaires','75','Activités vétérinaires'),(606,'7711A','Location de courte durée de voitures et de véhicules automobiles légers','77','Activités de location et location-bail'),(607,'7711B','Location de longue durée de voitures et de véhicules automobiles légers','77','Activités de location et location-bail'),(608,'7712Z','Location et location-bail de camions','77','Activités de location et location-bail'),(609,'7721Z','Location et location-bail d\'articles de loisirs et de sport','77','Activités de location et location-bail'),(610,'7722Z','Location de vidéocassettes et disques vidéo','77','Activités de location et location-bail'),(611,'7729Z','Location et location-bail d\'autres biens personnels et domestiques','77','Activités de location et location-bail'),(612,'7731Z','Location et location-bail de machines et équipements agricoles','77','Activités de location et location-bail'),(613,'7732Z','Location et location-bail de machines et équipements pour la construction','77','Activités de location et location-bail'),(614,'7733Z','Location et location-bail de machines de bureau et de matériel informatique','77','Activités de location et location-bail'),(615,'7734Z','Location et location-bail de matériels de transport par eau','77','Activités de location et location-bail'),(616,'7735Z','Location et location-bail de matériels de transport aérien','77','Activités de location et location-bail'),(617,'7739Z','Location et location-bail d\'autres machines, équipements et biens matériels n.c.a.','77','Activités de location et location-bail'),(618,'7740Z','Location-bail de propriété intellectuelle et de produits similaires, à l\'exception des ?uvres soumises à copyright','77','Activités de location et location-bail'),(619,'7810Z','Activités des agences de placement de main-d\'?uvre','78','Activités liées à l\'emploi'),(620,'7820Z','Activités des agences de travail temporaire','78','Activités liées à l\'emploi'),(621,'7830Z','Autre mise à disposition de ressources humaines','78','Activités liées à l\'emploi'),(622,'7911Z','Activités des agences de voyage','79','Activités des agences de voyage, voyagistes, services de réservation et activités connexes'),(623,'7912Z','Activités des voyagistes','79','Activités des agences de voyage, voyagistes, services de réservation et activités connexes'),(624,'7990Z','Autres services de réservation et activités connexes','79','Activités des agences de voyage, voyagistes, services de réservation et activités connexes'),(625,'8010Z','Activités de sécurité privée','80','Enquêtes et sécurité'),(626,'8020Z','Activités liées aux systèmes de sécurité','80','Enquêtes et sécurité'),(627,'8030Z','Activités d\'enquête','80','Enquêtes et sécurité'),(628,'8110Z','Activités combinées de soutien lié aux bâtiments','81','Services relatifs aux bâtiments et aménagement paysager'),(629,'8121Z','Nettoyage courant des bâtiments','81','Services relatifs aux bâtiments et aménagement paysager'),(630,'8122Z','Autres activités de nettoyage des bâtiments et nettoyage industriel','81','Services relatifs aux bâtiments et aménagement paysager'),(631,'8129A','Désinfection, désinsectisation, dératisation','81','Services relatifs aux bâtiments et aménagement paysager'),(632,'8129B','Autres activités de nettoyage n.c.a.','81','Services relatifs aux bâtiments et aménagement paysager'),(633,'8130Z','Services d\'aménagement paysager','81','Services relatifs aux bâtiments et aménagement paysager'),(634,'8211Z','Services administratifs combinés de bureau','82','Activités administratives et autres activités de soutien aux entreprises'),(635,'8219Z','Photocopie, préparation de documents et autres activités spécialisées de soutien de bureau','82','Activités administratives et autres activités de soutien aux entreprises'),(636,'8220Z','Activités de centres d\'appels','82','Activités administratives et autres activités de soutien aux entreprises'),(637,'8230Z','Organisation de foires, salons professionnels et congrès','82','Activités administratives et autres activités de soutien aux entreprises'),(638,'8291Z','Activités des agences de recouvrement de factures et des sociétés d\'information financière sur la clientèle','82','Activités administratives et autres activités de soutien aux entreprises'),(639,'8292Z','Activités de conditionnement','82','Activités administratives et autres activités de soutien aux entreprises'),(640,'8299Z','Autres activités de soutien aux entreprises n.c.a.','82','Activités administratives et autres activités de soutien aux entreprises'),(641,'8411Z','Administration publique générale','84','Administration publique et défense ; sécurité sociale obligatoire'),(642,'8412Z','Administration publique (tutelle) de la santé, de la formation, de la culture et des services sociaux, autre que sécurité sociale','84','Administration publique et défense ; sécurité sociale obligatoire'),(643,'8413Z','Administration publique (tutelle) des activités économiques','84','Administration publique et défense ; sécurité sociale obligatoire'),(644,'8421Z','Affaires étrangères','84','Administration publique et défense ; sécurité sociale obligatoire'),(645,'8422Z','Défense','84','Administration publique et défense ; sécurité sociale obligatoire'),(646,'8423Z','Justice','84','Administration publique et défense ; sécurité sociale obligatoire'),(647,'8424Z','Activités d\'ordre public et de sécurité','84','Administration publique et défense ; sécurité sociale obligatoire'),(648,'8425Z','Services du feu et de secours','84','Administration publique et défense ; sécurité sociale obligatoire'),(649,'8430A','Activités générales de sécurité sociale','84','Administration publique et défense ; sécurité sociale obligatoire'),(650,'8430B','Gestion des retraites complémentaires','84','Administration publique et défense ; sécurité sociale obligatoire'),(651,'8430C','Distribution sociale de revenus','84','Administration publique et défense ; sécurité sociale obligatoire'),(652,'8510Z','Enseignement pré-primaire','85','Enseignements / Formations'),(653,'8520Z','Enseignement primaire','85','Enseignements / Formations'),(654,'8531Z','Enseignement secondaire général','85','Enseignements / Formations'),(655,'8532Z','Enseignement secondaire technique ou professionnel','85','Enseignements / Formations'),(656,'8541Z','Enseignement post-secondaire non supérieur','85','Enseignements / Formations'),(657,'8542Z','Enseignement supérieur','85','Enseignements / Formations'),(658,'8551Z','Enseignement de disciplines sportives et d\'activités de loisirs','85','Enseignements / Formations'),(659,'8552Z','Enseignement culturel','85','Enseignements / Formations'),(660,'8553Z','Enseignement de la conduite','85','Enseignements / Formations'),(661,'8559A','Formation continue d\'adultes','85','Enseignements / Formations'),(662,'8559B','Autres enseignements','85','Enseignements / Formations'),(663,'8560Z','Activités de soutien à l\'enseignement','85','Enseignements / Formations'),(664,'8610Z','Activités hospitalières','86','Activités pour la santé humaine'),(665,'8621Z','Activité des médecins généralistes','86','Activités pour la santé humaine'),(666,'8622A','Activités de radiodiagnostic et de radiothérapie','86','Activités pour la santé humaine'),(667,'8622B','Activités chirurgicales','86','Activités pour la santé humaine'),(668,'8622C','Autres activités des médecins spécialistes','86','Activités pour la santé humaine'),(669,'8623Z','Pratique dentaire','86','Activités pour la santé humaine'),(670,'8690A','Ambulances','86','Activités pour la santé humaine'),(671,'8690B','Laboratoires d\'analyses médicales','86','Activités pour la santé humaine'),(672,'8690C','Centres de collecte et banques d\'organes','86','Activités pour la santé humaine'),(673,'8690D','Activités des infirmiers et des sages-femmes','86','Activités pour la santé humaine'),(674,'8690E','Activités des professionnels de la rééducation, de l\'appareillage et des pédicures-podologues','86','Activités pour la santé humaine'),(675,'8690F','Activités de santé humaine non classées ailleurs','86','Activités pour la santé humaine'),(676,'8710A','Hébergement médicalisé pour personnes âgées','87','Hébergement médico-social et social'),(677,'8710B','Hébergement médicalisé pour enfants handicapés','87','Hébergement médico-social et social'),(678,'8710C','Hébergement médicalisé pour adultes handicapés et autre hébergement médicalisé','87','Hébergement médico-social et social'),(679,'8720A','Hébergement social pour handicapés mentaux et malades mentaux','87','Hébergement médico-social et social'),(680,'8720B','Hébergement social pour toxicomanes','87','Hébergement médico-social et social'),(681,'8730A','Hébergement social pour personnes âgées','87','Hébergement médico-social et social'),(682,'8730B','Hébergement social pour handicapés physiques','87','Hébergement médico-social et social'),(683,'8790A','Hébergement social pour enfants en difficultés','87','Hébergement médico-social et social'),(684,'8790B','Hébergement social pour adultes et familles en difficultés et autre hébergement social','87','Hébergement médico-social et social'),(685,'8810A','Aide à domicile','88','Action sociale sans hébergement'),(686,'8810B','Accueil ou accompagnement sans hébergement d\'adultes handicapés ou de personnes âgées','88','Action sociale sans hébergement'),(687,'8810C','Aide par le travail','88','Action sociale sans hébergement'),(688,'8891A','Accueil de jeunes enfants','88','Action sociale sans hébergement'),(689,'8891B','Accueil ou accompagnement sans hébergement d\'enfants handicapés','88','Action sociale sans hébergement'),(690,'8899A','Autre accueil ou accompagnement sans hébergement d\'enfants et d\'adolescents','88','Action sociale sans hébergement'),(691,'8899B','Action sociale sans hébergement n.c.a.','88','Action sociale sans hébergement'),(692,'9001Z','Arts du spectacle vivant','90','Activités créatives, artistiques et de spectacle'),(693,'9002Z','Activités de soutien au spectacle vivant','90','Activités créatives, artistiques et de spectacle'),(694,'9003A','Création artistique relevant des arts plastiques','90','Activités créatives, artistiques et de spectacle'),(695,'9003B','Autre création artistique','90','Activités créatives, artistiques et de spectacle'),(696,'9004Z','Gestion de salles de spectacles','90','Activités créatives, artistiques et de spectacle'),(697,'9101Z','Gestion des bibliothèques et des archives','91','Bibliothèques, archives, musées et autres activités culturelles'),(698,'9102Z','Gestion des musées','91','Bibliothèques, archives, musées et autres activités culturelles'),(699,'9103Z','Gestion des sites et monuments historiques et des attractions touristiques similaires','91','Bibliothèques, archives, musées et autres activités culturelles'),(700,'9104Z','Gestion des jardins botaniques et zoologiques et des réserves naturelles','91','Bibliothèques, archives, musées et autres activités culturelles'),(701,'9200Z','Organisation de jeux de hasard et d\'argent','92','Organisation de jeux de hasard et d\'argent'),(702,'9311Z','Gestion d\'installations sportives','93','Activités sportives, récréatives et de loisirs'),(703,'9312Z','Activités de clubs de sports','93','Activités sportives, récréatives et de loisirs'),(704,'9313Z','Activités des centres de culture physique','93','Activités sportives, récréatives et de loisirs'),(705,'9319Z','Autres activités liées au sport','93','Activités sportives, récréatives et de loisirs'),(706,'9321Z','Activités des parcs d\'attractions et parcs à thèmes','93','Activités sportives, récréatives et de loisirs'),(707,'9329Z','Autres activités récréatives et de loisirs','93','Activités sportives, récréatives et de loisirs'),(708,'9411Z','Activités des organisations patronales et consulaires','94','Activités des organisations associatives'),(709,'9412Z','Activités des organisations professionnelles','94','Activités des organisations associatives'),(710,'9420Z','Activités des syndicats de salariés','94','Activités des organisations associatives'),(711,'9491Z','Activités des organisations religieuses','94','Activités des organisations associatives'),(712,'9492Z','Activités des organisations politiques','94','Activités des organisations associatives'),(713,'9499Z','Autres organisations fonctionnant par adhésion volontaire','94','Activités des organisations associatives'),(714,'9511Z','Réparation d\'ordinateurs et d\'équipements périphériques','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(715,'9512Z','Réparation d\'équipements de communication','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(716,'9521Z','Réparation de produits électroniques grand public','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(717,'9522Z','Réparation d\'appareils électroménagers et d\'équipements pour la maison et le jardin','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(718,'9523Z','Réparation de chaussures et d\'articles en cuir','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(719,'9524Z','Réparation de meubles et d\'équipements du foyer','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(720,'9525Z','Réparation d\'articles d\'horlogerie et de bijouterie','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(721,'9529Z','Réparation d\'autres biens personnels et domestiques','95','Réparation d\'ordinateurs et de biens personnels et domestiques'),(722,'9601A','Blanchisserie-teinturerie de gros','96','Autres services personnels'),(723,'9601B','Blanchisserie-teinturerie de détail','96','Autres services personnels'),(724,'9602A','Coiffure','96','Autres services personnels'),(725,'9602B','Soins de beauté','96','Autres services personnels'),(726,'9603Z','Services funéraires','96','Autres services personnels'),(727,'9604Z','Entretien corporel','96','Autres services personnels'),(728,'9609Z','Autres services personnels n.c.a.','96','Autres services personnels'),(729,'9700Z','Activités des ménages en tant qu\'employeurs de personnel domestique','97','Activités des ménages en tant qu\'employeurs de personnel domestique'),(730,'9810Z','Activités indifférenciées des ménages en tant que producteurs de biens pour usage propre','98','Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre'),(731,'9820Z','Activités indifférenciées des ménages en tant que producteurs de services pour usage propre','98','Activités indifférenciées des ménages en tant que producteurs de biens et services pour usage propre'),(732,'9900Z','Activités des organisations et organismes extraterritoriaux','99','Activités des organisations et organismes extraterritoriaux');
/*!40000 ALTER TABLE `t_referentiel_nace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_reponse_elec_formulaire`
--

LOCK TABLES `t_reponse_elec_formulaire` WRITE;
/*!40000 ALTER TABLE `t_reponse_elec_formulaire` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_reponse_elec_formulaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_role_juridique`
--

LOCK TABLES `t_role_juridique` WRITE;
/*!40000 ALTER TABLE `t_role_juridique` DISABLE KEYS */;
INSERT INTO `t_role_juridique` (`id_role_juridique`, `libelle_role_juridique`) VALUES (1,'ROLE_JURIDIQUE_MANDATAIRE'),(2,'ROLE_JURIDIQUE_CO_TRAITANT'),(3,'ROLE_JURIDIQUE_SOUS_TRAITANT');
/*!40000 ALTER TABLE `t_role_juridique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_sous_critere_attribution`
--

LOCK TABLES `t_sous_critere_attribution` WRITE;
/*!40000 ALTER TABLE `t_sous_critere_attribution` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sous_critere_attribution` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_support_annonce_consultation`
--

LOCK TABLES `t_support_annonce_consultation` WRITE;
/*!40000 ALTER TABLE `t_support_annonce_consultation` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_support_annonce_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_support_publication`
--

LOCK TABLES `t_support_publication` WRITE;
/*!40000 ALTER TABLE `t_support_publication` DISABLE KEYS */;
INSERT INTO `t_support_publication` (`id`, `image_logo`, `nom`, `visible`, `ordre`, `default_value`, `code`, `actif`, `url`, `groupe`, `tag_debut_fin_groupe`, `description`, `nbre_total_groupe`, `lieux_execution`, `detail_info`, `type_info`, `affichage_infos`, `affichage_message_support`, `selection_departements_parution`, `departements_parution`) VALUES (1,'support-BOAMP.png','BOAMP','1',1,'1','','1','http://www.boamp.fr/','1','2',NULL,3,NULL,NULL,NULL,'0','0','0',NULL),(2,'support-Les-Echos.png','Les Echos','1',3,'1','LESECHOS','1','http://marches-publics.lesechos.fr/','2','2',NULL,1,NULL,'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html','3','1','1','0','254,256,257,258,259,260,261,307'),(3,'support-JOUE.png','JOUE','1',2,'1','','1','#','1','3',NULL,3,NULL,NULL,NULL,'0','0','0',NULL),(4,'support-Le-Moniteur.png','Le Moniteur','1',9,'1','','1','#','3','2',NULL,1,NULL,NULL,NULL,'0','0','0',NULL),(5,'support-Le-Parisien.png','Le Parisien','0',4,'1','LEPARISIEN','0',NULL,'4','2',NULL,1,NULL,'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html','3','1','1','0','254,255,256,257,258,259,260,261,290'),(6,'supportBOAMPJOUE.png','BOAMP & JOUE','1',0,'1',NULL,'1',NULL,'1','1',NULL,3,NULL,NULL,NULL,'0','0','0',NULL),(7,'support-MOL.png','MarchéOnline','1',8,'1','','1',NULL,'6','2',NULL,1,NULL,NULL,NULL,'0','0','0',NULL),(8,'support-emarchepublics.png','e-marchespublics.com','1',5,'1','EMARCHEPUBLIC','1',NULL,'5','2',NULL,1,NULL,'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html','3','1','1','0',NULL),(9,'support-SO.png','Sud Ouest 17-24-33-40-47-64','0',10,'1','SO','0','http://www.sudouest-marchespublics.com','8','2',NULL,3,'#17#24#47#33#40#64#',NULL,NULL,'0','0','0',NULL),(10,'support-CL.png','Charente libre','0',11,'1','CL','0','http://www.sudouest-marchespublics.com','9','2',NULL,1,'#16#',NULL,NULL,'0','0','0',NULL),(11,'support-CL_SO.png','Charente libre + Sud Ouest Charente','0',12,'1','CL_SO','0','http://www.sudouest-marchespublics.com','10','2',NULL,1,'#16#',NULL,NULL,'0','0','0',NULL),(12,'support-SO_DL.png','Sud Ouest Dordogne + Dordogne Libre','0',13,'1','SO_DL','0','http://www.sudouest-marchespublics.com','11','2',NULL,1,'#24#',NULL,NULL,'0','0','0',NULL),(13,'support-SO_RP_EP.png','Sud Ouest Pyrénées atlantiques  + République des Pyrénées  + Éclair des Pyrénées','0',14,'1','SO_RP_EP','0','http://www.sudouest-marchespublics.com','12','2',NULL,1,'#64#',NULL,NULL,'0','0','0',NULL),(14,'support-LM.png','La Montagne','0',13,'1','LM','0','http://www.sudouest-marchespublics.com','15','2',NULL,1,'#19#23#',NULL,NULL,'0','0','0',NULL),(15,'support-LNR.png','La Nouvelle République','0',14,'1','LNR','0','http://www.sudouest-marchespublics.com','16','2',NULL,1,'#79#86#',NULL,NULL,'0','0','0',NULL),(16,'support-LPC.png','Le Populaire du centre','0',15,'1','LPC','0','http://www.sudouest-marchespublics.com','17','2',NULL,1,'#87#',NULL,NULL,'0','0','0',NULL),(17,'support-JBTP.png','Le journal du BTP','0',18,'1','JBTP','0','http://journal-du-btp.com','16','2',NULL,1,NULL,NULL,NULL,'0','0','0',NULL),(18,'support-forfait-bimedia-echos.png','Forfait bi-média Les Echos + E-marchespublics.com','0',6,'1','LESECHOSBIMEDIA','0',NULL,'17','2',NULL,1,NULL,'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html','3','1','1','0',NULL),(19,'support-forfait-bimedia-parisien.png','Forfait bi-média Le Parisien + E-marchespublics.com','0',7,'1','LEPARISIENBIMEDIA','0',NULL,'18','2',NULL,1,NULL,'https://www.e-marchespublics.com/documents/tarifs_les_echos_leparisien-emp.html','3','1','1','0',NULL),(20,'support-LAL.png','Le journal des Annonces Landaises','0',19,'1','LAL','0','https://annonces-landaises.com/','19','2',NULL,1,NULL,NULL,NULL,'0','0','0',NULL);
/*!40000 ALTER TABLE `t_support_publication` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_synthese_rapport_audit`
--

LOCK TABLES `t_synthese_rapport_audit` WRITE;
/*!40000 ALTER TABLE `t_synthese_rapport_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_synthese_rapport_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_tranche`
--

LOCK TABLES `t_tranche` WRITE;
/*!40000 ALTER TABLE `t_tranche` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_tranche` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_annonce_consultation`
--

LOCK TABLES `t_type_annonce_consultation` WRITE;
/*!40000 ALTER TABLE `t_type_annonce_consultation` DISABLE KEYS */;
INSERT INTO `t_type_annonce_consultation` (`id`, `libelle`, `visible`) VALUES (1,'Pré-information','0'),(2,'Avis nationaux','1'),(3,'Procédures DSP','1'),(4,'Avis sans publication préalable','0'),(5,'Avis rectificatif (formulaire européen)','0'),(6,'Avis d\'annulation (formulaire européen)','0'),(7,'Avis rectificatif (formulaire standard)','0'),(8,'Avis d\'annulation (formulaire standard)','0'),(9,'Avis rectificatif (formulaire MAPA)','0'),(10,'Avis d\'annulation (formulaire MAPA)','0'),(11,'Avis rectificatif (formulaire DSP)','0'),(12,'Avis d\'annulation (formulaire DSP)','0'),(13,'Avis européens','1');
/*!40000 ALTER TABLE `t_type_annonce_consultation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_contrat`
--

LOCK TABLES `t_type_contrat` WRITE;
/*!40000 ALTER TABLE `t_type_contrat` DISABLE KEYS */;
INSERT INTO `t_type_contrat` (`id_type_contrat`, `libelle_type_contrat`, `abreviation_type_contrat`, `type_contrat_statistique`, `multi`, `accord_cadre_sad`, `avec_chapeau`, `avec_montant`, `mode_echange_chorus`, `marche_subsequent`, `avec_montant_max`, `ordre_affichage`, `article_133`, `code_dume`, `concession`, `id_externe`) VALUES (1,'TYPE_CONTRAT_MARCHE','MA',0,'0','0','0','1','1','0','0',1,'1','01',0,'mar'),(2,'TYPE_CONTRAT_AC_MONO_ATTRIBUTAIRE','AC',3,'0','1','0','0','2','0','1',4,'1','03',0,'amo'),(3,'TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE','SA',4,'1','2','1','0','2','0','1',10,'1','06',0,'sad'),(4,'TYPE_CONTRAT_DELEGATION_DE_SERVICE_PUBLIC','AU',5,'0','0','0','1','0','0','0',7,'1','06',1,'dsp'),(5,'TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE','AM',3,'1','1','1','0','2','0','1',5,'1','03',0,'amu'),(6,'TYPE_CONTRAT_MARCHE_PUBLIC_MULTI_ATTRIBUTAIRES','AM',0,'1','0','1','1','1','0','0',3,'1','03',0,'marmu'),(7,'TYPE_CONTRAT_MARCHE_SUBSEQUENT','SU',1,'0','0','0','1','1','1','0',6,'1','04',0,'msa'),(8,'TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE','AU',5,'0','0','0','1','1','0','0',8,'1','02',0,'ppp'),(9,'TYPE_CONTRAT_PARTENARIAT_D_INNOVATION','AU',5,'0','0','0','1','1','0','0',9,'1','06',0,'pdi'),(10,'TYPE_CONTRAT_MARCHE_SPECIFIQUE','SP',5,'0','0','0','1','1','2','0',11,'1','06',0,'marsp'),(11,'TYPE_CONTRAT_MARCHE_DE_CONCEPTION_REALISATION','AU',5,'0','0','0','1','1','0','0',12,'1','06',0,'ccm'),(12,'TYPE_CONTRAT_AUTRE','AU',5,'0','0','0','1','1','0','0',13,'1','06',0,'aut'),(13,'TYPE_CONTRAT_AC_BON_COMMANDE_MONO_ATTRIBUTAIRE','AC',0,'0','0','1','1','1','0','0',2,'1',NULL,0,''),(14,'TYPE_CONTRAT_MARCHE_GLOBAL_PERFORMANCE','AU',0,'0','0','0','1','1','0','0',14,'1','06',0,'margp'),(15,'TYPE_CONTRAT_MARCHE_SECTORIEL','AU',0,'0','0','0','1','1','0','0',13,'1','06',0,'margs');
/*!40000 ALTER TABLE `t_type_contrat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_contrat_et_procedure`
--

LOCK TABLES `t_type_contrat_et_procedure` WRITE;
/*!40000 ALTER TABLE `t_type_contrat_et_procedure` DISABLE KEYS */;
INSERT INTO `t_type_contrat_et_procedure` (`id_type_contrat_et_procedure`, `id_type_contrat`, `id_type_procedure`, `organisme`) VALUES (1353,1,1,'a1a'),(1883,1,1,'a1t'),(1527,1,1,'a2z'),(1705,1,1,'e3r'),(1360,1,2,'a1a'),(1884,1,2,'a1t'),(1528,1,2,'a2z'),(1706,1,2,'e3r'),(1363,1,3,'a1a'),(1885,1,3,'a1t'),(1529,1,3,'a2z'),(1707,1,3,'e3r'),(1370,1,4,'a1a'),(1886,1,4,'a1t'),(1530,1,4,'a2z'),(1708,1,4,'e3r'),(1376,1,5,'a1a'),(1887,1,5,'a1t'),(1531,1,5,'a2z'),(1709,1,5,'e3r'),(1383,1,6,'a1a'),(1888,1,6,'a1t'),(1532,1,6,'a2z'),(1710,1,6,'e3r'),(1387,1,7,'a1a'),(1889,1,7,'a1t'),(1533,1,7,'a2z'),(1711,1,7,'e3r'),(1390,1,8,'a1a'),(1890,1,8,'a1t'),(1534,1,8,'a2z'),(1712,1,8,'e3r'),(1392,1,9,'a1a'),(1891,1,9,'a1t'),(1535,1,9,'a2z'),(1713,1,9,'e3r'),(1394,1,10,'a1a'),(1892,1,10,'a1t'),(1536,1,10,'a2z'),(1714,1,10,'e3r'),(1402,1,11,'a1a'),(1893,1,11,'a1t'),(1537,1,11,'a2z'),(1715,1,11,'e3r'),(1405,1,12,'a1a'),(1894,1,12,'a1t'),(1538,1,12,'a2z'),(1716,1,12,'e3r'),(1412,1,18,'a1a'),(1895,1,18,'a1t'),(1539,1,18,'a2z'),(1717,1,18,'e3r'),(1426,1,30,'a1a'),(1896,1,30,'a1t'),(1540,1,30,'a2z'),(1718,1,30,'e3r'),(1431,1,1000,'a1a'),(1897,1,1000,'a1t'),(1541,1,1000,'a2z'),(1,1,1000,'a4n'),(97,1,1000,'a7z'),(113,1,1000,'b4n'),(49,1,1000,'c8v'),(65,1,1000,'d2v'),(129,1,1000,'d3f'),(145,1,1000,'d4t'),(1719,1,1000,'e3r'),(161,1,1000,'e8r'),(177,1,1000,'f0g'),(193,1,1000,'f2h'),(209,1,1000,'f5j'),(33,1,1000,'g6l'),(17,1,1000,'g7h'),(225,1,1000,'h8j'),(241,1,1000,'i5o'),(257,1,1000,'j9k'),(273,1,1000,'k0l'),(289,1,1000,'l2m'),(81,1,1000,'pmi-min-1'),(337,1,1000,'s2d'),(321,1,1000,'t9y'),(305,1,1000,'w3x'),(661,1,1001,'a4n'),(702,1,1001,'a7z'),(713,1,1001,'b4n'),(867,1,1001,'c8v'),(875,1,1001,'d2v'),(724,1,1001,'d3f'),(735,1,1001,'d4t'),(746,1,1001,'e8r'),(757,1,1001,'f0g'),(768,1,1001,'f2h'),(779,1,1001,'f5j'),(681,1,1001,'g6l'),(671,1,1001,'g7h'),(790,1,1001,'h8j'),(801,1,1001,'i5o'),(812,1,1001,'j9k'),(823,1,1001,'k0l'),(834,1,1001,'l2m'),(691,1,1001,'pmi-min-1'),(885,1,1001,'s2d'),(856,1,1001,'t9y'),(845,1,1001,'w3x'),(662,1,1002,'a4n'),(703,1,1002,'a7z'),(714,1,1002,'b4n'),(868,1,1002,'c8v'),(876,1,1002,'d2v'),(725,1,1002,'d3f'),(736,1,1002,'d4t'),(747,1,1002,'e8r'),(758,1,1002,'f0g'),(769,1,1002,'f2h'),(780,1,1002,'f5j'),(682,1,1002,'g6l'),(672,1,1002,'g7h'),(791,1,1002,'h8j'),(802,1,1002,'i5o'),(813,1,1002,'j9k'),(824,1,1002,'k0l'),(835,1,1002,'l2m'),(692,1,1002,'pmi-min-1'),(886,1,1002,'s2d'),(857,1,1002,'t9y'),(846,1,1002,'w3x'),(9,1,1004,'a4n'),(105,1,1004,'a7z'),(121,1,1004,'b4n'),(57,1,1004,'c8v'),(73,1,1004,'d2v'),(137,1,1004,'d3f'),(153,1,1004,'d4t'),(169,1,1004,'e8r'),(185,1,1004,'f0g'),(201,1,1004,'f2h'),(217,1,1004,'f5j'),(41,1,1004,'g6l'),(25,1,1004,'g7h'),(233,1,1004,'h8j'),(249,1,1004,'i5o'),(265,1,1004,'j9k'),(281,1,1004,'k0l'),(297,1,1004,'l2m'),(89,1,1004,'pmi-min-1'),(345,1,1004,'s2d'),(329,1,1004,'t9y'),(313,1,1004,'w3x'),(353,1,1005,'a4n'),(437,1,1005,'a7z'),(451,1,1005,'b4n'),(395,1,1005,'c8v'),(409,1,1005,'d2v'),(465,1,1005,'d3f'),(479,1,1005,'d4t'),(493,1,1005,'e8r'),(507,1,1005,'f0g'),(521,1,1005,'f2h'),(535,1,1005,'f5j'),(381,1,1005,'g6l'),(367,1,1005,'g7h'),(549,1,1005,'h8j'),(563,1,1005,'i5o'),(577,1,1005,'j9k'),(591,1,1005,'k0l'),(605,1,1005,'l2m'),(423,1,1005,'pmi-min-1'),(647,1,1005,'s2d'),(633,1,1005,'t9y'),(619,1,1005,'w3x'),(360,1,1006,'a4n'),(444,1,1006,'a7z'),(458,1,1006,'b4n'),(402,1,1006,'c8v'),(416,1,1006,'d2v'),(472,1,1006,'d3f'),(486,1,1006,'d4t'),(500,1,1006,'e8r'),(514,1,1006,'f0g'),(528,1,1006,'f2h'),(542,1,1006,'f5j'),(388,1,1006,'g6l'),(374,1,1006,'g7h'),(556,1,1006,'h8j'),(570,1,1006,'i5o'),(584,1,1006,'j9k'),(598,1,1006,'k0l'),(612,1,1006,'l2m'),(430,1,1006,'pmi-min-1'),(654,1,1006,'s2d'),(640,1,1006,'t9y'),(626,1,1006,'w3x'),(1349,2,1,'a1a'),(1898,2,1,'a1t'),(1542,2,1,'a2z'),(1720,2,1,'e3r'),(1359,2,2,'a1a'),(1899,2,2,'a1t'),(1543,2,2,'a2z'),(1721,2,2,'e3r'),(1365,2,3,'a1a'),(1900,2,3,'a1t'),(1544,2,3,'a2z'),(1722,2,3,'e3r'),(1374,2,4,'a1a'),(1901,2,4,'a1t'),(1545,2,4,'a2z'),(1723,2,4,'e3r'),(1381,2,5,'a1a'),(1902,2,5,'a1t'),(1546,2,5,'a2z'),(1724,2,5,'e3r'),(1385,2,6,'a1a'),(1903,2,6,'a1t'),(1547,2,6,'a2z'),(1725,2,6,'e3r'),(1388,2,7,'a1a'),(1904,2,7,'a1t'),(1548,2,7,'a2z'),(1726,2,7,'e3r'),(1400,2,11,'a1a'),(1905,2,11,'a1t'),(1549,2,11,'a2z'),(1727,2,11,'e3r'),(1409,2,12,'a1a'),(1906,2,12,'a1t'),(1550,2,12,'a2z'),(1728,2,12,'e3r'),(1411,2,18,'a1a'),(1907,2,18,'a1t'),(1551,2,18,'a2z'),(1729,2,18,'e3r'),(1425,2,30,'a1a'),(1908,2,30,'a1t'),(1552,2,30,'a2z'),(1730,2,30,'e3r'),(1432,2,1000,'a1a'),(1909,2,1000,'a1t'),(1553,2,1000,'a2z'),(2,2,1000,'a4n'),(98,2,1000,'a7z'),(114,2,1000,'b4n'),(50,2,1000,'c8v'),(66,2,1000,'d2v'),(130,2,1000,'d3f'),(146,2,1000,'d4t'),(1731,2,1000,'e3r'),(162,2,1000,'e8r'),(178,2,1000,'f0g'),(194,2,1000,'f2h'),(210,2,1000,'f5j'),(34,2,1000,'g6l'),(18,2,1000,'g7h'),(226,2,1000,'h8j'),(242,2,1000,'i5o'),(258,2,1000,'j9k'),(274,2,1000,'k0l'),(290,2,1000,'l2m'),(82,2,1000,'pmi-min-1'),(338,2,1000,'s2d'),(322,2,1000,'t9y'),(306,2,1000,'w3x'),(663,2,1001,'a4n'),(704,2,1001,'a7z'),(715,2,1001,'b4n'),(869,2,1001,'c8v'),(877,2,1001,'d2v'),(726,2,1001,'d3f'),(737,2,1001,'d4t'),(748,2,1001,'e8r'),(759,2,1001,'f0g'),(770,2,1001,'f2h'),(781,2,1001,'f5j'),(683,2,1001,'g6l'),(673,2,1001,'g7h'),(792,2,1001,'h8j'),(803,2,1001,'i5o'),(814,2,1001,'j9k'),(825,2,1001,'k0l'),(836,2,1001,'l2m'),(693,2,1001,'pmi-min-1'),(887,2,1001,'s2d'),(858,2,1001,'t9y'),(847,2,1001,'w3x'),(664,2,1002,'a4n'),(705,2,1002,'a7z'),(716,2,1002,'b4n'),(870,2,1002,'c8v'),(878,2,1002,'d2v'),(727,2,1002,'d3f'),(738,2,1002,'d4t'),(749,2,1002,'e8r'),(760,2,1002,'f0g'),(771,2,1002,'f2h'),(782,2,1002,'f5j'),(684,2,1002,'g6l'),(674,2,1002,'g7h'),(793,2,1002,'h8j'),(804,2,1002,'i5o'),(815,2,1002,'j9k'),(826,2,1002,'k0l'),(837,2,1002,'l2m'),(694,2,1002,'pmi-min-1'),(888,2,1002,'s2d'),(859,2,1002,'t9y'),(848,2,1002,'w3x'),(10,2,1004,'a4n'),(106,2,1004,'a7z'),(122,2,1004,'b4n'),(58,2,1004,'c8v'),(74,2,1004,'d2v'),(138,2,1004,'d3f'),(154,2,1004,'d4t'),(170,2,1004,'e8r'),(186,2,1004,'f0g'),(202,2,1004,'f2h'),(218,2,1004,'f5j'),(42,2,1004,'g6l'),(26,2,1004,'g7h'),(234,2,1004,'h8j'),(250,2,1004,'i5o'),(266,2,1004,'j9k'),(282,2,1004,'k0l'),(298,2,1004,'l2m'),(90,2,1004,'pmi-min-1'),(346,2,1004,'s2d'),(330,2,1004,'t9y'),(314,2,1004,'w3x'),(354,2,1005,'a4n'),(438,2,1005,'a7z'),(452,2,1005,'b4n'),(396,2,1005,'c8v'),(410,2,1005,'d2v'),(466,2,1005,'d3f'),(480,2,1005,'d4t'),(494,2,1005,'e8r'),(508,2,1005,'f0g'),(522,2,1005,'f2h'),(536,2,1005,'f5j'),(382,2,1005,'g6l'),(368,2,1005,'g7h'),(550,2,1005,'h8j'),(564,2,1005,'i5o'),(578,2,1005,'j9k'),(592,2,1005,'k0l'),(606,2,1005,'l2m'),(424,2,1005,'pmi-min-1'),(648,2,1005,'s2d'),(634,2,1005,'t9y'),(620,2,1005,'w3x'),(361,2,1006,'a4n'),(445,2,1006,'a7z'),(459,2,1006,'b4n'),(403,2,1006,'c8v'),(417,2,1006,'d2v'),(473,2,1006,'d3f'),(487,2,1006,'d4t'),(501,2,1006,'e8r'),(515,2,1006,'f0g'),(529,2,1006,'f2h'),(543,2,1006,'f5j'),(389,2,1006,'g6l'),(375,2,1006,'g7h'),(557,2,1006,'h8j'),(571,2,1006,'i5o'),(585,2,1006,'j9k'),(599,2,1006,'k0l'),(613,2,1006,'l2m'),(431,2,1006,'pmi-min-1'),(655,2,1006,'s2d'),(641,2,1006,'t9y'),(627,2,1006,'w3x'),(1424,3,30,'a1a'),(1910,3,30,'a1t'),(1554,3,30,'a2z'),(1732,3,30,'e3r'),(1429,3,32,'a1a'),(1911,3,32,'a1t'),(1555,3,32,'a2z'),(1733,3,32,'e3r'),(1350,4,1,'a1a'),(1912,4,1,'a1t'),(1556,4,1,'a2z'),(1734,4,1,'e3r'),(1355,4,2,'a1a'),(1913,4,2,'a1t'),(1557,4,2,'a2z'),(1735,4,2,'e3r'),(1362,4,3,'a1a'),(1914,4,3,'a1t'),(1558,4,3,'a2z'),(1736,4,3,'e3r'),(1373,4,4,'a1a'),(1915,4,4,'a1t'),(1559,4,4,'a2z'),(1737,4,4,'e3r'),(1375,4,5,'a1a'),(1916,4,5,'a1t'),(1560,4,5,'a2z'),(1738,4,5,'e3r'),(1399,4,11,'a1a'),(1917,4,11,'a1t'),(1561,4,11,'a2z'),(1739,4,11,'e3r'),(1408,4,12,'a1a'),(1918,4,12,'a1t'),(1562,4,12,'a2z'),(1740,4,12,'e3r'),(670,4,17,'a4n'),(712,4,17,'a7z'),(723,4,17,'b4n'),(897,4,17,'c8v'),(898,4,17,'d2v'),(734,4,17,'d3f'),(745,4,17,'d4t'),(756,4,17,'e8r'),(767,4,17,'f0g'),(778,4,17,'f2h'),(789,4,17,'f5j'),(690,4,17,'g6l'),(680,4,17,'g7h'),(800,4,17,'h8j'),(811,4,17,'i5o'),(822,4,17,'j9k'),(833,4,17,'k0l'),(844,4,17,'l2m'),(701,4,17,'pmi-min-1'),(899,4,17,'s2d'),(866,4,17,'t9y'),(855,4,17,'w3x'),(1414,4,18,'a1a'),(1919,4,18,'a1t'),(1563,4,18,'a2z'),(1741,4,18,'e3r'),(1423,4,30,'a1a'),(1920,4,30,'a1t'),(1564,4,30,'a2z'),(1742,4,30,'e3r'),(1433,4,1000,'a1a'),(1921,4,1000,'a1t'),(1565,4,1000,'a2z'),(3,4,1000,'a4n'),(99,4,1000,'a7z'),(115,4,1000,'b4n'),(51,4,1000,'c8v'),(67,4,1000,'d2v'),(131,4,1000,'d3f'),(147,4,1000,'d4t'),(1743,4,1000,'e3r'),(163,4,1000,'e8r'),(179,4,1000,'f0g'),(195,4,1000,'f2h'),(211,4,1000,'f5j'),(35,4,1000,'g6l'),(19,4,1000,'g7h'),(227,4,1000,'h8j'),(243,4,1000,'i5o'),(259,4,1000,'j9k'),(275,4,1000,'k0l'),(291,4,1000,'l2m'),(83,4,1000,'pmi-min-1'),(339,4,1000,'s2d'),(323,4,1000,'t9y'),(307,4,1000,'w3x'),(11,4,1004,'a4n'),(107,4,1004,'a7z'),(123,4,1004,'b4n'),(59,4,1004,'c8v'),(75,4,1004,'d2v'),(139,4,1004,'d3f'),(155,4,1004,'d4t'),(171,4,1004,'e8r'),(187,4,1004,'f0g'),(203,4,1004,'f2h'),(219,4,1004,'f5j'),(43,4,1004,'g6l'),(27,4,1004,'g7h'),(235,4,1004,'h8j'),(251,4,1004,'i5o'),(267,4,1004,'j9k'),(283,4,1004,'k0l'),(299,4,1004,'l2m'),(91,4,1004,'pmi-min-1'),(347,4,1004,'s2d'),(331,4,1004,'t9y'),(315,4,1004,'w3x'),(355,4,1005,'a4n'),(439,4,1005,'a7z'),(453,4,1005,'b4n'),(397,4,1005,'c8v'),(411,4,1005,'d2v'),(467,4,1005,'d3f'),(481,4,1005,'d4t'),(495,4,1005,'e8r'),(509,4,1005,'f0g'),(523,4,1005,'f2h'),(537,4,1005,'f5j'),(383,4,1005,'g6l'),(369,4,1005,'g7h'),(551,4,1005,'h8j'),(565,4,1005,'i5o'),(579,4,1005,'j9k'),(593,4,1005,'k0l'),(607,4,1005,'l2m'),(425,4,1005,'pmi-min-1'),(649,4,1005,'s2d'),(635,4,1005,'t9y'),(621,4,1005,'w3x'),(362,4,1006,'a4n'),(446,4,1006,'a7z'),(460,4,1006,'b4n'),(404,4,1006,'c8v'),(418,4,1006,'d2v'),(474,4,1006,'d3f'),(488,4,1006,'d4t'),(502,4,1006,'e8r'),(516,4,1006,'f0g'),(530,4,1006,'f2h'),(544,4,1006,'f5j'),(390,4,1006,'g6l'),(376,4,1006,'g7h'),(558,4,1006,'h8j'),(572,4,1006,'i5o'),(586,4,1006,'j9k'),(600,4,1006,'k0l'),(614,4,1006,'l2m'),(432,4,1006,'pmi-min-1'),(656,4,1006,'s2d'),(642,4,1006,'t9y'),(628,4,1006,'w3x'),(1351,5,1,'a1a'),(1922,5,1,'a1t'),(1566,5,1,'a2z'),(1744,5,1,'e3r'),(1354,5,2,'a1a'),(1923,5,2,'a1t'),(1567,5,2,'a2z'),(1745,5,2,'e3r'),(1366,5,3,'a1a'),(1924,5,3,'a1t'),(1568,5,3,'a2z'),(1746,5,3,'e3r'),(1371,5,4,'a1a'),(1925,5,4,'a1t'),(1569,5,4,'a2z'),(1747,5,4,'e3r'),(1377,5,5,'a1a'),(1926,5,5,'a1t'),(1570,5,5,'a2z'),(1748,5,5,'e3r'),(1384,5,6,'a1a'),(1927,5,6,'a1t'),(1571,5,6,'a2z'),(1749,5,6,'e3r'),(1386,5,7,'a1a'),(1928,5,7,'a1t'),(1572,5,7,'a2z'),(1750,5,7,'e3r'),(1401,5,11,'a1a'),(1929,5,11,'a1t'),(1573,5,11,'a2z'),(1751,5,11,'e3r'),(1403,5,12,'a1a'),(1930,5,12,'a1t'),(1574,5,12,'a2z'),(1752,5,12,'e3r'),(1415,5,18,'a1a'),(1931,5,18,'a1t'),(1575,5,18,'a2z'),(1753,5,18,'e3r'),(1422,5,30,'a1a'),(1932,5,30,'a1t'),(1576,5,30,'a2z'),(1754,5,30,'e3r'),(1434,5,1000,'a1a'),(1933,5,1000,'a1t'),(1577,5,1000,'a2z'),(4,5,1000,'a4n'),(100,5,1000,'a7z'),(116,5,1000,'b4n'),(52,5,1000,'c8v'),(68,5,1000,'d2v'),(132,5,1000,'d3f'),(148,5,1000,'d4t'),(1755,5,1000,'e3r'),(164,5,1000,'e8r'),(180,5,1000,'f0g'),(196,5,1000,'f2h'),(212,5,1000,'f5j'),(36,5,1000,'g6l'),(20,5,1000,'g7h'),(228,5,1000,'h8j'),(244,5,1000,'i5o'),(260,5,1000,'j9k'),(276,5,1000,'k0l'),(292,5,1000,'l2m'),(84,5,1000,'pmi-min-1'),(340,5,1000,'s2d'),(324,5,1000,'t9y'),(308,5,1000,'w3x'),(667,5,1001,'a4n'),(708,5,1001,'a7z'),(719,5,1001,'b4n'),(873,5,1001,'c8v'),(881,5,1001,'d2v'),(730,5,1001,'d3f'),(741,5,1001,'d4t'),(752,5,1001,'e8r'),(763,5,1001,'f0g'),(774,5,1001,'f2h'),(785,5,1001,'f5j'),(687,5,1001,'g6l'),(677,5,1001,'g7h'),(796,5,1001,'h8j'),(807,5,1001,'i5o'),(818,5,1001,'j9k'),(829,5,1001,'k0l'),(840,5,1001,'l2m'),(697,5,1001,'pmi-min-1'),(891,5,1001,'s2d'),(862,5,1001,'t9y'),(851,5,1001,'w3x'),(668,5,1002,'a4n'),(709,5,1002,'a7z'),(720,5,1002,'b4n'),(874,5,1002,'c8v'),(882,5,1002,'d2v'),(731,5,1002,'d3f'),(742,5,1002,'d4t'),(753,5,1002,'e8r'),(764,5,1002,'f0g'),(775,5,1002,'f2h'),(786,5,1002,'f5j'),(688,5,1002,'g6l'),(678,5,1002,'g7h'),(797,5,1002,'h8j'),(808,5,1002,'i5o'),(819,5,1002,'j9k'),(830,5,1002,'k0l'),(841,5,1002,'l2m'),(698,5,1002,'pmi-min-1'),(892,5,1002,'s2d'),(863,5,1002,'t9y'),(852,5,1002,'w3x'),(12,5,1004,'a4n'),(108,5,1004,'a7z'),(124,5,1004,'b4n'),(60,5,1004,'c8v'),(76,5,1004,'d2v'),(140,5,1004,'d3f'),(156,5,1004,'d4t'),(172,5,1004,'e8r'),(188,5,1004,'f0g'),(204,5,1004,'f2h'),(220,5,1004,'f5j'),(44,5,1004,'g6l'),(28,5,1004,'g7h'),(236,5,1004,'h8j'),(252,5,1004,'i5o'),(268,5,1004,'j9k'),(284,5,1004,'k0l'),(300,5,1004,'l2m'),(92,5,1004,'pmi-min-1'),(348,5,1004,'s2d'),(332,5,1004,'t9y'),(316,5,1004,'w3x'),(356,5,1005,'a4n'),(440,5,1005,'a7z'),(454,5,1005,'b4n'),(398,5,1005,'c8v'),(412,5,1005,'d2v'),(468,5,1005,'d3f'),(482,5,1005,'d4t'),(496,5,1005,'e8r'),(510,5,1005,'f0g'),(524,5,1005,'f2h'),(538,5,1005,'f5j'),(384,5,1005,'g6l'),(370,5,1005,'g7h'),(552,5,1005,'h8j'),(566,5,1005,'i5o'),(580,5,1005,'j9k'),(594,5,1005,'k0l'),(608,5,1005,'l2m'),(426,5,1005,'pmi-min-1'),(650,5,1005,'s2d'),(636,5,1005,'t9y'),(622,5,1005,'w3x'),(363,5,1006,'a4n'),(447,5,1006,'a7z'),(461,5,1006,'b4n'),(405,5,1006,'c8v'),(419,5,1006,'d2v'),(475,5,1006,'d3f'),(489,5,1006,'d4t'),(503,5,1006,'e8r'),(517,5,1006,'f0g'),(531,5,1006,'f2h'),(545,5,1006,'f5j'),(391,5,1006,'g6l'),(377,5,1006,'g7h'),(559,5,1006,'h8j'),(573,5,1006,'i5o'),(587,5,1006,'j9k'),(601,5,1006,'k0l'),(615,5,1006,'l2m'),(433,5,1006,'pmi-min-1'),(657,5,1006,'s2d'),(643,5,1006,'t9y'),(629,5,1006,'w3x'),(1352,6,1,'a1a'),(1934,6,1,'a1t'),(1578,6,1,'a2z'),(1756,6,1,'e3r'),(1358,6,2,'a1a'),(1935,6,2,'a1t'),(1579,6,2,'a2z'),(1757,6,2,'e3r'),(1364,6,3,'a1a'),(1936,6,3,'a1t'),(1580,6,3,'a2z'),(1758,6,3,'e3r'),(1368,6,4,'a1a'),(1937,6,4,'a1t'),(1581,6,4,'a2z'),(1759,6,4,'e3r'),(1378,6,5,'a1a'),(1938,6,5,'a1t'),(1582,6,5,'a2z'),(1760,6,5,'e3r'),(1382,6,6,'a1a'),(1939,6,6,'a1t'),(1583,6,6,'a2z'),(1761,6,6,'e3r'),(1389,6,7,'a1a'),(1940,6,7,'a1t'),(1584,6,7,'a2z'),(1762,6,7,'e3r'),(1391,6,8,'a1a'),(1941,6,8,'a1t'),(1585,6,8,'a2z'),(1763,6,8,'e3r'),(1393,6,9,'a1a'),(1942,6,9,'a1t'),(1586,6,9,'a2z'),(1764,6,9,'e3r'),(1395,6,10,'a1a'),(1943,6,10,'a1t'),(1587,6,10,'a2z'),(1765,6,10,'e3r'),(1397,6,11,'a1a'),(1944,6,11,'a1t'),(1588,6,11,'a2z'),(1766,6,11,'e3r'),(1406,6,12,'a1a'),(1945,6,12,'a1t'),(1589,6,12,'a2z'),(1767,6,12,'e3r'),(1413,6,18,'a1a'),(1946,6,18,'a1t'),(1590,6,18,'a2z'),(1768,6,18,'e3r'),(1416,6,30,'a1a'),(1947,6,30,'a1t'),(1591,6,30,'a2z'),(1769,6,30,'e3r'),(1435,6,1000,'a1a'),(1948,6,1000,'a1t'),(1592,6,1000,'a2z'),(5,6,1000,'a4n'),(101,6,1000,'a7z'),(117,6,1000,'b4n'),(53,6,1000,'c8v'),(69,6,1000,'d2v'),(133,6,1000,'d3f'),(149,6,1000,'d4t'),(1770,6,1000,'e3r'),(165,6,1000,'e8r'),(181,6,1000,'f0g'),(197,6,1000,'f2h'),(213,6,1000,'f5j'),(37,6,1000,'g6l'),(21,6,1000,'g7h'),(229,6,1000,'h8j'),(245,6,1000,'i5o'),(261,6,1000,'j9k'),(277,6,1000,'k0l'),(293,6,1000,'l2m'),(85,6,1000,'pmi-min-1'),(341,6,1000,'s2d'),(325,6,1000,'t9y'),(309,6,1000,'w3x'),(665,6,1001,'a4n'),(706,6,1001,'a7z'),(717,6,1001,'b4n'),(871,6,1001,'c8v'),(879,6,1001,'d2v'),(728,6,1001,'d3f'),(739,6,1001,'d4t'),(750,6,1001,'e8r'),(761,6,1001,'f0g'),(772,6,1001,'f2h'),(783,6,1001,'f5j'),(685,6,1001,'g6l'),(675,6,1001,'g7h'),(794,6,1001,'h8j'),(805,6,1001,'i5o'),(816,6,1001,'j9k'),(827,6,1001,'k0l'),(838,6,1001,'l2m'),(695,6,1001,'pmi-min-1'),(889,6,1001,'s2d'),(860,6,1001,'t9y'),(849,6,1001,'w3x'),(666,6,1002,'a4n'),(707,6,1002,'a7z'),(718,6,1002,'b4n'),(872,6,1002,'c8v'),(880,6,1002,'d2v'),(729,6,1002,'d3f'),(740,6,1002,'d4t'),(751,6,1002,'e8r'),(762,6,1002,'f0g'),(773,6,1002,'f2h'),(784,6,1002,'f5j'),(686,6,1002,'g6l'),(676,6,1002,'g7h'),(795,6,1002,'h8j'),(806,6,1002,'i5o'),(817,6,1002,'j9k'),(828,6,1002,'k0l'),(839,6,1002,'l2m'),(696,6,1002,'pmi-min-1'),(890,6,1002,'s2d'),(861,6,1002,'t9y'),(850,6,1002,'w3x'),(13,6,1004,'a4n'),(109,6,1004,'a7z'),(125,6,1004,'b4n'),(61,6,1004,'c8v'),(77,6,1004,'d2v'),(141,6,1004,'d3f'),(157,6,1004,'d4t'),(173,6,1004,'e8r'),(189,6,1004,'f0g'),(205,6,1004,'f2h'),(221,6,1004,'f5j'),(45,6,1004,'g6l'),(29,6,1004,'g7h'),(237,6,1004,'h8j'),(253,6,1004,'i5o'),(269,6,1004,'j9k'),(285,6,1004,'k0l'),(301,6,1004,'l2m'),(93,6,1004,'pmi-min-1'),(349,6,1004,'s2d'),(333,6,1004,'t9y'),(317,6,1004,'w3x'),(357,6,1005,'a4n'),(441,6,1005,'a7z'),(455,6,1005,'b4n'),(399,6,1005,'c8v'),(413,6,1005,'d2v'),(469,6,1005,'d3f'),(483,6,1005,'d4t'),(497,6,1005,'e8r'),(511,6,1005,'f0g'),(525,6,1005,'f2h'),(539,6,1005,'f5j'),(385,6,1005,'g6l'),(371,6,1005,'g7h'),(553,6,1005,'h8j'),(567,6,1005,'i5o'),(581,6,1005,'j9k'),(595,6,1005,'k0l'),(609,6,1005,'l2m'),(427,6,1005,'pmi-min-1'),(651,6,1005,'s2d'),(637,6,1005,'t9y'),(623,6,1005,'w3x'),(364,6,1006,'a4n'),(448,6,1006,'a7z'),(462,6,1006,'b4n'),(406,6,1006,'c8v'),(420,6,1006,'d2v'),(476,6,1006,'d3f'),(490,6,1006,'d4t'),(504,6,1006,'e8r'),(518,6,1006,'f0g'),(532,6,1006,'f2h'),(546,6,1006,'f5j'),(392,6,1006,'g6l'),(378,6,1006,'g7h'),(560,6,1006,'h8j'),(574,6,1006,'i5o'),(588,6,1006,'j9k'),(602,6,1006,'k0l'),(616,6,1006,'l2m'),(434,6,1006,'pmi-min-1'),(658,6,1006,'s2d'),(644,6,1006,'t9y'),(630,6,1006,'w3x'),(1417,7,30,'a1a'),(1949,7,30,'a1t'),(1593,7,30,'a2z'),(1771,7,30,'e3r'),(1428,7,31,'a1a'),(1950,7,31,'a1t'),(1594,7,31,'a2z'),(1772,7,31,'e3r'),(669,8,1,'a4n'),(710,8,1,'a7z'),(721,8,1,'b4n'),(883,8,1,'c8v'),(893,8,1,'d2v'),(732,8,1,'d3f'),(743,8,1,'d4t'),(754,8,1,'e8r'),(765,8,1,'f0g'),(776,8,1,'f2h'),(787,8,1,'f5j'),(689,8,1,'g6l'),(679,8,1,'g7h'),(798,8,1,'h8j'),(809,8,1,'i5o'),(820,8,1,'j9k'),(831,8,1,'k0l'),(842,8,1,'l2m'),(699,8,1,'pmi-min-1'),(895,8,1,'s2d'),(864,8,1,'t9y'),(853,8,1,'w3x'),(1357,8,2,'a1a'),(1951,8,2,'a1t'),(1595,8,2,'a2z'),(1773,8,2,'e3r'),(1361,8,3,'a1a'),(1952,8,3,'a1t'),(1596,8,3,'a2z'),(1774,8,3,'e3r'),(1398,8,11,'a1a'),(1953,8,11,'a1t'),(1597,8,11,'a2z'),(1775,8,11,'e3r'),(1404,8,12,'a1a'),(1954,8,12,'a1t'),(1598,8,12,'a2z'),(1776,8,12,'e3r'),(1418,8,30,'a1a'),(1955,8,30,'a1t'),(1599,8,30,'a2z'),(1777,8,30,'e3r'),(1436,8,1000,'a1a'),(1956,8,1000,'a1t'),(1600,8,1000,'a2z'),(6,8,1000,'a4n'),(102,8,1000,'a7z'),(118,8,1000,'b4n'),(54,8,1000,'c8v'),(70,8,1000,'d2v'),(134,8,1000,'d3f'),(150,8,1000,'d4t'),(1778,8,1000,'e3r'),(166,8,1000,'e8r'),(182,8,1000,'f0g'),(198,8,1000,'f2h'),(214,8,1000,'f5j'),(38,8,1000,'g6l'),(22,8,1000,'g7h'),(230,8,1000,'h8j'),(246,8,1000,'i5o'),(262,8,1000,'j9k'),(278,8,1000,'k0l'),(294,8,1000,'l2m'),(86,8,1000,'pmi-min-1'),(342,8,1000,'s2d'),(326,8,1000,'t9y'),(310,8,1000,'w3x'),(14,8,1004,'a4n'),(110,8,1004,'a7z'),(126,8,1004,'b4n'),(62,8,1004,'c8v'),(78,8,1004,'d2v'),(142,8,1004,'d3f'),(158,8,1004,'d4t'),(174,8,1004,'e8r'),(190,8,1004,'f0g'),(206,8,1004,'f2h'),(222,8,1004,'f5j'),(46,8,1004,'g6l'),(30,8,1004,'g7h'),(238,8,1004,'h8j'),(254,8,1004,'i5o'),(270,8,1004,'j9k'),(286,8,1004,'k0l'),(302,8,1004,'l2m'),(94,8,1004,'pmi-min-1'),(350,8,1004,'s2d'),(334,8,1004,'t9y'),(318,8,1004,'w3x'),(1372,9,4,'a1a'),(1957,9,4,'a1t'),(1601,9,4,'a2z'),(1779,9,4,'e3r'),(1380,9,5,'a1a'),(1958,9,5,'a1t'),(1602,9,5,'a2z'),(1780,9,5,'e3r'),(1419,9,30,'a1a'),(1959,9,30,'a1t'),(1603,9,30,'a2z'),(1781,9,30,'e3r'),(7,9,1000,'a4n'),(103,9,1000,'a7z'),(119,9,1000,'b4n'),(55,9,1000,'c8v'),(71,9,1000,'d2v'),(135,9,1000,'d3f'),(151,9,1000,'d4t'),(167,9,1000,'e8r'),(183,9,1000,'f0g'),(199,9,1000,'f2h'),(215,9,1000,'f5j'),(39,9,1000,'g6l'),(23,9,1000,'g7h'),(231,9,1000,'h8j'),(247,9,1000,'i5o'),(263,9,1000,'j9k'),(279,9,1000,'k0l'),(295,9,1000,'l2m'),(87,9,1000,'pmi-min-1'),(343,9,1000,'s2d'),(327,9,1000,'t9y'),(311,9,1000,'w3x'),(15,9,1004,'a4n'),(111,9,1004,'a7z'),(127,9,1004,'b4n'),(63,9,1004,'c8v'),(79,9,1004,'d2v'),(143,9,1004,'d3f'),(159,9,1004,'d4t'),(175,9,1004,'e8r'),(191,9,1004,'f0g'),(207,9,1004,'f2h'),(223,9,1004,'f5j'),(47,9,1004,'g6l'),(31,9,1004,'g7h'),(239,9,1004,'h8j'),(255,9,1004,'i5o'),(271,9,1004,'j9k'),(287,9,1004,'k0l'),(303,9,1004,'l2m'),(95,9,1004,'pmi-min-1'),(351,9,1004,'s2d'),(335,9,1004,'t9y'),(319,9,1004,'w3x'),(358,9,1005,'a4n'),(442,9,1005,'a7z'),(456,9,1005,'b4n'),(400,9,1005,'c8v'),(414,9,1005,'d2v'),(470,9,1005,'d3f'),(484,9,1005,'d4t'),(498,9,1005,'e8r'),(512,9,1005,'f0g'),(526,9,1005,'f2h'),(540,9,1005,'f5j'),(386,9,1005,'g6l'),(372,9,1005,'g7h'),(554,9,1005,'h8j'),(568,9,1005,'i5o'),(582,9,1005,'j9k'),(596,9,1005,'k0l'),(610,9,1005,'l2m'),(428,9,1005,'pmi-min-1'),(652,9,1005,'s2d'),(638,9,1005,'t9y'),(624,9,1005,'w3x'),(365,9,1006,'a4n'),(449,9,1006,'a7z'),(463,9,1006,'b4n'),(407,9,1006,'c8v'),(421,9,1006,'d2v'),(477,9,1006,'d3f'),(491,9,1006,'d4t'),(505,9,1006,'e8r'),(519,9,1006,'f0g'),(533,9,1006,'f2h'),(547,9,1006,'f5j'),(393,9,1006,'g6l'),(379,9,1006,'g7h'),(561,9,1006,'h8j'),(575,9,1006,'i5o'),(589,9,1006,'j9k'),(603,9,1006,'k0l'),(617,9,1006,'l2m'),(435,9,1006,'pmi-min-1'),(659,9,1006,'s2d'),(645,9,1006,'t9y'),(631,9,1006,'w3x'),(1420,10,30,'a1a'),(1960,10,30,'a1t'),(1604,10,30,'a2z'),(1782,10,30,'e3r'),(1430,10,33,'a1a'),(1961,10,33,'a1t'),(1605,10,33,'a2z'),(1783,10,33,'e3r'),(711,11,1,'a7z'),(722,11,1,'b4n'),(884,11,1,'c8v'),(894,11,1,'d2v'),(733,11,1,'d3f'),(744,11,1,'d4t'),(755,11,1,'e8r'),(766,11,1,'f0g'),(777,11,1,'f2h'),(788,11,1,'f5j'),(799,11,1,'h8j'),(810,11,1,'i5o'),(821,11,1,'j9k'),(832,11,1,'k0l'),(843,11,1,'l2m'),(700,11,1,'pmi-min-1'),(896,11,1,'s2d'),(865,11,1,'t9y'),(854,11,1,'w3x'),(1356,11,2,'a1a'),(1962,11,2,'a1t'),(1606,11,2,'a2z'),(1784,11,2,'e3r'),(1367,11,3,'a1a'),(1963,11,3,'a1t'),(1607,11,3,'a2z'),(1785,11,3,'e3r'),(1369,11,4,'a1a'),(1964,11,4,'a1t'),(1608,11,4,'a2z'),(1786,11,4,'e3r'),(1379,11,5,'a1a'),(1965,11,5,'a1t'),(1609,11,5,'a2z'),(1787,11,5,'e3r'),(1396,11,11,'a1a'),(1966,11,11,'a1t'),(1610,11,11,'a2z'),(1788,11,11,'e3r'),(1407,11,12,'a1a'),(1967,11,12,'a1t'),(1611,11,12,'a2z'),(1789,11,12,'e3r'),(1421,11,30,'a1a'),(1968,11,30,'a1t'),(1612,11,30,'a2z'),(1790,11,30,'e3r'),(1437,11,1000,'a1a'),(1969,11,1000,'a1t'),(1613,11,1000,'a2z'),(8,11,1000,'a4n'),(104,11,1000,'a7z'),(120,11,1000,'b4n'),(56,11,1000,'c8v'),(72,11,1000,'d2v'),(136,11,1000,'d3f'),(152,11,1000,'d4t'),(1791,11,1000,'e3r'),(168,11,1000,'e8r'),(184,11,1000,'f0g'),(200,11,1000,'f2h'),(216,11,1000,'f5j'),(40,11,1000,'g6l'),(24,11,1000,'g7h'),(232,11,1000,'h8j'),(248,11,1000,'i5o'),(264,11,1000,'j9k'),(280,11,1000,'k0l'),(296,11,1000,'l2m'),(88,11,1000,'pmi-min-1'),(344,11,1000,'s2d'),(328,11,1000,'t9y'),(312,11,1000,'w3x'),(16,11,1004,'a4n'),(112,11,1004,'a7z'),(128,11,1004,'b4n'),(64,11,1004,'c8v'),(80,11,1004,'d2v'),(144,11,1004,'d3f'),(160,11,1004,'d4t'),(176,11,1004,'e8r'),(192,11,1004,'f0g'),(208,11,1004,'f2h'),(224,11,1004,'f5j'),(48,11,1004,'g6l'),(32,11,1004,'g7h'),(240,11,1004,'h8j'),(256,11,1004,'i5o'),(272,11,1004,'j9k'),(288,11,1004,'k0l'),(304,11,1004,'l2m'),(96,11,1004,'pmi-min-1'),(352,11,1004,'s2d'),(336,11,1004,'t9y'),(320,11,1004,'w3x'),(359,11,1005,'a4n'),(443,11,1005,'a7z'),(457,11,1005,'b4n'),(401,11,1005,'c8v'),(415,11,1005,'d2v'),(471,11,1005,'d3f'),(485,11,1005,'d4t'),(499,11,1005,'e8r'),(513,11,1005,'f0g'),(527,11,1005,'f2h'),(541,11,1005,'f5j'),(387,11,1005,'g6l'),(373,11,1005,'g7h'),(555,11,1005,'h8j'),(569,11,1005,'i5o'),(583,11,1005,'j9k'),(597,11,1005,'k0l'),(611,11,1005,'l2m'),(429,11,1005,'pmi-min-1'),(653,11,1005,'s2d'),(639,11,1005,'t9y'),(625,11,1005,'w3x'),(366,11,1006,'a4n'),(450,11,1006,'a7z'),(464,11,1006,'b4n'),(408,11,1006,'c8v'),(422,11,1006,'d2v'),(478,11,1006,'d3f'),(492,11,1006,'d4t'),(506,11,1006,'e8r'),(520,11,1006,'f0g'),(534,11,1006,'f2h'),(548,11,1006,'f5j'),(394,11,1006,'g6l'),(380,11,1006,'g7h'),(562,11,1006,'h8j'),(576,11,1006,'i5o'),(590,11,1006,'j9k'),(604,11,1006,'k0l'),(618,11,1006,'l2m'),(436,11,1006,'pmi-min-1'),(660,11,1006,'s2d'),(646,11,1006,'t9y'),(632,11,1006,'w3x'),(1410,12,17,'a1a'),(1970,12,17,'a1t'),(1614,12,17,'a2z'),(1792,12,17,'e3r'),(1427,12,30,'a1a'),(1971,12,30,'a1t'),(1615,12,30,'a2z'),(1793,12,30,'e3r'),(900,13,1,'a4n'),(901,13,1,'a7z'),(902,13,1,'b4n'),(903,13,1,'c8v'),(904,13,1,'d2v'),(905,13,1,'d3f'),(906,13,1,'d4t'),(907,13,1,'e8r'),(908,13,1,'f0g'),(909,13,1,'f2h'),(910,13,1,'f5j'),(911,13,1,'g6l'),(912,13,1,'g7h'),(913,13,1,'h8j'),(914,13,1,'i5o'),(915,13,1,'j9k'),(916,13,1,'k0l'),(917,13,1,'l2m'),(918,13,1,'pmi-min-1'),(919,13,1,'s2d'),(920,13,1,'t9y'),(921,13,1,'w3x'),(922,13,2,'a4n'),(923,13,2,'a7z'),(924,13,2,'b4n'),(925,13,2,'c8v'),(926,13,2,'d2v'),(927,13,2,'d3f'),(928,13,2,'d4t'),(929,13,2,'e8r'),(930,13,2,'f0g'),(931,13,2,'f2h'),(932,13,2,'f5j'),(933,13,2,'g6l'),(934,13,2,'g7h'),(935,13,2,'h8j'),(936,13,2,'i5o'),(937,13,2,'j9k'),(938,13,2,'k0l'),(939,13,2,'l2m'),(940,13,2,'pmi-min-1'),(941,13,2,'s2d'),(942,13,2,'t9y'),(943,13,2,'w3x'),(944,13,3,'a4n'),(945,13,3,'a7z'),(946,13,3,'b4n'),(947,13,3,'c8v'),(948,13,3,'d2v'),(949,13,3,'d3f'),(950,13,3,'d4t'),(951,13,3,'e8r'),(952,13,3,'f0g'),(953,13,3,'f2h'),(954,13,3,'f5j'),(955,13,3,'g6l'),(956,13,3,'g7h'),(957,13,3,'h8j'),(958,13,3,'i5o'),(959,13,3,'j9k'),(960,13,3,'k0l'),(961,13,3,'l2m'),(962,13,3,'pmi-min-1'),(963,13,3,'s2d'),(964,13,3,'t9y'),(965,13,3,'w3x'),(966,13,4,'a4n'),(967,13,4,'a7z'),(968,13,4,'b4n'),(969,13,4,'c8v'),(970,13,4,'d2v'),(971,13,4,'d3f'),(972,13,4,'d4t'),(973,13,4,'e8r'),(974,13,4,'f0g'),(975,13,4,'f2h'),(976,13,4,'f5j'),(977,13,4,'g6l'),(978,13,4,'g7h'),(979,13,4,'h8j'),(980,13,4,'i5o'),(981,13,4,'j9k'),(982,13,4,'k0l'),(983,13,4,'l2m'),(984,13,4,'pmi-min-1'),(985,13,4,'s2d'),(986,13,4,'t9y'),(987,13,4,'w3x'),(988,13,5,'a4n'),(989,13,5,'a7z'),(990,13,5,'b4n'),(991,13,5,'c8v'),(992,13,5,'d2v'),(993,13,5,'d3f'),(994,13,5,'d4t'),(995,13,5,'e8r'),(996,13,5,'f0g'),(997,13,5,'f2h'),(998,13,5,'f5j'),(999,13,5,'g6l'),(1000,13,5,'g7h'),(1001,13,5,'h8j'),(1002,13,5,'i5o'),(1003,13,5,'j9k'),(1004,13,5,'k0l'),(1005,13,5,'l2m'),(1006,13,5,'pmi-min-1'),(1007,13,5,'s2d'),(1008,13,5,'t9y'),(1009,13,5,'w3x'),(1010,13,8,'a4n'),(1011,13,8,'a7z'),(1012,13,8,'b4n'),(1013,13,8,'c8v'),(1014,13,8,'d2v'),(1015,13,8,'d3f'),(1016,13,8,'d4t'),(1017,13,8,'e8r'),(1018,13,8,'f0g'),(1019,13,8,'f2h'),(1020,13,8,'f5j'),(1021,13,8,'g6l'),(1022,13,8,'g7h'),(1023,13,8,'h8j'),(1024,13,8,'i5o'),(1025,13,8,'j9k'),(1026,13,8,'k0l'),(1027,13,8,'l2m'),(1028,13,8,'pmi-min-1'),(1029,13,8,'s2d'),(1030,13,8,'t9y'),(1031,13,8,'w3x'),(1032,13,9,'a4n'),(1033,13,9,'a7z'),(1034,13,9,'b4n'),(1035,13,9,'c8v'),(1036,13,9,'d2v'),(1037,13,9,'d3f'),(1038,13,9,'d4t'),(1039,13,9,'e8r'),(1040,13,9,'f0g'),(1041,13,9,'f2h'),(1042,13,9,'f5j'),(1043,13,9,'g6l'),(1044,13,9,'g7h'),(1045,13,9,'h8j'),(1046,13,9,'i5o'),(1047,13,9,'j9k'),(1048,13,9,'k0l'),(1049,13,9,'l2m'),(1050,13,9,'pmi-min-1'),(1051,13,9,'s2d'),(1052,13,9,'t9y'),(1053,13,9,'w3x'),(1054,13,10,'a4n'),(1055,13,10,'a7z'),(1056,13,10,'b4n'),(1057,13,10,'c8v'),(1058,13,10,'d2v'),(1059,13,10,'d3f'),(1060,13,10,'d4t'),(1061,13,10,'e8r'),(1062,13,10,'f0g'),(1063,13,10,'f2h'),(1064,13,10,'f5j'),(1065,13,10,'g6l'),(1066,13,10,'g7h'),(1067,13,10,'h8j'),(1068,13,10,'i5o'),(1069,13,10,'j9k'),(1070,13,10,'k0l'),(1071,13,10,'l2m'),(1072,13,10,'pmi-min-1'),(1073,13,10,'s2d'),(1074,13,10,'t9y'),(1075,13,10,'w3x'),(1076,13,11,'a4n'),(1077,13,11,'a7z'),(1078,13,11,'b4n'),(1079,13,11,'c8v'),(1080,13,11,'d2v'),(1081,13,11,'d3f'),(1082,13,11,'d4t'),(1083,13,11,'e8r'),(1084,13,11,'f0g'),(1085,13,11,'f2h'),(1086,13,11,'f5j'),(1087,13,11,'g6l'),(1088,13,11,'g7h'),(1089,13,11,'h8j'),(1090,13,11,'i5o'),(1091,13,11,'j9k'),(1092,13,11,'k0l'),(1093,13,11,'l2m'),(1094,13,11,'pmi-min-1'),(1095,13,11,'s2d'),(1096,13,11,'t9y'),(1097,13,11,'w3x'),(1098,13,12,'a4n'),(1099,13,12,'a7z'),(1100,13,12,'b4n'),(1101,13,12,'c8v'),(1102,13,12,'d2v'),(1103,13,12,'d3f'),(1104,13,12,'d4t'),(1105,13,12,'e8r'),(1106,13,12,'f0g'),(1107,13,12,'f2h'),(1108,13,12,'f5j'),(1109,13,12,'g6l'),(1110,13,12,'g7h'),(1111,13,12,'h8j'),(1112,13,12,'i5o'),(1113,13,12,'j9k'),(1114,13,12,'k0l'),(1115,13,12,'l2m'),(1116,13,12,'pmi-min-1'),(1117,13,12,'s2d'),(1118,13,12,'t9y'),(1119,13,12,'w3x'),(1120,13,18,'a4n'),(1121,13,18,'a7z'),(1122,13,18,'b4n'),(1123,13,18,'c8v'),(1124,13,18,'d2v'),(1125,13,18,'d3f'),(1126,13,18,'d4t'),(1127,13,18,'e8r'),(1128,13,18,'f0g'),(1129,13,18,'f2h'),(1130,13,18,'f5j'),(1131,13,18,'g6l'),(1132,13,18,'g7h'),(1133,13,18,'h8j'),(1134,13,18,'i5o'),(1135,13,18,'j9k'),(1136,13,18,'k0l'),(1137,13,18,'l2m'),(1138,13,18,'pmi-min-1'),(1139,13,18,'s2d'),(1140,13,18,'t9y'),(1141,13,18,'w3x'),(1142,13,22,'a4n'),(1143,13,22,'a7z'),(1144,13,22,'b4n'),(1145,13,22,'c8v'),(1146,13,22,'d2v'),(1147,13,22,'d3f'),(1148,13,22,'d4t'),(1149,13,22,'e8r'),(1150,13,22,'f0g'),(1151,13,22,'f2h'),(1152,13,22,'f5j'),(1153,13,22,'g6l'),(1154,13,22,'g7h'),(1155,13,22,'h8j'),(1156,13,22,'i5o'),(1157,13,22,'j9k'),(1158,13,22,'k0l'),(1159,13,22,'l2m'),(1160,13,22,'pmi-min-1'),(1161,13,22,'s2d'),(1162,13,22,'t9y'),(1163,13,22,'w3x'),(1164,13,24,'a4n'),(1165,13,24,'g6l'),(1166,13,24,'g7h'),(1167,13,25,'a4n'),(1168,13,25,'g6l'),(1169,13,25,'g7h'),(1170,13,26,'a4n'),(1171,13,26,'g6l'),(1172,13,26,'g7h'),(1173,13,27,'a4n'),(1174,13,27,'g6l'),(1175,13,27,'g7h'),(1176,13,34,'c8v'),(1177,13,35,'a4n'),(1178,13,35,'g6l'),(1179,13,35,'g7h'),(1180,13,36,'c8v'),(1181,13,37,'a4n'),(1182,13,37,'g6l'),(1183,13,37,'g7h'),(1184,13,38,'c8v'),(1185,13,39,'a4n'),(1186,13,39,'c8v'),(1187,13,39,'g6l'),(1188,13,39,'g7h'),(1189,13,41,'a4n'),(1190,13,41,'c8v'),(1191,13,41,'g6l'),(1192,13,41,'g7h'),(1193,13,43,'s2d'),(1194,13,45,'d2v'),(1195,13,46,'a4n'),(1196,13,46,'a7z'),(1197,13,46,'b4n'),(1198,13,46,'c8v'),(1199,13,46,'d2v'),(1200,13,46,'d3f'),(1201,13,46,'d4t'),(1202,13,46,'e8r'),(1203,13,46,'f0g'),(1204,13,46,'f2h'),(1205,13,46,'f5j'),(1206,13,46,'g6l'),(1207,13,46,'g7h'),(1208,13,46,'h8j'),(1209,13,46,'i5o'),(1210,13,46,'j9k'),(1211,13,46,'k0l'),(1212,13,46,'l2m'),(1213,13,46,'pmi-min-1'),(1214,13,46,'s2d'),(1215,13,46,'t9y'),(1216,13,46,'w3x'),(1217,13,1000,'a4n'),(1218,13,1000,'a7z'),(1219,13,1000,'b4n'),(1220,13,1000,'c8v'),(1221,13,1000,'d2v'),(1222,13,1000,'d3f'),(1223,13,1000,'d4t'),(1224,13,1000,'e8r'),(1225,13,1000,'f0g'),(1226,13,1000,'f2h'),(1227,13,1000,'f5j'),(1228,13,1000,'g6l'),(1229,13,1000,'g7h'),(1230,13,1000,'h8j'),(1231,13,1000,'i5o'),(1232,13,1000,'j9k'),(1233,13,1000,'k0l'),(1234,13,1000,'l2m'),(1235,13,1000,'pmi-min-1'),(1236,13,1000,'s2d'),(1237,13,1000,'t9y'),(1238,13,1000,'w3x'),(1239,13,1001,'a4n'),(1240,13,1001,'a7z'),(1241,13,1001,'b4n'),(1242,13,1001,'c8v'),(1243,13,1001,'d2v'),(1244,13,1001,'d3f'),(1245,13,1001,'d4t'),(1246,13,1001,'e8r'),(1247,13,1001,'f0g'),(1248,13,1001,'f2h'),(1249,13,1001,'f5j'),(1250,13,1001,'g6l'),(1251,13,1001,'g7h'),(1252,13,1001,'h8j'),(1253,13,1001,'i5o'),(1254,13,1001,'j9k'),(1255,13,1001,'k0l'),(1256,13,1001,'l2m'),(1257,13,1001,'pmi-min-1'),(1258,13,1001,'s2d'),(1259,13,1001,'t9y'),(1260,13,1001,'w3x'),(1261,13,1002,'a4n'),(1262,13,1002,'a7z'),(1263,13,1002,'b4n'),(1264,13,1002,'c8v'),(1265,13,1002,'d2v'),(1266,13,1002,'d3f'),(1267,13,1002,'d4t'),(1268,13,1002,'e8r'),(1269,13,1002,'f0g'),(1270,13,1002,'f2h'),(1271,13,1002,'f5j'),(1272,13,1002,'g6l'),(1273,13,1002,'g7h'),(1274,13,1002,'h8j'),(1275,13,1002,'i5o'),(1276,13,1002,'j9k'),(1277,13,1002,'k0l'),(1278,13,1002,'l2m'),(1279,13,1002,'pmi-min-1'),(1280,13,1002,'s2d'),(1281,13,1002,'t9y'),(1282,13,1002,'w3x'),(1283,13,1004,'a4n'),(1284,13,1004,'a7z'),(1285,13,1004,'b4n'),(1286,13,1004,'c8v'),(1287,13,1004,'d2v'),(1288,13,1004,'d3f'),(1289,13,1004,'d4t'),(1290,13,1004,'e8r'),(1291,13,1004,'f0g'),(1292,13,1004,'f2h'),(1293,13,1004,'f5j'),(1294,13,1004,'g6l'),(1295,13,1004,'g7h'),(1296,13,1004,'h8j'),(1297,13,1004,'i5o'),(1298,13,1004,'j9k'),(1299,13,1004,'k0l'),(1300,13,1004,'l2m'),(1301,13,1004,'pmi-min-1'),(1302,13,1004,'s2d'),(1303,13,1004,'t9y'),(1304,13,1004,'w3x'),(1305,13,1005,'a4n'),(1306,13,1005,'a7z'),(1307,13,1005,'b4n'),(1308,13,1005,'c8v'),(1309,13,1005,'d2v'),(1310,13,1005,'d3f'),(1311,13,1005,'d4t'),(1312,13,1005,'e8r'),(1313,13,1005,'f0g'),(1314,13,1005,'f2h'),(1315,13,1005,'f5j'),(1316,13,1005,'g6l'),(1317,13,1005,'g7h'),(1318,13,1005,'h8j'),(1319,13,1005,'i5o'),(1320,13,1005,'j9k'),(1321,13,1005,'k0l'),(1322,13,1005,'l2m'),(1323,13,1005,'pmi-min-1'),(1324,13,1005,'s2d'),(1325,13,1005,'t9y'),(1326,13,1005,'w3x'),(1327,13,1006,'a4n'),(1328,13,1006,'a7z'),(1329,13,1006,'b4n'),(1330,13,1006,'c8v'),(1331,13,1006,'d2v'),(1332,13,1006,'d3f'),(1333,13,1006,'d4t'),(1334,13,1006,'e8r'),(1335,13,1006,'f0g'),(1336,13,1006,'f2h'),(1337,13,1006,'f5j'),(1338,13,1006,'g6l'),(1339,13,1006,'g7h'),(1340,13,1006,'h8j'),(1341,13,1006,'i5o'),(1342,13,1006,'j9k'),(1343,13,1006,'k0l'),(1344,13,1006,'l2m'),(1345,13,1006,'pmi-min-1'),(1346,13,1006,'s2d'),(1347,13,1006,'t9y'),(1348,13,1006,'w3x');
/*!40000 ALTER TABLE `t_type_contrat_et_procedure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_groupement_entreprise`
--

LOCK TABLES `t_type_groupement_entreprise` WRITE;
/*!40000 ALTER TABLE `t_type_groupement_entreprise` DISABLE KEYS */;
INSERT INTO `t_type_groupement_entreprise` (`id_type_groupement`, `libelle_type_groupement`) VALUES (1,'TYPE_GROUPEMENT_SOLIDAIRE'),(2,'TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE'),(3,'TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE');
/*!40000 ALTER TABLE `t_type_groupement_entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_notification_agent`
--

LOCK TABLES `t_type_notification_agent` WRITE;
/*!40000 ALTER TABLE `t_type_notification_agent` DISABLE KEYS */;
INSERT INTO `t_type_notification_agent` (`id_type`, `libelle`) VALUES (1,'TYPE_NOTIFICATION_CONSULTATION');
/*!40000 ALTER TABLE `t_type_notification_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_type_procedure_dume`
--

LOCK TABLES `t_type_procedure_dume` WRITE;
/*!40000 ALTER TABLE `t_type_procedure_dume` DISABLE KEYS */;
INSERT INTO `t_type_procedure_dume` (`id`, `libelle`, `active`, `code_dume`) VALUES (1,'Procédure adaptée','1','01'),(2,'Appel d\'offre ouvert','1','02'),(3,'Appel d\'offre restreint','1','03'),(4,'Procédure concurrentielle avec négociation','1','04'),(5,'Procédure négociée  avec mise en concurrence préalable','1','05'),(6,'Marché négocié sans publicité ni mise en concurrence préalable','1','06'),(7,'Dialogue compétitif','1','07'),(8,'Concours','1','08'),(9,'Autres','1','09');
/*!40000 ALTER TABLE `t_type_procedure_dume` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `t_vision_rma_agent_organisme`
--

LOCK TABLES `t_vision_rma_agent_organisme` WRITE;
/*!40000 ALTER TABLE `t_vision_rma_agent_organisme` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_vision_rma_agent_organisme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `trace_operations_inscrit`
--

LOCK TABLES `trace_operations_inscrit` WRITE;
/*!40000 ALTER TABLE `trace_operations_inscrit` DISABLE KEYS */;
INSERT INTO `trace_operations_inscrit` (`id_trace`, `id_inscrit`, `id_entreprise`, `addr_ip`, `date`, `operations`, `organisme`, `ref_consultation`, `afficher`) VALUES (1,1,1,'172.17.0.1','2019-03-14','\n2019-03-14 17:32:32;page=Entreprise.EntrepriseHome&goto=;-;2019-03-14 17:32:32\n2019-03-14 17:32:32;page=Entreprise.EntrepriseAccueilAuthentifie;-;2019-03-14 17:32:32',NULL,NULL,'1');
/*!40000 ALTER TABLE `trace_operations_inscrit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_contrat_concession_pivot`
--

LOCK TABLES `type_contrat_concession_pivot` WRITE;
/*!40000 ALTER TABLE `type_contrat_concession_pivot` DISABLE KEYS */;
INSERT INTO `type_contrat_concession_pivot` (`id`, `libelle`) VALUES (1,'Concession de travaux'),(2,'Concession de service'),(3,'Concession de service public'),(4,'Délégation de service public');
/*!40000 ALTER TABLE `type_contrat_concession_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_contrat_mpe_pivot`
--

LOCK TABLES `type_contrat_mpe_pivot` WRITE;
/*!40000 ALTER TABLE `type_contrat_mpe_pivot` DISABLE KEYS */;
INSERT INTO `type_contrat_mpe_pivot` (`id`, `id_type_contrat_mpe`, `id_type_contrat_pivot`) VALUES (1,1,1),(2,2,3),(3,3,3),(4,4,1),(5,5,3),(6,6,3),(7,7,4),(8,8,2),(9,9,1),(10,10,1),(11,11,1),(12,12,1),(13,13,3);
/*!40000 ALTER TABLE `type_contrat_mpe_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_contrat_pivot`
--

LOCK TABLES `type_contrat_pivot` WRITE;
/*!40000 ALTER TABLE `type_contrat_pivot` DISABLE KEYS */;
INSERT INTO `type_contrat_pivot` (`id`, `libelle`) VALUES (1,'TYPE_CONTRAT_MARCHE'),(2,'TYPE_CONTRAT_PARTENARIAT_PUBLIC_PRIVE'),(3,'DEFINE_ACCORD_CADRES'),(4,'TEXT_MARCHE_SUBSEQUENT');
/*!40000 ALTER TABLE `type_contrat_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_procedure_concession_pivot`
--

LOCK TABLES `type_procedure_concession_pivot` WRITE;
/*!40000 ALTER TABLE `type_procedure_concession_pivot` DISABLE KEYS */;
INSERT INTO `type_procedure_concession_pivot` (`id`, `libelle`) VALUES (1,'Procédure négociée ouverte'),(2,'Procédure non négociée ouverte'),(3,'Procédure négociée restreinte'),(4,'Procédure non négociée restreinte');
/*!40000 ALTER TABLE `type_procedure_concession_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_procedure_mpe_pivot`
--

LOCK TABLES `type_procedure_mpe_pivot` WRITE;
/*!40000 ALTER TABLE `type_procedure_mpe_pivot` DISABLE KEYS */;
INSERT INTO `type_procedure_mpe_pivot` (`id`, `id_type_procedure_mpe`, `id_type_procedure_pivot`) VALUES (1,1,2),(2,2,3),(3,3,1),(4,4,2),(5,5,3),(6,7,7),(7,8,5),(8,9,5),(9,10,2),(10,11,2),(11,15,2),(12,30,2),(13,31,2),(14,32,2),(15,33,2),(16,35,2),(17,1000,4);
/*!40000 ALTER TABLE `type_procedure_mpe_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `type_procedure_pivot`
--

LOCK TABLES `type_procedure_pivot` WRITE;
/*!40000 ALTER TABLE `type_procedure_pivot` DISABLE KEYS */;
INSERT INTO `type_procedure_pivot` (`id`, `libelle`) VALUES (1,'PROCEDURE_ADAPTEE'),(2,'TEXT_APPEL_OFFRE_OUVERT'),(3,'APPEL_OFFRES_RESTREINT'),(4,'TEXT_PROCEDURE_CONCURENTIELLE_AVEC_NEGOCIATION'),(5,'TEXT_PROCEDURE_CONCURENTIELLE_AVEC_CONCURRENCE'),(6,'TEXT_PROCEDURE_CONCURENTIELLE_SANS_NEGOCIATION_NI_CONCURRENCE'),(7,'DIALOGUE_COMPETITIF');
/*!40000 ALTER TABLE `type_procedure_pivot` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `visite_lieux`
--

LOCK TABLES `visite_lieux` WRITE;
/*!40000 ALTER TABLE `visite_lieux` DISABLE KEYS */;
/*!40000 ALTER TABLE `visite_lieux` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-03-15 10:19:24
