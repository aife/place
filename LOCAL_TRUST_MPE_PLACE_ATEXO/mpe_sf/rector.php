<?php

// /!\
// /!\ Désactiver XDebug avant de lancer rector
// $ export XDEBUG_MODE=off

// À modifier ligne 126 dans mpe_sf/vendor/pradosoft/prado/framework/PradoBase.php
//register_shutdown_function(['\Prado\PradoBase', 'phpFatalErrorHandler']);
// + Ajouter "use Prado\Web\UI\TControl;" au début du fichier PradoBase.php

// /!\ A faire :
// $ cd mpe_sf/
// $ mkdir -p client/config/
// $ client/config/config.php

declare(strict_types=1);

use Rector\Config\RectorConfig;
use Rector\Set\ValueObject\LevelSetList;
use Rector\Symfony\Rector\MethodCall\GetParameterToConstructorInjectionRector;
use Rector\Symfony\Set\SymfonyLevelSetList;
use Rector\Symfony\Set\SymfonySetList;

return static function (RectorConfig $rectorConfig): void {
    $rectorConfig->paths([
//        __DIR__ . '/src/Attribute',
//        __DIR__ . '/src/Command',
//        __DIR__ . '/src/Controller',
//        __DIR__ . '/src/DataFixtures',
//        __DIR__ . '/src/DataPersister',
//        __DIR__ . '/src/DataProvider',
//        __DIR__ . '/src/DataTransformer',
//        __DIR__ . '/src/Doctrine',
//        __DIR__ . '/src/Dto',
//        __DIR__ . '/src/Entity',
//        __DIR__ . '/src/Repository',

//        __DIR__ . '/DoctrineMigrations',
//        __DIR__ . '/tests',

//        __DIR__ . '/src/Event',
//        __DIR__ . '/src/EventListener',
//        __DIR__ . '/src/EventSubscriber',
//        __DIR__ . '/src/Exception',
//        __DIR__ . '/src/Filter',
//        __DIR__ . '/src/Form',
//        __DIR__ . '/src/Legacy',
//        __DIR__ . '/src/Listener',
//        __DIR__ . '/src/Logger',
//        __DIR__ . '/src/Message',
//        __DIR__ . '/src/MessageHandler',
//        __DIR__ . '/src/Migrations',
//        __DIR__ . '/src/Model',
//        __DIR__ . '/src/OpenApi',
//        __DIR__ . '/src/Processor',
//        __DIR__ . '/src/Repository',
//        __DIR__ . '/src/Security',
//        __DIR__ . '/src/Serializer',
//        __DIR__ . '/src/Service',
//        __DIR__ . '/src/Traits',
//        __DIR__ . '/src/Twig',
//        __DIR__ . '/src/Utils',
        __DIR__ . '/src/Validator',
        __DIR__ . '/src/WebService',
//        __DIR__ . '/src/Serializer',

//        __DIR__ . '/../mpe-librairies',
//        __DIR__ . '/DoctrineMigrations',
//        __DIR__ . '/tests',
//        __DIR__ . '/legacy',
//        __DIR__ . '/legacy/protected/Bin',
//        __DIR__ . '/legacy/protected/Cli',
//        __DIR__ . '/legacy/protected/config',
//        __DIR__ . '/legacy/protected/Controls',
//        __DIR__ . '/legacy/protected/Data',
//        __DIR__ . '/legacy/protected/Layouts',
//        __DIR__ . '/legacy/protected/Library',
//        __DIR__ . '/legacy/protected/library',
//        __DIR__ . '/legacy/protected/Pages',
//        __DIR__ . '/legacy/protected/Propel',
//        __DIR__ . '/legacy/protected/runtime',
//        __DIR__ . '/legacy/protected/scripts-sql',
//        __DIR__ . '/legacy/protected/Service',
//        __DIR__ . '/legacy/protected/templates',
//        __DIR__ . '/legacy/protected/themes',
//        __DIR__ . '/legacy/protected/var',
//        __DIR__ . '/legacy/protected/Pages/Administration',
//        __DIR__ . '/legacy/protected/Pages/Agent',
//        __DIR__ . '/legacy/protected/Pages/Commun',
//        __DIR__ . '/legacy/protected/Pages/Cron',
//        __DIR__ . '/legacy/protected/Pages/Entreprise',
//        __DIR__ . '/legacy/protected/Pages/Frame',
//        __DIR__ . '/legacy/protected/Pages/GestionPub',
//        __DIR__ . '/legacy/protected/Pages/Helios',
//        __DIR__ . '/legacy/protected/Pages/WebService',
//        __DIR__ . '/legacy/protected/Pages/Zabbix',
    ]);

//    $rectorConfig->skip([
//        __DIR__ . '/legacy',
//        __DIR__ . '/legacy/protected/Library/phing/',
//    ]);

    $rectorConfig->importNames();

    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_81,
    ]);

//    $rectorConfig->skip([
//        \Rector\Php81\Rector\Class_\SpatieEnumClassToEnumRector::class,
//    ]);
};
