#!/bin/bash

phpbin=$(which php8.1)
composerbin=$(which composer2)

if [[ $composerbin == '' ]]
then
  composerbin=$(which composer)
fi

"${phpbin}" ${composerbin} CycloneDX:make-sbom  --output-file=bom.xml
