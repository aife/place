#!/bin/sh

#
# Copyright (C) Atexo - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
#

RED='\e[31m'
NC='\033[0m'

# full paths from the repo root separated by newlines
MUST_NOT_CHANGE='mpe_sf/assets/modern-admin/'
CAN_CHANGE='mpe_sf/assets/modern-admin/index.js
mpe_sf/assets/modern-admin/mpe.scss
mpe_sf/assets/modern-admin/scss/core/variables/_bootstrap-variables.scss
mpe_sf/assets/modern-admin/scss/core/variables/_components-variables.scss
mpe_sf/assets/modern-admin/scss/core/variables/_material-variables.scss
mpe_sf/assets/modern-admin/scss/core/colors/palette-variables.scss
mpe_sf/assets/modern-admin/scss/core/colors/material-palette-variables.scss
'

if git rev-parse --verify HEAD >/dev/null 2>&1
then
  against=HEAD
else
  # Initial commit: diff against an empty tree object
  against=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi

exec 1>&2

if git diff --cached --name-only $against |
   grep --quiet --fixed-strings "$MUST_NOT_CHANGE" |
   grep --quiet --line-regexp --fixed-strings -v "$CAN_CHANGE"
then
  echo
  echo "${RED}ERREUR : Les fichiers du thème Modern Admin ne doivent pas être modifiés (cf. README.md).${NC}"
  echo
  exit 1
else
  exit 0
fi
