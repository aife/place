#!/usr/bin/env bash

#
# Copyright (C) Atexo - All Rights Reserved
# Unauthorized copying of this file, via any medium is strictly prohibited
# Proprietary and confidential
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
GIT_DIRECTORY=$(git rev-parse --show-toplevel)

echo

if [ -d "${GIT_DIRECTORY}/.git/hooks/" ]
then
  printf "Initialisation des hooks Git... "

  for hookFile in $(ls ${DIR}/hooks)
  do
    rm -f ${GIT_DIRECTORY}/.git/hooks/$hookFile ||:
    ln -s ../../mpe_sf/grumphp-config/hooks/$hookFile ${GIT_DIRECTORY}/.git/hooks/$hookFile
  done

  printf "Terminée"
else
  echo "Le répertoire .git/hooks n'existe pas"; exit;
fi

echo; echo
