#!/usr/bin/env bash

BLUE=$'\e[44m'
NC=$'\033[0m'

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

PHP_VERSION=8.1

phpbin=$(which "php${PHP_VERSION}")
composerbin=$(which composer)

COMPOSER_MIRROR_PATH_REPOS=1 "${phpbin}" ${composerbin} install --no-plugins --no-interaction --no-ansi --no-scripts --no-suggest --quiet

PHPUNIT_OPTIONS="--colors=auto"

if [ "$PHPUNIT_COVERAGE_GENERATION" != "1" ]
then
  echo ${BLUE}; echo;
  echo " Sans code coverage."
  echo " Pour lancer avec la couverture de code, exécutez :"
  echo "   \$ PHPUNIT_COVERAGE_GENERATION=1 ./phpunit.sh"
  echo ${NC}; echo

  PHPUNIT_OPTIONS="$PHPUNIT_OPTIONS --no-coverage"
else
  export XDEBUG_MODE=coverage
  PHPUNIT_OPTIONS="$PHPUNIT_OPTIONS --coverage-cobertura ../reports/cobertura.xml"
fi

SYMFONY_PHPUNIT_REQUIRE="nikic/php-parser:4.13" "${phpbin}" ./vendor/bin/simple-phpunit $PHPUNIT_OPTIONS $@

result=$?

exit $result
