<?php

namespace Tests\Unit\Serializer\Messec;

use App\Model\Messec\Email;
use App\Model\Messec\Message;
use App\Model\Messec\PieceJointe;
use App\Serializer\Messec\EmailDenormalizer;
use App\Serializer\Messec\MessageDenormalizer;
use App\Serializer\Messec\PieceJointeDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class EmailDenormalizerTest extends TestCase
{
    public function testDenormalize()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $email = $serializer->deserialize($this->getData('liste'), Email::class, 'json');
        $this->assertTrue($email instanceof Email);
        $this->assertEquals(1, $email->getId());
        $message = $email->getMessage();
        $this->assertTrue($message instanceof Message);
        $this->assertEquals(2, $message->getId());
        $piecesJointes = $message->getPiecesJointes();
        $this->assertTrue($piecesJointes[0] instanceof PieceJointe);
        $this->assertTrue($piecesJointes[1] instanceof PieceJointe);
        $this->assertEquals(3, $piecesJointes[0]->getId());
        $this->assertEquals(4, $piecesJointes[1]->getId());
    }

    private function getDenormalizer()
    {
        return [
            new EmailDenormalizer(
                new ObjectNormalizer(),
                new MessageDenormalizer(
                    new ObjectNormalizer(),
                    new PieceJointeDenormalizer(new ObjectNormalizer())
                )
            ),
        ];
    }

    private function getEncoder()
    {
        return [new JsonEncoder()];
    }

    private function getData($type = null)
    {
        if ('liste' == $type) {
            return '[{"id":1,"message":{"id":2, "piecesJointes":[{"id":3},{"id":4}]},"reponse":{"id":5}}]';
        } else {
            return '{"id":1,"message":{"id":2, "piecesJointes":[{"id":3},{"id":4}]},"reponse":{"id":5}}';
        }
    }

    public function testSupportsDenormalization()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $isSupported = $serializer->supportsDenormalization($this->getData(), Email::class, 'json');
        $this->assertTrue($isSupported);
    }

    public function testInvalidSupportsDenormalization()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $isSupported = $serializer->supportsDenormalization($this->getData(), Message::class, 'json');
        $this->assertFalse($isSupported);
    }
}
