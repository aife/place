<?php

namespace Tests\Unit\Serializer\Messec;

use App\Model\Messec\Email;
use App\Model\Messec\PieceJointe;
use App\Serializer\Messec\PieceJointeDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class PieceJointeDenormalizerTest extends TestCase
{
    public function testDenormalize()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $piecesJointe = $serializer->deserialize($this->getData(), PieceJointe::class, 'json');
        $this->assertTrue($piecesJointe instanceof PieceJointe);
        $this->assertEquals(3, $piecesJointe->getId());
    }

    public function testSupportsDenormalization()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $isSupported = $serializer->supportsDenormalization($this->getData(), PieceJointe::class, 'json');
        $this->assertTrue($isSupported);
    }

    public function testInvalidSupportsDenormalization()
    {
        $serializer = new Serializer($this->getDenormalizer(), $this->getEncoder());
        $isSupported = $serializer->supportsDenormalization($this->getData(), Email::class, 'json');
        $this->assertFalse($isSupported);
    }

    private function getDenormalizer()
    {
        return [new PieceJointeDenormalizer(new ObjectNormalizer())];
    }

    private function getEncoder()
    {
        return [new JsonEncoder()];
    }

    private function getData()
    {
        return '{"id":3}';
    }
}
