<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Serializer\Messenger\Tncp;

use App\Message\Tncp\Consumer\MessageSuiviConsultationInput;
use App\Serializer\Messenger\Tncp\SuiviConsultationJsonMessageSerializer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Messenger\Exception\MessageDecodingFailedException;

class SuiviConsultationJsonMessageSerializerTest extends TestCase
{
    public function testDecode()
    {
        $serialiser = new SuiviConsultationJsonMessageSerializer();
        $envelopeEncoded = [
            'body' => "{\"uuid\":\"ebb020df-d2c3-44a7-9afc-2d5ba5f59100\",\"flux\":\"TNCP_SORTANTE\",\"statut\":
            \"TNCP_SORTANTE\",\"type\":\"PUBLICATION_CONSULTATION\",\"idObjetSource\":503901,\"idObjetDestination\":0,
            \"url\":\"\\/\",\"uuidPlateforme\":\"mpe_jenkins_release_sprint\",\"token\":\"\",
            \"dateEnvoi\":\"2022-09-08T10:36:39+02:00\"}",
            'properties' => [],
            'headers' => []
        ];

        $envelope = $serialiser->decode($envelopeEncoded);

        $this->assertInstanceOf(MessageSuiviConsultationInput::class, $envelope->getMessage());
        $this->assertEquals("ebb020df-d2c3-44a7-9afc-2d5ba5f59100", $envelope->getMessage()->getUuid());
    }

    public function testDecodeException()
    {
        $this->expectException(MessageDecodingFailedException::class);
        $serialiser = new SuiviConsultationJsonMessageSerializer();
        $envelopeEncoded = [
            'body' => "ewq",
            'properties' => [],
            'headers' => []
        ];

        $envelope = $serialiser->decode($envelopeEncoded);

        $this->assertInstanceOf(MessageSuiviConsultationInput::class, $envelope->getMessage());
        $this->assertEquals("ebb020df-d2c3-44a7-9afc-2d5ba5f59100", $envelope->getMessage()->getUuid());
    }
}
