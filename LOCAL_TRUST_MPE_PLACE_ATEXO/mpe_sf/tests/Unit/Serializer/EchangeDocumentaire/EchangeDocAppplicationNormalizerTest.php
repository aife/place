<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationClientNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationNormalizer;
use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EchangeDocAppplicationNormalizerTest.
 */
class EchangeDocAppplicationNormalizerTest extends TestCase
{
    public const CODE = 'code';
    public const LIBELLE = 'libelle';

    /**
     * @throws \ReflectionException
     */
    public function testNormalize()
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory);
        $normalize = new EchangeDocApplicationNormalizer($objectNormalizer);
        $echangeDocApplication = new EchangeDocApplication();
        $class = new \ReflectionClass($echangeDocApplication);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDocApplication, 1);
        $echangeDocApplication->setCode(self::CODE);
        $echangeDocApplication->setLibelle(self::LIBELLE);
        $normalize = $normalize->normalize($echangeDocApplication, $format, $context);

        $result = [
            self::CODE => self::CODE,
            self::LIBELLE => self::LIBELLE,
            EchangeDocApplicationClientNormalizer::ENVOI_DIC => false
        ];

        $this->assertEquals($result, $normalize);
    }
}
