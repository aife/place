<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Serializer\EchangeDocumentaire\EchangeDocNormalizer;
use PHPUnit\Framework\TestCase;

/**
 * Class EchangeDocNormalizerTest.
 */
class EchangeDocNormalizerTest extends TestCase
{
    /**
     * test fonction adresserDonneesHistorique.
     */
    public function testSerializer()
    {
        $data = [1 => ['id' => 1,
                        'nom' => 'Test',
                        'statut' => 'Envoyé',
                        'messageFonctionnel' => null,
                        'messageTechnique' => null, ],
                 2 => ['id' => 2,
                        'nom' => 'Test',
                        'statut' => 'Echec',
                        'messageFonctionnel' => null,
                        'messageTechnique' => null, ],
            ];
        $result = ['historique' => $data];
        $echangeDocSerializer = new EchangeDocNormalizer();
        $this->assertEquals($echangeDocSerializer->adresserDonneesHistorique($data), $result);
    }
}
