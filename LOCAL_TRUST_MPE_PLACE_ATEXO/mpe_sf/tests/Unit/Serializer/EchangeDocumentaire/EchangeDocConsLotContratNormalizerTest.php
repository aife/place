<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\ContratTitulaire;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\Entreprise;
use App\Repository\EntrepriseRepository;
use App\Serializer\EchangeDocumentaire\EchangeDocConsLotContratNormalizer;
use App\Service\ContratService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EchangeDocConsLotContratNormalizerTest extends TestCase
{
    private const TEST_DATA_SET = [
        'idContratTitulaire' => 1,
        'numeroContrat' => null,
        'montantContrat' => 42,
        'nom' => 'Toto',
    ];

    /** @var EntityManager|\PHPUnit\Framework\MockObject\MockObject */
    private $entityManager;

    /** @var EchangeDoc|\PHPUnit\Framework\MockObject\MockObject */
    private $echangeDoc;

    /**
     * @var ContratService|\PHPUnit\Framework\MockObject\MockObject
     */
    private $contratService;

    protected function setUp(bool $empty = false): void
    {
        $contratTitulaire = $this->createMock(ContratTitulaire::class);
        $contratTitulaire
            ->method('getIdContratTitulaire')
            ->willReturn(self::TEST_DATA_SET['idContratTitulaire']);
        $contratTitulaire
            ->method('getNumeroContrat')
            ->willReturn(self::TEST_DATA_SET['numeroContrat']);
        $contratTitulaire
            ->method('getMontantContrat')
            ->willReturn(self::TEST_DATA_SET['montantContrat']);
        $contratTitulaire
            ->method('getNomAgent')
            ->willReturn(self::TEST_DATA_SET['nom']);

        $echangeDoc = $this->createMock(EchangeDoc::class);
        $echangeDoc
            ->method('getContratTitulaire')
            ->willReturn($contratTitulaire);
        $this->echangeDoc = $echangeDoc;

        $this->contratService = $this->createMock(ContratService::class);
        $this->contratService
            ->method('getMontantMarche')
            ->willReturn(self::TEST_DATA_SET['montantContrat'])
            ;

        $entreprise = new Entreprise();
        $entreprise->setNom(self::TEST_DATA_SET['nom']);
        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->method('find')
            ->willReturn($entreprise);

        $entityManager = $this->createMock(EntityManager::class);
        $entityManager
            ->method('getRepository')
            ->willReturn($entrepriseRepository);
        $this->entityManager = $entityManager;
    }

    public function testNormalize()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);

        $normalizer = new EchangeDocConsLotContratNormalizer(
            $this->entityManager,
            $this->contratService,
            $parameterBag,
            $logger,
            $webServiceExec
        );

        $data = $normalizer->normalize($this->echangeDoc);
        $this->assertEquals([
            EchangeDocConsLotContratNormalizer::CONSULTATION_CONTRAT_LIE_ID => self::TEST_DATA_SET['idContratTitulaire'],
            EchangeDocConsLotContratNormalizer::CONSULTATION_CONTRAT_ATTRIBUTAIRE => self::TEST_DATA_SET['nom'],
            EchangeDocConsLotContratNormalizer::CONSULTATION_CONTRAT_LIE_NUMERO => self::TEST_DATA_SET['numeroContrat'],
            EchangeDocConsLotContratNormalizer::CONSULTATION_CONTRAT_MONTANT => self::TEST_DATA_SET['montantContrat'],
        ], $data);
    }

    public function testNormalizeEmptyData()
    {
        $this->setUp(true);
        $echangeDoc = $this->createMock(EchangeDoc::class);
        $entityManager = $this->createMock(EntityManager::class);
        $contratService = $this->createMock(ContratService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);

        $normalizer = new EchangeDocConsLotContratNormalizer(
            $entityManager,
            $contratService,
            $parameterBag,
            $logger,
            $webServiceExec,
        );
        $data = $normalizer->normalize($echangeDoc);
        $this->assertEmpty($data);
    }
}
