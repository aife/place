<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationClientNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocumentaireNormalizer;
use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EchangeDocApplicationClientNormalizerTest.
 */
class EchangeDocApplicationClientNormalizerTest extends TestCase
{
    public const ECHANGE_DOC_APPLICATION = 'echangeDocApplication';
    public const ECHANGE_DOC_APPLICATION_CLIENT = 'echangeDocApplicationClient';
    public const CODE = 'code';
    public const LIBELLE = 'libelle';
    public const ENVOI_DIC = false;
    public const NOT_NULL_TEST_STRING = 'toto';
    public const NOT_NULL_TEST_TRUE = 1;
    public const NOT_NULL_TEST_FALSE = 0;
    public const MULTI_MAIN_DOCS = '1';
    private const PARAPHEUR_CODE = 'PARAPHEUR_CODE';

    public $result = [];

    protected function setUp(): void
    {
        $this->result = [
            self::ECHANGE_DOC_APPLICATION => [
                self::CODE => self::CODE,
                self::LIBELLE => self::LIBELLE,
                EchangeDocApplicationClientNormalizer::ENVOI_DIC => self::ENVOI_DIC
            ],
            self::ECHANGE_DOC_APPLICATION_CLIENT => [
                self::CODE => self::CODE,
                self::LIBELLE => self::LIBELLE,
                EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATIONS => [
                    EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_1 => self::PARAPHEUR_CODE,
                    EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_2 => null,
                    EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_3 => null,
                    EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_4 => null,
                    EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_5 => null,
                ],
                EchangeDocApplicationClientNormalizer::MULTI_DOCS_PRINCIPAUX => self::MULTI_MAIN_DOCS,
            ],
            EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CHEMINEMENT => [
                EchangeDocApplicationClientNormalizer::ECHANGE_DOC_SIGNATURE => self::NOT_NULL_TEST_TRUE,
                EchangeDocApplicationClientNormalizer::ECHANGE_DOC_TDT => self::NOT_NULL_TEST_FALSE,
                EchangeDocApplicationClientNormalizer::ECHANGE_DOC_SAE => self::NOT_NULL_TEST_FALSE,
                EchangeDocApplicationClientNormalizer::ECHANGE_DOC_GED => self::NOT_NULL_TEST_FALSE,
            ],
        ];
    }

    /**
     * @throws \ReflectionException
     */
    public function testNormalize(): void
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
            EchangeDocumentaireNormalizer::SOUS_TYPE_PARAPHEUR => self::PARAPHEUR_CODE
        ];

        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory);
        $echangeDocApplicationNormalizer = new EchangeDocApplicationNormalizer(
            new ObjectNormalizer($classMetadataFactory)
        );
        $normalize = new EchangeDocApplicationClientNormalizer($objectNormalizer, $echangeDocApplicationNormalizer);

        $echangeDocApplication = new EchangeDocApplication();
        $class = new \ReflectionClass($echangeDocApplication);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDocApplication, 1);
        $echangeDocApplication->setCode(self::CODE);
        $echangeDocApplication->setLibelle(self::LIBELLE);
        $echangeDocApplication->setEnvoiDic(false);

        $echangeDoc = new EchangeDoc();
        $class = new \ReflectionClass($echangeDoc);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDoc, 1);
        $echangeDoc->setCheminementSignature(true);

        $echangeDocApplicationClient = new EchangeDocApplicationClient();
        $class = new \ReflectionClass($echangeDocApplicationClient);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDocApplicationClient, 1);
        $echangeDocApplicationClient->setCode(self::CODE);
        $echangeDocApplicationClient->setLibelle(self::LIBELLE);
        $echangeDocApplicationClient->setEchangeDocApplication($echangeDocApplication);
        $echangeDocApplicationClient->addEchangeDocs($echangeDoc);
        $echangeDocApplicationClient->setMultiDocsPrincipaux(true);

        $testArray = $normalize->normalize($echangeDocApplicationClient, $format, $context);
        $this->assertEquals($this->result, $testArray);

        $echangeDocApplicationClient->setClassification1(self::NOT_NULL_TEST_STRING);
        $echangeDocApplicationClient->setClassification2(self::NOT_NULL_TEST_STRING);
        $this->result[self::ECHANGE_DOC_APPLICATION_CLIENT]
        [EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATIONS]
        [EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_1] = self::PARAPHEUR_CODE;
        $this->result[self::ECHANGE_DOC_APPLICATION_CLIENT]
        [EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATIONS]
        [EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CLASSIFICATION_2] = self::NOT_NULL_TEST_STRING;

        $testArray = $normalize->normalize($echangeDocApplicationClient, $format, $context);
        $this->assertEquals($this->result, $testArray);

        unset($this->result[EchangeDocApplicationClientNormalizer::ECHANGE_DOC_CHEMINEMENT]);
        $echangeDocApplicationClient->removeEchangeDocs($echangeDoc);
        $testArray = $normalize->normalize($echangeDocApplicationClient, $format, $context);
        $this->assertEquals($this->result, $testArray);
    }
}
