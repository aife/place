<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\BloborganismeFile;
use App\Serializer\EchangeDocumentaire\BlobOrganismeEchangeNormalizer;
use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class BlobOrganismeEchangeNormalizerTest.
 */
class BlobOrganismeEchangeNormalizerTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testNormalize()
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory);
        $normalize = new BlobOrganismeEchangeNormalizer($objectNormalizer);
        $blob = new BloborganismeFile();
        $class = new \ReflectionClass($blob);
        $blob->setStatutSynchro(0);
        $blob->setHash('test');
        $blob->setOldId(0);
        $blob->setName('test');
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($blob, 1);
        $blobNormalize = $normalize->normalize($blob, $format, $context);
        $result = ['id' => 0, 'name' => 'test'];
        $this->assertEquals($result, $blobNormalize);
    }
}
