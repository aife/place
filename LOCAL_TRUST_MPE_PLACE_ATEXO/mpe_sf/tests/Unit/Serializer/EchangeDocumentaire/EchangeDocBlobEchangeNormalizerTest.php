<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\BloborganismeFile;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\Organisme;
use App\Repository\EchangeDocumentaire\EchangeDocBlobRepository;
use App\Serializer\EchangeDocumentaire\BlobOrganismeEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocBlobEchangeNormalizer;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EchangeDocBlobEchangeNormalizerTest.
 */
class EchangeDocBlobEchangeNormalizerTest extends TestCase
{
    /** @var EntityManager|\PHPUnit\Framework\MockObject\MockObject */
    private $entityManager;

    protected function setUp(): void
    {
        $echangeDocBlobRepository = $this->createMock(EchangeDocBlobRepository::class);
        $echangeDocBlobRepository
            ->method('findBy')
            ->willReturn([]);

        $echangeDocBlobRepository
            ->method('findOneBy')
            ->willReturn(1);

        $entityManager = $this->createMock(EntityManager::class);
        $entityManager
            ->method('getRepository')
            ->willReturn($echangeDocBlobRepository);
        $this->entityManager = $entityManager;
    }

    /**
     * @throws \ReflectionException
     */
    public function testNormalize()
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory);
        $blobOrganismeEchangeNormalizer = new BlobOrganismeEchangeNormalizer($objectNormalizer);
        $normalize = new EchangeDocBlobEchangeNormalizer(
            $objectNormalizer,
            $blobOrganismeEchangeNormalizer,
            $this->entityManager
        );

        $echange = new EchangeDocBlob();
        $class = new \ReflectionClass($echange);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echange, 1);

        $organisme = new Organisme();
        $class = new \ReflectionClass($organisme);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organisme, 1);

        $blobOrganisme = new BloborganismeFile();
        $class = new \ReflectionClass($blobOrganisme);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($blobOrganisme, 1);

        $blobOrganisme->setOrganisme($organisme);
        $blobOrganisme->setName('name');

        $echange->setPoids('10');
        $echange->setChecksum('AQWZSXEDC');
        $echange->setBlobOrganisme($blobOrganisme);
        $echange->setCategoriePiece(1);

        $blobNormalize = $normalize->normalize($echange, $format, $context);
        $result = [
            'id' => 1,
            'nom' => 'name',
            'taille' => '10',
            'checksum' => 'AQWZSXEDC',
            'type' => null,
            'relations' => [
                'documentPrincipal' => 1,
                'annexes' => [],
                'idPrimoSignature' => null,
            ],
        ];
        $this->assertEquals($result, $blobNormalize);
    }
}
