<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\TypeContrat;
use App\Entity\TypeProcedure;
use App\Serializer\EchangeDocumentaire\ConsultationEchangeNormalizer;
use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ConsultationEchangeNormalizerTest.
 */
class ConsultationEchangeNormalizerTest extends TestCase
{
    /**
     * @throws \ReflectionException
     */
    public function testNormalize()
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $objectNormalizer = new ObjectNormalizer($classMetadataFactory);
        $normalize = new ConsultationEchangeNormalizer($objectNormalizer);
        $consultation = new Consultation();
        $consultation->setTypeProcedure(new TypeProcedure());
        $consultation->setAcronymeOrg('test');
        $consultation->setCategorie(1);

        $typeMarche = new TypeContrat();
        $classTypeMarche = new \ReflectionClass($typeMarche);
        $propertyTypeMarche = $classTypeMarche->getProperty('idTypeContrat');
        $propertyTypeMarche->setAccessible(true);
        $propertyTypeMarche->setValue($typeMarche, 1);

        $consultation->setTypeMarche($typeMarche);

        $typeProcedure = new TypeProcedure();
        $typeProcedure->setAbbreviation('ABV');
        $organisme = new Organisme();
        $organisme->setAcronyme('test');
        $consultation->setOrganisme($organisme);
        $consultation->setTypeProcedure($typeProcedure);
        $class = new \ReflectionClass($consultation);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($consultation, 1);
        $consultationNormalize = $normalize->normalize($consultation, $format, $context);

        $result = [
            'id' => 1,
            'organisme' => 'test',
            'naturePrestation' => 'TRAVAUX',
            'typeProcedure' => 'ABV',
            'reference' => '',
            'idDirectionService' => null,
            'intitule' => null,
            'objet' => null,
            'contrat' => 1,
            'idEntite' => null,
        ];
        $this->assertEquals($result, $consultationNormalize);
    }
}
