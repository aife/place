<?php

namespace Tests\Unit\Serializer\EchangeDocumentaire;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Serializer\EchangeDocumentaire\ConsultationEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationClientNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocBlobEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocConsLotContratNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocumentaireNormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EchangeDocumentaireNormalizerTest.
 */
class EchangeDocumentaireNormalizerTest extends TestCase
{
    public const OBJET = 'objet';
    public const DESCRIPTION = 'description';

    /**
     * @throws \ReflectionException
     */
    public function testNormalize()
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];

        $consultationEchangeNormalizer = $this
            ->createMock(ConsultationEchangeNormalizer::class);
        $consultationEchangeNormalizer
            ->method('normalize')
            ->willReturn([]);
        $echangeDocApplicationClientNormalizer = $this
            ->createMock(EchangeDocApplicationClientNormalizer::class);
        $echangeDocApplicationClientNormalizer
            ->method('normalize')
            ->willReturn([]);

        $normalize = new EchangeDocumentaireNormalizer(
            $this->createMock(ObjectNormalizer::class),
            $consultationEchangeNormalizer,
            $this->createMock(EchangeDocBlobEchangeNormalizer::class),
            $echangeDocApplicationClientNormalizer,
            $this->createMock(EchangeDocConsLotContratNormalizer::class)
        );

        $echangeDoc = new EchangeDoc();
        $class = new \ReflectionClass($echangeDoc);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDoc, 1);

        $consultation = new Consultation();
        $class = new \ReflectionClass($consultation);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($consultation, 1);

        $applicationClient = new EchangeDocApplicationClient();
        $class = new \ReflectionClass($applicationClient);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($applicationClient, 1);

        $application = new EchangeDocApplication();
        $class = new \ReflectionClass($application);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($application, 1);

        $agent = new Agent();
        $class = new \ReflectionClass($agent);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($agent, 1);
        $agent->setNom('nom');
        $agent->setPrenom('prenom');

        $dateMock = $this->createMock(\DateTime::class);

        $echangeDoc->setAgent($agent);
        $echangeDoc->setObjet(self::OBJET);
        $echangeDoc->setDescription(self::DESCRIPTION);
        $echangeDoc->setStatut('ENVOYE');
        $echangeDoc->setConsultation($consultation);
        $echangeDoc->setEchangeDocApplicationClient($applicationClient);
        $echangeDoc->setCreatedAt($dateMock);
        $echangeDoc->setUpdatedAt($dateMock);

        $class = new \ReflectionClass($echangeDoc);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDoc, 1);
        $echangeDocNormalize = $normalize->normalize($echangeDoc, $format, $context);

        $expectedResult = [
            'id' => 1,
            self::OBJET => self::OBJET,
            self::DESCRIPTION => self::DESCRIPTION,
            'applicationDistante' => [],
            'statut' => 'ENVOYE',
            'agentCreateur' => [
                'id' => 1,
                'nom' => 'prenom nom',
            ],
            'dateCreation' => null,
            'dateModification' => null,
            'consultation' => [],
            'contratLie' => null,
        ];
        $this->assertEquals($expectedResult, $echangeDocNormalize);
    }

    /**
     * @throws \ReflectionException
     */
    public function testNormalizeEchangeDocumentsBlobNumber(): void
    {
        $format = 'json';
        $context = [
            'groups' => [
                'webservice',
            ],
        ];

        $consultationEchangeNormalizer = $this
            ->createMock(ConsultationEchangeNormalizer::class);
        $consultationEchangeNormalizer
            ->method('normalize')
            ->willReturn([]);
        $echangeDocApplicationClientNormalizer = $this
            ->createMock(EchangeDocApplicationClientNormalizer::class);
        $echangeDocApplicationClientNormalizer
            ->method('normalize')
            ->willReturn([]);

        $normalize = new EchangeDocumentaireNormalizer(
            $this->createMock(ObjectNormalizer::class),
            $consultationEchangeNormalizer,
            $this->createMock(EchangeDocBlobEchangeNormalizer::class),
            $echangeDocApplicationClientNormalizer,
            $this->createMock(EchangeDocConsLotContratNormalizer::class)
        );

        $echangeDoc = new EchangeDoc();
        $applicationClient = new EchangeDocApplicationClient();
        $agent = new Agent();
        $dateMock = $this->createMock(\DateTime::class);

        $echangeDoc->setAgent($agent);
        $echangeDoc->setObjet(self::OBJET);
        $echangeDoc->setDescription(self::DESCRIPTION);
        $echangeDoc->setStatut('ENVOYE');
        $echangeDoc->setEchangeDocApplicationClient($applicationClient);
        $echangeDoc->setCreatedAt($dateMock);
        $echangeDoc->setUpdatedAt($dateMock);

        $expectedNumberDocs = 5;

        $echangeDocBlobPrincipal1 = new class () extends EchangeDocBlob {
            public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
            {
                return self::FICHIER_PRINCIPAL;
            }
        };

        $echangeDocBlobPrincipal1Annexe1 = new class () extends EchangeDocBlob {
            public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
            {
                return self::FICHIER_ANNEXE;
            }
        };

        $echangeDocBlobPrincipal1Annexe2 = new class () extends EchangeDocBlob {
            public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
            {
                return self::FICHIER_ANNEXE;
            }
        };

        $echangeDocBlobPrincipal2 = new class () extends EchangeDocBlob {
            public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
            {
                return self::FICHIER_PRINCIPAL;
            }
        };

        $echangeDocBlobPrincipal2Annexe1 = new class () extends EchangeDocBlob {
            public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
            {
                return self::FICHIER_ANNEXE;
            }
        };

        $echangeDoc->addEchangeDocsBlob($echangeDocBlobPrincipal1);
        $echangeDoc->addEchangeDocsBlob($echangeDocBlobPrincipal1Annexe1);
        $echangeDoc->addEchangeDocsBlob($echangeDocBlobPrincipal1Annexe2);
        $echangeDoc->addEchangeDocsBlob($echangeDocBlobPrincipal2);
        $echangeDoc->addEchangeDocsBlob($echangeDocBlobPrincipal2Annexe1);

        $class = new \ReflectionClass($echangeDoc);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($echangeDoc, 1);
        $echangeDocNormalize = $normalize->normalize($echangeDoc, $format, $context);

        // 1 seul noeud 'documents'.
        $this->assertCount(
            1,
            $echangeDocNormalize[EchangeDocumentaireNormalizer::ECHANGE_DOC_DOCUMENTS_GROUP]
        );
        // Autant de sous-noeuds 'document' que de documents.
        $this->assertCount(
            $expectedNumberDocs,
            $echangeDocNormalize
            [EchangeDocumentaireNormalizer::ECHANGE_DOC_DOCUMENTS_GROUP]
            [EchangeDocumentaireNormalizer::ECHANGE_DOC_DOCUMENT]
        );
    }
}
