<?php

namespace Tests\Unit\Serializer\Denormalizer;

use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Serializer\Denormalizer\OrganismeDenormalizer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

class OrganismeDenormalizerTest extends TestCase
{
    public function testDenormalize()
    {
        $serializer = $this->getDenormalizer();
        $organisme = $serializer->deserialize($this->getData(), Organisme::class, 'json');
        $this->assertTrue($organisme instanceof Organisme);
        $this->assertEquals(null, $organisme->getId());
        $this->assertEquals('pmi-min-1', $organisme->getAcronyme());
        $this->assertEquals('6 RUE DE LA PAIX', $organisme->getAdresse());
        $this->assertEquals('93600', $organisme->getCp());
    }

    public function testSupportsDenormalization()
    {
        /** @var OrganismeDenormalizer $serializer */
        $serializer = $this->getDenormalizer();
        $isSupported = $serializer->supportsDenormalization($this->getData(), Organisme::class, 'json');
        $this->assertTrue($isSupported);
    }

    public function testInvalidSupportsDenormalization()
    {
        $serializer = $this->getDenormalizer();
        $isSupported = $serializer->supportsDenormalization($this->getData(), Consultation::class, 'json');
        $this->assertFalse($isSupported);
    }

    private function getDenormalizer()
    {
        $encoders = [new JsonEncoder(), new XmlEncoder()];
        $normalizers = [new OrganismeDenormalizer()];

        return new Serializer($normalizers, $encoders);
    }

    private function getData()
    {
        return '{"id":1,"acronyme":"pmi-min-1","categorieInsee":"4.1.30","sigle":"PM1","siren":"440909562",'.
            '"nic":"00033","denomination":"Dénomination","adresse":{"rue":"6 RUE DE LA PAIX",'.'
            "codePostal":"93600","ville":"Paris","pays":"France"}}';
    }
}
