<?php

/**
 * Created by PhpStorm.
 * User: malik
 * Date: 21/02/19
 * Time: 14:32.
 */

namespace Tests\Unit;

use App\Entity\Agent;
use App\Entity\CategoriesConsiderationsSociales;
use App\Entity\ConsLotContrat;
use App\Entity\Consultation\ClausesN1;
use App\Entity\Consultation\ClausesN2;
use App\Entity\Consultation\ClausesN3;
use App\Entity\GeolocalisationN1;
use App\Entity\Referentiel\Consultation\ClausesN1 as ReferentielClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2 as ReferentielClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3 as ReferentielClausesN3;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\GeolocalisationN2;
use App\Entity\InvitationConsultationTransverse;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TypeContrat;
use App\Entity\TypeContratConcessionPivot;
use App\Entity\ValeurReferentiel;
use App\Entity\WS\AgentTechniqueToken;
use App\Repository\AgentRepository;
use App\Repository\CategoriesConsiderationsSocialesRepository;
use App\Repository\ContratTitulaireRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Repository\GeolocalisationN1Repository;
use App\Repository\GeolocalisationN2Repository;
use App\Repository\InvitationConsultationTransverseRepository;
use App\Repository\OrganismeRepository;
use App\Repository\ServiceRepository;
use App\Repository\TypeContratRepository;
use App\Repository\WS\AgentTechniqueTokenRepository;
use App\Service\AtexoUtil;
use App\Service\ContratService;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use DOMDocument;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ReflectionClass;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Translation\Translator;

class AtexoTestCase extends TestCase
{
    protected $xsd = __DIR__ . '/../Functional/Controller/Api/WS_MPE_SF.xsd';

    protected function getTypesContrat()
    {
        $content = file_get_contents(__DIR__ . '/data/t_type_contrat.json');
        $jsonData = json_decode($content);

        $listeTypeContrat = [];

        foreach ($jsonData as $c) {
            $typeContrat = new TypeContrat();

            $class = new \ReflectionClass($typeContrat);
            $property = $class->getProperty('idTypeContrat');
            $property->setAccessible(true);
            $property->setValue($typeContrat, $c->id_type_contrat);
            $typeContrat->setLibelleTypeContrat($c->libelle_type_contrat);
            $typeContrat->setAbreviationTypeContrat($c->abreviation_type_contrat);
            $typeContrat->setTypeContratStatistique($c->type_contrat_statistique);
            $typeContrat->setMulti($c->multi);
            $typeContrat->setAccordCadreSad($c->accord_cadre_sad);
            $typeContrat->setAvecChapeau($c->avec_chapeau);
            $typeContrat->setAvecMontant($c->avec_montant);
            $typeContrat->setModeEchangeChorus($c->mode_echange_chorus);
            $typeContrat->setMarcheSubsequent($c->marche_subsequent);
            $typeContrat->setAvecMontantMax($c->avec_montant_max);
            $typeContrat->setOrdreAffichage($c->ordre_affichage);
            $typeContrat->setArticle133($c->article_133);
            $typeContrat->setCodeDume($c->code_dume);
            $typeContrat->setArticle133($c->article_133);
            $typeContrat->setConcession($c->concession);
            $typeContrat->setIdExterne($c->id_externe);
            $listeTypeContrat[] = $typeContrat;
        }

        return $listeTypeContrat;
    }

    protected function getContrats()
    {
        $content = file_get_contents(__DIR__ . '/data/contrats.json');
        $jsonData = json_decode($content);

        $contrats = [];

        foreach ($jsonData as $c) {
            $contactContrat = new ContactContrat();
            $contactContrat
                ->setIdContactContrat(1)
                ->setNom('nom')
                ->setPrenom('prenom')
                ->setEmail('email@email.fr')
            ;

            $contrat = new ContratTitulaire();

            $clauseSociale = new ClausesN1();
            $clauseN2 = new ClausesN2();
            $clauseN3 = new ClausesN3();
            $clauseN2->setReferentielClauseN2((new ReferentielClausesN2())
                ->setSlug(ReferentielClausesN2::CONDITION_EXECUTION));
            $clauseN3->setReferentielClauseN3((new ReferentielClausesN3())
                ->setSlug(ReferentielClausesN3::CLAUSE_SOCIALE_SIAE));

            $clauseSociale->setReferentielClauseN1((new ReferentielClausesN1())
                ->setSlug(ReferentielClausesN1::CLAUSES_SOCIALES));
            $clauseN2->addClausesN3($clauseN3);
            $clauseSociale->addClausesN2($clauseN2);

            $clauseEnvironnementale = new ClausesN1();
            $clauseN2 = new ClausesN2();
            $clauseN2->setReferentielClauseN2((new ReferentielClausesN2())
                ->setSlug(ReferentielClausesN2::CRITERES_SELECTIONS));
            $clauseEnvironnementale->setReferentielClauseN1((new ReferentielClausesN1())
                ->setSlug(ReferentielClausesN1::CLAUSES_ENVIRONNEMENTALES));
            $clauseEnvironnementale->addClausesN2($clauseN2);

            $contrat->setContact($contactContrat);
            $contrat->addClausesN1($clauseSociale);
            $contrat->addClausesN1($clauseEnvironnementale);

            foreach ($c as $property => $value) {
                if ('id_contrat_titulaire' == $property) {
                    $class = new \ReflectionClass($contrat);
                    $property = $class->getProperty('idContratTitulaire');
                    $property->setAccessible(true);
                    $contrat->setId($c->id_contrat_titulaire);
                }
                if (null === $value) {
                    continue;
                }

                $p = str_replace(' ', '', ucwords(str_replace('_', ' ', $property)));
                $method = sprintf('set%s', ucwords($p));

                if (!method_exists($contrat, $method)) {
                    continue;
                }

                if (false !== strpos($method, 'setDate')) {
                    $contrat->$method(new \DateTime('NOW'));
                    continue;
                }

                $contrat->{$method}($value);
            }
            $contrats[] = $contrat;
        }

        return $contrats;
    }

    protected function getAtexoObjectManager()
    {
        $objectManager = $this->createMock(EntityManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) {
                    static $callCount = [];
                    $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];

                    if (ContratTitulaire::class === $entityName) {
                        $repo = $this->createMock(ContratTitulaireRepository::class);

                        $repo->expects($this->any())
                            ->method('find')
                            ->with($this->anything())
                            ->will($this->returnCallback([$this, 'findContrat']));

                        return $repo;
                    }
                    if (AgentTechniqueToken::class === $entityName) {
                        $repo = $this->createMock(AgentTechniqueTokenRepository::class);
                        $agentTechniqueToken = new AgentTechniqueToken();
                        $agent = new Agent();
                        $agentTechniqueToken->setAgent($agent);
                        $repo->expects($this->once())
                            ->method('findOneBy')
                            ->with($this->anything())
                            ->willReturn($agentTechniqueToken);

                        return $repo;
                    }
                    if ($entityName === Service::class) {
                        $service = new Service();
                        $class = new ReflectionClass($service);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($service, 999999);
                        $service->setLibelle('test service');
                        $service->setOldId(32155);

                        $repo = $this->createMock(ServiceRepository::class);

                        $repo
                            ->method('findOneBy')
                            ->with($this->anything())
                            ->willReturn($service);

                        return $repo;
                    }
                    if ($entityName === Organisme::class) {
                        $organisme = new Organisme();
                        $class = new ReflectionClass($organisme);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($organisme, 999999);
                        $organisme->setDenominationOrg('test organisme');

                        $repo = $this->createMock(OrganismeRepository::class);

                        $repo
                            ->method('findOneBy')
                            ->with($this->anything())
                            ->willReturn($organisme);

                        return $repo;
                    }

                    if (TypeContrat::class === $entityName) {

                        $repo = $this->createMock(TypeContratRepository::class);

                        $repo->expects($this->any())
                            ->method('find')
                            ->with($this->anything())
                            ->will($this->returnCallback([$this, 'findTypeContrat']));

                        return $repo;
                    }

                    if (Etablissement::class === $entityName) {
                        $etablissement = new Etablissement();
                        $etablissement->setCodeEtablissement('testCodeEtablissement');
                        $repo = $this->createMock(EtablissementRepository::class);
                        $repo->expects($this->any())
                            ->method('find')
                            ->willReturn($etablissement);

                        return $repo;
                    }

                    if (GeolocalisationN2::class === $entityName) {
                        $repo = $this->createMock(GeolocalisationN2Repository::class);
                        $repo->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(new GeolocalisationN2());

                        return $repo;
                    }

                    if (GeolocalisationN1::class === $entityName) {
                        $repo = $this->createMock(GeolocalisationN1Repository::class);
                        $repo->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(new GeolocalisationN1());

                        return $repo;
                    }
                    if ($entityName === Agent::class) {
                        $agent = new Agent();
                        $class = new ReflectionClass($agent);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($agent, 999999);
                        $agent->setLogin('loginTest');
                        $agent->setNom('nomtest');
                        $agent->setPrenom('prenom');
                        $agent->setEmail('email@agent.fr');

                        $repo = $this->createMock(AgentRepository::class);

                        $repo
                            ->method('find')
                            ->with($this->anything())
                            ->willReturn($agent);

                        return $repo;
                    }
                    if (Entreprise::class === $entityName) {
                        $entreprise = new Entreprise();
                        $entreprise->setSiren('testSiren');
                        $repo = $this->createMock(EntrepriseRepository::class);
                        $repo->expects($this->any())
                            ->method('find')
                            ->willReturn($entreprise);

                        return $repo;
                    }

                    if (TypeContratConcessionPivot::class === $entityName) {
                        $repo = $this->createMock(ObjectRepository::class);
                        $repo->expects($this->any())
                            ->method('find')
                            ->willReturn(new TypeContratConcessionPivot());

                        return $repo;
                    }

                    if (ConsLotContrat::class === $entityName) {
                        $repo = $this->createMock(ObjectRepository::class);
                        $repo->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(null);

                        return $repo;
                    }

                    if (ValeurReferentiel::class === $entityName) {
                        $repo = $this->createMock(ObjectRepository::class);
                        $repo->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(null);

                        return $repo;
                    }

                    if (CategoriesConsiderationsSociales::class === $entityName) {
                        $repo = $this->createMock(CategoriesConsiderationsSocialesRepository::class);
                        $repo->expects($this->any())
                            ->method('getAllFormatedCategories')
                            ->willReturn([]);

                        return $repo;
                    }

                    if (InvitationConsultationTransverse::class === $entityName) {
                        $repo = $this->createMock(InvitationConsultationTransverseRepository::class);
                        $repo->expects($this->any())
                            ->method('findIfContratExists')
                            ->willReturn([]);

                        return $repo;
                    }
                }
            ));

        return $objectManager;
    }

    public function findTypeContrat($id)
    {
        $typesContrat = $this->getTypesContrat();

        foreach ($typesContrat as $typeContrat) {
            if ($typeContrat->getIdTypeContrat() == $id) {
                return $typeContrat;
            }
        }

        return null;
    }

    public function findContrat($id)
    {
        $contrats = $this->getContrats();

        foreach ($contrats as $contrat) {
            if ($contrat->getIdContratTitulaire() == $id) {
                return $contrat;
            }
        }

        return null;
    }

    /**
     * @param $response
     * @param $mode
     * @param int $statutCode
     */
    protected function checkResponse($response, $mode, $statutCode = 200)
    {
        $this->assertEquals($statutCode, $response->getStatusCode());
        $data = $response->getContent();

        if ('xml' == $mode) {
            $this->assertNotEmpty($data);
            $this->assertTrue($this->hasValidSchema($data));
        } elseif ('json' == $mode) {
            $this->assertNotEmpty($data);
            $this->assertTrue($this->isValidJson($data));

            $json = json_decode($data);
            $this->assertNotNull($json->mpe->reponse);
        }

        return $data;
    }

    /**
     * @param $xml
     *
     * @return bool
     */
    protected function hasValidSchema($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml, LIBXML_NOBLANKS);
        $dom->xinclude();

        return $dom->schemaValidate($this->xsd);
    }

    /**
     * @param $string
     *
     * @return bool
     */
    protected function isValidJson($string)
    {
        json_decode($string);

        return JSON_ERROR_NONE == json_last_error();
    }

    /**
     * @return AtexoUtil
     */
    public function getAtexoUtil()
    {
        $requestStackMock = $this->createMock(RequestStack::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        return new AtexoUtil(
            null,
            new Session(),
            new Translator('fr'),
            null,
            $requestStackMock,
            $parameterBagMock
        );
    }
}
