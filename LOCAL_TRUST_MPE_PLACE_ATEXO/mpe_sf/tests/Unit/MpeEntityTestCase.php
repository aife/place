<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

abstract class MpeEntityTestCase extends TestCase
{
    /**
     * Retourne la classe entité à tester.
     *
     * @return mixed
     */
    abstract protected function getTestedEntity();

    /**
     * Arguments à passer dans le constructeur de l'entité.
     */
    protected function getTestedEntityConstructorArguments(): array
    {
        return [];
    }

    /**
     * Liste des attributs attendus dans la classe entité.
     */
    protected function getExpectedAttributes(): array
    {
        $bddFieldNames = $this->getBddFieldNames();

        array_walk($bddFieldNames, [$this, 'snakeCaseToCamelCase']);

        return $bddFieldNames;
    }

    /**
     * Liste des champs de la table BDD associée à l'entité.
     */
    protected function getBddFieldNames(): array
    {
        return [];
    }

    /**
     * Liste des attributs de l'entité à ignorer lors du contrôle.
     */
    protected function getIgnoredObjectAttributes(): array
    {
        return [];
    }

    /**
     * Test qui vérifie la cohérences des attributs dans la classe et attendus dans le test.
     */
    public function testAttributes()
    {
        $testedEntity = $this->getTestedEntity();
        $entityObject = new $testedEntity(...$this->getTestedEntityConstructorArguments());

        $exceptedAttributes = $this->getExpectedAttributes();
        $ignoredAttributes = $this->getIgnoredObjectAttributes();

        $entityAttributes = array_keys((array) $entityObject);
        array_walk($entityAttributes, [$this, 'cleanAttributesArray']);

        foreach ($ignoredAttributes as $ignoredAttribute) {
            if (($key = array_search($ignoredAttribute, $entityAttributes)) !== false) {
                unset($entityAttributes[$key]);
            }
        }

        $this->assertEmpty(
            $result = array_diff($exceptedAttributes, $entityAttributes),
            "Certains attributs de l'entité sont en trop :\n" . var_export($result, 1)
        );

        $this->assertEmpty(
            $result = array_diff($entityAttributes, $exceptedAttributes),
            "Certains attributs attendus sont manquants dans l'entité :\n" . var_export($result, 1)
        );

        sort($exceptedAttributes);
        sort($entityAttributes);
        $this->assertEquals($exceptedAttributes, $entityAttributes);
    }

    /**
     * Permet de "cleaner" le nom de l'attribut.
     *
     * @param $value
     */
    protected function cleanAttributesArray(&$value)
    {
        $replaceChar = mb_substr($value, 0, 1);
        $value = str_replace("$replaceChar" . $this->getTestedEntity() . "$replaceChar", '', $value);

        // Un attribut d'un Trait est préfixé lors de l'introspection
        $value = str_replace("\0*\0", '', $value);
    }

    /**
     * Transforme une chaîne snake_case en camelCase.
     *
     * @param $string
     */
    protected function snakeCaseToCamelCase(&$string)
    {
        $string = lcfirst(str_replace('_', '', ucwords($string, '_')));
    }
}
