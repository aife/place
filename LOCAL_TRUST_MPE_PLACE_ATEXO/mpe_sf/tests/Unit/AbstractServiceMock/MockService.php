<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\AbstractServiceMock;

use App\Service\AbstractService;

class MockService extends AbstractService
{
    protected function getTimeOut()
    {
        $this->setHttpClientThrow(false);
        $this->request('POST', 'http://mpe-develop.local-trust.com');
    }
    protected function doRequest()
    {
        return $this->request('POST', 'http://mpe-develop.local-trust.com')->getContent(false);
    }
}
