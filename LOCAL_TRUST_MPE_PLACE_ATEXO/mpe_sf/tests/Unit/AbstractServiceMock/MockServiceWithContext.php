<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\AbstractServiceMock;

use App\Entity\Agent;
use App\Service\AbstractService;

class MockServiceWithContext extends AbstractService
{
    protected function doRequest()
    {
        $this->getAgentContext('', new Agent(), 'jdjgdghstysb');
        return $this->request('POST', 'http://mpe-develop.local-trust.com')->getContent(false);
    }
}
