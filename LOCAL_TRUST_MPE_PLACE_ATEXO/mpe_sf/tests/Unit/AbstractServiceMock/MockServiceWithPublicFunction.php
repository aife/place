<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\AbstractServiceMock;

use App\Service\AbstractService;

class MockServiceWithPublicFunction extends AbstractService
{
    public function getReports()
    {
        return '';
    }
}
