<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Message\Tncp\Producer;

use App\Entity\Consultation;
use App\Entity\InterfaceSuivi;
use App\Message\Tncp\Producer\MessageSuiviConsultation;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class MessageSuiviConsultationTest extends TestCase
{
    public function testFromModelAndData()
    {
        $interfaceSuivi = new InterfaceSuivi();
        $consultation = new Consultation();
        $consultation->setId(3);
        $interfaceSuivi->setUuid(Uuid::fromString('fea93fdb-89c0-4cd6-9873-a0a7c79acb3e'))
            ->setFlux('TNCP_SORTANTE')
            ->setAction('PUBLICATION_CONSULTATION')
            ->setConsultation($consultation)
            ->setIdObjetDestination('45')
            ->setEtape('etape')
            ;

        $message = MessageSuiviConsultation::normalize($interfaceSuivi, 'url', 'MPE_DEVELOP', 'token');

        $this->assertEquals('fea93fdb-89c0-4cd6-9873-a0a7c79acb3e', $message->getUuid());
        $this->assertEquals('MPE_DEVELOP', $message->getUuidPlateforme());
        $this->assertEquals('TNCP_SORTANTE', $message->getFlux());
    }
}
