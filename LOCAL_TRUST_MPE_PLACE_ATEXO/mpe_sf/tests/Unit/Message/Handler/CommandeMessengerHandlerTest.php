<?php

namespace Tests\Unit\Message\Handler;

use App\Message\CommandeMessenger;
use App\MessageHandler\CommandeMessengerHandler;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Tests\Unit\AtexoTestCase;

class CommandeMessengerHandlerTest extends AtexoTestCase
{
    /**
     * @var ParameterBagInterface
     */
    private ParameterBagInterface $parameterBag;

    /**
     * @var LoggerInterface
     */
    private LoggerInterface $logger;

    /**
     * @var MessageBusInterface|MockObject
     */
    private MockObject|MessageBusInterface $messageBusInterface;

    /**
     * @var CommandeMessengerHandler
     */
    private CommandeMessengerHandler $handler;

    protected function setUp()
    {
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);

        $this->messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->handler = new CommandeMessengerHandler(
            $this->logger,
            $this->parameterBag
        );
    }

    /**
     * @test
     */
    public function willDispatchAllMessagesOnANewFile()
    {

        $this->logger->expects($this->atLeast(1))
            ->method('info');

        $message = new CommandeMessenger('about');

        $handler = $this->handler;
        $result = $handler($message);

        $this->assertEquals(1, $result);
    }
}
