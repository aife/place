<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\OrganismeInputDataTransformer;
use App\Dto\Input\InscritInput;
use App\Dto\Input\OrganismeInput;
use App\Entity\Organisme;
use App\Service\AtexoConfiguration;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class OrganismeTransformerTest extends KernelTestCase
{
    private ValidatorInterface $validator;
    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
    }

    public function testTransform()
    {
        $InputOrganisme = new OrganismeInput();
        $InputOrganisme->denominationOrg = "AgentTechniqueTNR";
        $InputOrganisme->adresse = "rue des petits champs";
        $InputOrganisme->codepostal = "78300";
        $InputOrganisme->pays = "Fr";
        $InputOrganisme->ville = "Poissy";
        $InputOrganisme->email = "orga@gmail.com";
        $InputOrganisme->active = "1";
        $InputOrganisme->sigle = "ci";
        $InputOrganisme->description = "desc";
        $InputOrganisme->idExterne = "0";
        $InputOrganisme->complement = "comp";

        $inscritInputTransformer = new OrganismeInputDataTransformer($this->validator);
        $context = [
            "api-platform" => true,
            "operation_type" => "item",
            "collection_operation_name" => "put",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\OrganismeOutput",
                "name" => "OrganismeOutput",
            ],
            "output" => [
                "class" => "App\Dto\Output\OrganismeOutput",
                "name" => "InscritOutput",
            ],
            "api_allow_update" => true,
            "resource_class" => "App\Entity\Organisme",
            "request_uri" => "/api/v2/referentiels/organismes/min-integration-1",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/referentiels/organismes/min-integration-1",
            "skip_null_values" => true,
        ];
        $organisme = $inscritInputTransformer->transform($InputOrganisme, 'App\Entity\Organisme', $context);
        $ville = $organisme->getVille();
        $this->assertSame('Poissy', $ville);
    }

    public function testSupportsTransformation()
    {
        $organisme = new OrganismeInputDataTransformer($this->validator);

        $this->assertTrue(
            $organisme->supportsTransformation(
                new OrganismeInput(),
                'App\Entity\Organisme',
                ['input' => ['class' => 'App\Dto\Input\OrganismeInput']]
            )
        );
    }
    public function testInitialize()
    {
        $organismeInputDataTransformer = new OrganismeInputDataTransformer($this->validator);
        $organisme = new Organisme();
        $acronyme = "pmin-Max " . mt_rand(25, 188);
        $organisme->setAcronyme($acronyme);
        $organisme->setDenominationOrg('DO raw');
        $organisme->setAdresse('DO raw');
        $organisme->setCp('456906');
        $organisme->setPays('spain');
        $organisme->setVille("Malaga");
        $organisme->setEmail("IOO@home.com");
        $organisme->setSigle("CVIO");
        $organisme->setDescriptionOrg("Desc OP");
        $organisme->setIdExterne("1");
        $organisme->setComplement("myComp");
        $organismeInputDataTransformer->initialize(InscritInput::class, ["object_to_populate" => $organisme]);
        $this->assertSame($organisme->getAcronyme(), $acronyme);
    }
}
