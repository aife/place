<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\EntrepriseInputDataTransformer;
use App\Dto\Input\EntrepriseInput;
use App\Entity\Entreprise;
use App\Service\AtexoConfiguration;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EntrepriseTransformerTest extends KernelTestCase
{
    private ValidatorInterface $validator;
    private UserPasswordEncoderInterface $userPasswordEncoderInterface;
    private AtexoConfiguration $atexoConfiguration;
    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->userPasswordEncoderInterface = $this->createMock(UserPasswordEncoderInterface::class);
        $this->atexoConfiguration = $this->createMock(AtexoConfiguration::class);
    }
    public function testTransform()
    {
        $InputEntreprise = new EntrepriseInput();
        $dateNow = new \DateTime();

        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");

        $InputEntreprise->siren = "791918766";
        $InputEntreprise->telephone = "0623236654";
        $InputEntreprise->siteInternet = "www.entreprise-italie.it";
        $InputEntreprise->idExterne = "0";
        $InputEntreprise->adresse = "Rue 30 Allée Christaine";
        $InputEntreprise->ville = "Poissy";
        $InputEntreprise->codepostal = "78000";
        $InputEntreprise->pays = "France";
        $InputEntreprise->acronymePays = "FR";
        $InputEntreprise->formeJuridique = "20";
        $InputEntreprise->codeAPE = "CA";
        $InputEntreprise->libelleApe = "Restauration traditionnelle";
        $InputEntreprise->capitalSocial = "N.C";
        $InputEntreprise->raisonSociale = "ATEXO";
        $InputEntreprise->dateCreation = $dateCreation ?? $dateNow;
        $InputEntreprise->dateModification = $dateModification ?? $dateNow;

        $entrepriseInputTransformer = new EntrepriseInputDataTransformer($this->validator);
        $context = [
            "api-platform" => true,
            "operation_type" => "collection",
            "collection_operation_name" => "post",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\EntrepriseInput",
                "name" => "EntrepriseInput",
            ],
            "output" => [
                "class" => "App\Dto\Output\EntrepriseOutput",
                "name" => "InscritOutput",
            ],
            "api_allow_update" => false,
            "resource_class" => "App\Entity\Entreprise",
            "request_uri" => "/api/v2/Entreprise",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/entreprises",
            "skip_null_values" => true,
        ];

        $entreprise = $entrepriseInputTransformer->transform($InputEntreprise, 'App\Entity\Entreprise', $context);
        $site = $entreprise->getSiteInternet();

        $this->assertEquals("www.entreprise-italie.it", $site);
    }

    public function testSupportsTransformation()
    {
        $entreprise = new EntrepriseInputDataTransformer($this->validator);

        $this->assertTrue(
            $entreprise->supportsTransformation(
                new EntrepriseInput(),
                'App\Entity\Entreprise',
                ['input' => ['class' => 'App\Dto\Input\EntrepriseInput']]
            )
        );
    }

    /**
     * @return void
     */
    public function testInitialize(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $entrepriseInputTransformer = new EntrepriseInputDataTransformer($validator);

        $dateNow = new \DateTime();
        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");

        $entreprise = new Entreprise();
        $entreprise->setNom("NBN");
        $entreprise->setSiren("67543986");
        $entreprise->setTelephone("098745678");
        $entreprise->setSiteInternet("www.NCY.com");
        $entreprise->setIdExterne("1");
        $entreprise->setAdresse("Center Parck");
        $entreprise->setVille("New York");
        $entreprise->setCodepostal("089988777");
        $entreprise->setPays("United State");
        $entreprise->setAcronymePays("USA");
        $entreprise->setFormejuridique("HAL");
        $entreprise->setCodeape("NBAUSA");
        $entreprise->setLibelleApe("basket");
        $entreprise->setCapitalSocial("N.Y");
        $entreprise->setRaisonSociale("NBN");
        $entreprise->setDateCreation(
            $dateCreation->format('Y-m-d H:m:s') ??
            $dateNow->format('Y-m-d H:m:s')
        );
        $entreprise->setDateModification(
            $dateModification->format('Y-m-d H:m:s') ??
            $dateNow->format('Y-m-d H:m:s')
        );
        $entrepriseInput = $entrepriseInputTransformer->initialize(
            EntrepriseInput::class,
            ["object_to_populate" => $entreprise]
        );
        $this->assertSame($entreprise->getSiren(), $entrepriseInput->siren);
    }
}
