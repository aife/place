<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use App\DataTransformer\Input\OffreInputDataTransformer;
use App\Dto\Input\OffreInput;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Service\Consultation\ConsultationDepotService;
use PHPUnit\Framework\TestCase;

class OffreInputDataTransformerTest extends TestCase
{
    private OffreInputDataTransformer $offreInputDataTransformer;

    protected function setUp(): void
    {
        $this->offreInputDataTransformer = new OffreInputDataTransformer(
            $this->createMock(ConsultationDepotService::class)
        );
    }

    public function testTransformWithDateValueIsEmpty()
    {
        $object = new OffreInput();
        $inscrit = new Inscrit();

        $entreprise = new class extends Entreprise {
            public function getId()
            {
                return 123;
            }
        };

        $inscrit->setEntreprise($entreprise);

        $object->inscrit = $inscrit;
        $object->consultation= new Consultation();

        $mockConsultationDepotService = $this->createMock(ConsultationDepotService::class);
        $mockConsultationDepotService->expects($this->once())
            ->method('initDefaultFieldsOffre')
            ->willReturn(new Offre())
        ;
        $offreInputDataTransformer = new OffreInputDataTransformer($mockConsultationDepotService);

        $response = $offreInputDataTransformer->transform($object, 'App\Entity\Offre', [
            'input' => ['class' => 'App\Dto\Input\OffreInput']
        ]);

        $this->assertEquals(Offre::class, $response::class);
        $this->assertEqualsWithDelta(new \DateTime(), $response->getDateDepot(), 5);
    }

    public function testTransformWithDateValueIsNotEmpty()
    {
        $object = new OffreInput();
        $inscrit = new Inscrit();

        $entreprise = new class extends Entreprise {
            public function getId()
            {
                return 123;
            }
        };

        $inscrit->setEntreprise($entreprise);

        $object->inscrit = $inscrit;
        $object->consultation= new Consultation();
        $object->dateDepot = new \DateTime('2019-01-01');

        $mockConsultationDepotService = $this->createMock(ConsultationDepotService::class);
        $mockConsultationDepotService->expects($this->once())
            ->method('initDefaultFieldsOffre')
            ->willReturn(new Offre())
        ;
        $offreInputDataTransformer = new OffreInputDataTransformer($mockConsultationDepotService);


        $response = $offreInputDataTransformer->transform($object, 'App\Entity\Offre', [
            'input' => ['class' => 'App\Dto\Input\OffreInput']
        ]);

        $this->assertEquals(Offre::class, $response::class);
        $this->assertEquals(new \DateTime('2019-01-01'), $response->getDateDepot());
    }

    public function testSupportsTransformationReturnFalse()
    {
        $response = $this->offreInputDataTransformer->supportsTransformation(new Offre(), '', []);
        $this->assertFalse($response);

        $response = $this->offreInputDataTransformer->supportsTransformation(new OffreInput(), '', []);
        $this->assertFalse($response);

        $response = $this->offreInputDataTransformer->supportsTransformation(new OffreInput(), 'App\Entity\Offre', []);
        $this->assertFalse($response);
    }

    public function testSupportsTransformationReturnTrue()
    {
        $response = $this->offreInputDataTransformer->supportsTransformation(new OffreInput(), 'App\Entity\Offre', [
            'input' => ['class' => 'App\Dto\Input\OffreInput']
        ]);

        $this->assertTrue($response);
    }
}
