<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\UpdateContratTitulaireInputDataTransformer;
use App\Dto\Input\UpdateContratTitulaireInput;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\Agent;
use App\Entity\Consultation\ClausesN1;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TrancheArticle133;
use App\Entity\TypeContrat;
use App\Entity\ValeurReferentiel;
use App\Repository\ContactContratRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Repository\ServiceRepository;
use App\Repository\TrancheArticle133Repository;
use App\Repository\TypeContratRepository;
use App\Repository\ValeurReferentielRepository;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ContratTransformer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

class UpdateContratTitulaireInputDataTransformerTest extends TestCase
{
    public function testSupportsTransformationTrue(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $security = $this->createMock(Security::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $updateContratTitulaireInputDataTransformer = new UpdateContratTitulaireInputDataTransformer(
            $validator,
            $consultationService,
            $clausesService,
            $security,
            $contratTransformer,
            $entityManager,
            $parameterBag
        );

        $context = [
            "input" => [
                "class" => UpdateContratTitulaireInput::class,
                "name" => "UpdateContratTitulaireInput",
            ]
        ];

        self::assertTrue($updateContratTitulaireInputDataTransformer->supportsTransformation(
            [],
            ContratTitulaire::class,
            $context
        ));
    }

    public function testSupportsTransformationFalse(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $security = $this->createMock(Security::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $updateContratTitulaireInputDataTransformer = new UpdateContratTitulaireInputDataTransformer(
            $validator,
            $consultationService,
            $clausesService,
            $security,
            $contratTransformer,
            $entityManager,
            $parameterBag
        );

        $context = [
            "input" => [
                "class" => \stdClass::class,
                "name" => "stdclass",
            ]
        ];

        self::assertFalse($updateContratTitulaireInputDataTransformer->supportsTransformation(
            [],
            \stdClass::class,
            $context
        ));

        self::assertFalse($updateContratTitulaireInputDataTransformer->supportsTransformation(
            new ContratTitulaire(),
            \stdClass::class,
            $context
        ));
    }

    public function testTransform(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $security = $this->createMock(Security::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn(new Agent())
        ;

        $consultationService
            ->expects(self::once())
            ->method('getLieuExecution')
            ->with([75, 92])
            ->willReturn(',,254,258,')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getStatutIdByLabel')
            ->with('STATUT_NUMEROTATION_AUTONOME')
            ->willReturn('2')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getCategorieByLabel')
            ->with('TRAVAUX')
            ->willReturn(1)
        ;

        // Get contrat titulaire input object
        $contratTitulaire = $this->getContratTitulaireInputDataProvider();


        // Get existing contrat titulaire
        $existingContratTitulaire = $this->getExistingContratTitulaire();

        $validator
            ->expects(self::once())
            ->method('validate')
            ->with($contratTitulaire)
        ;

        $updateContratTitulaireInputDataTransformer = new UpdateContratTitulaireInputDataTransformer(
            $validator,
            $consultationService,
            $clausesService,
            $security,
            $contratTransformer,
            $entityManager,
            $parameterBag
        );

        $context = [
            "api-platform" => true,
            "object_to_populate" => $existingContratTitulaire,
            "operation_type" => "collection",
            "collection_operation_name" => "create",
            "api_allow_update" => false,
            "resource_class" => ContratTitulaire::class,
            "iri_only" => false,
            "input" => [
                "class" => UpdateContratTitulaireInput::class,
                "name" => "ContratTitulaireInput",
            ],
            "output" => [
                "class" => ContratTitulaireOutput::class,
                "name" => "ContratTitulaireOutput",
            ],
            "request_uri" => "/api/v2/contrat-titulaires",
            "uri" => "/api/v2/contrat-titulaires",
            "skip_null_values" => true,
            "api_denormalize" => true,
        ];

        $result = $updateContratTitulaireInputDataTransformer->transform(
            $contratTitulaire,
            ContratTitulaire::class,
            $context
        );

        self::assertSame('acronyme', $result->getOrganisme());
        self::assertSame(1, $result->getIdTypeContrat());
        self::assertSame(11, $result->getIdTitulaire());
        self::assertSame(12, $result->getIdTitulaireEtab());
        self::assertSame(41, $result->getIdTrancheBudgetaire());
        self::assertSame(15, $result->getCcagApplicable());
        self::assertSame(17, $result->getIdContact());
        self::assertSame(42, $result->getIdAgent());
        self::assertSame(1, $result->getHorsPassation());
        self::assertSame("After_Update_2016T00019", $result->getNumero());
        self::assertSame("2016MA00013401", $result->getNumeroLong());
        self::assertSame("After_Update_MAMPE2281", $result->getReferenceLibre());
        self::assertSame("After_Update_MAMPE2281", $result->getObjet());
        self::assertSame("FORFAITAIRE", $result->getFormePrix());
        self::assertSame('0', $result->getMarcheDefense());
        self::assertSame(8, $result->getDureeInitialeContrat());
        self::assertSame(",,254,258,", $result->getLieuExecution());
        self::assertSame("After_Update_Intitule Contrat", $result->getIntitule());
        self::assertSame(null, $result->getDateDebutExecution());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDateCreation());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDatePrevueNotification());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDatePrevueMaxFinContrat());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDateAttribution());
        self::assertEquals(new DateTime("2023-09-20"), $result->getDateModification());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDateNotification());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDateFin());
        self::assertEquals(new DateTime("2022-09-20"), $result->getDateMaxFin());
        self::assertNull($result->getIdChapeau());
        self::assertFalse((bool)$result->getChapeau());
        self::assertSame(2, $result->getStatut());
        self::assertSame(1, $result->getCategorie());
        self::assertSame(1234, $result->getMontant());
        self::assertSame("123456789", $result->getCodeCpv1());
        self::assertSame("##", $result->getCodeCpv2());
    }

    private function getContratTitulaireInputDataProvider(): UpdateContratTitulaireInput
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('acronyme');

        $typeContrat = new class extends TypeContrat {
            public function getIdTypeContrat()
            {
                return 1;
            }
        };

        $entreprise = new class extends Entreprise {
            public function getId()
            {
                return 11;
            }
        };

        $etablissement = new class extends Etablissement {
            public function getIdEtablissement()
            {
                return 12;
            }
        };

        $trancheBudgetaire = new class extends TrancheArticle133 {
            public function getId(): int
            {
                return 41;
            }
        };

        $ccagApplicable = new class extends ValeurReferentiel {
            public function getId()
            {
                return 15;
            }
        };

        $idContact = new class extends ContactContrat {
            public function getId()
            {
                return 17;
            }
        };

        $agent = new class extends Agent {
            public function getId()
            {
                return 42;
            }
        };

        $contratTitulaireInput = new UpdateContratTitulaireInput();
        $contratTitulaireInput->organisme = $organisme;
        $contratTitulaireInput->typeContrat = $typeContrat;
        $contratTitulaireInput->idTitulaire = $entreprise;
        $contratTitulaireInput->etablissement =  $etablissement;
        $contratTitulaireInput->trancheBudgetaire = $trancheBudgetaire;
        $contratTitulaireInput->ccagApplicable = $ccagApplicable;
        $contratTitulaireInput->idContact = $idContact;
        $contratTitulaireInput->idCreateur = $agent;
        $contratTitulaireInput->horsPassation = 1;
        $contratTitulaireInput->numero = "After_Update_2016T00019";
        $contratTitulaireInput->numeroLong = "2016MA00013401";
        $contratTitulaireInput->referenceLibre = "After_Update_MAMPE2281";
        $contratTitulaireInput->objet = "After_Update_MAMPE2281";
        $contratTitulaireInput->formePrix = "FORFAITAIRE";
        $contratTitulaireInput->defenseOuSecurite = "0";
        $contratTitulaireInput->dureeMaximaleMarche = 8;
        $contratTitulaireInput->lieuExecutions = [75, 92];
        $contratTitulaireInput->intitule = "After_Update_Intitule Contrat";
        $contratTitulaireInput->dateDebutExecution = null;
        $contratTitulaireInput->dateCreation = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleNotification = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleFinMarche = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleFinMaximaleMarche = new DateTime("2022-09-20");
        $contratTitulaireInput->decisionAttribution = new DateTime("2022-09-20");
        $contratTitulaireInput->dateModification = new DateTime("2023-09-20");
        $contratTitulaireInput->dateNotification = new DateTime("2022-09-20");
        $contratTitulaireInput->dateFin = new DateTime("2022-09-20");
        $contratTitulaireInput->dateMaxFin = new DateTime("2022-09-20");
        $contratTitulaireInput->statut = "STATUT_NUMEROTATION_AUTONOME";
        $contratTitulaireInput->naturePrestation = "TRAVAUX";
        $contratTitulaireInput->idChapeau = null;
        $contratTitulaireInput->montant = 1234;
        $contratTitulaireInput->cpv = [
            "codePrincipal" => "123456789",
            "codeSecondaire1" => "##"
        ];
        $contratTitulaireInput->clauses = [
            "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
            "clausesN2" => [
                [
                    "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                    "clausesN3" => [
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2"
                        ],
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4"
                        ]
                    ]
                ]
            ]
        ];

        return $contratTitulaireInput;
    }

    private function getExistingContratTitulaire(): ContratTitulaire
    {
        $referentielN1 = (new \App\Entity\Referentiel\Consultation\ClausesN1())
            ->setSlug('clauseSociales')
            ->setLabel('Clauses Sociales')
        ;
        $clauseN1 = (new ClausesN1())->setReferentielClauseN1($referentielN1);

        $contratTitulaire = new ContratTitulaire();

        $contratTitulaire->setOrganisme('acronyme');
        $contratTitulaire->setServiceId(789);
        $contratTitulaire->setIdTypeContrat(1);
        $contratTitulaire->setIdTitulaire(11);
        $contratTitulaire->setIdTitulaireEtab(12);
        $contratTitulaire->setIdTrancheBudgetaire(41);
        $contratTitulaire->setCcagApplicable(15);
        $contratTitulaire->setIdContactContrat(17);
        $contratTitulaire->setContact(17);
        $contratTitulaire->setIdAgent(42);
        $contratTitulaire->setHorsPassation(1);
        $contratTitulaire->setNumero("2016T00019");
        $contratTitulaire->setNumeroLong("2016MA00013401");
        $contratTitulaire->setReferenceLibre("MAMPE2281");
        $contratTitulaire->setObjet("MAMPE2281");
        $contratTitulaire->setFormePrix("FORFAITAIRE");
        $contratTitulaire->setMarcheDefense( "0");
        $contratTitulaire->setDureeInitialeContrat(8);
        $contratTitulaire->setLieuExecution(",,254,255,");
        $contratTitulaire->setIntitule("Intitule Contrat");
        $contratTitulaire->setDateDebutExecution(null);
        $contratTitulaire->setDateCreation(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueFinContrat(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueNotification(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueMaxFinContrat(new DateTime("2022-09-20"));
        $contratTitulaire->setDateAttribution(new DateTime("2022-09-20"));
        $contratTitulaire->setDateModification(new DateTime("2022-09-20"));
        $contratTitulaire->setDateNotification(new DateTime("2022-09-20"));
        $contratTitulaire->setDateFin(new DateTime("2022-09-20"));
        $contratTitulaire->setDateMaxFin(new DateTime("2022-09-20"));
        $contratTitulaire->setStatut('4');
        $contratTitulaire->setCategorie(1);
        $contratTitulaire->setIdChapeau(1456);
        $contratTitulaire->setMontant(1234);
        $contratTitulaire->setCodeCpv1("09000000");
        $contratTitulaire->setCodeCpv2("##");
        $contratTitulaire->addClausesN1($clauseN1);

        return $contratTitulaire;
    }

    public function testInitialize(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $security = $this->createMock(Security::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $updateContratTitulaireInputDataTransformer = new UpdateContratTitulaireInputDataTransformer(
            $validator,
            $consultationService,
            $clausesService,
            $security,
            $contratTransformer,
            $entityManager,
            $parameterBag
        );

        $existingContratTitulaire = $this->getExistingContratTitulaire();

        $context = [
            "api-platform" => true,
            "object_to_populate" => $existingContratTitulaire,
            "operation_type" => "collection",
            "collection_operation_name" => "create",
            "resource_class" => ContratTitulaire::class,
        ];

        $service = new Service();
        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository
            ->expects(self::once())
            ->method('find')
            ->with(789)
            ->willReturn($service)
        ;

        $typeContrat = new TypeContrat();
        $typeContratRepository = $this->createMock(TypeContratRepository::class);
        $typeContratRepository
            ->expects(self::once())
            ->method('find')
            ->with(1)
            ->willReturn($typeContrat)
        ;

        $tranche = new TrancheArticle133();
        $trancheRepository = $this->createMock(TrancheArticle133Repository::class);
        $trancheRepository
            ->expects(self::once())
            ->method('find')
            ->with(41)
            ->willReturn($tranche)
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('REFERENTIEL_CCAG_REFERENCE')
            ->willReturn(20)
        ;

        $valeurReferentiel = new ValeurReferentiel();
        $valeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 15,
                    'idReferentiel' => 20
                ]
            )
            ->willReturn($valeurReferentiel)
        ;

        $contactContrat = new ContactContrat();
        $contactContratRepository = $this->createMock(ContactContratRepository::class);
        $contactContratRepository
            ->expects(self::once())
            ->method('find')
            ->with(17)
            ->willReturn($contactContrat)
        ;

        $etablissement = new Etablissement();
        $etablissementRepository = $this->createMock(EtablissementRepository::class);
        $etablissementRepository
            ->expects(self::once())
            ->method('find')
            ->with(12)
            ->willReturn($etablissement)
        ;

        $entreprise = new class extends Entreprise {
            public function getId()
            {
                return 11;
            }
        };
        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('find')
            ->with(11)
            ->willReturn($entreprise)
        ;

        $entityManager
            ->expects(self::exactly(7))
            ->method('getRepository')
            ->withConsecutive(
                [Service::class],
                [TypeContrat::class],
                [TrancheArticle133::class],
                [ValeurReferentiel::class],
                [ContactContrat::class],
                [Etablissement::class],
                [Entreprise::class],
            )
            ->willReturnOnConsecutiveCalls(
                $serviceRepository,
                $typeContratRepository,
                $trancheRepository,
                $valeurReferentielRepository,
                $contactContratRepository,
                $etablissementRepository,
                $entrepriseRepository,
            )
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getArrayLieuExecutionById')
            ->with(",,254,255,")
            ->willReturn([75, 77])
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getStatutLabelById')
            ->with('4')
            ->willReturn('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getCpv')
            ->with($existingContratTitulaire)
            ->willReturn(
                [
                    'codePrincipal' => '09000000',
                    'codeSecondaire1' => '##'
                ]
            )
        ;
        

        $result = $updateContratTitulaireInputDataTransformer->initialize(
                UpdateContratTitulaireInput::class,
            $context
        );

        self::assertSame($result->idService, $service);
        self::assertSame($result->typeContrat, $typeContrat);
        self::assertSame($result->idTitulaire, $entreprise);
        self::assertSame($result->etablissement, $etablissement);
        self::assertSame($result->trancheBudgetaire, $tranche);
        self::assertSame($result->ccagApplicable, $valeurReferentiel);
        self::assertSame($result->idContact, $contactContrat);
        self::assertSame($result->horsPassation, 1);
        self::assertSame($result->numero, "2016T00019");
        self::assertSame($result->numeroLong, "2016MA00013401");
        self::assertSame($result->referenceLibre, "MAMPE2281");
        self::assertSame($result->objet, "MAMPE2281");
        self::assertSame($result->formePrix, "FORFAITAIRE");
        self::assertSame($result->defenseOuSecurite,  "0");
        self::assertSame($result->dureeMaximaleMarche, 8);
        self::assertSame($result->lieuExecutions, [75, 77]);
        self::assertSame($result->intitule, "Intitule Contrat");
        self::assertSame($result->dateDebutExecution, null);
        self::assertEquals($result->dateCreation, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleFinMarche, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleNotification, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleFinMaximaleMarche, new DateTime("2022-09-20"));
        self::assertEquals($result->decisionAttribution, new DateTime("2022-09-20"));
        self::assertEquals($result->dateModification, new DateTime("2022-09-20"));
        self::assertEquals($result->dateNotification, new DateTime("2022-09-20"));
        self::assertEquals($result->dateFin, new DateTime("2022-09-20"));
        self::assertEquals($result->dateMaxFin, new DateTime("2022-09-20"));
        self::assertSame($result->statut, "STATUT_NOTIFICATION_CONTRAT_EFFECTUEE");
        self::assertSame($result->naturePrestation, 'TRAVAUX');
        self::assertSame($result->idChapeau, 1456);
        self::assertSame($result->montant, 1234);
        self::assertSame($result->cpv, [
            'codePrincipal' => '09000000',
            'codeSecondaire1' => '##'
        ]);
    }
}
