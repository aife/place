<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\EtablissementInputDataTransformer;
use App\Dto\Input\EtablissementInput;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use PHPUnit\Framework\TestCase;

class EtablissementTransformerTest extends TestCase
{
    public function testTransform()
    {
        $InputEstablishment = new EtablissementInput();
        $entreprises = new Entreprise();
        $dateNow = new \DateTime();

        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");
        $InputEstablishment->siret = "1288887";
        $InputEstablishment->estSiege = "Atexo";
        $InputEstablishment->codeEtablissement = "8887894544";
        $InputEstablishment->idExterne = 1;
        $InputEstablishment->entreprise = $entreprises;
        $InputEstablishment->adresse = "Rue du Montparnasse, Paris 14";
        $InputEstablishment->codePostal = "75014";
        $InputEstablishment->ville = "Paris";
        $InputEstablishment->pays = "France";
        $InputEstablishment->dateCreation = $dateCreation ?? $dateNow;
        $InputEstablishment->dateModification = $dateModification ?? $dateNow;

        $validator = $this->createMock(ValidatorInterface::class);
        $etablissementInputTransformer = new EtablissementInputDataTransformer($validator);
        $context = [
            "api-platform" => true,
            "operation_type" => "collection",
            "collection_operation_name" => "post",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\EtablissementInput",
                "name" => "EtablissementInput",
            ],
            "output" => [
                "class" => "App\Dto\Output\EtablissementOutput",
                "name" => "EtablissementOutput",
            ],
            "api_allow_update" => false,
            "resource_class" => "App\Entity\Etablissement",
            "request_uri" => "/api/v2/etablissements",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/etablissements",
            "skip_null_values" => true,
        ];
        $etablissement = $etablissementInputTransformer->transform(
            $InputEstablishment,
            'App\Entity\Etablissement',
            $context
        );
        $gestSieg = $etablissement->getEstSiege();
        $this->assertInstanceOf(Etablissement::class, $etablissement);
        $this->assertEquals('Atexo', $gestSieg);
    }
    public function testSupportsTransformation()
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $etablissementInputTransformer = new EtablissementInputDataTransformer($validator);

        $this->assertTrue(
            $etablissementInputTransformer->supportsTransformation(
                new EtablissementInput(),
                'App\Entity\Etablissement',
                ['input' => ['class' => 'App\Dto\Input\EtablissementInput']]
            )
        );
    }
    public function testInitialize()
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $establishmentInputTransformer = new EtablissementInputDataTransformer($validator);
        $establishment = new Etablissement();
        $dateNow = new \DateTime();

        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");
        $establishment->setSiret("1288887");
        $establishment->setEstSiege("Atexo");
        $establishment->setCodeEtablissement("8887894544");
        $establishment->setIdExterne(1);
        $establishment->setIdEntreprise(3);
        $establishment->setEntreprise(new Entreprise());
        $establishment->setAdresse("Rue du Montparnasse, Paris 14");
        $establishment->setCodePostal("75014");
        $establishment->setVille("Paris");
        $establishment->setPays("France");
        $establishment->setDateCreation($dateCreation ?? $dateNow);
        $establishment->setDateModification($dateModification ?? $dateNow);
        $establishmentInput = $establishmentInputTransformer->initialize(
            EtablissementInput::class,
            ["object_to_populate" => $establishment]
        );
        $this->assertSame($establishment->getSiret(), $establishmentInput->siret);
    }
}
