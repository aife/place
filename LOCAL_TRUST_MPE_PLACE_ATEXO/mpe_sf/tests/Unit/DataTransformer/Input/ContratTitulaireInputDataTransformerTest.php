<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\ContratTitulaireInputDataTransformer;
use App\Dto\Input\ContratTitulaireInput;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\Agent;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Organisme;
use App\Entity\TrancheArticle133;
use App\Entity\TypeContrat;
use App\Entity\ValeurReferentiel;
use App\Service\ClausesService;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ContratTransformer;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;

class ContratTitulaireInputDataTransformerTest extends TestCase
{
    public function testSupportsTransformationTrue(): void
    {
        $security = $this->createMock(Security::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $clausesService = $this->createMock(ClausesService::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);

        $contratTitulaireInputDataTransformer = new ContratTitulaireInputDataTransformer(
            $security,
            $validator,
            $clausesService,
            $consultationService,
            $contratTransformer
        );

        $context = [
            "input" => [
                "class" => ContratTitulaireInput::class,
                "name" => "ContratTitulaireInput",
            ]
        ];

        self::assertTrue($contratTitulaireInputDataTransformer->supportsTransformation(
            [],
            ContratTitulaire::class,
            $context
        ));
    }

    public function testSupportsTransformationFalse(): void
    {
        $security = $this->createMock(Security::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $clausesService = $this->createMock(ClausesService::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);

        $contratTitulaireInputDataTransformer = new ContratTitulaireInputDataTransformer(
            $security,
            $validator,
            $clausesService,
            $consultationService,
            $contratTransformer
        );

        $context = [
            "input" => [
                "class" => \stdClass::class,
                "name" => "stdclass",
            ]
        ];

        self::assertFalse($contratTitulaireInputDataTransformer->supportsTransformation(
            [],
            \stdClass::class,
            $context
        ));

        self::assertFalse($contratTitulaireInputDataTransformer->supportsTransformation(
            new ContratTitulaire(),
            \stdClass::class,
            $context
        ));
    }

    public function testTransform(): void
    {
        $security = $this->createMock(Security::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $clausesService = $this->createMock(ClausesService::class);
        $consultationService = $this->createMock(ConsultationService::class);
        $contratTransformer = $this->createMock(ContratTransformer::class);

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn(new Agent())
        ;

        $consultationService
            ->expects(self::once())
            ->method('getLieuExecution')
            ->with([75, 92])
            ->willReturn(',,254,258,')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getStatutIdByLabel')
            ->with('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
            ->willReturn('4')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getCategorieByLabel')
            ->with('TRAVAUX')
            ->willReturn('1')
        ;

        // Get contrat titulaire object
        $contratTitulaire = $this->getContratTitulaireInputDataProvider();

        $validator
            ->expects(self::once())
            ->method('validate')
            ->with($contratTitulaire)
        ;

        $contratTitulaireInputDataTransformer = new ContratTitulaireInputDataTransformer(
            $security,
            $validator,
            $clausesService,
            $consultationService,
            $contratTransformer
        );

        $context = [
            "api-platform" => true,
            "operation_type" => "collection",
            "collection_operation_name" => "create",
            "api_allow_update" => false,
            "resource_class" => ContratTitulaire::class,
            "iri_only" => false,
            "input" => [
                "class" => ContratTitulaireInput::class,
                "name" => "ContratTitulaireInput",
            ],
            "output" => [
                "class" => ContratTitulaireOutput::class,
                "name" => "ContratTitulaireOutput",
            ],
            "request_uri" => "/api/v2/contrat-titulaires",
            "uri" => "/api/v2/contrat-titulaires",
            "skip_null_values" => true,
            "api_denormalize" => true,
        ];

        $result = $contratTitulaireInputDataTransformer->transform(
            $contratTitulaire,
            ContratTitulaire::class,
            $context
        );

        self::assertSame($result->getOrganisme(), 'acronyme');
        self::assertSame($result->getIdTypeContrat(), 1);
        self::assertSame($result->getIdTitulaire(), 11);
        self::assertSame($result->getIdTitulaireEtab(), 12);
        self::assertSame($result->getIdTrancheBudgetaire(), 41);
        self::assertSame($result->getCcagApplicable(), 15);
        self::assertSame($result->getIdContact(), 17);
        self::assertSame($result->getIdAgent(), 42);
        self::assertSame($result->getHorsPassation(), 1);
        self::assertSame($result->getNumero(), "2016T00019");
        self::assertSame($result->getNumeroLong(), "2016MA00013401");
        self::assertSame($result->getReferenceLibre(), "MAMPE2281");
        self::assertSame($result->getObjet(), "MAMPE2281");
        self::assertSame($result->getFormePrix(), "FORFAITAIRE");
        self::assertSame($result->getMarcheDefense(), '0');
        self::assertSame($result->getDureeInitialeContrat(), 8);
        self::assertSame($result->getLieuExecution(), ",,254,258,"); // TODO
        self::assertSame($result->getIntitule(), "Intitule Contrat");
        self::assertSame($result->getDateDebutExecution(), null);
        self::assertEquals($result->getDateCreation(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDatePrevueNotification(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDatePrevueMaxFinContrat(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDateAttribution(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDateModification(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDateNotification(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDateFin(), new DateTime("2022-09-20"));
        self::assertEquals($result->getDateMaxFin(), new DateTime("2022-09-20"));
        self::assertEquals($result->getStatut(), '4');
        self::assertSame($result->getCategorie(), 1);
        self::assertTrue((bool)$result->getChapeau());
        self::assertSame($result->getIdChapeau(), 1456);
        self::assertSame($result->getMontant(), 1234);
        self::assertSame($result->getCodeCpv1(), "09000000");
        self::assertSame($result->getCodeCpv2(), "##");
    }

    private function getContratTitulaireInputDataProvider(): ContratTitulaireInput
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('acronyme');

        $typeContrat = new class extends TypeContrat {
            public function getIdTypeContrat()
            {
                return 1;
            }
        };

        $entreprise = new class extends Entreprise {
            public function getId()
            {
                return 11;
            }
        };

        $etablissement = new class extends Etablissement {
            public function getIdEtablissement()
            {
                return 12;
            }
        };

        $trancheBudgetaire = new class extends TrancheArticle133 {
            public function getId(): int
            {
                return 41;
            }
        };

        $ccagApplicable = new class extends ValeurReferentiel {
            public function getId()
            {
                return 15;
            }
        };

        $idContact = new class extends ContactContrat {
            public function getId()
            {
                return 17;
            }
        };

        $agent = new class extends Agent {
            public function getId()
            {
                return 42;
            }
        };

        $contratTitulaireInput = new ContratTitulaireInput();
        $contratTitulaireInput->organisme = $organisme;
        $contratTitulaireInput->typeContrat = $typeContrat;
        $contratTitulaireInput->idTitulaire = $entreprise;
        $contratTitulaireInput->etablissement =  $etablissement;
        $contratTitulaireInput->trancheBudgetaire = $trancheBudgetaire;
        $contratTitulaireInput->ccagApplicable = $ccagApplicable;
        $contratTitulaireInput->idContact = $idContact;
        $contratTitulaireInput->idCreateur = $agent;
        $contratTitulaireInput->horsPassation = 1;
        $contratTitulaireInput->numero = "2016T00019";
        $contratTitulaireInput->numeroLong = "2016MA00013401";
        $contratTitulaireInput->referenceLibre = "MAMPE2281";
        $contratTitulaireInput->objet = "MAMPE2281";
        $contratTitulaireInput->formePrix = "FORFAITAIRE";
        $contratTitulaireInput->defenseOuSecurite = "0";
        $contratTitulaireInput->dureeMaximaleMarche = 8;
        $contratTitulaireInput->lieuExecutions = [75, 92];
        $contratTitulaireInput->intitule = "Intitule Contrat";
        $contratTitulaireInput->dateDebutExecution = null;
        $contratTitulaireInput->dateCreation = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleNotification = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleFinMarche = new DateTime("2022-09-20");
        $contratTitulaireInput->datePrevisionnelleFinMaximaleMarche = new DateTime("2022-09-20");
        $contratTitulaireInput->decisionAttribution = new DateTime("2022-09-20");
        $contratTitulaireInput->dateModification = new DateTime("2022-09-20");
        $contratTitulaireInput->dateNotification = new DateTime("2022-09-20");
        $contratTitulaireInput->dateFin = new DateTime("2022-09-20");
        $contratTitulaireInput->dateMaxFin = new DateTime("2022-09-20");
        $contratTitulaireInput->statut = "STATUT_NOTIFICATION_CONTRAT_EFFECTUEE";
        $contratTitulaireInput->naturePrestation = "TRAVAUX";
        $contratTitulaireInput->idChapeau = 1456;
        $contratTitulaireInput->montant = 1234;
        $contratTitulaireInput->cpv = [
            "codePrincipal" => "09000000",
            "codeSecondaire1" => "##"
        ];
        $contratTitulaireInput->clauses = [
            "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
            "clausesN2" => [
                [
                    "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                    "clausesN3" => [
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2"
                        ],
                        [
                            "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4"
                        ]
                    ]
                ]
            ]
        ];

        return $contratTitulaireInput;
    }
}
