<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\ServiceInputDataTransformer;
use App\Dto\Input\ServiceInput;
use App\Entity\Organisme;
use App\Entity\Service;
use Monolog\Test\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ServiceTransformerTest extends TestCase
{
    private ValidatorInterface $validator;


    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
    }

    public function testTransform()
    {
        $InputService = new ServiceInput();

        $InputService->idExterne = 0;
        $InputService->sigle = "56";
        $InputService->siren = "8845678";
        $InputService->email = "mnma@culture.gouv.fr";
        $InputService->siret = "884567899";
        $InputService->libelle = "NON";
        $InputService->adresse = "Boulevard Saint Michel";
        $InputService->codepostal = "75005";
        $InputService->ville = "Paris";
        $InputService->idParent = "567781321";
        $InputService->formeJuridique = "BA";
        $InputService->formeJuridique = "O787";
        $InputService->complement = "99";
        $InputService->organisme = new Organisme();

        $serviceInputTransformer = new ServiceInputDataTransformer($this->validator);
        $context = [
            "api-platform" => true,
            "operation_type" => "item",
            "collection_operation_name" => "put",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\ServiceInput",
                "name" => "ServiceInput",
            ],
            "output" => [
                "class" => "App\Dto\Output\ServiceOutput",
                "name" => "ServiceOutput",
            ],
            "api_allow_update" => true,
            "resource_class" => "App\Entity\Service",
            "request_uri" => "/api/v2/referentiels/services/2908",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/referentiels/services/2908",
            "skip_null_values" => true,
        ];


        $serviceI = $serviceInputTransformer->transform($InputService, 'App\Entity\Service', $context);
        $ville = $serviceI->getVille();
        $organisme = $serviceI->getOrganisme();
        $this->assertSame('Paris', $ville);
        $this->assertInstanceOf(Organisme::class, $organisme);
    }

    public function testSupportsTransformation()
    {
        $service = new ServiceInputDataTransformer($this->validator);

        $this->assertTrue(
            $service->supportsTransformation(
                new ServiceInput(),
                'App\Entity\Service',
                ['input' => ['class' => 'App\Dto\Input\ServiceInput']]
            )
        );
    }

    /**
     * @throws \Exception
     */
    public function testInitialize()
    {
        $serviceInputDataTransformer = new ServiceInputDataTransformer($this->validator);
        $service = new Service();


        $service->setSigle("GH24");
        $service->setSiren('8845678');
        $service->setCp('456906');
        $service->setPays('spain');
        $service->setVille("Malaga");
        $service->setMail("IOO@home.com");
        $service->setIdParent("CVIO");
        $service->setOrganisme(new Organisme());
        $service->setLibelle("1");
        $service->setIdExterne("1");
        $service->setFormeJuridique("1");
        $service->setFormeJuridiqueCode("BA18");
        $service->setComplement("99");

        $organismeInput = $serviceInputDataTransformer->initialize(
            ServiceInput::class,
            ["object_to_populate" => $service]
        );
        $this->assertSame($service->getSiren(), $organismeInput->siren);
        $this->assertSame($service->getComplement(), $organismeInput->complement);
    }
}
