<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\EnveloppeInputTransformer;
use App\Dto\Input\EnveloppeInput;
use App\Dto\Input\FichierEnveloppeInput;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\MediaUuid;
use App\Entity\Offre;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EnveloppeInputTransformerTest extends TestCase
{
    public function testTransform()
    {
        $input = new EnveloppeInput();
        $input->offre = new Offre();
        $input->offre->setConsultation((new Consultation())->setAcronymeOrg('pmi-min-1'));
        $input->numeroLot = 2;
        $input->type = 'OFFRE';

        $inputFichier = new FichierEnveloppeInput();
        $inputFichier->idFichier = (new MediaUuid());
        $inputFichier->idFichier->setMedia((new BloborganismeFile())
            ->setChemin('./tests/Unit/DataTransformer/Input/')
            ->setName('mock-file.txt')
            ->setHash('b7c0e954b9090978e825a8215eed02a5d6d36d36'));
        $inputFichier->typeFichier = 'PRI';
        $inputFichier->idSignature = (new MediaUuid())->setMedia((new BloborganismeFile())
            ->setChemin('./tests/Unit/DataTransformer/Input/')
            ->setName('mock-file.txt'));

        $input->fichiersEtSignature = [$inputFichier];

        $validator = $this->createMock(ValidatorInterface::class);
        $parametersBag = $this->createMock(ParameterBagInterface::class);
        $parametersBag->method('get')
            ->will(
                $this->returnValueMap([
                    ['TYPE_ENV_CANDIDATURE', 1],
                    ['TYPE_ENV_OFFRE', 2],
                    ['TYPE_ENV_ANONYMAT', 3],
                    ['TYPE_ENV_OFFRE_TECHNIQUE', 4],
                ])
            );

        $transformer = new EnveloppeInputTransformer($parametersBag, $validator);

        $enveloppe = $transformer->transform($input, 'App\Entity\Enveloppe', []);

        $fichierSignature = $enveloppe->getFichierEnveloppes()->first();
        $fichierDepot = $enveloppe->getFichierEnveloppes()->last();

        $this->assertInstanceOf(Enveloppe::class, $enveloppe);
        $this->assertCount(2, $enveloppe->getFichierEnveloppes());
        $this->assertInstanceOf(EnveloppeFichier::class, $enveloppe->getFichierEnveloppes()->first());

        $this->assertEquals(2, $enveloppe->getTypeEnv());
        $this->assertEquals(2, $enveloppe->getSousPli());

        $this->assertSame('SIG', $fichierSignature->getTypeFichier());
        $this->assertEquals(99, $fichierSignature->getTypePiece());

        $this->assertSame('PRI', $fichierDepot->getTypeFichier());
        $this->assertEquals(3, $fichierDepot->getTypePiece());
    }

    public function testSupportsTransformation()
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $parametersBag = $this->createMock(ParameterBagInterface::class);
        $transformer = new EnveloppeInputTransformer($parametersBag, $validator);

        $this->assertFalse(
            $transformer->supportsTransformation(
                new Enveloppe(),
                'App\Entity\Enveloppe',
                ['input' => ['class' => 'App\Dto\Input\EnveloppeInput']]
            )
        );
        $this->assertTrue(
            $transformer->supportsTransformation(
                new EnveloppeInput(),
                'App\Entity\Enveloppe',
                ['input' => ['class' => 'App\Dto\Input\EnveloppeInput']]
            )
        );
    }
}
