<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\InscritInputDataTransformer;
use App\Dto\Input\InscritInput;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class InscritTransformerTest extends KernelTestCase
{
    private ValidatorInterface $validator;
    private UserPasswordHasher $userPasswordHasherInterface;

    private AtexoConfiguration $atexoConfiguration;

    /**
     * @var array|float|int
     */
    private array|float|int $idArrayEntreprise;

    /**
     * @var int|float|mixed|string
     */

    private int $nbEntreprise;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->userPasswordHasherInterface = $this->createMock(UserPasswordHasher::class);
        $this->atexoConfiguration = $this->createMock(AtexoConfiguration::class);
    }

    public function testTransform()
    {
        $InputInscrit = new InscritInput();
        $dateNow = new \DateTime();

        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");
        $InputInscrit->prenom = "20";
        $InputInscrit->login = "A2Z2Z";
        $InputInscrit->adresse = "Rue des petits champs,paris";
        $InputInscrit->codepostal = "78300";
        $InputInscrit->ville = "Poissy";
        $InputInscrit->pays = "France";
        $InputInscrit->telephone = "0623236688";
        $InputInscrit->siret = "888388";
        $InputInscrit->dateCreation = $dateCreation ?? $dateNow;
        $InputInscrit->dateModification = $dateModification ?? $dateNow;
        $InputInscrit->etablissement = new Etablissement();
        $InputInscrit->inscritAnnuaireDefense = "0";
        $InputInscrit->idExterne = "0";
        $InputInscrit->entreprise = new Entreprise();
        $InputInscrit->mdp = "5458";
        $InputInscrit->email = "bbn@gmail.com";
        $InputInscrit->bloque = "1";
        $inscritInputTransformer = new InscritInputDataTransformer(
            $this->validator,
            $this->userPasswordHasherInterface,
            $this->atexoConfiguration
        );
        $context = [
            "api-platform" => true,
            "operation_type" => "collection",
            "collection_operation_name" => "post",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\InscritInput",
                "name" => "InscritInput",
            ],
            "output" => [
                "class" => "App\Dto\Output\InscritOutput",
                "name" => "InscritOutput",
            ],
            "api_allow_update" => false,
            "resource_class" => "App\Entity\Inscrit",
            "request_uri" => "/api/v2/inscrits",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/inscrits",
            "skip_null_values" => true,
        ];

        $this->userPasswordHasherInterface
            ->expects($this->once())
            ->method('hashPassword')
            ->willReturn('hashedPassword')
        ;

        $inscrit = $inscritInputTransformer->transform($InputInscrit, 'App\Entity\Inscrit', $context);
        $gestSieg = $inscrit->getEmail();
        $etablissement = $inscrit->getEtablissement();

        $this->assertInstanceOf(Inscrit::class, $inscrit);
        $this->assertInstanceOf(Etablissement::class, $etablissement);
        $this->assertSame('bbn@gmail.com', $gestSieg);
    }
    public function testSupportsTransformation()
    {
        $inscrit = new InscritInputDataTransformer(
            $this->validator,
            $this->userPasswordHasherInterface,
            $this->atexoConfiguration
        );
        $this->assertTrue(
            $inscrit->supportsTransformation(
                new InscritInput(),
                'App\Entity\Inscrit',
                ['input' => ['class' => 'App\Dto\Input\InscritInput']]
            )
        );
        $this->assertFalse(
            $inscrit->supportsTransformation(
                new InscritInput(),
                'App\Entity\Agent',
                ['input' => ['class' => 'App\Dto\Input\InscritInput']]
            )
        );
    }
    public function testInitialize()
    {
        $inscritInputDataTransformer = new InscritInputDataTransformer(
            $this->validator,
            $this->userPasswordHasherInterface,
            $this->atexoConfiguration
        );
        $inscrit = new Inscrit();
        $dateNow = new \DateTime();

        $idInscrit = 16157;
        $dateCreation = new \DateTime("2023-01-20T11:32:24.307Z");
        $dateModification = new \DateTime("2023-01-20T11:32:24.307Z");
        $inscrit->setId($idInscrit);
        $inscrit->setPrenom("1288887");
        $inscrit->setNom("Atexo");
        $inscrit->setLogin("8887894544");
        $inscrit->setAdresse(1);
        $inscrit->setCodepostal("75008");
        $inscrit->setPays("France");
        $inscrit->setTelephone("0655442323");
        $inscrit->setBloque("1");
        $inscrit->setSiret("11111");
        $inscrit->setDateCreation(
            $dateCreation->format('Y-m-d H:m:s') ??
            $dateNow->format('Y-m-d H:m:s')
        );
        $inscrit->setDateModification(
            $dateModification->format('Y-m-d H:m:s') ??
            $dateNow->format('Y-m-d H:m:s')
        );
        $inscrit->setInscritAnnuaireDefense("22");
        $inscrit->setIdExterne("0");
        $inscrit->setEntreprise(new Entreprise());
        $inscrit->setEtablissement(new Etablissement());
        $inscrit->setMdp('hashedPassword');
        $inscrit->setEmail("bo@gmailcom");
        $inscritInput = $inscritInputDataTransformer->initialize(
            InscritInput::class,
            ["object_to_populate" => $inscrit]
        );
        $this->assertSame($inscrit->getSiret(), $inscritInput->siret);
        $this->assertSame($inscrit->getId(), $idInscrit);
        $this->assertInstanceOf(Entreprise::class, $inscrit->getEntreprise());
    }
}
