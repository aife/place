<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\AgentInputDataTransformer;
use App\Dto\Input\AgentInput;
use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Service\Agent\Habilitation;
use App\Service\Consultation\ConsultationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AgentTransformerTest extends KernelTestCase
{
    private Habilitation $habilitationService;
    private EntityManagerInterface $em;
    private UserPasswordHasherInterface $encoder;
    private ValidatorInterface $validator;
    private ConsultationService $consultationService;

    protected function setUp(): void
    {
        $this->habilitationService = $this->createMock(Habilitation::class);
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->encoder = $this->createMock(UserPasswordHasherInterface::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->consultationService = $this->createMock(ConsultationService::class);
    }
    public function testTransform()
    {
        $InputAgent = new AgentInput();
        $randomLogin = str_shuffle("ROSE");
        $InputAgent->login = $randomLogin;
        $InputAgent->password = "8976";
        $InputAgent->email = "bbo@gmail.com";
        $InputAgent->nom = "Eng";
        $InputAgent->prenom = "Edf";
        $InputAgent->organisme = new Organisme();
        $InputAgent->idExterne = 1;
        $InputAgent->telephone = "0677677788";
        $InputAgent->fax = "0677677788";
        $InputAgent->service = new Service();
        $InputAgent->lieuExecution = [75, 92];
        $agentInputTransformer = new AgentInputDataTransformer(
            $this->encoder,
            $this->validator,
            $this->consultationService
        );
        $context = [
            "api-platform" => true,
            "operation_type" => "item",
            "collection_operation_name" => "patch",
            "iri_only" => false,
            "input" => [
                "class" => "App\Dto\Input\AgentInput",
                "name" => "AgentInput",
            ],
            "output" => [
                "class" => "App\Dto\Output\AgentOutput",
                "name" => "AgentOutput",
            ],
            "api_allow_update" => true,
            "deep_object_to_populate" => true,
            "resource_class" => "App\Entity\Agent",
            "request_uri" => "/api/v2/agents/11016",
            "uri" => "https://mpe-docker.local-trust.com/api/v2/agents/11016",
            "skip_null_values" => true,
        ];
        $agents = $agentInputTransformer->transform($InputAgent, 'App\Entity\Agent', $context);
        $this->assertSame('bbo@gmail.com', $agents->getEmail());
        $this->assertInstanceOf(Service::class, $agents->getService());
    }
}
