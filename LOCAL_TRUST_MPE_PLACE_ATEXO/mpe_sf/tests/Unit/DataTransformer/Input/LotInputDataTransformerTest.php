<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Input;

use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata;
use ApiPlatform\Core\Serializer\AbstractItemNormalizer;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\DataTransformer\Input\LotInputDataTransformer;
use App\Dto\Input\LotInput;
use App\Entity\Agent;
use App\Entity\CategorieConsultation;
use App\Entity\Consultation;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Service\ClausesService;
use App\Service\LotService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;

class LotInputDataTransformerTest extends TestCase
{
    private LotInputDataTransformer $lotInputDataTransformer;

    protected function setUp(): void
    {
        $this->lotInputDataTransformer = new LotInputDataTransformer(
            $this->createMock(Security::class),
            $this->createMock(ValidatorInterface::class),
            $this->createMock(LotService::class),
            $this->createMock(ClausesService::class),
            $this->createMock(ResourceMetadataFactoryInterface::class)
        );
    }

    public function testInitializeCreation(): void
    {
        $lotInput = $this->lotInputDataTransformer->initialize(LotInput::class);

        $this->assertNull($lotInput->intitule);
        $this->assertNull($lotInput->numero);
    }

    public function testInitializeModification(): void
    {
        $lot = new Lot();
        $lot->setLot(1);
        $lot->setDescription('desc');
        $lot->setDescriptionDetail('desc detail');
        $lot->setCategorie(new CategorieConsultation());
        $lot->setCodeCpv1('cpv1');
        $lot->setCodeCpv2('#cpv21#cpv22#cpv23');

        $lotInput = $this->lotInputDataTransformer->initialize(
            LotInput::class,
            [AbstractItemNormalizer::OBJECT_TO_POPULATE => $lot]
        );

        $this->assertSame($lot->getLot(), $lotInput->numero);
        $this->assertSame($lot->getDescription(), $lotInput->intitule);
        $this->assertSame($lot->getDescriptionDetail(), $lotInput->descriptionSuccinte);
        $this->assertSame($lot->getCategorie(), $lotInput->naturePrestation);
        $this->assertSame($lot->getCodeCpv1(), $lotInput->codeCpvPrincipal);
        $this->assertSame('cpv21', $lotInput->codeCpvSecondaire1);
        $this->assertSame('cpv22', $lotInput->codeCpvSecondaire2);
        $this->assertSame('cpv23', $lotInput->codeCpvSecondaire3);
        $this->assertSame($lot->getClausesN1(), $lotInput->clauses);
    }

    public function testTransformCreation(): void
    {
        $security = $this->createMock(Security::class);
        $security->expects($this->once())->method('getUser')->willReturn(new Agent());

        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->once())->method('validate');

        $lotService = $this->createMock(LotService::class);
        $lotService->expects($this->once())->method('getConcatenatedCodeCpv')->willReturn('#cpv21#cpv22#cpv23');

        $clausesService = $this->createMock(ClausesService::class);
        $clausesService->expects($this->once())->method('updateClauses');

        $rmfi = $this->createMock(ResourceMetadataFactoryInterface::class);
        $rmfi->expects($this->once())->method('create')->willReturn(new ResourceMetadata());

        $lotInputDataTransformer = new LotInputDataTransformer(
            $security,
            $validator,
            $lotService,
            $clausesService,
            $rmfi
        );

        $lotInput = new LotInput();
        $lotInput->numero = 1;
        $lotInput->intitule = 'desc';
        $lotInput->descriptionSuccinte = 'desc detail';
        $lotInput->naturePrestation = new CategorieConsultation();
        $lotInput->codeCpvPrincipal = 'cpv1';
        $lotInput->codeCpvSecondaire1 = 'cpv21';
        $lotInput->codeCpvSecondaire2 = 'cpv22';
        $lotInput->codeCpvSecondaire3 = 'cpv23';
        $lotInput->consultation = new Consultation();
        $organisme = new Organisme();
        $organisme->setAcronyme('acro');
        $lotInput->organisme = $organisme;
        $lotInput->clauses = [];

        $lot = $lotInputDataTransformer->transform(
            $lotInput,
            Lot::class,
            ['resource_class' => Lot::class]
        );

        $this->assertSame($lotInput->numero, $lot->getLot());
        $this->assertSame($lotInput->intitule, $lot->getDescription());
        $this->assertSame($lotInput->descriptionSuccinte, $lot->getDescriptionDetail());
        $this->assertSame($lotInput->naturePrestation, $lot->getCategorie());
        $this->assertSame($lotInput->codeCpvPrincipal, $lot->getCodeCpv1());
        $this->assertSame('#cpv21#cpv22#cpv23', $lot->getCodeCpv2());
        $this->assertSame($lotInput->consultation, $lot->getConsultation());
        $this->assertSame('acro', $lot->getOrganisme());
    }

    public function testTransformModification(): void
    {
        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->once())->method('validate');

        $lotService = $this->createMock(LotService::class);
        $lotService->expects($this->once())->method('getConcatenatedCodeCpv')->willReturn('#cpv21#cpv22#cpv23');

        $rmfi = $this->createMock(ResourceMetadataFactoryInterface::class);
        $rmfi->expects($this->once())->method('create')->willReturn(new ResourceMetadata());

        $lotInputDataTransformer = new LotInputDataTransformer(
            $this->createMock(Security::class),
            $validator,
            $lotService,
            $this->createMock(ClausesService::class),
            $rmfi
        );

        $lotInput = new LotInput();
        $lotInput->numero = 1;
        $lotInput->intitule = 'desc';
        $lotInput->descriptionSuccinte = 'desc detail';
        $lotInput->naturePrestation = new CategorieConsultation();
        $lotInput->codeCpvPrincipal = 'cpv1';
        $lotInput->codeCpvSecondaire1 = 'cpv21';
        $lotInput->codeCpvSecondaire2 = 'cpv22';
        $lotInput->codeCpvSecondaire3 = 'cpv23';

        $lot = $lotInputDataTransformer->transform(
            $lotInput,
            Lot::class,
            [AbstractItemNormalizer::OBJECT_TO_POPULATE => new Lot(), 'resource_class' => Lot::class]
        );

        $this->assertSame($lotInput->numero, $lot->getLot());
        $this->assertSame($lotInput->intitule, $lot->getDescription());
        $this->assertSame($lotInput->descriptionSuccinte, $lot->getDescriptionDetail());
        $this->assertSame($lotInput->naturePrestation, $lot->getCategorie());
        $this->assertSame($lotInput->codeCpvPrincipal, $lot->getCodeCpv1());
        $this->assertSame('#cpv21#cpv22#cpv23', $lot->getCodeCpv2());
    }

    public function testSupportsTransformation(): void
    {
        $response = $this->lotInputDataTransformer->supportsTransformation(new Lot(), '', []);
        $this->assertFalse($response);

        $response = $this->lotInputDataTransformer->supportsTransformation(new LotInput(), '', []);
        $this->assertFalse($response);

        $response = $this->lotInputDataTransformer->supportsTransformation(new LotInput(), Lot::class, []);
        $this->assertFalse($response);

        $response = $this->lotInputDataTransformer->supportsTransformation(
            new LotInput(),
            Lot::class,
            ['input' => ['class' => LotInput::class]]
        );
        $this->assertTrue($response);
    }
}
