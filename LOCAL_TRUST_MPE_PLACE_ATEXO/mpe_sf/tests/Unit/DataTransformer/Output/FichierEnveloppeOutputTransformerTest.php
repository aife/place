<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\EnveloppeOutputTransformer;
use App\DataTransformer\Output\FichierEnveloppeOutputTransformer;
use App\Dto\Output\EnveloppeOutput;
use App\Dto\Output\FichierEnveloppeOutput;
use App\Entity\BloborganismeFile;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class FichierEnveloppeOutputTransformerTest extends TestCase
{
    public function testTransform()
    {
        $iriService = $this->createMock(IriConverterInterface::class);
        $enveloppeFichier = new EnveloppeFichier();
        $enveloppeFichier->setBlob(new BloborganismeFile());
        $enveloppeFichier->setTypeFichier('SIG');
        $mockEm = $this->createMock(EntityManagerInterface::class);
        $transformer = new FichierEnveloppeOutputTransformer($iriService, $mockEm);
        $output = $transformer->transform($enveloppeFichier, 'App\Dto\Output\FichierEnveloppeOutput', []);

        $this->assertInstanceOf(FichierEnveloppeOutput::class, $output);
        $this->assertEquals('SIG', $output->typeFichier);
    }

    public function testSupportsTransformation()
    {
        $mockEm = $this->createMock(EntityManagerInterface::class);
        $iriService = $this->createMock(IriConverterInterface::class);
        $transformer = new FichierEnveloppeOutputTransformer($iriService, $mockEm);

        $this->assertTrue($transformer->supportsTransformation(
            new EnveloppeFichier(),
            'App\Dto\Output\FichierEnveloppeOutput',
            []
        ));
    }
}
