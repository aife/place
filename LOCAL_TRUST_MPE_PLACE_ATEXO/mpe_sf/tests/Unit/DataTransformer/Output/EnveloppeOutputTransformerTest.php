<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\EnveloppeOutputTransformer;
use App\DataTransformer\Output\FichierEnveloppeOutputTransformer;
use App\Dto\Output\EnveloppeOutput;
use App\Dto\Output\FichierEnveloppeOutput;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use PHPUnit\Framework\TestCase;

class EnveloppeOutputTransformerTest extends TestCase
{
    public function testTransform()
    {
        $iriService = $this->createMock(IriConverterInterface::class);
        $enveloppe = new Enveloppe();
        $enveloppe->setTypeEnv(3);
        $enveloppe->addFichierEnveloppe(new EnveloppeFichier());
        $enveloppeFichierTransformer = $this->createMock(FichierEnveloppeOutputTransformer::class);
        $transformer = new EnveloppeOutputTransformer($iriService, $enveloppeFichierTransformer);
        $output = $transformer->transform($enveloppe, 'App\Dto\Output\EnveloppeOutput', []);

        $this->assertInstanceOf(EnveloppeOutput::class, $output);
        $this->assertEquals('ANONYMAT', $output->type);
        $this->assertCount(1, $output->fichiersEtSignature);
    }

    public function testSupportsTransformation()
    {
        $enveloppeFichierTransformer = $this->createMock(FichierEnveloppeOutputTransformer::class);
        $iriService = $this->createMock(IriConverterInterface::class);
        $transformer = new EnveloppeOutputTransformer($iriService, $enveloppeFichierTransformer);

        $this->assertTrue($transformer->supportsTransformation(
            new Enveloppe(),
            'App\Dto\Output\EnveloppeOutput',
            []
        ));
    }
}
