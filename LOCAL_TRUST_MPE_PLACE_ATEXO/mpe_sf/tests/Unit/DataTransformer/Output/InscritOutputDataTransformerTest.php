<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\InscritOutputDataTransformer;
use App\Dto\Input\ContratTitulaireInput;
use App\Dto\Output\InscritOutput;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use App\Service\Inscrit\InscritService;
use DateTime;
use PHPUnit\Framework\TestCase;

class InscritOutputDataTransformerTest extends TestCase
{
    public function testSupportsTransformation(): void
    {
        $configuration = $this->createMock(AtexoConfiguration::class);
        $inscritService = $this->createMock(InscritService::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);

        $inscritOutputDataTransformer = new InscritOutputDataTransformer(
            $configuration,
            $inscritService,
            $iriConverter
        );

        $context = [
            "input" => [
                "class" => InscritOutput::class,
                "name" => "InscritOutput",
            ]
        ];

        self::assertTrue($inscritOutputDataTransformer->supportsTransformation(
            new Inscrit(),
            InscritOutput::class,
            $context
        ));

        self::assertFalse($inscritOutputDataTransformer->supportsTransformation(
            new Inscrit(),
            ContratTitulaireInput::class,
            $context
        ));

        self::assertFalse($inscritOutputDataTransformer->supportsTransformation(
            new \stdClass(),
            ContratTitulaireInput::class,
            $context
        ));
    }

    public function testTransformWithRgpdActif(): void
    {
        $configuration = $this->createMock(AtexoConfiguration::class);
        $inscritService = $this->createMock(InscritService::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);

        $entreprise = new Entreprise();
        $entreprise->setSiren('siren');
        $etablissement = new Etablissement();
        $etablissement->setCodeEtablissement('code');

        // Get contrat titulaire object
        $inscrit = $this->getInscritProvider();
        $inscrit
            ->setEtablissement($etablissement)
            ->setEntreprise($entreprise)
        ;

        $context = [
            "input" => [
                "class" => InscritOutput::class,
                "name" => "InscritOutput",
            ]
        ];

        $configurationPlateforme = new ConfigurationPlateforme();
        $configurationPlateforme->setRecueilConsentementRgpd(true);

        $iriConverter
            ->expects(self::exactly(2))
            ->method('getIriFromItem')
            ->withConsecutive(
                [$etablissement],
                [$entreprise]
            )
            ->willReturnOnConsecutiveCalls(
                '/api/v2/etablisssements/465',
                '/api/v2/entreprises/123',
            )
        ;

        $configuration
            ->expects(self::once())
            ->method('getConfigurationPlateforme')
            ->willReturn($configurationPlateforme)
        ;

        $inscritService
            ->expects(self::once())
            ->method('getRgpdLabels')
            ->with($inscrit)
            ->willReturn(['COMMUNICATION_PLACE', 'COMMUNICATION_SIA', 'COMMUNICATION_ENQUETE'])
        ;

        $inscritOutputDataTransformer = new InscritOutputDataTransformer(
            $configuration,
            $inscritService,
            $iriConverter
        );

        $result = $inscritOutputDataTransformer->transform(
            $inscrit,
            InscritOutput::class,
            $context
        );

        self::assertSame($result->identifiantTechnique, 42);
        self::assertSame($result->login, 'login');
        self::assertSame($result->prenom, 'prenom');
        self::assertSame($result->nom, 'nom');
        self::assertSame($result->email, 'email');
        self::assertSame($result->telephone, '0636363636');
        self::assertTrue($result->actif);
        self::assertSame($result->adresse, [
            "rue" => "adresse",
            "codePostal" => "cp",
            "ville" => "ville",
            "pays" => "pays"
        ]);
        self::assertSame($result->siren, 'siren');
        self::assertSame($result->codeEtablissement, 'code');
        self::assertSame($result->inscritAnnuaireDefense, '0');
        self::assertSame($result->etablissement, '/api/v2/etablisssements/465');
        self::assertSame($result->entreprise, '/api/v2/entreprises/123');
        self::assertTrue(isset($result->{'dateModificationRgpd'}));
        self::assertTrue(isset($result->{'rgpd'}));
        self::assertEquals($result->dateModificationRgpd, new Datetime('2023-05-15'));
        self::assertSame($result->rgpd, ['COMMUNICATION_PLACE', 'COMMUNICATION_SIA', 'COMMUNICATION_ENQUETE']);
    }

    public function testTransformWithRgpdDisabled(): void
    {
        $configuration = $this->createMock(AtexoConfiguration::class);
        $inscritService = $this->createMock(InscritService::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);

        $entreprise = new Entreprise();
        $entreprise->setSiren('siren');
        $etablissement = new Etablissement();
        $etablissement->setCodeEtablissement('code');

        // Get contrat titulaire object
        $inscrit = $this->getInscritProvider();
        $inscrit
            ->setEtablissement($etablissement)
            ->setEntreprise($entreprise)
        ;

        $context = [
            "input" => [
                "class" => InscritOutput::class,
                "name" => "InscritOutput",
            ]
        ];

        $iriConverter
            ->expects(self::exactly(2))
            ->method('getIriFromItem')
            ->withConsecutive(
                [$etablissement],
                [$entreprise]
            )
            ->willReturnOnConsecutiveCalls(
                '/api/v2/etablisssements/465',
                '/api/v2/entreprises/123',
            )
        ;

        $configurationPlateforme = new ConfigurationPlateforme();
        $configurationPlateforme->setRecueilConsentementRgpd(false);

        $configuration
            ->expects(self::once())
            ->method('getConfigurationPlateforme')
            ->willReturn($configurationPlateforme)
        ;

        $inscritService
            ->expects(self::never())
            ->method('getRgpdLabels')
        ;

        $inscritOutputDataTransformer = new InscritOutputDataTransformer(
            $configuration,
            $inscritService,
            $iriConverter
        );

        $result = $inscritOutputDataTransformer->transform(
            $inscrit,
            InscritOutput::class,
            $context
        );

        self::assertSame($result->identifiantTechnique, 42);
        self::assertSame($result->login, 'login');
        self::assertSame($result->prenom, 'prenom');
        self::assertSame($result->nom, 'nom');
        self::assertSame($result->email, 'email');
        self::assertSame($result->telephone, '0636363636');
        self::assertTrue($result->actif);
        self::assertSame($result->adresse, [
            "rue" => "adresse",
            "codePostal" => "cp",
            "ville" => "ville",
            "pays" => "pays"
        ]);
        self::assertSame($result->siren, 'siren');
        self::assertSame($result->codeEtablissement, 'code');
        self::assertSame($result->inscritAnnuaireDefense, '0');
        self::assertSame($result->etablissement, '/api/v2/etablisssements/465');
        self::assertSame($result->entreprise, '/api/v2/entreprises/123');
        self::assertFalse(isset($result->{'dateModificationRgpd'}));
        self::assertFalse(isset($result->{'rgpd'}));
    }

    public function getInscritProvider(): Inscrit
    {
        return (new Inscrit())
            ->setId(42)
            ->setNom('nom')
            ->setPrenom('prenom')
            ->setLogin('login')
            ->setEmail('email')
            ->setTelephone('0636363636')
            ->setBloque(0)
            ->setAdresse('adresse')
            ->setCodepostal('cp')
            ->setVille('ville')
            ->setPays('pays')
            ->setInscritAnnuaireDefense('0')
            ->setDateValidationRgpd(new Datetime('2023-05-15'))
            ->setRgpdCommunicationPlace(true)
            ->setRgpdEnquete(true)
            ->setRgpdCommunication(true)
            ;
    }
}
