<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\ContratTitulaireOutputDataTransformer;
use App\Dto\Input\ContratTitulaireInput;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\CategorieConsultation;
use App\Entity\Consultation\ClausesN1;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\TypeContrat;
use App\Repository\EtablissementRepository;
use App\Repository\TypeContratRepository;
use App\Serializer\ContratTitulaireNormalizer;
use App\Service\Consultation\ConsultationService;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\DataTransformer\ContratTransformer;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ContratTitulaireOutputDataTransformerTest extends TestCase
{
    public function testSupportsTransformation(): void
    {
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $contratTitulaireNormalizer = $this->createMock(ContratTitulaireNormalizer::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $clausesTransformer = $this->createMock(ClausesTransformer::class);

        $contratTitulaireOutputDataTransformer = new ContratTitulaireOutputDataTransformer(
            $contratTransformer,
            $contratTitulaireNormalizer,
            $iriConverter,
            $entityManager,
            $clausesTransformer,
            $this->createMock(ConsultationService::class)
        );

        $context = [
            "input" => [
                "class" => ContratTitulaireOutput::class,
                "name" => "ContratTitulaireOutput",
            ]
        ];

        self::assertTrue($contratTitulaireOutputDataTransformer->supportsTransformation(
            new ContratTitulaire(),
            ContratTitulaireOutput::class,
            $context
        ));

        self::assertFalse($contratTitulaireOutputDataTransformer->supportsTransformation(
            new ContratTitulaire(),
            ContratTitulaireInput::class,
            $context
        ));

        self::assertFalse($contratTitulaireOutputDataTransformer->supportsTransformation(
            new \stdClass(),
            ContratTitulaireOutput::class,
            $context
        ));
    }

    public function testTransform(): void
    {
        $contratTransformer = $this->createMock(ContratTransformer::class);
        $contratTitulaireNormalizer = $this->createMock(ContratTitulaireNormalizer::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $clausesTransformer = $this->createMock(ClausesTransformer::class);
        $consultationService = $this->createMock(ConsultationService::class);

        $contratTitulaireOutputDataTransformer = new ContratTitulaireOutputDataTransformer(
            $contratTransformer,
            $contratTitulaireNormalizer,
            $iriConverter,
            $entityManager,
            $clausesTransformer,
            $consultationService
        );

        // Get contrat titulaire object
        $contratTitulaire = $this->getExistingContratTitulaire();

        $contratTransformer
            ->expects(self::once())
            ->method('getTrancheBudgetaire')
            ->with(41)
            ->willReturn('0 à 3 999,99 HT')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getCcag')
            ->with(15)
            ->willReturn('CCAG')
        ;

        $typeContrat = new TypeContrat();
        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo
            ->expects(self::once())
            ->method('find')
            ->with(1)
            ->willReturn($typeContrat)
        ;

        $entreprise = new Entreprise();
        $etablissement = new Etablissement();
        $etablissement->setEntreprise($entreprise);
        $etablissementRepo = $this->createMock(EtablissementRepository::class);
        $etablissementRepo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['idEtablissement' => 12])
            ->willReturn($etablissement)
        ;

        $categorieConsultation = new CategorieConsultation();
        $categorieConsultationRepo = $this->createMock(TypeContratRepository::class);
        $categorieConsultationRepo
            ->method('find')
            ->willReturn($categorieConsultation)
        ;

        $entityManager
            ->method('getRepository')
            ->withConsecutive(
                [TypeContrat::class],
                [CategorieConsultation::class],
                [Etablissement::class]
            )
            ->willReturnOnConsecutiveCalls(
                $typeContratRepo,
                $categorieConsultationRepo,
                $etablissementRepo,
            )
        ;

        $contratTitulaireNormalizer
            ->expects(self::once())
            ->method('getStatut')
            ->with('4')
            ->willReturn('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getCpv')
            ->with($contratTitulaire)
            ->willReturn(
                [
                    "codePrincipal" => "09000000",
                    "codeSecondaire1" => "##"
                ]
            )
        ;

        $contratTransformer
            ->expects(self::once())
            ->method('getContratTransverse')
            ->with($contratTitulaire)
            ->willReturn(['value' => false])
        ;

        $clausesTransformer
            ->expects(self::once())
            ->method('getNestedClauses')
            ->with($contratTitulaire)
            ->willReturn([
                "slug" =>  "clausesSociales",
                "label" =>  "Considération(s) sociale(s)",
                "iri" =>  "\/api\/v2\/referentiels\/clauses-n1s\/1",
                "clausesN2" =>  [
                    [
                        "slug" =>  "marcheReserve",
                        "label" =>  "Le marché public est réservé à des",
                        "iri" =>  "\/api\/v2\/referentiels\/clauses-n2s\/4"
                    ]
                ]
            ])
        ;

        $iriConverter
            ->expects(self::exactly(4))
            ->method('getIriFromItem')
            ->withConsecutive(
                [$typeContrat],
                [$categorieConsultation],
                [$etablissement],
                [$entreprise],
            )
            ->willReturnOnConsecutiveCalls(
                '/api/v2/referentiels/contrats/1',
                '/api/v2/referentiels/nature-prestations/1',
                '/api/v2/etablissements/12',
                '/api/v2/entreprises/11',
            )
        ;

        $consultationService->method('getDepartmentsWithLieuExecution')->willReturn(['75', '92']);

        $context = [
            "input" => [
                "class" => ContratTitulaireOutput::class,
                "name" => "ContratTitulaireOutput",
            ]
        ];

        $result = $contratTitulaireOutputDataTransformer->transform(
            $contratTitulaire,
            ContratTitulaireOutput::class,
            $context
        );

        self::assertSame($result->idService, 789);
        self::assertSame($result->typeContrat, '/api/v2/referentiels/contrats/1');
        self::assertSame($result->idTitulaire, 11);
        self::assertSame($result->entreprise, '/api/v2/entreprises/11');
        self::assertSame($result->etablissement, '/api/v2/etablissements/12');
        self::assertSame($result->trancheBudgetaire, '0 à 3 999,99 HT');
        self::assertSame($result->ccagApplicable, 'CCAG');
        self::assertSame($result->idContact, 17);
        self::assertSame($result->horsPassation, 1);
        self::assertSame($result->numero, "2016T00019");
        self::assertSame($result->numeroLong, "2016MA00013401");
        self::assertSame($result->referenceLibre, "MAMPE2281");
        self::assertSame($result->objet, "MAMPE2281");
        self::assertSame($result->formePrix, "FORFAITAIRE");
        self::assertSame($result->defenseOuSecurite,  "0");
        self::assertSame($result->dureeMaximaleMarche, 8);
        self::assertSame($result->lieuExecution, ['75', '92']);
        self::assertSame($result->intitule, "Intitule Contrat");
        self::assertSame($result->dateDebutExecution, null);
        self::assertEquals($result->dateCreation, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleFinMarche, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleNotification, new DateTime("2022-09-20"));
        self::assertEquals($result->datePrevisionnelleFinMaximaleMarche, new DateTime("2022-09-20"));
        self::assertEquals($result->decisionAttribution, new DateTime("2022-09-20"));
        self::assertEquals($result->dateModification, new DateTime("2022-09-20"));
        self::assertEquals($result->dateNotification, new DateTime("2022-09-20"));
        self::assertEquals($result->dateFin, new DateTime("2022-09-20"));
        self::assertEquals($result->dateMaxFin, new DateTime("2022-09-20"));
        self::assertSame($result->statut, "STATUT_NOTIFICATION_CONTRAT_EFFECTUEE");
        self::assertSame($result->naturePrestation, '/api/v2/referentiels/nature-prestations/1');
        self::assertSame($result->idChapeau, 1456);
        self::assertSame($result->montant, 1234);
        self::assertSame($result->cpv, [
            'codePrincipal' => '09000000',
            'codeSecondaire1' => '##'
        ]);
        self::assertSame($result->contratTransverse, ['value' => false]);
        self::assertSame($result->clauses, [
                "slug" =>  "clausesSociales",
                "label" =>  "Considération(s) sociale(s)",
                "iri" =>  "\/api\/v2\/referentiels\/clauses-n1s\/1",
                "clausesN2" =>  [
                    [
                        "slug" =>  "marcheReserve",
                        "label" =>  "Le marché public est réservé à des",
                        "iri" =>  "\/api\/v2\/referentiels\/clauses-n2s\/4"
                    ]
                ]
            ]
        );
    }

    private function getExistingContratTitulaire(): ContratTitulaire
    {
        $referentielN1 = (new \App\Entity\Referentiel\Consultation\ClausesN1())
            ->setSlug('clauseSociales')
            ->setLabel('Clauses Sociales')
        ;
        $clauseN1 = (new ClausesN1())->setReferentielClauseN1($referentielN1);

        $contactContrat = new ContactContrat();
        $contactContrat->setIdContactContrat(17);

        $contratTitulaire = new ContratTitulaire();

        $contratTitulaire->setId(123);
        $contratTitulaire->setOrganisme('acronyme');
        $contratTitulaire->setServiceId(789);
        $contratTitulaire->setIdTypeContrat(1);
        $contratTitulaire->setIdTitulaire(11);
        $contratTitulaire->setIdTitulaireEtab(12);
        $contratTitulaire->setIdTrancheBudgetaire(41);
        $contratTitulaire->setCcagApplicable(15);
        $contratTitulaire->setIdContactContrat(17);
        $contratTitulaire->setContact($contactContrat);
        $contratTitulaire->setIdAgent(42);
        $contratTitulaire->setHorsPassation(1);
        $contratTitulaire->setNumero("2016T00019");
        $contratTitulaire->setNumeroLong("2016MA00013401");
        $contratTitulaire->setReferenceLibre("MAMPE2281");
        $contratTitulaire->setObjet("MAMPE2281");
        $contratTitulaire->setFormePrix("FORFAITAIRE");
        $contratTitulaire->setMarcheDefense( "0");
        $contratTitulaire->setDureeInitialeContrat(8);
        $contratTitulaire->setLieuExecution(",,254,255,");
        $contratTitulaire->setIntitule("Intitule Contrat");
        $contratTitulaire->setDateDebutExecution(null);
        $contratTitulaire->setDateCreation(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueFinContrat(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueNotification(new DateTime("2022-09-20"));
        $contratTitulaire->setDatePrevueMaxFinContrat(new DateTime("2022-09-20"));
        $contratTitulaire->setDateAttribution(new DateTime("2022-09-20"));
        $contratTitulaire->setDateModification(new DateTime("2022-09-20"));
        $contratTitulaire->setDateNotification(new DateTime("2022-09-20"));
        $contratTitulaire->setDateFin(new DateTime("2022-09-20"));
        $contratTitulaire->setDateMaxFin(new DateTime("2022-09-20"));
        $contratTitulaire->setStatut('4');
        $contratTitulaire->setCategorie(1);
        $contratTitulaire->setIdChapeau(1456);
        $contratTitulaire->setMontant(1234);
        $contratTitulaire->setCodeCpv1("09000000");
        $contratTitulaire->setCodeCpv2("##");
        $contratTitulaire->addClausesN1($clauseN1);

        return $contratTitulaire;
    }
}
