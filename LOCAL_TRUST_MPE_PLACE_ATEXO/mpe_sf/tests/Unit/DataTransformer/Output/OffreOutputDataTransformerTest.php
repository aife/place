<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\DataTransformer\Output;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\OffreOutputDataTransformer;
use App\Dto\Output\OffreOutput;
use App\Entity\Offre;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class OffreOutputDataTransformerTest extends TestCase
{
    private OffreOutputDataTransformer $offreOutputDataTransformer;

    protected function setUp(): void
    {
        $this->offreOutputDataTransformer = new OffreOutputDataTransformer(
            $this->createMock(IriConverterInterface::class),
            $this->createMock(EntityManagerInterface::class)
        );
    }

    public function transform()
    {
        $response = $this->offreOutputDataTransformer->transform(
            new Offre(),
            'App\Dto\Output\OffreOutput',
            []
        );

        $this->assertEquals($response::class,  OffreOutput::class);
    }

    public function testSupportsTransformationReturnFalse()
    {
        $response = $this->offreOutputDataTransformer->supportsTransformation(
            new OffreOutput(),
            'App\Dto\Output\OffreOutput',
            []
        );

        $this->assertFalse($response);

        $response = $this->offreOutputDataTransformer->supportsTransformation(
            new Offre(),
            'App\Dto\Output\Offre',
            []
        );

        $this->assertFalse($response);
    }

    public function testSupportsTransformationReturnTrue()
    {
        $response = $this->offreOutputDataTransformer->supportsTransformation(
            new Offre(),
            'App\Dto\Output\OffreOutput',
            []
        );

        $this->assertTrue($response);
    }
}