<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Listener;

use App\Listener\UtahListener;
use App\Service\Utah\UtahService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UtahListenerTest extends TestCase
{
    public function testUtahRequestNull()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $utahService = $this->createMock(UtahService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $utahListener = new UtahListener($parameterBag, $utahService, $logger);

        $event = new RequestEvent(
            $this->createMock(HttpKernelInterface::class),
            new Request(),
            HttpKernelInterface::SUB_REQUEST
        );

        $result = $utahListener->utah($event);
        $this->assertNull($result);
    }
}
