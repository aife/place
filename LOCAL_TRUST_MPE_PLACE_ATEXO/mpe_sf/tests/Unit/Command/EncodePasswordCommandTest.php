<?php

namespace Tests\Unit\Command;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Bundle\FrameworkBundle\Console\Application;

class EncodePasswordCommandTest extends KernelTestCase
{
    public function testExecute()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('password:encode');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['pass']);
        $commandTester->setInputs(['yes']);
        $commandTester->setInputs(['pepper']);
        $commandTester->execute(['command' => $command->getName()]);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('argo', $output);
    }
}
