<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Command\User;

use App\Command\User\CreateAdministratorCommand;
use App\Repository\AdministrateurRepository;
use App\Security\PasswordEncoder\CommonPasswordEncoder;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class CreateAdministratorCommandTest extends KernelTestCase
{
    public function testExecuteOptionNotSet(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $mockRepository = $this->createMock(AdministrateurRepository::class);
        $mockRepository->expects($this->never())
            ->method('persist')
        ;
        $mockRepository->expects($this->never())
            ->method('flush')
        ;

        $application->add(new CreateAdministratorCommand(
            $mockRepository,
            $this->createMock(CommonPasswordEncoder::class)
        ));
        $command = $application->find('mpe:create-admin');
        $commandTester = new CommandTester($command);

        foreach ($this->getOptions() as $key => $value) {
            $this->expectException(\InvalidArgumentException::class);
            $this->expectExceptionMessage('l\'option ' . $key . ' n\'est pas renseignée');
            $options = $this->getOptions();
            unset($options[$key]);
            $commandTester->execute(array_merge(['command'  => $command->getName()], $options));
            $response = $commandTester->getStatusCode();

            $this->assertEquals(0, $response);
        }
    }

    public function testExecute(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $mockRepository = $this->createMock(AdministrateurRepository::class);
        $mockRepository->expects($this->once())
            ->method('persist')
        ;
        $mockRepository->expects($this->once())
            ->method('flush')
        ;
        $application->add(new CreateAdministratorCommand(
            $mockRepository,
            $this->createMock(CommonPasswordEncoder::class)
        ));
        $command = $application->find('mpe:create-admin');
        $commandTester = new CommandTester($command);

        $commandTester->execute(array_merge(['command'  => $command->getName()], $this->getOptions()));
        $response = $commandTester->getStatusCode();

        $this->assertEquals(0, $response);
    }

    /**
     * @return string[]
     */
    private function getOptions(): array
    {
        return [
            '--nom' => 'Houbre',
            '--prenom' => 'Gary',
            '--password' => 'Prado1234',
            '--email' => 'gaho@yahoo.fr',
            '--login' => 'gaho',
        ];
    }
}
