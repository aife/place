<?php

namespace Tests\Unit\Command;

use App\Entity\Agent\NotificationAgent;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Persistence\ObjectRepository;
use LogicException;
use PHPUnit\Framework\TestCase;
use ReflectionClass;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;

class DeleteArchiveDocCommandTest extends TestCase
{
    public function testDelete()
    {
        $notification = new NotificationAgent();
        $class = new ReflectionClass($notification);
        $property = $class->getProperty('idNotification');
        $property->setAccessible(true);
        $property->setValue($notification, 999999);
        $notification->setLibelleNotifcation('test');

        $cheminArchive = './2019/11/12/archives_test.zip';

        $repo = $this->createMock(ObjectRepository::class);
        $repo->expects($this->any())
            ->method('find')
            ->willReturn([$notification]);

        $objectManager = $this->createMock(ObjectManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($repo);

        $application = new Application();
        $application->setDefinition(
            new InputDefinition(
                [
                    'command' => new InputArgument('mpe:suppression-archive-documentaire'),
                    'path_archive' => new InputOption('path_archive'),
                    'id_notification' => new InputOption('id_notification'),
                ]
            )
        );
        $command = new Command();
        $command->setName('mpe:suppression-archive-documentaire');
        $command->setApplication($application);
        $definition = new InputDefinition();
        $definition->setArguments(
            [new InputArgument('command'), new InputArgument('path_archive'), new InputArgument('id_notification')]
        );
        $command->setDefinition($definition);

        $output = new ConsoleOutput();
        $input = new ArgvInput(
            [
                'command' => $command->getName(),
                'path_archive' => $cheminArchive,
                'id_notification' => (string) $notification->getIdNotification(),
            ]
        );
        $this->assertEquals('mpe:suppression-archive-documentaire', $command->getName());
        $this->expectException(LogicException::class);
        $command->run($input, $output);
    }
}
