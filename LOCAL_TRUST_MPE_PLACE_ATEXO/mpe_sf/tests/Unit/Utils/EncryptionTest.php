<?php

namespace Tests\Unit\Utils;

use App\Utils\Encryption;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EncryptionTest extends TestCase
{
    public function testCryptId()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->withConsecutive(['MPE_SECRET_PRIVATE'], ['MPE_SECRET_PUBLIC'])
            ->willReturnOnConsecutiveCalls('wbqu66LZvaZd5qYG', '8NiGHGJxeEwtajVw');
        $hash = (new Encryption($parameterBag))->cryptId(10);

        $this->assertEquals('VUhOcGhjRzlZd01COFpBaDZXZEJkZz09', $hash);
    }

    public function testDecryptId()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->withConsecutive(['MPE_SECRET_PRIVATE'], ['MPE_SECRET_PUBLIC'])
            ->willReturnOnConsecutiveCalls('wbqu66LZvaZd5qYG', '8NiGHGJxeEwtajVw');
        $deHash = (new Encryption($parameterBag))->decryptId('VUhOcGhjRzlZd01COFpBaDZXZEJkZz09');
        $this->assertEquals(10, $deHash);
    }
}
