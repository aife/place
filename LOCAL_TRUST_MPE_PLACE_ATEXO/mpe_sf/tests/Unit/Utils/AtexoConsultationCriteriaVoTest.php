<?php

namespace Tests\Unit\Utils;

use App\Utils\AtexoConsultationCriteriaVo;
use ArrayObject;
use PHPUnit\Framework\TestCase;

/**
 * Class AtexoConsultationCriteriaVo.
 */
class AtexoConsultationCriteriaVoTest extends TestCase
{
    /**
     * Test qui vérifie la présence des attribus dans l'objet AtexoConsultationCriteriaVo.
     */
    public function testAttributeAtexoConsultationCriteriaVo()
    {
        $atexo_Consultation_CriteriaVo = null;
        $atexoConsultationCriteriaVo = new AtexoConsultationCriteriaVo($atexo_Consultation_CriteriaVo);
        $arrayobj = new ArrayObject((array) $atexoConsultationCriteriaVo);
        $nbAttribut = count($arrayobj);
        $i = 0;

        $arrayAttribut = [
            'referenceConsultation',
            'referenceConsultationRestreinte',
            'idReference',
            'idTypeAvis',
            'notIdTypeAvis',
            'idTypeProcedure',
            'idEtatConsultation',
            'acronymeOrganisme',
            'acronymeOrganismeProcRest',
            'idService',
            'idServiceRma',
            'typeAcces',
            'categorieConsultation',
            'codeAccesProcedureRestreinte',
            'idCodeCpv1',
            'idCodeCpv2',
            'idCodeCpv3',
            'idCodeCpv4',
            'dateFinStart',
            'dateFinEnd',
            'intituleConsultation',
            'objetConsultation',
            'commentaireInterne',
            'intituleLot',
            'description',
            'typeSearch',
            'keyWordRechercheRapide',
            'keyWordAdvancedSearch',
            'etatConsultation',
            'defaultSortByElement',
            'sortByElement',
            'sensOrderBy',
            'offset',
            'limit',
            'secondConditionOnEtatConsultation',
            'searchModeExact',
            '_lieuxExecution',
            '_connectedAgentId',
            '_calledFromPortail',
            '_periodicite',
            '_orgActive',
            '_publicationEurope',
            '_etatPublicationToBePublished',
            '_etatPublicationPublished',
            '_etatPublicationToBeUpdated',
            '_etatPublicationUpdated',
            '_alertesFromYesterday',
            '_calledFromElios',
            '_sansCritereActive',
            '_consultationAValiderSeulement',
            '_consultationAApprouverSeulement',
            '_noTemporaryCOnsultation',
            '_enLigneDepuis',
            '_enLigneJusquau',
            '_orgDenomination',
            '_typeOrganisme',
            '_domaineActivite',
            '_qualification',
            '_agrements',
            '_fromArchive',
            '_codesNuts',
            '_idsService',
            'referentielVo',
            '_idInscrit',
            '_idEntreprise',
            '_forPanierEntreprise',
            '_avecRetrait',
            '_avecQuestion',
            '_avecDepot',
            '_avecEchange',
            '_sansRetrait',
            '_sansQuestion',
            '_sansDepot',
            '_sansEchange',
            'idAlerteConsultation',
            'alerteConsultationCloturee',
            'publieHier',
            'withDce',
            'informationMarche',
            'dateTimeFin',
            '_avecConsClotureesSansPoursuivreAffichage',
            'exclureConsultationExterne',
            'afficherDoublon',
            'denominationAdapte',
            'clauseSociale',
            'atelierProtege',
            'ess',
            'siae',
            'clauseEnv',
            'Mps',
            'consultaionPublier',
            'consultationAnnulee',
            'idOperation',
            'inclureDescendance',
            'rechercheAvanceeAutoCompletion',
            'consultationAArchiver',
            'bourseCotraitance',
            'visionRma',
            'organismeTest',
            'socialeCommerceEquitable',
            'socialeInsertionActiviterEconomique',
        ];

        foreach ($arrayAttribut as $attribut) {
            $this->assertObjectHasAttribute($attribut, $atexoConsultationCriteriaVo);
            ++$i;
        }

        $this->assertEquals($nbAttribut, $i);
    }
}
