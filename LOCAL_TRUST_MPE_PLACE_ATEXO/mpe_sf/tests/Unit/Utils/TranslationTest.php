<?php

namespace Tests\Unit\Utils;

use App\Utils\Translation;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class TranslationTest extends TestCase
{
    /** @var TranslatorInterface|MockObject */
    private $translator;

    /** @var Translation */
    private $translationUtil;

    private function initMocks()
    {
        $this->translator = $this->createMock(TranslatorInterface::class);
    }

    private function iniServices()
    {
        $this->translationUtil = new Translation($this->translator);
    }

    public function testDoubleAtMessagesTranslate()
    {
        $this->initMocks();
        $this->translator->expects($this->any())
            ->method('trans')
            ->with('TEST_TRANS')
            ->willReturn('test');
        $this->iniServices();
        $trans = $this->translationUtil->doubleAtMessagesTranslate('@@TEST_TRANS@@');
        $this->assertEquals('test', $trans);
    }
}
