<?php

namespace Tests\Unit\Utils\Filesystem;

use App\Entity\BloborganismeFile;
use App\Repository\BloborganismeFileRepository;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionClass;

class MountManagerTest extends TestCase
{
    private MockObject|EntityManager $em;
    private MountManager $mountManager;

    protected function setUp(): void
    {
        parent::setUp();

        $this->em = $this->createMock(EntityManager::class);

        $organismeBlobFichier = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobFichier);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 1);
        $property = $class->getProperty('organisme');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 'pmi-min-1');
        $property = $class->getProperty('chemin');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, '2019/10/15/pmi-min-1/files');

        $organismeBlobFichierRepositoryMock = $this->createMock(BloborganismeFileRepository::class);

        $organismeBlobFichierRepositoryMock->expects($this->any())
            ->method('getOrganismeBlobFichier')
            ->willReturn($organismeBlobFichier);

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->willReturn($organismeBlobFichierRepositoryMock);
    }

    public function getMountmanager()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $this->mountManager = new MountManager(
            $logger,
            $this->createMock(EntityManagerInterface::class),
            '/data/local-trust/mpe_place_test/',
            '/var/tmp/mpe/',
            '/tmp/mnt2;/tmp/mnt3;/tmp/mnt5;/tmp/mnt4',
            new MemoryAdapter(),
        );
    }

    public function testGetFileSystems()
    {
        $this->getMountmanager();
        $this->assertTrue($this->mountManager->getFilesystem('common_tmp') instanceof Filesystem, true);
        $this->assertTrue($this->mountManager->getFilesystem('nas') instanceof Filesystem, true);
        $this->assertTrue($this->mountManager->getFilesystem('/tmp/mnt3') instanceof Filesystem, true);
        $this->assertTrue($this->mountManager->getFilesystem('/tmp/mnt4') instanceof Filesystem, true);
        $this->assertTrue($this->mountManager->getFilesystem('/tmp/mnt5') instanceof Filesystem, true);
        $this->assertTrue($this->mountManager->getFilesystem('upload_tmp_dir') instanceof Filesystem, true);
    }
}
