<?php

namespace Tests\Unit\Utils;

use App\Utils\Encryption;
use App\Utils\Zip;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ZipTest extends TestCase
{
    public function testGetArrayOrderedFilesForZip()
    {
        $container = new Container();
        $container->setParameter('MPE_SECRET_PRIVATE', 'wbqu66LZvaZd5qYG');
        $container->setParameter('MPE_SECRET_PUBLIC', '8NiGHGJxeEwtajVw');
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $map = [['MPE_SECRET_PRIVATE', 'wbqu66LZvaZd5qYG'], ['MPE_SECRET_PUBLIC', '8NiGHGJxeEwtajVw']];
        $parameterBag->method('get')->will($this->returnValueMap($map));
        $encryption = new Encryption($parameterBag);

        $mock = $this->getMockBuilder(Zip::class)->setMethods(['getInfoFilesZip'])->setConstructorArgs(
            [$encryption]
        )->getMock();

        $dce = [];
        $dce['chemins'] = [];
        $dce['chemins'] = [];

        $dce['chemins'][1] = 'racine/';
        $dce['chemins'][2] = 'racine/fichier.txt';
        $dce['chemins'][3] = 'racine/sousrepertoire/';
        $dce['chemins'][4] = 'racine/sousrepertoire/fichier.txt';

        $dce['tailles'][1] = 0;
        $dce['tailles'][2] = 2;
        $dce['tailles'][3] = 0;
        $dce['tailles'][4] = 2;

        $mock->expects($this->any())
            ->method('getInfoFilesZip')
            ->willReturn($dce);

        $index = 0;
        $result = $mock->getArrayOrderedFilesForZip('toto/eazeaz', $index, 'DCE');

        $this->assertNotEmpty($result);
        $this->assertEquals(
            'fichier.txt',
            $result['racine']['noeuds']['sousrepertoire']['noeuds']['fichier.txt']['nom']
        );
        $this->assertEquals(2, $result['racine']['noeuds']['sousrepertoire']['noeuds']['fichier.txt']['poids']);
        $this->assertEquals('fichier', $result['racine']['noeuds']['sousrepertoire']['noeuds']['fichier.txt']['type']);
        $this->assertEquals(
            'T28zL0pwU1RyWjI4NUxWZzVEeWVHdz09',
            $result['racine']['noeuds']['sousrepertoire']['noeuds']['fichier.txt']['id']
        );
        $this->assertEquals(
            'racine/sousrepertoire/',
            $result['racine']['noeuds']['sousrepertoire']['noeuds']['fichier.txt']['base']
        );
    }

    public function testGetInfoFilesZip()
    {
        $filePath = __DIR__ . '/../data/ZipTest.zip';

        $sut = new Zip($this->createMock(Encryption::class));
        $maxIndex = 0;

        $expected = [
            'chemins' => [
                'test/',
                'test/fichier1.txt',
                'test/niveau1/',
                'test/niveau1/fichier2.txt',
            ],
            'tailles' => [
                0,
                24,
                0,
                37,
            ],
        ];

        $result = $sut->getInfoFilesZip($filePath, $maxIndex);

        $this->assertEquals($expected, $result);
    }

    public function testGetCountFilesZip()
    {
        $filePath = __DIR__ . '/../data/ZipTest.zip';

        $sut = new Zip($this->createMock(Encryption::class));

        $result = $sut->getCountFilesZip($filePath);

        $this->assertEquals(2, $result);
    }
}
