<?php

namespace Tests\Unit\Utils;

use Exception;
use App\Utils\Utils;
use PHPUnit\Framework\TestCase;

/**
 * Class UtilsTest.
 */
class UtilsTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     *
     * @param $date
     * @param $format
     * @param $result
     */
    public function testValidateDate($date, $format, $expectedResult)
    {
        $result = (new Utils())->validateDate($date, $format);
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return array
     */
    public function dataProvider()
    {
        return $dates = [
            ['2012-02-28 12:12:12', 'Y-m-d H:i:s', true],
            ['2012-02-30 12:12:12', 'Y-m-d H:i:s', false],
            ['2012-02-28', 'Y-m-d', true],
            ['28-02-2012', 'd-m-Y', true],
            ['28/02/2012', 'd/m/Y', true],
            ['30/02/2012', 'd/m/Y', false],
            ['14:50', 'H:i', true],
            ['14:77', 'H:i', false],
        ];
    }

    public function testFlatArrayEmpty()
    {
        $entry = [];
        $result = (new Utils())->flatArray($entry);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey(0, $result);
    }

    public function testFlatArrayData()
    {
        $entry = [
            0 => ['id' => 1],
            1 => ['id' => 2],
        ];
        $result = (new Utils())->flatArray($entry);
        $this->assertNotEmpty($result);
        $this->assertArrayHasKey(0, $result);
        $this->assertArrayHasKey(1, $result);
        $this->assertArrayHasKey(2, $result);
    }

    /**
     * Test fonction transformeDataLieuxExecutionIntoArray.
     */
    public function testTransformeDataLieuxExecutionIntoArray()
    {
        $list = [
            0 => [
                    'id' => '228',
                    'denomination1' => "(21) Côte-d'Or",
                    'denomination2' => '21',
                ],
            1 => [
                    'id' => '230',
                    'denomination1' => '(71) Saône - et - Loire',
                    'denomination2' => '71',
                ],
            2 => [
                    'id' => '238',
                    'denomination1' => '(36) Indre',
                    'denomination2' => '36',
                ],
            3 => [
                    'id' => '2251',
                    'denomination1' => '(10) Aube',
                    'denomination2' => '10',
                ],
        ];

        $result = (new Utils())->transformeDataLieuxExecutionIntoArray($list);

        $this->assertNotEmpty($result);
        $this->assertArrayHasKey("(21) Côte-d'Or", $result);
        $this->assertArrayHasKey('(71) Saône - et - Loire', $result);
        $this->assertArrayHasKey('(10) Aube', $result);
        $this->assertContains('230-71', $result);
    }

    /**
     * Test fonction transformeDataLieuxExecutionIntoArray.
     */
    public function testTransformeDataLieuxExecutionIntoArrayException()
    {
        $list = [
            0 => [
                'idError' => '228',
                'denomination1Error' => "(21) Côte-d'Or",
                'denomination2Error' => '21',
            ],
        ];
        $this->expectException(Exception::class);
        $result = (new Utils())->transformeDataLieuxExecutionIntoArray($list);
    }

    public function testTruncateHTMLContents()
    {
        $htmlString = '<p><span>This is a 70 characters string, you can count if you do not trust me !</span></p>';

        $result = Utils::truncateHTMLContents($htmlString, 50);

        $this->assertEquals('<p><span>This is a 70 characters string, you can count if</span></p>', $result);
    }
}
