<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Twig;

use App\Exception\InvalidArgumentException;
use App\Twig\ConsultationStatusExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationStatusExtensionTest extends TestCase
{
    protected ?TranslatorInterface $translatorMock;

    protected function getSut(): ConsultationStatusExtension
    {
        $this->translatorMock = $this->createMock(TranslatorInterface::class);

        $this->translatorMock->method('trans')
            ->willReturnCallback(function (string $id) {
                return 'Translated ' . str_replace('STATUS_LABEL_', '', $id);
            });

        return new ConsultationStatusExtension($this->translatorMock);
    }

    public function getConsultationStatusIconByCodeDataProvider(): array
    {
        return [
            ['ELABORATION', 'ft-edit-1'],
            ['PREPARATION', 'ft-check-circle'],
            // Icône du 1er statut dans le cas de multi-statuts :
            ['CONSULTATION,OUVERTURE_ANALYSE', 'ft-globe'],
            ['CONSULTATION / OUVERTURE_ANALYSE / DECISION', 'ft-globe'],
        ];
    }

    /**
     * @dataProvider getConsultationStatusIconByCodeDataProvider
     * @return void
     */
    public function testGetConsultationStatusIconByCode($input, $expected)
    {
        $extension = $this->getSut();

        $this->assertEquals($expected, $extension->getConsultationStatusIconByCode($input));
    }

    public function testGetConsultationStatusIconByCodeException()
    {
        $this->expectException(InvalidArgumentException::class);

        $extension = $this->getSut();
        $extension->getConsultationStatusIconByCode('CODE INVALIDE');
    }

    public function getConsultationStatusIdsByCodesDataProvider(): array
    {
        return [
            ['ELABORATION', ['0']],
            ['PREPARATION', ['1']],
            // Gestion des multi-statuts :
            ['CONSULTATION,OUVERTURE_ANALYSE', ['2', '3']],
            ['CONSULTATION, OUVERTURE_ANALYSE', ['2', '3']],
            ['CONSULTATION / OUVERTURE_ANALYSE / DECISION', ['2', '3', '4']],
        ];
    }

    /**
     * @dataProvider getConsultationStatusIdsByCodesDataProvider
     * @return void
     */
    public function testGetConsultationStatusIdsByCodes($input, $expected)
    {
        $extension = $this->getSut();

        $this->assertEquals($expected, $extension->getConsultationStatusIdsByCodes($input));
    }

    public function testGetConsultationStatusCodeByIdException()
    {
        $this->expectException(InvalidArgumentException::class);

        $extension = $this->getSut();
        $extension->getConsultationStatusIdsByCodes('CODE INVALIDE');
    }

    public function getTranslatedConsultationStatusesByCodesDataProvider(): array
    {
        return [
            ['ELABORATION', 'Translated ELABORATION'],
            ['PREPARATION', 'Translated PREPARATION'],
            // Gestion des multi-statuts :
            ['CONSULTATION,OUVERTURE_ANALYSE', 'Translated CONSULTATION / Translated OUVERTURE_ANALYSE'],
            ['CONSULTATION, OUVERTURE_ANALYSE', 'Translated CONSULTATION / Translated OUVERTURE_ANALYSE'],
            [
                'CONSULTATION / OUVERTURE_ANALYSE / DECISION',
                'Translated CONSULTATION / Translated OUVERTURE_ANALYSE / Translated DECISION'
            ],
        ];
    }

    /**
     * @dataProvider getTranslatedConsultationStatusesByCodesDataProvider
     * @return void
     */
    public function testGetTranslatedConsultationStatusesByCodes($input, $expected)
    {
        $extension = $this->getSut();

        $this->assertEquals($expected, $extension->getTranslatedConsultationStatusesByCodes($input));
    }

    public function getTranslatedConsultationStatusesAbbreviationsByCodesDataProvider(): array
    {
        return [
            ['ELABORATION', 'Translated STATUS_ABBR_ELABORATION'],
            ['PREPARATION', 'Translated STATUS_ABBR_PREPARATION'],
            // Gestion des multi-statuts :
            [
                'CONSULTATION,OUVERTURE_ANALYSE',
                'Translated STATUS_ABBR_CONSULTATION / Translated STATUS_ABBR_OUVERTURE_ANALYSE'
            ],
            [
                'CONSULTATION, OUVERTURE_ANALYSE',
                'Translated STATUS_ABBR_CONSULTATION / Translated STATUS_ABBR_OUVERTURE_ANALYSE'
            ],
            [
                'CONSULTATION / OUVERTURE_ANALYSE / DECISION',
                'Translated STATUS_ABBR_CONSULTATION / Translated STATUS_ABBR_OUVERTURE_ANALYSE'
                . ' / Translated STATUS_ABBR_DECISION'
            ],
        ];
    }

    /**
     * @dataProvider getTranslatedConsultationStatusesAbbreviationsByCodesDataProvider
     * @return void
     */
    public function testGetTranslatedConsultationStatusesAbbreviationsByCodes($input, $expected)
    {
        $extension = $this->getSut();

        $this->assertEquals($expected, $extension->getTranslatedConsultationStatusesAbbreviationsByCodes($input));
    }
}
