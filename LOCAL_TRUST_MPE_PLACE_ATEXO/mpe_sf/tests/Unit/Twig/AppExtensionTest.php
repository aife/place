<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Twig;

use App\Service\AtexoUtil;
use App\Twig\AppExtension;
use App\Utils\Translation;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AppExtensionTest extends TestCase
{
    public function testFileAssetExistsNotFound()
    {
        $translationUtil = $this->createMock(Translation::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $session = $this->createMock(SessionInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $path = 'https://mpe-docker.local-trust.com/themes/images/bandeau-email-not-exist.jpg';

        $logger
            ->expects(self::once())
            ->method('error')
            ->with(sprintf("Le fichier %s est introuvable sur le serveur", $path))
        ;

        $appExtension = new class ($translationUtil, $atexoUtil, $session, $logger) extends AppExtension {
            public function getHeaders(string $path): array|false
            {
                return ['HTTP/1.1 404 Not Found'];
            }
        };

        $response = $appExtension->fileAssetExists($path);

        $this->assertFalse($response);
    }

    public function testFileAssetExistsFound()
    {
        $appExtension = $this->createPartialMock(AppExtension::class, ['getHeaders']);
        $path = 'https://mpe-docker.local-trust.com/themes/images/bandeau-email.jpg';

        $appExtension->method('getHeaders')
            ->willReturn(['HTTP/1.1 200 ok']);

        $response = $appExtension->fileAssetExists($path);

        $this->assertTrue($response);
    }
}
