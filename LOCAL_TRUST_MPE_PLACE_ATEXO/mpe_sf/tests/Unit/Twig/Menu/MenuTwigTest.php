<?php

namespace Tests\Unit\Twig\Menu;

use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFilter;
use Twig\TwigFunction;

class MenuTwigTest extends TestCase
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var string
     */
    private $nameTest;

    public function setUp(): void
    {
        parent::setUp();
        $loader = new FilesystemLoader(__DIR__ . '/../../../../templates/');
        $this->twig = new Environment($loader, ['debug' => true]);
        $translate = new TwigFilter(
            'trans',
            function ($string) {
            }
        );
        $this->twig->addFilter($translate);
        $this->nameTest = $this->getName();

        switch ($this->nameTest) {
            case 'testTwigBlockBoampEmpty':
                $twigFunction = new TwigFunction(
                    'logoBoamp',
                    function () {
                        return false;
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockBoamp':
                $twigFunction = new TwigFunction(
                    'logoBoamp',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockConnexionFCEmpty':
                $this->twig->addGlobal('ACTIVER_FC_AUTHENTIFICATION', '0');
                break;
            case 'testTwigBlockConnexionFC':
                $this->twig->addGlobal('ACTIVER_FC_AUTHENTIFICATION', '1');
                break;
            case 'testTwigFooter':
                $twigFunction = new TwigFunction(
                    'langues',
                    function () {
                        return 'fr';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'visibleLink',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'visibleLinkListeAcRGS',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'path',
                    function () {
                        return '/entreprise/footer/nos-partenaires';
                    }
                );
                $this->twig->addFunction($twigFunction);

                                $twigFunction = new TwigFunction(
                                    'asset',
                                    function () {
                                        return '/themes/js/entreprise.js';
                                    }
                                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'showLanguage',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'displayRgpd',
                    function () {
                        return false;
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockConnexionRepondre':
                $twigFunction = new TwigFunction(
                    'path',
                    function () {
                        return '/entreprise/footer/diagnostic-poste';
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockConnexionAide':
                $twigFunction = new TwigFunction(
                    'path',
                    function ($params) {
                        return '/entreprise/aide/' . (str_contains(
                            $params,
                            'teleph'
                        ) ? 'assistance-telephonique' : 'outils-informatiques');
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockMenuDS':
                $twigFunction = new TwigFunction(
                    'path',
                    function () {
                        return 'entreprise/dossier-volumineux/gestion/';
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockMenuAnnonces':
            case 'testTwigBlockMenuNonConnecter':
            case 'testTwigBlockConnexionStandart':
            case 'testTwigBlockMenuOutilSignature':
            case 'testTwigBlockMenuConnecter':
                $twigFunction = new TwigFunction(
                    'path',
                    function () {
                        return 'http://path/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'asset',
                    function () {
                        return 'http://asset/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'csrf_token',
                    function () {
                        return 'WfF1szMUHhiokx9AHFply5L2xAOfjRk';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'isActiveAuthentificationInscritByForm',
                    function () {
                        return '1';
                    }
                );
                $this->twig->addFunction($twigFunction);
                break;
            case 'testTwigBlockMenuSocle':
                $twigFunction = new TwigFunction(
                    'urlSocleVisible',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'urlSocle',
                    function () {
                        return 'http://urlSocle/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'urlPortail',
                    function () {
                        return 'http://urlPortail/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                break;
            case 'testTwigMenu':
                $twigFunction = new TwigFunction(
                    'langues',
                    function () {
                        return 'fr';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $this->twig->addGlobal('ACTIVER_FC_AUTHENTIFICATION', '0');

                $twigFunction = new TwigFunction(
                    'logoBoamp',
                    function () {
                        return false;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'urlSocleVisible',
                    function () {
                        return true;
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'urlSocle',
                    function () {
                        return 'http://urlSocle/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'urlPortail',
                    function () {
                        return 'http://urlPortail/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'path',
                    function () {
                        return 'http://path/';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'csrf_token',
                    function () {
                        return 'WfF1szMUHhiokx9AHFply5L2xAOfjRk';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'isActiveAuthentificationInscritByForm',
                    function () {
                        return '1';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'isActiveSocleEntreprise',
                    function () {
                        return '0';
                    }
                );
                $this->twig->addFunction($twigFunction);

                $twigFunction = new TwigFunction(
                    'get_header',
                    function () {
                        return '<header></header>';
                    }
                );

                $this->twig->addFunction($twigFunction);
                break;
        }
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testTwigBlockConnexionAide()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_connexion_aide.html.twig'))->render();
        $this->assertStringContainsString('?page=Entreprise.EntreprisePremiereVisite', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseGuide&Aide', $render);
        $this->assertStringContainsString('/entreprise/aide/assistance-telephonique', $render);
        $this->assertStringContainsString('/entreprise/aide/outils-informatiques', $render);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testTwigBlockBoampEmpty()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_boamp.html.twig'))->render();
        $this->assertStringContainsString('', $render);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testTwigBlockBoamp()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_boamp.html.twig'))->render();
        $this->assertStringContainsString('http://www.boamp.fr', $render);
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_panelLogoBoamp', $render);
    }

    public function testTwigBlockConnexionFCEmpty()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_connexion_fc.html.twig'))->render();
        $this->assertStringContainsString('', $render);
    }

    public function testTwigBlockConnexionFC()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_connexion_fc.html.twig'))->render();
        $this->assertStringContainsString('FranceConnect', $render);
        $this->assertStringContainsString('fc_bouton_alt2_v2.png', $render);
    }

    public function testTwigBlockConnexionRepondre()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_connexion_repondre.html.twig'))->render();
        $this->assertStringContainsString('/entreprise/footer/diagnostic-poste', $render);
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;AllCons&amp;orgTest',
            $render
        );
    }

    public function testTwigBlockConnexionStandart()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_connexion_standard.html.twig'))->render();
        $this->assertStringContainsString('form[_username]', $render);
        $this->assertStringContainsString('form[_password]', $render);
        $this->assertStringContainsString('_csrf_token', $render);
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_authentificationButton', $render);
    }

    public function testTwigBlockLogo()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_logo.html.twig'))->render();
        $this->assertStringContainsString('href="entreprise"', $render);
        $this->assertStringContainsString('logo-replace.png', $render);
    }

    public function testTwigBlockMenuAnnonces()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_annonces.html.twig'))->render();
        $this->assertStringContainsString('?page=Entreprise.EntrepriseGestionRecherches', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseParticipationEnchere', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseAdvancedSearch&AllAnn', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseAdvancedSearch&AvisAttribution', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseAdvancedSearch&AvisInformation', $render);
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons&type=restreinte',
            $render
        );
        $this->assertStringContainsString('?page=Entreprise.EntrepriseAdvancedSearch&AllCons', $render);
        $this->assertStringContainsString('btnGoToQuickResultSearch', $render);
        $this->assertStringContainsString('keyWord', $render);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function testTwigBlockMenuCoTraitance()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_co_traitance.html.twig'))->render();
        $this->assertStringContainsString('?page=Entreprise.RechercheCollaborateur', $render);
    }

    public function testTwigBlockMenuConnecter()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_connecter.html.twig'))->render();
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_panelConnected', $render);
        $this->assertStringContainsString('?page=Entreprise.EntrepriseAccueilAuthentifie', $render);
        $this->assertStringContainsString('http://path/', $render);
    }

    public function testTwigBlockMenuDS()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_ds.html.twig'))->render();
        $this->assertStringContainsString('entreprise/dossier-volumineux/gestion/', $render);
    }

    public function testTwigBlockMenuEntiteAchat()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_entite_achat.html.twig'))->render();
        $this->assertStringContainsString('?page=Entreprise.EntrepriseVisualiserEntitesAchatsRecherche', $render);
    }

    public function testTwigBlockMenuNonConnecter()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_non_connecter.html.twig'))->render();
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_nonAuthentifie', $render);
        $this->assertStringContainsString('_csrf_token', $render);
        $this->assertStringContainsString('password', $render);
        $this->assertStringContainsString('form[_password]', $render);
        $this->assertStringContainsString('form[_username]', $render);
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_panelDisconnected', $render);
        $this->assertStringContainsString(
            'index.php?page=Entreprise.EntrepriseHome&goto=%3Fpage%3DEntreprise.AccueilEntreprise',
            $render
        );
    }

    public function testTwigBlockMenuOutilSignature()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_outil_signature.html.twig'))
                  ->getSourceContext()
                  ->getCode();
        $this->assertStringContainsStringIgnoringCase('verification_signature_entreprise', $render);
    }

    public function testTwigBlockMenuPanier()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_panier.html.twig'))->render();
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;retraits',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansRetraits',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;questions',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;depots',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansDepots',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;echanges',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;sansEchanges',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;panierEntreprise&amp;panierEntreprise&amp;dateFinEnd',
            $render
        );
        $this->assertStringContainsString(
            '?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons&amp;panierEntreprise',
            $render
        );
    }

    public function testTwigBlockMenuSocle()
    {
        $render = ($this->twig->load('entreprise/menu/partial/block_menu_socle.html.twig'))->render();
        $this->assertStringContainsString('ctl0_bandeauEntrepriseWithoutMenuSf_dateAujourdhui', $render);
    }

    public function testTwigFooter()
    {
        $render = ($this->twig->load('entreprise/menu/footer.html.twig'))->render();

        $this->assertStringContainsString('/entreprise/footer/nos-partenaires', $render);
    }
}
