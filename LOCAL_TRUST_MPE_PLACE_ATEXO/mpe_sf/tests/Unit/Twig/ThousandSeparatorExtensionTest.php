<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Twig;

use App\Twig\ThousandSeparatorExtension;
use PHPUnit\Framework\TestCase;

class ThousandSeparatorExtensionTest extends TestCase
{
    /**
     * @dataProvider getNumbers
     */
    public function testThousandSeparator(?int $number, string $thousandSeparatedNumber)
    {
        $thousandSeparatorExtension = new ThousandSeparatorExtension();

        self::assertSame($thousandSeparatedNumber, $thousandSeparatorExtension->thousandSeparator($number));
    }

    public function getNumbers(): iterable
    {
        yield [0, '0'];
        yield [123, '123'];
        yield [123456, '123 456'];
        yield [123456789, '123 456 789'];
    }
}
