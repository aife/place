<?php

namespace Tests\Unit\Twig;

use App\Entity\ConfigurationPlateforme;
use App\Service\Configuration;
use App\Twig\AppTranslationExtension;
use App\Twig\MenuExtension;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

/**
 * Class MenuExtensionTest.
 */
class MenuExtensionTest extends AtexoTestCase
{
    /**
     * @var MenuExtension
     */
    private $menu;
    private $nameTest;

    public function setUp(): void
    {
        parent::setUp();
        $entityManagerMock = $this->createMock(EntityManager::class);
        $configurationMock = $this->createMock(Configuration::class);

        $context = new RequestContext();
        $routerMock = $this->createMock(RouterInterface::class);
        $translatorInterfaceMock = $this->createMock(AppTranslationExtension::class);
        $routerMock->expects($this->any())
            ->method('getContext')
            ->willReturn($context);

        $this->nameTest = $this->getName();
        $configurationPlateforme = new ConfigurationPlateforme();
        switch ($this->nameTest) {
            case 'testUrlPf':
            case 'testUrlSocleVisibleTrue':
            case 'testUrlSocleUrlIndexSsoEntreprise':
                $configurationPlateforme->setSocleExterneEntreprise('1');
                break;
            case 'testUrlSocleUrlPppInscript':
                $configurationPlateforme->setSocleExterneEntreprise('0');
                $configurationPlateforme->setSocleExternePpp('1');
                break;
            case 'testUrlSoclePfUrlEntreprise':
                $configurationPlateforme->setSocleExterneEntreprise('0');
                $configurationPlateforme->setSocleExternePpp('0');
                $configurationPlateforme->setAccueilEntreprisePersonnalise('1');
                break;
            case 'testUrlPortailExterne':
                $configurationPlateforme->setSocleExternePpp('1');
                break;
            case 'testGetVisibleLinkFalse':
                $translatorInterfaceMock->expects($this->any())
                    ->method('trans')
                    ->willReturn('');
                break;
            case 'testGetVisibleLinkTrue':
                $translatorInterfaceMock->expects($this->any())
                    ->method('trans')
                    ->willReturn('INFO_SITE');
                break;
            case 'testGetVisibleLinkListeAcRGSFalse':
                $configurationPlateforme->setListeACRgs('0');
                break;
            case 'testGetVisibleLinkListeAcRGSTrue':
                $configurationPlateforme->setListeACRgs('1');
                break;
        }

        $configurationMock->expects($this->any())
            ->method('getConfigurationPlateforme')
            ->willReturn($configurationPlateforme);

        $configurationMock->expects($this->any())
            ->method('getParameter')
            ->willReturnCallback(
                function ($parameter) {
                    if ('URL_INDEX_SSO_ENTREPRISE' === $parameter) {
                        return 'http://test/';
                    }

                    if ('URL_PPP_INSCRIPTION' === $parameter) {
                        return 'http://test-ppp/';
                    }

                    if ('PF_URL_ENTREPRISE' === $parameter) {
                        return 'http://PF_URL_ENTREPRISE/';
                    }

                    if ('URL_ACCUEIL_SPIP' === $parameter) {
                        return 'http://URL_ACCUEIL_SPIP/';
                    }

                    if ('URL_SOCLE_EXTERNE' === $parameter) {
                        return 'http://URL_SOCLE_EXTERNE/';
                    }

                    if ('AFFICHER_BANDEAU_LOGO_BOAMP_AGENT' === $parameter
                        && 'testGetLogoBoampTrue' === $this->nameTest
                    ) {
                        return '1';
                    }
                }
            );
        $this->menu = new MenuExtension($entityManagerMock, $configurationMock, $routerMock, $translatorInterfaceMock);
    }

    public function testUrlSocleUrlIndexSsoEntreprise()
    {
        $menu = $this->menu->getUrlSocle();
        $this->assertEquals('http://test/', $menu);
    }

    public function testUrlSocleUrlPppInscript()
    {
        $menu = $this->menu->getUrlSocle();
        $this->assertEquals('http://test-ppp/', $menu);
    }

    public function testUrlSoclePfUrlEntreprise()
    {
        $resultatAttendu = 'http://PF_URL_ENTREPRISE/?page=Entreprise'
            .'.EntrepriseHome&goto=%3Fpage%3DEntreprise.EntrepriseAccueilAuthentifie&inscription';
        $menu = $this->menu->getUrlSocle();
        $this->assertEquals($resultatAttendu, $menu);
    }

    public function testUrlPf()
    {
        $url = $this->menu->getUrlPf();
        $this->assertEquals('http://PF_URL_ENTREPRISE/', $url);
    }

    public function testUrlSocleVisibleFalse()
    {
        $visible = $this->menu->getUrlSocleVisible();
        $this->assertFalse($visible);
    }

    public function testUrlSocleVisibleTrue()
    {
        $visible = $this->menu->getUrlSocleVisible();
        $this->assertTrue($visible);
    }

    public function testUrlPortailSpip()
    {
        $url = $this->menu->getUrlPortail();
        $this->assertEquals('http://URL_ACCUEIL_SPIP/', $url);
    }

    public function testUrlPortailExterne()
    {
        $url = $this->menu->getUrlPortail();
        $this->assertEquals('http://URL_SOCLE_EXTERNE/', $url);
    }

    public function testGetLogoBoampFalse()
    {
        $visible = $this->menu->getLogoBoamp();
        $this->assertFalse($visible);
    }

    public function testGetLogoBoampTrue()
    {
        $visible = $this->menu->getLogoBoamp();
        $this->assertTrue($visible);
    }

    public function testGetVisibleLinkFalse()
    {
        $visible = $this->menu->getVisibleLink('INFO_SITE');
        $this->assertTrue($visible);
    }

    public function testGetVisibleLinkTrue()
    {
        $visible = $this->menu->getVisibleLink('INFO_SITE');
        $this->assertFalse($visible);
    }

    public function testGetVisibleLinkListeAcRGSFalse()
    {
        $visible = $this->menu->getVisibleLinkListeAcRGS();
        $this->assertFalse($visible);
    }

    public function testGetVisibleLinkListeAcRGSTrue()
    {
        $visible = $this->menu->getVisibleLinkListeAcRGS();
        $this->assertTrue($visible);
    }
}
