<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Twig;

use App\Entity\Inscrit;
use App\Service\CurrentUser;
use App\Twig\UtahExtension;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UtahExtensionTest extends TestCase
{
    public function testisAssistanceButtonDisplayedSuccess()
    {
        $currentUserMock = $this->createMock(CurrentUser::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock->set('UTILISER_UTAH', '1');
        $parameterBagMock->set('UTILISER_FAQ', '1');
        $parameterBagMock->set('UTILISER_UTAH_MODE_DECONNECTER', '1');

        $parameterBagMock->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'UTILISER_UTAH') {
                    $value = '1';
                }

                if ($params === 'UTILISER_FAQ') {
                    $value = '1';
                }

                if ($params === 'UTILISER_UTAH_MODE_DECONNECTER') {
                    $value = '1';
                }

                return $value;
            }));

        $user = $this->createMock(Inscrit::class);
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertTrue($utahExtension->isAssistanceButtonDisplayed($user));

        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertTrue($utahExtension->isAssistanceButtonDisplayed(null));

        $parameterBagMock->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'UTILISER_UTAH') {
                    $value = '0';
                }

                if ($params === 'UTILISER_FAQ') {
                    $value = '0';
                }

                if ($params === 'UTILISER_UTAH_MODE_DECONNECTER') {
                    $value = '1';
                }

                return $value;
            }));
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertTrue($utahExtension->isAssistanceButtonDisplayed($user));

        $parameterBagMock->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'UTILISER_UTAH') {
                    $value = '0';
                }

                if ($params === 'UTILISER_FAQ') {
                    $value = '0';
                }

                if ($params === 'UTILISER_UTAH_MODE_DECONNECTER') {
                    $value = '0';
                }

                return $value;
            }));
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertTrue($utahExtension->isAssistanceButtonDisplayed($user));
    }

    public function testisAssistanceButtonDisplayedFail()
    {
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'UTILISER_UTAH') {
                    $value = '1';
                }

                if ($params === 'UTILISER_FAQ') {
                    $value = '1';
                }

                if ($params === 'UTILISER_UTAH_MODE_DECONNECTER') {
                    $value = '0';
                }

                return $value;
            }));
        $currentUserMock = $this->createMock(CurrentUser::class);

        $user = $this->createMock(Inscrit::class);
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertFalse($utahExtension->isAssistanceButtonDisplayed($user));

        $currentUserMock = $this->createMock(CurrentUser::class);
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertFalse($utahExtension->isAssistanceButtonDisplayed(null));

        $parameterBagMock->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'UTILISER_UTAH') {
                    $value = '0';
                }

                if ($params === 'UTILISER_FAQ') {
                    $value = '0';
                }

                if ($params === 'UTILISER_UTAH_MODE_DECONNECTER') {
                    $value = '1';
                }

                return $value;
            }));
        $currentUserMock = $this->createMock(CurrentUser::class);

        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertFalse($utahExtension->isAssistanceButtonDisplayed($user));

        $currentUserMock = $this->createMock(CurrentUser::class);
        $utahExtension = new UtahExtension($currentUserMock, $parameterBagMock);
        $this->assertFalse($utahExtension->isAssistanceButtonDisplayed(null));
    }
}
