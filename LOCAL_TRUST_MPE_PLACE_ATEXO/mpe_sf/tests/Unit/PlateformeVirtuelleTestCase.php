<?php

namespace Tests\Unit;

use App\Repository\Configuration\PlateformeVirtuelleRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

abstract class PlateformeVirtuelleTestCase extends TestCase
{
    public const DOMAIN1 = 'domaine1.fr';
    public const DOMAIN2 = 'domaine2.fr';

    protected $logger;

    public function setUp(): void
    {
        parent::setUp();
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    protected function prepareRequestStack(array $cookies = [], array $server = null): RequestStack
    {
        if (is_null($server)) {
            $server = [
                'HTTPS' => 'on',
                'HTTP_HOST' => self::DOMAIN1,
                'REQUEST_URI' => '/entreprise'
            ];
        }

        $query = [];
        if (isset($server['QUERY_STRING'])) {
            parse_str($server['QUERY_STRING'], $query);
        }

        $request = new Request(
            $query,
            [],
            [],
            $cookies,
            [],
            $server,
            []
        );

        $requestStack = new RequestStack();
        $requestStack->push($request);

        return $requestStack;
    }

    protected function getPfvRepoMock($result)
    {
        $plateformeVirtuelleRepo = $this->createMock(PlateformeVirtuelleRepository::class);
        $plateformeVirtuelleRepo->expects($this->once())
            ->method('findOneBy')
            ->willReturn($result);

        return $plateformeVirtuelleRepo;
    }
}
