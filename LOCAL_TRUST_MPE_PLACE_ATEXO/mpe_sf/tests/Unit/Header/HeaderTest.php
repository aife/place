<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Header;

use App\Service\Header\Header;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HeaderTest extends TestCase
{
    private $headerService;

    public function setUp(): void
    {
        $this->headerService = new Header(
            $this->createMock(Environment::class),
            $this->createMock(ParameterBagInterface::class)
        );
    }

    public function testSetLinkRedirectToTheHomePage()
    {
        $this->headerService->setLinkRedirect('?page=Agent.AccueilAgentAuthentifie');
        $this->headerService->getTemplate('mpe');
        $this->assertEquals('mpe', $this->headerService->getTitle());
        $this->assertEquals('?page=Agent.AccueilAgentAuthentifie', $this->headerService->getLinkRedirect());
        $this->assertNotEquals('/', $this->headerService->getLinkRedirect());

        $this->headerService->setLinkRedirect('/');
        $this->headerService->getTemplate('faq');
        $this->assertEquals('faq', $this->headerService->getTitle());
        $this->assertEquals('/', $this->headerService->getLinkRedirect());

        $this->headerService->getTemplate('exec');
        $this->assertEquals('exec', $this->headerService->getTitle());
        $this->assertEquals(null, $this->headerService->getLinkRedirect());
    }
}
