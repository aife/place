<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Doctrine\Extension;

use App\Doctrine\Extension\ConsultationIdExtension;
use PHPUnit\Framework\TestCase;

class ConsultationIdExtensionTest extends TestCase
{
    public function shouldReturnOnlyIdsDataProvider(): array
    {
        return [
            [[], false],
            [['request_uri' => ''], false],
            [
                ['request_uri' => 'https://mpe-docker.local-trust.com/api/v2/consultations?page=1&itemsPerPage=10'],
                false,
            ],
            [['request_uri' => 'https://mpe-docker.local-trust.com/api/v2/consultations?idsOnly'], true],
            [['request_uri' => 'https://mpe-docker.local-trust.com/api/v2/consultations?page=1&idsOnly=1'], true],
        ];
    }

    /**
     * @dataProvider shouldReturnOnlyIdsDataProvider
     */
    public function testShouldReturnOnlyIds(array $context, $expected): void
    {
        $result = ConsultationIdExtension::shouldReturnOnlyIds($context);

        $this->assertSame($expected, $result);
    }
}
