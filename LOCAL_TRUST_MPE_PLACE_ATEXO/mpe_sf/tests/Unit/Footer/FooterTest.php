<?php

namespace Tests\Unit\Footer;

use App\Service\Footer\Footer;
use App\Service\RgpdService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class FooterTest extends TestCase
{
    private $footerService;
    public final const ENTREPRISE = 'entreprise';
    public final const AGENT = 'agent';
    public final const NOT_PARAM = 'no_param';

    public function setUp(): void
    {
        $mockTranslator = $this->createMock(TranslatorInterface::class);
        $mockSession = $this->createMock(SessionInterface::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $rgpdService = $this->createMock(RgpdService::class);
        $mockTranslator->expects($this->any())
            ->method('trans')
            ->willReturnCallback(
                function ($param) {
                    if ($param == 'TOTALEMENT_CONFORME') {
                        return 'totalement conforme';
                    } elseif ($param == 'NON_CONFORME') {
                        return 'non conforme';
                    } elseif ($param == 'PARTIELLEMENT_CONFORME') {
                        return 'partiellement conforme';
                    }
                }
            );
        $this->footerService = new Footer(
            $this->createMock(Environment::class),
            $this->createMock(ParameterBagInterface::class),
            $mockTranslator,
            $mockSession,
            $mockLogger,
            $rgpdService
        );
    }

    /**
     * @group test
     */
    public function testGetMsgAccessibility()
    {
        $msg = $this->footerService->getMsgAccessibility(self::ENTREPRISE);
        $this->assertEquals('partiellement conforme', $msg);

        $msg = $this->footerService->getMsgAccessibility(self::AGENT);
        $this->assertEquals('non conforme', $msg);

        $msg = $this->footerService->getMsgAccessibility(self::NOT_PARAM);
        $this->assertEquals('totalement conforme', $msg);
    }
}
