<?php

namespace Tests\Unit\Entity;

use App\Entity\Consultation\CritereAttribution;
use Tests\Unit\MpeEntityTestCase;

class CritereAttributionTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return CritereAttribution::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'enonce',
            'ordre',
            'ponderation',
            'idDonneeComplementaire',
            'sousCriteres',
            'donneeComplementaire',
            'lot'
        ];
    }
}
