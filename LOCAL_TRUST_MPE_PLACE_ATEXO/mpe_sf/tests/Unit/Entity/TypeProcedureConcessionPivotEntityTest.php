<?php

namespace Tests\Unit\Entity;

use App\Entity\TypeProcedureConcessionPivot;
use Tests\Unit\MpeEntityTestCase;

class TypeProcedureConcessionPivotEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TypeProcedureConcessionPivot::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'libelle',
            'contratTitulaires',
        ];
    }
}
