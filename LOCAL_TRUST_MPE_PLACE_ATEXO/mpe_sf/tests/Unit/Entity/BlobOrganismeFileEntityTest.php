<?php

namespace Tests\Unit\Entity;

use App\Entity\BloborganismeFile;
use DateTime;
use Tests\Unit\MpeEntityTestCase;

class BlobOrganismeFileEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return BloborganismeFile::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'organisme',
            'name',
            'deletionDatetime',
            'statutSynchro',
            'hash',
            'chemin',
            'dossier',
            'oldId',
            'extension',
            'file'
        ];
    }

    public function testMethode()
    {
        $bloborganismeFile = new BloborganismeFile();

        $bloborganismeFile->setOrganisme('pmi-min-1');
        $this->assertEquals('pmi-min-1', $bloborganismeFile->getOrganisme());

        $bloborganismeFile->setName('nom-fichier');
        $this->assertEquals('nom-fichier', $bloborganismeFile->getName());

        $datetime = new DateTime();
        $bloborganismeFile->setDeletionDatetime($datetime);
        $this->assertEquals($datetime, $bloborganismeFile->getDeletionDatetime());

        $bloborganismeFile->setStatutSynchro(0);
        $this->assertEquals(0, $bloborganismeFile->getStatutSynchro());

        $bloborganismeFile->setHash('qskhfkqsfksh');
        $this->assertEquals('qskhfkqsfksh', $bloborganismeFile->getHash());
    }
}
