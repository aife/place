<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocApplicationTest.
 */
class EchangeDocApplicationTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $echangeDocApplicationClient = new EchangeDocApplicationClient();
        $echangeDocApplication = new EchangeDocApplication();
        $code = 'code';
        $libelle = 'libelle';
        $echangeDocApplication->setCode($code);
        $echangeDocApplication->setLibelle($libelle);
        $echangeDocApplication->addEchangeDocApplicationClients($echangeDocApplicationClient);
        $this->assertEquals($code, $echangeDocApplication->getCode());
        $this->assertEquals($libelle, $echangeDocApplication->getLibelle());
        $this->assertEquals($echangeDocApplicationClient, $echangeDocApplication->getEchangeDocApplicationClients()[0]);
    }

    protected function getTestedEntity()
    {
        return EchangeDocApplication::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'code',
            'libelle',
            'echangeDocApplicationClients',
            'fluxActes',
            'primoSignature',
            'envoiDic',
        ];
    }
}
