<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocHistorique;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocHistoriqueTest.
 */
class EchangeDocHistoriqueTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return EchangeDocHistorique::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'echangeDoc',
            'agent',
            'createdAt',
            'messageFonctionnel',
            'messageTechnique',
            'statut',
        ];
    }
}
