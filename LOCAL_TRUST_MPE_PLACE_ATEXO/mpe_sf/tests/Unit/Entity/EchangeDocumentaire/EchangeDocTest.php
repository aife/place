<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocTest.
 */
class EchangeDocTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $echangeDoc = new EchangeDoc();
        $objet = 'objet';
        $description = 'description';
        $echangeDoc->setObjet($objet);
        $echangeDoc->setDescription($description);
        $echangeDocApplicationClient = new EchangeDocApplicationClient();
        $echangeDoc->setEchangeDocApplicationClient($echangeDocApplicationClient);

        $this->assertEquals($objet, $echangeDoc->getObjet());
        $this->assertEquals($description, $echangeDoc->getDescription());
        $this->assertTrue($echangeDoc->getEchangeDocApplicationClient() instanceof EchangeDocApplicationClient);
        $this->assertEquals($echangeDocApplicationClient, $echangeDoc->getEchangeDocApplicationClient());
    }

    protected function getTestedEntity()
    {
        return EchangeDoc::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'echangeDocApplicationClient',
            'objet',
            'description',
            'echangeDocBlobs',
            'statut',
            'createdAt',
            'consultation',
            'agent',
            'updatedAt',
            'echangeDocHistoriques',
            'cheminementSignature',
            'cheminementGed',
            'cheminementSae',
            'cheminementTdt',
            'contratTitulaire',
            'montantContrat',
            'raisonSocialeTitulaire',
            'referentielSousTypeParapheur',
            'uuidExterneExec',
            'numeroContrat',
        ];
    }
}
