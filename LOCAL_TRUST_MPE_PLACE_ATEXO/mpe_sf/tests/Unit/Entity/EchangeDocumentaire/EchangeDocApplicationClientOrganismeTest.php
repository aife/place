<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClientOrganisme;
use App\Entity\Organisme;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocApplicationClientOrganismeTest.
 */
class EchangeDocApplicationClientOrganismeTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $echangeDocApplicationClient = new EchangeDocApplicationClient();
        $organisme = new Organisme();
        $echangeDocApplicationClientOrganisme = new EchangeDocApplicationClientOrganisme();

        $echangeDocApplicationClientOrganisme->setOrganisme($organisme);
        $echangeDocApplicationClientOrganisme->setEchangeDocApplicationClient($echangeDocApplicationClient);

        $this->assertEquals($organisme, $echangeDocApplicationClientOrganisme->getOrganisme());
        $this->assertEquals(
            $echangeDocApplicationClient,
            $echangeDocApplicationClientOrganisme->getEchangeDocApplicationClient()
        );
    }

    protected function getTestedEntity()
    {
        return EchangeDocApplicationClientOrganisme::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organisme',
            'echangeDocApplicationClient',
        ];
    }
}
