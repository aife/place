<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\BloborganismeFile;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocBlobTest.
 */
class EchangeDocBlobTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $echangeDoc = new EchangeDoc();
        $blobOrganisme = new BloborganismeFile();
        $echangeDocBlob = new EchangeDocBlob();
        $chemin = 'path/to/the/end';
        $poids = 100;
        $checksum = 'ASWZ';
        $echangeDocBlob->setChemin($chemin);
        $echangeDocBlob->setBlobOrganisme($blobOrganisme);
        $echangeDocBlob->setEchangeDoc($echangeDoc);
        $echangeDocBlob->setPoids($poids);
        $echangeDocBlob->setChecksum($checksum);

        $this->assertEquals($chemin, $echangeDocBlob->getChemin());
        $this->assertEquals($blobOrganisme, $echangeDocBlob->getBlobOrganisme());
        $this->assertEquals($echangeDoc, $echangeDocBlob->getEchangeDoc());
        $this->assertEquals($poids, $echangeDocBlob->getPoids());
        $this->assertEquals($checksum, $echangeDocBlob->getChecksum());
        $this->assertTrue($echangeDocBlob->getEchangeDoc() instanceof EchangeDoc);
        $this->assertTrue($echangeDocBlob->getBlobOrganisme() instanceof BloborganismeFile);
    }

    protected function getTestedEntity()
    {
        return EchangeDocBlob::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'chemin',
            'echangeDoc',
            'blobOrganisme',
            'poids',
            'checksum',
            'echangeTypeActes',
            'categoriePiece',
            'echangeTypeStandard',
            'docPrincipal',
            'annexes',
        ];
    }
}
