<?php

namespace Tests\Unit\Entity\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplication;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClientOrganisme;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class EchangeDocApplicationClientTest.
 */
class EchangeDocApplicationClientTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $echangeDoc = new EchangeDoc();
        $echangeDocApplication = new EchangeDocApplication();
        $echangeDocApplicationClientOrganisme = new EchangeDocApplicationClientOrganisme();
        $echangeDocApplicationClient = new EchangeDocApplicationClient();

        $code = 'code';
        $libelle = 'libelle';
        $actif = true;
        $echangeDocApplicationClient->setCode($code);
        $echangeDocApplicationClient->setLibelle($libelle);
        $echangeDocApplicationClient->setActif($actif);
        $echangeDocApplicationClient->setEchangeDocApplication($echangeDocApplication);
        $echangeDocApplicationClient->addEchangeDocApplicationClientOrganismes($echangeDocApplicationClientOrganisme);
        $echangeDocApplicationClient->addEchangeDocs($echangeDoc);
        $this->assertEquals($code, $echangeDocApplicationClient->getCode());
        $this->assertEquals($libelle, $echangeDocApplicationClient->getLibelle());
        $this->assertEquals($actif, $echangeDocApplicationClient->getActif());
        $this->assertEquals($echangeDoc, $echangeDocApplicationClient->getEchangeDocs()[0]);
        $this->assertEquals($echangeDocApplication, $echangeDocApplicationClient->getEchangeDocApplication());
        $this->assertEquals(
            $echangeDocApplicationClientOrganisme,
            $echangeDocApplicationClient->getEchangeDocApplicationClientOrganismes()[0]
        );
    }

    protected function getTestedEntity()
    {
        return EchangeDocApplicationClient::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'code',
            'libelle',
            'actif',
            'echangeDocApplicationClientOrganismes',
            'echangeDocs',
            'echangeDocApplication',
            'cheminement_signature',
            'cheminement_ged',
            'cheminement_sae',
            'cheminement_tdt',
            'classification_1',
            'classification_2',
            'classification_3',
            'classification_4',
            'classification_5',
            'multiDocsPrincipaux',
            'envoiAutoArchivage'
        ];
    }
}
