<?php

namespace Tests\Unit\Entity\WS;

use App\Entity\Agent;
use App\Entity\WS\WebService;
use ArrayObject;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WebServiceEntityTest extends WebTestCase
{
    /**
     * Test qui vérifie la présence des attributs dans l'objet WebService.
     */
    public function testAttributesWebService()
    {
        $i = 0;
        $webService = new WebService();

        $arrayobj = new ArrayObject((array) $webService);
        $nbAttribut = $arrayobj->count();

        $tabProperties = [
            'id',
            'nomWs',
            'agents',
            'uniquementTechnique',
            'methodWs',
            'routeWs',
        ];

        foreach ($tabProperties as $property) {
            $this->assertObjectHasAttribute($property, $webService);
            ++$i;
        }

        $this->assertEquals($i, $nbAttribut);
    }

    public function testWebService()
    {
        $webService = new WebService();

        $webService->setId(1);
        $this->assertEquals(1, $webService->getId());

        $webService->setNomWs('service_1');
        $this->assertEquals('service_1', $webService->getNomWs());

        $collection = new ArrayCollection();
        $agent = new Agent();
        $agent->setId(1);
        $collection->add($agent);
        $webService->addAgent($agent);
        $this->assertEquals(1, count($webService->getAgents()));

        $webService->setUniquementTechnique(false);
        $this->assertEquals(false, $webService->isUniquementTechnique());

        $webService->setMethodWs('GET');
        $this->assertEquals('GET', $webService->getMethodWs());

        $webService->setRouteWs('/test/test');
        $this->assertEquals('/test/test', $webService->getRouteWs());
    }
}
