<?php

namespace Tests\Unit\Entity\WS;

use App\Entity\WS\AgentTechniqueAssociation;
use Tests\Unit\MpeEntityTestCase;

class AgentTechniqueAssociationEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return AgentTechniqueAssociation::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'idAgent',
            'organisme',
            'serviceId',
            'acronymeOrganisme',
            'service',
        ];
    }
}
