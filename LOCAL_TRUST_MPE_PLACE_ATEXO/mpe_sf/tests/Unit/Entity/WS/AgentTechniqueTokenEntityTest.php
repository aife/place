<?php

namespace Tests\Unit\Entity\WS;

use App\Entity\Agent;
use App\Entity\WS\AgentTechniqueToken;
use ArrayObject;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AgentTechniqueTokenEntityTest extends WebTestCase
{
    /**
     * Test qui vérifie la présence des attributs dans l'objet WebService.
     */
    public function testAttributesWebService()
    {
        $i = 0;
        $agentTechniqueToken = new AgentTechniqueToken();

        $arrayobj = new ArrayObject((array) $agentTechniqueToken);
        $nbAttribut = $arrayobj->count();

        $tabProperties = [
            'id',
            'token',
            'createAt',
            'agent',
            'updateAt',
            'dateExpiration',
        ];

        foreach ($tabProperties as $property) {
            $this->assertObjectHasAttribute($property, $agentTechniqueToken);
            ++$i;
        }

        $this->assertEquals($i, $nbAttribut);
    }

    public function testWebService()
    {
        $agentTechniqueToken = new AgentTechniqueToken();

        $agentTechniqueToken->setId(1);
        $this->assertEquals(1, $agentTechniqueToken->getId());

        $agentTechniqueToken->setToken('token');
        $this->assertEquals('token', $agentTechniqueToken->getToken());

        $agent = new Agent();
        $agent->setId(1);

        $agentTechniqueToken->setAgent($agent);
        $this->assertEquals($agent, $agentTechniqueToken->getAgent());

        $dateTime = new DateTime('now');
        $agentTechniqueToken->setDateExpiration($dateTime);
        $this->assertEquals($dateTime, $agentTechniqueToken->getDateExpiration());
    }
}
