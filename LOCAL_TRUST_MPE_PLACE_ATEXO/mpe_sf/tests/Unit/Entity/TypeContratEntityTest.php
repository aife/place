<?php

namespace Tests\Unit\Entity;

use App\Entity\TypeContrat;
use Tests\Unit\MpeEntityTestCase;

class TypeContratEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TypeContrat::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'idTypeContrat',
            'libelleTypeContrat',
            'abreviationTypeContrat',
            'typeContratStatistique',
            'multi',
            'accordCadreSad',
            'avecChapeau',
            'avecMontant',
            'modeEchangeChorus',
            'marcheSubsequent',
            'avecMontantMax',
            'ordreAffichage',
            'article133',
            'codeDume',
            'concession',
            'idExterne',
            'procedureCodes',
        ];
    }
}
