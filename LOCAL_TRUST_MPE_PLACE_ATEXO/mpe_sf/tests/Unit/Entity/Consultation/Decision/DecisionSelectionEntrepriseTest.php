<?php

namespace Tests\Unit\Entity\Consultation\Decision;

use App\Entity\Consultation\Decision\DecisionSelectionEntreprise;
use Tests\Unit\MpeEntityTestCase;

class DecisionSelectionEntrepriseTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return DecisionSelectionEntreprise::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'consultationRef',
            'organisme',
            'serviceId',
            'lot',
            'idEntreprise',
            'idEtablissement',
            'idOffre',
            'typeDepotReponse',
            'dateCreation',
            'dateModification',
            'idContratConstat',
            'consultationId',
        ];
    }
}
