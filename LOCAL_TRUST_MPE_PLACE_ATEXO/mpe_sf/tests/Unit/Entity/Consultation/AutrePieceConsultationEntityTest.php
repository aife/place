<?php

namespace Tests\Unit\Entity\Consultation;

use App\Entity\Consultation\AutrePieceConsultation;
use Tests\Unit\MpeEntityTestCase;

class AutrePieceConsultationEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return AutrePieceConsultation::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'blobId',
            'consultationId',
            'createdAt',
            'updatedAt',
        ];
    }

    public function testMethode()
    {
        $autrePieceConsultation = new AutrePieceConsultation();

        $autrePieceConsultation->setBlobId(1);
        $this->assertEquals(1, $autrePieceConsultation->getBlobId());

        $autrePieceConsultation->setConsultationId(1);
        $this->assertEquals(1, $autrePieceConsultation->getConsultationId());
    }
}
