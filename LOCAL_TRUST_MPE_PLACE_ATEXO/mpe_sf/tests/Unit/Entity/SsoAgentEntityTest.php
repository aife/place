<?php

namespace Tests\Unit\Entity;

use App\Entity\SsoAgent;
use Tests\Unit\MpeEntityTestCase;

class SsoAgentEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return SsoAgent::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'idSso',
            'idAgent',
            'organisme',
            'serviceId',
            'dateConnexion',
            'dateLastRequest',
        ];
    }
}
