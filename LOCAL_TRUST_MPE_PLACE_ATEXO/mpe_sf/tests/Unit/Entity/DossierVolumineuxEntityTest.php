<?php

namespace Tests\Unit\Entity;

use App\Entity\Agent;
use App\Entity\BlobFichier;
use App\Entity\Consultation;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Enveloppe;
use App\Entity\Inscrit;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\Unit\MpeEntityTestCase;

class DossierVolumineuxEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return DossierVolumineux::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'uuidReference',
            'nom',
            'taille',
            'dateCreation',
            'statut',
            'actif',
            'agent',
            'inscrit',
            'blobDescripteur',
            'blobLogfile',
            'consultations',
            'enveloppes',
            'uuidTechnique',
            'dateModification',
            'echanges',
        ];
    }

    public function testDossierVolumineux()
    {
        $dossiersVolumineux = new DossierVolumineux();

        $dossiersVolumineux->setNom('nom');
        $this->assertEquals('nom', $dossiersVolumineux->getNom());

        $dossiersVolumineux->setTaille('254654646');
        $this->assertEquals('254654646', $dossiersVolumineux->getTaille());

        $dateTime = new DateTime();
        $dossiersVolumineux->setDateCreation($dateTime);
        $this->assertEquals($dateTime, $dossiersVolumineux->getDateCreation());

        $dossiersVolumineux->setDateModification($dateTime);
        $this->assertEquals($dateTime, $dossiersVolumineux->getDateModification());

        $dossiersVolumineux->setStatut('init');
        $this->assertEquals('init', $dossiersVolumineux->getStatut());

        $dossiersVolumineux->setActif(true);
        $this->assertIsBool($dossiersVolumineux->isActif());

        $agent = new Agent();
        $dossiersVolumineux->setAgent($agent);
        $this->assertInstanceOf(Agent::class, $dossiersVolumineux->getAgent());

        $blob = new BlobFichier();
        $dossiersVolumineux->setBlobDescripteur($blob);
        $this->assertInstanceOf(BlobFichier::class, $dossiersVolumineux->getBlobDescripteur());

        $dossiersVolumineux->setblobLogfile($blob);
        $this->assertInstanceOf(BlobFichier::class, $dossiersVolumineux->getblobLogfile());

        $inscrit = new Inscrit();
        $dossiersVolumineux->setInscrit($inscrit);
        $this->assertInstanceOf(Inscrit::class, $dossiersVolumineux->getInscrit());

        $dossiersVolumineux->setUuidReference('3266-4654');
        $this->assertEquals('3266-4654', $dossiersVolumineux->getUuidReference());

        $dossiersVolumineux->setUuidTechnique('TECH-UUID-4566');
        $this->assertEquals('TECH-UUID-4566', $dossiersVolumineux->getUuidTechnique());

        $i = 0;
        while ($i <= 2) {
            $addConsultation = new Consultation();
            $dossiersVolumineux->addConsultation($addConsultation);
            ++$i;
        }

        $this->assertInstanceOf(ArrayCollection::class, $dossiersVolumineux->getConsultations());

        $i = 0;
        while ($i <= 2) {
            $addEnveloppe = new Enveloppe();
            $dossiersVolumineux->addEnveloppe($addEnveloppe);
            ++$i;
        }

        $this->assertInstanceOf(ArrayCollection::class, $dossiersVolumineux->getEnveloppes());
    }
}
