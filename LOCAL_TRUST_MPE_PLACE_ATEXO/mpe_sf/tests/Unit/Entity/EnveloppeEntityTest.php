<?php

namespace Tests\Unit\Entity;

use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\Unit\MpeEntityTestCase;

class EnveloppeEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Enveloppe::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'offre',
            'fichiers',
            'idEnveloppeElectro',
            'organisme',
            'offreId',
            'champsOptionnels',
            'fichier',
            'supprime',
            'cryptage',
            'nomFichier',
            'hash',
            'typeEnv',
            'sousPli',
            'attribue',
            'dateheureOuverture',
            'agentIdOuverture',
            'agentIdOuverture2',
            'donneesOuverture',
            'horodatageDonneesOuverture',
            'statutEnveloppe',
            'agentTelechargement',
            'dateTelechargement',
            'repertoireTelechargement',
            'nomAgentOuverture',
            'dateheureOuvertureAgent2',
            'enveloppeFictive',
            'uidResponse',
            'dossierVolumineux',
            'dateDebutDechiffrement',
        ];
    }

    public function testIdEnveloppeElectro()
    {
        $idEnveloppeElectro = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro($idEnveloppeElectro);
        $this->assertEquals($idEnveloppeElectro, $enveloppe->getIdEnveloppeElectro());
    }

    public function testOrganisme()
    {
        $organisme = 'pmi-min-1';
        $enveloppe = new Enveloppe();
        $enveloppe->setOrganisme($organisme);
        $this->assertEquals($organisme, $enveloppe->getOrganisme());
    }

    public function testOffreId()
    {
        $offreId = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setOffreId($offreId);
        $this->assertEquals($offreId, $enveloppe->getOffreId());
    }

    public function testChampsOptionnels()
    {
        $champsOptionnels = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setChampsOptionnels($champsOptionnels);
        $this->assertEquals($champsOptionnels, $enveloppe->getChampsOptionnels());
    }

    public function testFichier()
    {
        $fichier = 'fichier';
        $enveloppe = new Enveloppe();
        $enveloppe->setFichier($fichier);
        $this->assertEquals($fichier, $enveloppe->getFichier());
    }

    public function testSupprime()
    {
        $supprime = 'delete';
        $enveloppe = new Enveloppe();
        $enveloppe->setSupprime($supprime);
        $this->assertEquals($supprime, $enveloppe->getSupprime());
    }

    public function testCryptage()
    {
        $cryptage = 'crypt';
        $enveloppe = new Enveloppe();
        $enveloppe->setCryptage($cryptage);
        $this->assertEquals($cryptage, $enveloppe->getCryptage());
    }

    public function testNomFichier()
    {
        $nomFichier = 'nom';
        $enveloppe = new Enveloppe();
        $enveloppe->setNomFichier($nomFichier);
        $this->assertEquals($nomFichier, $enveloppe->getNomFichier());
    }

    public function testHash()
    {
        $hash = 'hash';
        $enveloppe = new Enveloppe();
        $enveloppe->setHash($hash);
        $this->assertEquals($hash, $enveloppe->getHash());
    }

    public function testTypeEnv()
    {
        $typeEnv = 'type';
        $enveloppe = new Enveloppe();
        $enveloppe->setTypeEnv($typeEnv);
        $this->assertEquals($typeEnv, $enveloppe->getTypeEnv());
    }

    public function testSousPli()
    {
        $typeEnv = 'sous';
        $enveloppe = new Enveloppe();
        $enveloppe->setSousPli($typeEnv);
        $this->assertEquals($typeEnv, $enveloppe->getSousPli());
    }

    public function testAttribue()
    {
        $attribue = 'attr';
        $enveloppe = new Enveloppe();
        $enveloppe->setAttribue($attribue);
        $this->assertEquals($attribue, $enveloppe->getAttribue());
    }

    public function testDateheureOuverture()
    {
        $dateheureOuverture = '2016-01-01 10:10:00';
        $enveloppe = new Enveloppe();
        $enveloppe->setDateheureOuverture($dateheureOuverture);
        $this->assertEquals($dateheureOuverture, $enveloppe->getDateheureOuverture());
    }

    public function testAgentIdOuverture()
    {
        $agentIdOuverture = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setAgentIdOuverture($agentIdOuverture);
        $this->assertEquals($agentIdOuverture, $enveloppe->getAgentIdOuverture());
    }

    public function testDateheureOuverture2()
    {
        $agentIdOuverture2 = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setAgentIdOuverture2($agentIdOuverture2);
        $this->assertEquals($agentIdOuverture2, $enveloppe->getAgentIdOuverture2());
    }

    public function testDonneesOuverture()
    {
        $donneesOuverture = 'données';
        $enveloppe = new Enveloppe();
        $enveloppe->setDonneesOuverture($donneesOuverture);
        $this->assertEquals($donneesOuverture, $enveloppe->getDonneesOuverture());
    }

    public function testHorodatageDonneesOuverture()
    {
        $horodatageDonneesOuverture = 'horodatage';
        $enveloppe = new Enveloppe();
        $enveloppe->setHorodatageDonneesOuverture($horodatageDonneesOuverture);
        $this->assertEquals($horodatageDonneesOuverture, $enveloppe->getHorodatageDonneesOuverture());
    }

    public function testStatutEnveloppe()
    {
        $statutEnveloppe = 'ok';
        $enveloppe = new Enveloppe();
        $enveloppe->setStatutEnveloppe($statutEnveloppe);
        $this->assertEquals($statutEnveloppe, $enveloppe->getStatutEnveloppe());
    }

    public function testAgentTelechargement()
    {
        $agentTelechargement = 1;
        $enveloppe = new Enveloppe();
        $enveloppe->setAgentTelechargement($agentTelechargement);
        $this->assertEquals($agentTelechargement, $enveloppe->getAgentTelechargement());
    }

    public function testDateTelechargement()
    {
        $dateTelechargement = '2018-02-02';
        $enveloppe = new Enveloppe();
        $enveloppe->setDateTelechargement($dateTelechargement);
        $this->assertEquals($dateTelechargement, $enveloppe->getDateTelechargement());
    }

    public function testRepertoireTelechargement()
    {
        $repertoireTelechargement = '/tmp/';
        $enveloppe = new Enveloppe();
        $enveloppe->setRepertoireTelechargement($repertoireTelechargement);
        $this->assertEquals($repertoireTelechargement, $enveloppe->getRepertoireTelechargement());
    }

    public function testNomAgentOuverture()
    {
        $nomAgentOuverture = 'Flo';
        $enveloppe = new Enveloppe();
        $enveloppe->setNomAgentOuverture($nomAgentOuverture);
        $this->assertEquals($nomAgentOuverture, $enveloppe->getNomAgentOuverture());
    }

    public function testDateheureOuvertureAgent2()
    {
        $dateheureOuvertureAgent2 = '2015-02-02 10:10:02';
        $enveloppe = new Enveloppe();
        $enveloppe->setDateheureOuvertureAgent2($dateheureOuvertureAgent2);
        $this->assertEquals($dateheureOuvertureAgent2, $enveloppe->getDateheureOuvertureAgent2());
    }

    public function testDateDebutDechiffrement()
    {
        $dateDebutDechiffrement = '2015-02-02 10:10:02';
        $enveloppe = new Enveloppe();
        $enveloppe->setDateDebutDechiffrement($dateDebutDechiffrement);
        $this->assertEquals($dateDebutDechiffrement, $enveloppe->getDateDebutDechiffrement());
    }

    public function testOffre()
    {
        $offre = new Offre();
        $enveloppe = new Enveloppe();
        $enveloppe->setOffre($offre);
        $this->assertEquals($offre, $enveloppe->getOffre());
    }

    public function testUidResponse()
    {
        $uidResponse = 'uid';
        $enveloppe = new Enveloppe();
        $enveloppe->setUidResponse($uidResponse);
        $this->assertEquals($uidResponse, $enveloppe->getUidResponse());
    }

    public function testEnveloppeFictive()
    {
        $enveloppeFictive = 'fictive';
        $enveloppe = new Enveloppe();
        $enveloppe->setEnveloppeFictive($enveloppeFictive);
        $this->assertEquals($enveloppeFictive, $enveloppe->getEnveloppeFictive());
    }

    public function testFichiers()
    {
        $container = $this->createMock(ContainerInterface::class);
        $enveloppeFichier = new EnveloppeFichier($container);
        $arrayCollection = new ArrayCollection();
        $arrayCollection->set('0', $enveloppeFichier);

        $enveloppe = new Enveloppe();
        $enveloppe->addFichierEnveloppe($enveloppeFichier);
        $this->assertEquals($arrayCollection, $enveloppe->getFichiers());
        $this->assertEquals($arrayCollection, $enveloppe->getFichierEnveloppes());

        $arrayCollection->remove(0);
        $enveloppe->removeFichierEnveloppe($enveloppeFichier);
        $this->assertEquals($arrayCollection, $enveloppe->getFichiers());
    }
}
