<?php

namespace Tests\Unit\Entity\Contrat;

use App\Entity\Contrat\NumerotationAutomatique;
use Tests\Unit\MpeEntityTestCase;

class NumerotationAutomatiqueTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return NumerotationAutomatique::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organisme',
            'serviceId',
            'annee',
            'contratMulti',
            'contratSADynamique',
            'contratTitulaire',
        ];
    }
}
