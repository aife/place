<?php

namespace Tests\Unit\Entity;

use App\Entity\Administrateur;
use Tests\Unit\MpeEntityTestCase;

class AdministrateurTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $administrateur = new Administrateur();

        $administrateur->setEmail('test@atexo.com');
        $this->assertEquals('test@atexo.com', $administrateur->getEmail());

        $administrateur->setAdminGeneral('admin_test');
        $this->assertEquals('admin_test', $administrateur->getAdminGeneral());

        $administrateur->setCertificat('certificat');
        $this->assertEquals('certificat', $administrateur->getCertificat());

        $administrateur->setLogin('admin_atexo');
        $this->assertEquals('admin_atexo', $administrateur->getLogin());

        $administrateur->setMdp('password');
        $this->assertEquals('password', $administrateur->getMdp());

        $administrateur->setNom('nom');
        $this->assertEquals('nom', $administrateur->getNom());

        $administrateur->setOriginalLogin('original_login');
        $this->assertEquals('original_login', $administrateur->getOriginalLogin());

        $administrateur->setPrenom('prenom');
        $this->assertEquals('prenom', $administrateur->getPrenom());

        $administrateur->setTentativesMdp('0');
        $this->assertEquals('0', $administrateur->getTentativesMdp());
    }

    protected function getTestedEntity()
    {
        return Administrateur::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'originalLogin',
            'login',
            'certificat',
            'mdp',
            'nom',
            'prenom',
            'email',
            'organisme',
            'adminGeneral',
            'tentativesMdp',
            'habilitationAdministrateurs',
        ];
    }
}
