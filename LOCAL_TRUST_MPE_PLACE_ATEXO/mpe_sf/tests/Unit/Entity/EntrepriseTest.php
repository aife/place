<?php

namespace Tests\Unit\Entity;

use App\Entity\Entreprise;
use Tests\Unit\MpeEntityTestCase;

class EntrepriseTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Entreprise::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
//            'bourses',
            'inscrits',
            'etablissements',
            'documents',
            'responsableengagements',
            'id',
            'adminId',
            'siren',
            'repmetiers',
            'nom',
            'adresse',
            'codepostal',
            'villeadresse',
            'paysadresse',
            'email',
            'taille',
            'formejuridique',
            'villeenregistrement',
            'motifnonindnum',
            'ordreprofouagrement',
            'dateconstsociete',
            'nomorginscription',
            'adrorginscription',
            'dateconstassoc',
            'dateconstassocetrangere',
            'nompersonnepublique',
            'nationalite',
            'redressement',
            'paysenregistrement',
            'sirenetranger',
            'numassoetrangere',
            'debutexerciceglob1',
            'finexerciceglob1',
            'debutexerciceglob2',
            'finexerciceglob2',
            'debutexerciceglob3',
            'finexerciceglob3',
            'ventesglob1',
            'ventesglob2',
            'ventesglob3',
            'biensglob1',
            'biensglob2',
            'biensglob3',
            'servicesglob1',
            'servicesglob2',
            'servicesglob3',
            'totalglob1',
            'totalglob2',
            'totalglob3',
            'codeape',
            'libelleApe',
            'origineCompte',
            'telephone',
            'fax',
            'siteInternet',
            'descriptionActivite',
            'activiteDomaineDefense',
            'anneeClotureExercice1',
            'anneeClotureExercice2',
            'anneeClotureExercice3',
            'effectifMoyen1',
            'effectifMoyen2',
            'effectifMoyen3',
            'effectifEncadrement1',
            'effectifEncadrement2',
            'effectifEncadrement3',
            'pme1',
            'pme2',
            'pme3',
            'adresse2',
            'nicSiege',
            'acronymePays',
            'dateCreation',
            'dateModification',
            'idInitial',
            'region',
            'province',
            'telephone2',
            'telephone3',
            'cnss',
            'rcNum',
            'rcVille',
            'domainesActivites',
            'numTax',
            'documentsCommerciaux',
            'intituleDocumentsCommerciaux',
            'tailleDocumentsCommerciaux',
            'qualification',
            'agrement',
            'moyensTechnique',
            'moyensHumains',
            'compteActif',
            'capitalSocial',
            'ifu',
            'idAgentCreateur',
            'nomAgent',
            'prenomAgent',
            'adressesElectroniques',
            'visibleBourse',
            'typeCollaboration',
            'entrepriseEa',
            'entrepriseSiae',
            'saisieManuelle',
            'createdFromDecision',
            'idCodeEffectif',
            'siretSiegeSocial',
            'formejuridiqueCode',
            'nombreEtablissementsActifs',
            'nomCommercial',
            'procedureCollective',
            'raisonSociale',
            'exercices',
            'mandataires',
            'etablissementSiege',
            'categorieEntreprise',
            'dateCessation',
            'etatAdministratif',
            'idExterne',
            'pieceGeneree',
            'updateDateApiGouv'
        ];
    }
}
