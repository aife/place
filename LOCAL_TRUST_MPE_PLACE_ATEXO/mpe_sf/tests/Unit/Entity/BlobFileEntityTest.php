<?php

namespace Tests\Unit\Entity;

use App\Entity\BlobFichier;
use DateTime;
use Tests\Unit\MpeEntityTestCase;

class BlobFileEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return BlobFichier::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'name',
            'deletionDatetime',
            'statutSynchro',
            'hash',
            'dossier',
            'chemin',
            'dossierVolumineuxDescripteur',
            'dossierVolumineuxLogfile',
            'oldId',
            'extension',
        ];
    }

    public function testMethode()
    {
        $blobFichier = new BlobFichier();

        $blobFichier->setId(1);
        $this->assertEquals(1, $blobFichier->getId());

        $blobFichier->setName('nom-fichier');
        $this->assertEquals('nom-fichier', $blobFichier->getName());

        $datetime = new DateTime();
        $blobFichier->setDeletionDatetime($datetime);
        $this->assertEquals($datetime, $blobFichier->getDeletionDatetime());

        $blobFichier->setStatutSynchro(0);
        $this->assertEquals(0, $blobFichier->getStatutSynchro());

        $blobFichier->setHash('qskhfkqsfksh');
        $this->assertEquals('qskhfkqsfksh', $blobFichier->getHash());
    }
}
