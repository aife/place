<?php

namespace Tests\Unit\Entity\Chorus;

use App\Entity\Chorus\ChorusGroupementAchat;
use Tests\Unit\MpeEntityTestCase;

class ChorusGroupementAchatTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ChorusGroupementAchat::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organisme',
            'idOa',
            'libelle',
            'code',
            'actif',
            'serviceId',
        ];
    }
}
