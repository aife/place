<?php

namespace Tests\Unit\Entity;

use App\Entity\InvitationConsultationTransverse;
use Tests\Unit\MpeEntityTestCase;

class InvitationConsultationTransverseTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return InvitationConsultationTransverse::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organismeEmetteur',
            'reference',
            'organismeInvite',
            'lot',
            'dateDecision',
            'idContratTitulaire',
            'ConsultationId',
        ];
    }
}
