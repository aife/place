<?php

namespace Tests\Unit\Entity;

use App\Entity\Consultation\ConsultationFavoris;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class ConsultationFavorisEntityTest.
 */
class ConsultationFavorisEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ConsultationFavoris::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'consultation',
            'agent',
        ];
    }
}
