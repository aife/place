<?php

namespace Tests\Unit\Entity;

use App\Entity\Procedure\TypeProcedureOrganisme;
use Tests\Unit\MpeEntityTestCase;

class TypeProcedureOrganismeEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity(): string
    {
        return TypeProcedureOrganisme::class;
    }

    protected function getIgnoredObjectAttributes(): array
    {
        return [
            'consultations',
            'procedurePortailType',
            'organismes',
        ];
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id_type_procedure',
            'organisme',
            'libelle_type_procedure',
            'abbreviation',
            'type_boamp',
            'id_type_procedure_portail',
            'categorie_procedure',
            'delai_alerte',
            'id_type_validation',
            'service_validation',
            'mapa',
            'activer_mapa',
            'libelle_type_procedure_fr',
            'libelle_type_procedure_en',
            'libelle_type_procedure_es',
            'libelle_type_procedure_su',
            'libelle_type_procedure_du',
            'libelle_type_procedure_cz',
            'libelle_type_procedure_ar',
            'id_montant_mapa',
            'code_recensement',
            'depouillable_phase_consultation',
            'consultation_transverse',
            'tag_Boamp',
            'ao',
            'mn',
            'dc',
            'autre',
            'sad',
            'accord_cadre',
            'pn',
            'tag_name_mesure_avancement',
            'abreviation_interface',
            'libelle_type_procedure_it',
            'publicite_types_form_xml',
            'tag_name_chorus',
            'equivalent_opoce',
            'equivalent_boamp',
            'ordre_affichage',
            'id_externe',
            'procedureSimplifie',
            'typeContratEtProcedure',
        ];
    }
}
