<?php

namespace Tests\Unit\Entity;

use App\Entity\HistoriqueSuppressionAgent;
use Tests\Unit\MpeEntityTestCase;

class HistoriqueSuppressionAgentEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return HistoriqueSuppressionAgent::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'idAgentSuppresseur',
            'serviceId',
            'nom',
            'prenom',
            'email',
            'organisme',
            'dateSuppression',
            'idAgentSupprime',
        ];
    }
}
