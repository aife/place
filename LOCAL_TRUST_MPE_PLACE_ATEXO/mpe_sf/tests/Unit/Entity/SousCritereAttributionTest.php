<?php

namespace Tests\Unit\Entity;

use App\Entity\Consultation\SousCritereAttribution;
use Tests\Unit\MpeEntityTestCase;

class SousCritereAttributionTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return SousCritereAttribution::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'enonce',
            'ponderation',
            'idCritereAttribution',
            'critereAttribution',
        ];
    }
}
