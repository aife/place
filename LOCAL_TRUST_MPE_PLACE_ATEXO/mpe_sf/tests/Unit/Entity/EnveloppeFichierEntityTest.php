<?php

namespace Tests\Unit\Entity;

use App\Entity\BloborganismeFile;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\EnveloppeFichierBloc;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\Unit\MpeEntityTestCase;

class EnveloppeFichierEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return EnveloppeFichier::class;
    }

    protected function getTestedEntityConstructorArguments(): array
    {
        return [ContainerInterface::class];
    }

    protected function getIgnoredObjectAttributes(): array
    {
        return [
            'blocsFichiersEnveloppes',
            'fichierSignature',
            'statutSignature',
            'container',
            'enveloppe',
            'blob',
            'idBlob',
            'blobSignature',
        ];
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id_fichier',
            'organisme',
            'id_enveloppe',
            'type_fichier',
            'num_ordre_fichier',
            'nom_fichier',
            'taille_fichier',
            'signature_fichier',
            'hash',
            'verification_certificat',
            'id_blob_signature',
            'type_piece',
            'id_type_piece',
            'is_hash',
            'nom_referentiel_certificat',
            'statut_referentiel_certificat',
            'nom_referentiel_fonctionnel',
            'message',
            'date_signature',
            'signature_infos',
            'signature_infos_date',
            'id_fichier_signature',
            'uid_response',
            'type_signature_fichier',
            'hash256',
            'resultat_verification_hash',
            'historiquePurge',
        ];
    }

    public function testIdFichier()
    {
        $idFichier = 10;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setIdFichier($idFichier);
        $this->assertEquals($idFichier, $fichierEnveloppe->getIdFichier());
    }

    public function testOrganisme()
    {
        $organisme = 'pmi-min-1';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setOrganisme($organisme);
        $this->assertEquals($organisme, $fichierEnveloppe->getOrganisme());
    }

    public function testIdEnveloppe()
    {
        $idEnveloppe = 1;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setIdEnveloppe($idEnveloppe);
        $this->assertEquals($idEnveloppe, $fichierEnveloppe->getIdEnveloppe());
    }

    public function testTypeFichier()
    {
        $typeFichier = 'pdf';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setTypeFichier($typeFichier);
        $this->assertEquals($typeFichier, $fichierEnveloppe->getTypeFichier());
    }

    public function testNumOrdreFichier()
    {
        $numOrdreFichier = 1;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setNumOrdreFichier($numOrdreFichier);
        $this->assertEquals($numOrdreFichier, $fichierEnveloppe->getNumOrdreFichier());
    }

    public function testNumFichier()
    {
        $nomFichier = 'fichierTest';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setNomFichier($nomFichier);
        $this->assertEquals($nomFichier, $fichierEnveloppe->getNomFichier());
    }

    public function testTailleFichier()
    {
        $tailleFichier = 'fichierTest';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setTailleFichier($tailleFichier);
        $this->assertEquals($tailleFichier, $fichierEnveloppe->getTailleFichier());
    }

    public function testSignatureFichier()
    {
        $signatureFichier = 'signature';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setSignatureFichier($signatureFichier);
        $this->assertEquals($signatureFichier, $fichierEnveloppe->getSignatureFichier());
    }

    public function testHash()
    {
        $hash = 'hash';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setHash($hash);
        $this->assertEquals($hash, $fichierEnveloppe->getHash());
    }

    public function testVerificationCertificat()
    {
        $hash = 'ok';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setVerificationCertificat($hash);
        $this->assertEquals($hash, $fichierEnveloppe->getVerificationCertificat());
    }

    public function testIdBlob()
    {
        $idBlob = 1;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $blob = new BloborganismeFile();
        $class = new ReflectionClass($blob);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($blob, $idBlob);

        $fichierEnveloppe->setBlob($blob);
        $this->assertEquals($idBlob, $fichierEnveloppe->getBlob()->getId());
    }

    public function testIdBlobSignature()
    {
        $idBlobSignature = 1;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setIdBlobSignature($idBlobSignature);
        $this->assertEquals($idBlobSignature, $fichierEnveloppe->getIdBlobSignature());
    }

    public function testTypePiece()
    {
        $typePiece = 'test';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setTypePiece($typePiece);
        $this->assertEquals($typePiece, $fichierEnveloppe->getTypePiece());
    }

    public function testIdTypePiece()
    {
        $idTypePiece = 1;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setIdTypePiece($idTypePiece);
        $this->assertEquals($idTypePiece, $fichierEnveloppe->getIdTypePiece());
    }

    public function testIsHash()
    {
        $isHash = true;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setIsHash($isHash);
        $this->assertEquals($isHash, $fichierEnveloppe->getIsHash());
    }

    public function testNomReferentielCertificat()
    {
        $nomReferentielCertificat = 'ref';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setNomReferentielCertificat($nomReferentielCertificat);
        $this->assertEquals($nomReferentielCertificat, $fichierEnveloppe->getNomReferentielCertificat());
    }

    public function testStatutReferentielCertificat()
    {
        $statutReferentielCertificat = 123;
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setStatutReferentielCertificat($statutReferentielCertificat);
        $this->assertEquals($statutReferentielCertificat, $fichierEnveloppe->getStatutReferentielCertificat());
    }

    public function testNomReferentielFonctionnel()
    {
        $nomReferentielFonctionnel = 'fon';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setNomReferentielFonctionnel($nomReferentielFonctionnel);
        $this->assertEquals($nomReferentielFonctionnel, $fichierEnveloppe->getNomReferentielFonctionnel());
    }

    public function testMessage()
    {
        $message = 'message';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setMessage($message);
        $this->assertEquals($message, $fichierEnveloppe->getMessage());
    }

    public function testDateSignature()
    {
        $dateSignature = '2018-02-01';
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setDateSignature($dateSignature);
        $this->assertEquals($dateSignature, $fichierEnveloppe->getDateSignature());
    }

    public function testBlocFichierEnveloppe()
    {
        $blocFichierEnveloppe = new EnveloppeFichierBloc();
        $arrayCollection = new ArrayCollection();
        $arrayCollection->set('0', $blocFichierEnveloppe);
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->addBlocFichierEnveloppe($blocFichierEnveloppe);
        $this->assertEquals($arrayCollection, $fichierEnveloppe->getBlocFichierEnveloppes());

        $arrayCollection->remove(0);
        $fichierEnveloppe->removeBlocFichierEnveloppe($blocFichierEnveloppe);
        $this->assertEquals($arrayCollection, $fichierEnveloppe->getBlocFichierEnveloppes());
    }

    public function testEnveloppe()
    {
        $enveloppe = new Enveloppe();
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierEnveloppe = new EnveloppeFichier($containerMock);
        $fichierEnveloppe->setEnveloppe($enveloppe);
        $this->assertEquals($enveloppe, $fichierEnveloppe->getEnveloppe());
    }
}
