<?php

namespace Tests\Unit\Entity;

use App\Entity\TypeContratConcessionPivot;
use Tests\Unit\MpeEntityTestCase;

class TypeContratConcessionPivotEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TypeContratConcessionPivot::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'libelle',
            'contratTitulaires',
        ];
    }
}
