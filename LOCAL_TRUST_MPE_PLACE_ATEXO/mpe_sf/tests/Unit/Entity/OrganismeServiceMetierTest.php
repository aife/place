<?php

namespace Tests\Unit\Entity;

use App\Entity\OrganismeServiceMetier;
use Tests\Unit\MpeEntityTestCase;

class OrganismeServiceMetierTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return OrganismeServiceMetier::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'organisme',
            'idServiceMetier',
        ];
    }
}
