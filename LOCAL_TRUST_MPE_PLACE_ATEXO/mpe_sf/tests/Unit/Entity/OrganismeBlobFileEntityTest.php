<?php

namespace Tests\Unit\Entity;

use App\Entity\BloborganismeFile;
use Tests\Unit\MpeEntityTestCase;

class OrganismeBlobFileEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return BloborganismeFile::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'organisme',
            'name',
            'deletionDatetime',
            'chemin',
            'dossier',
            'hash',
            'statutSynchro',
            'oldId',
            'extension',
            'file'
        ];
    }
}
