<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\ModificationContrat;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\WS\WebService;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\Unit\MpeEntityTestCase;

class AgentEntityTest extends MpeEntityTestCase
{
    public function testMethods()
    {
        $agent = new Agent();

        $agent->setId(1);
        $this->assertEquals(1, $agent->getId());

        $organisme = new Organisme();
        $organisme->setAcronyme('test');
        $agent->setOrganisme($organisme);
        $this->assertEquals($organisme, $agent->getOrganisme());

        $agent->setAcronymeOrganisme('test');
        $this->assertEquals('test', $agent->getAcronymeOrganisme());

        $agent->setActif(true);
        $this->assertEquals(true, $agent->getActif());

        $agent->setAdrPostale('test');
        $this->assertEquals('test', $agent->getAdrPostale());

        $agent->setAlerteChorus(true);
        $this->assertEquals(true, $agent->getAlerteChorus());

        $agent->setAlerteClotureConsultation(true);
        $this->assertEquals(true, $agent->getAlerteClotureConsultation());

        $agent->setAlerteCreationModificationAgent(true);
        $this->assertEquals(true, $agent->getAlerteCreationModificationAgent());

        $agent->setAlerteEchecPublicationBoamp(true);
        $this->assertEquals(true, $agent->getAlerteEchecPublicationBoamp());

        $agent->setAlertePublicationBoamp(true);
        $this->assertEquals(true, $agent->getAlertePublicationBoamp());

        $agent->setAlerteQuestionEntreprise(true);
        $this->assertEquals(true, $agent->getAlerteQuestionEntreprise());

        $agent->setAlerteReponseElectronique(true);
        $this->assertEquals(true, $agent->getAlerteReponseElectronique());

        $agent->setTypeComm(true);
        $this->assertEquals(true, $agent->getTypeComm());

        $agent->setTentativesMdp(1);
        $this->assertEquals(1, $agent->getTentativesMdp());

        $agent->setTelephone(1);
        $this->assertEquals(1, $agent->getTelephone());

        $agent->setServiceId(1);
        $this->assertEquals(1, $agent->getServiceId());

        $service = new Service();
        $service->setOrganisme($organisme);
        $agent->setService($service);
        $this->assertEquals($service, $agent->getService());

        $agent->setRecevoirMail(1);
        $this->assertEquals(1, $agent->getRecevoirMail());

        $agent->setPrenom('test');
        $this->assertEquals('test', $agent->getPrenom());

        $agent->setPassword(1);
        $this->assertEquals(1, $agent->getPassword());

        $agent->setNumTel(1);
        $this->assertEquals(1, $agent->getNumTel());

        $agent->setNumFax(1);
        $this->assertEquals(1, $agent->getNumFax());

        $agent->setNumCertificat(1);
        $this->assertEquals(1, $agent->getNumCertificat());

        $agent->setNomFonction(1);
        $this->assertEquals(1, $agent->getNomFonction());

        $agent->setTechnique(true);
        $this->assertEquals(true, $agent->isTechnique());

        $agent->setMdp(1);
        $this->assertEquals(1, $agent->getMdp());

        $agent->setLogin(1);
        $this->assertEquals(1, $agent->getLogin());

        $agent->setLieuExecution(1);
        $this->assertEquals(1, $agent->getLieuExecution());

        $agent->setIdProfilSocleExterne(1);
        $this->assertEquals(1, $agent->getIdProfilSocleExterne());

        $agent->setIdExterne(1);
        $this->assertEquals(1, $agent->getIdExterne());

        $habilitation = new HabilitationAgent();
        $agent->setHabilitation($habilitation);
        $this->assertEquals($habilitation, $agent->getHabilitation());

        $agent->addHabilitation($habilitation);
        $this->assertEquals([$habilitation], $agent->getHabilitations());

        $agent->setElu(1);
        $this->assertEquals(1, $agent->getElu());

        $agent->setCodeTheme(1);
        $this->assertEquals(1, $agent->getCodeTheme());

        $agent->setCodesNuts(1);
        $this->assertEquals(1, $agent->getCodesNuts());

        $agent->setCivilite(1);
        $this->assertEquals(1, $agent->getCivilite());

        $agent->setCertificat(1);
        $this->assertEquals(1, $agent->getCertificat());

        $agent->setAlerteReceptionMessage(1);
        $this->assertEquals(1, $agent->getAlerteReceptionMessage());

        $agent->setDateCreation(1);
        $this->assertEquals(1, $agent->getDateCreation());

        $agent->setDateModification(1);
        $this->assertEquals(1, $agent->getDateModification());

        $agent->setAlerteValidationConsultation(1);
        $this->assertEquals(1, $agent->getAlerteValidationConsultation());

        $modificationContrat = new ModificationContrat();
        $collection = new ArrayCollection();
        $collection->add($modificationContrat);
        $agent->addModificationContrat($modificationContrat);
        $this->assertEquals($collection, $agent->getModificationContrats());

        $webServiceCollection = new ArrayCollection();
        $webService = new WebService();
        $webService->setId(1);
        $webServiceCollection->add($webService);

        $agent->addWebservice($webService);
        $this->assertEquals($webServiceCollection, $agent->getWebservices());
    }

    protected function getTestedEntity()
    {
        return Agent::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'habilitations',
            'socleHabilitations',
            'organisme',
            'service',
            'modificationContrats',
            'habilitation',
            'id',
            'login',
            'email',
            'mdp',
            'certificat',
            'nom',
            'prenom',
            'tentativesMdp',
            'acronymeOrganisme',
            'serviceId',
            'recevoirMail',
            'elu',
            'nomFonction',
            'numTel',
            'numFax',
            'typeComm',
            'adrPostale',
            'civilite',
            'alerteReponseElectronique',
            'alerteClotureConsultation',
            'alerteReceptionMessage',
            'alertePublicationBoamp',
            'dateCreation',
            'alerteEchecPublicationBoamp',
            'alerteCreationModificationAgent',
            'dateModification',
            'idExterne',
            'idProfilSocleExterne',
            'lieuExecution',
            'alerteQuestionEntreprise',
            'actif',
            'deletedAt',
            'codesNuts',
            'numCertificat',
            'alerteValidationConsultation',
            'alerteChorus',
            'password',
            'codeTheme',
            'technique',
            'webservices',
            'dateConnexion',
            'typeHash',
            'dossierVolumineux',
            'consultationFavoris',
            'alerteMesConsultations',
            'alerteConsultationsMonEntite',
            'alerteConsultationsDesEntitesDependantes',
            'alerteConsultationsMesEntitesTransverses',
            'annexesFinanciereManuel',
            'dateValidationRgpd',
            'rgpdCommunicationPlace',
            'rgpdEnquete',
            'consultations',
            'referenceConsultationAgentTechniqueCreateur'
        ];
    }

    /**
     * @return string[]
     */
    protected function getExtendedFields(): array
    {
        return [
            'dateValidationRgpd',
            'rgpdCommunicationPlace',
            'rgpdEnquete'
        ];
    }
}
