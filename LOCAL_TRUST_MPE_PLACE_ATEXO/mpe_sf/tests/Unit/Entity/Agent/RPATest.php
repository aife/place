<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent\RPA;
use Tests\Unit\MpeEntityTestCase;

class RPATest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return RPA::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'acronymeOrg',
            'organisme',
            'nom',
            'prenom',
            'adresse1',
            'adresse2',
            'codepostal',
            'ville',
            'serviceId',
            'fonction',
            'pays',
            'dateCreation',
            'dateModification',
            'responsableArchive',
            'email',
            'telephone',
            'fax',
        ];
    }
}
