<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent\TireurPlan;
use Tests\Unit\MpeEntityTestCase;

class TireurPlanTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TireurPlan::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organisme',
            'nom',
            'email',
            'serviceId',
        ];
    }
}
