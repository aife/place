<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Entity\Agent\Habilitations;

use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use App\Entity\HabilitationAgent;
use Jaspersoft\Tool\RESTRequest;
use PHPUnit\Framework\TestCase;

class HabilitationAgentTest extends TestCase
{
    public function testHabilitationAgent()
    {
        $habilitationAgent = new HabilitationAgent();
        $recensementProgrammationStrategieAchat = new RecensementProgrammationStrategieAchat();
        $recensementProgrammationStrategieAchat->setStrategieAchatGestion(true);
        $habilitationAgent->setRecensementProgrammationStrategieAchat($recensementProgrammationStrategieAchat);

        $recensementProgrammationStrategieAchatReference = new RecensementProgrammationStrategieAchat();
        $recensementProgrammationStrategieAchatReference
            ->setStrategieAchatGestion(true);

        $this->assertEquals(
            $recensementProgrammationStrategieAchatReference,
            $habilitationAgent->getRecensementProgrammationStrategieAchat()
        );
    }
}
