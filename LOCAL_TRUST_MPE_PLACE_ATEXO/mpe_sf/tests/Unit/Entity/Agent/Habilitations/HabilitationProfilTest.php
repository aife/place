<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Entity\Agent\Habilitations;

use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use App\Entity\HabilitationProfil;
use PHPUnit\Framework\TestCase;

class HabilitationProfilTest extends TestCase
{
    public function testHabilitationAgent()
    {
        $habilitationProfil = new HabilitationProfil();
        $recensementProgrammationStrategieAchat = new RecensementProgrammationStrategieAchat();
        $recensementProgrammationStrategieAchat->setStrategieAchatGestion(true);
        $habilitationProfil->setRecensementProgrammationStrategieAchat($recensementProgrammationStrategieAchat);

        $recensementProgrammationStrategieAchatReference = new RecensementProgrammationStrategieAchat();
        $recensementProgrammationStrategieAchatReference
            ->setStrategieAchatGestion(true);

        $this->assertEquals(
            $recensementProgrammationStrategieAchatReference,
            $habilitationProfil->getRecensementProgrammationStrategieAchat()
        );
    }
}
