<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent\VisionRmaAgentOrganisme;
use Tests\Unit\MpeEntityTestCase;

class VisionRmaAgentOrganismeTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return VisionRmaAgentOrganisme::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'idAgent',
            'acronyme',
        ];
    }
}
