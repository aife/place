<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent\AgentServiceMetier;
use Tests\Unit\MpeEntityTestCase;

class AgentServiceMetierTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return AgentServiceMetier::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'idAgent',
            'idServiceMetier',
            'idProfilService',
            'dateCreation',
            'dateModification',
        ];
    }
}
