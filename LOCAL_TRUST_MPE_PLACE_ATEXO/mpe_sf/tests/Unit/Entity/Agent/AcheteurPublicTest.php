<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\Agent\AcheteurPublic;
use Tests\Unit\MpeEntityTestCase;

class AcheteurPublicTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return AcheteurPublic::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'organisme',
            'denomination',
            'prm',
            'adresse',
            'cp',
            'ville',
            'dept',
            'typeOrg',
            'telephone',
            'fax',
            'mail',
            'url',
            'urlAcheteur',
            'factureNumero',
            'factureCode',
            'factureDenomination',
            'facturationService',
            'factureAdresse',
            'factureCp',
            'factureVille',
            'facturePays',
            'idAapc',
            'boampLogin',
            'boampPassword',
            'boampMail',
            'boampTarget',
            'defaultFormValues',
            'defautFormAmBoamp',
            'defautFormAmBoampJoue',
            'defautFormMapaBoamp',
            'defautFormAconcours',
            'defautFormAsBoampJoue',
            'defautFormAaBoamp',
            'defautFormArMapaBoamp',
            'defautForm05Boamp',
            'defautFormRect',
            'defautFormAaBoampJoue',
            'serviceId',
            'livraisonService',
            'livraisonAdresse',
            'livraisonCodePostal',
            'livraisonVille',
            'livraisonPays',
            'typePouvoirActivite',
            'codeNuts',
            'modalitesFinancement',
            'moniteurProvenance',
            'codeAccesLogiciel',
            'departementMiseEnLigne',
            'referenceCommande',
            'token'
        ];
    }
}
