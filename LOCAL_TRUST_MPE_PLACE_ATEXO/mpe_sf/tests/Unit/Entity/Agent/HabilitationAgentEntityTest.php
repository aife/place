<?php

namespace Tests\Unit\Entity\Agent;

use App\Entity\HabilitationAgent;
use ArrayObject;
use PHPUnit\Framework\TestCase;

class HabilitationAgentEntityTest extends TestCase
{
    /**
     * Test qui vérifie la présence des attributs dans l'objet HabilitationAgent.
     */
    public function testAttributesHabilitationAgent()
    {
        $i = 0;
        $agent = new HabilitationAgent();

        $arrayobj = new ArrayObject((array) $agent);
        $nbAttribut = $arrayobj->count();

        $tabProperties = [
            'gestionAgentPole',
            'gestionFournisseursEnvoisPostaux',
            'gestionBiCles',
            'accesWs',
        ];

        foreach ($tabProperties as $property) {
            $this->assertObjectHasAttribute($property, $agent);
            //$i++;
        }
        /*
         * Je désactive ce test car on a 175 attribue a reprendre :(
         $this->assertEquals($i, $nbAttribut);
        */
    }

    public function testMethode()
    {
        $habilitationAgent = new HabilitationAgent();

        $habilitationAgent->setAccesWs(true);
        $this->assertEquals(true, $habilitationAgent->isAccesWs());
    }
}
