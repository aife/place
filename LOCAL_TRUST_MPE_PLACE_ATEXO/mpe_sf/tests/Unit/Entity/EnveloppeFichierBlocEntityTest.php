<?php

namespace Tests\Unit\Entity;

use App\Entity\EnveloppeFichier;
use App\Entity\EnveloppeFichierBloc;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Tests\Unit\MpeEntityTestCase;

class EnveloppeFichierBlocEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return EnveloppeFichierBloc::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'fichierEnveloppe',
            'idBlocFichier',
            'organisme',
            'idFichier',
            'numeroOrdreBloc',
            'idBlobChiffre',
            'idBlobDechiffre',
            'blobOrganismeChiffre',
        ];
    }

    public function testIdBlocFichier()
    {
        $idBlocFichier = 10;
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setIdBlocFichier($idBlocFichier);
        $this->assertEquals($idBlocFichier, $EnveloppeFichierBloc->getIdBlocFichier());
    }

    public function testOrganisme()
    {
        $organisme = 'pmi-min-1';
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setOrganisme($organisme);
        $this->assertEquals($organisme, $EnveloppeFichierBloc->getOrganisme());
    }

    public function testIdFichier()
    {
        $idFichier = 1;
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setIdFichier($idFichier);
        $this->assertEquals($idFichier, $EnveloppeFichierBloc->getIdFichier());
    }

    public function testNumeroOrdreBloc()
    {
        $numeroOrdreBloc = 1;
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setNumeroOrdreBloc($numeroOrdreBloc);
        $this->assertEquals($numeroOrdreBloc, $EnveloppeFichierBloc->getNumeroOrdreBloc());
    }

    public function testIdBlobChiffre()
    {
        $idBlobChiffre = 1;
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setIdBlobChiffre($idBlobChiffre);
        $this->assertEquals($idBlobChiffre, $EnveloppeFichierBloc->getIdBlobChiffre());
    }

    public function testIdBlobDechiffre()
    {
        $idBlobDechiffre = 1;
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setIdBlobDechiffre($idBlobDechiffre);
        $this->assertEquals($idBlobDechiffre, $EnveloppeFichierBloc->getIdBlobDechiffre());
    }

    public function testFichierEnveloppe()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $fichierenveloppe = new EnveloppeFichier($containerMock);
        $EnveloppeFichierBloc = new EnveloppeFichierBloc();
        $EnveloppeFichierBloc->setFichierEnveloppe($fichierenveloppe);
        $this->assertEquals($fichierenveloppe, $EnveloppeFichierBloc->getFichierEnveloppe());
    }
}
