<?php

namespace Tests\Unit\Entity;

use App\Entity\Consultation\ConsultationComptePub;
use Tests\Unit\MpeEntityTestCase;

class ConsultationComptePubTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ConsultationComptePub::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'idDonneesComplementaires',
            'organisme',
            'idComptePub',
            'boampLogin',
            'boampPassword',
            'boampMail',
            'boampTarget',
            'denomination',
            'prm',
            'adresse',
            'cp',
            'ville',
            'url',
            'factureDenomination',
            'factureAdresse',
            'factureCp',
            'factureVille',
            'instanceRecoursOrganisme',
            'instanceRecoursAdresse',
            'instanceRecoursCp',
            'instanceRecoursVille',
            'instanceRecoursUrl',
            'telephone',
            'email',
            'donneeComplementaire',
        ];
    }
}
