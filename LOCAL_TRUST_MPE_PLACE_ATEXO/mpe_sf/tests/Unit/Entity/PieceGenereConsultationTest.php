<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Entity;

use App\Entity\PieceGenereConsultation;
use Tests\Unit\MpeEntityTestCase;

class PieceGenereConsultationTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return PieceGenereConsultation::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'blobId',
            'consultation',
            'createdAt',
            'updatedAt',
            'lot',
            'entreprise',
            'token',
        ];
    }
}
