<?php

namespace Tests\Unit\Entity;

use App\Entity\TypeProcedure;
use Tests\Unit\MpeEntityTestCase;

class TypeProcedureEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TypeProcedure::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'idTypeProcedure',
            'libelleTypeProcedure',
            'libelleTypeProcedureFr',
            'libelleTypeProcedureEn',
            'libelleTypeProcedureEs',
            'libelleTypeProcedureSu',
            'libelleTypeProcedureDu',
            'libelleTypeProcedureCz',
            'libelleTypeProcedureAr',
            'abbreviation',
            'typeBoamp',
            'categorieProcedure',
            'idTypeProcedureAnm',
            'delaiAlerte',
            'mapa',
            'consultationTransverse',
            'codeRecensement',
            'abbreviationPortailAnm',
            'idModele',
            'libelleTypeProcedureIt',
            'valueBindingSub',
            'consultations',
            'typeProcedureOrganismes'
        ];
    }
}
