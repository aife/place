<?php

namespace Tests\Unit\Entity;

use App\Entity\HistorisationMotDePasse;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class HistorisationMotDePasseTest.
 */
class HistorisationMotDePasseTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return HistorisationMotDePasse::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'ancienMotDePasse',
            'dateModification',
            'inscrit',
        ];
    }
}
