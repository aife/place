<?php

namespace Tests\Unit\Entity;

use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Enveloppe;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Entity\Offre;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Tests\Unit\MpeEntityTestCase;

class OffresEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Offre::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'consultation',
            'inscrit',
            'enveloppes',
            'id',
            'organisme',
            'consultationId',
            'entrepriseId',
            'idEtablissement',
            'inscritId',
            'signatureenvxml',
            'horodatage',
            'mailSignataire',
            'untrusteddate',
            'untrustedserial',
            'envoiComplet',
            'dateDepotDiffere',
            'horodatageEnvoiDiffere',
            'signatureenvxmlEnvoiDiffere',
            'externalSerial',
            'internalSerial',
            'uidOffre',
            'offreSelectionnee',
            'observation',
            'xmlString',
            'nomEntrepriseInscrit',
            'nomInscrit',
            'prenomInscrit',
            'adresseInscrit',
            'adresse2Inscrit',
            'telephoneInscrit',
            'faxInscrit',
            'codePostalInscrit',
            'villeInscrit',
            'paysInscrit',
            'acronymePays',
            'siretEntreprise',
            'identifiantNational',
            'emailInscrit',
            'siretInscrit',
            'nomEntreprise',
            'horodatageAnnulation',
            'dateAnnulation',
            'signatureAnnulation',
            'depotAnnule',
            'stringAnnulation',
            'verificationCertificatAnnulation',
            'offreVariante',
            'reponsePasAPas',
            'numeroReponse',
            'statutOffres',
            'dateHeureOuverture',
            'agentidOuverture',
            'agentidOuverture2',
            'dateHeureOuvertureAgent2',
            'cryptageReponse',
            'nomAgentOuverture',
            'agentTelechargementOffre',
            'dateTelechargementOffre',
            'repertoireTelechargementOffre',
            'candidatureIdExterne',
            'etatChiffrement',
            'erreurChiffrement',
            'horodatageHashFichiers',
            'idPdfEchangeAccuse',
            'createdAt',
            'uidResponse',
            'dateDepot',
            'idBlobHorodatageHash',
            'idBlobXmlReponse',
            'plateformeVirtuelle',
            'tauxProductionFrance',
            'tauxProductionEurope',
        ];
    }

    public function testId()
    {
        $id = 1;
        $offre = new Offre();
        $offre->setId($id);
        $this->assertEquals($id, $offre->getId());
    }

    public function testOrganisme()
    {
        $organisme = 'min';
        $offre = new Offre();
        $offre->setOrganisme($organisme);
        $this->assertEquals($organisme, $offre->getOrganisme());
    }

    public function testConsultationRef()
    {
        $consultationRef = 'ref';
        $offre = new Offre();
        $offre->setConsultationId($consultationRef);
        $this->assertEquals($consultationRef, $offre->getConsultationId());
    }

    public function testEntrepriseId()
    {
        $entrepriseId = 1;
        $offre = new Offre();
        $offre->setEntrepriseId($entrepriseId);
        $this->assertEquals($entrepriseId, $offre->getEntrepriseId());
    }

    public function testIdEtablissement()
    {
        $idEtablissement = 1;
        $offre = new Offre();
        $offre->setIdEtablissement($idEtablissement);
        $this->assertEquals($idEtablissement, $offre->getIdEtablissement());
    }

    public function testInscritId()
    {
        $idEtablissement = 1;
        $offre = new Offre();
        $offre->setInscritId($idEtablissement);
        $this->assertEquals($idEtablissement, $offre->getInscritId());
    }

    public function testSignatureenvxml()
    {
        $signatureenvxml = 1;
        $offre = new Offre();
        $offre->setSignatureenvxml($signatureenvxml);
        $this->assertEquals($signatureenvxml, $offre->getSignatureenvxml());
    }

    public function testHorodatage()
    {
        $horodatage = 'horo';
        $offre = new Offre();
        $offre->setHorodatage($horodatage);
        $this->assertEquals($horodatage, $offre->getHorodatage());
    }

    public function testMailSignataire()
    {
        $mailsignataire = 'true';
        $offre = new Offre();
        $offre->setMailSignataire($mailsignataire);
        $this->assertEquals($mailsignataire, $offre->getMailSignataire());
    }

    public function testUntrusteddate()
    {
        $untrusteddate = new DateTime();
        $offre = new Offre();
        $offre->setUntrusteddate($untrusteddate);
        $this->assertEquals($untrusteddate, $offre->getUntrusteddate());
    }

    public function testUntrustedserial()
    {
        $untrustedserial = '{test: test}';
        $offre = new Offre();
        $offre->setUntrustedserial($untrustedserial);
        $this->assertEquals($untrustedserial, $offre->getUntrustedserial());
    }

    public function testEnvoiComplet()
    {
        $envoiComplet = 'ok';
        $offre = new Offre();
        $offre->setEnvoiComplet($envoiComplet);
        $this->assertEquals($envoiComplet, $offre->getEnvoiComplet());
    }

    public function testDateDepotDiffere()
    {
        $dateDepotDiffere = '2018-02-02';
        $offre = new Offre();
        $offre->setDateDepotDiffere($dateDepotDiffere);
        $this->assertEquals($dateDepotDiffere, $offre->getDateDepotDiffere());
    }

    public function testHorodatageEnvoiDiffere()
    {
        $horodatageEnvoiDiffere = '2018-02-02';
        $offre = new Offre();
        $offre->setHorodatageEnvoiDiffere($horodatageEnvoiDiffere);
        $this->assertEquals($horodatageEnvoiDiffere, $offre->getHorodatageEnvoiDiffere());
    }

    public function testSignatureenvxmlEnvoiDiffere()
    {
        $horodatageEnvoiDiffere = 'xml';
        $offre = new Offre();
        $offre->setSignatureenvxmlEnvoiDiffere($horodatageEnvoiDiffere);
        $this->assertEquals($horodatageEnvoiDiffere, $offre->getSignatureenvxmlEnvoiDiffere());
    }

    public function testExternalSerial()
    {
        $externalSerial = 'serial!';
        $offre = new Offre();
        $offre->setExternalSerial($externalSerial);
        $this->assertEquals($externalSerial, $offre->getExternalSerial());
    }

    public function testInternalSerial()
    {
        $internalSerial = 'serialExtern!';
        $offre = new Offre();
        $offre->setInternalSerial($internalSerial);
        $this->assertEquals($internalSerial, $offre->getInternalSerial());
    }

    public function testUidOffre()
    {
        $uidOffre = 'uid';
        $offre = new Offre();
        $offre->setUidOffre($uidOffre);
        $this->assertEquals($uidOffre, $offre->getUidOffre());
    }

    public function testOffreSelectionnee()
    {
        $offreSelectionnee = 1;
        $offre = new Offre();
        $offre->setOffreSelectionnee($offreSelectionnee);
        $this->assertEquals($offreSelectionnee, $offre->getOffreSelectionnee());
    }

    public function testObservation()
    {
        $observation = 1;
        $offre = new Offre();
        $offre->setObservation($observation);
        $this->assertEquals($observation, $offre->getObservation());
    }

    public function testXmlString()
    {
        $observation = 'xml';
        $offre = new Offre();
        $offre->setXmlString($observation);
        $this->assertEquals($observation, $offre->getXmlString());
    }

    public function testNomEntrepriseInscrit()
    {
        $observation = 'toto';
        $offre = new Offre();
        $offre->setNomEntrepriseInscrit($observation);
        $this->assertEquals($observation, $offre->getNomEntrepriseInscrit());
    }

    public function testNomInscrit()
    {
        $nomInscrit = 'toto';
        $offre = new Offre();
        $offre->setNomInscrit($nomInscrit);
        $this->assertEquals($nomInscrit, $offre->getNomInscrit());
    }

    public function testPrenomInscrit()
    {
        $prenomInscrit = 'toto';
        $offre = new Offre();
        $offre->setPrenomInscrit($prenomInscrit);
        $this->assertEquals($prenomInscrit, $offre->getPrenomInscrit());
    }

    public function testAdresseInscrit()
    {
        $adresseInscrit = 'toto';
        $offre = new Offre();
        $offre->setAdresseInscrit($adresseInscrit);
        $this->assertEquals($adresseInscrit, $offre->getAdresseInscrit());
    }

    public function testAdresse2Inscrit()
    {
        $adresse2Inscrit = 'quelque part';
        $offre = new Offre();
        $offre->setAdresse2Inscrit($adresse2Inscrit);
        $this->assertEquals($adresse2Inscrit, $offre->getAdresse2Inscrit());
    }

    public function testTelephoneInscrit()
    {
        $telephoneInscrit = '01.01.01.01.01';
        $offre = new Offre();
        $offre->setTelephoneInscrit($telephoneInscrit);
        $this->assertEquals($telephoneInscrit, $offre->getTelephoneInscrit());
    }

    public function testFaxInscrit()
    {
        $faxInscrit = '01.01.01.01.01';
        $offre = new Offre();
        $offre->setFaxInscrit($faxInscrit);
        $this->assertEquals($faxInscrit, $offre->getFaxInscrit());
    }

    public function testCodePostalInscrit()
    {
        $codePostalInscrit = '78700';
        $offre = new Offre();
        $offre->setCodePostalInscrit($codePostalInscrit);
        $this->assertEquals($codePostalInscrit, $offre->getCodePostalInscrit());
    }

    public function testVilleInscrit()
    {
        $villeInscrit = 'conflans';
        $offre = new Offre();
        $offre->setVilleInscrit($villeInscrit);
        $this->assertEquals($villeInscrit, $offre->getVilleInscrit());
    }

    public function testPaysInscrit()
    {
        $paysInscrit = 'france';
        $offre = new Offre();
        $offre->setPaysInscrit($paysInscrit);
        $this->assertEquals($paysInscrit, $offre->getPaysInscrit());
    }

    public function testAcronymePays()
    {
        $acronymePays = 'fr';
        $offre = new Offre();
        $offre->setAcronymePays($acronymePays);
        $this->assertEquals($acronymePays, $offre->getAcronymePays());
    }

    public function testSiretEntreprise()
    {
        $siretEntreprise = '7894566';
        $offre = new Offre();
        $offre->setSiretEntreprise($siretEntreprise);
        $this->assertEquals($siretEntreprise, $offre->getSiretEntreprise());
    }

    public function testIdentifiantNational()
    {
        $identifiantNational = '7894566';
        $offre = new Offre();
        $offre->setIdentifiantNational($identifiantNational);
        $this->assertEquals($identifiantNational, $offre->getIdentifiantNational());
    }

    public function testEmailInscrit()
    {
        $emailInscrit = 'test@atexo.com';
        $offre = new Offre();
        $offre->setEmailInscrit($emailInscrit);
        $this->assertEquals($emailInscrit, $offre->getEmailInscrit());
    }

    public function testSiretInscrit()
    {
        $siretInscrit = '741852';
        $offre = new Offre();
        $offre->setSiretInscrit($siretInscrit);
        $this->assertEquals($siretInscrit, $offre->getSiretInscrit());
    }

    public function testNomEntreprise()
    {
        $nomEntreprise = 'atexo';
        $offre = new Offre();
        $offre->setNomEntreprise($nomEntreprise);
        $this->assertEquals($nomEntreprise, $offre->getNomEntreprise());
    }

    public function testHorodatageAnnulation()
    {
        $horodatageAnnulation = 'annuler';
        $offre = new Offre();
        $offre->setHorodatageAnnulation($horodatageAnnulation);
        $this->assertEquals($horodatageAnnulation, $offre->getHorodatageAnnulation());
    }

    public function testDateAnnulation()
    {
        $dateAnnulation = '2018-01-02';
        $offre = new Offre();
        $offre->setDateAnnulation($dateAnnulation);
        $this->assertEquals($dateAnnulation, $offre->getDateAnnulation());
    }

    public function testSignatureAnnulation()
    {
        $signatureAnnulation = 'annuler';
        $offre = new Offre();
        $offre->setSignatureAnnulation($signatureAnnulation);
        $this->assertEquals($signatureAnnulation, $offre->getSignatureAnnulation());
    }

    public function testDepotAnnule()
    {
        $depotAnnule = 'annuler';
        $offre = new Offre();
        $offre->setDepotAnnule($depotAnnule);
        $this->assertEquals($depotAnnule, $offre->getDepotAnnule());
    }

    public function testStringAnnulation()
    {
        $stringAnnulation = 'annuler';
        $offre = new Offre();
        $offre->setStringAnnulation($stringAnnulation);
        $this->assertEquals($stringAnnulation, $offre->getStringAnnulation());
    }

    public function testVerificationCertificatAnnulation()
    {
        $verificationCertificatAnnulation = 'annuler';
        $offre = new Offre();
        $offre->setVerificationCertificatAnnulation($verificationCertificatAnnulation);
        $this->assertEquals($verificationCertificatAnnulation, $offre->getVerificationCertificatAnnulation());
    }

    public function testOffreVariante()
    {
        $offreVariante = 'variante';
        $offre = new Offre();
        $offre->setOffreVariante($offreVariante);
        $this->assertEquals($offreVariante, $offre->getOffreVariante());
    }

    public function testReponsePasAPas()
    {
        $pasAPas = true;
        $offre = new Offre();
        $offre->setReponsePasAPas($pasAPas);
        $this->assertEquals($pasAPas, $offre->getReponsePasAPas());
    }

    public function testNumeroReponse()
    {
        $numeroReponse = 1;
        $offre = new Offre();
        $offre->setNumeroReponse($numeroReponse);
        $this->assertEquals($numeroReponse, $offre->getNumeroReponse());
    }

    public function testStatutOffres()
    {
        $enveloppe = new Enveloppe();
        $statutOffres = 99;
        $offre = new Offre();
        $offre->addEnveloppe($enveloppe);
        $this->assertEquals($offre, $offre->setStatutOffres($statutOffres));
        $this->assertEquals($statutOffres, $offre->getStatutOffres());
    }

    public function testDateHeureOuverture()
    {
        $dateHeureOuverture = '2018-01-01 10:10:10';
        $offre = new Offre();
        $offre->setDateHeureOuverture($dateHeureOuverture);
        $this->assertEquals($dateHeureOuverture, $offre->getDateHeureOuverture());
    }

    public function testAgentidOuverture()
    {
        $agentidOuverture = 1;
        $offre = new Offre();
        $offre->setAgentidOuverture($agentidOuverture);
        $this->assertEquals($agentidOuverture, $offre->getAgentidOuverture());
    }

    public function testAgentidOuverture2()
    {
        $agentidOuverture2 = 1;
        $offre = new Offre();
        $offre->setAgentidOuverture2($agentidOuverture2);
        $this->assertEquals($agentidOuverture2, $offre->getAgentidOuverture2());
    }

    public function testDateHeureOuvertureAgent2()
    {
        $dateHeureOuvertureAgent2 = '2018-01-01 10:10:10';
        $offre = new Offre();
        $offre->setDateHeureOuvertureAgent2($dateHeureOuvertureAgent2);
        $this->assertEquals($dateHeureOuvertureAgent2, $offre->getDateHeureOuvertureAgent2());
    }

    public function testCryptageReponse()
    {
        $cryptageReponse = 'cryptage';
        $offre = new Offre();
        $offre->setCryptageReponse($cryptageReponse);
        $this->assertEquals($cryptageReponse, $offre->getCryptageReponse());
    }

    public function testNomAgentOuverture()
    {
        $nomAgentOuverture = 'Flo';
        $offre = new Offre();
        $offre->setNomAgentOuverture($nomAgentOuverture);
        $this->assertEquals($nomAgentOuverture, $offre->getNomAgentOuverture());
    }

    public function testAgentTelechargementOffre()
    {
        $agentTelechargementOffre = 1;
        $offre = new Offre();
        $offre->setAgentTelechargementOffre($agentTelechargementOffre);
        $this->assertEquals($agentTelechargementOffre, $offre->getAgentTelechargementOffre());
    }

    public function testDateTelechargementOffre()
    {
        $dateTelechargementOffre = '2018-01-02';
        $offre = new Offre();
        $offre->setDateTelechargementOffre($dateTelechargementOffre);
        $this->assertEquals($dateTelechargementOffre, $offre->getDateTelechargementOffre());
    }

    public function testRepertoireTelechargementOffre()
    {
        $repertoireTelechargementOffre = '/tmp/';
        $offre = new Offre();
        $offre->setRepertoireTelechargementOffre($repertoireTelechargementOffre);
        $this->assertEquals($repertoireTelechargementOffre, $offre->getRepertoireTelechargementOffre());
    }

    public function testCandidatureIdExterne()
    {
        $candidatureIdExterne = 10;
        $offre = new Offre();
        $offre->setCandidatureIdExterne($candidatureIdExterne);
        $this->assertEquals($candidatureIdExterne, $offre->getCandidatureIdExterne());
    }

    public function testConsultation()
    {
        $consultation = new Consultation();
        $consultation->setAcronymeOrg('pmi');

        $offre = new Offre();
        $offre->setConsultation($consultation);
        $this->assertEquals($consultation, $offre->getConsultation());
    }

    public function testEnveloppe()
    {
        $arrayCollection = new ArrayCollection();
        $enveloppe = new Enveloppe();
        $arrayCollection->set('0', $enveloppe);
        $offre = new Offre();
        $offre->addEnveloppe($enveloppe);
        $this->assertEquals($arrayCollection, $offre->getEnveloppes());
        $offre->removeEnveloppe($enveloppe);
        $arrayCollection->remove(0);
        $this->assertEquals($arrayCollection, $offre->getEnveloppes());
    }

    public function testErreurChiffrement()
    {
        $erreurChiffrement = 'error';
        $offre = new Offre();
        $offre->setErreurChiffrement($erreurChiffrement);
        $this->assertEquals($erreurChiffrement, $offre->getErreurChiffrement());
    }

    public function testEtatChiffrement()
    {
        $etatChiffrement = 4;
        $offre = new Offre();
        $offre->setEtatChiffrement($etatChiffrement);
        $this->assertEquals($etatChiffrement, $offre->getEtatChiffrement());
    }

    public function testHorodatageHashFichiers()
    {
        $horodatageHashFichiers = 'hash';
        $offre = new Offre();
        $offre->setHorodatageHashFichiers($horodatageHashFichiers);
        $this->assertEquals($horodatageHashFichiers, $offre->getHorodatageHashFichiers());
    }

    public function testIdPdfEchangeAccuse()
    {
        $idPdfEchangeAccuse = 12;
        $offre = new Offre();
        $offre->setIdPdfEchangeAccuse($idPdfEchangeAccuse);
        $this->assertEquals($idPdfEchangeAccuse, $offre->getIdPdfEchangeAccuse());
    }

    public function testInscrit()
    {
        $entreprise = new Entreprise();
        $entreprise->setAcronymePays('fr');
        $entreprise->setNom('Atexo');
        $entreprise->setNom('Atexo');

        $etablissement = new Etablissement();
        $etablissement->setSiret('789456');
        $etablissement->setCodeEtablissement('789456');

        $inscrit = new Inscrit();
        $inscrit->setNom('martin');
        $inscrit->setPrenom('florian');
        $inscrit->setAdresse('quelque part');
        $inscrit->setAdresse2('quelque part 2');
        $inscrit->setTelephone('01.02.03.04.05');
        $inscrit->setFax('01.02.03.04.05');
        $inscrit->setCodepostal('78700');
        $inscrit->setVille('conflans');
        $inscrit->setPays('France');
        $inscrit->setEmail('test@atexo.com');
        $inscrit->setEntrepriseId(1);
        $inscrit->setIdEtablissement(2);
        $inscrit->setEntreprise($entreprise);

        $offre = new Offre();
        $offre->setInscrit($inscrit);
        $this->assertEquals($inscrit, $offre->getInscrit());
    }

    public function testHasEnveloppeCandidature()
    {
        $consultation = new Consultation();
        $consultation->setEnvCandidature(true);
        $typeEnvCandValue = true;
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $this->assertEquals(false, $offre->hasEnveloppeCandidature($typeEnvCandValue));
    }

    public function testHasEnveloppeCandidatureReturnTrue()
    {
        $consultation = new Consultation();
        $consultation->setEnvCandidature(true);

        $enveloppe = new Enveloppe();
        $enveloppe->setTypeEnv(true);

        $typeEnvCandValue = true;
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $offre->addEnveloppe($enveloppe);
        $this->assertEquals(true, $offre->hasEnveloppeCandidature($typeEnvCandValue));
    }

    public function testUidResponse()
    {
        $uidResponse = 'uid';
        $offre = new Offre();
        $offre->setUidResponse($uidResponse);
        $this->assertEquals($uidResponse, $offre->getUidResponse());
    }

    public function testDateDepot()
    {
        $dateDepot = new DateTime('2018-02-03');
        $offre = new Offre();
        $offre->setDateDepot($dateDepot);
        $this->assertEquals($dateDepot, $offre->getDateDepot());
    }

    public function testCreatedAt()
    {
        $createdAt = '2018-02-03';
        $offre = new Offre();
        $offre->setCreatedAt($createdAt);
        $this->assertEquals($createdAt, $offre->getCreatedAt());
    }

    public function testIdBlobHorodatageHash()
    {
        $idBlobHorodatageHash = 1;
        $offre = new Offre();
        $offre->setIdBlobHorodatageHash($idBlobHorodatageHash);
        $this->assertEquals($idBlobHorodatageHash, $offre->getIdBlobHorodatageHash());
    }
}
