<?php

namespace Tests\Unit\Entity\Organisme;

use App\Entity\Organisme\ServiceMetier;
use Tests\Unit\MpeEntityTestCase;

class ServiceMetierTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ServiceMetier::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'sigle',
            'denomination',
            'urlAcces',
            'urlDeconnexion',
            'logo',
            'ordre',
        ];
    }
}
