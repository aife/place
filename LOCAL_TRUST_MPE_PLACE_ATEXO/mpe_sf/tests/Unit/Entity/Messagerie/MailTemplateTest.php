<?php

namespace Tests\Unit\Entity\Messagerie;

use App\Entity\Messagerie\MailTemplate;
use App\Entity\Messagerie\MailType;
use Tests\Unit\MpeEntityTestCase;

class MailTemplateTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $mailType = new MailType();
        $mailType->setLabel('MailType');
        $mailType->setCode('Code');

        $mailTemplate = new MailTemplate();
        $mailTemplate->setCode('code');
        $mailTemplate->setObjet('objet');
        $mailTemplate->setCorps('corps');
        $mailTemplate->setOrdreAffichage(1);
        $mailTemplate->setEnvoiModalite('AVEC_AR');
        $mailTemplate->setEnvoiModaliteFigee(true);
        $mailTemplate->setMailType($mailType);

        $this->assertEquals('code', $mailTemplate->getCode());
        $this->assertEquals('objet', $mailTemplate->getObjet());
        $this->assertEquals('corps', $mailTemplate->getCorps());
        $this->assertEquals($mailType, $mailTemplate->getMailType());
        $this->assertEquals(1, $mailTemplate->getOrdreAffichage());
        $this->assertEquals('AVEC_AR', $mailTemplate->getEnvoiModalite());
        $this->assertEquals(true, $mailTemplate->isEnvoiModaliteFigee());
        $this->assertEquals(false, $mailTemplate->isReponseAttendue());
        $this->assertEquals(false, $mailTemplate->isReponseAttendueFigee());
        $this->assertEquals(false, $mailTemplate->getReponseAgentAttendue());
        //$this->assertEquals($stringTest, $mailType->getLabel());
    }

    protected function getTestedEntity()
    {
        return MailTemplate::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'mailType',
            'code',
            'objet',
            'corps',
            'ordreAffichage',
            'envoiModalite',
            'envoiModaliteFigee',
            'reponseAttendue',
            'reponseAttendueFigee',
            'reponseAgentAttendue',
        ];
    }
}
