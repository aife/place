<?php

namespace Tests\Unit\Entity\Messagerie;

use App\Entity\Messagerie\MailType;
use App\Entity\Messagerie\MailTypeGroup;
use Tests\Unit\MpeEntityTestCase;

class MailTypeTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $mailTypeGroup = new MailTypeGroup();
        $mailTypeGroup->setLabel('groups');

        $stringTest = 'MailTypeTest';
        $stringCodeTest = 'code';
        $mailType = new MailType();
        $mailType->setLabel('MailTypeTest');
        $mailType->setCode('code');
        $mailType->setMailTypeGroup($mailTypeGroup);

        $this->assertEquals($stringTest, $mailType->getLabel());
        $this->assertEquals($stringCodeTest, $mailType->getCode());
        $this->assertEquals($mailTypeGroup, $mailType->getMailTypeGroup());
    }

    protected function getTestedEntity()
    {
        return MailType::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'label',
            'code',
            'mailTypeGroup',
        ];
    }
}
