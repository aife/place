<?php

namespace Tests\Unit\Entity\Messagerie;

use App\Entity\Messagerie\MailTypeGroup;
use Tests\Unit\MpeEntityTestCase;

class MailTypeGroupTest extends MpeEntityTestCase
{
    public function testMethode()
    {
        $stringTest = 'MailTypeGroupTest';
        $mailTypeGroup = new MailTypeGroup();
        $mailTypeGroup->setLabel('MailTypeGroupTest');
        $this->assertEquals($stringTest, $mailTypeGroup->getLabel());
    }

    protected function getTestedEntity()
    {
        return MailTypeGroup::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'label',
        ];
    }
}
