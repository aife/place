<?php

namespace Tests\Unit\Entity;

use App\Entity\Organisme\ServiceMetier;
use Tests\Unit\MpeEntityTestCase;

class ServiceMetierTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ServiceMetier::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'sigle',
            'denomination',
            'urlAcces',
            'logo',
            'urlDeconnexion',
            'ordre',
        ];
    }
}
