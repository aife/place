<?php

namespace Tests\Unit\Entity\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchive;
use Tests\Unit\MpeEntityTestCase;

class ConsultationArchiveTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ConsultationArchive::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'organisme',
            'consultationRef',
            'cheminFichier',
            'dateArchivage',
            'poidsArchivage',
            'anneeCreationConsultation',
            'statusGlobalTransmission',
            'statusFragmentation',
            'nombreBloc',
            'consultationId',
        ];
    }
}
