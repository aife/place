<?php

namespace Tests\Unit\Entity\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchiveBloc;
use Tests\Unit\MpeEntityTestCase;

class ConsultationArchiveBlocTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return ConsultationArchiveBloc::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'consultationArchive',
            'doc_id',
            'chemin_fichier',
            'numero_bloc',
            'poids_bloc',
            'date_envoi_debut',
            'date_envoi_fin',
            'status_transmission',
            'erreur',
            'compId',
        ];
    }
}
