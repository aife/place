<?php

namespace Tests\Unit\Entity;

use App\Entity\Service;
use Tests\Unit\MpeEntityTestCase;

/**
 * Class ServiceEntityTest.
 */
class ServiceEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Service::class;
    }

    protected function getIgnoredObjectAttributes(): array
    {
        return [
            'depth',
            'acronymeOrg',
            'consultations',
            'affiliationsService',
            'idParent',
            'oldIdParent',
            'idExterneParent',
            'affiliationsPole',
            'agents',
        ];
    }

    protected function getBddFieldNames(): array
    {
        return [
            'organisme',
            'type_service',
            'libelle',
            'sigle',
            'adresse',
            'adresse_suite',
            'cp',
            'ville',
            'telephone',
            'fax',
            'mail',
            'pays',
            'id_externe',
            'date_creation',
            'date_modification',
            'siren',
            'complement',
            'libelle_ar',
            'adresse_ar',
            'adresse_suite_ar',
            'ville_ar',
            'pays_ar',
            'libelle_fr',
            'adresse_fr',
            'adresse_suite_fr',
            'ville_fr',
            'pays_fr',
            'libelle_es',
            'adresse_es',
            'adresse_suite_es',
            'ville_es',
            'pays_es',
            'libelle_en',
            'adresse_en',
            'adresse_suite_en',
            'ville_en',
            'pays_en',
            'libelle_su',
            'adresse_su',
            'adresse_suite_su',
            'ville_su',
            'pays_su',
            'libelle_du',
            'adresse_du',
            'adresse_suite_du',
            'ville_du',
            'pays_du',
            'libelle_cz',
            'adresse_cz',
            'adresse_suite_cz',
            'ville_cz',
            'pays_cz',
            'libelle_it',
            'adresse_it',
            'adresse_suite_it',
            'ville_it',
            'pays_it',
            'chemin_complet',
            'chemin_complet_fr',
            'chemin_complet_en',
            'chemin_complet_es',
            'chemin_complet_su',
            'chemin_complet_du',
            'chemin_complet_cz',
            'chemin_complet_ar',
            'chemin_complet_it',
            'nom_service_archiveur',
            'identifiant_service_archiveur',
            'affichage_service',
            'activation_fuseau_horaire',
            'decalage_horaire',
            'lieu_residence',
            'alerte',
            'acces_chorus',
            'forme_juridique',
            'forme_juridique_code',
            'synchronisation_exec',
            'idEntite',
        ];
    }
}
