<?php

namespace Tests\Unit\Entity;

use App\Entity\Etablissement;
use Tests\Unit\MpeEntityTestCase;

class EtablissementTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Etablissement::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'inscrits',
            'entreprise',
            'idEtablissement',
            'idEntreprise',
            'codeEtablissement',
            'estSiege',
            'adresse',
            'adresse2',
            'codePostal',
            'ville',
            'pays',
            'saisieManuelle',
            'idInitial',
            'dateCreation',
            'dateModification',
            'dateSuppression',
            'statutActif',
            'inscritAnnuaireDefense',
            'long',
            'lat',
            'majLongLat',
            'tvaIntracommunautaire',
            'codeApe',
            'siret',
            'libelleApe',
            'dateFermeture',
            'etatAdministratif',
            'idExterne',
            'modificationContrats',
        ];
    }
}
