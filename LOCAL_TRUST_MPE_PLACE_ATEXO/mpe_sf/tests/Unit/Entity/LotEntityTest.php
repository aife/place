<?php

namespace Tests\Unit\Entity;

use App\Entity\Lot;
use Tests\Unit\MpeEntityTestCase;

class LotEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Lot::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'consultation',
            'organisme',
            'lot',
            'description',
            'idTrDescription',
            'categorie',
            'descriptionDetail',
            'idTrDescriptionDetail',
            'codeCpv1',
            'codeCpv2',
            'descriptionFr',
            'descriptionEn',
            'descriptionEs',
            'descriptionSu',
            'descriptionDu',
            'descriptionCz',
            'descriptionAr',
            'descriptionDetailFr',
            'descriptionDetailEn',
            'descriptionDetailEs',
            'descriptionDetailSu',
            'descriptionDetailDu',
            'descriptionDetailCz',
            'descriptionDetailAr',
            'idLotExterne',
            'cautionProvisoire',
            'qualification',
            'agrements',
            'addEchantillion',
            'dateLimiteEchantillion',
            'addReunion',
            'dateReunion',
            'variantes',
            'echantillon',
            'reunion',
            'visitesLieux',
            'addEchantillionFr',
            'addEchantillionEn',
            'addEchantillionEs',
            'addEchantillionSu',
            'addEchantillionDu',
            'addEchantillionCz',
            'addEchantillionAr',
            'addReunionFr',
            'addReunionEn',
            'addReunionEs',
            'addReunionSu',
            'addReunionDu',
            'addReunionCz',
            'addReunionAr',
            'descriptionDetailIt',
            'descriptionIt',
            'addEchantillionIt',
            'addReunionIt',
            'clauseSociale',
            'clauseEnvironnementale',
            'decision',
            'clauseSocialeConditionExecution',
            'clauseSocialeInsertion',
            'clauseSocialeAteliersProteges',
            'clauseSocialeSiae',
            'clauseSocialeEss',
            'clauseEnvSpecsTechniques',
            'clauseEnvCondExecution',
            'clauseEnvCriteresSelect',
            'idDonneeComplementaire',
            'marcheInsertion',
            'clauseSpecificationTechnique',
            'donneeComplementaire',
            'id',
            'pieceGeneree',
            'critereAttribution',
            'clausesN1'
        ];
    }
}
