<?php

namespace Tests\Unit\Entity;

use App\Entity\TypeProcedurePivot;
use Tests\Unit\MpeEntityTestCase;

class TypeProcedurePivotEntityTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return TypeProcedurePivot::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'id',
            'libelle',
            'contratTitulaires',
        ];
    }
}
