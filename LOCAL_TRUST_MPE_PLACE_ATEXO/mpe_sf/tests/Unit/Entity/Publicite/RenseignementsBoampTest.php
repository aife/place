<?php

namespace Tests\Unit\Entity\Publicite;

use App\Entity\Publicite\RenseignementsBoamp;
use Tests\Unit\MpeEntityTestCase;

class RenseignementsBoampTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return RenseignementsBoamp::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'acronymeOrg',
            'idType',
            'idCompte',
            'correspondant',
            'organisme',
            'adresse',
            'cp',
            'ville',
            'pays',
            'telephone',
            'poste',
            'fax',
            'mail',
            'url',
            'organeChargeProcedure',
        ];
    }
}
