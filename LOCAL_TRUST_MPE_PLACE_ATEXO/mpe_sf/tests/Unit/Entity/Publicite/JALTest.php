<?php

namespace Tests\Unit\Entity\Publicite;

use App\Entity\Jal;
use Tests\Unit\MpeEntityTestCase;

class JALTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Jal::class;
    }

    protected function getIgnoredObjectAttributes(): array
    {
        return [
            'organisme',
            'emailAr',
            'telecopie',
            'informationFacturation',
            'oldServiceId',
            'oldId'
        ];
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'serviceId',
            'nom',
            'email'
        ];
    }
}
