<?php

namespace Tests\Unit\Entity;

use App\Entity\SocleHabilitationAgent;
use Tests\Unit\MpeEntityTestCase;

class SocleHabilitationAgentTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return SocleHabilitationAgent::class;
    }

    protected function getExpectedAttributes(): array
    {
        return [
            'agent',
            'gestionAgentPoleSocle',
            'gestionAgentsSocle',
            'droitGestionServicesSocle',
        ];
    }
}
