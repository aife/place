<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use App\Dto\Input\ConsultationInput;
use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use App\Validator\ProcedureAccesConstraint;
use App\Validator\ProcedureAccesConstraintValidator;
use App\Validator\ProcedureAllotiConstraint;
use App\Validator\ProcedureAllotiConstraintValidator;
use App\Validator\ProcedureDumeConstraint;
use App\Validator\ProcedureDumeConstraintValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class ProcedureDumeConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): ConstraintValidatorInterface
    {
        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setDumeDemande('+1');
        $security = $this->createMock(Security::class);
        $procedureEquivalenceRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepository->method('findOneBy')->willReturn($procedureEquivalence);
        $configService = $this->createMock(AtexoConfiguration::class);
        $configService->method('isModuleEnabled')->willReturn(false);
        $requestMock = $this->createMock(RequestStack::class);
        $requestMock->method('getCurrentRequest')
            ->willReturn(new Request([], [], [], [], [], ['HTTP_X-API-VERSION' => 1]));


        $validator = new ProcedureDumeConstraintValidator($security, $procedureEquivalenceRepository, $configService);
        $validator->setRequest($requestMock);

        return $validator;
    }

    public function testDumeNotSet(): void
    {
        $this->validator->validate(null, new ProcedureDumeConstraint());

        $this->assertNoViolation();
    }

    public function testDumeSet(): void
    {
        $this->validator->validate(true, new ProcedureDumeConstraint());

        $this->buildViolation('Le champ dume n\'est pas autorisé.')->assertRaised();
    }
}
