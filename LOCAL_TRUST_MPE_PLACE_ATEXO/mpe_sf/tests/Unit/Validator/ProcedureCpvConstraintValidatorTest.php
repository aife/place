<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use App\Entity\ConfigurationPlateforme;
use App\Entity\ProcedureEquivalence;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Validator\ProcedureCpvConstraint;
use App\Validator\ProcedureCpvConstraintValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class ProcedureCpvConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator()
    {
        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setAfficherCodeCpv('1');
        $procedureEquivalence->setCodeCpvObligatoire('1');
        $security = $this->createMock(Security::class);
        $procedureEquivalenceRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepository->method('findOneBy')->willReturn($procedureEquivalence);
        $configRepository = $this->createMock(ConfigurationPlateformeRepository::class);
        $configRepository->method('getConfigurationPlateforme')->willReturn(new ConfigurationPlateforme());
        $requestMock = $this->createMock(RequestStack::class);
        $requestMock->method('getCurrentRequest')
            ->willReturn(new Request([], [], [], [], [], ['HTTP_X-API-VERSION' => 1]));

        $validator = new ProcedureCpvConstraintValidator($security, $procedureEquivalenceRepository, $configRepository);
        $validator->setRequest($requestMock);

        return $validator;
    }

    public function testCpvSet()
    {
        $this->validator->validate('4521122', new ProcedureCpvConstraint());

        $this->assertNoViolation();
    }

    public function testCpvNotSet()
    {
        $this->validator->validate(null, new ProcedureCpvConstraint());

        $this->buildViolation('Le champ cpv est obligatoire.')->assertRaised();
    }
}
