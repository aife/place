<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use App\Dto\Input\ConsultationInput;
use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Validator\ProcedureAccesConstraint;
use App\Validator\ProcedureAccesConstraintValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class ProcedureAccesConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): ConstraintValidatorInterface
    {
        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setProcedurePublicite('+1');
        $procedureEquivalence->setProcedureRestreinte('+0');
        $security = $this->createMock(Security::class);
        $procedureEquivalenceRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepository->method('findOneBy')->willReturn($procedureEquivalence);
        $requestMock = $this->createMock(RequestStack::class);
        $requestMock->method('getCurrentRequest')
            ->willReturn(new Request([], [], [], [], [], ['HTTP_X-API-VERSION' => 1]));

        $validator = new ProcedureAccesConstraintValidator($security, $procedureEquivalenceRepository);
        $validator->setRequest($requestMock);

        return $validator;
    }

    public function testConsultationRestreintIsTrue(): void
    {
        $this->validator->validate(true, new ProcedureAccesConstraint());

        $this->assertNoViolation();
    }

    public function testConsultationRestreintIsNull(): void
    {
        $this->validator->validate(null, new ProcedureAccesConstraint());

        $this->buildViolation('Le champ consultationRestreinte est obligatoire.')->assertRaised();
    }
}
