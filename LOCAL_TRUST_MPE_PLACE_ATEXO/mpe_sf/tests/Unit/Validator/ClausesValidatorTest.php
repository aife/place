<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Dto\Input\CreateConsultationInput;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Validator\ClausesValidator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Context\ExecutionContext;
use Symfony\Component\Validator\Violation\ConstraintViolationBuilder;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClausesValidatorTest extends TestCase
{
    private $constraint;

    protected function setUp()
    {
        $this->constraint = new class () extends Constraint {
            public string $notEmptyClauses = "TEXT_VALIDATOR_NOT_EMPTY_CLAUSES";

            public string $invalidClauseN1Referentiel = "Un référentiel de niveau 1 est manquant ou non valide";
            public string $invalidClauseN2Referentiel = "Un référentiel de niveau 2 est manquant ou non valide";
            public string $invalidClauseN3Referentiel = "Un référentiel de niveau 3 est manquant ou non valide";

            public string $noClausesN2Message = "Les clauses de niveau 2 sont manquantes";
            public string $noClausesN3Message = "Les clauses de niveau 3 sont manquantes";

            public string $noLinkClausesN1N2Message = "Une clause de niveau 2 n'est pas liée à sa clause de niveau 1";
            public string $noLinkClausesN2N3Message = "Une clause de niveau 3 n'est pas liée à sa clause de niveau 2";
        };
    }

    public function testUpdateClausesAlloti()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $consultationInput = new CreateConsultationInput();
        $consultationInput->alloti = true;

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn($consultationInput);

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->once())->method('trans')
            ->with('TEXT_VALIDATOR_NOT_EMPTY_CLAUSES')
            ->willReturn('lang');

        $clausesValidator = new ClausesValidator(
            $this->createMock(IriConverterInterface::class),
            $translatorMock
        );
        $clausesValidator->initialize($executionMock);

        $clauses = [1];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le tableau "clauses" null et vide
    public function testUpdateClausesCase1()
    {
        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->never())->method('getItemFromIri');

        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->validate(null, $this->constraint);


        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le referentiel (IRI) clauseN1 non valide
    public function testUpdateClausesCase2()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->once())->method('getItemFromIri')->willReturn(new ClausesN3());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n3s/1"
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le tableau "clausesN2" vide
    public function testUpdateClausesCase3()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->once())->method('getItemFromIri')->willReturn(new ClausesN1());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN2 vide
    public function testUpdateClausesCase4()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->once())->method('getItemFromIri')->willReturn(new ClausesN1());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN2 non valide
    public function testUpdateClausesCase5()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls(new ClausesN1(), new ClausesN3());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n3s/1",
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN2 non lié au référentiel (IRI) clauseN1
    public function testUpdateClausesCase6()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1Nb1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause1Nb2 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 2;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1Nb2);

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1Nb1, $clause2);


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur d'un référentiel (IRI) clauseN2 lié à des clausesN3 avec le tableau "clausesN3" vide
    public function testUpdateClausesCase7()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1);
        $clause2->addClausesN3(new ClausesN3());

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1, $clause2);


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN3 vide
    public function testUpdateClausesCase8()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1);
        $clause2->addClausesN3(new ClausesN3());

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1, $clause2);


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                        "clausesN3"           => [
                            [
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN3 non valide
    public function testUpdateClausesCase9()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1);
        $clause2->addClausesN3(new ClausesN3());

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(3))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1, $clause2, new ClausesN2());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                        "clausesN3"           => [
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n2s/2"
                            ],
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test d'erreur avec le référentiel (IRI) clauseN3 non lié au référentiel (IRI) clauseN2
    public function testUpdateClausesCase10()
    {
        $constraintViolationBuilderMock = $this->createMock(ConstraintViolationBuilder::class);
        $constraintViolationBuilderMock->expects($this->once())
            ->method('setCode')
            ->willReturn($constraintViolationBuilderMock);

        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->once())->method('buildViolation')->willReturn($constraintViolationBuilderMock);
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1);
        $clause2->addClausesN3(new ClausesN3());

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(3))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1, $clause2, new ClausesN3());


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                        "clausesN3"           => [
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/1"
                            ],
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4"
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }

    //Test valide
    public function testUpdateClausesValidCase()
    {
        $executionMock = $this->createMock(ExecutionContext::class);
        $executionMock->expects($this->never())->method('buildViolation');
        $executionMock->expects($this->once())->method('getObject')->willReturn(new class () {
        });

        $clause1 = new class () extends ClausesN1 {
            public function getId(): int
            {
                return 1;
            }
        };
        $clause2 = new ClausesN2();
        $clause2->setClauseN1($clause1);
        $clause2->addClausesN3(new ClausesN3());

        $clause3 = new ClausesN3();
        $clause3->addClausesN2($clause2);

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(3))->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($clause1, $clause2, $clause3);


        $clausesValidator = new ClausesValidator($iriConverterInterface, $this->createMock(TranslatorInterface::class));
        $clausesValidator->initialize($executionMock);


        $clauses = [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/1",
                        "clausesN3"           => [
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/1",
                                "clausesN4"           => []
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $clausesValidator->validate($clauses, $this->constraint);
    }
}
