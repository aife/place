<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use App\Dto\Input\ConsultationInput;
use App\Dto\Input\CreateConsultationInput;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceDumeRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use App\Validator\ProcedureAccesConstraint;
use App\Validator\ProcedureAccesConstraintValidator;
use App\Validator\ProcedureAllotiConstraint;
use App\Validator\ProcedureAllotiConstraintValidator;
use App\Validator\ProcedureDumeConstraint;
use App\Validator\ProcedureDumeConstraintValidator;
use App\Validator\ProcedurePubliciteConstraint;
use App\Validator\ProcedurePubliciteConstraintValidator;
use App\Validator\ProcedureRedacConstraint;
use App\Validator\ProcedureRedacConstraintValidator;
use App\Validator\ProcedureTypeDumeConstraint;
use App\Validator\ProcedureTypeDumeConstraintValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class ProcedureTypeDumeConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): ConstraintValidatorInterface
    {
        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setTypeProcedureDume('+1');
        $security = $this->createMock(Security::class);
        $procedureEquivalenceRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepository->method('findOneBy')->willReturn($procedureEquivalence);
        $configService = $this->createMock(AtexoConfiguration::class);
        $configService->method('isModuleEnabled')->willReturn(true);
        $procedureDumeRepository = $this->createMock(ProcedureEquivalenceDumeRepository::class);
        $typeProcedureOrganisme = new TypeProcedureOrganisme();
        $typeProcedureOrganisme->setIdTypeProcedure(1);
        $typeProcedureOrganisme->setOrganisme('pmi');
        $consultation = new CreateConsultationInput();
        $consultation->typeProcedure = $typeProcedureOrganisme;
        $consultation->organisme = (new Organisme())->setAcronyme('pmi');
        $this->setObject($consultation);
        $requestMock = $this->createMock(RequestStack::class);
        $requestMock->method('getCurrentRequest')
            ->willReturn(new Request([], [], [], [], [], ['HTTP_X-API-VERSION' => 1]));

        $validator = new ProcedureTypeDumeConstraintValidator(
            $security,
            $procedureEquivalenceRepository,
            $procedureDumeRepository,
            $configService
        );
        $validator->setRequest($requestMock);

        return $validator;
    }

    public function testTypeDumeNotSet(): void
    {
        $this->validator->validate(null, new ProcedureTypeDumeConstraint());

        $this->buildViolation('Le champ typeProcedureDume est obligatoire.')->assertRaised();
    }

    public function testTypeDumeSet(): void
    {
        $this->validator->validate(false, new ProcedureTypeDumeConstraint());

        $this->assertNoViolation();
    }
}
