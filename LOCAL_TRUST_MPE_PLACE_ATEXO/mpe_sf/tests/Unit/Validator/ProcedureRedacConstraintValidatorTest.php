<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Validator;

use App\Dto\Input\ConsultationInput;
use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AtexoConfiguration;
use App\Validator\ProcedureAccesConstraint;
use App\Validator\ProcedureAccesConstraintValidator;
use App\Validator\ProcedureAllotiConstraint;
use App\Validator\ProcedureAllotiConstraintValidator;
use App\Validator\ProcedureDumeConstraint;
use App\Validator\ProcedureDumeConstraintValidator;
use App\Validator\ProcedurePubliciteConstraint;
use App\Validator\ProcedurePubliciteConstraintValidator;
use App\Validator\ProcedureRedacConstraint;
use App\Validator\ProcedureRedacConstraintValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\ConstraintValidatorInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class ProcedureRedacConstraintValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): ConstraintValidatorInterface
    {
        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setDonneesComplementaireOui('+0');
        $procedureEquivalence->setDonneesComplementaireNon('1');
        $security = $this->createMock(Security::class);
        $procedureEquivalenceRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepository->method('findOneBy')->willReturn($procedureEquivalence);
        $configService = $this->createMock(AtexoConfiguration::class);
        $configService->method('isModuleEnabled')->willReturn(true);
        $requestMock = $this->createMock(RequestStack::class);
        $requestMock->method('getCurrentRequest')
            ->willReturn(new Request([], [], [], [], [], ['HTTP_X-API-VERSION' => 1]));

        $validator = new ProcedureRedacConstraintValidator($security, $procedureEquivalenceRepository, $configService);
        $validator->setRequest($requestMock);

        return $validator;
    }

    public function testRedacNotSet(): void
    {
        $this->validator->validate(null, new ProcedureRedacConstraint());

        $this->buildViolation('Le champ donneeComplementaireObligatoire est obligatoire.')->assertRaised();
    }

    public function testRedacSet(): void
    {
        $this->validator->validate(false, new ProcedureRedacConstraint());

        $this->assertNoViolation();
    }
}
