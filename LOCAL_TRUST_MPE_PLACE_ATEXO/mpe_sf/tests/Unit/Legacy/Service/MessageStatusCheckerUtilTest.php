<?php

namespace Tests\Unit\Legacy\Service;

use Application\Service\Atexo\MessageStatusCheckerUtil;
use PHPUnit\Framework\TestCase;

/**
 * Class MessageStatusCheckerUtilTest.
 */
class MessageStatusCheckerUtilTest extends TestCase
{
    /**
     * Test la fonction getScript.
     *
     * @return void
     */
    public function testTelechargerPlis()
    {
        $context = '{"urlServeurUpload":"https://mpe-docker.local-trust.com/crypto/cryptographie/","sessionId":"1d05be10-bb46-4e36-910f-10194e32764e","appId":"TELECHARGEMENT_PLI_PAR_PLI","responsesAnnoncesSize":1,"nombreTotalFichiers":1}';
        $actual = MessageStatusCheckerUtil::getScript($context);
        $expected = "<script>document.getElementById('assistance-launch').setAttribute('data-context', 'eyJ1cmxTZXJ2ZXVyVXBsb2FkIjoiaHR0cHM6Ly9tcGUtZG9ja2VyLmxvY2FsLXRydXN0LmNvbS9jcnlwdG8vY3J5cHRvZ3JhcGhpZS8iLCJzZXNzaW9uSWQiOiIxZDA1YmUxMC1iYjQ2LTRlMzYtOTEwZi0xMDE5NGUzMjc2NGUiLCJhcHBJZCI6IlRFTEVDSEFSR0VNRU5UX1BMSV9QQVJfUExJIiwicmVzcG9uc2VzQW5ub25jZXNTaXplIjoxLCJub21icmVUb3RhbEZpY2hpZXJzIjoxfQ==');document.getElementById('assistance-launch-link').click();</script>";
        $this->assertEquals($expected, $actual);
    }
}
