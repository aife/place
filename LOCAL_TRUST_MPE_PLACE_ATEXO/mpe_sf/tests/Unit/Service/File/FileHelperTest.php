<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use App\Entity\BloborganismeFile;
use App\Service\File\FileHelper;
class FileHelperTest extends TestCase
{
    public function testGetFileByBlobIdNull()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(EntityRepository::class);

        $parameterBag->method('get')->willReturnOnConsecutiveCalls('/data', '/data-prod');
        $repository->method('find')->willReturn(new BloborganismeFile());
        $em->method('getRepository')->willReturn($repository);

        $fileHelper = new FileHelper($parameterBag, $em);

        $this->assertNull($fileHelper->getFileByBlobId(2333333));

    }

    public function testGetFileByBlobId()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(EntityRepository::class);
        $organismeFile = new class () extends BloborganismeFile {
            public function getId()
            {
                return 2333333;
            }
        };

        $parameterBag->method('get')->willReturnOnConsecutiveCalls(__DIR__ , '/data-prod');
        $repository->method('find')->willReturn($organismeFile);
        $em->method('getRepository')->willReturn($repository);
        $fileHelper = new FileHelper($parameterBag, $em);
        $pathname = $fileHelper->getFileByBlobId(2333333);

        $this->assertNotNull($pathname);
        $this->assertStringContainsString('files/2333333-0', $pathname);
    }

    public function testGetFileByBlobIdFromPath()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(EntityRepository::class);
        $organismeFile = new class () extends BloborganismeFile {
            public function getId()
            {
                return 2333333;
            }

            public function getChemin()
            {
                return '/files/';
            }
        };

        $parameterBag->method('get')->willReturnOnConsecutiveCalls('/data-prod', __DIR__ );
        $repository->method('find')->willReturn($organismeFile);
        $em->method('getRepository')->willReturn($repository);
        $fileHelper = new FileHelper($parameterBag, $em);
        $pathname = $fileHelper->getFileByBlobId(2333333);

        $this->assertNotNull($pathname);
        $this->assertStringContainsString('files/2333333-0', $pathname);
    }
}
