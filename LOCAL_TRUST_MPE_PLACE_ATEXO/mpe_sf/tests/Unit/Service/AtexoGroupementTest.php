<?php

namespace Tests\Unit\Service;

use ReflectionClass;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\TGroupementEntreprise;
use App\Entity\TMembreGroupementEntreprise;
use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Repository\TMembreGroupementEntrepriseRepository;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEntreprise;
use App\Service\AtexoGroupement;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Security\Core\Security;

/**
 * Class AtexoGroupementTest.
 */
class AtexoGroupementTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testAjouterMembreAuGroupement($entreprise, $membreGroupement, $expected)
    {
        $container = $this->createMock(ContainerInterface::class);

        $em = $this->getEm($entreprise, $membreGroupement);

        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $atexoConfiguration->expects($this->any())
            ->method('hasConfigPlateforme')
            ->willReturn(true);

        $atexoEntreprise = $this->createMock(AtexoEntreprise::class);
        $atexoEntreprise->expects($this->any())
        ->method('synchroEntrepriseAvecApiGouvEntreprise')
        ->willReturn(null);

        $parameterBagInterface = new ParameterBag();
        $parameterBagInterface->set('ACTIVER_SYNCHRONISATION_SGMAP_LORS_DE_CREATION_ENTREPRISE', true);
        $parameterBagInterface->set('ID_ROLE_JURIDIQUE_CO_TRAITANT', 456);
        $parameterBagInterface->set('ID_ROLE_JURIDIQUE_SOUS_TRAITANT', 789);

        $security = $this->createMock(Security::class);

        $service = new AtexoGroupement(
            $container,
            $em,
            $atexoConfiguration,
            $atexoEntreprise,
            $parameterBagInterface,
            $security
        );

        $groupement = new class () extends TGroupementEntreprise {
            public function getIdGroupementEntreprise()
            {
                return 123;
            }
        };
        $idRoleJuridique = null;
        $siren = '';
        $siret = '';
        $idEntrepriseParent = null;
        $foreignCountry = [];
        $actual = $service->ajouterMembreAuGroupement(
            $groupement,
            $idRoleJuridique,
            $siren,
            $siret,
            $idEntrepriseParent,
            $foreignCountry
        );

        $this->assertEquals($expected, $actual);
    }

    public function dataProvider()
    {
        yield [new Entreprise(), new TMembreGroupementEntreprise(), 'SIRET_EXISTE_DEJA'];

        $entreprise = new Entreprise();
        $class = new ReflectionClass($entreprise);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($entreprise, 1);
        yield [$entreprise, null, ''];

        yield [null, new TMembreGroupementEntreprise(), 'ENTREPRISE_N_EXISTE_PAS'];
    }

    public function getEm($entreprise, $membreGroupement)
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) use ($entreprise, $membreGroupement) {
                    if (Entreprise::class === $entityName) {
                        $repository = EntrepriseRepository::class;
                        $method = 'getEntrepriseBySiren';
                        $entity = $entreprise;

                        return $this->getRepository($repository, $method, $entity);
                    }

                    if (TMembreGroupementEntreprise::class === $entityName) {
                        $repository = TMembreGroupementEntrepriseRepository::class;
                        $method = 'isMembreGroupementEntreprise';
                        $entity = $membreGroupement;

                        return $this->getRepository($repository, $method, $entity);
                    }

                    if (Etablissement::class === $entityName) {
                        $repository = EtablissementRepository::class;
                        $method = 'getEtablissementByCodeAndIdEntreprise';
                        $entity = new Etablissement();

                        return $this->getRepository($repository, $method, $entity);
                    }
                }
            ));

        return $em;
    }

    public function getRepository($repository, $method, $entity)
    {
        $repository = $this->createMock($repository);
        $repository->expects($this->any())
            ->method($method)
            ->willReturn($entity);

        return $repository;
    }
}
