<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Blob;

use App\Entity\BloborganismeFile;
use App\Entity\Organisme;
use App\Repository\BloborganismeFileRepository;
use App\Service\Blob\BlobOrganismeFileService;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BlobOrganismeFileServiceTest extends TestCase
{
    /**
     * @var EntityManagerInterface|MockObject
     */
    private $em;

    /**
     * @var Encryption|MockObject
     */
    private $encryption;

    /**
     * @var MountManager|MockObject
     */
    private $mountManager;

    /**
     * @var BlobOrganismeFileService
     */
    private $blobOrganismeFileService;

    public function setUp(): void
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->encryption = $this->createMock(Encryption::class);
        $this->mountManager = $this->createMock(MountManager::class);

        $this->blobOrganismeFileService = new BlobOrganismeFileService(
            $this->em,
            $this->encryption,
            $this->mountManager
        );
    }

    public function testGetFile(): void
    {
        $blobId = 1;
        $path = realpath(__DIR__ . '/../../data/test/fichier1.txt');

        $organisme = new Organisme();

        $blobFile = new class () extends BloborganismeFile {
            public function getId()
            {
                return 2;
            }
            public function getName()
            {
                return 'fileName.docx';
            }
        };
        $blobFile->setOrganisme($organisme);

        $this->encryption
            ->expects(self::never())
            ->method('decryptId')
            ->with($blobId)
        ;

        $blobOrganismeFileRepository = $this->createMock(BloborganismeFileRepository::class);
        $blobOrganismeFileRepository
            ->expects(self::once())
            ->method('find')
            ->with($blobId)
            ->willReturn($blobFile)
        ;

        $this->em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($blobOrganismeFileRepository)
        ;

        $this->mountManager
            ->expects(self::once())
            ->method('getAbsolutePath')
            ->with(2, $organisme, $this->em)
            ->willReturn($path)
        ;

        $result = $this->blobOrganismeFileService->getFile($blobId, true);
        self::assertSame(['name' => 'fileName.docx', 'realPath' => $path], $result);
    }

    public function testGetFileEncrypt(): void
    {
        $blobId = '1';
        $blobEncryptedId = 'hashEncrypted';
        $path = realpath(__DIR__ . '/../../data/test/fichier1.txt');

        $organisme = new Organisme();

        $blobFile = new class () extends BloborganismeFile {
            public function getId()
            {
                return 2;
            }
            public function getName()
            {
                return 'fileName.docx';
            }
        };
        $blobFile->setOrganisme($organisme);

        $this->encryption
            ->expects(self::once())
            ->method('decryptId')
            ->with($blobEncryptedId)
            ->willReturn($blobId)
        ;

        $blobOrganismeFileRepository = $this->createMock(BloborganismeFileRepository::class);
        $blobOrganismeFileRepository
            ->expects(self::once())
            ->method('find')
            ->with($blobId)
            ->willReturn($blobFile)
        ;

        $this->em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($blobOrganismeFileRepository)
        ;

        $this->mountManager
            ->expects(self::once())
            ->method('getAbsolutePath')
            ->with(2, $organisme, $this->em)
            ->willReturn($path)
        ;

        $result = $this->blobOrganismeFileService->getFile($blobEncryptedId);
        self::assertSame(['name' => 'fileName.docx', 'realPath' => $path], $result);
    }

    public function testGetFileNull(): void
    {
        $blobId = 1;

        $this->encryption
            ->expects(self::never())
            ->method('decryptId')
            ->with($blobId)
        ;

        $blobOrganismeFileRepository = $this->createMock(BloborganismeFileRepository::class);
        $blobOrganismeFileRepository
            ->expects(self::once())
            ->method('find')
            ->with($blobId)
            ->willReturn(null)
        ;

        $this->em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($blobOrganismeFileRepository)
        ;

        $result = $this->blobOrganismeFileService->getFile($blobId, true);
        self::assertSame(null, $result);
    }
}
