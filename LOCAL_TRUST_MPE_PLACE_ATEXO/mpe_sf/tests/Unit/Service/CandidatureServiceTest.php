<?php

namespace Tests\Unit\Service;

use ReflectionClass;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Repository\ConsultationRepository;
use App\Repository\OrganismeRepository;
use App\Repository\TCandidatureRepository;
use App\Service\CandidatureService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;

class CandidatureServiceTest extends TestCase
{
    private $candidatureService;
    private $request;

    protected function setUp(): void
    {
        $em = $this->getEntityManager();
        $parameters = $this->getParameters();
        $logger = $this->createMock(LoggerInterface::class);
        $security = $this->createMock(Security::class);

        $user = new Inscrit();
        $user->setEntrepriseId(10);
        $user->setIdEtablissement(40);
        $user->setId(60);

        $security->expects($this->any())
            ->method('getUser')
            ->willReturn($user);

        $this->candidatureService = new CandidatureService($em, $logger, $security, $parameters);

        $requestParams = [
            'organisme' => 'pmi-min-1',
            'consultation' => 93,
            'idInscrit' => 60,
            'idEntreprise' => 10,
            'idEtablissement' => 40,
        ];
        $this->request = new Request();
        foreach ($requestParams as $key => $val) {
            $this->request->request->set($key, $val);
        }

        $sessionMock = $this->createMock(Session::class);

        $sessionMock->expects($this->any())
            ->method('get')
            ->with('organismes_eligibles')
            ->willReturn([]);
        $this->request->setSession($sessionMock);
    }

    /**
     * @throws \Exception
     */
    public function testChangeTypeCandidatureStandard()
    {
        $this->request->request->set('phase', 'std');
        $result = $this->candidatureService->changeTypeCandidatureStandard($this->request);
        $this->assertEquals('std', $result->getTypeCandidature());
        $this->assertEquals(null, $result->getTypeCandidatureDume());
    }

    public function testChangeTypeCandidatureDume()
    {
        $this->request->request->set('phase', 'dume');
        $this->request->request->set('choice', 'online');
        $result = $this->candidatureService->changeTypeCandidatureDume($this->request);
        $this->assertEquals('dume', $result->getTypeCandidature());
        $this->assertEquals('online', $result->getTypeCandidatureDume());
    }

    public function testChangeRoleInscrit()
    {
        $this->request->request->set('role', 1);
        $result = $this->candidatureService->changeTypeCandidatureDume($this->request);
        $this->assertEquals(1, $result->getRoleInscrit());
    }

    public function getParameters()
    {
        return [
            'STATUT_ENV_BROUILLON' => 99,
            'TYPE_CANDIDATUE_DUME' => 'dume',
        ];
    }

    private function getEntityManager()
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Consultation::class == $repository) {
                        $repository = $this->createMock(ConsultationRepository::class);
                        $consultation = new Consultation();
                        $class = new ReflectionClass($consultation);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($consultation, 93);
                        $organisme = new Organisme();
                        $organisme->setAcronyme('pmi-min-1');
                        $consultation->setOrganisme($organisme);
                        $repository->expects($this->any())
                            ->method('getConsultationForDepot')
                            ->with(93)
                            ->willReturn($consultation);

                        return $repository;
                    }
                    if (Organisme::class == $repository) {
                        $repository = $this->createMock(OrganismeRepository::class);
                        $organisme = new Organisme();
                        $organisme->setAcronyme('pmi-min-1');
                        $repository->expects($this->any())
                            ->method('find')
                            ->with('pmi-min-1')
                            ->willReturn($organisme);

                        return $repository;
                    }
                    if (TCandidature::class == $repository) {
                        $repository = $this->createMock(TCandidatureRepository::class);
                        $candidature = new TCandidature();
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->with($this->anything())
                            ->willReturn(null);

                        return $repository;
                    }
                }
            );

        return $em;
    }
}
