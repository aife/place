<?php

namespace Tests\Unit\Service\Api;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\MarchePublie;
use App\Entity\Organisme;
use App\Entity\Organisme\ServiceMetier;
use App\Entity\Service;
use App\Exception\ApiProblemAlreadyExistException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\ApiProblemOrganismePoolException;
use App\Repository\AdministrateurRepository;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\Organisme\ServiceMetierRepository;
use App\Repository\OrganismeRepository;
use App\Repository\ServiceRepository;
use App\Security\PasswordEncoder\LegacySha1PasswordEncoder;
use App\Security\PasswordEncoder\LegacySha256PasswordEncoder;
use App\Security\PasswordEncoder\SodiumPepperEncoder;
use App\Service\Agent\AgentServiceMetierService;
use App\Service\AtexoAgent;
use App\Service\ConfigurationOrganismeService;
use App\Service\MarchePublieService;
use App\Service\OrganismeService;
use App\Service\PradoPasswordEncoder;
use App\Service\SocleHabilitationAgentService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\SodiumPasswordEncoder;

class OrganismeServiceTest extends TestCase
{
    /**
     * @var OrganismeService
     */
    private $organismeService;

    public function initService($typeEm = null)
    {
        if ('acronyme-no-pool' == $typeEm) {
            $em = $this->initEmNoPoolException();
        } else {
            $em = $this->initEm();
        }

        $container = $this->createMock(ContainerInterface::class);
        $serializer = $this->createMock(SerializerInterface::class);
        $configurationOrganismeService = new ConfigurationOrganismeService($container, $em, $serializer);
        $agentServiceMetierService = new AgentServiceMetierService($container, $em);
        $socleHabilitationAgentService = new SocleHabilitationAgentService($container, $em);
        $marchePublieService = new MarchePublieService($container, $em);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $sodiumEncoder = new SodiumPepperEncoder(
            $parameterBag,
            new LegacySha1PasswordEncoder(),
            new LegacySha256PasswordEncoder('true', 'a269BkoILM@6973AZuytrF!Psxe?8sqO')
        );
        $agentService = new AtexoAgent(
            $container,
            $socleHabilitationAgentService,
            $agentServiceMetierService,
            $sodiumEncoder
        );
        $logger = $this->createMock(LoggerInterface::class);

        return new OrganismeService(
            $container,
            $em,
            $marchePublieService,
            $configurationOrganismeService,
            $agentService,
            $socleHabilitationAgentService,
            $agentServiceMetierService,
            $logger
        );
    }

    public function testCreateNoPoolException()
    {
        $this->expectException(ApiProblemOrganismePoolException::class);
        $organismeService = $this->initService('acronyme-no-pool');
        $result = $organismeService->create(new Organisme());
    }

    public function testUpdateSucess()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('acronyme-exist');
        $organismeService = $this->initService();
        $result = $organismeService->update($organisme);
        $this->assertTrue($result instanceof Organisme);
    }

    private function initEm()
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Organisme::class == $repository) {
                        $repository = $this->createMock(OrganismeRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturnCallback(
                                function ($params) {
                                    if ('acronyme-exist' == $params['acronyme']) {
                                        $organisme = new Organisme();
                                        $organisme->setAcronyme('acronyme-exist');

                                        return $organisme;
                                    } else {
                                        return null;
                                    }
                                }
                            );

                        return $repository;
                    }
                    if (Administrateur::class == $repository) {
                        $administrateur = new Administrateur();
                        $administrateur->setNom('pmi-min-1');
                        $repository = $this->createMock(AdministrateurRepository::class);

                        $repository->expects($this->any())
                            ->method('getRandomAvailableOrganisme')
                            ->willReturnCallback(
                                function () {
                                    $organisme = new Administrateur();
                                    $organisme->setNom('acronyme-random');
                                    $organisme->setPrenom('ORGANISME RANDOM');

                                    return $organisme;
                                }
                            );

                        return $repository;
                    }
                }
            );

        return $em;
    }

    private function initEmNoPoolException()
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Administrateur::class == $repository) {
                        $repository = $this->createMock(AdministrateurRepository::class);
                        $repository->expects($this->any())
                            ->method('getRandomAvailableOrganisme')
                            ->willReturn(null);

                        return $repository;
                    }
                }
            );

        return $em;
    }

    public function testSync()
    {
        $organismeService = $this->initService();
        $organismeToUpdate = new Organisme();
        $organismeToUpdate->setDescriptionOrg('NEW DENOMINATION');

        $organismeInDb = new Organisme();
        $organismeInDb->setDescriptionOrg('OLD DENOMINATION');

        $newOrganisme = $organismeService->sync($organismeToUpdate, $organismeInDb);

        $this->assertEquals($organismeToUpdate->getDenominationOrg(), $newOrganisme->getDenominationOrg());
    }

    public function testCreateOnSuccess(): void
    {
        list(
            $container, $em, $marchePublieService, $configurationOrganismeService,
            $agentService, $socleHabilitationAgentService, $agentServiceMetierService, $organismeService
            ) = $this->prepareOrganismeService();

        $login = 'login';
        $mdp = 'mdp';
        $paramServiceMetierMpe = 1;
        $paramServiceMetierSocle = 2;
        $paramOrganismeReference = 'a1a';
        $paramPathInitScriptOrg = __DIR__ . '/../../../../legacy/protected/var/scripts-sql/init/scriptOrg.sql';
        $datetimeCreation = new \DateTime();
        $sigleSocle = 'SOCLE';
        $numberOfCreatedOrganisme = 1;
        $idProfileService = 1;
        $idProfileService2 = 12;

        $organisme = new Organisme();

        $administrateur = (new Administrateur())
            ->setLogin($login)
            ->setMdp($mdp)
        ;

        $serviceMetier = new ServiceMetier();
        $serviceMetier->setSigle($sigleSocle);

        $hyperAdmin = new class() extends Agent {
            public function getId()
            {
                return 42;
            }
        };

        $admin = new class() extends Agent {
            public function getId()
            {
                return 84;
            }
        };

        $em
            ->expects($this->once())
            ->method('beginTransaction')
        ;

        $administrateurRepository = $this->createMock(AdministrateurRepository::class);
        $administrateurRepository
            ->expects($this->once())
            ->method('getRandomAvailableOrganisme')
            ->willReturn($administrateur)
        ;

        $organismeExistsRepository = $this->createMock(OrganismeRepository::class);
        $organismeExistsRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['acronyme' => $login])
            ->willReturn(null)
        ;

        $em
            ->expects($this->exactly(2))
            ->method('persist')
        ;

        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $container
            ->expects($this->exactly(5))
            ->method('getParameter')
            ->willReturn(
                $paramServiceMetierMpe,
                $paramServiceMetierSocle,
                $paramOrganismeReference,
                $paramOrganismeReference,
                $paramPathInitScriptOrg
            )
        ;

        $marchePublieService
            ->expects($this->once())
            ->method('create')
            ->with($datetimeCreation->format('Y'), $login)
            ->willReturn(new MarchePublie())
        ;

        $configurationOrganismeService
            ->expects($this->once())
            ->method('copierConfigurationOrganisme')
            ->with($paramOrganismeReference, $login)
        ;

        $serviceMetierRepository = $this->createMock(ServiceMetierRepository::class);
        $serviceMetierRepository
            ->expects($this->once())
            ->method('find')
            ->with($paramServiceMetierSocle)
            ->willReturn($serviceMetier)
        ;

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects($this->once())
            ->method('syncTable')
            ->with($paramOrganismeReference, $login, $paramServiceMetierSocle, $paramPathInitScriptOrg)
            ->willReturn(true)
        ;

        $organismeRepository
            ->expects($this->once())
            ->method('getNumberOfCreatedOrganisme')
            ->willReturn($numberOfCreatedOrganisme)
        ;

        $agentService
            ->expects($this->once())
            ->method('createHyperAdmin')
            ->with($organisme)
            ->willReturn($hyperAdmin)
        ;

        $socleHabilitationAgentService
            ->expects($this->exactly(2))
            ->method('createForAdmin')
            ->withConsecutive([$hyperAdmin], [$admin])
        ;

        $agentServiceMetierService
            ->expects($this->exactly(4))
            ->method('createForAdmin')
            ->withConsecutive(
                [42, $paramServiceMetierMpe, $idProfileService],
                [42, $paramServiceMetierSocle, $idProfileService2],
                [84, $paramServiceMetierMpe, $idProfileService],
                [84, $paramServiceMetierSocle, $idProfileService2],
            )
        ;

        $agentService
            ->expects($this->once())
            ->method('createAdmin')
            ->with($organisme, $mdp)
            ->willReturn($admin)
        ;

        $em
            ->expects($this->once())
            ->method('commit')
        ;

        $em
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $administrateurRepository,
                $organismeExistsRepository,
                $serviceMetierRepository,
                $organismeRepository,
                $organismeRepository
            )
        ;

        $result = $organismeService->create($organisme);
        $this->assertSame($organisme, $result);
    }

    public function testCreateOnErrorExceptionApiProblemAlreadyExistException(): void
    {
        list(
            $container, $em, $marchePublieService, $configurationOrganismeService,
            $agentService, $socleHabilitationAgentService, $agentServiceMetierService, $organismeService
            ) = $this->prepareOrganismeService();

        $login = 'login';

        $organisme = new Organisme();

        $organismeExists = new Organisme();

        $administrateur = (new Administrateur())
            ->setLogin($login)
        ;

        $em
            ->expects($this->once())
            ->method('beginTransaction')
        ;

        $administrateurRepository = $this->createMock(AdministrateurRepository::class);
        $administrateurRepository
            ->expects($this->once())
            ->method('getRandomAvailableOrganisme')
            ->willReturn($administrateur)
        ;

        $organismeExistsRepository = $this->createMock(OrganismeRepository::class);
        $organismeExistsRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['acronyme' => $login])
            ->willReturn($organismeExists)
        ;

        $em
            ->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $administrateurRepository,
                $organismeExistsRepository,
            )
        ;

        $this->expectException(ApiProblemAlreadyExistException::class);
        $this->expectExceptionMessage("object already exist");

        $organismeService->create($organisme);
    }

    public function testUpdateOnErrorApiProblemNotFoundException(): void
    {
        list(
            $container, $em, $marchePublieService, $configurationOrganismeService,
            $agentService, $socleHabilitationAgentService, $agentServiceMetierService, $organismeService
            ) = $this->prepareOrganismeService();

        $login = 'login';

        $organisme = (new Organisme())
            ->setAcronyme($login)
        ;

        $organismeExistsRepository = $this->createMock(OrganismeRepository::class);
        $organismeExistsRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['acronyme' => $login])
            ->willReturn(null)
        ;

        $em
            ->expects($this->never())
            ->method('persist')
        ;

        $em
            ->expects($this->never())
            ->method('flush')
        ;

        $em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($organismeExistsRepository)
        ;

        $this->expectException(ApiProblemNotFoundException::class);
        $organismeService->update($organisme);
    }

    public function existsDataProvider()
    {
        return [
            ['', null, false],
            ['abcd', null, false],
            ['e3r', new Organisme(), true],
            ['enovos', new Organisme(), true],
        ];
    }

    /**
     * @dataProvider existsDataProvider
     * @param $acronyme
     * @param $returnedData
     * @param $expected
     */
    public function testExists($acronyme, $returnedData, $expected)
    {
        list(
            $container, $em, $marchePublieService, $configurationOrganismeService,
            $agentService, $socleHabilitationAgentService, $agentServiceMetierService, $organismeService
            ) = $this->prepareOrganismeService();

        $organismeExistsRepository = $this->createMock(OrganismeRepository::class);
        $organismeExistsRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->willReturn($returnedData);

        $em->method('getRepository')
            ->willReturn($organismeExistsRepository);

        /** @var OrganismeService $organismeService */
        $result = $organismeService->exists($acronyme);

        $this->assertIsBool($result);
        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    protected function prepareOrganismeService(): array
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $marchePublieService = $this->createMock(MarchePublieService::class);
        $configurationOrganismeService = $this->createMock(ConfigurationOrganismeService::class);
        $agentService = $this->createMock(AtexoAgent::class);
        $socleHabilitationAgentService = $this->createMock(SocleHabilitationAgentService::class);
        $agentServiceMetierService = $this->createMock(AgentServiceMetierService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $organismeService = new OrganismeService(
            $container,
            $em,
            $marchePublieService,
            $configurationOrganismeService,
            $agentService,
            $socleHabilitationAgentService,
            $agentServiceMetierService,
            $logger
        );
        return [
            $container,
            $em,
            $marchePublieService,
            $configurationOrganismeService,
            $agentService,
            $socleHabilitationAgentService,
            $agentServiceMetierService,
            $organismeService
        ];
    }

    public function testGetNomCourantAcheteurPublic()
    {
        $configurationOrganisme = new ConfigurationOrganisme();
        $configurationOrganisme->setOrganisationCentralisee('0');

        $configurationOrganismeRepo = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepo->expects($this->once())->method('findOneBy')->willReturn($configurationOrganisme);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())->method('getRepository')->willReturn($configurationOrganismeRepo);

        $organismeService = new OrganismeService(
            $this->createMock(ContainerInterface::class),
            $em,
            $this->createMock(MarchePublieService::class),
            $this->createMock(ConfigurationOrganismeService::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(SocleHabilitationAgentService::class),
            $this->createMock(AgentServiceMetierService::class),
            $this->createMock(LoggerInterface::class)
        );

        $organisme = new Organisme();
        $organisme->setDenominationOrg('un organisme');
        $organisme->setCp('75001');
        $organisme->setVille('Paris');

        $agent = new Agent();
        $agent->setOrganisme($organisme);

        $nom = $organismeService->getNomCourantAcheteurPublic($agent);

        $this->assertEquals('un organisme (75001 - Paris)', $nom);
    }

    public function testGetNomCourantAcheteurPublicWithOrganisationCentralisee()
    {
        $configurationOrganisme = new ConfigurationOrganisme();
        $configurationOrganisme->setOrganisationCentralisee('1');

        $configurationOrganismeRepo = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepo->expects($this->exactly(2))
            ->method('findOneBy')
            ->willReturn($configurationOrganisme);

        $service = new Service();
        $service->setLibelle('un service');
        $service->setCp('01000');
        $service->setVille('Ain');

        $serviceRepo = $this->createMock(ServiceRepository::class);
        $serviceRepo->expects($this->once())
            ->method('findOneBy')
            ->willReturn($service);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(3))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($configurationOrganismeRepo, $configurationOrganismeRepo, $serviceRepo);

        $organismeService = new OrganismeService(
            $this->createMock(ContainerInterface::class),
            $em,
            $this->createMock(MarchePublieService::class),
            $this->createMock(ConfigurationOrganismeService::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(SocleHabilitationAgentService::class),
            $this->createMock(AgentServiceMetierService::class),
            $this->createMock(LoggerInterface::class)
        );

        $organisme = new Organisme();
        $organisme->setDenominationOrg('un organisme');
        $organisme->setCp('75001');
        $organisme->setVille('Paris');

        $agent = new Agent();
        $agent->setOrganisme($organisme);

        $nom = $organismeService->getNomCourantAcheteurPublic($agent);
        $this->assertEquals('un organisme (75001 - Paris)', $nom);

        $agent->setServiceId(1);

        $nom = $organismeService->getNomCourantAcheteurPublic($agent);
        $this->assertEquals('un organisme - un service (01000 - Ain)', $nom);
    }
}
