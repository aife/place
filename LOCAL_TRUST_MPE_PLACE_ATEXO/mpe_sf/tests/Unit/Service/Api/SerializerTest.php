<?php

namespace Tests\Unit\Service\Api;

use App\Service\Api\Serializer;
use App\Service\ContratService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SerializerTest extends TestCase
{
    private $serializer;

    public function setUp(): void
    {
        date_default_timezone_set('Europe/Paris');

        $container = $this->createMock(ContainerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $contratService = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);

        $this->serializer = new Serializer(
            $container,
            $em,
            $logger,
            $contratService,
            $parameterBag,
            $webServiceExec
        );
    }

    public function testEmptyArrayJson()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","data":{}}}}';
        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            [],
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    public function testSimpleArrayJson()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","data":{"0":"id","1":"id","2":"id"}}}}';

        $array = ['id', 'id', 'id'];
        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    public function testArrayOfArrayJson()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","data":{"data":[{"index0":"valeur0"},{"index1":"valeur1"},'.
            '{"index2":"valeur2"},{"index3":"valeur3"},{"index4":"valeur4"}]
}}}}';

        $array['data'] = [];
        for ($i = 0; $i < 5; ++$i) {
            $array['data'][$i]['index'.$i] = 'valeur'.$i;
        }

        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    public function testArrayOfArrayOfArrayJson()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","data":{"data-2":"data0":[{"index0":"valeur0"},'.
            '{"index1":"valeur1"},{"index2":"valeur2"},{"index3":"valeur3"},{"index4":"valeur4"}]
,{"data-1":[{"index0":"valeur0"},{"index1":"valeur1"},{"index2":"valeur2"},{"index3":"valeur3"},{"index4":"valeur4"}]}
}}}}';

        $array['data-2'] = [];
        for ($i = 0; $i < 5; ++$i) {
            $array['data-2'][$i]['index'.$i] = 'valeur'.$i;
        }

        $array['data0']['data-1'] = $array['data-2'];

        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    public function testEmptyArrayXml()
    {
        $expect = '<?xml version="1.0"?>
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
<reponse statutReponse="OK"></reponse>
</mpe>
';

        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            [],
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    /**
     * Test unitaire a revoir !
     */
    /*public function testSimpleArrayXml()
    {
        ob_start();
        $array = ['id', 'id', 'id'];
        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->throwException(new Exception('le format du tableau n\'est pas valide !'));
        ob_end_clean();
    }*/

    public function testArrayOfArrayXml()
    {
        $expect = '<?xml version="1.0"?>
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
<reponse statutReponse="OK"><data>
<item0><index0>valeur0</index0></item0><item1><index1>valeur1</index1></item1><item2><index2>valeur2</index2>'.
            '</item2><item3><index3>valeur3</index3></item3><item4><index4>valeur4</index4></item4>
</data>
</reponse>
</mpe>
';

        $array['data'] = [];
        for ($i = 0; $i < 5; ++$i) {
            $array['data']['item'.$i]['index'.$i] = 'valeur'.$i;
        }

        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->expectOutputString($expect);
    }

    public function testArrayOfArrayOfArrayXml()
    {
        $expect = '<?xml version="1.0"?>
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
<reponse statutReponse="OK"><data-2>
<item key="0"><index0>valeur0</index0></item><item key="1"><index1>valeur1</index1></item>'.
            '<item key="2"><index2>valeur2</index2></item><item key="3"><index3>valeur3</index3></item>'.
            '<item key="4"><index4>valeur4</index4></item>
</data-2>
</reponse>
</mpe>
';

        for ($i = 0; $i < 5; ++$i) {
            $array['data-2'][$i]['index'.$i] = 'valeur'.$i;
        }

        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            $array,
            false,
            true
        );
        $this->expectOutputString($expect);
    }
}
