<?php

namespace Tests\Unit\Service\Api;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Repository\OrganismeRepository;
use App\Service\Api\Serializer;
use App\Service\ContratService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OrganismeTest extends TestCase
{
    private Serializer $serializer;
    private EntityManager $em;

    public function setUp(): void
    {
        $container     = $this->createMock(ContainerInterface::class);
        $logger        = $this->createMock(LoggerInterface::class);
        $objectManager = $this->getObjectManager();
        $container->method('get')->with('doctrine.orm.entity_manager')->willReturn($objectManager);
        $this->em         = $objectManager;
        $contratService   = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);

        $this->serializer = new Serializer(
            $container,
            $objectManager,
            $logger,
            $contratService,
            $parameterBag,
            $webServiceExec
        );
    }

    protected function getObjectManager(): EntityManager
    {
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->method('getRepository')
                      ->will($this->returnCallback([$this, 'getRepositoryMock']));

        return $objectManager;
    }

    public function getRepositoryMock($entityName)
    {
        static $callCount = [];
        $callCount[$entityName] = ! isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];

        if (Organisme::class === $entityName) {
            $organisme = new Organisme();
            $class     = new ReflectionClass($organisme);
            $property  = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($organisme, 1);

            $organismeRepository = $this->createMock(OrganismeRepository::class);
            $organismeRepository->method('findAllByParamWs')
                                ->willReturn([$organisme]);

            return $organismeRepository;
        } elseif (ConfigurationOrganisme::class === $entityName) {
            $repository = $this->createMock(OrganismeRepository::class);
            $repository->method('findOneBy')
                       ->willReturn([new ConfigurationOrganisme()]);

            return $repository;
        }

        return null;
    }

    public function testConstructResponseJsonWS()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","organismes":[{"id":1,"acronyme":"","categorieInsee":null,';
        $expect .= '"sigle":"","siren":"","idExterne":0,"idEntite":null,"denomination":null,"nic":"",';
        $expect .= '"adresse":{"rue":"","codePostal":"","ville":"","pays":null},"description":null,"logo":[]}' . "\n";
        $expect .= ']}}}';
        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            $this->em->getRepository(Organisme::class),
            false
        );
        $this->expectOutputString($expect);
    }

    public function testConstructResponseXmlWS()
    {
        $expect = /* @lang text */
            <<<EOD
        <?xml version="1.0"?>
        <mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
        <reponse statutReponse="OK"><organismes>
        <organisme><id>1</id><acronyme></acronyme><categorieInsee/><sigle></sigle><siren></siren><idExterne>0</idExterne><idEntite/><denomination/><nic></nic><adresse><rue></rue><codePostal></codePostal><ville></ville><pays/></adresse><description/><logo/></organisme>
        </organismes>
        </reponse>
        </mpe>
        
        EOD;

        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            $this->em->getRepository(Organisme::class),
            false
        );

        $this->expectOutputString($expect);
    }
}
