<?php

namespace Tests\Unit\Service\Api;

use App\Entity\ContactContrat;
use App\Repository\ContactContratRepository;
use App\Service\Api\Serializer;
use App\Service\ContratService;
use App\Service\WebServices\WebServicesExec;
use DateTime;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ContactTest extends TestCase
{
    private $serializer;

    public function setUp(): void
    {
        date_default_timezone_set('Europe/Paris');

        $container = $this->createMock(ContainerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->getObjectManager();
        $this->em = $objectManager;
        $contratService = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);

        $this->serializer = new Serializer(
            $container,
            $objectManager,
            $logger,
            $contratService,
            $parameterBag,
            $webServiceExec,
        );
    }

    protected function getObjectManager()
    {
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($entityName) {
                        static $callCount = [];
                        $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];
                        if (ContactContrat::class === $entityName) {
                            $contact = new ContactContrat();
                            $contact->headWs = 'entreprises';
                            $alimContact = [
                                'idContactContrat' => '200',
                                'nom' => 'DUPOND',
                                'prenom' => 'JEAN',
                                'email' => 'test@test.com',
                                'telephone' => '0625252525',
                            ];
                            foreach ($alimContact as $field => $value) {
                                $class = new ReflectionClass($contact);
                                $property = $class->getProperty($field);
                                $property->setAccessible(true);
                                $property->setValue($contact, $value);
                            }
                            $contact->setCreatedAt(new DateTime('2019-09-27 16:00:00'));

                            $contactRepository = $this->createMock(ContactContratRepository::class);
                            $contactRepository->expects($this->any())
                                ->method('findAllByParamWs')
                                ->willReturn([$contact]);
                            $contactRepository->headWs = 'contacts';

                            return $contactRepository;
                        }
                    }
                )
            );

        return $objectManager;
    }

    public function testConstructResponseWSJson()
    {
        $expect = '{"mpe":{"reponse":{"statutReponse":"OK","contacts":[{"nom":"DUPOND","prenom":"JEAN",' .
            '"email":"test@test.com","telephone":"0625252525","id":200,"inscrit":false,"idEtablissement":null,' .
            '"idEntreprise":null,"dateCreation":{"date":"2019-09-27 16:00:00.000000","timezone_type":3,' .
            '"timezone":"Europe\/Paris"}}' . "\n" . ']}}}';
        $this->serializer->constructResponseWS(
            'json',
            '1',
            '1',
            '1458035148',
            $this->em->getRepository(ContactContrat::class),
            false
        );
        $this->expectOutputString($expect);
    }

    public function testConstructResponseWSXml()
    {
        $expect = '<?xml version="1.0"?>
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
<reponse statutReponse="OK"><contacts>
<contact><nom>DUPOND</nom><prenom>JEAN</prenom><email>test@test.com</email><telephone>0625252525</telephone>' .
            '<id>200</id><inscrit>0</inscrit><idEtablissement/><idEntreprise/>' .
            '<dateCreation>2019-09-27T16:00:00+02:00</dateCreation></contact>
</contacts>
</reponse>
</mpe>
';

        $this->serializer->constructResponseWS(
            'xml',
            '1',
            '1',
            '1458035148',
            $this->em->getRepository(ContactContrat::class),
            false
        );

        $this->expectOutputString($expect);
    }
}
