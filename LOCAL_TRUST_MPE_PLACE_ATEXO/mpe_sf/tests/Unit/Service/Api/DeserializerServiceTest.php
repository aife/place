<?php

namespace Tests\Unit\Service\Api;

use App\Entity\Organisme;
use App\Service\Api\DeserializerService;
use PHPUnit\Framework\TestCase;

class DeserializerServiceTest extends TestCase
{
    public function testDeserializeOrganisme()
    {
        $denormalizerService = new DeserializerService();
        $organisme = $denormalizerService->deserializeOrganisme($this->getData(), 'json');

        $this->assertInstanceOf(Organisme::class, $organisme);
        $this->assertEquals('pmi-min-1', $organisme->getAcronyme());
        $this->assertEquals('6 RUE DE LA PAIX', $organisme->getAdresse());
        $this->assertEquals('93600', $organisme->getCp());
    }

    private function getData()
    {
        return [
            'id' => 1,
            'acronyme' => 'pmi-min-1',
            'adresse' => [
                'rue' => '6 RUE DE LA PAIX',
                'codePostal' => '93600',
            ],
        ];
    }
}
