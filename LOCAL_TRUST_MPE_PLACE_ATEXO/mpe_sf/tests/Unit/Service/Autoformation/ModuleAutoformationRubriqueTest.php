<?php

namespace Tests\Unit\Service\Autoformation;

use App\Entity\Autoformation\Rubrique;
use App\Entity\Langue;
use App\Repository\LangueRepository;
use App\Service\Autoformation\ModuleAutoformationRubrique;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Twig\Environment;

class ModuleAutoformationRubriqueTest extends TestCase
{
    public function testGetBody()
    {
        $body = '<body>Rubrique 1</body>';
        $envMock = $this->createMock(Environment::class);

        $envMock->expects($this->exactly(2))
            ->method('render')
            ->willReturn($body);

        $emMock = $this->createMock(EntityManagerInterface::class);

        $rubriqueMock = new ModuleAutoformationRubrique(
            $envMock,
            $emMock,
            $this->createMock( LoggerInterface::class)
        );

        $response = $rubriqueMock->getBody('entreprise');
        $this->assertEquals($body, $response);

        $response = $rubriqueMock->getBody('agent');
        $this->assertEquals($body, $response);

        $this->expectExceptionMessage('La page n\'exixte pas');
        $response = $rubriqueMock->getBody('entrepris');

        $this->assertIsString($response);
    }

    public function testCreate()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $langueRepositoryMock = $this->createMock(LangueRepository::class);
        $langueRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(
                new Langue()
            );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($langueRepositoryMock);

        $rubriqueMock = new ModuleAutoformationRubrique(
            $envMock,
            $emMock,
            $this->createMock( LoggerInterface::class)
        );

        $data = [
            'nameFr' => 'Rubrique 1',
            'page' => 'agent',
        ];

        $response = $rubriqueMock->create($data);

        $this->assertIsArray($response);
    }

    public function testCreateKo()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $langueRepositoryMock = $this->createMock(LangueRepository::class);
        $langueRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($langueRepositoryMock);

        $rubriqueMock = new ModuleAutoformationRubrique(
            $envMock,
            $emMock,
            $this->createMock( LoggerInterface::class)
        );

        $data = [
            'nameFr' => 'Rubrique 1',
            'page' => 'agent',
        ];

        $this->expectExceptionMessage('La langue fr n\'existe pas');
        $rubriqueMock->create($data);
    }

    public function testUpdate()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $rubriqueMock = new ModuleAutoformationRubrique(
            $envMock,
            $emMock,
            $this->createMock( LoggerInterface::class)
        );

        $data = [
            'nameFr' => 'Rubrique 1',
            'page' => 'agent',
        ];

        $rubrique = new Rubrique();

        $response = $rubriqueMock->update($rubrique, $data);

        $this->assertIsArray($response);
    }
}
