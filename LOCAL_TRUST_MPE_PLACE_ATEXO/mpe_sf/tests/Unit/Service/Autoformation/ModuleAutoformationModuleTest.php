<?php

namespace Tests\Unit\Service\Autoformation;

use App\Entity\Autoformation\ModuleAutoformation;
use App\Entity\Autoformation\Rubrique;
use App\Entity\Langue;
use App\Repository\Autoformation\RubriqueRepository;
use App\Repository\LangueRepository;
use App\Service\Autoformation\ModuleAutoformationModule;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class ModuleAutoformationModuleTest extends TestCase
{
    public function testGetBody()
    {
        $body = '<body>Rubrique 1</body>';
        $envMock = $this->createMock(Environment::class);
        $envMock->expects($this->exactly(2))
            ->method('render')
            ->willReturn($body);

        $emMock = $this->createMock(EntityManagerInterface::class);

        $rubriqueRepositoryMock = $this->createMock(RubriqueRepository::class);
        $rubriqueRepositoryMock->expects($this->exactly(2))
            ->method('findBy')
            ->willReturn([]);

        $emMock->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturn($rubriqueRepositoryMock);

        $moduleMock = new ModuleAutoformationModule(
            $envMock,
            $emMock
        );

        $response = $moduleMock->getBody('entreprise');
        $this->assertEquals($body, $response);

        $response = $moduleMock->getBody('agent');
        $this->assertEquals($body, $response);

        $this->expectExceptionMessage('La page n\'exixte pas');
        $response = $moduleMock->getBody('entrepris');
    }

    public function testPageExists()
    {
        $moduleMock = new  ModuleAutoformationModule(
            $this->createMock(Environment::class),
            $this->createMock(EntityManagerInterface::class)
        );

        $response = $moduleMock->pageExists('entreprise');
        $this->assertTrue($response);

        $response = $moduleMock->pageExists('agent');
        $this->assertTrue($response);

        $response = $moduleMock->pageExists('entrepris');
        $this->assertFalse($response);

    }

    public function testCreate()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $langueRepositoryMock = $this->createMock(LangueRepository::class);
        $langueRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(
                new Langue()
            );

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($langueRepositoryMock);

        $moduleMock = new ModuleAutoformationModule(
            $envMock,
            $emMock
        );

        $rubrique = new Rubrique();
        $data = [
            'nameFr' => 'Rubrique 1',
            'descriptionFr' => 'description 1',
            'page' => 'agent',
            'indexTypeFormat' => 0,
            'urlExterne' => 'test.com',
            'duration' => 1,
            'order' => 1
        ];

        $response = $moduleMock->create($rubrique, $data);

        $this->assertEquals(['success' => true], $response);
    }

    /**
     * @group test
     */
    public function testCreateKo()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);


        $langueRepositoryMock = $this->createMock(LangueRepository::class);
        $langueRepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($langueRepositoryMock);

        $moduleMock = new ModuleAutoformationModule(
            $envMock,
            $emMock
        );

        $rubrique = new Rubrique();
        $data = [
            'nameFr' => 'Rubrique 1',
            'descriptionFr' => 'description 1',
            'page' => 'agent',
            'indexTypeFormat' => 0,
            'urlExterne' => 'test.com',
            'duration' => 1,
            'order' => 1
        ];

        $this->expectExceptionMessage('La langue fr n\'existe pas');

        $moduleMock->create($rubrique, $data);

    }

    public function testUpdate()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $emMock->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturnCallback(
                function ($entity) {
                    if ($entity == Rubrique::class) {
                        $repository = $this->createMock(RubriqueRepository::class);
                        $repository->expects($this->once())
                            ->method('find')
                            ->willReturn(new Rubrique());

                        return $repository;
                    }
                    if ($entity == Langue::class) {
                        $repository = $this->createMock(LangueRepository::class);
                        $repository->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn(new Langue());

                        return $repository;
                    }
                }
            );

        $moduleMock = new ModuleAutoformationModule(
            $envMock,
            $emMock
        );

        $module = new ModuleAutoformation();
        $data = [
            'idRubrique' => 1,
            'nameFr' => 'Rubrique 1',
            'descriptionFr' => 'description 1',
            'page' => 'agent',
            'indexTypeFormat' => 0,
            'urlExterne' => 'test.com',
            'duration' => 1,
            'order' => 1
        ];

        $response = $moduleMock->update($module, $data);

        $this->assertEquals(['success' => true], $response);
    }

    public function testUpdateKo()
    {
        $envMock = $this->createMock(Environment::class);
        $emMock = $this->createMock(EntityManagerInterface::class);

        $emMock->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturnCallback(
                function ($entity) {
                    if ($entity == Rubrique::class) {
                        $repository = $this->createMock(RubriqueRepository::class);
                        $repository->expects($this->once())
                            ->method('find')
                            ->willReturn(new Rubrique());

                        return $repository;
                    }
                    if ($entity == Langue::class) {
                        $repository = $this->createMock(LangueRepository::class);
                        $repository->expects($this->once())
                            ->method('findOneBy')
                            ->willReturn(null);

                        return $repository;
                    }
                }
            );

        $moduleMock = new ModuleAutoformationModule(
            $envMock,
            $emMock
        );

        $module = new ModuleAutoformation();
        $data = [
            'idRubrique' => 1,
            'nameFr' => 'Rubrique 1',
            'descriptionFr' => 'description 1',
            'page' => 'agent',
            'indexTypeFormat' => 0,
            'urlExterne' => 'test.com',
            'duration' => 1,
            'order' => 1
        ];

        $this->expectExceptionMessage('La langue fr n\'existe pas');

       $moduleMock->update($module, $data);
    }
}
