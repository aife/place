<?php

namespace Tests\Unit\Service\Autoformation;

use App\Repository\Autoformation\RubriqueRepository;
use App\Service\Autoformation\ModuleAutoformation;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Twig\Environment;

class ModuleAutoformationTest extends TestCase
{
    public function testGetBody()
    {
        $body = '<body>Rubrique 1</body>';
        $envMock = $this->createMock(Environment::class);
        $envMock->expects($this->exactly(2))
            ->method('render')
            ->willReturn($body);

        $emMock = $this->createMock(EntityManagerInterface::class);

        $rubriqueRepositoryMock = $this->createMock(RubriqueRepository::class);
        $rubriqueRepositoryMock->expects($this->exactly(2))
            ->method('findBy')
            ->willReturn([]);

        $emMock->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturn($rubriqueRepositoryMock);

        $moduleMock = new ModuleAutoformation(
            $envMock,
            $emMock
        );

        $response = $moduleMock->getBody('entreprise');
        $this->assertEquals($body, $response);

        $response = $moduleMock->getBody('agent');
        $this->assertEquals($body, $response);

        $this->expectExceptionMessage('La page n\'exixte pas');
        $response = $moduleMock->getBody('entrepris');

    }

    public function testPageExists()
    {
        $moduleMock = new ModuleAutoformation(
            $this->createMock(Environment::class),
            $this->createMock(EntityManagerInterface::class)
        );

        $response = $moduleMock->pageExists('entreprise');
        $this->assertTrue($response);

        $response = $moduleMock->pageExists('agent');
        $this->assertTrue($response);

        $response = $moduleMock->pageExists('entrepris');
        $this->assertFalse($response);
    }

    public function testGetModuleMenuList()
    {
        $envMock = $this->createMock(Environment::class);

        $envMock->expects($this->once())
            ->method('render')
            ->willReturn('<li>Rubrique 1</li>');

        $emMock = $this->createMock(EntityManagerInterface::class);

        $rubriqueRepositoryMock = $this->createMock(RubriqueRepository::class);
        $rubriqueRepositoryMock->expects($this->once())
            ->method('findBy')
            ->willReturn([]);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($rubriqueRepositoryMock);

        $moduleMock = new ModuleAutoformation(
            $envMock,
            $emMock
        );

        $moduleMock->getModuleMenuList();
    }
}
