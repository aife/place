<?php

namespace Tests\Unit\Service\EventSubscriber;

use ReflectionClass;
use App\EventSubscriber\ModuleAutoformationSubscriber;
use App\Service\AtexoUtil;
use App\Service\File\FileUploader;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\RequestStack;

class ModuleAutoformationSubscriberTest extends TestCase
{
    public function testStrWithoutAccents()
    {
        $mock = new ModuleAutoformationSubscriber(
            $this->createMock(FileUploader::class),
            $this->createMock(RequestStack::class),
            $this->createMock(AtexoUtil::class)
        );

        $class = new ReflectionClass($mock);

        $method = $class->getMethod('strWithoutAccents');
        $method->setAccessible(true);

        $value = 'éAE_Dissocie';
        $response = $method->invoke($mock, $value);
        $this->assertEquals('eAE_Dissocie', $response);

        $value = 'accentuées';
        $response = $method->invoke($mock, $value);
        $this->assertEquals('accentuees', $response);

        $value = 'Ernö';
        $response = $method->invoke($mock, $value);
        $this->assertEquals('Erno', $response);

        $value = 'ÂÊÎÔÛÄËÏ';
        $response = $method->invoke($mock, $value);
        $this->assertEquals('AEIOUAEI', $response);

        $value = 'ÖÜÇÉ';
        $response = $method->invoke($mock, $value);
        $this->assertEquals('OUCE', $response);
    }
}
