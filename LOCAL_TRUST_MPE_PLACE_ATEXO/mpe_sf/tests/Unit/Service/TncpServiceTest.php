<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Service\TncpService;
use App\Service\WebservicesInterfaceSuivi;
use PHPUnit\Framework\TestCase;

class TncpServiceTest extends TestCase
{
    public function testGetConsultationStatus()
    {
        $response = new \stdClass();
        $objet1 = new \stdClass();
        $objet1->action = 'PUBLICATION_CONSULTATION';
        $objet1->statut = 1;
        $objet2 = new \stdClass();
        $objet2->action = 'MODIFICATION_DATE_LIMITE_REMISE_PLI';
        $objet2->statut = 3;
        $objet2->dateCreation = '2022-09-16T12:14:00+02:00';
        $objet3 = new \stdClass();
        $objet3->action = 'MODIFICATION_DATE_LIMITE_REMISE_PLI';
        $objet3->statut = 1;
        $response->{'hydra:member'} = [$objet1, $objet2, $objet3];
        $suiviService =  $this->createMock(WebservicesInterfaceSuivi::class);
        $suiviService->method('getContent')
           ->willReturn($response);

        $service = new TncpService($suiviService);
        $status = $service->getConsultationStatus(1);

        $this->assertArrayHasKey('status', $status);
        $this->assertArrayHasKey('date', $status);
        $this->assertSame('EN_ATTENTE', $status['status']['PUBLICATION_CONSULTATION']);
        $this->assertSame('NON_REALISE', $status['status']['MODIFICATION_DCE']);
        $this->assertSame('16/09/2022 12:14:00', $status['date']['MODIFICATION_DATE_LIMITE_REMISE_PLI']);
    }
}
