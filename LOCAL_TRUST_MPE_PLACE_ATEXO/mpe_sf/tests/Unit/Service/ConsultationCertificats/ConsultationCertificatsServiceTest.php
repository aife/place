<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\ConsultationCertificats;

use App\Entity\CertificatChiffrement;
use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Repository\CertificatChiffrementRepository;
use App\Service\ConsultationCertificats\ConsultationCertificatsService;
use PHPUnit\Framework\TestCase;

class ConsultationCertificatsServiceTest extends TestCase
{
    public function testGetList()
    {
        $CertificatChiffrement = $this->createMock(CertificatChiffrementRepository::class);
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-min-1');
        $consultation = new Consultation();
        $consultation->setId(500002);
        $consultation->setOrganisme($organisme);
        $certificatChiffrement = new CertificatChiffrement();
        $certificatChiffrement->setId(33514);
        $certificatChiffrement->setIndexCertificat(80);
        $certificatChiffrement->setSousPli(0);
        $context = [
            $certificatChiffrement
        ];

        $CertificatChiffrement->expects(self::once())
        ->method('findCertificats')->willReturn($context);

        $service = new ConsultationCertificatsService($CertificatChiffrement);
        $list = $service->getList($consultation);

        $this->assertArrayHasKey('indexCertificat', $list[0]);
        $this->assertEquals($list[0]['id'], 33514);
    }
}
