<?php

namespace Tests\Unit\Service;

use DateTime;
use ReflectionClass;
use App\Entity\BloborganismeFile;
use App\Service\AtexoFichier;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\Local;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use App\Utils\Filesystem\MountManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AtexoFichierTest extends TestCase
{
    private $baseRootDir;
    private $container;
    private $documentRootDirMultiple;
    private $em;
    private $fileSystem;
    private $atexoAdapter;
    private $organisme;
    private $mountManager;

    protected function setUp(): void
    {
        $this->organisme = 'pmi-min-1';
        $this->baseRootDir = '/opt/local-trust/';
        $this->container = $this->createMock(ContainerInterface::class);
        $this->documentRootDirMultiple =
            '/tmp/mnt2/;' .
            '/tmp/mnt1/;' .
            '/tmp/mnt3/';
        $this->em = $this->createMock(EntityManager::class);

        $this->fileSystem = $this->createMock(Filesystem::class);

        $this->fileSystem->expects($this->any())
            ->method('has')
            ->with($this->anything())
            ->willReturn(true);

        $this->fileSystem->expects($this->any())
            ->method('copy')
            ->with($this->anything())
            ->willReturn(true);

        $this->fileSystem->expects($this->any())
            ->method('sha1File')
            ->with($this->anything())
            ->willReturn('azertyuiop');

        $this->fileSystem->expects($this->any())
            ->method('write')
            ->with($this->anything())
            ->willReturn(true);

        $this->atexoAdapter = $this->createMock(Local::class);

        $this->fileSystem->expects($this->any())
            ->method('getRepertoire')
            ->with($this->anything())
            ->willReturn((new DateTime())->format('Y/m/d') . '/pmi-min-1/files/');
    }

    public function testInsertFile()
    {
        $organismeBlobMock = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobMock);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobMock, 1);

        $registy = $this->createMock(Registry::class);
        $mock = $this->createMock(EntityManager::class);

        $registy->expects($this->any())
            ->method('getManager')
            ->willReturn($mock);

        $this->container->expects($this->any())
            ->method('get')
            ->willReturn($registy);
        $this->fileSystemNas = new Filesystem(new MemoryAdapter());
        $pathFile = 'tmpGenere.txt';
        $this->fileSystem->write($pathFile, 'fichier de test');
        $this->fileSystemTmp = new Filesystem(new MemoryAdapter());
        $this->fileSystemTmp->write($pathFile, 'fichier de test');

        $this->getMountmanager();

        $atexoFichierOrganisme = new AtexoFichier(
            $this->container,
            $mock,
            $this->mountManager
        );

        $pathFile = 'tmpGenere.txt';
        $fileName = 'tmpGenere.txt';
        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $fileSystemNas->write($fileName, 'test');
        $t = $atexoFichierOrganisme->insertFile(
            $fileName,
            $pathFile,
            $this->organisme
        );

        //Normalement cela envoie un Id mais dans le cas de ce test on renvoie un id Null
        $this->assertEquals(0, $t);
    }

    public function testCreateDir()
    {
        $registy = $this->createMock(Registry::class);
        $mock = $this->createMock(EntityManager::class);

        $registy->expects($this->any())
            ->method('getManager')
            ->willReturn($mock);

        $this->container->expects($this->any())
            ->method('get')
            ->willReturn($registy);

        $this->fileSystemNas = new Filesystem(new MemoryAdapter());

        $this->getMountmanager();

        $atexoFichierOrganisme = new AtexoFichier(
            $this->container,
            $mock,
            $this->mountManager
        );

        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $fileSystemNas->createDir('directoryAlreadyExist');
        $isCreated = $atexoFichierOrganisme->createDir('newDirectory');
        $this->assertTrue($isCreated);
    }

    public function getMountmanager()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $this->mountManager = new MountManager(
            $logger,
            $this->createMock(EntityManagerInterface::class),
            '/data/local-trust/mpe_place_test/',
            '/var/tmp/mpe/',
            '/tmp/mnt2;/tmp/mnt3;/tmp/mnt5;/tmp/mnt4',
            new MemoryAdapter(),
        );
    }
}
