<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\ApiGateway;

use App\Service\ApiGateway\WebServicesSynchro;
use App\Service\Entreprise\EntrepriseAPIService;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesSynchroTest extends TestCase
{
    public function testSynchroApiGatewayExercices(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $apiService = $this->createMock(EntrepriseAPIService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $httpClient = $this->createMock(HttpClientInterface::class);

        $siren = 'siren';
        $nic = 'nic';
        $urlExercice = 'api-gateway-url/dgfip/etablissements/sirennic/chiffres_affaires/';

        $url = $urlExercice;
        $siretRecipient = '123456789';

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['URL_API_ENTREPRISE_CHIFFRES_AFFAIRES'],
                ['API_ENTREPRISE_TOKEN']
            )
            ->willReturnOnConsecutiveCalls(
                $urlExercice,
                $siretRecipient
            )
        ;

        $responseBody = [
            "exercices" => [
                [
                    "chiffre_affaires" => "42000",
                    "date_fin_exercice" => "2020-12-31",
                    "date_fin_exercice_timestamp" => 1609369200
                ],
                [
                    "chiffre_affaires" => "43442",
                    "date_fin_exercice" => "2019-12-31",
                    "date_fin_exercice_timestamp" => 1577746800
                ],
                [
                    "chiffre_affaires" => "43624",
                    "date_fin_exercice" => "2018-12-31",
                    "date_fin_exercice_timestamp" => 1546210800
                ]
            ]
        ];

        $client = new MockHttpClient([
            new MockResponse(
                json_encode($responseBody),
                ['http_code' => 200]
            )
        ]);

        self::assertEquals(
            \Symfony\Component\HttpFoundation\Response::HTTP_OK,
            $client->request('GET', $url)->getStatusCode()
        );

        $webServicesSynchro = new WebServicesSynchro($parameterBag, $apiService, $logger, $client);

        $webServicesSynchro->synchroApiGatewayExercices($siren, $nic);
    }

    public function testSynchroApiGatewayExercicesException(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $apiService = $this->createMock(EntrepriseAPIService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $siren = 'siren';
        $nic = 'nic';
        $urlExercice = 'api-gateway-url/exercices/';

        $url = $urlExercice . $siren . $nic;

        $url = $urlExercice;
        $siretRecipient = '123456789';

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['URL_API_ENTREPRISE_CHIFFRES_AFFAIRES'],
                ['API_ENTREPRISE_TOKEN']
            )
            ->willReturnOnConsecutiveCalls(
                $urlExercice,
                $siretRecipient
            )
        ;

        $client = new MockHttpClient([
            new MockResponse('', ['http_code' => 404])
        ]);

        $logger
            ->expects(self::once())
            ->method('warning')
        ;

        $webServicesSynchro = new WebServicesSynchro($parameterBag, $apiService, $logger, $client);

        $webServicesSynchro->synchroApiGatewayExercices($siren, $nic);
    }
}
