<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DataTransformer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Service\DataTransformer\ClausesTransformer;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClausesTransformerTest extends TestCase
{
    /**
     * @dataProvider objectUsingClauses
     */
    public function testGetNestedClauses(Consultation|Lot|ContratTitulaire $object): void
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);

        /*** Clause sociales ***/
        /******* N3 *******/
        // N3 INSERTION_ACTIVITE_ECONOMIQUE - N2 Condition Execution
        $referentielClauseN2ConditionExecutionN3_IAE = (new ClausesN3())
            ->setLabel('INSERTION_ACTIVITE_ECONOMIQUE')
            ->setSlug('insertionActiviteEconomique')
            ->setActif(true)
        ;
        $clauseN2ConditionExecutionN3_IAE = (new Consultation\ClausesN3())
            ->setReferentielClauseN3($referentielClauseN2ConditionExecutionN3_IAE)
        ;
        // N3 ACHATS_ETHIQUES_TRACABILITE_SOCIALE - N2 Condition Execution
        $referentielClauseN2ConditionExecutionN3_AETS = (new ClausesN3())
            ->setLabel('ACHATS_ETHIQUES_TRACABILITE_SOCIALE')
            ->setSlug('ACHATS_ETHIQUES_TRACABILITE_SOCIALE')
            ->setActif(true)
        ;
        $clauseN2ConditionExecutionN3_AETS = (new Consultation\ClausesN3())
            ->setReferentielClauseN3($referentielClauseN2ConditionExecutionN3_AETS)
        ;
        // N3 CLAUSE_SOCIALE_FORMATION_SCOLAIRE - N2 Condition Execution
        $referentielClauseN2ConditionExecutionN3_CSFS = (new ClausesN3())
            ->setLabel('CLAUSE_SOCIALE_FORMATION_SCOLAIRE')
            ->setSlug('CLAUSE_SOCIALE_FORMATION_SCOLAIRE')
            ->setActif(true)
        ;
        $clauseN2ConditionExecutionN3_CSFS = (new Consultation\ClausesN3())
            ->setReferentielClauseN3($referentielClauseN2ConditionExecutionN3_CSFS)
        ;
        // N3 LUTTE_CONTRE_DISCRIMINATIONS - N2 Marche Reserve
        $referentielClauseN2ConditionExecutionN3_LCD = (new ClausesN3())
            ->setLabel('LUTTE_CONTRE_DISCRIMINATIONS')
            ->setSlug('LUTTE_CONTRE_DISCRIMINATIONS')
            ->setActif(true)
        ;
        $clauseN2ConditionExecutionN3_LCD = (new Consultation\ClausesN3())
            ->setReferentielClauseN3($referentielClauseN2ConditionExecutionN3_LCD)
        ;

        /******* N2 *******/
        // N2 Condition Execution
        $referentielClauseN2ConditionExecution = (new ClausesN2())
            ->setLabel('conditionExecution')
            ->setSlug('conditionExecution')
            ->setActif(true)
        ;
        $clauseN2ConditionExecution = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2ConditionExecution)
            ->addClausesN3($clauseN2ConditionExecutionN3_IAE)
            ->addClausesN3($clauseN2ConditionExecutionN3_AETS)
            ->addClausesN3($clauseN2ConditionExecutionN3_CSFS)
        ;

        // N2 Marche Reserve
        $referentielClauseN2MarcheReserve = (new ClausesN2())
            ->setLabel('marcheReserve')
            ->setSlug('marcheReserve')
            ->setActif(true)
        ;
        $clauseN2MarcheReserve = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2MarcheReserve)
            ->addClausesN3($clauseN2ConditionExecutionN3_LCD)
        ;

        // N2 Marche insertion
        $referentielClauseN2MarcheInsertion = (new ClausesN2())
            ->setLabel('insertion')
            ->setSlug('insertion')
            ->setActif(true)
        ;
        $clauseN2MarcheInsertion = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2MarcheInsertion)
        ;

        /******* N1 *******/
        // N1 Clause Sociale
        $referentielClauseN1Soc = (new ClausesN1())
            ->setLabel('clausesSociales')
            ->setSlug('clausesSociales')
            ->setActif(true)
        ;
        $clauseN1Sociale = (new Consultation\ClausesN1())
            ->setReferentielClauseN1($referentielClauseN1Soc)
            ->addClausesN2($clauseN2ConditionExecution)
            ->addClausesN2($clauseN2MarcheReserve)
            ->addClausesN2($clauseN2MarcheInsertion)
        ;

        /*** Clause Environnementale ***/
        /******* N2 *******/
        // N2 Condition Execution
        $referentielClauseN2SpecificationsTechniques = (new ClausesN2())
            ->setLabel('specificationsTechniques')
            ->setSlug('specificationsTechniques')
            ->setActif(true)
        ;
        $specificationsTechniques = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2SpecificationsTechniques)
        ;

        // N2 Condition Execution
        $referentielClauseN2ConditionExecution = (new ClausesN2())
            ->setLabel('conditionExecution')
            ->setSlug('conditionExecution')
            ->setActif(true)
        ;
        $conditionExecution = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2ConditionExecution)
        ;

        /******* N1 *******/
        // N1 Clause Environnementale
        $referentielClauseN1Env = (new ClausesN1())
            ->setLabel('clauseEnvironnementale')
            ->setSlug('clauseEnvironnementale')
            ->setActif(true)
        ;
        $clauseN1Environnementale = (new Consultation\ClausesN1())
            ->setReferentielClauseN1($referentielClauseN1Env)
            ->addClausesN2($specificationsTechniques)
            ->addClausesN2($conditionExecution)
        ;

        $object
            ->addClausesN1($clauseN1Sociale)
            ->addClausesN1($clauseN1Environnementale)
        ;

        $translator
            ->expects(self::exactly(11))
            ->method('trans')
            ->withConsecutive(
                ['clausesSociales'],
                ['conditionExecution'],
                ['INSERTION_ACTIVITE_ECONOMIQUE'],
                ['ACHATS_ETHIQUES_TRACABILITE_SOCIALE'],
                ['CLAUSE_SOCIALE_FORMATION_SCOLAIRE'],
                ['marcheReserve'],
                ['LUTTE_CONTRE_DISCRIMINATIONS'],
                ['insertion'],
                ['clauseEnvironnementale'],
                ['specificationsTechniques'],
                ['conditionExecution'],
            )
            ->willReturnOnConsecutiveCalls(
                'Considération sociale',
                'condition Execution',
                'INSERTION_ACTIVITE_ECONOMIQUE',
                'ACHATS_ETHIQUES_TRACABILITE_SOCIALE',
                'CLAUSE_SOCIALE_FORMATION_SCOLAIRE',
                'marcheReserve',
                'LUTTE_CONTRE_DISCRIMINATIONS',
                'insertion',
                'Considération Environnementale',
                'specificationsTechniques',
                'conditionExecution',
            )
        ;

        $iriConverter
            ->expects(self::exactly(11))
            ->method('getIriFromItem')
            ->willReturnOnConsecutiveCalls(
                '/api/v2/clauses/clauses-n1/1',
                '/api/v2/clauses/clauses-n2/1',
                '/api/v2/clauses/clauses-n3/2',
                '/api/v2/clauses/clauses-n3/3',
                '/api/v2/clauses/clauses-n3/4',
                '/api/v2/clauses/clauses-n2/2',
                '/api/v2/clauses/clauses-n3/5',
                '/api/v2/clauses/clauses-n2/3',
                '/api/v2/clauses/clauses-n1/2',
                '/api/v2/clauses/clauses-n2/6',
                '/api/v2/clauses/clauses-n2/7',
            )
        ;

        $clausesTransformer = new ClausesTransformer($translator, $iriConverter);

        $expectedResult = [
            0 => [
                "slug" => "clausesSociales",
                "label" => "Considération sociale",
                "iri" => "/api/v2/clauses/clauses-n1/1",
                "active" => true,
                "clausesN2" => [
                    0 => [
                        "slug" => "conditionExecution",
                        "active" => true,
                        "label" => "condition Execution",
                        "iri" => "/api/v2/clauses/clauses-n2/1",
                        "clausesN3" => [
                            0 => [
                                "slug" => "insertionActiviteEconomique",
                                "active" => true,
                                "label" => "INSERTION_ACTIVITE_ECONOMIQUE",
                                "iri" => "/api/v2/clauses/clauses-n3/2",
                            ],
                            1 => [
                                "slug" => "ACHATS_ETHIQUES_TRACABILITE_SOCIALE",
                                "active" => true,
                                "label" => "ACHATS_ETHIQUES_TRACABILITE_SOCIALE",
                                "iri" => "/api/v2/clauses/clauses-n3/3",
                            ],
                            2 => [
                                "slug" => "CLAUSE_SOCIALE_FORMATION_SCOLAIRE",
                                "active" => true,
                                "label" => "CLAUSE_SOCIALE_FORMATION_SCOLAIRE",
                                "iri" => "/api/v2/clauses/clauses-n3/4",
                            ],
                        ]
                    ],
                    1 => [
                        "slug" => "marcheReserve",
                        "active" => true,
                        "label" => "marcheReserve",
                        "iri" => "/api/v2/clauses/clauses-n2/2",
                        "clausesN3" => [
                            0 => [
                                "slug" => "LUTTE_CONTRE_DISCRIMINATIONS",
                                "active" => true,
                                "label" => "LUTTE_CONTRE_DISCRIMINATIONS",
                                "iri" => "/api/v2/clauses/clauses-n3/5",
                            ]
                        ]
                    ],
                    2 => [
                        "slug" => "insertion",
                        "active" => true,
                        "label" => "insertion",
                        "iri" => "/api/v2/clauses/clauses-n2/3",
                    ],
                ]
            ],
            1 => [
                "slug" => "clauseEnvironnementale",
                "label" => "Considération Environnementale",
                "iri" => "/api/v2/clauses/clauses-n1/2",
                "active" => true,
                "clausesN2" => [
                    0 => [
                        "slug" => "specificationsTechniques",
                        "active" => true,
                        "label" => "specificationsTechniques",
                        "iri" => "/api/v2/clauses/clauses-n2/6",
                    ],
                    1 => [
                        "slug" => "conditionExecution",
                        "active" => true,
                        "label" => "conditionExecution",
                        "iri" => "/api/v2/clauses/clauses-n2/7",
                    ]
                ]
            ]
        ];

        self::assertSame($expectedResult, $clausesTransformer->getNestedClauses($object));
    }

    public function objectUsingClauses(): array
    {
        return [
            'Consultation' => [new Consultation()],
            'Lot' => [new Lot()],
            'Contrat Titulaire' => [new ContratTitulaire()],
        ];
    }
}
