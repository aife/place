<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DataTransformer;

use App\Entity\ContratTitulaire;
use App\Entity\GeolocalisationN2;
use App\Entity\TrancheArticle133;
use App\Entity\ValeurReferentiel;
use App\Repository\GeolocalisationN2Repository;
use App\Repository\InvitationConsultationTransverseRepository;
use App\Repository\TrancheArticle133Repository;
use App\Repository\ValeurReferentielRepository;
use App\Service\DataTransformer\ContratTransformer;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ContratTransformerTest extends TestCase
{
    public function testGetLieuExecutions(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $lieuExecution = ",,254,255,256,";

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setLieuExecution($lieuExecution);

        $geolocation = new class() extends GeolocalisationN2 {
            public function getId()
            {
                return 254;
            }
            public function getDenomination1()
            {
                return '(75) Paris';
            }
        };
        $geolocation2 = new class() extends GeolocalisationN2 {
            public function getId()
            {
                return 255;
            }
            public function getDenomination1()
            {
                return '(77) Seine-et-Marne';
            }
        };
        $geolocation3 = new class() extends GeolocalisationN2 {
            public function getId()
            {
                return 256;
            }
            public function getDenomination1()
            {
                return '(78) Yvelines';
            }
        };

        $geolocRepository = $this->createMock(GeolocalisationN2Repository::class);
        $geolocRepository
            ->expects(self::once())
            ->method('getDenominationLieuxExecution')
            ->with(['', '',254, 255, 256, ''])
            ->willReturn([
                $geolocation,
                $geolocation2,
                $geolocation3,
            ])
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($geolocRepository)
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        $expectedResult = '(75) Paris, (77) Seine-et-Marne, (78) Yvelines';

        self::assertSame($contratTransformer->getLieuExecutions($contratTitulaire), $expectedResult);
    }

    /**
     * @dataProvider lieuxExecution
     */
    public function testGetLieuExecutionsIsEmpty(?string $lieuExecution, string $expectedResult): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setLieuExecution($lieuExecution);

        $geolocRepository = $this->createMock(GeolocalisationN2Repository::class);
        $geolocRepository
            ->expects(self::never())
            ->method('getDenominationLieuxExecution')
        ;

        $entityManager
            ->expects(self::never())
            ->method('getRepository')
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getLieuExecutions($contratTitulaire), $expectedResult);
    }

    public function lieuxExecution(): array
    {
        return [
            'lieu Execution is empty string' => ['', ''],
            'lieu Execution is null' => [null, ''],
        ];
    }

    public function testGetTrancheBudgetaire(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $idTranche = 42;
        $libelleTranche = '90 000 HT à 124 999,99 HT';

        $trancheBudgetaire = new TrancheArticle133();
        $trancheBudgetaire->setLibelleTrancheBudgetaire($libelleTranche);

        $trancheBudgetaireRepo = $this->createMock(TrancheArticle133Repository::class);
        $trancheBudgetaireRepo
            ->expects(self::once())
            ->method('find')
            ->with($idTranche)
            ->willReturn($trancheBudgetaire)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($trancheBudgetaireRepo)
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getTrancheBudgetaire($idTranche), $libelleTranche);
    }

    public function testGetTrancheBudgetaireIsNull(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $idTranche = null;

        $trancheBudgetaireRepo = $this->createMock(TrancheArticle133Repository::class);
        $trancheBudgetaireRepo
            ->expects(self::never())
            ->method('find')
        ;

        $entityManager
            ->expects(self::never())
            ->method('getRepository')
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertNull($contratTransformer->getTrancheBudgetaire($idTranche));
    }

    public function testCcag(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $ccagApplicable = 42;
        $libelleReferentiel = 'Sélection des candidatures';

        $valeurReferentiel = new ValeurReferentiel();
        $valeurReferentiel->setLibelleValeurReferentiel($libelleReferentiel);

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('REFERENTIEL_CCAG_REFERENCE')
            ->willReturn('20')
        ;

        $valeurReferentielRepo = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => $ccagApplicable,
                    'idReferentiel' => '20'
                ]
            )
            ->willReturn($valeurReferentiel)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($valeurReferentielRepo)
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getCcag($ccagApplicable), $libelleReferentiel);
    }

    public function testCcagIsNull(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $ccagApplicable = null;

        $parameterBag
            ->expects(self::never())
            ->method('get')
            ->with('REFERENTIEL_CCAG_REFERENCE')
        ;

        $valeurReferentielRepo = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepo
            ->expects(self::never())
            ->method('findOneBy')
            ->with(
                [
                    'id' => $ccagApplicable,
                    'idReferentiel' => '20'
                ]
            )
        ;

        $entityManager
            ->expects(self::never())
            ->method('getRepository')
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertNull($contratTransformer->getCcag($ccagApplicable));
    }

    /**
     * @dataProvider codesCpv
     */
    public function testGetCpvWith(?string $cpv1, ?string $cpv2, array $expectedResult): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setCodeCpv1($cpv1);
        $contratTitulaire->setCodeCpv2($cpv2);

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getCpv($contratTitulaire), $expectedResult);
    }

    public function codesCpv(): array
    {
        return [
            'cpv1 + cpv2' => ['30200000', '#37000000#03100000#03400000#',
                [
                    'codePrincipal' => '30200000',
                    'codeSecondaire1' => '#37000000#03100000#03400000#'
                ]
            ],
            'cpv1' => ['30200000', null, ['codePrincipal' => '30200000']],
            'cpv1 et cpv2 diez' => ['30200000', '##',
                [
                    'codePrincipal' => '30200000',
                    'codeSecondaire1' => '##'
                ]
            ],
            'cpv1 empty + cpv2' => [null, '#37000000#03100000#03400000#', []],
            'null' => [null, null, []],
            'empty cpv1' => [null, '##', []]
        ];
    }

    public function testGetContratTransverse(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setId(42);

        $entiteEligible = [
            [
                "id" => 17,
                "acronyme" => "a4n",
                "denominationOrg" => "Ministères de l'Économie et des Finances, de l'Action et des Comptes publics"
            ],
            [
                "id" => 26,
                "acronyme" => "f2h",
                "denominationOrg" => "Etablissements et organismes de l'enseignement supérieur, de la recherche"
            ],
            [
                "id" => 28,
                "acronyme" => "g6l",
                "denominationOrg" => "Ministère de la numérotation automatique"
            ]

        ];

        $ictRepo = $this->createMock(InvitationConsultationTransverseRepository::class);
        $ictRepo
            ->expects(self::once())
            ->method('findIfContratExists')
            ->with(42)
            ->willReturn($entiteEligible)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($ictRepo)
        ;

        $expectedResult = [
            "value" => "true",
            "entiteEligible" =>  [
                [
                    "id" => 17,
                    "acronyme" => "a4n",
                    "denomination" => "Ministères de l'Économie et des Finances, de l'Action et des Comptes publics"
                ],
                [
                    "id" => 26,
                    "acronyme" => "f2h",
                    "denomination" => "Etablissements et organismes de l'enseignement supérieur, de la recherche"
                ],
                [
                    "id" => 28,
                    "acronyme" => "g6l",
                    "denomination" => "Ministère de la numérotation automatique"
                ],
            ]
        ];

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getContratTransverse($contratTitulaire), $expectedResult);
    }

    public function testGetContratTransverseConsultationTransverseNotFound(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setId(42);

        $ictRepo = $this->createMock(InvitationConsultationTransverseRepository::class);
        $ictRepo
            ->expects(self::once())
            ->method('findIfContratExists')
            ->with(42)
            ->willReturn([])
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($ictRepo)
        ;

        $expectedResult = [
            "value" => "false"
        ];

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getContratTransverse($contratTitulaire), $expectedResult);
    }

    /**
     * @dataProvider listNaturePrestation
     */
    public function testGetCategorieByLabel(string $labelCategorie, ?string $tradKey, int $result): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        if ($tradKey) {
            $parameterBag
                ->expects(self::once())
                ->method('get')
                ->with($tradKey)
                ->willReturn($result)
                ;
        }

        self::assertSame($result, $contratTransformer->getCategorieByLabel($labelCategorie));
    }

    public function listNaturePrestation(): array
    {
        return [
            'FOURNITURES' => ['FOURNITURES', 'TYPE_PRESTATION_FOURNITURES', 1],
            'TRAVAUX' => ['TRAVAUX', 'TYPE_PRESTATION_TRAVAUX', 2],
            'SERVICES' => ['SERVICES', 'TYPE_PRESTATION_SERVICES', 3],
            'WRONG_CATEGORIE' => ['WRONG_CATEGORIE', null, 0],
        ];
    }

    /**
     * @dataProvider listStatus
     */
    public function testGetStatutIdByLabel(string $labelCategorie, ?string $tradKey, string|int $result): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        if ($tradKey) {
            $parameterBag
                ->expects(self::once())
                ->method('get')
                ->with($tradKey)
                ->willReturn($result)
            ;
        }

        self::assertSame($result, $contratTransformer->getStatutIdByLabel($labelCategorie));
    }

    public function listStatus(): array
    {
        return [
            'STATUT_DECISION_CONTRAT' => ['STATUT_DECISION_CONTRAT', 'STATUT_DECISION_CONTRAT', '0'],
            'STATUT_DONNEES_CONTRAT_A_SAISIR' => [
                'STATUT_DONNEES_CONTRAT_A_SAISIR',
                'STATUT_DONNEES_CONTRAT_A_SAISIR',
                '1'
            ],
            'STATUT_NUMEROTATION_AUTONOME' => ['STATUT_NUMEROTATION_AUTONOME', 'STATUT_NUMEROTATION_AUTONOME', '2'],
            'STATUT_NOTIFICATION_CONTRAT' => ['STATUT_NOTIFICATION_CONTRAT', 'STATUT_NOTIFICATION_CONTRAT', '3'],
            'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE' => [
                'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE',
                'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE',
                '4'
            ],
            'WRONG_STATUT' => ['WRONG_STATUT', null, 0],
        ];
    }

    public function testGetStatutLabelByIdStatut(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        $parameterBag
            ->expects(self::exactly(5))
            ->method('get')
            ->withConsecutive(
                ['STATUT_DECISION_CONTRAT'],
                ['STATUT_DONNEES_CONTRAT_A_SAISIR'],
                ['STATUT_NUMEROTATION_AUTONOME'],
                ['STATUT_NOTIFICATION_CONTRAT'],
                ['STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'],
            )
            ->willReturnOnConsecutiveCalls(
                '0',
                '1',
                '2',
                '3',
                '4',
            )
        ;

        self::assertSame(
            'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE',
            $contratTransformer->getStatutLabelById('4')
        );
    }

    public function testGetStatutLabelByIdStatutEmpty(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        $parameterBag
            ->expects(self::exactly(5))
            ->method('get')
            ->withConsecutive(
                ['STATUT_DECISION_CONTRAT'],
                ['STATUT_DONNEES_CONTRAT_A_SAISIR'],
                ['STATUT_NUMEROTATION_AUTONOME'],
                ['STATUT_NOTIFICATION_CONTRAT'],
                ['STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'],
            )
            ->willReturnOnConsecutiveCalls(
                '0',
                '1',
                '2',
                '3',
                '4',
            )
        ;

        self::assertSame(
            '0',
            $contratTransformer->getStatutLabelById('45')
        );
    }

    public function testGetArrayLieuExecutionById(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $lieuExecution = ",,254,255,";

        $geolocation = new class() extends GeolocalisationN2 {
            public function getId()
            {
                return 254;
            }
            public function getDenomination2()
            {
                return 75;
            }
        };
        $geolocation2 = new class() extends GeolocalisationN2 {
            public function getId()
            {
                return 255;
            }
            public function getDenomination2()
            {
                return 77;
            }
        };

        $geolocRepository = $this->createMock(GeolocalisationN2Repository::class);
        $geolocRepository
            ->expects(self::once())
            ->method('findBy')
            ->willReturn([
                $geolocation,
                $geolocation2,
            ])
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($geolocRepository)
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        $expectedResult = [75, 77];

        self::assertSame($contratTransformer->getArrayLieuExecutionById($lieuExecution), $expectedResult);
    }

    public function testGetArrayLieuExecutionByIdIsEmpty(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $geolocRepository = $this->createMock(GeolocalisationN2Repository::class);
        $geolocRepository
            ->expects(self::never())
            ->method('findBy')
        ;

        $entityManager
            ->expects(self::never())
            ->method('getRepository')
        ;

        $contratTransformer = new ContratTransformer($entityManager, $parameterBag);

        self::assertSame($contratTransformer->getArrayLieuExecutionById(''), []);
        self::assertSame($contratTransformer->getArrayLieuExecutionById(null), []);
    }
}
