<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Offre;

use DateTime;
use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\Offre\AnnexeFinanciere;
use App\Entity\Organisme;
use App\Repository\BloborganismeFileRepository;
use App\Repository\EnveloppeFichierRepository;
use App\Repository\Offre\AnnexeFinanciereRepository;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoService;
use App\Service\CurrentUser;
use App\Service\DocGen\AnalyseFinanciere;
use App\Service\Offre\AnalyseFinanciereService;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\EspaceDocumentaire\Authorization;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Tests\Unit\Service\AgentTechniqueTokenTest;

class AnalyseFinanciereServiceTest extends TestCase
{
    /**
     * @var AnalyseFinanciereService
     */
    private $analyseFinanciereService;

    /**
     * @var ParameterBagInterface|MockObject
     */
    private $parameterBagInterface;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private $em;

    /**
     * @var Authorization|MockObject
     */
    private $perimetreAgent;

    /**
     * @var BlobOrganismeFileService|MockObject
     */
    private $blobOrganismeFileService;

    /**
     * @var AtexoFichierOrganisme|MockObject
     */
    private $atexoFichierOrganisme;

    /**
     * @var AtexoService|MockObject
     */
    private $atexoService;

    /**
     * @var MockObject|SessionInterface
     */
    private $session;

    public function setUp(): void
    {
        $this->parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->perimetreAgent = $this->createMock(Authorization::class);
        $this->blobOrganismeFileService = $this->createMock(BlobOrganismeFileService::class);
        $this->atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $this->atexoService = $this->createMock(AtexoService::class);
        $this->session = $this->createMock(SessionInterface::class);
        $this->agent = new Agent();
        $this->agent->setId(1);
        $organisme = new Organisme();
        $organisme->setAcronyme('acronyme');
        $this->agent->setOrganisme($organisme);
        $this->currentUser = $this->createPartialMock(CurrentUser::class, ['getCurrentUser']);
        $this->currentUser->method('getCurrentUser')
            ->willReturn($this->agent);
        $this->analyseFinanciereService = new AnalyseFinanciereService(
            $this->parameterBagInterface,
            $this->em,
            $this->perimetreAgent,
            $this->blobOrganismeFileService,
            $this->atexoFichierOrganisme,
            $this->atexoService,
            $this->session,
            $this->createMock(AnalyseFinanciere::class),
            $this->currentUser
        );
    }

    public function testGetAnnexeFinanciereReponse(): void
    {
        /** @var ContainerInterface|MockObject $containerMock */
        $containerMock = $this->createMock(ContainerInterface::class);

        $typeFichierAfi = 'AFI';
        $typePieceAfi = 5;
        $typeFichierAutre = 'Autre';
        $typePieceAutre = 4;
        $statutOffre = [2, 5];

        $enveloppeFichier1 = new EnveloppeFichier($containerMock);
        $enveloppeFichier1
            ->setTypeFichier($typeFichierAfi)
            ->setTypePiece($typePieceAfi)
        ;

        // We check that only AFI typed enveloppeFichier will be returned
        $enveloppeFichier2 = new EnveloppeFichier($containerMock);
        $enveloppeFichier2
            ->setTypeFichier($typeFichierAutre)
            ->setTypePiece($typePieceAutre)
        ;

        $enveloppeFichier3 = new EnveloppeFichier($containerMock);
        $enveloppeFichier3
            ->setTypeFichier($typeFichierAfi)
            ->setTypePiece($typePieceAfi)
        ;

        $enveloppe = new Enveloppe();
        $enveloppe
            ->addFichierEnveloppe($enveloppeFichier1)
            ->addFichierEnveloppe($enveloppeFichier2)
            ->addFichierEnveloppe($enveloppeFichier3)
        ;

        $offre = new Offre();
        $offre->addEnveloppe($enveloppe);

        $consultation = new class() extends Consultation {
            public function getId(): int
            {
                return 1;
            }
        };
        $consultation
            ->addOffre($offre)
            ->setAlloti(0)
        ;

        $this->parameterBagInterface
            ->expects($this->any())
            ->method('get')
            ->willReturn(2, 5)
        ;

        $enveloppeFichierRepository = $this->createMock(EnveloppeFichierRepository::class);
        $enveloppeFichierRepository
            ->expects($this->once())
            ->method('getEnveloppeAnnexeFinanciere')
            ->with($consultation, $statutOffre)
            ->willReturn([$enveloppeFichier1, $enveloppeFichier2])
        ;

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($enveloppeFichierRepository)
        ;

        $result = $this->analyseFinanciereService->getAnnexeFinanciereReponse($consultation);
        $this->assertCount(2, $result);
        $this->assertSame([$enveloppeFichier1, $enveloppeFichier2], $result);
    }

    public function testGetAnnexeFinanciereFile(): void
    {
        $agentId = 1;
        $consultationId = 11;
        $blobId = 111;
        $path = realpath(__DIR__ . '/../../data/test/fichier1.txt');

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAgentAuthorized')
            ->with($agentId)
            ->willReturn(true)
        ;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAuthorized')
            ->with($consultationId)
            ->willReturn(true)
        ;

        $this->blobOrganismeFileService
            ->expects($this->once())
            ->method('getFile')
            ->with($blobId, true)
            ->willReturn(['realPath' => $path, 'name' => 'fileName.docx'])
        ;

        $result = $this->analyseFinanciereService->getAnnexeFinanciereFile($agentId, $consultationId, $blobId);
        $this->assertSame(BinaryFileResponse::class, get_class($result));
    }

    public function testGetAnnexeFinanciereFileAgentAccessNotAllowed(): void
    {
        $agentId = 1;
        $consultationId = 11;
        $blobId = 111;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAgentAuthorized')
            ->with($agentId)
            ->willReturn(false)
        ;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAuthorized')
            ->with($consultationId)
            ->willReturn(true)
        ;

        $this->blobOrganismeFileService
            ->expects($this->never())
            ->method('getFile')
        ;

        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Impossible d\'accèder à cette resource !');

        $this->analyseFinanciereService->getAnnexeFinanciereFile($agentId, $consultationId, $blobId);
    }

    public function testGetAnnexeFinanciereFileAgentConsultationAccessNotAllowed(): void
    {
        $agentId = 1;
        $consultationId = 11;
        $blobId = 111;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAgentAuthorized')
            ->with($agentId)
            ->willReturn(true)
        ;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAuthorized')
            ->with($consultationId)
            ->willReturn(false)
        ;

        $this->blobOrganismeFileService
            ->expects($this->never())
            ->method('getFile')
        ;

        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Impossible d\'accèder à cette resource !');

        $this->analyseFinanciereService->getAnnexeFinanciereFile($agentId, $consultationId, $blobId);
    }

    public function testRemoveAnnexeFinanciereManuel(): void
    {
        $agentId = 1;
        $consultationId = 11;
        $annexeFinanciereId = 111;

        $blobOrganismeFile = new BloborganismeFile();
        $annexeFinanciere = new AnnexeFinanciere();
        $annexeFinanciere->setBlob($blobOrganismeFile);

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAgentAuthorized')
            ->with($agentId)
            ->willReturn(true)
        ;

        $this->perimetreAgent
            ->expects($this->once())
            ->method('isAuthorized')
            ->with($consultationId)
            ->willReturn(true)
        ;

        $annexeFinanciereRepository = $this->createMock(AnnexeFinanciereRepository::class);
        $annexeFinanciereRepository
            ->expects($this->once())
            ->method('find')
            ->with($annexeFinanciereId)
            ->willReturn($annexeFinanciere)
        ;

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($annexeFinanciereRepository)
        ;

        $this->em
            ->expects($this->exactly(2))
            ->method('remove')
            ->withConsecutive([$blobOrganismeFile], [$annexeFinanciere])
        ;

        $this->em
            ->expects($this->once())
            ->method('flush')
        ;

        $this->analyseFinanciereService->removeAnnexeFinanciereManuel($agentId, $consultationId, $annexeFinanciereId);
    }

    public function testAddAnnexeFinanciereManuel(): void
    {
        $annexeFinanciereId = 111;
        $agentId = 222;
        $filename = 'fichier1.txt';
        $path = realpath(__DIR__ . '/../../data/test/fichier1.txt');
        $acronyme = 'acronyme';
        $dateRemise = new DateTime();

        $upladedFile = new UploadedFile($path, $filename);
        $data = [
            'importFichier' => $upladedFile,
            'dateRemiseDate' => $dateRemise,
            'dateRemiseHeure' => $dateRemise,
            'entreprise' => 'entreprise name',
            'numeroLot' => 'Lot 1',
            'numeroPli' => 'Pli 1',
        ];

        $consultation = new Consultation();

        $organisme = new Organisme();
        $organisme->setAcronyme($acronyme);

        $blobOrganismeFile = new class() extends BloborganismeFile {
            public function getId()
            {
                return 12;
            }
        };

        $blobOrganismeFileRepository = $this->createMock(BloborganismeFileRepository::class);
        $blobOrganismeFileRepository
            ->expects($this->once())
            ->method('find')
            ->with($annexeFinanciereId)
            ->willReturn($blobOrganismeFile)
        ;

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($blobOrganismeFileRepository)
        ;

        $this->atexoFichierOrganisme
            ->expects($this->once())
            ->method('moveTmpFile')
            ->with($path)
        ;

        $this->atexoFichierOrganisme
            ->expects($this->once())
            ->method('insertFile')
            ->with($filename, $path, $acronyme)
            ->willReturn($annexeFinanciereId)
        ;

        $result = $this->analyseFinanciereService->add($data, $consultation);
        $this->assertSame(get_class($result), AnnexeFinanciere::class);
        $this->assertSame($result->getAgent(), $this->agent);
        $this->assertSame($result->getBlob(), $blobOrganismeFile);
        $this->assertSame($result->getConsultation(), $consultation);
        $this->assertSame($result->getDateRemise()->format('Y-m-d H:i'), $dateRemise->format('Y-m-d H:i'));
        $this->assertSame($result->getEntreprise(), 'entreprise name');
        $this->assertSame($result->getNumeroLot(), 'Lot 1');
        $this->assertSame($result->getNumeroPli(), 'Pli 1');
    }

    public function testAddAnnexeFinanciereNoData(): void
    {
        $annexeFinanciereId = 111;
        $filename = 'fichier1.txt';
        $path = realpath(__DIR__ . '/../../data/test/fichier1.txt');
        $acronyme = 'acronyme';
        $dateRemise = new DateTime();

        $upladedFile = new UploadedFile($path, $filename);
        $data = [
            'importFichier' => $upladedFile,
            'dateRemise' => $dateRemise,
            'entreprise' => null,
        ];

        $consultation = new Consultation();

        $blobOrganismeFile = new class() extends BloborganismeFile {
            public function getId()
            {
                return 12;
            }
        };

        $blobOrganismeFileRepository = $this->createMock(BloborganismeFileRepository::class);
        $blobOrganismeFileRepository
            ->expects($this->once())
            ->method('find')
            ->with($annexeFinanciereId)
            ->willReturn($blobOrganismeFile)
        ;

        $this->em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($blobOrganismeFileRepository)
        ;

        $this->atexoFichierOrganisme
            ->expects($this->once())
            ->method('moveTmpFile')
            ->with($path)
        ;

        $this->atexoFichierOrganisme
            ->expects($this->once())
            ->method('insertFile')
            ->with($filename, $path, $acronyme)
            ->willReturn($annexeFinanciereId)
        ;

        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Les données sont incorrect!');

        $this->analyseFinanciereService->add($data, $consultation);
    }
}
