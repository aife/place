<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Offre;

use App\Message\AskDumePublicationGroupementEntreprise;
use Exception;
use ReflectionClass;
use DateTime;
use App\Entity\CandidatureMps;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TMembreGroupementEntreprise;
use App\Listener\ResponseListener;
use App\Repository\EnveloppeFichierRepository;
use App\Repository\PanierEntrepriseRepository;
use App\Repository\TCandidatureRepository;
use App\Repository\TDumeNumeroRepository;
use App\Repository\TGroupementEntrepriseRepository;
use App\Repository\TListeLotsCandidatureRepository;
use App\Repository\TMembreGroupementEntrepriseRepository;
use App\Security\UidManagement;
use App\Service\AtexoChiffrementOffre;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoGroupement;
use App\Service\AtexoHorodatageOffre;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationSaveDepotService;
use App\Service\Consultation\ConsultationService;
use App\Service\Dume\DumeService;
use App\Service\InscritOperationsTracker;
use App\Service\Offre\ValidateDepotOffreService;
use App\Service\PlateformeVirtuelle\Context;
use App\Service\Referentiel;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

class ValidateDepotOffreServiceTest extends TestCase
{
    private const STATUT_ENV_OUVERTE = 5;
    private const STATUT_ENV_BROUILLON = 99;
    private const STATUT_ENV_FERMETURE_EN_ATTENTE = 12;
    private const UID_RESPONSE = '1920abg6';
    private const TYPE_CANDIDATURE_DUME = 'dume';
    private const TYPE_CANDIDATURE_DUME_ONLINE = 'online';
    private const ID_ROLE_JURIDIQUE_MANDATAIRE = 1;
    private const EXTENSION_ATTENTE_CHIFFREMENT = '.attente_chiffrement';
    private const EXTENSION_NON_CHIFFRE = '.non_chiffre';

    public function testErrorValidationWithWrongOffreStatus()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_OUVERTE);
        $consultation = new Consultation();
        $consultation->setId(20);
        $offre->setConsultation($consultation);

        $this->expectException(Exception::class);
        $this->expectExceptionCode(Response::HTTP_CONFLICT);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->once())
            ->method('get')
            ->willReturn(self::STATUT_ENV_BROUILLON);

        $validateDepotOffreService = $this->initializeValidateDepotOffre(['parameterBagInterface' => $parameter]);
        $validateDepotOffreService->validate($offre);
    }

    public function testUpdateOffre()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $offre->addEnveloppe($enveloppe);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $updateOffreMethod = $reflectionClass->getMethod('updateOffre');
        $updateOffreMethod ->setAccessible(true);

        $context = $this->createMock(Context::class);
        $context->expects($this->once())
            ->method('getCurrentPlateformeVirtuelle')
            ->willReturn(new PlateformeVirtuelle());

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get')
            ->willReturn(self::STATUT_ENV_FERMETURE_EN_ATTENTE);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'context' => $context
        ]);

        $offreResult = $updateOffreMethod->invokeArgs($validateDepotOffreService, [$offre]);

        $this->assertEquals((new DateTime())->format('Y-m-d'), $offreResult->getDateDepot()->format('Y-m-d'));
        $this->assertEquals(1, $offreResult->getEnvoiComplet());
        $this->assertEquals('test@gmail.com', $offreResult->getMailSignataire());
        $this->assertEquals(self::STATUT_ENV_FERMETURE_EN_ATTENTE, $offreResult->getStatutOffres());
    }

    public function testHandleEnveloppeEmpty()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $handleEnveloppeMethod = $reflectionClass->getMethod('handleEnveloppe');
        $handleEnveloppeMethod ->setAccessible(true);

        $loggerInterface = $this->createMock(LoggerInterface::class);
        $loggerInterface->expects($this->once())
            ->method('error');

        $validateDepotOffreService = $this->initializeValidateDepotOffre(['loggerInterface' => $loggerInterface,]);
        $handleEnveloppeMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testHandleEnveloppeWithoutFileEnveloppe()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro(1);
        $offre->addEnveloppe($enveloppe);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $handleEnveloppeMethod = $reflectionClass->getMethod('handleEnveloppe');
        $handleEnveloppeMethod ->setAccessible(true);

        $repoEnveloppeFichier = $this->createMock(EnveloppeFichierRepository::class);
        $repoEnveloppeFichier->expects($this->once())
            ->method('getNombreEnveloppeFichier')
            ->willReturn(0);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repoEnveloppeFichier);

        $loggerInterface = $this->createMock(LoggerInterface::class);
        $loggerInterface->expects($this->exactly(2))
            ->method('info');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'loggerInterface' => $loggerInterface
        ]);

        $handleEnveloppeMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testHandleEnveloppeWithFileEnveloppe()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro(1);
        $offre->addEnveloppe($enveloppe);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $handleEnveloppeMethod = $reflectionClass->getMethod('handleEnveloppe');
        $handleEnveloppeMethod ->setAccessible(true);

        $repoEnveloppeFichier = $this->createMock(EnveloppeFichierRepository::class);
        $repoEnveloppeFichier->expects($this->once())
            ->method('getNombreEnveloppeFichier')
            ->willReturn(1);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repoEnveloppeFichier);

        $loggerInterface = $this->createMock(LoggerInterface::class);
        $loggerInterface->expects($this->once())
            ->method('info');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'loggerInterface' => $loggerInterface
        ]);

        $handleEnveloppeMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testAddEnveloppeFictiveWithoutEnveloppeOffre()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $consultation->setEnvOffre(1);
        $consultation->setEnvCandidature(1);
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro(1);
        $enveloppe->setTypeEnv(0);
        $offre->addEnveloppe($enveloppe);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $addEnveloppeFictiveMethod = $reflectionClass->getMethod('addEnveloppeFictive');
        $addEnveloppeFictiveMethod ->setAccessible(true);

        $consultationService = $this->createMock(ConsultationService::class);
        $consultationService->expects($this->never())
            ->method('createEnveloppe');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'consultationService' => $consultationService
        ]);

        $addEnveloppeFictiveMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testAddEnveloppeFictiveWithEnveloppeOffre()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $consultation->setEnvOffre(1);
        $consultation->setEnvCandidature(2);
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro(1);
        $enveloppe->setTypeEnv(1);
        $offre->addEnveloppe($enveloppe);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $addEnveloppeFictiveMethod = $reflectionClass->getMethod('addEnveloppeFictive');
        $addEnveloppeFictiveMethod ->setAccessible(true);

        $consultationService = $this->createMock(ConsultationService::class);
        $consultationService->expects($this->exactly(2))
            ->method('createEnveloppe');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'consultationService' => $consultationService
        ]);

        $addEnveloppeFictiveMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testAddEnveloppeFictiveWithEnveloppeOffreAlloti()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setStatutOffres(self::STATUT_ENV_BROUILLON);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setChiffrementOffre('0');
        $consultation->setEnvOffre(1);
        $consultation->setEnvCandidature(2);
        $consultation->setAlloti('1');
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setEmail('test@gmail.com');
        $offre->setInscrit($inscrit);

        $enveloppe = new Enveloppe();
        $enveloppe->setIdEnveloppeElectro(1);
        $enveloppe->setTypeEnv(1);
        $offre->addEnveloppe($enveloppe);

        $listeLotsCandidature = new TListeLotsCandidature();
        $listeLotsCandidature->setNumLot(1);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $addEnveloppeFictiveMethod = $reflectionClass->getMethod('addEnveloppeFictive');
        $addEnveloppeFictiveMethod ->setAccessible(true);

        $consultationService = $this->createMock(ConsultationService::class);
        $consultationService->expects($this->exactly(2))
            ->method('createEnveloppe');

        $repoListeLotsCandidature = $this->createMock(TListeLotsCandidatureRepository::class);
        $repoListeLotsCandidature->expects($this->once())
            ->method('getListeLotByUserAndConsultationAndWithCandidature')
            ->willReturn([$listeLotsCandidature]);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repoListeLotsCandidature);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'consultationService' => $consultationService
        ]);

        $addEnveloppeFictiveMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testSaveCandidatureMps()
    {
        $offre = new Offre();
        $offre->setId(1);

        $consultation = new Consultation();
        $consultation->setId(20);
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $offre->setInscrit($inscrit);

        $candidatureMps = new CandidatureMps();

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $saveCandidatureMpsMethod = $reflectionClass->getMethod('saveCandidatureMps');
        $saveCandidatureMpsMethod ->setAccessible(true);

        $consultationDepotService = $this->createMock(ConsultationDepotService::class);
        $consultationDepotService->expects($this->once())
            ->method('getCandidatureMps')
            ->willReturn($candidatureMps);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'consultationDepotService' => $consultationDepotService
        ]);

        $saveCandidatureMpsMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testCloseCandidature()
    {
        $offre = new Offre();
        $offre->setId(1);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setAlloti('1');
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $inscrit->setEntrepriseId(1);
        $inscrit->setIdEtablissement(1);
        $offre->setInscrit($inscrit);

        $candidature = new TCandidature();

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $closeCandidatureMethod = $reflectionClass->getMethod('closeCandidature');
        $closeCandidatureMethod ->setAccessible(true);

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->exactly(4))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                self::STATUT_ENV_BROUILLON,
                self::STATUT_ENV_BROUILLON,
                self::STATUT_ENV_BROUILLON,
                self::STATUT_ENV_FERMETURE_EN_ATTENTE
            );

        $repoListeLotsCandidature = $this->createMock(TListeLotsCandidatureRepository::class);
        $repoListeLotsCandidature->expects($this->once())
            ->method('getListeLotByUserAndConsultationAndWithCandidature')
            ->willReturn([new TListeLotsCandidature()]);

        $repoCandidature = $this->createMock(TCandidatureRepository::class);
        $repoCandidature->expects($this->once())
            ->method('findOneBy')
            ->willReturn($candidature);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repoListeLotsCandidature, $repoCandidature);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'parameterBagInterface' => $parameterBagInterface
        ]);

        $candidatureResult = $closeCandidatureMethod->invokeArgs(
            $validateDepotOffreService,
            [$offre, self::UID_RESPONSE]
        );

        $this->assertInstanceOf(TCandidature::class, $candidatureResult);
        $this->assertInstanceOf(Offre::class, $candidatureResult->getIdOffre());
        $this->assertEquals($offre, $candidatureResult->getIdOffre());
        $this->assertEquals(self::STATUT_ENV_FERMETURE_EN_ATTENTE, $candidatureResult->getStatus());
    }

    public function testDumeCandidature()
    {
        $candidature = new TCandidature();

        $dumeContext = new TDumeContexte();
        $candidature->setIdDumeContexte($dumeContext);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $dumeCandidatureMethod = $reflectionClass->getMethod('dumeCandidature');
        $dumeCandidatureMethod ->setAccessible(true);

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get')
            ->willReturn(1);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface
        ]);

        $dumeCandidatureMethod->invokeArgs($validateDepotOffreService, [$candidature]);
    }

    public function testGroupementEntreprise()
    {
        $offre = new Offre();
        $offre->setId(1);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setAlloti('1');
        $offre->setConsultation($consultation);

        $candidature = new class () extends TCandidature {
            public function getId()
            {
                return 1;
            }
        };

        $candidature->setTypeCandidature('dume');
        $candidature->setTypeCandidatureDume('online');

        $tDumeContext = new class () extends TDumeContexte {
            public function getId()
            {
                return 1;
            }
        };
        $candidature->setIdDumeContexte($tDumeContext);

        $tGroupementEntreprise = new class () extends TGroupementEntreprise {
            public function getIdGroupementEntreprise()
            {
                return 1;
            }
        };

        $mandataire = new TMembreGroupementEntreprise();

        $numeroSnMandataire = new TDumeNumero();
        $numeroSnMandataire->setNumeroDumeNational('a7uo');

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $groupementEntrepriseMethod = $reflectionClass->getMethod('groupementEntreprise');
        $groupementEntrepriseMethod ->setAccessible(true);

        $atexoConfig = $this->createMock(AtexoConfiguration::class);
        $atexoConfig->expects($this->once())
            ->method('hasConfigPlateforme')
            ->willReturn(true);

        $atexoGroupement = $this->createMock(AtexoGroupement::class);
        $atexoGroupement->expects($this->once())
            ->method('saveGroupementEntreprise');

        $requestStack = $this->createMock(RequestStack::class);
        $requestStack->expects($this->once())
            ->method('getCurrentRequest')
            ->willReturn(new Request());

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->exactly(3))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                self::TYPE_CANDIDATURE_DUME,
                self::TYPE_CANDIDATURE_DUME_ONLINE,
                self::ID_ROLE_JURIDIQUE_MANDATAIRE
            );

        $dumeService = $this->createMock(DumeService::class);

        $repoTGroupementEntreprise = $this->createMock(TGroupementEntrepriseRepository::class);
        $repoTGroupementEntreprise->expects($this->once())
            ->method('findOneBy')
            ->willReturn($tGroupementEntreprise);

        $repoTMembreGroupementEntreprise = $this->createMock(TMembreGroupementEntrepriseRepository::class);
        $repoTMembreGroupementEntreprise->expects($this->once())
            ->method('findOneBy')
            ->willReturn($mandataire);

        $repoTDumeNumero = $this->createMock(TDumeNumeroRepository::class);
        $repoTDumeNumero->expects($this->once())
            ->method('findOneBy')
            ->willReturn($numeroSnMandataire);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(3))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repoTGroupementEntreprise,
                $repoTMembreGroupementEntreprise,
                $repoTDumeNumero
            );

        $message = new AskDumePublicationGroupementEntreprise('1');
        $envelope = new Envelope($message);

        $messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $messageBusInterface->expects($this->exactly(1))
            ->method('dispatch')->with($message)->willReturn($envelope);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'parameterBagInterface' => $parameterBagInterface,
            'atexoConfig' => $atexoConfig,
            'atexoGroupement' => $atexoGroupement,
            'requestStack' => $requestStack,
            'dumeService' => $dumeService,
            'messageBusInterface' => $messageBusInterface,
        ]);

        $groupementEntrepriseMethod->invokeArgs(
            $validateDepotOffreService,
            [$candidature, $offre, self::UID_RESPONSE]
        );
    }

    public function testSendEmailAgentInscrit()
    {
        $offre = new Offre();
        $offre->setId(1);

        $consultation = new Consultation();
        $consultation->setId(20);
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $offre->setInscrit($inscrit);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $sendEmailAgentInscritMethod = $reflectionClass->getMethod('sendEmailAgentInscrit');
        $sendEmailAgentInscritMethod ->setAccessible(true);

        $consultationSaveDepotService = $this->createMock(ConsultationSaveDepotService::class);
        $consultationSaveDepotService->expects($this->once())
            ->method('addJobEmailAR');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'consultationSaveDepotService' => $consultationSaveDepotService
        ]);

        $sendEmailAgentInscritMethod->invokeArgs($validateDepotOffreService, [$offre]);
    }

    public function testSyncDocEntrepriseInscrit()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setEntrepriseId(1);
        $offre->setIdEtablissement(1);

        $consultation = new Consultation();
        $offre->setConsultation($consultation);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $syncDocEntrepriseInscritMethod = $reflectionClass->getMethod('syncDocEntrepriseInscrit');
        $syncDocEntrepriseInscritMethod ->setAccessible(true);

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get')
            ->willReturn(true);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface
        ]);

        $syncDocEntrepriseInscritMethod->invokeArgs($validateDepotOffreService, [$offre]);
    }

    public function testAddConsultationPanier()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setEntrepriseId(1);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setOrganisme(new Organisme());
        $offre->setConsultation($consultation);

        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $offre->setInscrit($inscrit);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $addConsultationPanierMethod = $reflectionClass->getMethod('addConsultationPanier');
        $addConsultationPanierMethod ->setAccessible(true);

        $atexoConfig = $this->createMock(AtexoConfiguration::class);
        $atexoConfig->expects($this->once())
            ->method('hasConfigPlateforme')
            ->willReturn(true);

        $repoPanierEntreprise = $this->createMock(PanierEntrepriseRepository::class);
        $repoPanierEntreprise->expects($this->once())
            ->method('getPanierEntreprise')
            ->willReturn(null);
        $repoPanierEntreprise->expects($this->once())
            ->method('addConsultationToPanierEntreprise');

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($repoPanierEntreprise);


        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'em' => $em,
            'atexoConfig' => $atexoConfig
        ]);

        $addConsultationPanierMethod->invokeArgs($validateDepotOffreService, [$offre]);
    }

    public function testChangeExtensionFileChiffrement()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setOrganisme('test');

        $consultation = new Consultation();
        $consultation->setChiffrementOffre('1');
        $offre->setConsultation($consultation);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $changeExtensionFileMethod = $reflectionClass->getMethod('changeExtensionFile');
        $changeExtensionFileMethod ->setAccessible(true);

        $atexoEnveloppeFichier = $this->createMock(AtexoEnveloppeFichier::class);
        $atexoEnveloppeFichier->expects($this->once())
            ->method('changeExtensionFichiersOffre');

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get')
            ->willReturn(self::EXTENSION_ATTENTE_CHIFFREMENT);

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'atexoEnveloppeFichier' => $atexoEnveloppeFichier
        ]);

        $changeExtensionFileMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testChangeExtensionFileNonChiffre()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setOrganisme('test');

        $consultation = new Consultation();
        $consultation->setChiffrementOffre('0');
        $offre->setConsultation($consultation);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $changeExtensionFileMethod = $reflectionClass->getMethod('changeExtensionFile');
        $changeExtensionFileMethod ->setAccessible(true);

        $atexoEnveloppeFichier = $this->createMock(AtexoEnveloppeFichier::class);
        $atexoEnveloppeFichier->expects($this->once())
            ->method('changeExtensionFichiersOffre');

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->exactly(2))
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                self::EXTENSION_ATTENTE_CHIFFREMENT,
                self::EXTENSION_NON_CHIFFRE,
            );

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'atexoEnveloppeFichier' => $atexoEnveloppeFichier
        ]);

        $changeExtensionFileMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testChangeExtensionFileError()
    {
        $offre = new Offre();
        $offre->setId(1);
        $offre->setOrganisme('test');

        $consultation = new Consultation();
        $consultation->setChiffrementOffre('1');
        $offre->setConsultation($consultation);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $changeExtensionFileMethod = $reflectionClass->getMethod('changeExtensionFile');
        $changeExtensionFileMethod ->setAccessible(true);

        $atexoEnveloppeFichier = $this->createMock(AtexoEnveloppeFichier::class);
        $atexoEnveloppeFichier->expects($this->once())
            ->method('changeExtensionFichiersOffre')
            ->will($this->throwException(new Exception()));

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get')
            ->willReturn(self::EXTENSION_ATTENTE_CHIFFREMENT);

        $loggerInterface = $this->createMock(LoggerInterface::class);
        $loggerInterface->expects($this->once())
            ->method('error');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'atexoEnveloppeFichier' => $atexoEnveloppeFichier,
            'loggerInterface' => $loggerInterface
        ]);

        $changeExtensionFileMethod->invokeArgs($validateDepotOffreService, [$offre, self::UID_RESPONSE]);
    }

    public function testTrackingOperationsInscrit()
    {
        $offre = new Offre();
        $offre->setId(1);

        $consultation = new Consultation();
        $offre->setConsultation($consultation);

        $reflectionClass = new ReflectionClass(ValidateDepotOffreService::class);
        $trackingOperationsInscritMethod = $reflectionClass->getMethod('trackingOperationsInscrit');
        $trackingOperationsInscritMethod ->setAccessible(true);

        $atexoValeursReferentielles = $this->createMock(Referentiel::class);
        $atexoValeursReferentielles->expects($this->once())
            ->method('getIdValeurReferentiel')
            ->willReturn(10);

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get');

        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $inscritOperationTracker->expects($this->once())
            ->method('tracerOperationsInscrit');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'atexoValeursReferentielles' => $atexoValeursReferentielles,
            'inscritOperationTracker' => $inscritOperationTracker
        ]);

        $trackingOperationsInscritMethod->invokeArgs($validateDepotOffreService, [
            $offre,
            new DateTime(),
            new DateTime(),
            'string description',
            'string details',
            'string url',
            null,
            true
        ]);
    }

    public function testGetIdValeurReferentielle()
    {
        $atexoValeursReferentielles = $this->createMock(Referentiel::class);
        $atexoValeursReferentielles->expects($this->once())
            ->method('getIdValeurReferentiel')
            ->willReturn(10);

        $parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $parameterBagInterface->expects($this->once())
            ->method('get');

        $validateDepotOffreService = $this->initializeValidateDepotOffre([
            'parameterBagInterface' => $parameterBagInterface,
            'atexoValeursReferentielles' => $atexoValeursReferentielles
        ]);

        $validateDepotOffreService->getIdValeurReferentielle();
    }

    private function initializeValidateDepotOffre(array $mock = [])
    {
        $em = $mock['em'] ?? $this->createMock(EntityManagerInterface::class);
        $parameterBagInterface = $mock['parameterBagInterface'] ?? $this->createMock(ParameterBagInterface::class);
        $context = $mock['context'] ?? $this->createMock(Context::class);
        $loggerInterface = $mock['loggerInterface'] ?? $this->createMock(LoggerInterface::class);
        $requestStack = $mock['requestStack'] ?? $this->createMock(RequestStack::class);
        $consultationService = $mock['consultationService'] ?? $this->createMock(ConsultationService::class);
        $horodatage = $this->createMock(AtexoHorodatageOffre::class);
        $dumeService = $mock['dumeService'] ?? $this->createMock(DumeService::class);
        $responseListener = $this->createMock(ResponseListener::class);
        $consultationSaveDepotService = $mock['consultationSaveDepotService'] ??
            $this->createMock(ConsultationSaveDepotService::class);
        $atexoConfig = $mock['atexoConfig'] ?? $this->createMock(AtexoConfiguration::class);
        $inscritOperationTracker = $mock['inscritOperationTracker'] ??
            $this->createMock(InscritOperationsTracker::class);
        $atexoEnveloppeFichier = $mock['atexoEnveloppeFichier'] ?? $this->createMock(AtexoEnveloppeFichier::class);
        $atexoChiffrementOffre = $this->createMock(AtexoChiffrementOffre::class);
        $atexoGroupement = $mock['atexoGroupement'] ?? $this->createMock(AtexoGroupement::class);
        $consultationDepotService = $mock['consultationDepotService'] ??
            $this->createMock(ConsultationDepotService::class);
        $uidManagement = $this->createMock(UidManagement::class);
        $atexoValeursReferentielles = $mock['atexoValeursReferentielles'] ?? $this->createMock(Referentiel::class);
        $messageBusInterface = $mock['messageBusInterface'] ?? $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        return new ValidateDepotOffreService(
            $em,
            $parameterBagInterface,
            $context,
            $loggerInterface,
            $requestStack,
            $consultationService,
            $horodatage,
            $dumeService,
            $responseListener,
            $consultationSaveDepotService,
            $atexoConfig,
            $inscritOperationTracker,
            $atexoEnveloppeFichier,
            $atexoChiffrementOffre,
            $atexoGroupement,
            $consultationDepotService,
            $uidManagement,
            $atexoValeursReferentielles,
            $messageBusInterface
        );
    }
}
