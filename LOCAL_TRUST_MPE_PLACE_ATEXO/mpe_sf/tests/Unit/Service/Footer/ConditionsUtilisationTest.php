<?php

namespace Tests\Unit\Service\Footer;

use App\Service\Footer\ConditionsUtilisation;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConditionsUtilisationTest extends TestCase
{

    public function typeUserProvider()
    {
        return [
            'entreprise' => ['entreprise'],
            'agent' => ['agent'],
        ];
    }

    /**
     * @dataProvider typeUserProvider
     */
    public function testVisibiliteBlocCguSpecifiqueIsTrue($typeUser)
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator
            ->expects($this->exactly(2))
            ->method('trans')
            ->withConsecutive(
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE'],
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT']
            )
            ->willReturnOnConsecutiveCalls(
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE_TOTO',
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT_TOTO'
            );

        $conditionsUtilisation = new ConditionsUtilisation($translator);
        $result = $conditionsUtilisation->visibiliteBlocCguSpecifique($typeUser);
        $this->assertTrue($result);
    }

    /**
     * @dataProvider typeUserProvider
     */
    public function testVisibiliteBlocCguSpecifiqueIsFalse($typeUser)
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator
            ->expects($this->exactly(2))
            ->method('trans')
            ->withConsecutive(
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE'],
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT']
            )
            ->willReturnOnConsecutiveCalls(
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE',
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT'
            );

        $conditionsUtilisation = new ConditionsUtilisation($translator);

        $result = $conditionsUtilisation->visibiliteBlocCguSpecifique($typeUser);
        $this->assertFalse($result);
    }


    public function testGetLibelleBlocCguSpecifiqueEntreprise()
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator
            ->expects($this->any())
            ->method('trans')
            ->withConsecutive(
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE'],
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT'],
            )
            ->willReturnOnConsecutiveCalls(
                'Contenu conditions utilisations specifiques entreprise',
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT'
            );

        $conditionsUtilisation = new ConditionsUtilisation($translator);
        $result = $conditionsUtilisation->getLibelleBlocCguSpecifique('entreprise');
        $this->assertSame(
            'Contenu conditions utilisations specifiques entreprise',
            $result
        );
    }

    public function testGetLibelleBlocCguSpecifiqueAgent()
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator
            ->expects($this->any())
            ->method('trans')
            ->withConsecutive(
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE'],
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT'],
            )
            ->willReturnOnConsecutiveCalls(
                'Contenu conditions utilisations specifiques entreprise',
                'Contenu conditions utilisations specifiques agent'
            );

        $conditionsUtilisation = new ConditionsUtilisation($translator);
        $result = $conditionsUtilisation->getLibelleBlocCguSpecifique('agent');
        $this->assertSame(
            'Contenu conditions utilisations specifiques agent',
            $result
        );
    }

    public function testGetLibelleBlocCguSpecifiqueEmpty()
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator
            ->expects($this->any())
            ->method('trans')
            ->withConsecutive(
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE'],
                ['CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT'],
            )
            ->willReturnOnConsecutiveCalls(
                'Contenu conditions utilisations specifiques entreprise',
                'Contenu conditions utilisations specifiques agent'
            );

        $conditionsUtilisation = new ConditionsUtilisation($translator);
        $result = $conditionsUtilisation->getLibelleBlocCguSpecifique('nobody');
        $this->assertEmpty($result);
    }
}
