<?php

namespace Tests\Unit\Service\Footer;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Entity\Organisme;
use App\Service\ConfigurationOrganismeService;
use App\Service\Footer\PrerequisTechnique;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Security;

class PrerequisTechniqueTest extends TestCase
{
    public function testGetModeServerCrytoIsFalse(): void
    {
        $parameters = $this->createMock(ContainerBagInterface::class);
        $security = $this->createMock(Security::class);
        $confOrganismeService = $this->createMock(ConfigurationOrganismeService::class);

        $prerequisTechnique = new PrerequisTechnique($parameters, $security, $confOrganismeService);

        $organisme = new Organisme();
        $organisme->setAcronyme('brice-test');

        $user = new Agent();
        $user->setOrganisme($organisme);

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($user)
        ;

        $parameters
            ->expects(self::once())
            ->method('get')
            ->with('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            ->willReturn(false)
        ;

        $confOrganismeService
            ->expects(self::never())
            ->method('moduleIsEnabled')
        ;

        $result = $prerequisTechnique->getModeServeurCrypto('agent');
        self::assertFalse($result);
    }

    public function testGetModeServerCrytoIsTrue(): void
    {
        $parameters = $this->createMock(ContainerBagInterface::class);
        $security = $this->createMock(Security::class);
        $confOrganismeService = $this->createMock(ConfigurationOrganismeService::class);

        $prerequisTechnique = new PrerequisTechnique($parameters, $security, $confOrganismeService);

        $organisme = new Organisme();
        $organisme->setAcronyme('brice-test');

        $user = new Agent();
        $user->setOrganisme($organisme);

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($user);

        $parameters
            ->expects(self::once())
            ->method('get')
            ->with('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            ->willReturn(true)
        ;

        $confOrganismeService
            ->expects(self::once())
            ->method('moduleIsEnabled')
            ->with($user->getOrganisme(), 'ModeApplet')
            ->willReturn(false)
        ;

        $result = $prerequisTechnique->getModeServeurCrypto('agent');
        self::assertTrue($result);
    }

    public function testGetModeServerCrytoIsTrueFromEntreprise(): void
    {
        $parameters = $this->createMock(ContainerBagInterface::class);
        $security = $this->createMock(Security::class);
        $confOrganismeService = $this->createMock(ConfigurationOrganismeService::class);

        $prerequisTechnique = new PrerequisTechnique(
            $parameters,
            $security,
            $confOrganismeService
        );

        $user = new Inscrit();

        $parameters
            ->expects(self::once())
            ->method('get')
            ->with('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            ->willReturn(true)
        ;

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($user)
        ;

        $confOrganismeService
            ->expects(self::never())
            ->method('moduleIsEnabled')
        ;

        self::assertTrue($prerequisTechnique->getModeServeurCrypto('entreprise'));
    }
}
