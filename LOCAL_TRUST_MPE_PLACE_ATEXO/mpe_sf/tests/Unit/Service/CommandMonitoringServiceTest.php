<?php

namespace Tests\Unit\Service;

use App\Entity\CommandMonitoring;
use App\Repository\CommandMonitoringRepository;
use App\Service\CommandMonitoringService;
use App\Service\OutputMonitoredCommandService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class CommandMonitoringServiceTest extends TestCase
{
    public function testCreateCommandLog()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $this->createMock(OutputMonitoredCommandService::class),
            $this->createMock(CommandMonitoringRepository::class)
        );

        $response = $commandMonitoring->createCommandLog('my command name', ['param1' => 'test', 'param2' => 'test2']);

        $this->assertEquals('my command name', $response->getName());
        $this->assertEquals(CommandMonitoringService::IN_PROGRESS, $response->getStatus());
        $this->assertEquals('param1 => test || param2 => test2', $response->getParams());
    }

    public function testSetSuccess()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $this->createMock(OutputMonitoredCommandService::class),
            $this->createMock(CommandMonitoringRepository::class)
        );

        $command = new CommandMonitoring();

        $commandMonitoring->setSuccess($command);

        $this->assertEquals(CommandMonitoringService::SUCCESS, $command->getStatus());
    }

    public function testSetFailure()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $this->createMock(OutputMonitoredCommandService::class),
            $this->createMock(CommandMonitoringRepository::class)
        );

        $command = new CommandMonitoring();

        $commandMonitoring->setFailure($command, 'Erreur log');

        $this->assertEquals(CommandMonitoringService::FAILED, $command->getStatus());
        $this->assertEquals('Erreur log', $command->getLogs());
    }

    public function testGetOutputMonitoredCommand()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $outputMock = $this->createMock(OutputMonitoredCommandService::class);

        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $outputMock,
            $this->createMock(CommandMonitoringRepository::class)
        );

        $return = $commandMonitoring->getOutputMonitoredCommand();

        $this->assertEquals($outputMock, $return);
    }

    public function testIsCommandLaunchedWillReturnFalse()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $outputMock = $this->createMock(OutputMonitoredCommandService::class);
        $commandRepo = $this->createMock(CommandMonitoringRepository::class);

        $commandRepo->method('find')->willReturn(false);
        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $outputMock,
            $commandRepo
        );
        $isLaunched = $commandMonitoring->isCommandLaunched('command:name');

        $this->assertFalse($isLaunched);
    }

    public function testIsCommandLaunchedWillReturnTrue()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $outputMock = $this->createMock(OutputMonitoredCommandService::class);
        $commandRepo = $this->createMock(CommandMonitoringRepository::class);

        $commandRepo->method('find')->willReturn(true);
        $commandMonitoring = new CommandMonitoringService(
            $entityManager,
            $outputMock,
            $commandRepo
        );
        $isLaunched = $commandMonitoring->isCommandLaunched('command:name');

        $this->assertTrue($isLaunched);
    }
}
