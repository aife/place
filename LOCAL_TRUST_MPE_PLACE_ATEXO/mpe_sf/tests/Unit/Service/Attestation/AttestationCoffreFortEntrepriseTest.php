<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Service\Attestation;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\BlobFichier;
use App\Entity\Entreprise;
use App\Entity\Justificatifs;
use App\Repository\BlobFichierRepository;
use App\Repository\JustificatifsRepository;
use App\Service\Attestation\AttestationCoffreFortEntreprise;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class AttestationCoffreFortEntrepriseTest extends TestCase
{
    public function testGetAttestations(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $siren = '123456789';
        $entreprise = new Entreprise();
        $entreprise->setSiren($siren);
        $entreprise->setId(1);

        $logger
            ->expects(self::exactly(4))
            ->method('info')
            ->withConsecutive(
                ["Début recherche justificatifs coffre fort de l'entreprise ayant pour siren 123456789"],
                ["Le fichier a été trouvé pour le document justificatif coffre-fort id=22 et nom=Attestation_fiscal"],
                ["Le fichier a été trouvé pour le document justificatif coffre-fort id=33 et nom=Attestation_autre"],
                ["Fin recherche justificatifs coffre fort de l'entreprise ayant pour siren 123456789"],
            )
        ;

        $idBlob1 = 2;
        $idBlob2 = 3;

        $justificatif1 = new class () extends Justificatifs {
            public function getId(): ?int
            {
                return 22;
            }
        };
        $justificatif1
            ->setJustificatif($idBlob1)
            ->setIntituleJustificatif('Attestation_fiscal')
            ->setNom('justificatif1')
        ;

        $justificatif2 = new class () extends Justificatifs {
            public function getId(): ?int
            {
                return 33;
            }
        };
        $justificatif2
            ->setJustificatif($idBlob2)
            ->setIntituleJustificatif('Attestation_autre')
            ->setNom('justificatif2')
        ;

        $justificatifRepository = $this->createMock(JustificatifsRepository::class);
        $justificatifRepository
            ->expects(self::once())
            ->method('findBy')
            ->with(['idEntreprise' => 1])
            ->willReturn([$justificatif1, $justificatif2])
        ;

        $blobFile1 = new BlobFichier();
        $blobFile2 = new BlobFichier();

        $blobFileRepository = $this->createMock(BlobFichierRepository::class);
        $blobFileRepository
            ->expects(self::exactly(2))
            ->method('find')
            ->withConsecutive([$idBlob1], [$idBlob2])
            ->willReturnOnConsecutiveCalls(
                $blobFile1,
                $blobFile2
            )
        ;

        $iriBlobFile1 = '/api/v2/attestations-medias/2/download';
        $iriBlobFile2 = '/api/v2/attestations-medias/3/download';

        $iriConverter
            ->expects(self::exactly(2))
            ->method('getIriFromItem')
            ->withConsecutive([$blobFile1], [$blobFile2])
            ->willReturnOnConsecutiveCalls(
                $iriBlobFile1,
                $iriBlobFile2
            )
        ;

        $entityManager
            ->expects(self::exactly(3))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $justificatifRepository,
                $blobFileRepository,
                $blobFileRepository,
            );

        $expectedResult = [
            [
                'intituleJustificatif' => 'Attestation_fiscal',
                'nomJustificatif' => 'justificatif1',
                'file' => $iriBlobFile1
            ],
            [
                'intituleJustificatif' => 'Attestation_autre',
                'nomJustificatif' => 'justificatif2',
                'file' => $iriBlobFile2
            ],
        ];

        $attestationCoffreFortEntreprise = new AttestationCoffreFortEntreprise($entityManager, $iriConverter, $logger);

        self::assertSame($expectedResult, $attestationCoffreFortEntreprise->getAttestations($entreprise));
    }

    public function testGetAttestationsBlobNotFound(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $siren = '123456789';
        $entreprise = new Entreprise();
        $entreprise->setSiren($siren);
        $entreprise->setId(1);

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Début recherche justificatifs coffre fort de l'entreprise ayant pour siren 123456789"],
                ["Fin recherche justificatifs coffre fort de l'entreprise ayant pour siren 123456789"],
            )
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Le fichier n'a pas été trouvé pour le document justificatif coffre-fort id=22 et nom=Attestation_fiscal")
        ;

        $idBlob1 = 2;

        $justificatif1 = new class () extends Justificatifs {
            public function getId(): ?int
            {
                return 22;
            }
        };
        $justificatif1
            ->setJustificatif($idBlob1)
            ->setIntituleJustificatif('Attestation_fiscal')
            ->setNom('justificatif1')
        ;

        $justificatifRepository = $this->createMock(JustificatifsRepository::class);
        $justificatifRepository
            ->expects(self::once())
            ->method('findBy')
            ->with(['idEntreprise' => 1])
            ->willReturn([$justificatif1])
        ;

        $blobFileRepository = $this->createMock(BlobFichierRepository::class);
        $blobFileRepository
            ->expects(self::once())
            ->method('find')
            ->with($idBlob1)
            ->willReturn(null)
        ;

        $iriBlobFile1 = '';

        $iriConverter
            ->expects(self::never())
            ->method('getIriFromItem')
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $justificatifRepository,
                $blobFileRepository
            )
        ;

        $expectedResult = [
            [
                'intituleJustificatif' => 'Attestation_fiscal',
                'nomJustificatif' => 'justificatif1',
                'file' => $iriBlobFile1,
                'errorMessage' => "Le fichier n'a pas été trouvé pour le document justificatif coffre-fort id=22 et nom=Attestation_fiscal"
            ]
        ];

        $attestationCoffreFortEntreprise = new AttestationCoffreFortEntreprise($entityManager, $iriConverter, $logger);

        self::assertSame($expectedResult, $attestationCoffreFortEntreprise->getAttestations($entreprise));
    }
}
