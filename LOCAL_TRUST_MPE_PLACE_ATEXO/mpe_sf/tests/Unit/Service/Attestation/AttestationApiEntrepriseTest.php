<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Service\Attestation;

use ReflectionException;
use ReflectionClass;
use DateTime;
use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Agent;
use App\Entity\BlobFichier;
use App\Entity\DerniersAppelsValidesWsSgmapDocuments;
use App\Entity\DocumentType;
use App\Entity\Entreprise;
use App\Entity\EntrepriseDocument;
use App\Entity\EntrepriseDocumentVersion;
use App\Entity\Etablissement;
use App\Repository\BlobFichierRepository;
use App\Repository\DerniersAppelsValidesWsSgmapDocumentsRepository;
use App\Repository\DocumentTypeRepository;
use App\Repository\EntrepriseDocumentRepository;
use App\Repository\EntrepriseDocumentVersionRepository;
use App\Service\AtexoUtil;
use App\Service\Attestation\AttestationApiEntreprise;
use App\Service\Blob\BlobFileService;
use App\Service\Entreprise\EntrepriseAPIService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

class AttestationApiEntrepriseTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testGetFormatedDocumentName(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $getFormatedDocumentNameMethod = $reflectionClass->getMethod('getFormatedDocumentName');
        $getFormatedDocumentNameMethod->setAccessible(true);

        $documentTypeName = 'Attestation fiscal annee 2022';
        $siren = '123456789';

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $getFormatedDocumentNameMethod->invokeArgs(
            $attestationApiEntreprise,
            [$documentTypeName, $siren]
        );

        self::assertSame('Attestation_fiscal_annee_2022_123456789', $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateNewDocumentIfNotExist(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $createNewDocumentIfNotExist = $reflectionClass->getMethod('createNewDocumentIfNotExist');
        $createNewDocumentIfNotExist->setAccessible(true);

        $etab = new Etablissement();
        $etab->setIdEtablissement(2);

        $entreprise = new Entreprise();
        $entreprise->setId(1);
        $entreprise->setSiren('123456789');
        $entreprise->addEtablissement($etab);

        $documentType = new class () extends DocumentType {
            public function getIdDocumentType(): int
            {
                return 3;
            }
        };
        $documentType->setNomDocumentType('Attestation de vigilance URSSAF');

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Enregistrement nouveau document EntrepriseDocument"],
                ["Fin enregistrement nouveau document EntrepriseDocument"],
            )
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $createNewDocumentIfNotExist->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $documentType]
        );

        self::assertInstanceOf(EntrepriseDocument::class, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testCreateNewVersionDocument(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $createNewVersionDocument = $reflectionClass->getMethod('createNewVersionDocument');
        $createNewVersionDocument->setAccessible(true);

        $blobFile = new BlobFichier();
        $blobFile->setId(7);

        $documentEntreprise = new EntrepriseDocument();
        $url = "http://api-entreprise/document-urssaf-2022.pdf";
        $extensionFichierExterne = "pdf";
        $content = ['file' => $url];

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Enregistrement nouvelle version document pour blobfileId = 7"],
                ["Fin enregistrement nouvelle version document pour blobfileId = 7"],
            )
        ;

        $blobFileService
            ->expects(self::once())
            ->method('getFileSize')
            ->with($blobFile)
            ->willReturn('255 Ko')
        ;

        $atexoUtil
            ->expects(self::once())
            ->method('getExtension')
            ->with($url)
            ->willReturn($extensionFichierExterne)
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $createNewVersionDocument->invokeArgs(
            $attestationApiEntreprise,
            [$blobFile, $documentEntreprise, $url, $content]
        );

        self::assertInstanceOf(EntrepriseDocumentVersion::class, $result);
    }

    /**
     * @throws ReflectionException
     */
    public function testSaveDateDernierAppelValideWs(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $saveDateDernierAppelValideWs = $reflectionClass->getMethod('saveDateDernierAppelValideWs');
        $saveDateDernierAppelValideWs->setAccessible(true);

        $entreprise = new Entreprise();
        $entreprise->setId(1);
        $entreprise->setSiren('123456789');

        $documentType = new class () extends DocumentType {
            public function getIdDocumentType(): int
            {
                return 3;
            }
        };

        $agent = new Agent();
        $agent->setId(456);

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Enregistrement nouvelle date de dernier appel valide du WS"],
                ["Date dernier appel valide du WS enregistre pour l'entreprise : 1"],
            )
        ;

        $appelWsRepository = $this->createMock(DerniersAppelsValidesWsSgmapDocumentsRepository::class);
        $appelWsRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
            ->willReturn(null)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($appelWsRepository)
        ;

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($agent)
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $saveDateDernierAppelValideWs->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $documentType]
        );
    }

    /**
     * @throws ReflectionException
     */
    public function testIsTimeLimitToSynchronizeIsExceededFalse(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $isTimeLimitToSynchronizeIsExceeded = $reflectionClass->getMethod('isTimeLimitToSynchronizeIsExceeded');
        $isTimeLimitToSynchronizeIsExceeded->setAccessible(true);

        $entreprise = new Entreprise();
        $documentType = new DocumentType();

        $date = (new DateTime())->modify('- 1 days');

        $logger
            ->expects(self::exactly(3))
            ->method('info')
            ->withConsecutive(
                ["Debut verification date de dernier appel valide du WS"],
                ["Date de dernier appel valide du WS : " . $date->format('Y-m-d H:i:s')],
                ["Le dernier appel valide du WS date de moins de 0 jours donc pas de synchro"],
            )
        ;

        $dernierAppelValideWs = new DerniersAppelsValidesWsSgmapDocuments();
        $dernierAppelValideWs->setDateDernierAppelValide($date);

        $appelWsRepository = $this->createMock(DerniersAppelsValidesWsSgmapDocumentsRepository::class);
        $appelWsRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
            ->willReturn($dernierAppelValideWs)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($appelWsRepository)
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION')
            ->willReturn('- 1 days')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $isTimeLimitToSynchronizeIsExceeded->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $documentType]
        );

        self::assertFalse($result);
    }

    /**
     * @throws ReflectionException
     * @dataProvider getDateOfSynchoApiGouvEntreprise
     */
    public function testIsTimeLimitToSynchronizeIsExceededTrue(DateTime $date): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $isTimeLimitToSynchronizeIsExceeded = $reflectionClass->getMethod('isTimeLimitToSynchronizeIsExceeded');
        $isTimeLimitToSynchronizeIsExceeded->setAccessible(true);

        $entreprise = new Entreprise();
        $documentType = new DocumentType();

        $logger
            ->expects(self::exactly(4))
            ->method('info')
            ->withConsecutive(
                ["Debut verification date de dernier appel valide du WS"],
                ["Date de dernier appel valide du WS : " . $date->format('Y-m-d H:i:s')],
                ["Le dernier appel valide dépasse le paramétre de délai de synchro donc lancement ApiGouvEntreprise"],
                ["Fin verification date de dernier appel valide du WS"],
            )
        ;

        $dernierAppelValideWs = new DerniersAppelsValidesWsSgmapDocuments();
        $dernierAppelValideWs->setDateDernierAppelValide($date);

        $appelWsRepository = $this->createMock(DerniersAppelsValidesWsSgmapDocumentsRepository::class);
        $appelWsRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
            ->willReturn($dernierAppelValideWs)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($appelWsRepository)
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION')
            ->willReturn('- 1 days')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $isTimeLimitToSynchronizeIsExceeded->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $documentType]
        );

        self::assertTrue($result);
    }

    public function getDateOfSynchoApiGouvEntreprise(): array
    {
        return [
            'date_moins_3_jours' => [(new DateTime())->modify('- 3 days')],
            'date_moins_30_jours' => [(new DateTime())->modify('- 30 days')],
            'date_moins_2_années' => [(new DateTime())->modify('- 2 years')],
            'date_plus_30_jours' => [(new DateTime())->modify('+ 30 days')],
        ];
    }

    /**
     * @throws ReflectionException
     * @dataProvider getTypeDocuments*/
    public function testGetDocumentByDocumentType(
        int $typeDocument,
        string $documentTypeCode,
        string $documentTypeNom,
        string $url,
    ): void {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $getDocumentByDocumentType = $reflectionClass->getMethod('getDocumentByDocumentType');
        $getDocumentByDocumentType->setAccessible(true);

        $wsReturnedContent['data']['document_url'] = $url;

        $date = (new DateTime())->modify('- 30 days');

        $entreprise = new Entreprise();
        $entreprise->setId(42);
        $entreprise->setSiren(123456789);

        $documentType = new class () extends DocumentType {
            public function getIdDocumentType(): int
            {
                return 1;
            }
        };
        $documentType
            ->setNomDocumentType($documentTypeNom)
            ->setCode($documentTypeCode)
        ;

        $logger
            ->expects(self::exactly(5))
            ->method('info')
            ->withConsecutive(
                [
                    sprintf(
                        "Debut récupération document pour l'entreprise 123456789 ayant pour type de document : %s",
                        $typeDocument
                    )
                ],
                ["Debut verification date de dernier appel valide du WS"],
                ["Date de dernier appel valide du WS : " . $date->format('Y-m-d H:i:s')],
                ["Le dernier appel valide dépasse le paramétre de délai de synchro donc lancement ApiGouvEntreprise"],
                ["Fin verification date de dernier appel valide du WS"],
            )
        ;

        $entrepriseDocument = new class () extends EntrepriseDocument {
            public function getIdDocument()
            {
                return 5;
            }
        };

        $entrepriseDocumentRepository = $this->createMock(EntrepriseDocumentRepository::class);
        $entrepriseDocumentRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise->getId(),
                    'idTypeDocument' => $typeDocument
                ]
            )
            ->willReturn($entrepriseDocument)
        ;

        $documentTypeRepository = $this->createMock(DocumentTypeRepository::class);
        $documentTypeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                ['idDocumentType' => $typeDocument]
            )
            ->willReturn($documentType)
        ;

        $documentEntrepriseVersion = new class () extends EntrepriseDocumentVersion {
            public function getIdVersionDocument(): int
            {
                return 5;
            }

            public function getIdBlob(): int
            {
                return 24;
            }
        };

        $entrepriseDocVersionRepository = $this->createMock(EntrepriseDocumentVersionRepository::class);
        $entrepriseDocVersionRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                ['idDocument' => $entrepriseDocument->getIdDocument()],
                ['idVersionDocument' => 'desc']
            )
            ->willReturn($documentEntrepriseVersion)
        ;

        $dernierAppelValideWs = new DerniersAppelsValidesWsSgmapDocuments();
        $dernierAppelValideWs->setDateDernierAppelValide($date);

        $appelWsRepository = $this->createMock(DerniersAppelsValidesWsSgmapDocumentsRepository::class);
        $appelWsRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
            ->willReturn($dernierAppelValideWs)
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION')
            ->willReturn('- 1 days')
        ;

        $entrepriseAPIService
            ->expects(self::once())
            ->method('getAttestationByDocumentType')
            ->with($documentType, $entreprise->getSiren())
            ->willReturn($wsReturnedContent)
        ;

        $blobFileService
            ->expects(self::once())
            ->method('uploadExternalFileToBlobFile')
            ->with(
                $documentType,
                $url,
                $wsReturnedContent,
                $documentType->getNomDocumentType()
            )
            ->willReturn(null)
        ;

        $blobFile = new BlobFichier();

        $blobFichierRepository = $this->createMock(BlobFichierRepository::class);
        $blobFichierRepository
            ->expects(self::once())
            ->method('find')
            ->with($documentEntrepriseVersion->getIdBlob())
            ->willReturn($blobFile)
        ;

        $iriConverter
            ->expects(self::once())
            ->method('getIriFromItem')
            ->with($blobFile)
            ->willReturn('/api/v2/attestations-medias/24/download')
        ;

        $entityManager
            ->expects(self::exactly(5))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $entrepriseDocumentRepository,
                $documentTypeRepository,
                $entrepriseDocVersionRepository,
                $appelWsRepository,
                $blobFichierRepository,
            )
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $getDocumentByDocumentType->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $typeDocument]
        );

        $expectedResult = [
            'labelTypeDocument' => $documentTypeNom,
            'codeTypeDocument' => $documentTypeCode,
            'file' => '/api/v2/attestations-medias/24/download',
        ];

        self::assertSame($expectedResult, $result);
    }

    public function getTypeDocuments(): array
    {
        return [
            'attestation_fiscale' => [
                1,
                'attestation_fiscale',
                'Attestation de régularité fiscale',
                'url/fiscal/api/entreprise'
            ],
            'attestation_sociale_vg' => [
                3,
                'attestation_sociale_vg',
                'Attestation de vigilance URSSAF',
                'url/vigilance/api/entreprise'
            ],
        ];
    }

    /**
     * @throws ReflectionException
     * @dataProvider getTypeDocuments*/
    public function testGetDocumentByDocumentTypeReturnError(
        int $typeDocument,
        string $documentTypeCode,
        string $documentTypeNom
    ): void {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entrepriseAPIService = $this->createMock(EntrepriseAPIService::class);
        $security = $this->createMock(Security::class);
        $blobFileService = $this->createMock(BlobFileService::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);

        $reflectionClass = new ReflectionClass(AttestationApiEntreprise::class);
        $getDocumentByDocumentType = $reflectionClass->getMethod('getDocumentByDocumentType');
        $getDocumentByDocumentType->setAccessible(true);

        $wsReturnedContent = [
            'errors' => ["Siren invalide, ACOSS ne peut délivrer une attestation"]
        ];

        $date = (new DateTime())->modify('- 30 days');

        $entreprise = new Entreprise();
        $entreprise->setId(42);
        $entreprise->setSiren(123456789);

        $documentType = new class () extends DocumentType {
            public function getIdDocumentType(): int
            {
                return 1;
            }
        };
        $documentType
            ->setNomDocumentType($documentTypeNom)
            ->setCode($documentTypeCode)
        ;

        $logger
            ->expects(self::exactly(5))
            ->method('info')
            ->withConsecutive(
                [
                    sprintf(
                        "Debut récupération document pour l'entreprise 123456789 ayant pour type de document : %s",
                        $typeDocument
                    )
                ],
                ["Debut verification date de dernier appel valide du WS"],
                ["Date de dernier appel valide du WS : " . $date->format('Y-m-d H:i:s')],
                ["Le dernier appel valide dépasse le paramétre de délai de synchro donc lancement ApiGouvEntreprise"],
                ["Fin verification date de dernier appel valide du WS"],
            )
        ;

        $entrepriseDocument = new class () extends EntrepriseDocument {
            public function getIdDocument()
            {
                return 5;
            }
        };

        $entrepriseDocumentRepository = $this->createMock(EntrepriseDocumentRepository::class);
        $entrepriseDocumentRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise->getId(),
                    'idTypeDocument' => $typeDocument
                ]
            )
            ->willReturn($entrepriseDocument)
        ;

        $documentTypeRepository = $this->createMock(DocumentTypeRepository::class);
        $documentTypeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                ['idDocumentType' => $typeDocument]
            )
            ->willReturn($documentType)
        ;

        $documentEntrepriseVersion = new class () extends EntrepriseDocumentVersion {
            public function getIdVersionDocument(): int
            {
                return 5;
            }

            public function getIdBlob(): int
            {
                return 24;
            }
        };

        $entrepriseDocVersionRepository = $this->createMock(EntrepriseDocumentVersionRepository::class);
        $entrepriseDocVersionRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                ['idDocument' => $entrepriseDocument->getIdDocument()],
                ['idVersionDocument' => 'desc']
            )
            ->willReturn($documentEntrepriseVersion)
        ;

        $dernierAppelValideWs = new DerniersAppelsValidesWsSgmapDocuments();
        $dernierAppelValideWs->setDateDernierAppelValide($date);

        $appelWsRepository = $this->createMock(DerniersAppelsValidesWsSgmapDocumentsRepository::class);
        $appelWsRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
            ->willReturn($dernierAppelValideWs)
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION')
            ->willReturn('- 1 days')
        ;

        $entrepriseAPIService
            ->expects(self::once())
            ->method('getAttestationByDocumentType')
            ->with($documentType, $entreprise->getSiren())
            ->willReturn($wsReturnedContent)
        ;

        $blobFileService
            ->expects(self::never())
            ->method('uploadExternalFileToBlobFile')
        ;

        $blobFichierRepository = $this->createMock(BlobFichierRepository::class);
        $blobFichierRepository
            ->expects(self::never())
            ->method('find')
        ;

        $iriConverter
            ->expects(self::never())
            ->method('getIriFromItem')
        ;

        $entityManager
            ->expects(self::exactly(4))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $entrepriseDocumentRepository,
                $documentTypeRepository,
                $entrepriseDocVersionRepository,
                $appelWsRepository
            )
        ;

        $entityManager
            ->expects(self::never())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::never())
            ->method('flush')
        ;

        $attestationApiEntreprise = new AttestationApiEntreprise(
            $entityManager,
            $iriConverter,
            $parameterBag,
            $logger,
            $entrepriseAPIService,
            $security,
            $blobFileService,
            $atexoUtil
        );

        $result = $getDocumentByDocumentType->invokeArgs(
            $attestationApiEntreprise,
            [$entreprise, $typeDocument]
        );

        $expectedResult = [
            'labelTypeDocument' => $documentTypeNom,
            'codeTypeDocument' => $documentTypeCode,
            'errorMessage' => ['Siren invalide, ACOSS ne peut délivrer une attestation']
        ];

        self::assertSame($expectedResult, $result);
    }
}
