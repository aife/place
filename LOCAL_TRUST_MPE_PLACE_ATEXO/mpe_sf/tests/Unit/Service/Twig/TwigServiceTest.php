<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\Twig;

use Twig\Error\RuntimeError;
use App\Service\Twig\TwigService;
use PHPUnit\Framework\TestCase;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class TwigServiceTest.
 */
class TwigServiceTest extends TestCase
{
    /**
     * @throws RuntimeError
     */
    public function testTwigEscapeFilter()
    {
        $loader = new FilesystemLoader('./templates');
        $twig = new Environment($loader);
        $service = new TwigService($twig);
        $str = 'Traduction accentuée avec "guillemets et retour chariot" &amp; &
        Texte à la ligne!';
        $str = "$str";
        $expected = 'Traduction\u0020accentu\u00E9e\u0020avec\u0020\u0022guillemets\u0020et\u0020retour\u0020chariot\u0022\u0020\u0026amp\u003B\u0020\u0026\n\u0020\u0020\u0020\u0020\u0020\u0020\u0020\u0020Texte\u0020\u00E0\u0020la\u0020ligne\u0021';
        $actual = $service->twigEscapeFilter($str, 'js');
        $this->assertEquals($expected, $actual);
    }
}
