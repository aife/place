<?php

namespace Tests\Unit\Service;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Organisme;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\AtexoConfiguration;
use App\Service\CurrentUser;
use App\Utils\Utils;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Cache\CacheInterface;

class AtexoConfigurationTest extends TestCase
{
    /** @var EntityManagerInterface|MockObject */
    private $em;

    private ParameterBagInterface|MockObject $parameterBag;

    /** @var AtexoConfiguration */
    private $configuration;

    private $cache;

    /** @var CurrentUser $currentUser */
    private $currentUser;

    /** @var Utils $utils */
    private $utils;

    private function initMocks()
    {
        $this->em = $this->createMock(EntityManagerInterface::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->cache = $this->createMock(CacheInterface::class);
        $this->currentUser = $this->createMock(CurrentUser::class);
        $this->utils = $this->createMock(Utils::class);
    }

    private function initServices()
    {
        $this->configuration = new AtexoConfiguration(
            $this->parameterBag,
            $this->em,
            $this->cache,
            $this->currentUser,
            $this->utils
        );
    }

    public function testEnableModule()
    {
        $this->initMocks();
        $this->em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (ConfigurationPlateforme::class == $repository) {
                        $repository = $this->createMock(ConfigurationPlateformeRepository::class);
                        $repository->expects($this->any())
                            ->method('getConfigurationPlateforme')
                            ->willReturn(new ConfigurationPlateforme());
                    } elseif (ConfigurationOrganisme::class == $repository) {
                        $configurationOrganisme = new ConfigurationOrganisme();
                        $configurationOrganisme->setDonneesRedac('1');
                        $repository = $this->createMock(ConfigurationOrganismeRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn($configurationOrganisme);
                    }

                    return $repository;
                }
            );
        $this->initServices();
        $right = $this->configuration->isModuleEnabled('DonneesRedac', new Organisme(), 'get');
        $this->assertTrue($right);
    }

    public function testNotFoundModule()
    {
        $this->initMocks();
        $this->em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (ConfigurationPlateforme::class == $repository) {
                        $repository = $this->createMock(ConfigurationPlateformeRepository::class);
                        $repository->expects($this->any())
                            ->method('getConfigurationPlateforme')
                            ->willReturn(new ConfigurationPlateforme());
                    } elseif (ConfigurationOrganisme::class == $repository) {
                        $repository = $this->createMock(ConfigurationOrganismeRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(new ConfigurationOrganisme());
                    }

                    return $repository;
                }
            );
        $this->initServices();
        $this->expectException(DataNotFoundException::class);
        $this->configuration->isModuleEnabled('badModule', new Organisme(), 'get');
    }
}
