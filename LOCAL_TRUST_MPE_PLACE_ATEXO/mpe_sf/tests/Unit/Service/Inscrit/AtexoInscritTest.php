<?php

namespace Tests\Unit\Service\Inscrit;

use Exception;
use App\Entity\Inscrit;
use App\Service\AtexoInscrit;
use PHPUnit\Framework\TestCase;

class AtexoInscritTest extends TestCase
{
    /**
     * @dataProvider inscritProvider
     */
    public function testValidateInfoInscrit($arrayInscrit, $arrayErrors)
    {
        $atexoInscrit = $this->createMock(AtexoInscrit::class);
        $atexoInscrit->expects($this->any())
            ->method('validateInfo')
            ->with($arrayInscrit)
            ->willReturn($arrayErrors);

        $this->assertEquals($arrayErrors, $atexoInscrit->validateInfo($arrayInscrit));
    }

    public function inscritProvider()
    {
        return [
            // Inscrit Valide
            [[
                'id' => '0',
                'idEtablissement' => '31311',
                'login' => 'test-atexo1',
                'motDePasse' => 'e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b',
                'email' => 'test-atexo1@atexo.com',
                'nom' => 'nom',
                'prenom' => 'prenom',
                'adresse' => [
                        'rue' => 'rue des petits champs',
                        'codePostal' => '75000',
                        'ville' => 'Paris',
                        'pays' => 'France',
                    ],
                'telephone' => '0123456789',
                'actif' => '1',
                'siret' => '38391710100058',
                'inscritAnnuaireDefense' => '1', ],
                [], ],
            // Inscrit avec Siret non valide, email non valide
            [[
                'id' => '0',
                'idEtablissement' => '31311',
                'login' => 'test-atexo1',
                'motDePasse' => 'e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b',
                'email' => 'email-non-valide',
                'nom' => 'nom',
                'prenom' => 'prenom',
                'adresse' => [
                        'rue' => 'rue des petits champs',
                        'codePostal' => '75000',
                        'ville' => 'Paris',
                        'pays' => 'France',
                    ],
                'telephone' => '0123456789',
                'actif' => '1',
                'siret' => '38391710100058000000',
                'inscritAnnuaireDefense' => '1', ], ['Siret non valide', 'Email non valide']],
        ];
    }

    /**
     * Test SerliazeInscrit.
     */
    public function testSerializeInscrit()
    {
        $arrayInscrit = [
            'id' => '0',
            'idEtablissement' => '31311',
            'login' => 'test-atexo1',
            'motDePasse' => 'e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b',
            'email' => 'test-atexo1@atexo.com',
            'nom' => 'nom',
            'prenom' => 'prenom',
            'adresse' => [
                    'rue' => 'rue des petits champs',
                    'codePostal' => '75000',
                    'ville' => 'Paris',
                    'pays' => 'France',
                ],
            'telephone' => '0123456789',
            'actif' => '1',
            'siret' => '38391710100058',
            'inscritAnnuaireDefense' => '1', ];

        $inscrit = new Inscrit();
        $inscrit->setIdEtablissement(31311);
        $inscrit->setLogin('test-atexo1');
        $inscrit->setMdp('e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b');
        $inscrit->setEmail('test-atexo1@atexo.com');
        $inscrit->setNom('nom');
        $inscrit->setPrenom('prenom');
        $inscrit->setTelephone('0123456789');
        $inscrit->setBloque(0);
        $inscrit->setSiret(38391710100058);
        $inscrit->setInscritAnnuaireDefense('1');
        $inscrit->setCodepostal('75000');
        $inscrit->setVille('Paris');
        $inscrit->setPays('France');
        $inscrit->setAdresse('rue des petits champs');

        $atexoInscrit = $this->createMock(AtexoInscrit::class);
        $atexoInscrit->expects($this->any())
            ->method('serializeInscrit')
            ->with($arrayInscrit)
            ->willReturn($inscrit);

        $expectedInscrit = $atexoInscrit->serializeInscrit($arrayInscrit);
        $this->assertInstanceOf(Inscrit::class, $expectedInscrit);
        $this->assertEquals($expectedInscrit->getLogin(), $inscrit->getLogin());
        $this->assertEquals($expectedInscrit->getMdp(), $inscrit->getMdp());
        $this->assertEquals($expectedInscrit->getEmail(), $inscrit->getEmail());
        $this->assertEquals($expectedInscrit->getNom(), $inscrit->getNom());
        $this->assertEquals($expectedInscrit->getTelephone(), $inscrit->getTelephone());
        $this->assertEquals($expectedInscrit->getBloque(), $inscrit->getBloque());
        $this->assertEquals($expectedInscrit->getSiret(), $inscrit->getSiret());
        $this->assertEquals($expectedInscrit->getInscritAnnuaireDefense(), $inscrit->getInscritAnnuaireDefense());
        $this->assertEquals($expectedInscrit->getAdresse(), $inscrit->getAdresse());
        $this->assertEquals($expectedInscrit->getCodepostal(), $inscrit->getCodepostal());
        $this->assertEquals($expectedInscrit->getVille(), $inscrit->getVille());
        $this->assertEquals($expectedInscrit->getPays(), $inscrit->getPays());
        $this->assertEquals($expectedInscrit->getIdEtablissement(), $inscrit->getIdEtablissement());
    }

    /**
     * @param $mode
     * @param $upsertedInscrit
     * @dataProvider inscritModeProvider
     *
     * @throws Exception
     */
    public function testCreateOrUpdateEntity($mode, $upsertedInscrit)
    {
        $nom = ('create' === $mode) ? 'nom' : 'Updated Nom';
        $prenom = ('create' === $mode) ? 'prenom' : 'Updated Prenom';
        $dateModification = ('create' === $mode) ? '2019:09:03 11:06:07' : '2019:09:07 11:06:07';

        $dateTime = $this->createMock('DateTime');
        $dateTime->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:03 11:06:07'));

        $inscrit = $this->createMock(Inscrit::class);
        $inscrit->expects($this->any())
            ->method('getSiret')
            ->willReturn(38391710100058);

        $inscrit->expects($this->any())
            ->method('getLogin')
            ->willReturn('test-atexo1');

        $inscrit->expects($this->any())
            ->method('getEmail')
            ->willReturn('test-atexo1@atexo.com');

        $inscrit->expects($this->any())
            ->method('getMdp')
            ->willReturn('e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b');

        $inscrit->expects($this->any())
            ->method('getNom')
            ->willReturn($nom);

        $inscrit->expects($this->any())
            ->method('getPrenom')
            ->willReturn($prenom);

        $inscrit->expects($this->any())
            ->method('getTelephone')
            ->willReturn('0123456789');

        $inscrit->expects($this->any())
            ->method('getBloque')
            ->willReturn(0);

        $inscrit->expects($this->any())
            ->method('getCodePostal')
            ->willReturn(75000);

        $inscrit->expects($this->any())
            ->method('getVille')
            ->willReturn('Paris');

        $inscrit->expects($this->any())
            ->method('getPays')
            ->willReturn('France');

        $inscrit->expects($this->any())
            ->method('getAdresse')
            ->willReturn('rue des petits champs');

        $inscrit->expects($this->any())
            ->method('getIdEtablissement')
            ->willReturn('31311');

        $inscrit->expects($this->any())
            ->method('getInscritAnnuaireDefense')
            ->willReturn(1);

        $inscrit->expects($this->any())
            ->method('getDateCreation')
            ->willReturn('2019:09:03 11:06:07');

        $inscrit->expects($this->any())
            ->method('getDateModification')
            ->willReturn($dateModification);

        $atexoInscrit = $this->createMock(AtexoInscrit::class);
        $atexoInscrit->expects($this->any())
            ->method('createOrUpdateEntity')
            ->with($inscrit, $mode)
            ->willReturn($upsertedInscrit);

        $expectedInscrit = $atexoInscrit->createOrUpdateEntity($inscrit, $mode);

        $this->assertEquals($expectedInscrit->getLogin(), $inscrit->getLogin());
        $this->assertEquals($expectedInscrit->getMdp(), $inscrit->getMdp());
        $this->assertEquals($expectedInscrit->getEmail(), $inscrit->getEmail());
        $this->assertEquals($expectedInscrit->getNom(), $inscrit->getNom());
        $this->assertEquals($expectedInscrit->getPrenom(), $inscrit->getPrenom());
        $this->assertEquals($expectedInscrit->getTelephone(), $inscrit->getTelephone());
        $this->assertEquals($expectedInscrit->getBloque(), $inscrit->getBloque());
        $this->assertEquals($expectedInscrit->getSiret(), $inscrit->getSiret());
        $this->assertEquals($expectedInscrit->getInscritAnnuaireDefense(), $inscrit->getInscritAnnuaireDefense());
        $this->assertEquals($expectedInscrit->getAdresse(), $inscrit->getAdresse());
        $this->assertEquals($expectedInscrit->getCodepostal(), $inscrit->getCodepostal());
        $this->assertEquals($expectedInscrit->getVille(), $inscrit->getVille());
        $this->assertEquals($expectedInscrit->getPays(), $inscrit->getPays());
        $this->assertEquals($expectedInscrit->getDateCreation(), $inscrit->getDateCreation());
        $this->assertEquals($expectedInscrit->getIdEtablissement(), $inscrit->getIdEtablissement());

        // Check date Modification
        if ('update' === $mode) {
            $this->assertEquals($expectedInscrit->getDateModification(), $inscrit->getDateModification());
        }
    }

    public function inscritModeProvider()
    {
        $dateTime = $this->createMock('DateTime');
        $dateTime->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:03 11:06:07'));

        $dateModificationTime = $this->createMock('DateTime');
        $dateModificationTime->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:07 11:06:07'));

        $inscrit = new Inscrit();
        $inscrit->setIdEtablissement(31311);
        $inscrit->setLogin('test-atexo1');
        $inscrit->setMdp('e8c3f3d87ee23e76dc8a5f57f060f9a0f6a9a97fd55bb616eaad66fca7fadd1b');
        $inscrit->setEmail('test-atexo1@atexo.com');
        $inscrit->setNom('nom');
        $inscrit->setPrenom('prenom');
        $inscrit->setTelephone('0123456789');
        $inscrit->setBloque(0);
        $inscrit->setSiret(38391710100058);
        $inscrit->setInscritAnnuaireDefense('1');
        $inscrit->setCodepostal('75000');
        $inscrit->setVille('Paris');
        $inscrit->setPays('France');
        $inscrit->setAdresse('rue des petits champs');

        $date = $dateTime->format('Y:m:d H:i:s');
        $inscrit->setDateCreation($date);

        $upsertedInscrit = clone $inscrit;
        $upsertedInscrit->setNom('Updated Nom');
        $upsertedInscrit->setPrenom('Updated Prenom');
        $upsertedInscrit->setDateModification($dateModificationTime->format('Y:m:d H:i:s'));

        return [
            ['create', $inscrit],
            ['update', $upsertedInscrit],
        ];
    }
}
