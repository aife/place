<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Inscrit;

use App\Entity\Inscrit;
use App\Service\Inscrit\InscritService;
use PHPUnit\Framework\TestCase;

class InscritServiceTest extends TestCase
{
    /**
     * @dataProvider inscritDataProvider
     */
    public function testGetRgpdLabels(Inscrit $inscrit, $result)
    {
        $entrepriseService = new InscritService();

        self::assertSame($entrepriseService->getRgpdLabels($inscrit), $result);
    }

    public function inscritDataProvider(): array
    {
        return [
            "no_rgpd" => [(new Inscrit()), []],
            "rgpd_place" => [
                (new Inscrit())->setRgpdCommunicationPlace(true),
                ['COMMUNICATION_PLACE']
            ],
            "rgpd_communication" => [
                (new Inscrit())->setRgpdCommunication(true),
                ['COMMUNICATION_SIA']
            ],
            "rgpd_enquête" => [
                (new Inscrit())->setRgpdEnquete(true),
                ['COMMUNICATION_ENQUETE']
            ],
            "rgpd_all" => [
                (new Inscrit())
                    ->setRgpdCommunicationPlace(true)
                    ->setRgpdCommunication(true)
                    ->setRgpdEnquete(true),
                ['COMMUNICATION_PLACE', 'COMMUNICATION_SIA', 'COMMUNICATION_ENQUETE']
            ],
            "rgpd_all_communications" => [
                (new Inscrit())
                    ->setRgpdCommunicationPlace(true)
                    ->setRgpdCommunication(true),
                ['COMMUNICATION_PLACE', 'COMMUNICATION_SIA']
            ],
            "rgpd_communication_enquete" => [
                (new Inscrit())->setRgpdCommunication(true)->setRgpdEnquete(true),
                ['COMMUNICATION_SIA', 'COMMUNICATION_ENQUETE']
            ],
        ];
    }
}
