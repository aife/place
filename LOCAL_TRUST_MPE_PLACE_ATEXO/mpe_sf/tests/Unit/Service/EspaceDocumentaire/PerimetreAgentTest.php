<?php

namespace Tests\Unit\Service\EspaceDocumentaire;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Exception\ApiProblemForbiddenException;
use App\Service\AtexoConfiguration;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\SearchAgent;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Security\Core\Security;

/**
 * Class PerimetreAgentTest.
 */
class PerimetreAgentTest extends TestCase
{
    private $auth;

    public function setUp(): void
    {
        parent::setUp();
        $this->auth = $this->createMock(AuthorizationAgent::class);
        $this->auth->expects($this->any())
            ->method('isInTheScope')
            ->willReturn(true);
    }

    /**
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function testPerimetreAgentNoAuth()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $em = $this->createMock(EntityManager::class);
        $searchAgent = $this->createMock(SearchAgent::class);
        $security = $this->createMock(Security::class);
        $session = new Session(new MockArraySessionStorage());
        $reference = 395;
        $session->set('contexte_authentification', ['whatever' => 123]);
        $container = $this->createMock(ContainerInterface::class);

        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
            $this->auth
        );
        $perimetreAgent->isAuthorized($reference);
    }

    /**
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function testPerimetreAgentAuth()
    {
        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $reference = 395;
        $session->set('contexte_authentification', ['id' => 1]);

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryAgent, $repositoryHabilitationAgent);

        $security = $this->createMock(Security::class);

        $container = $this->createMock(ContainerInterface::class);

        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
            $this->auth
        );
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration);

        $this->assertEquals(true, $perimetreAgent->isAuthorized($reference));
    }

    public function testAuthorisation()
    {
        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $idAgent = 1;
        $session->set('contexte_authentification', ['id' => 1]);

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryAgent, $repositoryHabilitationAgent);

        $security = $this->createMock(Security::class);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
           $this->auth
        );
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration);

        $this->assertEquals(true, $perimetreAgent->isAgentAuthorized($idAgent));
    }

    public function testNonAuthorisation()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $idAgent = 20;
        $session->set('contexte_authentification', ['id' => 1]);

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryAgent, $repositoryHabilitationAgent);

        $security = $this->createMock(Security::class);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
            $this->auth
        );
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration);

        $perimetreAgent->isAgentAuthorized($idAgent);
    }
}
