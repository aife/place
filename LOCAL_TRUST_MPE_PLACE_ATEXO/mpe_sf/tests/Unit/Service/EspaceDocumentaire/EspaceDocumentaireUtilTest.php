<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\EspaceDocumentaire;

use Generator;
use App\Entity\Avis;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\RG;
use App\Entity\TDumeNumero;
use App\Repository\AvisRepository;
use App\Repository\ComplementRepository;
use App\Repository\OffreRepository;
use App\Repository\RGRepository;
use App\Service\AtexoUtil;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use App\Utils\Zip;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EspaceDocumentaireUtilTest extends TestCase
{
    /**
     * @var Consultation
     */
    private $consultation;

    /**
     * @var string
     */
    private $organisme;

    public function setUp(): void
    {
        $this->organisme = 'pmi-min-1';
        $this->consultation = new Consultation();

        $this->consultation->setId('10');
        $this->consultation->setAlloti('1');
    }

    public function testGetAllBlockCounterFiles(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    $data = ['offre 1 ', 'offre 2', 'offre 3'];
                    $mockRepository = null;
                    switch ($repository) {
                        case Offre::class:
                            $mockRepository = $this->createMock(OffreRepository::class);

                            $mockRepository->expects($this->any())
                                ->method('getAttributaire')
                                ->willReturn([]);

                            $mockRepository->expects($this->any())
                                ->method('getAllDepotFromConsultationId')
                                ->willReturn([]);

                            break;
                        case Avis::class:
                            $mockRepository = $this->createMock(AvisRepository::class);
                            $mockRepository->expects($this->once())
                                ->method('getAvisFilesAvailable')
                                ->willReturn($data);
                            break;
                        case RG::class:
                            $rg = $this->createMock(RG::class);
                            $mockRepository = $this->createMock(RGRepository::class);
                            $mockRepository->expects($this->once())
                                ->method('getReglement')
                                ->willReturn($rg);
                            break;
                        case DCE::class:
                            $mockRepository = $this->createMock(DCE::class);
                            $mockRepository->expects($this->once())
                                ->method('getDce')
                                ->willReturn(123);
                            break;
                        case Complement::class:
                            $mockRepository = $this->createMock(ComplementRepository::class);
                            $mockRepository->expects($this->once())
                                ->method('getLastComplement')
                                ->willReturn($data);
                            break;
                    }

                    return $mockRepository;
                }
            );

        $espaceDocumentaireUtil = $this->getEspaceDocumentaireUtil($em);
        $data = $espaceDocumentaireUtil->getAllBlockCounterFiles($this->consultation, $this->organisme);

        $this->assertIsArray($data);

        $this->assertArrayHasKey('pieces_modele', $data);
        $this->assertArrayHasKey('pieces_libre', $data);
        $this->assertArrayHasKey('pieces_attributaire', $data);
        $this->assertArrayHasKey('pieces_publicite', $data);
        $this->assertArrayHasKey('pieces_depot', $data);
        $this->assertArrayHasKey('pieces_dce', $data);

        $this->assertEquals(0, $data['pieces_modele']);
        $this->assertEquals(0, $data['pieces_libre']);
        $this->assertEquals(0, $data['pieces_attributaire']);
        $this->assertEquals(3, $data['pieces_publicite']);
        $this->assertEquals(0, $data['pieces_depot']);
        $this->assertEquals(2, $data['pieces_dce']);
    }

    public function testGetCounterFilesByIdOffre(): void
    {
        $mock = $this->createPartialMock(EspaceDocumentaireUtil::class, ['getPieces']);

        $mock->expects($this->any())
            ->method('getPieces')
            ->willReturn(
                [
                    'type' => 'PLI',
                    'offre' => '1718',
                    'alloti' => '0',
                    'listes' => [
                        'enveloppe_candidature' => [
                            [
                                'type' => 'PRI',
                                'nom' => 'AE_Lot1.pdf',
                                'poids' => '11.4 Ko',
                                'id_blob' => 'c01xWHFGdTQ4ZFlOWWdETG03M1lkdz09',
                                'alloti' => 0,
                            ],
                            'open' => true,
                        ],
                        'enveloppe_offre' => [
                            [
                                'type' => 'PRI',
                                'nom' => 'AE rempli Lot1.pdf',
                                'poids' => '66.3 Ko',
                                'id_blob' => 'YWtRalcrcG1YWjVwalRySHZEdDg3QT09',
                                'alloti' => 0,
                            ],
                            'open' => true,
                        ],
                        'dume_entreprise' => [],
                    ],
                ]
            );

        $data = $mock->getCounterFilesByIdOffre($this->consultation, $this->organisme, 123);

        $this->assertEquals(2, $data);
    }

    public function testPiecesPublicite(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    $mockRepository = null;
                    switch ($repository) {
                        case Avis::class:
                            $mockRepository = $this->createMock(AvisRepository::class);

                            $mockRepository->expects($this->once())
                                ->method('getAvisFilesAvailable')
                                ->willReturn(
                                    [
                                        [
                                            'id' => '16',
                                            'organisme' => 'pmi-min-1',
                                            'consultationId' => 502068,
                                            'avis' => 15529,
                                            'intituleAvis' => 0,
                                            'nomAvis' => '',
                                        ],
                                        [
                                            'id' => '17',
                                            'organisme' => 'pmi-min-1',
                                            'consultationId' => 502068,
                                            'avis' => 15529,
                                            'intituleAvis' => 0,
                                            'nomAvis' => '',
                                        ],
                                    ]
                                );

                            break;
                    }

                    return $mockRepository;
                }
            );

        $data = $this->getEspaceDocumentaireUtil($em)
            ->piecesPublicite($this->consultation, $this->organisme);

        $this->assertEquals(2, $data);
    }

    public function testPiecesDesAttributaires(): void
    {
        $mock = $this->createPartialMock(EspaceDocumentaireUtil::class, ['getPiecesDesAttributaires', 'getOffres']);

        $mock->expects($this->any())
            ->method('getOffres')
            ->willReturn(
                [
                    ['id' => 1],
                ]
            );

        $mock->expects($this->any())
            ->method('getPiecesDesAttributaires')
            ->willReturn(
                [
                    'alloti' => '1',
                    'listes' => [
                        'dume_entreprise' => [
                            [
                                'nom' => '6bsbkcbx.pdf',
                                'poids' => false,
                                'id_blob' => 'blcwdCtoWnZjVk9PR2I3V1krOVNLQT09',
                                'type' => 'PDF',
                                'extension' => 'pdf',
                            ],
                            [
                                'nom' => '6bsbkcbx.xml',
                                'poids' => false,
                                'id_blob' => 'SVd1OW01SkZvaWtiZXBlSHlNUVlMdz09',
                                'type' => 'XML',
                                'extension' => 'XML',
                            ],
                        ],
                        'enveloppe_candidature' => [
                            [
                                'type' => 'PRI',
                                'nom' => 'Candidature.zip',
                                'poids' => '169 Ko',
                                'id_blob' => 'Rm5UOWlzSEoydjhNTHBGRXF0eHhZUT09',
                                'alloti' => 0,
                            ],
                            'open' => true,
                        ],
                        'enveloppe_offre' => [
                            [
                                'label' => 'Lot 1 - LOT A',
                                'numLot' => 1,
                                'labelLot' => 'Lot 1',
                                'open' => true,
                                'fichiers' => [
                                    [
                                        'type' => 'PRI',
                                        'nom' => 'Offre.zip',
                                        'poids' => '197 Ko',
                                        'id_blob' => 'QzdJNWxQNUNWRmRITmVkZCsybDQ2Zz09',
                                    ],
                                ],
                            ],
                        ],
                        'enveloppe_anonymat' => [],
                    ],
                ]
            );

        $data = $mock->piecesDesAttributaires($this->consultation, $this->organisme);

        $this->assertEquals(4, $data);
    }

    public function testGetCountEnveloppe(): void
    {
        $data = [
            [
                'type' => 'PRI',
                'nom' => 'Avis de publicite complémentaire - 1.0.pdf',
                'poids' => '51.2 Ko',
                'id_blob' => 'M3J3Q2NrdkZ0bnM2M3JnUUg5eG5KZz09',
                'alloti' => 0,
            ],
            [
                'type' => 'PRI',
                'nom' => 'BDC.pdf',
                'poids' => '74.8 Ko',
                'id_blob' => 'Yk5PN1FrUXBIa2ZxbXVZTXpIa3REdz09',
                'alloti' => 0,
            ],
            [
                'type' => 'PRI',
                'nom' => 'Bordereau des prix Modele.xls',
                'poids' => '14 Ko',
                'id_blob' => 'clpLVHVkZU9KQnBJV2h3VVBZOER1dz09',
                'alloti' => 0,
            ],
            'open' => true,
        ];
        $counter = $this->getEspaceDocumentaireUtil($this->createMock(EntityManagerInterface::class))
            ->getCountEnveloppe($data);

        $this->assertEquals(3, $counter);

        $data['open'] = false;

        $counter = $this->getEspaceDocumentaireUtil($this->createMock(EntityManagerInterface::class))
            ->getCountEnveloppe($data);

        $this->assertEquals(0, $counter);
    }

    public function testGetCountEnveloppeWithKeyFilesExist(): void
    {
        $data = [
            [
                'fichiers' => [
                    [
                        'type' => 'ACE',
                        'nom' => 'AE TEST - 20201203141834 - Signature 1.pdf',
                        'extension' => 'pdf',
                        'poids' => '28.2 Ko',
                        'id_blob' => 'QmcwZ25pUElabEtyUjRabStMenFNdz09',
                    ],
                    [
                        'type' => 'PRI',
                        'nom' => 'Offre_TEST.zip',
                        'extension' => 'zip',
                        'poids' => '27.8 Ko',
                        'id_blob' => 'SGhCUDAvRkQ2eEhzWGlQOE9oUGlJUT09',
                    ],
                ],
                'label' => 'Lot 1 - Lot 2',
                'open' => true,
                'numLot' => 1,
                'labelLot' => 'Lot 1',
            ],
            [
                'fichiers' => [
                    [
                        'type' => 'ACE',
                        'nom' => 'AE TEST - 20201203141834 - Signature 1.pdf',
                        'extension' => 'pdf',
                        'poids' => '28.2 Ko',
                        'id_blob' => 'QmcwZ25pUElabEtyUjRabStMenFNdz09',
                    ],
                    [
                        'type' => 'PRI',
                        'nom' => 'Offre_TEST.zip',
                        'extension' => 'zip',
                        'poids' => '27.8 Ko',
                        'id_blob' => 'SGhCUDAvRkQ2eEhzWGlQOE9oUGlJUT09',
                    ],
                ],
                'label' => 'Lot 2 - Lot 3',
                'open' => true,
                'numLot' => 1,
                'labelLot' => 'Lot 1',
            ],
        ];

        $counter = $this->getEspaceDocumentaireUtil($this->createMock(EntityManagerInterface::class))
            ->getCountEnveloppe($data);

        $this->assertEquals(4, $counter);
    }

    /**
     * @return EspaceDocumentaireUtil
     */
    private function getEspaceDocumentaireUtil(EntityManagerInterface $em)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $mountManager = $this->createMock(MountManager::class);
        $zip = $this->createMock(Zip::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);

        return new EspaceDocumentaireUtil(
            $parameterBag,
            $em,
            $mountManager,
            $zip,
            $atexoUtil,
            $translator,
            $encryption
        );
    }

    /**
     * @param $dumeEntreprise
     * @param $expected
     * @dataProvider providerTestGetDumeResult
     */
    public function testGetDumeResult($dumeEntreprise, $expected)
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $service = $this->getEspaceDocumentaireUtil($em);
        $organisme = new Organisme();
        $result = [];
        $res = $service->getDumeResult($organisme, $dumeEntreprise, $result);
        if (null === $dumeEntreprise) {
            $actual = $res['message'];
        } else {
            $actual = $res['listes']['dume_entreprise'][0]['nom'];
        }
        $this->assertEquals($expected, $actual);
    }

    /**
     * Données de test.
     *
     * @return Generator
     */
    public function providerTestGetDumeResult()
    {
        $dumeEntreprise = new TDumeNumero();
        $dumeEntreprise->setNumeroDumeNational('ABCDEF');
        yield [$dumeEntreprise, 'ABCDEF.pdf'];
        yield [null, null];
    }
    public function testGetDepots()
    {
        $offres = [
            "id" => 1689,
            "numeroReponse" => 1,
            "nomEntrepriseInscrit" => "ATEXO",
            "statutOffres" => 2
        ];

        $offreRepository = $this->createMock(OffreRepository::class);
        $offreRepository
            ->expects($this->once())
            ->method('getAllDepotFromConsultationId')
            ->willReturn($offres);

        $em = $this->createMock(EntityManagerInterface::class);

        $em
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($offreRepository);

        $data = $this->getEspaceDocumentaireUtil($em)->getDepots($this->consultation);

        $this->assertEquals($offres, $data);
    }
}
