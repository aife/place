<?php

namespace Tests\Unit\Service\EspaceDocumentaire;

use ZipArchive;
use ReflectionClass;
use ReflectionException;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Repository\ConsultationRepository;
use App\Service\EspaceDocumentaire\EspaceDocumentaireService;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class EspaceDocumentaireServiceTest.
 */
class EspaceDocumentaireServiceTest extends TestCase
{
    private $parameterBag;
    private $container;
    private $entityManagerMock;
    private $encryptions;
    private $espaceDocumentaire;
    private $listeFichiers;
    private $consultation;
    private $archive;
    private $folderTmp;
    private $numFiles;
    private $checkArchive;
    private $archiveName;

    /**
     * Setup.
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->container = $this->createMock(ContainerInterface::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->entityManagerMock = $this->createMock(EntityManager::class);
        $this->mountManagerMock = $this->createMock(MountManager::class);
        $logger = $this->createMock(Logger::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $this->encryptions = new Encryption($parameterBag);
        $this->zipArchive = new ZipArchive();
        $this->espaceDocumentaire = new EspaceDocumentaireService(
            $this->entityManagerMock,
            $this->encryptions,
            $this->mountManagerMock,
            $this->parameterBag,
            $this->zipArchive
        );

        $this->folderTmp = '/var/tmp/mpe';
        $this->parameterBag->expects($this->any())
            ->method('get')
            ->willReturnCallback(
                function ($parameter) {
                    if ('COMMON_TMP' === $parameter) {
                        return $this->folderTmp;
                    }
                }
            );
        $folder = $this->folderTmp . '/test/dossier/';
        if (!is_dir($this->folderTmp . '/test/dossier/')) {
            mkdir($folder, 0777, true);
        }

        $fp = fopen($this->folderTmp . '/test/fichier1.txt', 'a+');
        fwrite($fp, 'test');
        fclose($fp);

        $fp = fopen($this->folderTmp . '/test/dossier/fichier2.txt', 'a+');
        fwrite($fp, 'test');
        fclose($fp);

        $this->listeFichiers = [
            'PIECESDCE' => [
                'DUMEACHETEUR' => [
                    'XML' => [$this->encryptions->cryptId(2)],
                    'PDF' => [$this->encryptions->cryptId(2)],
                ],
                'RC' => $this->encryptions->cryptId(2),
                'DCE' => [
                    //idBlob
                    $this->encryptions->cryptId(2) => [
                        $this->encryptions->cryptId('/test/fichier1.txt'),
                    ],
                ],
                'AUTRESPIECES' => $this->encryptions->cryptId(2),
            ],
            'PLI' => [
                'El 1 - ATEXO' => [
                    'DUMEOE' => [
                        'XML' => $this->encryptions->cryptId(2),
                        'PDF' => $this->encryptions->cryptId(2),
                    ],
                    'ENVELOPPE_CANDIDATURE' => [
                        $this->encryptions->cryptId(2),
                        $this->encryptions->cryptId(2),
                    ],
                    'ENVELOPPE_OFFRE' => [
                        'FMA-LOT-123 / FMA-lot-1' => [
                            $this->encryptions->cryptId(2),
                            $this->encryptions->cryptId(2),
                        ],
                        'FMA-LOT-123 / FMA-lot-3' => [
                            $this->encryptions->cryptId(2),
                            $this->encryptions->cryptId(2),
                        ],
                    ],
                    'ENVELOPPE_ANONYMA' => [
                        'FMA-LOT-123 / FMA-lot-3' => [
                            $this->encryptions->cryptId(2),
                            $this->encryptions->cryptId(2),
                        ],
                    ],
                ],
            ],
            'ATTRIBUTAIRE' => [
                    'El 1 - ATEXO' => [
                        'ENVELOPPE_CANDIDATURE' => [
                            $this->encryptions->cryptId(2),
                            $this->encryptions->cryptId(2),
                        ],
                        'ENVELOPPE_OFFRE' => [
                            'FMA-LOT-123 / FMA-lot-1' => [
                                $this->encryptions->cryptId(2),
                                $this->encryptions->cryptId(2),
                            ],
                            'FMA-LOT-123 / FMA-lot-3' => [
                                $this->encryptions->cryptId(2),
                                $this->encryptions->cryptId(2),
                            ],
                        ],
                    ],
            ],
            'PUBLICITE' => [
                $this->encryptions->cryptId(2),
                $this->encryptions->cryptId(2),
            ],
            'AUTRESPIECESCONSULTATION' => [
                $this->encryptions->cryptId(2),
            ],
        ];

        $this->checkArchive = [
            'PIECESDCE' => true,
            'PLI' => true,
        ];

        $this->mountManagerMock->expects($this->any())
            ->method('getAbsolutePath')
            ->willReturn(__DIR__ . '/../../../../../mpe_sf/tests/Unit/data/test.zip');

        $this->consultation = new Consultation();
        $class = new ReflectionClass($this->consultation);
        $this->consultation->setReferenceUtilisateur('test');
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($this->consultation, 1);

        $blobRc = new BloborganismeFile();
        $classBloborganismeFile = new ReflectionClass($blobRc);
        $propertyBloborganismeFile = $classBloborganismeFile->getProperty('id');
        $propertyBloborganismeFile->setAccessible(true);
        $propertyBloborganismeFile->setValue($blobRc, 1);

        $blobRc->setOrganisme('pmi-min-1');
        $blobRc->setChemin('/test');
        $blobRc->setHash('kfjsljdfmsj');
        $blobRc->setName('fichier');

        $this->entityManagerMock->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Consultation::class == $repository) {
                        $consultation = new Consultation();
                        $class = new ReflectionClass($consultation);
                        $consultation->setReferenceUtilisateur('test');
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($consultation, 1);
                        $repository = $this->createMock(ConsultationRepository::class);
                        $repository->expects($this->any())
                            ->method('find')
                            ->willReturn($consultation);

                        return $repository;
                    }

                    if (BloborganismeFile::class == $repository) {
                        $blobRc = new BloborganismeFile();
                        $classBloborganismeFile = new ReflectionClass($blobRc);
                        $propertyBloborganismeFile = $classBloborganismeFile->getProperty('id');
                        $propertyBloborganismeFile->setAccessible(true);
                        $propertyBloborganismeFile->setValue($blobRc, 1);
                        $repository = $this->createMock(ObjectRepository::class);
                        $blobRc->setOrganisme('pmi-min-1');
                        $blobRc->setChemin('/test');
                        $blobRc->setHash('kfjsljdfmsj');
                        $blobRc->setName('fichier');

                        $repository->expects($this->any())
                            ->method('find')
                            ->willReturn($blobRc);

                        return $repository;
                    }
                }
            );

        $referenceUtilisateur = $this->consultation->getReferenceUtilisateur();
        $date = '20191129105138';
        $this->archive = hash('sha256', $referenceUtilisateur) . $date . '.zip';
        $this->numFiles = 14;
        $this->archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (file_exists($this->archiveName)) {
            unlink($this->archiveName);
        }
    }

    /**
     * @throws ReflectionException
     */
    public function testGetGenerateArchive()
    {
        $consultationId = 1;
        $json = base64_encode(json_encode($this->listeFichiers));
        $date = '20191129105138';
        $result = $this->espaceDocumentaire->genereteArchive($consultationId, $json, $date);
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        foreach ($result['resultatArchive'] as $ckeck) {
            $this->assertEquals(true, $ckeck);
        }
        $this->checkNumFileForArchive($this->numFiles, $archiveName);
    }

    public function testExtractionFichierDCE()
    {
        $key = 'PIECESDCE';
        $fichier = $this->listeFichiers[$key];
        $fichier = json_decode(json_encode($fichier));
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $this->espaceDocumentaire->extractionFichier($key, $fichier);
        }

        $this->checkNumFileForArchive(5, $archiveName);
    }

    public function testExtractionRc()
    {
        $key = 'RC';
        $pieces = $this->listeFichiers['PIECESDCE'][$key];
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $path = 'PIECESDCE/RC';
            $this->espaceDocumentaire->extractionFiles($pieces, $path);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionAutre()
    {
        $key = 'AUTRESPIECES';
        $pieces = $this->listeFichiers['PIECESDCE'][$key];
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $path = 'PIECESDCE/AUTRESPIECES';
            $this->espaceDocumentaire->extractionFiles($pieces, $path);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionDumeAgentXML()
    {
        $key = 'DUMEACHETEUR';
        $pieces = $this->listeFichiers['PIECESDCE'][$key]['XML'][0];
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $result = $this->espaceDocumentaire->extractionDumeAgent($pieces, 'XML');
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionDumeAgentPDF()
    {
        $key = 'DUMEACHETEUR';
        $pieces = $this->listeFichiers['PIECESDCE'][$key]['PDF'][0];
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $result = $this->espaceDocumentaire->extractionDumeAgent($pieces, 'PDF');
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionPli()
    {
        $key = 'PLI';
        $fichier = $this->listeFichiers[$key];
        $fichier = json_decode(json_encode($fichier));
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $this->espaceDocumentaire->extractionPli($fichier, 'PLI');
        }

        $this->checkNumFileForArchive(4, $archiveName);
    }

    public function testExtractionEnveloppe()
    {
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this->listeFichiers['PLI']));
            $this->espaceDocumentaire->extractionEnveloppe($pieces, 'PLI', true);
        }

        $this->checkNumFileForArchive(4, $archiveName);
    }

    public function testExtractionPiecesEnveloppe()
    {
        $path = 'PLI/EL1 - ATEXO/';
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this
                ->listeFichiers['PLI']['El 1 - ATEXO']['ENVELOPPE_OFFRE']));
            $result = $this->espaceDocumentaire->extractionPiecesEnveloppe($pieces, $path);
        }

        $this->checkNumFileForArchive(2, $archiveName);
    }

    public function testCheckConstructionArchive()
    {
        $result = $this->espaceDocumentaire->checkConstructionArchive($this->checkArchive);
        $this->assertEquals(true, $result);
    }

    public function testExtractionPublicite()
    {
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this->listeFichiers['PUBLICITE']), true);
            $result = $this->espaceDocumentaire->extractionPublicite($pieces);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionDce()
    {
        $key = 'DCE';
        $pieces = $this->listeFichiers['PIECESDCE'][$key];
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $path = 'PIECESDCE/DCE';
            $this->espaceDocumentaire->extractionDCE($pieces, $path);
        }
        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionPiecesLibres()
    {
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this->listeFichiers['AUTRESPIECESCONSULTATION']), true);
            $result = $this->espaceDocumentaire->extractionPiecesLibres($pieces);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionDumeOEXML()
    {
        $path = 'PLI/EL1 - ATEXO/';
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this
                ->listeFichiers['PLI']['El 1 - ATEXO']['DUMEOE']['XML']));
            $result = $this->espaceDocumentaire->extractionDumeOE($pieces, $path, false);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    public function testExtractionDumeOEPdf()
    {
        $path = 'PLI/EL1 - ATEXO/';
        $archiveName = $this->parameterBag->get('COMMON_TMP') . '/' . $this->archive;
        if (true === $this->zipArchive->open($archiveName, ZipArchive::CREATE)) {
            $pieces = json_decode(json_encode($this
                ->listeFichiers['PLI']['El 1 - ATEXO']['DUMEOE']['PDF']));
            $result = $this->espaceDocumentaire->extractionDumeOE($pieces, $path, false);
        }

        $this->checkNumFileForArchive(1, $archiveName);
    }

    /**
     * @param $numFiles
     * @param $archiveName
     */
    public function checkNumFileForArchive($numFiles, $archiveName)
    {
        if (true === $this->zipArchive->open($archiveName)) {
            $this->assertEquals($numFiles, $this->zipArchive->numFiles);
            $this->zipArchive->close();

            unlink($this->folderTmp . '/' . $this->archive);
        }
    }

    public function testRenamePathFolder()
    {
        $path = '/PLI/DUMEOE/XML/XXXXX.xml';
        $result = $this->espaceDocumentaire->renamePathFolder($path);
        $this->assertEquals('/Pièces de tous les dépôts/DUME/XML/XXXXX.xml', $result);
    }

    public function testCheckValidArchive()
    {
        $tab = [
            'PIECE' => true,
            'DUME' => false,
        ];
        $result = $this->espaceDocumentaire->checkValidArchive($tab);
        $this->assertEquals(false, $result);
    }
}
