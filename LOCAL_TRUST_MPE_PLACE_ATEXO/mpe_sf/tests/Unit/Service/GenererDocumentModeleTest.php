<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service;

use App\Service\WebServicesRedac;
use DateTime;
use App\Entity\BloborganismeFile;
use App\Entity\DocumentTemplate;
use App\Entity\DocumentTemplateSurcharge;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\Entreprise;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\PieceGenereConsultation;
use App\Exception\BlobNotFoundException;
use App\Repository\BloborganismeFileRepository;
use App\Repository\DocumentTemplateRepository;
use App\Repository\DocumentTemplateSurchargeRepository;
use App\Repository\EchangeDocumentaire\EchangeDocBlobRepository;
use App\Service\ArgumentPieceGenereModele;
use App\Service\AtexoFichierOrganisme;
use App\Service\DocGen\Docgen;
use App\Service\DocGen\DocumentGenere;
use App\Service\DocGen\MergeFields;
use App\Service\DocGen\OffreNonRetenue;
use App\Service\DocGen\Template;
use App\Service\GenererDocumentModele;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\This;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GenererDocumentModeleTest extends TestCase
{
    /**
     * @var array
     */
    private $pieces;
    /**
     * @var array
     */
    private $documentConsultations;
    /**
     * @var string
     */
    private $organisme;

    protected function setUp(): void
    {
        $this->pieces = [
            'type' => 'PIECESGENEREESCONSULTATION',
            'message' => 'Vous n\'êtes pas autorisé à accéder à cette ressource !',
            'fichiers' => [],
            'statut' => 200,
        ];
        $document = new PieceGenereConsultation();
        $document->setCreatedAt(new DateTime('NOW'));
        $this->documentConsultations = [
            $document,
        ];
        $this->organisme = 'pmi-min-1';
    }

    public function testGetDocumentConsultations()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    $bloborganismeFile = new BloborganismeFile();
                    $bloborganismeFile->setOrganisme('pmi-min-1');
                    $bloborganismeFile->setName('PV_CAO_admission.docx');
                    $bloborganismeFile->setStatutSynchro(0);
                    $bloborganismeFile->setHash('d3f36a9fe9cc9ae8ebdea4f506330f0f2db11da7');
                    $bloborganismeFile->setChemin('2021/01/07/pmi-min-1/files/');
                    $mockRepository = null;
                    switch ($repository) {
                        case BloborganismeFile::class:
                            $mockRepository = $this->createMock(BloborganismeFileRepository::class);
                            $mockRepository->expects($this->any())
                                ->method('findOneBy')
                                ->willReturn($bloborganismeFile);
                            break;
                        case EchangeDocBlob::class:
                            $mockRepository = $this->createMock(EchangeDocBlobRepository::class);
                            $mockRepository->expects($this->once())
                                ->method('findBy')
                                ->willReturn([]);
                            break;
                    }

                    return $mockRepository;
                }
            );
        $mockGenererDocumentModele = $this->getGenererDocumentModele($em);
        $documents = $mockGenererDocumentModele->getDocumentConsultations(
            $this->pieces,
            $this->documentConsultations,
            $this->createMock(Encryption::class),
            $this->createMock(MountManager::class),
            $this->organisme
        );
        $this->assertIsArray($documents);
        $this->assertArrayHasKey('type', $documents);
        $this->assertArrayHasKey('fichiers', $documents);
        $this->assertArrayHasKey('statut', $documents);
        $this->assertEquals(1, count($documents['fichiers']));
    }

    public function testGetFileExtension()
    {
        $mockGenererDocumentModele = $this->createPartialMock(GenererDocumentModele::class, []);

        $extension = $mockGenererDocumentModele->getFileExtension('Demande_precisions.docx');
        self::assertSame('docx', $extension);

        $extension = $mockGenererDocumentModele->getFileExtension('Demande_precisions');
        self::assertEmpty($extension);
    }

    public function testGetNameFile()
    {
        $mockDocumentModele = $this->getGenererDocumentModele($this->createMock(EntityManagerInterface::class));
        $lot = new Lot();
        $lot->setLot(30);

        $entreprise = new Entreprise();
        $entreprise->setNom('ATC');

        $response = $mockDocumentModele->getNameFile($lot, $entreprise, 'test', 'pdf');
        $this->assertEquals('test_lot_30_ATC.pdf', $response);

        $response = $mockDocumentModele->getNameFile($lot, null, 'test', 'pdf');
        $this->assertEquals('test_lot_30.pdf', $response);

        $response = $mockDocumentModele->getNameFile(null, $entreprise, 'test', 'pdf');
        $this->assertEquals('test_ATC.pdf', $response);
    }

    public function testKsortModeles(): void
    {
        $data = [
            'Noti1' => 'Template1',
            'Noti10' => 'Template10',
            'Noti2' => 'Template2',
            'Noti11' => 'Template11',
            'Noti3' => 'Template3',
        ];

        $newData =  [
            'Noti1' => 'Template1',
            'Noti2' => 'Template2',
            'Noti3' => 'Template3',
            'Noti10' => 'Template10',
            'Noti11' => 'Template11',
        ];

        $genererDocumentModele =
            $this->getGenererDocumentModele($this->createMock(EntityManagerInterface::class));
        $response = $genererDocumentModele->ksortModeles($data);

        $this->assertEquals($newData, $response);
        $this->assertIsArray($response);
        $this->assertNotEmpty($response);

        $response = $genererDocumentModele->ksortModeles([]);
        $this->assertIsArray($response);
        $this->assertEmpty($response);
    }

    private function getGenererDocumentModele($mockEm)
    {
        $mockParameterBag = $this->createMock(ParameterBagInterface::class);
        $mockArgumentPiece = $this->createMock(MergeFields::class);
        $mockArgumentPiece->expects($this->any())
            ->method('pieceGenereeModele')
            ->willReturn([]);
        $mockParameterBag->expects($this->any())
            ->method('get')
            ->willReturn('https://ressources.local-trust.com/docs/modeles/atexo/gen_doc_mpe/');

        return new GenererDocumentModele(
            $mockEm,
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Encryption::class),
            $this->createMock(MountManager::class),
            $mockArgumentPiece,
            $this->createMock(HttpClientInterface::class),
            $mockParameterBag,
            $this->createMock(Docgen::class),
            $this->createMock(OffreNonRetenue::class),
            $this->createMock(Template::class),
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );
    }

    public function testGetCustomTemplatePathDefault(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $documentName = 'NOTI1.docx';
        $acronyme = 'acronyme';

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('URL_GENDOC_TEMPLATE')
            ->willReturn('gendoc_path/')
        ;

        $documentTemplateSurchargeRepo = $this->createMock(DocumentTemplateSurchargeRepository::class);
        $documentTemplateSurchargeRepo
            ->expects(self::exactly(2))
            ->method('getOverrideTemplate')
            ->withConsecutive(
                [$documentName, DocumentTemplateSurcharge::PRIORITY_ORGANISME, $acronyme],
                [$documentName, DocumentTemplateSurcharge::PRIORITY_CLIENT]
            )
            ->willReturnOnConsecutiveCalls(null, null)
        ;

        $em
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($documentTemplateSurchargeRepo, $documentTemplateSurchargeRepo)
        ;

        $templateInformations = $genererDocumentModele->getCustomTemplatePath($documentName, $acronyme);
        $templatePath = $templateInformations['path'] . $templateInformations['fileName'];

        self::assertSame($templatePath, 'gendoc_path/NOTI1.docx');
    }

    public function testGetCustomTemplatePathClient(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $documentName = 'NOTI1.docx';
        $acronyme = 'acronyme';

        $documentSurcharge = (new DocumentTemplateSurcharge())
            ->setFileName('surcharge_client.docx')
        ;

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['URL_GENDOC_TEMPLATE'],
                ['BASE_ROOT_DIR'],
            )
            ->willReturnOnConsecutiveCalls('gendoc_path/', 'data/')
        ;

        $documentTemplateSurchargeRepo = $this->createMock(DocumentTemplateSurchargeRepository::class);
        $documentTemplateSurchargeRepo
            ->expects(self::exactly(2))
            ->method('getOverrideTemplate')
            ->withConsecutive(
                [$documentName, DocumentTemplateSurcharge::PRIORITY_ORGANISME, $acronyme],
                [$documentName, DocumentTemplateSurcharge::PRIORITY_CLIENT]
            )
            ->willReturnOnConsecutiveCalls(null, $documentSurcharge)
        ;

        $em
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($documentTemplateSurchargeRepo, $documentTemplateSurchargeRepo)
        ;

        $templateInformations = $genererDocumentModele->getCustomTemplatePath($documentName, $acronyme);
        $templatePath = $templateInformations['path'] . $templateInformations['fileName'];

        self::assertSame($templatePath, 'data/doc-template/client/surcharge_client.docx');
    }

    public function testGetCustomTemplatePathOrganisme(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $documentName = 'NOTI1.docx';
        $acronyme = 'acronyme';

        $documentSurcharge = (new DocumentTemplateSurcharge())
            ->setFileName('surcharge_organisme.docx')
        ;

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['URL_GENDOC_TEMPLATE'],
                ['BASE_ROOT_DIR'],
            )
            ->willReturnOnConsecutiveCalls('gendoc_path/', 'data/')
        ;

        $documentTemplateSurchargeRepo = $this->createMock(DocumentTemplateSurchargeRepository::class);
        $documentTemplateSurchargeRepo
            ->expects(self::once())
            ->method('getOverrideTemplate')
            ->with($documentName, DocumentTemplateSurcharge::PRIORITY_ORGANISME, $acronyme)
            ->willReturn($documentSurcharge)
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($documentTemplateSurchargeRepo)
        ;

        $templateInformations = $genererDocumentModele->getCustomTemplatePath($documentName, $acronyme);
        $templatePath = $templateInformations['path'] . $templateInformations['fileName'];

        self::assertSame($templatePath, 'data/doc-template/organisme/acronyme/surcharge_organisme.docx');
    }

    public function testGetDocumentsAfterOverrideByTemplateSurcharge(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $documentTemplateArray = [
            "AF" => [
                "nom" => "AF.docx",
                "nomAfficher" => "AF - Affichage",
                "document" => "AF",
            ],
            "NOTI1" => [
                "nom" => "NOTI1.docx",
                "nomAfficher" => "NOTI1 - Affichage",
                "document" => "NOTI1",
            ],
            "NOTI3" => [
                "nom" => "NOTI3.docx",
                "nomAfficher" => "NOTI3 - Affichage",
                "document" => "NOTI3"
            ]
        ];

        $documentTemplateSurchargeNoti1 = new class () extends DocumentTemplateSurcharge {
            public function getNomAfficher(): ?string
            {
                return 'NOTI1 Surcharge Client';
            }
            public function getDocumentTemplate(): DocumentTemplate
            {
                return new class () extends DocumentTemplate {
                    public function getNom(): ?string
                    {
                        return 'NOTI1.docx';
                    }
                    public function getDocument(): ?string
                    {
                        return 'NOTI1';
                    }
                };
            }
            public function getPriority(): ?int
            {
                return DocumentTemplateSurcharge::PRIORITY_CLIENT;
            }
        };

        $documentTemplateSurchargeNoti3Client = new class () extends DocumentTemplateSurcharge {
            public function getNomAfficher(): ?string
            {
                return 'NOTI3 Surcharge Client';
            }
            public function getDocumentTemplate(): DocumentTemplate
            {
                return new class () extends DocumentTemplate {
                    public function getNom(): ?string
                    {
                        return 'NOTI3.docx';
                    }
                    public function getDocument(): ?string
                    {
                        return 'NOTI3';
                    }
                };
            }
            public function getPriority(): ?int
            {
                return DocumentTemplateSurcharge::PRIORITY_CLIENT;
            }
        };

        $documentTemplateSurchargeNoti3Organisme = new class () extends DocumentTemplateSurcharge {
            public function getNomAfficher(): ?string
            {
                return 'NOTI3 Surcharge Organisme';
            }
            public function getDocumentTemplate(): DocumentTemplate
            {
                return new class () extends DocumentTemplate {
                    public function getNom(): ?string
                    {
                        return 'NOTI3.docx';
                    }
                    public function getDocument(): ?string
                    {
                        return 'NOTI3';
                    }
                };
            }
            public function getOrganisme(): ?Organisme
            {
                return new Organisme();
            }
            public function getPriority(): ?int
            {
                return DocumentTemplateSurcharge::PRIORITY_ORGANISME;
            }
        };

        $acronyme = 'acronyme';

        $documentTemplatesRepo = $this->createMock(DocumentTemplateRepository::class);
        $documentTemplatesRepo
            ->expects(self::once())
            ->method('findAllTemplate')
            ->willReturn($documentTemplateArray)
        ;

        $documentTemplateSurchargeRepo = $this->createMock(DocumentTemplateSurchargeRepository::class);
        $documentTemplateSurchargeRepo
            ->expects(self::once())
            ->method('findSurchargeTemplate')
            ->with($acronyme)
            ->willReturn([
                $documentTemplateSurchargeNoti3Organisme,
                $documentTemplateSurchargeNoti1,
                $documentTemplateSurchargeNoti3Client
            ])
        ;

        $em
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $documentTemplatesRepo,
                $documentTemplateSurchargeRepo
            )
        ;

        $expectedResults = [
            "AF" => [
                "nom" => "AF.docx",
                "nomAfficher" => "AF - Affichage",
                "document" => "AF",
            ],
            "NOTI1" => [
                "nom" => "NOTI1.docx",
                "nomAfficher" => "NOTI1 Surcharge Client",
                "document" => "NOTI1",
            ],
            "NOTI3" => [
                "nom" => "NOTI3.docx",
                "nomAfficher" => "NOTI3 Surcharge Organisme",
                "document" => "NOTI3"
            ]
        ];

        self::assertSame(
            $expectedResults,
            $genererDocumentModele->getDocumentsAfterOverrideByTemplateSurcharge($acronyme)
        );
    }

    public function testGetEditedDocument()
    {
        $document = new PieceGenereConsultation();
        $document->setCreatedAt(new DateTime('NOW'));

        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $this->createMock(DocumentGenere::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $this->expectException(BlobNotFoundException::class);
        $genererDocumentModele->getEditedDocument($document, $this->organisme);
    }

    public function testGetFullNameDocumentGenereError()
    {
        $document = new PieceGenereConsultation();
        $document->setCreatedAt(new DateTime('NOW'));

        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);
        $documentGenere = $this->createMock(DocumentGenere::class);

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $documentGenere,
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $this->expectException(BlobNotFoundException::class);
        $genererDocumentModele->getFullNameDocumentGenere($document, $this->organisme);
    }

    public function testGetFullNameDocumentGenereSuccess()
    {
        $document = new PieceGenereConsultation();
        $document->setCreatedAt(new DateTime('NOW'));

        $blob = new BloborganismeFile();
        $blob->setExtension('.docx');
        $document->setBlobId($blob);

        $em = $this->createMock(EntityManagerInterface::class);
        $atexoBlobOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $encryption = $this->createMock(Encryption::class);
        $mountManager = $this->createMock(MountManager::class);
        $argument = $this->createMock(MergeFields::class);
        $client = $this->createMock(HttpClientInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $docgen = $this->createMock(Docgen::class);
        $onr = $this->createMock(OffreNonRetenue::class);
        $template = $this->createMock(Template::class);
        $documentGenere = $this->createMock(DocumentGenere::class);

        $mountManager->expects($this->once())
            ->method('getAbsolutePath')
            ->willReturn('NOTI')
        ;

        $genererDocumentModele = new GenererDocumentModele(
            $em,
            $atexoBlobOrganisme,
            $translator,
            $encryption,
            $mountManager,
            $argument,
            $client,
            $parameterBag,
            $docgen,
            $onr,
            $template,
            $documentGenere,
            $this->createMock(LoggerInterface::class),
            $this->createMock(WebServicesRedac::class),
            $this->createMock(Security::class)
        );

        $fileName = $genererDocumentModele->getFullNameDocumentGenere($document, $this->organisme);

        $this->assertEquals($fileName, 'NOTI.docx');
    }
}
