<?php

namespace Tests\Unit\Service;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Exception\ApiProblemAlreadyExistException;
use App\Repository\ConfigurationOrganismeRepository;
use App\Service\ConfigurationOrganismeService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ConfigurationOrganismeServiceTest.
 */
class ConfigurationOrganismeServiceTest extends TestCase
{
    /**
     * Permet de copier une config source vers une config cible.
     */
    public function testCopierConfigurationOrganisme()
    {
        $container = $this->createMock(ContainerInterface::class);
        $serializer = $this->createMock(SerializerInterface::class);
        $em = $this->createMock(EntityManager::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) {
                    static $callCount = [];

                    $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];
                    if (ConfigurationOrganisme::class === $entityName) {
                        $marchePublieRepository = $this->createMock(ConfigurationOrganismeRepository::class);

                        if (1 == $callCount[$entityName]) {
                            $coSource = new ConfigurationOrganisme();
                            $coSource->setOrganisme('organismeSource');
                            $coSource->setNumeroProjetAchat('1');
                            $coSource->setEspaceCollaboratif('0');
                            $coSource->setDonneesComplementaires('0');
                        } else {
                            $coSource = null;
                        }
                        $marchePublieRepository->expects($this->any())
                            ->method('getByAcronyme')
                            ->willReturn($coSource);

                        return $marchePublieRepository;
                    }
                }
            ));

        $configurationOrganismeService = new ConfigurationOrganismeService($container, $em, $serializer);
        $coCible = $configurationOrganismeService->copierConfigurationOrganisme(
            'organismeSource',
            'organismeCible'
        );

        $this->assertTrue($coCible instanceof ConfigurationOrganisme);
        $this->assertEquals('organismeCible', $coCible->getOrganisme());
        $this->assertEquals('1', $coCible->getNumeroProjetAchat());
        $this->assertEquals('0', $coCible->getEspaceCollaboratif());
        $this->assertEquals('0', $coCible->getDonneesComplementaires());
    }

    public function testException()
    {
        $this->expectException(ApiProblemAlreadyExistException::class);
        $this->expectExceptionCode(0);
        $container = $this->createMock(ContainerInterface::class);
        $serializer = $this->createMock(SerializerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) {
                    static $callCount = [];

                    $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];
                    if (ConfigurationOrganisme::class === $entityName) {
                        $marchePublieRepository = $this->createMock(
                            ConfigurationOrganismeRepository::class
                        );
                        $coSource = new ConfigurationOrganisme();
                        $coSource->setOrganisme('organismeSource');
                        $coSource->setNumeroProjetAchat('1');
                        $coSource->setEspaceCollaboratif('0');
                        $coSource->setDonneesComplementaires('0');
                        $marchePublieRepository->expects($this->any())
                            ->method('getByAcronyme')
                            ->willReturn($coSource);

                        return $marchePublieRepository;
                    }
                }
            ));

        $configurationOrganismeService = new ConfigurationOrganismeService($container, $em, $serializer);
        $configurationOrganismeService->copierConfigurationOrganisme(
            'organismeSource',
            'organismeCible'
        );
    }

    public function testModuleIsEnabledIsTrue()
    {
        $container = $this->createMock(ContainerInterface::class);

        $organisme = new Organisme();

        $configurationOrganisme = new ConfigurationOrganisme();
        $configurationOrganisme->setModeApplet(1);

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository
            ->expects($this->any())
            ->method('findOneBy')
            ->with(['organisme' => $organisme])
            ->willReturn($configurationOrganisme);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($configurationOrganismeRepository);

        $serializer = $this->createMock(SerializerInterface::class);

        $configurationOrganismeService = new ConfigurationOrganismeService($container, $entityManager, $serializer);

        $result = $configurationOrganismeService->moduleIsEnabled($organisme, 'ModeApplet');
        $this->assertTrue($result);
    }

    public function testModuleIsEnabledIsFalse()
    {
        $container = $this->createMock(ContainerInterface::class);

        $organisme = new Organisme();

        $configurationOrganisme = null;

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository
            ->expects($this->any())
            ->method('findOneBy')
            ->with(['organisme' => $organisme])
            ->willReturn($configurationOrganisme);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($configurationOrganismeRepository);

        $serializer = $this->createMock(SerializerInterface::class);

        $configurationOrganismeService = new ConfigurationOrganismeService($container, $entityManager, $serializer);

        $result = $configurationOrganismeService->moduleIsEnabled($organisme, 'ModeApplet');
        $this->assertFalse($result);
    }

    public function testIsModuleDisabledForOneOrganisme(): void
    {
        $configurationOrganisme = new ConfigurationOrganisme();

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['ModeApplet' => false])
            ->willReturn($configurationOrganisme);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($configurationOrganismeRepository);

        $configurationOrganismeService = new ConfigurationOrganismeService(
            $this->createMock(ContainerInterface::class),
            $entityManager,
            $this->createMock(SerializerInterface::class)
        );

        $result = $configurationOrganismeService->isModuleDisabledForOneOrganisme('ModeApplet');

        $this->assertTrue($result);
    }

    public function testIsModuleEnabledForOneOrganisme(): void
    {
        $configurationOrganisme = new ConfigurationOrganisme();

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository
            ->expects($this->exactly(2))
            ->method('findOneBy')
            ->with(['ModeApplet' => true])
            ->willReturnOnConsecutiveCalls($configurationOrganisme, null);

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturn($configurationOrganismeRepository);

        $configurationOrganismeService = new ConfigurationOrganismeService(
            $this->createMock(ContainerInterface::class),
            $entityManager,
            $this->createMock(SerializerInterface::class)
        );

        $result = $configurationOrganismeService->isModuleEnabledForOneOrganisme('ModeApplet');
        $this->assertTrue($result);

        $result = $configurationOrganismeService->isModuleEnabledForOneOrganisme('ModeApplet');
        $this->assertFalse($result);
    }
}
