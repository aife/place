<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Version;

use App\Kernel;
use App\Service\Version\VersionService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class VersionServiceTest extends TestCase
{
    public function testGetApplicationVersion(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $kernel = $this->createMock(Kernel::class);

        $versionService = new VersionService($parameterBag, $kernel);

        $versionDirPath = __DIR__ . '/../../data/version/Kernel';

        $kernel
            ->method('getProjectDir')
            ->willReturn($versionDirPath)
        ;

        self::assertSame(
            $versionService->getApplicationVersion(),
            'testVersionNumber'
        )
        ;
    }

    public function testGetApplicationVersionDate(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $kernel = $this->createMock(Kernel::class);

        $versionService = new VersionService($parameterBag, $kernel);

        $versionDirPath = __DIR__ . '/../../data/version/Kernel';

        $kernel
            ->method('getProjectDir')
            ->willReturn($versionDirPath)
        ;

        self::assertSame(
            str_replace("\n", "", $versionService->getApplicationVersionDate()),
            'testVersionDate'
        )
        ;
    }
}
