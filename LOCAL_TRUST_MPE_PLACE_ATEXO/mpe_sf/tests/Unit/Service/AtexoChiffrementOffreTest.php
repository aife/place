<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\CertificatChiffrement;
use App\Entity\Consultation;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Repository\CertificatChiffrementRepository;
use App\Repository\ConsultationRepository;
use App\Service\AtexoChiffrementOffre;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\Chiffrement;
use AtexoCrypto\Dto\InfosHorodatage;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AtexoChiffrementOffreTest extends TestCase
{
    /** @var AtexoChiffrementOffre */
    private $serviceChiffrement;

    /** @var MockObject */
    private $em;

    /** @var AtexoCrypto */
    private $crypto;

    private ParameterBagInterface $parameterBag;

    public function setUp(): void
    {
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $this->crypto = $this->createMock(AtexoCrypto::class);
        $this->serviceChiffrement = new AtexoChiffrementOffre($this->parameterBag, $logger, $this->crypto, $this->em);
    }

    public function testAucunChiffrement()
    {
        $offre = new Offre();
        $offre->setStatutOffres(10);
        $return = $this->serviceChiffrement->demanderChiffrementOffre($offre);
        $this->assertEquals(false, $return);
    }

    public function testChiffrement()
    {
        // Certificat
        $certificat = new CertificatChiffrement();
        $certificatRepo = $this->createMock(CertificatChiffrementRepository::class);
        $certificatRepo->expects($this->any())
            ->method('findCertificats')
            ->willReturn($certificat);

        // Consultation
        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setOrganisme((new Organisme())->setAcronyme('acronyme'));
        $consultation->setChiffrementOffre("chiffrement_offre");

        $consultationRepo = $this->createMock(ConsultationRepository::class);
        $consultationRepo->expects($this->any())
            ->method('find')
            ->willReturn($consultation);

        // Repositories
        $this->em
            ->method('getRepository')
            ->will(
                $this->returnCallback(
                    function ($entityName) use ($certificatRepo, $consultationRepo) {
                        if ($entityName === Consultation::class) {
                            return $consultationRepo;
                        } elseif ($entityName === CertificatChiffrement::class) {
                            return $certificatRepo;
                        }

                        return null;
                    }
                )
            );

        $offre = new Offre();
        $offre->setConsultationId(1);
        $offre->setStatutOffres('10');

        $this->parameterBag
            ->method('get')
            ->withConsecutive(
                ['STATUT_ENV_EN_ATTENTE_CHIFFREMENT'],
                ['STATUT_ENV_CHIFFREMENT_EN_COURS'],
                ['url_mpe_ws'],
                ['URL_CRYPTO'],
                ['BASE_ROOT_DIR'],
                ['login_serveur_crypto'],
                ['mp_serveur_crypto'],
                ['FONCTION_CHIFFREMENT'],
                ['ETAT_CHIFFREMENT_OK'],
            )
            ->willReturnOnConsecutiveCalls(
                '10',
                '8',
                'url_mpe_ws/',
                'URL_CRYPTO',
                'BASE_ROOT_DIR/files/',
                'login_serveur_crypto',
                'mp_serveur_crypto',
                'FONCTION_CHIFFREMENT',
                '1',
            )
        ;

        // Crypto
        $this->crypto
            ->expects(self::once())
            ->method('demandeChiffrement')
            ->willReturn(new InfosHorodatage())
        ;

        self::assertTrue($this->serviceChiffrement->demanderChiffrementOffre($offre));
    }
}
