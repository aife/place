<?php

namespace Tests\Unit\Service;

use Generator;
use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\ModificationContrat\Authorization;
use App\Service\ModificationContrat\ModificationContratService;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\ModificationContrat;
use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Tests\Unit\AtexoTestCase;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validation;
use Symfony\Contracts\Translation\TranslatorInterface;

class ModificationContratServiceTest extends AtexoTestCase
{
    /**
     * Test insert modification contrat.
     *
     * @dataProvider nodeProvider
     */
    public function testInsertModificationContrat($node, $entreprise, $etablissement)
    {
        $objectModification = $node["objetModification"];
        $node['objetModification'] =  $objectModification;
        $configurationPlateforme = new ConfigurationPlateforme();
        $service = $this->getService($entreprise, $etablissement, $configurationPlateforme);
        $modifications = [new ModificationContrat(), new ModificationContrat()];
        $agent = new Agent();
        $contrat = new ContratTitulaire();

        $nodeTitulaire = $service->getNodeTitulaire($node);
        $typeIdentifiant = $service->getTypeIdentifiant($nodeTitulaire);
        $siret = $service->getSiret($nodeTitulaire);
        $denominationSociale = $service->getDenominationSociale($nodeTitulaire);
        $mockParameterBagInterface = $this->createMock(ParameterBagInterface::class);

        $modificationContrat = $service->insertModificationContrat(
            $node,
            $modifications,
            $agent,
            $contrat,
            $siret,
            $typeIdentifiant,
            $denominationSociale,
            $mockParameterBagInterface,
            $entreprise,
            $etablissement,
        );
        $this->assertEquals($objectModification, $modificationContrat->getObjetModification());
        $this->assertEquals(count($modifications) + 1, $modificationContrat->getNumOrdre());
    }

    /**
     * @return Generator
     */
    public function nodeProvider()
    {
        $node = [
            'objetModification' => 'Test objet modification',
            'idContrat' => 5438,
            'idAgent' => 11362,
            'dateSignatureModification' => '2020-10-27T13:24:06.013+01:00',
            'datePublicationDonneesModification' => '2020-10-27T13:24:06.013+01:00',
            'dureeMois' => 100,
            'montant' => 1000.00,
            'titulaires' => [
                'titulaire' => [
                    'typeIdentifiant' => 'SIRET',
                    'id' => '44090956200033',
                    'denominationSociale' => 'Test Dénomination sociale',
                ],
            ],
        ];
        $entreprise = null;
        $etablissement = null;
        yield [$node, $entreprise, $etablissement];

        $entreprise = new Entreprise();
        $entreprise->setId(1);
        $etablissement = new Etablissement();
        $etablissement->setIdEtablissement(1);
        $configurationPlateforme = new ConfigurationPlateforme();
        yield [$node, $entreprise, $etablissement, $configurationPlateforme];
    }

    /**
     * Test SIRET invalide.
     */
    public function testGetEntepriseInvalidSiret()
    {
        $siret = '6200033';
        $entreprise = new Entreprise();
        $expected = new Etablissement();
        $configurationPlateforme = new ConfigurationPlateforme();
        $service = $this->getService($entreprise, $expected, $configurationPlateforme);
        $this->expectException(ValidatorException::class);
        $service->getEntreprise($siret, 'SIRET');
    }

    /**
     * Test recupération etablissement.
     */
    public function testGetEtablissementValidSiret()
    {
        $request = new Request();
        $request->request->set('idAgent', 1234);
        $entreprise = new Entreprise();
        $expected = new Etablissement();
        $configurationPlateforme = new ConfigurationPlateforme();
        $service = $this->getService($entreprise, $expected, $configurationPlateforme);
        $siret = '44090956200033';
        $actual = $service->getEtablissement($siret, $entreprise);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return ModificationContratService
     */
    public function getService($entreprise, $etablissement, $configurationPlateforme)
    {
        $em = $this->getObjectManager($entreprise, $etablissement, $configurationPlateforme);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->any())->method('trans')->willReturn('a trans');
        $validator = Validation::createValidatorBuilder()->enableAnnotationMapping()->getValidator();
        $atexoEntreprise = $this->createMock(AtexoEntreprise::class);
        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $authorization = $this->createMock(Authorization::class);
        $mockParameterBagInterface = $this->createMock(ParameterBagInterface::class);

        return new ModificationContratService(
            $em,
            $validator,
            $atexoEntreprise,
            $atexoEtablissement,
            $translatorMock,
            $authorization,
            $mockParameterBagInterface
        );
    }

    /**
     * @param $entreprise
     *
     * @return MockObject
     */
    protected function getObjectManager($entreprise, $etablissement, $configurationPlateforme)
    {
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) use ($entreprise, $etablissement, $configurationPlateforme) {
                    if ($entityName === Entreprise::class) {
                        $repository = $this->createMock(EntrepriseRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn($entreprise);

                        return $repository;
                    }
                    if (Etablissement::class === $entityName) {
                        $repository = $this->createMock(EtablissementRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn($etablissement);

                        return $repository;
                    }
                    if ($entityName === ConfigurationPlateforme::class) {
                        $repository = $this->createMock(ConfigurationPlateformeRepository::class);
                        $repository->expects($this->any())
                            ->method('getConfigurationPlateforme')
                            ->willReturn($configurationPlateforme);
                        return $repository;
                    }
                }
            ));

        return $objectManager;
    }
}
