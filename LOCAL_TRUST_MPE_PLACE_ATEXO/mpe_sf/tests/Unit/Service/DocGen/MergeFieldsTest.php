<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DocGen;

use App\Service\DonneeComplementaire\DonneeComplementaireService;
use ReflectionClass;
use App\Entity\Consultation;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Repository\OrganismeRepository;
use App\Service\DocGen\MergeFields;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\Translation\TranslatorInterface;

class MergeFieldsTest extends TestCase
{
    private $argument;

    private $organisme;

    private const ORGANISME_DATA = [
        'acronyme'  => 'Test',
        'address'   => '1 rue de Paris',
        'city'      => 'Paris',
        'zipCode'   => '75000',
        'email'     => 'paris@paris.com',
        'phone'     => '0102030405',
        'fax'       => '0902030405',
    ];


    public function setUp(): void
    {
        $organisme = new Organisme();

        $organisme->setAcronyme(self::ORGANISME_DATA['acronyme']);
        $organisme->setAdresse(self::ORGANISME_DATA['address']);
        $organisme->setVille(self::ORGANISME_DATA['city']);
        $organisme->setCp(self::ORGANISME_DATA['zipCode']);
        $organisme->setEmail(self::ORGANISME_DATA['email']);
        $organisme->setTel(self::ORGANISME_DATA['phone']);
        $organisme->setTelecopie(self::ORGANISME_DATA['fax']);


        $repo = $this->createMock(OrganismeRepository::class);
        $repo->method('findOneBy')->willReturn($organisme);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')->willReturn($repo);

        $this->organisme = $organisme;

        $this->argument = new MergeFields(
            $em,
            $this->createMock(\App\Service\DonneeComplementaireService::class),
            $this->createMock(DonneeComplementaireService::class),
            $this->createMock(TranslatorInterface::class)
        );
    }

    public function testGetArgumentWithLot(): void
    {
        $class = new ReflectionClass(MergeFields::class);

        $method = $class->getMethod('getArgument');
        $method->setAccessible(true);

        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $lots = [];
        $lots[] = [
            'intitule' => 'Lot-40',
            'numero' => 40,
        ];

        $data = $method->invoke($this->argument, $consultation, $lots);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertNotEmpty($data[0]['value']['lot']);
        $this->assertEquals(self::ORGANISME_DATA['address'], $data[0]['value']['organisme']['rue']);
        $this->assertEquals(self::ORGANISME_DATA['city'], $data[0]['value']['organisme']['ville']);
        $this->assertEquals(self::ORGANISME_DATA['zipCode'], $data[0]['value']['organisme']['codePostal']);
        $this->assertEquals(self::ORGANISME_DATA['email'], $data[0]['value']['organisme']['email']);
        $this->assertEquals(self::ORGANISME_DATA['phone'], $data[0]['value']['organisme']['telephone']);
        $this->assertEquals(self::ORGANISME_DATA['fax'], $data[0]['value']['organisme']['telecopie']);
    }

    public function testGetArgumentWithOutLot(): void
    {
        $class = new ReflectionClass(MergeFields::class);

        $method = $class->getMethod('getArgument');
        $method->setAccessible(true);

        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $data = $method->invoke($this->argument, $consultation);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertEmpty($data[0]['value']['lot']);
    }

    public function testGetArgumentWithEntreprise(): void
    {
        $class = new ReflectionClass(MergeFields::class);

        $method = $class->getMethod('getArgument');
        $method->setAccessible(true);

        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $etablissemnt = new Etablissement();
        $etablissemnt->setVille('Paris');

        $entreprises['contact'] =  [
            'email' => 'ATC',
            'nom' => 'Dubois',
            'prenom' => 'Flo',
            'telecopie' => '',
            'telephone' => '',
        ];

        $data = $method->invoke($this->argument, $consultation, [], $entreprises);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[1]['value']);
    }
    public function testTableauComparatifAnalyseFinanciereWithLot(): void
    {
        $consultation = new Consultation();
        $lot = new Lot();

        $lot->setLot(30);
        $lot->setDescription('Lot -30');
        $lot->setOrganisme('pmi-min-1');
        $lot->setIdDonneeComplementaire(45);
        $lot->setDecision('Lot 1');

        $donneeComplementaire = new DonneeComplementaire();
        $criteresAttribution = new CritereAttribution();

        $criteresAttribution->setEnonce('critere 1');
        $criteresAttribution->setPonderation('100');

        $criteresAttribution->setSousCriteres(new ArrayCollection([]));

        $donneeComplementaire->setCriteresAttribution(new ArrayCollection([$criteresAttribution]));

        $lot->setDonneeComplementaire($donneeComplementaire);

        $consultation->addLot($lot)
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $data = $this->argument->tableauComparatifAnalyseFinanciere($consultation);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertNotEmpty($data[0]['value']['lot']);
    }

    public function testTableauComparatifAnalyseFinanciereWithOutLot(): void
    {
        $consultation = new Consultation();

        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());


        $data = $this->argument->tableauComparatifAnalyseFinanciere($consultation);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertEmpty($data[0]['value']['lot']);
    }

    public function testPieceGenereeModeleWithLot(): void
    {
        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $lot = new Lot();
        $lot->setLot(40);
        $lot->setDescription('Lot-40');

        $data = $this->argument->pieceGenereeModele($consultation, $lot, null);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertNotEmpty($data[0]['value']['lot']);
    }

    public function testPieceGenereeModeleWithOutLot(): void
    {
        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());
        $data = $this->argument->pieceGenereeModele($consultation, null, null);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[0]['value']);
        $this->assertEmpty($data[0]['value']['lot']);
    }

    public function testPieceGenereeModeleWithEntreprise(): void
    {
        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $etablissemnt = new Etablissement();
        $etablissemnt->setVille('Paris');

        $entreprise = new Entreprise();
        $entreprise->setNom('ATEXO');
        $entreprise->addEtablissement($etablissemnt);

        $data = $this->argument->pieceGenereeModele($consultation, null, $entreprise);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($data[1]['value']);
        $this->assertCount(6, $data[1]['value'][0]);
    }

    public function testPieceGenereeModeleWithOutEntreprise(): void
    {
        $consultation = new Consultation();
        $consultation->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime());
        $consultation->setOrganisme($this->organisme);
        $consultation->setDonneeComplementaire(new DonneeComplementaire());

        $data = $this->argument->pieceGenereeModele($consultation, null, null);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertEmpty($data[1]['value']);
    }

    public function testGetDataEntreprise()
    {
        $entreprise = new Entreprise();

        $entreprise->setNom('ATC');
        $entreprise->setEmail('atc@gmail.com');

        $class = new ReflectionClass(MergeFields::class);

        $method = $class->getMethod('getDataEntreprise');
        $method->setAccessible(true);

        $data = $method->invoke($this->argument, $entreprise, new Consultation(), null);

        $this->assertCount(1, $data);
        $this->assertCount(6, $data[0]);
        $this->assertIsString($data[0]['contact']['nom']);
        $this->assertIsString($data[0]['contact']['prenom']);
        $this->assertIsString($data[0]['contact']['telecopie']);
        $this->assertIsString($data[0]['contact']['telephone']);
        $this->assertIsString($data[0]['contact']['email']);
    }

    public function testPieceGenereeModeleWithLotDonneeComplementaire(): void
    {
        $em = $this->createMock(EntityManager::class);
        $donneeComplementaireService = $this->createMock(\App\Service\DonneeComplementaireService::class);
        $complementaireService = $this->createMock(DonneeComplementaireService::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);

        $donneeComplementaire = new DonneeComplementaire();
        $consultation = new Consultation();
        $consultation
            ->setObjet('Obj-40')
            ->setReferenceUtilisateur('Ref-40')
            ->setDatefin(new DateTime())
            ->setOrganisme($this->organisme)
            ->setDonneeComplementaire($donneeComplementaire)
        ;

        $donneeComplementaireLot = (new DonneeComplementaire())
            ->setIdCritereAttribution(2)
            ->setVariantesAutorisees(true)
            ->setVarianteExigee(false)
            ->setVariantesTechniquesObligatoires(true)
            ->setVariantesTechniquesDescription('variantesTechniquesDescription')
            ->setNombreReconductions(2)
            ->setModalitesReconduction('modalitesReconduction')
        ;

        $lot = new class () extends Lot {
            public function getId()
            {
                return 42;
            }
        };
        $lot
            ->setLot(40)
            ->setDescription('Lot-40')
            ->setDonneeComplementaire($donneeComplementaireLot)
        ;

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($this->organisme)
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($organismeRepository)
        ;

        $donneeComplementaireService
            ->expects(self::exactly(2))
            ->method('getDureeMarche')
            ->withConsecutive([$donneeComplementaireLot], [$donneeComplementaire])
            ->willReturnOnConsecutiveCalls(
                [
                    "dureeValue" => 3,
                    "dureeType" => "MONTH",
                    "descriptionLibre" => "descriptionLibre",
                ],
                []
            )
        ;

        $donneeComplementaireService
            ->expects(self::exactly(2))
            ->method('getLabelCCAGReference')
            ->withConsecutive([$donneeComplementaireLot], [$donneeComplementaire])
            ->willReturnOnConsecutiveCalls("CCAG-FCS", "")
        ;

        $criteres = [
            'typeCriteriaAttribution' => 'LIBELLE_CRITERE_ATTRIBUTION_PONDERATION',
            'listCriteriaAttribution' => [
                [
                    'enonce' => 'critere',
                    'ordre' => 1,
                    'ponderation' => 100,
                    'sousCritereAttribution' => [
                        [
                            'enonce' => 'sousCritere1',
                            'ponderation' => 50,
                        ],
                        [
                            'enonce' => 'sousCritere2',
                            'ponderation' => 50,
                        ],
                    ],
                ],
            ],
        ];
        $complementaireService
            ->expects(self::once())
            ->method('getLotCriteriaAttribution')
            ->with($lot)
            ->willReturn($criteres)
        ;

        $mergeFields = new MergeFields(
            $em,
            $donneeComplementaireService,
            $complementaireService,
            $translatorMock
        );

        $data = $mergeFields->pieceGenereeModele($consultation, $lot, null);

        $expectedResult =  [
            "id" => 42,
            "intitule" => "Lot-40",
            "numero" => 40,
            "donneesComplementaires" => [
                "dureeMarche" => [
                    "dureeValue" => 3,
                    "dureeType" => "MONTH",
                    "descriptionLibre" => "descriptionLibre",
                ],
                "ccag" => "CCAG-FCS",
                "varianteAutorisee" => true,
                "varianteExigee" => false,
                "prestationsSupplementaires" => [
                    "enabled" => true,
                    "description" => 'variantesTechniquesDescription',
                ],
                "reconduction" => [
                    "nombre" => 2,
                    "modalite" => "modalitesReconduction",
                ],
                "criteres" => [
                    "typeCriteriaAttribution" => "LIBELLE_CRITERE_ATTRIBUTION_PONDERATION",
                    "listCriteriaAttribution" => [
                        [
                            "enonce" => "critere",
                            "ordre" => 1,
                            "ponderation" => 100,
                            "sousCritereAttribution" => [
                                [
                                    "enonce" => "sousCritere1",
                                    "ponderation" => 50,
                                ],
                                [
                                    "enonce" => "sousCritere2",
                                    "ponderation" => 50,
                                ],
                            ],
                        ],
                    ],
                ],
                'tranches' => null,
                'formePrix' => null,
            ],
        ];

        $value = $data[0]['value'];

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
        $this->assertNotEmpty($value);
        $this->assertNotEmpty($value['lot']);
        $this->assertSame($value['lot'], $expectedResult);
    }
}
