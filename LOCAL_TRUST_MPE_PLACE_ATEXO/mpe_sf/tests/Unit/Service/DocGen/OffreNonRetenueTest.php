<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DocGen;

use ReflectionClass;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Lot;
use App\Service\DocGen\AnalyseOffre;
use App\Service\DocGen\DocumentGenere;
use App\Service\DocGen\MergeFields;
use App\Service\DocGen\OffreNonRetenue;
use App\Service\DocGen\Template;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OffreNonRetenueTest extends TestCase
{
    /**
     * @var OffreNonRetenue
     */
    private $mockOffreNonRetenue;

    public function setUp(): void
    {
        $this->mockOffreNonRetenue = new OffreNonRetenue(
            $this->createMock(AnalyseOffre::class),
            $this->createMock(DocumentGenere::class),
            $this->createMock(Template::class),
            $this->createMock(MergeFields::class)
        );
    }

    public function testIsConsultationAlloti()
    {
        $consultation = new Consultation();
        $consultation->setAlloti('0');

        $class = new ReflectionClass(OffreNonRetenue::class);

        $method = $class->getMethod('isConsultationAlloti');
        $method->setAccessible(true);

        $response = $method->invoke($this->mockOffreNonRetenue, $consultation);

        $this->assertFalse($response);

        $consultation->setAlloti('1');

        $response = $method->invoke($this->mockOffreNonRetenue, $consultation);

        $this->assertTrue($response);
    }

    public function testIsDocumentNameValid()
    {
        $class = new ReflectionClass(OffreNonRetenue::class);

        $method = $class->getMethod('isDocumentNameValid');
        $method->setAccessible(true);

        $response = $method->invoke($this->mockOffreNonRetenue, 'DOC13');

        $this->assertFalse($response);

        $response = $method->invoke($this->mockOffreNonRetenue, 'NOTI3.docx');

        $this->assertTrue($response);
    }

    /**
     * @group test
     */
    public function testGetDataAnalyseOffreForConsultationNotAlloti()
    {
        $entreprise = new Entreprise();

        $entreprise->setNom('ATC');

        $data[0] = [
            'key' => 'consultation',
            'value' => [
                'organisme' => [
                'denomination' => 'MIN-DEMO - Ministere 0 de démonstration'
                ],
                'directionService' => 'MIN-DEMO / DE - Direction de l\'eau',
                'objet' => 'consultation redac activé + attribution',
                'reference' => 'Redac_Attribution',
                'lot' => []
            ]
        ];

        $data[1] = [
            'key' => 'candidat',
            'value' => [
                0 => [
                'contact' =>  [
                'email' => null,
                  'nom' => '',
                  'prenom' => '',
                  'telecopie' => null,
                  'telephone' => null
                ],
                'entreprise' =>  [
                'rue' => '231 RUE DU MARCHE ST HONORE',
                  'codePostal' => '75001',
                  'ville' => 'PARIS 1',
                  'pays' => 'France',
                ],
                'etablissement' =>  [
                'rue' => '231 RUE DU MARCHE ST HONORE',
                  'siret' => '44090956200033',
                  'pays' => 'France',
                  'codePostal' => '75001',
                  'ville' => 'PARIS 1',
                ],
                'raisonSociale' => 'ATEXO',
                'role' => 'Titulaire',
                ]
            ]
        ];

        $data[2] = [
            'key' => 'attributaire',
            'value' => [
                'raisonSocial' => 'à compléter'
            ]
        ];

        $data[3] = [
            'key' => 'soumissionnaire',
            'value' =>  [
                'classementString' => 'à compléter',
                'noteGlobale' => 'à compléter',
                'noteGlobaleAttributaire' => 'à compléter',
            ]
        ];

        $values = [
            'reference' => 'Redac_Attribution',
          'intitule' => 'consultation redac activé',
          'objet' => null,
          'lots' => null,
          'entrepriseMieuxDisante' => '',
          'status' => 'En cours',
          'soumissionnaires' =>  [
                0 => [
                'raisonSocial' => 'ATC',
              'siret' => '44090956200033',
              'criteres' => [
                0 => [
                'maximumNote' => 50,
                  'description' => 'critere1',
                  'evaluationNote' => 0,
                  'evaluationNoteAttributaire' => 0,
                  'commentaire' => null,
                  'sousCriteres' => null,
                ],
                1 => [
                'maximumNote' => 50,
                  'description' => 'critere2',
                  'evaluationNote' => 0,
                  'evaluationNoteAttributaire' => 0,
                  'commentaire' => null,
                  'sousCriteres' => null,
                ]
                ],
                'classement' => 0,
                'classementString' => '0eme',
                'noteGlobale' => 0,
                'noteGlobaleAttributaire' => 0
                ]
            ],
            'attributaire' => null
        ];

        $class = new ReflectionClass(OffreNonRetenue::class);

        $method = $class->getMethod('getDataAnalyseOffreForConsultationNotAlloti');
        $method->setAccessible(true);

        $response = $method->invoke($this->mockOffreNonRetenue, $entreprise, $values, $data);

        $this->assertCount(4, $response);
        $this->assertEquals($data[2], $response[2]);
        $this->assertEquals($data[3], $response[3]);

        $values = [
            'reference' => 'Redac_Attribution',
            'intitule' => 'consultation redac activé',
            'objet' => null,
            'lots' => null,
            'entrepriseMieuxDisante' => '',
            'status' => 'Terminé',
            'soumissionnaires' =>  [
                0 => [
                    'raisonSocial' => 'ATC',
                    'siret' => '44090956200033',
                    'criteres' => [
                        0 => [
                            'maximumNote' => 50,
                            'description' => 'critere1',
                            'evaluationNote' => 0,
                            'evaluationNoteAttributaire' => 0,
                            'commentaire' => null,
                            'sousCriteres' => null,
                        ],
                        1 => [
                            'maximumNote' => 50,
                            'description' => 'critere2',
                            'evaluationNote' => 0,
                            'evaluationNoteAttributaire' => 0,
                            'commentaire' => null,
                            'sousCriteres' => null,
                        ]
                    ],
                    'classement' => 0,
                    'classementString' => '0eme',
                    'noteGlobale' => 0,
                    'noteGlobaleAttributaire' => 0
                ]
            ],
            'attributaire' => null
        ];

        $response = $method->invoke($this->mockOffreNonRetenue, $entreprise, $values, $data);

        $this->assertCount(4, $response);
        $this->assertEquals($data[2], $response[2]);
        $this->assertNotEquals($data[3], $response[3]);

        $data[3] = [
            'key' => 'soumissionnaire',
            'value' => [
                'raisonSocial' => 'ATC',
                'siret' => '44090956200033',
                'criteres' => [
                    0 => [
                        'maximumNote' => 50,
                        'description' => 'critere1',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ],
                    1 => [
                        'maximumNote' => 50,
                        'description' => 'critere2',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ],
                ],
                'classement' => 0,
                'classementString' => '0eme',
                'noteGlobale' => 0,
                'noteGlobaleAttributaire' => 0,
            ]
        ];
        $this->assertEquals($data[3], $response[3]);
    }
    public function testGetDataAnalyseOffreForConsultationAlloti()
    {
        $data[0] = [
            'key' => 'consultation',
            'value' => [
                'organisme' => [
                    'denomination' => 'MIN-DEMO - Ministere 0 de démonstration'
                ],
                'directionService' => 'MIN-DEMO / DE - Direction de l\'eau',
                'objet' => 'consultation redac activé + attribution',
                'reference' => 'Redac_Attribution',
                'lot' => [
                    [
                        'intitule' => 'Lot100',
                        'numero' => 100
                    ]
                ]
            ]
        ];

        $data[1] = [
            'key' => 'candidat',
            'value' => [
                0 => [
                    'contact' =>  [
                        'email' => null,
                        'nom' => '',
                        'prenom' => '',
                        'telecopie' => null,
                        'telephone' => null
                    ],
                    'entreprise' =>  [
                        'rue' => '231 RUE DU MARCHE ST HONORE',
                        'codePostal' => '75001',
                        'ville' => 'PARIS 1',
                        'pays' => 'France',
                    ],
                    'etablissement' =>  [
                        'rue' => '231 RUE DU MARCHE ST HONORE',
                        'siret' => '44090956200033',
                        'pays' => 'France',
                        'codePostal' => '75001',
                        'ville' => 'PARIS 1',
                    ],
                    'raisonSociale' => 'ATEXO',
                    'role' => 'Titulaire',
                ]
            ]
        ];

        $data[2] = [
            'key' => 'attributaire',
            'value' => [
                'raisonSocial' => 'à compléter'
            ]
        ];

        $data[3] = [
            'key' => 'soumissionnaire',
            'value' =>  [
                'classementString' => 'à compléter',
                'noteGlobale' => 'à compléter',
                'noteGlobaleAttributaire' => 'à compléter',
            ]
        ];

        $values = [
            'reference' => 'Redac_Attribution',
            'intitule' => 'consultation redac activé',
            'objet' => null,
            'lots' => [
                0 => [
                    'numero' => 100,
                    'intitule' => 'Lot100',
                    'entrepriseMieuxDisante' => '',
                    'status' => 'En cours',
                    'soumissionnaires' => [
                    0 => [
                    'raisonSocial' => 'ATEXO',
                      'siret' => '44090956200033',
                      'criteres' => [
                    0 => [
                    'maximumNote' => 50,
                          'description' => 'Je teste 2',
                          'evaluationNote' => 0,
                          'evaluationNoteAttributaire' => 0,
                          'commentaire' => null,
                          'sousCriteres' => null,
                        ],
                        1 => [
                    'maximumNote' => 50,
                          'description' => 'Je teste 1',
                          'evaluationNote' => 0,
                          'evaluationNoteAttributaire' => 0,
                          'commentaire' => null,
                          'sousCriteres' => null,
                        ]
                      ],
                      'classement' => 0,
                      'classementString' => '0eme',
                      'noteGlobale' => 0,
                      'noteGlobaleAttributaire' => 0,
                    ],
                    ],
                    'attributaire' => null,
                ],
                1 =>  [
                    'numero' => 101,
                    'intitule' => 'Lot101',
                    'entrepriseMieuxDisante' => '',
                    'status' => 'En cours',
                    'soumissionnaires' => [
                        0 => [
                            'raisonSocial' => 'ATEXO',
                            'siret' => '44090956200033',
                            'criteres' => [
                                0 => [
                                    'maximumNote' => 100,
                                    'description' => 'Chef',
                                    'evaluationNote' => 0,
                                    'evaluationNoteAttributaire' => 0,
                                    'commentaire' => null,
                                    'sousCriteres' => null,
                                ]
                            ],
                            'classement' => 0,
                            'classementString' => '0eme',
                            'noteGlobale' => 0,
                            'noteGlobaleAttributaire' => 0,
                        ],
                    ],
                    'attributaire' => null
                ]

            ],
            'entrepriseMieuxDisante' => '',
            'status' => 'Terminé',
            'soumissionnaires' =>  [],
            'attributaire' => null
        ];

        $lot = new Lot();

        $lot->setLot(100);

        $entreprise = new Entreprise();

        $entreprise->setNom('ATEXO');

        $class = new ReflectionClass(OffreNonRetenue::class);

        $method = $class->getMethod('getDataAnalyseOffreForConsultationAlloti');
        $method->setAccessible(true);

        $response = $method->invoke($this->mockOffreNonRetenue, $lot, $entreprise, $values, $data);

        $this->assertCount(4, $response);
        $this->assertEquals($data[2], $response[2]);
        $this->assertEquals($data[3], $response[3]);
    }
    public function testGetValues()
    {
        $documentName = 'NOTI3.docx';

        $mock = $this->mockOffreNonRetenue;

        $response =  $mock->getValues($documentName, new Consultation(), null, null);

        $this->assertEquals(
            '[{"key":"attributaire","value":{"raisonSocial":"\u00e0 compl\u00e9ter"}},{"key":"soumissionnaire","value":{"classementString":"\u00e0 compl\u00e9ter","noteGlobale":"\u00e0 compl\u00e9ter","noteGlobaleAttributaire":"\u00e0 compl\u00e9ter"}}]',
            $response
        );

        $response =  $mock->getValues($documentName, new Consultation(), new Lot(), null);

        $this->assertEquals(
            '[{"key":"attributaire","value":{"raisonSocial":"\u00e0 compl\u00e9ter"}},{"key":"soumissionnaire","value":{"classementString":"\u00e0 compl\u00e9ter","noteGlobale":"\u00e0 compl\u00e9ter","noteGlobaleAttributaire":"\u00e0 compl\u00e9ter"}}]',
            $response
        );

        $documentName = 'OUV3';
        $this->expectException(FileException::class);
        $mock->getValues($documentName, new Consultation(), null, null);
    }
    
    public function testGetDataSoumissionnaire()
    {
        $entreprise = new Entreprise();

        $entreprise->setNom('ATC');
        $soumissionnaires = [
            [
                'raisonSocial' => 'ATEXO',
                'siret' => '44090956200033',
                'criteres' => [
                    0 => [
                        'maximumNote' => 50,
                        'description' => 'Je teste 2',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ],
                    1 => [
                        'maximumNote' => 50,
                        'description' => 'Je teste 1',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ]
                ],
                'classement' => 0,
                'classementString' => '0eme',
                'noteGlobale' => 0,
                'noteGlobaleAttributaire' => 0,
            ],
            [
                'raisonSocial' => 'ATC',
                'siret' => '55090956200033',
                'criteres' => [
                    0 => [
                        'maximumNote' => 50,
                        'description' => 'Je teste 4',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ],
                    1 => [
                        'maximumNote' => 50,
                        'description' => 'Je teste 5',
                        'evaluationNote' => 0,
                        'evaluationNoteAttributaire' => 0,
                        'commentaire' => null,
                        'sousCriteres' => null,
                    ]
                ],
                'classement' => 0,
                'classementString' => '0eme',
                'noteGlobale' => 0,
                'noteGlobaleAttributaire' => 0,
            ]
        ];

        $class = new ReflectionClass(OffreNonRetenue::class);

        $method = $class->getMethod('getDataSoumissionnaire');
        $method->setAccessible(true);

        $response = $method->invoke($this->mockOffreNonRetenue, $entreprise, $soumissionnaires);

        $this->assertCount(7, $response);
        $this->assertEquals('ATC', $response['raisonSocial']);
    }
}
