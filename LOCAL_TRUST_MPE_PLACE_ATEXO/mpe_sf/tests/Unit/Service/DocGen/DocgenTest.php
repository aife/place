<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DocGen;

use DateTime;
use App\Entity\PieceGenereConsultation;
use App\Service\DocGen\Docgen;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DocgenTest extends TestCase
{
    public function testTokenIsValid()
    {
        $piece = new PieceGenereConsultation();
        $piece->setToken('Cgdnd3Mtd2l6EAMyBAgAEBMyCggAEA0QBRAeEBMyCggAEA0QBRAeEBMyCggAEA0QBRAeEBMyCggAEAgQDRAeEBMyCggAEAg');

        $docgenService = new Docgen(
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(HttpClientInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(MountManager::class),
            $this->createMock(LoggerInterface::class)
        );

        $response = $docgenService->tokenIsValid($piece);
        $this->assertTrue($response);

        $piece->setUpdatedAt(new DateTime('2000-01-01'));

        $response = $docgenService->tokenIsValid($piece);
        $this->assertFalse($response);
    }
}
