<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DocGen;

use ReflectionClass;
use Exception;
use App\Entity\Consultation;
use App\Service\DocGen\AnalyseFinanciere;
use App\Service\DocGen\Docgen;
use App\Service\DocGen\DocumentGenere;
use App\Service\DocGen\MergeFields;
use App\Service\DocGen\Template;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class AnalyseFinanciereTest extends TestCase
{
    public function testGetPositions()
    {
        $class = new ReflectionClass(AnalyseFinanciere::class);

        $method = $class->getMethod('getPositions');
        $method->setAccessible(true);

        $data = $method->invoke($this->getMockAnalyseFinaciere());

        $this->assertIsArray($data);
        $this->assertCount(8, $data);
    }

    public function testGetDataPartFiles(): void
    {
        try {
            $binaryFile1 = $this->createMock(BinaryFileResponse::class);
            $binaryFile1->expects($this->once())
                ->method('getFile')
                ->willReturn(new File('var/tmp/001', false));


            $class = new ReflectionClass(AnalyseFinanciere::class);

            $method = $class->getMethod('getDataPartFiles');
            $method->setAccessible(true);

            $files = [
                $binaryFile1
            ];

            $method->invoke($this->getMockAnalyseFinaciere(), $files);
        } catch (Exception $e) {
            $this->assertEquals(2, $e->getCode());
            $this->assertEquals(
                "file_get_contents(var/tmp/001): Failed to open stream: No such file or directory",
                $e->getMessage()
            );
        }
    }

    public function testGetPieceGenereeWithFileNotExist()
    {
        $translatorMock = $this->createMock(TranslatorInterface::class);

        $translatorMock->expects($this->once())
            ->method('trans')
            ->willReturn('Le fichier n\'a pas été trouvé');

        $consultation = new Consultation();

        $binaryFile = $this->createMock(BinaryFileResponse::class);

        $files = [
            $binaryFile
        ];

        $mock = $this->getMockAnalyseFinaciere($translatorMock);

        $this->expectException(FileNotFoundException::class);
        $this->expectExceptionMessage('Le fichier n\'a pas été trouvé');
        $mock->getPieceGeneree($consultation, $files, 'tes.xlsx', 'atc');
    }

    private function getMockAnalyseFinaciere(
        TranslatorInterface $translatorMock = null,
        Template $templateMock = null
    ): AnalyseFinanciere {
        return new AnalyseFinanciere(
            $this->createMock(MergeFields::class),
            ($templateMock != null) ? $templateMock : $this->createMock(Template::class),
            ($translatorMock != null) ? $translatorMock : $this->createMock(TranslatorInterface::class),
            $this->createMock(Docgen::class),
            $this->createMock(DocumentGenere::class),
        );
    }
}
