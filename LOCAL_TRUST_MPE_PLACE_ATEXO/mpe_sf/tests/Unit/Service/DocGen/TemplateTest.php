<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\DocGen;

use App\Service\DocGen\Template;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class TemplateTest extends TestCase
{
    private $mockTemplate;

    public function setUp(): void
    {
        $this->mockTemplate = new Template(
            $this->createMock(ParameterBagInterface::class)
        );
    }

    public function testWithoutExtension()
    {
        $response = $this->mockTemplate->withoutExtension('template.xml');
        $this->assertEquals('template', $response);
    }

    public function testGetExtension()
    {
        $response = $this->mockTemplate->getExtension('template.xml');
        $this->assertEquals('.xml', $response);
    }
}
