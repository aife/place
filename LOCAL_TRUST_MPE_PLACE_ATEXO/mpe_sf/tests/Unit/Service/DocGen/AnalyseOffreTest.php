<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\DocGen;

use ReflectionClass;
use App\Entity\Consultation;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Lot;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\DocGen\AnalyseOffre;
use App\Service\DocGen\Docgen;
use App\Service\DocGen\DocumentGenere;
use App\Service\DocGen\Template;
use App\Service\LotCandidature;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AnalyseOffreTest extends TestCase
{
    private $consultation;

    public function setUp(): void
    {
        $this->consultation = new Consultation();

        $donneeComplementaire = new DonneeComplementaire();
        $criteresAttribution = new CritereAttribution();

        $criteresAttribution->setEnonce('critere 1');
        $criteresAttribution->setPonderation('100');

        $criteresAttribution->setSousCriteres(new ArrayCollection([]));

        $donneeComplementaire->setCriteresAttribution(new ArrayCollection([$criteresAttribution]));

        $this->consultation->setId(10);
        $this->consultation->setIntitule('Consultation de test');
        $this->consultation->setReferenceUtilisateur('Consultation de demo');
        $this->consultation->setDonneeComplementaire($donneeComplementaire);
    }

    public function testCriteresAttributionExists(): void
    {
        $mock = $this->getMockAnalyseOffre();

        $responseTrue = $mock->criteresAttributionExists($this->consultation);
        $this->assertTrue($responseTrue);

        $this->consultation->getDonneeComplementaire()->setCriteresAttribution(new ArrayCollection([]));

        $responseFalse = $mock->criteresAttributionExists($this->consultation);
        $this->assertFalse($responseFalse);
    }

    public function testConsultationNonAllotie()
    {
        $this->consultation->setIntitule('Consultation de test');
        $this->consultation->setReferenceUtilisateur('Consultation de test 300');

        $mock = $this->getMockAnalyseOffre();
        $response = $mock->getData($this->consultation);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('intitule', $response);
        $this->assertArrayHasKey('reference', $response);
        $this->assertArrayHasKey('criteres', $response);
        $this->assertArrayHasKey('soumissionnaires', $response);

        $this->assertNotEmpty($response['intitule']);
        $this->assertNotEmpty($response['reference']);
        $this->assertNotEmpty($response['criteres']);
    }

    public function testConsultationAllotie()
    {
        $lot = new Lot();

        $lot->setLot(30);
        $lot->setOrganisme('pmi-min-1');
        $lot->setIdDonneeComplementaire(45);
        $lot->setDecision('Lot 1');

        $donneeComplementaire = new DonneeComplementaire();
        $criteresAttribution = new CritereAttribution();

        $criteresAttribution->setEnonce('critere 1');
        $criteresAttribution->setPonderation('100');

        $criteresAttribution->setSousCriteres(new ArrayCollection([]));

        $donneeComplementaire->setCriteresAttribution(new ArrayCollection([$criteresAttribution]));

        $lot->setDonneeComplementaire($donneeComplementaire);

        $this->consultation->addLot($lot);

        $mockLotCandidature = $this->createMock(LotCandidature::class);
        $mockLotCandidature->expects($this->any())
            ->method('getRaisonSocialAndSiretForEntreprise')
            ->willReturn([
                ['raisonSocial' => 'ATEXO', 'siret' => '44090956200033'],
            ]);

        $mock = $this->getMockAnalyseOffre($mockLotCandidature);

        $response = $mock->getData($this->consultation);

        $this->assertIsArray($response);
        $this->assertArrayHasKey('intitule', $response);
        $this->assertArrayHasKey('reference', $response);
        $this->assertArrayHasKey('lots', $response);
        $this->assertArrayNotHasKey('criteres', $response);
        $this->assertArrayNotHasKey('soumissionnaires', $response);

        $this->assertNotEmpty($response['intitule']);
        $this->assertNotEmpty($response['reference']);
        $this->assertNotEmpty($response['lots']);
    }

    public function testGetFileOriginalName(): void
    {
        $mock = $this->getMockAnalyseOffre();
        $name = $mock->getFileOriginalName($this->consultation);

        $this->assertEquals('analyse-des-offres-Consultationdedemo', $name);
    }

    public function testCriteresAttributionConsultationExists(): void
    {
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('criteresAttributionConsultationExists');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(true, $output);
    }

    public function testCriteresAttributionConsultationNotExists(): void
    {
        $consultation = new Consultation();
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('criteresAttributionConsultationExists');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $consultation);

        $this->assertEquals(false, $output);
    }
    public function testCriteresAttributionLotExists(): void
    {
        $consultation = new Consultation();
        $lot = new Lot();

        $lot->setLot(30);
        $lot->setOrganisme('pmi-min-1');
        $lot->setIdDonneeComplementaire(45);
        $lot->setDecision('Lot 1');

        $donneeComplementaire = new DonneeComplementaire();
        $criteresAttribution = new CritereAttribution();

        $criteresAttribution->setEnonce('critere 1');
        $criteresAttribution->setPonderation('100');

        $criteresAttribution->setSousCriteres(new ArrayCollection([]));

        $donneeComplementaire->setCriteresAttribution(new ArrayCollection([$criteresAttribution]));

        $lot->setDonneeComplementaire($donneeComplementaire);

        $consultation->addLot($lot);

        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('criteresAttributionLotExists');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $consultation);

        $this->assertEquals(true, $output);
    }

    public function testCriteresAttributionLotNotExists(): void
    {
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('criteresAttributionLotExists');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(false, $output);
    }
    public function testIsExistCriteriasetOutBelowWithTheirWeighting()
    {
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('isExistCriteriasetOutBelowWithTheirWeighting');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(true, $output);
    }

    public function testIsExistSingleCostCriterion(): void
    {
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('isExistSingleCostCriterion');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(false, $output);

        $donneeComplementaire = new DonneeComplementaire();
        $donneeComplementaire->setIdCritereAttribution(5);
        $this->consultation->setDonneeComplementaire($donneeComplementaire);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(true, $output);
    }

    public function testIsExistSinglePriceCriterion(): void
    {
        $class = new ReflectionClass(AnalyseOffre::class);

        $method = $class->getMethod('isExistSinglePriceCriterion');
        $method->setAccessible(true);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(false, $output);

        $donneeComplementaire = new DonneeComplementaire();
        $donneeComplementaire->setIdCritereAttribution(3);
        $this->consultation->setDonneeComplementaire($donneeComplementaire);

        $output = $method->invoke($this->getMockAnalyseOffre(), $this->consultation);

        $this->assertEquals(true, $output);
    }

    private function getMockAnalyseOffre($mockLotCandidature = null)
    {
        return new AnalyseOffre(
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Docgen::class),
            $this->createMock(Template::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $mockLotCandidature ?? $this->createMock(LotCandidature::class),
            $this->createMock(DocumentGenere::class),
            $this->createMock(BlobOrganismeFileService::class),
            $this->createMock(LoggerInterface::class)
        );
    }
}
