<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Exception\MethodMustOverrideException;
use App\Exception\UnauthorizedPublicMethod;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Tests\Unit\AbstractServiceMock\MockService;
use Tests\Unit\AbstractServiceMock\MockServiceWithContext;
use Tests\Unit\AbstractServiceMock\MockServiceWithPublicFunction;

class AbstractServiceTest extends TestCase
{
    public function testNotAllowAdd()
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->expectException(UnauthorizedPublicMethod::class);
        $service = new MockServiceWithPublicFunction($httpClient, $logger, $parameterBag);
    }

    public function testGetContentWith500()
    {
        $response = new MockResponse('', ['http_code' => 500]);
        $client = new MockHttpClient($response);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $service = new MockService($client, $logger, $parameterBag);
        $result = $service->getContent('doRequest');
        $userMessage = $service->getEndUserExceptionMessage('REDAC');

        $this->assertEquals('Le service REDAC a retourné une erreur serveur.', $userMessage);
    }

    public function testGetContentWith400()
    {
        $body = function () {
            yield 'hello';
            // empty strings are turned into timeouts so that they are easy to test
            yield '';
            yield 'world';
        };
        $response = new MockResponse('Number of TV must be GTE 3.', ['http_code' => 400]);
        $client = new MockHttpClient($response);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $service = new MockService($client, $logger, $parameterBag);
        $result = $service->getContent('doRequest');
        $userMessage = $service->getEndUserExceptionMessage('REDAC');

        $this->assertEquals('Les options de la requête envoyée au service REDAC sont invalides.', $userMessage);
    }

    public function testGetContentWithTimeOut()
    {
        $body = function () {
            yield 'hello';
            // empty strings are turned into timeouts so that they are easy to test
            yield '';
            yield 'world';
        };
        $response = new MockResponse($body(), ['http_code' => 400]);
        $client = new MockHttpClient($response);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $service = new MockService($client, $logger, $parameterBag);
        $service->getContent('getTimeOut');
        $userMessage = $service->getEndUserExceptionMessage('REDAC');

        $this->assertEquals(
            'Timeout lors de l\'appel au webservice REDAC',
            $userMessage
        );
    }

    public function testGetExceptionOverride()
    {
        $response = new MockResponse('', ['http_code' => 500]);
        $client = new MockHttpClient($response);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $service = new MockServiceWithContext($client, $logger, $parameterBag);
        $this->expectErrorMessage('The function setDataXml must be overridden
             in services using getAgentContext function.');
        $this->expectException(MethodMustOverrideException::class);
        $result = $service->getContent('doRequest');
    }
}
