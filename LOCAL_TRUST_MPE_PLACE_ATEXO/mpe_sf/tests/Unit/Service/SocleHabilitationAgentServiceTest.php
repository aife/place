<?php

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Entity\SocleHabilitationAgent;
use App\Service\SocleHabilitationAgentService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocleHabilitationAgentServiceTest.
 */
class SocleHabilitationAgentServiceTest extends TestCase
{
    /**
     * Test de création des habilliations d'un admin.
     */
    public function testCreateForAdmin()
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $socleHabilitationAgentService = new SocleHabilitationAgentService($container, $em);
        $socleHabilitationAgent = $socleHabilitationAgentService->createForAdmin(new Agent());

        $this->assertTrue($socleHabilitationAgent instanceof SocleHabilitationAgent);
        $this->assertInstanceOf(Agent::class, $socleHabilitationAgent->getAgent());
        $this->assertEquals(1, $socleHabilitationAgent->getGestionAgentPoleSocle());
        $this->assertEquals(1, $socleHabilitationAgent->getDroitGestionServicesSocle());
        $this->assertEquals(1, $socleHabilitationAgent->getDroitGestionServicesSocle());
    }
}
