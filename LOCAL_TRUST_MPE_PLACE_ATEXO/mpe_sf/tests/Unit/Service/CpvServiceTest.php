<?php

namespace Tests\Unit\Service;

use App\Service\CpvService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CpvServiceTest extends TestCase
{
    /**
     * @dataProvider getCpvProvider
     *
     * @param $code
     * @param $categorie
     * @param $expected
     */
    public function testIsValidCpv($code, $categorie, $expected)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ('PRESTATION_TRAVAUX' === $params) {
                    $value = 'TRAVAUX';
                }

                if ('PRESTATION_FOURNITURES' === $params) {
                    $value = 'FOURNITURES';
                }

                if ('PRESTATION_SERVICES' === $params) {
                    $value = 'SERVICES';
                }

                return $value;
            }));

        $cpvService = new CpvService($parameterBag);

        $actualResponse = $cpvService->isValidCpv($code, $categorie);

        $this->assertEquals($expected, $actualResponse);
    }

    /**
     * @dataProvider getCodeProvider
     *
     * @param $code
     * @param $expected
     */
    public function testGetCategorieByCpv($code, $expected)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ('TYPE_PRESTATION_TRAVAUX' === $params) {
                    $value = 1;
                }

                if ('TYPE_PRESTATION_FOURNITURES' === $params) {
                    $value = 2;
                }

                if ('TYPE_PRESTATION_SERVICES' === $params) {
                    $value = 3;
                }

                return $value;
            }));

        $cpvService = new CpvService($parameterBag);

        $actualResponse = $cpvService->getCategorieByCpv($code);

        $this->assertEquals($expected, $actualResponse);
    }

    /**
     * @dataProvider getCategorieProvider
     *
     * @param $idCategorie
     * @param $expected
     */
    public function testgetCategorieLabel($idCategorie, $expected)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ('TYPE_PRESTATION_TRAVAUX' === $params) {
                    $value = 1;
                }

                if ('TYPE_PRESTATION_FOURNITURES' === $params) {
                    $value = 2;
                }

                if ('TYPE_PRESTATION_SERVICES' === $params) {
                    $value = 3;
                }

                return $value;
            }));

        $cpvService = new CpvService($parameterBag);

        $actualResponse = $cpvService->getCategorieLabel($idCategorie);

        $this->assertEquals($expected, $actualResponse);
    }

    public function getCpvProvider()
    {
        return [
            ['39121200', 'Fournitures', true],
            ['39121200', 'Travaux', false],
            ['39121200', 'Services', false],
            ['45200000', 'Fournitures', false],
            ['45200000', 'Travaux', true],
            ['45200000', 'Services', false],
            ['66518000', 'Fournitures', false],
            ['66518000', 'Travaux', false],
            ['66518000', 'Services', true],
        ];
    }

    public function getCodeProvider()
    {
        return [
            ['39121200', 2],
            ['45200000', 1],
            ['66518000', 3],
        ];
    }

    public function getCategorieProvider()
    {
        return [
            [1, 'Travaux'],
            [2, 'Fournitures'],
            [3, 'Services'],
        ];
    }
}
