<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\HistoriquePurge;
use App\Service\HistoriquePurgeService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Style\SymfonyStyle;

class HistoriquePurgeServiceTest extends TestCase
{
    public function testDisplayLogList(): void
    {
        $ioMock = $this->createMock(SymfonyStyle::class);
        $headers = ['Header 1', 'Header 2'];
        $logInfoCommand = [
            [
                ['Data 1', 'Data 2'],
                ['Data 3', 'Data 4'],
            ],
        ];

        $expectedBody = [
            'Header 1, Header 2',
            'Data 1, Data 2',
            'Data 3, Data 4',
        ];

        $ioMock
            ->expects(self::once())
            ->method('listing')
            ->with($expectedBody)
        ;

        $historiquePurgeService = new HistoriquePurgeService();
        $historiquePurgeService->displayLogList($ioMock, $headers, $logInfoCommand);
    }

    public function testNotifyResultsInConsoleEmptyLogInfoCommand(): void
    {
        $ioMock = $this->createMock(SymfonyStyle::class);
        $logInfoCommand = [];
        $nbFiles = 5;
        $dryRun = false;
        $textInit = 'files';

        $ioMock
            ->expects(self::once())
            ->method('warning')
            ->with('Aucun fichier à supprimer')
        ;

        $historiquePurgeService = new HistoriquePurgeService();
        $historiquePurgeService->notifyResultsInConsole($ioMock, $logInfoCommand, $nbFiles, $dryRun, $textInit);
    }

    public function testNotifyResultsInConsoleAllFilesDeleted(): void
    {
        $ioMock = $this->createMock(SymfonyStyle::class);
        $logInfoCommand = [
            HistoriquePurge::STATE_DELETED => [
                ['File 1'],
                ['File 2'],
                ['File 3'],
                ['File 4'],
            ],
        ];
        $nbFiles = 4;
        $dryRun = false;
        $textInit = 'files';

        $ioMock
            ->expects(self::once())
            ->method('success')
            ->with('Tous les fichiers (4) ont été supprimé avec succès')
        ;

        $historiquePurgeService = new HistoriquePurgeService();
        $historiquePurgeService->notifyResultsInConsole($ioMock, $logInfoCommand, $nbFiles, $dryRun, $textInit);
    }

    public function testNotifyResultsInConsoleWithErrors(): void
    {
        $ioMock = $this->createMock(SymfonyStyle::class);
        $logInfoCommand = [
            HistoriquePurge::STATE_INITIAL => [
                ['File 1'],
                ['File 2'],
                ['File 3'],
                ['File 4'],
            ],
            HistoriquePurge::STATE_ERROR_FILE_NOT_FOUND => [
                ['File 5'], ['File 6'],
            ],
        ];
        $nbFiles = 6;
        $dryRun = true;
        $textInit = 'files';

        $expectedWarningMessage = [
            "La command n'a pas pu purger tous les files séléctionnés (6): ",
            '- 4 files sont restés à leur état initial',
            '- 2 files ne peuvent pas être supprimé car le(s) fichier(s) n\'existent pas sur le serveur',
        ];

        $ioMock
            ->expects(self::once())
            ->method('warning')
            ->with($expectedWarningMessage)
        ;

        $historiquePurgeService = new HistoriquePurgeService();
        $historiquePurgeService->notifyResultsInConsole($ioMock, $logInfoCommand, $nbFiles, $dryRun, $textInit);
    }

    public function testNotifyResultsInConsoleWithDryRunErrors(): void
    {
        $ioMock = $this->createMock(SymfonyStyle::class);
        $logInfoCommand = [
            HistoriquePurge::STATE_INITIAL => [
                ['File 1'],
                ['File 2'],
                ['File 3'],
                ['File 4'],
            ],
            HistoriquePurge::STATE_ERROR_FILE_NOT_FOUND => [
                ['File 5'],['File 6'],
            ],
        ];
        $nbFiles = 6;
        $dryRun = false;
        $textInit = 'files';

        $expectedErrorMessage = [
            "La command n'a pas pu purger tous les files séléctionnés (6): ",
            '- 4 files sont restés à leur état initial',
            "- 2 files n'ont pas pu être supprimé car le(s) fichier(s) n'existent pas sur le serveur",
        ];

        $ioMock
            ->expects(self::once())
            ->method('error')
            ->with($expectedErrorMessage)
        ;

        $historiquePurgeService = new HistoriquePurgeService();
        $historiquePurgeService->notifyResultsInConsole($ioMock, $logInfoCommand, $nbFiles, $dryRun, $textInit);
    }
}
