<?php

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\WS\AgentTechniqueAssociation;
use App\Repository\AgentRepository;
use App\Repository\WS\AgentTechniqueAssociationRepository;
use App\Service\AgentTechniqueTokenService;
use Doctrine\ORM\EntityManager;
use Tests\Unit\AtexoTestCase;

class AgentTechniqueTokenTest extends AtexoTestCase
{
    public function testGetAgentFromToken()
    {
        $em = $this->getAtexoObjectManager();
        $agentTechniqueTokenService = new AgentTechniqueTokenService($em);
        $agentResponse = $agentTechniqueTokenService->getAgentFromToken('token');
        $this->assertEquals(new Agent(), $agentResponse, 'La fonction doit retourner un objet de type agent');
    }

    /**
     * @dataProvider dataProviderOk
     *
     * @param $organismeTechnique
     * @param $serviceTechnique
     * @param $organismeAgent
     * @param $serviceAgent
     * @param $expected
     */
    public function testIsGrantedFromAgentWsOk($organismeTechnique, $serviceTechnique, $organismeAgent, $serviceAgent)
    {
        $em = $this->createMock(EntityManager::class);
        $agentRepositoryMock = $this->createMock(AgentRepository::class);
        $agentRepositoryMock->expects($this->any())
            ->method('getArParentsId')
            ->willReturn([$serviceAgent]);

        $organisme = new Organisme();
        $organisme->setAcronyme($organismeTechnique);

        $agentTechniqueAssociationRepositoryMock = $this->createMock(AgentTechniqueAssociationRepository::class);
        $arrayAssociation = [];
        if ($organismeTechnique) {
            $assoc = new AgentTechniqueAssociation();
            $class = new \ReflectionClass($assoc);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($assoc, 1);
            $property = $class->getProperty('organisme');
            $property->setAccessible(true);
            $property->setValue($assoc, $organisme);
            $property = $class->getProperty('serviceId');
            $property->setAccessible(true);
            $property->setValue($assoc, $serviceTechnique);
            $arrayAssociation = [$assoc];
        }
        $agentTechniqueAssociationRepositoryMock->expects($this->any())
            ->method('findBy')
            ->willReturn($arrayAssociation);

        $agentTechniqueAssociationRepositoryMock->expects($this->any())
            ->method('getAssociationAgentForOrgAndServices')
            ->willReturn($arrayAssociation);

        $em->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Agent::class],
                [AgentTechniqueAssociation::class]
            )
            ->willReturnOnConsecutiveCalls(
                $agentRepositoryMock,
                $agentTechniqueAssociationRepositoryMock
            )
        ;

        $service = new Service();
        $class = new \ReflectionClass($service);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $service->setId($serviceTechnique);

        $agentTechnique = new Agent();
        $agentTechnique->setOrganisme($organisme);
        $agentTechnique->setService($service);

        $nodeFlux = ['acronymeOrganisme' => $organismeAgent, 'service' => ['id' => $serviceAgent]];

        $agentTechniqueTokenService = new AgentTechniqueTokenService($em);
        $isGranted = $agentTechniqueTokenService->isGrantedFromAgentWs($agentTechnique, $nodeFlux);

        self::assertTrue($isGranted, 'Retourne true si lagent est autorisé à accéder');
    }

    /**
     * @return array
     */
    public function dataProviderOk()
    {
        return $dates = [
            ['pmi-min-1', 0, 'pmi-min-1', 2],
            ['pmi-min-1', 2, 'pmi-min-1', 2],
        ];
    }

    /**
     * @dataProvider dataProviderKo
     *
     * @param $organismeTechnique
     * @param $serviceTechnique
     * @param $organismeAgent
     * @param $serviceAgent
     * @param $expected
     */
    public function testIsGrantedFromAgentWsKo($organismeTechnique, $serviceTechnique, $organismeAgent, $serviceAgent)
    {
        $em = $this->createMock(EntityManager::class);
        $agentRepositoryMock = $this->createMock(AgentRepository::class);
        $agentRepositoryMock->expects($this->any())
            ->method('getArParentsId')
            ->willReturn([$serviceAgent]);



        $organisme = new Organisme();
        $organisme->setAcronyme($organismeTechnique);

        $agentTechniqueAssociationRepositoryMock = $this->createMock(AgentTechniqueAssociationRepository::class);
        $arrayAssociation = [];
        if ($organismeTechnique) {
            $assoc = new AgentTechniqueAssociation();
            $class = new \ReflectionClass($assoc);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($assoc, 1);
            $property = $class->getProperty('organisme');
            $property->setAccessible(true);
            $property->setValue($assoc, $organisme);
            $property = $class->getProperty('serviceId');
            $property->setAccessible(true);
            $property->setValue($assoc, $serviceTechnique);
            $arrayAssociation = [$assoc];
        }
        $agentTechniqueAssociationRepositoryMock->expects($this->any())
            ->method('findBy')
            ->willReturn($arrayAssociation);

        $agentTechniqueAssociationRepositoryMock->expects($this->any())
            ->method('getAssociationAgentForOrgAndServices')
            ->willReturn([]);

        $em->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Agent::class],
                [AgentTechniqueAssociation::class]
            )
            ->willReturnOnConsecutiveCalls(
                $agentRepositoryMock,
                $agentTechniqueAssociationRepositoryMock
            )
        ;

        $service = new Service();
        $class = new \ReflectionClass($service);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $service->setId($serviceTechnique);

        $agentTechnique = new Agent();
        $agentTechnique->setOrganisme($organisme);
        $agentTechnique->setService($service);

        $nodeFlux = ['acronymeOrganisme' => $organismeAgent, 'service' => ['id' => $serviceAgent]];

        $agentTechniqueTokenService = new AgentTechniqueTokenService($em);
        $isGranted = $agentTechniqueTokenService->isGrantedFromAgentWs($agentTechnique, $nodeFlux);

        self::assertFalse($isGranted, 'Retourne true si lagent est autorisé à accéder');
    }

    /**
     * @return array
     */
    public function dataProviderKo()
    {
        return $dates = [
            ['pmi-min-1', 2, 'fauxorganisme', 2],
            ['pmi-min-1', 2, 'pmi-min-1', 4],
        ];
    }
}
