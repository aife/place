<?php

namespace Tests\Unit\Service\Dume;

use Generator;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Repository\BloborganismeFileRepository;
use App\Repository\ConsultationRepository;
use App\Repository\OffreRepository;
use App\Repository\TCandidatureRepository;
use App\Repository\TDumeContexteRepository;
use App\Repository\TDumeNumeroRepository;
use App\Service\Dume\DumeService;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

/**
 * Class DumeServiceTest.
 */
class DumeServiceTest extends AtexoTestCase
{
    /** @var DumeService */
    private $dumeService;

    private $hasFile = true;

    public function testGetDumeNumeroFromIdDumeContexte()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $tokenStorage = $this->createMock(TokenStorageInterface::class);
        $mountManager = $this->createMock(MountManager::class);

        $user = new Inscrit();
        $user->setEntrepriseId(10);
        $user->setIdEtablissement(40);
        $user->setId(60);
        $token = new UsernamePasswordToken($user, 'bar', 'key');

        $tokenStorage
            ->expects(self::once())
            ->method('getToken')
            ->willReturn($token)
        ;

        $parameters = $this->getParameters();
        $dumeService = new DumeService(
            $entityManager,
            $logger,
            $translatorInterface,
            $tokenStorage,
            $mountManager,
            $parameters
        );

        $dumeNumeroRepo = $this->createMock(TDumeNumeroRepository::class);
        $dumeNumeroRepo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['idDumeContexte' => 93])
            ->willReturn(new TDumeContexte())
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(TDumeNumero::class)
            ->willReturn($dumeNumeroRepo)
        ;

        self::assertInstanceOf(TDumeContexte::class, $dumeService->getDumeNumeroFromIdDumeContexte(93));
    }

    public function testGetDumeNumeroFromGroupement()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $tokenStorage = $this->createMock(TokenStorageInterface::class);
        $mountManager = $this->createMock(MountManager::class);

        $user = new Inscrit();
        $user->setEntrepriseId(10);
        $user->setIdEtablissement(40);
        $user->setId(60);
        $token = new UsernamePasswordToken($user, 'bar', 'key');

        $tokenStorage
            ->expects(self::once())
            ->method('getToken')
            ->willReturn($token)
        ;

        $parameters = $this->getParameters();
        $dumeService = new DumeService(
            $entityManager,
            $logger,
            $translatorInterface,
            $tokenStorage,
            $mountManager,
            $parameters
        );

        $dumeNumeroRepo = $this->createMock(TDumeNumeroRepository::class);
        $dumeNumeroRepo
            ->expects(self::once())
            ->method('getDumeNumeroFromGroupement')
            ->with(93, 600)
            ->willReturn([])
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(TDumeNumero::class)
            ->willReturn($dumeNumeroRepo)
        ;

        $dumeService->getDumeNumeroFromGroupement(93, 600);
    }

    public function testGetDumeContexteFromId()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $tokenStorage = $this->createMock(TokenStorageInterface::class);
        $mountManager = $this->createMock(MountManager::class);

        $user = new Inscrit();
        $user->setEntrepriseId(10);
        $user->setIdEtablissement(40);
        $user->setId(60);
        $token = new UsernamePasswordToken($user, 'bar', 'key');

        $tokenStorage
            ->expects(self::once())
            ->method('getToken')
            ->willReturn($token)
        ;

        $parameters = $this->getParameters();
        $dumeService = new DumeService(
            $entityManager,
            $logger,
            $translatorInterface,
            $tokenStorage,
            $mountManager,
            $parameters
        );

        $dumeContexte = new class extends TDumeContexte {
            public function getId()
            {
                return 93;
            }
        };

        $dumeNumeroRepo = $this->createMock(TDumeNumeroRepository::class);
        $dumeNumeroRepo
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['id' => 93])
            ->willReturn($dumeContexte)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(TDumeContexte::class)
            ->willReturn($dumeNumeroRepo)
        ;

        $dumeContexte = $dumeService->getDumeContexteFromId(93);

        self::assertEquals(93, $dumeContexte->getId(), 'Test si ID est OK');
    }

    /**
     * @dataProvider infoDumeProvider
     */
    public function testGetInfoDume($dumeDemande, $isStandard, $image, $message)
    {
        $this->init();
        $consultation = new Consultation();
        $consultation->setDumeDemande($dumeDemande);
        $dumeContexte = new TDumeContexte();
        $dumeContexte->setIsStandard($isStandard);

        $result = $this->dumeService->getInfoDume($consultation, $dumeContexte);
        $this->assertNotEmpty($result);
        $this->assertEquals($image, $result['image']);
        $this->assertEquals($message, $result['message']);
    }

    public function infoDumeProvider()
    {
        // dumeDemande , isStandard , image , message
        return [
            [0, false, 'consultation_depot/assets/images/logo-dume-publie.png', 'DUME défini par l\'acheteur'],
            [0, true, 'consultation_depot/assets/images/logo-dume-non-publie.png', 'DUME standard'],
            [1, false, 'consultation_depot/assets/images/logo-dume-publie.png', 'DUME défini par l\'acheteur'],
            [1, true, 'consultation_depot/assets/images/logo-dume-non-publie.png', 'DUME standard'],
        ];
    }

    public function testGetDumeContexteWaitingOrValidOrPublish()
    {
        $this->init();
        $consultation = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };

        $result = $this->dumeService->getDumeContexteWaitingOrValidOrPublish($consultation);

        $this->assertEquals(93, $result->getId());
    }

    public function testGetDepotDumeValid()
    {
        $this->init();
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-min-1');
        $consultation = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };
        $consultation->setOrganisme($organisme);

        $tabMultiDepot = $this->dumeService->getDepotDumeValid(
            true,
            new Entreprise(),
            '00033',
            'TVAINTRA',
            $consultation,
            null
        );

        $this->assertEquals(true, $tabMultiDepot['isDumePublier']);
        $this->assertEquals('SNTEST', $tabMultiDepot['numSN']);
    }

    public function testCreateDumeNumero()
    {
        $this->init();
        $result = $this->dumeService->createDumeNumero(20, 'TESTSN');
        $this->assertEquals(20, $result->getIdDumeContexte());
        $this->assertEquals('TESTSN', $result->getNumeroDumeNational());
    }

    public function testCreateDumeContexte()
    {
        $this->init();
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-min-1');
        $consultation = new Consultation();
        $consultation->setOrganisme($organisme);
        $result = $this->dumeService->createDumeContexte($consultation, $organisme, 20);
        $this->assertEquals($consultation, $result->getConsultation());
        $this->assertEquals($organisme, $result->getOrganisme());
        $this->assertEquals('oe', $result->getTypeDume());
        $this->assertEquals(99, $result->getStatus());
    }

    public function testDownloadDume()
    {
        $this->init();
        $result = $this->dumeService->downloadDume('xml', 10, 93);
        $this->assertNotEmpty($result);
        $this->assertEquals('10093-0', $result['fileName']);
        $this->assertEquals(
            '/data/apache2/mpe-docker.local-trust.com/data/2020/06/29/pmi-min-1/files/10093-0',
            $result['filePath']
        );
        $this->assertEquals('SNTEST', $result['dumeNumero']);
    }

    protected function init($hasFile = true): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $em = $this->getEntityManager();
        $translatorInterface = $this->getTranslator();

        $tokenStorage = $this->createMock(TokenStorageInterface::class);
        $user = new Inscrit();
        $user->setEntrepriseId(10);
        $user->setIdEtablissement(40);
        $user->setId(60);
        $token = new UsernamePasswordToken($user, 'bar', 'key');

        $tokenStorage->expects($this->any())
            ->method('getToken')
            ->willReturn($token);
        $parameters = $this->getParameters();
        $mountManager = $this->getMountManager($hasFile);
        $this->dumeService = new DumeService(
            $em,
            $logger,
            $translatorInterface,
            $tokenStorage,
            $mountManager,
            $parameters
        );
    }

    private function getEntityManager()
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (TDumeContexte::class == $repository) {
                        $repository = $this->createMock(TDumeContexteRepository::class);

                        $dumeContexte = new TDumeContexte();
                        $class = new \ReflectionClass($dumeContexte);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($dumeContexte, 93);
                        $dumeContexte->setOrganisme((new Organisme())->setAcronyme('pmi-min-1'));
                        $dumeContexte->setIsStandard(true);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->with(['id' => 93])
                            ->willReturn($dumeContexte);

                        $repository->expects($this->any())
                            ->method('getWaitingOrValidOrPublish')
                            ->withAnyParameters()
                            ->willReturn($dumeContexte);

                        return $repository;
                    }
                    if (TDumeNumero::class == $repository) {
                        $repository = $this->createMock(TDumeNumeroRepository::class);

                        $dumeNumero = new TDumeNumero();
                        $class = new \ReflectionClass($dumeNumero);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($dumeNumero, 600);
                        $dumeNumero->setIdDumeContexte(new TDumeContexte());
                        $dumeNumero->setNumeroDumeNational('SNTEST');
                        $dumeNumero->setBlobIdXml(10093);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn($dumeNumero);
                        $repository->expects($this->any())
                            ->method('find')
                            ->with(93)
                            ->willReturn($dumeNumero);
                        $repository->expects($this->any())
                            ->method('getDumeNumeroFromGroupement')
                            ->with(1, 150)
                            ->willReturn($dumeNumero);

                        return $repository;
                    }
                    if (TCandidature::class == $repository) {
                        $repository = $this->createMock(TCandidatureRepository::class);

                        $candidature = new TCandidature();
                        $class = new \ReflectionClass($candidature);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($candidature, 540);
                        $dumeContexte = new TDumeContexte();
                        $class = new \ReflectionClass($dumeContexte);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($dumeContexte, 93);
                        $dumeContexte->setOrganisme((new Organisme())->setAcronyme('pmi-min-1'));
                        $dumeContexte->setIsStandard(true);
                        $dumeContexte->setStatus(3);
                        $candidature->setIdDumeContexte($dumeContexte);
                        $candidature->setTypeCandidature('dume');
                        $candidature->setTypeCandidatureDume('online');
                        $repository->expects($this->any())
                            ->method('getDumePublier')
                            ->willReturn($candidature);

                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn($candidature);

                        return $repository;
                    }

                    if (Offre::class == $repository) {
                        $repository = $this->createMock(OffreRepository::class);

                        $offre = new Offre();
                        $class = new \ReflectionClass($offre);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($offre, 10);
                        $offre->setConsultationId(93);
                        $offre->setStatutOffres(1);
                        $repository->expects($this->any())
                            ->method('find')
                            ->with(10)
                            ->willReturn($offre);

                        return $repository;
                    }
                    if (Consultation::class == $repository) {
                        $repository = $this->createMock(ConsultationRepository::class);

                        $consultation = new Consultation();
                        $class = new \ReflectionClass($consultation);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($consultation, 93);
                        $organisme = new Organisme();
                        $organisme->setAcronyme('pmi-min-1');
                        $consultation->setOrganisme($organisme);
                        $repository->expects($this->any())
                            ->method('find')
                            ->with(93)
                            ->willReturn($consultation);

                        return $repository;
                    }
                    if (BloborganismeFile::class == $repository) {
                        $repository = $this->createMock(BloborganismeFileRepository::class);

                        $bloborganismeFile = new BloborganismeFile();
                        $class = new \ReflectionClass($bloborganismeFile);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($bloborganismeFile, 66);
                        $bloborganismeFile->setOrganisme(('pmi-min-1'));
                        $bloborganismeFile->setChemin('2020/06/29/pmi-min-1/files/');
                        $repository->expects($this->any())
                            ->method('getOrganismeBlobFichier')
                            ->with(10093, 'pmi-min-1')
                            ->willReturn($bloborganismeFile);

                        return $repository;
                    }
                }
            );

        return $em;
    }

    private function getTranslator()
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->any())
            ->method('trans')
            ->willReturnCallback(
                function ($cle) {
                    switch ($cle) {
                        case 'DEFINE_DUME_ACHETEUR':
                            $result = 'dume_acheteur';
                            break;
                        case 'DEFINE_DUME_ACHETEUR_NON_PUBLIE':
                            $result = 'DUME standard';
                            break;
                        case 'DEFINE_DUME_ACHETEUR_PUBLIE':
                            $result = 'DUME défini par l\'acheteur';
                            break;
                        case 'MSG_ERREUR_TECHNIQUE':
                            $result = 'ERREUR';
                    }

                    return $result;
                }
            );

        return $translator;
    }

    public function getParameters()
    {
        return [
            'TYPE_DUME_ACHETEUR' => 'acheteur',
            'STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION' => '2',
            'STATUT_DUME_CONTEXTE_VALIDE' => '1',
            'STATUT_DUME_CONTEXTE_PUBLIE' => '3',
            'TYPE_CANDIDATUE_DUME' => 'dume',
            'TYPE_CANDIDATUE_DUME_ONLINE' => 'online',
            'TYPE_DUME_OE' => 'oe',
            'STATUT_ENV_BROUILLON' => 99,
            'BASE_ROOT_DIR' => '/data/apache2/mpe-docker.local-trust.com/data/',
            'COMMON_TMP' => '/data/apache2/mpe-docker.local-trust.com/data/tmp/',
        ];
    }

    public function getMountManager($hasFile = true)
    {
        $fileSystem = $this->createMock(Filesystem::class);
        $fileSystem->expects($this->any())
            ->method('has')
            ->with($this->anything())
            ->willReturn($hasFile);
        $mountManager = $this->createMock(MountManager::class);
        $mountManager->expects($this->any())
            ->method('getAbsolutePath')
            ->with($this->anything())
            ->willReturn('/data/apache2/mpe-docker.local-trust.com/data/2020/06/29/pmi-min-1/files/10093-0');
        $mountManager->expects($this->any())
            ->method('getFilePath')
            ->with($this->anything())
            ->willReturn('2020/06/25/pmi-min-1/files/10093-0');

        $mountManager->expects($this->any())
            ->method('getFilesystem')
            ->with($this->anything())
            ->willReturn($fileSystem);

        return $mountManager;
    }

    /**
     * @dataProvider provider
     */
    public function testDownloadZipDUMEById($hasFile, $idNumero, $acronymeOrganisme, $format, $expected)
    {
        $this->init($hasFile);
        $result = $this->dumeService->downloadZipDUMEById($idNumero, $acronymeOrganisme, $format);
        $actual = 'test';
        if (isset($result['archiveName'])) {
            if (false !== strstr($result['archiveName'], $expected)) {
                $actual = $expected;
            }
        } else {
            $actual = $expected;
        }

        $this->assertEquals($expected, $actual);
    }

    /**
     * @return Generator
     */
    public function provider()
    {
        yield [true, 93, 2, 'xml', 'DUME_ACHETEUR_10093'];
        yield [true, 93, 2, 'pdf', 'test'];
        yield [false, 93, 2, 'pdf', []];
    }
}
