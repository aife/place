<?php

namespace Tests\Unit\Service\Superviseur;

use App\Repository\TDumeContexteRepository;
use App\Service\AtexoConfiguration;
use App\Service\Superviseur\SuperviseurDume;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;

class SuperviseurDumeTest extends TestCase
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Container
     */
    private $container;

    protected function setUp(): void
    {
        $this->container = $this->createMock(Container::class);
        $this->entityManager = $this->createMock(EntityManager::class);
    }

    /**
     * Tester la fonction getDumesEnAttente.
     */
    public function testGetDumesEnAttente()
    {
        $this->container
            ->expects($this->exactly(5))
            ->method('getParameter')
            ->withConsecutive(
                ['URL_PF_DUME_API'],
                ['CURLOPT_CONNECTTIMEOUT_MS'],
                ['CURLOPT_TIMEOUT_MS'],
                ['URL_PROXY'],
                ['PORT_PROXY']
            )
            ->willReturnOnConsecutiveCalls('https://mpe-docker.local-trust.com/dume-api', '60', '1000', '', '');

        $conf = $this->createMock(AtexoConfiguration::class);

        $conf->expects($this->any())
            ->method('hasConfigPlateforme')
            ->willReturn(true);

        $this->container->expects($this->any())
            ->method('get')
            ->willReturn($conf);

        $repo = $this->createMock(TDumeContexteRepository::class);

        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repo);

        $repo->expects($this->any())
            ->method('getDumesEnAttente')
            ->willReturn(3);

        $superviseurDume = new SuperviseurDume($this->container, $this->entityManager);
        $result = $superviseurDume->checkDumeInterface();
        $this->assertTrue(is_array($result));
        $this->assertEquals(3, $result['specificStatuses'][0]['specificStatus']['value']);
    }
}
