<?php

namespace Tests\Unit\Service\Superviseur;

use App\Service\Superviseur\SuperviseurNas;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;

class SuperviseurNasTest extends TestCase
{
    public function testRandomString()
    {
        $this->container = new Container();
        $superviseurNAS = new SuperviseurNas($this->container);
        $text = $superviseurNAS->randomString(6);
        $this->assertNotEmpty($text);
    }
}
