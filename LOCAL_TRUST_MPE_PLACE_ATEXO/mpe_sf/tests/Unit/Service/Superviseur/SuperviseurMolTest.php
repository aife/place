<?php

namespace Tests\Unit\Service\Superviseur;

use App\Service\Superviseur\SuperviseurMol;
use PHPUnit\Framework\TestCase;

class SuperviseurMolTest extends TestCase
{
    /**
     * Tester la fonction getAnnoncesEnAttente.
     */
    public function testgetAnnoncesEnAttente()
    {
        $superviseurMol = $this->createMock(SuperviseurMol::class);

        $superviseurMol
            ->expects($this->any())
            ->method('getAnnoncesEnAttente')
            ->willReturn(3);

        $this->assertEquals(3, $superviseurMol->getAnnoncesEnAttente());
    }
}
