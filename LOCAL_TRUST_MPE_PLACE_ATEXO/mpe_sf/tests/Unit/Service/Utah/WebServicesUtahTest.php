<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Utah;

use App\Service\Utah\WebServicesUtah;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class WebServicesUtahTest extends TestCase
{
    public function testGetAuthKeycloakTokenWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $baseUrl = 'https://identite-release.local-trust.com';
        $urlIdentity = '/auth/realms/utah/protocol/openid-connect/token';
        $urlAuth = $baseUrl . $urlIdentity;

        $parameterBag
            ->expects(self::exactly(5))
            ->method('get')
            ->withConsecutive(
                ['URL_UTAH_KEYCLOAK_OPEN_ID'],
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
            )
            ->willReturnOnConsecutiveCalls(
                $urlIdentity,
                'mpe_release',
                'mpe_release',
                $baseUrl,
                20,
            )
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF", "refresh_token":"DFHESDFGFDH"}',
                ['http_code' => 200]
            )
        ]);

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Début d'appel au webservice $urlAuth"],
                ["Fin d'appel au webservice $urlAuth avec une reponse valide"],
            )
        ;

        $webServiceUtah = new WebServicesUtah($parameterBag, $client, $logger);
        $result = $webServiceUtah->getContent('getAuthResponse');

        $expectedResult = [
            "access_token" => 'FDSERT43HYTD5432SDPMLHRTDSFF',
            "refresh_token" => 'DFHESDFGFDH'
        ];

        self::assertEquals($expectedResult, $result);
    }

    public function testGetAuthKeycloakTokenWithStatusCodeInvalid()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $baseUrl = 'https://identite-release.local-trust.com';
        $urlIdentity = '/auth/realms/utah/protocol/openid-connect/token';
        $urlAuth = $baseUrl . $urlIdentity;

        $parameterBag
            ->expects(self::exactly(5))
            ->method('get')
            ->withConsecutive(
                ['URL_UTAH_KEYCLOAK_OPEN_ID'],
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
            )
            ->willReturnOnConsecutiveCalls(
                $urlIdentity,
                'mpe_release',
                'mpe_release',
                $baseUrl,
                20
            )
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '',
                ['http_code' => 500]
            )
        ]);

        $logger
            ->expects(self::once())
            ->method('info')
            ->withConsecutive(
                ["Début d'appel au webservice $urlAuth"],
                ["Fin d'appel au webservice $urlAuth"],
            )
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Fin d'appel au webservice $urlAuth : Le serveur a retourné une erreur")
        ;

        self::expectException(ServerException::class);
        self::expectExceptionCode(500);

        $webServiceUtah = new WebServicesUtah($parameterBag, $client, $logger);
        $webServiceUtah->getContent('getAuthResponse');
    }
}
