<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Utah;

use App\Service\Utah\UtahCommunicationService;
use App\Service\Utah\WebServicesUtah;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UtahCommunicationServiceTest extends TestCase
{
    public function testGetUrlAssistanceFaq()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'PF_URL_REFERENCE') {
                    $value = 'http://mpe-docker.local-trust.com/';
                }

                if ($params === 'URL_FAQ') {
                    $value = 'http://faq-docker.local-trust.com';
                }
                return $value;
            }));


        $logger = $this->createMock(LoggerInterface::class);
        $webServicesUtahAuth = $this->createMock(WebServicesUtah::class);
        $utahCommunicationService = new UtahCommunicationService($parameterBag, $logger, $webServicesUtahAuth);
        $result = $utahCommunicationService->getUrlAssistance();
        $this->assertEquals('http://faq-docker.local-trust.com', $result);
    }

    public function testGetUrlAssistanceUtah()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $parameterBag->method('get')
            ->will($this->returnCallback(function ($params) {
                $value = null;
                if ($params === 'PF_URL_REFERENCE') {
                    $value = 'http://mpe-docker.local-trust.com/';
                }

                if ($params === 'URL_FAQ') {
                    $value = 'http://utah-docker.local-trust.com';
                }
                return $value;
            }));


        $logger = $this->createMock(LoggerInterface::class);
        $webServicesUtahAuth = $this->createMock(WebServicesUtah::class);
        $utahCommunicationService = new UtahCommunicationService($parameterBag, $logger, $webServicesUtahAuth);
        $result = $utahCommunicationService->getUrlAssistance();
        $this->assertEquals('http://utah-docker.local-trust.com', $result);
    }

    public function testRecupererToken()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->will($this->returnCallback(function ($params) {
                $value = null;

                if ($params === 'URL_UTAH_RECUPERATION_TOKEN') {
                    $value = 'http://utah-docker.local-trust.com/';
                }

                if ($params === 'URL_FAQ') {
                    $value = 'http://utah-docker.local-trust.com';
                }
                return $value;
            }));
        $logger = $this->createMock(LoggerInterface::class);
        $webServicesUtahAuth = $this->createMock(WebServicesUtah::class);
        $utahCommunicationService = new UtahCommunicationService($parameterBag, $logger, $webServicesUtahAuth);
        $result = $utahCommunicationService->recupererToken('{}');
        $this->assertFalse($result);
    }
}
