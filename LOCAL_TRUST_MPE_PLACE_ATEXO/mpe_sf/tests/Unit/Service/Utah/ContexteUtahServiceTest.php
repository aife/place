<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Utah;

use App\Service\Utah\ContexteUtahService;
use PHPUnit\Framework\TestCase;

class ContexteUtahServiceTest extends TestCase
{

    public function testAddVisibleSupportEtUtilisateur()
    {
        $contexteUtahService = new ContexteUtahService();
        $result = $contexteUtahService->add(
            'nom',
            'label',
            'valeur',
            'string'
        );
        $this->assertIsArray($result->getChamp('nom'));
        $this->assertEquals('label', $result->getChamp('nom')['label']);
        $this->assertEquals('valeur', $result->getChamp('nom')['valeur']);
        $this->assertEquals('string', $result->getChamp('nom')['type_valeur']);
        $this->assertEquals(1, $result->getChamp('nom')['visible_utilisateur']);
        $this->assertEquals(1, $result->getChamp('nom')['visible_support']);
    }

    public function testAddNonVisibleSupport()
    {
        $contexteUtahService = new ContexteUtahService();
        $result = $contexteUtahService->add(
            'nom',
            'label',
            'valeur',
            'string',
            0,
            1
        );
        $this->assertIsArray($result->getChamp('nom'));
        $this->assertEquals('label', $result->getChamp('nom')['label']);
        $this->assertEquals('valeur', $result->getChamp('nom')['valeur']);
        $this->assertEquals('string', $result->getChamp('nom')['type_valeur']);
        $this->assertEquals(0, $result->getChamp('nom')['visible_utilisateur']);
        $this->assertEquals(1, $result->getChamp('nom')['visible_support']);
    }
}
