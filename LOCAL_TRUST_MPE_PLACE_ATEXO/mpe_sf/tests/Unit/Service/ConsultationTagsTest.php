<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Agent;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\GeolocalisationN2;
use App\Entity\HabilitationAgent;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Exception\ConsultationNotFoundException;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Consultation\ClausesN1Repository;
use App\Repository\Consultation\ClausesN2Repository;
use App\Repository\Consultation\ClausesN3Repository;
use App\Repository\ConsultationRepository;
use App\Repository\ConsultationTagsRepository;
use App\Repository\GeolocalisationN2Repository;
use App\Service\Agent\ReferenceTypeService;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoUtil;
use App\Service\ClausesService;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\Consultation\ConsultationService;
use App\Service\ConsultationTags;
use App\Service\CurrentUser;
use App\Service\InscritOperationsTracker;
use App\Service\InterfaceSub;
use App\Service\ObscureDataManagement;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationTagsTest extends TestCase
{
    public function testGetTagByConsultationId(): void
    {
        $consultationRepo = $this->createMock(ConsultationRepository::class);
        $consultationRepo->expects($this->once())
            ->method('find')->willReturn(new Consultation());

        $consultationTags = new class () extends \App\Entity\ConsultationTags {
            public function getId(): int
            {
                return 22;
            }
        };

        $consultationTagsRepo = $this->createMock(ConsultationTagsRepository::class);
        $consultationTagsRepo->expects($this->once())
            ->method('findOneBy')->willReturn($consultationTags);

        $service = new ConsultationTags(
            $this->createMock(TranslatorInterface::class),
            $consultationRepo,
            $consultationTagsRepo
        );

        $return = $service->getTagByConsultationId(1, 'code');

        $this->assertEquals(22, $return);

        $consultationRepo = $this->createMock(ConsultationRepository::class);
        $consultationRepo->expects($this->once())
            ->method('find')->willReturn(new Consultation());

        $consultationTagsRepo = $this->createMock(ConsultationTagsRepository::class);
        $consultationTagsRepo->expects($this->once())
            ->method('findOneBy')->willReturn(null);

        $service = new ConsultationTags(
            $this->createMock(TranslatorInterface::class),
            $consultationRepo,
            $consultationTagsRepo
        );

        $return = $service->getTagByConsultationId(1, 'code');

        $this->assertEquals(null, $return);
    }

    public function testGetReferentielTags(): void
    {
        $trans = $this->createMock(TranslatorInterface::class);
        $trans->expects($this->once())
            ->method('trans')->willReturn('a label');

        $service = new ConsultationTags(
            $trans,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(ConsultationTagsRepository::class)
        );

        $return = $service->getReferentielTags();

        $this->assertNotEmpty($return);
        $this->assertIsArray($return);

        foreach ($return as $value) {
            $this->assertNotEmpty($value['code']);
            $this->assertNotEmpty($value['label']);
            $this->assertNotEmpty($value['logo']);
        }
    }
}
