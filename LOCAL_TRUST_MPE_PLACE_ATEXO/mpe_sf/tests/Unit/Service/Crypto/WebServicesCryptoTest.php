<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Service\Crypto;

use ReflectionClass;
use App\Exception\UnexpectedResponseStatusException;
use App\Service\Crypto\WebServicesCrypto;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesCryptoTest extends TestCase
{
    public function testPrepAndSend(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $url = 'https://webservice/crypto/signature';
        $expectedCodes = [200];
        $verb = 'POST';
        $requestBody = null;
        $returnData = true;
        $contentType = 'application/json';
        $acceptType = 'application/json';
        $headers = [];

        $logger
            ->expects(self::exactly(4))
            ->method('info')
            ->withConsecutive(
                ["WebServicesCrypto - Debut prepAndSend avec comme url ws : https://webservice/crypto/signature et verbe http : POST"],
                ["WebServicesCrypto - body info Content-Type: application/json, Accept: application/json"],
                ["WebServicesCrypto - Retry call API avec nombre max d'essai: 3 et délai de: 1s"],
                ["WebServicesCrypto - Fin WS prepAndSend avec comme url ws : https://webservice/crypto/signature et verbe http : POST"],
            )
        ;

        $client = new MockHttpClient([
            new MockResponse('{"signature":"Error", "response":"HTTP_SERVICE_UNAVAILABLE"}', ['http_code' => 503]),
            new MockResponse(
                '{"signature":"valide", "response":"ok", "message":"Found after 1 failed try"}',
                ['http_code' => 200]
            ),
        ]);

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['PARAM_HTTP_CLIENT_MAX_RETRY'],
                ['PARAM_HTTP_CLIENT_DELAY'],
            )
            ->willReturnOnConsecutiveCalls(
                3,
                1000
            )
        ;

        $webServiceCrypto = new WebServicesCrypto($client, $logger, $parameterBag);

        $result = $webServiceCrypto->prepAndSend(
            $url,
            $expectedCodes,
            $verb,
            $requestBody,
            $returnData,
            $contentType,
            $acceptType,
            $headers
        );

        self::assertSame($result, '{"signature":"valide", "response":"ok", "message":"Found after 1 failed try"}');
    }

    public function testPrepAndSendReturnTrue(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $url = 'https://webservice/crypto/signature';
        $expectedCodes = [200];
        $verb = 'POST';
        $requestBody = null;
        $returnData = false;
        $contentType = 'application/json';
        $acceptType = 'application/json';
        $headers = [];

        $logger
            ->expects(self::exactly(4))
            ->method('info')
            ->withConsecutive(
                ["WebServicesCrypto - Debut prepAndSend avec comme url ws : https://webservice/crypto/signature et verbe http : POST"],
                ["WebServicesCrypto - body info Content-Type: application/json, Accept: application/json"],
                ["WebServicesCrypto - Retry call API avec nombre max d'essai: 3 et délai de: 1s"],
                ["WebServicesCrypto - Fin WS prepAndSend avec comme url ws : https://webservice/crypto/signature et verbe http : POST"],
            )
        ;

        $client = new MockHttpClient([
            new MockResponse('{"signature":"valide", "response":"ok"}', ['http_code' => 200]),
        ]);

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['PARAM_HTTP_CLIENT_MAX_RETRY'],
                ['PARAM_HTTP_CLIENT_DELAY'],
            )
            ->willReturnOnConsecutiveCalls(
                3,
                1000
            )
        ;

        $webServiceCrypto = new WebServicesCrypto($client, $logger, $parameterBag);

        $result = $webServiceCrypto->prepAndSend(
            $url,
            $expectedCodes,
            $verb,
            $requestBody,
            $returnData,
            $contentType,
            $acceptType,
            $headers
        );

        self::assertTrue($result);
    }

    /**
     * @dataProvider getAssociatedVerbs
     */
    public function testGetRequestVerb(string $customedVerb, string $expectedVerb)
    {
        $client = $this->createMock(HttpClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $webServiceCrypto = new WebServicesCrypto($client, $logger, $parameterBag);

        $reflectionClass = new ReflectionClass(WebServicesCrypto::class);
        $privateMethod = $reflectionClass->getMethod('getRequestVerb');
        $privateMethod->setAccessible(true);

        $result = $privateMethod->invokeArgs($webServiceCrypto, [$customedVerb]);

        self::assertSame($result, $expectedVerb);
    }

    public function getAssociatedVerbs(): iterable
    {
        yield ['GET', 'GET'];
        yield ['POST', 'POST'];
        yield ['PUT', 'PUT'];
        yield ['DELETE', 'DELETE'];
        yield ['PATCH', 'PATCH'];
        yield ['POST_MP', 'POST'];
        yield ['POST_BIN', 'POST'];
        yield ['PUT_MP', 'PUT'];
        yield ['PUT_BIN', 'PUT'];
    }


    public function testHandleError(): void
    {
        $client = $this->createMock(HttpClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $webServiceCrypto = new WebServicesCrypto($client, $logger, $parameterBag);

        $reflectionClass = new ReflectionClass(WebServicesCrypto::class);
        $privateMethod = $reflectionClass->getMethod('handleError');
        $privateMethod->setAccessible(true);

        $url = 'https://webservice/crypto/sign';
        $responseBody = '{"signature":"error", "response":"error"}';

        $logger
            ->expects(self::once())
            ->method('error')
            ->with(
                "An unexpected HTTP status code was returned by the server lors de l'appel au WS Crypto sur l'url https://webservice/crypto/sign qui retourne ce codeStatus 503"
            )
        ;

        self::expectException(UnexpectedResponseStatusException::class);

        $privateMethod->invokeArgs($webServiceCrypto, [$url, 503, $responseBody]);
    }
}
