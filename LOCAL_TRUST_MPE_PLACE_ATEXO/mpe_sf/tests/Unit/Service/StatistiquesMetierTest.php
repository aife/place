<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Service\StatistiquesMetier;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Tests\Unit\AtexoTestCase;

/**
 * Class StatistiquesMetierTest.
 */
class StatistiquesMetierTest extends AtexoTestCase
{
    private $container;

    protected function setUp(): void
    {
        $this->container = $this->createMock(Container::class);
        $this->container->method('getParameter')->with('MONTANT_MAPA_INF_90')->willReturn(1);
    }

    public function testCalalculStatistiques()
    {
        $entry = [];
        $logger = $this->createMock(Logger::class);
        $em = $this->createMock(EntityManager::class);
        $param = $this->createMock(ParameterBag::class);
        $param->method('get')->with('MONTANT_MAPA_INF_90')->willReturn('1');
        $service = new StatistiquesMetier($logger, $param, $em);
        $resultat = $service->calculStatistiques($entry);
        $this->assertEquals($resultat['mapaInf'], 0);
        $this->assertEquals($resultat['mapaSupp'], 0);
        $this->assertEquals($resultat['totalMapa'], 0);
        $this->assertEquals($resultat['ao'], 0);
        $this->assertEquals($resultat['mn'], 0);
        $this->assertEquals($resultat['pn'], 0);
        $this->assertEquals($resultat['dc'], 0);
        $this->assertEquals($resultat['autre'], 0);
        $this->assertEquals($resultat['total'], 0);
        $this->assertEquals($resultat['sad'], 0);
        $this->assertEquals($resultat['accordcadre'], 0);
        $this->assertEquals($resultat['totalAccordCadreSad'], 0);
    }

    public function testCalculStatistiquesWithData()
    {
        $entry = [
            '0' => [
                'nombreConsultation' => 59,
                'id_type_procedure' => 1,
                'id_type_procedure_portail' => 1,
                'ao' => 1,
                'mn' => 0,
                'pn' => 0,
                'dc' => 0,
                'autre' => 0,
                'mapa' => 0,
                'id_montant_mapa' => 0,
                'sad' => 0,
                'accordcadre' => 0,
            ],
            '1' => [
                'nombreConsultation' => 200,
                'id_type_procedure' => 1,
                'id_type_procedure_portail' => 1,
                'ao' => 1,
                'mn' => 5,
                'pn' => 0,
                'dc' => 10,
                'autre' => 0,
                'mapa' => 0,
                'id_montant_mapa' => 1000,
                'sad' => 0,
                'accordcadre' => 9,
            ],
        ];
        $logger = $this->createMock(Logger::class);
        $em = $this->createMock(EntityManager::class);
        $param = $this->createMock(ParameterBag::class);
        $param->method('get')
            ->with('MONTANT_MAPA_INF_90')
            ->willReturn('1');
        $service = new StatistiquesMetier($logger, $param, $em);
        $resultat = $service->calculStatistiques($entry);
        $this->assertEquals($resultat['mapaInf'], 0);
        $this->assertEquals($resultat['mapaSupp'], 0);
        $this->assertEquals($resultat['totalMapa'], 0);
        $this->assertEquals($resultat['ao'], 259);
        $this->assertEquals($resultat['mn'], 0);
        $this->assertEquals($resultat['pn'], 0);
        $this->assertEquals($resultat['dc'], 0);
        $this->assertEquals($resultat['autre'], 0);
        $this->assertEquals($resultat['total'], 259);
        $this->assertEquals($resultat['sad'], 0);
        $this->assertEquals($resultat['accordcadre'], 0);
        $this->assertEquals($resultat['totalAccordCadreSad'], 0);
    }
}
