<?php

namespace Tests\Unit\Service\Etablissement;

use DateTime;
use Exception;
use App\Entity\Etablissement;
use App\Service\AtexoEtablissement;
use PHPUnit\Framework\TestCase;

class AtexoEtablissementTest extends TestCase
{
    /**
     * @dataProvider etablissementProvider
     */
    public function testValidateInfoEtablissement($arrayEtablissement, $arrayErrors)
    {
        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $atexoEtablissement->expects($this->any())
            ->method('validateInfo')
            ->with($arrayEtablissement)
            ->willReturn($arrayErrors);

        $this->assertEquals($arrayErrors, $atexoEtablissement->validateInfo($arrayEtablissement));
    }

    public function etablissementProvider()
    {
        return [
            // Etablissement Valide
            [[
                'id' => 0,
                'siret' => 38391710100058,
                'idEntreprise' => 16669,
                'siege' => 1,
                'dateCreation' => '2019-08-23T11:23:04+02:00',
                'dateModification' => '2019-08-23T11:23:04+02:00',
                'adresse' => [
                        'rue' => '52 BD DE L\'YERRES',
                        'codePostal' => 91000,
                        'ville' => 'Argenteuil',
                        'pays' => 'France',
                    ],
            ], []],
            // Etablissement avec Siret non valide
            [[
                'id' => 0,
                'siret' => 3839171010005800000000,
                'idEntreprise' => 16669,
                'siege' => 1,
                'dateCreation' => '2019-08-23T11:23:04+02:00',
                'dateModification' => '2019-08-23T11:23:04+02:00',
                'adresse' => [
                        'rue' => '52 BD DE L\'YERRES',
                        'codePostal' => 91000,
                        'ville' => 'Argenteuil',
                        'pays' => 'France',
                    ],
            ], ['Siret non valide']],
        ];
    }

    public function testSerializeEtablissement()
    {
        $arrayEtablissement = [
            'id' => 0,
            'siret' => 38391710100058,
            'idEntreprise' => 16669,
            'siege' => 1,
            'dateCreation' => '2019-08-23T11:23:04+02:00',
            'dateModification' => '2019-08-23T11:23:04+02:00',
            'adresse' => [
                    'rue' => '52 BD DE L\'YERRES',
                    'codePostal' => 91000,
                    'ville' => 'Argenteuil',
                    'pays' => 'France',
                ],
        ];

        $etablissement = new Etablissement();
        $etablissement->setSiret(38391710100058);
        $etablissement->setEstSiege(1);
        $etablissement->setDateCreation(new DateTime('2019-08-23T11:23:04+02:00'));
        $etablissement->setDateModification(new DateTime('2019-08-23T11:23:04+02:00'));
        $etablissement->setCodePostal(91000);
        $etablissement->setPays('France');
        $etablissement->setVille('Argenteuil');
        $etablissement->setAdresse('52 BD DE L\'YERRES');

        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $atexoEtablissement->expects($this->any())
            ->method('serializeEtablissement')
            ->with($arrayEtablissement)
            ->willReturn($etablissement);

        $expectedEtablissement = $atexoEtablissement->serializeEtablissement($arrayEtablissement);
        $this->assertInstanceOf(Etablissement::class, $expectedEtablissement);
        $this->assertEquals($expectedEtablissement->getSiret(), $etablissement->getSiret());
        $this->assertEquals($expectedEtablissement->getEstSiege(), $etablissement->getEstSiege());
        $this->assertEquals($expectedEtablissement->getDateCreation(), $etablissement->getDateCreation());
        $this->assertEquals($expectedEtablissement->getDateModification(), $etablissement->getDateModification());
        $this->assertEquals($expectedEtablissement->getAdresse(), $etablissement->getAdresse());
    }

    /**
     * @group time-sensitive
     *
     * @param $mode
     * @param $upsertedEntreprise
     * @dataProvider etablissementModeProvider
     *
     * @throws Exception
     */
    public function testCreateOrUpdateEntity($mode, $upsertedEtablissement)
    {
        $dateTime = $this->createMock(DateTime::class);
        $dateTime->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:03 11:06:07'));

        $etablissement = $this->createMock(Etablissement::class);
        $etablissement->expects($this->any())
            ->method('getSiret')
            ->willReturn(38391710100058);

        $etablissement->expects($this->any())
            ->method('getEstSiege')
            ->willReturn(1);

        $etablissement->expects($this->any())
            ->method('getAdresse')
            ->willReturn("52 BD DE L'YERRES");

        $etablissement->expects($this->any())
            ->method('getPays')
            ->willReturn('France');

        $etablissement->expects($this->any())
            ->method('getCodePostal')
            ->willReturn(91000);

        $etablissement->expects($this->any())
            ->method('getDateCreation')
            ->willReturn(new DateTime('2019:09:03 11:06:07'));

        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $atexoEtablissement->expects($this->any())
            ->method('createOrUpdateEntity')
            ->with($etablissement, $mode)
            ->willReturn($upsertedEtablissement);

        $expectedEtablissement = $atexoEtablissement->createOrUpdateEntity($etablissement, $mode);

        $this->assertEquals($expectedEtablissement->getSiret(), $etablissement->getSiret());
        $this->assertEquals($expectedEtablissement->getEstSiege(), $etablissement->getEstSiege());
        $this->assertEquals($expectedEtablissement->getDateCreation(), $etablissement->getDateCreation());
        $this->assertEquals($expectedEtablissement->getDateModification(), $etablissement->getDateModification());
        $this->assertEquals($expectedEtablissement->getAdresse(), $etablissement->getAdresse());
        $this->assertEquals($expectedEtablissement->getCodePostal(), $etablissement->getCodePostal());
        $this->assertEquals($expectedEtablissement->getPays(), $etablissement->getPays());
        $this->assertEquals($expectedEtablissement->getDateCreation(), $etablissement->getDateCreation());
    }

    public function etablissementModeProvider()
    {
        $upsertedEtablissement = new Etablissement();
        $upsertedEtablissement->setSiret(38391710100058);
        $upsertedEtablissement->setEstSiege(1);
        $upsertedEtablissement->setCodePostal(91000);
        $upsertedEtablissement->setPays('France');
        $upsertedEtablissement->setVille('Argenteuil');
        $upsertedEtablissement->setAdresse('52 BD DE L\'YERRES');

        return [
            ['create', $upsertedEtablissement->setDateCreation(new \DateTime('2019:09:03 11:06:07'))],
            ['update', $upsertedEtablissement],
        ];
    }
}
