<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Spaser;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Service\Spaser\WebServicesSpaser;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Response;

class WebServicesSpaserTest extends TestCase
{
    private const IDENTITY_URL = 'https://identite-release.local-trust.com';
    private const IDENTITY_REALM_ENPOINT = '/auth/realms/spaser/protocol/openid-connect/token';
    private const SPASER_URL = 'https://spaser-docker.local-trust.com/';

    public function testGetAuthKeycloakTokenWith200(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $parameterBag
            ->expects($this->any())
            ->method('get')
            ->withConsecutive(
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
            )
            ->willReturnOnConsecutiveCalls(
                'mpe_release',
                'mpe_release',
            )
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF", "refresh_token":"DFHESDFGFDH"}',
                ['http_code' => Response::HTTP_OK]
            )
        ]);

        $logger
            ->expects(self::exactly(3))
            ->method('info')
            ->withConsecutive(
                ["Récupération du token avec le WS Identity"],
                ["Début d'appel au webservice /auth/realms/spaser/protocol/openid-connect/token"],
                ["Fin d'appel au webservice /auth/realms/spaser/protocol/openid-connect/token avec une reponse valide"],
            )
        ;

        $webServiceSpaser = new WebServicesSpaser($client, $logger, $parameterBag);
        $result = $webServiceSpaser->getContent('getAuthResponse');

        $expectedResult = [
            "access_token" => 'FDSERT43HYTD5432SDPMLHRTDSFF',
            "refresh_token" => 'DFHESDFGFDH'
        ];

        self::assertEquals($expectedResult, $result);
    }

    public function testGetAuthKeycloakTokenWithStatusCodeInvalid(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $urlIdentity = self::IDENTITY_REALM_ENPOINT;

        $parameterBag
            ->expects($this->any())
            ->method('get')
            ->withConsecutive(
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
            )
            ->willReturnOnConsecutiveCalls(
                'mpe_release',
                'mpe_release',
                '',
                20,
            )
        ;

        $client = new MockHttpClient([
            new MockResponse(
                '',
                ['http_code' => Response::HTTP_INTERNAL_SERVER_ERROR]
            )
        ]);

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Récupération du token avec le WS Identity"],
                ["Début d'appel au webservice /auth/realms/spaser/protocol/openid-connect/token"],
                ["Fin d'appel au webservice /auth/realms/spaser/protocol/openid-connect/token avec une reponse valide"],
            )
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Fin d'appel au webservice $urlIdentity : Le serveur a retourné une erreur")
        ;

        self::expectException(ServerException::class);
        self::expectExceptionCode(500);

        $webServiceSpaser = new WebServicesSpaser($client, $logger, $parameterBag);
        $webServiceSpaser->getContent('getAuthResponse');
    }

    public function testGetSpaserAuthUrl(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $baseUrl = self::IDENTITY_URL;
        $urlIdentity = self::IDENTITY_REALM_ENPOINT;
        $urlAuth = $baseUrl . $urlIdentity;

        $endpointContextSpaser = self::SPASER_URL;
        $redirectUrlSpaser = 'saisie-indicateur/consultation?token=token?year=2023&externalId=84';
        $urlInitContext = $endpointContextSpaser . 'api/authorization-contexts/init';

        $agent = new Agent();
        $agent
            ->setId(42)
            ->setPrenom('Pierre Paul')
            ->setNom('Jacques')
        ;

        $consultation = new Consultation();
        $consultation->setId(84);
        $consultation->setDatefin(new DateTimeImmutable("2023-09-30 10:25:36"));

        $parameterBag
            ->expects($this->any())
            ->method('get')
            ->withConsecutive(
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
                ['URL_SPASER'],
                ['URL_SPASER'],
                ['UID_PF_MPE'],
            )
            ->willReturnOnConsecutiveCalls(
                'mpe_release',
                'mpe_release',
                $baseUrl,
                20,
                $endpointContextSpaser,
                $endpointContextSpaser,
                'uuid-pf-mpe',
            )
        ;

        $spaserRedirectReponse = [
            'redirectUrl' =>  $redirectUrlSpaser
        ];

        $client = new MockHttpClient([
            new MockResponse(
                '{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF", "refresh_token":"DFHESDFGFDH"}',
                ['http_code' => Response::HTTP_OK]
            ),
            new MockResponse(
                json_encode($spaserRedirectReponse),
                ['http_code' => Response::HTTP_CREATED]
            )
        ]);

        $logger
            ->expects(self::exactly(8))
            ->method('info')
            ->withConsecutive(
                ["Début appel WS Spaser"],
                ["Récupération du token avec le WS Identity"],
                ["Début d'appel au webservice $urlAuth"],
                ["Fin d'appel au webservice $urlAuth avec une reponse valide"],
                [
                    sprintf(
                        "Appel au WS Spaser pour récupérer l'url de redirection (indicateur|dashboard|admin): %s",
                        $urlInitContext
                    )
                ],
                ["Début d'appel au webservice $urlInitContext"],
                ["Fin d'appel au webservice $urlInitContext avec une reponse valide"],
                ["Appel au WS réussi pour le endpoint $urlInitContext"],
            )
        ;

        $webServiceSpaser = new WebServicesSpaser($client, $logger, $parameterBag);
        $result = $webServiceSpaser->getContent(
            'getSpaserAuthUrl',
            $agent,
            'indicateur',
            $consultation
        );

        self::assertEquals($endpointContextSpaser . $redirectUrlSpaser, $result);
    }

    public function testGetSpaserAuthUrlInitContextKo(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $baseUrl = self::IDENTITY_URL;
        $urlIdentity = self::IDENTITY_REALM_ENPOINT;
        $urlAuth = $baseUrl . $urlIdentity;

        $endpointContextSpaser = self::SPASER_URL;
        $redirectUrlSpaser = '';
        $urlInitContext = $endpointContextSpaser . 'api/authorization-contexts/init';

        $agent = new Agent();
        $agent
            ->setId(42)
            ->setPrenom('Pierre Paul')
            ->setNom('Jacques')
        ;

        $consultation = new Consultation();
        $consultation->setId(84);
        $consultation->setDatefin(new DateTimeImmutable("2023-09-30 10:25:36"));

        $parameterBag
            ->expects($this->any())
            ->method('get')
            ->withConsecutive(
                ['IDENTITE_USERNAME'],
                ['IDENTITE_PASSWORD'],
                ['IDENTITE_BASE_URL'],
                ['timeOutService'],
                ['URL_SPASER'],
                ['URL_SPASER'],
                ['UID_PF_MPE'],
            )
            ->willReturnOnConsecutiveCalls(
                'mpe_release',
                'mpe_release',
                $baseUrl,
                20,
                $endpointContextSpaser,
                $endpointContextSpaser,
                'uuid-pf-mpe',
            )
        ;

        $spaserRedirectReponse = [
            'redirectUrl' =>  $redirectUrlSpaser
        ];

        $client = new MockHttpClient([
            new MockResponse(
                '{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF", "refresh_token":"DFHESDFGFDH"}',
                ['http_code' => Response::HTTP_OK]
            ),
            new MockResponse(
                json_encode($spaserRedirectReponse),
                ['http_code' => Response::HTTP_OK]
            )
        ]);

        $logger
            ->expects(self::exactly(7))
            ->method('info')
            ->withConsecutive(
                ["Début appel WS Spaser"],
                ["Récupération du token avec le WS Identity"],
                ["Début d'appel au webservice $urlAuth"],
                ["Fin d'appel au webservice $urlAuth avec une reponse valide"],
                [
                    sprintf(
                        "Appel au WS Spaser pour récupérer l'url de redirection (indicateur|dashboard|admin): %s",
                        $urlInitContext
                    )
                ],
                ["Début d'appel au webservice $urlInitContext"],
                ["Fin d'appel au webservice $urlInitContext avec une reponse valide"],
            )
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with(
                "Echec lors de la récupération du lien de redirection via le WS spaser pour le endpoint $urlInitContext"
            )
        ;

        $webServiceSpaser = new WebServicesSpaser($client, $logger, $parameterBag);
        $result = $webServiceSpaser->getContent(
            'getSpaserAuthUrl',
            $agent,
            'indicateur',
            $consultation
        );

        self::assertEquals($endpointContextSpaser . $redirectUrlSpaser, $result);
    }
}
