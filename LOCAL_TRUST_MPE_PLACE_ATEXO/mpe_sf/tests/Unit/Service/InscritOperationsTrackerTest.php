<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use DateTime;
use App\Entity\InscritOperationsTracker;
use App\Service\Referentiel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Service\InscritOperationsTracker as TrackingService;

class InscritOperationsTrackerTest extends TestCase
{
    public function testRetrieveTraceOperationsInscrit(): void
    {
        $inscritTracker = new InscritOperationsTracker();
        $inscritTracker->setOrganisme('AGR');
        $repository = $this->createMock(EntityRepository::class);
        $repository->method('findOneBy')
            ->willReturn($inscritTracker);
        $trackingService = $this->getService($repository);

        $trackingObject = $trackingService->retrieveTraceOperationsInscrit(
            1,
            13,
            '127.0.9.1',
            new DateTime()
        );

        $this->assertInstanceOf(InscritOperationsTracker::class, $trackingObject);
        $this->assertEquals('AGR', $trackingObject->getOrganisme());
    }

    public function testRetrieveTraceOperationsInscritWithObjectNotFound()
    {
        $repository = $this->createMock(EntityRepository::class);
        $repository->method('findOneBy')
            ->willReturn(null);
        $trackingService = $this->getService($repository);

        $trackingObject = $trackingService->retrieveTraceOperationsInscrit(
            9999,
            9999,
            '127.0.9.1',
            new DateTime()
        );

        $this->assertNull($trackingObject);
    }

    public function testGetInfosValeursReferentielles()
    {
        $referentiel = $this->createMock(Referentiel::class);
        $referentiel->method('getIdValeurReferentiel')
            ->willReturn(16);
        $referentiel->method('getLibelleValeurReferentiel')
            ->willReturn('Solidaire');
        $valeursReferentielles = $this->getService(null, $referentiel)->getInfosValeursReferentielles('SOLIDAIRE');

        $this->assertIsArray($valeursReferentielles);
        $this->assertEquals(16, $valeursReferentielles[0]);
        $this->assertEquals('Solidaire', $valeursReferentielles[1]);
    }

    public function testGetInfosValeursReferentiellesWithNofoudData()
    {
        $valeursReferentielles = $this->getService()->getInfosValeursReferentielles('SOLIDAIRE');

        $this->assertIsArray($valeursReferentielles);
        $this->assertNull($valeursReferentielles[0]);
        $this->assertNull($valeursReferentielles[1]);
    }

    public function testCompleteDescription()
    {
        $description = 'Début de chargement (upload) [_type_fichier_] : [_liste_fichiers_tailles_]';
        $translator = $this->createMock(TranslatorInterface::class);

        $translator
            ->expects(self::exactly(2))
            ->method('trans')
            ->withConsecutive(
                ['DEFINE_DE_CANDIDATURE'],
                ['DEFINE_DE_LA_PIECE_LIBRE'],
            )
            ->willReturnOnConsecutiveCalls(
                'DEFINE_DE_CANDIDATURE',
                'PRI',
            )
        ;

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('TYPE_ENV_CANDIDATURE')
            ->willReturn('PRI')
        ;

        $this->getService(
            null,
            null,
            $translator,
            $parameterBag
        )->completeDescription(
            $description,
            '1',
            'PRI',
            'PRI'
        );

        self::assertSame($description, 'Début de chargement (upload) PRI : 1');
    }

    private function getService(
        EntityRepository $repository = null,
        Referentiel $referentiel = null,
        TranslatorInterface $translator = null,
        ParameterBagInterface $parameterBag = null
    ): TrackingService {
        if (null === $parameterBag) {
            $parameterBag = $this->createMock(ParameterBagInterface::class);
        }
        if (null === $repository) {
            $repository = $this->createMock(EntityRepository::class);
        }
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $request = $this->createMock(RequestStack::class);
        if (null === $referentiel) {
            $referentiel = $this->createMock(Referentiel::class);
        }
        if (null === $translator) {
            $translator = $this->createMock(TranslatorInterface::class);
        }
        $security = $this->createMock(Security::class);

        return new TrackingService(
            $parameterBag,
            $em,
            $request,
            $referentiel,
            $translator,
            $security
        );
    }

    public function testGetCleDescription()
    {
        $cleDescription = $this->getService()->getCleDescription('ADD', 'PRI');

        $this->assertEquals('DESCRIPTION26', $cleDescription);
    }

    public function testGetCleDescriptionDeleteAction()
    {
        $cleDescription = $this->getService()->getCleDescription('DELETE', 'PRI');

        $this->assertEquals('DESCRIPTION29', $cleDescription);
    }
}
