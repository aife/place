<?php

namespace Tests\Unit\Service;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\Hasher\SodiumPasswordHasher;
use App\Entity\Agent;
use App\Service\PradoPasswordEncoder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class PradoPasswordEncoderTest extends TestCase
{
    public function testEncodePassword()
    {
        $pradoEncoder = $this->getPradoPasswordEncoder();
        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');

        $this->assertStringContainsString('arg', $encodedPassword);
    }

    public function testIsPasswordValid()
    {
        $pradoEncoder = $this->getPradoPasswordEncoder();
        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');
        $isSamePassword = $pradoEncoder->isPasswordValid(Agent::class, $encodedPassword, 'pass');

        $this->assertTrue($isSamePassword);
    }

    public function testIsNotPasswordValid()
    {
        $pradoEncoder = $this->getPradoPasswordEncoder();
        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');
        $isSamePassword = $pradoEncoder->isPasswordValid(Agent::class, $encodedPassword, 'notSamePass');

        $this->assertFalse($isSamePassword);
    }

    private function getPradoPasswordEncoder(): PradoPasswordEncoder
    {
        $encoderFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $encoderFactory->method('getPasswordHasher')
            ->willReturn(new SodiumPasswordHasher());

        return new PradoPasswordEncoder($encoderFactory, $this->createMock(ParameterBagInterface::class));
    }
}
