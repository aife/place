<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Organisme;

use App\Model\OrganismeModel;
use App\Service\Organisme\SearchOrganismeTools;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class SearchOrganismeToolsTest extends TestCase
{
    public function testMapFilters()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $tools = new SearchOrganismeTools($em);
        $entitePublique = $this->createMock(OrganismeModel::class);
        $entitePublique->id = 34;
        $entitePublique->name = "Chambres de Commerce et d'Industrie (CCI)";
        $entitePublique->acronyme = "l2m";
        $entitePublique->sigle = "CCI";
        $entitePublique->serviceAccessible = "PLACE";
        $data = [
            "keyword" => "Ministère de l'agriculture et de l'alimentation",
            "entitePublique" => $entitePublique,
            "blabla" => "blabla",
        ];
        $expected = [
            "blabla" => "blabla",
            "denominationOrg" => "Ministère de l'agriculture et de l'alimentation",
            "id" => 34,
        ];
        $results = $tools->mapFilters($data);

        $this->assertSame($expected, $results);
        $this->assertEmpty($tools->mapFilters([]));
    }
}
