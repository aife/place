<?php

namespace Tests\Unit\Service\Contrats;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\DataTransformer\Output\ContratTitulaireOutputDataTransformer;
use App\Entity\CategorieConsultation;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Service;
use App\Entity\TypeContrat;
use App\Repository\EtablissementRepository;
use App\Repository\ServiceRepository;
use App\Repository\TypeContratRepository;
use App\Serializer\ContratTitulaireNormalizer;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use App\Service\Consultation\ConsultationService;
use App\Service\ContratService;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\DataTransformer\ContratTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

class AtexoContratTitulaireTest extends AtexoTestCase
{
    /**
     * @dataProvider getContratProvider
     */
    public function testFormatContractExtendedAction($contrat)
    {
        $em = $this->getAtexoObjectManager();
        $containerMock = $this->createMock(ContainerInterface::class);
        $serviceatexo_util = $this
            ->createMock(AtexoUtil::class, []);
        $serviceatexo_util->expects($this->any())
            ->method('formatterMontant')
            ->with($this->equalTo(150))
            ->will($this->returnValue(150));
        $containerMock->expects($this->any())
            ->method('get')
            ->with($this->equalTo('atexo.atexo_util'))
            ->will($this->returnValue($serviceatexo_util));


        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $logger = $this->createMock(LoggerInterface::class);

        $service = new ContratService(
            $containerMock,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );

        $marche = $service->listMarcheForContrat(['contrat' => $contrat], true);

        $this->assertNotEmpty($marche[0]->getDonneesComplementaires()->getNumerosContrat()->getIdTechnique());
        $chapeauMultiAttributaires =
            $marche[0]->getDonneesComplementaires()->getContratsChapeaux()->getContratChapeauMultiAttributaires();
        $chapeauACSAD = $marche[0]->getDonneesComplementaires()->getContratsChapeaux()->getContratChapeauAcSad();
        $this->assertEquals(
            $contrat->getIdContratMulti(),
            $chapeauMultiAttributaires->getIdTechniqueContratChapeauMultiAttributaires()
        );
        $this->assertEquals($contrat->getLienACSAD(), $chapeauACSAD->getIdTechniqueContratChapeauAcSad());
        // Nous ne pouvons pas avoir deux chapeaux sur un même contrat
        $this->assertTrue(!empty($chapeauMultiAttributaires) && !empty($chapeauACSAD));
    }

    public function getContratProvider()
    {
        $contrats = $this->getContrats();
        $provider = [];
        foreach ($contrats as $contrat) {
            $provider[] = [$contrat];
        }

        return $provider;
    }
}
