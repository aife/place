<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\PlateformeVirtuelle;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Repository\Configuration\PlateformeVirtuelleRepository;
use App\Service\PlateformeVirtuelle\Context;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Tests\Unit\PlateformeVirtuelleTestCase;

class ContextTest extends PlateformeVirtuelleTestCase
{
    public function currentDomainDataProvider(): array
    {
        return [
            [self::DOMAIN1],
            [self::DOMAIN2],
        ];
    }

    /**
     * @dataProvider currentDomainDataProvider
     */
    public function testGetCurrentDomain(string $domain)
    {
        $em = $this->createMock(EntityManager::class);

        $plateformeVirtuelleRepo = $this->createMock(PlateformeVirtuelleRepository::class);
        $em
            ->expects(self::once())
            ->method('getRepository')
            ->with(PlateformeVirtuelle::class)
            ->willReturn($plateformeVirtuelleRepo)
        ;

        $pfvService = new Context($this->getPreparedRequestStack($domain), $em, $this->logger);

        $currentDomain = $pfvService->getCurrentDomain();

        $this->assertEquals($domain, $currentDomain);
    }

    /**
     * Quand il existe une PFV correspondant au domaine en BDD.
     */
    public function testGetCurrentPlateformeVirtuelleExists()
    {
        $domain = self::DOMAIN1;

        $plateformeVirtuelle = new PlateformeVirtuelle();
        $plateformeVirtuelle->setId(100);
        $plateformeVirtuelle->setDomain($domain);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->getPfvRepoMock($plateformeVirtuelle));

        $pfvService = new Context($this->getPreparedRequestStack($domain), $em, $this->logger);

        $this->assertInstanceOf(PlateformeVirtuelle::class, $pfvService->getCurrentPlateformeVirtuelle());
        $this->assertIsInt($pfvService->getCurrentPlateformeVirtuelleId());
    }

    /**
     * Quand il n'existe pas de PFV correspondant au domaine en BDD.
     */
    public function testGetCurrentPlateformeVirtuelleNotExists()
    {
        $domain = self::DOMAIN1;

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->getPfvRepoMock(null));

        $pfvService = new Context($this->getPreparedRequestStack($domain), $em, $this->logger);

        $this->assertNull($pfvService->getCurrentPlateformeVirtuelle());
        $this->assertNull($pfvService->getCurrentPlateformeVirtuelleId());
    }

    protected function getPreparedRequestStack($domain): RequestStack
    {
        $server = [
            'HTTPS' => 'on',
            'HTTP_HOST' => $domain,
        ];

        return $this->prepareRequestStack([], $server);
    }
}
