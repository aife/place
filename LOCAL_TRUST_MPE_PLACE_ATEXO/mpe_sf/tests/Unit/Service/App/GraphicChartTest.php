<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\App;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Service\App\GraphicChart;
use Doctrine\ORM\EntityManager;
use Exception;
use Symfony\Component\HttpFoundation\RequestStack;
use Tests\Unit\PlateformeVirtuelleTestCase;

/**
 * Class GraphicChartTest.
 *
 * @author Sébastien Lepers <sebastien.lepers@atexo.com>
 */
class GraphicChartTest extends PlateformeVirtuelleTestCase
{
    /**
     * Si le cookie 'no-design' existe, on ne doit pas faire de
     * nouvelle requête en BDD.
     */
    public function testNoDesignCookie()
    {
        $requestStack = $this->prepareRequestStack(['no-design' => '1']);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->never())
            ->method('getRepository');

        $this->callService($requestStack, $em);
    }

    /**
     * @param RequestStack  $requestStack
     * @param EntityManager $em
     *
     * @throws Exception
     */
    protected function callService($requestStack, $em): void
    {
        $graphicChartService = new GraphicChart($requestStack, $em, $this->logger);
        $graphicChartService->setDesignCookie();
    }

    /**
     * Si le cookie 'no-design' n'existe pas, on cherche en BDD le domaine en cours.
     * S'il n'existe pas de PlateformeVirtuelle correspondant au domaine ou si son code_design est null,
     * on renseigne le cookie 'no-design' pour éviter de refaire la recherche en BDD à chaque appel à MPE.
     *
     * @runInSeparateProcess
     * @dataProvider noDesignCookieDataProvider
     */
    public function testSetNoDesignCookie($pfvMock)
    {
        $requestStack = $this->prepareRequestStack([]);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->getPfvRepoMock($pfvMock));

        $this->callService($requestStack, $em);

        $headers = xdebug_get_headers();

        $this->assertContains(
            'Set-Cookie: ' . GraphicChart::NO_DESIGN_COOKIE . '=1; path=/; secure; HttpOnly',
            $headers
        );
    }

    public function noDesignCookieDataProvider()
    {
        yield [null];
        yield [(new PlateformeVirtuelle())->setDomain(self::DOMAIN1)];
    }

    /**
     * Si le cookie 'no-design' n'existe pas, on cherche en BDD le domaine en cours.
     * S'il existe une PlateformeVirtuelle correspondant au domaine, on renseigne le cookie 'design'
     * avec le code design associé.
     *
     * @runInSeparateProcess
     */
    public function testSetDesignCookie()
    {
        $requestStack = $this->prepareRequestStack([]);
        $domain = self::DOMAIN1;

        $plateformeVirtuelle = new PlateformeVirtuelle();
        $plateformeVirtuelle->setDomain($domain);
        $plateformeVirtuelle->setCodeDesign('code1');

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->getPfvRepoMock($plateformeVirtuelle));

        $this->expectException(Exception::class);

        try {
            $this->callService($requestStack, $em);
        } catch (Exception $e) {
            $headers = xdebug_get_headers();

            $expectedHeaders =
                [
                    'Set-Cookie: ' . GraphicChart::DESIGN_COOKIE . '=code1; path=/; secure; HttpOnly',
                    'Set-Cookie: ' . GraphicChart::NO_DESIGN_COOKIE
                    . '=deleted; expires=Thu, 01-Jan-1970 00:00:01 GMT; Max-Age=0; path=/; secure; HttpOnly',
                    'Status: 302 Moved Temporarily',
                    'Location: https://' . $domain . '/entreprise',
                ];
            foreach ($expectedHeaders as $expectedHeader) {
                $this->assertContains(
                    $expectedHeader,
                    $headers,
                    'Il manque le header suivant : ' . $expectedHeader . PHP_EOL
                    . 'Les headers retournés sont : ' . var_export($headers, 1)
                );
            }

            throw $e;
        }
    }

    /**
     * Si le cookie 'no-design' n'existe pas et qu'il existe une PlateformeVirtuelle correspondant au domaine,
     * on redirige vers la page demandée (pour prise en compte du cookie 'design'.
     *
     * @runInSeparateProcess
     */
    public function testRedirectsToSamePage()
    {
        $domain = self::DOMAIN2;

        $server = [
            'HTTPS' => 'on',
            'HTTP_HOST' => $domain,
            'QUERY_STRING' => 'AllCons&page=Entreprise.EntrepriseAdvancedSearch',
        ];

        $requestStack = $this->prepareRequestStack([], $server);

        $plateformeVirtuelle = new PlateformeVirtuelle();
        $plateformeVirtuelle->setDomain($domain);
        $plateformeVirtuelle->setCodeDesign('code1');

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($this->getPfvRepoMock($plateformeVirtuelle));

        $this->expectException(Exception::class);

        try {
            $this->callService($requestStack, $em);
        } catch (Exception $e) {
            $headers = xdebug_get_headers();

            $this->assertContains(
                'Status: 302 Moved Temporarily',
                $headers
            );

            $this->assertContains(
                'Location: https://' . $domain . '/?AllCons=&page=Entreprise.EntrepriseAdvancedSearch',
                $headers
            );

            throw $e;
        }
    }
}
