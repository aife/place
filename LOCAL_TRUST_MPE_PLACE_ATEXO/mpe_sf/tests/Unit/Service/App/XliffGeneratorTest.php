<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\App;

use App\Service\App\XliffGenerator;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SimpleXMLElement;

class XliffGeneratorTest extends TestCase
{
    const DEFAULT_XML = '<?xml version="1.0" encoding="utf-8"?>'.
    '<xliff version="1.2">'.
    '<file datatype="plaintext" original="file.ext" source-language="EN" target-language="EN-EN">'.
    '<body>%s</body></file></xliff>';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected function setUp(): void
    {
        parent::setUp();

        $this->logger = new NullLogger();
    }

    protected function assertEqualsXml($expected, SimpleXMLElement $actual)
    {
        $expected = (new SimpleXMLElement($expected))->asXML();
        $this->assertEquals($expected, $actual->asXML());
    }

    protected function assertXmlContainsExpected($expectedArray, SimpleXMLElement $actual)
    {
        foreach ($expectedArray as $expected) {
            $this->assertStringContainsString($expected, $actual->asXml());
        }
    }

    protected function getSut($logger = null)
    {
        $logger = ($logger) ?? $this->logger;

        return new XliffGenerator($logger);
    }

    protected function generateXmlFromData($data, $logger = null)
    {
        return $this->getSut($logger)->getXliff($data, 'en');
    }

    public function testGenerateXmlWithEmptyData()
    {
        $data = [];

        $expected = sprintf(self::DEFAULT_XML, '');
        $xml = $this->generateXmlFromData($data);

        $this->assertEquals(SimpleXMLElement::class, get_class($xml));
        $this->assertEqualsXml($expected, $xml);
    }

    public function testGenerateXmlWithCorrectData()
    {
        $data = [
            'TOUS_LES_STATUS' => '--- All status ---',
            'TEXT_ACHETEURS_PUBLICS' => 'Public Purchasers',
        ];

        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $loggerMock->expects($this->never())->method('debug');

        $xml = $this->generateXmlFromData($data, $loggerMock);

        $expectedArray = [
            '<trans-unit id="TOUS_LES_STATUS"><source>TOUS_LES_STATUS</source>'
            .'<target>--- All status ---</target></trans-unit>',
            '<trans-unit id="TEXT_ACHETEURS_PUBLICS"><source>TEXT_ACHETEURS_PUBLICS</source>'
            .'<target>Public Purchasers</target></trans-unit>',
        ];

        $this->assertXmlContainsExpected($expectedArray, $xml);
    }

    public function testGenerateXmlWithSpecialCharacters()
    {
        $data = [
            'TEXT_SUPERIEUR_90000_EURO_HT' => '> 20 000 EUR VAT excl.',
            'RECOMMANDATION_NOMMAGE_FICHIERS' => 'Avoid special characters (eg: °, &amp;, #) in the name of the files.',
            'HTML_CONTENT' => 'This is <strong>strong</strong>',
            'HTML_CONTENT_WITH_URL' => 'This is <a href="/anywhere?param1=val&amp;p2=v">a link</a>',
        ];

        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $loggerMock->expects($this->never())->method('debug');

        $xml = $this->generateXmlFromData($data);

        $expectedArray = [
            '<trans-unit id="TEXT_SUPERIEUR_90000_EURO_HT"><source>TEXT_SUPERIEUR_90000_EURO_HT</source>'
            .'<target>&gt; 20 000 EUR VAT excl.</target></trans-unit>',
            '<trans-unit id="RECOMMANDATION_NOMMAGE_FICHIERS"><source>RECOMMANDATION_NOMMAGE_FICHIERS</source>'
            .'<target>Avoid special characters (eg: °, &amp;, #) in the name of the files.</target></trans-unit>',
            '<trans-unit id="HTML_CONTENT"><source>HTML_CONTENT</source>'
            .'<target>This is &lt;strong&gt;strong&lt;/strong&gt;</target></trans-unit>',
            '<trans-unit id="HTML_CONTENT_WITH_URL"><source>HTML_CONTENT_WITH_URL</source>'
            .'<target>This is &lt;a href="/anywhere?param1=val&amp;p2=v"&gt;a link&lt;/a&gt;</target></trans-unit>',
        ];
        $this->assertXmlContainsExpected($expectedArray, $xml);
    }

    public function testGenerateXmlWithBadCharacters()
    {
        $data = [
            'RECOMMANDATION_NOMMAGE_FICHIERS' => 'Avoid special characters (eg: °, & , #) in the name of the files.',
            'HTML_CONTENT_WITH_URL' => 'This is <a href="/anywhere?param1=anything&param2=value">a link</a>',
            'DEFINE_ACCEPTATION_REGLEMENT_AGENT' => 'I acknowledge having read the &lt; a title="CGU Link (new window)"
                    href="index.php?page=Agent.ConditionsUtilisation&calledFrom=agent"&gt;
                    general conditions of use &lt; / a &gt; Of the dematerialization platform and I accept them.',
            'TEXT_AIDE_ASSISTANCE_ENTREPRISE' => 'Un service de support téléphonique est mis à disposition des entreprises souhaitant soumissionner aux marchés publics.<br /><br /> Ce service s\'adresse aux personnes familières de l\'utilisation des outils bureautiques en général (Explorateur Windows, manipulation de fichiers, dossiers ZIP, etc.) et d\'Internet en particulier.<br /> Avant de contacter l\'assistance téléphonique, assurez-vous d\'avoir téléchargé et consulté <a href="index.php?page=entreprise.EntrepriseGuide&Aide" target="_blank" >les guides mis à votre disposition dans la rubrique « Aide » </a>. <br /><br /> Le service de support est ouvert de 9h00 à 19h00 les jours ouvrés. Pour joindre nos équipes support, nous vous invitons à créer un ticket d\'assistance en ligne.',
        ];

        $loggerMock = $this->getMockBuilder(LoggerInterface::class)->getMock();
        $loggerMock->expects($this->exactly(count($data)))->method('debug');

        $xml = $this->generateXmlFromData($data, $loggerMock);

        $expectedArray = [];

        foreach (array_keys($data) as $key) {
            $expectedArray[] = "<trans-unit id=\"$key\"><source>$key</source><target>$key</target></trans-unit>";
        }
        $this->assertXmlContainsExpected($expectedArray, $xml);
    }

    public function testIsValidXml()
    {
        /** @var XliffGenerator $sut */
        $sut = $this->getSut();

        $validXmlArray = [
            'This is <a href="/anywhere?param1=anything&amp;param2=value">a link</a>',
            '<b>Lorem</b> ipsum',
            '<b>Lorem</b> ipsum<br>',
            '<b>Attention : l\'Acte d\'Engagement n\'a pas été renseigné dans le champ dédié à cet effet.</b>
                    <br>Assurez-vous que ce dernier a bien été joint à votre réponse avant de la soumettre.',
        ];
        $invalidXmlArray = [
            'Avoid special characters (eg: °, & , #) in the name of the files.',
            'I acknowledge having read the &lt; a title="CGU Link (new window)"
                    href="index.php?page=Agent.ConditionsUtilisation&calledFrom=agent"&gt;
                    general conditions of use &lt; / a &gt; Of the dematerialization platform and I accept them.',
        ];

        foreach ($validXmlArray as $xml) {
            $this->assertTrue($sut->isValidXml('key', $xml), "This should be valid : '$xml'");
        }

        foreach ($invalidXmlArray as $xml) {
            $this->assertFalse($sut->isValidXml('key', $xml), "This should be invalid : '$xml'");
        }
    }
}
