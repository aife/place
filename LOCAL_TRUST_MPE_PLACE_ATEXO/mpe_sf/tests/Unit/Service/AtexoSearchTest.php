<?php

namespace Tests\Unit\Service;

use DateTime;
use App\Service\AtexoSearch;
use App\Service\AtexoUtil;
use App\Service\Messagerie\MessagerieService;
use App\Utils\AtexoConsultationCriteriaVo;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AtexoUtilTest.
 */
class AtexoSearchTest extends TestCase
{
    private $entityManager;
    private $container;
    private $logMock;
    private $sessionMock;
    private $service;
    private $serviceUtils;
    private $translatorMock;
    private $criteriaVO;
    private $requestStackMock;
    private $messagerieServiceMock;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->container = $this->createMock(Container::class);
        $this->logMock = $this->createMock(Logger::class);
        $this->sessionMock = $this->createMock(Session::class);
        $this->translatorMock = $this->createMock(TranslatorInterface::class);
        $this->criteriaVO = $this->createMock(AtexoConsultationCriteriaVo::class);
        $this->requestStackMock = $this->createMock(RequestStack::class);
        $this->messagerieServiceMock = $this->createMock(MessagerieService::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);


        $this->criteriaVO->expects($this->any())
            ->method('getConsultationAArchiver')
            ->willReturn(true);
        $this->messagerieServiceMock->expects($this->any())
            ->method('getlistConsultationsPanierWithEchangeMessec2')
            ->willReturn('');

        $this->criteriaVO->expects($this->any())
            ->method('getcalledFromPortail')
            ->willReturn(false);

        $this->container
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($params) {
                        $value = null;

                        if ('DUREE_RAPPEL_ARCHIVAGE' === $params) {
                            $value = 2;
                        }

                        if ('STATUS_DECISION' === $params) {
                            $value = '4';
                        }

                        if ('TYPE_AVIS_CONSULTATION' === $params) {
                            $value = 3;
                        }

                        if ('STATUS_DECISION' === $params) {
                            $value = '4';
                        }

                        if ('CONSULTATION_DEPOUIABLE_PHASE_1' === $params) {
                            $value = '1';
                        }

                        if ('STATUS_OUVERTURE_ANALYSE' === $params) {
                            $value = '4';
                        }

                        if ('STATUS_CONSULTATION' === $params) {
                            $value = '2';
                        }

                        if ('STATUS_PREPARATION' === $params) {
                            $value = '1';
                        }

                        if ('STATUS_ELABORATION' === $params) {
                            $value = '0';
                        }

                        if ('STATUS_A_ARCHIVER' === $params) {
                            $value = '5';
                        }

                        if ('STATUS_ARCHIVE_REALISEE' === $params) {
                            $value = '6';
                        }

                        if ('CONSULTATION_DEPOUIABLE_PHASE_0' === $params) {
                            $value = '0';
                        }

                        if ('CONSULTATION_DEPOUIABLE_PHASE_1' === $params) {
                            $value = '1';
                        }

                        if ('STATUS_APRES_DECISION' === $params) {
                            $value = '7';
                        }

                        if ('STATUS_AVANT_ARCHIVE' === $params) {
                            $value = '8';
                        }

                        if ('STATUS_JUSTE_DECISION' === $params) {
                            $value = '-4';
                        }

                        return $value;
                    }
                )
            );

        $this->serviceUtils = new AtexoUtil(
            $this->container,
            $this->sessionMock,
            $this->translatorMock,
            $this->logMock,
            $this->requestStackMock,
            $parameterBagMock
        );

        $this->service = new AtexoSearch(
            $this->container,
            $this->entityManager,
            $this->logMock,
            $this->serviceUtils,
            $this->messagerieServiceMock,
            $this->sessionMock
        );
    }

    public function testSupprimerDernierCaractere()
    {
        $string = '';
        $chaine = '';
        $result = $this->service->supprimerDernierCaractere($chaine);

        $this->assertEquals($string, $result);
    }

    public function testGetSqlCountAgent()
    {
        $queryParams = ['param1' => true];
        $string = " AND ( 
DEPOUILLABLE_PHASE_CONSULTATION = '0'
AND ID_TYPE_AVIS = :type_avis_consultation

AND id_type_procedure_org NOT IN (
SELECT
	Type_Procedure_Organisme.id_type_procedure
FROM
	Type_Procedure_Organisme
WHERE
	Type_Procedure_Organisme.organisme = :organismeAgent
	AND (( Type_Procedure_Organisme.sad = '1') OR (Type_Procedure_Organisme.accord_cadre ='1'))

)  ) AND (  
 (
ID_ETAT_CONSULTATION = 0
AND ( DATEFIN > CURDATE() OR DATEFIN = '0000-00-00 00:00:00')
AND (
	 DATE_MISE_EN_LIGNE_CALCULE = ''
	 OR DATE_MISE_EN_LIGNE_CALCULE = '0000-00-00 00:00:00'
	 OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()
	 OR DATE_MISE_EN_LIGNE_CALCULE IS NULL
)
AND DATEDEBUT != '' AND DATEDEBUT != '0000-00-00 00:00:00'
AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH )
 )  
OR (
	ID_ETAT_CONSULTATION = 0 AND DATEFIN != ''
	AND DATEFIN != '0000-00-00 00:00:00'
	AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH)
)  
OR (
	ID_ETAT_CONSULTATION = :status_decision
	AND  DATE_DECISION != ''
	AND DATE_DECISION != '0000-00-00 00:00:00'
	AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL :duree_rappel_archivage MONTH)
 )  )";

        $result = $this->service->getSqlConsultationAArchiver(
            $this->criteriaVO,
            $queryParams
        );
        $this->assertEquals($string, $result);

        $resultParams = [
            'param1' => true,
            'duree_rappel_archivage' => 2,
            'status_decision' => 4,
            'type_avis_consultation' => 3,
        ];
        $this->assertEquals($resultParams, $queryParams);
    }

    public function testGetSqlExclureConsultationsSadAc()
    {
        $string = '
AND id_type_procedure_org NOT IN (
SELECT
	Type_Procedure_Organisme.id_type_procedure
FROM
	Type_Procedure_Organisme
WHERE
	Type_Procedure_Organisme.organisme = :organismeAgent
	AND (( Type_Procedure_Organisme.sad = \'1\') OR (Type_Procedure_Organisme.accord_cadre =\'1\'))

)';
        $result = $this->service->getSqlExclureConsultationsSadAc();
        $this->assertEquals($string, $result);
    }

    public function testGetSqlConditionPreparation()
    {
        $string = '
 (
ID_ETAT_CONSULTATION = 0
AND ( DATEFIN > CURDATE() OR DATEFIN = \'0000-00-00 00:00:00\')
AND (
	 DATE_MISE_EN_LIGNE_CALCULE = \'\'
	 OR DATE_MISE_EN_LIGNE_CALCULE = \'0000-00-00 00:00:00\'
	 OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()
	 OR DATE_MISE_EN_LIGNE_CALCULE IS NULL
)
AND DATEDEBUT != \'\' AND DATEDEBUT != \'0000-00-00 00:00:00\'
AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH )
 )';
        $result = $this->service->getSqlConditionPreparation();
        $this->assertEquals($string, $result);
    }

    public function testGetSqlConditionOuvertureAnalyse()
    {
        $string = '
OR (
	ID_ETAT_CONSULTATION = 0 AND DATEFIN != \'\'
	AND DATEFIN != \'0000-00-00 00:00:00\'
	AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH)
)';
        $result = $this->service->getSqlConditionOuvertureAnalyse();
        $this->assertEquals($string, $result);
    }

    public function testGetSqlConditionDecision()
    {
        $sting = '
OR (
	ID_ETAT_CONSULTATION = :status_decision
	AND  DATE_DECISION != \'\'
	AND DATE_DECISION != \'0000-00-00 00:00:00\'
	AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL :duree_rappel_archivage MONTH)
 )';
        $result = $this->service->getSqlConditionDecision();
        $this->assertEquals($sting, $result);
    }

    public function testGetSqlStatusDecision()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('4');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('4');

        $string = ' AND ( consultation.id_etat_consultation=:EtatConsultation OR ' .
            '( depouillable_phase_consultation=:phaseSAD AND NOT ( ( ( datefin>now() ' .
            'OR datefin = \'0000-00-00 00:00:00\' ) AND  (date_mise_en_ligne_calcule=\'\' ' .
            'OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = \'0000-00-00 00:00:00\' ' .
            'OR date_mise_en_ligne_calcule>now()) ) )))';
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL " .
            "OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation' => '4',
            'phaseSAD' => '1',
        ];
        $result = $this->service->getSqlStatusDecision(
            $this->criteriaVO,
            $conditionEtatPreparation,
            $queryParams
        );
        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusOuvertureAnalyse()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('4');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('4');

        $string = ' AND ( (consultation.id_etat_consultation=:EtatConsultation ' .
            'OR (consultation.id_etat_consultation=:Decision AND   ( datefin <= now()  ' .
            'AND datefin != \'0000-00-00 00:00:00\')  ) ) OR ( depouillable_phase_consultation=:phaseSAD ' .
            'AND NOT (  ( ( datefin>now() OR datefin = \'0000-00-00 00:00:00\' ) ' .
            'AND  (date_mise_en_ligne_calcule=\'\'' .
            ' OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = \'0000-00-00 00:00:00\' ' .
            'OR date_mise_en_ligne_calcule>now()) )   ) ' .
            'AND (datefin<=now() AND datefin != \'0000-00-00 00:00:00\') ) )';
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL " .
            "OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $result = $this->service->getSqlStatusOuvertureAnalyse(
            $this->criteriaVO,
            $conditionEtatPreparation,
            $queryParams
        );
        $this->assertEquals($string, $result);
    }

    public function testGetSqlStatusConsultationPeriodiciteIsNull()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getPeriodicite')
            ->willReturn(null);

        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('2');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('2');

        $string = ' AND ( datefin > now() AND consultation.id_etat_consultation=\'0\' ' .
            'AND (  (date_mise_en_ligne_calcule!=\'\' AND date_mise_en_ligne_calcule IS NOT NULL ' .
            'AND date_mise_en_ligne_calcule != \'0000-00-00 00:00:00\'  AND date_mise_en_ligne_calcule<=now())) )';
        $result = $this->service->getSqlStatusConsultation($this->criteriaVO);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlStatusConsultationPeriodiciteIsNotNull()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getPeriodicite')
            ->willReturn(3);

        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('2');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('2');

        $todayDate = (new DateTime())->format('Y-m-d');
        $threeDaysAgoDate = (new DateTime('-3 day'))->format('Y-m-d') . ' 00:00:00';
        $string = ' AND ( datefin > now() AND consultation.id_etat_consultation=\'0\' ' .
            'AND (  date_mise_en_ligne_calcule <\'' . $todayDate . '\'  AND date_mise_en_ligne_calcule >=  \''
            . $threeDaysAgoDate . '\'  AND date_mise_en_ligne_calcule!=\'\' ' .
            'AND date_mise_en_ligne_calcule IS NOT NULL ' .
            'AND date_mise_en_ligne_calcule != \'0000-00-00 00:00:00\' ))';
        $result = $this->service->getSqlStatusConsultation($this->criteriaVO);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlStatusPreparation()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('1');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('1');

        $string = ' AND  ( ( datefin>now() OR datefin = \'0000-00-00 00:00:00\' ) ' .
            'AND  (date_mise_en_ligne_calcule=\'\' ' .
            'OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = \'0000-00-00 00:00:00\' ' .
            'OR date_mise_en_ligne_calcule>now()) )  AND consultation.id_etat_consultation=\'0\' ' .
            'AND consultation.etat_en_attente_validation =\'1\' ';

        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL " .
            "OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $result = $this->service->getSqlStatusPreparation($this->criteriaVO, $conditionEtatPreparation);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlStatusElaboration()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('0');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('0');

        $string = ' AND  ( ( datefin>now() OR datefin = \'0000-00-00 00:00:00\' ) ' .
            'AND  (date_mise_en_ligne_calcule=\'\' ' .
            'OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = \'0000-00-00 00:00:00\' ' .
            'OR date_mise_en_ligne_calcule>now()) )  AND consultation.id_etat_consultation=\'0\' ' .
            'AND consultation.etat_en_attente_validation =\'0\' ';
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL " .
            "OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $result = $this->service->getSqlStatusElaboration($this->criteriaVO, $conditionEtatPreparation);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlStatusAArchiver()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('5');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('5');

        $string = ' AND consultation.id_etat_consultation=:EtatConsultation ' .
            'AND depouillable_phase_consultation=:notSad  ';
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation' => '5',
            'notSad' => '0',
        ];
        $queryParams['EtatConsultation'] = $this->container->getParameter('STATUS_A_ARCHIVER');
        $queryParams['notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
        $result = $this->service->getSqlStatusAArchiver($this->criteriaVO, $queryParams);

        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusArchiveRealisee()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('6');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('6');

        $string = ' AND consultation.id_etat_consultation=:EtatConsultation ' .
            'AND depouillable_phase_consultation=:notSad  ';
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation' => '6',
            'notSad' => '0',
        ];

        $result = $this->service->getSqlStatusArchiveRealisee($this->criteriaVO, $queryParams);
        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusApresDecision()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('7');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('7');

        $string = ' AND (consultation.id_etat_consultation>=:EtatConsultation1 ' .
            'AND consultation.id_etat_consultation<=:EtatConsultation2) AND depouillable_phase_consultation=:notSad ';
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation1' => '4',
            'EtatConsultation2' => '6',
            'notSad' => '0',
        ];
        $result = $this->service->getSqlStatusApresDecision($this->criteriaVO, $queryParams);
        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusAvantArchiveNotVisionRma()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('8');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('8');

        $this->criteriaVO->expects($this->any())
            ->method('getVisionRma')
            ->willReturn(false);

        $string = ' AND ( consultation.id_etat_consultation<=:EtatConsultation1 ) ';
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL '.
        'OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation1' => '4',
        ];
        $result = $this->service->getSqlStatusAvantArchive(
            $this->criteriaVO,
            $conditionEtatPreparation,
            $queryParams
        );

        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusAvantArchiveVisionRma()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('8');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('8');

        $this->criteriaVO->expects($this->any())
            ->method('getVisionRma')
            ->willReturn(true);

        $string = ' AND ( consultation.id_etat_consultation<=:EtatConsultation1 )  AND NOT (  ( ( datefin>now() ' .
            'OR datefin = \'0000-00-00 00:00:00\' ) AND  (date_mise_en_ligne_calcule=\'\' ' .
            'OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = \'0000-00-00 00:00:00\' ' .
            'OR date_mise_en_ligne_calcule>now()) )   ) ';
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL " .
            "OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation1' => '4',
        ];
        $result = $this->service->getSqlStatusAvantArchive(
            $this->criteriaVO,
            $conditionEtatPreparation,
            $queryParams
        );

        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }

    public function testGetSqlStatusJusteDecision()
    {
        $this->criteriaVO->expects($this->any())
            ->method('getEtatConsultation')
            ->willReturn('-4');

        $this->criteriaVO->expects($this->any())
            ->method('getSecondConditionOnEtatConsultation')
            ->willReturn('-4');

        $string = ' AND consultation.id_etat_consultation=:EtatConsultation1 ' .
            'AND depouillable_phase_consultation=:notSad ';
        $queryParams = ['data' => 'data'];
        $queryParamsFinal = [
            'data' => 'data',
            'EtatConsultation1' => '4',
            'notSad' => '0',
        ];

        $result = $this->service->getSqlStatusJusteDecision($this->criteriaVO, $queryParams);

        $this->assertEquals($string, $result);
        $this->assertEquals($queryParamsFinal, $queryParams);
    }
}
