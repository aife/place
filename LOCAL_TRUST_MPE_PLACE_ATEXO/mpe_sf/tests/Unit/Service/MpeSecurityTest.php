<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use DateTime;
use Exception;
use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEntreprise;
use App\Service\MpeSecurity;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MpeSecurityTest extends TestCase
{
    /**
     * @var MpeSecurity
     */
    private $mpeSecurity;

    public function isUserActiveDependingOnTentativesMdpDataProvider(): array
    {
        return [
            [0, true],
            [2, true],
            [3, false],
            [5, false],
        ];
    }

    /**
     * @dataProvider isUserActiveDependingOnTentativesMdpDataProvider
     */
    public function testIsUserActiveDependingOnTentativesMdp($nbTentatives, $expectedResult)
    {
        $user = new Inscrit();
        $user->setTentativesMdp($nbTentatives);

        $result = $this->mpeSecurity->isUserActive($user);
        $this->assertEquals($expectedResult, $result);
    }

    public function isUserActiveDependingOnBloqueDataProvider(): array
    {
        return [
            ['0', true],
            ['1', false],
        ];
    }

    /**
     * @dataProvider isUserActiveDependingOnBloqueDataProvider
     */
    public function testIsUserActiveDependingOnBloque($bloque, $expectedResult)
    {
        $user = new Inscrit();
        $user->setTentativesMdp(0);
        $user->setBloque($bloque);

        $this->assertEquals($expectedResult, $this->mpeSecurity->isUserActive($user));
    }

    public function isUserActiveDependingOnActifDataProvider(): array
    {
        return [
            ['0', false],
            ['1', true],
        ];
    }

    /**
     * @dataProvider isUserActiveDependingOnActifDataProvider
     */
    public function testIsUserActiveDependingOnActif($actif, $expectedResult)
    {
        $user = new Agent();
        $user->setTentativesMdp(0);
        $user->setActif($actif);

        $this->assertEquals($expectedResult, $this->mpeSecurity->isUserActive($user));
    }

    public function testIsUserActiveDependingOnDeleted()
    {
        $user = new Agent();
        $user->setTentativesMdp(0);
        $user->setActif('1');
        $this->assertTrue(
            $this->mpeSecurity->isUserActive($user),
            'Un Agent dont la date deletedAt est null peut être actif.'
        );

        $user->setDeletedAt(new DateTime('2 days ago'));
        $this->assertFalse(
            $this->mpeSecurity->isUserActive($user),
            'Un Agent dont la date deletedAt n\'est pas null est inactif.'
        );
    }

    public function incrementTentativesMdpDataProvider(): array
    {
        return [
            [0, 1],
            [2, 3],
            [3, 3],
        ];
    }

    /**
     * @dataProvider incrementTentativesMdpDataProvider
     * @param int $initialValue
     * @param int $expected
     */
    public function testIncrementTentativesMdp(int $initialValue, int $expected)
    {
        $user = new Agent();
        $user->setTentativesMdp($initialValue);

        $this->mpeSecurity->incrementTentativesMdp($user);

        $this->assertEquals($expected, $user->getTentativesMdp());
    }

    public function testResetTentativesMdp()
    {
        $user = new Agent();
        $user->setTentativesMdp(3);

        $this->mpeSecurity->resetTentativesMdp($user);

        $this->assertEquals(0, $user->getTentativesMdp());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->mpeSecurity = new MpeSecurity(...$this->getMocks());
    }

    protected function getMocks(): array
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->with('MAX_TENTATIVES_MDP')
            ->willReturn(3);

        $em = $this->createMock(EntityManagerInterface::class);

        $cp = new ConfigurationPlateforme();
        $cp->setAuthenticateAgentByLogin(1);
        $cp->setAuthenticateInscritByLogin(1);

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $em->expects($this->any())->method('getRepository')->willReturn($repo);

        return [
            $em,
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $parameterBag
        ];
    }

    public function testIsTechnicalAgent()
    {
        $user = new Agent();
        $user->setTechnique(true);

        $mpeSecurity = new MpeSecurity(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ParameterBagInterface::class)
        );

        $result = $mpeSecurity->isTechnicalAgent($user);

        $this->assertTrue($result);
    }

    public function testIsNotTechnicalAgent()
    {
        $user = new Inscrit();

        $mpeSecurity = new MpeSecurity(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ParameterBagInterface::class)
        );

        $result = $mpeSecurity->isTechnicalAgent($user);

        $this->assertFalse($result);
    }

    public function testUpdateConnectionTime()
    {
        $user = new Agent();
        $timeBeforeUpdate = new DateTime();
        $this->mpeSecurity->updateConnectionTime($user);

        $this->assertNotNull($user->getDateConnexion());
        $this->assertGreaterThan($timeBeforeUpdate, $user->getDateConnexion());
    }

    public function testUpdateConnectionTimeForInscrit()
    {
        try {
            $user = new Inscrit();
            $this->mpeSecurity->updateConnectionTime($user);

            $this->assertTrue(true);
        } catch (Exception $exception) {
            $this->fail('Update last connection time for agent thrown exception');
        }
    }
}
