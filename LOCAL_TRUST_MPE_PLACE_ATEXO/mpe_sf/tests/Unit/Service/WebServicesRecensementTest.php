<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service;

use App\Entity\Agent\Habilitations\RecensementProgrammationStrategieAchat;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\AgentRepository;
use App\Security\ClientOpenIdConnect;
use App\Service\AbstractService;
use App\Service\AgentTechniqueTokenService;
use App\Service\CpvService;
use App\Service\OrganismeService;
use App\Service\WebServicesRecensement;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesRecensementTest extends TestCase
{
    /**
     * @var Agent
     */
    private $agent;

    protected function setUp(): void
    {
        $this->agent = new Agent();
        $organisme = $this->createMock(Organisme::class);
        $service = $this->createMock(Service::class);

        $this->agent->setId(1);
        $this->agent->setLogin('testAdmin');
        $this->agent->setAcronymeOrganisme('pmi-min-1');
        $this->agent->setOrganisme($organisme);
        $h = $this->createMock(HabilitationAgent::class);
        $h->method('getRecensementProgrammationStrategieAchat')
            ->willReturn(new RecensementProgrammationStrategieAchat());
        $this->agent->setService($service)->setHabilitation($h);
    }
    public function testGetUrlRecensementWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://recensement-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $tokenManager = $this->createMock(JWTTokenManagerInterface::class);
        $tokenManager->method('create')->willReturn('a token');
        $repository = $this->createMock(AgentRepository::class);
        $organismeService = $this->createMock(OrganismeService::class);
        $repository->method('findAgentTechniqueByLogin')
            ->willReturn($this->agent);
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF", "refresh_token": "AZERYUUUVVV"}', ['http_code' => 200]),
            new MockResponse('h234DSEZ432', ['http_code' => 200])
        ]);
        $service = new WebServicesRecensement($parameterBag, $client, $loger, $em, $organismeService, $tokenManager);
        $result = $service->getContent('getUrlRecensement', $this->agent);

        $this->assertEquals(
            'https://recensement-release.local-trust.com/login/FDSERT43HYTD5432SDPMLHRTDSFF/h234DSEZ432',
            $result
        );
    }

    public function testGetUrlRecensementWith500()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://recensement-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $tokenManager = $this->createMock(JWTTokenManagerInterface::class);
        $tokenManager->method('create')->willReturn('a token');
        $repository = $this->createMock(AgentRepository::class);
        $repository->method('findAgentTechniqueByLogin')
            ->willReturn($this->agent);
        $em = $this->createMock(EntityManagerInterface::class);
        $organismeService = $this->createMock(OrganismeService::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 500]),
            new MockResponse('h234DSEZ432', ['http_code' => 200])
        ]);

        $this->expectException(ServerException::class);
        $service = new WebServicesRecensement($parameterBag, $client, $loger, $em, $organismeService, $tokenManager);
        $result = $service->getContent('getUrlRecensement', $this->agent);
    }
}
