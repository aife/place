<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Entity\Consultation;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Entity\TListeLotsCandidature;
use App\Exception\ConsultationNotAuthorizedException;
use App\Exception\UidNotFoundException;
use App\Service\AtexoConfiguration;
use App\Service\CandidatureService;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use App\Service\Dume\DumeService;
use App\Service\InscritOperationsTracker;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use TypeError;

class ConsultationDepotServiceTest extends TestCase
{
    /** @var ParameterBagInterface|MockObject */
    private $parameterBag;

    /** @var SessionInterface|MockObject */
    private $session;

    /** @var EntityManagerInterface|MockObject */
    private $entityManager;

    /** @var TranslatorInterface|MockObject */
    private $translator;

    /** @var LoggerInterface|MockObject */
    private $logger;

    /** @var InscritOperationsTracker|MockObject */
    private $inscritOperationTracker;

    /** @var AtexoConfiguration|MockObject */
    private $moduleStateChecker;

    /** @var CandidatureService|MockObject */
    private $candidatureService;

    /** @var DumeService|MockObject */
    private $dumeService;

    /** @var DossierVolumineuxService|MockObject */
    private $dossiersVolumineuxService;

    /** @var ConsultationDepotService|MockObject */
    private $consultationDepotService;

    private function initMocks()
    {
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->session = $this->createMock(SessionInterface::class);
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->translator->expects($this->any())->method('trans')->willReturn('a translation');
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $this->moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $this->candidatureService = $this->createMock(CandidatureService::class);
        $this->dumeService = $this->createMock(DumeService::class);
        $this->dossiersVolumineuxService = $this->createMock(DossierVolumineuxService::class);
    }

    private function iniConsultationDepotService()
    {
        $this->consultationDepotService = new ConsultationDepotService(
            $this->parameterBag,
            $this->session,
            $this->moduleStateChecker,
            $this->translator,
            $this->logger,
            $this->entityManager,
            $this->candidatureService,
            $this->dumeService,
            $this->dossiersVolumineuxService,
            $this->inscritOperationTracker
        );
    }

    public function testCheckRepondreApresClotureNullArgument()
    {
        $this->initMocks();
        $this->iniConsultationDepotService();
        $this->expectException(TypeError::class);
        $this->consultationDepotService->checkRepondreApresCloture(null);
    }

    public function testCheckRepondreApresCloture()
    {
        // Cas 1 : module actif
        $this->initMocks();
        $this->moduleStateChecker->expects($this->any())
            ->method('hasConfigPlateforme')
            ->with('entrepriseRepondreConsultationApresCloture')
            ->willReturn(true);
        $this->iniConsultationDepotService();
        $consultation = new Consultation();
        $this->assertTrue($this->consultationDepotService->checkRepondreApresCloture($consultation));

        // Cas 2 : module inactif et date de fin > now
        $this->initMocks();
        $this->moduleStateChecker->expects($this->any())
            ->method('hasConfigPlateforme')
            ->with('entrepriseRepondreConsultationApresCloture')
            ->willReturn(false);
        $this->iniConsultationDepotService();
        $consultation = new Consultation();
        $consultation->setDatefin((new DateTime())->modify("+10 day"));
        $this->assertTrue($this->consultationDepotService->checkRepondreApresCloture($consultation));

        // Cas 3 : module inactif et date de fin < now
        $this->initMocks();
        $this->moduleStateChecker->expects($this->any())
            ->method('hasConfigPlateforme')
            ->with('entrepriseRepondreConsultationApresCloture')
            ->willReturn(false);
        $this->iniConsultationDepotService();
        $consultation = new Consultation();
        $consultation->setDatefin((new DateTime())->modify("-10 day"));
        $this->expectException(ConsultationNotAuthorizedException::class);
        $this->consultationDepotService->checkRepondreApresCloture($consultation);
    }

    public function testCheckConsultationRestreinteNullArgument()
    {
        $this->initMocks();
        $this->iniConsultationDepotService();
        $this->expectException(TypeError::class);
        $this->consultationDepotService->checkConsultationRestreinte(null);
    }

    public function testCheckConsultationRestreinte()
    {
        $this->initMocks();
        $this->parameterBag->expects($this->exactly(3))
            ->method('get')
            ->with('type_acces_procedure')
            ->willReturn(['restreinte' => '2']);
        $this->session->expects($this->any())
            ->method('get')
            ->with('contexte_authentification_1')
            ->willReturn(['codeAcces' => '']);
        $this->iniConsultationDepotService();

        // Cas 1 - ok
        $consultation = new Consultation();
        $consultation->setTypeAcces('1');
        $this->assertTrue($this->consultationDepotService->checkConsultationRestreinte($consultation));

        // Cas 2 - ok
        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setTypeAcces('2')
            ->setCodeProcedure('');
        $this->assertTrue($this->consultationDepotService->checkConsultationRestreinte($consultation));

        // Cas 3 - Exception
        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setTypeAcces('2')
            ->setCodeProcedure('mock');
        $this->expectException(ConsultationNotAuthorizedException::class);
        $this->consultationDepotService->checkConsultationRestreinte($consultation);
    }

    public function testCreateOffreBadUid()
    {
        $this->initMocks();
        $this->iniConsultationDepotService();
        $this->expectException(UidNotFoundException::class);
        $this->consultationDepotService->createOffre(new Inscrit(), new Consultation(), '');
    }

    public function testCreateOffre()
    {
        $this->initMocks();
        $this->parameterBag->expects($this->once())
            ->method('get')
            ->with('STATUT_ENV_BROUILLON')
            ->willReturn(1);
        $this->iniConsultationDepotService();
        $offre = $this->consultationDepotService->createOffre(new Inscrit(), new Consultation(), '1234');
        $this->assertInstanceOf(Offre::class, $offre);
    }

    public function testUpdateTypeCandidatureAndRelationListLotCandidature()
    {
        $this->initMocks();
        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if ($repository == TCandidature::class) {
                        $repository = $this->createMock(ObjectRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(new TCandidature());
                    } elseif ($repository == TListeLotsCandidature::class) {
                        $repository = $this->createMock(ObjectRepository::class);
                        $repository->expects($this->any())
                            ->method('findBy')
                            ->willReturn([
                                new TListeLotsCandidature(),
                                new TListeLotsCandidature(),
                            ]);
                    }
                    return $repository;
                }
            );
        $this->iniConsultationDepotService();
        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $consultation = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };

        $candidature = $this->consultationDepotService->updateTypeCandidatureAndRelationListLotCandidature(
            $consultation,
            $inscrit,
            'phase',
            'choice'
        );
        $this->assertInstanceOf(TCandidature::class, $candidature);

        $this->initMocks();
        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if ($repository == TCandidature::class) {
                        $repository = $this->createMock(ObjectRepository::class);
                        $repository->expects($this->any())
                            ->method('findOneBy')
                            ->willReturn(null);
                    } elseif ($repository == TListeLotsCandidature::class) {
                        $repository = $this->createMock(ObjectRepository::class);
                        $repository->expects($this->any())
                            ->method('findBy')
                            ->willReturn(null);
                    }
                    return $repository;
                }
            );
        $this->iniConsultationDepotService();
        $candidature = $this->consultationDepotService->updateTypeCandidatureAndRelationListLotCandidature(
            $consultation,
            $inscrit,
            'phase',
            'choice'
        );
        $this->assertInstanceOf(TCandidature::class, $candidature);
    }

    public function testGetDossierVolumineuxEnvolDisabled(): void
    {
        $parameter = $this->createMock(ParameterBagInterface::class);
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $candidatureService = $this->createMock(CandidatureService::class);
        $dumeService = $this->createMock(DumeService::class);
        $dossiersVolumineuxService = $this->createMock(DossierVolumineuxService::class);

        $consultationDepotService = new ConsultationDepotService(
            $parameter,
            $session,
            $moduleStateChecker,
            $translator,
            $logger,
            $entityManager,
            $candidatureService,
            $dumeService,
            $dossiersVolumineuxService,
            $inscritOperationTracker
        );

        $consultation = new Consultation();
        $consultation->setEnvolActivation(false);

        $dossiersVolumineuxService
            ->expects($this->never())
            ->method('getActiveDVforCurrentUser')
        ;

        $this->assertSame($consultationDepotService->getDossierVolumineux($consultation), ['dossiersVolumineux' => '']);
    }

    public function testGetDossierVolumineuxEmptyDossierVolumineux(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $candidatureService = $this->createMock(CandidatureService::class);
        $dumeService = $this->createMock(DumeService::class);
        $dossiersVolumineuxService = $this->createMock(DossierVolumineuxService::class);

        $consultationDepotService = new ConsultationDepotService(
            $parameterBag,
            $session,
            $moduleStateChecker,
            $translator,
            $logger,
            $entityManager,
            $candidatureService,
            $dumeService,
            $dossiersVolumineuxService,
            $inscritOperationTracker
        );

        $consultation = new Consultation();
        $consultation->setEnvolActivation(true);

        $dossiersVolumineuxService
            ->expects($this->once())
            ->method('getActiveDVforCurrentUser')
            ->willReturn(null)
        ;

        $this->assertSame(
            $consultationDepotService->getDossierVolumineux($consultation),
            ['dossiersVolumineux' => null]
        );
    }

    public function testGetDossierVolumineux(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $session = $this->createMock(SessionInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $candidatureService = $this->createMock(CandidatureService::class);
        $dumeService = $this->createMock(DumeService::class);
        $dossiersVolumineuxService = $this->createMock(DossierVolumineuxService::class);

        $consultationDepotService = new ConsultationDepotService(
            $parameterBag,
            $session,
            $moduleStateChecker,
            $translator,
            $logger,
            $entityManager,
            $candidatureService,
            $dumeService,
            $dossiersVolumineuxService,
            $inscritOperationTracker
        );

        $consultation = new Consultation();
        $consultation->setEnvolActivation(true);

        $dossierVolumineux = new DossierVolumineux();

        $dossiersVolumineuxService
            ->expects($this->once())
            ->method('getActiveDVforCurrentUser')
            ->willReturn($dossierVolumineux)
        ;

        $this->assertSame(
            $consultationDepotService->getDossierVolumineux($consultation),
            ['dossiersVolumineux' => $dossierVolumineux]
        );
    }
    
    public function testInitDefaultFieldsOffre()
    {
        $this->initMocks();
        $this->parameterBag->expects($this->once())
            ->method('get')
            ->with('STATUT_ENV_BROUILLON')
            ->willReturn(99);

        $this->iniConsultationDepotService();

        $offre = $this->consultationDepotService->initDefaultFieldsOffre();

        $this->assertEquals(Offre::class, $offre::class);
        $this->assertEquals('', $offre->getHorodatage());
        $this->assertEquals('', $offre->getXmlString());
        $this->assertEquals(0, $offre->getEtatChiffrement());
        $this->assertEquals(0, $offre->getEnvoiComplet());
    }
}
