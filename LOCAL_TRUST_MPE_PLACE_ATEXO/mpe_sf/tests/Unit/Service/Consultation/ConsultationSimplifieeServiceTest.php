<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use DateTime;
use App\Entity\Agent;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Entity\Service;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\EntrepriseRepository;
use App\Repository\GeolocalisationN2Repository;
use App\Service\Agent\AgentInviterService;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\Agent\DceService;
use App\Service\Agent\HistorisationConsultationSimplifieeService;
use App\Service\Agent\ReferenceTypeService;
use App\Service\AtexoConfiguration;
use App\Service\Consultation\ConsultationService;
use App\Service\Consultation\FavorisService;
use App\Service\Crypto\CryptoService;
use App\Service\Handler\EmailAlerteAgentValidationHandler;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ConsultationSimplifieeServiceTest extends TestCase
{
    private $mockProcedureEquivalenceAgentService;
    private $mockDceService;
    private $mockParameterBagInterface;
    private $mockEntityManagerInterface;
    private $mockLogger;
    private $service;
    private $mockSessionInterface;
    private $mockEmailAlerteAgentValidationHandler;
    private $mockConsultationService;

    public function setUp(): void
    {
        parent::setUp();

        $this->mockProcedureEquivalenceAgentService = $this
            ->createMock(ProcedureEquivalenceAgentService::class);
        $this->mockDceService = $this
            ->createMock(DceService::class);
        $this->mockEntityManagerInterface = $this
            ->createMock(EntityManagerInterface::class);
        $this->mockParameterBagInterface = $this
            ->createMock(ParameterBagInterface::class);
        $this->mockSessionInterface = $this
            ->createMock(SessionInterface::class);
        $this->mockEmailAlerteAgentValidationHandler = $this
            ->createMock(EmailAlerteAgentValidationHandler::class);
        $this->mockLogger = $this
            ->createMock(LoggerInterface::class);
        $this->mockConsultationService = $this
            ->createMock(ConsultationService::class);

        $this->service = new ConsultationSimplifieeService(
            $this->mockProcedureEquivalenceAgentService,
            $this->mockDceService,
            $this->mockEntityManagerInterface,
            new ReferenceTypeService(
                $this->mockEntityManagerInterface,
                new AtexoConfiguration($this->mockParameterBagInterface, $this->mockEntityManagerInterface),
                $this->mockParameterBagInterface
            ),
            $this->mockParameterBagInterface,
            $this->mockSessionInterface,
            $this->mockEmailAlerteAgentValidationHandler,
            $this->mockLogger,
            $this->mockConsultationService
        );
    }

    public function testGetSelectedGeoN2()
    {
        $repoGeoloc = $this->createMock(GeolocalisationN2Repository::class);
        $repoGeoloc->expects($this->once())
            ->method('getLieuxExecution')
            ->willReturn(
                [
                    [
                        'id' => 224,
                        'denomination1' => '(63) Puy-de-Dôme',
                        'denomination2' => '63'
                    ],
                    [
                        'id' => 254,
                        'denomination1' => '(75) Paris',
                        'denomination2' => '75'
                    ],
                ]
            );
        $this->mockEntityManagerInterface->expects($this->once())
            ->method('getRepository')
            ->willReturn($repoGeoloc);

        $this->mockSessionInterface->expects($this->once())
            ->method('get')
            ->willReturn('224,254');

        $consultationSimplifieeService = $this->service;

        $agent = new Agent();
        $result = $consultationSimplifieeService->getSelectedGeoN2($agent);

        $reference = [
            "ids" => "224,254",
            "list" => [
                "(63) Puy-de-Dôme" => "224-63",
                "(75) Paris" => "254-75"
            ]
        ];
        $this->assertEquals($reference, $result);
    }

    public function testIdsToGeoN2Empty()
    {
        $mock = $this->createMock(ConsultationSimplifieeService::class);
        $result = $mock->idsToGeoN2('test,');

        $this->assertEquals([], $result);
    }

    /**
     * @dataProvider providerTypeAcces
     */
    public function testGetTypeAccesFromTypeProcedure($expected, $activate)
    {
        $repo = $this->createMock(EntityRepository::class);
        $repo->expects($this->any())
            ->method('findOneBy')
            ->willReturn(new ProcedureEquivalence());
        $this->mockEntityManagerInterface->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repo);
        $this->mockProcedureEquivalenceAgentService
            ->expects($this->any())
            ->method('getActivate')
            ->willReturn($activate);
        $actual = $this->service->getTypeAccesFromTypeProcedure(1, 'organisme');
        $this->assertEquals($expected, $actual);
    }

    public function providerTypeAcces(): array
    {
        return [
            [
                ['type_acces' => Consultation::TYPE_ACCES_RESTREINT, 'procedure_public' => '', 'procedure_restreint' => ''],
                1
            ],
            [
                ['type_acces' => Consultation::TYPE_ACCES_PUBLIC, 'procedure_public' => '', 'procedure_restreint' => ''],
                0
            ],
        ];
    }

    /**
     * @dataProvider providerElementProcedureEquivalence
     *
     * @throws ReflectionException
     */
    public function testSetElementProcedureEquivalence($activate, $parameter)
    {
        $repo = $this->createMock(EntityRepository::class);
        $repo->expects($this->any())
            ->method('findOneBy')
            ->willReturn(new ProcedureEquivalence());
        $this->mockEntityManagerInterface->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repo);
        $this->mockProcedureEquivalenceAgentService
            ->expects($this->any())
            ->method('getActivate')
            ->willReturn($activate);
        $this->mockProcedureEquivalenceAgentService
            ->expects($this->any())
            ->method('setValueProcedure')
            ->willReturn(new Consultation());
        $this->mockParameterBagInterface
            ->expects($this->any())
            ->method('get')
            ->willReturn($parameter);

        $class = new ReflectionClass(ConsultationSimplifieeService::class);
        $method = $class->getMethod('setElementProcedureEquivalence');
        $method->setAccessible(true);

        /** @var Consultation $actual */
        $actual = $method->invoke($this->service, new Consultation());

        $this->assertEquals($parameter, $actual->getRegleMiseEnLigne());
    }

    public function providerElementProcedureEquivalence(): array
    {
        return [
            [0, 'MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE'],
            [1, 'MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE'],
            [2, 'MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE'],
            [3, 'MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP'],
            [4, 'MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP'],
        ];
    }

    public function testSetInitValueForAgent()
    {
        $dateCreate = new DateTime();
        $dateCreateString = $dateCreate->format('Y-m-d');

        $consultation = new Consultation();
        $agent = new Agent();
        $agent->setId(1);
        $agent->setNom('marc');
        $agent->setPrenom('passet');
        $agent->setDateCreation($dateCreateString);
        $agent->setServiceId(1);

        $service = new Service();
        $service->setId(1);
        $service->setDateCreation($dateCreateString);
        $organisme = new Organisme();
        $organisme->setDateCreation($dateCreate);
        $agent->setService($service);
        $agent->setOrganisme($organisme);
        $typeProcedureOrganisme = new TypeProcedureOrganisme();
        $typeProcedureOrganisme->setIdTypeProcedure(1);
        $consultation->setTypeProcedureOrganisme($typeProcedureOrganisme);

        $agentReference = new Agent();
        $agentReference->setId(1);
        $agentReference->setNom('marc');
        $agentReference->setPrenom('passet');
        $agentReference->setDateCreation($dateCreateString);
        $agentReference->setServiceId(1);

        $serviceReference = new Service();
        $serviceReference->setId(1);
        $serviceReference->setDateCreation($dateCreateString);
        $organismeReference = new Organisme();
        $organismeReference->setDateCreation($dateCreate);
        $agentReference->setService($serviceReference);
        $agentReference->setOrganisme($organismeReference);
        $typeProcedureOrganismeReference = new TypeProcedureOrganisme();
        $typeProcedureOrganismeReference->setIdTypeProcedure(1);

        $consultationReference = new Consultation();
        $consultationReference->setTypeProcedureOrganisme($typeProcedureOrganismeReference);
        $consultationReference->setServiceId(1);
        $consultationReference->setServiceAssocieId(1);
        $consultationReference->setServiceValidation(1);
        $consultationReference->setService($serviceReference);
        $consultationReference->setOrganisme($organismeReference);
        $consultationReference->setIdCreateur(1);
        $consultationReference->setNomCreateur('marc');
        $consultationReference->setPrenomCreateur('passet');
        $consultationReference->setIdTypeProcedure(1);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->willReturn($agent);

        $this->mockEntityManagerInterface->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryAgent);

        $service = new ConsultationSimplifieeService(
            $this->mockProcedureEquivalenceAgentService,
            $this->mockDceService,
            $this->mockEntityManagerInterface,
            new ReferenceTypeService(
                $this->mockEntityManagerInterface,
                new AtexoConfiguration($this->mockParameterBagInterface, $this->mockEntityManagerInterface),
                $this->mockParameterBagInterface
            ),
            $this->mockParameterBagInterface,
            $this->mockSessionInterface,
            $this->mockEmailAlerteAgentValidationHandler,
            $this->mockLogger,
            $this->mockConsultationService
        );

        $class = new ReflectionClass(ConsultationSimplifieeService::class);
        $method = $class->getMethod('setInitValueForAgent');
        $method->setAccessible(true);
        $output = $method->invoke($service, $consultation, $agent);
        $this->assertEquals($consultationReference->getAgentCreateur(), $output->getAgentCreateur());
    }

    public function testcreateDce()
    {
        $this->mockDceService->expects($this->any())
            ->method('createDce')
            ->willReturn(new DCE());
        $output = $this->service->createDce('chemin', new Consultation(), new Agent());
        $this->assertEquals(true, $output);

        $this->mockDceService->expects($this->any())
            ->method('createDce')
            ->willThrowException(new Exception());
        $output = $this->service->createDce('chemin', new Consultation(), new Agent());
        $this->assertEquals(false, $output);
    }

    public function testgetUploadedDce()
    {
        $output = $this->service->getUploadedDce('var');
        $this->assertInstanceOf(DCE::class, $output);

        $output = $this->service->getUploadedDce('');
        $this->assertEquals(null, $output);
    }

    public function testCreateHistoriquesConsultation()
    {
        $agent = new Agent();
        $agent->setId(1234);
        $agent->setServiceId(33);
        $agent->setOrganisme(new Organisme());
        $agent->setNom('Atexo');
        $agent->setPrenom('oxeta');


        $entrepriseRepo = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo->expects($this->once())
            ->method('findBy')
            ->willReturn([]);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($entrepriseRepo);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->exactly(2))
            ->method('get')
            ->willReturnOnConsecutiveCalls('test1', 'test2');

        $agentInviterService = $this->createMock(AgentInviterService::class);
        $agentInviterService->expects($this->once())
            ->method('getListAgentInviter')
            ->willReturn([1]);

        $cryptoService = $this->createMock(CryptoService::class);
        $cryptoService->expects($this->once())
            ->method('timeStampData')
            ->willReturn(['horodatage' => 'horo', 'untrustedDate' => 'untrust']);


        $histoConsultationService = new HistorisationConsultationSimplifieeService(
            $cryptoService,
            $mockEntityManager,
            $parameterBag,
            $agentInviterService
        );

        $return = $histoConsultationService->createHistoriquesConsultation(1, $agent);

        $this->assertEquals(1, $return->getConsultationId());
        $this->assertEquals(1234, $return->getAgentId());
        $this->assertEquals('Atexo', $return->getNomAgent());
        $this->assertEquals('oxeta', $return->getPrenomAgent());
        $this->assertEquals('test1', $return->getNomElement());
        $this->assertEquals('test2', $return->getStatut());
        $this->assertEquals('', $return->getValeurDetail1());
        $this->assertEquals('', $return->getValeurDetail2());
        $this->assertEquals('0', $return->getNumeroLot());
        $this->assertEquals('horo', $return->getHorodatage());
        $this->assertEquals('untrust', $return->getUntrusteddate());
    }

    public function testCreateDateFin()
    {
        $agent = new Agent();
        $agent->setId(1234);
        $agent->setOrganisme(new Organisme());

        $dateTime = new DateTime();
        $consultation = new Consultation();
        $consultation->setId(2345);
        $consultation->setDatefin($dateTime);


        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->once())
            ->method('get')
            ->willReturn('testStatut');

        $histoConsultationService = new HistorisationConsultationSimplifieeService(
            $this->createMock(CryptoService::class),
            $this->createMock(EntityManagerInterface::class),
            $parameterBag,
            $this->createMock(AgentInviterService::class)
        );

        $horodatage = ['horodatage' => 'horo', 'untrustedDate' => 'untrust'];

        $return = $histoConsultationService->createDateFin($consultation, $agent, $horodatage, false);

        $this->assertEquals(2345, $return->getConsultationId());
        $this->assertEquals(1234, $return->getAgentId());
        $this->assertEquals('testStatut', $return->getStatut());
        $this->assertEquals('horo', $return->getHorodatage());
        $this->assertEquals('untrust', $return->getUntrusteddate());
        $this->assertEquals($dateTime->format('Y-m-d h:i:s'), $return->getDateFin());
        $this->assertEquals($dateTime->format('Y-m-d h:i:s'), $return->getDateFinLocale());
    }
}
