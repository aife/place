<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Repository\Consultation\ConsultationFavorisRepository;
use App\Service\Consultation\FavorisService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class FavorisServiceTest extends TestCase
{
    public function testAddConsultationFavoris(): void
    {
        $emMock = $this->createMock(EntityManager::class);
        $loggerMock = $this->createMock(LoggerInterface::class);
        $favoris = new FavorisService($emMock, $loggerMock);
        $agent = new Agent();
        $agent->setId(1);
        $consultation = new Consultation();
        $consultation->setId(1);

        $repositoryEmpty = $this->createMock(ConsultationFavorisRepository::class);
        $repositoryEmpty->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryEmpty);

        $reponse = $favoris->addConsultationFavoris($agent, $consultation);

        $this->assertEquals('', $reponse);
    }
}
