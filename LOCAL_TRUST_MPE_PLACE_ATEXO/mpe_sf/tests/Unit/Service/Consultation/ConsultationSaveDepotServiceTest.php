<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Service\Consultation\ConsultationSaveDepotService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ConsultationSaveDepotServiceTest extends TestCase
{
    /** @var ConsultationSaveDepotService */
    private $consultationSaveDepotService;

    /** @var EntityManagerInterface|MockObject */
    private $entityManager;

    /** @var LoggerInterface|MockObject */
    private $logger;

    /** @var MessageBusInterface  */
    private MessageBusInterface $messageBusInterface;

    private function initMocks()
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->logger = $this->createMock(LoggerInterface::class);
    }

    private function iniConsultationSaveDepotService()
    {
        $this->consultationSaveDepotService = new ConsultationSaveDepotService(
            $this->entityManager,
            $this->logger,
            $this->messageBusInterface,
        );
    }

    public function testAddJobEmailAR()
    {
        $this->initMocks();
        $this->iniConsultationSaveDepotService();

        $errors = $this->consultationSaveDepotService->addJobEmailAR('1', '1', '1', '1');
        $this->assertIsArray($errors);
    }
}
