<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Repository\OffrePapierRepository;
use App\Repository\OffreRepository;
use App\Service\Consultation\CommentService;
use App\Entity\ContratTitulaire;
use App\Entity\Etablissement;
use App\Entity\Entreprise;
use App\Entity\Offre;
use App\Entity\OffrePapier;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CommentServiceTest extends TestCase
{
    public function testGetCommentByContratWithEtablissement(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $etablissement = new Etablissement();
        $etablissement->setCodePostal('12345');
        $etablissement->setVille('City');

        $entreprise = new Entreprise();
        $entreprise->setNom('Atexo');

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setIdTitulaireEtab(12);
        $contratTitulaire->setId(25);

        $etablissementRepository = $this->createMock(EtablissementRepository::class);
        $etablissementRepository
            ->expects(self::once())
            ->method('find')
            ->with(12)
            ->willReturn($etablissement)
        ;

        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['id' => 25])
            ->willReturn($entreprise)
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Etablissement::class],
                [Entreprise::class]
            )
            ->willReturnOnConsecutiveCalls(
                $etablissementRepository,
                $entrepriseRepository
            )
        ;

        $parameterBag
            ->expects(self::never())
            ->method('get')
            ->with('TYPE_ENVELOPPE_ELECTRONIQUE')
        ;

        $commentService = new CommentService($entityManager, $parameterBag);
        $result = $commentService->getCommentByContrat($contratTitulaire);


        self::assertSame("\nAtexo (12345 - City)", $result);
    }

    public function testGetCommentByContratWithoutEtablissement(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setIdTitulaireEtab(1);

        $entreprise = new Entreprise();
        $entreprise->setNom('Atexo');

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setIdTitulaireEtab(12);
        $contratTitulaire->setId(25);

        $etablissementRepository = $this->createMock(EtablissementRepository::class);
        $etablissementRepository
            ->expects(self::once())
            ->method('find')
            ->with(12)
            ->willReturn(null)
        ;

        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['id' => 25])
            ->willReturn($entreprise)
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Etablissement::class],
                [Entreprise::class]
            )
            ->willReturnOnConsecutiveCalls(
                $etablissementRepository,
                $entrepriseRepository
            )
        ;

        $parameterBag
            ->expects($this->never())
            ->method('get')
        ;

        $commentService = new CommentService($entityManager, $parameterBag);
        $result = $commentService->getCommentByContrat($contratTitulaire);

        self::assertSame("\nAtexo (- - -)", $result);
    }

    public function testGetNomTitulaireContratWithEntreprise(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $reflectionClass = new \ReflectionClass(CommentService::class);
        $methodPrivate = $reflectionClass->getMethod('getNomTitulaireContrat');
        $methodPrivate->setAccessible(true);

        $entreprise = new Entreprise();
        $entreprise->setNom('Atexo');

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setId(25);

        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['id' => 25])
            ->willReturn($entreprise)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->with(Entreprise::class)
            ->willReturn($entrepriseRepository)
        ;

        $parameterBag
            ->expects(self::never())
            ->method('get')
            ->with('TYPE_ENVELOPPE_ELECTRONIQUE')
        ;

        $commentService = new CommentService($entityManager, $parameterBag);
        $result = $methodPrivate->invokeArgs($commentService, [$contratTitulaire]);

        self::assertSame('Atexo', $result);
    }

    public function testGetNomTitulaireContratWithOffre(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $offre = new Offre();
        $offre->setNomEntrepriseInscrit('Nom Entreprise Inscrit');

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setId(1);
        $contratTitulaire->setTypeDepotReponse('typeDepot_1');
        $contratTitulaire->setIdOffre(27);
        $contratTitulaire->setOrganisme('pmi-min-1');

        $reflectionClass = new \ReflectionClass(CommentService::class);
        $methodPrivate = $reflectionClass->getMethod('getNomTitulaireContrat');
        $methodPrivate->setAccessible(true);

        $entreprise = new Entreprise();
        $entreprise->setNom('Atexo');

        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $offreRepository = $this->createMock(OffreRepository::class);
        $offreRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 27,
                    'organisme' => 'pmi-min-1',
                ]
            )
            ->willReturn($offre)
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Entreprise::class],
                [Offre::class],
            )
            ->willReturnOnConsecutiveCalls(
                $entrepriseRepository,
                $offreRepository,
            )
        ;

        $parameterBag->expects(self::once())
            ->method('get')
            ->with('TYPE_ENVELOPPE_ELECTRONIQUE')
            ->willReturn('typeDepot_1')
        ;

        $commentService = new CommentService($entityManager, $parameterBag);

        $result = $methodPrivate->invokeArgs($commentService, [$contratTitulaire]);

        self::assertSame('Nom Entreprise Inscrit', $result);
    }

    public function testGetNomTitulaireContratWithOffrePapier(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $offre = new OffrePapier();
        $offre->setNomEntreprise('Nom Entreprise Offre Papier');

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setId(1);
        $contratTitulaire->setTypeDepotReponse('typeDepot_papier');
        $contratTitulaire->setIdOffre(27);
        $contratTitulaire->setOrganisme('pmi-min-1');

        $reflectionClass = new \ReflectionClass(CommentService::class);
        $methodPrivate = $reflectionClass->getMethod('getNomTitulaireContrat');
        $methodPrivate->setAccessible(true);

        $entreprise = new Entreprise();
        $entreprise->setNom('Atexo');

        $entrepriseRepository = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $offrePapierRepository = $this->createMock(OffrePapierRepository::class);
        $offrePapierRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 27,
                    'organisme' => 'pmi-min-1',
                ]
            )
            ->willReturn($offre)
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [Entreprise::class],
                [OffrePapier::class],
            )
            ->willReturnOnConsecutiveCalls(
                $entrepriseRepository,
                $offrePapierRepository,
            )
        ;

        $parameterBag->expects(self::once())
            ->method('get')
            ->with('TYPE_ENVELOPPE_ELECTRONIQUE')
            ->willReturn('typeDepot_1')
        ;

        $commentService = new CommentService($entityManager, $parameterBag);

        $result = $methodPrivate->invokeArgs($commentService, [$contratTitulaire]);

        self::assertSame('Nom Entreprise Offre Papier', $result);
    }
}
