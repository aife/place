<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationComptePub;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\ConsultationTags;
use App\Entity\FormePrix;
use App\Entity\Lot;
use App\Exception\ApiContentNotCompliantToConsultationValidationRulesException;
use App\Service\ClausesService;
use App\Service\Consultation\DuplicationConsultation;
use App\Service\WebServices\DuplicationService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class DuplicationConsultationTest extends TestCase
{
    public function testDuplicateConsultationComptePub(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $donneComplementaireIdRessource = '/api/v2/donnee-complementaire/12';

        $comptePub = new class () extends ConsultationComptePub {
            public function getId(): int
            {
                return 123;
            }
        };

        $donneComplementaire = (new DonneeComplementaire())
            ->setConsultationComptePub($comptePub)
        ;

        $iriConverter
            ->expects(self::once())
            ->method('getItemFromIri')
            ->with($donneComplementaireIdRessource)
            ->willReturn($donneComplementaire)
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Début duplication ConsultationComptePub id: 123")],
                [self::stringContains("Fin duplication ConsultationComptePub, nouveau id: 123")]
            )
        ;

        $duplicationService
            ->expects(self::once())
            ->method('duplicateComptePub')
            ->willReturn($comptePub)
        ;

        $duplicationConsultation->duplicateConsultationComptePub($donneComplementaire, $donneComplementaireIdRessource);
    }

    public function testDuplicateConsultationComptePubReturnNull(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $donneComplementaireIdRessource = '/api/v2/donnee-complementaire/12';

        $donneComplementaire = new DonneeComplementaire();

        $iriConverter
            ->expects(self::once())
            ->method('getItemFromIri')
            ->with($donneComplementaireIdRessource)
            ->willReturn(null)
        ;

        $logger
            ->expects(self::never())
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Début duplication ConsultationComptePub id: 123")],
                [self::stringContains("Fin duplication ConsultationComptePub, nouveau id: 123")]
            )
        ;

        $duplicationService
            ->expects(self::never())
            ->method('duplicateComptePub')
        ;

        $duplicationConsultation->duplicateConsultationComptePub($donneComplementaire, $donneComplementaireIdRessource);
    }

    public function testDuplicateFormePrix(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $newDonneComplementaire = new DonneeComplementaire();

        $donneComplementaireIdRessource = '/api/v2/donnee-complementaire/12';
        $formePrixId = 123;

        $donneComplementaire = (new DonneeComplementaire())
            ->setIdFormePrix($formePrixId)
        ;

        $iriConverter
            ->expects(self::once())
            ->method('getItemFromIri')
            ->with($donneComplementaireIdRessource)
            ->willReturn($newDonneComplementaire)
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Duplication consultation : Début duplication FormePrix id: 123")],
                [self::stringContains("Duplication consultation : Fin duplication FormePrix, nouveau id: 456")]
            )
        ;

        $newFormePrix = new class () extends FormePrix {
            public function getIdFormePrix(): ?int
            {
                return 456;
            }
        };

        $duplicationService
            ->expects(self::once())
            ->method('createFormePrix')
            ->with($formePrixId)
            ->willReturn($newFormePrix)
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
            ->with(self::isInstanceOf(DonneeComplementaire::class))
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $duplicationConsultation->duplicateFormePrix($donneComplementaire, $donneComplementaireIdRessource);
    }

    public function testDuplicateFormePrixReturnNull(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $donneComplementaireIdRessource = '/api/v2/donnee-complementaire/12';
        $formePrixId = 123;

        $donneComplementaire = (new DonneeComplementaire())
            ->setIdFormePrix($formePrixId)
        ;

        $iriConverter
            ->expects(self::once())
            ->method('getItemFromIri')
            ->with($donneComplementaireIdRessource)
            ->willReturn(null)
        ;

        $logger
            ->expects(self::never())
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Duplication consultation : Début duplication FormePrix id: 123")],
                [self::stringContains("Duplication consultation : Fin duplication FormePrix, nouveau id: 456")]
            )
        ;

        $duplicationService
            ->expects(self::never())
            ->method('createFormePrix')
            ->with($formePrixId)
        ;

        $entityManager
            ->expects(self::never())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::never())
            ->method('flush')
        ;

        $duplicationConsultation->duplicateFormePrix($donneComplementaire, $donneComplementaireIdRessource);
    }


    public function testDuplicateConsultationTags(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );


        $tag1 = (new ConsultationTags())->setTagCode('tag1');
        $tag2 = (new ConsultationTags())->setTagCode('tag2');

        $consultation = new Consultation();
        $consultation
            ->addTags($tag1)
            ->addTags($tag2)
        ;

        $newConsultation = new Consultation();

        $entityManager
            ->expects(self::exactly(2))
            ->method('persist')
            ->withConsecutive(
                [(new ConsultationTags())->setConsultation($newConsultation)->setTagCode('tag1')],
                [(new ConsultationTags())->setConsultation($newConsultation)->setTagCode('tag2')]
            )
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $duplicationConsultation->duplicateConsultationTags($consultation, $newConsultation);
    }

    public function testDuplicateConsultationTagsEmpty(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $consultation = new Consultation();

        $entityManager
            ->expects(self::never())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::never())
            ->method('flush')
        ;

        $duplicationConsultation->duplicateConsultationTags($consultation, null);
    }

    public function testDuplicateSuccess(): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $request = new Request();
        $request->setLocale('fr');

        // Duplication Donnee Complementaire
        $sousCritereAttribution1 = new class () extends Consultation\SousCritereAttribution {
            public function getIdSousCritereAttribution(): int
            {
                return 11;
            }
        };
        $sousCritereAttribution2 = new class () extends Consultation\SousCritereAttribution {
            public function getIdSousCritereAttribution(): int
            {
                return 12;
            }
        };
        $newSousCritereJson1 = "{@id=13}";
        $newSousCritereJson1Iri = "api/v2/sous-critere-attribution/13";

        $newSousCritereJson2 = "{@id=14}";
        $newSousCritereJson2Iri = "api/v2/sous-critere-attribution/14";

        $critereAttribution1 = new class () extends Consultation\CritereAttribution {
            public function getId(): int
            {
                return 1;
            }
        };
        $critereAttribution1->setSousCriteres(
            new ArrayCollection([$sousCritereAttribution1, $sousCritereAttribution2])
        );

        $critereAttribution2 = new class () extends Consultation\CritereAttribution {
            public function getId(): int
            {
                return 2;
            }
        };

        $newCritereJson1 = "{@id=3}";
        $newCritereJson1Iri = "api/v2/critere-attribution/3";

        $newCritereJson2 = "{@id=4}";
        $newCritereJson2Iri = "api/v2/critere-attribution/4";

        $ssCritereAddKeys = [
            "critereAttribution" => "api/v2/critere-attribution/3",
            "lot" => null
        ];

        $donneeComplementaire = new class () extends DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 42;
            }
        };

        $formePrixId = 456;

        $newFormePrix = new class () extends FormePrix {
            public function getIdFormePrix(): ?int
            {
                return 457;
            }
        };

        $donneeComplementaire
            ->setCriteresAttribution(new ArrayCollection([$critereAttribution1, $critereAttribution2]))
            ->setIdFormePrix($formePrixId)
        ;

        $newDonneeComplementaire = new class () extends DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 43;
            }
        };

        $newDonneeComplementaireJson = "{@id=43}";
        $newDonneeComplementaireIri = "api/v2/donnee-complementaires/43";


        // Consultation
        $lot = new class () extends Lot {
            public function getId(): int
            {
                return 1;
            }
        };
        $lot
            ->setLot(1)
        ;

        $lot2 = new class () extends Lot {
            public function getId(): int
            {
                return 2;
            }
        };
        $lot2
            ->setLot(2)
        ;

        $lotAddKeys = [
            "consultation" => "api/v2/consultations/3",
            "donneeComplementaire" => null,
            "clauses" => [],
            "idTraductionDescription" => null,
            "idTraductionDescriptionDetail" => null,
        ];

        $lotJsonResponse1 = "{@id=1}";
        $lotIdRessource1 = "api/v2/lots/1";
        $lotJsonResponse2 = "{@id=2}";
        $lotIdRessource2 = "api/v2/lots/2";

        $consultation = new class () extends Consultation {
            public function getId(): int
            {
                return 25;
            }
            public function isAlloti(): bool
            {
                return true;
            }
        };

        $consultation
            ->setDonneeComplementaire($donneeComplementaire)
            ->addLot($lot)
            ->addLot($lot2)
        ;

        $consultationJsonResponse = "{@id=25}";
        $consultationIdRessource = "api/v2/consultations/3";

        $query = [
            'duplicate' => true,
        ];

        $agent = new Agent();
        $agent->setLogin('loginAgent');

        $newConsultation = new class () extends Consultation {
            public function getId()
            {
                return 26;
            }
        };

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($agent)
        ;

        $logger
            ->expects(self::exactly(19))
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Début Duplication consultation : avec l'agent: loginAgent")],
                ["Duplication consultation : Début duplication Donne Complementaire id: 42"],
                [
                    sprintf(
                        "Duplication consultation : Fin duplication Donne Complementaire, nouveau id: %s",
                        $newDonneeComplementaireIri
                    )
                ],
                ["Duplication consultation : Début duplication CritereAttribution id: 1"],
                [
                    sprintf(
                        "Duplication consultation : Fin duplication CritereAttribution, nouveau id: %s",
                        "api/v2/critere-attribution/3"
                    )
                ],
                ["Duplication consultation : Début duplication SousCritereAttribution id: 11"],
                [
                    sprintf(
                        "Duplication consultation : Fin duplication SousCritereAttribution, nouveau id: %s",
                        "api/v2/sous-critere-attribution/13"
                    )
                ],
                ["Duplication consultation : Début duplication SousCritereAttribution id: 12"],
                [
                    sprintf(
                        "Duplication consultation : Fin duplication SousCritereAttribution, nouveau id: %s",
                        "api/v2/sous-critere-attribution/14"
                    )
                ],
                ["Duplication consultation : Début duplication CritereAttribution id: 2"],
                [
                    sprintf(
                        "Duplication consultation : Fin duplication CritereAttribution, nouveau id: %s",
                        "api/v2/critere-attribution/4"
                    )
                ],
                ["Duplication consultation : Début duplication FormePrix id: 456"],
                ["Duplication consultation : Fin duplication FormePrix, nouveau id: 457"],
                ["Duplication consultation : Début duplication CONSULTATION id: 25"],
                ["Duplication consultation : Fin duplication CONSULTATION, nouveau id: api/v2/consultations/3"],
                [
                    sprintf(
                        "Duplication consultation : Début duplication Lot 1 id: 1 de la consultation %s",
                        $consultationIdRessource
                    )
                ],
                ["Duplication consultation : Fin duplication Lot, nouveau id: api/v2/lots/1"],
                [
                    sprintf(
                        "Duplication consultation : Début duplication Lot 2 id: 2 de la consultation %s",
                        $consultationIdRessource
                    )
                ],
                ["Duplication consultation : Fin duplication Lot, nouveau id: api/v2/lots/2"]
            )
        ;

        $duplicationService
            ->expects(self::exactly(8))
            ->method('duplicateObjectAndGetNewObject')
            ->withConsecutive(
                [42, '/donnee-complementaires', ['idDonneeComplementaire', 'criteresAttribution', 'formePrix', 'tranches']],
                [1, '/critere-attributions', ['donneeComplementaire', 'sousCritereAttributions', 'lot']],
                [11, '/sous-critere-attributions', ['critereAttribution'], $ssCritereAddKeys],
                [12, '/sous-critere-attributions', ['critereAttribution'], $ssCritereAddKeys],
                [2, '/critere-attributions', ['donneeComplementaire', 'sousCritereAttributions', 'lot']],
                [25, '/consultations', [], [], $query],
                [1, '/lots', ['consultation', 'clauses'], $lotAddKeys],
                [2, '/lots', ['consultation', 'clauses'], $lotAddKeys],
            )
            ->willReturnOnConsecutiveCalls(
                $newDonneeComplementaireJson,
                $newCritereJson1,
                $newSousCritereJson1,
                $newSousCritereJson2,
                $newCritereJson2,
                $consultationJsonResponse,
                $lotJsonResponse1,
                $lotJsonResponse2,
            )
        ;

        $duplicationService
            ->expects(self::exactly(8))
            ->method('getIriResourceFromWsResult')
            ->withConsecutive(
                [$newDonneeComplementaireJson],
                [$newCritereJson1],
                [$newSousCritereJson1],
                [$newSousCritereJson2],
                [$newCritereJson2],
                [$consultationJsonResponse],
                [$lotJsonResponse1],
                [$lotJsonResponse2],
            )
            ->willReturnOnConsecutiveCalls(
                $newDonneeComplementaireIri,
                $newCritereJson1Iri,
                $newSousCritereJson1Iri,
                $newSousCritereJson2Iri,
                $newCritereJson2Iri,
                $consultationIdRessource,
                $lotIdRessource1,
                $lotIdRessource2,
            )
        ;

        $iriConverter
            ->expects(self::exactly(3))
            ->method('getItemFromIri')
            ->withConsecutive(
                [$newDonneeComplementaireIri],
                [$newDonneeComplementaireIri],
                [$consultationIdRessource],
            )
            ->willReturnOnConsecutiveCalls(
                $newDonneeComplementaire,
                null,
                $newConsultation,
            )
        ;

        $duplicationService
            ->expects(self::once())
            ->method('createFormePrix')
            ->with($formePrixId)
            ->willReturn($newFormePrix)
        ;

        $entityManager
            ->expects(self::once())
            ->method('persist')
            ->withConsecutive(
                [$newDonneeComplementaire],
            )
        ;

        $entityManager
            ->expects(self::exactly(2))
            ->method('flush')
        ;

        $duplicationService
            ->expects(self::once())
            ->method('getKeysToRemove')
            ->with($consultation)
            ->willReturn([])
        ;

        $duplicationService
            ->expects(self::once())
            ->method('getKeysToAdd')
            ->with($consultation, $agent, $newDonneeComplementaireIri)
            ->willReturn([])
        ;

        $translator
            ->expects(self::once())
            ->method('trans')
            ->with('DUPLICATION_CONSULTATION_SUCCESS')
            ->willReturn('Succès de la duplication de la consultation')
        ;

        $flashBag
            ->expects(self::once())
            ->method('add')
            ->with('success', 'Succès de la duplication de la consultation')
        ;

        self::assertSame('26', $duplicationConsultation->duplicate($consultation, $request));
    }

    /**
     * @dataProvider exceptionProvider
     */
    public function testDuplicateWithErrors(Exception $exceptionClass): void
    {
        $duplicationService = $this->createMock(DuplicationService::class);
        $clausesService = $this->createMock(ClausesService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $security = $this->createMock(Security::class);
        $flashBag = $this->createMock(FlashBagInterface::class);
        $translator = $this->createMock(TranslatorInterface::class);

        $duplicationConsultation = new DuplicationConsultation(
            $duplicationService,
            $clausesService,
            $logger,
            $entityManager,
            $iriConverter,
            $security,
            $flashBag,
            $translator
        );

        $request = new Request();
        $request->setLocale('fr');

        // Consultation
        $consultation = new class () extends Consultation {
            public function getId(): int
            {
                return 25;
            }
        };

        $query = [
            'duplicate' => true,
        ];

        $agent = new Agent();
        $agent->setLogin('loginAgent');

        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($agent)
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                [self::stringContains("Début Duplication consultation : avec l'agent: loginAgent")],
                ["Duplication consultation : Début duplication CONSULTATION id: 25"],
            )
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('error')
            ->withConsecutive(
                ["Duplication consultation : ERREUR duplication CONSULTATION id: 25"],
                [self::stringContains("Duplication consultation : message erreur")],
            )
        ;

        $duplicationService
            ->method('duplicateObjectAndGetNewObject')
            ->with([25, '/consultations', [], [], $query])
            ->willThrowException($exceptionClass);
        ;

        $duplicationService
            ->expects(self::never())
            ->method('getIriResourceFromWsResult')
        ;

        $iriConverter
            ->expects(self::never())
            ->method('getItemFromIri')
        ;

        $duplicationService
            ->expects(self::never())
            ->method('createFormePrix')
        ;

        $entityManager
            ->expects(self::never())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::never())
            ->method('flush')
        ;

        $duplicationService
            ->expects(self::once())
            ->method('getKeysToRemove')
            ->with($consultation)
            ->willReturn([])
        ;

        $duplicationService
            ->expects(self::once())
            ->method('getKeysToAdd')
            ->willReturn([])
        ;

        $translator
            ->expects(self::once())
            ->method('trans')
            ->with('TEXT_MESSAGE_ERROR_DUPLICATION')
            ->willReturn('Erreur de duplication')
        ;

        $flashBag
            ->expects(self::once())
            ->method('add')
            ->with('error', 'Erreur de duplication')
        ;

        self::assertNull($duplicationConsultation->duplicate($consultation, $request));
    }

    public function exceptionProvider(): array
    {
        return [
            "Exception" => [new Exception('Your error message')],
            "ApiContentNotCompliantToConsultationValidationRulesException" => [
                new ApiContentNotCompliantToConsultationValidationRulesException('Your error message')
            ],
        ];
    }
}
