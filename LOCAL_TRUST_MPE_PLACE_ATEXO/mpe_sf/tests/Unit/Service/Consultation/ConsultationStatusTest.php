<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use Exception;
use RuntimeException;
use ReflectionClass;
use App\Entity\Consultation;
use App\Service\Consultation\ConsultationStatus;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationStatusTest extends TestCase
{
    public const DATE_FORMAT = 'Y-m-d H:i:s';
    public const STATUS_VALUE = [
        'ELABORATION'       => '0',
        'PREPARATION'       => '1',
        'CONSULTATION'      => '2',
        'OUVERTURE_ANALYSE' => '3',
        'DECISION'          => '4'
    ];

    /**
     * Ce test gère le cas où en BDD le status est supérieur aux status suivant:
     * ELABORATION, PREPARATION, CONSULTATION, OUVERTURE_ANALYSE
     * Car dans le cas contraire la valeur en BDD sera toujours zéro.
     * Donc ce test portera sur le statut DECISION.
     *
     * @return void
     * @throws Exception
     */
    public function testStatusDecision(): void
    {
        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setIdEtatConsultation(4);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                ['DECISION_PARTIELLE_OUI'],
                ['CONSULTATION_DEPOUIABLE_PHASE_1'],
            );

        $consultationStatusObject = new ConsultationStatus($parameter);
        $result = $consultationStatusObject->getConsultationStatus($consultation);

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals(self::STATUS_VALUE['DECISION'], $result[0]);
    }

    public function testStatusPreparation(): void
    {
        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setEtatEnAttenteValidation(1);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(1))
            ->method('get');

        $consultationStatusObject = new ConsultationStatus($parameter);
        $result = $consultationStatusObject->getConsultationStatus($consultation);

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals(self::STATUS_VALUE['PREPARATION'], $result[0]);
    }

    public function testStatusElaboration(): void
    {
        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setEtatEnAttenteValidation(0);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(1))
            ->method('get');

        $consultationStatusObject = new ConsultationStatus($parameter);
        $result = $consultationStatusObject->getConsultationStatus($consultation);

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals(self::STATUS_VALUE['ELABORATION'], $result[0]);
    }

    public function testStatusConsultation(): void
    {
        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setDatefin('9000-01-01');
        $consultation->setDateMiseEnLigneCalcule(
            (new DateTime('NOW'))->format(self::DATE_FORMAT)
        );

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get');
        $consultationStatusObject = new ConsultationStatus($parameter);
        $result = $consultationStatusObject->getConsultationStatus($consultation);

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals(self::STATUS_VALUE['CONSULTATION'], $result[0]);
    }

    public function testStatusOuvertureAnalyse(): void
    {
        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setDatefin(
            (new DateTime('NOW'))->format(self::DATE_FORMAT)
        );

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get');

        $consultationStatusObject = new ConsultationStatus($parameter);
        $result = $consultationStatusObject->getConsultationStatus($consultation);

        $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertEquals(self::STATUS_VALUE['OUVERTURE_ANALYSE'], $result[0]);
    }

    public function testConsultationWithoutStatusException(): void
    {
        $parameter = $this->createMock(ParameterBagInterface::class);
        $consultationStatusObject = new ConsultationStatus($parameter);

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setIdEtatConsultation(1);
        $consultation->setDateMiseEnLigneCalcule('1970-01-01');

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Consultation without status');
        $consultationStatusObject->getConsultationStatus($consultation);
    }

    public function testMultiStatusOuvertureAnalyseAndDecision(): void
    {
        $reflectionClass = new ReflectionClass(ConsultationStatus::class);
        $methodPrivate = $reflectionClass->getMethod('getMultiStatus');
        $methodPrivate ->setAccessible(true);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                ['DECISION_PARTIELLE_OUI'],
                ['CONSULTATION_DEPOUIABLE_PHASE_1'],
            )
            ->willReturnOnConsecutiveCalls(
                1
            );

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setDecisionPartielle(1);

        $consultationStatusObject = new ConsultationStatus($parameter);

        $status[] = self::STATUS_VALUE['OUVERTURE_ANALYSE'];

        $result = $methodPrivate->invokeArgs($consultationStatusObject, [$consultation, $status]);

        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertSame(self::STATUS_VALUE['OUVERTURE_ANALYSE'], $result[0]);
        $this->assertSame(self::STATUS_VALUE['DECISION'], $result[1]);
    }

    public function testMultiStatusConsulAndOuvAnalyseAndDecision(): void
    {
        $reflectionClass = new ReflectionClass(ConsultationStatus::class);
        $methodPrivate = $reflectionClass->getMethod('getMultiStatus');
        $methodPrivate ->setAccessible(true);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                ['DECISION_PARTIELLE_OUI'],
                ['CONSULTATION_DEPOUIABLE_PHASE_1'],
            )
            ->willReturnOnConsecutiveCalls(1, '1');

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setDepouillablePhaseConsultation('1');
        $consultation->setDatefin('9000-01-01');

        $consultationStatusObject = new ConsultationStatus($parameter);

        $status[] = self::STATUS_VALUE['CONSULTATION'];

        $result = $methodPrivate->invokeArgs($consultationStatusObject, [$consultation, $status]);

        $this->assertIsArray($result);
        $this->assertCount(3, $result);
        $this->assertSame(self::STATUS_VALUE['CONSULTATION'], $result[0]);
        $this->assertSame(self::STATUS_VALUE['OUVERTURE_ANALYSE'], $result[1]);
        $this->assertSame(self::STATUS_VALUE['DECISION'], $result[2]);
    }

    public function testMultiStatusOuvAnalyseAndDecisionEndDateConsultPast(): void
    {
        $reflectionClass = new ReflectionClass(ConsultationStatus::class);
        $methodPrivate = $reflectionClass->getMethod('getMultiStatus');
        $methodPrivate ->setAccessible(true);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->exactly(2))
            ->method('get')
            ->withConsecutive(
                ['DECISION_PARTIELLE_OUI'],
                ['CONSULTATION_DEPOUIABLE_PHASE_1'],
            )
            ->willReturnOnConsecutiveCalls(1, '1');

        $consultation = new Consultation();
        $consultation->setId(20);
        $consultation->setDepouillablePhaseConsultation('1');
        $consultation->setDatefin('1970-01-01');

        $consultationStatusObject = new ConsultationStatus($parameter);

        $status[] = self::STATUS_VALUE['CONSULTATION'];

        $result = $methodPrivate->invokeArgs($consultationStatusObject, [$consultation, $status]);

        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertSame(self::STATUS_VALUE['OUVERTURE_ANALYSE'], $result[0]);
        $this->assertSame(self::STATUS_VALUE['DECISION'], $result[1]);
    }
}
