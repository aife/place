<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Entity\Enveloppe;
use App\Service\Consultation\ConsultationUploadDepotService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class ConsultationUploadDepotServiceTest extends TestCase
{
    /** @var ConsultationUploadDepotService */
    private $consultationUploadDepotService;

    /** @var EntityManagerInterface|MockObject */
    private $entityManager;

    /** @var LoggerInterface|MockObject */
    private $logger;

    private function initMocks()
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
    }

    private function iniConsultationSaveDepotService()
    {
        $this->consultationUploadDepotService = new ConsultationUploadDepotService($this->logger, $this->entityManager);
    }

    public function testGetEnveloppeBadOffreIdBadOffreId()
    {
        $this->initMocks();
        $this->iniConsultationSaveDepotService();
        $this->expectException(Exception::class);
        $this->consultationUploadDepotService->getEnveloppe('1234', 0, 1, 1, 'org');
    }

    public function testGetEnveloppeBadOffreIdBadTypeEnveloppe()
    {
        $this->initMocks();
        $this->iniConsultationSaveDepotService();
        $this->expectException(Exception::class);
        $this->consultationUploadDepotService->getEnveloppe('1234', 1, null, 1, 'org');
    }

    public function testGetEnveloppeBadOffreIdBadLot()
    {
        $this->initMocks();
        $this->iniConsultationSaveDepotService();
        $this->expectException(Exception::class);
        $this->consultationUploadDepotService->getEnveloppe('1234', 1, 1, null, 'org');
    }

    public function testGetEnveloppeBadOffreIdBadOrg()
    {
        $this->initMocks();
        $this->iniConsultationSaveDepotService();
        $this->expectException(Exception::class);
        $this->consultationUploadDepotService->getEnveloppe('1234', 1, 1, 1, '');
    }

    public function testGetEnveloppeBadOffreId()
    {
        $this->initMocks();
        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->with(Enveloppe::class)
            ->willReturnCallback(
                function () {
                    $repository = $this->createMock(ObjectRepository::class);
                    $repository->expects($this->any())
                        ->method('findOneBy')
                        ->willReturn(new Enveloppe());
                    return $repository;
                }
            );
        $this->iniConsultationSaveDepotService();
        $enveloppe = $this->consultationUploadDepotService->getEnveloppe('1234', 1, 1, 1, 'org');
        $this->assertInstanceOf(Enveloppe::class, $enveloppe);
    }
}
