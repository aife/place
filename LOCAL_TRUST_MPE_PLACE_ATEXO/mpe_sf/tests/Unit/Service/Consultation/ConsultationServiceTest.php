<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Agent;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\GeolocalisationN2;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Exception\ConsultationNotFoundException;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Consultation\ConsultationFavorisRepository;
use App\Repository\ConsultationRepository;
use App\Repository\GeolocalisationN2Repository;
use App\Service\Agent\AgentInviterService;
use App\Service\Agent\Guests;
use App\Service\Agent\ReferenceTypeService;
use App\Service\AgentService;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoUtil;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\Consultation\ConsultationService;
use App\Service\Consultation\FavorisService;
use App\Service\CurrentUser;
use App\Service\InscritOperationsTracker;
use App\Service\InterfaceSub;
use App\Service\ObscureDataManagement;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationServiceTest extends TestCase
{
    /** @var ConsultationService */
    private $consultationService;

    protected function setUp(): void
    {
        $entityManager = $this->getEntityManager();
        $plateformeVirtuelleService = $this->createMock(PlateformeVirtuelleService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->any())->method('trans')->willReturn('a translation');
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $obscureDataManagement = $this->createMock(ObscureDataManagement::class);
        $currentUser = $this->createMock(CurrentUser::class);
        $logger = $this->createMock(LoggerInterface::class);
        $interfaceSub = $this->createMock(InterfaceSub::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $atexoEnveloppeFichierService = $this->createMock(AtexoEnveloppeFichier::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $referenceTypeService = $this->createMock(ReferenceTypeService::class);
        $favoriService = $this->createMock(FavorisService::class);
        $agentInviterService = $this->createMock(AgentInviterService::class);
        $agentService = $this->createMock(AgentService::class);
        $guestService = $this->createMock(Guests::class);

        $this->consultationService = new ConsultationService(
            $plateformeVirtuelleService,
            $translator,
            $entityManager,
            $parameterBag,
            $obscureDataManagement,
            $currentUser,
            $logger,
            $interfaceSub,
            $atexoUtil,
            $atexoFichierOrganisme,
            $atexoEnveloppeFichierService,
            $inscritOperationTracker,
            $moduleStateChecker,
            $referenceTypeService,
            $favoriService,
            $agentInviterService,
            $agentService,
            $guestService
        );
    }

    public function testGetConsultationNullArgument()
    {
        $this->expectException(ConsultationNotFoundException::class);
        $this->consultationService->getConsultation(null);
    }

    public function testGetConsultationNotFound()
    {
        $this->expectException(ConsultationNotFoundException::class);
        $this->consultationService->getConsultation(99, 2);
    }

    public function testGetConsultationFound()
    {
        $consultation = $this->consultationService->getConsultation(1);
        $this->assertInstanceOf(Consultation::class, $consultation);
    }

    private function getEntityManager(): EntityManagerInterface
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if ($repository == Consultation::class) {
                        $repository = $this->createMock(ConsultationRepository::class);
                        $repository->expects($this->any())
                            ->method('getConsultationForDepot')
                            ->willReturnCallback(
                                function ($params) {
                                    if ($params === 1) {
                                        return new Consultation();
                                    } else {
                                        return null;
                                    }
                                }
                            );
                    }
                    if ($repository == ConsultationFavoris::class) {
                        $repository = $this->createMock(ConsultationFavorisRepository::class);
                        $repository->expects($this->any())
                            ->method('getFavorisForAgent')
                            ->willReturnCallback(
                                function ($param1, $param2) {
                                    $consultation = new Consultation();
                                    $consultation->setId(1);
                                    $agent = new Agent();
                                    $agent->setId(1);
                                    $consultationFavoris = new ConsultationFavoris();
                                    $consultationFavoris->setConsultation($consultation);
                                    $consultationFavoris->setAgent($agent);
                                    if (
                                        $param1 === $consultationFavoris->getConsultation()->getId()
                                        && $param2 === $consultationFavoris->getAgent()->getId()
                                    ) {
                                        return $consultationFavoris;
                                    } else {
                                        return false;
                                    }
                                }
                            );
                    }

                    return $repository;
                }
            );

        return $em;
    }

    public function testIsFavoriByAgent()
    {
        $consultation = new Consultation();
        $consultation->setId(1);

        $consultationFavori = new ConsultationFavoris();
        $consultationFavori->setConsultation($consultation);

        $agent = new Agent();
        $agent->setId(1);
        $agent->addConsultationFavoris($consultationFavori);

        $result = $this->consultationService->isFavoriByAgent($consultation, $agent);
        $this->assertEquals(true, $result);

        $consultationBis = new Consultation();
        $consultationBis->setId(3);

        $result = $this->consultationService->isFavoriByAgent($consultationBis, $agent);
        $this->assertEquals(false, $result);
    }

    public function testSetMessecForInitialization()
    {
        $configurationPlateforme = new ConfigurationPlateforme();

        $repositoryPlateformeVirtuelle = $this->createMock(ConfigurationPlateformeRepository::class);
        $repositoryPlateformeVirtuelle->expects($this->any())
            ->method('getConfigurationPlateforme')
            ->willReturn($configurationPlateforme);

        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryPlateformeVirtuelle);

        $consultation = new Consultation();
        $consultationReference = new Consultation();
        $consultationReference->setVersionMessagerie(1);

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $output = $consultationService->setMessecForInitialization($consultation);

        $this->assertEquals($consultationReference->getVersionMessagerie(), $output->getVersionMessagerie());
    }

    public function testSetPFVForInitialization()
    {
        $_SERVER['SERVER_NAME'] = 'https://atexo.com';

        $plateformeVirtuelle = new PlateformeVirtuelle();
        $plateformeVirtuelle->setDomain('https://atexo.com');
        $repositoryPlateformeVirtuelle = $this->createMock(ObjectRepository::class);
        $repositoryPlateformeVirtuelle->expects($this->any())
            ->method('findOneBy')
            ->willReturn($plateformeVirtuelle);

        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryPlateformeVirtuelle);

        $plateformeVirtuelleReference = new PlateformeVirtuelle();
        $plateformeVirtuelleReference->setDomain('https://atexo.com');
        $consultationReference = new Consultation();
        $consultationReference->setPlateformeVirtuelle($plateformeVirtuelleReference);
        $consultation = new Consultation();

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );
        $output = $consultationService->setPFVForInitialization($consultation);

        $this->assertEquals($consultationReference->getPlateformeVirtuelle(), $output->getPlateformeVirtuelle());
    }

    public function testSetLieuxExecutionForInitialization()
    {
        $fakeGeoOne = new class () extends GeolocalisationN2 {
            public function getId()
            {
                return 123;
            }
        };
        $fakeGeoTwo = new class () extends GeolocalisationN2 {
            public function getId()
            {
                return 125;
            }
        };

        $geoRepo = $this->createMock(GeolocalisationN2Repository::class);
        $geoRepo->expects($this->once())
            ->method('findBy')
            ->willReturn([$fakeGeoOne, $fakeGeoTwo]);

        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($geoRepo);

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $consultation = new Consultation();

        $output = $consultationService->setLieuxExecutionForInitialization($consultation, [75, 92]);
        $this->assertEquals(',,123,125,', $output->getLieuExecution());
    }

    public function testSetMontantMarcheForInitialization()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->once())->method('persist');

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $consultation = new Consultation();

        $output = $consultationService->setMontantMarcheForInitialization($consultation, 132465);
        $this->assertEquals(132465, $output->getDonneeComplementaire()->getMontantMarche());
    }

    public function testSetInvitePonctuelForInitializationIsPermanent()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->never())->method('persist');
        $em->expects($this->never())->method('flush');

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setInvitePermanentMonEntite(true);

        $agent = new Agent();
        $agent->setHabilitation($habilitationAgent);

        $organisme = new Organisme();
        $consultation = (new Consultation())->setOrganisme($organisme);

        $consultationService->setInvitePonctuelForInitialization($consultation, $agent);
    }

    public function testSetInvitePonctuelForInitializationIsPonctuel(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->expects($this->once())->method('persist');
        $em->expects($this->once())->method('flush');

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $em,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(ReferenceTypeService::class),
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setInvitePermanentMonEntite(false);

        $agent = new Agent();
        $agent->setHabilitation($habilitationAgent);

        $organisme = new Organisme();
        $consultation = (new Consultation())->setOrganisme($organisme);

        $consultationService->setInvitePonctuelForInitialization($consultation, $agent);
    }

    public function testSetReferenceUtilisateurAutoNumerotationEnabledForInitialization()
    {
        $consultation = new Consultation();
        $consultation->setReferenceUtilisateur('DEMO22-10');

        $refTypeService = $this->createMock(ReferenceTypeService::class);
        $refTypeService->expects($this->once())
            ->method('setReferenceType')
            ->willReturn($consultation);

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $refTypeService,
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $result = $consultationService->setReferenceUtilisateurForInitialization(
            new Consultation(),
            new Agent(),
            'PA-RESENCEMENT-123467'
        );

        $this->assertSame('DEMO22-10', $result->getReferenceUtilisateur());
    }

    public function testSetReferenceUtilisateurAutoNumerotationDisabledForInitialization()
    {
        $consultation = new Consultation();

        $refTypeService = $this->createMock(ReferenceTypeService::class);
        $refTypeService->expects($this->once())
            ->method('setReferenceType')
            ->willReturn($consultation);

        $consultationService = new ConsultationService(
            $this->createMock(PlateformeVirtuelleService::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(ObscureDataManagement::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(InterfaceSub::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(AtexoEnveloppeFichier::class),
            $this->createMock(InscritOperationsTracker::class),
            $this->createMock(AtexoConfiguration::class),
            $refTypeService,
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        $result = $consultationService->setReferenceUtilisateurForInitialization(
            new Consultation(),
            new Agent(),
            'PA-RESENCEMENT-123467'
        );

        $this->assertSame('PA-RESENCEMENT-123467', $result->getReferenceUtilisateur());
    }

    public function testSetConsultationWithParametreProcedure()
    {
        $result = $this->consultationService->setConsultationWithParametreProcedure(
            new Consultation(),
            $this->createMock(ProcedureEquivalenceAgentService::class)
        );

        $this->assertInstanceOf(Consultation::class, $result);
    }

    /**
     * @dataProvider providerTypeReponseElectronique
     */
    public function testGetTypeResponse(Consultation $consultation, string $expectedResult): void
    {
        $plateformeVirtuelleService = $this->createMock(PlateformeVirtuelleService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $obscureDataManagement = $this->createMock(ObscureDataManagement::class);
        $currentUser = $this->createMock(CurrentUser::class);
        $logger = $this->createMock(LoggerInterface::class);
        $interfaceSub = $this->createMock(InterfaceSub::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $atexoEnveloppeFichierService = $this->createMock(AtexoEnveloppeFichier::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $referenceTypeService = $this->createMock(ReferenceTypeService::class);

        $consultationService = new ConsultationService(
            $plateformeVirtuelleService,
            $translator,
            $em,
            $parameterBag,
            $obscureDataManagement,
            $currentUser,
            $logger,
            $interfaceSub,
            $atexoUtil,
            $atexoFichierOrganisme,
            $atexoEnveloppeFichierService,
            $inscritOperationTracker,
            $moduleStateChecker,
            $referenceTypeService,
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        self::assertSame($consultationService->getTypeResponse($consultation), $expectedResult);
    }

    public function providerTypeReponseElectronique(): array
    {
        return [
            [
                (new Consultation())->setReponseObligatoire(1),
                'OBLIGATOIRE'
            ],
            [
                (new Consultation())->setReponseObligatoire(0)->setAutoriserReponseElectronique(1),
                'AUTORISEE'
            ],
            [
                (new Consultation())->setReponseObligatoire(0)->setAutoriserReponseElectronique(0),
                'REFUSEE'
            ],
        ];
    }

    /**
     * @dataProvider providerTypeSignatureElectronique
     */
    public function testGetSignatureElectronique(Consultation $consultation, string $expectedResult): void
    {
        $plateformeVirtuelleService = $this->createMock(PlateformeVirtuelleService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $obscureDataManagement = $this->createMock(ObscureDataManagement::class);
        $currentUser = $this->createMock(CurrentUser::class);
        $logger = $this->createMock(LoggerInterface::class);
        $interfaceSub = $this->createMock(InterfaceSub::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $atexoEnveloppeFichierService = $this->createMock(AtexoEnveloppeFichier::class);
        $inscritOperationTracker = $this->createMock(InscritOperationsTracker::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $referenceTypeService = $this->createMock(ReferenceTypeService::class);

        $consultationService = new ConsultationService(
            $plateformeVirtuelleService,
            $translator,
            $em,
            $parameterBag,
            $obscureDataManagement,
            $currentUser,
            $logger,
            $interfaceSub,
            $atexoUtil,
            $atexoFichierOrganisme,
            $atexoEnveloppeFichierService,
            $inscritOperationTracker,
            $moduleStateChecker,
            $referenceTypeService,
            $this->createMock(FavorisService::class),
            $this->createMock(AgentInviterService::class),
            $this->createMock(AgentService::class),
            $this->createMock(Guests::class)
        );

        self::assertSame($consultationService->getSignatureElectronique($consultation), $expectedResult);
    }

    public function providerTypeSignatureElectronique(): array
    {
        return [
            [
                (new Consultation())->setSignatureOffre(1)->setAutoriserReponseElectronique(1),
                'REQUISE'
            ],
            [
                (new Consultation())->setSignatureOffre(2)->setAutoriserReponseElectronique(1),
                'AUTORISEE'
            ],
            [
                (new Consultation())->setSignatureOffre(0)->setAutoriserReponseElectronique(0),
                'NON_REQUISE'
            ],
        ];
    }
}
