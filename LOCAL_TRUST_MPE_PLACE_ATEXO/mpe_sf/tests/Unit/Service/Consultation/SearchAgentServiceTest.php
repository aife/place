<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use stdClass;
use ReflectionClass;
use ReflectionException;
use Exception;
use App\Exception\DataNotFoundException;
use App\Service\Consultation\SearchAgentService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchAgentServiceTest extends TestCase
{
    public function testDataWidgetsStatsEmpty(): void
    {
        $apiResponse = new stdClass();
        $paramUrlFromForm = [];

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->never())
            ->method('trans');

        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));
        $result = $searchAgentService->getDataWidgetsStats($apiResponse, $paramUrlFromForm);

        $this->assertIsArray($result);
        $this->assertCount(0, $result);
    }

    public function testDataWidgetsStatsOk(): void
    {
        $apiResponse = $this->apiResponse();
        $paramUrlFromForm = ['groupBy' => 'categorie'];

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->never())
            ->method('trans');

        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));
        $result = $searchAgentService->getDataWidgetsStats($apiResponse, $paramUrlFromForm);

        $this->assertIsArray($result);
        $this->assertCount(2, $result);
        $this->assertArrayHasKey('names', $result);
        $this->assertCount(3, $result['names']);
        $this->assertArrayHasKey('totals', $result);
        $this->assertCount(3, $result['totals']);
    }

    public function testDataWidgetsStatsStatutCalculeMultiOk(): void
    {
        $apiResponse = $this->apiResponse();
        unset($apiResponse->{'hydra:member'}[1]);
        unset($apiResponse->{'hydra:member'}[2]);

        $paramUrlFromForm = ['groupBy' => 'statutCalcule'];

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->any())
            ->method('trans')
            ->willReturnMap([
                ['STATUS_LABEL_CONSULTATION', [], null, null, 'Consultation'],
                ['STATUS_LABEL_OUVERTURE_ANALYSE', [], null, null, 'Ouverture et analyse'],
                ['STATUS_LABEL_DECISION', [], null, null, 'Décision'],
                ['STATUS_LABEL_ELABORATION', [], null, null, 'Elaboration'],
                ['STATUS_LABEL_PREPARATION', [], null, null, 'Préparation'],
            ]);

        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));
        $result = $searchAgentService->getDataWidgetsStats($apiResponse, $paramUrlFromForm);
        $this->assertIsArray($result);
        $this->assertCount(5, $result);
        $this->assertArrayHasKey('names', $result);
        $this->assertArrayHasKey('colors', $result);
        $this->assertArrayHasKey('total_consultation', $result);
        $this->assertArrayHasKey('status_key_trad', $result);
        $this->assertCount(2, $result['names']);
        $this->assertSame(
            'Consultation / Ouverture et analyse / Décision',
            $result['names'][0]
        );
        $this->assertArrayHasKey('totals', $result);
        $this->assertCount(2, $result['totals']);
        $this->assertEquals($result['total_consultation'], array_sum($result['totals']));
    }

    public function testDataWidgetsStatsStatutCalculeSimpleOk(): void
    {
        $apiResponse = $this->apiResponse();
        unset($apiResponse->{'hydra:member'}[0]);
        unset($apiResponse->{'hydra:member'}[3]);

        $paramUrlFromForm = ['groupBy' => 'statutCalcule'];

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->any())
            ->method('trans')
            ->willReturnMap([
                ['STATUS_LABEL_CONSULTATION', [], null, null, 'Consultation'],
                ['STATUS_LABEL_OUVERTURE_ANALYSE', [], null, null, 'Ouverture et analyse'],
                ['STATUS_LABEL_DECISION', [], null, null, 'Décision'],
                ['STATUS_LABEL_ELABORATION', [], null, null, 'Elaboration'],
                ['STATUS_LABEL_PREPARATION', [], null, null, 'Préparation'],
            ]);
        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));
        $result = $searchAgentService->getDataWidgetsStats($apiResponse, $paramUrlFromForm);

        $this->assertIsArray($result);
        $this->assertCount(5, $result);
        $this->assertArrayHasKey('colors', $result);
        $this->assertArrayHasKey('total_consultation', $result);
        $this->assertArrayHasKey('status_key_trad', $result);
        $this->assertArrayHasKey('names', $result);
        $this->assertCount(2, $result['names']);
        $this->assertSame(
            'Ouverture et analyse',
            $result['names'][0]
        );
        $this->assertArrayHasKey('totals', $result);
        $this->assertCount(2, $result['totals']);
    }

    public function testGetDataWidgetsStatsTypeProcedureSort(): void
    {
        $data = [
            'names'     => ['Travaux', 'Fourniture', 'Service'],
            'totals'    => [110, 30, 5]
        ];

        $apiResponse = $this->apiResponse();

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->never())
            ->method('trans');

        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));

        $reflectionClass = new ReflectionClass(SearchAgentService::class);
        $methodPrivate = $reflectionClass->getMethod('getDataWidgetsStatsForCategorieOrTypeProcedure');
        $methodPrivate->setAccessible(true);

        $result = $methodPrivate->invokeArgs($searchAgentService, [$apiResponse->{'hydra:member'}, true]);

        $this->assertIsArray($result);
        $this->assertEquals($data, $result);
    }

    /**
     * @return void
     * @throws ReflectionException
     */
    public function testSortStatutCalcule(): void
    {
        $items = [
            'total_consultation' => 839,
            'totals' => [25, 214, 238, 1, 25, 266, 70],
            'names' => [
                'Préparation',
                'Elaboration',
                'Consultation',
                'Ouverture et analyse',
                'Ouverture et analyse / Décision',
                'Décision',
                'Consultation / Ouverture et analyse / Décision',
            ],
            'status_key_trad' => [
                'PREPARATION',
                'ELABORATION',
                'CONSULTATION',
                'OUVERTURE_ANALYSE',
                'OUVERTURE_ANALYSE / DECISION',
                'DECISION',
                'CONSULTATION / OUVERTURE_ANALYSE / DECISION',
            ]
        ];

        $expectedResult = [
            'total_consultation' => 839,
            'totals' => [214, 25, 238, 70, 1, 25, 266],
            'names' => [
                'Elaboration',
                'Préparation',
                'Consultation',
                'Consultation / Ouverture et analyse / Décision',
                'Ouverture et analyse',
                'Ouverture et analyse / Décision',
                'Décision'
            ],
            'status_key_trad' => [
                'ELABORATION',
                'PREPARATION',
                'CONSULTATION',
                'CONSULTATION / OUVERTURE_ANALYSE / DECISION',
                'OUVERTURE_ANALYSE',
                'OUVERTURE_ANALYSE / DECISION',
                'DECISION'
            ],
            'colors' => ['#FBC02D', '#FF7D4D', '#2196F3', '#1976D2', '#01D380', '#00B7A5', '#12856D']
        ];

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->expects($this->any())
            ->method('trans')
            ->willReturnMap([
                ['STATUS_LABEL_CONSULTATION', [], null, null, 'Consultation'],
                ['STATUS_LABEL_OUVERTURE_ANALYSE', [], null, null, 'Ouverture et analyse'],
                ['STATUS_LABEL_DECISION', [], null, null, 'Décision'],
                ['STATUS_LABEL_ELABORATION', [], null, null, 'Elaboration'],
                ['STATUS_LABEL_PREPARATION', [], null, null, 'Préparation'],
            ]);
        $searchAgentService = new SearchAgentService($translatorMock, $this->createMock(LoggerInterface::class));

        $reflectionClass = new ReflectionClass(SearchAgentService::class);
        $methodPrivate = $reflectionClass->getMethod('sortStatutCalcule');
        $methodPrivate->setAccessible(true);

        $result = $methodPrivate->invokeArgs($searchAgentService, [$items]);

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetDataWidgetsStatsForStatutCalculeError(): void
    {
        $items = [
            (object) [
                '0' => (object) [
                    '@type' => 'Consultation',
                    '@id' => '/api/v2/consultations/501226',
                    'id' => 501226
                ],
                'statutCalcule' => '1',
                'total' => 401
            ]
        ];

        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->any())
            ->method('trans')
            ->willThrowException(new Exception());

        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())
            ->method('critical');

        $searchAgentService = new SearchAgentService($translator, $logger);

        $reflectionClass = new ReflectionClass(SearchAgentService::class);
        $methodPrivate = $reflectionClass->getMethod('getDataWidgetsStatsForStatutCalcule');
        $methodPrivate->setAccessible(true);

        $this->expectException(Exception::class);
        $methodPrivate->invokeArgs($searchAgentService, [$items]);
    }

    public function testSortStatutCalculeErrorWithItemsEmpty(): void
    {
        $items = [];

        $translator = $this->createMock(TranslatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $searchAgentService = new SearchAgentService($translator, $logger);

        $reflectionClass = new ReflectionClass(SearchAgentService::class);
        $methodPrivate = $reflectionClass->getMethod('sortStatutCalcule');
        $methodPrivate->setAccessible(true);

        $this->expectException(DataNotFoundException::class);
        $methodPrivate->invokeArgs($searchAgentService, [$items]);
    }

    public function testSortStatutCalculeWithItemsNoConsultations(): void
    {
        $items = ['total_consultation' => 0];

        $translator = $this->createMock(TranslatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $searchAgentService = new SearchAgentService($translator, $logger);

        $reflectionClass = new \ReflectionClass(SearchAgentService::class);
        $methodPrivate = $reflectionClass->getMethod('sortStatutCalcule');
        $methodPrivate->setAccessible(true);

        $result = $methodPrivate->invokeArgs($searchAgentService, [$items]);

        $this->assertEquals(['total_consultation' => 0], $result);
    }

    /**
     * Simulation API response
     */
    private function apiResponse(): stdClass
    {
        $obj3 = new stdClass();
        $obj3->{'libelle'} = 'Service';
        $obj3->{'total'} = '5';
        $obj3->{'statutCalcule'} = '4';

        $obj1 = new stdClass();
        $obj1->{'libelle'} = 'Travaux';
        $obj1->{'total'} = '10';
        $obj1->{'statutCalcule'} = '3,4';

        $obj2 = new stdClass();
        $obj2->{'libelle'} = 'Fourniture';
        $obj2->{'total'} = '30';
        $obj2->{'statutCalcule'} = '3';

        $obj4 = new stdClass();
        $obj4->{'libelle'} = 'Travaux';
        $obj4->{'total'} = '100';
        $obj4->{'statutCalcule'} = '2,3,4';

        $apiResponse = new stdClass();
        $apiResponse->{'hydra:member'} = [
            $obj1, $obj2, $obj3, $obj4
        ];

        return $apiResponse;
    }
}
