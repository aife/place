<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Consultation;

use App\Entity\Consultation;
use App\Entity\ProcedureEquivalence;
use App\Entity\TDumeContexte;
use App\Repository\TDumeContexteRepository;
use App\Service\AtexoConfiguration;
use App\Service\Consultation\ConsultationValidationService;
use App\Service\Dume\DumeService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Entity\ConsultationComptePubTest;

class ConsultationValidationServiceTest extends TestCase
{
    public function testConsultationValidationExist(): void
    {
        $donneComplementaire = new class extends Consultation\DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 1;
            }
        };
        $consultationComptePub = new class extends Consultation\ConsultationComptePub {
            public function getId(): int
            {
                return 1;
            }
        };

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $dumeService = $this->createMock(DumeService::class);

        $repo = $this->createMock(TDumeContexteRepository::class);
        $repo->expects($this->any())
            ->method('findOneBy')
            ->willReturn(new TDumeContexte());

        $entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($repo);

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoConfig = $this->createMock(AtexoConfiguration::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $security = $this->createMock(Security::class);

        $translatorMock->expects($this->exactly(24))->method('trans')->willReturnOnConsecutiveCalls(
            "Durée du marché ou Délai d'exécution des prestations",
            "Visite obligatoire",
            "Critère(s) d'attribution",
            "CCAG de référence",
            "Délai de validité des offres",
            "Forme de prix",
            "Code postal (Adresse de l'organisme qui passe le marché)",
            "Ville (Adresse de l'organisme qui passe le marché)",
            "Téléphone (Adresse de l'organisme qui passe le marché)",
            "Email (Adresse de l'organisme qui passe le marché)",
            "URL (Adresse de l'instance chargée des procédures de recours)",
            "Nom de l'organisme (Adresse de facturation de la publicité )",
            "Adresse (Adresse de l'instance chargée des procédures de recours)",
            "Code postal (Adresse de l'instance chargée des procédures de recours)",
            "Ville (Adresse de l'instance chargée des procédures de recours)",
            "URL (Adresse de l'instance chargée des procédures de recours)",
            "Nom de l'organisme (Adresse de facturation de la publicité )",
            "Adresse (Adresse de facturation de la publicité )",
            "Code postal (Adresse de facturation de la publicité )",
            "Ville (Adresse de facturation de la publicité )",
            "Aptitude à exercer l'activité professionnelle ; conditions / moyens de preuve",
            "Capacité économique et financière ;  conditions / moyens de preuve",
            "Capacités techniques et professionnelles ;  conditions / moyens de preuve",
            "Constitution des dossiers de réponses",
        );

        $consultationValidation = new ConsultationValidationService(
            $entityManager,
            $translatorMock,
            $atexoConfig,
            $parameterBag,
            $security,
            $dumeService,
        );

        $consultationComptePub->setDonneeComplementaire($donneComplementaire);
        $consultationComptePub->setPrm(0);

        $donneComplementaire->setDureeMarche(0);
        $donneComplementaire->setVisiteObligatoire(0);
        $donneComplementaire->setIdCritereAttribution(0);
        $donneComplementaire->setIdCcagReference(0);
        $donneComplementaire->setIdFormePrix(0);
        $donneComplementaire->setActiviteProfessionel(0);
        $donneComplementaire->setEconomiqueFinanciere(0);
        $donneComplementaire->setTechniquesProfessionels(0);
        $donneComplementaire->setConsultationComptePub($consultationComptePub);
        $donneComplementaire->setMotsCles(1);

        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setDonneeComplementaire($donneComplementaire);
        $consultation->setDonneeComplementaireObligatoire(true);
        $consultation->setDonneePubliciteObligatoire(true);


        $reponse = $consultationValidation->consultationValidate($consultation);

        $this->assertEquals(
            [
                0 => "Durée du marché ou Délai d'exécution des prestations",
                1 => "Visite obligatoire",
                2 => "Critère(s) d'attribution",
                3 => "CCAG de référence",
                4 => "Délai de validité des offres",
                5 => "Forme de prix",
                6 => "Code postal (Adresse de l'organisme qui passe le marché)",
                7 => "Ville (Adresse de l'organisme qui passe le marché)",
                8 => "Téléphone (Adresse de l'organisme qui passe le marché)",
                9 => "Email (Adresse de l'organisme qui passe le marché)",
                10 => "URL (Adresse de l'instance chargée des procédures de recours)",
                11 => "Nom de l'organisme (Adresse de facturation de la publicité )",
                12 => "Adresse (Adresse de l'instance chargée des procédures de recours)",
                13 => "Code postal (Adresse de l'instance chargée des procédures de recours)",
                14 => "Ville (Adresse de l'instance chargée des procédures de recours)",
                15 => "URL (Adresse de l'instance chargée des procédures de recours)",
                16 => "Nom de l'organisme (Adresse de facturation de la publicité )",
                17 => "Adresse (Adresse de facturation de la publicité )",
                18 => "Code postal (Adresse de facturation de la publicité )",
                19 => "Ville (Adresse de facturation de la publicité )",
                20 => "Aptitude à exercer l'activité professionnelle ; conditions / moyens de preuve",
                21 => "Capacité économique et financière ;  conditions / moyens de preuve",
                22 => "Capacités techniques et professionnelles ;  conditions / moyens de preuve",
                23 =>  "Constitution des dossiers de réponses",
            ],
            $reponse
        );
    }

    public function testConsultationValidationNotExist(): void
    {
        $donneComplementaire = new class extends Consultation\DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 1;
            }
        };
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $repo = $this->createMock(TDumeContexteRepository::class);
        $repo->expects($this->any())
            ->method('findOneBy')
            ->willReturn(new TDumeContexte());

        $entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($repo);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoConfig = $this->createMock(AtexoConfiguration::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $security = $this->createMock(Security::class);
        $dumeService = $this->createMock(DumeService::class);
        $consultationValidation = new ConsultationValidationService(
            $entityManager,
            $translatorMock,
            $atexoConfig,
            $parameterBag,
            $security,
            $dumeService,
        );
        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setDonneeComplementaire($donneComplementaire);
        $consultation->setDonneeComplementaireObligatoire(false);
        $consultation->setDonneePubliciteObligatoire(false);
        $consultation->setEnvOffre('1');
        $reponse = $consultationValidation->consultationValidate($consultation);

        $this->assertEquals(
            [],
            $reponse
        );
    }
}
