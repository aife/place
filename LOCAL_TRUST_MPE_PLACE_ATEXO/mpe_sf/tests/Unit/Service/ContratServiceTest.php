<?php

namespace Tests\Unit\Service;

use App\Service\Consultation\ConsultationService;
use DateTime;
use ReflectionClass;
use ApiPlatform\Core\Api\IriConverterInterface;
use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\DataTransformer\Output\ContratTitulaireOutputDataTransformer;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\TechniqueAchat;
use App\Model\DonneesEssentielles\Marche;
use App\Entity\ModificationContrat;
use App\Entity\Consultation\ClausesN1;
use App\Entity\TypeContrat;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ConsiderationsSocialesType;
use App\Repository\CategoriesConsiderationsSocialesRepository;
use App\Repository\TypeContratRepository;
use App\Serializer\ContratTitulaireNormalizer;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use App\Service\ContratService;
use App\Service\DataTransformer\ClausesTransformer;
use App\Service\DataTransformer\ContratTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ConsiderationsEnvironnementalesType;

class ContratServiceTest extends AtexoTestCase
{
    /**
     * Test de la fonction avec num_donnees_essentielles_manuel = 1.
     */
    public function testSetModificationContratForMarcheAvecDECPManuelle()
    {
        $em = $this->getAtexoObjectManager();

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('pmi-min-1');
        $contratTitulaire->setNumIdUniqueMarchePublic('ADO-0710-AA');

        $marcheInitiale = new Marche();
        $marcheInitiale->setId('ADO-0710-AA');

        $modificationContrat = new ModificationContrat();
        $modificationContrat->setIdContratTitulaire($contratTitulaire);
        $modificationContrat->setMontant(1);
        $modificationContrat->setNumOrdre(2);
        $modificationContrat->setDureeMarche(null);
        $modificationContrat->setIdEtablissement(null);
        $modificationContrat->setObjetModification('OBJET');
        $modificationContrat->setDateSignature(new DateTime());
        $modificationContrat->setDateModification(new DateTime());

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $modificationsContrat = [$modificationContrat];
        $logger = $this->createMock(LoggerInterface::class);

        $clausesTransformer = $this->createMock(ClausesTransformer::class);

        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $clausesTransformer,
        );
        $marche = $contratService->setModificationContratForMarche($modificationsContrat, $marcheInitiale);

        $this->assertEquals($contratTitulaire->getNumIdUniqueMarchePublic(), $marche->getId());
    }

    /**
     * Test de la fonction avec num_donnees_essentielles_manuel = 0.
     */
    public function testSetModificationContratForMarcheSansDECPManuelle()
    {
        $em = $this->getAtexoObjectManager();

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('pmi-min-1');
        $contratTitulaire->setNumIdUniqueMarchePublic('ADO-0710-AA');

        $marcheInitiale = new Marche();
        $marcheInitiale->setId('ADO-0710-AA');

        $modificationContrat = new ModificationContrat();
        $modificationContrat->setIdContratTitulaire($contratTitulaire);
        $modificationContrat->setMontant(1);
        $modificationContrat->setNumOrdre(2);
        $modificationContrat->setDureeMarche(null);
        $modificationContrat->setIdEtablissement(null);
        $modificationContrat->setObjetModification('OBJET');
        $modificationContrat->setDateSignature(new DateTime());
        $modificationContrat->setDateModification(new DateTime());

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(false);
        $modificationsContrat = [$modificationContrat];

        $logger = $this->createMock(LoggerInterface::class);
        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );
        $marche = $contratService->setModificationContratForMarche($modificationsContrat, $marcheInitiale);

        $this->assertEquals('ADO-0710-02', $marche->getId());
    }

    public function testDisplayMontantAvecChapeauAvecMontant()
    {
        $contratChapeau = new ContratTitulaire();
        $contratChapeau->setMontantMaxEstime(1000);

        //set idContratTitulaire du contrat chapeau
        $class = new ReflectionClass($contratChapeau);
        $property = $class->getProperty('idContratTitulaire');
        $property->setAccessible(true);
        $property->setValue($contratChapeau, 1);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('IKO-ORG');
        $contratTitulaire->setNumIdUniqueMarchePublic('TEST-CONTRAT-MPE-10173');
        $contratTitulaire->setIdTypeContrat(3);
        $contratTitulaire->setChapeau($contratChapeau->getIdContratTitulaire());

        // set IdCOntratTitulaire
        $class = new ReflectionClass($contratTitulaire);
        $property = $class->getProperty('idContratTitulaire');
        $property->setAccessible(true);
        $property->setValue($contratTitulaire, 10173);

        $repositoryCategories = $this->createMock(CategoriesConsiderationsSocialesRepository::class);
        $repositoryCategories->expects($this->any())
            ->method('getAllFormatedCategories')
            ->willReturn([]);

        //create a contrat type and set values avecChapeau et avecMontantMax
        $typeContrat = new TypeContrat();
        $typeContrat->setAvecChapeau(1);
        $typeContrat->setAvecMontantMax(1);

        $class = new ReflectionClass($typeContrat);
        $property = $class->getProperty('idTypeContrat');
        $property->setAccessible(true);
        $property->setValue($typeContrat, 3);

        $repositoryContrat = $this->createMock(ObjectRepository::class);
        $repositoryContrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idContratTitulaire' => 10173])
            ->willReturn($contratTitulaire);

        $repositoryTypeContrat = $this->createMock(ObjectRepository::class);
        $repositoryTypeContrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idTypeContrat' => 3])
            ->willReturn($typeContrat);

        $repositoryContratChapeau = $this->createMock(ObjectRepository::class);
        $repositoryContratChapeau->expects($this->any())
            ->method('findOneBy')
            ->with(['idContratTitulaire' => $contratTitulaire->getChapeau()])
            ->willReturn($contratChapeau);

        $marche = new Marche();
        $marche->setId('TEST-MARCHE-MPE-10173');

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(false);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryContrat,
                $repositoryTypeContrat,
                $repositoryContratChapeau,
                $repositoryCategories
            );
        $logger = $this->createMock(LoggerInterface::class);
        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );
        $contratService->displayMontant($contratTitulaire, $marche);
        $this->assertEquals(
            $marche->getMontant(),
            1000,
            'Le montant du marché est équale au montant max du contrat chapeau'
        );
    }

    public function testDisplayMontantSansChapeauAvecMontant()
    {
        $repositoryCategories = $this->createMock(CategoriesConsiderationsSocialesRepository::class);
        $repositoryCategories->expects($this->any())
            ->method('getAllFormatedCategories')
            ->willReturn([]);
        //create a contrat type and set values avecChapeau et avecMontantMax
        $typeContrat = new TypeContrat();
        $typeContrat->setAvecChapeau(0);
        $typeContrat->setAvecMontantMax(1);

        $class = new ReflectionClass($typeContrat);
        $property = $class->getProperty('idTypeContrat');
        $property->setAccessible(true);
        $property->setValue($typeContrat, 3);

        $repositoryTypeContrat = $this->createMock(ObjectRepository::class);
        $repositoryTypeContrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idTypeContrat' => 3])
            ->willReturn($typeContrat);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('IKO-ORG');
        $contratTitulaire->setNumIdUniqueMarchePublic('TEST-CONTRAT-MPE-10173');
        $contratTitulaire->setIdTypeContrat(3);
        $contratTitulaire->setMontantMaxEstime(9000);

        // set IdCOntratTitulaire
        $class = new ReflectionClass($contratTitulaire);
        $property = $class->getProperty('idContratTitulaire');
        $property->setAccessible(true);
        $property->setValue($contratTitulaire, 10173);

        $marche = new Marche();
        $marche->setId('TEST-MARCHE-MPE-10173');

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(false);

        $contrat = $this->createMock(ObjectRepository::class);
        $contrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idContratTitulaire' => 10173])
            ->willReturn($contratTitulaire);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $contrat,
                $repositoryTypeContrat,
                $repositoryCategories
            );
        $logger = $this->createMock(LoggerInterface::class);
        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );
        $contratService->displayMontant($contratTitulaire, $marche);
        $this->assertEquals($marche->getMontant(), 9000, 'Le montant du marché est équale au montant max du contrat');
    }

    public function testDisplayMontantSansChapeauSansMontant()
    {
        $repositoryCategories = $this->createMock(CategoriesConsiderationsSocialesRepository::class);
        $repositoryCategories->expects($this->any())
            ->method('getAllFormatedCategories')
            ->willReturn([]);
        //create a contrat type and set values avecChapeau et avecMontantMax
        $typeContrat = new TypeContrat();
        $typeContrat->setAvecChapeau(0);
        $typeContrat->setAvecMontantMax(0);

        $class = new ReflectionClass($typeContrat);
        $property = $class->getProperty('idTypeContrat');
        $property->setAccessible(true);
        $property->setValue($typeContrat, 3);

        $repositoryTypeContrat = $this->createMock(ObjectRepository::class);
        $repositoryTypeContrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idTypeContrat' => 3])
            ->willReturn($typeContrat);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('IKO-ORG');
        $contratTitulaire->setNumIdUniqueMarchePublic('TEST-CONTRAT-MPE-10173');
        $contratTitulaire->setIdTypeContrat(3);
        $contratTitulaire->setMontant(50);

        // set IdCOntratTitulaire
        $class = new ReflectionClass($contratTitulaire);
        $property = $class->getProperty('idContratTitulaire');
        $property->setAccessible(true);
        $property->setValue($contratTitulaire, 10173);

        $marche = new Marche();
        $marche->setId('TEST-MARCHE-MPE-10173');

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(false);

        $contrat = $this->createMock(ObjectRepository::class);
        $contrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idContratTitulaire' => 10173])
            ->willReturn($contratTitulaire);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $contrat,
                $repositoryTypeContrat,
                $repositoryCategories
            );
        $logger = $this->createMock(LoggerInterface::class);
        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );
        $contratService->displayMontant($contratTitulaire, $marche);
        $this->assertEquals($marche->getMontant(), 50, 'Le montant du marché est équale au montant du contrat');
    }

    public function testDisplayMontantPasDeMontant()
    {
        $repositoryCategories = $this->createMock(CategoriesConsiderationsSocialesRepository::class);
        $repositoryCategories->expects($this->any())
            ->method('getAllFormatedCategories')
            ->willReturn([]);
        //create a contrat type and set values avecChapeau et avecMontantMax
        $typeContrat = new TypeContrat();
        $typeContrat->setAvecChapeau(0);
        $typeContrat->setAvecMontantMax(1);

        $class = new ReflectionClass($typeContrat);
        $property = $class->getProperty('idTypeContrat');
        $property->setAccessible(true);
        $property->setValue($typeContrat, 3);

        $repositoryTypeContrat = $this->createMock(ObjectRepository::class);
        $repositoryTypeContrat->expects($this->any())
            ->method('findOneBy')
            ->with(['idTypeContrat' => 3])
            ->willReturn($typeContrat);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setOrganisme('IKO-ORG');
        $contratTitulaire->setNumIdUniqueMarchePublic('TEST-CONTRAT-MPE-10173');
        $contratTitulaire->setIdTypeContrat(3);
        $contratTitulaire->setMontant(50);

        // set IdCOntratTitulaire
        $class = new ReflectionClass($contratTitulaire);
        $property = $class->getProperty('idContratTitulaire');
        $property->setAccessible(true);
        $property->setValue($contratTitulaire, 10173);

        $marche = new Marche();
        $marche->setId('TEST-MARCHE-MPE-10173');

        $container = $this->createMock(ContainerInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $moduleStateChecker = $this->createMock(AtexoConfiguration::class);
        $moduleStateChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(false);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryTypeContrat,
                $repositoryCategories
            );
        $logger = $this->createMock(LoggerInterface::class);
        $contratService = new ContratService(
            $container,
            $translatorMock,
            $em,
            $atexoUtil,
            $moduleStateChecker,
            $logger,
            $this->createMock(ClausesTransformer::class)
        );
        $contratService->displayMontant($contratTitulaire, $marche);

        $this->assertEquals(
            $marche->getMontant(),
            0,
            'Le montant du marché est zéro puisqu\'on n\'a pas de contrat chapeau'
        );
    }

    public function testTechniqueAchatLibelleNull()
    {
        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($typeContratRepo);

        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $entityManagerMock,
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $consultation = new Consultation();
        $consultation->setTypeMarche(new TypeContrat());

        $result = $contratService->getTechniqueAchatLibelle($consultation);

        $this->assertNull($result);
    }

    public function testTypeContratTechniqueAchatLibelle()
    {
        $techniqueAchat = new TechniqueAchat();
        $techniqueAchat->setLibelle('Sans objet');

        $typeContrat = new TypeContrat();
        $typeContrat->setTechniqueAchat($techniqueAchat);

        $consultation = new Consultation();
        $consultation->setTypeMarche($typeContrat);

        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo->expects($this->once())
            ->method('find')
            ->willReturn($typeContrat);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($typeContratRepo);

        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $entityManagerMock,
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $result = $contratService->getTechniqueAchatLibelle($consultation);

        $this->assertSame('Sans objet', $result);
    }

    public function testConsultationTechniqueAchatLibelle()
    {
        $techniqueAchat = new TechniqueAchat();
        $techniqueAchat->setLibelle('Sans objet');

        $typeContrat = new TypeContrat();
        $typeContrat->setTechniqueAchat($techniqueAchat);

        $consultation = new Consultation();
        $consultation->setTypeMarche($typeContrat);
        $consultation->setAutreTechniqueAchat('Autre technique achat');

        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo->expects($this->once())
            ->method('find')
            ->willReturn($typeContrat);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($typeContratRepo);

        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $entityManagerMock,
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $result = $contratService->getTechniqueAchatLibelle($consultation);

        $this->assertSame('Autre technique achat', $result);
    }

    public function testGetTechniqueAchatLibelleByTypeContratId()
    {
        $techniqueAchat = new TechniqueAchat();
        $techniqueAchat->setLibelle('Sans objet');

        $typeContrat = new TypeContrat();
        $typeContrat->setTechniqueAchat($techniqueAchat);

        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo->expects($this->once())
            ->method('find')
            ->willReturn($typeContrat);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($typeContratRepo);

        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $entityManagerMock,
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $result = $contratService->getTechniqueAchatLibelleByTypeContratId(1);

        $this->assertSame('Sans objet', $result);
    }

    public function testGetTechniqueAchatLibelleByTypeContratIdWithAnotherLabel()
    {
        $techniqueAchat = new TechniqueAchat();
        $techniqueAchat->setLibelle('Un autre libelle');

        $typeContrat = new TypeContrat();
        $typeContrat->setTechniqueAchat($techniqueAchat);

        $typeContratRepo = $this->createMock(TypeContratRepository::class);
        $typeContratRepo->expects($this->once())
            ->method('find')
            ->willReturn($typeContrat);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($typeContratRepo);

        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $entityManagerMock,
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $result = $contratService->getTechniqueAchatLibelleByTypeContratId(1);

        $this->assertSame('Un autre libelle', $result);
    }

    public function testGetInitConsiderationsEnvironnementalesType()
    {
        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $reflectionClass = new ReflectionClass(ContratService::class);
        $method = $reflectionClass->getMethod('getInitConsiderationsEnvironnementalesType');
        $method->setAccessible(true);
        $response = $method->invoke($contratService);

        $this->assertInstanceOf(ConsiderationsEnvironnementalesType::class, $response);
        $this->assertObjectHasAttribute('clausesEnvironnementalesSpecificationTechnique', $response);
        $this->assertObjectHasAttribute('clausesEnvironnementalesConditionExecution', $response);
        $this->assertObjectHasAttribute('criteresEnvironnementauxAttribution', $response);
    }

    public function testGetInitConsiderationsSocialesType()
    {
        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $reflectionClass = new ReflectionClass(ContratService::class);
        $method = $reflectionClass->getMethod('getInitConsiderationsSocialesType');
        $method->setAccessible(true);
        $response = $method->invoke($contratService);

        $this->assertInstanceOf(ConsiderationsSocialesType::class, $response);

        $this->assertObjectHasAttribute('clausesSocialesConditionExecution', $response);
        $this->assertObjectHasAttribute('clausesSocialesSpecificationTechnique', $response);
        $this->assertObjectHasAttribute('criteresSociauxAttribution', $response);
        $this->assertObjectHasAttribute('marcheReserve', $response);
        $this->assertObjectHasAttribute('objetInsertion', $response);
    }

    public function testGetAchatResponsablesType(): void
    {
        $clauses = [
            [
                'slug' => 'clausesSociales',
                'label' => 'Considération(s) sociale(s)',
                'iri' => '/app.php/api/v2/referentiels/clauses-n1s/1',
                'clausesN2' => [
                    [
                        'slug' => 'conditionExecution',
                        'label' => 'Le marché public comprend une clause sociale comme condition d \'exécution',
                        'iri' => '/app.php/api/v2/referentiels/clauses-n2s/1',
                        'clausesN3' => [
                            [
                                'slug' => 'clauseSocialeFormationScolaire',
                                'label' => 'Clause sociale de formation sous statut scolaire',
                                'iri' => '/app.php/api/v2/referentiels/clauses-n3s/2'
                            ],
                            [
                                'slug' => 'commerceEquitable',
                                'label' => 'Commerce équitable',
                                'iri' => '/app.php/api/v2/referentiels/clauses-n3s/4'
                            ]
                        ]
                    ]
                ]
            ]
        ];

        $contratService = $this->createPartialMock(ContratService::class, ['getOuPutClauses']);
        $contratService->method('getOuPutClauses')->willReturn($clauses);

        $response = $contratService->getAchatResponsablesType(new ContratTitulaire());

        $this->assertCount(1, $response);
        $this->assertObjectHasAttribute('iri', $response[0]);
    }

    public function testCreateObjectStdClass(): void
    {
        $contratService = new ContratService(
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ClausesTransformer::class)
        );

        $reflectionClass = new \ReflectionClass(ContratService::class);
        $methodPrivate = $reflectionClass->getMethod('createObjectStdClass');
        $methodPrivate->setAccessible(true);

        $clauses = [
                [
                    'slug' => 'clauseSocialeFormationScolaire',
                    'label' => 'Clause sociale de formation sous statut scolaire',
                    'iri' => '/app.php/api/v2/referentiels/clauses-n3s/2'
                ],
                [
                    'slug' => 'commerceEquitable',
                    'label' => 'Commerce équitable',
                    'iri' => '/app.php/api/v2/referentiels/clauses-n3s/4'
                ]
        ];

        $response = $methodPrivate->invokeArgs($contratService, $clauses);

        $this->assertIsObject($response);
        $this->assertObjectHasAttribute('iri', $response);
        $this->assertObjectHasAttribute('label', $response);
        $this->assertObjectHasAttribute('slug', $response);
    }
}
