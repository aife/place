<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Service\WebservicesMpeConsultations;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebservicesMpeConsultationsTest extends TestCase
{
    public function testGetWebServiceQueryParams(): void
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $security = $this->createMock(Security::class);
        $tokenManager = $this->createMock(JWTTokenManagerInterface::class);

        $reflectionClass = new \ReflectionClass(WebservicesMpeConsultations::class);
        $protectedMethod = $reflectionClass->getMethod('searchAgents');
        $protectedMethod ->setAccessible(true);

        $client = new MockHttpClient([new MockResponse(
            '{"hydra:member":[{"id":20,"login":"support","email":"support@atexo.com"},
            {"id":22,"login":"agent1","email":"agent1@atexo.com"}]}',
            ['http_code' => 200]
        )]);

        $parameterBag
            ->expects(self::exactly(2))
            ->method('get')
            ->withConsecutive(
                ['PF_URL_REFERENCE'],
                ['timeOutService'],
            )
            ->willReturnOnConsecutiveCalls(
                'https://url-mpe.com',
                20,
            )
        ;

        $agent = new Agent();
        $security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($agent)
        ;

        $tokenManager
            ->expects(self::once())
            ->method('create')
            ->with($agent)
        ;

        $webservicesMpeConsultations = new WebservicesMpeConsultations(
            $client,
            $logger,
            $parameterBag,
            $security,
            $tokenManager
        );

        $wsResponse = $protectedMethod->invokeArgs($webservicesMpeConsultations, []);

        $result = json_decode($wsResponse);

        self::assertSame($result->{"hydra:member"}[0]->login, 'support');
        self::assertSame($result->{"hydra:member"}[1]->login, 'agent1');
    }
}
