<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Clauses;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Repository\ConsultationRepository;
use App\Repository\Referentiel\Consultation\ClausesN1Repository;
use App\Repository\Referentiel\Consultation\ClausesN2Repository;
use App\Repository\Referentiel\Consultation\ClausesN3Repository;
use App\Repository\Consultation\ClausesN1Repository as ClausesRepository;
use App\Entity\Referentiel\Consultation\ClausesN2 as ReferentielClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3 as ReferentielClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN1 as ReferentielClausesN1;
use App\Service\AgentTechniqueTokenService;
use App\Service\Clauses\ClauseUseApiV0;
use App\Service\Clauses\ClauseUsePrado;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionClass;

class ClauseUseApiV0Test extends TestCase
{
    public function testGetIriFromItem()
    {
        $iriConverterMock =  $this->createMock(IriConverterInterface::class);
        $iriConverterMock->expects($this->once())
            ->method('getIriFromItem')
            ->willReturn('/api.php/api/v2/referentiels/clauses-n1s/1')
            ;
        $clauseApiV0Service = new ClauseUseApiV0(
            $this->createMock(ClausesN1Repository::class),
            $this->createMock(ClausesN2Repository::class),
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesRepository::class),
            $iriConverterMock,
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ClauseUsePrado::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ConsultationRepository::class)
        );

        $reflectionClass = new ReflectionClass(ClauseUseApiV0::class);
        $method = $reflectionClass->getMethod('getIriFromItem');
        $method->setAccessible(true);

        $uri = $method->invoke($clauseApiV0Service, new ReferentielClausesN1());

        $this->assertEquals('/api/v2/referentiels/clauses-n1s/1', $uri);
    }

    public function testXmlToArray()
    {
        $clauseApiV0Service = new ClauseUseApiV0(
            $this->createMock(ClausesN1Repository::class),
            $this->createMock(ClausesN2Repository::class),
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesRepository::class),
            $this->createMock(IriConverterInterface::class),
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ClauseUsePrado::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ConsultationRepository::class)
        );

        $reflectionClass = new ReflectionClass(ClauseUseApiV0::class);
        $method = $reflectionClass->getMethod('xmlToArray');
        $method->setAccessible(true);

        $testXml = '<xml><critere>INSERTION_ACTIVITE_ECONOMIQUE</critere><critere>CLAUSE_SOCIALE_FORMATION_SOUS_STATUT_SCOLAIRE</critere><critere>LUTTE_CONTRE_DISCRIMINATIONS</critere></xml>';
        $xml = simplexml_load_string($testXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        $data = $method->invoke($clauseApiV0Service, $xml);

        $this->assertIsArray($data);
        $this->assertArrayHasKey('critere', $data);
        $this->assertCount(3, $data['critere']);
    }

    public function testAddClauseN3()
    {
        $clausesN3RepositoryMock =  $this->createMock(ClausesN3Repository::class);
        $clausesN3RepositoryMock->expects($this->exactly(3))
            ->method('findOneBy')
            ->willReturn(new ReferentielClausesN3())
            ;
        $clauseApiV0Service = new ClauseUseApiV0(
            $this->createMock(ClausesN1Repository::class),
            $this->createMock(ClausesN2Repository::class),
            $clausesN3RepositoryMock,
            $this->createMock(ClausesRepository::class),
            $this->createMock(IriConverterInterface::class),
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ClauseUsePrado::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ConsultationRepository::class)
        );

        $reflectionClass = new ReflectionClass(ClauseUseApiV0::class);
        $method = $reflectionClass->getMethod('addClauseN3');
        $method->setAccessible(true);

        $testXml = '<xml><critere>INSERTION_ACTIVITE_ECONOMIQUE</critere><critere>CLAUSE_SOCIALE_FORMATION_SOUS_STATUT_SCOLAIRE</critere><critere>LUTTE_CONTRE_DISCRIMINATIONS</critere></xml>';
        $xml = simplexml_load_string($testXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        $data = $method->invoke($clauseApiV0Service, $xml);

        $this->assertIsArray($data);
        $this->assertCount(3, $data);
    }

    public function testAddClauseN2()
    {
        $clausesN2RepositoryMock =  $this->createMock(ClausesN2Repository::class);
        $clausesN2RepositoryMock->expects($this->exactly(2))
            ->method('findOneBy')
            ->willReturn(new ReferentielClausesN2())
        ;

        $clauseApiV0Service = new ClauseUseApiV0(
            $this->createMock(ClausesN1Repository::class),
            $clausesN2RepositoryMock,
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesRepository::class),
            $this->createMock(IriConverterInterface::class),
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ClauseUsePrado::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ConsultationRepository::class)
        );

        $reflectionClass = new ReflectionClass(ClauseUseApiV0::class);
        $method = $reflectionClass->getMethod('addClauseN2');
        $method->setAccessible(true);

        $testXml = '<xml><clausesEnvironnementales><specificationsTechniques>true</specificationsTechniques><criteresSelections>true</criteresSelections></clausesEnvironnementales></xml>';
        $xml = simplexml_load_string($testXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        $data = $method->invoke($clauseApiV0Service, $xml);

        $this->assertIsArray($data);
        $this->assertCount(2, $data);
        $this->assertArrayHasKey('referentielClauseN2', $data[0]);
        $this->assertArrayHasKey('clausesN3', $data[0]);
        $this->assertArrayHasKey('referentielClauseN2', $data[1]);
        $this->assertArrayHasKey('clausesN3', $data[1]);
    }

    /**
     * @group test
     * @return void
     * @throws \ReflectionException
     */
    public function testAddClauseN1()
    {
        $clausesN1RepositoryMock =  $this->createMock(ClausesN1Repository::class);
        $clausesN1RepositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(new ReferentielClausesN1())
        ;

        $clauseApiV0Service = new ClauseUseApiV0(
            $clausesN1RepositoryMock,
            $this->createMock(ClausesN2Repository::class),
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesRepository::class),
            $this->createMock(IriConverterInterface::class),
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ClauseUsePrado::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ConsultationRepository::class)
        );

        $reflectionClass = new ReflectionClass(ClauseUseApiV0::class);
        $method = $reflectionClass->getMethod('addClauseN1');
        $method->setAccessible(true);

        $testXml = '<xml><clausesEnvironnementales><specificationsTechniques>true</specificationsTechniques><criteresSelections>true</criteresSelections></clausesEnvironnementales></xml>';
        $xml = simplexml_load_string($testXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        $data = $method->invoke($clauseApiV0Service, $xml, 'clauseEnvironnementale');

        $this->assertIsArray($data);
        $this->assertCount(2, $data);
        $this->assertArrayHasKey('referentielClauseN1', $data);
        $this->assertArrayHasKey('clausesN2', $data);
    }
}
