<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Entity\ResetPasswordRequest;
use App\Repository\AgentRepository;
use App\Repository\InscritRepository;
use App\Repository\ResetPasswordRequestRepository;
use App\Security\FormAuthenticator;
use App\Service\AtexoUtil;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;
use Twig\Environment;
use App\Service\ResetPasswordRequest as ResetPasswordRequestService;

class ResetPasswordRequestTest extends AtexoTestCase
{
    public function testGetResetPasswordRequestByJeton()
    {
        $resetPassword = new ResetPasswordRequest();
        $resetPassword->setJeton('38f9e652876339d736780a1d90f7f3a20991e650');
        $repository = $this->createMock(ResetPasswordRequestRepository::class);
        $repository->method('findOneBy')
            ->willReturn($resetPassword);
        $actualResetPassword = $this->getService($repository)->getResetPasswordRequestByJeton(
            '38f9e652876339d736780a1d90f7f3a20991e650'
        );

        $this->assertInstanceOf(ResetPasswordRequest::class, $actualResetPassword);
        $this->assertEquals('38f9e652876339d736780a1d90f7f3a20991e650', $actualResetPassword->getJeton());
    }

    public function testValidateTokenAndFetchUserForAgent()
    {
        $expectedUser = new Agent();
        $expectedUser->setEmail('agent@atexo.com');

        $resetPassword = new ResetPasswordRequest();
        $resetPassword->setModificationFaite('0');
        $resetPassword->setEmail('agent@atexo.com');
        $resetPassword->setTypeUser('agent');
        $resetPassword->setDateFinValidite(date('Y-m-d H:i:s', strtotime(' +1 day')));
        $repository = $this->createMock(AgentRepository::class);
        $repository->method('findOneBy')
            ->willReturn($expectedUser);
        $user = $this->getService($repository)->validateTokenAndFetchUser($resetPassword);

        $this->assertInstanceOf(Agent::class, $user);
        $this->assertEquals($resetPassword->getEmail(), $user->getEmail());
    }

    public function testValidateTokenAndFetchUserForEntreprise()
    {
        $expectedUser = new Inscrit();
        $expectedUser->setEmail('entreprise@atexo.com');

        $resetPassword = new ResetPasswordRequest();
        $resetPassword->setModificationFaite('0');
        $resetPassword->setEmail('entreprise@atexo.com');
        $resetPassword->setTypeUser('entreprise');
        $resetPassword->setDateFinValidite(date('Y-m-d H:i:s', strtotime(' +1 day')));
        $repository = $this->createMock(InscritRepository::class);
        $repository->method('findOneBy')
            ->willReturn($expectedUser);
        $user = $this->getService($repository)->validateTokenAndFetchUser($resetPassword);

        $this->assertInstanceOf(Inscrit::class, $user);
        $this->assertEquals($resetPassword->getEmail(), $user->getEmail());
    }

    public function testLogin()
    {
        $expectedUser = new Inscrit();
        $expectedUser->setEmail('entreprise@atexo.com');

        $repository = $this->createMock(InscritRepository::class);
        $request = $this->createMock(Request::class);
        $repository->method('findOneBy')
            ->willReturn($expectedUser);
        $response = $this->getService($repository)->logIn($request, 'entreprise', $expectedUser);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    private function getService(EntityRepository $repository): ResetPasswordRequestService
    {
        $mailer = $this->createMock(MailerInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $twig = $this->createMock(Environment::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $resetPasswordRequestRepository = $this->createMock(ResetPasswordRequestRepository::class);
        $response = new Response();
        $response->setStatusCode(Response::HTTP_OK);
        $guardAuthenticatorHandler = $this->createMock(GuardAuthenticatorHandler::class);
        $guardAuthenticatorHandler->method('authenticateUserAndHandleSuccess')
            ->willReturn($response);
        $formAuthenticator = $this->createMock(FormAuthenticator::class);

        return new ResetPasswordRequestService(
            $mailer,
            $em,
            $parameterBag,
            $twig,
            $translator,
            $atexoUtil,
            $guardAuthenticatorHandler,
            $formAuthenticator,
            $resetPasswordRequestRepository
        );
    }
}
