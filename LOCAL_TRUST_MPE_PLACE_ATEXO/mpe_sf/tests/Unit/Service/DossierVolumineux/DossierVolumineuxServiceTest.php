<?php

namespace Tests\Unit\Service\DossierVolumineux;

use Doctrine\ORM\Exception\ORMException;
use ReflectionException;
use ReflectionClass;
use Exception;
use InvalidArgumentException;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Echange;
use App\Entity\EchangeDestinataire;
use App\Entity\Inscrit;
use App\Entity\Organisme;
use App\Repository\AgentRepository;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\ConsultationRepository;
use App\Repository\DossierVolumineux\DossierVolumineuxRepository;
use App\Repository\EchangeRepository;
use App\Repository\InscritRepository;
use App\Repository\OrganismeRepository;
use App\Service\AtexoConfiguration;
use App\Service\ClientWs\ClientWs;
use App\Service\CurrentUser;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Tests\Unit\AtexoTestCase;

/**
 * Class AtexoUtilTest.
 */
class DossierVolumineuxServiceTest extends AtexoTestCase
{
    public function getService($session = null, $echange = null)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $currentUser = $this->createMock(CurrentUser::class);
        $objectManager = $this->getObjectManager($echange);
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);

        return new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );
    }

    /**
     * @dataProvider isAllowedDossiersVolumineuxProvider
     */
    public function testIsAllowedDossiersVolumineux($allowedDossiersVolumineux, $idDossierVolumineux, $expected)
    {
        $dossierVolumineux = new DossierVolumineux();
        $dossierVolumineux->setId($idDossierVolumineux);
        $session = new Session(new MockArraySessionStorage());
        $session->set('allowedDossiersVolumineux', $allowedDossiersVolumineux);
        $dossierVolumineuxService = $this->getService($session);
        $actual = $dossierVolumineuxService->isAgentAllowedToDownload($dossierVolumineux);
        $this->assertSame($expected, $actual);
    }

    public function isAllowedDossiersVolumineuxProvider()
    {
        yield [[1, 2, 3], 1, true];
        yield [[2, 3, 4], 1, false];
        yield [null, 1, false];
    }

    /**
     * @dataProvider getIdProvider
     *
     * @param $typerUser
     * @param $nom
     *
     * @throws ORMException
     * @throws ReflectionException
     */
    public function testSuccessCreateDossierVolumineux($typerUser, $nom)
    {
        $parameterBag = new ParameterBag();
        $currentUser = $this->createMock(CurrentUser::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        if ('entreprise' == $typerUser) {
            $currentUser->expects($this->any())
                ->method('getIdInscrit')
                ->willReturn('1');

            $currentUser->expects($this->any())
                ->method('isAgent')
                ->willReturn(false);
        } else {
            $currentUser->expects($this->any())
                ->method('getIdAgent')
                ->willReturn('1');

            $currentUser->expects($this->any())
                ->method('isAgent')
                ->willReturn(true);
        }

        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);

        $objectManager = $this->getObjectManager();
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );
        $result = $dossierVolumineuxService->createDossierVolumineux($nom);
        $this->assertTrue(is_string($result->getUuidTechnique()));
    }

    protected function getObjectManager($echange = null)
    {
        $objectManager = $this->createMock(EntityManager::class);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) use ($echange) {
                    static $callCount = [];
                    $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];

                    if (DossierVolumineux::class === $entityName) {
                        $dossierVolumineux = ['nom' => 'eezaea'];
                        $dossierVolumineuxRepository = $this->createMock(DossierVolumineuxRepository::class);
                        $dossierVolumineuxRepository->expects($this->any())
                            ->method('find')
                            ->willReturn($dossierVolumineux);

                        return $dossierVolumineuxRepository;
                    }

                    if (Agent::class === $entityName) {
                        $agent = new Agent();
                        $class = new ReflectionClass($agent);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($agent, 1);
                        $agentRepository = $this->createMock(AgentRepository::class);
                        $agentRepository->expects($this->any())
                            ->method('find')
                            ->willReturn($agent);

                        return $agentRepository;
                    }

                    if (Inscrit::class === $entityName) {
                        $inscrit = new Inscrit();
                        $class = new ReflectionClass($inscrit);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($inscrit, 1);

                        $inscritRepository = $this->createMock(InscritRepository::class);
                        $inscritRepository->expects($this->any())
                            ->method('find')
                            ->willReturn($inscrit);

                        return $inscritRepository;
                    }

                    if (Echange::class === $entityName) {
                        $repository = $this->createMock(EchangeRepository::class);
                        $repository->expects($this->any())
                            ->method('find')
                            ->willReturn($echange);

                        return $repository;
                    }
                }
            ));

        return $objectManager;
    }

    /**
     * @param $params
     * @param $paramName
     *
     * @dataProvider getParameterProviderValid
     */
    public function testIsCheckedParameter($params)
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);
        $objectManager = $this->getObjectManager();
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $callback = $dossierVolumineuxService->checkParameters($params);
        $this->assertTrue($callback);
    }

    /**
     * @param $params
     * @param $paramName
     *
     * @dataProvider getParameterProviderException
     */
    public function testIsCheckedParameterException($params)
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $violations = new ConstraintViolationList([
            $this->createMock(ConstraintViolation::class),
        ]);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn($violations);

        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);

        $objectManager = $this->getObjectManager();
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        try {
            $dossierVolumineuxService->checkParameters($params);
        } catch (Exception $exception) {
            $this->assertInstanceOf(InvalidArgumentException::class, $exception);
        }
    }

    public function testIsValidToCreateException()
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $violations = new ConstraintViolationList([
            $this->createMock(ConstraintViolation::class),
        ]);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn($violations);

        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);
        $objectManager = $this->getObjectManager();
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        try {
            $aD = [];
            $dossierVolumineuxService->checkIfValidToCreate($aD);
        } catch (Exception $exception) {
            $this->assertInstanceOf(InvalidArgumentException::class, $exception);
        }
    }

    /**
     * @dataProvider getParameterProvider
     *
     * @param $uuidReference
     *
     * @throws ReflectionException
     */
    public function testGetDossierVolumineux($uuidReference)
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $objectManager = $this->createMock(EntityManager::class);

        $dossierVolumineuxRepository = $this->createMock(DossierVolumineuxRepository::class);

        $dossierVolumineux = new DossierVolumineux();
        $class = new ReflectionClass($dossierVolumineux);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($dossierVolumineux, 1);

        $property = $class->getProperty('uuidReference');
        $property->setAccessible(true);
        $property->setValue($dossierVolumineux, 'UUI-TECHIQUE-0123');

        $dossierVolumineuxRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($dossierVolumineux);

        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($dossierVolumineuxRepository);
        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $dossierVolumineux = $dossierVolumineuxService->getDossierVolumineuxByUuid($uuidReference);
        $this->assertInstanceOf(DossierVolumineux::class, $dossierVolumineux);
    }

    public function testIsCheckServiceDossierVolumineux()
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $repoOrganisme = $this->createMock(OrganismeRepository::class);
        $repoOrganisme->expects($this->once())
            ->method('findBy')
            ->willReturn([new Organisme(), new Organisme()]);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($repoOrganisme);

        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());

        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $this->assertTrue($dossierVolumineuxService->checkServiceDossierVolumineux());
    }

    public function testIsNotCheckServiceDossierVolumineux()
    {
        $this->expectException(BadRequestHttpException::class);
        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $repoOrganisme = $this->createMock(OrganismeRepository::class);
        $repoOrganisme->expects($this->once())
            ->method('findBy')
            ->willReturn([]);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($repoOrganisme);

        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $atexoConfiguration->expects($this->any())
            ->method('hasConfigPlateforme')
            ->willReturn(false);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $dossierVolumineuxService->checkServiceDossierVolumineux();
    }

    /**
     * @return array
     */
    public function getParameterProviderValid()
    {
        return [
            [
                [
                    'reference' => 'UUI-TECHIQUE-0123',
                    'nomDossierVolumineux' => 'DOSSIER-VOLUMINEUX',
                    'serverLogin' => 'phpunit_test',
                    'serverPassword' => 'phpunit_test0123',
                    'serverURL' => 'http://127.0.0.1/',
                    'statut' => '0',
                ],
            ],
        ];
    }

    public function getParameterProviderException()
    {
        return [
            [
                ['reference' => ''],
                'reference',
            ],
        ];
    }

    public function getParameterProvider()
    {
        return [
            ['reference' => 'UUI-TECHIQUE-0123'],
        ];
    }

    public function getIdProvider()
    {
        return [
            ['agent', 'test'],
            ['entreprise', 'test'],
        ];
    }

    /**
     * @param $typeUser
     * @param $nom
     * @param $id
     */

    /**
     * @dataProvider getEnableProvider
     */
    public function testSuccessEnableDossierVolumineux($typerUser, $nom, $id)
    {
        $dossierVolumineux = new DossierVolumineux();
        $class = new ReflectionClass($dossierVolumineux);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($dossierVolumineux, $id);
        $dossierVolumineux->setNom($nom);
        $dossierVolumineux->setStatut(0);
        $currentUser = $this->createMock(CurrentUser::class);

        if ('entreprise' == $typerUser) {
            $currentUser->expects($this->any())
                ->method('getIdInscrit')
                ->willReturn('1');

            $currentUser->expects($this->any())
                ->method('isAgent')
                ->willReturn(false);

            $inscrit = new Inscrit();
            $class = new ReflectionClass($inscrit);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($inscrit, 1);
            $dossierVolumineux->setInscrit($inscrit);
        } else {
            $idAgent = '1';
            $currentUser->expects($this->any())
                ->method('getIdAgent')
                ->willReturn($idAgent);

            $currentUser->expects($this->any())
                ->method('isAgent')
                ->willReturn(true);

            $agent = new Agent();
            $class = new ReflectionClass($agent);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($agent, 1);
            $dossierVolumineux->setAgent($agent);
        }

        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $logger = $this->createMock(LoggerInterface::class);

        $dossierVolumineuxRepository = $this->createMock(DossierVolumineuxRepository::class);
        $dossierVolumineuxRepository
            ->expects($this->once())
            ->method('find')
            ->with($id)
            ->willReturn($dossierVolumineux);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($dossierVolumineuxRepository);
        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $result = $dossierVolumineuxService->enableDossierVolumineux($id);

        $this->assertTrue($result);
    }

    public function getEnableProvider()
    {
        return [
            ['agent', 'test', 93],
            ['entreprise', 'test', 93],
        ];
    }

    /**
     * @dataProvider getIsDownloadableProvider
     *
     * @param $isAuthorized
     * @param $isAttachedToOnlineConsultation
     * @param $isAttachedToCurrentUserExchange
     * @param $isAgentAllowedToDownload
     * @param $isCurrentUserConnected
     * @param $expected
     *
     * @throws Exception
     */
    public function testIsDownloadable(
        $isAuthorized,
        $isAttachedToOnlineConsultation,
        $isAttachedToCurrentUserExchange,
        $isAgentAllowedToDownload,
        $isCurrentUserConnected,
        $expected
    ) {
        $dossierVolumineux = new DossierVolumineux();
        $container = $this->createMock(ContainerInterface::class);
        $atexoConfigurationMock = $this->createMock(AtexoConfiguration::class);

        $atexoConfigurationMock->expects($this->any())
            ->method('hasConfigPlateforme')
            ->will($this->returnValue(true));

        $container->expects($this->any())
            ->method('get')
            ->with('App\Service\AtexoConfiguration')
            ->willReturn($atexoConfigurationMock);

        $dossierVolumineuxService = $this->createPartialMock(
            DossierVolumineuxService::class,
            [
                'isAuthorized',
                'isAttachedToOnlineConsultation',
                'isAttachedToCurrentUserExchange',
                'isAgentAllowedToDownload',
                'isCurrentUserConnected',
            ]
        );
        $dossierVolumineuxService
            ->expects($this->any())
            ->method('isAuthorized')
            ->willReturn($isAuthorized);
        $dossierVolumineuxService
            ->expects($this->any())
            ->method('isAttachedToOnlineConsultation')
            ->willReturn($isAttachedToOnlineConsultation);
        $dossierVolumineuxService
            ->expects($this->any())
            ->method('isAttachedToCurrentUserExchange')
            ->willReturn($isAttachedToCurrentUserExchange);
        $dossierVolumineuxService
            ->expects($this->any())
            ->method('isAgentAllowedToDownload')
            ->willReturn($isAgentAllowedToDownload);
        $dossierVolumineuxService
            ->expects($this->any())
            ->method('isCurrentUserConnected')
            ->willReturn($isCurrentUserConnected);

        $this->assertEquals($expected, $dossierVolumineuxService->isDownloadable($dossierVolumineux));
    }

    public function getIsDownloadableProvider()
    {
        return [
            [0, 0, 0, 0, true, false],
            [0, 0, 1, 0, true, true],
            [0, 1, 0, 0, true, true],
            [0, 1, 1, 0, true, true],
            [1, 0, 0, 0, true, true],
            [1, 0, 1, 0, true, true],
            [1, 1, 0, 1, true, true],
            [1, 1, 1, 0, true, true],
            [1, 1, 1, 1, true, true],
            [0, 1, 0, 0, false, true],
            [0, 0, 1, 0, false, false],
            [0, 0, 0, 1, false, false],
        ];
    }

    public function getIsAttachedToOnlineConsultationProvider()
    {
        return [
            [1, 2, 0, false],
            [2, 1, 0, false],
            [1, 1, 5, true],
        ];
    }

    /**
     * @dataProvider getIsAttachedToOnlineConsultationProvider
     *
     * @return DossierVolumineuxService
     *
     * @throws ReflectionException
     */
    public function testIsAttachedToOnlineConsultation($idDV, $idDVIntoConsultation, $nbConsultation, $expected)
    {
        $dossierVolumineux = new DossierVolumineux();
        $class = new ReflectionClass($dossierVolumineux);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($dossierVolumineux, $idDV);
        $dossierVolumineux->setNom('TOTO');
        $dossierVolumineux->setStatut(1);

        $consultationRepository = $this->createMock(ConsultationRepository::class);
        $consultationRepository->expects($this->any())
            ->method('findOnlineConsultationWithDossierVolumineux')
            ->willReturn($nbConsultation);

        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($consultationRepository);
        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $this->assertEquals($expected, $dossierVolumineuxService->isAttachedToOnlineConsultation($dossierVolumineux));
    }

    public function getIsAttachedToCurrentUserExchangeProvider()
    {
        return [
            [1, true, true],
            [1, false, false],
        ];
    }

    /**
     * @dataProvider getIsAttachedToCurrentUserExchangeProvider
     *
     * @param $idDV
     * @param $exist
     * @param $expected
     *
     * @throws ReflectionException
     */
    public function testIsAttachedToCurrentUserExchange($idDV, $exist, $expected)
    {
        $dossierVolumineux = new DossierVolumineux();
        $class = new ReflectionClass($dossierVolumineux);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($dossierVolumineux, $idDV);
        $dossierVolumineux->setNom('TOTO');
        $dossierVolumineux->setStatut(1);

        $echangeRepository = $this->createMock(EchangeRepository::class);
        $echangeRepository->expects($this->any())
            ->method('isCurrentUserRecipientToDossierVolumineux')
            ->willReturn($exist);

        $currentUser = $this->createMock(CurrentUser::class);

        $validator = $this->createMock(ValidatorInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManager::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($echangeRepository);
        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $this->assertEquals($expected, $dossierVolumineuxService->isAttachedToCurrentUserExchange($dossierVolumineux));
    }

    /**
     * @dataProvider getCheckAccessProvider
     *
     * @param $isDossierVolumineux
     * @param $dossierVolumineuxInscrit
     * @param $idInscrit
     * @param $dossierVolumineuxAgent
     * @param $idAgent
     * @param $expected
     */
    public function testCheckAccess(
        $isDossierVolumineux,
        $dossierVolumineuxInscrit,
        $idInscrit,
        $dossierVolumineuxAgent,
        $idAgent,
        $expected
    ) {
        $dossierVolumineux = new DossierVolumineux();
        if (!empty($dossierVolumineuxInscrit)) {
            $inscrit = new Inscrit();
            $inscrit->setId($dossierVolumineuxInscrit);
            $dossierVolumineux->setInscrit($inscrit);
        }
        if (!empty($dossierVolumineuxAgent)) {
            $agent = new Agent();
            $agent->setId($dossierVolumineuxAgent);
            $dossierVolumineux->setAgent($agent);
        }

        $configurationOrganisme = new ConfigurationOrganisme();
        $configurationOrganisme->setModuleEnvol($isDossierVolumineux);
        $repository = $this->createMock(ConfigurationOrganismeRepository::class);

        $willReturn = $isDossierVolumineux ? [$configurationOrganisme] : [];
        $repository
            ->expects($this->any())
            ->method('findBy')
            ->willReturn($willReturn);

        $currentUser = $this->createMock(CurrentUser::class);
        $currentUser
            ->method('getIdInscrit')
            ->willReturn((string) $idInscrit);
        $currentUser
            ->expects($this->any())
            ->method('getIdAgent')
            ->willReturn((string) $idAgent);

        $objectManager = $this->createMock(EntityManager::class);
        $objectManager
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository);
        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);
        $logger = $this->createMock(LoggerInterface::class);
        $clientWs = $this->createMock(ClientWs::class);
        $parameterBag = new ParameterBag();
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = new DossierVolumineuxService(
            $parameterBag,
            $currentUser,
            $objectManager,
            $validator,
            $logger,
            $clientWs,
            $atexoConfiguration,
            $session
        );

        $actual = $dossierVolumineuxService->checkAccess($dossierVolumineux);

        $this->assertEquals($expected, $actual);
    }

    public function getCheckAccessProvider()
    {
        return [
            [false, 1, 1, null, null, false],
            [true, 1, 1, null, null, true],
            [true, 1, 2, null, null, false],
            [false, null, null, 1, 1, false],
            [true, null, null, 1, 1, true],
            [true, null, null, 1, 2, false],
        ];
    }

    /**
     * @dataProvider isDownloadableFromMail
     */
    public function testIsDownloadableFromMail($echange, $uuidReference2, $expected)
    {
        $echangeDestinataire = new EchangeDestinataire();
        $dossierVolumineux = new DossierVolumineux();
        $dossierVolumineux->setUuidReference($uuidReference2);
        $session = new Session(new MockArraySessionStorage());
        $dossierVolumineuxService = $this->getService($session, $echange);
        $actual = $dossierVolumineuxService->isDownloadableFromMail(
            $dossierVolumineux,
            $echangeDestinataire
        );
        $this->assertSame($expected, $actual);
    }

    public function isDownloadableFromMail()
    {
        // echange non trouvé
        yield [null, 'UUID_REFERENCE_2', false];

        $echange = new Echange();
        $dossierVolumineux = new DossierVolumineux();
        $dossierVolumineux->setUuidReference('UUID_REFERENCE_1');
        $echange->setDossierVolumineux($dossierVolumineux);

        // echange trouvé + UUID différents
        yield [$echange, 'UUID_REFERENCE_2', false];

        // echange trouvé + UUID identiques
        yield [$echange, 'UUID_REFERENCE_1', true];
    }
}
