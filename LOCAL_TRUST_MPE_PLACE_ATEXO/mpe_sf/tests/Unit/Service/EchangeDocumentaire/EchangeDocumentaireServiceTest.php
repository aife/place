<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service\EchangeDocumentaire;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use PHPUnit\Framework\TestCase;

class EchangeDocumentaireServiceTest extends TestCase
{
    /**
     * @var array
     */
    private $files;

    public function setUp(): void
    {
        $this->files = [
            'principal' => 'AUTRESPIECESCONSULTATION##T2Z2OE1KRCtOMU83d21EUWIzY3h6Zz09',
            'typage' => '12',
            'annexes' => [
                0 => 'AUTRESPIECESCONSULTATION##T2Z2OE1KRCtOMU83d21EUWIzY3h6Zz09',
                1 => 'AUTRESPIECESCONSULTATION##Tkx5cjNyV0s0dm1RUkN2K0FRUFRSZz09',
            ],
            'typage_annexe' => [
                0 => ['AUTRESPIECESCONSULTATION##T2Z2OE1KRCtOMU83d21EUWIzY3h6Zz09' => '8'],
                1 => ['AUTRESPIECESCONSULTATION##Tkx5cjNyV0s0dm1RUkN2K0FRUFRSZz09' => '21'],
            ],
            'jetons' => [
                0 => 'AUTRESPIECESCONSULTATION##T2Z2OP1KRCtOMU83d21EUWIzY3h6Zz09',
            ],
        ];
    }

    public function testAddJetonFiles(): void
    {
        $mockEchangeService = $this->priceFactoryMock = $this->createPartialMock(
            EchangeDocumentaireService::class,
            ['newEchangeDocBlob', 'persist']
        );

        $echangeDocBlob = new EchangeDocBlob();
        $this->priceFactoryMock->expects($this->once())
            ->method('newEchangeDocBlob')
            ->willReturn($echangeDocBlob);

        $this->priceFactoryMock->expects($this->once())
            ->method('persist');

        $mockEchangeService->addJetonFiles($this->files, new EchangeDoc(), $echangeDocBlob);
    }
    public function testaddAnnexeFiles(): void
    {
        $mockEchangeService = $this->priceFactoryMock = $this->createPartialMock(
            EchangeDocumentaireService::class,
            ['newEchangeDocBlob', 'persist', 'setEchangeDocTypePieceActesOrPieceStandard']
        );

        $echangeDocBlob = new EchangeDocBlob();
        $this->priceFactoryMock->expects($this->exactly(2))
            ->method('newEchangeDocBlob')
            ->willReturn($echangeDocBlob);

        $this->priceFactoryMock->expects($this->exactly(2))
            ->method('setEchangeDocTypePieceActesOrPieceStandard')
            ->willReturn(new EchangeDocBlob());

        $this->priceFactoryMock->expects($this->exactly(2))
            ->method('persist');

        $typageAnnexe = [
            'PIECESDCE##DCE##VzBhckRVQlN1bjlJUXpHaXpFdU9CUT09##dldZR3F5dURNOHJRR0pnZW9DWkYwUT09' => '18',
            'PIECESDCE##DCE##VzBhckRVQlN1bjlJUXpHaXpFdU9CUT09##V0R5d1JhaitlWWM1WEdGeWpPNUphazlZan=' => '14',
            'AUTRESPIECESCONSULTATION##T2Z2OE1KRCtOMU83d21EUWIzY3h6Zz09' => '9',
            'AUTRESPIECESCONSULTATION##Tkx5cjNyV0s0dm1RUkN2K0FRUFRSZz09' => '10',
        ];

        $mockEchangeService->addAnnexeFiles($this->files, $typageAnnexe, new EchangeDoc(), true, $echangeDocBlob);
    }
}
