<?php

namespace Tests\Unit\Service;

use App\Repository\InvitePermanentTransverseRepository;
use App\Service\AtexoSearchAgent;
use App\Service\AtexoUtil;
use App\Service\Messagerie\MessagerieService;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;

class AtexoSearchAgentTest extends TestCase
{
    private $entityManager;
    private $container;
    private $service;
    private $logMock;
    private $serviceUtils;
    private $sessionMock;
    private $translatorMock;
    private $requestStack;
    private $messagerieServiceMock;

    protected function setUp(): void
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->container = $this->createMock(Container::class);
        $this->logMock = $this->createMock(Logger::class);
        $this->sessionMock = $this->createMock(Session::class);
        $this->translatorMock = $this->createMock(TranslatorInterface::class);
        $this->requestStack = $this->createMock(RequestStack::class);
        $this->messagerieServiceMock = $this->createMock(MessagerieService::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        $this->serviceUtils = new AtexoUtil(
            $this->container,
            $this->sessionMock,
            $this->translatorMock,
            $this->logMock,
            $this->requestStack,
            $parameterBagMock
        );
        $this->service = new AtexoSearchAgent(
            $this->container,
            $this->entityManager,
            $this->logMock,
            $this->serviceUtils,
            $this->messagerieServiceMock,
            $this->sessionMock
        );
    }

    public function testGetSqlCountAgent()
    {
        $string = 'SELECT
   DISTINCT consultation.id';
        $result = $this->service->getSqlCountAgent();
        $this->assertEquals($string, $result);
    }

    public function testGetSqlAgent()
    {
        $string = 'SELECT
   DISTINCT consultation.id,
   consultation.datefin,
   reference_utilisateur,
   nom_createur,
   prenom_createur,
   intitule,
   objet,
   id_type_avis,
   alloti';
        $result = $this->service->getSqlAgent();
        $this->assertEquals($string, $result);
    }

    public function testGetSqlLeftJoinCategorieLot()
    {
        $string = 'LEFT JOIN
		CategorieLot ON (CategorieLot.consultation_id = consultation.id '.
            'AND CategorieLot.organisme = consultation.organisme)';
        $result = $this->service->getSqlLeftJoinCategorieLot();
        $this->assertEquals($string, $result);
    }

    public function testGetInvitePermanentMonEntite()
    {
        $array = [2];
        $result = $this->service->getInvitePermanentMonEntite(true, 2);
        $this->assertEquals($array, $result);
    }

    public function testGetInvitePermanentEntiteDependante()
    {
        $array = [1, 3, 4, 5];
        $listOfServicesAllowed = [5];
        $invitePermanentEntiteDependante = true;
        $idServiceAgentConnected = 2;
        $sousServicesIncluantLeService = [1, 3, 4];
        $result = $this->service->getInvitePermanentEntiteDependante(
            $listOfServicesAllowed,
            $invitePermanentEntiteDependante,
            $idServiceAgentConnected,
            $sousServicesIncluantLeService
        );
        $this->assertEquals($array, $result);
    }

    public function testGetInvitePermanentTransverse()
    {
        $array = [1, 3, 4, 5];
        $listOfServicesAllowed = [5];
        $invitePermanentTransverse = true;
        $idUser = 2;

        $repository = $this->createMock(InvitePermanentTransverseRepository::class);
        $repository->expects($this->any())
            ->method('getServicesIdByAgentId')
            ->willReturn([1, 3, 4]);

        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repository);

        $result = $this->service->getInvitePermanentTransverse(
            $listOfServicesAllowed,
            $invitePermanentTransverse,
            $idUser
        );

        $this->assertEquals($array, $result);
    }

    public function testGetValidationIntermediaireOperateurIn()
    {
        $string = '((consultation.id_regle_validation=\'\' OR consultation.id_regle_validation=\'\' )
AND service_validation_intermediaire IN (2,3,5,6)
AND (date_validation_intermediaire IS NULL OR date_validation_intermediaire = \'0000-00-00 00:00:00\') )';
        $listOfServicesAllowed = '(2,3,5,6)';
        $validationIntermediaire = true;
        $result = $this->service->getSqlValidationIntermediaire(
            $listOfServicesAllowed,
            $validationIntermediaire
        );

        $this->assertEquals($string, $result);
    }

    public function testGetValidationIntermediaireOperateurEgale()
    {
        $string = '((consultation.id_regle_validation=\'\' OR consultation.id_regle_validation=\'\' )
AND service_validation_intermediaire = 2
AND (date_validation_intermediaire IS NULL OR date_validation_intermediaire = \'0000-00-00 00:00:00\') )';
        $listOfServicesAllowed = '2';
        $validationIntermediaire = true;
        $result = $this->service->getSqlValidationIntermediaire(
            $listOfServicesAllowed,
            $validationIntermediaire,
            '='
        );

        $this->assertEquals($string, $result);
    }

    public function testGetValidationFinaleOperateurIn()
    {
        $string = '(
	(
		(
			   consultation.id_regle_validation=\'\'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation IN (\'1,2,3\')
				AND (
						datevalidation IS NULL
						OR datevalidation = \'0000-00-00 00:00:00\'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation=\'\'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation IN (\'1,2,3\')
				AND (
					datevalidation IS NULL
					OR datevalidation = \'0000-00-00 00:00:00\'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != \'0000-00-00 00:00:00\'
				AND etat_approbation=\'1\'
			)
	)
)';
        $validationFinale = true;
        $listOfServicesAllowed = '1,2,3';
        $result = $this->service->getSqlValidationFinal($validationFinale, $listOfServicesAllowed);
        $this->assertEquals($string, $result);
    }

    public function testGetValidationFinaleOperateurEgale()
    {
        $string = '(
	(
		(
			   consultation.id_regle_validation=\'\'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation IN (\'2\')
				AND (
						datevalidation IS NULL
						OR datevalidation = \'0000-00-00 00:00:00\'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation=\'\'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation IN (\'2\')
				AND (
					datevalidation IS NULL
					OR datevalidation = \'0000-00-00 00:00:00\'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != \'0000-00-00 00:00:00\'
				AND etat_approbation=\'1\'
			)
	)
)';
        $validationFinale = true;
        $listOfServicesAllowed = '2';
        $result = $this->service->getSqlValidationFinal($validationFinale, $listOfServicesAllowed);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlValidationSimpleOperateurIn()
    {
        $string = "(
	consultation.id_regle_validation=''
	AND service_validation IN (1,2,3)
	AND (datevalidation IS NULL OR datevalidation = '0000-00-00 00:00:00')
)";
        $validationSimple = true;
        $listOfServicesAllowed = '(1,2,3)';
        $result = $this->service->getSqlValidationSimple($validationSimple, $listOfServicesAllowed);
        $this->assertEquals($string, $result);
    }

    public function testGetSqlValidationSimpleOperateurEgale()
    {
        $string = "(
	consultation.id_regle_validation=''
	AND service_validation = 2
	AND (datevalidation IS NULL OR datevalidation = '0000-00-00 00:00:00')
)";
        $validationSimple = true;
        $listOfServicesAllowed = '2';
        $result = $this->service->getSqlValidationSimple($validationSimple, $listOfServicesAllowed, '=');
        $this->assertEquals($string, $result);
    }
}
