<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Agent;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\GeolocalisationN2;
use App\Entity\HabilitationAgent;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN4;
use App\Exception\ConsultationNotFoundException;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Consultation\ClausesN1Repository;
use App\Repository\Consultation\ClausesN2Repository;
use App\Repository\Consultation\ClausesN3Repository;
use App\Repository\Consultation\ClausesN4Repository;
use App\Repository\ConsultationRepository;
use App\Repository\ContratTitulaireRepository;
use App\Repository\GeolocalisationN2Repository;
use App\Repository\LotRepository;
use App\Service\Agent\ReferenceTypeService;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoUtil;
use App\Service\ClausesService;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\Consultation\ConsultationService;
use App\Service\CurrentUser;
use App\Service\InscritOperationsTracker;
use App\Service\InterfaceSub;
use App\Service\ObscureDataManagement;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClausesServiceTest extends TestCase
{
    public function testUpdateClauses(): void
    {
        $consultation = new Consultation();
        $consultation->addClausesN1(new Consultation\ClausesN1());

        $clauseN1Repo = $this->createMock(ClausesN1Repository::class);
        $clauseN1Repo->expects($this->once())
            ->method('remove');

        $clauseN1Repo->expects($this->once())
            ->method('add');

        $clauseN2Repo = $this->createMock(ClausesN2Repository::class);
        $clauseN2Repo->expects($this->once())
            ->method('add');

        $clauseN3Repo = $this->createMock(ClausesN3Repository::class);
        $clauseN3Repo->expects($this->exactly(2))
            ->method('add');

        $clauseN4Repo = $this->createMock(ClausesN4Repository::class);
        $clauseN4Repo->expects($this->exactly(1))
            ->method('add');

        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $refClauseN1 = new ClausesN1();
        $refClauseN1->setActif(true);
        $refClauseN2 = new ClausesN2();
        $refClauseN2->setActif(true);
        $refClauseN3_1 = new ClausesN3();
        $refClauseN3_1->setActif(true);
        $refClauseN3_2 = new ClausesN3();
        $refClauseN3_2->setActif(true);
        $refClauseN4 = new ClausesN4();
        $refClauseN4->setActif(true);
        $iriConverterInterface->expects($this->exactly(5))
            ->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($refClauseN1, $refClauseN2, $refClauseN3_1, $refClauseN3_2, $refClauseN4);

        $clausesService = new ClausesService(
            $clauseN1Repo,
            $clauseN2Repo,
            $clauseN3Repo,
            $clauseN4Repo,
            $iriConverterInterface,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        $clauses =  [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2",
                        "clausesN3"           => [
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/2",
                                "clausesN4" => []
                            ],
                            [
                                "referentielClauseN3" => "/api/v2/referentiels/clauses-n3s/4",
                                "clausesN4" => [
                                   [ "referentielClauseN4" => [
                                       "id" => "/api/v2/referentiels/clauses-n4s/1",
                                       "value" => 25
                                   ]]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $clausesService->updateClauses($consultation, $clauses);
    }

    public function testUpdateClausesWithoutN3(): void
    {
        $lot = new Lot();
        $lot->addClausesN1(new Consultation\ClausesN1());

        $clauseN1Repo = $this->createMock(ClausesN1Repository::class);
        $clauseN1Repo->expects($this->once())
            ->method('remove');
        $clauseN1Repo->expects($this->once())
            ->method('add');
        $clauseN2Repo = $this->createMock(ClausesN2Repository::class);
        $clauseN2Repo->expects($this->once())
            ->method('add');

        $refClauseN1 = new ClausesN1();
        $refClauseN1->setActif(true);
        $refClauseN2 = new ClausesN2();
        $refClauseN2->setActif(true);
        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))
            ->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($refClauseN1, $refClauseN2);


        $clausesService = new ClausesService(
            $clauseN1Repo,
            $clauseN2Repo,
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesN4Repository::class),
            $iriConverterInterface,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        $clauses =  [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2"
                    ]
                ]
            ]
        ];
        $clausesService->updateClauses($lot, $clauses);
    }

    public function testUpdateClausesWithInactif(): void
    {
        $lot = new Lot();
        $lot->addClausesN1(new Consultation\ClausesN1());

        $clauseN1Repo = $this->createMock(ClausesN1Repository::class);
        $clauseN1Repo->expects($this->once())
            ->method('remove');
        $clauseN1Repo->expects($this->once())
            ->method('add');
        $clauseN2Repo = $this->createMock(ClausesN2Repository::class);
        $clauseN2Repo->expects($this->never())
            ->method('add');

        $refClauseN1 = new ClausesN1();
        $refClauseN1->setActif(true);
        $refClauseN2 = new ClausesN2();
        $refClauseN2->setActif(false);
        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))
            ->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($refClauseN1, $refClauseN2);


        $clausesService = new ClausesService(
            $clauseN1Repo,
            $clauseN2Repo,
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesN4Repository::class),
            $iriConverterInterface,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        $clauses =  [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2"
                    ]
                ]
            ]
        ];
        $clausesService->updateClauses($lot, $clauses);
    }

    public function testUpdateClausesWithContrat(): void
    {
        $contrat = new ContratTitulaire();
        $contrat->addClausesN1(new Consultation\ClausesN1());

        $clauseN1Repo = $this->createMock(ClausesN1Repository::class);
        $clauseN1Repo->expects($this->once())
            ->method('remove');
        $clauseN1Repo->expects($this->once())
            ->method('add');
        $clauseN2Repo = $this->createMock(ClausesN2Repository::class);
        $clauseN2Repo->expects($this->once())
            ->method('add');

        $refClauseN1 = new ClausesN1();
        $refClauseN1->setActif(true);
        $refClauseN2 = new ClausesN2();
        $refClauseN2->setActif(true);
        $iriConverterInterface = $this->createMock(IriConverterInterface::class);
        $iriConverterInterface->expects($this->exactly(2))
            ->method('getItemFromIri')
            ->willReturnOnConsecutiveCalls($refClauseN1, $refClauseN2);


        $clausesService = new ClausesService(
            $clauseN1Repo,
            $clauseN2Repo,
            $this->createMock(ClausesN3Repository::class),
            $this->createMock(ClausesN4Repository::class),
            $iriConverterInterface,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        $clauses =  [
            [
                "referentielClauseN1" => "/api/v2/referentiels/clauses-n1s/1",
                "clausesN2"           => [
                    [
                        "referentielClauseN2" => "/api/v2/referentiels/clauses-n2s/2"
                    ]
                ]
            ]
        ];
        $clausesService->updateClauses($contrat, $clauses);
    }

    public function testGetNestedClauses(): void
    {
        $clausesN1Repository = $this->createMock(ClausesN1Repository::class);
        $clausesN2Repository = $this->createMock(ClausesN2Repository::class);
        $clausesN3Repository = $this->createMock(ClausesN3Repository::class);
        $clausesN4Repository = $this->createMock(ClausesN4Repository::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $consultationRepository = $this->createMock(ConsultationRepository::class);

        /******* N2 *******/
        // N2 Condition Execution
        $referentielClauseN2ConditionExecution = (new ClausesN2())
            ->setLabel('conditionExecution')
            ->setSlug('conditionExecution')
        ;
        $clauseN2ConditionExecution = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2ConditionExecution)
        ;

        // N2 Marche Reserve
        $referentielClauseN2MarcheReserve = (new ClausesN2())
            ->setLabel('marcheReserve')
            ->setSlug('marcheReserve')
        ;
        $clauseN2MarcheReserve = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2MarcheReserve)
        ;

        // N2 Marche insertion
        $referentielClauseN2MarcheInsertion = (new ClausesN2())
            ->setLabel('insertion')
            ->setSlug('insertion')
        ;
        $clauseN2MarcheInsertion = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2MarcheInsertion)
        ;

        /******* N1 *******/
        // N1 Clause Sociale
        $referentielClauseN1Soc = (new ClausesN1())
            ->setLabel('clausesSociales')
            ->setSlug('clausesSociales')
        ;
        $clauseN1Sociale = (new Consultation\ClausesN1())
            ->setReferentielClauseN1($referentielClauseN1Soc)
            ->addClausesN2($clauseN2ConditionExecution)
            ->addClausesN2($clauseN2MarcheReserve)
            ->addClausesN2($clauseN2MarcheInsertion)
        ;

        /*** Clause Environnementale ***/
        /******* N2 *******/
        // N2 Condition Execution
        $referentielClauseN2SpecificationsTechniques = (new ClausesN2())
            ->setLabel('specificationsTechniques')
            ->setSlug('specificationsTechniques')
        ;
        $specificationsTechniques = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2SpecificationsTechniques)
        ;

        // N2 Condition Execution
        $referentielClauseN2ConditionExecution = (new ClausesN2())
            ->setLabel('conditionExecution')
            ->setSlug('conditionExecution')
        ;
        $conditionExecution = (new Consultation\ClausesN2())
            ->setReferentielClauseN2($referentielClauseN2ConditionExecution)
        ;

        /******* N1 *******/
        // N1 Clause Environnementale
        $referentielClauseN1Env = (new ClausesN1())
            ->setLabel('clauseEnvironnementale')
            ->setSlug('clauseEnvironnementale')
        ;
        $clauseN1Environnementale = (new Consultation\ClausesN1())
            ->setReferentielClauseN1($referentielClauseN1Env)
            ->addClausesN2($specificationsTechniques)
            ->addClausesN2($conditionExecution)
        ;

        $consultationId = 42;
        $consultation = new Consultation();
        $consultation->setId($consultationId);
        $consultation
            ->addClausesN1($clauseN1Sociale)
            ->addClausesN1($clauseN1Environnementale)
        ;

        $consultationRepository
            ->expects(self::once())
            ->method('find')
            ->with($consultationId)
            ->willReturn($consultation)
        ;

        $clausesService = new ClausesService(
            $clausesN1Repository,
            $clausesN2Repository,
            $clausesN3Repository,
            $clausesN4Repository,
            $iriConverter,
            $consultationRepository,
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        $expectedResult = [
            "clausesSociales" => [
                "conditionExecution",
                "marcheReserve",
                "insertion"
            ],
            "clauseEnvironnementale" => [
                "specificationsTechniques",
                "conditionExecution"
            ]
        ];

        self::assertSame($expectedResult, $clausesService->getClausesN1LabelByConsultationId($consultationId));
    }

    public function testGetNestedClausesEmpty(): void
    {
        $clausesN1Repository = $this->createMock(ClausesN1Repository::class);
        $clausesN2Repository = $this->createMock(ClausesN2Repository::class);
        $clausesN3Repository = $this->createMock(ClausesN3Repository::class);
        $clausesN4Repository = $this->createMock(ClausesN4Repository::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $consultationRepository = $this->createMock(ConsultationRepository::class);

        $consultationRepository
            ->expects(self::once())
            ->method('find')
            ->with(42)
            ->willReturn(null)
        ;

        $clausesService = new ClausesService(
            $clausesN1Repository,
            $clausesN2Repository,
            $clausesN3Repository,
            $clausesN4Repository,
            $iriConverter,
            $consultationRepository,
            $this->createMock(LotRepository::class),
            $this->createMock(ContratTitulaireRepository::class)
        );

        self::assertEmpty($clausesService->getClausesN1LabelByConsultationId(42));
    }

    public function testHasClauseSocialeByIdAndType(): void
    {
        $clausesN1Repository = $this->createMock(ClausesN1Repository::class);
        $clausesN2Repository = $this->createMock(ClausesN2Repository::class);
        $clausesN3Repository = $this->createMock(ClausesN3Repository::class);
        $clausesN4Repository = $this->createMock(ClausesN4Repository::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $contratRepo = $this->createMock(ContratTitulaireRepository::class);

        $refrentielClauseN1 = new ClausesN1();
        $refrentielClauseN1->setSlug('clausesSociales');
        $clauseN1 = new Consultation\ClausesN1();
        $clauseN1->setReferentielClauseN1($refrentielClauseN1);
        $contrat = new ContratTitulaire();
        $contrat->addClausesN1($clauseN1);
        $refrentiel2ClauseN1 = new ClausesN1();
        $refrentiel2ClauseN1->setSlug('clausesPasSociales');
        $clause2N1 = new Consultation\ClausesN1();
        $clause2N1->setReferentielClauseN1($refrentiel2ClauseN1);
        $contrat2 = new ContratTitulaire();
        $contrat2->addClausesN1($clause2N1);

        $contratRepo->expects(self::exactly(3))
            ->method('find')->with(42)->willReturnOnConsecutiveCalls(null, $contrat, $contrat2);

        $clausesService = new ClausesService(
            $clausesN1Repository,
            $clausesN2Repository,
            $clausesN3Repository,
            $clausesN4Repository,
            $iriConverter,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $contratRepo
        );

        $return = $clausesService->hasClauseSocialeByIdAndType(42, 'contrat');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseSocialeByIdAndType(42, 'contrat');
        $this->assertTrue($return);
        $return = $clausesService->hasClauseSocialeByIdAndType(42, 'contrat');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseSocialeByIdAndType(42, 'consultation');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseSocialeByIdAndType(42, 'lot');
        $this->assertFalse($return);
    }

    public function testHasClauseEnvironnementaleByIdAndType(): void
    {
        $clausesN1Repository = $this->createMock(ClausesN1Repository::class);
        $clausesN2Repository = $this->createMock(ClausesN2Repository::class);
        $clausesN3Repository = $this->createMock(ClausesN3Repository::class);
        $clausesN4Repository = $this->createMock(ClausesN4Repository::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $contratRepo = $this->createMock(ContratTitulaireRepository::class);

        $refrentielClauseN1 = new ClausesN1();
        $refrentielClauseN1->setSlug('clauseEnvironnementale');
        $clauseN1 = new Consultation\ClausesN1();
        $clauseN1->setReferentielClauseN1($refrentielClauseN1);
        $contrat = new ContratTitulaire();
        $contrat->addClausesN1($clauseN1);
        $refrentiel2ClauseN1 = new ClausesN1();
        $refrentiel2ClauseN1->setSlug('clausePasEnvironnementale');
        $clause2N1 = new Consultation\ClausesN1();
        $clause2N1->setReferentielClauseN1($refrentiel2ClauseN1);
        $contrat2 = new ContratTitulaire();
        $contrat2->addClausesN1($clause2N1);

        $contratRepo->expects(self::exactly(3))
            ->method('find')->with(42)->willReturnOnConsecutiveCalls(null, $contrat, $contrat2);

        $clausesService = new ClausesService(
            $clausesN1Repository,
            $clausesN2Repository,
            $clausesN3Repository,
            $clausesN4Repository,
            $iriConverter,
            $this->createMock(ConsultationRepository::class),
            $this->createMock(LotRepository::class),
            $contratRepo
        );

        $return = $clausesService->hasClauseEnvironnementaleByIdAndType(42, 'contrat');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseEnvironnementaleByIdAndType(42, 'contrat');
        $this->assertTrue($return);
        $return = $clausesService->hasClauseEnvironnementaleByIdAndType(42, 'contrat');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseEnvironnementaleByIdAndType(42, 'consultation');
        $this->assertFalse($return);
        $return = $clausesService->hasClauseEnvironnementaleByIdAndType(42, 'lot');
        $this->assertFalse($return);
    }
}
