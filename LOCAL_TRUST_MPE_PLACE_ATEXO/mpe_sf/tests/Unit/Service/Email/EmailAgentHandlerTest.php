<?php

namespace Tests\Unit\Service\Email;

use App\Service\Agent\Guests;
use Swift_Message;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeProcedure;
use App\Event\ValidateResponseEvent;
use App\Repository\AffiliationServiceRepository;
use App\Repository\AgentRepository;
use App\Repository\Consultation\ConsultationFavorisRepository;
use App\Repository\Consultation\InterneConsultationRepository;
use App\Repository\Consultation\InterneConsultationSuiviSeulRepository;
use App\Repository\HabilitationAgentRepository;
use App\Repository\InvitePermanentTransverseRepository;
use App\Repository\OrganismeRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Service\AtexoConfiguration;
use App\Service\Email\AtexoMailManager;
use App\Service\Handler\EmailAgentHandler;
use App\Service\Handler\Option;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Translation\DataCollectorTranslator;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

class EmailAgentHandlerTest extends TestCase
{
    private $stateModuleChecker;
    private $messageFactory;
    private $mailSender;
    private $validator;
    private $options;
    private $agentRepository;
    private $consultationFavorisRepository;
    private $interneConsultationSuiviSeulRepository;
    private $interneConsultationRepository;
    private $habilitationAgentRepository;
    private $invitePermanentTransverseRepository;
    private $organismeRepository;
    private $typeProcedureOrganismeRepository;
    private $event;
    private $consultation;
    private $organisme;
    private $agents;
    private $trans;
    private $affiliationServiceRepository;
    private $logger;
    private $entityManager;
    private $twig;

    private Guests $agentGuests;

    /**
     * @var MockObject|ParameterBagInterface
     */
    private $parameterBagInterface;
    /**
     * @var AtexoCrypto|MockObject
     */
    private $crypto;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(Logger::class);
        $this->stateModuleChecker = $this->createMock(AtexoConfiguration::class);
        $this->messageFactory = $this->createMock(SwiftMessageFactory::class);
        $this->mailSender = $this->createMock(MailSender::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->options = $this->createMock(Option::class);

        $this->crypto = $this->createMock(AtexoCrypto::class);
        $this->parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $this->parameterBagInterface->expects($this->any())->method('get')->willReturnOnConsecutiveCalls('a parameter', null);

        $this->entityManager = $this->createMock(EntityManager::class);
        $this->agentRepository = $this->createMock(AgentRepository::class);
        $this->consultationFavorisRepository = $this->createMock(ConsultationFavorisRepository::class);
        $this->typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $this->organismeRepository = $this->createMock(OrganismeRepository::class);
        $this->event = $this->createMock(ValidateResponseEvent::class);
        $this->consultation = $this->createMock(Consultation::class);
        $this->organisme = $this->createMock(Organisme::class);
        $this->trans = $this->createMock(DataCollectorTranslator::class);

        $this->affiliationServiceRepository = $this->createMock(AffiliationServiceRepository::class);
        $swiftMessage = $this->createMock(Swift_Message::class);

        $this->agentGuests = $this->createMock(Guests::class);

        $guests = [
            1, 2
        ];
        $this->agentGuests->expects($this->any())
            ->method('getAllGuestsByConsulation')
            ->willReturn($guests);
        $this->agents = [];
        for ($i = 1; $i < 5; ++$i) {
            $favoris = new ConsultationFavoris();
            $consultation = new Consultation();
            $consultation->setReference($i);
            $agent = new Agent();
            $agent->setId($i);
            $agent->setEmail('mail' . $i . '@atexo.com');
            $favoris->setConsultation($consultation);
            $favoris->setAgent($agent);
            $listFavoris[] = $favoris;
        }
        $this->consultationFavorisRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($listFavoris);

        $this->trans->expects($this->any())
            ->method('trans')
            ->willReturn('tran');
        $this->options->translator = $this->trans;

        $this->messageFactory->expects($this->any())
            ->method('getInstance')
            ->willReturn($this->messageFactory);

        $this->messageFactory->expects($this->any())
            ->method('setSubject')
            ->willReturn($this->messageFactory);

        $this->messageFactory->expects($this->any())
            ->method('setFrom')
            ->willReturn($this->messageFactory);

        $this->messageFactory->expects($this->any())
            ->method('setTo')
            ->willReturn($this->messageFactory);

        $this->messageFactory->expects($this->any())
            ->method('setTemplate')
            ->willReturn($this->messageFactory);

        $this->messageFactory->expects($this->any())
            ->method('setContent')
            ->willReturn($this->messageFactory);

        $this->organisme->expects($this->any())
            ->method('getAcronyme')
            ->willReturn('pmi-min-1');

        $this->validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $this->consultation->expects($this->any())
            ->method('getOrganisme')
            ->willReturn($this->organisme);

        $this->event->expects($this->any())
            ->method('getConsultation')
            ->willReturn($this->consultation);

        $this->affiliationServiceRepository->expects($this->any())
            ->method('getParentByServiceIdAndOrganisme')
            ->willReturn([]);

        $this->messageFactory->expects($this->any())
            ->method('create')
            ->willReturn($swiftMessage);

        $this->mailSender->expects($this->any())
            ->method('send')
            ->willReturn([]);

        $this->typeProcedureOrganismeRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(
                (new TypeProcedureOrganisme())
                    ->setProcedurePortailType(
                        (new TypeProcedure())->setLibelleTypeProcedure('Appel d\'offres ouvert')
                    )
            );

        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->will(
                $this->returnValueMap(
                    [
                        [Agent::class, $this->agentRepository],
                        [ConsultationFavoris::class, $this->consultationFavorisRepository],
                        [AffiliationService::class, $this->affiliationServiceRepository],
                        [Organisme::class, $this->organismeRepository],
                        [TypeProcedureOrganisme::class, $this->typeProcedureOrganismeRepository],
                    ]
                )
            );

        $this->twig = $this->createMock(Environment::class);
    }

    protected function createAgentHandler()
    {
        $mail = new EmailAgentHandler(
            $this->stateModuleChecker,
            $this->messageFactory,
            $this->mailSender,
            $this->validator,
            $this->options,
            $this->entityManager,
            $this->crypto,
            $this->parameterBagInterface,
            $this->logger,
            $this->twig,
            $this->agentGuests
        );

        return $mail;
    }

    public function testHandleNoSendMail()
    {
        $mail = $this->createAgentHandler();
        $this->agentRepository->expects($this->any())
            ->method('getAgentsWhoWantBeAlertedElectronicResponse')
            ->willReturn([]);

        $reponse = $mail->handle($this->event);

        $this->assertEquals(null, $reponse);
    }

    public function testHandleSendMail()
    {
        $agent = new Agent();
        $agent->setId(1);
        $agent->setServiceId(1);
        $agent->setEmail('mail1@atexo.com');
        $organisme = new Organisme();
        $organisme->setPfUrl('http://mpe.local-trust.com/');
        $agent->setOrganisme($organisme);
        $agents[] = $agent;

        $this->agentRepository->expects($this->any())
            ->method('getAgentsWhoWantBeAlertedElectronicResponse')
            ->willReturn($agents);

        $mail = $this->createAgentHandler();
        $mailManager = $this->createMock(AtexoMailManager::class);
        $mail->setMailManager($mailManager);
        $mailManager->expects($this->atLeast(1))->method('sendMail');

        $reponse = $mail->handle($this->event);
        $this->assertEquals(null, $reponse);
    }
}
