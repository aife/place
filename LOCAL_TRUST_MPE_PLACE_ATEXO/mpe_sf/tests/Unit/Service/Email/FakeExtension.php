<?php

namespace Tests\Unit\Service\Email;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class FakeExtension extends AbstractExtension
{
    private const FAKE_URL_FUNCTION = 'baseUrl';
    private const FAKE_ISO2DATE_FUNCTION = 'iso2frnDateTime';
    private const FAKE_DATE_FUNCTION = 'fakeDate';
    private const FAKE_SIZE_FONCTION = 'sizeFormatFilter';
    private const FAKE_TRANS_FUNCTION = 'trans';

    public function baseUrl($route, array $vars = [])
    {
        return '/';
    }

    public function trans($message, array $arguments = [], $domain = null, $locale = null)
    {
        return $message;
    }

    public function iso2frnDateTime($_isoDate, $withTime = true, $withTimeSeconds = false)
    {
        return self::FAKE_ISO2DATE_FUNCTION;
    }

    public function fakeDate()
    {
        return 'date';
    }

    public function sizeFormatFilter(string $tailleBrute, string $unity = 'Mo')
    {
        return $tailleBrute;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('absolute_url', [$this, self::FAKE_URL_FUNCTION]),
            new TwigFunction('asset', [$this, self::FAKE_URL_FUNCTION]),
            new TwigFunction('picto_statut_signature', [$this, self::FAKE_URL_FUNCTION]),
            new TwigFunction('libelle_statut_signature', [$this, self::FAKE_URL_FUNCTION]),
            new TwigFunction('infos_signature', [$this, self::FAKE_URL_FUNCTION]),
        ];
    }

    public function getFilters()
    {
        return [
            new TwigFilter('date', [$this, self::FAKE_DATE_FUNCTION]),
            new TwigFilter('sizeFormat', [$this, self::FAKE_SIZE_FONCTION]),
            new TwigFilter('trans', [$this, self::FAKE_TRANS_FUNCTION]),
            new TwigFilter('iso2frnDateTime', [$this, self::FAKE_ISO2DATE_FUNCTION]),
        ];
    }
}
