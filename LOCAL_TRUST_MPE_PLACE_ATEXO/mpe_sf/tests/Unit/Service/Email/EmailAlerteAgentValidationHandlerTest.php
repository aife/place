<?php

namespace Tests\Unit\Service\Email;

use App\Entity\Consultation;
use App\Service\Agent\AgentInviterService;
use App\Service\AtexoConfiguration;
use App\Service\Handler\EmailAlerteAgentValidationHandler;
use App\Service\Handler\Option;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

class EmailAlerteAgentValidationHandlerTest extends TestCase
{
    private $stateModuleChecker;
    private $messageFactory;
    private $mailSender;
    private $validator;
    private $options;
    private $agentRepository;
    private $consultationFavorisRepository;
    private $interneConsultationSuiviSeulRepository;
    private $interneConsultationRepository;
    private $habilitationAgentRepository;
    private $invitePermanentTransverseRepository;
    private $organismeRepository;
    private $event;
    private $consultation;
    private $organisme;
    private $agents;
    private $trans;
    private $affiliationServiceRepository;
    private $logger;
    private $entityManager;
    private $twig;

    /**
     * @var MockObject|ParameterBagInterface
     */
    private $parameterBagInterface;
    /**
     * @var AtexoCrypto|MockObject
     */
    private $crypto;

    /** @var EmailAlerteAgentValidationHandler */
    private $serviceMail;

    /** @var AgentInviterService|MockObject */
    private $agentInviterService;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(Logger::class);
        $this->stateModuleChecker = $this->createMock(AtexoConfiguration::class);
        $this->messageFactory = $this->createMock(SwiftMessageFactory::class);
        $this->mailSender = $this->createMock(MailSender::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->options = $this->createMock(Option::class);
        $this->crypto = $this->createMock(AtexoCrypto::class);
        $this->parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->twig = $this->createMock(Environment::class);
        $this->agentInviterService = $this->createMock(AgentInviterService::class);

        $this->agentInviterService
            ->expects($this->any())
            ->method('getListAgentInviter')
            ->willReturn(['agent1@atexo.com']);

        $this->serviceMail = new EmailAlerteAgentValidationHandler(
            $this->stateModuleChecker,
            $this->messageFactory,
            $this->mailSender,
            $this->validator,
            $this->options,
            $this->entityManager,
            $this->crypto,
            $this->parameterBagInterface,
            $this->logger,
            $this->twig,
            $this->agentInviterService
        );
    }

    public function testGetListEmailAgent()
    {
        $listesReference = [
            'agent1@atexo.com',
        ];
        $consultation = new Consultation();
        $consultation->setAcronymeOrg('pmi-min-1');
        $consultation->setServiceAssocieId(1);
        $consultation->setServiceId(2);

        $listes = $this->serviceMail->getListEmailAgent($consultation);
        $this->assertEquals($listesReference, $listes);
    }
}
