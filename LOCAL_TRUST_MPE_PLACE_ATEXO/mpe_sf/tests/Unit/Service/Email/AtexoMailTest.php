<?php

namespace Tests\Unit\Service\Email;

use DOMDocument;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TwigFunction;

class AtexoMailTest extends TestCase
{
    private const TEST_TEMPLATE_DIR = __DIR__ . '/template';

    /** @var Environment */
    private $toTestTwig;

    /** @var array */
    private $testData;

    protected function setUp(): void
    {
        $this->testData = Yaml::parseFile(
            __DIR__ . '/../../../../config/packages/mail/test_data.yml'
        );

        $toTestFileSystem = new FilesystemLoader(__DIR__ . '/../../../../templates/email/');
        $this->toTestTwig = new Environment($toTestFileSystem, [
            'cache' => false,
        ]);
        $this->toTestTwig->addExtension(new FakeExtension());

        $twigFunction = new TwigFunction(
            'file_asset_exists',
            function () {
                return false;
            }
        );
        $this->toTestTwig->addFunction($twigFunction);
    }

    public function testAccuseReceptionReponseEntreprise()
    {
        $standardTemplate = file_get_contents(
            self::TEST_TEMPLATE_DIR . '/ar-reponse-entreprise.html.twig'
        );
        $toTestTemplate = $this->toTestTwig->load('ar-reponse-entreprise.html.twig');
        $testDatas = $this->testData['accuse-reception-reponse-entreprise'];

        $expectedDom = $this->getDomFromHtml($standardTemplate);
        $actualDom = $this->getDomFromHtml($toTestTemplate->render($testDatas));

        self::assertSame(
            $expectedDom->lastChild->nodeType,
            $actualDom->lastChild->nodeType
        );

        self::assertSame(
            $expectedDom->lastChild->tagName,
            $actualDom->lastChild->tagName
        );

        self::assertSame(
            $this->getCleanTextFromDom($expectedDom),
            $this->getCleanTextFromDom($actualDom)
        );
    }

    public function testAccuseReceptionResponseAgent()
    {
        $standardTemplate = file_get_contents(
            self::TEST_TEMPLATE_DIR . '/ar-reponse-agent.html.twig'
        );
        $toTestTemplate = $this->toTestTwig->load('ar-reponse-agent.html.twig');
        $testDatas = $this->testData['accuse-reception-reponse-agent'];

        $expectedDom = $this->getDomFromHtml($standardTemplate);
        $actualDom = $this->getDomFromHtml($toTestTemplate->render($testDatas));

        self::assertSame(
            $expectedDom->lastChild->nodeType,
            $actualDom->lastChild->nodeType
        );

        self::assertSame(
            $expectedDom->lastChild->tagName,
            $actualDom->lastChild->tagName
        );

        $this->assertSame(
            $this->getCleanTextFromDom($expectedDom),
            $this->getCleanTextFromDom($actualDom)
        );
    }

    protected function getDomFromHtml(string $html)
    {
        $dom = new DOMDocument();
        $dom->loadHTML($html);
        $dom->formatOutput = false;
        $dom->preserveWhiteSpace = false;

        return $dom;
    }

    protected function getCleanTextFromDom(DOMDocument $domDocument)
    {
        $text = strip_tags($domDocument->saveHTML());
        $text = preg_replace('/^\s*(.*)\s*$/m', '$1', $text);
        $text = str_replace(' ', ' ', $text);
        $text = str_replace(' ', ' ', $text);

        return preg_replace('/\s+/', ' ', $text);
    }
}
