<?php

namespace Tests\Unit\Service\Email;

use App\Service\Email\MjmlManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Exception\InvalidArgumentException;

class MjmlManagerTest extends TestCase
{
    /** @var Logger */
    private $logger;

    protected function setUp(): void
    {
        $this->logger = $this->createMock(Logger::class);
    }

    public function testCompileMjmlBadName()
    {
        $mjmlManager = new MjmlManager($this->logger);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('File must be named *.mjml.twig');
        $mjmlManager->compileMjml('test1.test.twig');
    }

    public function testCompileMjmlFileNotFound()
    {
        $mjmlManager = new MjmlManager($this->logger);
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('File test1.mjml.twig not found');
        $mjmlManager->compileMjml('test1.mjml.twig');
    }
}
