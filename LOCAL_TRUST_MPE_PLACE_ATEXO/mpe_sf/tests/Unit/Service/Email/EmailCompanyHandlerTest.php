<?php

namespace Tests\Unit\Service\Email;

use DateTime;
use Symfony\Component\Translation\TranslatorBagInterface;
use App\Entity\CandidatureMps;
use App\Entity\Consultation;
use App\Entity\EnveloppeFichier;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use App\Entity\TypeProcedure;
use App\Event\ValidateResponseEvent;
use App\Repository\CandidatureMpsRepository;
use App\Repository\EnveloppeFichierRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Repository\TCandidatureRepository;
use App\Repository\TDumeNumeroRepository;
use App\Service\AtexoConfiguration;
use App\Service\AtexoFichierOrganisme;
use App\Service\Email\AtexoMailManager;
use App\Service\Handler\EmailCompanyHandler;
use App\Service\Handler\Option;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManager;
use Knp\Snappy\Pdf;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class EmailCompanyHandlerTest extends TestCase
{
    protected const TEST_EMAIL = 'test@mailinator.com';
    protected const TYPE_ENVELOP_ELECTRONIC = 1;

    protected $stateModuleChecker;
    protected $messageFactory;
    protected $mailSender;
    protected $validator;
    protected $options;
    protected $container;
    protected $entityManager;
    protected $atexoFichierOrganisme;
    protected $pdfGenerator;
    protected $candidatureMpsRepository;
    protected $enveloppeFichierRepository;
    protected $dumeNumeroRepository;
    protected $tCandidatureRepository;
    protected $typeProcedureOrganismeRepository;
    protected $event;
    protected $logger;
    protected $twig;

    /**
     * @var AtexoCrypto|MockObject
     */
    private $crypto;

    /**
     * @var MockObject|ParameterBagInterface
     */
    private $parameterBagInterface;

    protected function setUp(): void
    {
        $this->stateModuleChecker = $this->createMock(AtexoConfiguration::class);
        $this->messageFactory = $this->createMock(SwiftMessageFactory::class);
        $this->mailSender = $this->createMock(MailSender::class);
        $this->validator = $this->createMock(ValidatorInterface::class);
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);
        $this->options = $this->createMock(Option::class);
        $this->container = $this->createMock(Container::class);

        $this->crypto = $this->createMock(AtexoCrypto::class);
        $this->parameterBagInterface = $this->createMock(ParameterBagInterface::class);
        $this->parameterBagInterface->expects($this->any())->method('get')
            ->willReturnOnConsecutiveCalls([], 'a parameter', 'a parameter', 'a parameter');

        $this->atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);
        $this->pdfGenerator = $this->createMock(Pdf::class);
        $this->candidatureMpsRepository = $this->createMock(CandidatureMpsRepository::class);
        $this->enveloppeFichierRepository = $this->createMock(EnveloppeFichierRepository::class);
        $this->dumeNumeroRepository = $this->createMock(TDumeNumeroRepository::class);
        $this->tCandidatureRepository = $this->createMock(TCandidatureRepository::class);
        $this->typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $this->event = $this->createMock(ValidateResponseEvent::class);

        $candidature = $this->createMock(TCandidature::class);
        $this->tCandidatureRepository->expects($this->any())
            ->method('__call')
            ->with('findByIdOffre')
            ->willReturn([$candidature]);

        $this->stateModuleChecker->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $offre = $this->createMock(Offre::class);
        $offre->expects($this->any())
            ->method('getUntrusteddate')
            ->willReturn(new DateTime());
        $offre->expects($this->any())
            ->method('getMailSignataire')
            ->willReturn(self::TEST_EMAIL);
        $consultation = $this->createMock(Consultation::class);
        $consultation->expects($this->any())
            ->method('getMarchePublicSimplifie')
            ->willReturn(true);
        $this->event->expects($this->any())
            ->method('getConsultation')
            ->willReturn($consultation);
        $this->event->expects($this->any())
            ->method('getResponse')
            ->willReturn($offre);
        $inscrit = $this->createMock(Inscrit::class);
        $inscrit->expects($this->any())
            ->method('getEmail')
            ->willReturn(self::TEST_EMAIL);
        $this->event->expects($this->any())
            ->method('getCandidate')
            ->willReturn($inscrit);
        $translator = $this->createMock(TranslatorInterface::class);
        $this->options->translator = $translator;

        $this->parameterBagInterface->expects($this->any())
            ->method('get')
            ->will(
                $this->returnValueMap(
                    [
                        ['type_enveloppe', []],
                    ]
                )
            );

        $this->typeProcedureOrganismeRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn(
                (new TypeProcedureOrganisme())
                    ->setProcedurePortailType(
                        (new TypeProcedure())->setLibelleTypeProcedure('Appel d\'offres ouvert')
                    )
            );

        $this->entityManager->expects($this->any())
            ->method('getRepository')
            ->will(
                $this->returnValueMap(
                    [
                        [EnveloppeFichier::class, $this->enveloppeFichierRepository],
                        [TCandidature::class, $this->tCandidatureRepository],
                        [TDumeNumero::class, $this->dumeNumeroRepository],
                        [CandidatureMps::class, $this->candidatureMpsRepository],
                        [TypeProcedureOrganisme::class, $this->typeProcedureOrganismeRepository],
                    ]
                )
            );

        $this->logger = $this->createMock(LoggerInterface::class);
        $this->twig = $this->createMock(Environment::class);
    }

    protected function createCompanyHandler()
    {
        $mail = new EmailCompanyHandler(
            $this->stateModuleChecker,
            $this->messageFactory,
            $this->mailSender,
            $this->validator,
            $this->options,
            $this->entityManager,
            $this->crypto,
            $this->parameterBagInterface,
            $this->pdfGenerator,
            $this->logger,
            $this->twig,
            $this->atexoFichierOrganisme
        );

        return $mail;
    }

    public function testEmailSent()
    {
        $mailManager = $this->createMock(AtexoMailManager::class);
        $mail = $this->createCompanyHandler();
        $mail->setMailManager($mailManager);
        $mailManager->expects(self::once())->method('sendMail');

        $mail->handle($this->event);
    }
}
