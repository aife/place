<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use DateTime;
use App\Entity\Consultation;
use App\Entity\Offre;
use App\Entity\Telechargement;
use App\Repository\ConsultationRepository;
use App\Service\CurrentUser;
use App\Service\GestionRegistres;
use App\Utils\EntityPurchase;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GestionRegistresTest extends TestCase
{
    public function testGetAllRegistreEmpty()
    {
        $consultation = new Consultation();

        $consultation->setTitre('Test001');

        $repositoryMock = $this->createMock(ConsultationRepository::class);
        $repositoryMock->expects($this->once())
            ->method('find')
            ->with(1)
            ->willReturn($consultation);

        $parameterMock = $this->createMock(ParameterBagInterface::class);
        $purchaseMock = $this->createMock(EntityPurchase::class);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $registerMock = new  GestionRegistres($entityManagerMock, $parameterMock, $purchaseMock);

        $registres = $registerMock->getAll(1);

        $this->assertIsArray($registres);
        $this->assertArrayHasKey('depots', $registres);
        $this->assertArrayHasKey('questions', $registres);
        $this->assertArrayHasKey('retraits', $registres);
        $this->assertEmpty($registres['retraits']);
        $this->assertEmpty($registres['depots']);
        $this->assertEmpty($registres['questions']);
    }

    public function testGetAllRegistreNotEmpty()
    {
        $consultation = new Consultation();

        $consultation->setTitre('Test001');
        $consultation->addOffre(new Offre());
        $consultation->addTelechargement(new Telechargement());

        $repositoryMock = $this->createMock(ConsultationRepository::class);
        $repositoryMock->expects($this->once())
            ->method('find')
            ->with(1)
            ->willReturn($consultation);

        $parameterMock = $this->createMock(ParameterBagInterface::class);

        $parameterMock->expects($this->once())
            ->method('get')
            ->with('STATUT_ENV_BROUILLON')
            ->willReturn(99)
        ;

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);
        $purchaseMock = $this->createMock(EntityPurchase::class);

        $registerMock = new  GestionRegistres($entityManagerMock, $parameterMock, $purchaseMock);

        $registres = $registerMock->getAll(1);

        $this->assertIsArray($registres);
        $this->assertArrayHasKey('depots', $registres);
        $this->assertArrayHasKey('questions', $registres);
        $this->assertArrayHasKey('retraits', $registres);
        $this->assertEquals(1, $registres['retraits']);
        $this->assertEquals(1, $registres['depots']);
        $this->assertEmpty($registres['questions']);
    }

    public function testGetAllConsultationNotExist()
    {
        $repositoryMock = $this->createMock(ConsultationRepository::class);
        $repositoryMock->expects($this->once())
            ->method('find')
            ->with(1)
            ->willReturn(null);

        $parameterMock = $this->createMock(ParameterBagInterface::class);

        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);
        $purchaseMock = $this->createMock(EntityPurchase::class);

        $registerMock = new  GestionRegistres($entityManagerMock, $parameterMock, $purchaseMock);

        $this->expectException(EntityNotFoundException::class);
        $registerMock->getAll(1);
    }

    public function testConsultationHasValidationHabilitation()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $parameterMock = $this->createMock(ParameterBagInterface::class);
        $user = $this->createMock(CurrentUser::class);
        $purchaseMock = $this->createMock(EntityPurchase::class);
        $registerMock = new  GestionRegistres($entityManagerMock, $parameterMock, $purchaseMock);

        $consultation = new Consultation();
        $consultation->setDatefin(new DateTime('2018-06-06'));
        $hasHabilitation = $registerMock->consultationHasValidationHabilitation($consultation, $user);

        $this->assertFalse($hasHabilitation);
    }

    public function testConsultationHasValidationHabilitationChoix5()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $parameterMock = $this->createMock(ParameterBagInterface::class);
        $user = $this->createMock(CurrentUser::class);
        $purchaseMock = $this->createMock(EntityPurchase::class);
        $registerMock = new  GestionRegistres($entityManagerMock, $parameterMock, $purchaseMock);
        $parameterMock->method('get')
            ->willReturn('5');
        $user->method('checkHabilitation')
            ->willReturn(true);

        $consultation = new Consultation();
        $consultation->setDateValidationIntermediaire('0000-00-00');
        $consultation->setIdRegleValidation(5);
        $consultation->setDatefin(new DateTime('2022-06-06'));
        $hasHabilitation = $registerMock->consultationHasValidationHabilitation($consultation, $user);
        $consultation->setEtatEnAttenteValidation('1');

        $this->assertTrue($hasHabilitation);
    }
}
