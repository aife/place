<?php

namespace Tests\Unit\Service\Layout;

use App\Entity\ConfigurationOrganisme;
use App\Repository\ConfigurationOrganismeRepository;
use ReflectionClass;
use App\Entity\Agent;
use App\Entity\ComptesAgentsAssocies;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use App\Repository\ComptesAgentsAssociesRepository;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\Agent\Habilitation;
use App\Service\AtexoMenu;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Contracts\Translation\TranslatorInterface;

class AtexoMenuTest extends TestCase
{
    public function testGetSocleInterneDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getSocleInterne();

        $this->assertEquals(false, $real);
    }

    public function testGetSocleInterneEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getSocleInterne();

        $this->assertEquals(true, $real);
    }

    public function testGetSocleExternePppDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleExternePpp('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getSocleExternePpp();

        $this->assertEquals(false, $real);
    }

    public function testGetSocleExternePppEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleExternePpp('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getSocleExternePpp();

        $this->assertEquals(true, $real);
    }

    public function testGetPartagerConsultationDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setPartagerConsultation('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getPartagerConsultation();

        $this->assertEquals(false, $real);
    }

    public function testGetPartagerConsultationEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setPartagerConsultation('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getPartagerConsultation();

        $this->assertEquals(true, $real);
    }

    public function testGetAfficherImageOrganismeDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setAfficherImageOrganisme('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getAfficherImageOrganisme();

        $this->assertEquals(false, $real);
    }

    public function testGetAfficherImageOrganismeEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setAfficherImageOrganisme('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getAfficherImageOrganisme();

        $this->assertEquals(true, $real);
    }

    public function testGetMultiLinguismeAgentDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setMultiLinguismeAgent('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getMultiLinguismeAgent();

        $this->assertEquals(false, $real);
    }

    public function testGetMultiLinguismeAgentEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setMultiLinguismeAgent('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );
        $real = $atexoMenu->getMultiLinguismeAgent();

        $this->assertEquals(true, $real);
    }

    public function testIsPanelLogoBoampDisabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('AFFICHER_BANDEAU_LOGO_BOAMP_AGENT' === $parameter) {
                            return '0';
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPanelLogoBoamp();
        $this->assertEquals(false, $real);
    }

    public function testIsPanelLogoBoampEnabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('AFFICHER_BANDEAU_LOGO_BOAMP_AGENT' === $parameter) {
                            return '1';
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPanelLogoBoamp();
        $this->assertEquals(true, $real);
    }

    public function testIsAgentConnectDisabled()
    {
        $sessionMock = $this->createMock(Session::class);

        $userMock = $this->createMock(CurrentUser::class);
        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_ENTREPRISE')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(false);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isAgentConnect();
        $this->assertEquals(false, $real);
    }

    public function testIsAgentConnectEnabled()
    {
        $sessionMock = $this->createMock(Session::class);

        $userMock = $this->createMock(CurrentUser::class);
        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_AGENT')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isAgentConnect();
        $this->assertEquals(true, $real);
    }

    public function testIsPanelLogoDisabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];

                        if ('URL_LOGO' === $parameter) {
                            return 'LOGO';
                        }
                        if ('ALT_URL_LOGO' === $parameter) {
                            return 'ALT';
                        }
                        if ('TITLE_URL_LOGO' === $parameter) {
                            return 'TITLE';
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPanelLogo();
        $expected = [
            'logo' => 'LOGO',
            'alt' => 'ALT',
            'title' => 'TITLE',
        ];
        $this->assertEquals($expected, $real);
    }

    public function testIsPanelLogoEnabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('URL_LOGO' === $parameter) {
                            return 'URL_LOGO';
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPanelLogo();
        $expected = [
            'logo' => 'true',
        ];
        $this->assertEquals($expected, $real);
    }

    public function testGetAcroOrganismeEnable()
    {
        $sessionMock = $this->createMock(Session::class);

        $userMock = $this->createMock(CurrentUser::class);
        $userMock->expects($this->any())
            ->method('getAcronymeOrga')
            ->willReturn('pim-min');
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->getAcroOrganisme();
        $this->assertEquals('pim-min', $real);
    }

    public function testIsPortailVisibleSocleInterne()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('1');
        $cp->setSocleExternePpp('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('URL_ACCUEIL_SPIP' === $parameter) {
                            return 'URL_ACCUEIL_SPIP';
                        }
                        if ('URL_ACCUEIL_SPIP_AGENT' === $parameter) {
                            return 'URL_ACCUEIL_SPIP_AGENT';
                        }
                        if ('URL_SOCLE_EXTERNE' === $parameter) {
                            return 'URL_SOCLE_EXTERNE';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPortailVisible();
        $this->assertEquals('URL_ACCUEIL_SPIP_AGENT', $real);
    }

    public function testIsPortailVisibleSocleExterne()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('0');
        $cp->setSocleExternePpp('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('URL_ACCUEIL_SPIP' === $parameter) {
                            return 'URL_ACCUEIL_SPIP';
                        }
                        if ('URL_ACCUEIL_SPIP_AGENT' === $parameter) {
                            return 'URL_ACCUEIL_SPIP_AGENT';
                        }
                        if ('URL_SOCLE_EXTERNE' === $parameter) {
                            return 'URL_SOCLE_EXTERNE';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPortailVisible();
        $this->assertEquals('URL_SOCLE_EXTERNE', $real);
    }

    public function testIsPortailVisibleDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('0');
        $cp->setSocleExternePpp('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('URL_ACCUEIL_SPIP' === $parameter) {
                            return 'URL_ACCUEIL_SPIP';
                        }
                        if ('URL_ACCUEIL_SPIP_AGENT' === $parameter) {
                            return 'URL_ACCUEIL_SPIP_AGENT';
                        }
                        if ('URL_SOCLE_EXTERNE' === $parameter) {
                            return 'URL_SOCLE_EXTERNE';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isPortailVisible();
        $this->assertEquals('false', $real);
    }

    public function testLinkAccueilAgentUnconnected()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);

        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_ENTREPRISE')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(false);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(false);

        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('PF_URL' === $parameter) {
                            return 'PF_URL';
                        }
                        if ('PF_URL_AGENT' === $parameter) {
                            return 'PF_URL_AGENT';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->getLinkAccueilAgent();
        $this->assertEquals('PF_URLindex.php?page=Agent.AgentHome', $real);
    }

    public function testLinkAccueilAgentSocleInterne()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);

        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_AGENT')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);

        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('PF_URL' === $parameter) {
                            return 'PF_URL';
                        }
                        if ('PF_URL_AGENT' === $parameter) {
                            return 'PF_URL_AGENT';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->getLinkAccueilAgent();
        $this->assertEquals('PF_URL_AGENTindex.php?page=Agent.AccueilAgentAuthentifieSocleinterne', $real);
    }

    public function testLinkAccueilAgentConnected()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setSocleInterne('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);

        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_AGENT')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);

        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('PF_URL' === $parameter) {
                            return 'PF_URL';
                        }
                        if ('PF_URL_AGENT' === $parameter) {
                            return 'PF_URL_AGENT';
                        }
                    }
                )
            );

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->getLinkAccueilAgent();
        $this->assertEquals('PF_URL_AGENTagent', $real);
    }

    public function testIsNotificationsAgentDisabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setNotificationsAgent('0');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);

        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_AGENT')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);

        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isNotificationsAgent();
        $this->assertEquals(false, $real);
    }

    public function testIsNotificationsAgentEnabled()
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);

        $cp = new ConfigurationPlateforme();
        $classCP = new ReflectionClass($cp);
        $propertyCP = $classCP->getProperty('idAuto');
        $propertyCP->setAccessible(true);
        $propertyCP->setValue($cp, 1);

        $cp->setNotificationsAgent('1');

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->any())->method('getConfigurationPlateforme')->willReturn($cp);

        $doctrineMock->expects($this->any())->method('getRepository')->willReturn($repo);

        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);

        $userMock->expects($this->any())
            ->method('hasRole')
            ->with('ROLE_AGENT')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isAgent')
            ->willReturn(true);
        $userMock->expects($this->any())
            ->method('isConnected')
            ->willReturn(true);

        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->isNotificationsAgent();
        $this->assertEquals(true, $real);
    }

    public function testAfficherFooterAgentDisabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];

                        if ('AFFICHAGE_FOOTER_MPE_VERSION_AGENT' === $parameter) {
                            return 1;
                        }
                        if ('MPE_VERSION' === $parameter) {
                            return 'MPE_VERSION';
                        }
                        if ('NOMBRE_CARACTERE_NUMERO_COURT_VERSION' === $parameter) {
                            return 5;
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->afficherFooterAgent();
        $expected = substr('MPE_VERSION', 0, 5);
        $this->assertEquals($expected, $real);
    }

    public function testAfficherFooterAgentEnabled()
    {
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $containerMock->expects($this->any())
            ->method('getParameter')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];

                        if ('AFFICHAGE_FOOTER_MPE_VERSION_AGENT' === $parameter) {
                            return 0;
                        }
                        if ('MPE_VERSION' === $parameter) {
                            return 'MPE_VERSION';
                        }
                        if ('NOMBRE_CARACTERE_NUMERO_COURT_VERSION' === $parameter) {
                            return 5;
                        }
                    }
                )
            );
        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->afficherFooterAgent();
        $this->assertEquals('false', $real);
    }

    public function testSelectFlagOn()
    {
        $langue = 'fr';
        $session = new Session(new MockArraySessionStorage());
        $session->set('lang', 'fr');
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $session,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->selectFlag($langue);
        $this->assertEquals('on', $real);
    }

    public function testSelectFlagOff()
    {
        $langue = 'en';
        $session = new Session(new MockArraySessionStorage());
        $session->set('lang', 'fr');
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $session,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->selectFlag($langue);
        $this->assertEquals('off', $real);
    }

    public function testGetLang()
    {
        $mock = $this->createMock(Request::class);
        $sessionMock = $this->createMock(Session::class);
        $userMock = $this->createMock(CurrentUser::class);
        $requestMock = $this->createMock(RequestStack::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $doctrineMock = $this->createMock(EntityManager::class);
        $habilitationMock = $this->createMock(Habilitation::class);

        $requestMock->expects($this->any())
            ->method('getCurrentRequest')
            ->willReturn($mock);
        $mock->expects($this->any())
            ->method('getLocale')
            ->willReturn('fr');

        $atexoMenu = new AtexoMenu(
            $containerMock,
            $doctrineMock,
            $sessionMock,
            $userMock,
            $requestMock,
            $translatorMock,
            $habilitationMock
        );

        $real = $atexoMenu->getLang();
        $this->assertEquals('.fr.js', $real);
    }

    public function testAuthentificationAgentMultiOrganismeIsFalse()
    {
        $configPlateforme = new ConfigurationPlateforme();
        $configPlateforme->setAuthentificationAgentMultiOrganismes(0);

        $configPlateformeRepository = $this->createMock(ConfigurationPlateformeRepository::class);
        $configPlateformeRepository->expects($this->any())
            ->method('getConfigurationPlateforme')
            ->willReturn($configPlateforme);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($configPlateformeRepository);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->isAuthentificationAgentMultiOrganismes();

        $this->assertFalse($result);
    }

    public function testAuthentificationAgentMultiOrganismeIsTrue()
    {
        $configPlateforme = new ConfigurationPlateforme();
        $configPlateforme->setAuthentificationAgentMultiOrganismes(1);

        $configPlateformeRepository = $this->createMock(ConfigurationPlateformeRepository::class);
        $configPlateformeRepository->expects($this->any())
            ->method('getConfigurationPlateforme')
            ->willReturn($configPlateforme);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($configPlateformeRepository);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $this->createMock(CurrentUser::class),
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->isAuthentificationAgentMultiOrganismes();

        $this->assertTrue($result);
    }

    public function testGetComptesAgentsAssociesWithOtherUserType()
    {
        $user = new Inscrit();

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser->expects($this->once())
            ->method('getCurrentUser')
            ->willReturn($user);

        $comptesAgentsAssociesRepo = $this->createMock(ComptesAgentsAssociesRepository::class);
        $comptesAgentsAssociesRepo->expects($this->never())
            ->method('findComptesAssocies');

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->never())
            ->method('getRepository')
            ->willReturn($comptesAgentsAssociesRepo);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->getComptesAgentsAssocies();

        $this->assertEmpty($result);
    }

    public function testGetComptesAgentsAssociesIsEmpty()
    {
        $agent = new Agent();
        $agent->setId(270);

        $comptesAgentsAssocies = new ComptesAgentsAssocies();
        $comptesAgentsAssocies->setComptePrincipal(275);
        $comptesAgentsAssocies->setCompteSecondaire(274);

        $arrayCompte = [];

        if (
            $agent->getId() == $comptesAgentsAssocies->getComptePrincipal()
            || $agent->getId() == $comptesAgentsAssocies->getCompteSecondaire()
        ) {
            $arrayCompte[] = $comptesAgentsAssocies;
        }

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser->expects($this->once())
            ->method('getCurrentUser')
            ->willReturn($agent);

        $comptesAgentsAssociesRepo = $this->createMock(ComptesAgentsAssociesRepository::class);
        $comptesAgentsAssociesRepo->expects($this->any())
            ->method('findComptesAssocies')
            ->with($agent)
            ->willReturn($arrayCompte);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($comptesAgentsAssociesRepo);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->getComptesAgentsAssocies();

        $this->assertEmpty($result);
    }

    public function testGetComptesAgentsAssociesIsNotEmpty()
    {
        $agent = new Agent();
        $agent->setId(275);

        $comptesAgentsAssocies = new ComptesAgentsAssocies();
        $comptesAgentsAssocies->setComptePrincipal(275);
        $comptesAgentsAssocies->setCompteSecondaire(274);

        $arrayCompte = [];

        if (
            $agent->getId() == $comptesAgentsAssocies->getComptePrincipal()
            || $agent->getId() == $comptesAgentsAssocies->getCompteSecondaire()
        ) {
            $arrayCompte[] = $comptesAgentsAssocies;
        }

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser->expects($this->once())
            ->method('getCurrentUser')
            ->willReturn($agent);

        $comptesAgentsAssociesRepo = $this->createMock(ComptesAgentsAssociesRepository::class);
        $comptesAgentsAssociesRepo->expects($this->once())
            ->method('findComptesAssocies')
            ->with($agent)
            ->willReturn($arrayCompte);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($comptesAgentsAssociesRepo);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->getComptesAgentsAssocies();

        $this->assertNotEmpty($result);
        $this->assertEquals(1, count($result));
    }

    public function testGetCompteAgentAssocieIsPrimaryAndActiveIsFalse()
    {
        $user = new Inscrit();

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser->expects($this->once())
            ->method('getCurrentUser')
            ->willReturn($user);

        $comptesAgentsAssociesRepo = $this->createMock(ComptesAgentsAssociesRepository::class);
        $comptesAgentsAssociesRepo->expects($this->never())
            ->method('getAllActiveComptesAssocies');


        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->never())
            ->method('getRepository')
            ->willReturn($comptesAgentsAssociesRepo);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->getCompteAgentAssocieIsPrimaryAndActive();

        $this->assertFalse($result);
    }

    public function testGetCompteAgentAssocieIsPrimaryAndActiveIsTrue()
    {
        $user = new Agent();

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser->expects($this->once())
            ->method('getCurrentUser')
            ->willReturn($user);

        $comptesAgentsAssociesRepo = $this->createMock(ComptesAgentsAssociesRepository::class);
        $comptesAgentsAssociesRepo->expects($this->once())
            ->method('getAllActiveComptesAssocies')
            ->willReturn(['el1', 'el2']);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($comptesAgentsAssociesRepo);

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $result = $atexoMenu->getCompteAgentAssocieIsPrimaryAndActive();

        $this->assertTrue($result);
    }

    /**
     * @dataProvider spaserAcessDataProvider
     */
    public function testIsAccessSpaserGranted(bool $spaserAccess, bool $expected): void
    {
        $user = new Agent();
        $user->setAcronymeOrganisme('acronyme');

        $mockCurrentUser = $this->createMock(CurrentUser::class);
        $mockCurrentUser
            ->expects($this->once())
            ->method('getAcronymeOrga')
            ->willReturn('acronyme')
        ;

        $configurationOrganisme = new ConfigurationOrganisme();
        $configurationOrganisme->setAccesModuleSpaser($spaserAccess);

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository
            ->expects($this->once())
            ->method('getByAcronyme')
            ->with('acronyme')
            ->willReturn($configurationOrganisme)
        ;

        $entityManager = $this->createMock(EntityManagerInterface::class);
        $entityManager
            ->expects($this->once())
            ->method('getRepository')
            ->willReturn($configurationOrganismeRepository)
        ;

        $atexoMenu = new AtexoMenu(
            $this->createMock(ContainerInterface::class),
            $entityManager,
            $this->createMock(SessionInterface::class),
            $mockCurrentUser,
            $this->createMock(RequestStack::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Habilitation::class)
        );

        $this->assertSame($expected, $atexoMenu->isAccessSpaserGranted());
    }

    public function spaserAcessDataProvider(): array
    {
        return [
            "access granted" => [true, true],
            "access not granted" => [false, false],
        ];
    }
}
