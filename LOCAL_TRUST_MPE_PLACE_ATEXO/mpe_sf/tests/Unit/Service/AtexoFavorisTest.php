<?php

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Service\AtexoFavoris;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;

/**
 * Class AtexoFavorisTest.
 */
class AtexoFavorisTest extends TestCase
{
    private $containerMock;
    private $emMock;
    private $logger;

    protected function setUp(): void
    {
        $consultation = new Consultation();
        $agent = new Agent();

        $consultationFavoris = new ConsultationFavoris();
        $consultationFavoris->setConsultation($consultation);
        $consultationFavoris->setAgent($agent);

        $this->containerMock = $this->createMock(ContainerInterface::class);

        $repository = $this->createMock(ObjectRepository::class);
        $repository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($consultationFavoris);

        $this->emMock = $this->createMock(EntityManager::class);

        $this->emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repository);

        $this->logger = $this->createMock(Logger::class);
    }

    public function testAddFavorisInsert()
    {
        $reference = '1';
        $user = '1';

        $repositoryEmpty = $this->createMock(ObjectRepository::class);
        $repositoryEmpty->expects($this->any())
            ->method('findOneBy')
            ->willReturn(null);

        $agent = new Agent();
        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('find')
            ->willReturn($agent);

        $consultation = new Consultation();
        $repositoryConsultation = $this->createMock(ObjectRepository::class);
        $repositoryConsultation->expects($this->any())
            ->method('findOneBy')
            ->willReturn($consultation);

        $emMock = $this->createMock(EntityManager::class);

        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryEmpty, $repositoryAgent, $repositoryConsultation);

        $service = new AtexoFavoris($this->containerMock, $emMock, $this->logger);
        $result = $service->addFavoris($reference, $user);

        $this->assertTrue($result['statut']);
    }

    public function testAddFavorisExist()
    {
        $reference = '1';
        $user = '1';

        $service = new AtexoFavoris($this->containerMock, $this->emMock, $this->logger);
        $result = $service->addFavoris($reference, $user);

        $this->assertFalse($result['statut']);
    }

    public function testdeleteFavoris()
    {
        $reference = '1';
        $user = '1';
        $service = new AtexoFavoris($this->containerMock, $this->emMock, $this->logger);
        $result = $service->deleteFavoris($reference, $user);
        $this->assertTrue($result['statut']);
    }
}
