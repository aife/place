<?php

namespace Tests\Unit\Service\Messec;

use Exception;
use ReflectionClass;
use App\Entity\Consultation;
use App\Model\Messec\Email;
use App\Model\Messec\Message;
use App\Model\Messec\PieceJointe;
use App\Repository\ConsultationRepository;
use App\Service\ClientWs\ResponseWs;
use App\Service\Messagerie\ArchiveMessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Service\Messec\ArchiveService;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ArchiveServiceTest extends TestCase
{
    private $entityManagerMock;

    private $container;
    private $logger;

    /**
     *
     * @var ArchiveService
     */
    private $archiveService;

    public function testFormatEchangesForDIC()
    {
        $emails = $this->getMessecEmails();
        $response = $this->archiveService->formatEchangesForDIC($emails, 'MESSECTOKEN');
        $this->assertTrue(is_array($response), 'La réponse doit être un array');
        $this->assertTrue(is_array($response[0]), 'chaque élément doit être un array');
        $echange = $response[0];
        $this->assertEquals(11, $echange['ID']);
        $this->assertStringContainsString('tempmessec', $echange['UID']);
        $this->assertStringContainsString('tempmessec', $echange['UID']);
        $this->assertStringContainsString('.pdf', $echange['PDF_ECHANGE']);
        $this->assertTrue(is_array($echange['PJ_MESSAGE_1']), 'la liste des PJs doit être un array');
        $this->assertTrue(is_array($echange['PJ_MESSAGE_2']), 'la liste des PJs doit être un array');
    }

    private function getMessecEmails()
    {
        $pjAgent = new PieceJointe();
        $pjAgent->setId(1);
        $pjAgent->setNom('PJAGENT.png');
        $pjAgent->setContentType('image/png');

        $pjEntreprise = new PieceJointe();
        $pjEntreprise->setId(2);
        $pjEntreprise->setNom('PJENTREPRISE.png');
        $pjEntreprise->setContentType('image/png');

        $messageAgent = new Message();
        $messageAgent->setPiecesJointes([$pjAgent]);

        $messageEntreprise = new Message();
        $messageEntreprise->setPiecesJointes([$pjEntreprise]);

        $emailEntreprise = new Email();
        $emailEntreprise->setCodeLien('codeLien');
        $emailEntreprise->setMessage($messageEntreprise);

        $email = new Email();
        $email->setId(11);
        $email->setCodeLien('codeLien');
        $email->setMessage($messageAgent);
        $email->setReponse($emailEntreprise);

        return [$email];
    }

    public function testGetFormatedToken()
    {
        $response = $this->archiveService->getFormatedToken(93);
        $token = trim($response->getBody()->getContents(), '"');
        $this->assertEquals('MESSECTOKEN', $token, 'nous devons recevoir un TOKEN si tout est OK');
    }

    public function testExceptionGetFormatedToken()
    {
        // Nous devons recevoir une exception si nous n'avons pas réussi à récupérer le token
        $this->expectException(Exception::class);
        $this->archiveService->getFormatedToken(69);
    }

    public function testGetEchangesFromMessecResponse()
    {
        $echanges = $this->archiveService->getEchangesFromMessecResponse($this->getMessecData());
        $this->assertTrue($echanges instanceof Email, 'Nous devons recevoir un objet Email');
    }

    private function getMessecData()
    {
        return json_encode(['content' => ['id' => 111]]);
    }

    public function testExceptionGetEchangesFromMessecResponse()
    {
        // Si les données reçu par MESSEC sont mal formatées (ou ont changées), nous devons avoir une exception)
        $this->expectException(Exception::class);
        $echanges = $this->archiveService->getEchangesFromMessecResponse($this->getWrongMessecData());
    }

    private function getWrongMessecData()
    {
        return json_encode(['fake' => ['id' => 111]]);
    }

    public function testGetResponseContent()
    {
        $response = $this->archiveService->getResponseContent($this->getMessecData());
        $response = json_decode($response);
        $this->assertTrue(is_object($response), 'nous devons avoir un objet json contenant la réponse MESSEC');
        $this->assertEquals(111, $response->id);
    }

    public function testSaveMessecFile()
    {
        $response = $this->archiveService->saveMessecFile('dirtest/', 'fileQuiMarche.txt', 'MYDATA');
        $this->assertTrue($response, 'Nous devons recevoir true si le fichier a bien été enregistré');
    }

    public function testSaveMessecFileWithCreateDir()
    {
        $response = $this->archiveService->saveMessecFile('dirtocreate/', 'fileQuiMarche.txt', 'MYDATA');
        $this->assertTrue($response, 'Nous devons recevoir true si le fichier a bien été enregistré');
    }

    public function testExceptionSaveMessecFile()
    {
        // Nous devons recevoir une exception si le fichier n'a pas été sauvegardé
        $this->expectException(Exception::class);
        $response = $this->archiveService->saveMessecFile('dirtest/', 'file.txt', 'MYDATA');
    }

    public function testGetAndSavePdf()
    {
        $response = $this->archiveService->getAndSavePdf(11, 'MESSECTOKEN', 'dirtest/', 'test.pdf');
        $this->assertTrue($response, 'Nous devons recevoir true si le fichier a bien été enregistré');
    }

    public function testExceptionGetAndSavePdf()
    {
        // Nous devons recevoir une exception si le fichier n'a pas été sauvegardé
        $this->expectException(Exception::class);
        $this->archiveService->getAndSavePdf(12, 'MESSECTOKEN', 'dirtest/', 'test.pdf');
    }

    public function testGetAndSavePieceJointe()
    {
        $response = $this->archiveService->getAndSavePieceJointe('codeLien', 111, 'image/png', 'dirtest/', 'test.png');
        $this->assertTrue($response, 'Nous devons recevoir true si le fichier a bien été enregistré');
    }

    public function testExceptionGetAndSavePieceJointe()
    {
        // Nous devons recevoir une exception si le fichier n'a pas été sauvegardé
        $this->expectException(Exception::class);
        $this->archiveService->getAndSavePieceJointe('codeLien', 112, 'image/png', 'dirtest/', 'test.png');
    }

    protected function setUp(): void
    {
        $this->initEm();
        $this->container = $this->createMock(ContainerInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);


        $this->container->expects($this->any())
            ->method('getParameter')
            ->willReturnCallback(
                function ($param) {
                    if ('COMMON_TMP' == $param) {
                        return '/tmp/';
                    } else {
                        return false;
                    }
                }
            );

        $logger = $this->createMock(LoggerInterface::class);
        $this->container->expects($this->any())
            ->method('get')
            ->with('mpe.logger')
            ->willReturn($logger);

        $messagerieService = $this->createMock(ArchiveMessagerieService::class);
        $webServicesMessagerie = $this->createMock(WebServicesMessagerie::class);

        $webServicesMessagerie->expects($this->any())
            ->method('getContent')
            ->willReturnCallback(
                function ($method, ...$params) {
                    if ('initialisationSuiviToken' == $method) {
                        $param = end($params);
                        if (93 == $param['idObjetMetier']) {
                            $body = '"MESSECTOKEN"';
                            $guzzlResponse = new ResponseWs(200, [], $body);

                            return $guzzlResponse;
                        } else {
                            throw new Exception();
                        }
                    }
                    if ('getPdf' == $method) {
                        if (11 == $params[0] && 'MESSECTOKEN' == $params[1]) {
                            return '"DATAPDF"';
                        } else {
                            throw new Exception();
                        }
                    }
                    if ('getPieceJointe' == $method) {
                        if ('codeLien' == $params[0] && 112 != $params[1] && 'image/png' == $params[2]) {
                            return '"DATAPNG"';
                        } else {
                            throw new Exception();
                        }
                    }
                }
            );

        $this->archiveService = new ArchiveService(
            $this->entityManagerMock,
            $this->container,
            $messagerieService,
            $this->logger,
            $webServicesMessagerie,
            $this->getMountmanager()
        );
    }

    private function initEm()
    {
        $this->entityManagerMock = $this->createMock(EntityManager::class);
        $this->entityManagerMock->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Consultation::class == $repository) {
                        $consultation = new Consultation();
                        $class = new ReflectionClass($consultation);
                        $consultation->setReferenceUtilisateur('REFERENCEUTILISATEUR');
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($consultation, 93);
                        $repository = $this->createMock(ConsultationRepository::class);
                        $repository->expects($this->any())
                            ->method('find')
                            ->willReturn($consultation);

                        return $repository;
                    }
                }
            );
    }

    public function getMountmanager()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $mountManager = new MountManager(
            $logger,
            $this->createMock(EntityManagerInterface::class),
            '/data/local-trust/mpe_place_test/',
            '/var/tmp/mpe/',
            '/tmp/mnt2;/tmp/mnt3;/tmp/mnt5;/tmp/mnt4',
            new MemoryAdapter(),
        );

        $mountManager->getFilesystem('common_tmp')->createDir('testdir');
        $mountManager->getFilesystem('common_tmp')->write('dirtest/file.txt', 'MYDADA');

        return $mountManager;
    }

    private function getMessecEchanges()
    {
        $pjAgent = new PieceJointe();
        $pjAgent->setId(1);
        $pjAgent->setNom('PJAGENT.png');
        $pjAgent->setContentType('image/png');

        $pjEntreprise = new PieceJointe();
        $pjEntreprise->setId(2);
        $pjEntreprise->setNom('PJENTREPRISE.png');
        $pjEntreprise->setContentType('image/png');

        $messageAgent = new Message();
        $messageAgent->setPiecesJointes([$pjAgent]);

        $messageEntreprise = new Message();
        $messageEntreprise->setPiecesJointes([$pjEntreprise]);

        $emailEntreprise = new Email();
        $emailEntreprise->setCodeLien('codeLien');
        $emailEntreprise->setMessage($messageEntreprise);

        $email = new Email();
        $email->setId(11);
        $email->setCodeLien('codeLien');
        $email->setMessage($messageAgent);
        $email->setReponse($emailEntreprise);

        return [$email];
    }
}
