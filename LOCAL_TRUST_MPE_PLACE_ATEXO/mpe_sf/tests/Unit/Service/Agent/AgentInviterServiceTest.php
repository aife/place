<?php

namespace Tests\Unit\Service\Agent;

use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\AffiliationServiceRepository;
use App\Repository\HabilitationAgentRepository;
use App\Repository\InvitePermanentTransverseRepository;
use App\Service\Agent\AgentInviterService;
use App\Service\Agent\Habilitation;
use App\Service\ContratService;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class AgentInviterServiceTest extends TestCase
{
    const ORGANISME = 'PMI-MIN-1';
    const SERVICE = 2;

    public function testGetPermanentGuestsMonEntitee()
    {
        $em = $this->createMock(EntityManager::class);
        $habilitationAgentRepository = $this->getPermanentGuestsMonEntitee($em);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($habilitationAgentRepository);

        $agentInviterService = new AgentInviterService($em, $this->createMock(ContratService::class), $this->createMock(Habilitation::class));
        $this->assertEquals(
            [1, 2],
            $agentInviterService
            ->getPermanentGuestsMonEntitee(self::ORGANISME, self::SERVICE)
        );
    }

    public function testGetPermanentGuestsEntiteeDependante()
    {
        $em = $this->createMock(EntityManager::class);

        $callback = $this->getPermanentGuestsEntiteeDependante($em);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($callback[0], $callback[1]);

        $agentInviterService = new AgentInviterService($em, $this->createMock(ContratService::class), $this->createMock(Habilitation::class));
        $this->assertEquals(
            [],
            $agentInviterService
            ->getPermanentGuestsEntiteeDependante(self::ORGANISME, self::SERVICE)
        );
    }

    public function testGetPermanentGuestsEntiteeTransverse()
    {
        $em = $this->createMock(EntityManager::class);
        $callback = $this->getPermanentGuestsEntiteeTransverse($em);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $callback[0],
                $callback[1],
                $callback[1]
            );

        $agentInviterService = new AgentInviterService($em, $this->createMock(ContratService::class), $this->createMock(Habilitation::class));
        $this->assertEquals(
            [1, 2],
            $agentInviterService
            ->getPermanentGuestsEntiteeTransverse(self::ORGANISME, self::SERVICE)
        );
    }

    /**
     * @throws \Exception
     */
    public function testGetListAgentInviter(): void
    {
        $em = $this->createMock(EntityManager::class);

        $this->getListAgent();
        $this->getListPole();

        $habilitationAgentRepository = $this->getPermanentGuestsMonEntitee($em);
        $callback = $this->getPermanentGuestsEntiteeDependante($em);
        $callbackTransverse = $this->getPermanentGuestsEntiteeTransverse($em);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $habilitationAgentRepository,
                $callback[0],
                $callback[1],
                $callbackTransverse[0],
                $callbackTransverse[1],
                $callbackTransverse[1]
            );

        $agentInviterService = new AgentInviterService($em, $this->createMock(ContratService::class), $this->createMock(Habilitation::class));
        $this->assertEquals([1, 2, 1, 2], $agentInviterService
            ->getListAgentInviter(self::ORGANISME, self::SERVICE));
    }

    /**
     * @param $em
     *
     * @return HabilitationAgentRepository|\PHPUnit\Framework\MockObject\MockObject
     */
    public function getPermanentGuestsMonEntitee($em)
    {
        $permanentGuestsMonEntitee = $this->getListAgent();

        $habilitationAgentRepository = $this->createMock(HabilitationAgentRepository::class);
        $habilitationAgentRepository->expects($this->any())
            ->method('retrievePermanentGuests')
            ->willReturn($permanentGuestsMonEntitee);

        return $habilitationAgentRepository;
    }

    /**
     * @param $em
     */
    public function getPermanentGuestsEntiteeDependante($em): array
    {
        $permanentGuestsMonEntitee = $this->getListAgent();
        $tabPole = $this->getListPole();

        $affiliationServiceRepository = $this->createMock(AffiliationServiceRepository::class);
        $affiliationServiceRepository->expects($this->any())
            ->method('getParentByServiceIdAndOrganisme')
            ->willReturn($tabPole);

        $callBack[] = $affiliationServiceRepository;
        $entiteDependanteHabilitationAgentRepository = $this->createMock(HabilitationAgentRepository::class);
        $entiteDependanteHabilitationAgentRepository->expects($this->any())
            ->method('retrievePermanentEntiteDependante')
            ->willReturn($permanentGuestsMonEntitee);

        $callBack[] = $entiteDependanteHabilitationAgentRepository;

        return $callBack;
    }

    /**
     * @param $em
     */
    public function getPermanentGuestsEntiteeTransverse($em): array
    {
        $habilitationAgentRepository = $this->getPermanentGuestsMonEntitee($em);

        $invitePermanentTransvers = new InvitePermanentTransverse();
        $invitePermanentTransvers->setAgent(1);
        $invitePermanentTransvers->setService(1);
        $invitePermanentTransvers->setAcronyme(self::ORGANISME);

        $invitePermanentTransverseRepository = $this->createMock(ObjectRepository::class);
        $invitePermanentTransverseRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($invitePermanentTransvers);

        $callBack[] = $habilitationAgentRepository;
        $callBack[] = $invitePermanentTransverseRepository;

        return $callBack;
    }

    public function getListAgent(): array
    {
        $organisme = new Organisme();
        $organisme->setAcronyme(self::ORGANISME);
        $service = new Service();
        $service->setId(1);
        for ($i = 1; $i < 3; ++$i) {
            $habilitationAgent = new HabilitationAgent();
            $agent = new Agent();
            $agent->setOrganisme($organisme);
            $agent->setId($i);
            $agent->setService($service);
            $habilitationAgent->setAgent($agent);
            $permanentGuestsMonEntitee[] = $habilitationAgent;
        }

        return $permanentGuestsMonEntitee;
    }

    public function getListPole(): array
    {
        $tabPole = [];
        for ($i = 1; $i < 3; ++$i) {
            $affiliationService = new AffiliationService();
            $service = new Service();
            $service->setId($i);

            $organisme = new Organisme();
            $organisme->setAcronyme(self::ORGANISME);
            $affiliationService->setServiceParentId($service);
            $affiliationService->setOrganisme($organisme);
            $tabPole[] = $affiliationService;
        }

        return $tabPole;
    }

    public function testGetEntitesWhereAgentIsInvited()
    {
        $agent = new Agent();
        $service = new class () extends Service {
            public function getId()
            {
                return 42;
            }
        };
        $agent->setService($service);

        $habilitationService = $this->createMock(Habilitation::class);

        $contratService = $this->createMock(ContratService::class);

        $entityManager = $this->createMock(EntityManager::class);

        $repository = $this->createMock(InvitePermanentTransverseRepository::class);

        $invitePermanent = $this->getMockBuilder(InvitePermanentTransverse::class)
            ->disableOriginalConstructor()
            ->getMock();

        $habilitationService->expects($this->exactly(2))
            ->method('checkHabilitation')
            ->withConsecutive(
                [$this->equalTo($agent), $this->equalTo('getInvitePermanentMonEntite')],
                [$this->equalTo($agent), $this->equalTo('getInvitePermanentEntiteDependante')]
            )
            ->willReturn(true);

        $contratService->expects($this->once())
            ->method('getSubServices')
            ->with($this->equalTo($agent->getService()->getId()))
            ->willReturn([new Service(), new Service()]);

        $entityManager->expects($this->once())
            ->method('getRepository')
            ->with($this->equalTo(InvitePermanentTransverse::class))
            ->willReturn($repository);

        $repository->expects($this->once())
            ->method('findBy')
            ->with($this->equalTo(['agent' => $agent]))
            ->willReturn([$invitePermanent]);

        $agentInviterService = new AgentInviterService($entityManager, $contratService, $habilitationService);

        $result = $agentInviterService->getEntitesWhereAgentIsInvited($agent);

        $this->assertNotEmpty($result);
        $this->assertCount(2, $result);
    }
}
