<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\HabilitationTypeProcedure;
use App\Repository\AgentRepository;
use App\Repository\HabilitationTypeProcedureRepository;
use App\Service\Agent\HabilitationTypeProcedureService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class HabilitationTypeProcedureServiceTest extends TestCase
{
    public function testCheckHabilitationForPradoIsFalse(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $agentRepository = $this->createMock(AgentRepository::class);
        $habilitationRepository = $this->createMock(HabilitationTypeProcedureRepository::class);

        $habilitationRepository->method('findByAgentAndSlug')->willReturn([]);
        $em->method('getRepository')->willReturn($habilitationRepository);

        $agentRepository->method('find')->willReturn(new Agent());

        $habilitationService = new HabilitationTypeProcedureService($em, $parameterBag, $agentRepository);

        $this->assertFalse($habilitationService->checkHabilitationForPrado(1, 'creer_consultation'));
    }

    public function testCheckHabilitationForPradoIsTrue(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $agentRepository = $this->createMock(AgentRepository::class);
        $habilitationRepository = $this->createMock(HabilitationTypeProcedureRepository::class);

        $habilitationRepository->method('findByAgentAndSlug')->willReturn([new HabilitationTypeProcedure()]);
        $em->method('getRepository')->willReturn($habilitationRepository);

        $agentRepository->method('find')->willReturn(new Agent());

        $habilitationService = new HabilitationTypeProcedureService($em, $parameterBag, $agentRepository);

        $this->assertTrue($habilitationService->checkHabilitationForPrado(1, 'creer_consultation'));
    }
}
