<?php

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Repository\AgentRepository;
use App\Security\ClientOpenIdConnect;
use App\Service\AbstractService;
use App\Service\Agent\Habilitation;
use App\Service\Agent\StatisticsService;
use App\Service\AgentTechniqueTokenService;
use App\Service\WebServicesRecensement;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StatisticsServiceTest extends TestCase
{
    public function testGetReportsWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://bi-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $habilitation = $this->createMock(Habilitation::class);
        $session = $this->createMock(SessionInterface::class);
        $repository = $this->createMock(AgentRepository::class);
        $reportsTests = [
            [
                'id' => '350e8041-b9da-4a37-b61b-3c159d8dd94e',
                'name' => 'Chiffres clés',
                'icone' => 'fa fa-tachometer-alt'
            ],
            [
                'id' => '8c2546f2-8144-42d3-8e0d-f6fcff9c07cf',
                'name' => 'Entreprises',
                'icone' => 'fa fa-industry'
            ],
            [
                'id' => '2852ac8c-f522-49f0-a054-22117e3d5869',
                'name' => 'Contrats',
                'icone' => 'fa fa-briefcase'
            ],
            [
                'id' => '8dfa9d3f-1df3-47ca-a094-621ad867d4fa',
                'name' => 'Consultations',
                'icone' => 'fa fa-globe'
            ]
        ];
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse(json_encode($reportsTests), ['http_code' => 200])
        ]);
        $agent = new Agent();

        $service = new StatisticsService($parameterBag, $client, $habilitation, $em, $session, $loger);
        $result = $service->getContent('getReports', $agent);

        $this->assertIsArray($result);
        $this->assertEquals($reportsTests, $result);
    }

    public function testGetReportsWithoutModule()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://bi-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $habilitation = $this->createMock(Habilitation::class);
        $session = $this->createMock(SessionInterface::class);
        $repository = $this->createMock(AgentRepository::class);
        $reportsTests = [
            [
                'id'    => '350e8041-b9da-4a37-b61b-3c159d8dd94e',
                'name'  => 'Chiffres clés',
                'icone' => 'fa fa-tachometer-alt',
                'offer' => ['id' => 1, 'code' => StatisticsService::FREE_CODE]
            ],
            [
                'id'    => '8c2546f2-8144-42d3-8e0d-f6fcff9c07cf',
                'name'  => 'Entreprises',
                'icone' => 'fa fa-industry',
                'offer' => ['id' => 2, 'code' => StatisticsService::PREMIUM_CODE]
            ],
            [
                'id'    => '2852ac8c-f522-49f0-a054-22117e3d5869',
                'name'  => 'Contrats',
                'icone' => 'fa fa-briefcase',
                'offer' => ['id' => 2, 'code' => StatisticsService::PREMIUM_CODE]
            ],
            [
                'id'    => '8dfa9d3f-1df3-47ca-a094-621ad867d4fa',
                'name'  => 'Consultations',
                'icone' => 'fa fa-globe',
                'offer' => ['id' => 2, 'code' => StatisticsService::PREMIUM_CODE]
            ]
        ];
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse(json_encode($reportsTests), ['http_code' => 200])
        ]);
        $agent = new Agent();

        $service = new StatisticsService($parameterBag, $client, $habilitation, $em, $session, $loger);
        $result = $service->getContent('getReports', $agent);

        $this->assertIsArray($result);
        $this->assertEquals(1, count($result));
        $this->assertEquals($reportsTests[0]['id'], $result[0]['id']);
    }

    public function testGetReportsWithSession()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://bi-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $habilitation = $this->createMock(Habilitation::class);
        $session = $this->createMock(SessionInterface::class);
        $repository = $this->createMock(AgentRepository::class);
        $reportsTests = [
            [
                'id' => '350e8041-b9da-4a37-b61b-3c159d8dd94e',
                'name' => 'Chiffres clés',
                'icone' => 'fa fa-tachometer-alt'
            ],
            [
                'id' => '8c2546f2-8144-42d3-8e0d-f6fcff9c07cf',
                'name' => 'Entreprises',
                'icone' => 'fa fa-industry'
            ],
            [
                'id' => '2852ac8c-f522-49f0-a054-22117e3d5869',
                'name' => 'Contrats',
                'icone' => 'fa fa-briefcase'
            ],
            [
                'id' => '8dfa9d3f-1df3-47ca-a094-621ad867d4fa',
                'name' => 'Consultations',
                'icone' => 'fa fa-globe'
            ]
        ];
        $session->method('get')
            ->willReturn($reportsTests);
        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')
            ->willReturn($repository);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 500]),
            new MockResponse(json_encode($reportsTests), ['http_code' => 500])
        ]);
        $agent = new Agent();

        $service = new StatisticsService($parameterBag, $client, $habilitation, $em, $session, $loger);
        $result = $service->getContent('getReports', $agent);

        $this->assertIsArray($result);
        $this->assertEquals($reportsTests, $result);
    }

    public function testXmlFileStructure()
    {
        $agent = new Agent();
        $organisme = new Organisme();

        $agent->setNom('TEST');
        $agent->setLogin('Logan');
        $organisme->setAcronyme('pmi-min');
        $agent->setOrganisme($organisme);

        $reflectionClass = new \ReflectionClass(StatisticsService::class);
        $methodPrivate = $reflectionClass->getMethod('setDataXml');
        $methodPrivate ->setAccessible(true);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');

        $statisticsService = new StatisticsService(
            $parameterBag,
            $this->createMock(HttpClientInterface::class),
            $this->createMock(Habilitation::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(SessionInterface::class),
            $this->createMock(LoggerInterface::class),
        );

        $xml = $methodPrivate->invokeArgs($statisticsService, [$agent]);

        $xmlJson = json_encode(simplexml_load_string($xml));
        $xmlArray = json_decode($xmlJson, true);

        $this->assertSame(8, count($xmlArray['envoi']['agent']));
        $this->assertSame(4, count($xmlArray['envoi']['agent']['organisme']));
        $this->assertSame('TEST', $xmlArray['envoi']['agent']['nom']);
        $this->assertSame('pmi-min', $xmlArray['envoi']['agent']['organisme']['acronyme']);
    }

    public function testCreateContextWithAuthentication()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://bi-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $habilitation = $this->createMock(Habilitation::class);
        $session = $this->createMock(SessionInterface::class);
        $repository = $this->createMock(AgentRepository::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $session->method('get')
            ->willReturn("FDSERT43HYTD5432SDPMLHRTDSFF");
        $em->method('getRepository')
            ->willReturn($repository);

        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('uuid-17820-CNH', ['http_code' => 200])
        ]);
        $agent = new Agent();
        $agent->setLogin('Logan');
        $agent->setOrganisme(new Organisme());

        $service = new StatisticsService($parameterBag, $client, $habilitation, $em, $session, $loger);
        $result = $service->getContent('createContext', $agent);

        $this->assertIsString($result);
        $this->assertEquals('uuid-17820-CNH', $result);
    }
}
