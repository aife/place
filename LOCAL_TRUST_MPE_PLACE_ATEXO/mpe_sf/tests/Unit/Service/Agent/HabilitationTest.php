<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationProfil;
use App\Repository\HabilitationAgentRepository;
use App\Service\Agent\Habilitation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class HabilitationTest extends TestCase
{
    public function testCheckHabilitationIsFalse()
    {
        $agent = new Agent();
        $habilitation =  new HabilitationAgent();

        $mockHabilitationAgentRepository = $this->createMock(HabilitationAgentRepository::class);
        $mockHabilitationAgentRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($habilitation);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockHabilitationAgentRepository);

        $mockHabilitation = new Habilitation($mockEntityManager, $logger);

        $asHabilitation = $mockHabilitation->checkHabilitation($agent, 'isGestionEnvol');

        $this->assertFalse($asHabilitation);
    }

    public function testCheckHabilitationIsTrue()
    {
        $agent = new Agent();
        $habilitation =  new HabilitationAgent();

        $habilitation->setGestionEnvol(true);

        $agent->setHabilitation($habilitation);

        $mockHabilitationAgentRepository = $this->createMock(HabilitationAgentRepository::class);
        $mockHabilitationAgentRepository->expects($this->once())
            ->method('findOneBy')
            ->willReturn($habilitation);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockHabilitationAgentRepository);

        $mockHabilitation = new Habilitation($mockEntityManager, $logger);

        $asHabilitation = $mockHabilitation->checkHabilitation($agent, 'isGestionEnvol');


        $this->assertTrue($asHabilitation);
    }

    public function testCreateHabilitationsAgent(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $userName = 'username';
        $profilId = '2';

        $habilitationProfil = new HabilitationProfil();
        $habilitationProfil->setGestionAgentPole('1');
        $habilitationProfil->setGestionBiCles('0');
        $habilitationProfil->setCreerConsultation('1');
        $habilitationProfil->setModifierConsultation('1');
        $habilitationProfil->setValiderConsultation('0');
        $habilitationProfil->setPublierConsultation('1');
        $habilitationProfil->setAdministrerOrganisme(1);
        $habilitationProfil->setAccesEchangeDocumentaire(true);
        $habilitationProfil->setGestionEnvol(true);

        $habilitationProfilRepository = $this->createMock(ServiceEntityRepository::class);
        $habilitationProfilRepository
            ->expects(self::once())
            ->method('find')
            ->with(2)
            ->willReturn($habilitationProfil)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($habilitationProfilRepository)
        ;

        $logger
            ->expects(self::never())
            ->method('warning')
        ;

        $habilitationService = new Habilitation($entityManager, $logger);

        $returnedHabilitationAgent = $habilitationService->createHabilitationsAgent($userName, $profilId);

        self::assertSame('1', $returnedHabilitationAgent->getGestionAgentPole());
        self::assertSame('0', $returnedHabilitationAgent->getGestionBiCles());
        self::assertSame('1', $returnedHabilitationAgent->getCreerConsultation());
        self::assertSame('1', $returnedHabilitationAgent->getModifierConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getValiderConsultation());
        self::assertSame('1', $returnedHabilitationAgent->getPublierConsultation());
        self::assertSame(1, $returnedHabilitationAgent->getAdministrerOrganisme());
        self::assertTrue($returnedHabilitationAgent->isAccesEchangeDocumentaire());
        self::assertTrue($returnedHabilitationAgent->isGestionEnvol());
    }

    public function testCreateHabilitationsAgentHabilitationProfilNotFound(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $userName = 'username';
        $profilId = '2';

        $habilitationProfil = new HabilitationProfil();
        $habilitationProfil->setGestionAgentPole('1');
        $habilitationProfil->setGestionBiCles('0');
        $habilitationProfil->setCreerConsultation('1');
        $habilitationProfil->setModifierConsultation('1');
        $habilitationProfil->setValiderConsultation('0');
        $habilitationProfil->setPublierConsultation('1');
        $habilitationProfil->setAdministrerOrganisme(1);
        $habilitationProfil->setAccesEchangeDocumentaire(true);
        $habilitationProfil->setGestionEnvol(true);

        $habilitationProfilRepository = $this->createMock(ServiceEntityRepository::class);
        $habilitationProfilRepository
            ->expects(self::once())
            ->method('find')
            ->with(2)
            ->willReturn(null)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($habilitationProfilRepository)
        ;

        $logger
            ->expects(self::once())
            ->method('warning')
            ->with("Habilitation profil not found for user username and profileId=2")
        ;

        $habilitationService = new Habilitation($entityManager, $logger);

        $returnedHabilitationAgent = $habilitationService->createHabilitationsAgent($userName, $profilId);

        self::assertSame('0', $returnedHabilitationAgent->getGestionAgentPole());
        self::assertSame('0', $returnedHabilitationAgent->getGestionBiCles());
        self::assertSame('1', $returnedHabilitationAgent->getCreerConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getModifierConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getValiderConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getPublierConsultation());
        self::assertSame(0, $returnedHabilitationAgent->getAdministrerOrganisme());
        self::assertFalse($returnedHabilitationAgent->isAccesEchangeDocumentaire());
        self::assertFalse($returnedHabilitationAgent->isGestionEnvol());
    }

    public function testCreateHabilitationsAgentWithEmptyProfilId(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $userName = 'username';

        $habilitationProfilRepository = $this->createMock(ServiceEntityRepository::class);
        $habilitationProfilRepository
            ->expects(self::once())
            ->method('find')
            ->with(null)
            ->willReturn(null)
        ;

        $entityManager
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($habilitationProfilRepository)
        ;

        $logger
            ->expects(self::once())
            ->method('warning')
            ->with("Habilitation profil not found for user username and profileId=")
        ;

        $habilitationService = new Habilitation($entityManager, $logger);

        $returnedHabilitationAgent = $habilitationService->createHabilitationsAgent($userName, null);

        self::assertSame('0', $returnedHabilitationAgent->getGestionAgentPole());
        self::assertSame('0', $returnedHabilitationAgent->getGestionBiCles());
        self::assertSame('1', $returnedHabilitationAgent->getCreerConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getModifierConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getValiderConsultation());
        self::assertSame('0', $returnedHabilitationAgent->getPublierConsultation());
        self::assertSame(0, $returnedHabilitationAgent->getAdministrerOrganisme());
        self::assertFalse($returnedHabilitationAgent->isAccesEchangeDocumentaire());
        self::assertFalse($returnedHabilitationAgent->isGestionEnvol());
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetSetterMethod(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $reflectionClass = new \ReflectionClass(Habilitation::class);
        $methodPrivate = $reflectionClass->getMethod('getSetterMethod');
        $methodPrivate ->setAccessible(true);

        $habilitationService = new Habilitation($entityManager, $logger);

        $this->assertSame(
            'setPublierConsultation',
            $methodPrivate->invokeArgs($habilitationService, ['getPublierConsultation'])
        );
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetSetterMethodWithIsGetter(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $reflectionClass = new \ReflectionClass(Habilitation::class);
        $methodPrivate = $reflectionClass->getMethod('getSetterMethod');
        $methodPrivate ->setAccessible(true);

        $habilitationService = new Habilitation($entityManager, $logger);

        $this->assertSame('setGestionEnvol', $methodPrivate->invokeArgs($habilitationService, ['isGestionEnvol']));
    }

    /**
     * @throws \ReflectionException
     */
    public function testGetSetterMethodReturnNull(): void
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);

        $reflectionClass = new \ReflectionClass(Habilitation::class);
        $methodPrivate = $reflectionClass->getMethod('getSetterMethod');
        $methodPrivate ->setAccessible(true);

        $habilitationService = new Habilitation($entityManager, $logger);

        $this->assertNull($methodPrivate->invokeArgs($habilitationService, ['badMethodName']));
    }

    public function testCreateOrUpdateHabilitationAgentFromProfil()
    {
        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockLogger = $this->createMock(LoggerInterface::class);
        $habilitation = new Habilitation($mockEntityManager, $mockLogger);

        $profilHabilitation = new HabilitationProfil();
        $profilHabilitation->setCreerConsultation('1');
        $profilHabilitation->setAccesReponses('1');
        $profilHabilitation->setAccederTousTelechargements('1');
        $profilHabilitation->setAdministrerOrganisme(1);

        $agentHabilitation = $habilitation->createOrUpdateHabilitationAgentFromProfil($profilHabilitation, null);

        $this->assertSame($agentHabilitation->getCreerConsultation(), $profilHabilitation->getCreerConsultation());
        $this->assertSame($agentHabilitation->getAccesReponses(), $profilHabilitation->getAccesReponses());
        $this->assertSame(
            $agentHabilitation->getAdministrerOrganisme(),
            $profilHabilitation->getAdministrerOrganisme()
        );
        $this->assertSame(
            $agentHabilitation->getAccederTousTelechargements(),
            $profilHabilitation->getAccederTousTelechargements()
        );
    }
}
