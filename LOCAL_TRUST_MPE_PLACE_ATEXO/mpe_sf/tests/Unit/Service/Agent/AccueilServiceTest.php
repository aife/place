<?php

namespace Tests\Unit\Service\Agent;

use App\Doctrine\Extension\ConsultationIdExtension;
use App\Service\Agent\AccueilService;
use App\Service\Consultation\SearchAgentService;
use App\Service\CurrentUser;
use App\Service\WebservicesMpeConsultations;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AccueilServiceTest extends TestCase
{
    private AccueilService|MockObject $accueilService;

    private SearchAgentService|MockObject $searchAgentService;

    private WebservicesMpeConsultations|MockObject $webservicesMpeConsultations;

    protected function setUp(): void
    {
        parent::setUp();
        $this->searchAgentService = $this->createMock(SearchAgentService::class);
        $this->webservicesMpeConsultations = $this->createPartialMock(
            WebservicesMpeConsultations::class,
            ['searchConsultations']
        );
        $this->accueilService = new AccueilService($this->searchAgentService, $this->webservicesMpeConsultations);
    }

    public function testCheckHabilitationForMenu(): void
    {
        $currentUser = $this->createMock(CurrentUser::class);
        $this->assertFalse($this->accueilService->checkHabilitationForMenu($currentUser));

        $currentUser->method('checkHabilitation')->with('gererMapaInferieurMontant')->willReturn(true);
        $this->assertTrue($this->accueilService->checkHabilitationForMenu($currentUser));
    }

    public function testGetStatistics(): void
    {
        $apiResponse = $this->apiResponse();
        unset($apiResponse->{'hydra:member'}[1]);
        unset($apiResponse->{'hydra:member'}[2]);
        $data = [
            'groupBy' => 'statutCalcule',
            'pagination' => false,
            ConsultationIdExtension::IDS_ONLY => 1,
        ];

        $wsConsultation = $this->createMock(WebservicesMpeConsultations::class);
        $wsConsultation->expects($this->once())
            ->method('getContent')
            ->willReturn($apiResponse);

        $agentService = $this->createMock(SearchAgentService::class);
        $agentService->expects($this->once())
            ->method('getDataWidgetsStats')
            ->with($apiResponse, $data);

        $accueilService = new AccueilService($agentService, $wsConsultation);
        $accueilService->getStatistics();
    }

    private function apiResponse(): \stdClass
    {
        $obj3 = new \stdClass();
        $obj3->{'libelle'} = 'Service';
        $obj3->{'total'} = '5';
        $obj3->{'statutCalcule'} = '4';

        $obj1 = new \stdClass();
        $obj1->{'libelle'} = 'Travaux';
        $obj1->{'total'} = '10';
        $obj1->{'statutCalcule'} = '3,4';

        $obj2 = new \stdClass();
        $obj2->{'libelle'} = 'Fourniture';
        $obj2->{'total'} = '30';
        $obj2->{'statutCalcule'} = '3';

        $obj4 = new \stdClass();
        $obj4->{'libelle'} = 'Travaux';
        $obj4->{'total'} = '100';
        $obj4->{'statutCalcule'} = '2,3,4';

        $apiResponse = new \stdClass();
        $apiResponse->{'hydra:member'} = [
            $obj1, $obj2, $obj3, $obj4
        ];

        return $apiResponse;
    }
}
