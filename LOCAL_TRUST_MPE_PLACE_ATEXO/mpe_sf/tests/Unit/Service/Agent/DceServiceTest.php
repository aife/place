<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\Organisme;
use App\Service\Agent\DceService;
use App\Service\AtexoFichierOrganisme;
use App\Utils\Filesystem\MountManager;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\InfosHorodatage;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\Memory\MemoryAdapter;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DceServiceTest extends TestCase
{
    /**
     * @var MountManager
     */
    private $mountManager;

    protected function setUp(): void
    {
        parent::setUp();

        $logger = $this->createMock(LoggerInterface::class);
        $this->mountManager = new MountManager(
            $logger,
            $this->createMock(EntityManagerInterface::class),
            '/data/local-trust/mpe_place_test/',
            '/var/tmp/mpe/',
            '',
            new MemoryAdapter(),
        );
    }

    public function testCreateDceFileDoesNotExist(): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $emMock = $this->createMock(EntityManagerInterface::class);
        $cryptoServiceMock = $this->createMock(AtexoCrypto::class);
        $loggerMock = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');

        $fichierOrg = new AtexoFichierOrganisme($containerMock, $emMock, $this->mountManager, $parameterBag);

        $sut = new DceService(
            $parameterBag,
            $fichierOrg,
            $emMock,
            $this->mountManager,
            $cryptoServiceMock,
            $loggerMock
        );
        $agent = new Agent();
        $consultation = new Consultation();

        $this->expectExceptionMessageMatches('/Fichier non trouvé :.*/');
        $this->expectException(\InvalidArgumentException::class);

        $sut->createDce('/path/that/doesnt/exist', $consultation, $agent);
    }

    public function testCreateDceErrorMoveTmpFileException(): void
    {
        $parameterBagMock = $this->createMock(ParameterBag::class);
        $parameterBagMock->expects($this->any())->method('get')->willReturn('a parameter');
        $emMock = $this->createMock(EntityManagerInterface::class);
        $cryptoServiceMock = $this->createMock(AtexoCrypto::class);
        $loggerMock = $this->createMock(LoggerInterface::class);
        $atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);

        $dceService = new DceService(
            $parameterBagMock,
            $atexoFichierOrganisme,
            $emMock,
            $this->mountManager,
            $cryptoServiceMock,
            $loggerMock
        );

        $fileDcePath = realpath(__DIR__ . '/../../data/DCE.zip');
        $paramCommontTmp = '/var/tmp/mpe/';
        $fileHash = hash_file('sha256', $fileDcePath);
        $jetonHorodatageBase64 = 'jetonHorodatageBase64';
        $horodatageBase64 = 'horodatageBase64';
        $exceptionMessage = 'Impossible de déplacer le fichier ' . $fileDcePath;

        $consultation = new Consultation();
        $consultation->setId(1);

        $agent = new Agent();
        $agent->setId(2);

        $infoHorodatage = new InfosHorodatage();
        $infoHorodatage->setHorodatage($horodatageBase64);
        $infoHorodatage->setJetonHorodatageBase64($jetonHorodatageBase64);

        $parameterBagMock
            ->expects($this->once())
            ->method('get')
            ->willReturn($paramCommontTmp)
        ;

        $fileSystem = $this->mountManager->getFilesystem('common_tmp');
        $fileSystem->createDir(realpath(__DIR__ . '/../../data'));
        $fileSystem->write($fileDcePath, 'Contenu');

        $cryptoServiceMock
            ->expects($this->once())
            ->method('demandeHorodatage')
            ->with($fileHash)
            ->willReturn($infoHorodatage)
        ;

        $atexoFichierOrganisme
            ->expects($this->once())
            ->method('moveTmpFile')
            ->with($fileDcePath)
            ->willReturn(false)
        ;

        $loggerMock
            ->expects($this->once())
            ->method('error')
            ->with($exceptionMessage)
        ;

        $this->expectExceptionMessage($exceptionMessage);
        $this->expectException(\Exception::class);

        $dceService->createDce($fileDcePath, $consultation, $agent);
    }

    public function testCreateDceSuccess(): void
    {
        $parameterBagMock = $this->createMock(ParameterBag::class);
        $emMock = $this->createMock(EntityManagerInterface::class);
        $cryptoServiceMock = $this->createMock(AtexoCrypto::class);
        $loggerMock = $this->createMock(LoggerInterface::class);
        $atexoFichierOrganisme = $this->createMock(AtexoFichierOrganisme::class);

        $dceService = new DceService(
            $parameterBagMock,
            $atexoFichierOrganisme,
            $emMock,
            $this->mountManager,
            $cryptoServiceMock,
            $loggerMock
        );

        $fileDcePath = realpath(__DIR__ . '/../../data/DCE.zip');
        $fileDceName = 'DCE.zip';
        $paramCommontTmp = '/var/tmp/mpe/';
        $paramExtensionDce = '.dce';
        $paramAjoutFile = '4';
        $fileHash = hash_file('sha256', $fileDcePath);
        $jetonHorodatageBase64 = 'jetonHorodatageBase64';
        $horodatageBase64 = '64';
        $acronyme = 'acronyme';

        $organisme = new Organisme();
        $organisme->setAcronyme($acronyme);

        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setOrganisme($organisme);

        $agent = new Agent();
        $agent->setId(2);

        $infoHorodatage = new InfosHorodatage();
        $infoHorodatage->setHorodatage($horodatageBase64);
        $infoHorodatage->setJetonHorodatageBase64($jetonHorodatageBase64);

        $blobFile = new class () extends BloborganismeFile {
            public function getId()
            {
                return 42;
            }
        };

        $parameterBagMock
            ->expects($this->exactly(3))
            ->method('get')
            ->willReturn($paramCommontTmp, $paramExtensionDce, $paramAjoutFile)
        ;

        $fileSystem = $this->mountManager->getFilesystem('common_tmp');
        $fileSystem->createDir(realpath(__DIR__ . '/../../data'));
        $fileSystem->write($fileDcePath, 'Contenu');

        $cryptoServiceMock
            ->expects($this->once())
            ->method('demandeHorodatage')
            ->with($fileHash)
            ->willReturn($infoHorodatage)
        ;

        $atexoFichierOrganisme
            ->expects($this->once())
            ->method('moveTmpFile')
            ->with($fileDcePath)
            ->willReturn(true)
        ;

        $loggerMock
            ->expects($this->never())
            ->method('error')
        ;

        $atexoFichierOrganisme
            ->expects($this->once())
            ->method('insertFile')
            ->with($fileDceName, $fileDcePath, $acronyme, null, $paramExtensionDce)
            ->willReturn(42)
        ;

        $emMock
            ->expects($this->once())
            ->method('persist')
            ->withAnyParameters()
        ;

        $emMock
            ->expects($this->once())
            ->method('flush')
        ;

        $result = $dceService->createDce($fileDcePath, $consultation, $agent);

        $this->assertSame(get_class($result), DCE::class);
        $this->assertSame($result->getAgentId(), $agent->getId());
        $this->assertSame($result->getConsultation(), $consultation);
        $this->assertSame($result->getDce(), $blobFile->getId());
        $this->assertSame($result->getHorodatage(), $jetonHorodatageBase64);
        $this->assertSame($result->getOrganisme(), $acronyme);
        $this->assertSame($result->getStatus(), $paramAjoutFile);
        $this->assertSame($result->getNomDce(), $fileDceName);
        $this->assertSame($result->getNomFichier(), $fileDceName);
    }
}
