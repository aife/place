<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\ComptesAgentsAssocies;
use App\Entity\Organisme;
use App\Repository\AgentRepository;
use App\Repository\ComptesAgentsAssociesRepository;
use App\Service\Agent\AccountsAssocService;
use App\Utils\EntityPurchase;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AccountsAssocServiceTest extends TestCase
{
    public function testListAccountAssocIsEmpty()
    {
        $agent = new Agent();
        $typeAssoc = 1;

        $accountsAssoc = [];

        $repoCompteAgentAssoc = $this->createMock(ComptesAgentsAssociesRepository::class);
        $repoCompteAgentAssoc->expects($this->once())
            ->method('findComptesAssocies')
            ->willReturn($accountsAssoc);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturn($repoCompteAgentAssoc);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->any())
            ->method('get')
            ->willReturn(1);

        $accountsAssocService = new AccountsAssocService(
            $em,
            $this->createMock(EntityPurchase::class),
            $parameter
        );

        $result = $accountsAssocService->listAccountsAssoc($agent, $typeAssoc);

        $this->assertEmpty($result);
        $this->assertCount(0, $result);
    }

    public function testListAccountAssocIsFull()
    {
        $agent = new Agent();
        $agent->setId(475);
        $agent->setServiceId(3);

        $typeAssoc = 1;

        $compteAgentAssoc1 = new ComptesAgentsAssocies();
        $compteAgentAssoc1->setComptePrincipal(475);
        $compteAgentAssoc1->setCompteSecondaire(6);

        $compteAgentAssoc2 = new ComptesAgentsAssocies();
        $compteAgentAssoc2->setComptePrincipal(475);
        $compteAgentAssoc2->setCompteSecondaire(473);

        $accountsAssoc = [$compteAgentAssoc1, $compteAgentAssoc2];

        $repoCompteAgentAssoc = $this->createMock(ComptesAgentsAssociesRepository::class);
        $repoCompteAgentAssoc->expects($this->once())
            ->method('findComptesAssocies')
            ->with($agent)
            ->willReturn($accountsAssoc);

        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->expects($this->any())
            ->method('get')
            ->willReturn(1);

        $organisme1 = new Organisme();
        $organisme1->setAcronyme('pmi');
        $organisme2 = new Organisme();
        $organisme2->setAcronyme('e3r');

        $agentA1 = new Agent();
        $agentA1->setId(6);
        $agentA1->setServiceId(6);
        $agentA1->setOrganisme($organisme1);
        $agentA2 = new Agent();
        $agentA2->setId(475);
        $agentA2->setServiceId(475);
        $agentA2->setOrganisme($organisme2);
        $agentsA = [$agentA1->getId() => $agentA1, $agentA2->getId() => $agentA2];

        $agentB1 = new Agent();
        $agentB1->setId(473);
        $agentB1->setServiceId(473);
        $agentB1->setOrganisme($organisme1);
        $agentB2 = new Agent();
        $agentB2->setId(475);
        $agentB2->setServiceId(475);
        $agentB2->setOrganisme($organisme2);
        $agentsB = [$agentB1->getId() => $agentB1, $agentB2->getId() => $agentB2];

        $repoAgent = $this->createMock(AgentRepository::class);
        $repoAgent->method('findListAgentFromAccountAssoc')
            ->withConsecutive(
                [
                    [$compteAgentAssoc1->getComptePrincipal(), $compteAgentAssoc1->getCompteSecondaire()]
                ],
                [
                    [$compteAgentAssoc2->getComptePrincipal(), $compteAgentAssoc2->getCompteSecondaire()]
                ]
            )
            ->willReturnOnConsecutiveCalls($agentsA, $agentsB);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repoCompteAgentAssoc, $repoAgent, $repoAgent);

        $accountsAssocService = new AccountsAssocService(
            $em,
            $this->createMock(EntityPurchase::class),
            $parameter
        );

        $result = $accountsAssocService->listAccountsAssoc($agent, $typeAssoc);

        $this->assertNotEmpty($result);
        $this->assertCount(3, $result);

        $this->assertArrayHasKey(6, $result);
        $this->assertArrayHasKey(473, $result);
        $this->assertArrayHasKey(475, $result);
    }
}
