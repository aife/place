<?php

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\SsoAgent;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\ServiceRepository;
use App\Repository\SsoAgentRepository;
use App\Service\AgentService;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;

class AgentServiceTest extends TestCase
{
    public function testGetSsoFalse()
    {
        $currentUserMock = $this->createMock(CurrentUser::class);
        $agentServiceMetierRepositoryMock = $this->createMock(SsoAgentRepository::class);
        $agentServiceMetierRepositoryMock->expects($this->any())
            ->method("getSsoAgentValid")
            ->willReturn(false);

        $emMock = $this->createMock(EntityManager::class);
        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($agentServiceMetierRepositoryMock);

        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);
        $agentServiceMetier = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('', $agentServiceMetier->getSso($currentUserMock, '1'));
    }

    public function testGetSso()
    {
        $currentUserMock = $this->createMock(CurrentUser::class);
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $agentServiceMetierRepositoryMock = $this->createMock(SsoAgentRepository::class);
        $sso = new SsoAgent();
        $sso->setIdSso('4a117581669b8');
        $agentServiceMetierRepositoryMock->expects($this->any())
            ->method("getSsoAgentValid")
            ->willReturn($sso);

        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($agentServiceMetierRepositoryMock);

        $agentServiceMetier = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('4a117581669b8', $agentServiceMetier->getSso($currentUserMock, '1'));
    }


    public function testGetSigleLabelleServiceForServiceIdEgale0()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(0);
        $service->setLibelle('labelService');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('OACRO - OrgaAcronyme', $agentService->getLabelService($agent));
    }

    public function testGetSigleLabelleServiceForServiceIdDifferent0()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(3);
        $service->setSigle('SigleService');
        $service->setLibelle('labelService');
        $service->setCheminComplet('ParentSigle - SigleService - labelService');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository->method('findOneBy')
            ->willReturn($service);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $serviceRepository
            );

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('ParentSigle - SigleService - labelService', $agentService->getLabelService($agent));
    }

    public function testGetLabelleServiceForServiceIdEgale0()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(0);
        $service->setLibelle('labelService');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('OrgaAcronyme', $agentService->getLabelService($agent, false));
    }

    public function testGetLabelleServiceForServiceIdDifferent0()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(3);
        $service->setSigle('SigleService');
        $service->setLibelle('labelService');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository->method('findOneBy')
            ->willReturn($service);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $serviceRepository
            );

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('labelService', $agentService->getLabelService($agent, false));
    }

    public function testGetSiren()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(3);
        $service->setSigle('SigleService');
        $service->setLibelle('labelService');
        $service->setSiren('1234567890');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository->method('findOneBy')
            ->willReturn($service);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $serviceRepository
            );

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('1234567890', $agentService->getSirenService($agent));
    }

    public function testGetComplement()
    {
        $emMock = $this->createMock(EntityManager::class);
        $parameterBagInterfaceMock = $this->createMock(ParameterBagInterface::class);

        $organisme = new Organisme();
        $organisme->setDenominationOrg('OrgaAcronyme');
        $organisme->setSigle('OACRO');

        $service = new Service();
        $service->setId(3);
        $service->setSigle('SigleService');
        $service->setLibelle('labelService');
        $service->setComplement('complement');

        $agent = new Agent();
        $agent->setNom('4a117581669b8');
        $agent->setOrganisme($organisme);
        $agent->setService($service);
        $agent->setServiceId($service->getId());

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository->method('findOneBy')
            ->willReturn($service);

        $emMock->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $serviceRepository
            );

        $agentService = new AgentService(
            $parameterBagInterfaceMock,
            $emMock,
        );

        $this->assertEquals('complement', $agentService->getComplementService($agent));
    }

    public function testIsAgentInArray()
    {
        $agentService = new AgentService(
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(EntityManager::class)
        );

        $agent = new class extends Agent {
            public function getId()
            {
                return 42;
            }
        };
        $agent2 = new class extends Agent {
            public function getId()
            {
                return 2;
            }
        };
        $this->assertEquals(true, $agentService->isAgentInArray($agent, [$agent]));
        $this->assertEquals(false, $agentService->isAgentInArray($agent, [$agent2]));
    }
}
