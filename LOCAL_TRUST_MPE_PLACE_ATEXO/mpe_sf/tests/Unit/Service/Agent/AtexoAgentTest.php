<?php

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent;
use App\Security\PasswordEncoder\LegacySha1PasswordEncoder;
use App\Security\PasswordEncoder\LegacySha256PasswordEncoder;
use App\Security\PasswordEncoder\SodiumPepperEncoder;
use App\Service\Agent\AgentServiceMetierService;
use App\Service\AtexoAgent;
use App\Service\PradoPasswordEncoder;
use App\Service\SocleHabilitationAgentService;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\SodiumPasswordEncoder;

class AtexoAgentTest extends TestCase
{
    public function testUpdateNewAgentPasswordByArgon2()
    {
        $container = $this->createMock(ContainerInterface::class);
        $socleHabilitationAgentService = $this->createMock(SocleHabilitationAgentService::class);
        $agentServiceMetierService = $this->createMock(AgentServiceMetierService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $sodiumEncoder = new SodiumPepperEncoder(
            $parameterBag,
            new LegacySha1PasswordEncoder(),
            new LegacySha256PasswordEncoder('true', 'a269BkoILM@6973AZuytrF!Psxe?8sqO')
        );
        $serviceAgent = new AtexoAgent(
            $container,
            $socleHabilitationAgentService,
            $agentServiceMetierService,
            $sodiumEncoder
        );
        $agent = new Agent();
        $agent = $serviceAgent->updateNewAgentPasswordByArgon2($agent, 'password');

        $this->assertEquals('ARGON2', $agent->getTypeHash());
        $this->assertStringContainsString('argon', $agent->getPassword());
    }
}
