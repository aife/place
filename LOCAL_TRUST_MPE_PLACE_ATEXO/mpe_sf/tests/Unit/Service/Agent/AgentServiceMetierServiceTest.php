<?php

namespace Tests\Unit\Service\Agent;

use App\Entity\Agent\AgentServiceMetier;
use App\Service\Agent\AgentServiceMetierService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AgentServiceMetierServiceTest extends TestCase
{
    /**
     * Création d'un service metier pour un agent.
     */
    public function testCreateForAdmin()
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);

        $agentServiceMetierService = new AgentServiceMetierService($container, $em);
        $agentServiceMetier = $agentServiceMetierService->createForAdmin(93, 2, 50);

        $this->assertTrue($agentServiceMetier instanceof AgentServiceMetier);
        $this->assertEquals(93, $agentServiceMetier->getIdAgent());
        $this->assertEquals(50, $agentServiceMetier->getIdProfilService());
        $this->assertEquals(2, $agentServiceMetier->getIdServiceMetier());
    }
}
