<?php

namespace Tests\Unit\Service\Authorization;

use stdClass;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Service\AtexoConfiguration;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\SearchAgent;
use App\Service\WebservicesMpeConsultations;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Security\Core\Security;

/**
 * Class AuthorizationAgentTest.
 */
class AuthorizationAgentTest extends TestCase
{
    /**
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function testPerimetreAgentNoAuth()
    {
        $em = $this->createMock(EntityManager::class);
        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $container = $this->createMock(ContainerInterface::class);
        $security = $this->createMock(Security::class);

        $perimetreAgent = new AuthorizationAgent(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
            $this->createMock(WebservicesMpeConsultations::class)
        );
        $auth = $perimetreAgent->isAuthorized(12345);

        $this->assertFalse($auth);
    }

    /**
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function testPerimetreAgentAuth()
    {
        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['idAgent' => 1])
            ->willReturn($habilitationAgent);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryAgent, $repositoryHabilitationAgent);

        $container = $this->createMock(ContainerInterface::class);
        $security = $this->createMock(Security::class);
        $security->method('getUser')->willReturn($agent);
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration);

        $apiResponse = Response::HTTP_OK;

        $obj3 = new stdClass();

        $obj3->{'libelle'} = 'Service';
        $obj3->{'total'} = '5';
        $obj3->{'statutCalcule'} = '4';

        $webservicesMpeConsultations = $this->createMock(WebservicesMpeConsultations::class);

        $webservicesMpeConsultations
            ->expects($this->once())
            ->method('getContent')
            ->willReturn($apiResponse)
            ;

        $perimetreAgent = new AuthorizationAgent(
            $em,
            $searchAgent,
            $session,
            $container,
            $security,
           $webservicesMpeConsultations,
        );

        $this->assertTrue($perimetreAgent->isAuthorized(12345));
    }
}
