<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\WebServices;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Entity\TypeProcedureDume;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Repository\TypeProcedureDumeRepository;
use App\Service\AtexoConfiguration;
use App\Service\LotService;
use App\Service\Procedure\GroupBuyers;
use App\Service\Procedure\ValeurEstimeeRule;
use App\Service\WebServices\ApiEndpointLibrary;
use App\Service\WebServices\DuplicationConstraintService;
use App\Service\WebServices\ReferenceUtilisateurGenerator;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class DuplicationConstraintServiceTest extends TestCase
{
    public function testKeysToRemoveWithProcedureEquivalenceReturnsAdditionalKeysToRemove()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $consultation->setOrganisme(new Organisme())->setIdTypeProcedureOrg(2);

        $typeProcedureOrganisme = (new TypeProcedureOrganisme())
            ->setIdTypeProcedure(42)
        ;

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($typeProcedureOrganisme)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setEnvOffreTypeMultiple('-1');
        $procedureEquivalence->setEnvOffreTypeUnique('-1');

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($procedureEquivalence)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );
        $result = $duplicationConstraintService->keysToRemove($consultation);

        $expectedResult = [
            'alloti',
            'donneePubliciteObligatoire',
            'dume',
            'typeProcedureDume',
            'dumeSimplifie',
            'valeurEstimee',
            'procedureOuverte',
            'typeProcedureDume',
            'dume',
            'donneeComplementaireObligatoire',
            'attestationConsultation'
        ];
        self::assertSame($expectedResult, $result);
    }

    public function testKeysToRemoveWithoutProcedureEquivalenceReturnsEmptyArray()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $consultation->setOrganisme(new Organisme())->setIdTypeProcedureOrg(2);

        $typeProcedureOrganisme = (new TypeProcedureOrganisme())
            ->setIdTypeProcedure(42)
        ;

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($typeProcedureOrganisme)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );

        $result = $duplicationConstraintService->keysToRemove($consultation);

        self::assertEmpty($result);
    }

    public function testGetKeysDumeWithoutProcedureEquivalenceReturnsEmptyArray()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $consultation
            ->setOrganisme(new Organisme())
            ->setIdTypeProcedureOrg(2)
            ->setDumeDemande(true)
            ->setTypeProcedureDume(42)
            ->setTypeFormulaireDume(1)
        ;

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );
        $result = $duplicationConstraintService->getKeysDume($consultation);

        self::assertEmpty($result);
    }

    public function testGetKeysDumeWithoutTypeProcedureDumeReturnsKeysToAddWithoutTypeProcedureDume()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $consultation
            ->setTypeProcedureDume(1)
            ->setDumeDemande(true)
            ->setTypeFormulaireDume(1)
        ;

        $entityManagerMock
            ->method('getRepository')
            ->willReturn($this->createMock(TypeProcedureDumeRepository::class))
        ;

        $entityManagerMock
            ->getRepository(TypeProcedureDume::class)
            ->method('find')
            ->willReturn(null)
        ;

        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setDumeDemande('+');

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($procedureEquivalence)
        ;

        $atexoConfiguration
            ->expects(self::once())
            ->method('isModuleEnabled')
            ->willReturn(true)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );

        $result = $duplicationConstraintService->getKeysDume($consultation);

        $expectedResult = [
            ApiEndpointLibrary::KEY_DUME_DEMANDE => true,
            ApiEndpointLibrary::KEY_DUME_SIMPLIFIE => true,
        ];

        self::assertSame($expectedResult, $result);
    }

    public function testGetKeysDumeWithoutDumeDemandeReturnsEmptyArray()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $procedureEquivalence = new ProcedureEquivalence();

        $typeProcedureOrganisme = (new TypeProcedureOrganisme())->setIdTypeProcedure(42);

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($typeProcedureOrganisme)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($procedureEquivalence)
        ;

        $atexoConfiguration
            ->expects(self::once())
            ->method('isModuleEnabled')
            ->willReturn(true)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );

        $result = $duplicationConstraintService->getKeysDume($consultation);

        $expectedResult = [
            ApiEndpointLibrary::KEY_DUME_SIMPLIFIE => false,
        ];

        self::assertSame($result, $expectedResult);
    }

    public function testGetKeyCpvWithProcedureEquivalenceAndCodeCpvObligatoireReturnsKeysToAdd()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $consultation->setCodeCpv1('cpv_1');
        $consultation->setCodeCpv2('#cpv2#cpv3#');

        $lotServiceMock
            ->expects(self::once())
            ->method('getExplodedCodeCpv')
            ->willReturn(['cpv2', 'cpv3'])
        ;

        $typeProcedureOrganisme = (new TypeProcedureOrganisme())->setIdTypeProcedure(42);

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($typeProcedureOrganisme)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setCodeCpvObligatoire('1');

        $procedureEquivalenceRepositoryMock
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn($procedureEquivalence)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );

        $result = $duplicationConstraintService->getKeyCpv($consultation);

        $expectedResult = [
            ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL => 'cpv_1',
            ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE1 => 'cpv2',
            ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE2 => 'cpv3',
            ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE3 => null,
        ];

        self::assertSame($expectedResult, $result);
    }

    public function testGetKeyCpvWithoutProcedureEquivalenceReturnsEmptyArray()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalenceRepositoryMock
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator
        );
        $result = $duplicationConstraintService->getKeyCpv($consultation);

        self::assertEmpty($result);
    }

    public function testGetKeyCpvWithoutCodeCpvObligatoireReturnsEmptyArray()
    {
        $entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $iriConverter = $this->createMock(IriConverterInterface::class);
        $procedureEquivalenceRepositoryMock = $this->createMock(ProcedureEquivalenceRepository::class);
        $lotServiceMock = $this->createMock(LotService::class);
        $atexoConfiguration = $this->createMock(AtexoConfiguration::class);
        $valeurEstimeeRule = $this->createMock(ValeurEstimeeRule::class);
        $groupementRule = $this->createMock(GroupBuyers::class);
        $procedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $referenceUtilisateurGenerator = $this->createMock(ReferenceUtilisateurGenerator::class);

        $consultation = new Consultation();
        $procedureEquivalence = new ProcedureEquivalence();

        $typeProcedureOrganismeRepository = $this->createMock(TypeProcedureOrganismeRepository::class);
        $typeProcedureOrganismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $entityManagerMock
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($typeProcedureOrganismeRepository)
        ;

        $procedureEquivalenceRepositoryMock
            ->method('findOneBy')
            ->willReturn($procedureEquivalence)
        ;

        $duplicationConstraintService = new DuplicationConstraintService(
            $entityManagerMock,
            $iriConverter,
            $procedureEquivalenceRepositoryMock,
            $lotServiceMock,
            $atexoConfiguration,
            $valeurEstimeeRule,
            $groupementRule,
            $procedureOrganismeRepository,
            $referenceUtilisateurGenerator,
        );

        $result = $duplicationConstraintService->getKeyCpv($consultation);

        $expectedResult = [
            ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL => null,
        ];

        self::assertSame($result, $expectedResult);
    }
}
