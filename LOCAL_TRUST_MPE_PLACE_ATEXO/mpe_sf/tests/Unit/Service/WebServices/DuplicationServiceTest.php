<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\WebServices;

use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Repository\ContratTitulaireRepository;
use App\Service\ClausesService;
use App\Service\Consultation\CommentService;
use App\Service\WebServices\DuplicationConstraintService;
use App\Service\WebServices\DuplicationService;
use App\Service\WebServices\FormePrixService;
use App\Service\WebServices\WebServiceFactory;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DuplicationServiceTest extends TestCase
{
    public function testGetKeysContratInfo()
    {
        $entity = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $webServiceFactory = $this->createMock(WebServiceFactory::class);
        $clausesService = $this->createMock(ClausesService::class);
        $duplicationConstraint = $this->createMock(DuplicationConstraintService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $commentService = $this->createMock(CommentService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $formePrixService = $this->createMock(FormePrixService::class);

        $consultation = new Consultation();
        $consultation->setIdContrat(32);

        $contrat = new ContratTitulaire();

        $contratTitulaireRepository = $this->createMock(ContratTitulaireRepository::class);
        $contratTitulaireRepository
            ->expects(self::once())
            ->method('find')
            ->with(32)
            ->willReturn($contrat)
        ;

        $contratTitulaireRepository
            ->expects(self::once())
            ->method('findBy')
            ->with(['idContratMulti' => 32])
            ->willReturn([])
        ;

        $entity
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [ContratTitulaire::class],
                [ContratTitulaire::class],
            )
            ->willReturnOnConsecutiveCalls(
                $contratTitulaireRepository,
                $contratTitulaireRepository,
            )
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Duplication consultation : Début duplication Contrat Infos id: 32"],
                ["Duplication consultation : Fin duplication Contrat Infos id: 32"],
            )
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')
            ->willReturn(false)
        ;

        $translator
            ->expects(self::once())
            ->method('trans')
            ->with('CONTRACTANT_S')
            ->willReturn('Contractant(s): ')
        ;

        $commentService
            ->expects(self::once())
            ->method('getCommentByContrat')
            ->with($contrat)
            ->willReturn('Atexo (75015 - Paris)')
        ;

        $duplicationService = new DuplicationService(
            $entity,
            $logger,
            $webServiceFactory,
            $clausesService,
            $duplicationConstraint,
            $parameterBag,
            $commentService,
            $translator,
            $formePrixService
        );

        $result = $duplicationService->getKeysContratInfo($consultation);
        $expectedResult = [
            "idContratTitulaire" => 32,
            "commentaireInterne" => "Contractant(s): \nAtexo (75015 - Paris)",
        ];

        self::assertSame($expectedResult, $result);
    }

    public function testGetKeysContratInfoMultiContracts()
    {
        $entity = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $webServiceFactory = $this->createMock(WebServiceFactory::class);
        $clausesService = $this->createMock(ClausesService::class);
        $duplicationConstraint = $this->createMock(DuplicationConstraintService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $commentService = $this->createMock(CommentService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $formePrixService = $this->createMock(FormePrixService::class);

        $consultation = new Consultation();
        $consultation->setIdContrat(32);

        $contrat = new ContratTitulaire();

        $contratTitulaireRepository = $this->createMock(ContratTitulaireRepository::class);
        $contratTitulaireRepository
            ->expects(self::once())
            ->method('find')
            ->with(32)
            ->willReturn($contrat)
        ;

        $contratTitulaireRepository
            ->expects(self::once())
            ->method('findBy')
            ->with(['idContratMulti' => 32])
            ->willReturn(
                [
                    new ContratTitulaire(),
                    new ContratTitulaire(),
                    new ContratTitulaire()
                ]
            )
        ;

        $entity
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive(
                [ContratTitulaire::class],
                [ContratTitulaire::class],
            )
            ->willReturnOnConsecutiveCalls(
                $contratTitulaireRepository,
                $contratTitulaireRepository,
            )
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Duplication consultation : Début duplication Contrat Infos id: 32"],
                ["Duplication consultation : Fin duplication Contrat Infos id: 32"],
            )
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')
            ->willReturn(false)
        ;

        $translator
            ->expects(self::once())
            ->method('trans')
            ->with('CONTRACTANT_S')
            ->willReturn('Contractant(s): ')
        ;

        $commentService
            ->expects(self::exactly(3))
            ->method('getCommentByContrat')
            ->withConsecutive(
                [$contrat],
                [$contrat],
                [$contrat],
            )
            ->willReturnOnConsecutiveCalls(
                "Atexo (75015 - Paris)",
                "\nFnac (93150 - Blanc-Mesnil)",
                "\nLeroyMerlin (92564 - Le Myséricordieux)",
            )
        ;

        $duplicationService = new DuplicationService(
            $entity,
            $logger,
            $webServiceFactory,
            $clausesService,
            $duplicationConstraint,
            $parameterBag,
            $commentService,
            $translator,
            $formePrixService
        );

        $result = $duplicationService->getKeysContratInfo($consultation);
        $expectedResult = [
            "idContratTitulaire" => 32,
            "commentaireInterne" => sprintf(
                "%s%s",
                "Contractant(s): \nAtexo (75015 - Paris)\nFnac (93150 - Blanc-Mesnil)",
                "\nLeroyMerlin (92564 - Le Myséricordieux)"
            ),
        ];

        self::assertSame($expectedResult, $result);
    }

    public function testGetKeysContratInfoWithNoContract()
    {
        $entity = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $webServiceFactory = $this->createMock(WebServiceFactory::class);
        $clausesService = $this->createMock(ClausesService::class);
        $duplicationConstraint = $this->createMock(DuplicationConstraintService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $commentService = $this->createMock(CommentService::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $formePrixService = $this->createMock(FormePrixService::class);

        $consultation = new Consultation();
        $consultation->setIdContrat(32);

        $contratTitulaireRepository = $this->createMock(ContratTitulaireRepository::class);
        $contratTitulaireRepository
            ->expects(self::once())
            ->method('find')
            ->with(32)
            ->willReturn(null)
        ;

        $entity
            ->expects(self::once())
            ->method('getRepository')
            ->with(ContratTitulaire::class)
            ->willReturn($contratTitulaireRepository)
        ;

        $logger
            ->expects(self::exactly(2))
            ->method('info')
            ->withConsecutive(
                ["Duplication consultation : Début duplication Contrat Infos id: 32"],
                ["Duplication consultation : Fin duplication Contrat Infos id: 32"],
            )
        ;

        $duplicationService = new DuplicationService(
            $entity,
            $logger,
            $webServiceFactory,
            $clausesService,
            $duplicationConstraint,
            $parameterBag,
            $commentService,
            $translator,
            $formePrixService
        );

        self::assertSame([], $duplicationService->getKeysContratInfo($consultation));
    }
}
