<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\WebServices\Organisme;

use App\Entity\Agent;
use App\Service\WebServices\Organisme\WebservicesOrgansimes;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebservicesOrgansimesTest extends TestCase
{
    private ParameterBagInterface $parameterBag;

    private JWTTokenManagerInterface $tokenManager;

    private Security $security;

    private LoggerInterface $logger;

    private HttpClientInterface $httpClient;

    private Agent $agent;

    public function setUp()
    {
        $this->httpClient = $this->createMock(HttpClientInterface::class);
        $this->logger = $this->createMock(LoggerInterface::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->security = $this->createMock(Security::class);
        $this->tokenManager = $this->createMock(JWTTokenManagerInterface::class);
        $this->agent = new Agent();
    }

    public function testGetOrganismes(): void
    {
        $reflectionClass = new \ReflectionClass(WebservicesOrgansimes::class);
        $searchOrganismes = $reflectionClass->getMethod('searchOrganismes');
        $searchOrganismes->setAccessible(true);

        $client = new MockHttpClient([new MockResponse(
            '{"hydra:member":[{"sigle":"MTESTATEXO","denomination":"Ministère de test","acronyme":"a7z"}]}',
            ['http_code' => 200]
        )]);

        $this->parameterBag
            ->expects(self::any())
            ->method('get')
            ->willReturn('https://url-mpe.com');

        $this->security
            ->expects(self::once())
            ->method('getUser')
            ->willReturn($this->agent);

        $this->tokenManager
            ->expects(self::once())
            ->method('create')
            ->with($this->agent);

        $webservicesOragnismes = new WebservicesOrgansimes(
            $client,
            $this->logger,
            $this->parameterBag,
            $this->security,
            $this->tokenManager
        );

        $wsResponse = $searchOrganismes->invokeArgs($webservicesOragnismes, []);
        $result = json_decode($wsResponse);
        self::assertSame($result->{"hydra:member"}[0]->sigle, 'MTESTATEXO');
    }
}
