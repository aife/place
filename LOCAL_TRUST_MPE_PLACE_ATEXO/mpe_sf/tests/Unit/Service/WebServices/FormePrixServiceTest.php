<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\WebServices;

use App\Entity\FormePrix;
use App\Entity\FormePrixHasRefTypePrix;
use App\Entity\FormePrixPfHasRefVariation;
use App\Entity\FormePrixPuHasRefVariation;
use App\Repository\FormePrixHasRefTypePrixRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use App\Service\WebServices\FormePrixService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class FormePrixServiceTest extends TestCase
{
    public function testDuplicateFormePrixPfHasRefVariation()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $formePrixHasRefTypePrixRepository = $this->createMock(FormePrixHasRefTypePrixRepository::class);
        $formePrixPfHasRefVariationRepository = $this->createMock(FormePrixPfHasRefVariationRepository::class);
        $formePrixPuHasRefVariationRepository = $this->createMock(FormePrixPuHasRefVariationRepository::class);

        $service = new FormePrixService(
            $entityManager,
            $formePrixHasRefTypePrixRepository,
            $formePrixPfHasRefVariationRepository,
            $formePrixPuHasRefVariationRepository
        );

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $formePrixPfHasRefVariationRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(new FormePrixPfHasRefVariation())
        ;

        $service->duplicateFormePrixPfHasRefVariation(1, 2);
    }

    public function testDuplicateFormePrixPuHasRefVariation()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $formePrixHasRefTypePrixRepository = $this->createMock(FormePrixHasRefTypePrixRepository::class);
        $formePrixPfHasRefVariationRepository = $this->createMock(FormePrixPfHasRefVariationRepository::class);
        $formePrixPuHasRefVariationRepository = $this->createMock(FormePrixPuHasRefVariationRepository::class);

        $service = new FormePrixService(
            $entityManager,
            $formePrixHasRefTypePrixRepository,
            $formePrixPfHasRefVariationRepository,
            $formePrixPuHasRefVariationRepository
        );

        $entityManager
            ->expects(self::once())
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $formePrixPuHasRefVariationRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(new FormePrixPuHasRefVariation())
        ;

        $service->duplicateFormePrixPuHasRefVariation(1, 2);
    }

    public function testDuplicateFormePrixVariationWithNull()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $formePrixHasRefTypePrixRepository = $this->createMock(FormePrixHasRefTypePrixRepository::class);
        $formePrixPfHasRefVariationRepository = $this->createMock(FormePrixPfHasRefVariationRepository::class);
        $formePrixPuHasRefVariationRepository = $this->createMock(FormePrixPuHasRefVariationRepository::class);

        $service = new FormePrixService(
            $entityManager,
            $formePrixHasRefTypePrixRepository,
            $formePrixPfHasRefVariationRepository,
            $formePrixPuHasRefVariationRepository
        );

        $entityManager
            ->expects($this->never())
            ->method('persist')
        ;

        $entityManager
            ->expects($this->never())
            ->method('flush')
        ;

        $formePrixPfHasRefVariationRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->willReturn(null)
        ;

        $service->duplicateFormePrixPfHasRefVariation(1, 2);
    }

    public function testDuplicateFormePrixHasRefTypePrix()
    {
        $entityManager = $this->createMock(EntityManagerInterface::class);
        $formePrixHasRefTypePrixRepository = $this->createMock(FormePrixHasRefTypePrixRepository::class);
        $formePrixPfHasRefVariationRepository = $this->createMock(FormePrixPfHasRefVariationRepository::class);
        $formePrixPuHasRefVariationRepository = $this->createMock(FormePrixPuHasRefVariationRepository::class);

        $service = new FormePrixService(
            $entityManager,
            $formePrixHasRefTypePrixRepository,
            $formePrixPfHasRefVariationRepository,
            $formePrixPuHasRefVariationRepository
        );

        $entityManager
            ->expects($this->exactly(2))
            ->method('persist')
        ;

        $entityManager
            ->expects(self::once())
            ->method('flush')
        ;

        $formePrixHasRefTypePrixRepository
            ->expects(self::once())
            ->method('findBy')
            ->willReturn([new FormePrixHasRefTypePrix(), new FormePrixHasRefTypePrix()])
        ;

        $service->duplicateFormePrixHasRefTypePrix(1, 2);
    }
}
