<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Entreprise;

use Exception;
use App\Entity\SocietesExclues;
use App\Repository\SocietesExcluesRepository;
use App\Service\Entreprise\SocietesExcluesService;
use App\Service\ExcelService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class SocietesExcluesServiceTest extends TestCase
{
    public function testGetDataFileSocietesExclues()
    {
        $mockRepo = $this->createMock(SocietesExcluesRepository::class);
        $mockRepo->expects($this->once())
            ->method('findAll')
            ->willReturn([new SocietesExclues(), new SocietesExclues()]);

        $mockEm = $this->createMock(EntityManagerInterface::class);
        $mockEm->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockRepo);

        $mockExcelService = $this->createMock(ExcelService::class);
        $mockLogger = $this->createMock(LoggerInterface::class);

        $service = new SocietesExcluesService($mockEm, $mockExcelService, $mockLogger);
        $result = $service->getDataForFileSocietesExclues();

        $this->assertIsArray($result);
        $this->assertArrayHasKey('headers', $result);
        $this->assertCount(7, $result['headers']);
        $this->assertArrayHasKey('informations', $result);
        $this->assertCount(2, $result);
    }

    public function testFileSocietesExclues()
    {
        $mockRepo = $this->createMock(SocietesExcluesRepository::class);
        $mockRepo->expects($this->once())
            ->method('findAll')
            ->willReturn([new SocietesExclues(), new SocietesExclues()]);

        $mockEm = $this->createMock(EntityManagerInterface::class);
        $mockEm->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockRepo);

        $mockExcelService = $this->createMock(ExcelService::class);
        $mockExcelService->expects($this->once())
            ->method('generateBasicExcelFile')
            ->willReturn(['dirname', 'basename', 'extension', 'filename']);

        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockLogger->expects($this->never())
            ->method('error');

        $service = new SocietesExcluesService($mockEm, $mockExcelService, $mockLogger);
        $result = $service->getFileSocietesExclues();

        $this->assertCount(4, $result);
    }

    public function testFileSocietesExcluesException()
    {
        $mockRepo = $this->createMock(SocietesExcluesRepository::class);
        $mockRepo->expects($this->once())
            ->method('findAll')
            ->willReturn([new SocietesExclues(), new SocietesExclues()]);

        $mockEm = $this->createMock(EntityManagerInterface::class);
        $mockEm->expects($this->once())
            ->method('getRepository')
            ->willReturn($mockRepo);

        $mockExcelService = $this->createMock(ExcelService::class);
        $mockExcelService->expects($this->once())
            ->method('generateBasicExcelFile')
            ->willThrowException(new Exception());

        $mockLogger = $this->createMock(LoggerInterface::class);
        $mockLogger->expects($this->once())
            ->method('error');

        $service = new SocietesExcluesService($mockEm, $mockExcelService, $mockLogger);
        $service->getFileSocietesExclues();
    }
}
