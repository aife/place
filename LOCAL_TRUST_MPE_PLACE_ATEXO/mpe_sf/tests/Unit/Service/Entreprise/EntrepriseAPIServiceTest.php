<?php

namespace Tests\Unit\Service\Entreprise;

use App\Entity\ConfigurationPlateforme;
use App\Repository\ConfigurationPlateformeRepository;
use App\Service\ApiGateway\ApiEntreprise;
use App\Service\Asynchronous\AsyncRequests;
use App\Service\Entreprise\EntrepriseAPIService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EntrepriseAPIServiceTest extends TestCase
{
    public function testGetEntreprise()
    {
        $entrepriseAPIService = $this->createPartialMock(
            EntrepriseAPIService::class,
            [
                'getEntrepriseIfExists'
            ]
        );

        $entrepriseTests = [
            'nom'                   => 'Wayne Corp',
            'siren'                 => '123456789',
            'codeApe'               => '123WC',
            'acronymePays'          => 'FR',
            'formeJuridique'        => 'Lorem ipsum',
            'categorieEntreprise'   => 'PME',
            'etablissements'         => [
                [
                    'codeEtablissement'         => '00001',
                    'estSiege'                  => true,
                    'inscritAnnuaireDefense'    => false,
                    'adresse'                   => '1 rue de Gotham',
                    'codePostal'                => '75001',
                    'ville'                     => 'Paris'
                ]
            ]
        ];


        $entrepriseAPIService->expects($this->once())
            ->method('getEntrepriseIfExists')
            ->willReturn($entrepriseTests);

        $entrepriseContent = $entrepriseAPIService->getEntreprise('123456789', '00001');

        $this->assertIsArray($entrepriseContent);
        $this->assertEquals($entrepriseTests, $entrepriseContent);
    }

    public function testGetEntrepriseWithEstablishmentCall()
    {
        $entrepriseAPIService = $this->createPartialMock(
            EntrepriseAPIService::class,
            [
                'getEntrepriseIfExists', 'callEstablishment'
            ]
        );

        $entrepriseTests = [
            'nom'                   => 'Wayne Corp',
            'siren'                 => '123456789',
            'codeApe'               => '123WC',
            'acronymePays'          => 'FR',
            'formeJuridique'        => 'Lorem ipsum',
            'categorieEntreprise'   => 'PME',
            'etablissements'         => [
                [
                    'codeEtablissement'         => '00001',
                    'estSiege'                  => true,
                    'inscritAnnuaireDefense'    => false,
                    'adresse'                   => '1 rue de Gotham',
                    'codePostal'                => '75001',
                    'ville'                     => 'Paris'
                ]
            ]
        ];

        $etablishmentFromApiTests = [
            'data' => [
                'siret' => '12345678900002',
                'siege_social'  => false,
                'adresse'   => [
                    'numero_voie'    => '2',
                    'type_voie'    => 'rue',
                    'libelle_voie'    => 'de Gotham',
                    'code_postal'   => '75001',
                    'libelle_commune'      => 'Paris'
                ]
            ]
        ];

        $entrepriseAPIService->expects($this->once())
            ->method('getEntrepriseIfExists')
            ->willReturn($entrepriseTests);
        $entrepriseAPIService->expects($this->once())
            ->method('callEstablishment')
            ->willReturn($etablishmentFromApiTests);

        $entrepriseContent = $entrepriseAPIService->getEntreprise('123456789', '00002');
        $this->assertIsArray($entrepriseContent);

        $entrepriseTests['etablissements'][] = [
            'codeEtablissement'         => '00002',
            'estSiege'                  => false,
            'inscritAnnuaireDefense'    => false,
            'adresse'                   => '2 rue de Gotham',
            'codePostal'                => '75001',
            'ville'                     => 'Paris'
        ];

        $this->assertEquals($entrepriseTests, $entrepriseContent);
    }

    public function testGetEntrepriseWithEntrepriseCall()
    {
        $entrepriseAPIService = $this->createPartialMock(
            EntrepriseAPIService::class,
            [
                'getEntrepriseIfExists', 'callEstablishment', 'callEntreprise'
            ]
        );

        $entrepriseTests = [
            'nom'                   => 'Wayne Corp',
            'siren'                 => '123456789',
            'codeApe'               => '123WC',
            'acronymePays'          => 'FR',
            'formeJuridique'        => 'Lorem ipsum',
            'categorieEntreprise'   => 'PME',
            'etablissements'         => [
                [
                    'codeEtablissement'         => '00001',
                    'estSiege'                  => true,
                    'inscritAnnuaireDefense'    => false,
                    'adresse'                   => '3 rue de Gotham',
                    'codePostal'                => '75001',
                    'ville'                     => 'Paris'
                ]
            ]
        ];

        $entrepriseFromApiTests = [
            'data' => [
                'siren'                 => '123456789',
                'activite_principale'   => [
                    'code' => '123WC'
                ],
                'type'                  => 'personne_morale',
                'personne_morale_attributs'   => [
                    'raison_sociale' => 'Wayne Corp'
                ],
                'forme_juridique'       => ['libelle' => 'Lorem ipsum'],
                'categorie_entreprise'  => 'PME',
                'unite_legale'  => [
                    'siret_siege_social' => '12345678900001'
                ],
                'siret'                 => '12345678900001',
                'pays_implantation'     => ['code' => 'FR'],
                'adresse'               => [
                    'code_postal'           => '75001',
                    'libelle_commune'       => 'Paris',
                    'acheminement_postal'   => ['l4' => '3 rue de Gotham'],
                    'code_pays_etranger'   => 'FR',
                ]
            ],
        ];

        $entrepriseAPIService->expects($this->once())
            ->method('getEntrepriseIfExists')
            ->willReturn([]);
        $entrepriseAPIService->expects($this->never())
            ->method('callEstablishment');
        $entrepriseAPIService->expects($this->once())
            ->method('callEntreprise')
            ->willReturn($entrepriseFromApiTests);

        $entrepriseContent = $entrepriseAPIService->getEntreprise('123456789', null);

        $this->assertIsArray($entrepriseContent);
        $this->assertEquals($entrepriseTests, $entrepriseContent);
    }

    public function testGetWebServiceQueryParams(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $httpClient = $this->createMock(HttpClientInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $asyncRequests = $this->createMock(AsyncRequests::class);
        $apiEntreprise = $this->createMock(ApiEntreprise::class);

        $entrepriseApiService = new EntrepriseAPIService(
            $parameterBag,
            $httpClient,
            $em,
            $logger,
            $asyncRequests,
            $apiEntreprise,
        );

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('API_ENTREPRISE_SIRET_CLIENT')
            ->willReturn('123456789')
        ;

        $expectedResult = [
            'context'   => 'MPS',
            'object'    => 'Synchronisation donnees Entreprise, Etablissement ou Exercices',
            'recipient' => '123456789',
        ];

        $result = $entrepriseApiService->getWebServiceQueryParams();

        self::assertSame($result, $expectedResult);
    }
}
