<?php

namespace Tests\Unit\Service\Entreprise;

use stdClass;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use App\Service\Entreprise\EntrepriseVerificationService;
use App\Service\Messagerie\MessagerieService;
use App\Utils\AtexoConsultationCriteriaVo;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

class EntrepriseVerificationTest extends AtexoTestCase
{
    /**
     * @dataProvider getDataProvider
     */
    public function testParamsDepot($data)
    {
        $logMock = $this->createMock(Logger::class);
        $requestStackMock = $this->createMock(RequestStack::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        $atexoUtil = new AtexoUtil(
            $this->createMock(ContainerInterface::class),
            $this->createMock(SessionInterface::class),
            $this->createMock(TranslatorInterface::class),
            $logMock,
            $requestStackMock,
            $parameterBagMock
        );

        $entreprise = new Entreprise();
        $entreprise->setSiren($data->siren);
        $entreprise->setSaisieManuelle($data->saisieManuelle);

        $etablissement = new Etablissement();
        $etablissement->setCodeEtablissement($data->codeEtablissement);
        $etablissement->setEtatAdministratif($data->etatAdministratif);
        $etablissement->setSaisieManuelle($data->etablissementSaisieManuelle);

        $user = new Inscrit();
        $user->setEntreprise($entreprise);
        $user->setEtablissement($etablissement);

        $session = $this->session = new Session(new MockArraySessionStorage());
        $translator = $this->createMock(TranslatorInterface::class);
        $tokenStorage = new TokenStorage();
        $tokenStorage->setToken(new UsernamePasswordToken($user, null, 'firewall', []));
        $atexoConfigurationMock = $this->createMock(AtexoConfiguration::class);

        $atexoConfigurationMock->expects($this->any())
            ->method('hasConfigPlateforme')
            ->will($this->returnValue(true));

        $service = new EntrepriseVerificationService(
            $atexoUtil,
            $session,
            $translator,
            $tokenStorage,
            $atexoConfigurationMock
        );
        $params = $service->getParamsDepot();
        $actual = '';
        if (isset($params['msg']) && isset($params['typeAlert'])) {
            $actual = $params['msg'].$params['typeAlert'];
        }
        $expected = $data->msg.$data->typeAlert;
        $this->assertEquals($actual, $expected);
    }

    public function getDataProvider()
    {
        $data = new stdClass();

        //Régle de gestion 17: Pas SIREN
        $data->siren = '  ';
        $data->codeEtablissement = '123456';
        $data->saisieManuelle = false;
        $data->etablissementSaisieManuelle = true;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF;
        $data->msg = 'ENTREPRISE_VERIFICATION_SIRET_VIDE_INFO';
        $data->typeAlert = 'info';

        yield [$data];

        //Regle de gestion 18: AVEC SIREN SIRET NON CONFORME
        $data->siren = '123456';
        $data->codeEtablissement = '123456';
        $data->saisieManuelle = false;
        $data->etablissementSaisieManuelle = true;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF;
        $data->msg = 'ENTREPRISE_VERIFICATION_SIRET_ERREUR';
        $data->typeAlert = 'danger';

        yield [$data];

        //Régle de gestion 19 Entreprise NON Synchronisé
        $data->siren = '440909562';
        $data->codeEtablissement = '00033';
        $data->saisieManuelle = true;
        $data->etablissementSaisieManuelle = true;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF;
        $data->msg = 'ENTREPRISE_NON_SYNCHRO';
        $data->typeAlert = 'info';

        yield [$data];

        //Régle de gestion 20 Entreprise Synchronisé, Siret Non Synchronisé
        $data->siren = '440909562';
        $data->codeEtablissement = '00033';
        $data->saisieManuelle = false;
        $data->etablissementSaisieManuelle = true;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF;
        $data->msg = 'ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO';
        $data->typeAlert = 'warning';

        yield [$data];

        //Régle de gestion 21 Etat Administratif fermé
        $data->siren = '440909562';
        $data->codeEtablissement = '00033';
        $data->saisieManuelle = false;
        $data->etablissementSaisieManuelle = false;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_FERME;
        $data->msg = 'ETABLISSEMENT_FERME';
        $data->typeAlert = 'warning';

        yield [$data];

        //Régle de gestion 21 Etat Administratif fermé
        $data->siren = '440909562';
        $data->codeEtablissement = '00033';
        $data->saisieManuelle = false;
        $data->etablissementSaisieManuelle = false;
        $data->etatAdministratif = EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF;
        $data->msg = 'ETABLISSEMENT_NON_FERME';
        $data->typeAlert = 'info';

        yield [$data];
    }
}
