<?php

namespace Tests\Unit\Service\Entreprise;

use DateTime;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\MessageAccueil;
use App\Repository\EntrepriseRepository;
use App\Repository\EtablissementRepository;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoUtil;
use App\Service\Entreprise\EntrepriseAPIService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AtexoEntrepriseTest extends TestCase
{
    /**
     * @dataProvider entrepriseProvider
     */
    public function testValidateInfoEntreprise($arrayEntreprise, $arrayErrors)
    {
        $atexoEntreprise = $this->createMock(AtexoEntreprise::class);
        $atexoEntreprise->expects($this->any())
            ->method('validateInfo')
            ->with($arrayEntreprise)
            ->willReturn($arrayErrors);

        $this->assertEquals($arrayErrors, $atexoEntreprise->validateInfo($arrayEntreprise));
    }

    public function entrepriseProvider()
    {
        return [
            // Entreprise Valide
            [[
                'id' => 0,
                'siren' => 383917102,
                'telephone' => null,
                'fax' => null,
                'siteInternet' => null,
                'effectif' => 0,
                'adresse' => [
                    'ville' => 'EVRY-COURCOURONNES',
                    'rue' => '52 BD DE L\'YERRES',
                    'codePostal' => 91000,
                    'pays' => 'France',
                    'acronymePays' => 'FR',
                ],
                'formeJuridique' => 'Société à responsabilité limitée (sans autre indication)',
                'codeAPE' => '4643Z',
                'libelleAPE' => null,
                'dateModification' => '2019-08-23T11:20:14+02:00',
                'capitalSocial' => 3195502,
                'raisonSociale' => 'BRITA WASSER FILTER SYSTEME FRANCE',
                'email' => 'entreprise@atexo.com',
                'etablisssements' => [
                    'etablissement' => [
                        ['id' => 31407,
                            'siret' => 38391710100058,
                            'idEntreprise' => 16669,
                            'siege' => 1,
                            'dateCreation' => '2019-08-23T11:23:04+02:00',
                            'dateModification' => '2019-08-23T11:23:04+02:00',
                            'adresse' => [
                                    'rue' => '52 BD DE L\'YERRES',
                                    'codePostal' => 91000,
                                    'ville' => 'Argenteuil',
                                    'pays' => 'France',
                                ],
                        ],
                    ],
                ],
            ], []],
            // Entreprise avec des données non valides
            [[
                'id' => 0,
                'siren' => 383917102333,
                'telephone' => null,
                'fax' => null,
                'siteInternet' => null,
                'effectif' => 0,
                'adresse' => [
                    'ville' => 'EVRY-COURCOURONNES',
                    'rue' => '52 BD DE L\'YERRES',
                    'codePostal' => 91000,
                    'pays' => 'France',
                    'acronymePays' => 'FR',
                ],
                'formeJuridique' => 'Société à responsabilité limitée (sans autre indication)',
                'codeAPE' => '4643Z',
                'libelleAPE' => null,
                'dateModification' => '2019-08-23T11:20:14+02:00',
                'capitalSocial' => 3195502,
                'raisonSociale' => 'BRITA WASSER FILTER SYSTEME FRANCE',
                'email' => 'email erroné',
                'etablisssements' => [
                    'etablissement' => [
                        ['id' => 31407,
                            'siret' => 38391710100058,
                            'idEntreprise' => 16669,
                            'siege' => 1,
                            'dateCreation' => '2019-08-23T11:23:04+02:00',
                            'dateModification' => '2019-08-23T11:23:04+02:00',
                            'adresse' => [
                                    'rue' => '52 BD DE L\'YERRES',
                                    'codePostal' => 91000,
                                    'ville' => 'Argenteuil',
                                    'pays' => 'France',
                                ],
                        ],
                    ],
                ],
            ], ['Siren non valide', 'Email non valide']],
        ];
    }

    public function testSerializeEntreprise()
    {
        $arrayEntreprise = [
            'id' => 0,
            'siren' => 383917102,
            'telephone' => null,
            'fax' => null,
            'siteInternet' => null,
            'effectif' => 0,
            'adresse' => [
                'ville' => 'EVRY-COURCOURONNES',
                'rue' => '52 BD DE L\'YERRES',
                'codePostal' => 91000,
                'pays' => 'France',
                'acronymePays' => 'FR',
            ],
            'formeJuridique' => 'Société à responsabilité limitée (sans autre indication)',
            'codeAPE' => '4643Z',
            'libelleAPE' => null,
            'dateModification' => '2019-08-23T11:20:14+02:00',
            'capitalSocial' => 3195502,
            'raisonSociale' => 'BRITA WASSER FILTER SYSTEME FRANCE',
            'email' => 'entreprise@atexo.com',
            'etablisssements' => [
                'etablissement' => [
                    ['id' => 31407,
                        'siret' => 38391710100058,
                        'idEntreprise' => 16669,
                        'siege' => 1,
                        'dateCreation' => '2019-08-23T11:23:04+02:00',
                        'dateModification' => '2019-08-23T11:23:04+02:00',
                        'adresse' => [
                                'rue' => '52 BD DE L\'YERRES',
                                'codePostal' => 91000,
                                'ville' => 'Argenteuil',
                                'pays' => 'France',
                            ],
                    ],
                ],
            ],
        ];

        $entreprise = new Entreprise();
        $entreprise
            ->setSiren(383917102)
            ->setEmail('entreprise@atexo.com')
            ->setEffectif(0)
            ->setAdresse('52 BD DE L\'YERRES')
            ->setFormejuridique('Société à responsabilité limitée (sans autre indication)')
            ->setCodeape('4643Z')
            ->setCapitalSocial(3195502)
            ->setRaisonSociale('BRITA WASSER FILTER SYSTEME FRANCE');

        $etablissement = new Etablissement();
        $etablissement->setSiret(38391710100058);
        $etablissement->setEstSiege(1);
        $etablissement->setDateCreation(new DateTime('2019-08-23T11:23:04+02:00'));
        $etablissement->setDateModification(new DateTime('2019-08-23T11:23:04+02:00'));
        $etablissement->setCodePostal(91000);
        $etablissement->setPays('France');
        $etablissement->setVille('Argenteuil');
        $etablissement->setAdresse('52 BD DE L\'YERRES');
        $entreprise->addEtablissement($etablissement);

        $atexoEntreprise = $this->createMock(AtexoEntreprise::class);
        $atexoEntreprise->expects($this->any())
            ->method('serializeEntreprise')
            ->with($arrayEntreprise)
            ->willReturn($entreprise);

        $entrepriseEntity = $atexoEntreprise->serializeEntreprise($arrayEntreprise);

        $this->assertInstanceOf(Entreprise::class, $entrepriseEntity);
        $this->assertEquals($entrepriseEntity->getSiren(), $entreprise->getSiren());
        $this->assertEquals($entrepriseEntity->getEmail(), $entreprise->getEmail());
        $this->assertEquals($entrepriseEntity->getEffectif(), $entreprise->getEffectif());
        $this->assertEquals($entrepriseEntity->getAdresse(), $entreprise->getAdresse());
        $this->assertEquals($entrepriseEntity->getFormejuridique(), $entreprise->getFormejuridique());
        $this->assertEquals($entrepriseEntity->getCodeape(), $entreprise->getCodeape());
        $this->assertEquals($entrepriseEntity->getCapitalSocial(), $entreprise->getCapitalSocial());
        $this->assertEquals($entrepriseEntity->getRaisonSociale(), $entreprise->getRaisonSociale());
        $this->assertEquals($entrepriseEntity->getEtablissements(), $entreprise->getEtablissements());
    }

    /**
     * @dataProvider entrepriseModeProvider
     */
    public function testCreateOrUpdateEntity($mode, $upsertedEntreprise)
    {
        $stub = $this->createMock('DateTime');
        $stub->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:03 11:06:07'));

        $entreprise = new Entreprise();
        $entreprise
            ->setSiren(383917102)
            ->setEmail('entreprise@atexo.com')
            ->setEffectif(0)
            ->setAdresse('52 BD DE L\'YERRES')
            ->setFormejuridique('Société à responsabilité limitée (sans autre indication)')
            ->setCodeape('4643Z')
            ->setCapitalSocial(3195502)
            ->setRaisonSociale('BRITA WASSER FILTER SYSTEME FRANCE');

        $date = $stub->format('Y:m:d H:i:s');
        $entreprise->setDateCreation($date);

        $atexoEntreprise = $this->createMock(AtexoEntreprise::class);
        $atexoEntreprise->expects($this->any())
            ->method('createOrUpdateEntity')
            ->with($entreprise, $mode)
            ->willReturn($upsertedEntreprise);
        $entrepriseUpsertedEntity = $atexoEntreprise->createOrUpdateEntity($entreprise, $mode);

        $this->assertEquals($entrepriseUpsertedEntity->getSiren(), $entreprise->getSiren());
        $this->assertEquals($entrepriseUpsertedEntity->getEmail(), $entreprise->getEmail());
        $this->assertEquals($entrepriseUpsertedEntity->getEffectif(), $entreprise->getEffectif());
        $this->assertEquals($entrepriseUpsertedEntity->getAdresse(), $entreprise->getAdresse());
        $this->assertEquals($entrepriseUpsertedEntity->getFormejuridique(), $entreprise->getFormejuridique());
        $this->assertEquals($entrepriseUpsertedEntity->getCodeape(), $entreprise->getCodeape());
        $this->assertEquals($entrepriseUpsertedEntity->getCapitalSocial(), $entreprise->getCapitalSocial());
        $this->assertEquals($entrepriseUpsertedEntity->getRaisonSociale(), $entreprise->getRaisonSociale());

        $createdDate = new DateTime($entreprise->getDateCreation());
        $createdDateForUpsertedEntity = new DateTime($entrepriseUpsertedEntity->getDateCreation());
        $this->assertEquals($createdDate->format('Y:m:d'), $createdDateForUpsertedEntity->format('Y:m:d'));
    }

    public function entrepriseModeProvider()
    {
        $dateTime = $this->createMock('DateTime');
        $dateTime->expects($this->any())
            ->method('format')
            ->will($this->returnValue('2019:09:03 11:06:07'));

        $upsertedEntreprise = new Entreprise();
        $upsertedEntreprise
            ->setSiren(383917102)
            ->setEmail('entreprise@atexo.com')
            ->setEffectif(0)
            ->setAdresse('52 BD DE L\'YERRES')
            ->setFormejuridique('Société à responsabilité limitée (sans autre indication)')
            ->setCodeape('4643Z')
            ->setCapitalSocial(3195502)
            ->setRaisonSociale('BRITA WASSER FILTER SYSTEME FRANCE');

        return [
            ['create', $upsertedEntreprise->setDateCreation($dateTime->format('Y:m:d H:i:s'))],
            ['update', $upsertedEntreprise],
        ];
    }

    public function testCreateFromInscription()
    {
        $entrepriseBody = [
            "entreprise" => [
                "raisonSociale" => "Kennedy Perk",
                "formejuridique" => "SARL",
                "paysenregistrement" => "FRANCE",
                "categorieEntreprise" => "PME",
                "sirenEtranger" => "fr2"
            ],
            "etablissements" => [
                [
                    "adresse" => "3 bd des bois",
                    "adresse2" => "Apt 131",
                    "codePostal" => "75001",
                    "ville" => "Paris",
                    "pays" => "France",
                    "estSiege" => true,
                    "selected" => true
                ]
            ]
        ];

        $etablissement = new Etablissement();
        $etablissement->setAdresse($entrepriseBody['etablissements'][0]['adresse']);
        $etablissement->setAdresse2($entrepriseBody['etablissements'][0]['adresse2']);
        $etablissement->setCodePostal($entrepriseBody['etablissements'][0]['codePostal']);
        $etablissement->setVille($entrepriseBody['etablissements'][0]['ville']);
        $etablissement->setPays($entrepriseBody['etablissements'][0]['pays']);
        $etablissement->setEstSiege($entrepriseBody['etablissements'][0]['estSiege']);

        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $atexoEtablissement->expects($this->once())
            ->method('createEntity')
            ->willReturn($etablissement);

        $entrepriseRepo = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($entrepriseRepo);

        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $atexoEtablissement,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class)
        );

        $return = $atexoEntreprise->createFromInscription($entrepriseBody);

        $this->assertEquals($entrepriseBody['entreprise']['raisonSociale'], $return['entreprise']->getRaisonSociale());
        $this->assertEquals(
            $entrepriseBody['entreprise']['formejuridique'],
            $return['entreprise']->getFormejuridique()
        );
        $this->assertEquals(
            $entrepriseBody['entreprise']['paysenregistrement'],
            $return['entreprise']->getPaysenregistrement()
        );
        $this->assertEquals(
            $entrepriseBody['entreprise']['categorieEntreprise'],
            $return['entreprise']->getCategorieEntreprise()
        );
        $this->assertEquals($entrepriseBody['entreprise']['sirenEtranger'], $return['entreprise']->getSirenetranger());
        $this->assertEquals($etablissement->getAdresse(), $return['establishments'][0]->getAdresse());
        $this->assertEquals($etablissement->getAdresse2(), $return['establishments'][0]->getAdresse2());
        $this->assertEquals($etablissement->getCodePostal(), $return['establishments'][0]->getCodePostal());
        $this->assertEquals($etablissement->getVille(), $return['establishments'][0]->getVille());
        $this->assertEquals($etablissement->getPays(), $return['establishments'][0]->getPays());
        $this->assertEquals($etablissement->getEstSiege(), $return['establishments'][0]->getEstSiege());
    }

    public function testCreateFromInscriptionWhenAlreadyRegistered()
    {
        $entrepriseBody = [
            "entreprise" => [
                "siren"     => "440909562"
            ]
        ];

        $entreprise = new Entreprise();
        $entreprise->setId(1);
        $entreprise->setRaisonSociale('Atexo');
        $entreprise->setFormejuridique('forme juridique');
        $entreprise->setPaysenregistrement('France');
        $entreprise->setCategorieEntreprise('PME');

        $etablissement = new Etablissement();
        $etablissement->setAdresse('3 bd des bois');
        $etablissement->setAdresse2('Apt 131');
        $etablissement->setCodePostal('75001');
        $etablissement->setVille('Paris');
        $etablissement->setPays('France');
        $etablissement->setEstSiege(true);

        $entrepriseRepo = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo->expects($this->once())
            ->method('getEntrepriseBySiren')
            ->with($entrepriseBody['entreprise']['siren'])
            ->willReturn($entreprise);

        $etablissementRepo = $this->createMock(EtablissementRepository::class);
        $etablissementRepo->expects($this->once())
            ->method('findBy')
            ->with(['idEntreprise' => $entreprise->getId()])
            ->willReturn([$etablissement]);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->exactly(2))
            ->method('getRepository')
            ->withConsecutive([Entreprise::class], [Etablissement::class])
            ->willReturnOnConsecutiveCalls($entrepriseRepo, $etablissementRepo);

        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class)
        );

        $return = $atexoEntreprise->createFromInscription($entrepriseBody);

        $this->assertEquals($entreprise->getRaisonSociale(), $return['entreprise']->getRaisonSociale());
        $this->assertEquals($entreprise->getFormejuridique(), $return['entreprise']->getFormejuridique());
        $this->assertEquals($entreprise->getPaysenregistrement(), $return['entreprise']->getPaysenregistrement());
        $this->assertEquals($entreprise->getCategorieEntreprise(), $return['entreprise']->getCategorieEntreprise());
        $this->assertEquals($entreprise->getSirenetranger(), $return['entreprise']->getSirenetranger());
        $this->assertEquals($etablissement->getAdresse(), $return['establishments'][0]->getAdresse());
        $this->assertEquals($etablissement->getAdresse2(), $return['establishments'][0]->getAdresse2());
        $this->assertEquals($etablissement->getCodePostal(), $return['establishments'][0]->getCodePostal());
        $this->assertEquals($etablissement->getVille(), $return['establishments'][0]->getVille());
        $this->assertEquals($etablissement->getPays(), $return['establishments'][0]->getPays());
        $this->assertEquals($etablissement->getEstSiege(), $return['establishments'][0]->getEstSiege());
    }

    public function testCreateFromInscriptionWhenNotRegistered()
    {
        $entrepriseBody = [
            "entreprise" => [
                "siren"     => "440909562"
            ]
        ];

        $entreprise = new Entreprise();
        $entreprise->setId(1);
        $entreprise->setRaisonSociale('Atexo');
        $entreprise->setFormejuridique('forme juridique');
        $entreprise->setPaysenregistrement('France');
        $entreprise->setCategorieEntreprise('PME');

        $etablissement = new Etablissement();
        $etablissement->setAdresse('3 bd des bois');
        $etablissement->setAdresse2('Apt 131');
        $etablissement->setCodePostal('75001');
        $etablissement->setVille('Paris');
        $etablissement->setPays('France');
        $etablissement->setEstSiege(true);

        $entrepriseRepo = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo->expects($this->once())
            ->method('getEntrepriseBySiren')
            ->with($entrepriseBody['entreprise']['siren'])
            ->willReturn(null);

        $entrepriseRepo2 = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo2->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->exactly(2))
            ->method('getRepository')
            ->withConsecutive([Entreprise::class], [Entreprise::class])
            ->willReturnOnConsecutiveCalls($entrepriseRepo, $entrepriseRepo2);

        $atexoEtablissement = $this->createMock(AtexoEtablissement::class);
        $atexoEtablissement->expects($this->once())
            ->method('createOrUpdateEntity')
            ->willReturn($etablissement);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls('France', 'fr', 'fr');

        $returnFromAPI = [
            'data' => [
                'raison_sociale'        => 'Atexo',
                'siren'                 => '440909562',
                'pays_implantation'     => ['code' => 'FR'],
                'forme_juridique_code'  => '5699',
                'categorie_entreprise'  => 'PME',
                'capital_social'        => 1,
                'nom_commercial'        => 'Atexo',
                'procedure_collective'  => false,
                'mandataires_sociaux'   => [],
                'siret_siege_social'    => '44090956200001',
                'etat_administratif'    => 'A',
                'type'                  => 'personne_morale',
                'personne_morale_attributs' => [
                    'raison_sociale' => 'Atexo',
                ],
                'forme_juridique' => [
                    'libelle' => 'forme juridique',
                ],
                'activite_principale' => [
                    'code' => '1234',
                ]
            ],
            'etablissement_siege'   => [
                'siret'     => '44090956200001',
                'adresse'   => [
                    'l4'            => '1 rue de paris',
                    'code_postal'   => '75001',
                    'libelle_commune'      => 'Paris'
                ]
            ]
        ];
        $entrepriseApiService = $this->createMock(EntrepriseAPIService::class);
        $entrepriseApiService->expects($this->once())
            ->method('callEntreprise')
            ->with($entrepriseBody['entreprise']['siren'])
            ->willReturn($returnFromAPI);


        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $entrepriseApiService,
            $atexoEtablissement,
            $parameterBag,
            $this->createMock(TranslatorInterface::class)
        );

        $return = $atexoEntreprise->createFromInscription($entrepriseBody);

        $this->assertEquals('Atexo', $return['entreprise']->getRaisonSociale());
        $this->assertEquals('forme juridique', $return['entreprise']->getFormejuridique());
        $this->assertEquals('fr', $return['entreprise']->getPaysenregistrement());
        $this->assertEquals('PME', $return['entreprise']->getCategorieEntreprise());
        $this->assertEquals('440909562', $return['entreprise']->getSiren());
        $this->assertEquals('1234', $return['entreprise']->getCodeape());
        $this->assertEquals($etablissement->getAdresse(), $return['establishments'][0]->getAdresse());
        $this->assertEquals($etablissement->getAdresse2(), $return['establishments'][0]->getAdresse2());
        $this->assertEquals($etablissement->getCodePostal(), $return['establishments'][0]->getCodePostal());
        $this->assertEquals($etablissement->getVille(), $return['establishments'][0]->getVille());
        $this->assertEquals($etablissement->getPays(), $return['establishments'][0]->getPays());
        $this->assertEquals($etablissement->getEstSiege(), $return['establishments'][0]->getEstSiege());
    }

    public function testGetHomeBasicMessages()
    {
        $msgHome = new MessageAccueil();
        $msgHome->setContenu('message accueil');

        $entrepriseRepo = $this->createMock(EntrepriseRepository::class);
        $entrepriseRepo->expects($this->once())
            ->method('findOneBy')
            ->willReturn($msgHome);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($entrepriseRepo);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->once())
            ->method('get')
            ->willReturn('config home message');

        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->exactly(7))
            ->method('trans')
            ->willReturn('une variable de langue');

        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $mockEntityManager,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $this->createMock(AtexoEtablissement::class),
            $parameterBag,
            $translator
        );

        $return = $atexoEntreprise->getHomeBasicMessages();

        $this->assertIsArray($return);
        $this->assertEquals('message accueil', $return['homeMessage']);
        $this->assertIsArray($return['langMessages']);
        foreach ($return['langMessages'] as $langMessage) {
            $this->assertIsString($langMessage);
            $this->assertEquals('une variable de langue', $langMessage);
        }
    }

    public function testExplodeDataAdress()
    {
        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class)
        );

        $data = [
            'telephone' => '01258567897777',
            'ville' => 'Paris',
            'rue' => 'rue des petits champs',
            'codePostal' => '75000',
            'pays' => 'France',
            'acronymePays' => 'FR',
            'adresse' =>  'rue des petits champs'
        ];

        $response = $atexoEntreprise->explodeDataAdress($data);

        $this->assertIsArray($response);
        $this->assertIsString($response['adresse']);
        $this->assertEquals('rue des petits champs', $response['adresse']);
    }

    public function testExplodeDataAdressKeyVilleIsNull()
    {
        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class)
        );

        $data = [
            'telephone' => '01258567897777',
            'adresse' =>  [
                'rue' => 'rue des petits champs',
                'codePostal' => '75000',
                'pays' => 'France',
                'acronymePays' => 'FR'
            ]
        ];

        $response = $atexoEntreprise->explodeDataAdress($data);

        $this->assertIsArray($response);
        $this->assertEquals('', $response['ville']);
    }

    public function testExplodeDataAdressKeyAdresseNotExist()
    {
        $atexoEntreprise = new AtexoEntreprise(
            $this->createMock(ContainerInterface::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(EntrepriseAPIService::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class)
        );

        $data = [
            'telephone' => '01258567897777',
        ];

        $response = $atexoEntreprise->explodeDataAdress($data);

        $this->assertIsArray($response);
        $this->assertEquals($data, $response);
    }
}
