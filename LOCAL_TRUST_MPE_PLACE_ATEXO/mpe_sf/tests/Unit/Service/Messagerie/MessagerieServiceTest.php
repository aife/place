<?php

namespace Tests\Unit\Service\Messagerie;

use App\Service\AtexoUtil;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use App\Entity\Consultation;
use App\Entity\Consultation\Decision\DecisionSelectionEntreprise;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Inscrit;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\QuestionDCE;
use App\Entity\Telechargement;
use App\Repository\InscritRepository;
use App\Repository\OffreRepository;
use App\Repository\QuestionDCERepository;
use App\Repository\TelechargementRepository;
use App\Service\ClientWs\ClientWs;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Service\WebservicesMpeConsultations;
use App\Utils\Encryption;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Class MessagerieServiceTest.
 */
class MessagerieServiceTest extends TestCase
{
    private $messagerieService;

    /**
     * @var EntityManager|MockObject
     */
    private $entityManagerMock;

    /**
     * @var Encryption|MockObject
     */
    private $encryption;
    /**
     * @var MockObject|ContainerInterface
     */
    private $container;
    /**
     * @var array
     */
    private $json;
    /**
     * @var MockObject|TranslatorInterface
     */
    private $translator;
    /**
     * @var MockObject|Environment
     */
    private $engineInterface;

    public function setUp(): void
    {
        parent::setUp();

        $this->container = $this->createMock(ContainerInterface::class);
        $this->translator = $this->createMock(TranslatorInterface::class);
        $this->container->expects($this->any())
            ->method('getParameter')
            ->willReturnCallback(
                function ($parameter) {
                    if ('URL_MESSAGERIE_SECURISEE' === $parameter) {
                        return 'http://ms-rec.local-trust.com/messagerieSecurisee/';
                    }
                }
            );

        $this->entityManagerMock = $this->createMock(EntityManager::class);

        $this->encryption = $this->createMock(Encryption::class);
        $this->engineInterface = $this->createMock(Environment::class);
        $logger = $this->createMock(Logger::class);
        $clientWs = $this->createMock(ClientWs::class);
        $this->messagerieService = new MessagerieService(
            $this->entityManagerMock,
            $this->encryption,
            $this->container,
            $this->translator,
            $clientWs,
            $this->engineInterface,
            $logger,
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(WebservicesMpeConsultations::class),
            $this->createMock(UrlGeneratorInterface::class),
            $this->createMock(WebServicesMessagerie::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(Security::class)
        );

        $this->json = [
            'idObjetMetierPfEmetteur' => '14822',
            'idPfEmetteur' => 'RSEM_VDPR7',
            'urlPfEmetteur' => 'https://atx53.local-trust.com/epm.passation/messagerieSecurisee/',
            'emailExpediteur' => 'rsem@atexo.com',
            'nomCompletExpediteur' => 'TOTO',
            'nomPfEmetteur' => 'RSEM',
        ];
    }

    /* Test a mettre dans les test fonctionnel et a revoir
     public function testInitialidationMessage()
    {
        $token = $this->messagerieService->initialidationMessage($this->json);
        $status = $token->getStatusCode();
        $this->assertEquals(200, $status);
    }*/

    public function testGetListRegistreQuestions()
    {
        $consultationId = 150;
        $organisme = 'pmi-min-1';

        $questionDCE = new QuestionDCE();
        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $inscrit->setNom('test');
        $inscrit->setPrenom('test');
        $questionDCE->setInscrit($inscrit);

        $questionDCERepository = $this->createMock(QuestionDCERepository::class);
        $questionDCERepository->expects($this->any())
            ->method('findBy')
            ->willReturn([$questionDCE]);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn($questionDCERepository);

        $listes = $this->messagerieService->getListRegistreQuestions($consultationId, $organisme);

        $this->assertEquals(1, count($listes));
    }

    public function testGetListRegistreDepots()
    {
        $consultationId = 150;
        $organisme = 'pmi-min-1';

        $offre = new Offre();
        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $inscrit->setNom('test');
        $inscrit->setPrenom('test');
        $offre->setInscrit($inscrit);

        $offreRepository = $this->createMock(OffreRepository::class);
        $offreRepository->expects($this->any())
            ->method('getConsultationOffresByStatus')
            ->willReturn([$offre]);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn($offreRepository);

        $listes = $this->messagerieService->getListRegistreDepots($consultationId, $organisme);

        $this->assertEquals(1, count($listes));
    }

    public function testGetListRegistreRetraits()
    {
        $consultationId = 150;
        $organisme = 'pmi-min-1';

        $telechargement = new Telechargement();
        $inscrit = new Inscrit();
        $inscrit->setId(1);
        $inscrit->setNom('test');
        $inscrit->setPrenom('test');
        $inscrit->setEmail('test@pp.fr');
        $telechargement->setIdInscrit(1);

        $telechargementRepository = $this->createMock(TelechargementRepository::class);
        $telechargementRepository->expects($this->any())
            ->method('findBy')
            ->willReturn([$telechargement]);

        $inscritRepository = $this->createMock(InscritRepository::class);
        $inscritRepository->expects($this->any())
            ->method('find')
            ->willReturn($inscrit);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($telechargementRepository, $inscritRepository);

        $listes = $this->messagerieService->getListRegistreRetraits($consultationId, $organisme);

        $this->assertEquals(1, count($listes));
    }

    public function testGetListeDestinatairesIssusPhasePrecedenteDeContratNoContact()
    {
        $consultation = $this->createMock(Consultation::class);
        $contratTitulaire = $this->createMock(ContratTitulaire::class);
        $repositoryContratTitulaire = $this->createMock(ObjectRepository::class);
        $repositoryContratTitulaire->expects($this->any())
            ->method('find')
            ->willReturn($contratTitulaire);
        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn($repositoryContratTitulaire);
        $listes = $this->messagerieService->getListeDestinatairesIssusPhasePrecedenteDeContrat($consultation);
        $this->assertEquals(0, count($listes));
    }

    public function testGetListeDestinatairesIssusPhasePrecedenteDeContrat()
    {
        $consultation = $this->createMock(Consultation::class);

        $contratTitulaire = new ContratTitulaire();
        $contratTitulaire->setChapeau(1);
        $contratTitulaire->setId(1);

        $repositoryContratTitulaire = $this->createMock(ObjectRepository::class);
        $repositoryContratTitulaire->expects($this->any())
            ->method('find')
            ->willReturn($contratTitulaire);

        $contratsAttributaires = [];
        for ($i = 0; $i < 2; ++$i) {
            $contratsAttributaire = new ContratTitulaire();
            $contratsAttributaire->setChapeau(1);
            $contratsAttributaire->setId($i);
            $contratsAttributaires[] = $contratsAttributaire;
        }
        $repositoryContratsAttributaires = $this->createMock(ObjectRepository::class);
        $repositoryContratsAttributaires->expects($this->any())
            ->method('findBy')
            ->willReturn($contratsAttributaires);

        $contact = new ContactContrat();
        $contact->setEmail('123@atexo.fr');
        $contact->setNom('123');
        $contact->setPrenom('123');
        $repositoryContact = $this->createMock(ObjectRepository::class);
        $repositoryContact->expects($this->any())
            ->method('find')
            ->willReturn($contact);

        $entreprise = new Entreprise();
        $repositoryEntrepise = $this->createMock(ObjectRepository::class);
        $repositoryEntrepise->expects($this->any())
            ->method('find')
            ->willReturn($entreprise);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn(
                $repositoryContratTitulaire,
                $repositoryContratsAttributaires,
                $repositoryContact,
                $repositoryEntrepise,
                $repositoryEntrepise
            );

        $listes = $this->messagerieService->getListeDestinatairesIssusPhasePrecedenteDeContrat($consultation);

        $this->assertCount(1, $listes);
    }

    public function testGetListeDestinatairesIssusPhasePrecedenteDeConsultationInitiale()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-min-1');
        $consultation = new Consultation();
        $consultation->setId(1);
        $consultation->setOrganisme($organisme);
        $lots = [
          0,
        ];

        $collectionDecisionSelectionEntreprise = [];
        $decisionSelectionEntreprise = new DecisionSelectionEntreprise();

        $collectionDecisionSelectionEntreprise[] = $decisionSelectionEntreprise;
        $repositoryDecisionSelectionEntreprise = $this->createMock(ObjectRepository::class);
        $repositoryDecisionSelectionEntreprise->expects($this->any())
            ->method('findBy')
            ->willReturn($collectionDecisionSelectionEntreprise);

        $offre = new Offre();
        $repositoryOffre = $this->createMock(ObjectRepository::class);
        $repositoryOffre->expects($this->any())
            ->method('findOneBy')
            ->willReturn($offre);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn(
                $repositoryDecisionSelectionEntreprise,
                $repositoryOffre
            );
        $listes = $this->messagerieService
            ->getListeDestinatairesIssusPhasePrecedenteDeConsultationInitiale($lots, $consultation);
        $this->assertCount(1, $listes);
    }

    public function testGetListeDestinatairesIssusPhasePrecedente()
    {
        $lot = new Lot();

        $consultation = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };

        $consultation->addLot($lot);
        $consultation->setReferenceConsultationInit(1);

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-min-1');
        $consultationInitiale = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };

        $consultationInitiale->setOrganisme($organisme);
        $repositoryConsultationInitiale = $this->createMock(ObjectRepository::class);
        $repositoryConsultationInitiale->expects($this->any())
            ->method('findOneBy')
            ->willReturn($consultationInitiale);

        $collectionDecisionSelectionEntreprise = [];
        $decisionSelectionEntreprise = new DecisionSelectionEntreprise();

        $collectionDecisionSelectionEntreprise[] = $decisionSelectionEntreprise;
        $repositoryDecisionSelectionEntreprise = $this->createMock(ObjectRepository::class);
        $repositoryDecisionSelectionEntreprise->expects($this->any())
            ->method('findBy')
            ->willReturn($collectionDecisionSelectionEntreprise);

        $offre = new Offre();
        $repositoryOffre = $this->createMock(ObjectRepository::class);
        $repositoryOffre->expects($this->any())
            ->method('findOneBy')
            ->willReturn($offre);

        $this->entityManagerMock
            ->expects($this->any())
            ->method('getRepository')
            ->willReturn(
                $repositoryConsultationInitiale,
                $repositoryDecisionSelectionEntreprise,
                $repositoryOffre
            );

        $listes = $this->messagerieService->getListeDestinatairesIssusPhasePrecedente($consultation);
        $this->assertCount(1, $listes);
    }

    public function testInitializeContentForSuiviToken()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->exactly(2))->method('get')
            ->willReturnOnConsecutiveCalls('nom_plateforme', 'id_plateforme');

        $hydra = new stdClass();
        $hydra->{'0'} = new stdClass();
        $hydra->{'0'}->{'id'} = 2;
        $hydra->{'objet'} = 'un objet';
        $hydra->{'reference'} = 'une reference';
        $returnFromApiPlatform = new stdClass();
        $returnFromApiPlatform->{'hydra:member'} = [
            $hydra
        ];
        $wsConsultations = $this->createMock(WebservicesMpeConsultations::class);
        $wsConsultations->expects($this->once())->method('getContent')
            ->willReturn($returnFromApiPlatform);

        $urlGeneratorInterface = $this->createMock(UrlGeneratorInterface::class);
        $urlGeneratorInterface->expects($this->once())->method('generate')
            ->willReturn('/agent/messagerie/suivi/UVRZTmhrY3hrLzRsTXh2Z1BIK3B4dz09');

        $em = $this->createMock(EntityManager::class);
        $queryBuilder = $this->createMock(QueryBuilder::class);
        $query = $this->getMockBuilder(AbstractQuery::class)
            ->disableOriginalConstructor()
            ->onlyMethods(['getSingleResult'])
            ->getMockForAbstractClass();
        //dd($query);
        $queryBuilder->expects($this->once())->method('select')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('from')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('leftJoin')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('where')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('setParameter')->willReturn($queryBuilder);
        $queryBuilder->expects($this->once())->method('getQuery')->willReturn($query);
        $query->expects($this->once())->method('getSingleResult')->willReturn([
            'objet' => 'un objet',
            'reference' => 'une reference'
            ]);
        $em->expects($this->once())->method('createQueryBuilder')
            ->willReturn($queryBuilder);

        $messagerieService = new MessagerieService(
            $em,
            $this->createMock(Encryption::class),
            $this->createMock(ContainerInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(ClientWs::class),
            $this->createMock(Environment::class),
            $this->createMock(Logger::class),
            $parameterBag,
            $wsConsultations,
            $urlGeneratorInterface,
            $this->createMock(WebServicesMessagerie::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(Security::class)
        );

        $params = $messagerieService->initializeContentForSuiviToken();

        $this->assertSame('nom_plateforme', $params['nomPfEmetteur']);
        $this->assertSame(MessagerieService::TYPE_PF_RECHERCHE, $params['typePlateformeRecherche']);
        $this->assertSame('id_plateforme', $params['idPlateformeRecherche']);
        $this->assertSame(2, $params['objetsMetier'][0]['identifiant']);
        $this->assertSame('un objet', $params['objetsMetier'][0]['objet']);
        $this->assertSame('une reference', $params['objetsMetier'][0]['reference']);
        $this->assertSame(MessagerieService::URL_RETOUR . '2', $params['objetsMetier'][0]['urlRetour']);
        $this->assertSame(
            '/agent/messagerie/suivi/UVRZTmhrY3hrLzRsTXh2Z1BIK3B4dz09',
            $params['objetsMetier'][0]['urlSuivi']
        );
    }
}
