<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Messagerie;

use ReflectionClass;
use App\Service\AtexoUtil;
use App\Service\Messagerie\WebServicesMessagerie;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesMessagerieTest extends TestCase
{
    private WebServicesMessagerie $webServicesMessagerie;

    public function setUp(): void
    {
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $atexoUtil->expects($this->exactly(1))->method('getPfUrlDomaine')
            ->willReturnOnConsecutiveCalls('messec');

        $this->webServicesMessagerie = new WebServicesMessagerie(
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(HttpClientInterface::class),
            $this->createMock(LoggerInterface::class),
            $atexoUtil,
        );
    }
    
    public function testInitParamsWidgetMessec()
    {
        $reflectionClass = new ReflectionClass(WebServicesMessagerie::class);
        $methodProtected = $reflectionClass->getMethod('initParamsWidgetMessec');
        $methodProtected ->setAccessible(true);

        $response = $methodProtected->invokeArgs($this->webServicesMessagerie, ['reponseNonLue']);

        $this->assertIsArray($response);
        $this->assertCount(1, $response);
        $this->assertTrue($response['reponseNonLue']);
    }

    public function testInitParamsWidgetMessecReponseAttendue()
    {
        $reflectionClass = new ReflectionClass(WebServicesMessagerie::class);
        $methodProtected = $reflectionClass->getMethod('initParamsWidgetMessec');
        $methodProtected ->setAccessible(true);

        $response = $methodProtected->invokeArgs($this->webServicesMessagerie, ['reponseAttendue']);

        $this->assertIsArray($response);
        $this->assertCount(3, $response);
        $this->assertTrue($response['reponseAttendue']);
        $this->assertTrue($response['statutAcquitte']);
        $this->assertTrue($response['statutDelivre']);
    }
}