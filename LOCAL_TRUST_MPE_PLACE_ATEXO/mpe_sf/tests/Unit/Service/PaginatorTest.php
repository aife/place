<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Service\Paginator;
use PHPUnit\Framework\TestCase;

class PaginatorTest extends TestCase
{
    public function testGetPagination()
    {
        $responseView = '{
            "@id": "/api/v2/consultations?page=2",
            "@type": "hydra:PartialCollectionView",
            "hydra:first": "/api/v2/consultations?page=1",
            "hydra:last": "/api/v2/consultations?page=25",
            "hydra:previous": "/api/v2/consultations?page=1",
            "hydra:next": "/api/v2/consultations?page=3"
        }';
        $apiView = json_decode($responseView);
        $uri = '/agent/consultation/recherche?keyWord=&typeProcedureOrganisme=&dlroApres='
        . '&dlroAvant=&form_lieuxExecution=&miseEnLigneApres=&miseEnLigneAvant=&search=';

        $paginator = new Paginator();
        $paginator = $paginator->getPagination($apiView, $uri);

        $this->assertIsArray($paginator);

        $this->assertArrayHasKey('current', $paginator);
        $this->assertEquals(2, $paginator['current']);

        $this->assertArrayHasKey('first', $paginator);
        $this->assertEquals(1, $paginator['first']);

        $this->assertArrayHasKey('last', $paginator);
        $this->assertEquals(25, $paginator['last']);

        $this->assertArrayHasKey('next', $paginator);
        $this->assertEquals($uri . '&page=3', $paginator['next']);

        $this->assertArrayHasKey('previous', $paginator);
        $this->assertEquals($uri . '&page=1', $paginator['previous']);

        $this->assertArrayHasKey('itemsPerPage10', $paginator);
        $this->assertEquals($uri, $paginator['itemsPerPage10']);

        $this->assertArrayHasKey('itemsPerPage20', $paginator);
        $this->assertEquals($uri . '&itemsPerPage=20', $paginator['itemsPerPage20']);

        $this->assertArrayHasKey('itemsPerPage20isActive', $paginator);
        $this->assertFalse($paginator['itemsPerPage20isActive']);
    }
}
