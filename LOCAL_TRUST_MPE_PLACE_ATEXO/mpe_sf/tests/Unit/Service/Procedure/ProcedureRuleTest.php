<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\ProcedureRule;
use App\Entity\ProcedureEquivalence;

class ProcedureRuleTest extends TestCase
{
    public function testIsSelectedWithPositiveValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('+1');

        $rule = new ProcedureRule();

        $result = $rule->isSelected($procedure, 'getDonneesComplementaireOui');

        $this->assertTrue($result);
    }

    public function testIsSelectedWithZeroValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('+0');

        $rule = new ProcedureRule();

        $result = $rule->isSelected($procedure, 'getDonneesComplementaireOui');

        $this->assertFalse($result);
    }

    public function testCanSeeWithPositiveValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('+1');

        $rule = new ProcedureRule();

        $result = $rule->canSee($procedure, 'getDonneesComplementaireOui');

        $this->assertTrue($result);
    }

    public function testCanSeeWithNegativeValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('-1');

        $rule = new ProcedureRule();

        $result = $rule->canSee($procedure, 'getDonneesComplementaireOui');

        $this->assertFalse($result);
    }

    public function testIsFixedWithPositiveValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('+1');

        $rule = new ProcedureRule();

        $result = $rule->isFixed($procedure, 'getDonneesComplementaireOui');

        $this->assertFalse($result);
    }

    public function testIsFixedWithNegativeValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('-1');

        $rule = new ProcedureRule();

        $result = $rule->isFixed($procedure, 'getDonneesComplementaireOui');

        $this->assertFalse($result);
    }

    public function testIsFixedWithZeroValue()
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setDonneesComplementaireOui('+0');

        $rule = new ProcedureRule();

        $result = $rule->isFixed($procedure, 'getDonneesComplementaireOui');

        $this->assertFalse($result);
    }
}
