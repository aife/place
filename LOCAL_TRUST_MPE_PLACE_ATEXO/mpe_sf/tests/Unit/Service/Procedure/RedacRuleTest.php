<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\RedacRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;
use App\Service\AtexoConfiguration;
use App\Model\ConsultationModule;
use App\Model\ConsultationInput;

class RedacRuleTest extends TestCase
{
    public function testApplyWithModuleEnabledAndOptionsVisible()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceModuleRsem', $organism)
            ->willReturn(true);

        $rule = new RedacRule($configuration);

        $procedure->setDonneesComplementaireOui('+1');
        $procedure->setDonneesComplementaireNon('+0');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertInstanceOf(ConsultationModule::class, $result->redac);
        $this->assertInstanceOf(ConsultationInput::class, $result->redac->yes);
        $this->assertInstanceOf(ConsultationInput::class, $result->redac->no);
        $this->assertTrue($result->redac->yes->display);
        $this->assertTrue($result->redac->no->display);
        $this->assertTrue($result->redac->yes->selected);
        $this->assertFalse($result->redac->no->selected);
        $this->assertFalse($result->redac->yes->fixed);
        $this->assertFalse($result->redac->no->fixed);
    }

    public function testApplyWithModuleDisabled()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceModuleRsem', $organism)
            ->willReturn(false);

        $rule = new RedacRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->redac);
    }

    public function testApplyWithOptionsHidden()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceModuleRsem', $organism)
            ->willReturn(true);

        $rule = new RedacRule($configuration);

        $procedure->setDonneesComplementaireOui('-1');
        $procedure->setDonneesComplementaireNon('-1');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->redac->yes->display);
        $this->assertFalse($result->redac->no->display);
    }
}
