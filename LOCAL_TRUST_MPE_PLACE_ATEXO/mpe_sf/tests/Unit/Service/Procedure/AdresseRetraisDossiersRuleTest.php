<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\AdresseRetraisDossiersRule;
use App\Service\Procedure\JoRule;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdresseRetraisDossiersRuleTest extends TestCase
{
    private $configuration;
    private $parameterBag;
    private $adresseRetraisDossiersRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->adresseRetraisDossiersRule = new AdresseRetraisDossiersRule($this->configuration, $this->parameterBag);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationAdresseRetraisDossiers', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->adresseRetraisDossiersRule->apply($form, $organism);

        $this->assertTrue($result->adresseRetraisDossiers);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'AdresseRetraisDossiersRule';

        $result = AdresseRetraisDossiersRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationAdresseRetraisDossiers', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->adresseRetraisDossiersRule->isActive($organism);

        $this->assertTrue($result);
    }
}
