<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\JoRule;
use PHPUnit\Framework\TestCase;

class JoRuleTest extends TestCase
{
    private $configuration;
    private $joRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->joRule = new JoRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('typageJo2024', $organism, 'is')
            ->willReturn(true);

        $result = $this->joRule->apply($form, $organism);

        $this->assertTrue($result->jo);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'JoRule';

        $result = JoRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('typageJo2024', $organism, 'is')
            ->willReturn(true);

        $result = $this->joRule->isActive($organism);

        $this->assertTrue($result);
    }
}
