<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\AccesRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;

class AccesRuleTest extends TestCase
{
    public function testApplyWithAccessAllowed()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();

        $accessRule = new AccesRule();

        $procedure->setProcedurePublicite('+1');
        $procedure->setProcedureRestreinte('+0');

        $result = $accessRule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertTrue($result->acces->yes->display || $result->acces->no->display);
        $this->assertTrue($result->acces->no->selected);
        $this->assertFalse($result->acces->yes->selected);
    }

    public function testApplyWithAccessDenied()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();

        $procedure->setProcedurePublicite('-1');
        $procedure->setProcedureRestreinte('-1');

        $accessRule = new AccesRule();

        $result = $accessRule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->acces->yes->display);
        $this->assertFalse($result->acces->no->display);
    }
}
