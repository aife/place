<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\PubliciteRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;
use App\Service\AtexoConfiguration;
use App\Model\ConsultationModule;
use App\Model\ConsultationInput;

class PubliciteRuleTest extends TestCase
{
    public function testApplyWithModuleEnabledAndTypesVisible()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('Publicite', $organism)
            ->willReturn(true);

        $rule = new PubliciteRule($configuration);

        $procedure->setAutoriserPubliciteOui('+1');
        $procedure->setAutoriserPubliciteNon('+0');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertTrue($result->publicite->yes->display);
        $this->assertTrue($result->publicite->yes->selected);
        $this->assertFalse($result->publicite->yes->fixed);
        $this->assertTrue($result->publicite->no->display);
        $this->assertFalse($result->publicite->no->selected);
        $this->assertFalse($result->publicite->no->fixed);
    }

    public function testApplyWithModuleDisabled()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('Publicite', $organism)
            ->willReturn(false);

        $rule = new PubliciteRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->publicite);
    }

    public function testApplyWithTypesHidden()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('Publicite', $organism)
            ->willReturn(true);

        $rule = new PubliciteRule($configuration);

        $procedure->setAutoriserPubliciteOui('-1');
        $procedure->setAutoriserPubliciteNon('-1');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->publicite->yes->display);
        $this->assertFalse($result->publicite->no->display);
    }
}
