<?php

namespace Tests\Unit\Service\Procedure;

use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Entity\ProcedureEquivalenceDume;
use App\Repository\Procedure\ProcedureEquivalenceDumeRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\Consultation\ConsultationProcedureEquivalence;
use App\Service\Consultation\ConsultationService;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProcedureEqualenceAgentServiceTest extends TestCase
{
    private $procedure;

    protected function setUp()
    {
        $emMock = $this->createMock(EntityManagerInterface::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $this->procedure = new ProcedureEquivalenceAgentService($emMock, $parameterBagMock, $this->createMock(ConsultationProcedureEquivalence::class));
    }

    public function testGetActivate1()
    {
        $this->assertEquals(1, $this->procedure->getActivate('+1'));
    }

    public function testGetActivate0()
    {
        $this->assertEquals(0, $this->procedure->getActivate('+0'));
    }

    public function testGetActivateError()
    {
        $this->assertEquals(0, $this->procedure->getActivate('+3'));
    }

    public function testSetValueProcedure()
    {
        $consultationReference = new Consultation();
        $consultationReference->setEnvOffre(1);

        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setEnvOffre('+1');

        $consultation = new Consultation();

        $consultation = $this->procedure->setValueProcedure('EnvOffre', $consultation, $procedureEquivalence);

        $this->assertEquals($consultationReference->getEnvOffre(), $consultation->getEnvOffre());
    }

    public function testInitializeConsultationWithoutChangeParametreProcedure()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('test');

        $consultation = $this->getConsultationFromWs();
        $consultation->setIdTypeProcedureOrg(1);
        $consultation->setOrganisme($organisme);

        $repoProcedureEquivalence = $this->createMock(ProcedureEquivalenceRepository::class);
        $repoProcedureEquivalence->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $repoProcedureEquivalenceDume = $this->createMock(ProcedureEquivalenceDumeRepository::class);
        $repoProcedureEquivalenceDume->expects($this->never())
            ->method('findOneBy');

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repoProcedureEquivalence, $repoProcedureEquivalenceDume);

        $procedureEquivalenceAgentService = new ProcedureEquivalenceAgentService($em, $this->createMock(ParameterBagInterface::class), $this->createMock(ConsultationProcedureEquivalence::class));
        $result = $procedureEquivalenceAgentService->initializeConsultationWithParametreProcedure(
            $consultation,
            ConsultationService::PARAMETRE_PROCEDURE_LIST_FOR_WS_CONSULTATION
        );

        $this->assertInstanceOf(Consultation::class, $result);

        $this->assertSame('0', $result->getAlloti());
        $this->assertSame('1Pt9', $result->getCodeCpv1());
        $this->assertSame('1', $result->getDumeDemande());
        $this->assertSame(0, $result->getTypeFormulaireDume());
        $this->assertSame(1, $result->getTypeProcedureDume());
        $this->assertSame('1', $result->getAutoriserReponseElectronique());
        $this->assertSame('0', $result->getReponseObligatoire());
        $this->assertSame(30, $result->getPoursuivreAffichage());
        $this->assertSame('DAY', $result->getPoursuivreAffichageUnite());
        $this->assertSame('2', $result->getSignatureOffre());
        $this->assertSame('1', $result->getModeOuvertureReponse());
        $this->assertSame('1', $result->getChiffrementOffre());
        $this->assertSame('1', $result->getEnvCandidature());
        $this->assertSame('0', $result->getEnvOffre());
        $this->assertSame('0', $result->getEnvAnonymat());
    }

    public function testInitializeConsultationWithParametreProcedure()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('test');

        $consultation = $this->getConsultationFromWs();
        $consultation->setIdTypeProcedureOrg(1);
        $consultation->setOrganisme($organisme);

        $procedureEquivalence = $this->getParametrageProcedure();
        $procedureEquivalenceDume = new ProcedureEquivalenceDume();
        $procedureEquivalenceDume->setIdTypeProcedureDume(19);

        $repoProcedureEquivalence = $this->createMock(ProcedureEquivalenceRepository::class);
        $repoProcedureEquivalence->expects($this->once())
            ->method('findOneBy')
            ->with([
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $consultation->getOrganisme()->getAcronyme()
            ])
            ->willReturn($procedureEquivalence);

        $repoProcedureEquivalenceDume = $this->createMock(ProcedureEquivalenceDumeRepository::class);
        $repoProcedureEquivalenceDume->expects($this->once())
            ->method('findOneBy')
            ->with([
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $consultation->getOrganisme()->getAcronyme(),
                'selectionner' => '1'
            ])
            ->willReturn($procedureEquivalenceDume);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repoProcedureEquivalence, $repoProcedureEquivalenceDume);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('1');


        $procedureEquivalenceAgentService = new ProcedureEquivalenceAgentService($em, $parameterBag, new ConsultationProcedureEquivalence($em, $parameterBag));
        $result = $procedureEquivalenceAgentService->initializeConsultationWithParametreProcedure(
            $consultation,
            ConsultationService::PARAMETRE_PROCEDURE_LIST_FOR_WS_CONSULTATION
        );

        $this->assertInstanceOf(Consultation::class, $result);
        $this->assertSame('0', $result->getAlloti());
        $this->assertNull($result->getCodeCpv1());
        $this->assertNull($result->getCodeCpv2());
        $this->assertSame('1', $result->getDumeDemande());
        $this->assertSame(1, $result->getTypeFormulaireDume());
        $this->assertSame(19, $result->getTypeProcedureDume());
        $this->assertSame('1', $result->getAutoriserReponseElectronique());
        $this->assertSame('1', $result->getReponseObligatoire());
        $this->assertSame(0, $result->getPoursuivreAffichage());
        $this->assertSame('HOUR', $result->getPoursuivreAffichageUnite());
        $this->assertSame('1', $result->getSignatureOffre());
        $this->assertSame('0', $result->getModeOuvertureReponse());
        $this->assertSame('0', $result->getChiffrementOffre());
        $this->assertSame('0', $result->getEnvCandidature());
        $this->assertSame('1', $result->getEnvOffre());
        $this->assertSame('1', $result->getEnvAnonymat());
    }

    private function getConsultationFromWs(): Consultation
    {
        $consultation = new Consultation();
        $consultation->setAlloti('0');
        $consultation->setCodeCpv1('1Pt9');
        $consultation->setDumeDemande('1');
        $consultation->setTypeFormulaireDume(0);
        $consultation->setTypeProcedureDume(1);
        $consultation->setAutoriserReponseElectronique('1');
        $consultation->setReponseObligatoire('0');
        $consultation->setPoursuivreAffichage(30);
        $consultation->setPoursuivreAffichageUnite('DAY');
        $consultation->setSignatureOffre('2');
        $consultation->setModeOuvertureReponse('1');
        $consultation->setChiffrementOffre('1');
        $consultation->setEnvCandidature('1');
        $consultation->setEnvOffre('0');
        $consultation->setEnvAnonymat('0');

        return $consultation;
    }

    /**
     * Simule un paramétrage de type procédure
     * @return ProcedureEquivalence
     */
    private function getParametrageProcedure(): ProcedureEquivalence
    {
        $procedure = new ProcedureEquivalence();
        $procedure->setEnvOffreTypeMultiple('+1');
        $procedure->setAfficherCodeCpv('+0');
        $procedure->setDumeDemande('+0');
        $procedure->setTypeFormulaireDumeStandard('0');
        $procedure->setElecResp('+0');
        $procedure->setRepObligatoire('+1');
        $procedure->setDelaiPoursuiteAffichage('1');
        $procedure->setDelaiPoursuivreAffichageUnite('HOUR');
        $procedure->setSignatureDisabled('0');
        $procedure->setSignatureEnabled('1');
        $procedure->setSignatureAutoriser('0');
        $procedure->setModeOuvertureReponse('+0');
        $procedure->setCipherEnabled('+0');
        $procedure->setEnvCandidature('+0');
        $procedure->setEnvOffre('+1');
        $procedure->setEnvAnonymat('+1');
        $procedure->setPoursuiteDateLimiteRemisePli('+1');
        $procedure->setDelaiPoursuiteAffichage(30);

        return $procedure;
    }
}
