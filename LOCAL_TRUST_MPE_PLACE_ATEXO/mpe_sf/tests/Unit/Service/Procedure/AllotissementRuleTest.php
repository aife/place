<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\AllotissementRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;

class AllotissementRuleTest extends TestCase
{
    public function testApplyWithAllotissementAllowed()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();

        $allotissementRule = new AllotissementRule();

        $procedure->setEnvOffreTypeMultiple('+1');
        $procedure->setEnvOffreTypeUnique('+0');

        $result = $allotissementRule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertTrue($result->allotissement->yes->display);
        $this->assertTrue($result->allotissement->yes->selected);
        $this->assertFalse($result->allotissement->yes->fixed);
        $this->assertTrue($result->allotissement->no->display);
        $this->assertFalse($result->allotissement->no->selected);
        $this->assertFalse($result->allotissement->no->fixed);
    }

    public function testApplyWithAllotissementDenied()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();

        $allotissementRule = new AllotissementRule();

        $procedure->setEnvOffreTypeMultiple('-1');
        $procedure->setEnvOffreTypeUnique('-1');

        $result = $allotissementRule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->allotissement->yes->display);
        $this->assertFalse($result->allotissement->no->display);
    }
}
