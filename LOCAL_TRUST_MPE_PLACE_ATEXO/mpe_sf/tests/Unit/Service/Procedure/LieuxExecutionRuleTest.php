<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\LieuxExecutionRule;
use PHPUnit\Framework\TestCase;

class LieuxExecutionRuleTest extends TestCase
{
    private $configuration;
    private $lieuxExecutionRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->lieuxExecutionRule = new LieuxExecutionRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('LieuxExecution', $organism)
            ->willReturn(true);

        $result = $this->lieuxExecutionRule->apply($form, $organism);

        $this->assertTrue($result->lieuxExecution);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'LieuxExecutionRule';

        $result = LieuxExecutionRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('LieuxExecution', $organism)
            ->willReturn(true);

        $result = $this->lieuxExecutionRule->isActive($organism);

        $this->assertTrue($result);
    }
}
