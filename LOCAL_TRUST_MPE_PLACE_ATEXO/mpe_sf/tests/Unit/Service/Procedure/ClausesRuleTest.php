<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\ClausesRule;
use PHPUnit\Framework\TestCase;

class ClausesRuleTest extends TestCase
{
    private $configuration;
    private $clausesRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->clausesRule = new ClausesRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationClause', $organism)
            ->willReturn(true);

        $result = $this->clausesRule->apply($form, $organism);

        $this->assertTrue($result->clauses);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'ClausesRule';

        $result = ClausesRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationClause', $organism)
            ->willReturn(true);

        $result = $this->clausesRule->isActive($organism);

        $this->assertTrue($result);
    }
}
