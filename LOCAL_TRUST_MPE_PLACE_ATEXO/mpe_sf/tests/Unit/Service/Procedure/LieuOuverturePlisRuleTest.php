<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\AdresseRetraisDossiersRule;
use App\Service\Procedure\JoRule;
use App\Service\Procedure\LieuOuverturePlisRule;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class LieuOuverturePlisRuleTest extends TestCase
{
    private $configuration;
    private $parameterBag;
    private $lieuOuverturePlisRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->lieuOuverturePlisRule = new LieuOuverturePlisRule($this->configuration, $this->parameterBag);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationLieuOuverturePlis', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->lieuOuverturePlisRule->apply($form, $organism);

        $this->assertTrue($result->lieuOuverturePlis);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'LieuOuverturePlisRule';

        $result = LieuOuverturePlisRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationLieuOuverturePlis', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->lieuOuverturePlisRule->isActive($organism);

        $this->assertTrue($result);
    }
}
