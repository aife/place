<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\AdresseDepotOffresRule;
use App\Service\Procedure\AdresseRetraisDossiersRule;
use App\Service\Procedure\JoRule;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdresseDepotOffresRuleTest extends TestCase
{
    private $configuration;
    private $parameterBag;
    private $adresseDepotOffresRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->adresseDepotOffresRule = new AdresseDepotOffresRule($this->configuration, $this->parameterBag);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationAdresseDepotOffres', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->adresseDepotOffresRule->apply($form, $organism);

        $this->assertTrue($result->adresseDepotOffres);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'AdresseDepotOffresRule';

        $result = AdresseDepotOffresRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('ConsultationAdresseDepotOffres', $organism)
            ->willReturn(true);

        $this->parameterBag->expects($this->once())
            ->method('get')
            ->willReturn(false);

        $result = $this->adresseDepotOffresRule->isActive($organism);

        $this->assertTrue($result);
    }
}
