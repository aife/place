<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\ValeurEstimeeRule;
use PHPUnit\Framework\TestCase;

class ValeurEstimeeRuleTest extends TestCase
{
    private $configuration;
    private $valeurEstimeeRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->valeurEstimeeRule = new ValeurEstimeeRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration
            ->method('isModuleEnabled')
            ->withConsecutive(['AfficherValeurEstimee', $organism], ['Publicite', $organism])
            ->willReturnOnConsecutiveCalls(true, true);

        $result = $this->valeurEstimeeRule->apply($form, $organism);

        $this->assertTrue($result->valeurEstimee);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'ValeurEstimeeRule';

        $result = ValeurEstimeeRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration
            ->method('isModuleEnabled')
            ->withConsecutive(['AfficherValeurEstimee', $organism], ['Publicite', $organism])
            ->willReturnOnConsecutiveCalls(true, true);

        $result = $this->valeurEstimeeRule->isActive($organism);

        $this->assertTrue($result);
    }
}
