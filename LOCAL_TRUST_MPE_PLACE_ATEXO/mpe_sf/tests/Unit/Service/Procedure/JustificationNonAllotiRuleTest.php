<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\JustificationNonAllotiRule;
use PHPUnit\Framework\TestCase;

class JustificationNonAllotiRuleTest extends TestCase
{
    private $configuration;
    private $justificationNonAllotiRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->justificationNonAllotiRule = new JustificationNonAllotiRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('DonneesRedac', $organism)
            ->willReturn(true);

        $result = $this->justificationNonAllotiRule->apply($form, $organism);

        $this->assertTrue($result->justificationNonAlloti);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'JustificationNonAllotiRule';

        $result = JustificationNonAllotiRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('DonneesRedac', $organism)
            ->willReturn(true);

        $result = $this->justificationNonAllotiRule->isActive($organism);

        $this->assertTrue($result);
    }
}
