<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\CpvRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;
use App\Model\ConsultationModule;
use App\Model\ConsultationInput;
use App\Service\AtexoConfiguration;

class CpvRuleTest extends TestCase
{
    public function testApplyWithModuleEnabledAndCpvSelected()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('AffichageCodeCpv', $organism)
            ->willReturn(true);

        $rule = new CpvRule($configuration);

        $procedure->setAfficherCodeCpv('+1');
        $procedure->setCodeCpvObligatoire('+1');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertInstanceOf(ConsultationModule::class, $result->cpv);
        $this->assertInstanceOf(ConsultationInput::class, $result->cpv->yes);
        $this->assertTrue($result->cpv->yes->display);
        $this->assertTrue($result->cpv->yes->mandatory);
    }

    public function testApplyWithModuleDisabled()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('AffichageCodeCpv', $organism)
            ->willReturn(false);

        $rule = new CpvRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->cpv);
    }

    public function testApplyWithCpvNotSelected()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('AffichageCodeCpv', $organism)
            ->willReturn(true);

        $rule = new CpvRule($configuration);

        $procedure->setAfficherCodeCpv('+0');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->cpv);
    }
}
