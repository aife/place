<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\EntiteAdjudicatriceRule;
use App\Service\Procedure\JoRule;
use PHPUnit\Framework\TestCase;

class EntiteAdjudicatriceRuleTest extends TestCase
{
    private $configuration;
    private $entiteAdjudicatriceRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->entiteAdjudicatriceRule = new EntiteAdjudicatriceRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('EntiteAdjudicatrice', $organism)
            ->willReturn(true);

        $result = $this->entiteAdjudicatriceRule->apply($form, $organism);

        $this->assertTrue($result->entiteAdjudicatrice);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'EntiteAdjudicatriceRule';

        $result = EntiteAdjudicatriceRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('EntiteAdjudicatrice', $organism)
            ->willReturn(true);

        $result = $this->entiteAdjudicatriceRule->isActive($organism);

        $this->assertTrue($result);
    }
}
