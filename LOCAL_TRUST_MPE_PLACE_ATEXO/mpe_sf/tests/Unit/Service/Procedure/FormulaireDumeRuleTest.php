<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\FormulaireDumeRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;
use App\Service\AtexoConfiguration;

class FormulaireDumeRuleTest extends TestCase
{
    public function testApplyWithModuleEnabledAndTypesVisible()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(true);

        $procedure->setTypeFormulaireDumeStandard('+0');
        $procedure->setTypeFormulaireDumeSimplifie('+1');

        $rule = new FormulaireDumeRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertTrue($result->formulaireDume->yes->display);
        $this->assertTrue($result->formulaireDume->yes->selected);
        $this->assertFalse($result->formulaireDume->yes->fixed);
        $this->assertTrue($result->formulaireDume->no->display);
        $this->assertFalse($result->formulaireDume->no->selected);
        $this->assertFalse($result->formulaireDume->no->fixed);
    }

    public function testApplyWithModuleDisabled()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(false);

        $rule = new FormulaireDumeRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->formulaireDume);
    }

    public function testApplyWithTypesHidden()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(true);

        $procedure->setTypeFormulaireDumeStandard('-1');
        $procedure->setTypeFormulaireDumeSimplifie('-1');

        $rule = new FormulaireDumeRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->formulaireDume->yes->display);
        $this->assertFalse($result->formulaireDume->no->display);
    }
}
