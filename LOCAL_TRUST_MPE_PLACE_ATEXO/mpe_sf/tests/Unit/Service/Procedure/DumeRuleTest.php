<?php

namespace Tests\Unit\Service\Procedure;

use PHPUnit\Framework\TestCase;
use App\Service\Procedure\DumeRule;
use App\Model\ConsultationForm;
use App\Entity\ProcedureEquivalence;
use App\Entity\Organisme;
use App\Model\ConsultationModule;
use App\Model\ConsultationInput;
use App\Service\AtexoConfiguration;

class DumeRuleTest extends TestCase
{
    public function testApplyWithModuleEnabledAndDumeRequested()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(true);

        $rule = new DumeRule($configuration);

        $procedure->setDumeDemande('+1');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertInstanceOf(ConsultationModule::class, $result->dume);
        $this->assertInstanceOf(ConsultationInput::class, $result->dume->yes);
        $this->assertTrue($result->dume->yes->display);
        $this->assertTrue($result->dume->yes->selected);
    }

    public function testApplyWithModuleDisabled()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(false);

        $rule = new DumeRule($configuration);

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->dume);
    }

    public function testApplyWithDumeNotRequested()
    {
        $form = new ConsultationForm();
        $procedure = new ProcedureEquivalence();
        $organism = new Organisme();
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('InterfaceDume', $organism)
            ->willReturn(true);

        $rule = new DumeRule($configuration);

        $procedure->setDumeDemande('-0');

        $result = $rule->apply($form, $procedure, $organism);

        $this->assertInstanceOf(ConsultationForm::class, $result);
        $this->assertFalse($result->dume);
    }
}
