<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;
use App\Service\Procedure\EntiteAdjudicatriceRule;
use App\Service\Procedure\JoRule;
use App\Service\Procedure\NumeroProjetAchatRule;
use PHPUnit\Framework\TestCase;

class NumeroProjetAchatRuleTest extends TestCase
{
    private $configuration;
    private $numeroProjetAchatRule;

    protected function setUp(): void
    {
        $this->configuration = $this->createMock(AtexoConfiguration::class);
        $this->numeroProjetAchatRule = new NumeroProjetAchatRule($this->configuration);
    }

    public function testApply()
    {
        $form = new ConsultationForm();
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('NumeroProjetAchat', $organism)
            ->willReturn(true);

        $result = $this->numeroProjetAchatRule->apply($form, $organism);

        $this->assertTrue($result->numeroProjetAchat);
    }

    public function testGetRuleName()
    {
        $expectedRuleName = 'NumeroProjetAchatRule';

        $result = NumeroProjetAchatRule::getRuleName();

        $this->assertSame($expectedRuleName, $result);
    }

    public function testIsActive()
    {
        $organism = new Organisme();

        $this->configuration->expects($this->once())
            ->method('isModuleEnabled')
            ->with('NumeroProjetAchat', $organism)
            ->willReturn(true);

        $result = $this->numeroProjetAchatRule->isActive($organism);

        $this->assertTrue($result);
    }
}
