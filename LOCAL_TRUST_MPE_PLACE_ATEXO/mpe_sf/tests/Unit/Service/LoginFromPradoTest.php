<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Repository\AgentRepository;
use App\Security\FormAuthenticator;
use App\Service\LoginFromPrado;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Tests\Unit\AtexoTestCase;

class LoginFromPradoTest extends AtexoTestCase
{
    public function testLoginByUsername()
    {
        $user = new Agent();
        $user->setLogin('AdminOrme');
        $authenticator = $this->createMock(GuardAuthenticatorHandler::class);
        $formAuthenticator = $this->createMock(FormAuthenticator::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $security = $this->createMock(Security::class);
        $security->method('getUser')
            ->willReturn($user);

        $loginService = new LoginFromPrado(
            $authenticator,
            $formAuthenticator,
            $em,
            $security
        );

        $expectedUser = $loginService->loginByUserName('AdminOrme');

        $this->assertInstanceOf(Agent::class, $expectedUser);
        $this->assertEquals('AdminOrme', $expectedUser->getLogin());
    }
    public function testLoginByUsernameNotInSession()
    {
        $user = new Agent();
        $user->setLogin('AdminOrme');
        $authenticator = $this->createMock(GuardAuthenticatorHandler::class);
        $formAuthenticator = $this->createMock(FormAuthenticator::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $repository = $this->createMock(AgentRepository::class);
        $repository->method('findOneBy')
            ->willReturn($user);
        $em->method('getRepository')
            ->willReturn($repository);
        $security = $this->createMock(Security::class);
        $security->method('getUser')
            ->willReturnOnConsecutiveCalls(null, $user);

        $loginService = new LoginFromPrado(
            $authenticator,
            $formAuthenticator,
            $em,
            $security
        );

        $expectedUser = $loginService->loginByUserName('AdminOrme');

        $this->assertInstanceOf(Agent::class, $expectedUser);
        $this->assertEquals('AdminOrme', $expectedUser->getLogin());
    }
}
