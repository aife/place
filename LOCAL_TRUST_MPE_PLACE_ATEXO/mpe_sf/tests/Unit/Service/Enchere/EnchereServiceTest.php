<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Enchere;

use DateTime;
use App\Entity\EnchereEntreprisePmi;
use App\Entity\EnchereOffre;
use App\Repository\Enchere\EnchereEntreprisePmisRepository;
use App\Repository\Enchere\EnchereOffreRepository;
use App\Service\Enchere\EnchereService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class EnchereServiceTest extends TestCase
{
    public function testGetDataPrix()
    {
        $data = [
            "Atexal" => [ "2017-01-28T16:57:05.000000" => 10000.0 ]
        ];
        $emMock = $this->createMock(EntityManagerInterface::class);

        $enchereEntreprisePmis =  new EnchereEntreprisePmi();
        $enchereEntreprisePmis->setId(1);
        $enchereEntreprisePmis->setNom('Atexal');
        $collectionEnchereEntreprisePmis = [];
        $collectionEnchereEntreprisePmis[] = $enchereEntreprisePmis;
        $mockEnchereEntreprisePmisRepository = $this->createMock(EnchereEntreprisePmisRepository::class);
        $mockEnchereEntreprisePmisRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($collectionEnchereEntreprisePmis);

        $enchereOffre =  new EnchereOffre();
        $enchereOffre->setId(1);
        $date = new DateTime('2017-01-28T16:57:05.000000');
        $enchereOffre->setDate($date);
        $enchereOffre->setValeurtc('10000.0');
        $collectionEnchereOffre = [];
        $collectionEnchereOffre[] = $enchereOffre;
        $mockEnchereOffreRepository = $this->createMock(EnchereOffreRepository::class);
        $mockEnchereOffreRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($collectionEnchereOffre);


        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($mockEnchereEntreprisePmisRepository, $mockEnchereOffreRepository);


        $enchereService = new EnchereService($emMock);
        $enchere = $enchereService->getData('12');
        $this->assertEquals($data, $enchere);
    }

    public function testGetDataNotes()
    {
        $data = [
            "Atexal" => [ "2017-01-28T16:57:05.000000" => 8 ]
        ];
        $emMock = $this->createMock(EntityManagerInterface::class);

        $enchereEntreprisePmis =  new EnchereEntreprisePmi();
        $enchereEntreprisePmis->setId(1);
        $enchereEntreprisePmis->setNom('Atexal');
        $collectionEnchereEntreprisePmis = [];
        $collectionEnchereEntreprisePmis[] = $enchereEntreprisePmis;
        $mockEnchereEntreprisePmisRepository = $this->createMock(EnchereEntreprisePmisRepository::class);
        $mockEnchereEntreprisePmisRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($collectionEnchereEntreprisePmis);

        $enchereOffre =  new EnchereOffre();
        $enchereOffre->setId(1);
        $date = new DateTime('2017-01-28T16:57:05.000000');
        $enchereOffre->setDate($date);
        $enchereOffre->setValeurngc('8');
        $collectionEnchereOffre = [];
        $collectionEnchereOffre[] = $enchereOffre;
        $mockEnchereOffreRepository = $this->createMock(EnchereOffreRepository::class);
        $mockEnchereOffreRepository->expects($this->once())
            ->method('findBy')
            ->willReturn($collectionEnchereOffre);


        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($mockEnchereEntreprisePmisRepository, $mockEnchereOffreRepository);


        $enchereService = new EnchereService($emMock);
        $enchere = $enchereService->getData('12', 'notes');
        $this->assertEquals($data, $enchere);
    }
}
