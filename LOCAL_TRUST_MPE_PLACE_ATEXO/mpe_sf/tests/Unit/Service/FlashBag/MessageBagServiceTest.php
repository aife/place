<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\FlashBag;

use App\Service\FlashBag\MessageBagService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageBagServiceTest extends TestCase
{
    /**
     * @param string $parameter
     * @param string|bool|null $value
     * @param string $tradLabel
     * @param string $typeMessage
     * @param int $occurence
     * @param bool $isErreurDelaiDlro
     * @return void
     *
     * @dataProvider providerParametersData
     */
    public function testDisplayFlashMessages(
        string $parameter,
        ?string $value,
        string $tradLabel,
        string $typeMessage,
        int $occurence,
        bool $isErreurDelaiDlro = false
    ): void {
        $translator = $this->createMock(TranslatorInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $session = $this->createMock(Session::class);
        $flashBag = $this->createMock(FlashBagInterface::class);

        $messageBagService = new MessageBagService($translator, $parameterBag, $session);

        $translator
            ->expects(self::once())
            ->method('trans')
            ->with($tradLabel)
            ->willReturn($tradLabel)
        ;

        if ($isErreurDelaiDlro) {
            $parameterBag
                ->expects(self::exactly($occurence))
                ->method('get')
                ->with('DELAI_DLRO')
            ;
        }

        $flashBag
            ->expects(self::exactly($occurence))
            ->method('add')
            ->with($typeMessage, $tradLabel)
        ;

        $session
            ->expects(self::exactly($occurence))
            ->method('getFlashBag')
            ->willReturn($flashBag)
        ;

        $messageBagService->displayFlashMessages([$parameter => $value]);
    }

    /**
     * @return array[]
     */
    public function providerParametersData(): array
    {
        return [
            'save=null' => ['save', null, 'TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION', 'success', 0],
            'save=false' => ['save', 'false', 'TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION', 'success', 0],
            'save=true' => ['save', 'true', 'TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION', 'success', 1],

            'withoutDce' => ['withoutDce', null, "MSG_AVERTISSEMENT_AUCUN_DCE_CONSULTATION", 'warning', 1],
            'etatDateFin' => ['etatDateFin', null, "MSG_AVERTISSEMENT_DATE_FIN", 'warning', 1],
            'dateFinContratInvalid' => ['dateFinContratInvalid', null, "DATE_FIN_CONTRAT_DEPASSEE", 'warning', 1],
            'siretInv' => ['siretInvalid', null, "MSG_CONTROLE_SIRET_ACHETEUR_TABLEAU_BORD_CONSULTATION", 'warning', 1],
            'EchecPub' => ['EchecPub', null, "MESSAGE_ECHEC_ENVOIE_DEPUIS_VALIDER_CONSULTATION", 'warning', 1],
            'ArchiveExpirer' => ['ArchiveExpirer', null, "TELECHARGEMENT_EXPIRER", 'warning', 1],

            'noCheckedEnv' => ['noCheckedEnv', null, "TEXT_CHOISIR_TYPE_ENVELOPPE", 'warning', 0],
            'noCheckedEnv=1' => ['noCheckedEnv', '1', "TEXT_CHOISIR_TYPE_ENVELOPPE", 'warning', 1],
            'noAvisGenerated' => ['noAvisGenerated', null, "TEXT_PROBLEME_GENERATION_AVIS_PDF", 'warning', 0],
            'noAvisGenerated=1' => ['noAvisGenerated', '1', "TEXT_PROBLEME_GENERATION_AVIS_PDF", 'warning', 1],

            'etatDateCreationDateFin' => ['etatDateCreationDateFin', null, "ERREUR_DELAI_DLRO", 'warning', 1, true],
        ];
    }

    public function testDisplayFlashMessagesWhenParamNotValid(): void
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $session = $this->createMock(Session::class);
        $flashBag = $this->createMock(FlashBagInterface::class);

        $messageBagService = new MessageBagService($translator, $parameterBag, $session);

        $parameter = 'paramNotValid';
        $value = null;

        $translator
            ->expects(self::never())
            ->method('trans')
        ;

        $parameterBag
            ->expects(self::never())
            ->method('get')
        ;

        $flashBag
            ->expects(self::never())
            ->method('add')
        ;

        $session
            ->expects(self::never())
            ->method('getFlashBag')
        ;

        $messageBagService->displayFlashMessages([$parameter => $value]);
    }
}
