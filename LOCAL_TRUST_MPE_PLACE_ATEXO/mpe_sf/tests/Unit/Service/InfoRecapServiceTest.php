<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\EnveloppeFichier;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Repository\EnveloppeFichierRepository;
use App\Repository\OffreRepository;
use App\Repository\TCandidatureRepository;
use App\Service\AtexoConfiguration;
use App\Service\Dume\DumeService;
use App\Service\GeolocalisationService;
use App\Service\InfoRecapService;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class InfoRecapServiceTest extends TestCase
{
    public function testGetDepotContenusTransmis()
    {
        $emMock = $this->createMock(EntityManagerInterface::class);
        $configurationMock = $this->createMock(AtexoConfiguration::class);
        $geolocalisationService = $this->createMock(GeolocalisationService::class);
        $tokenStorageMock = $this->createMock(TokenStorageInterface::class);
        $dumeServiceMock = $this->createMock(DumeService::class);
        $envMock = $this->createMock(Environment::class);
        $tokenMock = $this->createMock(PostAuthenticationGuardToken::class);

        $user = new Inscrit();
        $user->setId(1);
        $user->setNom('LOCAL TRUST');
        $user->setSiret('444914568');
        $user->setAdresse('231 RUE DU MARCHE ST HONORE');

        $entreprise = new Entreprise();
        $entreprise->setNom('ATC');

        $user->setEntreprise($entreprise);

        $tokenMock->expects($this->once())
            ->method('getUser')
            ->willReturn($user)
            ;

        $tokenStorageMock->expects($this->once())
            ->method('getToken')
            ->willReturn($tokenMock)
            ;

        $emMock->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Offre::class === $repository) {
                        $repository = $this->createMock(OffreRepository::class);

                        $repository->expects($this->once())
                            ->method('getOffresForContenusTransmis')
                            ->willReturn([]);

                        return $repository;
                    }
                    if (TCandidature::class === $repository) {
                        $repository = $this->createMock(TCandidatureRepository::class);

                        $repository->expects($this->once())
                            ->method('getCandidaturesByRole')
                            ->willReturn([]);

                        return $repository;
                    }
                    if (EnveloppeFichier::class === $repository) {
                        $repository = $this->createMock(EnveloppeFichierRepository::class);

                        $repository->expects($this->once())
                            ->method('getListeFichiersDepot')
                            ->willReturn([]);

                        return $repository;
                    }
                }
            );

        $parameterBag = $this->createMock(Encryption::class);
        $infoservice = new InfoRecapService(
            $emMock,
            $configurationMock,
            $geolocalisationService,
            $tokenStorageMock,
            $dumeServiceMock,
            $envMock,
            $parameterBag,
            [
                'STATUT_ENV_BROUILLON' => '99',
                'STATUT_DUME_CONTEXTE_VALIDE' => '1',
                'TYPE_CANDIDATUE_DUME' => 'dume',
                'TYPE_CANDIDATUE_DUME_ONLINE' => 'online',
                'PF_URL_MPE' => '/',
                'URL_MPE' => '/',
                'type_enveloppe' => []
            ]
        );

        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-10');
        $consulation = new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };
        $consulation->setOrganisme($organisme);

        $response = $infoservice->getDepotContenusTransmis($consulation);

        $this->assertIsString($response);
    }
}
