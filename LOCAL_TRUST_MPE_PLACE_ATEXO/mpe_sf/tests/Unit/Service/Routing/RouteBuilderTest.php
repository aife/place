<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Routing;

use App\Service\Routing\RouteBuilder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RouteBuilderTest extends TestCase
{
    public function testGetRoute(): void
    {
        $router = $this->createMock(UrlGeneratorInterface::class);

        $routeBuilder = new RouteBuilder($router);

        $router
            ->expects(self::once())
            ->method('generate')
            ->with('consultation_search_index')
            ->willReturn('/path?id=5')
        ;

        self::assertSame($routeBuilder->getRoute('consultation_search_index', ['id' => 5]), '/path?id=5');
    }
}
