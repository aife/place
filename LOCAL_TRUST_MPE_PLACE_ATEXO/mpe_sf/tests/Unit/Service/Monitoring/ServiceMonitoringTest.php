<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Monitoring;

use App\Repository\ConsultationRepository;
use App\Service\AbstractService;
use App\Service\Agent\StatisticsService;
use App\Service\AtexoDumeService;
use App\Service\Monitoring\DatabaseMonitoring;
use App\Service\Monitoring\DumeMonitoring;
use App\Service\Monitoring\FilesystemMonitoring;
use App\Service\Monitoring\PubMonitoring;
use App\Service\Publicite\EchangeConcentrateur;
use App\Utils\Filesystem\MountManager;
use Atexo\CryptoBundle\AtexoCrypto;
use App\Service\Bandeau\Menu\Agent\Submenus\Sourcing;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Service\Monitoring\BiMonitoring;
use App\Service\Monitoring\ChorusMonitoring;
use App\Service\Monitoring\CryptoMonitoring;
use App\Service\Monitoring\EnvolMonitoring;
use App\Service\Monitoring\MessecMonitoring;
use App\Service\Monitoring\MolMonitoring;
use App\Service\Monitoring\RecensementMonitoring;
use App\Service\Monitoring\RedacMonitoring;
use App\Service\Monitoring\RessourcesMonitoring;
use App\Service\Monitoring\SourcingMonitoring;
use App\Service\WebServicesRecensement;
use App\Service\WebServicesSourcing;
use AtexoDume\Client\Client as ClientDume;
use AtexoDume\Service\AuthentificationService;
use League\Flysystem\FilesystemInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Stopwatch\StopwatchEvent;

class ServiceMonitoringTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */

    public function testCheck($service, $dependencies)
    {
        $service = new $service(...$dependencies);
        $response = $service->check();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('externalModule', $response);
        $this->assertEquals('OK', $response['externalModule']['status']);
    }

    /**
     * @dataProvider dataProviderFailed
     */
    public function testCheckFailed($service, $dependencies)
    {
        $service = new $service(...$dependencies);
        $response = $service->check();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('externalModule', $response);
        $this->assertEquals('KO', $response['externalModule']['status']);
    }

    private function dataProvider(): array
    {
        return [
            [ChorusMonitoring::class, $this->createHttpMock('', ['http_code' => 200])],
            [EnvolMonitoring::class, $this->createHttpMock('', ['http_code' => 415])],
            [MolMonitoring::class, $this->createHttpMock('', ['http_code' => 200])],
            [RedacMonitoring::class, $this->createHttpMock('<xml><code>4</code></xml>', ['http_code' => 200])],
            [RessourcesMonitoring::class, $this->createHttpMock('', ['http_code' => 200])],
            [BiMonitoring::class, $this->createAbstractServiceMock(StatisticsService::class, 200)],
            [MessecMonitoring::class, $this->createAbstractServiceMock(WebServicesMessagerie::class, 200)],
            [RecensementMonitoring::class, $this->createAbstractServiceMock(WebServicesRecensement::class, 200)],
            [SourcingMonitoring::class, $this->createAbstractServiceMock(WebServicesSourcing::class, 200)],
            [CryptoMonitoring::class, $this->createMockForCrypto('{"code": 201}')],
            [DumeMonitoring::class, $this->createMockForDume()],
            [PubMonitoring::class, $this->createMockForPub('ghdtyd--d-ghhgdèè-')],
        ];
    }

    private function dataProviderFailed(): array
    {
        return [
            [EnvolMonitoring::class, $this->createHttpMock('', ['http_code' => 500])],
            [MolMonitoring::class, $this->createHttpMock('', ['http_code' => 500])],
            [RedacMonitoring::class, $this->createHttpMock('<xml><code>9</code></xml>', ['http_code' => 500])],
            [RessourcesMonitoring::class, $this->createHttpMock('', ['http_code' => 500])],
            [BiMonitoring::class, $this->createAbstractServiceMock(StatisticsService::class, 500)],
            [MessecMonitoring::class, $this->createAbstractServiceMock(WebServicesMessagerie::class, '')],
            [RecensementMonitoring::class, $this->createAbstractServiceMock(WebServicesRecensement::class, 500)],
            [SourcingMonitoring::class, $this->createAbstractServiceMock(WebServicesSourcing::class, 500)],
            [CryptoMonitoring::class, $this->createMockForCrypto('{"code": "500"}')],
            [PubMonitoring::class, $this->createMockForPub('')],
        ];
    }

    private function createHttpMock(string $responseBody, array $responseHeaders)
    {
        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->method('get')->willReturn('url');
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        return [
            new MockHttpClient(new MockResponse($responseBody, $responseHeaders)),
            $parameter,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        ];
    }

    private function createAbstractServiceMock(string $service, string | int $response)
    {
        $service = $this->createMock($service);
        $service->method('getContent')->willReturn($response);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        return [
            $service,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        ];
    }

    private function createMockForCrypto(string $response): array
    {
        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->method('get')->willReturn('url');
        $service = $this->createMock(AtexoCrypto::class);
        $service->method('verifierDisponibilite')->willReturn($response);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        return [
            $service,
            $parameter,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        ];
    }

    private function createMockForDume()
    {
        $parameter = $this->createMock(ParameterBagInterface::class);
        $parameter->method('get')->willReturn('url');
        $authenticationService = $this->createMock(AuthentificationService::class);
        $authenticationService->method('login')->willReturn('');
        $client = $this->createMock(ClientDume::class);
        $client->method('authentificationService')->willReturn($authenticationService);
        $service = $this->createMock(AtexoDumeService::class);
        $service->method('createConnexionLtDume')->willReturn($client);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        return [
            $service,
            $parameter,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        ];
    }

    public function createMockForPub($response)
    {
        $service = $this->createMock(EchangeConcentrateur::class);
        $service->method('getToken')->willReturn($response);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        return [
            $service,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        ];
    }

    public function testMonitoringDatabase()
    {
        $repository = $this->createMock(ConsultationRepository::class);
        $repository->method('findBy')->willReturn([]);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        $service = new DatabaseMonitoring(
            $repository,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        );

        $response = $service->check();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('database', $response);
        $this->assertEquals('OK', $response['database']['status']);
    }

    public function testMonitoringFilesystem()
    {
        $filesystem = $this->createMock(FilesystemInterface::class);
        $mount = $this->createMock(MountManager::class);
        $mount->method('getFilesystem')->willReturn($filesystem);
        $stopWatch = $this->createMock(Stopwatch::class);
        $stopWatch->method('getEvent')->willReturn(new StopwatchEvent(3444));

        $service = new FilesystemMonitoring(
            $mount,
            $stopWatch,
            $this->createMock(LoggerInterface::class)
        );

        $response = $service->check();

        $this->assertIsArray($response);
        $this->assertArrayHasKey('fileSystem', $response);
        $this->assertEquals('OK', $response['fileSystem']['status']);
    }

}
