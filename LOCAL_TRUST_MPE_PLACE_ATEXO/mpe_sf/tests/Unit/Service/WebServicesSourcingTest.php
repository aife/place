<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Service;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\AgentRepository;
use App\Security\ClientOpenIdConnect;
use App\Service\AbstractService;
use App\Service\AgentService;
use App\Service\AgentTechniqueTokenService;
use App\Service\CpvService;
use App\Service\OrganismeService;
use App\Service\WebServicesRecensement;
use App\Service\WebServicesSourcing;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesSourcingTest extends TestCase
{
    /**
     * @var Agent
     */
    private $agent;

    protected function setUp(): void
    {
        $this->agent = new Agent();
        $organisme = $this->createMock(Organisme::class);
        $service = $this->createMock(Service::class);

        $this->agent->setId(1);
        $this->agent->setLogin('testAdmin');
        $this->agent->setAcronymeOrganisme('pmi-min-1');
        $this->agent->setOrganisme($organisme);
        $this->agent->setService($service);
        $this->agent->setServiceId(null);
    }

    public function testGetUrlSourcingWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('h234DSEZ432', ['http_code' => 200])
        ]);
        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $result = $service->getContent('getUrlSourcing', $this->agent);

        $this->assertEquals(
            'https://sourcing-release.local-trust.com/agent/login/FDSERT43HYTD5432SDPMLHRTDSFF/h234DSEZ432',
            $result
        );
    }

    public function testGetUrlSourcingWith400()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 400]),
            new MockResponse('h234DSEZ432', ['http_code' => 200])
        ]);
        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $this->expectException(ClientException::class);
        $result = $service->getContent('getUrlSourcing', $this->agent);
    }

    public function testSearchDecpContractsWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('{
              "content": [
                {
                  "numeroIdentificationUnique": "226300010000152020M02100",
                  "attributaires": [
                    {
                        "typeIdentifiant": "SIRET",
                      "id": "77567227232200",
                      "denominationSociale": "CROIX ROUGE FRANCAISE"
                    }
                  ],
                  "modifications": [],
                  "dateNotification": "2020-01-01",
                  "dureeMax": 36,
                  "lieuExecution": {
                    "code": "63",
                    "type": "Code département",
                    "nom": "SEM DEPARTEMENT PUY DE DOME"
                  },
                  "montant": 100000,
                  "codeCPV": "80510000-2",
                  "acheteur": {
                    "id": "22630001000015",
                    "nom": "BUDGET DEPARTEMENTAL"
                  },
                  "formePrix": "Ferme",
                  "objet": "FORMATION DES ASSISTANTS MATERNELS",
                  "natureContrat": "Marché",
                  "procedurePassation": "Appel d\'offres ouvert"
                }
              ],
              "empty": false,
              "first": true,
              "last": false,
              "totalPages": 24774,
              "totalElements": 24774,
              "size": 1,
              "number": 0,
              "numberOfElements": 0
            }', ['http_code' => 200])
        ]);
        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $result = $service->getContent('SearchDecpContracts', $criteriaVo);

        $this->assertIsObject($result);
        $this->assertIsInt($result->totalElements);
        $this->assertIsArray($result->content);
    }

    public function testGetDecpContractWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('{
              "numeroIdentificationUnique": "20003019500115202018N0667-0003",
              "attributaires": [
                {
                  "typeIdentifiant": "SIRET",
                  "id": "33890589600019",
                  "denominationSociale": "SNA - PROSPERI"
                }
              ],
              "modifications": [
                {
                  "objet": "Modification montants/durée no1 suite à la reconduction expresse de la période no2",
                  "dateNotification": "2020-10-26+01:00",
                  "datePublication": null,
                  "dureeMois": "24",
                  "montant": null,
                  "attributaires": []
                }
              ],
              "dateNotification": "2020-04-09",
              "dureeMax": 48,
              "lieuExecution": {
                "code": "06364",
                "type": "Code postal",
                "nom": "NICE CEDEX 4"
              },
              "montant": 5540000,
              "codeCPV": "45232410-9",
              "acheteur": {
                "id": "20003019500115",
                "nom": "METROPOLE NICE COTE D\'AZUR"
              },
              "formePrix": "Révisable",
              "objet": "HD REHABILITATION DES OUVRAGES D\'ASSAINISSEMENT VISITABLES",
              "natureContrat": "Accord-cadre",
              "procedurePassation": "Procédure adaptée"
            }', ['http_code' => 200])
        ]);

        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $result = $service->getContent('GetDecpContract', $this->agent);

        $this->assertIsObject($result);
        $this->assertIsArray($result->modifications);
    }

    public function testGetDonneesEssentiellesEtalabFormatXmlWith200()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('{
              "content": [
                {
                  "numeroIdentificationUnique": "226300010000152020M02100",
                  "attributaires": [
                    {
                        "typeIdentifiant": "SIRET",
                        "id": "77567227232200",
                        "denominationSociale": "CROIX ROUGE FRANCAISE"
                    }
                  ],
                  "modifications": [],
                  "dateNotification": "2020-01-01",
                  "dureeMax": 36,
                  "lieuExecution": {
                    "code": "63",
                    "type": "Code département",
                    "nom": "SEM DEPARTEMENT PUY DE DOME"
                  },
                  "montant": 100000,
                  "codeCPV": "80510000-2",
                  "acheteur": {
                    "id": "22630001000015",
                    "nom": "BUDGET DEPARTEMENTAL"
                  },
                  "formePrix": "Ferme",
                  "objet": "FORMATION DES ASSISTANTS MATERNELS",
                  "natureContrat": "Marché",
                  "procedurePassation": "Appel d\'offres ouvert"
                }
              ],
              "empty": false,
              "first": true,
              "last": false,
              "totalPages": 24774,
              "totalElements": 24774,
              "size": 1,
              "number": 0,
              "numberOfElements": 0
            }', ['http_code' => 200])
        ]);

        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $result = $service->getContent('getDonneesEssentiellesEtalabFormatXml', $criteriaVo);

        $this->assertNotEmpty($result);
        $this->assertStringNotContainsString($result, 'xml');
    }

    public function testIsCompanyRegistered(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')->willReturn('https://sourcing-release.local-trust.com');
        $loger = $this->createMock(LoggerInterface::class);
        $cpvService = $this->createMock(CpvService::class);
        $client = new MockHttpClient([
            new MockResponse('{"access_token":"FDSERT43HYTD5432SDPMLHRTDSFF"}', ['http_code' => 200]),
            new MockResponse('{"email" : "email@atexo.com"}', ['http_code' => 200])
        ]);
        $organismeService = $this->createMock(OrganismeService::class);
        $agentService = $this->createMock(AgentService::class);
        $service = new WebServicesSourcing(
            $parameterBag,
            $cpvService,
            $client,
            $loger,
            $agentService,
            $organismeService
        );
        $result = $service->getContent('isCompanyRegistered', 'email@atexo.com');

        $this->assertInstanceOf(\stdClass::class, $result);
        $this->assertSame('email@atexo.com', $result->email);
    }
}
