<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Perimeter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Repository\InvitePermanentTransverseRepository;
use App\Service\Perimeter\PerimetreVisionService;
use App\Service\WebservicesMpeConsultations;
use App\Utils\EntityPurchase;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class PerimetreVisionServiceTest extends TestCase
{
    public function testIsFromSubResourceClassAgentApplyPerimeter(): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $request = new Request(
            query: ['applyPerimeterVisionConsultations' => 'true'],
            attributes: ['_api_resource_class' => Lot::class],
        );

        $requestStack
            ->expects(self::once())
            ->method('getCurrentRequest')
            ->willReturn($request)
        ;

        self::assertTrue($consultationService->isFromSubResourceClass(Agent::class));
    }

    public function testIsFromSubResourceClassAgentNotApplyPerimeter(): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $request = new Request(['applyPerimeterVisionConsultations' => 'false'], [], [], [], [], [], []);

        $requestStack
            ->expects(self::once())
            ->method('getCurrentRequest')
            ->willReturn($request)
        ;

        self::assertFalse($consultationService->isFromSubResourceClass(Agent::class));
    }

    public function testIsFromSubResourceClassAgentAll(): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $request = new Request([], [], [], [], [], [], []);

        $requestStack
            ->expects(self::once())
            ->method('getCurrentRequest')
            ->willReturn($request)
        ;

        self::assertFalse($consultationService->isFromSubResourceClass(Agent::class));
    }

    public function testIsFromSubResourceClassLot(): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $request = new Request([], [], [], [], [], [], []);

        $requestStack
            ->expects(self::once())
            ->method('getCurrentRequest')
            ->willReturn($request)
        ;

        self::assertTrue($consultationService->isFromSubResourceClass(Lot::class));
    }

    public function testIsFromSubResourceClassConsultation(): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $request = new Request([], [], [], [], [], [], []);

        $requestStack
            ->expects(self::once())
            ->method('getCurrentRequest')
            ->willReturn($request)
        ;

        self::assertFalse($consultationService->isFromSubResourceClass(Consultation::class));
    }

    public function testGetAgentsCreateurInPerimeterVision(): void
    {
        $requestStack = $this->createMock(RequestStack::class);

        $wsMpe = new class (
            $this->createMock(HttpClientInterface::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(JWTTokenManagerInterface::class),
        ) extends WebservicesMpeConsultations {
            protected function searchAgents(
                array $query = ['applyPerimeterVisionConsultations' => true, 'itemsPerPage' => 300]
            ): mixed {
                $object1 = new \stdClass();
                $object1->{'0'} = new \stdClass();
                $object1->{'id'} = 12;
                $object1->{'libelleDetail'} = 'Jackie Chan';

                $object2 = new \stdClass();
                $object2->{'0'} = new \stdClass();
                $object2->{'id'} = 24;
                $object2->{'libelleDetail'} = 'Jet Lee';

                $returnFromApiPlatform = new \stdClass();
                $returnFromApiPlatform->{'hydra:member'} = [
                    $object1, $object2
                ];

                return $returnFromApiPlatform;
            }
        };

        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $result = $consultationService->getAgentsCreateurInPerimeterVision();

        self::assertSame($result[0], ['id' => 12, 'text' => 'Jackie Chan']);
        self::assertSame($result[1], ['id' => 24, 'text' => 'Jet Lee']);
    }

    /**
     * @dataProvider dataProviderServices
     */
    public function testGetServicesInPerimeterVision(HabilitationAgent $habilitationAgent, array $expectedResult): void
    {
        $requestStack = $this->createMock(RequestStack::class);
        $wsMpe = $this->createMock(WebservicesMpeConsultations::class);
        $entityPurchase = $this->createMock(EntityPurchase::class);
        $invitePermanentTransverseRepository = $this->createMock(InvitePermanentTransverseRepository::class);

        $consultationService = new PerimetreVisionService(
            $requestStack,
            $wsMpe,
            $entityPurchase,
            $invitePermanentTransverseRepository,
        );

        $organisme = (new Organisme())->setAcronyme('pmi-min-1');

        $agent = new Agent();
        $agent->setId(11362)->setOrganisme($organisme)->setHabilitation($habilitationAgent);

        $entityPurchase
            ->expects(self::once())
            ->method('retrieveAllChildrenServices')
            ->with(0, 'pmi-min-1', true)
            ->willReturn([2, 3, 4, 5, 6, 7, 8, 9])
        ;

        if ($habilitationAgent->getInvitePermanentTransverse()) {
            $invitePermanentTransverseRepository
                ->expects(self::once())
                ->method('getServicesIdByAgentId')
                ->with(11362)
                ->willReturn([456, 789, 132])
            ;
        } else {
            $invitePermanentTransverseRepository
                ->expects(self::never())
                ->method('getServicesIdByAgentId')
                ->with(11362)
            ;
        }

        self::assertSame($expectedResult, $consultationService->getServicesInPerimeterVision($agent));
    }

    private function dataProviderServices(): array
    {
        $habilitationMonEntite = new HabilitationAgent();
        $habilitationMonEntite->setInvitePermanentMonEntite(true);

        $habilitationEntiteDependante = new HabilitationAgent();
        $habilitationEntiteDependante->setInvitePermanentEntiteDependante(true);

        $habilitationPermanentTrans = new HabilitationAgent();
        $habilitationPermanentTrans->setInvitePermanentTransverse(true);

        $habilitationAll = new HabilitationAgent();
        $habilitationAll->setInvitePermanentMonEntite(true);
        $habilitationAll->setInvitePermanentEntiteDependante(true);
        $habilitationAll->setInvitePermanentTransverse(true);

        return [
            "InvitePermanentMonEntite" => [$habilitationMonEntite, [0]],
            "InvitePermanentEntiteDependante" => [$habilitationEntiteDependante, [2, 3, 4, 5, 6, 7, 8, 9]],
            "InvitePermanentTransverse" => [$habilitationPermanentTrans, [132, 456, 789]],
            "All" => [$habilitationAll, [0, 2, 3, 4, 5, 6, 7, 8, 9, 132, 456, 789]],
        ];
    }
}
