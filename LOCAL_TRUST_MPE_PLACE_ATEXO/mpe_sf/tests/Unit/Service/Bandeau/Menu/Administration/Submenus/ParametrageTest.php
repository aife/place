<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\Parametrage;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class ParametrageTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Parametrage::class;
    }

    public function canSeeProvider(): array
    {
        return [
            //submenu plateforme
            ['PLATEFORME', [], [], [],[], false],
            ['PLATEFORME', [], [], ['hyper_admin'], [],true],
            ['ADMINISTRATION_MESSACE_ACCUEIL', [], [], ['hyper_admin'],[], true],
            ['GESTION_ORGANISME', [], [], [], [],false],
            ['GESTION_ORGANISME', ['gestion_organismes'], [], ['hyper_admin'],[], true],
            ['ADMINISTRATEUR_AGENT', [], [], [], [],false],
            ['ADMINISTRATEUR_AGENT', [], [], ['hyper_admin'],[], true],
            ['PLATEFORME_GESTION_SERVICE', [], [], [],[], false],
            ['PLATEFORME_GESTION_SERVICE', ['gestion_organisme_par_agent'], [], ['hyper_admin'],[], true],
            ['GESTION_DOMAINE_ACTIVITE', [], [], [], [],false],
            ['GESTION_DOMAINE_ACTIVITE', ['consultation_domaines_activites'], [], ['hyper_admin'],[], true],
            ['PLATEFORME_SUIVI_ACCES', [], [], [], [], false],
            ['PLATEFORME_SUIVI_ACCES', [], [], ['hyper_admin'], [], true],
            ['AUTOFORMATION_AGENT', [], [], [], [], false],
            ['AUTOFORMATION_AGENT', ['modules_autoformation'], [], ['hyper_admin'], [], true],
            ['AUTOFORMATION_ENTREPRISE', [], [], [], [], false],
            ['AUTOFORMATION_ENTREPRISE', ['modules_autoformation'], [], ['hyper_admin'], [], true],
            //submenu entite publique
            ['ENTITE_PUBLIQUE', [], [], [], [],true],
            ['CATEGORIE_MAPPA', [], [], [],[], false],
            ['CATEGORIE_MAPPA', ['procedure_adaptee'], [], [], [],true],
            ['PARAMETRAGE_PROCEDURES', [], [], [],[], true],
            ['REGLES_VALIDATION', [], [], [], [],true],
            ['COMPTES_JAL', [], [], [], [],true],
            ['SUIVI_ACCES', [], [], [], [],true],
            //submenu service
            ['SERVICE', [], [], [], [],true],
            ['GESTION_SERVICE', [], [], [],[], true],
            ['GESTION_AGENTS', [], [], [],[], true],
            ['GESTION_HABILITATIONS', [], [], [],[], true],
            ['GESTION_COMPTES_BOAMP', [], [], [], [],false],
            ['GESTION_COMPTES_BOAMP', ['publicite_format_xml'], [], [],[], true],
            ['GESTION_COMPTES_MONITEUR', [], [], [], [], false],
            ['GESTION_COMPTES_MONITEUR', ['publicite_format_xml'], [], [],[], true],
            ['SERVICE_COMPTES_JAL', [], [], [], [],true],
            ['ADRESSE_FACTURATION_JAL', [], [], [], [],false],
            ['ADRESSE_FACTURATION_JAL', ['gestion_adresses_facturation_jal'], [], [], [],true],
            ['COMPTE_CENTRALE_PUBLICATION', [], [], [], [],false],
            ['COMPTE_CENTRALE_PUBLICATION', [], ['centrale_publication'], [], [],true],
            ['GESTION_FAVORIS_PUBLICITE', [], [], [], [],false],
            ['GESTION_FAVORIS_PUBLICITE', ['publicite'], [], [],[], true],
            ['CLES_CHIFFREMENT', [], [], [], [],true],
            ['FOURNISSEUR_DOCUMENT', [], [], [], [],true],
            ['GESTION_MANDATAIRE', [], [], [], [],false],
            ['GESTION_MANDATAIRE', [], ['gestion_mandataire'], [], [],true],
            ['PARAMETRAGE_ADRESSE', [], [], [], [],false],
            ['PARAMETRAGE_ADRESSE', ['gerer_adresses_service'], [], [], [],true],
            ['GERER_INSCRIPTION_FOURNISSEUR', [], [], [], [],false],
            ['GERER_INSCRIPTION_FOURNISSEUR', ['gestion_entreprise_par_agent'], [], [],[], true],
            //submenu newsletter
            ['NEWSLETTER', [], [], [],[], false],
            ['NEWSLETTER', ['gestion_newsletter'], [], [], [],true],
            ['MESSAGE_AGENTS', [], [], [], [],false],
            ['MESSAGE_AGENTS', ['gestion_newsletter'], [], [],[], true],
            ['MESSAGE_REDACTEURS', [], [], [], [],false],
            ['MESSAGE_REDACTEURS', ['gestion_newsletter'], ['interface_module_rsem'], [],[], true],
            //submenu certificat electronique
            ['CERTIFICAT_ELECTRONIQUE', [], [], [],[], false],
            ['CERTIFICAT_ELECTRONIQUE', ['gestion_entreprise_par_agent'], [], [],[], true],
            ['CERTIFICAT_ELECTRONIQUE', ['gerer_certificats_agent'], [], [],[], true],
            ['GESTION_CERTIFICAT_FOURNISSEUR', [], [], [], [],false],
            ['GESTION_CERTIFICAT_FOURNISSEUR', ['gestion_entreprise_par_agent'], [], [],[], true],
            ['GESTION_CERTIFICAT_AGENT', [], [], [], [],false],
            ['GESTION_CERTIFICAT_AGENT', ['gerer_certificats_agent'], [], [], [],true],
            ['LISTE_SIRET_ACHETEUR', [], ['extraction_siret_acheteur'], [], [],true],
            ['DOCUMENT_MODELE', [], [], [], [], false],
            ['DOCUMENT_MODELE', [], ['module_administration_document'], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            //submenu plateforme
            ['ADMINISTRATION_MESSACE_ACCUEIL', [], [], [], [],false],
            ['ADMINISTRATION_MESSACE_ACCUEIL', [], [], ['hyper_admin', 'gerer_messages_accueil'], [],true],
            ['GESTION_ORGANISME', [], [], [], [],false],
            ['GESTION_ORGANISME', [], [], ['hyper_admin', 'gerer_organismes'],[], true],
            ['ADMINISTRATEUR_AGENT', [], [], [],[], false],
            ['ADMINISTRATEUR_AGENT', [], [], ['hyper_admin'],[], true],
            ['PLATEFORME_GESTION_SERVICE', [], [], [], [],false],
            ['PLATEFORME_GESTION_SERVICE', [], [], ['hyper_admin'], [],true],
            ['GESTION_DOMAINE_ACTIVITE', [], [], [],[], false],
            ['GESTION_DOMAINE_ACTIVITE', [], [], ['hyper_admin'], [], true],
            ['PLATEFORME_SUIVI_ACCES', [], [], [], [], false],
            ['PLATEFORME_SUIVI_ACCES', [], [], ['hyper_admin','suivi_acces'], [], true],
            ['AUTOFORMATION_AGENT', [], [], [], [], false],
            ['AUTOFORMATION_AGENT', [], [], ['module_autoformation'], [], true],
            ['AUTOFORMATION_ENTREPRISE', [], [], [], [], false],
            ['AUTOFORMATION_ENTREPRISE', [], [], ['module_autoformation'], [], true],
            //submenu entite publique
            ['ENTITE_PUBLIQUE', [], [], [],[], false],
            ['CATEGORIE_MAPPA', [], [], [], [],false],
            ['CATEGORIE_MAPPA', [], [], ['gestion_mapa'],[], true],
            ['PARAMETRAGE_PROCEDURES', [], [], ['administrer_procedure'],[], true],
            ['REGLES_VALIDATION', [], [], ['gestion_type_validation'], [],true],
            ['COMPTES_JAL', [], [], ['gestion_compte_jal'], [],true],
            ['COMPTES_JAL', [], [], ['administrer_adresses_facturation_jal'], [],true],
            ['SUIVI_ACCES', [], [], ['suivi_acces'], [],true],
            //submenu service
            ['SERVICE', [], [], [], [],false],
            ['GESTION_SERVICE', [], [], [], [],false],
            ['GESTION_SERVICE', [], [], ['gestion_agent_pole'], [],true],
            ['GESTION_AGENTS', [], [], [], [],false],
            ['GESTION_AGENTS', [], [], ['gestion_agents'], [],true],
            ['GESTION_HABILITATIONS', [], [], [], [],false],
            ['GESTION_HABILITATIONS', [], [], ['gestion_habilitations'], [],true],
            ['GESTION_COMPTES_BOAMP', [], [], [], [],false],
            ['GESTION_COMPTES_BOAMP', [], [], ['gestion_compte_boamp'],[], true],
            ['GESTION_COMPTES_MONITEUR', [], [], [], [],false],
            ['GESTION_COMPTES_MONITEUR', [], [], ['gestion_compte_groupe_moniteur'],[], true],
            ['SERVICE_COMPTES_JAL', [], [], [],[], false],
            ['SERVICE_COMPTES_JAL', [], [], ['gestion_compte_jal'],[], true],
            ['ADRESSE_FACTURATION_JAL', [], [], [], [],false],
            ['ADRESSE_FACTURATION_JAL', [], [], ['gestion_adresses_facturation_jal'],[], true],
            ['COMPTE_CENTRALE_PUBLICATION', [], [], [], [],false],
            ['COMPTE_CENTRALE_PUBLICATION', [], [], ['gestion_centrale_pub'],[], true],
            ['GESTION_FAVORIS_PUBLICITE', [], [], [], [],true],
            ['CLES_CHIFFREMENT', [], [], [],[], false],
            ['CLES_CHIFFREMENT', [], [], ['gestion_bi_cles'],[], true],
            ['FOURNISSEUR_DOCUMENT', [], [], [],[], false],
            ['FOURNISSEUR_DOCUMENT', [], [], ['gestion_fournisseurs_envois_postaux'],[], true],
            ['GESTION_MANDATAIRE', [], [], [], [],false],
            ['GESTION_MANDATAIRE', [], [], ['gestion_mandataire'],[], true],
            ['PARAMETRAGE_ADRESSE', [], [], [], [],false],
            ['PARAMETRAGE_ADRESSE', [], [], ['gerer_adresses_service'], [],true],
            ['GERER_INSCRIPTION_FOURNISSEUR', [], [], [], [],false],
            ['GERER_INSCRIPTION_FOURNISSEUR', [], [], ['gerer_les_entreprises'], [],true],
            //submenu newsletter
            ['NEWSLETTER', [], [], [], [],false],
            ['MESSAGE_AGENTS', [], [], [], [],false],
            ['MESSAGE_AGENTS', [], [], ['gerer_newsletter'],[], true],
            ['MESSAGE_REDACTEURS', [], [], [], [],false],
            ['MESSAGE_REDACTEURS', [], [], ['gerer_newsletter_redac'], [],true],
            //submenu certificat electronique
            ['CERTIFICAT_ELECTRONIQUE', [], [], [], [],false],
            ['GESTION_CERTIFICAT_FOURNISSEUR', [], [], [],[], false],
            ['GESTION_CERTIFICAT_FOURNISSEUR', [], [], ['gerer_les_entreprises'],[], true],
            ['GESTION_CERTIFICAT_AGENT', [], [], [],[], false],
            ['GESTION_CERTIFICAT_AGENT', [], [], ['gestion_certificats_agent'],[], true],
            ['LISTE_SIRET_ACHETEUR', [], [], ['telecharger_siret_acheteur'],[], true],
            ['DOCUMENT_MODELE', [], [], [], [], false],
            ['DOCUMENT_MODELE', [], [], ['administration_documents_modeles'], [], true],
        ];
    }
}
