<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\ClausierClient;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class ClausierClientTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return ClausierClient::class;
    }

    public function canSeeProvider(): array
    {
        return [
            ['CLAUSES_CLIENT', [], [], [], [],false],
            ['CLAUSES_CLIENT', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_CLIENT_RECHERCHER', [], [], [], [],false],
            ['CLAUSES_CLIENT_RECHERCHER', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_CLIENT_CREER', [], [], [], [],false],
            ['CLAUSES_CLIENT_CREER', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_CLIENT_EXPORTER', [], [], [], [],false],
            ['CLAUSES_CLIENT_EXPORTER', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_CLIENT_CANEVAS', [], [], [], [],false],
            ['CLAUSES_CLIENT_CANEVAS', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_CLIENT_CANEVAS_RECHERCHER', [], [], [], [],false],
            ['CLAUSES_CLIENT_CANEVAS_RECHERCHER', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_CLIENT_CANEVAS_CREER', [], [], [], [],false],
            ['CLAUSES_CLIENT_CANEVAS_CREER', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_CLIENT_GABARIT', [], [], [], [],false],
            ['CLAUSES_CLIENT_GABARIT', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_CLIENT_GABARIT_MODIFIER', [], [], [], [],false],
            ['CLAUSES_CLIENT_GABARIT_MODIFIER', [], ['interface_module_rsem'], [], [],true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['CLAUSES_CLIENT', [], [], [],[], false],
            ['CLAUSES_CLIENT_RECHERCHER', [], [], [],[], false],
            ['CLAUSES_CLIENT_RECHERCHER', [], [], ['administrer_clauses'],[], true],
            ['CLAUSES_CLIENT_RECHERCHER', [], [], ['valider_clauses'], [],true],
            ['CLAUSES_CLIENT_CREER', [], [], [], [],false],
            ['CLAUSES_CLIENT_CREER', [], [], ['administrer_clauses'],[], true],
            ['CLAUSES_CLIENT_CREER', [], [], ['valider_clauses'],[], true],
            ['CLAUSES_CLIENT_EXPORTER', [], [], [], [],false],
            ['CLAUSES_CLIENT_EXPORTER', [], [], ['administrer_clauses'],[], true],
            ['CLAUSES_CLIENT_EXPORTER', [], [], ['valider_clauses'], [],true],
            ['CLAUSES_CLIENT_CANEVAS', [], [], [], [],false],
            ['CLAUSES_CLIENT_CANEVAS_RECHERCHER', [], [], [],[], false],
            ['CLAUSES_CLIENT_CANEVAS_RECHERCHER', [], [], ['administrer_clauses'],[], true],
            ['CLAUSES_CLIENT_CANEVAS_RECHERCHER', [], [], ['valider_clauses'], [],true],
            ['CLAUSES_CLIENT_CANEVAS_CREER', [], [], [], [],false],
            ['CLAUSES_CLIENT_CANEVAS_CREER', [], [], ['administrer_clauses'],[], true],
            ['CLAUSES_CLIENT_CANEVAS_CREER', [], [], ['valider_clauses'],[], true],
            ['CLAUSES_CLIENT_GABARIT', [], [], [], [],false],
            ['CLAUSES_CLIENT_GABARIT_MODIFIER', [], [], [],[], false],
            ['CLAUSES_CLIENT_GABARIT_MODIFIER', [], [], ['gerer_gabarit'],[], true],
        ];
    }
}
