<?php

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\ClausesPersonnalisees;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class ClausesPersonnaliseesTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return ClausesPersonnalisees::class;
    }

    public function canSeeProvider(): array
    {
        return [
            ['CLAUSES_PERSO_ENTITE_ACHAT', [], [], [], [],false],
            ['CLAUSES_PERSO_ENTITE_ACHAT', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', [], [], [], [],false],
            ['CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_PERSO_ENTITE_ACHAT_GABARIT', [], [], [], [],false],
            ['CLAUSES_PERSO_ENTITE_ACHAT_GABARIT', [], ['interface_module_rsem'], [], [],true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['CLAUSES_PERSO_ENTITE_ACHAT', [], [], [], [],false],
            ['CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', [], [], [], [],false],
            ['CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', [], [], ['administrer_clauses_entite_achats'], [],true],
            ['CLAUSES_PERSO_ENTITE_ACHAT_GABARIT', [], [], [],[], false],
            ['CLAUSES_PERSO_ENTITE_ACHAT_GABARIT', [], [], ['gerer_gabarit_entite_achats'], [],true],
        ];
    }
}
