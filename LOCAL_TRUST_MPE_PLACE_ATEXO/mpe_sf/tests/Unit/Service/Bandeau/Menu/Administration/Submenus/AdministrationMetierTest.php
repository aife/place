<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\AdministrationMetier;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class AdministrationMetierTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return AdministrationMetier::class;
    }

    public function canSeeProvider(): array
    {
        return [
            ['ACTIONS_SPECIFIQUES', [], [], [], [],true],
            ['TELECHARGEMENT_PLIS_CHIFFRES', [], [], [], [],true],
            ['SUPPRESSION_PLIS_REFUSES', [], [], [], [],true],
            ['ACTIONS_ANNEXES', [], [], [], [],false],
            ['ACTIONS_ANNEXES', ['article_133_generation_pf'], [], [],[], true],
            ['ACTIONS_ANNEXES', ['statistiques_mesure_demat'], [], [],[], true],
            ['ACTIONS_ANNEXES', [], ['interface_chorus_pmi'], [],[], true],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', [], [], [], [], false],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', [], [], [], ['SPECIFIQUE_PLACE' => 1], false],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', ['article_133_generation_pf'], [], [],
                ['SPECIFIQUE_PLACE' => 1], false],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', ['article_133_generation_pf'], [], [],
                ['SPECIFIQUE_PLACE' => 0], true],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', ['article_133_generation_pf'], [], [], [], true],
            ['MESURES_DEMATERIALISATION', [], [], [], [],false],
            ['MESURES_DEMATERIALISATION', ['statistiques_mesure_demat'], [], [], [],true],
            ['ECHANGE_CHORUS', [], [], [],[], false],
            ['ECHANGE_CHORUS', [], ['interface_chorus_pmi'], [], [],true],
            ['SOCIETES_EXCLUES', [], [], [], [],false],
            ['SOCIETES_EXCLUES', ['menu_agent_societes_exclues'], [], [],[], true],
            ['SOCIETES_EXCLUES_CREER', [], [], [], [],false],
            ['SOCIETES_EXCLUES_CREER', ['menu_agent_societes_exclues'], [], [], [],true],
            ['SOCIETES_EXCLUES_RECHERCHE_AVANCEE', [], [], [],[], false],
            ['SOCIETES_EXCLUES_RECHERCHE_AVANCEE', ['menu_agent_societes_exclues'], [], [],[], true],
            ['GESTION_FICHIER_A_PUBLIER', [], [], [], [], false],
            ['GESTION_FICHIER_A_PUBLIER', ['article_133_upload_fichier'], [], [],[], true],
            ['ADMINISTRATION_SPASER', [], [], [], [], false],
            ['ADMINISTRATION_SPASER', [], ['acces_module_spaser'], [],[], false],
            ['ADMINISTRATION_SPASER', [], [], ['gestion_spaser_consultations'],[], false],
            ['ADMINISTRATION_SPASER', [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
            ['CONFIGURATION_SPASER', [], [], [], [], false],
            ['CONFIGURATION_SPASER', [], ['acces_module_spaser'], [],[], false],
            ['CONFIGURATION_SPASER', [], [], ['gestion_spaser_consultations'],[], false],
            ['CONFIGURATION_SPASER', [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], [], [], [], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], ['acces_module_spaser'], [],[], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], [], ['gestion_spaser_consultations'],[], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['ACTIONS_SPECIFIQUES', [], [], [],[], false],
            ['TELECHARGEMENT_PLIS_CHIFFRES', [], [], [],[], false],
            ['TELECHARGEMENT_PLIS_CHIFFRES', [], [], ['telechargement_groupe_anticipe_plis_chiffres'], [],true],
            ['SUPPRESSION_PLIS_REFUSES', [], [], [], [],false],
            ['SUPPRESSION_PLIS_REFUSES', [], [], ['supprimer_enveloppe'],[], true],
            ['ACTIONS_ANNEXES', [], [], [], [],false],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', [], [], [], [],false],
            ['PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', [], [], ['publication_marches'], [],true],
            ['MESURES_DEMATERIALISATION', [], [], [],[], true],
            ['ECHANGE_CHORUS', [], [], [], [],false],
            ['ECHANGE_CHORUS', [], [], ['suivi_flux_chorus_transversal'],[], true],
            ['SOCIETES_EXCLUES', [], [], [], [],false],
            ['SOCIETES_EXCLUES_CREER', [], [], [],[], false],
            ['SOCIETES_EXCLUES_CREER', [], [], ['portee_societes_exclues'],[], true],
            ['SOCIETES_EXCLUES_CREER', [], [], ['portee_societes_exclues_tous_organismes'], [],true],
            ['SOCIETES_EXCLUES_RECHERCHE_AVANCEE', [], [], [],[], true],
            ['GESTION_FICHIER_A_PUBLIER', [], [], [], [], false],
            ['GESTION_FICHIER_A_PUBLIER', [], [], ['publication_marches'], [], true],
            ['CONFIGURATION_SPASER', [], [], [], [], false],
            ['CONFIGURATION_SPASER', [], ['acces_module_spaser'], [],[], false],
            ['CONFIGURATION_SPASER', [], [], ['gestion_spaser_consultations'],[], false],
            ['CONFIGURATION_SPASER', [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], [], [], [], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], ['acces_module_spaser'], [],[], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], [], ['gestion_spaser_consultations'],[], false],
            ['SAISIE_INDICATEUR_TRANSVERSE', [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
        ];
    }
}
