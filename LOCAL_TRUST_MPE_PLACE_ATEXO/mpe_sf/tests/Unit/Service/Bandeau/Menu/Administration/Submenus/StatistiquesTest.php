<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\Statistiques;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class StatistiquesTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Statistiques::class;
    }

    public function canSeeProvider(): array
    {
        return [
            //submenu
            ['STATISTIQUES_PLATEFORME', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', [], [], [],[], false],
            ['STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', ['procedure_adaptee'], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE', [], [], [],[], false],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', ['publicite_format_xml'], [], ['hyper_admin'],[], true],
            ['STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE', [], [], [], [],false],
            [
                'STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE',
                ['bourse_cotraitance'],
                [],
                ['hyper_admin'],
                [],true,
            ],
            //submenu
            ['STATISTIQUES_ENTITE_PUBLIQUE', [], [], [], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', [], [], [], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', [], [], [], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE', [], [], [], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', [], [], [],[], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', ['publicite_format_xml'], [], [], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', ['procedure_adaptee'], [], [],[], true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', [], [], [], [], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', [], ['suivi_passation'], [], [], true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', [], [], [], [], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', [], ['suivi_passation'], [], [], true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS', [], [], [],[],  false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS', [], ['suivi_passation'], [], [], true],
            //submenu
            ['STATISTIQUES_REDAC', [], [], [], [], false],
            ['STATISTIQUES_REDAC', [], ['interface_module_rsem'], ['hyper_admin'],[], true],
            ['STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME', [], [], [],[], false],
            [
                'STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME',
                [],
                ['interface_module_rsem'],
                ['hyper_admin'], [],true,
            ],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION', [], [], [], [], false],[
                'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION',
                [],
                ['interface_module_rsem'],
                ['hyper_admin'],[], true,
            ],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', [], [], [],[], false],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', [], ['interface_module_rsem'], ['hyper_admin'], [],true],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE', [], [], [], [],false],
            [
                'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE',
                [],
                ['interface_module_rsem'],
                ['hyper_admin'],[], true,
            ],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', [], [], [], [],false],
            [
                'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT',
                [],
                ['interface_module_rsem'],
                ['hyper_admin'], [],true,
            ],
            ['STATISTIQUES_REDAC_DOCUMENT_CANEVAS', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DOCUMENT_CANEVAS', [], ['interface_module_rsem'], ['hyper_admin'], [],true],
            ['STATISTIQUES_REDAC_DOCUMENT_LIBRES', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DOCUMENT_LIBRES', [], ['interface_module_rsem'], ['hyper_admin'], [],true],
            ['STATISTIQUES_REDAC_DOCUMENTS', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DOCUMENTS', [], ['interface_module_rsem'], ['hyper_admin'], [],true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['STATISTIQUES_PLATEFORME', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE', [], [], [],[], false],
            ['STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', [], [], ['hyper_admin'],[], true],
            ['STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE', [], [], [], [],false],
            ['STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE', [], [], ['hyper_admin'], [],true],
            //submenu
            ['STATISTIQUES_ENTITE_PUBLIQUE', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', [], [], ['gerer_statistiques_metier'], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', [], [], ['gerer_statistiques_metier'],[], true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE', [], [], [],[], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE', [], [], ['gerer_statistiques_metier'], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', [], [], [],[], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', [], [], ['gerer_statistiques_metier'],[], true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', [], [], [],[], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', [], [], ['gerer_statistiques_metier'], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', [], [], ['gerer_statistiques_metier'], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', [], [], [], [],false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', [], [], ['gerer_statistiques_metier'], [],true],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS', [], [], [],[], false],
            ['STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS', [], [], ['gerer_statistiques_metier'], [], true],
            //submenu
            ['STATISTIQUES_REDAC', [], [], [], [], false],
            ['STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME', [], [], [],[], false],
            ['STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME', [], [], ['hyper_admin'], [], true],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION', [], [], ['hyper_admin'], [], true],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', [], [], [],[], false],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', [], [], ['hyper_admin'],[], true],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE', [], [], [], [], false],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE', [], [], ['hyper_admin'], [], true],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', [], [], ['hyper_admin'], [], true],
            ['STATISTIQUES_REDAC_DOCUMENT_CANEVAS', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DOCUMENT_CANEVAS', [], [], ['hyper_admin'], [],true],
            ['STATISTIQUES_REDAC_DOCUMENT_LIBRES', [], [], [], [],false],
            ['STATISTIQUES_REDAC_DOCUMENT_LIBRES', [], [], ['hyper_admin'], [], true],
            ['STATISTIQUES_REDAC_DOCUMENTS', [], [], [], [], false],
            ['STATISTIQUES_REDAC_DOCUMENTS', [], [], ['hyper_admin'], [], true],
        ];
    }
}
