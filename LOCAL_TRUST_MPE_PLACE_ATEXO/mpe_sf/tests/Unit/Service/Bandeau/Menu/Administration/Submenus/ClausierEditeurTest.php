<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Administration\Submenus\ClausierEditeur;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class ClausierEditeurTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return ClausierEditeur::class;
    }

    public function canSeeProvider(): array
    {
        return [
            ['CLAUSES_EDITEURS', [], [], [], [], false],
            ['CLAUSES_EDITEURS', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS', ['plateforme_editeur'], [], [], [], false],
            ['CLAUSES_EDITEURS_RECHERCHER', [], [], [], [],false],
            ['CLAUSES_EDITEURS_RECHERCHER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_CREER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_CREER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_EXPORTER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_EXPORTER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_CANEVAS', [], [], [], [], false],
            ['CLAUSES_EDITEURS_CANEVAS', ['plateforme_editeur'], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_EDITEURS_CANEVAS_RECHERCHER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_CANEVAS_RECHERCHER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_CANEVAS_CREER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_CANEVAS_CREER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_VERSION', [], [], [], [], false],
            ['CLAUSES_EDITEURS_VERSION', [], ['interface_module_rsem'], [],[], true],
            ['CLAUSES_EDITEURS_VERSION_CREER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_VERSION_CREER', ['plateforme_editeur'], ['interface_module_rsem'], [], [], true],
            ['CLAUSES_EDITEURS_VERSION_HISTORIQUE', [], [], [], [],false],
            ['CLAUSES_EDITEURS_VERSION_HISTORIQUE', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_EDITEURS_GABARIT', [], [], [],[], false],
            ['CLAUSES_EDITEURS_GABARIT', [], ['interface_module_rsem'], [], [],true],
            ['CLAUSES_EDITEURS_GABARIT_MODIFIER', [], [], [],[], false],
            ['CLAUSES_EDITEURS_GABARIT_MODIFIER', ['plateforme_editeur'], ['interface_module_rsem'], [],[], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['CLAUSES_EDITEURS', [], [], [], [],false],
            ['CLAUSES_EDITEURS_RECHERCHER', [], [], [], [],false],
            ['CLAUSES_EDITEURS_RECHERCHER', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_RECHERCHER', [], [], ['valider_clauses_editeur'], [],true],
            ['CLAUSES_EDITEURS_CREER', [], [], [], [],false],
            ['CLAUSES_EDITEURS_CREER', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_CREER', [], [], ['valider_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_EXPORTER', [], [], [], [],false],
            ['CLAUSES_EDITEURS_EXPORTER', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_EXPORTER', [], [], ['valider_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_CANEVAS', [], [], [], [],false],
            ['CLAUSES_EDITEURS_CANEVAS_RECHERCHER', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_CANEVAS_RECHERCHER', [], [], ['valider_clauses_editeur'], [],true],
            ['CLAUSES_EDITEURS_CANEVAS_CREER', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_CANEVAS_CREER', [], [], ['valider_clauses_editeur'], [],true],
            ['CLAUSES_EDITEURS_VERSION', [], [], [], [],false],
            ['CLAUSES_EDITEURS_VERSION_CREER', [], [], [], [],false],
            ['CLAUSES_EDITEURS_VERSION_CREER', [], [], ['publier_version_clausier_editeur'], [], true],
            ['CLAUSES_EDITEURS_VERSION_HISTORIQUE', [], [], [], [], false],
            ['CLAUSES_EDITEURS_VERSION_HISTORIQUE', [], [], ['administrer_clauses_editeur'],[], true],
            ['CLAUSES_EDITEURS_GABARIT', [], [], [],[], false],
            ['CLAUSES_EDITEURS_GABARIT_MODIFIER', [], [], [], [], false],
            ['CLAUSES_EDITEURS_GABARIT_MODIFIER', [], [], ['gerer_gabarit_editeur'], [], true],
        ];
    }
}
