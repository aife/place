<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu;

use App\Service\Bandeau\Menu\CachedMenuRenderer;
use Knp\Menu\Matcher\MatcherInterface;
use Knp\Menu\MenuFactory;
use Knp\Menu\MenuItem;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Cache\ItemInterface as CacheItemInterface;
use Twig\Environment;

class CachedMenuRendererTest extends TestCase
{
    public const HTML_HASH = 'cached-menu-html-hash';
    public const HTML_COMPUTED_CONTENT = '<h2>Computed content</h2>';
    public const HTML_CACHED_CONTENT = '<h2>Cached content</h2>';

    /**
     * @var ArrayAdapter
     */
    private $cache;
    /**
     * @var Session
     */
    private $session;
    /**
     * @var Security
     */
    private $securityMock;
    /**
     * @var MockObject|Environment
     */
    private $twigMock;

    protected function setUp(): void
    {
        $this->cache = new ArrayAdapter();
        $this->session = new Session(new MockArraySessionStorage(), new NamespacedAttributeBag());

        $this->securityMock = $this->createMock(Security::class);
        $this->twigMock = $this->createMock(Environment::class);
    }

    protected function getSUT(bool $isConnectedUser = true): CachedMenuRenderer
    {
        if ($isConnectedUser) {
            $this->securityMock->method('isGranted')->willReturn(true);
        }

        return new CachedMenuRenderer(
            $this->twigMock,
            '',
            $this->createMock(MatcherInterface::class),
            $this->cache,
            $this->session,
            $this->securityMock
        );
    }

    protected function getMenuItem(): MenuItem
    {
        $menuFactory = new MenuFactory();

        return new MenuItem('name', $menuFactory);
    }

    /**
     * User has not ROLE_AGENT
     * Render method should return an empty string.
     */
    public function testRenderWhenUserNotConnected()
    {
        $expectedHtml = '';
        $this->securityMock->method('isGranted')->willReturn(false);
        $renderer = $this->getSUT(false);

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
    }

    /**
     * Hash is not in session
     * Cache does not exist
     * Render method should be called once and result cached.
     */
    public function testRenderWhenNoHashInSessionNoCache()
    {
        $expectedHtml = self::HTML_COMPUTED_CONTENT;
        $this->twigMock->expects($this->once())->method('render')->willReturn($expectedHtml);
        $renderer = $this->getSUT();

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
        $this->assertEquals(md5($expectedHtml), $this->session->get($renderer->getSessionKey()));
    }

    /**
     * Hash is not in session
     * Cache exists
     * Render method should be called once and result cached.
     */
    public function testRenderWhenNoHashInSessionButCacheExists()
    {
        $expectedHtml = self::HTML_COMPUTED_CONTENT;
        $expectedHtmlHash = md5($expectedHtml);

        $this->cache->get(
            $expectedHtmlHash,
            function (CacheItemInterface $item) use ($expectedHtml) {
                return $expectedHtml;
            }
        );
        $this->twigMock->expects($this->once())->method('render')->willReturn($expectedHtml);
        $renderer = $this->getSUT();

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
        $this->assertEquals($expectedHtmlHash, $this->session->get($renderer->getSessionKey()));
    }

    /**
     * Hash is in session
     * Cache exists
     * Render method should NOT be called and result should be retrieved from cache.
     */
    public function testRenderWhenHashInSessionAndCacheExists()
    {
        $expectedHtml = self::HTML_CACHED_CONTENT;
        $expectedHtmlHash = md5($expectedHtml);

        $this->cache->get(
            CachedMenuRenderer::CACHE_KEY_PREFIX.$expectedHtmlHash,
            function (CacheItemInterface $item) use ($expectedHtml) {
                return $expectedHtml;
            }
        );
        $renderer = $this->getSUT();
        $this->session->set($renderer->getSessionKey(), $expectedHtmlHash);
        $this->twigMock->expects($this->never())->method('render');

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
        $this->assertEquals($expectedHtmlHash, $this->session->get($renderer->getSessionKey()));
    }

    /**
     * Hash is in session
     * Cache is invalid
     * Render method should be called once and result cached.
     */
    public function testRenderWhenHashInSessionAndCacheIsInvalid()
    {
        $expectedHtml = self::HTML_COMPUTED_CONTENT;
        $expectedHtmlHash = md5($expectedHtml);

        $this->session->set(CachedMenuRenderer::SESSION_KEY, $expectedHtmlHash);
        $this->twigMock->expects($this->once())->method('render')->willReturn($expectedHtml);
        $renderer = $this->getSUT();

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
        $this->assertEquals($expectedHtmlHash, $this->session->get($renderer->getSessionKey()));
    }

    /**
     * Outdated hash is in session
     * Cache is invalid
     * Configuration has changed => menu will be different than first time cached.
     */
    public function testRenderWhenHashInSessionAndCacheIsInvalidAndConfigHasChanged()
    {
        $expectedHtml = self::HTML_COMPUTED_CONTENT;
        $expectedHtmlHash = md5($expectedHtml);

        $outdatedHtml = '<h2>Outdated content</h2>';
        $outdatedHtmlHash = md5($outdatedHtml);
        $this->session->set(CachedMenuRenderer::SESSION_KEY, $outdatedHtmlHash);

        $this->twigMock->expects($this->once())->method('render')->willReturn($expectedHtml);
        $renderer = $this->getSUT();

        $result = $renderer->render($this->getMenuItem());

        $this->assertEquals($expectedHtml, $result);
        $this->assertEquals($expectedHtmlHash, $this->session->get($renderer->getSessionKey()));
    }
}
