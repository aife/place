<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use App\Service\Bandeau\Menu\Button\Modification;

class ModificationTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Modification::class;
    }

    public function canSeeProvider(): iterable
    {
        $similarValues = [Modification::TRANSLATION_PREFIX, [], [], [], []];

        return [
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['1']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['2']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['3']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['2', '4']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
        ];
    }

    public function canClickProvider(): iterable
    {
        $similarValues = [Modification::TRANSLATION_PREFIX, [], [], [], []];
        return [
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['2']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), ['isReadOnlyUser' => true], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), ['isReadOnlyUser' => false], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
            [Modification::TRANSLATION_PREFIX, [], [], ['modifier_consultation_avant_validation'], [],
                $this->initConsultationWithStatutCalcule(['0']), [], true],
            [Modification::TRANSLATION_PREFIX, [], [], ['modifier_consultation_avant_validation'], [],
                $this->initConsultationWithStatutCalcule(['4']), [], false],
            [Modification::TRANSLATION_PREFIX, [], [],
                ['modifier_consultation_procedures_formalisees_apres_validation'], [],
                $this->initConsultationWithStatutCalcule(['2']), [], true],
        ];
    }
}
