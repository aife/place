<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use App\Service\Bandeau\Menu\Button\Validation;

class ValidationTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Validation::class;
    }

    public function canSeeProvider(): iterable
    {
        $similarValues = [Validation::TRANSLATION_PREFIX, [], [], [], []];

        return [
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['1']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['3']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
        ];
    }

    public function canClickProvider(): iterable
    {
        $similarValues = [Validation::TRANSLATION_PREFIX, [], [], [], [], $this->initDefaultConsultation()];
        return [
            [...$similarValues, [], false],
            [...$similarValues, ['consultationHasValidationHabilitation' => false], false],
            [...$similarValues, ['consultationHasValidationHabilitation' => true], true],
        ];
    }
}
