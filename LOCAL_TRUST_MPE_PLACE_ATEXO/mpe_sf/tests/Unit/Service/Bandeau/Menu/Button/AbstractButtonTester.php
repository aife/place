<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Service\Consultation\ConsultationStatus;
use Doctrine\ORM\EntityManager;
use Doctrine\Persistence\ObjectRepository;
use PHPUnit\Framework\TestCase;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\SubmenuTesterTrait;

abstract class AbstractButtonTester extends TestCase implements ButtonTestInterface
{
    use SubmenuTesterTrait {
        getService as protected traitGetService;
    }

    public ?Consultation $consultation = null;
    public ?EntityManager $em = null;
    protected bool $isReadOnlyUser = false;
    protected bool $consultationHasValidationHabilitation = false;
    protected bool $hasHabilitationPublicite = false;


    /**
     * @dataProvider canSeeProvider
     */
    public function testCanSee(
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $agentService,
        Consultation $consultation,
        array $testsParameters,
        bool $expected
    ): void {
        $this->runSpecificTest(
            'canSee',
            $label,
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $agentService,
            $consultation,
            $testsParameters,
            $expected
        );
    }

    /**
     * @dataProvider canClickProvider
     */
    public function testCanClick(
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $agentService,
        Consultation $consultation,
        array $testsParameters,
        bool $expected
    ): void {
        $this->runSpecificTest(
            'canClick',
            $label,
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $agentService,
            $consultation,
            $testsParameters,
            $expected
        );
    }

    /**
     * @param string $method
     * @param string $label
     * @param array $activePlateformeModules
     * @param array $activeOrganismeModules
     * @param array $agentHabilitations
     * @param array $agentService
     * @param Consultation $consultation
     * @param array $testsParameters
     * @param bool $expected
     */
    protected function runSpecificTest(
        string $method,
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $agentService,
        Consultation $consultation,
        array $testsParameters,
        bool $expected
    ): void {
        $this->consultation = $consultation;
        $this->isReadOnlyUser = $testsParameters['isReadOnlyUser'] ?? false;
        $this->consultationHasValidationHabilitation =
            $testsParameters['consultationHasValidationHabilitation'] ?? false;
        $this->hasHabilitationPublicite =
            $testsParameters['hasHabilitationPublicite'] ?? false;

        $service = $this->getService(
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $agentService
        );

        $actual = $service->$method($label);

        $this->assertEquals(
            $expected,
            $actual,
            sprintf(
                "%s - %s : \n%s\n%s\n%s",
                $method,
                $label,
                'Modules plateforme : ' . implode(', ', $activePlateformeModules),
                'Modules organisme : ' . implode(', ', $activeOrganismeModules),
                'Habilitations agent : ' . implode(', ', $agentHabilitations)
            )
        );
    }

    public function getService(
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $agentService
    ) {
        $sut = $this->traitGetService(
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $agentService
        );

        $sut->mergeOptions(['consultation' => $this->consultation]);
        $sut->mergeOptions(['idConsulation' => $this->consultation->getId()]);
        $sut->getItem();

        return $sut;
    }

    public function initDefaultConsultation(): Consultation
    {
        $consultation = new Consultation();
        $consultation->setId(123);

        $organisme = new Organisme();
        $organisme->setAcronyme('test');
        $consultation->setOrganisme($organisme);

        $consultation->statutCalcule = [ConsultationStatus::STATUS['ELABORATION']];
        $consultation->typeProcedureOrg = new TypeProcedureOrganisme();
        return $consultation;
    }

    public function initConsultationWithStatutCalcule(array $statutsCalcules): Consultation
    {
        $consultation = $this->initDefaultConsultation();
        $consultation->statutCalcule = $statutsCalcules;
        return $consultation;
    }
}
