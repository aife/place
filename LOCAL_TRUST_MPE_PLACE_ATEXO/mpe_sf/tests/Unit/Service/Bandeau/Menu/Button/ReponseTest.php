<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use App\Service\Bandeau\Menu\Button\Reponse;

class ReponseTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Reponse::class;
    }

    public function canSeeProvider(): iterable
    {
        $similarValues = [Reponse::TRANSLATION_PREFIX, [], [], [], []];

        return [
            [...$similarValues, $this->initConsultationWithStatutCalcule(['0']), [], false],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['3']), [], true],
            [...$similarValues, $this->initConsultationWithStatutCalcule(['4']), [], false],
        ];
    }

    public function canClickProvider(): iterable
    {
        $similarValues = [Reponse::TRANSLATION_PREFIX, [], [], [], []];
        return [
            [...$similarValues, $this->initDefaultConsultation(), [], false],
            [Reponse::TRANSLATION_PREFIX, [], [], ['acces_reponses'], [],
                $this->initDefaultConsultation(), ['isReadOnlyUser' => true], false],
            [Reponse::TRANSLATION_PREFIX, [], [], ['acces_reponses'], [],
                $this->initDefaultConsultation(), [], true],
        ];
    }
}
