<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use App\Service\Bandeau\Menu\Button\Details;

class DetailsTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Details::class;
    }

    public function canSeeProvider(): iterable
    {
        $similarValues = [Details::TRANSLATION_PREFIX, [], [], [], []];

        return [
            [...$similarValues, $this->initDefaultConsultation(), [], true],
        ];
    }

    public function canClickProvider(): iterable
    {
        $similarValues = [Details::TRANSLATION_PREFIX, [], [], [], []];
        return [
            [...$similarValues, $this->initDefaultConsultation(), [], true],
        ];
    }
}
