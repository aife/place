<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Button;

use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\SubmenuTestInterface;

interface ButtonTestInterface extends SubmenuTestInterface
{
}
