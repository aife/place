<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Registre;

use App\Service\Bandeau\Menu\Registre\Retrait;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class RetraitTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Retrait::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Retrait::TRANSLATION_PREFIX, [], [], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [
                Retrait::TRANSLATION_PREFIX,
                [],
                [],
                [
                    'acces_registre_retraits_papier',
                    'acces_registre_retraits_electronique',
                    'suivi_seul_registre_retraits_papier',
                    'suivi_seul_registre_retraits_electronique'
                ],
                [],
                true
            ],
        ];
    }
}
