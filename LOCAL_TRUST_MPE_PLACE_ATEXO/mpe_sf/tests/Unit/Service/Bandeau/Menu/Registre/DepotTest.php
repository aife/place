<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Registre;

use App\Service\Bandeau\Menu\Registre\Depot;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class DepotTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Depot::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Depot::TRANSLATION_PREFIX, [], [], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [
                Depot::TRANSLATION_PREFIX,
                [],
                [],
                [
                    'acces_registre_depots_papier',
                    'acces_registre_depots_electronique',
                    'suivi_seul_registre_depots_papier',
                    'suivi_seul_registre_depots_electronique'
                ],
                [],
                true
            ],
        ];
    }
}
