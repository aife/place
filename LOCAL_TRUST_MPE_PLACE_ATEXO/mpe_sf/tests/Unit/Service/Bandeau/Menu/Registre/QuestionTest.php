<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Registre;

use App\Service\Bandeau\Menu\Registre\Question;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class QuestionTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Question::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Question::TRANSLATION_PREFIX, [], [], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [
                Question::TRANSLATION_PREFIX,
                [],
                [],
                [
                    'acces_registre_questions_papier',
                    'acces_registre_questions_electronique',
                    'suivi_seul_registre_questions_papier',
                    'suivi_seul_registre_questions_electronique'
                ],
                [],
                true
            ],
        ];
    }
}
