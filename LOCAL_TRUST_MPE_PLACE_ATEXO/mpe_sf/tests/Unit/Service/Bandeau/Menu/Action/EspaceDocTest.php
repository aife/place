<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Action\EspaceDoc;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class EspaceDocTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return EspaceDoc::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [EspaceDoc::TRANSLATION_PREFIX, [], [], [], [], false],
            [EspaceDoc::TRANSLATION_PREFIX, [], ['espace_documentaire'], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [EspaceDoc::TRANSLATION_PREFIX, [], [], [], [], false],
            [EspaceDoc::TRANSLATION_PREFIX, [], [], ['espace_documentaire_consultation'], [], true],
        ];
    }
}
