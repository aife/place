<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Action\Spaser;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class SpaserTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Spaser::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Spaser::TRANSLATION_PREFIX, [], [], [], [], false],
            [Spaser::TRANSLATION_PREFIX, [], ['acces_module_spaser'], [],[], false],
            [Spaser::TRANSLATION_PREFIX, [], [], ['gestion_spaser_consultations'],[], false],
            [Spaser::TRANSLATION_PREFIX, [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [Spaser::TRANSLATION_PREFIX, [], [], [], [], false],
            [Spaser::TRANSLATION_PREFIX, [], ['acces_module_spaser'], [],[], false],
            [Spaser::TRANSLATION_PREFIX, [], [], ['gestion_spaser_consultations'],[], false],
            [Spaser::TRANSLATION_PREFIX, [], ['acces_module_spaser'], ['gestion_spaser_consultations'],[], true],
        ];
    }
}
