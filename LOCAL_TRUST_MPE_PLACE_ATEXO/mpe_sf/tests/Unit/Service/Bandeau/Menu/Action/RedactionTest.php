<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use ReflectionClass;
use App\Service\Bandeau\Menu\Action\Redaction;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;
use Tests\Unit\Service\Bandeau\Menu\Button\AbstractButtonTester;
use Tests\Unit\Service\Bandeau\Menu\Button\ButtonTestInterface;

class RedactionTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Redaction::class;
    }

    public function getService(
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $agentService
    ) {
        $sut = parent::getService(
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $agentService
        );

        $reflection = new ReflectionClass($sut);
        $property = $reflection->getProperty('redactionActive');
        $property->setAccessible(true);
        $property->setValue($sut, true);

        return $sut;
    }

    public function canSeeProvider(): array
    {
        return [
           [Redaction::TRANSLATION_PREFIX, [], [], [], [], $this->initDefaultConsultation(), [], false],
            [Redaction::TRANSLATION_PREFIX, [], ['interface_module_rsem'], [], [],
                $this->initDefaultConsultation(), [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [Redaction::TRANSLATION_PREFIX, [], [], [], [], $this->initDefaultConsultation(), [], false],
            [Redaction::TRANSLATION_PREFIX, [], [], ['redaction_documents_redac'], [],
                $this->initDefaultConsultation(), [], true],
            [Redaction::TRANSLATION_PREFIX, [], [], ['validation_documents_redac'], [],
                $this->initDefaultConsultation(), [], true],
            [Redaction::TRANSLATION_PREFIX, [], [], ['validation_documents_redac'], [],
                $this->initDefaultConsultation(), ['isReadOnlyUser' => true], false],
        ];
    }
}
