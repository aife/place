<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Action\Publicite;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;
use Tests\Unit\Service\Bandeau\Menu\Button\AbstractButtonTester;
use Tests\Unit\Service\Bandeau\Menu\Button\ButtonTestInterface;

class PubliciteTest extends AbstractButtonTester implements ButtonTestInterface
{
    public function getClassUnderTestName(): string
    {
        return Publicite::class;
    }

    public function canSeeProvider(): array
    {
        $similarValues = [Publicite::TRANSLATION_PREFIX, [], [], [], []];
        return [
            [...$similarValues, $this->initDefaultConsultation(), [], true],
        ];
    }

    public function canClickProvider(): array
    {
        $similarValues = [Publicite::TRANSLATION_PREFIX, [], [], [], []];
        return [
            [...$similarValues, $this->initDefaultConsultation(), [], false],
            [Publicite::TRANSLATION_PREFIX, [], [], ['publier_consultation'], [],
                $this->initDefaultConsultation(), ['isReadOnlyUser' => true], false],
            [Publicite::TRANSLATION_PREFIX, [], [], ['publier_consultation'], [],
                $this->initDefaultConsultation(), [], true],
        ];
    }
}
