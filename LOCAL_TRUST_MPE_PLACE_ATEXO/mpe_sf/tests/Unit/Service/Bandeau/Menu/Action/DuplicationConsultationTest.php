<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Action\DuplicationConsultation;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class DuplicationConsultationTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return DuplicationConsultation::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [DuplicationConsultation::TRANSLATION_PREFIX, [], [], [], [], false],
            [DuplicationConsultation::TRANSLATION_PREFIX, [], [], ['duplication_consultations'], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [DuplicationConsultation::TRANSLATION_PREFIX, [], [], [], [], false],
            [DuplicationConsultation::TRANSLATION_PREFIX, [], [], ['duplication_consultations'], [], true],
        ];
    }
}
