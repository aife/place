<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Action\EchangeDoc;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class EchangeDocTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return EchangeDoc::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [EchangeDoc::TRANSLATION_PREFIX, [], [], [], [], false],
            [EchangeDoc::TRANSLATION_PREFIX, [], ['echanges_documents'], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            [EchangeDoc::TRANSLATION_PREFIX, [], [], [], [], false],
            [EchangeDoc::TRANSLATION_PREFIX, [], [], ['acces_echange_documentaire'], [], true],
        ];
    }
}
