<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Service\Bandeau\Menu\Administration\Submenus\Parametrage;
use App\Service\Bandeau\Menu\Agent\Submenus\Passation;
use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use Exception;
use Knp\Menu\ItemInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class AbstractSubmenuTest extends TestCase
{
    use SubmenuTesterTrait;

    /**
     * @return AbstractSubmenu|MockObject
     */
    protected function getAbstractServiceInstance(
        array $activePlateformeModules = [],
        array $activeOrganismeModules = [],
        array $agentHabilitations = [],
        array $parameters = []
    ) {
        list($factory, $em, $parameterBag, $user, $agentService,
            $routerMock, $encryptionMock, $registreService, $routeMock, $translatorMock)
            = $this->prepareServiceMocks(
                $activePlateformeModules,
                $activeOrganismeModules,
                $agentHabilitations,
                $parameters
            );

        return $this->getMockForAbstractClass(
            AbstractSubmenu::class,
            [
                $factory, $em, $parameterBag, $user, $agentService,
                $routerMock, $encryptionMock, $registreService, $routeMock, $translatorMock
            ]
        );
    }

    public function testConvertToCamelCase()
    {
        $sut = $this->getAbstractServiceInstance();

        $result = $sut->convertToCamelCase('attribute_name');

        $this->assertEquals('AttributeName', $result);
    }

    public function prefixUriDataProvider()
    {
        return [
            ['/', 'https://mpe-docker.local-trust.com/', 'https://mpe-docker.local-trust.com/'],
            ['/uri', 'https://mpe-docker.local-trust.com/', 'https://mpe-docker.local-trust.com/uri'],
            ['/', '/', '/'],
            ['/uri', '/', '/uri'],
            ['#', 'https://mpe-docker.local-trust.com/', '#'],
        ];
    }

    /**
     * @dataProvider prefixUriDataProvider
     *
     * @param $uri
     * @param $parameterValue
     * @param $expected
     */
    public function testPrefixUri($uri, $parameterValue, $expected)
    {
        $options = ['uri' => $uri];
        $sut = $this->getAbstractServiceInstance([], [], [], ['PF_URL' => $parameterValue]);
        $sut->prefixUri($options);

        $this->assertEquals($expected, $options['uri']);
    }

    public function getGetterMethodNameDataProvider()
    {
        return [
            ['code_cpv', ConfigurationPlateforme::class, 'getCodeCpv'],
            ['menu_agent_complet', ConfigurationPlateforme::class, 'isMenuAgentComplet'],
            ['gerer_archives', ConfigurationOrganisme::class, 'getGererArchives'],
        ];
    }

    /**
     * @dataProvider getGetterMethodNameDataProvider
     *
     * @param $code
     * @param $className
     * @param $expected
     */
    public function testGetGetterMethodName($code, $className, $expected)
    {
        $sut = $this->getAbstractServiceInstance();
        $result = $sut->getGetterMethodName($code, $className);

        $this->assertEquals($expected, $result);
    }

    public function testGetPrefixedLabel()
    {
        $sut = $this->getAbstractServiceInstance();
        $result = $sut->getPrefixedLabel('LIBELLE');

        $this->assertEquals('CHANGE_ME_LIBELLE', $result);
    }

    public function testGetCssClass()
    {
        $sut = $this->getAbstractServiceInstance();

        $this->assertEquals('change-me', $sut->getCssClassFromLabel());
        $this->assertEquals('change-me-libelle', $sut->getCssClassFromLabel('LIBELLE'));
    }

    public function testAddSpecificCssClass()
    {
        $sut = $this->getAbstractServiceInstance();

        $options = $sut->addSpecificCssClass('', []);
        $this->assertStringContainsString('change-me', $options['attributes']['class']);

        $options = $sut->addSpecificCssClass('libelle', []);
        $this->assertStringContainsString('change-me-libelle', $options['attributes']['class']);

        $options = $sut->addSpecificCssClass('libelle', ['attributes' => ['class' => 'existing-class']]);
        $this->assertStringContainsString('existing-class change-me-libelle', $options['attributes']['class']);
    }

    public function testAddCssClass()
    {
        $sut = $this->getAbstractServiceInstance();
        $options = $sut->addCssClassToOptions('row', []);
        $this->assertEquals('row', $options['attributes']['class']);

        $options = $sut->addCssClassToOptions('column', []);
        $this->assertEquals('column', $options['attributes']['class']);

        $options = $sut->addCssClassToOptions('column', []);
        $options = $sut->addCssClassToOptions('col-md-4', $options);
        $this->assertEquals('column col-md-4', $options['attributes']['class']);

        $options = $sut->addCssClassToOptions('column', []);
        $options = $sut->addCssClassToOptions('column', $options);
        $this->assertEquals('column', $options['attributes']['class']);
    }

    public function testHasConfigurationPlateforme()
    {
        $sut = $this->getAbstractServiceInstance(['gestion_organisme_par_agent']);
        $this->assertTrue($sut->hasConfigurationPlateforme('gestion_organisme_par_agent'));
        $this->assertFalse($sut->hasConfigurationPlateforme('socle_interne'));
    }

    public function testHasConfigurationOrganisme()
    {
        $sut = $this->getAbstractServiceInstance([], ['interface_module_rsem']);
        $this->assertTrue($sut->hasConfigurationOrganisme('interface_module_rsem'));
        $this->assertFalse($sut->hasConfigurationOrganisme('extraction_accords_cadres'));
    }

    public function testHasHabilitationAgent()
    {
        $sut = $this->getAbstractServiceInstance([], [], ['suivi_acces']);
        $this->assertTrue($sut->hasHabilitationAgent('suivi_acces'));
        $this->assertFalse($sut->hasHabilitationAgent('gestion_mapa'));
    }

    public function testInit()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(false);
        $sut->init();
        $this->assertNull($sut->menu, 'Le menu doit être null car par défaut on n\'affiche rien');

        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->init();
        $this->assertNotNull($sut->menu);
        $this->assertEquals(AbstractSubmenu::TRANSLATION_PREFIX, $sut->menu->getName());
        $this->assertEquals(0, $sut->menu->count());
    }

    public function testAddElement()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->init();

        $sut->addElement('LABEL');
        $this->assertEquals(1, $sut->menu->count());
    }

    public function testAddElementCannotSee()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturnOnConsecutiveCalls(true, false);
        $sut->init();

        $sut->addElement('LABEL');
        $this->assertEquals(0, $sut->menu->count());
    }

    public function testAddLink()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $sut->addLink('LABEL', '/agent');
        $this->assertEquals(1, $sut->menu->count());
        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertNotContains('h3', $item->getExtras());
        $this->assertNotContains('form', $item->getExtras());
    }

    public function testAddLinkCannotClick()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(false);
        $sut->init();

        $sut->addLink('LABEL', '/agent');
        $this->assertEquals(1, $sut->menu->count());
        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertNull($item->getUri());
    }

    public function testAddSubmenu()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $sut->addSubmenu('LABEL');
        $sut->addSubmenu('LABEL2');
        $this->assertEquals(2, $sut->menu->count());
        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertEquals('h3', $item->getExtra('tag'));
        $this->assertNotContains('form', $item->getExtras());
        $item2 = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL2');
        $this->assertNotContains('mt-5', explode(' ', $item2->getAttribute('class')));
    }

    public function testAddSubmenuSameColumn()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $sut->addSubmenu('LABEL');
        $sut->addSubmenu('LABEL2', [], false);
        $this->assertEquals(1, $sut->menu->count());
        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertEquals('h3', $item->getExtra('tag'));
        $this->assertNotContains('form', $item->getExtras());
        $item2 = $item->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL2');
        $this->assertContains('mt-5', explode(' ', $item2->getAttribute('class')));
    }

    public function testAddSubmenuSublevels()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $sut->addSubmenu('LABEL1');
        $sut->addLink('LINK1', '#');
        $sut->addLink('LINK2', '#');
        $sut->addLink('LINK3', '#');
        $sut->addSubmenu('LABEL2');

        $this->assertEquals(2, $sut->menu->count());
        $item1 = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL1');
        $this->assertEquals(1, $item1->getLevel());
        $this->assertEquals(3, $item1->count());
        $this->assertEquals('h3', $item1->getExtra('tag'));
        $this->assertEquals(2, $item1->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LINK1')->getLevel());

        $item2 = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL2');
        $this->assertEquals(1, $item2->getLevel());
    }

    public function testAddForm()
    {
        $sut = $this->getAbstractServiceInstance([], [], [], ['PF_URL' => 'https://mpe.com/']);
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $sut->addForm('LABEL', '/search', 'myInputName');
        $this->assertEquals(1, $sut->menu->count());

        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertEquals('https://mpe.com/search', $item->getUri());
        $this->assertTrue($item->getExtra('form'));
        $this->assertEquals('myInputName', $item->getExtra('formInputName'));
        $this->assertNotContains('disabled', $item->getExtra('fieldsetAttributes', []));
    }

    public function testAddDisabledForm()
    {
        $sut = $this->getAbstractServiceInstance([], [], [], ['PF_URL' => 'https://mpe.com/']);
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(false);
        $sut->init();

        $sut->addForm('LABEL', '/search', 'myInputName');
        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_LABEL');
        $this->assertEquals('disabled', $item->getExtra('fieldsetAttributes')['disabled']);
    }

    public function testAddDiv()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $className = 'card';

        $sut->addDiv($className);
        $this->assertEquals(1, $sut->menu->count());

        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_div-1');
        $this->assertEquals($className, $item->getAttribute('class'));
    }

    public function testColumn()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();
        $sut->bootstrapCards = true;

        $column = $sut->addColumn(1);

        $this->assertTrue($column->getExtra('column'));
        $this->assertEquals('col-md-4 col-12 mb-2', $column->getAttribute('class'));
    }

    public function addColumnDataProvider()
    {
        return [[2], [3]];
    }

    /**
     * @dataProvider addColumnDataProvider
     */
    public function testAddColumns($columnsCount)
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();
        $sut->bootstrapCards = true;

        $sut->addColumns($columnsCount);
        $this->assertEquals($columnsCount, $sut->menu->count());
    }

    public function testAddColumnsSansModeBootstrapCards()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();

        $this->expectException(Exception::class);
        $sut->addColumns(2);
    }

    public function testSetColumn()
    {
        $sut = $this->getAbstractServiceInstance();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);
        $sut->init();
        $sut->bootstrapCards = true;

        $sut->addRow();
        $sut->addColumns(3);

        $sut->setColumn(2);
        $sut->addSubmenu('LABEL');

        $item = $sut->menu->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_row-1');
        $column1 = $item->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_column-1');
        $column2 = $item->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_column-2');
        $column3 = $item->getChild(AbstractSubmenu::TRANSLATION_PREFIX . '_column-3');

        $this->assertCount(0, $column1);
        $this->assertCount(1, $column2);
        $this->assertCount(0, $column3);
    }

    public function testMenuAgentPassation()
    {
        $sut = $this->getMockBuilder(Passation::class)
            ->setConstructorArgs($this->prepareServiceMocks([], [], [], []))
            ->setMethods(['canSee', 'canClick'])
            ->getMock();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);

        $item = $sut->getItem();

        $this->assertCount(5, $item->getChildren());

        $firstUl = $item->getChild(Passation::TRANSLATION_PREFIX . '_' . 'CONSULTATIONS');

        $this->assertCount(8, $firstUl);
    }

    public function testAdministrationParametrage()
    {
        $sut = $this->getMockBuilder(Parametrage::class)
            ->setConstructorArgs($this->prepareServiceMocks([], [], [], []))
            ->setMethods(['canSee', 'canClick'])
            ->getMock();
        $sut->method('canSee')->willReturn(true);
        $sut->method('canClick')->willReturn(true);

        $item = $sut->getItem();
        $card = $item->getChild(Parametrage::TRANSLATION_PREFIX . '_div-1');
        $cardContent = $card->getChild(Parametrage::TRANSLATION_PREFIX . '_div-1');
        $cardBody = $cardContent->getChild(Parametrage::TRANSLATION_PREFIX . '_div-1');

        $this->assertEquals('card', $card->getAttribute('class'));
        $this->assertEquals('card-content', $cardContent->getAttribute('class'));
        $this->assertEquals('card-body', $cardBody->getAttribute('class'));

        $this->assertCount(2, $cardBody->getChildren(), 'On doit avoir 1 div row');
        $row = $cardBody->getChild(Parametrage::TRANSLATION_PREFIX . '_row-1');
        $this->assertInstanceOf(ItemInterface::class, $row);
        $this->assertTrue($row->getExtra('row'));
        $this->assertNull($row->getExtra('column'));
        $this->assertEmpty($row->getLabel());
        $this->assertEquals('row', $row->getAttribute('class'));
        $this->assertCount(3, $row->getChildren(), 'La ligne doit avoir 3 columns');

        $column1 = $row->getChild(Parametrage::TRANSLATION_PREFIX . '_column-1');
        $this->assertInstanceOf(ItemInterface::class, $column1);
        $this->assertTrue($column1->getExtra('column'));
        $this->assertNull($column1->getExtra('row'));
        $this->assertEmpty($column1->getLabel());
        $this->assertEquals('col-md-4 col-12 mb-2', $column1->getAttribute('class'));
        $this->assertCount(18, $column1->getChildren());

        $column2 = $row->getChild(Parametrage::TRANSLATION_PREFIX . '_column-2');
        $this->assertCount(15, $column2->getChildren());

        $column3 = $row->getChild(Parametrage::TRANSLATION_PREFIX . '_column-3');
        $this->assertCount(7, $column3->getChildren());

        $column1->getChildren();
    }

    public function testAdministrationParametrageAucunDroit()
    {
        $sut = $this->getMockBuilder(Parametrage::class)
            ->setConstructorArgs($this->prepareServiceMocks([], [], [], []))
            ->setMethods(['canSee', 'canClick'])
            ->getMock();
        $sut->method('canSee')->willReturn(false);
        $sut->method('canClick')->willReturn(false);

        $item = $sut->getItem();

        $this->assertNull($item);
    }
}
