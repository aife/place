<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\HabilitationAgent;
use App\Entity\ProcedureEquivalence;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\Consultation\InterneConsultationSuiviSeulRepository;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Service\AgentService;
use App\Service\CurrentUser;
use App\Service\GestionRegistres;
use App\Service\Routing\RouteBuilderInterface;
use App\Utils\Encryption;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Statement;
use Doctrine\ORM\EntityManager;
use Knp\Menu\MenuFactory;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Contracts\Translation\TranslatorInterface;

trait SubmenuTesterTrait
{
    public function getService(
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $parameters
    ) {
        $classUnderTest = $this->getClassUnderTestName();

        list(
            $factory, $em, $parameterBag, $user, $agentService, $routerMock,
            $encryptionMock, $registreService, $routeMock, $translatorMock
            )
            = $this->prepareServiceMocks(
                $activePlateformeModules,
                $activeOrganismeModules,
                $agentHabilitations,
                $parameters
            );

        return new $classUnderTest(
            $factory,
            $em,
            $parameterBag,
            $user,
            $agentService,
            $routerMock,
            $encryptionMock,
            $registreService,
            $routeMock,
            $translatorMock
        );
    }

    public function prepareServiceMocks(
        array $activePlateformeModules = [],
        array $activeOrganismeModules = [],
        array $agentHabilitations = [],
        array $parameters = []
    ): array {
        $factory = new MenuFactory();

        $configurationPlatformRepository = $this->createMock(ConfigurationPlateformeRepository::class);
        $configurationPlatformRepository->method('getConfigurationPlateforme')
            ->willReturn($this->getConfigurationPlateformeMock($activePlateformeModules));

        $configurationOrganismeRepository = $this->createMock(ConfigurationOrganismeRepository::class);
        $configurationOrganismeRepository->method('findOneBy')
            ->willReturn($this->getConfigurationOrganismeMock($activeOrganismeModules));

        $interneConsultationSuiviSeulRepository = $this->createMock(InterneConsultationSuiviSeulRepository::class);
        $interneConsultationSuiviSeulRepository->method('isReadOnlyUser')->willReturn($this->isReadOnlyUser ?? false);

        $procedureEquivalence = new ProcedureEquivalence();
        $procedureEquivalence->setProcedurePublicite('+1');
        $procedureEquivalenceRepositoryRepository = $this->createMock(ProcedureEquivalenceRepository::class);
        $procedureEquivalenceRepositoryRepository->method('findOneBy')->willReturn($procedureEquivalence);

        $em = $this->createMock(EntityManager::class);
        $em->method('getRepository')
            ->willReturnMap(
                [
                    [ConfigurationPlateforme::class, $configurationPlatformRepository],
                    [ConfigurationOrganisme::class, $configurationOrganismeRepository],
                    [InterneConsultationSuiviSeul::class, $interneConsultationSuiviSeulRepository],
                    [ProcedureEquivalence::class, $procedureEquivalenceRepositoryRepository],
                ]
            );

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->method('get')
            ->willReturnCallback(
                function (string $parameter) use ($parameters) {
                    return isset($parameters[$parameter]) ? $parameters[$parameter] : '';
                }
            );

        $user = $this->createMock(CurrentUser::class);
        $user->method('getAcronymeOrga')
            ->willReturn('pmi');
        $user->method('isAgent')
            ->willReturn(true);
        $user->method('checkHabilitation')
            ->willReturnCallback(
                function ($habilitation) use ($agentHabilitations) {
                    return in_array(
                        (new CamelCaseToSnakeCaseNameConverter())->normalize(lcfirst($habilitation)),
                        $agentHabilitations
                    );
                }
            );

        $agentService = $this->createMock(AgentService::class);
        $routerMock = $this->createMock(UrlGeneratorInterface::class);
        $routerMock->method('generate')->willReturn('/route');
        $encryptionMock = $this->createMock(Encryption::class);
        $routeMock = $this->createMock(RouteBuilderInterface::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $connectionMock = $this->createMock(Connection::class);
        $statementMock = $this->createMock(Statement::class);
        $connectionMock->method('prepare')
            ->willReturn($statementMock);
        $em->method('getConnection')
            ->willReturn($connectionMock);

        $registreService = $this->createMock(GestionRegistres::class);
        $registreService->method('consultationHasValidationHabilitation')
            ->willReturn($this->consultationHasValidationHabilitation ?? false);

        return [
            $factory, $em, $parameterBag, $user, $agentService,
            $routerMock, $encryptionMock, $registreService, $routeMock, $translatorMock
        ];
    }

    protected function getSetterMethodName($code): string
    {
        return 'set' . ucfirst((new CamelCaseToSnakeCaseNameConverter())->denormalize($code));
    }

    protected function getConfigurationPlateformeMock(array $activePlateformeModules): ConfigurationPlateforme
    {
        $mock = new ConfigurationPlateforme();

        $this->resetDefaultValues($mock);

        foreach ($activePlateformeModules as $activePlateformeModule) {
            $setterMethodName = $this->getSetterMethodName($activePlateformeModule);
            $mock->$setterMethodName('1');
        }

        return $mock;
    }

    protected function getConfigurationOrganismeMock(array $activeOrganismeModules): ConfigurationOrganisme
    {
        $mock = new ConfigurationOrganisme();

        $this->resetDefaultValues($mock);

        foreach ($activeOrganismeModules as $activeOrganismeModule) {
            $setterMethodName = $this->getSetterMethodName($activeOrganismeModule);
            $mock->$setterMethodName('1');
        }

        return $mock;
    }

    protected function getHabilitationsAgentMock(array $habilitationsAgent): HabilitationAgent
    {
        $mock = new HabilitationAgent();

        $this->resetDefaultValues($mock);

        foreach ($habilitationsAgent as $habilitationAgent) {
            $setterMethodName = $this->getSetterMethodName($habilitationAgent);
            $mock->$setterMethodName('1');
        }

        return $mock;
    }

    protected function resetDefaultValues(object $mock): void
    {
        $reflect = new ReflectionClass($mock);
        $attributes = $reflect->getProperties();

        foreach ($attributes as $attribute) {
            $setterMethodName = $this->getSetterMethodName($attribute->getName());
            if (method_exists($mock, $setterMethodName)) {
                $mock->$setterMethodName('0');
            }
        }
    }
}
