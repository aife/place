<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\Recensement;

class RecensementTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Recensement::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Recensement::TRANSLATION_PREFIX, [], [], [], [], false],
            [Recensement::TRANSLATION_PREFIX, ['menu_agent_complet'], [], [], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['besoin_unitaire_consultation'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['besoin_unitaire_creation_modification'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['demande_achat_consultation'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['demande_achat_creation_modification'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['projet_achat_consultation'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['projet_achat_creation_modification'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['validation_opportunite'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['validation_budget'], [], true],
            [Recensement::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['validation_achat'], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['', [], [], [], [], false],
        ];
    }
}
