<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

interface SubmenuTestInterface
{
    public function getClassUnderTestName(): string;

    public function canSeeProvider(): iterable;

    public function canClickProvider(): iterable;
}
