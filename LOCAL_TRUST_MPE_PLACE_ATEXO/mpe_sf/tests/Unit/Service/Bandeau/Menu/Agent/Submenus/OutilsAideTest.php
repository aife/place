<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\OutilsAide;
use Symfony\Contracts\Translation\TranslatorInterface;

class OutilsAideTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return OutilsAide::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [OutilsAide::TRANSLATION_PREFIX, [], [], [], [], true],
            ['OUTILS_DE_SIGNATURE', [], [], [], [], true],
            ['SIGNER_UN_DOCUMENT', [], [], [], [], true],
            ['VERIFIER_LA_SIGNATURE', [], [], [], [], true],
            ['OUTILS_INFORMATIQUES', [], [], [], [], true],
            ['DECHIFFREMENT_EN_LIGNE', [], [], [], [], true],
            ['OUVRIR_OFFRE_HORS_LIGNE', [], [], [], [], false],
            ['ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS', [], [], [], [], false],
            ['AUTRES_OUTILS', [], [], [], [], true],
            ['SE_PREPARER_A_SE_DEPOUILLER', [], [], [], [], true],
            ['TESTER_LA_CONFIGURATION_DE_MON_POSTE', [], [], [], [], true],
            ['ANNUAIRE_FOURNISSEUR', [], [], [], [], true],
            ['ANNUAIRE_FOURNISSEUR_RECHERCHER', [], [], [], [], true],
            ['AIDE', [], [], [], [], true],
            ['GUIDE_UTILISATION', [], [], [], [], true],
            ['ASSISTANCE_TELEPHONIQUE', [], [], [], [], true],
            ['ABREVIATIONS_GLOSSAIRES', [], [], [], [], true],
            ['OUTILS_DE_FORMATION', [], [], [], [], true],
            ['DOCUMENTS_DE_REFERENCE', ['documents_reference'], [], [], [], true],
            ['DOCUMENTS_DE_REFERENCE', [], [], [], [], false],
            ['ANNUAIRE_ACHETEUR_PUBLIC', ['annuaire_acheteurs_publics'], [], [], [], true],
            ['ANNUAIRE_ACHETEUR_PUBLIC', [], [], [], [], false],
            ['ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER', ['annuaire_acheteurs_publics'], [], [], [], true],
            ['ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER', [], [], [], [], false],
            ['CERTIFICATS', [], [], [], [], false],
            ['CERTIFICATS', ['module_certificat'], [], [], [], true],
            ['TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME', [], [], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['OUTILS_DE_SIGNATURE', [], [], [], [], false],
            ['SIGNER_UN_DOCUMENT', [], [], [], [], true],
            ['VERIFIER_LA_SIGNATURE', [], [], [], [], true],
            ['OUTILS_INFORMATIQUES', [], [], [], [], false],
            ['DECHIFFREMENT_EN_LIGNE', [], [], [], [], true],
            ['OUVRIR_OFFRE_HORS_LIGNE', [], [], [], [], false],
            ['ACTIVER_MON_ASSISTANT_MARCHES_PUBLICS', [], [], [], [], false],
            ['AUTRES_OUTILS', [], [], [], [], true],
            ['SE_PREPARER_A_SE_DEPOUILLER', [], [], [], [], false],
            ['TESTER_LA_CONFIGURATION_DE_MON_POSTE', [], [], [], [], true],
            ['ANNUAIRE_FOURNISSEUR', [], [], [], [], false],
            ['ANNUAIRE_FOURNISSEUR_RECHERCHER', [], [], ['suivi_entreprise'], [], true],
            ['AIDE', [], [], [], [], false],
            ['GUIDE_UTILISATION', [], [], [], [], true],
            ['ASSISTANCE_TELEPHONIQUE', [], [], [], [], true],
            ['ABREVIATIONS_GLOSSAIRES', [], [], [], [], true],
            ['OUTILS_DE_FORMATION', [], [], [], [], true],
            ['DOCUMENTS_DE_REFERENCE', [], [], [], [], true],
            ['ANNUAIRE_ACHETEUR_PUBLIC', [], [], [], [], true],
            ['ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER', [], [], ['annuaire_acheteur'], [], true],
            ['ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER', [], [], [], [], false],
            ['CERTIFICATS', [], [], [], [], true],
            ['TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME', [], [], [], [], true],
        ];
    }

    /**
     * @dataProvider canSeeDecouvrezDernieresFonctionnalitesDataProvider
     * @param string $translation
     * @param bool $expected
     * @return void
     */
    public function testCanSeeDecouvrezDernieresFonctionnalites(string $translation, bool $expected): void
    {
        list(
            $factory, $em, $parameterBag, $user, $agentService, $routerMock,
            $encryptionMock, $registreService, $routeMock, $translatorMock
            ) = $this->prepareServiceMocks([], [], [], []);

        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->method('trans')->willReturn($translation);

        $service = new OutilsAide(
            $factory,
            $em,
            $parameterBag,
            $user,
            $agentService,
            $routerMock,
            $encryptionMock,
            $registreService,
            $routeMock,
            $translatorMock
        );
        $result = $service->canSee('TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME');

        $this->assertSame($expected, $result);
    }

    public function canSeeDecouvrezDernieresFonctionnalitesDataProvider(): array
    {
        return [
            ['', true],
            ['TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME', false],
        ];
    }
}
