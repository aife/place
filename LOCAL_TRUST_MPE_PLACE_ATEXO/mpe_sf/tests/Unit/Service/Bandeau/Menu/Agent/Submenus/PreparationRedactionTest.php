<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\PreparationRedaction;

class PreparationRedactionTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return PreparationRedaction::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [PreparationRedaction::TRANSLATION_PREFIX, [], [], [], [],false],
            [PreparationRedaction::TRANSLATION_PREFIX, ['menu_agent_complet'], [], [],[], true],
            [PreparationRedaction::TRANSLATION_PREFIX, [], ['base_dce'], [], [],true],
            [PreparationRedaction::TRANSLATION_PREFIX, [], ['interface_module_rsem'], [], [],true],

            ['BASE_DCE', ['menu_agent_complet'], ['base_dce'], [], [],true],
            ['BASE_DCE', ['menu_agent_complet'], [], [], [],true],
            ['BASE_DCE', [], [], [], [],false],
            ['RECHERCHE_AVANCEE', [], [], [], [],false],
            ['RECHERCHE_AVANCEE', ['menu_agent_complet'], ['base_dce'], [], [],true],
            ['RECHERCHE_AVANCEE', [], ['base_dce'], [], [],true],
            ['PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', [], ['base_dce'], [], [],false],
            ['PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', [], [], [], [],false],
            ['PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', ['menu_agent_complet'], [], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES', [], ['interface_module_rsem'], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES_CLAUSES', [], ['interface_module_rsem'], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES_GABARITS', [], ['interface_module_rsem'], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES', [], [], [], [],false],
            ['MES_CLAUSES_PERSONNALISEES_CLAUSES', [], [], [], [],false],
            ['MES_CLAUSES_PERSONNALISEES_GABARITS', [], [], [], [],false],
            [
                'REDACTION_DES_PIECES',
                ['menu_agent_complet'],
                ['interface_module_rsem'],
                [], [],false,
            ],
            [
                'REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC',
                ['menu_agent_complet'],
                ['interface_module_rsem'],
                [], [],false,
            ],
            [
                'REDACTION_DES_PIECES',
                ['menu_agent_complet'],
                [],
                [],[],
                true,
            ],
            [
                'REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC',
                ['menu_agent_complet'],
                [],
                [], [],true,
            ],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['RECHERCHE_AVANCEE', [], [], [], [],true],
            ['PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', [], [], [], [],true],
            ['REDACTION_DES_PIECES_IMAGAGE_DU_MODULE_REDAC', [], [], [], [],true],
            ['REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC', [], [], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES_CLAUSES', [], [], [], [],true],
            ['MES_CLAUSES_PERSONNALISEES_GABARITS', [], [], ['gerer_gabarit_agent'],[], true],
            ['MES_CLAUSES_PERSONNALISEES_GABARITS', [], [], [],[], false],
        ];
    }
}
