<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\ValidationEcoSip;

class ValidationEcoSipTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return ValidationEcoSip::class;
    }

    public function canSeeProvider(): iterable
    {
        return [
            [ValidationEcoSip::TRANSLATION_PREFIX, [], [], [], [], false],
            ['VALIDATION_ECO_SIP', [], [], [], [], false],
            ['VALIDATION_ECO', [], [], [], [], false],
            ['VALIDATION_SIP', [], [], [], [], false],
            ['VALIDATION_ECO', [], ['module_eco_sip'], [], [], false],
            ['VALIDATION_SIP', [], ['module_eco_sip'], [], [], false],
            ['VALIDATION_ECO', [], ['module_eco_sip'], ['gestion_validation_eco'], [], true],
            ['VALIDATION_SIP', [], ['module_eco_sip'], ['gestion_validation_sip'], [], true],
            ['VALIDATION_ECO', [], [], ['gestion_validation_eco'], [], false],
            ['VALIDATION_SIP', [], [], ['gestion_validation_sip'], [], false],
        ];
    }

    public function canClickProvider(): iterable
    {
        return [
            ['VALIDATION_ECO_SIP', [], [], [], [], false],
            ['VALIDATION_ECO', [], [], [], [], false],
            ['VALIDATION_ECO', [], [], [], [], false],
            ['VALIDATION_SIP', [], [], [], [], false],
            ['VALIDATION_ECO', [], ['module_eco_sip'], [], [], false],
            ['VALIDATION_SIP', [], ['module_eco_sip'], [], [], false],
            ['VALIDATION_ECO', [], ['module_eco_sip'], ['gestion_validation_eco'], [], true],
            ['VALIDATION_SIP', [], ['module_eco_sip'], ['gestion_validation_sip'], [], true],
            ['VALIDATION_ECO', [], [], ['gestion_validation_eco'], [], false],
            ['VALIDATION_SIP', [], [], ['gestion_validation_sip'], [], false],
        ];
    }
}
