<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\Commission;

class CommissionTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Commission::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Commission::TRANSLATION_PREFIX, [], [], [], [], false],
            ['GESTION_DES_SEANCES', [], [], [], [], false],
            ['FORMULAIRE_SEANCE', [], [], [], [], false],
            ['ADMINISTRATION', [], [], [], [], false],
            ['SUIVI_SEANCE', [], [], [], [], false],
            ['LISTE_COMMISSIONS', [], [], [], [], false],
            ['LISTE_INTERVENANTS_EXTERNES', [], [], [], [], false],
            ['GESTION_DES_SEANCES', [], ['cao'], [], [], true],
            ['FORMULAIRE_SEANCE', [], ['cao'], [], [], true],
            ['ADMINISTRATION', [], ['cao'], [], [], true],
            ['SUIVI_SEANCE', [], ['cao'], [], [], true],
            ['LISTE_COMMISSIONS', [], ['cao'], [], [], true],
            ['LISTE_INTERVENANTS_EXTERNES', [], ['cao'], [], [], true],

        ];
    }

    public function canClickProvider(): array
    {
        return [
            [Commission::TRANSLATION_PREFIX, [], [], [], [], false],
            ['GESTION_DES_SEANCES', [], [], [], [], false],
            ['FORMULAIRE_SEANCE', [], [], [], [], false],
            ['ADMINISTRATION', [], [], [], [], false],
            ['SUIVI_SEANCE', [], [], [], [], false],
            ['LISTE_COMMISSIONS', [], [], [], [], false],
            ['LISTE_INTERVENANTS_EXTERNES', [], [], [], [], false],
            ['GESTION_DES_SEANCES', [], ['cao'], [], [], true],
            ['FORMULAIRE_SEANCE', [], ['cao'], [], [], true],
            ['ADMINISTRATION', [], ['cao'], [], [], true],
            ['SUIVI_SEANCE', [], ['cao'], [], [], true],
            ['LISTE_COMMISSIONS', [], ['cao'], [], [], true],
            ['LISTE_INTERVENANTS_EXTERNES', [], ['cao'], [], [], true],
        ];
    }
}
