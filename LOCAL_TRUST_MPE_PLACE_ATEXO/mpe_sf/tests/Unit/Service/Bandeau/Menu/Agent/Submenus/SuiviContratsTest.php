<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\SuiviContrats;

class SuiviContratsTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return SuiviContrats::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [SuiviContrats::TRANSLATION_PREFIX, [], [], [], [], true],
            ['CONTRATS_ET_DONNEES_ESSENTIELLES', [], [], [], [],true],
            ['ACCORD_CADRE_ET_SAD', [], [], [], [],true],
            ['CONTRATS_RECHERCHE_RAPIDE', [], [], [], [],true],
            ['TOUS_LES_CONTRATS', [], [], [], [],true],
            ['SAISIR_UN_CONTRAT', [], [], [], [],true],
            ['RECHERCHE_AVANCEE', [], [], [], [],true],
            ['EXECUTION_DES_CONTRATS', ['menu_agent_complet'], ['module_exec'], [], [],true],
            ['EXECUTION_DES_CONTRATS', [], ['module_exec'], [], [],true],
            ['EXECUTION_DES_CONTRATS', ['menu_agent_complet'], [], [], [],true],
            ['EXECUTION_DES_CONTRATS', [], [], [], [],false],

            ['ACCEDER_AU_MODULE_EXEC', [], [], [], [],false],
            ['ACCEDER_AU_MODULE_EXEC', [], ['module_exec'], [], [],true],

            ['PLUS_INFO_SUR_LE_MODULE_EXEC', ['menu_agent_complet'], [], [], [], true],
            ['PLUS_INFO_SUR_LE_MODULE_EXEC', [], [], [], [], false],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['CONTRATS_ET_DONNEES_ESSENTIELLES', [], [], [], [],false],
            ['ACCORD_CADRE_ET_SAD', [], [], [], [],true],
            ['CONTRATS_RECHERCHE_RAPIDE', [], [], [], [],true],
            ['TOUS_LES_CONTRATS', [], [], [], [],true],
            ['RECHERCHE_AVANCEE', [], [], [], [],true],
            ['PLUS_INFO_SUR_LE_MODULE_EXEC', [], [], [], [],true],
            ['SAISIR_UN_CONTRAT', [], [], ['creer_contrat'], [],true],
            ['SAISIR_UN_CONTRAT', [], [], [], [],false],
            ['EXECUTION_DES_CONTRATS', [], [], [], [],false],
        ];
    }
}
