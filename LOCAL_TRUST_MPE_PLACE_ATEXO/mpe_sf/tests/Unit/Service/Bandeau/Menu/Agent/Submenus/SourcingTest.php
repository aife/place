<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\Sourcing;

class SourcingTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Sourcing::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Sourcing::TRANSLATION_PREFIX, [], [], [], [], false],
            [Sourcing::TRANSLATION_PREFIX, [], ['module_sourcing'], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['', [], [], [], [],false],
        ];
    }
}
