<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\SourcingImage;

class SourcingImageTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return SourcingImage::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [SourcingImage::TRANSLATION_PREFIX, [], [], [], [], false],
            [SourcingImage::TRANSLATION_PREFIX, ['menu_agent_complet'], ['module_sourcing'], [], [], false],
            [SourcingImage::TRANSLATION_PREFIX, ['menu_agent_complet'], [], [], [], true],

            ['TEXT_SOUS_MENU', [], [], [], [], false],
            ['TEXT_SOUS_MENU', ['menu_agent_complet'], ['module_sourcing'], [], [], false],
            ['TEXT_SOUS_MENU', ['menu_agent_complet'], [], [], [], true],

            ['LIEN_INFO', [], [], [], [], false],
            ['LIEN_INFO', ['menu_agent_complet'], ['module_sourcing'], [], [], false],
            ['LIEN_INFO', ['menu_agent_complet'], [], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['', [], [], [], [],false],
        ];
    }
}
