<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\Passation;

class PassationTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Passation::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Passation::TRANSLATION_PREFIX, [], [], [], [], true],
            ['CONSULTATIONS', [], [], [], [], true],
            ['CONSULTATIONS_CREER', [], [], [], [], true],
            ['CONSULTATIONS_SIMPLIFIEE_CREER', [], [], [], [], false],
            ['CONSULTATIONS_SIMPLIFIEE_CREER', [], ['consultation_simplifiee'], [], [], true],
            ['CONSULTATIONS_RECHERCHE_RAPIDE', [], [], [], [], true],
            ['CONSULTATIONS_TOUTES', [], [], [], [], true],
            ['RECHERCHE_AVANCEE', [], [], [], [], true],
            ['AUTRES_ANNONCES', [], [], [], [], true],
            ['AUTRES_GERER_MES_RECHERCHES', [], [], [], [], false],
            ['AUTRES_GERER_MES_RECHERCHES', [], ['recherches_favorites_agent'], [], [], true],
            ['TOUTES_LES_ANNONCES_D_ATTRIBUTION', [], [], [], [], true],
            ['AUTRES_RECHERCHE_AVANCEE', [], [], [], [], true],
            ['ARCHIVES', [], [], [], [], true],
            ['ARCHIVES_RECHERCHE_RAPIDE', [], [], [], [], true],
            ['MES_TELECHARGEMENT_ASYNCHRONES', [], [], [], [], true],
            ['TOUT_LES_TELECHARGEMENTS_ASYNCHRONES', [], [], [], [], true],
            ['RECHERCHE_AVANCEE_ARCHIVES', [], [], [], [], true],
            ['ARCHIVES_TOUTES_LES_CONSULTATIONS', [], [], [], [], true],
            ['CONSULTATIONS_RMA_TOUTES', [], [], [], [], false],
            ['CONSULTATIONS_RMA_TOUTES', [], ['profil_rma'], [], [], true],
            ['GERER_MES_RECHERCHES', [], [], [], [], false],
            ['GERER_MES_RECHERCHES', [], ['recherches_favorites_agent'], [], [], true],
            ['CREER', [], [], [], [], false],
            ['CREER', ['creer_autre_annonce'], [], [], [], true],
            ['TOUS_LES_EXTRAITS_DE_PV', ['autre_annonce_extrait_pv'], [], [], [], true],
            ['TOUS_LES_EXTRAITS_DE_PV', [], [], [], [], false],
            ['TOUS_LES_RAPPORTS_D_ACHEVEMENT', ['autre_annonce_rapport_achevement'], [], [], [], true],
            ['TOUS_LES_RAPPORTS_D_ACHEVEMENT', [], [], [], [], false],
            ['TOUTES_LES_DECISIONS_DE_RESILIATION', ['autre_annonce_decision_resiliation'], [], [], [], true],
            ['TOUTES_LES_DECISIONS_DE_RESILIATION', [], [], [], [], false],
            ['TOUTES_LES_ANNONCES_D_INFORMATION', ['autre_annonce_information'], [], [], [], true],
            ['ANNONCE_DE_PROGRAMME_PREVISIONNEL', ['autre_annonce_programme_previsionnel'], [], [], [], true],
            ['ANNONCE_DE_PROGRAMME_PREVISIONNEL', [], [], [], [], false],
            ['ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT', ['autre_annonce_synthese_rapport_audit'], [], [], [], true],
            ['ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT', [], [], [], [], false],

            ['OPERATION', [], ['gestion_operations'], [], [], true],
            ['OPERATION_CREER', [], ['gestion_operations'], [], [], true],
            ['TOUTES_LES_OPERATIONS', [], ['gestion_operations'], [], [], true],
            ['OPERATION', [], [], [], [], false],
            ['OPERATION_CREER', [], [], [], [], false],
            ['TOUTES_LES_OPERATIONS', [], [], [], [], false],

            ['DOSSIER_VOLUMINEUX', ['menu_agent_complet'], [], [], [], true],
            ['DOSSIER_VOLUMINEUX', [], ['module_envol'], [], [], true],
            ['DOSSIER_VOLUMINEUX', [], [], [], [], false],

            ['DOSSIER_VOLUMINEUX_URI', [], ['module_envol'], [], [], true],
            ['DOSSIER_VOLUMINEUX_URI', [], [], [], [], false],

            ['PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL', ['menu_agent_complet'], [], [], [], true],
            ['PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL', [], ['module_envol'], [], [], false],
            ['PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL', [], [], [], [], false],
            ['TELECHARGEMENTS_DES_ARCHIVES', [], [], [], [], false],
            ['TELECHARGEMENTS_DES_ARCHIVES', [], ['archivage_consultation_sur_pf'], [], [], true],

            ['ENCHERES', [], [], [], [], false],
            ['ENCHERES', [], ['encheres'], [], [], true],
            ['ENCHERES_CREER', [], [], [], [], false],
            ['ENCHERES_CREER', [], ['encheres'], [], [], true],
            ['SUIVRE', [], [], [], [], false],
            ['SUIVRE', [], ['encheres'], [], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['CONSULTATIONS_RECHERCHE_RAPIDE', [], [], [], [], true],
            ['CONSULTATIONS_TOUTES', [], [], [], [], true],
            ['RECHERCHE_AVANCEE', [], [], [], [], true],
            ['GERER_MES_RECHERCHES', [], [], [], [], false],
            ['GERER_MES_RECHERCHES', [], ['recherches_favorites_agent'], [], [], true],
            ['CREER', [], [], [], [], true],
            ['TOUTES_LES_ANNONCES_D_INFORMATION', [], [], [], [], true],
            ['TOUS_LES_EXTRAITS_DE_PV', [], [], [], [], true],
            ['TOUS_LES_RAPPORTS_D_ACHEVEMENT', [], [], [], [], true],
            ['TOUTES_LES_ANNONCES_D_ATTRIBUTION', [], [], [], [], true],
            ['TOUTES_LES_DECISIONS_DE_RESILIATION', [], [], [], [], true],
            ['ANNONCE_DE_PROGRAMME_PREVISIONNEL', [], [], [], [], true],
            ['ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT', [], [], [], [], true],
            ['AUTRES_RECHERCHE_AVANCEE', [], [], [], [], true],
            ['AUTRES_GERER_MES_RECHERCHES', [], [], [], [], false],
            ['AUTRES_GERER_MES_RECHERCHES', [], ['recherches_favorites_agent'], [], [], true],
            ['TOUTES_LES_OPERATIONS', [], [], [], [], true],
            ['DOSSIER_VOLUMINEUX_URI', [], [], ['gestion_envol'], [], true],
            ['PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL', [], [], [], [], true],
            ['CONSULTATIONS_CREER', [], [], [], [], false],
            ['CONSULTATIONS_CREER', [], [], ['gerer_mapa_inferieur_montant'], [], true],
            ['CONSULTATIONS_CREER', [], [], ['gerer_mapa_superieur_montant'], [], true],
            ['CONSULTATIONS_CREER', [], [], ['administrer_procedures_formalisees'], [], true],
            ['CONSULTATIONS_SIMPLIFIEE_CREER', [], [], [], [], false],
            ['CONSULTATIONS_SIMPLIFIEE_CREER', [], [], ['gerer_mapa_inferieur_montant'], [], false],
            [
                'CONSULTATIONS_SIMPLIFIEE_CREER',
                [],
                ['consultation_simplifiee'],
                ['administrer_procedures_simplifiees'],
                [],
                true,
            ],
            ['CONSULTATIONS_RMA_TOUTES', [], [], [], [], false],
            ['CONSULTATIONS_RMA_TOUTES', [], [], ['profil_rma'], [], true],
            ['OPERATION_CREER', [], [], ['gerer_operations'], [], true],
            ['OPERATION_CREER', [], [], [], [], false],
            ['ENCHERES', [], [], [], [], false],
            ['ENCHERES_CREER', [], [], ['gerer_encheres'], [], true],
            ['ENCHERES_CREER', [], [], [], [], false],
            ['SUIVRE', [], [], [], [], false],
            ['SUIVRE', [], [], ['suivre_encheres'], [], true],

            ['TELECHARGEMENTS_DES_ARCHIVES', [], [], [], [], false],
            ['TELECHARGEMENTS_DES_ARCHIVES', [], [], ['download_archives'], [], true],

            ['ARCHIVES_RECHERCHE_RAPIDE', [], [], [], [], false],
            ['RECHERCHE_AVANCEE_ARCHIVES', [], [], [], [], false],
            ['MES_TELECHARGEMENT_ASYNCHRONES', [], [], [], [], false],
            ['ARCHIVES_TOUTES_LES_CONSULTATIONS', [], [], [], [], false],

            ['ARCHIVES_RECHERCHE_RAPIDE', [], [], ['gerer_archives'], [], true],
            ['RECHERCHE_AVANCEE_ARCHIVES', [], [], ['gerer_archives'], [], true],
            ['MES_TELECHARGEMENT_ASYNCHRONES', [], [], ['gerer_archives'], [], true],
            ['ARCHIVES_TOUTES_LES_CONSULTATIONS', [], [], ['gerer_archives'], [], true],

            ['TOUT_LES_TELECHARGEMENTS_ASYNCHRONES', [], [], ['acceder_tous_telechargements'], [], true],
            ['TOUT_LES_TELECHARGEMENTS_ASYNCHRONES', [], [], [], [], false],
        ];
    }
}
