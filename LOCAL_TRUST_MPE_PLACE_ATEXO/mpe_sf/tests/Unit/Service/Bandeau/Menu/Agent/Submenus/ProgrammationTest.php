<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\Programmation;

class ProgrammationTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Programmation::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [Programmation::TRANSLATION_PREFIX, [], [], [], [],false],
            [Programmation::TRANSLATION_PREFIX, ['menu_agent_complet'], [], [], [],true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['', [], [], [], [],false],
        ];
    }
}
