<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use PHPUnit\Framework\TestCase;
use Tests\Unit\Service\Bandeau\Menu\Button\ButtonTestInterface;

abstract class AbstractSubmenuTester extends TestCase implements ButtonTestInterface
{
    use SubmenuTesterTrait;

    /**
     * @dataProvider canSeeProvider
     */
    public function testCanSee(
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $parameters,
        bool $expected
    ) {
        $this->runSpecificTest(
            'canSee',
            $label,
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $parameters,
            $expected
        );
    }

    /**
     * @dataProvider canClickProvider
     */
    public function testCanClick(
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $parameters,
        bool $expected
    ) {
        $this->runSpecificTest(
            'canClick',
            $label,
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $parameters,
            $expected
        );
    }

    /**
     * @param string $method
     * @param string $label
     * @param array $activePlateformeModules
     * @param array $activeOrganismeModules
     * @param array $agentHabilitations
     * @param array $parameters
     * @param bool $expected
     */
    protected function runSpecificTest(
        string $method,
        string $label,
        array $activePlateformeModules,
        array $activeOrganismeModules,
        array $agentHabilitations,
        array $parameters,
        bool $expected
    ) {
        $service = $this->getService(
            $activePlateformeModules,
            $activeOrganismeModules,
            $agentHabilitations,
            $parameters
        );

        $actual = $service->$method($label);

        $this->assertEquals(
            $expected,
            $actual,
            sprintf(
                "%s - %s : \n%s\n%s\n%s\n%s",
                $method,
                $label,
                'Modules plateforme : ' . implode(', ', $activePlateformeModules),
                'Modules organisme : ' . implode(', ', $activeOrganismeModules),
                'Habilitations agent : ' . implode(', ', $agentHabilitations),
                'Paramètres : ' . http_build_query($parameters, '', ', ')
            )
        );
    }
}
