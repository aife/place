<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Agent\Submenus\StrategieAchat;

class StrategieAchatTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return StrategieAchat::class;
    }

    public function canSeeProvider(): array
    {
        return [
            [StrategieAchat::TRANSLATION_PREFIX, [], [], [], [], false],
            [StrategieAchat::TRANSLATION_PREFIX, ['menu_agent_complet'], [], [], [], true],
            [StrategieAchat::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['strategie_achat_gestion'], [], true],
            [StrategieAchat::TRANSLATION_PREFIX, [], ['module_recensement_programmation'], ['recensement_programmation_administration'], [], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['', [], [], [], [], false],
        ];
    }
}
