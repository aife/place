<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Bandeau\Menu\SocleInterne\Submenus;

use App\Service\Bandeau\Menu\SocleInterne\Submenus\Parametrage;
use Tests\Unit\Service\Bandeau\Menu\Agent\Submenus\AbstractSubmenuTester;

class ParametrageProviderTest extends AbstractSubmenuTester
{
    public function getClassUnderTestName(): string
    {
        return Parametrage::class;
    }

    public function canSeeProvider(): array
    {
        return [
            ['PORTAIL', [], [], [],[], true],
            ['ADMINISTRATEUR_AGENT', [], [], [], [], true],
            ['SERVICE', [], [], [], [], true],
            ['GESTION_SERVICE', [], [], [],[], true],
            ['GESTION_AGENTS', [], [], [],[], true],
        ];
    }

    public function canClickProvider(): array
    {
        return [
            ['SERVICE', [], [], [], [], false],
            ['ADMINISTRATEUR_AGENT', [], [], [], [], false],
            ['ADMINISTRATEUR_AGENT', [], [], ['hyper_admin'], [], true],
            ['GESTION_SERVICE', [], [], [], [], false],
            ['GESTION_SERVICE', [], [], ['gestion_agent_pole'], [], true],
            ['GESTION_AGENTS', [], [], [], [], false],
            ['GESTION_AGENTS', [], [], ['gestion_agents'], [], true],
        ];
    }
}
