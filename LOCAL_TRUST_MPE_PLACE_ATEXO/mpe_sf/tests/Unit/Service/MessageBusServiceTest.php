<?php

namespace Tests\Unit\Service;

use App\Message\DumePublication;
use App\Message\GenerationArchiveDocumentaire;
use App\Service\MessageBusService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class MessageBusServiceTest extends \Tests\Unit\AtexoTestCase
{
    public function testPublishDumePublication(): void
    {
        $logger = $this->createMock(LoggerInterface::class);
        $message = new DumePublication('acheteur', true, 1);
        $envelope = new Envelope($message);
        $messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageBusInterface->expects($this->exactly(1))
            ->method('dispatch')
            ->with($message)
            ->willReturn($envelope);

        $messageBusService = new MessageBusService($messageBusInterface, $logger);
        $messageBusService->publishDumePublication('acheteur', true, 1);
    }

    public function testPublishGenerationArchiveDocumentaire()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $message = new GenerationArchiveDocumentaire(
            '1600',
            '1',
            base64_encode('{"toto":["tata"]}')
        );
        $envelope = new Envelope($message);
        $messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $messageBusInterface->expects($this->exactly(1))
            ->method('dispatch')
            ->with($message)
            ->willReturn($envelope);

        $messageBusService = new MessageBusService($messageBusInterface, $logger);
        $messageBusService->publishGenerationArchiveDocumentaire(
            '1600',
            '1',
            base64_encode('{"toto":["tata"]}')
        );
    }
}
