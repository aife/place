<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Admin;

use App\Repository\OrganismeServiceMetierRepository;
use App\Service\Admin\ServiceMetierOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ServiceMetierOrganismeTest extends TestCase
{
    public function testGetAccessibleServices()
    {
        $rows[] = [
            'idServiceMetier' => 1,
            'sigle' => 'PLACE',
            'denomination' => 'MARCHES_PUBLICS_ELECTRONIQUES',
            'ServiceActive' => 1
        ];

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->exactly(2))
            ->method('get')
            ->willReturn(1)
            ;
        $manager = $this->createMock(EntityManagerInterface::class);
        $repoOrganismeServiceMetier = $this->createMock(OrganismeServiceMetierRepository::class);
        $repoOrganismeServiceMetier->expects($this->once())
            ->method('findByOrganismeOrServiceMetier')
            ->willReturn($rows);

        $manager->expects($this->any())
            ->method('getRepository')
            ->willReturn($repoOrganismeServiceMetier);


        $serviceMetierOrganisme = new ServiceMetierOrganisme(
            $manager,
            $parameterBag
        );
        $result = $serviceMetierOrganisme->getAccessibleServices('pmi-min-1', false);

        $this->assertCount(1, $result);
        $this->assertCount(5, $result[1]);
    }
}
