<?php

namespace Tests\Unit\Service;

use ReflectionClass;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\AffiliationServiceRepository;
use App\Repository\AgentRepository;
use App\Repository\Consultation\ConsultationFavorisRepository;
use App\Repository\Consultation\InterneConsultationRepository;
use App\Repository\Consultation\InterneConsultationSuiviSeulRepository;
use App\Repository\HabilitationAgentRepository;
use App\Repository\InvitePermanentTransverseRepository;
use App\Service\Perimeter\Perimeter;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Tests\Unit\AtexoTestCase;

/**
 * Class PerimeterServiceTest.
 */
class PerimeterServiceTest extends AtexoTestCase
{
    const EXPECTED = 'test@test.test';

    public function testGetListEmailAgent()
    {
        $expected = self::EXPECTED;
        $service = $this->getService();
        $consultation = $this->getConsultation();
        $result = $service->getListEmailAgent($consultation);
        $actual = $result[0] ?? null;

        $this->assertEquals($expected, $actual);
    }

    public function getConsultation()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('abc');
        $consultation = new Consultation();
        $consultation->setOrganisme($organisme);
        $class = new ReflectionClass($consultation);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($consultation, 1);

        return $consultation;
    }

    /**
     * @return Perimeter
     */
    public function getService()
    {
        $em = $this->getObjectManager();
        $logger = $this->createMock(LoggerInterface::class);
        $container = $this->createMock(ContainerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        return new Perimeter($em, $logger, $container, $parameterBag);
    }

    /**
     * @return EntityManagerInterface
     */
    protected function getObjectManager()
    {
        $objectManager = $this->createMock(EntityManagerInterface::class);
        $objectManager
            ->method('getRepository')
            ->willReturnCallback(
                function ($entityName) {
                    $agent = new Agent();
                    $agent->setEmail(PerimeterServiceTest::EXPECTED);
                    $class = new ReflectionClass($agent);
                    $property = $class->getProperty('id');
                    $property->setAccessible(true);
                    $property->setValue($agent, 1);

                    $service = new Service();
                    $class = new ReflectionClass($service);
                    $property = $class->getProperty('id');
                    $property->setAccessible(true);
                    $property->setValue($service, 1);

                    if (ConsultationFavoris::class === $entityName) {
                        $consultationFavory = new ConsultationFavoris();
                        $consultationFavory->setAgent($agent);
                        $repository = $this->createMock(ConsultationFavorisRepository::class);
                        $repository->expects($this->any())
                                    ->method('findBy')
                                    ->willReturn([$consultationFavory]);

                        return $repository;
                    }
                    if (InterneConsultation::class === $entityName) {
                        $entities = [(array) (new InterneConsultation())];
                        $repository = $this->createMock(InterneConsultationRepository::class);
                        $repository->expects($this->any())
                                    ->method('getGuest')
                                    ->willReturn($entities);

                        return $repository;
                    }

                    if (InterneConsultationSuiviSeul::class === $entityName) {
                        $entities = [(array) (new InterneConsultationSuiviSeul())];
                        $repository = $this->createMock(InterneConsultationSuiviSeulRepository::class);
                        $repository->expects($this->any())
                                    ->method('getGuest')
                                    ->willReturn($entities);

                        return $repository;
                    }

                    if (HabilitationAgent::class === $entityName) {
                        $entity = new HabilitationAgent();
                        $entity->setAgent($agent);
                        $repository = $this->createMock(HabilitationAgentRepository::class);
                        $repository->expects($this->any())
                                    ->method('retrievePermanentGuests')
                                    ->willReturn([$entity]);

                        return $repository;
                    }

                    if (InvitePermanentTransverse::class === $entityName) {
                        $entity = new InvitePermanentTransverse();
                        $repository = $this->createMock(InvitePermanentTransverseRepository::class);
                        $repository->expects($this->any())
                                    ->method('findOneBy')
                                    ->willReturn([$entity]);

                        return $repository;
                    }

                    if (AffiliationService::class === $entityName) {
                        $entity = new AffiliationService();
                        $entity->setServiceParentId($service);
                        $entities = [$entity];
                        $repository = $this->createMock(AffiliationServiceRepository::class);
                        $repository->expects($this->any())
                                    ->method('getParentByServiceIdAndOrganisme')
                                    ->willReturn($entities);

                        return $repository;
                    }

                    if (Agent::class === $entityName) {
                        $repository = $this->createMock(AgentRepository::class);
                        $repository->expects($this->any())
                                    ->method('getAgentsByAlerte')
                                    ->willReturn([$agent]);

                        return $repository;
                    }
                }
            );

        return $objectManager;
    }
}
