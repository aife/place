<?php

namespace Tests\Unit\Service;

use ReflectionClass;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\Organisme;
use App\Entity\RG;
use App\Repository\ComplementRepository;
use App\Repository\ConsultationRepository;
use App\Repository\DCERepository;
use App\Repository\RGRepository;
use App\Service\AtexoTelechargementConsultation;
use App\Service\AtexoUtil;
use App\Service\DocumentService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

class DocumentServiceTest extends AtexoTestCase
{
    private $consultation;

    protected function setUp(): void
    {
        $consultation = new Consultation();
        $class = new ReflectionClass($consultation);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($consultation, 93);
        $consultation->setAcronymeOrg('pmi-min-1');
        $this->consultation = $consultation;
    }

    public function testGetLienComplement()
    {
        $result = $this->getDocumentService()->getLienComplement($this->consultation);
        $this->assertEquals('/index.php?page=Entreprise.EntrepriseDownloadComplement&id=OTM=&orgAcronyme=pmi-min-1', $result['lien']);
        $this->assertEquals('En savoir plus sur la consultation - 1,95 Ko', $result['taille']);
    }

    public function testGetLienDCE()
    {
        $result = $this->getDocumentService()->getLienDCE($this->consultation);
        $this->assertEquals('/index.php?page=Entreprise.EntrepriseDemandeTelechargementDce&id=93&orgAcronyme=pmi-min-1', $result['lien']);
        $this->assertEquals('Dossier de consultation - 1,95 Ko', $result['taille']);
    }

    public function testGetLienRg()
    {
        $result = $this->getDocumentService()->getLienRg($this->consultation);
        $this->assertEquals('/index.php?page=Entreprise.EntrepriseDownloadReglement&id=OTM=&orgAcronyme=pmi-min-1', $result['lien']);
        $this->assertEquals('Règlement de consultation - 1,95 Ko', $result['taille']);
    }

    public function testGetLienRcByIdConsultation()
    {
        $lien = $this->getDocumentService()->getLienRcByIdConsultation(93);
        $this->assertEquals('/index.php?page=Entreprise.EntrepriseDownloadReglement&id=OTM=&orgAcronyme=pmi-min-1', $lien);
    }

    public function testGetLienRgByIdConsultationFailed()
    {
        $em = $this->createMock(EntityManager::class);
        $consultationRepository = $this->createMock(ConsultationRepository::class);
        $consultationRepository->expects($this->once())
            ->method('find')
            ->with(90)
            ->willReturn(null);

        $em->expects($this->once())
            ->method('getRepository')
            ->willReturn($consultationRepository);

        $this->expectException(NotFoundHttpException::class);
        $this->getDocumentService($em)->getLienRcByIdConsultation('90');
    }

    public function testGetLienRgByIdConsultationReturnEmpty()
    {
        $em = $this->createMock(EntityManager::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Consultation::class == $repository) {
                        $mockRepository = $this->createMock(ConsultationRepository::class);
                        $mockRepository->expects($this->once())
                            ->method('find')
                            ->with(90)
                            ->willReturn($this->consultation);

                        return $mockRepository;
                    }
                    if (RG::class == $repository) {
                        $mockRepository = $this->createMock(RGRepository::class);
                        $mockRepository->expects($this->once())
                            ->method('getReglement')
                            ->willReturn([]);

                        return $mockRepository;
                    }
                }
            );

        $lien = $this->getDocumentService($em)->getLienRcByIdConsultation(90);

        $this->assertEquals('', $lien);
    }

    public function getParameters()
    {
        return [
            'URL_MPE' => '/',
        ];
    }

    private function getEntityManager()
    {
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (Consultation::class == $repository) {
                        $repository = $this->createMock(ConsultationRepository::class);
                        $consultation = new Consultation();
                        $consultation->setAcronymeOrg('pmi-min-1');
                        $class = new ReflectionClass($consultation);
                        $property = $class->getProperty('id');
                        $property->setAccessible(true);
                        $property->setValue($consultation, 93);
                        $organisme = new Organisme();
                        $organisme->setAcronyme('pmi-min-1');
                        $consultation->setOrganisme($organisme);
                        $repository->expects($this->any())
                            ->method('find')
                            ->with(93)
                            ->willReturn($consultation);

                        return $repository;
                    }
                    if (RG::class == $repository) {
                        $repository = $this->createMock(RGRepository::class);
                        $rg = new RG();
                        $rg->setOrganisme('pmi-min-1');
                        $rg->setConsultationId(93);
                        $rg->setRg(193);
                        $repository->expects($this->any())
                            ->method('getReglement')
                            ->with(93, 'pmi-min-1')
                            ->willReturn($rg);

                        return $repository;
                    }
                    if (DCE::class == $repository) {
                        $repository = $this->createMock(DCERepository::class);
                        $dce = new DCE();
                        $dce->setOrganisme('pmi-min-1');
                        $dce->setConsultationId(93);
                        $dce->setDce(193);
                        $repository->expects($this->any())
                            ->method('getDce')
                            ->with(93, 'pmi-min-1')
                            ->willReturn($dce);

                        return $repository;
                    }
                    if (Complement::class == $repository) {
                        $repository = $this->createMock(ComplementRepository::class);
                        $complement = new Complement();
                        $complement->setOrganisme('pmi-min-1');
                        $complement->setConsultationId(93);
                        $complement->setComplement(193);
                        $repository->expects($this->any())
                            ->method('getComplement')
                            ->with(93, 'pmi-min-1')
                            ->willReturn($complement);

                        return $repository;
                    }
                }
            );

        return $em;
    }

    private function getTranslator()
    {
        $translator = $this->createMock(TranslatorInterface::class);
        $translator->expects($this->any())
            ->method('trans')
            ->willReturnCallback(
                function ($cle) {
                    switch ($cle) {
                        case 'DEFINE_REGLEMENT_CONSULTATION':
                            $result = 'Règlement de consultation';
                            break;
                        case 'DEFINE_DOSSIER_CONSULTATION':
                            $result = 'Dossier de consultation';
                            break;
                        case 'DEFINE_SAVOIR_PLUS_CONSULTATION':
                            $result = 'En savoir plus sur la consultation';
                    }

                    return $result;
                }
            );

        return $translator;
    }

    private function getDocumentService($em = null): DocumentService
    {
        $em = $em ?? $this->getEntityManager();
        $translatorInterface = $this->getTranslator();
        $telechargementConsultationService = $this->createMock(AtexoTelechargementConsultation::class);
        $telechargementConsultationService->expects($this->any())->method('getFileSize')->with(193, 'pmi-min-1')->willReturn(2000);
        $utilitaire = $this->createMock(AtexoUtil::class);
        $utilitaire->expects($this->any())->method('arrondirSizeFile')->with(1.953125)->willReturn('1,95 Ko');
        $parameters = $this->getParameters();

        return new DocumentService($em, $utilitaire, $telechargementConsultationService, $translatorInterface, $parameters);
    }
}
