<?php

namespace Tests\Unit\Service;

use App\Entity\MarchePublie;
use App\Repository\MarchePublieRepository;
use App\Service\MarchePublieService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MarchePublieServiceTest.
 */
class MarchePublieServiceTest extends TestCase
{
    /**
     * Test de création d'un MarchePublie.
     */
    public function testCreate()
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($entityName) {
                    static $callCount = [];
                    $callCount[$entityName] = !isset($callCount[$entityName]) ? 1 : ++$callCount[$entityName];
                    if (MarchePublie::class === $entityName) {
                        $marchePublie = new MarchePublie();
                        $marchePublie->setOrganisme('fakeOrganisme');
                        $marchePublie->setServiceId(5);
                        $marchePublie->setNumeroMarcheAnnee(2020);
                        $marchePublie->setIsPubliee('0');

                        $marchePublieRepository = $this->createMock(MarchePublieRepository::class);

                        $marchePublieRepository->expects($this->any())
                            ->method('getMarchePublieByYear')
                            ->with('pmi-min-1', 0, 2020)
                            ->willReturn(null);

                        return $marchePublieRepository;
                    }
                }
            ));

        $marchePublieService = new MarchePublieService($container, $em);
        $marchePublieCreated = $marchePublieService->create(2020, 'pmi-min-1');
        $this->assertTrue($marchePublieCreated instanceof MarchePublie);
        $this->assertEquals('pmi-min-1', $marchePublieCreated->getOrganisme());
    }
}
