<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service;

use App\Service\CertificateTransformer;
use AtexoCrypto\Dto\InfosSignature;
use PHPUnit\Framework\TestCase;

class CertificateTransformerTest extends TestCase
{
    public function testCertificateReturnNull()
    {
        $transformered = CertificateTransformer::transform('filename.pdf', 'signingFile.xml', null);

        $this->assertNull($transformered);
    }

    public function testTransform()
    {
        $infoSignature = new InfosSignature();
        $infoSignature->setPeriodiciteValide(true);
        $infoSignature->setRepertoiresChaineCertification([['nom' => 'LTS', 'statut' => 'OK']]);
        $infoSignature->setAbsenceRevocationCRL(0);
        $infoSignature->setSignatureValide(1);
        $infoSignature->setEmetteur('OU:atexo');
        $infoSignature->setSignataireComplet('OU:atexo');

        $transformed = CertificateTransformer::transform('filename.pdf', 'signingFile.xml', $infoSignature, true);

        $this->assertInstanceOf(CertificateTransformer::class, $transformed[0]);
        $this->assertTrue($transformed[0]->isValidCertificate);
        $this->assertSame('LTS', $transformed[0]->certificateReference);
    }
}
