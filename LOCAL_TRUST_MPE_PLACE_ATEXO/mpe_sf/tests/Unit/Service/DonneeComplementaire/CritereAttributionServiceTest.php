<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DonneeComplementaire;

use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\SousCritereAttribution;
use App\Service\DonneeComplementaire\CritereAttributionService;
use PHPUnit\Framework\TestCase;

class CritereAttributionServiceTest extends TestCase
{
    public function testIsCriterePonderationValidWithEmptyCriteres()
    {
        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid([], 50.0);

        self::assertTrue($result);
    }

    public function testIsCriterePonderationValidWithValidPonderation()
    {
        $criteres = [
            (new CritereAttribution())->setPonderation(30.0),
            (new CritereAttribution())->setPonderation(20.0),
        ];

        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid($criteres, 50.0);

        self::assertTrue($result);
    }

    public function testIsCriterePonderationValidWithInvalidPonderation()
    {
        $criteres = [
            (new CritereAttribution())->setPonderation(60.0),
            (new CritereAttribution())->setPonderation(40.0),
        ];

        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid($criteres, 50.0);

        self::assertFalse($result);
    }

    public function testIsSousCriterePonderationValidWithEmptyCriteres()
    {
        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid([], 50.0);

        self::assertTrue($result);
    }

    public function testIsSousCriterePonderationValidWithValidPonderation()
    {
        $sousCriteres = [
            (new SousCritereAttribution())->setPonderation(30.0),
            (new SousCritereAttribution())->setPonderation(20.0),
        ];

        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid($sousCriteres, 50.0);

        self::assertTrue($result);
    }

    public function testIsSousCriterePonderationValidWithInvalidPonderation()
    {
        $sousCriteres = [
            (new SousCritereAttribution())->setPonderation(60.0),
            (new SousCritereAttribution())->setPonderation(40.0),
        ];

        $service = new CritereAttributionService();

        $result = $service->isCriterePonderationValid($sousCriteres, 50.0);

        self::assertFalse($result);
    }
}
