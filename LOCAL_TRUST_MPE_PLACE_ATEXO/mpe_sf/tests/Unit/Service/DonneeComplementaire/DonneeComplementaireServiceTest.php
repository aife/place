<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\DonneeComplementaire;

use App\Entity\FormePrix;
use App\Entity\FormePrixPuHasRefVariation;
use App\Entity\Tranche;
use App\Entity\ValeurReferentiel;
use App\Repository\FormePrixHasRefTypePrixRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use App\Repository\FormePrixRepository;
use App\Repository\TrancheRepository;
use App\Repository\ValeurReferentielRepository;
use Doctrine\Common\Collections\ArrayCollection;
use ReflectionClass;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Lot;
use App\Repository\Consultation\CritereAttributionRepository;
use App\Repository\ConsultationRepository;
use App\Repository\LotRepository;
use App\Service\DonneeComplementaire\DonneeComplementaireService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;

class DonneeComplementaireServiceTest extends TestCase
{
    public function testTypeCriteriaAttributionIsEmpty()
    {
        $donneeComplementaireService = new DonneeComplementaireService(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $result = $donneeComplementaireService->getTypeCriteriaAttribution(null);

        $this->assertEmpty($result);
    }

    /**
     * @dataProvider criteriaAttributionProvider
     */
    public function testTypeCriteriaAttributionIsOk($i, $expected)
    {
        $donneeComplementaireService = new DonneeComplementaireService(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $result = $donneeComplementaireService->getTypeCriteriaAttribution($i);

        $this->assertSame($expected, $result);
    }

    public function criteriaAttributionProvider()
    {
        return [
            [0, ''],
            [1, 'LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC'],
            [2, 'LIBELLE_CRITERE_ATTRIBUTION_PONDERATION'],
            [3, 'LIBELLE_CRITERE_ATTRIBUTION_PRIX_UNIQUE'],
            [4, 'LIBELLE_CRITERE_ATTRIBUTION_DANS_DOC_CONSULTATION'],
            [5, 'LIBELLE_CRITERE_ATTRIBUTION_COUT_UNIQUE'],
            [6, ''],
        ];
    }

    public function testSaveCriteriaAttributionGetException()
    {
        // Test 1 : No criteria send
        $donneeComplementaireService = new DonneeComplementaireService(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $dataRaw = '{"criteria": []}';
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('No criteria found in the json');
        $donneeComplementaireService->saveCriteriaAttribution($dataRaw);

        // Test 2 : Consultation not found
        $consultationRepo = $this->createMock(ConsultationRepository::class);
        $consultationRepo->expects($this->once())
            ->method('find')
            ->with(502052)
            ->willReturn(null);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->once())
            ->method('getRepository')
            ->willReturn($consultationRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $dataRaw = '{"criteria": 
            [{"id_consultation": "502052", "id_lot": "1866","name": "Mon critère 1 test", "rate": "20",
             "sub_criteria":null}]
        }';
        $this->expectException(NotFoundHttpException::class);
        $this->expectExceptionMessage('Consultation 502052 not found.');
        $donneeComplementaireService->saveCriteriaAttribution($dataRaw);
    }

    public function testSaveEntireCriteriaAttribution()
    {
        $dataRaw = '{"criteria" : 
            [
                {"id_consultation": "502052", "id_lot": "1866","name": "Mon critère 1 test", "rate": "20",
                    "sub_criteria": null, "identical_bareme": true},
                {"id_consultation": "502052", "id_lot": "1867", "name": "Mon critère 2 test", "rate": "80", 
                    "sub_criteria" : [
                        {"name": "Mon sous critère 1 test", "rate": "60"},
                        {"name": "Mon sous critère 2 test", "rate": "20"}
                    ], 
                    "identical_bareme": null
                },
                {"id_consultation": "502052", "id_lot": "","name": "Mon critère 3 test", "rate": "50",
                    "sub_criteria": null, "identical_bareme": null}
            ]
        }';

        $data = json_decode($dataRaw, true);

        $idConsultation = $data['criteria'][0]['id_consultation'];

        $consultation = new Consultation();
        $consultation->setId($idConsultation);

        $donneeComplementaire = new DonneeComplementaire();
        $consultation->setDonneeComplementaire($donneeComplementaire);

        $consultationRepo = $this->createMock(ConsultationRepository::class);
        $consultationRepo->expects($this->once())
            ->method('find')
            ->with($idConsultation)
            ->willReturn($consultation);

        $lot1 = new Lot();
        $donneeComplementaireLot1 = new DonneeComplementaire();
        $donneeComplementaireLot1->setCriteresIdentiques($data['criteria'][0]['identical_bareme']);
        $lot1->setDonneeComplementaire($donneeComplementaireLot1);
        $reflectionClass1 = new ReflectionClass(Lot::class);
        $reflection1 = $reflectionClass1->getProperty('id');
        $reflection1->setAccessible(true);
        $reflection1->setValue($lot1, $data['criteria'][0]['id_lot']);

        $this->assertTrue((bool) $lot1->getDonneeComplementaire()->getCriteresIdentiques());

        $lot2 = new Lot();
        $donneeComplementaireLot2 = new DonneeComplementaire();
        $lot2->setDonneeComplementaire($donneeComplementaireLot2);
        $reflectionClass2 = new ReflectionClass(Lot::class);
        $reflection2 = $reflectionClass2->getProperty('id');
        $reflection2->setAccessible(true);
        $reflection2->setValue($lot2, $data['criteria'][1]['id_lot']);

        $lotRepo = $this->createMock(LotRepository::class);
        $lotRepo->expects($this->exactly(2))
            ->method('find')
            ->withConsecutive(
                [$data['criteria'][0]['id_lot']],
                [$data['criteria'][1]['id_lot']]
            )
            ->willReturnOnConsecutiveCalls($lot1, $lot2);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($consultationRepo, $lotRepo, $lotRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $donneeComplementaireService->saveCriteriaAttribution($dataRaw);
    }

    public function testGetCriteriaAttributionInfosWithoutLotIsEmpty()
    {
        $consultation = new Consultation();

        $lotRepo = $this->createMock(LotRepository::class);
        $lotRepo->expects($this->once())
            ->method('findBy')
            ->with(['consultation' => $consultation])
            ->willReturn([]);

        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->once())
            ->method('findBy')
            ->with(
                ['donneeComplementaire' => $consultation->getDonneeComplementaire()],
                ['id' => 'ASC']
            )
            ->willReturn([]);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($lotRepo, $criteriaAttributionRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $result = $donneeComplementaireService->getCriteriaAttributionInfos($consultation);

        $this->checkStructureArrayReturned($result);
        $this->assertCount(1, $result['listCriteriaAttribution']);
        $this->assertSame([], $result['listCriteriaAttribution'][0]['criteria']);
        $this->assertSame([], $result['infoLotFront']);
    }

    public function testGetCriteriaAttributionInfosWithoutLotIsFull()
    {
        $donneeComplementaire = new DonneeComplementaire();
        $donneeComplementaire->setIdCritereAttribution(2);

        $consultation = new Consultation();
        $consultation->setDonneeComplementaire($donneeComplementaire);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('REFERENTIEL_CRITERE_ATTRIBUTION')
            ->willReturn(19)
        ;

        $valeurRefentiel = (new ValeurReferentiel())
            ->setId(2)
            ->setIdReferentiel(19)
            ->setLibelleValeurReferentiel('Critères énoncés ci-après avec leur pondération')
        ;
        $valeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 2,
                    'idReferentiel' => 19,
                ]
            )
            ->willReturn($valeurRefentiel)
        ;

        $lotRepo = $this->createMock(LotRepository::class);
        $lotRepo->expects($this->once())
            ->method('findBy')
            ->with(['consultation' => $consultation])
            ->willReturn([]);

        $criteriaAttribution = new CritereAttribution();
        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->once())
            ->method('findBy')
            ->with(
                ['donneeComplementaire' => $consultation->getDonneeComplementaire()],
                ['id' => 'ASC']
            )
            ->willReturn([$criteriaAttribution]);


        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($valeurReferentielRepository, $lotRepo, $criteriaAttributionRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $parameterBag,
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $result = $donneeComplementaireService->getCriteriaAttributionInfos($consultation);

        $resultCritereAttribution = [
                'id' => 2,
                'idReferentiel' => 19,
                'libelle' => "Critères énoncés ci-après avec leur pondération",
            ]
        ;

        $this->checkStructureArrayReturned($result);
        $this->assertSame($resultCritereAttribution, $result['typeCriteriaAttribution']);
        $this->assertCount(1, $result['listCriteriaAttribution']);
        $this->assertCount(1, $result['listCriteriaAttribution'][0]['criteria']);
        $this->assertInstanceOf(
            CritereAttribution::class,
            $result['listCriteriaAttribution'][0]['criteria'][0]
        );
        $this->assertSame([], $result['infoLotFront']);
    }

    public function testGetCriteriaAttributionInfosWithLotIsEmpty()
    {
        $donneeComplementaire = new DonneeComplementaire();
        $donneeComplementaire->setIdCritereAttribution(2);

        $consultation = new Consultation();

        $lot = new Lot();
        $lot->setDonneeComplementaire($donneeComplementaire);
        $lotRepo = $this->createMock(LotRepository::class);
        $lotRepo->expects($this->once())
            ->method('findBy')
            ->with(['consultation' => $consultation])
            ->willReturn([$lot]);

        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->exactly(2))
            ->method('findBy')
            ->withConsecutive(
                [
                    ['donneeComplementaire' => $donneeComplementaire, 'lot' => $lot],
                    ['id' => 'ASC']
                ],
                [
                    ['donneeComplementaire' => $donneeComplementaire],
                    ['id' => 'ASC']
                ]
            )
            ->willReturnOnConsecutiveCalls([], []);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($lotRepo, $criteriaAttributionRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $result = $donneeComplementaireService->getCriteriaAttributionInfos($consultation);

        $this->checkStructureArrayReturned($result);
        $this->assertCount(1, $result['listCriteriaAttribution']);
        $this->assertCount(2, $result['listCriteriaAttribution'][0]);
        $this->assertArrayHasKey('lot', $result['listCriteriaAttribution'][0]);
        $this->assertArrayHasKey('criteria', $result['listCriteriaAttribution'][0]);
        $this->assertCount(0, $result['listCriteriaAttribution'][0]['criteria']);
        $this->assertInstanceOf(Lot::class, $result['listCriteriaAttribution'][0]['lot']);
        $this->assertNotEmpty($result['infoLotFront']);
    }

    public function testGetCriteriaAttributionInfosWithLotIsFull()
    {
        $donneeComplementaire = new DonneeComplementaire();
        $donneeComplementaire->setIdCritereAttribution(2);
        $donneeComplementaire->setCriteresIdentiques(1);

        $consultation = new Consultation();
        $consultation->setDonneeComplementaire($donneeComplementaire);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('REFERENTIEL_CRITERE_ATTRIBUTION')
            ->willReturn(19)
        ;

        $valeurRefentiel = (new ValeurReferentiel())
            ->setId(2)
            ->setIdReferentiel(19)
            ->setLibelleValeurReferentiel('Critères énoncés ci-après avec leur pondération')
        ;
        $valeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 2,
                    'idReferentiel' => 19,
                ]
            )
            ->willReturn($valeurRefentiel)
        ;

        $lot = new Lot();
        $lot->setDonneeComplementaire($donneeComplementaire);
        $lotRepo = $this->createMock(LotRepository::class);
        $lotRepo->expects($this->once())
            ->method('findBy')
            ->with(['consultation' => $consultation])
            ->willReturn([$lot]);

        $criteriaAttribution = new CritereAttribution();
        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->once())
            ->method('findBy')
            ->with(
                ['donneeComplementaire' => $donneeComplementaire, 'lot' => $lot],
                ['id' => 'ASC']
            )
            ->willReturn([$criteriaAttribution]);

        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($valeurReferentielRepository, $lotRepo, $criteriaAttributionRepo);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $parameterBag,
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $result = $donneeComplementaireService->getCriteriaAttributionInfos($consultation);

        $this->checkStructureArrayReturned($result);
        $this->assertArrayHasKey('lot', $result['listCriteriaAttribution'][0]);
        $this->assertArrayHasKey('criteria', $result['listCriteriaAttribution'][0]);
        $this->assertInstanceOf(Lot::class, $result['listCriteriaAttribution'][0]['lot']);
        $this->assertNotEmpty($result['infoLotFront']);
        $this->assertSame(1, $result['identicalBareme']);
    }

    public function testGetTranches()
    {
        $valeurReferentiel = new ValeurReferentiel();
        $valeurReferentiel->setId(1)
            ->setLibelleValeurReferentiel('Prix révisables');
        $tranche = new Tranche();
        $tranche->setCodeTranche(3)
            ->setIntituleTranche('Tranche1')
            ->setNatureTranche('TF')
            ->setIdFormePrix(1);
        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockTrancheRepository = $this->createMock(TrancheRepository::class);
        $mockValeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $mockTrancheRepository->method('findBy')
            ->willReturn([$tranche]);
        $mockValeurReferentielRepository->method('findBy')
            ->willReturn([$valeurReferentiel]);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($mockValeurReferentielRepository, $mockValeurReferentielRepository, $mockValeurReferentielRepository, $mockTrancheRepository);

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $consultation = new Consultation();
        $consultation->setAlloti('1');
        $lot = new Lot();
        $donneeComplementaire = new class () extends DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 1;
            }
        };
        $lot->setDonneeComplementaire($donneeComplementaire);

        $data = $donneeComplementaireService->getLotTranches($lot);

        $this->assertIsArray($data);
        $this->assertArrayHasKey('numero', $data[0]);
        $this->assertSame('Tranche1', $data[0]['intitule']);
    }

    public function testGetFormePrix()
    {
        $valeurReferentiel = new ValeurReferentiel();
        $valeurReferentiel->setId(1)
            ->setLibelleValeurReferentiel('Prix révisables');
        $forme = new class () extends FormePrix {
            public function getIdFormePrix(): ?int
            {
                return 1;
            }
        };
        $pf = new FormePrixPuHasRefVariation();
        $pf->setIdFormePrix(33)
            ->setIdVariation(1);
        $forme->setFormePrix('PF');
        $tranche = new Tranche();
        $tranche->setCodeTranche(3)
            ->setIdFormePrix(4)
            ->setIntituleTranche('Tranche1')
            ->setNatureTranche('TF');
        $mockEntityManager = $this->createMock(EntityManagerInterface::class);
        $mockTrancheRepository = $this->createMock(TrancheRepository::class);
        $mockValeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $mockTrancheRepository->method('findBy')
            ->willReturn([$tranche]);
        $mockValeurReferentielRepository->method('findBy')
            ->willReturn([$valeurReferentiel]);
        $mockEntityManager->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($mockValeurReferentielRepository, $mockValeurReferentielRepository, $mockValeurReferentielRepository, $mockTrancheRepository);
        $formePrixRepository =  $this->createMock(FormePrixRepository::class);
        $formePrixRepository->method('findOneBy')
            ->willReturn($forme);
        $pfRepository = $this->createMock(FormePrixPfHasRefVariationRepository::class);
        $pfRepository->method('findOneBy')
            ->willReturn($pf);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock->method('get')
            ->willReturnMap([
                ['KEY_PRIX_UNITAIRE', 'PU'],
                ['KEY_PRIX_FORFAITAIRE', 'PF']
            ]);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $translatorMock->method('trans')
            ->willReturn('Prix forfaitaire');

        $donneeComplementaireService = new DonneeComplementaireService(
            $mockEntityManager,
            $formePrixRepository,
            $parameterBagMock,
            $translatorMock,
            $pfRepository,
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );
        $consultation = new Consultation();
        $consultation->setAlloti('1');
        $lot = new Lot();
        $donneeComplementaire = new class () extends DonneeComplementaire {
            public function getIdDonneeComplementaire(): int
            {
                return 1;
            }
        };
        $lot->setDonneeComplementaire($donneeComplementaire);

        $data = $donneeComplementaireService->getLotFormePrix($lot);

        $this->assertIsArray($data);
        $this->assertArrayHasKey('formePrix', $data[0]);
        $this->assertArrayHasKey('variation', $data[0]);
        $this->assertSame('Prix forfaitaire', $data[0]['formePrix']);
        $this->assertSame('Prix révisables', $data[0]['variation']);
    }

    private function checkStructureArrayReturned(array $infos): void
    {
        $this->assertCount(5, $infos);
        $this->assertArrayHasKey('typeCriteriaAttribution', $infos);
        $this->assertArrayHasKey('listCriteriaAttribution', $infos);
        $this->assertArrayHasKey('infoLotFront', $infos);
        $this->assertArrayHasKey('identicalBareme', $infos);
    }

    public function testGetLotCriteriaAttributionWithCriteria()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $sousCritere1_1 = (new Consultation\SousCritereAttribution())
            ->setPonderation('50')->setEnonce('sousCritere1')
        ;
        $sousCritere1_2 = (new Consultation\SousCritereAttribution())
            ->setPonderation('50')->setEnonce('sousCritere2')
        ;

        $sousCritere2_1 = (new Consultation\SousCritereAttribution())
            ->setPonderation('25')->setEnonce('sousCritere3')
        ;
        $sousCritere2_2 = (new Consultation\SousCritereAttribution())
            ->setPonderation('75')->setEnonce('sousCritere4')
        ;

        $critere1 = (new CritereAttribution())
            ->setPonderation('66')
            ->setEnonce('critere1')
            ->setOrdre(1)
            ->setSousCriteres(new ArrayCollection([$sousCritere1_1, $sousCritere1_2]))
        ;
        $critere2 = (new CritereAttribution())
            ->setPonderation('34')
            ->setEnonce('critere2')
            ->setOrdre(2)
            ->setSousCriteres(new ArrayCollection([$sousCritere2_1, $sousCritere2_2]))
        ;

        $donneeComplementaire = (new DonneeComplementaire())
            ->setCriteresAttribution(new ArrayCollection([$critere1, $critere2]))
            ->setIdCritereAttribution(2)
        ;

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with('REFERENTIEL_CRITERE_ATTRIBUTION')
            ->willReturn(19)
        ;

        $valeurRefentiel = (new ValeurReferentiel())
            ->setId(2)
            ->setIdReferentiel(19)
            ->setLibelleValeurReferentiel('Critères énoncés ci-après avec leur pondération')
        ;
        $valeurReferentielRepository = $this->createMock(ValeurReferentielRepository::class);
        $valeurReferentielRepository
            ->expects($this->once())
            ->method('findOneBy')
            ->with(
                [
                    'id' => 2,
                    'idReferentiel' => 19,
                ]
            )
            ->willReturn($valeurRefentiel)
        ;

        $lot = (new Lot())->setDonneeComplementaire($donneeComplementaire);

        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->exactly(2))
            ->method('findBy')
            ->withConsecutive(
                [
                    ['donneeComplementaire' => $donneeComplementaire, 'lot' => $lot],
                    ['id' => 'ASC']
                ],
                [
                    ['donneeComplementaire' => $donneeComplementaire],
                    ['id' => 'ASC']
                ]
            )
            ->willReturnOnConsecutiveCalls([], [$critere1, $critere2])
        ;

        $em
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($valeurReferentielRepository, $criteriaAttributionRepo, $criteriaAttributionRepo)
        ;

        $donneeComplementaireService = new DonneeComplementaireService(
            $em,
            $this->createMock(FormePrixRepository::class),
            $parameterBag,
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $result = $donneeComplementaireService->getLotCriteriaAttribution($lot);

        $expectedResult = [
            'typeCriteriaAttribution' => [
                'id' => 2,
                'idReferentiel' => 19,
                'libelle' => "Critères énoncés ci-après avec leur pondération",
            ],
            'listCriteriaAttribution' => [
                [
                    'enonce' => 'critere1',
                    'ordre' => 1,
                    'ponderation' => 66,
                    'sousCritereAttribution' => [
                        [
                            'enonce' => 'sousCritere1',
                            'ponderation' => 50,
                        ],
                        [
                            'enonce' => 'sousCritere2',
                            'ponderation' => 50,
                        ],
                    ],
                ],
                [
                    'enonce' => 'critere2',
                    'ordre' => 2,
                    'ponderation' => 34,
                    'sousCritereAttribution' => [
                        [
                            'enonce' => 'sousCritere3',
                            'ponderation' => 25,
                        ],
                        [
                            'enonce' => 'sousCritere4',
                            'ponderation' => 75,
                        ],
                    ],
                ],
            ]
        ];

        $this->assertEquals($expectedResult, $result);
    }

    public function testGetLotCriteriaAttributionWithoutCriteria()
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $donneeComplementaire = new DonneeComplementaire();

        $lot = (new Lot())->setDonneeComplementaire($donneeComplementaire);

        $criteriaAttributionRepo = $this->createMock(CritereAttributionRepository::class);
        $criteriaAttributionRepo->expects($this->exactly(2))
            ->method('findBy')
            ->withConsecutive(
                [
                    ['donneeComplementaire' => $donneeComplementaire, 'lot' => $lot],
                    ['id' => 'ASC']
                ],
                [
                    ['donneeComplementaire' => $donneeComplementaire],
                    ['id' => 'ASC']
                ]
            )
            ->willReturnOnConsecutiveCalls([], [])
        ;

        $em
            ->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($criteriaAttributionRepo, $criteriaAttributionRepo)
        ;

        $donneeComplementaireService = new DonneeComplementaireService(
            $em,
            $this->createMock(FormePrixRepository::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(FormePrixPfHasRefVariationRepository::class),
            $this->createMock(FormePrixPuHasRefVariationRepository::class),
            $this->createMock(FormePrixHasRefTypePrixRepository::class)
        );

        $result = $donneeComplementaireService->getLotCriteriaAttribution($lot);

        $expectedResult = [
            'typeCriteriaAttribution' => [],
            'listCriteriaAttribution' => []
        ];

        $this->assertEquals($expectedResult, $result);
    }
}
