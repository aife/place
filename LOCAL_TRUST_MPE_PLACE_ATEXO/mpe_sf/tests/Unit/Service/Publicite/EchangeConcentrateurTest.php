<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Publicite;

use App\Entity\Service;
use App\Repository\AgentRepository;
use App\Service\OrganismeService;
use App\Service\WebservicesConcentrateur;
use DateTime;
use App\Entity\Consultation\CritereAttribution;
use DOMDocument;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationComptePub;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\GeolocalisationN2;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\Tranche;
use App\Entity\TypeProcedure;
use App\Entity\ValeurReferentiel;
use App\Model\TypePrestation;
use App\Repository\ConsultationRepository;
use App\Repository\GeolocalisationN2Repository;
use App\Repository\LotRepository;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use App\Service\ClientWs\ClientWs;
use App\Service\ContratService;
use App\Service\DonneeComplementaireService;
use App\Service\Publicite\EchangeConcentrateur;
use App\Utils\Publicite\Utils;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Exception;
use Gesdinet\JWTRefreshTokenBundle\Entity\RefreshToken;
use Gesdinet\JWTRefreshTokenBundle\Generator\RefreshTokenGeneratorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Runner\BaseTestRunner;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;
use Tests\Unit\TwigTemplateTrait;
use Twig\Environment;

class EchangeConcentrateurTest extends AtexoTestCase
{
    use TwigTemplateTrait;

    public const XML_OUTPUT_PATH = __DIR__ . '/../../../../var/test/EchangeConcentrateurTest.xml';

    protected EchangeConcentrateur $echangeConcentrateur;

    protected function tearDown(): void
    {
        parent::tearDown();

        // Pour pouvoir vérifier la validation du XML vis-à-vis de la XSD dans PhpStorm,
        // il faut que le fichier ne soit pas en lecture seule.
        if (is_file(self::XML_OUTPUT_PATH)) {
            chmod(self::XML_OUTPUT_PATH, 0666);
        }
        $status = $this->getStatus();
        if (BaseTestRunner::STATUS_PASSED == $status) {
            if (is_file(self::XML_OUTPUT_PATH)) {
                unlink(self::XML_OUTPUT_PATH);
            }
            if (is_dir(dirname(self::XML_OUTPUT_PATH))) {
                rmdir(dirname(self::XML_OUTPUT_PATH));
            }
        }
    }

    public function generateValidXmlAnnonceDataProvider(): array
    {
        return [
            ['Consultation non allotie', false],
            ['Consultation allotie', true],
        ];
    }

    /**
     * @dataProvider generateValidXmlAnnonceDataProvider
     *
     * @throws Exception
     */
    public function testGenerateValidXmlAnnonce(string $testTitle, bool $allotie)
    {
        $xml = $this->getGeneratedXml($allotie);

        $this->assertNotEmpty($xml);

        libxml_use_internal_errors(true);
        $docXml = simplexml_load_string($xml);
        $this->assertNotFalse($docXml, 'Le XML généré doit être valide : ' . $testTitle);
        $errors = libxml_get_errors();
        $this->assertEmpty($errors, 'Parser le XML généré ne doit déclencher aucune erreur : ' . $testTitle);
        libxml_clear_errors();
    }

    public function xmlNodesValuesDataProvider(): array
    {
        return [
            ['SENDER/IDENTIFICATION/ESENDER_LOGIN', 'LAPFMPE'],
            ['SENDER/IDENTIFICATION/NO_DOC_EXT', '2020-000123'],
            ['SENDER/CONTACT/ORGANISATION', 'Dénomination Organisme'],
            ['SENDER/CONTACT/PHONE', '+33 100040404'],
            ['SENDER/CONTACT/E_MAIL', 'agent@organisme.fr'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/OFFICIALNAME', 'Dénomination compte pub'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/ADDRESS', 'Adresse compte pub'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/TOWN', 'Lyon'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/POSTAL_CODE', '69003'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/PHONE', '+33 100040404'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/E_MAIL', 'comptepub-agent@organisme.fr'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/CONTACT_POINT', 'Nom compte pub'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/URL_GENERAL', 'http://url-compte-pub.fr'],
            ['//CONTRACTING_BODY/ADDRESS_CONTRACTING_BODY/URL_BUYER', 'https://docker.local-trust.com'],
            [
                '//CONTRACTING_BODY/URL_DOCUMENT',
        'https://docker.local-trust.com/index.php?page=Entreprise.EntrepriseDetailsConsultation&id=123&orgAcronyme=',
            ],
            ['//COMPLEMENTARY_INFO/ADDRESS_REVIEW_BODY/OFFICIALNAME', 'Instance Recours Organisme'],
            ['//COMPLEMENTARY_INFO/ADDRESS_REVIEW_BODY/ADDRESS', 'Instance Recours Adresse'],
            ['//COMPLEMENTARY_INFO/ADDRESS_REVIEW_BODY/TOWN', 'Marseille'],
            ['//COMPLEMENTARY_INFO/ADDRESS_REVIEW_BODY/POSTAL_CODE', '13007'],
            ['//COMPLEMENTARY_INFO/ADDRESS_REVIEW_BODY/URL_GENERAL', 'http://url-instance-recours.com/'],
            ['//LEFTI/LEGAL_FORM/P', ''],
            ['//OBJECT_CONTRACT/OBJECT_DESCR[@ITEM=1]/TITLE/P', 'Lot 1 : étude'],
            ['//OBJECT_CONTRACT/OBJECT_DESCR[@ITEM=2]/TITLE/P', 'Lot 2 : réalisation'],
        ];
    }

    /**
     * @dataProvider xmlNodesValuesDataProvider
     *
     * @throws Exception
     */
    public function testGeneratedXmlContainsExpectedData($nodePath, $expectedValue)
    {
        $xml = $this->getGeneratedXml(true);
        $xml = str_replace('xmlns=', 'ns=', $xml);
        $docXml = simplexml_load_string($xml);

        $node = $docXml->xpath($nodePath);
        $this->assertCount(
            1,
            $node,
            "Le noeud $nodePath n'a pas été trouvé ou alors en plusieurs exemplaires."
        );
        $xmlNode = $node[0];

        $this->assertEquals($expectedValue, (string) $xmlNode);
    }

    public function xmlNodesDataProvider(): array
    {
        return [
            ['//ADDRESS_CONTRACTING_BODY/n2016:NUTS[@CODE="BE221"]'],
            ['//ADDRESS_FURTHER_INFO/n2016:NUTS[@CODE="BE221"]'],
            ['//OBJECT_CONTRACT/OBJECT_DESCR/n2016:NUTS[@CODE="BE221"]'],
            ['//OBJECT_CONTRACT/OBJECT_DESCR/n2016:NUTS[@CODE="CH056"]'],
        ];
    }

    /**
     * @dataProvider xmlNodesDataProvider
     *
     * @throws Exception
     */
    public function testGeneratedXmlContainsExpectedNodes($nodePath)
    {
        $xml = $this->getGeneratedXml();
        $xml = str_replace('xmlns=', 'ns=', $xml);
        $docXml = simplexml_load_string($xml);

        $node = $docXml->xpath($nodePath);
        $this->assertCount(
            1,
            $node,
            "Le noeud $nodePath n'a pas été trouvé ou alors en plusieurs exemplaires."
        );
    }

    protected function getComptePub(): ConsultationComptePub
    {
        $comptePub = new ConsultationComptePub();
        $comptePub->setDenomination('Dénomination compte pub');
        $comptePub->setPrm('Nom compte pub');
        $comptePub->setAdresse('Adresse compte pub');
        $comptePub->setCp('69003');
        $comptePub->setVille('Lyon');
        $comptePub->setEmail('comptepub-agent@organisme.fr');
        $comptePub->setTelephone('+33 100040404');
        $comptePub->setUrl('http://url-compte-pub.fr');
        $comptePub->setInstanceRecoursOrganisme('Instance Recours Organisme');
        $comptePub->setInstanceRecoursAdresse('Instance Recours Adresse');
        $comptePub->setInstanceRecoursCp('13007');
        $comptePub->setInstanceRecoursVille('Marseille');
        $comptePub->setInstanceRecoursUrl('http://url-instance-recours.com/');
        $comptePub->setFactureDenomination('Facture Dénomination');
        $comptePub->setFactureAdresse('Facture Adresse');
        $comptePub->setFactureCp('59000');
        $comptePub->setFactureVille('Lille');

        return $comptePub;
    }

    protected function getConsultation(
        DonneeComplementaire $donneesComplementaires,
        Organisme $organisme,
        bool $allotie = false,
        int $id = 123
    ): Consultation {
        $consultation = new Consultation();
        $consultation->setId($id);
        $consultation->setCategorie(TypePrestation::TYPE_PRESTATION_TRAVAUX);
        $consultation->setDonneeComplementaire($donneesComplementaires);
        $consultation->setOrganisme($organisme);
        $consultation->setDatedebut(new DateTime('2020-12-05'));
        $consultation->setDateFin(new DateTime('2021-06-21'));
        $consultation->setCodesNuts('DK050#ES514');
        $consultation->setReferenceUtilisateur('REFERENCE_UTILISATEUR');
        $consultation->setCodeCpv1('03111100');

        // Les codes CPV complémentaires ne sont pas ajoutés dans le XML
        // car on ne sait pas comment matcher avec les cpv_supplementary_codes (AA01, AA01, ...)
        // attendus par la XSD.
        $consultation->setCodeCpv2('03111101#03111102');

        $consultation->setObjet('Objet de la consultation.' . PHP_EOL . 'Sur 2 lignes.');
        $consultation->setProcedureType((new TypeProcedure())->setValueBindingsub('01_AOO'));
        $consultation->setAlloti($allotie ? '1' : '0');

        $consultation->setGroupement(false);
        $consultation->setLieuExecution('75,93');
        $consultation->setProcedureOuverte(true);
        $consultation->setIdCreateur(44);

        return $consultation;
    }

    protected function getOrganisme(): Organisme
    {
        $organisme = new Organisme();
        $organisme->setDenominationOrg('Dénomination Organisme');
        $organisme->setPays('France');
        $organisme->setTel('01 23 45 67 89');
        $organisme->setEmail('email@organisme.fr');
        $organisme->setSiren('110046018');
        $organisme->setComplement('00013');

        return $organisme;
    }

    /**
     * @param bool $allotie
     * @return EchangeConcentrateur
     */
    protected function getEchangeConcentrateur(bool $allotie = false): EchangeConcentrateur
    {
        if (!isset($this->echangeConcentrateur)) {
            $parameterBag = $this->createMock(ParameterBagInterface::class);
            $logger = $this->createMock(LoggerInterface::class);
            $em = $this->createMock(EntityManagerInterface::class);
            $clientWS = $this->createMock(ClientWs::class);
            $contratService = $this->createMock(ContratService::class);
            $contratService->method('getTechniqueAchatLibelle')->willReturn('Sans objet');

            $twig = $this->getTemplate(dirname(__FILE__) . '/../../../../templates');

            $parameterBag
                ->method('get')
                ->willReturnMap(
                    [
                        ['TYPE_PRESTATION_TRAVAUX', TypePrestation::TYPE_PRESTATION_TRAVAUX],
                        ['TYPE_PRESTATION_FOURNITURES', TypePrestation::TYPE_PRESTATION_FOURNITURES],
                        ['TYPE_PRESTATION_SERVICES', TypePrestation::TYPE_PRESTATION_SERVICES],
                        ['PF_URL_REFERENCE', 'https://ma-pf-mpe.fr/'],
                        ['UID_PF_MPE', 'LA_PF_MPE'],
                    ]
                );

            $critereAttribution1 = new CritereAttribution();
            $critereAttribution1->setEnonce('Coût de la solution');
            $critereAttribution1->setPonderation(30);
            $critereAttribution1->setSousCriteres(new ArrayCollection());

            $critereAttribution2 = new CritereAttribution();
            $critereAttribution2->setEnonce('Efficacité de la solution');
            $critereAttribution2->setPonderation(70);
            $critereAttribution2->setSousCriteres(new ArrayCollection());

            $donneesComplementaires = $this->createMock(DonneeComplementaire::class);
            $donneesComplementaires
                ->method('getIdDonneeComplementaire')
                ->willReturn(456);
            $donneesComplementaires
                ->method('getMontantMarche')
                ->willReturn(123000.00);
            $donneesComplementaires
                ->method('getIdCritereAttribution')
                ->willReturn(1313131);
            $donneesComplementaires
                ->method('getCriteresAttribution')
                ->willReturn([$critereAttribution1, $critereAttribution2]);
            $donneesComplementaires
                ->method('getIdDureeDelaiDescription')
                ->willReturn(5858585);
            $donneesComplementaires
                ->method('getIdChoixMoisJour')
                ->willReturn(9696969);
            $donneesComplementaires
                ->method('getDureeMarche')
                ->willReturn(5);
            $donneesComplementaires
                ->method('getDelaiValiditeOffres')
                ->willReturn(7);
            $donneesComplementaires
                ->method('getIdGroupementAttributaire')
                ->willReturn(484848);

            $em->method('getRepository')
                ->will(
                    $this->returnValueMap(
                        [
                            [Consultation::class, $this->getConsultationRepository($donneesComplementaires, $allotie)],
                            [Lot::class, $this->getConsultationLotRepository($donneesComplementaires)],
                            [ConsultationComptePub::class, $this->getComptePubRepository()],
                            [Agent::class, $this->getAgentRepository()],
                            [Tranche::class, $this->getTrancheRepository()],
                            [GeolocalisationN2::class, $this->getGeolocalisationN2Repository()],
                        ]
                    )
                );

            $donneeComplService = $this->getDonneeComplementaireService();

            $this->echangeConcentrateur = new EchangeConcentrateur(
                $parameterBag,
                $logger,
                $em,
                $clientWS,
                $donneeComplService,
                $twig,
                $contratService,
                $this->createMock(OrganismeService::class),
                $this->createMock(JWTTokenManagerInterface::class),
                $this->createMock(RefreshTokenGeneratorInterface::class),
                $this->createMock(WebservicesConcentrateur::class),
            );
        }

        return $this->echangeConcentrateur;
    }

    protected function getDonneeComplementaireService(): DonneeComplementaireService
    {
        $em = $this->createMock(EntityManagerInterface::class);

        $em->method('getRepository')
            ->will(
                $this->returnValueMap(
                    [
                        [ConsultationComptePub::class, $this->getComptePubRepository()],
                        [ValeurReferentiel::class, $this->getValeurReferentielRepository()],
                        [GeolocalisationN2::class, $this->getGeolocalisationN2Repository()],
                    ]
                )
            );

        $util = $this->createMock(AtexoUtil::class);
        $util->method('getPfUrlDomaine')
            ->willReturn('https://docker.local-trust.com');

        $container = $this->createMock(ContainerInterface::class);
        $container->method('get')
            ->with('atexo.atexo_util')
            ->willReturn($util);
        $container->method('getParameter')
            ->will(
                $this->returnValueMap(
                    [
                        ['REFERENTIEL_FORME_GROUPEMENT', 'REFERENTIEL_FORME_GROUPEMENT'],
                        ['REFERENTIEL_CRITERE_ATTRIBUTION', 'REFERENTIEL_CRITERE_ATTRIBUTION'],
                        ['REFERENTIEL_DUREE_DELAI', 'REFERENTIEL_DUREE_DELAI'],
                        ['REFERENTIEL_UNITE_DUREE_MARCHE', 'REFERENTIEL_UNITE_DUREE_MARCHE'],
                    ]
                )
            );

        $configService = $this->createMock(AtexoConfiguration::class);
        $translator = $this->createMock(TranslatorInterface::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $utils = new Utils();

        return new DonneeComplementaireService($container, $em, $configService, $translator, $parameterBagMock, $utils);
    }

    /**
     * @throws Exception
     */
    protected function getGeneratedXml(bool $allotie = false): string
    {
        $outputPath = self::XML_OUTPUT_PATH;
        $outputDir = dirname($outputPath);
        if (!is_dir($outputDir)) {
            mkdir($outputDir, 0777, true);
        }

        $echangeConcentrateur = $this->getEchangeConcentrateur($allotie);

        $xml = $echangeConcentrateur->generateXmlAnnonce(123);

        $xmlDocument = new DOMDocument('1.0');
        $xmlDocument->preserveWhiteSpace = false;
        $xmlDocument->formatOutput = true;
        $xmlDocument->loadXML($xml);

        file_put_contents($outputPath, $xmlDocument->saveXML());

        return $xml;
    }

    protected function getComptePubRepository(): EntityRepository
    {
        $comptePub = $this->getComptePub();

        $comptePubRepository = $this->createMock(EntityRepository::class);
        $comptePubRepository
            ->method('findOneBy')
            ->willReturn($comptePub);

        return $comptePubRepository;
    }

    protected function getAgentRepository(): EntityRepository
    {
        $agent = new Agent();
        $agent->setNumTel('01 00 04 04 04');
        $agent->setEmail('agent@organisme.fr');

        $agentRepository = $this->createMock(EntityRepository::class);
        $agentRepository
            ->method('find')
            ->willReturn($agent);

        return $agentRepository;
    }

    protected function getTrancheRepository(): EntityRepository
    {
        $trancheRepository = $this->createMock(EntityRepository::class);
        $trancheRepository
            ->method('findBy')
            ->willReturn([]);

        return $trancheRepository;
    }

    protected function getGeolocalisationN2Repository(): GeolocalisationN2Repository
    {
        $entityRepository = $this->createMock(GeolocalisationN2Repository::class);
        $entityRepository
            ->method('getCodeNutsLieuxExecution')
            ->willReturn(['BE221', 'CH056']);
        $entityRepository
            ->method('getLieuxExecution')
            ->willReturn(
                [
                    [
                        "id" => 1,
                        "denomination1" => "(75) Paris",
                        "denomination2" => "75"
                    ],
                    [
                        "id" => 2,
                        "denomination1" => "(93) Seine-Saint-Denis",
                        "denomination2" => "93"
                    ]
                ]
            );

        return $entityRepository;
    }

    protected function getValeurReferentielRepository(): EntityRepository
    {
        $valeurRef1 = new ValeurReferentiel();
        $valeurRef1->setLibelleValeurReferentiel('LIBELLE_VALEUR_REFERENTIEL')
            ->setLibelle2('layerDefinitionCriteres_2');

        $valeurRef2 = new ValeurReferentiel();
        $valeurRef2->setLibelle2('DUREE_MARCHEE');

        $valeurRef3 = new ValeurReferentiel();
        $valeurRef3->setLibelle2('dureeMois');

        $valeurRef4 = new ValeurReferentiel();
        $valeurRef4->setLibelleValeurReferentiel('Forme requise en cas de groupement : Conjoint');

        $valeurRefRepository = $this->createMock(EntityRepository::class);
        $valeurRefRepository
            ->method('findOneBy')
            ->will(
                $this->returnValueMap(
                    [
                        [['idReferentiel' => 'REFERENTIEL_CRITERE_ATTRIBUTION', 'id' => 1313131], null, $valeurRef1],
                        [['idReferentiel' => 'REFERENTIEL_DUREE_DELAI', 'id' => 5858585], null, $valeurRef2],
                        [['idReferentiel' => 'REFERENTIEL_UNITE_DUREE_MARCHE', 'id' => 9696969], null, $valeurRef3],
                        [['idReferentiel' => 'REFERENTIEL_FORME_GROUPEMENT', 'id' => 484848], null, $valeurRef4],
                    ]
                )
            );

        return $valeurRefRepository;
    }

    protected function getConsultationRepository(
        DonneeComplementaire $donneesComplementaires,
        bool $allotie = false
    ): ConsultationRepository {
        $organisme = $this->getOrganisme();

        $consultation = $this->getConsultation($donneesComplementaires, $organisme, $allotie);

        $consultationRepository = $this->createMock(ConsultationRepository::class);
        $consultationRepository
            ->method('find')
            ->with($consultation->getId())
            ->willReturn($consultation);

        return $consultationRepository;
    }

    protected function getConsultationLotRepository(
        DonneeComplementaire $donneesComplementaires
    ): LotRepository {
        $lot1 = new Lot();
        $lot1->setDescription('Lot 1 : étude');
        $lot1->setCodeCpv1('03142000');
        $lot1->setDonneeComplementaire($donneesComplementaires);
        $lot1->setClauseSocialeSiae('1');

        $lot2 = new Lot();
        $lot2->setDescription('Lot 2 : réalisation');
        $lot2->setCodeCpv1('03222000');
        $lot2->setDonneeComplementaire($donneesComplementaires);

        $repository = $this->createMock(LotRepository::class);
        $repository
            ->method('findLotsConsultation')
            ->willReturn([$lot1, $lot2]);

        return $repository;
    }

    public function getConsultationUidDataProvider(): array
    {
        return [
            [123456, '2019-10-04', '2019-123456'],
            [321654, '2020-05-04', '2020-321654'],
            [1234, null, '2000-001234'],
        ];
    }

    /**
     * @dataProvider getConsultationUidDataProvider
     *
     * @param $consultationId
     * @param $datetime
     * @param $expected
     *
     * @throws Exception
     */
    public function testGetConsultationUid($consultationId, $datetime, $expected)
    {
        $consultation = new Consultation();
        $consultation->setId($consultationId);
        if (!is_null($datetime)) {
            $consultation->setDatedebut(new DateTime($datetime));
        }

        $echangeConcentrateur = $this->getEchangeConcentrateur();
        $result = $echangeConcentrateur->getConsultationUid($consultation);

        $this->assertEquals($expected, $result);
    }

    public function formatTelephoneDataProvider(): array
    {
        return [
            ['0123456789', '+33 123456789'],
            ['01 23 45 67 89', '+33 123456789'],
            ['01.23.45.67.89', '+33 123456789'],
            ['01-23-45-67-89', '+33 123456789'],
            ['01/23/45/67/89', '+33 123456789'],
            ['+33 123456789', '+33 123456789'],
            ['+33123456789', '+33 123456789'],
            ['0123456789', '+33 123456789'],
            ['+32 123456789', '+32 123456789'],
            ['+32 123 456 789', '+32 123456789'],
            [null, null],
            ['', null],
            ['+53 012abc', null],
            [str_repeat('1230', 30), null],
        ];
    }

    /**
     * @dataProvider formatTelephoneDataProvider
     *
     * @param $telephone
     * @param $expected
     */
    public function testFormatTelephone($telephone, $expected)
    {
        $utils = new Utils();
        $result = $utils->formatTelephone($telephone);

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider generateValidXmlAnnonceDataProvider
     *
     * @throws Exception
     */
    public function testValidateAgainstXSD(string $testTitle, bool $allotie)
    {
        libxml_use_internal_errors(true);

        $xml = $this->getGeneratedXml($allotie);
        $xsdPath = realpath(__DIR__ . '/../../../../legacy/ressources/publicite/joue/TED_ESENDERS.xsd');
        // Pro tip : pour vérifier si un fichier XML est valide selon une XSD,
        // il est possible d'utiliser la commande linux suivante :
        // $ xmllint --noout --schema \
        //     mpe_sf/legacy/ressources/publicite/joue/TED_ESENDERS.xsd \
        //     mpe_sf/var/test/EchangeConcentrateurTest.xml
        // L'autre avantage de cette commande est qu'elle retourne le détail des erreurs éventuelles de la XSD.

        $domDoc = new DOMDocument();
        $domDoc->loadXML($xml);

        $isValid = $domDoc->schemaValidate((string) $xsdPath);
        $errorsList = libxml_get_errors();
        libxml_clear_errors();
        $formattedErrors = '';

        foreach ($errorsList as $errorMsg) {
            $formattedErrors .= PHP_EOL . ' * ' . $errorMsg->message;
        }

        $this->assertTrue(
            $isValid,
            'Validation XSD en échec : ' . $testTitle
            . PHP_EOL . 'Erreurs : ' . PHP_EOL . $formattedErrors
            . PHP_EOL . 'Le fichier XML généré se trouve là : '
            . realpath(self::XML_OUTPUT_PATH)
        );
    }

    public function testGetDataEnvoi(): void
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->exactly(2))->method('get')->willReturnOnConsecutiveCalls('uid_mpe', 'url_ref');

        $organismeServiceMock = $this->createMock(OrganismeService::class);
        $organismeServiceMock->expects($this->once())
            ->method('getNomCourantAcheteurPublic')->willReturn('mynomacheteurpublic');

        $agentTechnique = new Agent();

        $tokenManagerMock = $this->createMock(JWTTokenManagerInterface::class);
        $tokenManagerMock->expects($this->once())->method('create')->with($agentTechnique)->willReturn('mytoken');

        $refreshToken = new RefreshToken();
        $refreshToken->setRefreshToken('myrefreshtoken');
        $refreshTokenGeneratorMock = $this->createMock(RefreshTokenGeneratorInterface::class);
        $refreshTokenGeneratorMock->expects($this->once())->method('createForUserWithTtl')
            ->with($agentTechnique, EchangeConcentrateur::AGENT_TECHNIQUE_TTL_REFRESH_TOKEN)->willReturn($refreshToken);

        $agentRepoMock = $this->createMock(AgentRepository::class);
        $agentRepoMock->expects($this->once())->method('findAgentTechniqueByLogin')
            ->with(EchangeConcentrateur::AGENT_TECHNIQUE_LOGIN)->willReturn($agentTechnique);
        $emMock = $this->createMock(EntityManagerInterface::class);
        $emMock->expects($this->once())->method('getRepository')->willReturn($agentRepoMock);


        $echangeConcentrateur = new EchangeConcentrateur(
            $parameterBag,
            $this->createMock(LoggerInterface::class),
            $emMock,
            $this->createMock(ClientWs::class),
            $this->createMock(DonneeComplementaireService::class),
            $this->createMock(Environment::class),
            $this->createMock(ContratService::class),
            $organismeServiceMock,
            $tokenManagerMock,
            $refreshTokenGeneratorMock,
            $this->createMock(WebservicesConcentrateur::class),
        );

        $organisme = new class () extends Organisme {
            public function getId(): int
            {
                return 23;
            }
        };
        $organisme->setAcronyme('myacro');
        $organisme->setDenominationOrg('mydenomination');
        $organisme->setSiren('mysiren');
        $organisme->setComplement('mycomplement');

        $service = new class () extends Service {
            public function getId(): int
            {
                return 24;
            }
        };
        $service->setLibelle('mylabel');
        $service->setSiren('mysiren');
        $service->setComplement('mycomplement');

        $agent = new class () extends Agent {
            public function getId(): int
            {
                return 22;
            }
        };
        $agent->setLogin('mylogin');
        $agent->setNom('myname');
        $agent->setPrenom('myprenom');
        $agent->setAcronymeOrganisme('myacro');
        $agent->setEmail('myemail');
        $agent->setTelephone('0102');
        $agent->setNumFax('0302');
        $agent->setOrganisme($organisme);
        $agent->setService($service);

        $data = $echangeConcentrateur->getDataEnvoi($agent);

        $expectedData = [
            'envoi' => [
                'agent' => [
                    'id'                        => 22,
                    'identifiant'               => 'mylogin',
                    'nom'                       => 'myname',
                    'prenom'                    => 'myprenom',
                    'plateforme'                => 'uid_mpe',
                    'acronymeOrganisme'         => 'myacro',
                    'email'                     => 'myemail',
                    'telephone'                 => '0102',
                    'fax'                       => '0302',
                    'sigleUrl'                  => '',
                    'nomCourantAcheteurPublic'  => 'mynomacheteurpublic',
                    'api'                       => [
                        'url'                       => 'url_ref',
                        'token'                     => 'mytoken',
                        'refreshToken'              => 'myrefreshtoken'
                    ],
                    'organisme'                 => [
                        'id'                        => 23,
                        'acronyme'                  => 'myacro',
                        'denominationOrganisme'     => 'mydenomination',
                        'siren'                     => 'mysiren',
                        'complement'                => 'mycomplement'
                    ],
                    'service'                 => [
                        'id'                        => 24,
                        'libelle'                   => 'mylabel',
                        'siren'                     => 'mysiren',
                        'complement'                => 'mycomplement'
                    ]
                ]
            ]
        ];

        $this->assertEquals($data, $expectedData);
    }
}
