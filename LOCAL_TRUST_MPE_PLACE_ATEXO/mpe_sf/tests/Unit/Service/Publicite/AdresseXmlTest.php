<?php

namespace Tests\Unit\Service\Publicite;

class AdresseXmlTest extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'adresse.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $param1['adresse'] = [
            'nomOrganisme' => 'OFFICIALNAME',
            'nom' => 'CONTACT_POINT',
            'adresse' => 'adresse',
            'ville' => 'ville',
            'cp' => '75008',
            'url' => 'url',
            'urlAcheteur' => 'urlAcheteur',
        ];
        $res1 = <<<XML
<OFFICIALNAME>OFFICIALNAME</OFFICIALNAME>
<ADDRESS>adresse</ADDRESS>
<TOWN>ville</TOWN>
<POSTAL_CODE>75008</POSTAL_CODE>
<COUNTRY VALUE="FR"/>
<CONTACT_POINT>CONTACT_POINT</CONTACT_POINT>
<URL>url</URL>
<URL_BUYER>urlAcheteur</URL_BUYER>
XML;
        $param2['adresse'] = [
            'adresse' => 'adresse',
            'ville' => 'ville',
            'cp' => '75008',
            'url' => 'url',
        ];
        $res2 = <<<XML
<ADDRESS>adresse</ADDRESS>
<TOWN>ville</TOWN>
<POSTAL_CODE>75008</POSTAL_CODE>
<COUNTRY VALUE="FR"/>
<URL>url</URL>
XML;

        $param3['adresse'] = [
            'adresse' => 'adresse',
            'ville' => 'ville',
            'cp' => '75008',
            'telephone' => '+33 123456789',
            'email' => 'email@organisme.fr',
            'url' => 'url',
        ];
        $res3 = <<<XML
<ADDRESS>adresse</ADDRESS>
<TOWN>ville</TOWN>
<POSTAL_CODE>75008</POSTAL_CODE>
<COUNTRY VALUE="FR"/>
<PHONE>+33123456789</PHONE>
<E_MAIL>email@organisme.fr</E_MAIL>
<URL>url</URL>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
            [$param3, $res3],
        ];
    }
}
