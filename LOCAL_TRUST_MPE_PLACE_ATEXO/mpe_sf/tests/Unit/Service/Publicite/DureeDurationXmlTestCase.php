<?php

namespace Tests\Unit\Service\Publicite;

use App\Entity\Consultation\DonneeComplementaire;
use App\Service\DonneeComplementaireService;

class DureeDurationXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'duree_duration.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $duree = ['descriptionLibre' => 'description libre'];
        $param1 = $this->getParams($duree);
        $res1 = <<<XML
<DURATIONTYPE="MONTH">1</DURATION>
XML;
        $duree = ['dureeValue' => '12', 'dureeType' => 'MONTH'];
        $param2 = $this->getParams($duree);
        $res2 = <<<XML
<DURATION TYPE="MONTH">12</DURATION>
XML;
        $duree = ['dureeValue' => '20', 'dureeType' => 'DAY'];
        $param3 = $this->getParams($duree);
        $res3 = <<<XML
<DURATION TYPE="DAY">20</DURATION>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
            [$param3, $res3],
        ];
    }

    public function getParams($duree)
    {
        $donneeCompl = $this->createMock(DonneeComplementaire::class);
        $donneeComplService = $this->createMock(DonneeComplementaireService::class);
        $donneeComplService->expects($this->any())
            ->method('getDureeMarche')
            ->willReturn($duree);

        return [
            'donneeComplService' => $donneeComplService,
            'donnesCompl' => $donneeCompl,
        ];
    }
}
