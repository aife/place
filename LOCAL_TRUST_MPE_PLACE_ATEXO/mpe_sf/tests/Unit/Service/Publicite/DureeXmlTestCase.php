<?php

namespace Tests\Unit\Service\Publicite;

use App\Entity\Consultation\DonneeComplementaire;
use App\Service\DonneeComplementaireService;

class DureeXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'duree.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $duree = ['descriptionLibre' => 'description libre'];
        $param1 = $this->getParams($duree);
        $res1 = <<<XML
<OPTIONS/><OPTIONS_DESCR>
    <P>description libre</P>
</OPTIONS_DESCR>
XML;
        $duree = ['dureeValue' => '12', 'dureeType' => 'MONTH'];
        $param2 = $this->getParams($duree);
        $res2 = <<<XML
<NO_OPTIONS/>
XML;
        $duree = ['descriptionLibre' => 'description libre'];
        $param3 = $this->getParams($duree);
        $res3 = <<<XML
<OPTIONS/><OPTIONS_DESCR><P>descriptionlibre</P></OPTIONS_DESCR>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
            [$param3, $res3],
        ];
    }

    public function getParams($duree)
    {
        $donneeCompl = $this->createMock(DonneeComplementaire::class);
        $donneeComplService = $this->createMock(DonneeComplementaireService::class);
        $donneeComplService->expects($this->any())
            ->method('getDureeMarche')
            ->willReturn($duree);

        return [
            'donneeComplService' => $donneeComplService,
            'donnesCompl' => $donneeCompl,
        ];
    }
}
