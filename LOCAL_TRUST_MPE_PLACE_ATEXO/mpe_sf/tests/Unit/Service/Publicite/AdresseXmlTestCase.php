<?php

namespace Tests\Unit\Service\Publicite;

class AdresseXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'adresse.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $param1['adresse'] = [
            'nomOrganisme' => 'OFFICIALNAME',
            'nom' => 'CONTACT_POINT',
            'adresse' => 'adresse',
            'ville' => 'ville',
            'cp' => '75008',
            'url' => 'url',
            'urlAcheteur' => 'urlAcheteur',
        ];
        $res1 = <<<XML
<OFFICIALNAME>OFFICIALNAME</OFFICIALNAME>
<CONTACT_POINT>CONTACT_POINT</CONTACT_POINT>
<ADDRESS>adresse</ADDRESS>
<TOWN>ville</TOWN>
<POSTAL_CODE>75008</POSTAL_CODE>
<COUNTRY VALUE="FR"/>
<URL_GENERAL>url</URL_GENERAL>
<URL_BUYER>urlAcheteur</URL_BUYER>
XML;
        $param2['adresse'] = [
            'adresse' => 'adresse',
            'ville' => 'ville',
            'cp' => '75008',
            'url' => 'url',
        ];
        $res2 = <<<XML
<ADDRESS>adresse</ADDRESS>
<TOWN>ville</TOWN>
<POSTAL_CODE>75008</POSTAL_CODE>
<COUNTRY VALUE="FR"/>
<URL_GENERAL>url</URL_GENERAL>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
        ];
    }
}
