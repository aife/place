<?php

namespace Tests\Unit\Service\Publicite;

use App\Service\AtexoConfiguration;
use App\Service\DonneeComplementaireService;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;
use Tests\Unit\TwigTemplateTrait;

abstract class AbstractXmlTestCase extends AtexoTestCase
{
    use TwigTemplateTrait;

    private $container;
    private $em;
    private $confService;
    private $trans;
    protected $templateTwig;

    public function setUp(): void
    {
        $this->container = $this->createMock(ContainerInterface::class);
        $this->em = $this->createMock(EntityManager::class);
        $this->confService = $this->createMock(AtexoConfiguration::class);
        $this->trans = $this->createMock(TranslatorInterface::class);
    }

    protected function doTest($template, $params, $res)
    {
        $twig = $this->getTemplate(dirname(__FILE__).
            '/../../../../templates/publicite');
        $xml = str_replace(["\n", "\r", ' '], ['', '', ''], $twig->render($template, $params));
        $res = str_replace(["\n", "\r", ' '], ['', '', ''], $res);
        $this->assertEquals($res, $xml);
    }

    protected function getDonneeComplementaireService()
    {
        return new DonneeComplementaireService(
            $this->container,
            $this->em,
            $this->confService,
            $this->trans
        );
    }

    /**
     * @dataProvider getDataProvider
     *
     * @param $params
     * @param $res
     */
    public function testGenerationXml($params, $res)
    {
        if ($this->templateTwig) {
            $this->doTest($this->templateTwig, $params, $res);
        }
    }

    public function getDataProvider()
    {
        return [];
    }
}
