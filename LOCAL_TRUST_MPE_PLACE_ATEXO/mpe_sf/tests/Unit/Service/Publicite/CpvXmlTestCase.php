<?php

namespace Tests\Unit\Service\Publicite;

use App\Entity\Consultation;
use App\Entity\Lot;

class CpvXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'cpv.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $c1 = $this->createMock(Consultation::class);
        $c2 = $this->createMock(Lot::class);
        $c3 = $this->createMock(Lot::class);

        $c1->expects($this->any())
            ->method('getCodeCpv1')
            ->willReturn('70000000');
        $c1->expects($this->any())
            ->method('getArrayCpvSecondaire')
            ->willReturn(['70123000', '70123001', '70123002']);
        $res1 = <<<XML
<CPV_CODE CODE="70000000"/>
<CPV_SUPPLEMENTARY_CODE CODE="70123000"/>
<CPV_SUPPLEMENTARY_CODE CODE="70123001"/>
<CPV_SUPPLEMENTARY_CODE CODE="70123002"/>
XML;

        $c2->expects($this->any())
            ->method('getCodeCpv1')
            ->willReturn('70000000');
        $c2->expects($this->any())
            ->method('getArrayCpvSecondaire')
            ->willReturn([]);
        $res2 = <<<XML
<CPV_CODE CODE="70000000"/>
XML;
        $c3->expects($this->any())
            ->method('getCodeCpv1')
            ->willReturn('');
        $c3->expects($this->any())
            ->method('getArrayCpvSecondaire')
            ->willReturn(['70123000', '70123001', '70123002']);
        $res3 = '';

        return [
            [['data' => $c1], $res1],
            [['data' => $c2], $res2],
            [['data' => $c3], $res3],
        ];
    }
}
