<?php

namespace Tests\Unit\Service\Publicite;

use App\Entity\Consultation\DonneeComplementaire;
use App\Service\DonneeComplementaireService;

class CriteresXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'critere.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $param1 = $this->getCritereProcurementDoc();
        $res1 = <<<XML
<AC>
<AC_PROCUREMENT_DOC/>
</AC>
XML;

        $param2 = $this->getCriterePrix();
        $res2 = <<<XML
<AC>
<AC_PRICE/>
</AC>
XML;
        $param3 = $this->getCritereCout();
        $res3 = <<<XML
<AC>
<AC_PRICE>
    <AC_WEIGHTING>100%</AC_WEIGHTING>
    <AC_CRITERION>coût</AC_CRITERION>
</AC_PRICE>
</AC>
XML;
        $param4 = $this->getCritereSansPonderation();
        $res4 = <<<XML
<AC>
<AC_QUALITY>
    <AC_CRITERION>C1 critère 1</AC_CRITERION>
    <AC_WEIGHTING>100</AC_WEIGHTING>
</AC_QUALITY>
<AC_QUALITY>
    <AC_CRITERION>C2 critère 2</AC_CRITERION>
    <AC_WEIGHTING>100</AC_WEIGHTING>
</AC_QUALITY>
<AC_QUALITY>
    <AC_CRITERION>C2.1 critère 2.1</AC_CRITERION>
    <AC_WEIGHTING>100</AC_WEIGHTING>
</AC_QUALITY>
<AC_QUALITY>
    <AC_CRITERION>C3 critère 3</AC_CRITERION>
    <AC_WEIGHTING>100</AC_WEIGHTING>
</AC_QUALITY>
<AC_PRICE/>
</AC>
XML;

        $param5 = $this->getCritereAvecPonderation();
        $res5 = <<<XML
<AC>
<AC_COST>
    <AC_CRITERION>C1 critère 1</AC_CRITERION>
    <AC_WEIGHTING>90%</AC_WEIGHTING>
</AC_COST>
<AC_COST>
    <AC_CRITERION>C1.1 critère 1.1</AC_CRITERION>
    <AC_WEIGHTING>40%</AC_WEIGHTING>
</AC_COST>
<AC_COST>
    <AC_CRITERION>C1.2 critère 1.2</AC_CRITERION>
    <AC_WEIGHTING>50%</AC_WEIGHTING>
</AC_COST>
<AC_COST>
    <AC_CRITERION>C2 critère 2</AC_CRITERION>
    <AC_WEIGHTING>10%</AC_WEIGHTING>
</AC_COST>
</AC>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
            [$param3, $res3],
            [$param4, $res4],
            [$param5, $res5],
        ];
    }

    public function getCritereProcurementDoc()
    {
        $criteres['procurementDoc'] = true;

        return $this->getParams($criteres);
    }

    public function getCriterePrix()
    {
        $criteres['prix'] = true;

        return $this->getParams($criteres);
    }

    public function getCritereCout()
    {
        $criteres['cout'] = true;

        return $this->getParams($criteres);
    }

    public function getCritereSansPonderation()
    {
        $criteres['sansPonderation'] = [
            ['name' => 'C1 critère 1'],
            ['name' => 'C2 critère 2'],
            ['name' => 'C2.1 critère 2.1'],
            ['name' => 'C3 critère 3'],
        ];

        return $this->getParams($criteres);
    }

    public function getCritereAvecPonderation()
    {
        $criteres['avecPonderation'] = [
            ['name' => 'C1 critère 1', 'poid' => '90%'],
            ['name' => 'C1.1 critère 1.1', 'poid' => '40%'],
            ['name' => 'C1.2 critère 1.2', 'poid' => '50%'],
            ['name' => 'C2 critère 2', 'poid' => '10%'],
        ];

        return $this->getParams($criteres);
    }

    public function getParams($criteres)
    {
        $donneeCompl = $this->createMock(DonneeComplementaire::class);
        $donneeComplCons = $this->createMock(DonneeComplementaire::class);
        $donneeComplService = $this->createMock(DonneeComplementaireService::class);
        $donneeComplService->expects($this->any())
            ->method('getCriteresAttribution')
            ->willReturn($criteres);

        return [
            'donneeComplService' => $donneeComplService,
            'donnesCompl' => $donneeCompl,
            'donneeComplCons' => $donneeComplCons,
        ];
    }
}
