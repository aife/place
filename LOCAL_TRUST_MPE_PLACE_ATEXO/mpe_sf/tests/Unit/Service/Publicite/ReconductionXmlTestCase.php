<?php

namespace Tests\Unit\Service\Publicite;

use App\Entity\Consultation\DonneeComplementaire;

class ReconductionXmlTestCase extends AbstractXmlTestCase
{
    public function setUp(): void
    {
        $this->templateTwig = 'reconduction.xml.twig';
        parent::setUp();
    }

    public function getDataProvider()
    {
        $param1 = $this->getParams('0', 0, '');
        $res1 = <<<XML
<NO_RENEWAL/>
XML;
        $param2 = $this->getParams('1', 0, '');
        $res2 = <<<XML
<RENEWAL/>
<RENEWAL_DESCR></RENEWAL_DESCR>
XML;
        $param3 = $this->getParams('1', 2, '');
        $res3 = <<<XML
<RENEWAL/>
<RENEWAL_DESCR>
<P>DEFINE_MARCHE_RECONDUCTIBLE:2TEXT_FOIS.</P>
</RENEWAL_DESCR>
XML;
        $param4 = $this->getParams('1', 2, 'Modalites');
        $res4 = <<<XML
<RENEWAL/>
<RENEWAL_DESCR>
<P>DEFINE_MARCHE_RECONDUCTIBLE:2TEXT_FOIS.</P>
<P>DEFINE_MODALITE_RECONDUCTIONS:Modalites.</P>
</RENEWAL_DESCR>
XML;

        return [
            [$param1, $res1],
            [$param2, $res2],
            [$param3, $res3],
            [$param4, $res4],
        ];
    }

    public function getParams($reconduction, $nbre = 0, $modalites = null)
    {
        $donneeCompl = $this->createMock(DonneeComplementaire::class);
        $donneeCompl->expects($this->any())
            ->method('getReconductible')
            ->willReturn($reconduction);
        $donneeCompl->expects($this->any())
            ->method('getNombreReconductions')
            ->willReturn($nbre);
        $donneeCompl->expects($this->any())
            ->method('getModalitesReconduction')
            ->willReturn($modalites);

        return [
            'donnesCompl' => $donneeCompl,
        ];
    }
}
