<?php

namespace Tests\Unit\Service;

use App\Service\AtexoEchangeChorus;

use App\Service\Chorus\FicheModificative;
use App\Service\ContratService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EchangeChorusServiceTest extends TestCase
{
    /**
     * @dataProvider getSiretAttributaireProvider
     */
    public function testSiretAttributaire(?string $entreprise, ?string $etablissement, ?string $expected): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $contratService = $this->createMock(ContratService::class);
        $ficheModificativeService = $this->createMock(FicheModificative::class);
        $chorusEchangeService = new AtexoEchangeChorus($container, $em, $parameterBag, $contratService, $ficheModificativeService);

        self::assertSame(
            $chorusEchangeService->getSiretAttributaire($entreprise, $etablissement),
            $expected
        );
    }

    public function getSiretAttributaireProvider(): array
    {
        return [
            'Entreprise and Etablissement are null' => [null, null, null],
            'Entreprise is null' => [null, '13245', '13245'],
            'Etablissement is null' => ['123456789', null, '123456789'],
            'Etablissement code with char' => [null, '13245AB', null],
            'Etablissement code only char' => ['123456789', 'aaaa', '123456789'],
            'Siret attributaire length too long' => ['123456789', '123456789', null],
            'Siret ok' => ['123456789', '12345', '12345678912345']
        ];
    }

    /**
     * @dataProvider getCodeApe
     */
    public function testCodeApe(?string $codeape, ?string $expected): void
    {
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManagerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $contratService = $this->createMock(ContratService::class);
        $ficheModificativeService = $this->createMock(FicheModificative::class);
        $chorusEchangeService = new AtexoEchangeChorus($container, $em, $parameterBag, $contratService, $ficheModificativeService);

        self::assertSame(
            $chorusEchangeService->getCodeApe($codeape),
            $expected
        );
    }

    public function getCodeApe(): array
    {
        return [
            'codeApe is null' => [null,  null],
            'codeApe is empty' => ['', null],
            'codeApe length too long' => ['123456789', null],
            'codeApe ok' => ['132AB', '132AB']
        ];
    }
}
