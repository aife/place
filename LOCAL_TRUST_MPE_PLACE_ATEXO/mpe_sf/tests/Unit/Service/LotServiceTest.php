<?php

namespace Tests\Unit\Service;

use App\Service\ClausesMigrationService;
use App\Service\ClausesService;
use stdClass;
use App\Dto\Input\LotInput;
use App\Service\LotService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class LotServiceTest extends TestCase
{
    private $session;

    private $em;

    protected function setUp(): void
    {
        $this->session = new Session(new MockArraySessionStorage());
        $this->em = $this->createMock(EntityManagerInterface::class);
    }

    public function testLotsCanImport()
    {
        $this->session->set('contexte_authentification', ['consultationsAutorisees' => [1521]]);
        $clausesMigrationService = $this->createMock(ClausesMigrationService::class);

        $lotService = new LotService($this->em, $this->session, $clausesMigrationService, $this->createMock(ClausesService::class));
        $this->assertTrue($lotService->canImport(1521));
    }

    public function testLotsCanNotImportFullPhp7()
    {
        $this->session->set('contexte_authentification', ['consultationsAutorisees' => [1522]]);
        $clausesMigrationService = $this->createMock(ClausesMigrationService::class);

        $lotService = new LotService($this->em, $this->session, $clausesMigrationService, $this->createMock(ClausesService::class));
        $this->assertFalse($lotService->canImport(1521));
    }

    public function testLotsCanImportEmptyFullPhp7()
    {
        $this->session->set('contexte_authentification', ['consultationsAutorisees' => [1521]]);
        $clausesMigrationService = $this->createMock(ClausesMigrationService::class);

        $lotService = new LotService($this->em, $this->session, $clausesMigrationService, $this->createMock(ClausesService::class));
        $this->assertTrue($lotService->canImport(1521));
    }

    /**
     * @dataProvider codeCpv
     */
    public function testGetExplodedCodeCpv(?string $codeCpv, string $separator, array $expectedResult): void
    {
        $session = $this->createMock(SessionInterface::class);
        $clausesMigrationService = $this->createMock(ClausesMigrationService::class);

        $lotService = new LotService($this->em, $session, $clausesMigrationService, $this->createMock(ClausesService::class));

        self::assertSame($lotService->getExplodedCodeCpv($codeCpv, $separator), $expectedResult);
    }

    public function codeCpv(): array
    {
        return [
            ['#90910000#90911000#90911200#', '#', ['90910000', '90911000', '90911200']],
            ['#123456#789123##', '#', ['123456', '789123']],
            ['#50000000#79520000#', '#', ['50000000', '79520000']],
            ['#48000000#', '#', ['48000000']],
            ['##', '#', []],
            ['', '#', []],
            [null, '#', []],
        ];
    }

    /**
     * @dataProvider lotInput
     */
    public function testGetConcatenatedCodeCpv(object $lotInput, ?string $expectedResult): void
    {
        $session = $this->createMock(SessionInterface::class);
        $clausesMigrationService = $this->createMock(ClausesMigrationService::class);

        $lotService = new LotService($this->em, $session, $clausesMigrationService, $this->createMock(ClausesService::class));

        self::assertSame($lotService->getConcatenatedCodeCpv($lotInput), $expectedResult);
    }

    public function lotInput(): array
    {
        $object1 = new stdClass();
        $object1->codeCpvSecondaire1 = '123456';
        $object1->codeCpvSecondaire2 = null;
        $object1->codeCpvSecondaire3 = null;

        $object2 = new stdClass();
        $object2->codeCpvSecondaire1 = '123456';
        $object2->codeCpvSecondaire2 = '789123';
        $object2->codeCpvSecondaire3 = null;

        $object3 = new stdClass();
        $object3->codeCpvSecondaire1 = '123456';
        $object3->codeCpvSecondaire2 = '789123';
        $object3->codeCpvSecondaire3 = '987654';

        $object4 = new stdClass();
        $object4->codeCpvSecondaire1 = '';
        $object4->codeCpvSecondaire2 = '';
        $object4->codeCpvSecondaire3 = '987654';

        $object5 = new stdClass();
        $object5->codeCpvSecondaire1 = '';
        $object5->codeCpvSecondaire2 = '';
        $object5->codeCpvSecondaire3 = '';

        return [
            [new LotInput(), null],
            [$object1, '#123456#'],
            [$object2, '#123456#789123#'],
            [$object3, '#123456#789123#987654#'],
            [$object4, '#987654#'],
            [$object5, null],
        ];
    }
}
