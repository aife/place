<?php

namespace Tests\Unit\Service;

use ReflectionClass;
use DateTime;
use ReflectionException;
use App\Entity\BloborganismeFile;
use App\Repository\BloborganismeFileRepository;
use App\Service\AtexoFichierOrganisme;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use App\Utils\Filesystem\MountManager;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

class AtexoFichierOrganismeTest extends TestCase
{
    private ContainerInterface|MockObject $container;
    private string $organisme;
    private EntityManager|MockObject $em;
    private MountManager $mountManager;
    private $parameterBag;

    protected function setUp(): void
    {
        parent::setUp();
        $this->organisme = 'pmi-min-1';
        $this->container = $this->createMock(ContainerInterface::class);
        $this->em = $this->createMock(EntityManager::class);

        $organismeBlobMock = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobMock);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobMock, 1);

        $registy = $this->createMock(Registry::class);
        $mock = $this->createMock(EntityManager::class);
        $this->parameterBag = $this->createMock(ParameterBagInterface::class);
        $this->parameterBag->expects($this->any())
            ->method('get')
            ->willReturn('a parameter');

        $registy->expects($this->any())
            ->method('getManager')
            ->willReturn($mock);

        $this->container->expects($this->any())
            ->method('get')
            ->willReturn($registy);

        $fileSystem = $this->createMock(Filesystem::class);

        $pathFile = 'tmpGenere.txt';
        $fileSystem->write($pathFile, 'fichier de test');
        $fileSystemTmp = new Filesystem(new MemoryAdapter());
        $fileSystemTmp->write($pathFile, 'fichier de test');

        $fileSystem->expects($this->any())
            ->method('has')
            ->with($this->anything())
            ->willReturn(true);

        $fileSystem->expects($this->any())
            ->method('getSize')
            ->with($this->anything())
            ->willReturn(15);

        $fileSystem->expects($this->any())
            ->method('copy')
            ->with($this->anything())
            ->willReturn(true);

        $fileSystem->expects($this->any())
            ->method('sha1File')
            ->with($this->anything())
            ->willReturn('azertyuiop');

        $fileSystem->expects($this->any())
            ->method('write')
            ->with($this->anything())
            ->willReturn(true);

        $fileSystem->expects($this->any())
            ->method('getRepertoire')
            ->with($this->anything())
            ->willReturn((new DateTime())->format('Y/m/d') . '/pmi-min-1/files/');
    }

    public function testInsertFileEmpty()
    {
        $this->getMountmanager();
        $atexoFichierOrganisme = $this->getAtexoFichierOrganisme();

        $fileName = $pathFile = $organisme = '';
        try {
            $t = $atexoFichierOrganisme->insertFile($fileName, $pathFile, $organisme);
            $this->assertEquals(null, $t);
        } catch (Exception $exception) {
            $this->assertEquals('chemin du fichier incorrect', trim($exception->getMessage()));
        }
    }

    public function getMountmanager()
    {
        $logger = $this->createMock(LoggerInterface::class);
        $this->mountManager = new MountManager(
            $logger,
            $this->createMock(EntityManagerInterface::class),
            '/data/local-trust/mpe_place_test/',
            '/var/tmp/mpe/',
            '/tmp/mnt2;/tmp/mnt3;/tmp/mnt5;/tmp/mnt4',
            new MemoryAdapter(),
        );
    }

    /**
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function testInsertFile()
    {
        $blobId = '';
        $today = (new DateTime())->format('Y/m/d');
        $pathFile = "$today/pmi-min-1/files/";
        $filename = "$blobId-0";
        $this->getMountmanager();
        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $filenameTmp = 'TMP-123456';
        $fileSystemNas->createDir('tmp');
        $expected = 'test';
        $fileSystemNas->write("tmp/$filenameTmp", $expected);
        $atexoFichierOrganisme = $this->getAtexoFichierOrganisme();
        $atexoFichierOrganisme->setServerTmpName($filenameTmp);
        /** @var BloborganismeFile $t */
        $t = $atexoFichierOrganisme->insertFile(
            $filename,
            $pathFile,
            $this->organisme
        );

        //Normalement cela envoie un Id mais dans le cas de ce test on renvoie un id Null
        $this->assertEquals(null, $t);

        // le contenu du fichier créé sur la nas est correct
        $actual = $fileSystemNas->read($pathFile . $filename);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testDeleteBlobFile()
    {
        $organismeBlobRepositoryMock = $this->createMock(BloborganismeFileRepository::class);

        $organismeBlob = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlob);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlob, 1);
        $property = $class->getProperty('organisme');
        $property->setAccessible(true);
        $property->setValue($organismeBlob, 'pmi-min-1');

        $organismeBlobRepositoryMock->expects($this->any())
            ->method('getOrganismeBlobFichier')
            ->willReturn($organismeBlob);

        $organismeBlobFichierRepositoryMock = $this->createMock(BloborganismeFileRepository::class);
        $organismeBlobFichier = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobFichier);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 1);
        $property = $class->getProperty('organisme');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 'pmi-min-1');
        $property = $class->getProperty('chemin');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, '2019/10/15/pmi-min-1/files');

        $organismeBlobFichierRepositoryMock->expects($this->any())
            ->method('getOrganismeBlobFichier')
            ->willReturn($organismeBlobFichier);

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->willReturn($organismeBlobFichierRepositoryMock);

        $this->getMountmanager();

        $atexoFichierOrganisme = $this->getAtexoFichierOrganisme();
        $isDeleted = $atexoFichierOrganisme->deleteBlobFile('1', 'pmi-min-1');
        $this->assertTrue($isDeleted);
    }

    /**
     * @return void
     */
    public function mockOrganismeBlobFichierRepository()
    {
        $organismeBlobFichierRepositoryMock = $this->createMock(BloborganismeFileRepository::class);
        $organismeBlobFichier = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobFichier);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 1);
        $property = $class->getProperty('organisme');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 'pmi-min-1');
        $property = $class->getProperty('chemin');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, '2019/10/15/pmi-min-1/files');

        $organismeBlobFichierRepositoryMock->expects($this->any())
            ->method('getOrganismeBlobFichier')
            ->willReturn($organismeBlobFichier);

        $this->em->expects($this->any())
            ->method('getRepository')
            ->with($this->anything())
            ->willReturn($organismeBlobFichierRepositoryMock);
    }

    public function getAtexoFichierOrganisme(): AtexoFichierOrganisme
    {
        return new AtexoFichierOrganisme(
            $this->container,
            $this->em,
            $this->mountManager,
            $this->parameterBag
        );
    }

    /**
     * @throws FileExistsException
     */
    public function testMoveTmpFile()
    {
        $filename = 'TMP-123456';
        $this->getMountmanager();
        $fileSystemTmp = $this->mountManager->getFilesystem('upload_tmp_dir');
        if (!$fileSystemTmp->has($filename)) {
            $fileSystemTmp->write($filename, 'test');
        }
        $service = $this->getAtexoFichierOrganisme();
        $request = new Request();
        $host = '127.0.0.1';
        $request->server->add(['SERVER_ADDR' => $host]);
        $expected = $host . '-' . $filename;
        $actual = $service->moveTmpFile($filename, $request);
        $this->assertEquals($expected, $actual);
    }

    public function testMoveBlobFile()
    {
        $blobId = 123456;
        $this->getMountmanager();
        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $filenameTmp = 'TMP-123456';
        $fileSystemNas->createDir('tmp');
        $expected = 'test';
        $fileSystemNas->write("tmp/$filenameTmp", $expected);
        $today = (new DateTime())->format('Y/m/d');
        $pathFile = "$today/pmi-min-1/files/";
        $service = $this->getAtexoFichierOrganisme();
        $service->setServerTmpName($filenameTmp);
        $expected = $pathFile;
        $blob = new BloborganismeFile();
        $class = new ReflectionClass($blob);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($blob, $blobId);

        $actual = $service->moveBlobFile($blob, 'pmi-min-1', null);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws Exception
     */
    public function testInsertFileWithExtention()
    {
        $blobId = '';
        $today = (new DateTime())->format('Y/m/d');
        $pathFile = "$today/pmi-min-1/files/";
        $filename = "$blobId-0.brouillon";
        $this->getMountmanager();
        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $filenameTmp = 'TMP-123456';
        $fileSystemNas->createDir('tmp');
        $expected = 'test';
        $fileSystemNas->write("tmp/$filenameTmp", $expected);
        $atexoFichierOrganisme = $this->getAtexoFichierOrganisme();
        $atexoFichierOrganisme->setServerTmpName($filenameTmp);
        /** @var int|null $t */
        $t = $atexoFichierOrganisme->insertFile(
            $filename,
            $pathFile,
            $this->organisme,
            null,
            '.brouillon'
        );

        //Normalement cela envoie un Id mais dans le cas de ce test on renvoie un id Null
        $this->assertEquals(null, $t);

        // le contenu du fichier créé sur la nas est correct
        $actual = $fileSystemNas->read($pathFile . $filename);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @throws FileExistsException
     * @throws ReflectionException
     */
    public function testChangeExtensionBlob()
    {
        $blobId = '1';
        $today = (new DateTime())->format('Y/m/d');
        $pathFile = "$today/pmi-min-1/files/";
        $filename = "$blobId-0.brouillon";
        $this->getMountmanager();
        $fileSystemNas = $this->mountManager->getFilesystem('nas');
        $expected = 'test';
        $fileSystemNas->write($pathFile . $filename, $expected);
        $organismeBlobFichierRepositoryMock = $this->createMock(BloborganismeFileRepository::class);
        $organismeBlobFichier = new BloborganismeFile();
        $class = new ReflectionClass($organismeBlobFichier);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 1);
        $property = $class->getProperty('organisme');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, 'pmi-min-1');
        $property = $class->getProperty('chemin');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, $pathFile);
        $property = $class->getProperty('extension');
        $property->setAccessible(true);
        $property->setValue($organismeBlobFichier, '.brouillon');

        $organismeBlobFichierRepositoryMock->expects($this->any())
            ->method('getorganismeblobfichier')
            ->willReturn($organismeBlobFichier);

        $this->em->expects(self::exactly(2))
            ->method('getRepository')
            ->withConsecutive([BloborganismeFile::class], [BloborganismeFile::class])
            ->willReturnOnConsecutiveCalls(
                $organismeBlobFichierRepositoryMock,
                $organismeBlobFichierRepositoryMock
            );

        $atexoFichierOrganisme = $this->getAtexoFichierOrganisme();
        $atexoFichierOrganisme->changeExtensionBlob('1', 'pmi-min-1', '.attente_chiffrement');
        $filename = "$blobId-0.attente_chiffrement";
        $actual = $fileSystemNas->read($pathFile . $filename);
        $this->assertEquals($expected, $actual);
    }
}
