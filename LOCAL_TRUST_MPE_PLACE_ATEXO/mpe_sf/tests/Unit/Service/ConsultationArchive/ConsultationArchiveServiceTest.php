<?php

namespace Tests\Unit\Service\ConsultationArchive;

use Exception;
use App\Entity\ConsultationArchive\ConsultationArchiveAtlas;
use App\Service\ConsultationArchive\ConsultationArchiveService;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AtexoUtilTest.
 */
class ConsultationArchiveServiceTest extends TestCase
{
    /**
     * @dataProvider getNombreBlocProvider
     */
    public function testGetNombreBloc($consultationsArchiveAtlas, $expected)
    {
        $em = $this->createMock(EntityManager::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $service = new ConsultationArchiveService($em, $containerMock, $logger);
        $res = $service->filter($consultationsArchiveAtlas);
        $actual = count($res);
        $this->assertEquals($actual, $expected);
    }

    /**
     * @dataProvider getNombreBlocProvider
     */
    public function testIsComplete($consultationsArchiveAtlas, $notexpected, $expected)
    {
        $em = $this->createMock(EntityManager::class);
        $containerMock = $this->createMock(ContainerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $service = new ConsultationArchiveService($em, $containerMock, $logger);

        if (false === $expected) {
            $this->expectException(Exception::class);
        }
        $actual = $service->archiveComplete($consultationsArchiveAtlas);
        if (true === $expected) {
            $this->assertEquals($actual, $expected);
        }
    }

    public function getNombreBlocProvider()
    {
        // blocs complets
        $docIds = ['1241_1_h8j', '1241_2_h8j', '1241_3_h8j'];
        $numerosBloc = [1, 2, 3];
        $tailles = ['524288000', '524288000', '500'];

        $consultationsArchiveAtlas = $this->getConsultationsArchiveAtlas(
            $docIds,
            $numerosBloc,
            $tailles
        );
        yield [$consultationsArchiveAtlas, 3, true];

        // bloc complets + doublon
        $docIds = ['1241h8j', '1241_1_h8j', '1241_2_h8j', '1241_3_h8j'];
        $numerosBloc = [1, 1, 2, 3];
        $tailles = ['10', '524288000', '524288000', '500'];
        $consultationsArchiveAtlas = $this->getConsultationsArchiveAtlas(
            $docIds,
            $numerosBloc,
            $tailles
        );
        yield [$consultationsArchiveAtlas, 3, false];

        // bloc complets + un bloc de mauvaise taille
        $docIds = ['1241h8j', '1241_1_h8j', '1241_2_h8j', '1241_3_h8j'];
        $numerosBloc = [1, 1, 2, 3];
        $tailles = ['10', '999', '524288000', '500'];
        $consultationsArchiveAtlas = $this->getConsultationsArchiveAtlas(
            $docIds,
            $numerosBloc,
            $tailles
        );
        yield [$consultationsArchiveAtlas, 3, false];

        // 1 bloc manquant
        $docIds = ['1241_1_h8j', '1241_3_h8j'];
        $numerosBloc = [1, 3];
        $tailles = ['524288000', '500'];
        $consultationsArchiveAtlas = $this->getConsultationsArchiveAtlas(
            $docIds,
            $numerosBloc,
            $tailles
        );
        yield [$consultationsArchiveAtlas, 2, false];
    }

    public function getConsultationsArchiveAtlas(
        array $docIds,
        array $numerosBloc,
        array $tailles
    ) {
        $consultationsArchiveAtlas = [];
        $count = 0;
        foreach ($docIds as $docId) {
            $consultationArchiveAtlas = new ConsultationArchiveAtlas();
            $consultationArchiveAtlas->setDocId($docId);
            $consultationArchiveAtlas->setTaille($tailles[$count]);
            $consultationArchiveAtlas->setNumeroBloc($numerosBloc[$count]);
            $consultationsArchiveAtlas[] = $consultationArchiveAtlas;
            ++$count;
        }

        return $consultationsArchiveAtlas;
    }
}
