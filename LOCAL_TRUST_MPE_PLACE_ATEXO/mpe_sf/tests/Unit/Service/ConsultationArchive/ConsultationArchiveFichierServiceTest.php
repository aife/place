<?php

namespace Tests\Unit\Service\ConsultationArchive;

use Exception;
use App\Service\ConsultationArchive\ConsultationArchiveFichierService;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AtexoUtilTest.
 */
class ConsultationArchiveFichierServiceTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     *
     * @param $filesystem
     * @param $expected
     *
     * @throws Exception
     */
    public function testPopulate($filesystem, $expected)
    {
        $output = $this->createMock(OutputInterface::class);

        $em = $this->createMock(EntityManager::class);
        $container = $this->createMock(ContainerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $consultationArchiveFichierService = new ConsultationArchiveFichierService($em, $container, $logger);
        $consultationArchiveFichierService->setFilesystem($filesystem);
        $consultationArchiveFichierService->setOutput($output);
        $actual = $consultationArchiveFichierService->populate();
        $this->assertEquals($expected, $actual);
    }

    public function dataProvider()
    {
        $adapter = new MemoryAdapter();

        $filesystem = new Filesystem($adapter);
        $filesystem->createDir('./g7h');
        $filesystem->write('./g7h/test_467567856578.zip', str_repeat('0', 200));
        $filesystem->write('./g7h/test_467567856578.txt', str_repeat('0', 100));
        $filesystem->write('./g7h/test_111111111118.zip', str_repeat('0', 1000));
        $filesystem->createDir('./a4n');
        $filesystem->write('./a4n/test_467567856578.zip', str_repeat('0', 200));
        $filesystem->write('./a4n/test_467567856578.txt', str_repeat('0', 100));
        $filesystem->write('./a4n/test_111111111118.zip', str_repeat('0', 1000));
        $filesystem->write('./a4n/test_111111111119.zip', str_repeat('0', 10000));
        $filesystem->write('./a4n/accent , é ê ; /  \ _111111111119.zip', str_repeat('0', 10000));

        yield [$filesystem, true];
        unset($filesystem);

        $adapter = new MemoryAdapter();
        $filesystem = new Filesystem($adapter);
        $filesystem->createDir('./a4n');
        $filesystem->write('./a4n/accent , é ê ; /  \ _111111111119.zip', str_repeat('0', 10000));
        $filesystem->write('./test/a4n/accent , é ê ; /  \ _111111111120.zip', str_repeat('0', 10000));
        $filesystem->write('./a4n/accent , é ê ; /  \ _111111111121.zip', str_repeat('0', 10000));
        yield [$filesystem, false];
    }
}
