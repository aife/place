<?php

namespace Tests\Unit\Service\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchive;
use App\Entity\ConsultationArchive\ConsultationArchiveBloc;
use App\Service\ConsultationArchive\ConsultationArchiveSplitService;
use App\Utils\Filesystem\Adapter\Filesystem;
use App\Utils\Filesystem\Adapter\MemoryAdapter;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AtexoUtilTest.
 */
class ConsultationArchiveSplitServiceTest extends TestCase
{
    public const TEST_TIMEOUT = 3600;

    /**
     * test function main.
     *
     * @throws \Exception
     */
    public function testPopulate()
    {
        $consultationsArchive = $this->getConsultationsArchive();
        $service = $this->getService();
        $service->setPoidsMaximum(10000);
        $listActual = $service->populate((array) $consultationsArchive);
        $actual = count($listActual);
        $expected = 2;
        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider providerException
     *
     * @param $str
     */
    public function testExtractNumeroBlocException($str)
    {
        $this->expectException(Exception::class);
        $service = $this->getService();
        $service->extractNumeroBloc($str);
    }

    public function providerException()
    {
        yield ['123456'];
        yield ['123456-1.zip'];
    }

    /**
     * @dataProvider provider
     *
     * @param $str
     * @param $expected
     *
     * @throws Exception
     */
    public function testExtractNumeroBloc($str, $expected)
    {
        $service = $this->getService();
        $actual = $service->extractNumeroBloc($str);
        $this->assertEquals($expected, $actual);
    }

    public function provider()
    {
        yield ['123456-000001', 1];
    }

    /**
     * @return ConsultationArchive
     */
    public function getConsultationsArchive()
    {
        $consultationArchive = new ConsultationArchive();
        $consultationArchive->setCheminFichier('a4n_1234567.zip');
        $consultationArchive->setConsultationRef('1234567');
        $consultationArchive->setOrganisme('a4n');
        $consultationsArchive[] = $consultationArchive;

        $consultationArchive = new ConsultationArchive();
        $consultationArchive->setCheminFichier('a4n_1234567.zip');
        $consultationArchive->setConsultationRef('1234567');
        $consultationArchive->setOrganisme('a4n');
        $consultationsArchive[] = $consultationArchive;

        return $consultationsArchive;
    }

    public function getService()
    {
        try {
            $service = false;
            /** @var OutputInterface $output */
            $output = $this->createMock(OutputInterface::class);

            $repository = $this->createMock(ObjectRepository::class);
            $repository->expects($this->any())
                ->method('findBy')
                ->with($this->anything())
                ->willReturn([]);

            $em = $this->createMock(EntityManager::class);
            $em
                ->expects($this->any())
                ->method('getRepository')
                ->willReturn($repository);

            $container = $this->createMock(ContainerInterface::class);
            $path = './';
            $logger = $this->createMock(LoggerInterface::class);
            $service = new ConsultationArchiveSplitService($em, $container, $logger, self::TEST_TIMEOUT);

            $service->setPath($path);

            $adapter = new MemoryAdapter();
            $filesystem = new Filesystem($adapter);
            $file = './a4n_1234567.zip';
            $content = '';
            for ($i = 1; $i <= 100; ++$i) {
                $content .= str_pad($i, 10, STR_PAD_LEFT);
            }
            $content .= 'é';
            $filesystem->write($file, $content);
            $service->setChunk(100);
            $service->setFilesystem($filesystem);
            $service->setOutput($output);
        } catch (Exception $e) {
            // not for tests
        }

        return $service;
    }

    /**
     * @return array
     */
    public function getExpected()
    {
        $consultationArchive = new ConsultationArchive();
        $consultationArchive->setCheminFichier('./a4n_123456.zip');
        $consultationArchive->setStatusFragmentation(true);

        $listExpected = [];
        $consultationArchiveBloc = new ConsultationArchiveBloc();
        $consultationArchiveBloc->setDocId('a4n_1_123456');
        $consultationArchiveBloc->setConsultationArchive($consultationArchive);
        $listExpected[] = $consultationArchiveBloc;

        $consultationArchiveBloc = new ConsultationArchiveBloc();
        $consultationArchiveBloc->setDocId('a4n_2_123456');
        $consultationArchiveBloc->setConsultationArchive($consultationArchive);
        $listExpected[] = $consultationArchiveBloc;

        return $listExpected;
    }

    /**
     * test function main.
     *
     * @throws \Exception
     */
    public function testGetChunk()
    {
        $service = $this->getService();
        $expected = 1;
        $service->setChunk($expected);
        $actual = $service->getChunk();
        $this->assertSame($expected, $actual);
    }

    /**
     * @dataProvider chunkException
     * test function main
     */
    public function testGetChunkException($chunk)
    {
        $this->expectException(Exception::class);
        $service = $this->getService();
        $service->setChunk($chunk);
        $service->getChunk();
    }

    public function chunkException()
    {
        yield [-1];
        yield [false];
        yield ['false'];
    }
}
