<?php

namespace Tests\Unit\Service;

use Symfony\Component\Translation\Translator;
use App\Service\AtexoUtil;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

/**
 * Class AtexoUtilTest.
 */
class AtexoUtilTest extends AtexoTestCase
{
    private $entityManager;
    private $container;
    private $service;
    private $logMock;
    private $serviceUtils;
    private $sessionMock;
    private $translatorMock;
    private $requestStack;

    /**
     * @dataProvider arrayProvider
     */
    public function testImplodeArrayWithSeparator($service, $arrayParent, $separator, $expected)
    {
        $atexoUtil = parent::getAtexoUtil();

        $test = $atexoUtil->implodeArrayWithSeparator($service, $arrayParent, $separator);

        $this->assertEquals($expected, $test);
    }

    public function arrayProvider()
    {
        return [
            [
                'test',
                [['libelle' => 'foo'], ['libelle' => 'bar'], ['libelle' => 'baz']],
                ' - ',
                'baz - bar - foo - test',
            ],
            ['a4n', [['libelle' => 'g7h'], ['libelle' => 'l2m'], ['libelle' => 't9y']], ' / ', 't9y / l2m / g7h / a4n'],
            ['test', [['libelle' => 'foo']], ' / ', 'foo / test'],
            ['a4n', [['libelle' => 'g7h']], ' - ', 'g7h - a4n'],
            ['test', [], ' - ', 'test'],
            ['a4n', [], ' / ', 'a4n'],
        ];
    }

    /**
     * @dataProvider acheteurProvider
     */
    public function testTruncate($str, $expected)
    {
        $atexoUtil = parent::getAtexoUtil();

        $actual = $atexoUtil->truncate($str, 100);

        $this->assertEquals($actual, $expected);
    }

    public function acheteurProvider()
    {
        return [
            [str_repeat('é', 100), str_repeat('é', 100)],
            [str_repeat('é', 101), str_repeat('é', 97).'...']
        ];
    }

    /**
     * @dataProvider validSiretProvider
     */
    public function testIsValidSiret($siret, $expected)
    {
        $requestStackMock = $this->createMock(RequestStack::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        $atexoUtil = new AtexoUtil(null, new Session(), new Translator('fr'), null, $requestStackMock, $parameterBagMock);
        $actual = $atexoUtil->isValidSiret($siret);
        $this->assertEquals($actual, $expected);
    }

    public function validSiretProvider()
    {
        return [
            ['', false],
            ['50073189800390987654321', false],
            ['50', false],
            ['50073189800390', true],
            ['44090956200033', true],
        ];
    }

    /**
     * @dataProvider getSizeNameProvider
     *
     * @param $expected
     */
    public function testGetSizeName(int $size, string $expected)
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->container = $this->createMock(Container::class);
        $this->logMock = $this->createMock(Logger::class);
        $this->sessionMock = $this->createMock(Session::class);
        $this->translatorMock = $this->createMock(TranslatorInterface::class);
        $this->requestStack = $this->createMock(RequestStack::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        $atexoUtil = new AtexoUtil(
            $this->container,
            $this->sessionMock,
            $this->translatorMock,
            $this->logMock,
            $this->requestStack,
            $parameterBagMock
        );
        $actual = $atexoUtil->getSizeName($size);
        $this->assertEquals($actual, $expected);
    }

    public function getSizeNameProvider()
    {
        return [
            [0, '0 octets'],
            [10, '10 octets'],
            [1234, '1.21 Ko'],
            [1234567, '1.18 Mo'],
            [12345678901, '11.5 Go'],
            [12345678901234, '11.2 To'],
            [123456789012345678, '110 Po'],
        ];
    }

    /**
     * @dataProvider getArrayArboProvider
     *
     * @param int $size
     * @param $expected
     */
    public function testGetArrayArbo(array $array, array $expected)
    {
        $this->entityManager = $this->createMock(EntityManager::class);
        $this->container = $this->createMock(Container::class);
        $this->logMock = $this->createMock(Logger::class);
        $this->sessionMock = $this->createMock(Session::class);
        $this->translatorMock = $this->createMock(TranslatorInterface::class);
        $this->requestStack = $this->createMock(RequestStack::class);
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);

        $atexoUtil = new AtexoUtil(
            $this->container,
            $this->sessionMock,
            $this->translatorMock,
            $this->logMock,
            $this->requestStack,
            $parameterBagMock
        );
        $actual = $atexoUtil->getArrayArbo($array);

        $this->assertEquals($actual, $expected);
    }

    public function testKebabCaseToLowerCamelCase()
    {
        $atexoUtil = new AtexoUtil(
            $this->createMock(Container::class),
            $this->createMock(Session::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Logger::class),
            $this->createMock(RequestStack::class),
            $this->createMock(ParameterBagInterface::class)
        );

        $response = $atexoUtil->kebabCaseToCamelCase('statut-non-delivre');

        $this->assertEquals('statutNonDelivre', $response);
    }

    public function testKebabCaseToUpperCamelCase()
    {
        $atexoUtil = new AtexoUtil(
            $this->createMock(Container::class),
            $this->createMock(Session::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(Logger::class),
            $this->createMock(RequestStack::class),
            $this->createMock(ParameterBagInterface::class)
        );

        $response = $atexoUtil->kebabCaseToCamelCase('statut-non-delivre', true);

        $this->assertEquals('StatutNonDelivre', $response);
    }

    public function getArrayArboProvider()
    {
        return [
            [
                [],
                [],
            ],
            [
                ['GRANDPERE##PERE##FILS'],
                ['GRANDPERE' => ['PERE' => [0 => 'FILS']]],
            ],
            [
                ['GRANDPERE##PERE##FILS1', 'GRANDPERE##PERE##FILS2'],
                ['GRANDPERE' => ['PERE' => [0 => 'FILS1', 1 => 'FILS2']]],
            ],
            [
                [
                    'GRANDPERE##PERE1##FILS1',
                    'GRANDPERE##PERE2##FILS1',
                    'GRANDPERE##PERE1##FILS2',
                    'GRANDPERE##PERE2##FILS2',
                ],
                ['GRANDPERE' => ['PERE1' => [0 => 'FILS1', 1 => 'FILS2'], 'PERE2' => [0 => 'FILS1', 1 => 'FILS2']]],
            ],
        ];
    }
}
