<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Service\Logger;

use App\Entity\Agent;
use App\Logger\ExtraInfoProcessor;
use App\Service\Version\VersionService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Security;

class ExtraInfoProcessorTest extends TestCase
{
    public function testAddExtraInfoToMonolog()
    {
        $mockSecurity = $this->createMock(Security::class);
        $mockParameterBag = $this->createMock(VersionService::class);

        $mockSecurity->method('getUser')->willReturn(new Agent());
        $mockParameterBag->method('getApplicationVersion')->willReturn('2022.00.02.17');

        $processor = new ExtraInfoProcessor($mockSecurity, $mockParameterBag);
        $result = $processor([]);

        $this->assertIsArray($result);
        $this->assertSame('Agent', $result['extra']['Context']['user-type']);
        $this->assertSame('2022.00.02.17', $result['extra']['Context']['mpe-version']);
    }

    public function testAddExtraInfoToMonologWithNoUserConnected()
    {
        $mockSecurity = $this->createMock(Security::class);
        $mockParameterBag = $this->createMock(VersionService::class);

        $mockSecurity->method('getUser')->willReturn(null);
        $mockParameterBag->method('getApplicationVersion')->willReturn('2022.00.02.17');

        $processor = new ExtraInfoProcessor($mockSecurity, $mockParameterBag);
        $result = $processor([]);

        $this->assertIsArray($result);
        $this->assertSame('Anonyme', $result['extra']['Context']['user-type']);
        $this->assertSame('2022.00.02.17', $result['extra']['Context']['mpe-version']);
    }
}
