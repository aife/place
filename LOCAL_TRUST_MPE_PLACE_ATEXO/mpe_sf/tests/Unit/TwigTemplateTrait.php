<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit;

use Symfony\Component\Translation\Translator;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

trait TwigTemplateTrait
{
    protected function getTemplate($templateDir)
    {
        $loader = new FilesystemLoader($templateDir);
        $twig = new Environment($loader, ['debug' => true, 'cache' => false]);
        $translator = new Translator('fr');
        $twig->addExtension(new TranslationExtension($translator));

        return $twig;
    }
}
