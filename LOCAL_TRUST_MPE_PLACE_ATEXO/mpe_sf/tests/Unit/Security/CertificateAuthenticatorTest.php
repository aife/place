<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Repository\AgentRepository;
use App\Repository\InscritRepository;
use App\Security\CertificateAuthenticator;
use App\Security\GuardManager;
use App\Service\AtexoConfiguration;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class CertificateAuthenticatorTest extends TestCase
{
    /**
     * @var CertificateAuthenticator
     */
    private $certificateAuthenticator;

    /**
     * @var AtexoConfiguration|MockObject
     */
    private $atexoConfiguration;

    protected function setUp(): void
    {
        parent::setUp();
        $this->certificateAuthenticator = new CertificateAuthenticator(...$this->getMocks());
    }

    protected function getMocks(EntityManagerInterface $entityManagerMock = null): array
    {
        $this->atexoConfiguration = $this->createMock(AtexoConfiguration::class);

        return [
            $this->createMock(TranslatorInterface::class),
            new GuardManager(
                $this->createMock(EntityManagerInterface::class),
                $this->createMock(ParameterBagInterface::class),
                $this->createMock(Security::class),
                $this->createMock(RouterInterface::class)
            ),
            $this->atexoConfiguration,
            $entityManagerMock ?? $this->createMock(EntityManagerInterface::class),
            $this->createMock(LoggerInterface::class)
        ];
    }

    public function supportsDataProvider(): array
    {
        return [
            ['/', [], false, false],
            ['/agent/login-by-certificate', [], false, false],
            ['/agent/login-by-certificate', [], true, false],
            ['/agent/login-by-certificate', ['SSL_CLIENT_CERT' => ''], true, false],
            ['/agent/login-by-certificate', ['SSL_CLIENT_M_SERIAL' => ''], true, false],
            ['/agent/login-by-certificate', ['SSL_CLIENT_CERT' => '', 'SSL_CLIENT_M_SERIAL' => ''], true, true],
            ['/', ['SSL_CLIENT_CERT' => '', 'SSL_CLIENT_M_SERIAL' => ''], true, false],
            ['/agent/login-by-certificate', ['SSL_CLIENT_CERT' => '', 'SSL_CLIENT_M_SERIAL' => ''], false, false],
        ];
    }

    /**
     * @dataProvider supportsDataProvider
     * @param string $uri
     * @param array $serverVariables
     * @param bool $authByCertificateActivated
     * @param bool $expected
     */
    public function testSupports(string $uri, array $serverVariables, bool $authByCertificateActivated, bool $expected)
    {
        $this->atexoConfiguration->method('hasConfigPlateforme')->willReturn($authByCertificateActivated);

        $request = Request::create($uri, 'GET', [], [], [], $serverVariables);
        $result = $this->certificateAuthenticator->supports($request);

        $this->assertEquals($expected, $result);
    }

    public function testGetCredentials()
    {
        $certificateValue = 'fake certificate value';
        $certSerialValue = 'fake certificate serial value';
        $serverVariables = [
            'SSL_CLIENT_CERT' => $certificateValue,
            'SSL_CLIENT_M_SERIAL' => $certSerialValue,
            'WHATEVER' => 'anything',
        ];

        $request = Request::create('/', 'GET', [], [], [], $serverVariables);
        $result = $this->certificateAuthenticator->getCredentials($request);

        $this->assertArrayHasKey('certificate', $result);
        $this->assertArrayHasKey('cert_serial', $result);

        $this->assertEquals(
            [
                'certificate' => $certificateValue,
                'cert_serial' => $certSerialValue,
            ],
            $result
        );
    }
    public function testGetUser()
    {
        $userProvider = $this->createMock(AgentRepository::class);
        $userProvider->expects($this->any())
            ->method('supportsClass')
            ->willReturn(true);
        ;

        $repositoryAgent = $this->createMock(ObjectRepository::class);

        $agent = new Agent();

        $agent->setLogin('adminTest');

        $repositoryAgent
            ->expects($this->once())
            ->method('findOneBy')
            ->with(['certificat' => 'CERTIFICATE_VALUE'])
            ->willReturn($agent);

        $emMock = $this->createMock(EntityManagerInterface::class);
        $emMock->method('getRepository')->willReturn($repositoryAgent);

        $certificateAuthenticator = new CertificateAuthenticator(...$this->getMocks($emMock));
        $user = $certificateAuthenticator->getUser(['certificate' => 'CERTIFICATE_VALUE'], $userProvider);

        $this->assertEquals($agent, $user);

        // L'auth par certificat n'est pas possible côté Entreprise
        $userProvider = $this->createMock(InscritRepository::class);
        $result = $certificateAuthenticator->getUser(['certificate' => 'CERTIFICATE_VALUE'], $userProvider);
        $this->assertNull($result);
    }

    public function testCheckCredentials()
    {
        $user = new Agent();

        $user->setNumCertificat('n° certif');
        $result = $this->certificateAuthenticator->checkCredentials(['cert_serial' => 'n° certif'], $user);
        $this->assertTrue($result);

        $result = $this->certificateAuthenticator->checkCredentials(['cert_serial' => 'mauvais n° certif'], $user);
        $this->assertFalse($result);

        $user = new Inscrit();
        $result = $this->certificateAuthenticator->checkCredentials(['cert_serial' => 'n° certif'], $user);
        $this->assertFalse($result);
    }

    public function testOnAuthenticationFailure()
    {
        $request = Request::create('/');

        $request->setSession(new Session(new MockArraySessionStorage()));

        $guardManager = $this->createMock(GuardManager::class);
        $certificateAuthenticator = new CertificateAuthenticator(
            $this->createMock(TranslatorInterface::class),
            $guardManager,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(LoggerInterface::class)
        );

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn('/agent/login')
        ;

        $certificateAuthenticator->onAuthenticationFailure($request, new AuthenticationException());

        $errors = $request->getSession()->getFlashbag()->get('error');

        $this->assertCount(1, $errors);
    }
}
