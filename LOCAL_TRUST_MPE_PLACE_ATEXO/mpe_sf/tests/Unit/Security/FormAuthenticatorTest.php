<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Security;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use App\Repository\AdministrateurRepository;
use App\Repository\AgentRepository;
use App\Repository\ConfigurationPlateformeRepository;
use App\Repository\InscritRepository;
use App\Repository\ServiceRepository;
use App\Security\Checker\UserChecker;
use App\Security\Exception\AccountDisabledException;
use App\Security\FormAuthenticator;
use App\Security\GuardManager;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEntreprise;
use App\Service\MpeSecurity;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FormAuthenticatorTest extends TestCase
{
    /**
     * @var SessionStorageInterface|null
     */
    protected $storage;

    /**
     * @var SessionInterface|null
     */
    protected $session;

    /**
     * @var FormAuthenticator
     */
    protected $formAuthenticator;

    /**
     * @var MockObject
     */
    protected $mpeSecurityMock;

    /**
     * @var MockObject
     */
    protected $userPasswordEncoderMock;

    protected const URI_AGENT = '/agent/login';
    protected const URI_ENTREPRISE = '/entreprise/login';
    protected const URI_ADMIN = '/admin/login';
    protected const URI_SUPERADMIN = '/superadmin/login';

    protected const LOGIN_URIS = [
        self::URI_ENTREPRISE,
        self::URI_ADMIN,
        self::URI_SUPERADMIN,
    ];

    protected function setUp(): void
    {
        parent::setUp();
        $this->formAuthenticator = new FormAuthenticator(...$this->getMocks());
        $this->storage = new MockArraySessionStorage();
        $this->session = new Session($this->storage, new AttributeBag(), new FlashBag());
    }

    protected function getMocks()
    {
        $this->mpeSecurityMock = $this->createMock(MpeSecurity::class);
        $this->userPasswordEncoderMock = $this->createMock(UserPasswordHasherInterface::class);

        return [
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->mpeSecurityMock,
            new GuardManager(
                $this->createMock(EntityManagerInterface::class),
                $this->createMock(ParameterBagInterface::class),
                $this->createMock(Security::class),
                $this->createMock(RouterInterface::class)
            ),
            $this->createMock(LoggerInterface::class),
            $this->userPasswordEncoderMock,
            $this->createMock(ConfigurationPlateformeRepository::class),
        ];
    }

    protected function addFormData(Request $request, string $username = 'aUsername'): Request
    {
        $request->request->add(
            [
                FormAuthenticator::LOGINFORM => [
                    '_username' => $username,
                    '_password' => 'greatPassword',
                    '_token' => 'token123abc',
                ],
            ]
        );
        return $request;
    }

    public function testSupports()
    {
        foreach (self::LOGIN_URIS as $uri) {
            $request = Request::create($uri);
            $result = $this->formAuthenticator->supports($request);
            $this->assertFalse($result, "Sans infos de formulaire, l'authenticator ne doit pas supporter " . $uri);
        }

        foreach (self::LOGIN_URIS as $uri) {
            $request = Request::create($uri, 'POST');
            $request = $this->addFormData($request);
            $request->setSession($this->session);
            $result = $this->formAuthenticator->supports($request);
            $this->assertTrue($result, "L'authenticator doit supporter " . $uri);
        }
    }

    public function testNoCatchGetUriLogin()
    {
        //Agent
        $this->assertNoCatchGetUriLogin(self::URI_AGENT);
        //Entreprise
        $this->assertNoCatchGetUriLogin(self::URI_ENTREPRISE);
        //Admin
        $this->assertNoCatchGetUriLogin(self::URI_ADMIN);
        //SuperAdmin
        $this->assertNoCatchGetUriLogin(self::URI_SUPERADMIN);
    }

    public function testCatchPostUriLogin()
    {
        //Agent
        $this->assertCatchPostUriLogin(self::URI_AGENT);
        //Entreprise
        $this->assertCatchPostUriLogin(self::URI_ENTREPRISE);
        //Admin
        $this->assertCatchPostUriLogin(self::URI_ADMIN);
        //SuperAdmin
        $this->assertCatchPostUriLogin(self::URI_SUPERADMIN);
    }

    public function testGetUserFails()
    {
        //Agent
        $this->assertGetUserFails(self::URI_AGENT);
        //Entreprise
        $this->assertGetUserFails(self::URI_ENTREPRISE);
        //Admin
        $this->assertGetUserFails(self::URI_ADMIN);
        //SuperAdmin
        $this->assertGetUserFails(self::URI_SUPERADMIN);
    }

    public function testLoginSucceeds()
    {
        //Agent
        $this->assertLoginSucceeds(self::URI_AGENT, new Agent(), AgentRepository::class);
        //Entreprise
        $this->assertLoginSucceeds(self::URI_ENTREPRISE, new Inscrit(), InscritRepository::class);
        //Admin
        $this->assertLoginSucceeds(self::URI_ADMIN, new Administrateur(), AdministrateurRepository::class);
    }

    protected function assertNoCatchGetUriLogin(string $uri)
    {
        $request = Request::create($uri);
        $result = $this->formAuthenticator->getCredentials($request);
        $this->assertIsArray($result);
        foreach (['login', 'password', 'csrf_token'] as $item) {
            $this->assertNull($result[$item]);
        }
    }

    protected function assertCatchPostUriLogin(string $uri)
    {
        $request = Request::create($uri, 'POST');

        $this->assertArrayHasKey('login', $this->formAuthenticator->getCredentials($request));
        $this->assertArrayHasKey('password', $this->formAuthenticator->getCredentials($request));
        $this->assertArrayHasKey('csrf_token', $this->formAuthenticator->getCredentials($request));
    }

    public function getCredentialsDataProvider(): array
    {
        return [
            [self::URI_AGENT, Agent::class],
            [self::URI_ENTREPRISE, Inscrit::class],
            [self::URI_ADMIN, Administrateur::class],
            [self::URI_SUPERADMIN, Administrateur::class],
        ];
    }

    /**
     * @dataProvider getCredentialsDataProvider
     * @param string $uri
     * @param string $class
     */
    public function testGetCredentials(string $uri, string $class)
    {
        $mocks = $this->getMocks();
        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $mocks[4] = $guardManager;
        $this->formAuthenticator = new FormAuthenticator(...$mocks);

        $request = Request::create(
            $uri,
            'POST',
            [
                FormAuthenticator::LOGINFORM => [],
            ]
        );

        $credentials = $this->formAuthenticator->getCredentials($request);

        foreach (['login', 'password', 'csrf_token'] as $param) {
            $this->assertArrayHasKey($param, $credentials);
            $this->assertEmpty($credentials[$param]);
        }

        $this->assertEquals($class, $credentials['user_type']);

        // With right parameters
        $username = 'user';
        $request = Request::create($uri, 'POST');
        $request = $this->addFormData($request, $username);

        $credentials = $this->formAuthenticator->getCredentials($request);

        $this->assertArrayHasKey('login', $credentials);
        $this->assertNotEmpty($credentials['login']);
        $this->assertEquals($username, $credentials['login']);
    }

    public function testOnAuthenticationFailure()
    {
        $request = Request::create(self::URI_AGENT);
        $request->setSession($this->session);

        $this->mpeSecurityMock->expects($this->never())->method('incrementTentativesMdp');

        $guardManager = $this->createMock(GuardManager::class);

        $authenticator = new FormAuthenticator(
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(MpeSecurity::class),
            $guardManager,
            $this->createMock(LoggerInterface::class),
            $this->createMock(UserPasswordHasherInterface::class),
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn('/agent/login')
        ;

        $response = $authenticator->onAuthenticationFailure($request, new AuthenticationException());

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals(1, sizeof($request->getSession()->getFlashBag()->get('error')));
    }

    public function testOnAuthenticationFailureIncrementTentativesMdp()
    {
        $request = Request::create(self::URI_AGENT);
        $request->setSession($this->session);


        $guardManager = $this->createMock(GuardManager::class);
        $passwordEncoder = $this->createMock(UserPasswordHasher::class);
        $mpeSecurityMock = $this->createMock(MpeSecurity::class);

        $passwordEncoder->method('isPasswordValid')->willReturn(false);

        $authenticator = new FormAuthenticator(
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $mpeSecurityMock,
            $guardManager,
            $this->createMock(LoggerInterface::class),
            $passwordEncoder,
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $mpeSecurityMock->expects(self::once())->method('incrementTentativesMdp');

        $passwordEncoder
            ->expects(self::once())
            ->method('isPasswordValid')
            ->willReturn(false)
        ;

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn('/agent/login')
        ;

        $authenticator->checkCredentials(['password' => 'mdp'], new Agent());
        $authenticator->onAuthenticationFailure($request, new AuthenticationException());
    }

    public function testOnAuthenticationSuccess()
    {
        $request = Request::create(self::URI_AGENT);
        $request->setSession($this->session);

        $guardManager = $this->createMock(GuardManager::class);
        $authenticator = new FormAuthenticator(
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(MpeSecurity::class),
            $guardManager,
            $this->createMock(LoggerInterface::class),
            $this->createMock(UserPasswordHasherInterface::class),
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn('/agent/login')
        ;

        $response = $authenticator
            ->onAuthenticationSuccess($request, $this->createMock(TokenInterface::class), null);

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals(0, sizeof($request->getSession()->getFlashBag()->get('error')));
    }

    public function testOnAuthenticationSuccessWithSocleInterne()
    {
        $request = Request::create(self::URI_AGENT);
        $request->setSession($this->session);

        $guardManager = $this->createMock(GuardManager::class);
        $authenticator = new FormAuthenticator(
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(MpeSecurity::class),
            $guardManager,
            $this->createMock(LoggerInterface::class),
            $this->createMock(UserPasswordHasherInterface::class),
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn('agent/socle-interne')
        ;

        $response = $authenticator
            ->onAuthenticationSuccess($request, $this->createMock(TokenInterface::class), null);

        self::assertSame($response->getTargetUrl(), 'agent/socle-interne');
        self::assertEquals(0, sizeof($request->getSession()->getFlashBag()->get('error')));
    }

    public function testGetPassword()
    {
        $password = 'securedPwd';

        $this->assertEquals($password, $this->formAuthenticator->getPassword(['password' => $password]));
    }

    public function testSupportsRememberMe()
    {
        $this->assertTrue($this->formAuthenticator->supportsRememberMe());
    }

    protected function assertGetUserFails(string $uri)
    {
        $request = Request::create($uri);
        $request->setSession($this->session);
        $credentials = $this->formAuthenticator->getCredentials($request);
        $userProvider = $this->createMock(UserProviderInterface::class);

        // Without CSRF toker
        $this->expectException(InvalidCsrfTokenException::class);
        $this->formAuthenticator->getUser($credentials, $userProvider);

        // With right parameters but user does not exist
        $username = 'unique-username';

        $request = $this->addFormData($request, $username);
        $request->setSession($this->session);

        $userProvider->expects($this->once())
            ->method('loadUserByIdentifier')
            ->willReturn(null);

        $this->expectException(CustomUserMessageAuthenticationException::class);
        $this->formAuthenticator->getUser($credentials, $userProvider);
    }

    protected function assertLoginSucceeds(string $uri, UserInterface $user, string $repository)
    {
        $username = 'unique-username';

        $request = Request::create($uri);
        $request->setSession($this->session);

        $credentials =  [
            'csrf_token' => 'csrf_token',
            'login' => $username,
            'password' => $username
        ];

        $userProvider = $this->createMock(UserProviderInterface::class);

        $request = $this->addFormData($request, $username);

        $user->setLogin($username);

        $repository = $this->createMock($repository);
        $repository
            ->expects($this->once())
            ->method('loadUserByIdentifier')
            ->with($username)
            ->willReturn($user)
        ;

        // Token manager accepts token
        $csrfTokenManager = $this->createMock(CsrfTokenManagerInterface::class);
        $csrfTokenManager->expects($this->once())
            ->method('isTokenValid')
            ->willReturn(true);

        $authenticator = new FormAuthenticator(
            $csrfTokenManager,
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $this->createMock(MpeSecurity::class),
            $this->createMock(GuardManager::class),
            $this->createMock(LoggerInterface::class),
            $this->createMock(UserPasswordHasherInterface::class),
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $authenticatedUser = $authenticator->getUser($credentials, $repository);

        $this->assertEquals($user->getLogin(), $authenticatedUser->getUsername());
    }

    public function checkCredentialsDataProvider()
    {
        return [
            [Agent::class, false, 0, '1', false, 'Agent : Mauvais mdp'],
            [Agent::class, true, 2, '1', true, 'Agent : Avec le bon mdp'],
            [Agent::class, true, 3, '1', false, 'Agent : Tentatives mdp dépassé'],
            [Agent::class, true, 0, '0', false, 'Agent : User inactif'],

            [Inscrit::class, false, 0, '1', false, 'Inscrit : Mauvais mdp'],
            [Inscrit::class, true, 2, '1', true, 'Inscrit : Avec le bon mdp'],
            [Inscrit::class, true, 3, '1', false, 'Inscrit : Tentatives mdp dépassé'],
            [Inscrit::class, true, 0, '0', false, 'Inscrit : User inactif'],

            [Administrateur::class, true, 0, '0', true, 'Admin : Bon mdp (en clair...)'],
            [Administrateur::class, false, 0, '0', false, 'Admin : Mauvais mdp (en clair...)'],
            [Administrateur::class, true, 4, '0', false, 'Admin : Tentatives mdp dépassé'],
        ];
    }

    /**
     * @dataProvider checkCredentialsDataProvider
     */
    public function testCheckCredentials(
        $userClass,
        bool $isPasswordValid,
        $tentativesMdp,
        $actif,
        $expected,
        $message
    ) {
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock->method('get')->willReturnMap(
            [
                ['MAX_TENTATIVES_MDP', 3],
                ['ENABLE_SHA256_PASSWORDS', '1'],
            ]
        );


        $cp = new ConfigurationPlateforme();
        $cp->setAuthenticateAgentByLogin(1);
        $cp->setAuthenticateInscritByLogin(1);

        $repo = $this->createMock(ConfigurationPlateformeRepository::class);
        $repo->expects($this->once())->method('getConfigurationPlateforme')->willReturn($cp);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->once())->method('getRepository')->willReturn($repo);

        $mpeSecurity = new MpeSecurity(
            $em,
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(LoggerInterface::class),
            $parameterBagMock,
        );

        $passwordEncoderMock = $this->createMock(UserPasswordHasher::class);
        $passwordEncoderMock->method('isPasswordValid')->willReturn($isPasswordValid);

        $authenticator = new FormAuthenticator(
            $this->createMock(CsrfTokenManagerInterface::class),
            $this->createMock(RouterInterface::class),
            $this->createMock(TranslatorInterface::class),
            $mpeSecurity,
            $this->createMock(GuardManager::class),
            $this->createMock(LoggerInterface::class),
            $passwordEncoderMock,
            $this->createMock(ConfigurationPlateformeRepository::class)
        );

        $userChecker = new UserChecker($mpeSecurity);

        $credentials = [
            'user_type' => $userClass,
            'password' => 'test0123',
        ];

        $user = new $userClass();
        if (Agent::class === $userClass) {
            $user->setActif($actif);
        }
        if (Inscrit::class === $userClass) {
            $user->setBloque($actif ? '0' : '1');
        }
        $user->setTentativesMdp($tentativesMdp);

        if (
            $tentativesMdp >= 3
            || (Administrateur::class !== $userClass && !$actif)
        ) {
            $this->expectException(AccountDisabledException::class);
        }

        $userChecker->checkPreAuth($user);
        $result = $authenticator->checkCredentials($credentials, $user);

        $this->assertEquals($expected, $result, $message);
    }
}
