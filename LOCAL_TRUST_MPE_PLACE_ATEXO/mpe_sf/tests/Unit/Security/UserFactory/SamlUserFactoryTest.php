<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\UserFactory;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Exception\OrganismeNotFoundException;
use App\Repository\OrganismeRepository;
use App\Repository\ServiceRepository;
use App\Security\UserFactory\SamlUserFactory;
use App\Service\Agent\Habilitation;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SamlUserFactoryTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCreateUserWithServiceNull(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $habilitationService = $this->createMock(Habilitation::class);

        $username = 'saml_user_login';

        $mappedAttributes = [
            'email' => ['saml_user@email.fr'],
            'lastName' => ['nom'],
            'firstName' => ['prenom'],
        ];

        $organismeAcronyme = 'pmi-min-1';
        $profileId = '1';

        $logger
            ->expects(self::exactly(3))
            ->method('info')
            ->withConsecutive(
                ["Begin creation User saml_user_login via SamlUserFactory"],
                ["User saml_user_login created via SamlUserFactory with organism=pmi-min-1, profileId=1, serviceId="],
                ["End creation User saml_user_login via SamlUserFactory"],
            )
        ;

        $parameterBag
            ->expects(self::exactly(3))
            ->method('get')
            ->withConsecutive(
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_SERVICE"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_HABILITATION_PROFIL"],
            )
            ->willReturnOnConsecutiveCalls(
                $organismeAcronyme,
                null,
                $profileId,
            )
        ;

        $organisme = new Organisme();
        $organisme->setAcronyme($organismeAcronyme);

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['acronyme' => $organismeAcronyme])
            ->willReturn($organisme)
        ;

        $logger
            ->expects(self::never())
            ->method('error')
        ;

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository
            ->expects(self::never())
            ->method('findOneBy')
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($organismeRepository)
        ;

        $habilitationAgent = new HabilitationAgent();
        $habilitationService
            ->expects(self::once())
            ->method('createHabilitationsAgent')
            ->with($username, $profileId)
            ->willReturn($habilitationAgent)
        ;

        $samlUserFactory = new SamlUserFactory($em, $logger, $parameterBag, $habilitationService);

        $result =  $samlUserFactory->createUser($username, $mappedAttributes);

        self::assertInstanceOf(Agent::class, $result);
        self::assertSame($username, $result->getLogin());
        self::assertSame('saml_user@email.fr', $result->getEmail());
        self::assertSame($organisme, $result->getOrganisme());
        self::assertSame($habilitationAgent, $result->getHabilitation());
    }

    /**
     * @throws Exception
     */
    public function testCreateUserWithService(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $habilitationService = $this->createMock(Habilitation::class);

        $username = 'saml_user_login';

        $mappedAttributes = [
            'email' => ['saml_user@email.fr'],
            'lastName' => ['nom'],
            'firstName' => ['prenom'],
        ];

        $organismeAcronyme = 'pmi-min-1';
        $profileId = '1';
        $serviceId = '2';

        $logger
            ->expects(self::exactly(3))
            ->method('info')
            ->withConsecutive(
                ["Begin creation User saml_user_login via SamlUserFactory"],
                ["User saml_user_login created via SamlUserFactory with organism=pmi-min-1, profileId=1, serviceId=2"],
                ["End creation User saml_user_login via SamlUserFactory"],
            )
        ;

        $parameterBag
            ->expects(self::exactly(3))
            ->method('get')
            ->withConsecutive(
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_SERVICE"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_HABILITATION_PROFIL"],
            )
            ->willReturnOnConsecutiveCalls(
                $organismeAcronyme,
                $serviceId,
                $profileId,
            )
        ;

        $organisme = new Organisme();
        $organisme->setAcronyme($organismeAcronyme);

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['acronyme' => $organismeAcronyme])
            ->willReturn($organisme)
        ;

        $logger
            ->expects(self::never())
            ->method('error')
        ;

        $service = new Service();
        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'id' => $serviceId,
                'organisme' => $organisme,
            ])
            ->willReturn($service)
        ;

        $em
            ->expects(self::exactly(2))
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $organismeRepository,
                $serviceRepository
            )
        ;

        $habilitationAgent = new HabilitationAgent();
        $habilitationService
            ->expects(self::once())
            ->method('createHabilitationsAgent')
            ->with($username, $profileId)
            ->willReturn($habilitationAgent)
        ;

        $samlUserFactory = new SamlUserFactory($em, $logger, $parameterBag, $habilitationService);

        $result =  $samlUserFactory->createUser($username, $mappedAttributes);

        self::assertInstanceOf(Agent::class, $result);
        self::assertSame($username, $result->getLogin());
        self::assertSame('saml_user@email.fr', $result->getEmail());
        self::assertSame($organisme, $result->getOrganisme());
        self::assertSame($habilitationAgent, $result->getHabilitation());
        self::assertSame($service, $result->getService());
    }

    /**
     * @throws Exception
     */
    public function testCreateUserThrowExceptionWhenOrganismNotFound(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $habilitationService = $this->createMock(Habilitation::class);

        $username = 'saml_user_login';

        $mappedAttributes = [
            'email' => ['saml_user@email.fr'],
            'lastName' => ['nom'],
            'firstName' => ['prenom'],
        ];

        $organismeAcronyme = 'pmi-min-1';
        $profileId = '1';
        $serviceId = '2';

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Begin creation User saml_user_login via SamlUserFactory")
        ;

        $parameterBag
            ->expects(self::exactly(3))
            ->method('get')
            ->withConsecutive(
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_SERVICE"],
                ["OAUTH_KEYCLOAK_SAML2_REFERENCE_HABILITATION_PROFIL"],
            )
            ->willReturnOnConsecutiveCalls(
                $organismeAcronyme,
                $serviceId,
                $profileId,
            )
        ;

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['acronyme' => $organismeAcronyme])
            ->willReturn(null)
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with("Can't create User - No organism found for acronyme: pmi-min-1")
        ;

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository
            ->expects(self::never())
            ->method('findOneBy')
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($organismeRepository)
        ;

        $habilitationService
            ->expects(self::never())
            ->method('createHabilitationsAgent')
            ->with($username, $profileId)
        ;

        $samlUserFactory = new SamlUserFactory($em, $logger, $parameterBag, $habilitationService);

        self::expectErrorMessage("Can't create User - No organism found for acronyme: ");
        self::expectException(OrganismeNotFoundException::class);

        $samlUserFactory->createUser($username, $mappedAttributes);
    }

    /**
     * @throws Exception
     */
    public function testCreateUserThrowExceptionWhenOrganismNotFoundInParameter(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $habilitationService = $this->createMock(Habilitation::class);

        $username = 'saml_user_login';

        $mappedAttributes = [
            'email' => ['saml_user@email.fr'],
            'lastName' => ['nom'],
            'firstName' => ['prenom'],
        ];

        $organismeAcronyme = '';
        $profileId = '1';
        $serviceId = '2';

        $errorMsg = "Error - No organisme acronym found in parameter `OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME`";

        $logger
            ->expects(self::once())
            ->method('info')
            ->with("Begin creation User saml_user_login via SamlUserFactory")
        ;

        $parameterBag
            ->expects(self::once())
            ->method('get')
            ->with("OAUTH_KEYCLOAK_SAML2_REFERENCE_ORGANISME")
            ->willReturn($organismeAcronyme)
        ;

        $organismeRepository = $this->createMock(OrganismeRepository::class);
        $organismeRepository
            ->expects(self::never())
            ->method('findOneBy')
        ;

        $logger
            ->expects(self::once())
            ->method('error')
            ->with($errorMsg)
        ;

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $serviceRepository
            ->expects(self::never())
            ->method('findOneBy')
        ;

        $em
            ->expects(self::never())
            ->method('getRepository')
        ;

        $habilitationService
            ->expects(self::never())
            ->method('createHabilitationsAgent')
            ->with($username, $profileId)
        ;

        $samlUserFactory = new SamlUserFactory($em, $logger, $parameterBag, $habilitationService);

        self::expectErrorMessage($errorMsg);
        self::expectException(OrganismeNotFoundException::class);

        $samlUserFactory->createUser($username, $mappedAttributes);
    }
}
