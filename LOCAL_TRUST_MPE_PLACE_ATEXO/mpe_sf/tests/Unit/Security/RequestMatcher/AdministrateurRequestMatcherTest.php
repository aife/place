<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\RequestMatcher;

use App\Security\GuardManager;
use App\Security\RequestMatcher\AdministrateurRequestMatcher;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class AdministrateurRequestMatcherTest extends TestCase
{
    public function matchesDataProvider(): array
    {
        return [
            ['/', [], false],
            ['/admin', [], true],
            ['/agent', [], false],
            ['/entreprise', [], false],
            ['/', ['page' => 'Administration.Anything'], true],
            ['/', ['page' => 'Agent.Anything'], false],
            ['/', ['page' => 'Entreprise.Anything'], false],
            ['/login/microsoft/check', ['user_type' => 'agent'], false],
            ['/login/microsoft/check', [], false],
            ['/login/microsoft/check', ['user_type' => 'inscrit'], false],
        ];
    }

    /**
     * @dataProvider matchesDataProvider
     * @param string $path
     * @param array $parameters
     * @param bool $expected
     */
    public function testMatches(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);

        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $matcher = new AdministrateurRequestMatcher($guardManager);
        $result = $matcher->matches($request);

        $this->assertEquals($expected, $result);
    }
}
