<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\RequestMatcher;

use App\Entity\ConfigurationPlateforme;
use App\Repository\ConfigurationPlateformeRepository;
use App\Security\GuardManager;
use App\Security\RequestMatcher\AgentSamlRequestMatcher;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class AgentSamlRequestMatcherTest extends TestCase
{
    public function matchesDataProvider(): array
    {
        return [
            ['/', [], true, false],
            ['/agent', [], true, true],
            ['/agent', [], false, false],
            ['/saml/acs', [], true, true],
            ['/saml/acs', [], false, false],
            ['/admin', [], true, false],
            ['/entreprise', [], true, false],
            ['/', ['page' => 'Agent.Anything'], true, true],
            ['/', ['page' => 'Agent.Anything'], false, false],
            ['/', ['page' => 'Administration.Anything'], true, false],
            ['/', ['page' => 'Entreprise.Anything'], true, false],
        ];
    }

    /**
     * @dataProvider matchesDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $isModuleEnabled
     * @param bool $expected
     */
    public function testMatches(string $uri, array $parameters, bool $isModuleEnabled, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);

        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );

        $configurationPlateforme = new ConfigurationPlateforme();
        if ($isModuleEnabled) {
            $configurationPlateforme->setAuthenticateAgentSaml('1');
        }

        $repositoryMock = $this->createMock(ConfigurationPlateformeRepository::class);
        $repositoryMock->method('getConfigurationPlateforme')->willReturn($configurationPlateforme);

        $em = $this->createMock(EntityManagerInterface::class);
        $em->method('getRepository')->willReturn($repositoryMock);

        $matcher = new AgentSamlRequestMatcher($guardManager, $em);
        $result = $matcher->matches($request);

        $this->assertEquals($expected, $result);
    }
}
