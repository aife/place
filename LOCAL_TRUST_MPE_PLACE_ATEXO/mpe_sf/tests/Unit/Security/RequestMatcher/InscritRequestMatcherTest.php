<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\RequestMatcher;

use App\Security\GuardManager;
use App\Security\OpenidKeycloakAuthenticator;
use App\Security\RequestMatcher\InscritRequestMatcher;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class InscritRequestMatcherTest extends TestCase
{
    public function matchesDataProvider(): array
    {
        return [
            ['/', [], false],
            ['/entreprise', [], true],
            ['/agent', [], false],
            ['/admin', [], false],
            ['/', ['page' => 'Entreprise.Anything'], true],
            ['/', ['page' => 'Agent.Anything'], false],
            ['/', ['page' => 'Administration.Anything'], false],
            ['/login/microsoft/check', ['user_type' => 'inscrit'], true],
            ['/login/microsoft/check', [], false],
            ['/login/microsoft/check', ['user_type' => 'agent'], false],
        ];
    }

    /**
     * @dataProvider matchesDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $expected
     */
    public function testMatches(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);

        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $openidKeycloakAuthenticator = $this->createMock(OpenidKeycloakAuthenticator::class);

        $matcher = new InscritRequestMatcher($guardManager, $openidKeycloakAuthenticator);
        $result = $matcher->matches($request);

        $this->assertEquals($expected, $result);
    }
}
