<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Checker;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Security\Checker\UserChecker;
use App\Security\Exception\AccountDisabledException;
use App\Service\MpeSecurity;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\User\UserInterface;

class UserCheckerTest extends TestCase
{
    /**
     * @var UserChecker
     */
    private $userChecker;

    /**
     * @var MpeSecurity|MockObject
     */
    private $mpeSecurityMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->mpeSecurityMock = $this->createMock(MpeSecurity::class);
        $this->userChecker = new UserChecker($this->mpeSecurityMock);
    }

    protected function prepareSecurityAndGetUser(bool $securityReturnValue): UserInterface
    {
        $userMock = $this->createMock(UserInterface::class);

        $this->mpeSecurityMock
            ->expects($this->once())
            ->method('isUserActive')
            ->willReturn($securityReturnValue);

        return $userMock;
    }

    public function testCheckPreAuthActiveUser()
    {
        $userMock = $this->prepareSecurityAndGetUser(true);

        $this->userChecker->checkPreAuth($userMock);
    }

    public function testCheckPreAuthInactiveUser()
    {
        $userMock = $this->prepareSecurityAndGetUser(false);

        $this->expectException(AccountDisabledException::class);

        $this->userChecker->checkPreAuth($userMock);
    }

    public function checkPostAuthDataProvider(): array
    {
        return [
            [new Agent(), true],
            [new Inscrit(), true],
            [new Administrateur(), false],
        ];
    }

    /**
     * @dataProvider checkPostAuthDataProvider
     * @param UserInterface $user
     * @param bool $shouldBeCalled
     */
    public function testCheckPostAuth(UserInterface $user, bool $shouldBeCalled)
    {
        $this->mpeSecurityMock
            ->expects($shouldBeCalled ? $this->once() : $this->never())
            ->method('resetTentativesMdp');
        $this->mpeSecurityMock
            ->expects($shouldBeCalled ? $this->once() : $this->never())
            ->method('activerSynchronisationSgmap');

        $this->userChecker->checkPostAuth($user);
    }
}
