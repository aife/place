<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Voter;

use App\Entity\MediaUuid;
use App\Security\Voter\MediaVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class MediaVoterTest extends TestCase
{
    public function testCanDownloadFile()
    {
        $token = $this->createMock(TokenInterface::class);
        $voter = new MediaVoter();
        $media = new MediaUuid();
        $media->setCreatedAt(new \DateTime());
        $canDownload = $voter->vote($token, $media, ['MEDIA_VIEW']);

        $this->assertEquals(1, $canDownload);
    }

    public function testCantDownloadFile()
    {
        $token = $this->createMock(TokenInterface::class);
        $voter = new MediaVoter();
        $media = new MediaUuid();
        $media->setCreatedAt(new \DateTime('- 1 day'));
        $canDownload = $voter->vote($token, $media, ['MEDIA_VIEW']);

        $this->assertEquals(-1, $canDownload);
    }
}
