<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Voter;

use App\Entity\Consultation;
use App\Security\Voter\ConsultationSimplifieeVoter;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\CurrentUser;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ConsultationSimplifieeVoterTest extends TestCase
{
    public $userMock;
    public $authorizationAgentMock;
    public $tokenMock;

    public function setUp(): void
    {
        $this->userMock = $this->createMock(CurrentUser::class);
        $this->authorizationAgentMock = $this->createMock(AuthorizationAgent::class);
        $this->tokenMock = $this->createMock(TokenInterface::class);
    }

    /**
     * @dataProvider providerCreateOne
     *
     * @param $attribute
     * @param $subject
     * @param $expected
     * @param $habilitation
     * @param $check
     */
    public function testVoteCreateWithAgentConnected($attribute, $subject, $expected, $habilitation, $check)
    {
        $this->userMock->method('isAgent')->willReturn(true);
        $this->userMock->method('checkHabilitation')->willReturnCallback(function ($param) use ($habilitation, $check) {
            if ($param === $habilitation) {
                return $check;
            }

            return false;
        });
        $consultationSimplifiee = new ConsultationSimplifieeVoter($this->userMock, $this->authorizationAgentMock);
        $this->assertEquals($expected, $consultationSimplifiee->vote($this->tokenMock, $subject, [$attribute]));
    }

    /**
     * @dataProvider providerCreateTwo
     *
     * @param $attribute
     * @param $subject
     * @param $expected
     * @param $habilitation
     * @param $check
     */
    public function testVoteCreateWithAgentNotConnected($attribute, $subject, $expected, $habilitation, $check)
    {
        $this->userMock->method('isAgent')->willReturn(false);
        $this->userMock->method('checkHabilitation')->willReturnCallback(function ($param) use ($habilitation, $check) {
            if ($param === $habilitation) {
                return $check;
            }

            return false;
        });
        $consultationSimplifiee = new ConsultationSimplifieeVoter($this->userMock, $this->authorizationAgentMock);
        $this->assertEquals($expected, $consultationSimplifiee->vote($this->tokenMock, $subject, [$attribute]));
    }

    /**
     * @dataProvider providerEditOne
     *
     * @param $attribute
     * @param $subject
     * @param $expected
     * @param $habilitation
     * @param $check
     */
    public function testVoteEditWithAgentConnectedAndAuthorized($attribute, $subject, $expected, $habilitation, $check)
    {
        $this->userMock = $this->createMock(CurrentUser::class);
        $this->userMock->method('isAgent')->willReturn(true);
        $this->userMock->method('checkHabilitation')->willReturnCallback(function ($param) use ($habilitation, $check) {
            if ($param === $habilitation) {
                return $check;
            }

            return false;
        });

        $this->authorizationAgentMock->method('isAuthorized')->willReturn(true);
        $consultationSimplifiee = new ConsultationSimplifieeVoter($this->userMock, $this->authorizationAgentMock);
        $this->assertEquals($expected, $consultationSimplifiee->vote($this->tokenMock, $subject, [$attribute]));
    }

    /**
     * @dataProvider providerEditTwo
     *
     * @param $attribute
     * @param $subject
     * @param $expected
     * @param $habilitation
     * @param $check
     */
    public function testVoteEditWithAgentNotConnectedAndAuthorized(
        $attribute,
        $subject,
        $expected,
        $habilitation,
        $check
    ) {
        $this->userMock->method('isAgent')->willReturn(false);
        $this->userMock->method('checkHabilitation')->willReturnCallback(function ($param) use ($habilitation, $check) {
            if ($param === $habilitation) {
                return $check;
            }

            return false;
        });
        $this->authorizationAgentMock->method('isAuthorized')->willReturn(true);
        $consultationSimplifiee = new ConsultationSimplifieeVoter($this->userMock, $this->authorizationAgentMock);
        $this->assertEquals($expected, $consultationSimplifiee->vote($this->tokenMock, $subject, [$attribute]));
    }

    /**
     * @dataProvider providerEditTwo
     *
     * @param $attribute
     * @param $subject
     * @param $expected
     * @param $habilitation
     * @param $check
     */
    public function testVoteEditWithAgentConnectedAndNotAuthorized(
        $attribute,
        $subject,
        $expected,
        $habilitation,
        $check
    ) {
        $this->userMock->method('isAgent')->willReturn(true);
        $this->userMock->method('checkHabilitation')->willReturnCallback(function ($param) use ($habilitation, $check) {
            if ($param === $habilitation) {
                return $check;
            }

            return false;
        });
        $this->authorizationAgentMock->method('isAuthorized')->willReturn(false);
        $consultationSimplifiee = new ConsultationSimplifieeVoter($this->userMock, $this->authorizationAgentMock);
        $this->assertEquals($expected, $consultationSimplifiee->vote($this->tokenMock, $subject, [$attribute]));
    }

    public function providerCreateOne(): array
    {
        $consultation = $this->getFakeConsultation();

        return [
            ['', null, 0, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', null, 1, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', null, 1, 'gererMapaSuperieurMontant', true],
            ['CS_CREATE', null, 1, 'administrerProcedure', true],
            ['CS_CREATE', null, -1, 'xxxxxxxx', false],
            ['CS_CREATE', null, -1, 'xxxxxxxx', true],
            ['CS_CREATE', $consultation, -1, 'administrerProcedure', false],
            ['CS_CREATE', $consultation, -1, 'xxxxx', true],
            ['CS_CREATE', $consultation, 1, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', $consultation, 1, 'gererMapaSuperieurMontant', true],
            ['CS_CREATE', $consultation, 1, 'administrerProcedure', true],
        ];
    }

    public function providerCreateTwo(): array
    {
        $consultation = $this->getFakeConsultation();

        return [
            ['', null, 0, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', null, -1, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', null, -1,  'gererMapaSuperieurMontant', true],
            ['CS_CREATE', null, -1, 'administrerProcedure', true],
            ['CS_CREATE', null, -1, 'xxxxxxxx', false],
            ['CS_CREATE', null, -1, 'xxxxxxxx', true],
            ['CS_CREATE', $consultation, -1, 'administrerProcedure', false],
            ['CS_CREATE', $consultation, -1, 'xxxxx', true],
            ['CS_CREATE', $consultation, -1, 'gererMapaInferieurMontant', true],
            ['CS_CREATE', $consultation, -1, 'gererMapaSuperieurMontant', true],
            ['CS_CREATE', $consultation, -1, 'administrerProcedure', true],
        ];
    }

    public function providerEditOne(): array
    {
        $consultation = $this->getFakeConsultation();

        return [
            ['CS_EDIT', null, 0, 'gererMapaInferieurMontant', true],
            ['CS_EDIT', null, 0, 'gererMapaSuperieurMontant', true],
            ['CS_EDIT', null, 0, 'administrerProcedure', true],
            ['CS_EDIT', null, 0, 'xxxxxxxx', false],
            ['CS_EDIT', null, 0, 'xxxxxxxx', true],
            ['CS_EDIT', $consultation, -1, 'administrerProcedure', false],
            ['CS_EDIT', $consultation, -1, 'xxxxx', true],
            ['CS_EDIT', $consultation, 1, 'gererMapaInferieurMontant', true],
            ['CS_EDIT', $consultation, 1, 'gererMapaSuperieurMontant', true],
            ['CS_EDIT', $consultation, 1, 'administrerProcedure', true],
        ];
    }

    public function providerEditTwo(): array
    {
        $consultation = $this->getFakeConsultation();

        return [
            ['CS_EDIT', null, 0, 'gererMapaInferieurMontant', true],
            ['CS_EDIT', null, 0, 'gererMapaSuperieurMontant', true],
            ['CS_EDIT', null, 0, 'administrerProcedure', true],
            ['CS_EDIT', null, 0, 'xxxxxxxx', false],
            ['CS_EDIT', null, 0, 'xxxxxxxx', true],
            ['CS_EDIT', $consultation, -1, 'administrerProcedure', false],
            ['CS_EDIT', $consultation, -1, 'xxxxx', true],
            ['CS_EDIT', $consultation, -1, 'gererMapaInferieurMontant', true],
            ['CS_EDIT', $consultation, -1, 'gererMapaSuperieurMontant', true],
            ['CS_EDIT', $consultation, -1, 'administrerProcedure', true],
        ];
    }

    public function getFakeConsultation()
    {
        return new class extends Consultation {
            public function getId()
            {
                return 1;
            }
        };
    }
}
