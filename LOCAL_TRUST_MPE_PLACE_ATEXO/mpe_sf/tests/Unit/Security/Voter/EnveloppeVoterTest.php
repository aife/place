<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Voter;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Enveloppe;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Security\Voter\EnveloppeVoter;
use App\Service\Authorization\AuthorizationAgent;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class EnveloppeVoterTest extends TestCase
{
    public function testCanCreateEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $agent = new Agent();
        $token->method('getUser')->willReturn($agent);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $consultation = new Consultation();
        $consultation->setDateMiseEnLigneCalcule(new \DateTime());
        $consultation->setDateFinUnix(time() + 1000);
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canCreate = $voter->vote($token, $enveloppe, ['CREATE_ENVELOPPE']);

        $this->assertEquals(1, $canCreate);
    }

    public function testCantCreateEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $agent = new Agent();
        $token->method('getUser')->willReturn($agent);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $consultation = new Consultation();
        $consultation->setDateMiseEnLigneCalcule(new \DateTime());
        $consultation->setDateFinUnix(time() - 1000);
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canCreate = $voter->vote($token, $enveloppe, ['CREATE_ENVELOPPE']);

        $this->assertEquals(-1, $canCreate);
    }

    public function testAgentCanViewEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $agent = new Agent();
        $token->method('getUser')->willReturn($agent);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $authAgent->method('isInTheScope')->willReturn(true);
        $parameterBag->method('get')->willReturn(2);
        $consultation = new Consultation();
        $consultation->setId(5);
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setStatutEnveloppe(2);
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canView = $voter->vote($token, $enveloppe, ['VIEW_ENVELOPPE']);

        $this->assertEquals(1, $canView);
    }

    public function testAgentCantViewEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $agent = new Agent();
        $token->method('getUser')->willReturn($agent);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $authAgent->method('isInTheScope')->willReturn(false);
        $parameterBag->method('get')->willReturn(2);
        $consultation = new Consultation();
        $consultation->setId(5);
        $offre = new Offre();
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setStatutEnveloppe(2);
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canView = $voter->vote($token, $enveloppe, ['VIEW_ENVELOPPE']);

        $this->assertEquals(-1, $canView);
    }

    public function testInscritCanViewEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $inscrit = new Inscrit();
        $entreprise = new Entreprise();
        $entreprise->setId(5);
        $inscrit->setEntreprise($entreprise);
        $token->method('getUser')->willReturn($inscrit);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $parameterBag->method('get')->willReturn(2);
        $consultation = new Consultation();
        $consultation->setId(5);
        $offre = new Offre();
        $offre->setEntrepriseId(5);
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setStatutEnveloppe(2);
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canView = $voter->vote($token, $enveloppe, ['VIEW_ENVELOPPE']);

        $this->assertEquals(1, $canView);
    }

    public function testInscritCantViewEnveloppe()
    {
        $token = $this->createMock(TokenInterface::class);
        $inscrit = new Inscrit();
        $entreprise = new Entreprise();
        $entreprise->setId(45);
        $inscrit->setEntreprise($entreprise);
        $token->method('getUser')->willReturn($inscrit);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $authAgent = $this->createMock(AuthorizationAgent::class);
        $parameterBag->method('get')->willReturn(2);
        $consultation = new Consultation();
        $consultation->setId(5);
        $offre = new Offre();
        $offre->setEntrepriseId(5);
        $offre->setConsultation($consultation);
        $enveloppe = new Enveloppe();
        $enveloppe->setStatutEnveloppe(2);
        $enveloppe->setOffre($offre);
        $voter = new EnveloppeVoter($parameterBag, $authAgent);
        $canView = $voter->vote($token, $enveloppe, ['VIEW_ENVELOPPE']);

        $this->assertEquals(-1, $canView);
    }
}
