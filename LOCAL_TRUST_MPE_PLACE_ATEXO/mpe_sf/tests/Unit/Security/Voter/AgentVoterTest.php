<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Voter;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Security\Voter\AgentVoter;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;

class AgentVoterTest extends TestCase
{
    public function testAgentTechnique()
    {
        $token = $this->createMock(TokenInterface::class);
        $security = $this->createMock(Security::class);
        $agent = new Agent();
        $security->method('getUser')->willReturn($agent);
        $subject = new Agent();
        $agentVoter = new AgentVoter($security);
        $access = $agentVoter->vote($token, $subject, ['AGENT_EDIT']);

        $this->assertEquals(1, $access);
    }

    public function testAgentCreateAgentNotInOwnerService()
    {
        $token = $this->createMock(TokenInterface::class);
        $security = $this->createMock(Security::class);
        $agent = new Agent();
        $agent->setService((new Service())->setId(3));
        $agent->setOrganisme((new Organisme())->setAcronyme('pmi'));
        $security->method('getUser')->willReturn($agent);
        $subject = new Agent();
        $subject->setService((new Service())->setId(1));
        $subject->setOrganisme((new Organisme())->setAcronyme('pmi'));
        $agentVoter = new AgentVoter($security);
        $access = $agentVoter->vote($token, $subject, ['AGENT_EDIT']);

        $this->assertEquals(-1, $access);
    }

    public function testAgentCreateAgentInOwnerService()
    {
        $token = $this->createMock(TokenInterface::class);
        $security = $this->createMock(Security::class);
        $agent = new Agent();
        $service = (new Service())->setId(3);
        $agent->setService($service);
        $agent->setOrganisme((new Organisme())->setAcronyme('pmi'));
        $security->method('getUser')->willReturn($agent);
        $subject = new Agent();
        $subject->setService($service);
        $subject->setOrganisme((new Organisme())->setAcronyme('pmi'));
        $agentVoter = new AgentVoter($security);
        $access = $agentVoter->vote($token, $subject, ['AGENT_EDIT']);

        $this->assertEquals(1, $access);
    }

    public function testAgentCreateAgentInNotOwnerOrganisme()
    {
        $token = $this->createMock(TokenInterface::class);
        $security = $this->createMock(Security::class);
        $agent = new Agent();
        $agent->setOrganisme((new Organisme())->setAcronyme('u6j'));
        $security->method('getUser')->willReturn($agent);
        $subject = new Agent();
        $subject->setService((new Service())->setId(1));
        $subject->setOrganisme((new Organisme())->setAcronyme('pmi'));
        $agentVoter = new AgentVoter($security);
        $access = $agentVoter->vote($token, $subject, ['AGENT_EDIT']);

        $this->assertEquals(-1, $access);
    }

    public function testAgentCreateAgentInOwnerOrganisme()
    {
        $token = $this->createMock(TokenInterface::class);
        $security = $this->createMock(Security::class);
        $agent = new Agent();
        $organisme = (new Organisme())->setAcronyme('pmi');
        $agent->setOrganisme($organisme);
        $security->method('getUser')->willReturn($agent);
        $subject = new Agent();
        $subject->setService((new Service())->setId(1));
        $subject->setOrganisme($organisme);
        $agentVoter = new AgentVoter($security);
        $access = $agentVoter->vote($token, $subject, ['AGENT_EDIT']);

        $this->assertEquals(1, $access);
    }
}
