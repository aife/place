<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security;

use App\Security\ClientOpenIdConnect;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ClientOpenIdConnectTest extends TestCase
{
    public function testLogIn()
    {
        $accessToken = 'tested_access_token';

        $response = $this->createMock(ResponseInterface::class);
        $response->method('toArray')->willReturn(
            [
                'access_token' => $accessToken,
                'refresh_token' => 'tested_refresh_token',
            ]
        );

        $httpClientMock = $this->createMock(HttpClientInterface::class);
        $httpClientMock->expects($this->once())->method('request')->willReturn($response);

        $openIdConnect = new ClientOpenIdConnect($httpClientMock);

        $openIdConnect->logIn('/', [], []);
        $result = $openIdConnect->getAccessToken();

        $this->assertEquals($accessToken, $result);
    }
}
