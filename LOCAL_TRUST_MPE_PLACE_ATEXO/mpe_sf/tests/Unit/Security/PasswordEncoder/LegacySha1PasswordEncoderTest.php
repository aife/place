<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\PasswordEncoder;

use App\Security\PasswordEncoder\LegacySha1PasswordEncoder;
use PHPUnit\Framework\TestCase;

class LegacySha1PasswordEncoderTest extends TestCase
{
    public function encodePasswordDataProvider(): array
    {
        return [
            ['weakPwd', '865c865269ce17ec1949e6d2b0bd42b3ba3b334a'],
            ['anotherPwd', '8a161c40146dad1f52f37f5693057b1a8e92c50f'],
        ];
    }

    /**
     * @dataProvider encodePasswordDataProvider
     * @param string $plainPassword
     * @param string $expected
     */
    public function testEncodePassword(string $plainPassword, string $expected)
    {
        $encoder = new LegacySha1PasswordEncoder();
        $this->assertEquals($expected, $encoder->encodePassword($plainPassword, null));
    }

    public function testIsPasswordValid()
    {
        $encoder = new LegacySha1PasswordEncoder();
        $this->assertTrue($encoder->isPasswordValid('865c865269ce17ec1949e6d2b0bd42b3ba3b334a', 'weakPwd', null));
        $this->assertTrue($encoder->isPasswordValid('8a161c40146dad1f52f37f5693057b1a8e92c50f', 'anotherPwd', null));
        $this->assertFalse($encoder->isPasswordValid('5b451c40146dad1f52f37f5693057b1a8e92c50f', 'anotherPwd', null));
        $this->assertFalse($encoder->isPasswordValid('anotherPwd', 'anotherPwd', null));
    }

    public function testNeedsRehash()
    {
        $encoder = new LegacySha1PasswordEncoder();
        $this->assertTrue($encoder->needsRehash('whatever'));
        $this->assertTrue($encoder->needsRehash('dfdfdfd'));
    }
}
