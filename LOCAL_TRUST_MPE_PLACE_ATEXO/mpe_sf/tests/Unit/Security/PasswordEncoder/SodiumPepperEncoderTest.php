<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\PasswordEncoder;

use App\Entity\Agent;
use App\Security\PasswordEncoder\LegacySha1PasswordEncoder;
use App\Security\PasswordEncoder\LegacySha256PasswordEncoder;
use App\Security\PasswordEncoder\SodiumPepperEncoder;
use App\Service\PradoPasswordEncoder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Encoder\SodiumPasswordEncoder;

class SodiumPepperEncoderTest  extends TestCase
{
    public function testEncodePassword()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $encodedPassword = $sodiumEncoder->encodePassword('pass', null);

        $this->assertStringContainsString('arg', $encodedPassword);
    }

    public function testIsPasswordValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $encodedPassword = $sodiumEncoder->encodePassword('pass', null);
        $isSamePassword = $sodiumEncoder->isPasswordValid($encodedPassword, 'pass', null);

        $this->assertTrue($isSamePassword);
    }

    public function testIsNotPasswordValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $encodedPassword = $sodiumEncoder->encodePassword('pass', null);
        $isSamePassword = $sodiumEncoder->isPasswordValid($encodedPassword, 'notSamePass', null);

        $this->assertFalse($isSamePassword);
    }

    public function testIsPasswordValidWithInvalidPepper()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $pepper = 'M%{h6';
        $encodedPassword = $sodiumEncoder->encodePassword('pass', $pepper);
        $isSamePassword = $sodiumEncoder->isPasswordValid($encodedPassword, 'pass', null);

        $this->assertFalse($isSamePassword);
    }

    public function testIsPasswordValidWithValidPepper()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $pepper = 'M%{h6';
        $encodedPassword = $sodiumEncoder->encodePassword('pass', $pepper);
        $isSamePassword = $sodiumEncoder->isPasswordValid($encodedPassword, 'pass' . $pepper, null);

        $this->assertTrue($isSamePassword);
    }

    public function testIfLegacySha1PasswordIsValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $ecodedSha1 = 'd033e22ae348aeb5660fc2140aec35850c4da997';
        $isValid = $sodiumEncoder->isPasswordValid($ecodedSha1, 'admin', null);

        $this->assertTrue($isValid);
    }

    public function testIfLegacySha1PasswordIsNotValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $ecodedSha1 = 'd033e22ae348aeb5660fc2140aec35850c4da997';
        $isValid = $sodiumEncoder->isPasswordValid($ecodedSha1, 'passNotValid', null);

        $this->assertFalse($isValid);
    }

    public function testIfLegacySha256PasswordIsValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $ecodedSha256 = 'a2934f6fd25151895df9c913d22c470c1902f2aefb88189dab7e83e57835a939';
        $isValid = $sodiumEncoder->isPasswordValid($ecodedSha256, 'admin', null);

        $this->assertTrue($isValid);
    }

    public function testIfLegacySha256PasswordIsNotValid()
    {
        $sodiumEncoder = $this->getSodiumPepperEncoder();
        $ecodedSha256 = 'a2934f6fd25151895df9c913d22c470c1902f2aefb88189dab7e83e57835a939';
        $isValid = $sodiumEncoder->isPasswordValid($ecodedSha256, 'passNotValid', null);

        $this->assertFalse($isValid);
    }

    private function getSodiumPepperEncoder(): SodiumPepperEncoder
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);

        return new SodiumPepperEncoder(
            $parameterBag,
            new LegacySha1PasswordEncoder(),
            new LegacySha256PasswordEncoder('true', 'a269BkoILM@6973AZuytrF!Psxe?8sqO')
        );
    }

}