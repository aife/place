<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\PasswordEncoder;

use App\Security\PasswordEncoder\LegacySha256PasswordEncoder;
use PHPUnit\Framework\TestCase;

class LegacySha256PasswordEncoderTest extends TestCase
{
    public function encodePasswordDataProvider(): array
    {
        return [
            ['weakPwd', '', '44c9500fdd320cfe1ecaf302f6fc595bb52871eae22aabd02b415371ef38b641'],
            ['anotherPwd', '', 'cfd6b1b251b28dd1857eb729919b101c88b8e3196d72ed4906dda9a226b79bb7'],
            ['anotherPwd', 'withsalt!', 'aa3cc53470e681818ada86fdd0fe21e16494007f6b0f3d8eee843b27ad44340a'],
        ];
    }

    /**
     * @dataProvider encodePasswordDataProvider
     * @param string $plainPassword
     * @param string $salt
     * @param string $expected
     */
    public function testEncodePassword(string $plainPassword, string $salt, string $expected)
    {
        $encoder = new LegacySha256PasswordEncoder();
        $this->assertEquals($expected, $encoder->encodePassword($plainPassword, $salt));

        $encoder = new LegacySha256PasswordEncoder('1', $salt);
        $this->assertEquals($expected, $encoder->encodePassword($plainPassword, ''));
    }

    public function testIsPasswordValid()
    {
        $encoder = new LegacySha256PasswordEncoder();
        $this->assertTrue(
            $encoder->isPasswordValid(
                '44c9500fdd320cfe1ecaf302f6fc595bb52871eae22aabd02b415371ef38b641',
                'weakPwd',
                null
            )
        );
        $this->assertTrue(
            $encoder->isPasswordValid(
                'cfd6b1b251b28dd1857eb729919b101c88b8e3196d72ed4906dda9a226b79bb7',
                'anotherPwd',
                null
            )
        );
        $this->assertTrue(
            $encoder->isPasswordValid(
                'aa3cc53470e681818ada86fdd0fe21e16494007f6b0f3d8eee843b27ad44340a',
                'anotherPwd',
                'withsalt!'
            )
        );
        $this->assertFalse($encoder->isPasswordValid('wrongencodedvalue', 'anotherPwd', null));

        $encoder = new LegacySha256PasswordEncoder('1', 'withsalt!');
        $this->assertTrue(
            $encoder->isPasswordValid(
                'aa3cc53470e681818ada86fdd0fe21e16494007f6b0f3d8eee843b27ad44340a',
                'anotherPwd',
                null
            )
        );

        $encoder = new LegacySha256PasswordEncoder('0');
        $this->assertFalse(
            $encoder->isPasswordValid(
                '44c9500fdd320cfe1ecaf302f6fc595bb52871eae22aabd02b415371ef38b641',
                'weakPwd',
                null
            )
        );
        $this->assertFalse(
            $encoder->isPasswordValid(
                'aa3cc53470e681818ada86fdd0fe21e16494007f6b0f3d8eee843b27ad44340a',
                'anotherPwd',
                'withsalt!'
            )
        );
    }

    public function testNeedsRehash()
    {
        $encoder = new LegacySha256PasswordEncoder();
        $this->assertTrue($encoder->needsRehash('whatever'));
    }
}
