<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\PasswordEncoder;

use App\Entity\Agent;
use App\Security\PasswordEncoder\CommonPasswordEncoder;
use App\Security\PasswordEncoder\LegacyPlainTextPasswordEncoder;
use App\Security\PasswordEncoder\LegacySha1PasswordEncoder;
use App\Security\PasswordEncoder\LegacySha256PasswordEncoder;
use PHPUnit\Framework\TestCase;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;
use Symfony\Component\PasswordHasher\Hasher\SodiumPasswordHasher;
use Symfony\Component\PasswordHasher\PasswordHasherInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\SodiumPasswordEncoder;

class CommonPasswordEncoderTest extends TestCase
{
    public function testEncodePassword(): void
    {
        $encoderFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $encoderFactory
            ->expects(self::once())
            ->method('getPasswordHasher')
            ->willReturn(new SodiumPasswordHasher());

        $pradoEncoder = new CommonPasswordEncoder($encoderFactory);

        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');

        self::assertStringContainsString('arg', $encodedPassword);
    }

    /**
     * @dataProvider getShaPassword
     */
    public function testEncodePasswordSha(PasswordHasherInterface $encoderType, string $shaPassword): void
    {
        $encoderFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $encoderFactory
            ->expects(self::once())
            ->method('getPasswordHasher')
            ->willReturn($encoderType);

        $pradoEncoder = new CommonPasswordEncoder($encoderFactory);

        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');

        self::assertSame($shaPassword, $encodedPassword);
    }

    /**
     * @dataProvider encoderType
     */
    public function testIsPasswordValid(PasswordHasherInterface $encoderType): void
    {
        $encoderFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $encoderFactory
            ->expects(self::exactly(2))
            ->method('getPasswordHasher')
            ->willReturn($encoderType)
        ;

        $pradoEncoder = new CommonPasswordEncoder($encoderFactory);

        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');
        $isSamePassword = $pradoEncoder->isPasswordValid(Agent::class, $encodedPassword, 'pass');

        self::assertTrue($isSamePassword);
    }

    /**
     * @dataProvider encoderType
     */
    public function testIsPasswordNotValid(PasswordHasherInterface $encoderType): void
    {
        $encoderFactory = $this->createMock(PasswordHasherFactoryInterface::class);
        $encoderFactory
            ->expects(self::exactly(2))
            ->method('getPasswordHasher')
            ->willReturn($encoderType)
        ;

        $pradoEncoder = new CommonPasswordEncoder($encoderFactory);

        $encodedPassword = $pradoEncoder->encodePassword(Agent::class, 'pass');
        $isSamePassword = $pradoEncoder->isPasswordValid(Agent::class, $encodedPassword, 'aaa');

        self::assertFalse($isSamePassword);
    }

    public function encoderType(): array
    {
        return [
            "Sodium" => [new SodiumPasswordHasher()],
            "Sha1 (legacy)" => [new LegacySha1PasswordEncoder()],
            "Sha256 (legacy)" => [new LegacySha256PasswordEncoder()],
            "Plain text (legacy)" => [new LegacyPlainTextPasswordEncoder()],
        ];
    }

    public function getShaPassword(): array
    {
        return [
            "Sha1" => [new LegacySha1PasswordEncoder(), sha1('pass')],
            "Sha256" => [new LegacySha256PasswordEncoder(), hash('sha256', 'pass')],
            "Plain text (legacy)" => [new LegacyPlainTextPasswordEncoder(), 'pass'],
        ];
    }
}
