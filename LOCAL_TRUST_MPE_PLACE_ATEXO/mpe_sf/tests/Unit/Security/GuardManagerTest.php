<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security;

use App\Entity\ConfigurationPlateforme;
use App\Repository\ConfigurationPlateformeRepository;
use App\Security\ExternalSsoAuthenticator;
use App\Security\FormAuthenticator;
use App\Security\GuardManager;
use App\Security\Logout\Handler;
use App\Security\OpenidKeycloakAuthenticator;
use App\Security\OpenidMicrosoftAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class GuardManagerTest extends TestCase
{
    public function isGuardEnabledDataProvider(): array
    {
        return [
            ['inscrit', FormAuthenticator::class, '1', '0', true],
            ['inscrit', FormAuthenticator::class, '0', '0', false],
            ['agent', FormAuthenticator::class, '0', '1', true],
            ['agent', FormAuthenticator::class, '0', '0', false],
            ['administrateur', FormAuthenticator::class, '0', '0', true],
            ['superadmin', FormAuthenticator::class, '0', '0', true],

            ['inscrit', OpenidKeycloakAuthenticator::class, '1', '0', true],
            ['inscrit', OpenidKeycloakAuthenticator::class, '0', '0', false],
            ['agent', OpenidKeycloakAuthenticator::class, '0', '1', true],
            ['agent', OpenidKeycloakAuthenticator::class, '0', '0', false],
            ['administrateur', OpenidKeycloakAuthenticator::class, '0', '0', false],
            ['administrateur', OpenidKeycloakAuthenticator::class, '1', '1', false],
            ['administrateur', OpenidKeycloakAuthenticator::class, '1', '0', false],
            ['superadmin', OpenidKeycloakAuthenticator::class, '0', '0', false],
            ['superadmin', OpenidKeycloakAuthenticator::class, '1', '1', false],
            ['superadmin', OpenidKeycloakAuthenticator::class, '1', '0', false],

            ['inscrit', OpenidMicrosoftAuthenticator::class, '1', '0', true],
            ['inscrit', OpenidMicrosoftAuthenticator::class, '0', '0', false],
            ['agent', OpenidMicrosoftAuthenticator::class, '0', '1', true],
            ['agent', OpenidMicrosoftAuthenticator::class, '0', '0', false],
            ['administrateur', OpenidMicrosoftAuthenticator::class, '0', '0', false],
            ['administrateur', OpenidMicrosoftAuthenticator::class, '1', '1', false],
            ['administrateur', OpenidMicrosoftAuthenticator::class, '1', '0', false],
            ['superadmin', OpenidMicrosoftAuthenticator::class, '0', '0', false],
            ['superadmin', OpenidMicrosoftAuthenticator::class, '1', '1', false],
            ['superadmin', OpenidMicrosoftAuthenticator::class, '1', '0', false],

            ['agent', ExternalSsoAuthenticator::class, '0', '1', true],
            ['agent', ExternalSsoAuthenticator::class, '0', '0', false],
            ['inscrit', ExternalSsoAuthenticator::class, '1', '0', true],
            ['inscrit', ExternalSsoAuthenticator::class, '0', '0', false],
            ['administrateur', ExternalSsoAuthenticator::class, '0', '0', false],
            ['superadmin', ExternalSsoAuthenticator::class, '0', '0', false],
        ];
    }

    /**
     * @dataProvider isGuardEnabledDataProvider
     * @param string $userType
     * @param string $guardClass
     * @param string $authInscrit
     * @param string $authAgent
     * @param bool $expected
     */
    public function testIsGuardEnabled(
        string $userType,
        string $guardClass,
        string $authInscrit,
        string $authAgent,
        bool $expected
    ) {
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm($guardClass, $authInscrit, $authAgent),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->isGuardEnabled($userType, $guardClass);

        $this->assertEquals($expected, $result);
    }

    public function getMainGuardDataProvider(): array
    {
        return [
            ['/entreprise/login', FormAuthenticator::class, '1', '0', FormAuthenticator::class],
            ['/entreprise/login', FormAuthenticator::class, '0', '1', null],
            ['/agent/login', FormAuthenticator::class, '0', '1', FormAuthenticator::class],
            ['/agent/login', FormAuthenticator::class, '1', '0', null],

            ['/admin/login', FormAuthenticator::class, '0', '0', FormAuthenticator::class],
            ['/admin/login', FormAuthenticator::class, '1', '1', FormAuthenticator::class],
            ['/admin/login', OpenidMicrosoftAuthenticator::class, '1', '1', FormAuthenticator::class],
            ['/admin/login', OpenidKeycloakAuthenticator::class, '1', '1', FormAuthenticator::class],

            ['/entreprise/login', OpenidKeycloakAuthenticator::class, '1', '0', OpenidKeycloakAuthenticator::class],
            ['/entreprise/login', OpenidKeycloakAuthenticator::class, '0', '1', null],
            ['/agent/login', OpenidKeycloakAuthenticator::class, '0', '1', OpenidKeycloakAuthenticator::class],
            ['/agent/login', OpenidKeycloakAuthenticator::class, '1', '0', null],

            ['/entreprise/login', OpenidMicrosoftAuthenticator::class, '1', '0', OpenidMicrosoftAuthenticator::class],
            ['/entreprise/login', OpenidMicrosoftAuthenticator::class, '0', '1', null],
            ['/agent/login', OpenidMicrosoftAuthenticator::class, '0', '1', OpenidMicrosoftAuthenticator::class],
            ['/agent/login', OpenidMicrosoftAuthenticator::class, '1', '0', null],

            ['/superadmin/login', FormAuthenticator::class, '0', '0', FormAuthenticator::class],
            ['/superadmin/login', FormAuthenticator::class, '1', '1', FormAuthenticator::class],
            ['/superadmin/login', OpenidMicrosoftAuthenticator::class, '1', '1', FormAuthenticator::class],
            ['/superadmin/login', OpenidKeycloakAuthenticator::class, '1', '1', FormAuthenticator::class],

            ['/agent/login', ExternalSsoAuthenticator::class, '1', '1', ExternalSsoAuthenticator::class],
            ['/agent/login', ExternalSsoAuthenticator::class, '0', '1', ExternalSsoAuthenticator::class],
            ['/agent/login', ExternalSsoAuthenticator::class, '1', '0', ExternalSsoAuthenticator::class],
            ['/agent/login', ExternalSsoAuthenticator::class, '0', '0', null],
            ['/entreprise/login', ExternalSsoAuthenticator::class, '1', '1', ExternalSsoAuthenticator::class],
            ['/entreprise/login', ExternalSsoAuthenticator::class, '0', '1', ExternalSsoAuthenticator::class],
            ['/entreprise/login', ExternalSsoAuthenticator::class, '1', '0', ExternalSsoAuthenticator::class],
            ['/entreprise/login', ExternalSsoAuthenticator::class, '0', '0', null],
        ];
    }

    /**
     * @dataProvider getMainGuardDataProvider
     * @param string $uri
     * @param string $guardClass
     * @param string $authInscrit
     * @param string $authAgent
     * @param string|null $expected
     */
    public function testGetMainGuard(
        string $uri,
        string $guardClass,
        string $authInscrit,
        string $authAgent,
        ?string $expected
    ) {
        $request = Request::create($uri);
        $mockParameterBagInterface = $this->createMock(ParameterBagInterface::class);

        $mockParameterBagInterface->expects(self::any())
            ->method('get')
            ->willReturn(false);

        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm($guardClass, $authInscrit, $authAgent),
            $mockParameterBagInterface,
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );



        $result = $guardManager->getMainGuard($request);

        $this->assertEquals($expected, $result);
    }

    protected function getConfigurationPlateformeEm($guardClass, $authInscrit, $authAgent): EntityManagerInterface
    {
        $configurationPlatform = new ConfigurationPlateforme();

        $configurationPlatform->setAuthenticateInscritByLogin('0');
        $configurationPlatform->setAuthenticateAgentByLogin('0');
        $configurationPlatform->setAuthenticateInscritOpenidKeycloak('0');
        $configurationPlatform->setAuthenticateAgentOpenidKeycloak('0');
        $configurationPlatform->setAuthenticateInscritOpenidMicrosoft('0');
        $configurationPlatform->setAuthenticateAgentOpenidMicrosoft('0');
        $configurationPlatform->setSocleExterneAgent('0');
        $configurationPlatform->setSocleExterneEntreprise('0');
        $configurationPlatform->setSocleInterne('1');

        if (FormAuthenticator::class === $guardClass) {
            $configurationPlatform->setAuthenticateInscritByLogin($authInscrit);
            $configurationPlatform->setAuthenticateAgentByLogin($authAgent);
        } elseif (OpenidKeycloakAuthenticator::class === $guardClass) {
            $configurationPlatform->setAuthenticateInscritOpenidKeycloak($authInscrit);
            $configurationPlatform->setAuthenticateAgentOpenidKeycloak($authAgent);
        } elseif (OpenidMicrosoftAuthenticator::class === $guardClass) {
            $configurationPlatform->setAuthenticateInscritOpenidMicrosoft($authInscrit);
            $configurationPlatform->setAuthenticateAgentOpenidMicrosoft($authAgent);
        } elseif (ExternalSsoAuthenticator::class === $guardClass) {
            $configurationPlatform->setSocleExterneEntreprise($authInscrit);
            $configurationPlatform->setSocleExterneAgent($authAgent);
        }

        $repositoryMock = $this->createMock(ConfigurationPlateformeRepository::class);
        $repositoryMock->method('getConfigurationPlateforme')->willReturn($configurationPlatform);

        $emMock = $this->createMock(EntityManagerInterface::class);
        $emMock->method('getRepository')->willReturn($repositoryMock);

        return $emMock;
    }

    public function getRedirectUrlDataProvider(): array
    {
        return [
            ['/entreprise/login', [], '/'],
            ['/', ['page' => 'Entreprise.EntrepriseAdvancedSearch'], '/'],
            [
                '/entreprise/login',
                [],
                'https://mpe-docker.local-trust.com/entreprise/footer/info-site',
                'https://mpe-docker.local-trust.com/entreprise/footer/info-site'
            ],
            ['/entreprise/login', [], '/', 'https://an-external-website.com/anything'],
            ['/agent', [], '/agent/login'],
            ['/agent/whatever', [], '/agent/login'],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifie'], '/agent/login'],
            ['/admin/login', [], '/admin/login'],
            ['/', ['page' => 'Administration.GestionOrganisme'], '/admin/login'],
            ['/superadmin/login', [], '/superadmin/login'],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifieSocleinterne'], '/agent/login', null, true],
        ];
    }

    /**
     * @dataProvider getRedirectUrlDataProvider
     * @param $uri
     * @param $parameters
     * @param $expected
     * @param null $httpReferer
     */
    public function testGetRedirectUrl($uri, $parameters, $expected, $httpReferer = null, $socleInterne = false)
    {
        $request = Request::create($uri, 'GET', $parameters);
        if (!is_null($httpReferer)) {
            $request->server->set('HTTPS', 'On');
            $request->server->set('SERVER_PORT', '443');
            $request->headers->set('HOST', 'mpe-docker.local-trust.com');
            $request->server->set('HTTP_REFERER', $httpReferer);
        }

        $router = $this->createMock(RouterInterface::class);
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm('', '', ''),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $router
        );

        if ($socleInterne) {
            $router
                ->method('generate')
                ->with('atexo_agent_accueil_socle_interne_index')
                ->willReturn('/agent/socle-interne')
            ;
        } else {
            $router->expects(self::never())->method('generate');
        }

        $result = $guardManager->getRedirectUrl($request, $socleInterne);

        $this->assertEquals($expected, $result);
    }

    public function getLogoutUrlDataProvider(): array
    {
        $mpeBaseUrl = 'https://mpe-docker.local-trust.com';

        return [
            [
                $mpeBaseUrl . '/entreprise',
                $mpeBaseUrl . '/entreprise/logout',
            ],
            [
                $mpeBaseUrl . '/entreprise',
                $mpeBaseUrl . '/index.php/entreprise/logout',
            ],
            [
                $mpeBaseUrl . '/agent',
                $mpeBaseUrl . '/agent/logout',
            ],
            [
                $mpeBaseUrl . '/agent',
                $mpeBaseUrl . '/index.php/agent/logout',
            ],
            [
                'http://keycloak-docker.local-trust.com/auth/realms/unroyaume/protocol/openid-connect/logout'
                . '?redirect_uri=https%3A%2F%2Fmpe-docker.local-trust.com%2F&session=logout',
                $mpeBaseUrl . '/entreprise/logout',
                '',
                OpenidKeycloakAuthenticator::class,
            ],
            [
                'https://sso-logout-url.fr/',
                $mpeBaseUrl . '/entreprise/logout',
                'https://sso-logout-url.fr/',
                OpenidKeycloakAuthenticator::class,
            ],
            [
                'https://sso-logout-url.fr/',
                $mpeBaseUrl . '/agent/logout',
                'https://sso-logout-url.fr/',
                OpenidKeycloakAuthenticator::class,
            ],
        ];
    }

    /**
     * @dataProvider getLogoutUrlDataProvider
     * @param string $expectedTargetUrl
     * @param string $currentUrl
     * @param string $ssoLogoutUrl
     * @param string $mainGuard
     */
    public function testGetLogoutUrl(
        string $expectedTargetUrl,
        string $currentUrl,
        string $ssoLogoutUrl = '',
        string $mainGuard = ''
    ) {
        $parameterBagMock = $this->createMock(ParameterBagInterface::class);
        $parameterBagMock->method('get')->willReturnMap(
            [
                ['URL_PPP_OPENSSO_LOGOUT', $ssoLogoutUrl],
                ['OAUTH_KEYCLOAK_AUTH_SERVER_URL', 'http://keycloak-docker.local-trust.com/auth'],
                ['OAUTH_KEYCLOAK_REALM', 'unroyaume'],
            ]
        );

        $routerMock = $this->createMock(RouterInterface::class);
        $routerMock->method('generate')->willReturn($expectedTargetUrl);

        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm($mainGuard, '', ''),
            $parameterBagMock,
            $this->createMock(Security::class),
            $routerMock
        );

        $request = Request::create($currentUrl, 'GET');
        $request->server->set('HTTPS', 'On');
        $request->server->set('SERVER_PORT', '443');
        $request->headers->set('HOST', 'mpe-docker.local-trust.com');

        $result = $guardManager->getLogoutUrl($request);

        $this->assertEquals($expectedTargetUrl, $result);
    }

    public function isEntrepriseDataProvider(): array
    {
        return [
            ['/entreprise/login', [], true],
            ['/', ['page' => 'Entreprise.EntrepriseAdvancedSearch'], true],
            ['/agent/login', [], false],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifie'], false],
            ['/admin/login', [], false],
            ['/', ['page' => 'Administration.GestionOrganisme'], false],
            ['/superadmin/login', [], false],
        ];
    }

    /**
     * @dataProvider isEntrepriseDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $expected
     */
    public function testIsEntreprise(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm('', '', ''),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->isEntreprise($request);

        $this->assertEquals($expected, $result);
    }

    public function isAgentDataProvider(): array
    {
        return [
            ['/entreprise/login', [], false],
            ['/', ['page' => 'Entreprise.EntrepriseAdvancedSearch'], false],
            ['/agent/login', [], true],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifie'], true],
            ['/admin/login', [], false],
            ['/', ['page' => 'Administration.GestionOrganisme'], false],
            ['/superadmin/login', [], false],
            ['/', ['page' => 'GestionPub.TableauDeBordAvisPublicite'], true],
            ['/', ['page' => 'Commission.AjoutOrdreDuJour'], true],
            ['/', ['page' => 'Commission.SuiviSeance'], true],
        ];
    }

    /**
     * @dataProvider isAgentDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $expected
     */
    public function testIsAgent(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm('', '', ''),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->isAgent($request);

        $this->assertEquals($expected, $result);
    }

    public function isAdministrateurDataProvider(): array
    {
        return [
            ['/entreprise/login', [], false],
            ['/', ['page' => 'Entreprise.EntrepriseAdvancedSearch'], false],
            ['/agent/login', [], false],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifie'], false],
            ['/admin/login', [], true],
            ['/', ['page' => 'Administration.GestionOrganisme'], true],
            ['/superadmin/login', [], false],
        ];
    }

    /**
     * @dataProvider isAdministrateurDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $expected
     */
    public function testIsAdministrateur(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm('', '', ''),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->isAdministrateur($request);

        $this->assertEquals($expected, $result);
    }

    public function isSuperAdminDataProvider(): array
    {
        return [
            ['/entreprise/login', [], false],
            ['/', ['page' => 'Entreprise.EntrepriseAdvancedSearch'], false],
            ['/agent/login', [], false],
            ['/', ['page' => 'Agent.AccueilAgentAuthentifie'], false],
            ['/admin/login', [], false],
            ['/', ['page' => 'Administration.GestionOrganisme'], false],
            ['/superadmin/login', [], true],
        ];
    }

    /**
     * @dataProvider isSuperAdminDataProvider
     * @param string $uri
     * @param array $parameters
     * @param bool $expected
     */
    public function testIsSuperAdmin(string $uri, array $parameters, bool $expected)
    {
        $request = Request::create($uri, 'GET', $parameters);
        $guardManager = new GuardManager(
            $this->getConfigurationPlateformeEm('', '', ''),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->isSuperAdmin($request);

        $this->assertEquals($expected, $result);
    }

    public function decodeJwtTokenDataProvider(): array
    {
        $object = new \stdClass();
        $object->key1 = 'Value 1';
        $object->key2 = 'Value 2';

        return [
            ['xxx.yyy.zzz', null],
            ['xxx.eyJrZXkxIjogIlZhbHVlIDEiLCAia2V5MiI6ICJWYWx1ZSAyIn0=.zzz', $object],
        ];
    }

    /**
     * @dataProvider decodeJwtTokenDataProvider
     * @param $token
     * @param $expected
     */
    public function testDecodeJwtToken($token, $expected)
    {
        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->decodeJwtToken($token);

        $this->assertEquals($expected, $result);
    }

    public function getOpenidMappingDataProvider(): array
    {
        return [
            ['whatever', null],
            [OpenidKeycloakAuthenticator::class, ['login' => 'preferred_username']],
            [OpenidMicrosoftAuthenticator::class,
                [
                    'login' => 'login',
                    'service_id' => 'service_id',
                    'organism_alias' => 'organisme_acronyme',
                    'firstname' => 'prenom',
                    'lastname' => 'family_name',
                    'email' => 'email',
                ]
            ],
        ];
    }

    /**
     * @dataProvider getOpenidMappingDataProvider
     * @param $guardClassName
     * @param $expected
     */
    public function testGetOpenidMapping($guardClassName, $expected)
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())
            ->method('get')
            ->with('oauth_mapping')
            ->willReturn([
                'microsoft' => [
                    'login' => 'login',
                    'service_id' => 'service_id',
                    'organism_alias' => 'organisme_acronyme',
                    'firstname' => 'prenom',
                    'lastname' => 'family_name',
                    'email' => 'email'
                ],
                'keycloak' => ['login' => 'preferred_username']
            ]);

        $guardManager = new GuardManager(
            $this->createMock(EntityManagerInterface::class),
            $parameterBag,
            $this->createMock(Security::class),
            $this->createMock(RouterInterface::class)
        );
        $result = $guardManager->getOpenidMapping($guardClassName);

        $this->assertEquals($expected, $result);
    }
}
