<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security;

use App\Repository\AgentRepository;
use App\Security\GuardManager;
use App\Security\OpenidKeycloakAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use KnpU\OAuth2ClientBundle\Client\OAuth2ClientInterface;
use League\OAuth2\Client\Token\AccessToken;
use Monolog\Logger;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBag;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\SessionStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class OpenidKeycloakAuthenticatorTest extends TestCase
{
    /**
     * @var SessionStorageInterface|null
     */
    protected $storage;

    /**
     * @var SessionInterface|null
     */
    protected $session;

    /**
     * @var MockObject
     */
    protected $mpeSecurityMock;

    /**
     * @var GuardManager|MockObject
     */
    private $guardManagerMock;

    /**
     * @var OpenidKeycloakAuthenticator
     */
    private $openidKeycloakAuthenticator;

    /**
     * @var ClientRegistry|MockObject
     */
    private $clientRegistryMock;

    /**
     * @var EntityManagerInterface|MockObject
     */
    private $entityManagerMock;

    protected function setUp(): void
    {
        parent::setUp();
        $this->openidKeycloakAuthenticator = new OpenidKeycloakAuthenticator(...$this->getMocks());
        $this->storage = new MockArraySessionStorage();
        $this->session = new Session($this->storage, new AttributeBag(), new FlashBag());
    }

    protected function getMocks()
    {
        $this->clientRegistryMock = $this->createMock(ClientRegistry::class);
        $this->entityManagerMock = $this->createMock(EntityManagerInterface::class);
        $this->guardManagerMock = $this->createMock(GuardManager::class);

        return [
            $this->clientRegistryMock,
            $this->entityManagerMock,
            $this->guardManagerMock,
            $this->createMock(LoggerInterface::class),
            $this->createMock(TranslatorInterface::class),
        ];
    }

    protected function getSupportedRequest(): Request
    {
        $request = Request::create('/');
        $request->attributes->set('_route', 'mpe_auth_keycloak_check');
        $request->query->set('code', 'value');
        $request->query->set('state', 'value');
        $request->query->set('user_type', GuardManager::USER_TYPE_AGENT);

        return $request;
    }

    public function testSupports()
    {
        $authenticator = $this->openidKeycloakAuthenticator;

        // Guard is disabled by configuration
        $this->assertFalse($authenticator->supports($this->getSupportedRequest()));

        // Guard is enabled by configuration
        $this->guardManagerMock->method('isGuardEnabled')->willReturn(true);
        $this->assertTrue($authenticator->supports($this->getSupportedRequest()));

        $mandatoryQueryParameters = ['code', 'state'];

        // Missing mandatory parameters should not be supported
        foreach ($mandatoryQueryParameters as $mandatoryQueryParameter) {
            $request = $this->getSupportedRequest();
            $request->query->remove($mandatoryQueryParameter);
            $this->assertFalse($authenticator->supports($request));
        }

        // Wrong route should not be supported
        $request = $this->getSupportedRequest();
        $request->attributes->set('_route', 'wrong_route');
        $this->assertFalse($authenticator->supports($request));
    }
    /**
     * @dataProvider tokenProvider
     */
    public function testGetCredentials($token, $data)
    {
        $mocks = $this->getMocks();
        $client = $this->createMock(ClientRegistry::class);
        $clientOAuth2 = $this->createMock(OAuth2ClientInterface::class);
        $logger = $this->createMock(LoggerInterface::class);
        $accessToken = new AccessToken(['access_token' => $token]);
        $openMock = $this->createPartialMock(
            OpenidKeycloakAuthenticator::class,
            ['fetchAccessToken',
                'getClientRegistry',
                'getKeycloakClient',
                'getLogger'
            ]
        );
        $openMock->method('fetchAccessToken')->willReturn($accessToken);
        $openMock->method('getClientRegistry')->willReturn($client);
        $openMock->method('getKeycloakClient')->willReturn($clientOAuth2);
        $openMock->method('getLogger')->willReturn($logger);
        $openMock->setMapping(
            ['login' => 'preferred_username',
                'usertype' => 'usertype',
                'usertype_agent' => 'usertype']
        );
        $authenticator = new OpenidKeycloakAuthenticator(...$mocks);
        $request = new Request();

        $result = $openMock->getCredentials($request);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('user_type', $result);
        $this->assertArrayHasKey('login', $result);
        $this->assertEquals($result['user_type'], $data['user_type']);
        $this->assertEquals($result['login'], $data['login']);
    }

    public function testOnAuthenticationSuccess()
    {
        $authenticator = $this->openidKeycloakAuthenticator;
        $request = $this->getSupportedRequest();

        $this->guardManagerMock->expects($this->once())->method('getRedirectUrl')->willReturn('/');

        $result = $authenticator->onAuthenticationSuccess(
            $request,
            $this->createMock(TokenInterface::class),
            ''
        );

        $this->assertInstanceOf(RedirectResponse::class, $result);
    }

    public function testOnAuthenticationFailure()
    {
        $authenticator = $this->openidKeycloakAuthenticator;
        $request = $this->getSupportedRequest();
        $request->setSession($this->session);

        $this->guardManagerMock->expects($this->never())->method('getRedirectUrl');
        $redirectUrl = 'https://mpe-docker.local-trust.com/';
        $this->guardManagerMock->expects($this->once())->method('getLogoutUrl')
            ->willReturn($redirectUrl);

        $exceptionMock = $this->createMock(AuthenticationException::class);
        $result = $authenticator->onAuthenticationFailure($request, $exceptionMock);

        $this->assertInstanceOf(RedirectResponse::class, $result);
        $this->assertEquals(302, $result->getStatusCode());
        $this->assertEquals($redirectUrl, $result->getTargetUrl());
    }

    public function testStart()
    {
        $authenticator = $this->openidKeycloakAuthenticator;
        $request = $this->getSupportedRequest();

        $this->guardManagerMock->expects($this->once())->method('getRedirectUrl')->willReturn('/');

        $result = $authenticator->start($request);

        $this->assertInstanceOf(RedirectResponse::class, $result);
        $this->assertEquals('/', $result->getTargetUrl());
    }

    public function testGetUser()
    {
        $mocks = $this->getMocks();
        $userProviderInterfaceMock = $this->createMock(UserProviderInterface::class);

        $whatevermapped = 'whatever';
        $this->guardManagerMock->expects($this->once())
            ->method('getOpenidMapping')->willReturn(['login' => $whatevermapped]);

        $authenticator = new OpenidKeycloakAuthenticator(...$mocks);

        $userLogin = 'userLogin';
        $jwtPayloadMock = new \stdClass();
        $jwtPayloadMock->$whatevermapped = $userLogin;

        $repositoryMock = $this->createMock(AgentRepository::class);
        $repositoryMock->expects($this->once())->method('findOneBy')
            ->with(['login' => $userLogin]);
        $this->entityManagerMock->method('getRepository')->willReturn($repositoryMock);

        $authenticator->getUser(
            ['user_type' => 'agent', 'login' => $userLogin],
            $userProviderInterfaceMock
        );
    }

    /**
     * @return \string[][]
     */
    public function tokenProvider()
    {
        $token1 = <<<'TAG'
eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJZUdVWWZxbFVhRjJUTmxwU1Y1QU1MWHd2dXYwa0pRdGp2NXNyTVBBcjFNIn0
.eyJqdGkiOiIzMzYzMGFlYi0wZTFkLTQzOTgtOTAxNi1kMjM2ODgwYzZmOTciLCJleHAiOjE2MzYwMzM0MjYsIm5iZiI6MCwiaWF0IjoxNjM2MDM
zMTI2LCJpc3MiOiJodHRwczovL3Nzby1wcmVwcm9kLm1lZ2FsaXMuYnJldGFnbmUuYnpoL2F1dGgvcmVhbG1zL21lZ2FsaXMiLCJhdWQiOiJhY2N
vdW50Iiwic3ViIjoiZjplOTA1MDc3Zi05NmMyLTQ0ZGQtYmNkMC04Mzc4NzI5Y2QwMjU6dGhpYmF1ZC5wdWpvbEBhdGV4by5jb20iLCJ0eXAiOiJ
CZWFyZXIiLCJhenAiOiJzYWxsZWRlc21hcmNoZXMiLCJhdXRoX3RpbWUiOjE2MzYwMzIxODUsInNlc3Npb25fc3RhdGUiOiJhZDY5YTg4YS00NjV
iLTQ1YjYtYWVhYi02ZjdhODVjNDUwZDQiLCJhY3IiOiIwIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vbWFyY2hlcy1wcmVwcm9kLm1lZ2F
saXMuYnJldGFnbmUuYnpoIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwidXN
lciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyI
sInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJ1aWQiOiI3MzgwMyIsImVtYWlsX3ZlcmlmaWVkIjpmYWx
zZSwibmFtZSI6IlRoaWJhdWQgUHVqb2wiLCJleHRlcm5hbGlkIjoiNzM4MDMiLCJ1c2VydHlwZSI6IkFHRU5UIiwicHJlZmVycmVkX3VzZXJuYW1
lIjoidGhpYmF1ZC5wdWpvbEBhdGV4by5jb20iLCJnaXZlbl9uYW1lIjoiVGhpYmF1ZCIsImZhbWlseV9uYW1lIjoiUHVqb2wiLCJlbWFpbCI6InR
oaWJhdWQucHVqb2xAYXRleG8uY29tIn0.NYr5vfcIL8SRtyl3JK9C-A7mhnl9YR8EVR41eN2Ge4sFgebCdYVWeL9sgayznVLKy_XiZHwnWfnsvGr
FGZcdbWbHKxKlIogBP_5FpIdJAYdEj5K3h5ymcBzsR_PbXMMP3dRYgCqBz8zI8suVRDuCZnTmiM1GZ4cA-CMM-Nh6sUVm44O2SSNYXl04rJAkauu
Bm76AiehJd9L9uSLa806UgPEkgYYVK4IvVgGRUSMs5-WT-ISXWFIFPH9dxrNlQn_6PXD2w3h6JNlLVE8TZTMxKeBmAus31bRtxImgZ4G_BTDJ3EB
F5n77h5V2cs1Zq8ThWbnYEgz2sGKM7C1p5J7NYw
TAG;
        $token2 = <<<'TAG'
eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJJZUdVWWZxbFVhRjJUTmxwU1Y1QU1MWHd2dXYwa0pRdGp2NXNyTVBBcjFNIn0.
eyJqdGkiOiJlZmMwMzEzMS05MzdhLTRiODAtOTA1NC02MTczYWFlMTQxNzciLCJleHAiOjE2MzYwMzIyMjcsIm5iZiI6MCwiaWF0IjoxNjM2MDMx
OTI3LCJpc3MiOiJodHRwczovL3Nzby1wcmVwcm9kLm1lZ2FsaXMuYnJldGFnbmUuYnpoL2F1dGgvcmVhbG1zL21lZ2FsaXMiLCJhdWQiOiJhY2Nv
dW50Iiwic3ViIjoiZjplOTA1MDc3Zi05NmMyLTQ0ZGQtYmNkMC04Mzc4NzI5Y2QwMjU6bG9pYy5wb3VsYWluQHZhbGRpbGxlLWF1YmlnbmUuZnIi
LCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJzYWxsZWRlc21hcmNoZXMiLCJhdXRoX3RpbWUiOjE2MzYwMzE5MjcsInNlc3Npb25fc3RhdGUiOiI1YjFj
ZTYyNS0zMmVkLTQwOWMtOTBlOS1jZTdiZjJhNGU5ODgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vbWFyY2hlcy1wcmVw
cm9kLm1lZ2FsaXMuYnJldGFnbmUuYnpoIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0
aW9uIiwidXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3Vu
dC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoib3BlbmlkIGVtYWlsIHByb2ZpbGUiLCJ1aWQiOiI4MTI4MiIsImVtYWlsX3Zlcmlm
aWVkIjpmYWxzZSwibmFtZSI6Ikxvw69jIFBvdWxhaW4iLCJleHRlcm5hbGlkIjoiODEyODIiLCJ1c2VydHlwZSI6IkFHRU5UIiwicHJlZmVycmVk
X3VzZXJuYW1lIjoibG9pYy5wb3VsYWluQHZhbGRpbGxlLWF1YmlnbmUuZnIiLCJnaXZlbl9uYW1lIjoiTG_Dr2MiLCJmYW1pbHlfbmFtZSI6IlBv
dWxhaW4iLCJlbWFpbCI6Im15ZWMzLnNpYis4MTQ5MC1wb3VsYWluQGdtYWlsLmNvbSJ9.QqFKZhDwZ7LU2I5p0Bz1Dr3ivFgm7P1bzl0jvMbjfhk
Kcm_Mut0p2w7uE6O5B7e8AXvMopzIU1ozyRzwuqaPL_9riBdq8T8Kq3rf0VNJ3IF0Iay38krPLRwKKTwDkAiU21tAa3D-qI0oL3mxYLvnYek59T4
6BskUppTkHP8t4dp3PmuAAWnHgPHvUnP6VsfUf9VqJba8lpj47Vz_1_u6Adbv5UQmokfLWMYBDI_DFbviAFHHfB5Y4j6E1CqTvfBizkIDVUSpoil
8bACuqMxQTkLvyz_Xtgc4qHJu0UVARnxHslJyqjOWtMiwqJnAtltj5VyhHx8-RFijpgwJR-7bag
TAG;
        return [
            [$token1, ['user_type' => 'inscrit', 'login' => 'thibaud.pujol@atexo.com' ]],
            [$token2, ['user_type' => 'inscrit', 'login' => 'loic.poulain@valdille-aubigne.fr' ]]
        ];
    }
}
