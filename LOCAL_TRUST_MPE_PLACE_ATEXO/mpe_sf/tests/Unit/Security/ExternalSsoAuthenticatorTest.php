<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Repository\AgentRepository;
use App\Repository\InscritRepository;
use App\Security\ExternalSsoAuthenticator;
use App\Security\GuardManager;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class ExternalSsoAuthenticatorTest extends TestCase
{
    /**
     * @dataProvider urlRedirectProvider
     *
     * @param string $urlRequest
     */
    public function testStart(string $urlRequest): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $request = new Request();

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn($urlRequest)
        ;

        $result = $externalSsoAuth->start($request);

        self::assertSame(RedirectResponse::class, get_class($result));
        self::assertSame($urlRequest, $result->getTargetUrl());
    }

    public function urlRedirectProvider(): array
    {
        return [
            'Entreprise' => ['/', '/'],
            'Agent' => ['/agent/login', '/agent/login'],
            'Admin' => ['/admin/login', '/admin/login'],
            'SuperAdmin' => ['/superadmin/login', '/superadmin/login'],
        ];
    }

    public function testSupportsTrue(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $request = new Request([], [], ['_route' => 'mpe_auth_sso_check']);
        $request->server->set('user-login', 'login');
        $request->server->set('user-external-id', '123456');
        $request->server->set('user-type', 'type');

        $guardManager
            ->expects(self::once())
            ->method('isGuardEnabled')
            ->with('type', ExternalSsoAuthenticator::class)
            ->willReturn(true)
        ;

        self::assertTrue($externalSsoAuth->supports($request));
    }

    public function testSupportsFalse(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $request = new Request([], [], ['_route' => 'wrong_route']);

        $guardManager
            ->expects(self::never())
            ->method('isGuardEnabled')
        ;

        self::assertFalse($externalSsoAuth->supports($request));
    }

    public function testCredentials(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $request = new Request();
        $request->server->set('user-type', 'type');
        $request->server->set('user-login', 'login');
        $request->server->set('user-external-id', '123456');
        $request->server->set('service-id', '654321');

        $expectedResult = [
            'user_type' => 'type',
            'user_login' => 'login',
            'user_external_id' => '123456',
            'service_id' => '654321',
        ];

        self::assertSame($expectedResult, $externalSsoAuth->getCredentials($request));
    }

    public function testCheckCredentials(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        self::assertTrue($externalSsoAuth->checkCredentials(['password' => 'mdp'], new Agent()));
    }

    public function testSupportsRememberMe(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        self::assertFalse($externalSsoAuth->supportsRememberMe());
    }

    public function testGetUserAgentByLogin(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $userProvider = $this->createMock(UserProviderInterface::class);

        $login = 'login';
        $credentials = [
            'user_type' => GuardManager::USER_TYPE_AGENT,
            'user_login' => $login,
            'user_external_id' => '123456',
            'service_id' => '654321',
        ];

        $agent = new Agent();

        $agentRepository = $this->createMock(AgentRepository::class);
        $agentRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['login' => $login])
            ->willReturn($agent)
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($agentRepository)
        ;

        self::assertSame($agent, $externalSsoAuth->getUser($credentials, $userProvider));
    }

    public function testGetUserEntrepriseByLogin(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $userProvider = $this->createMock(UserProviderInterface::class);

        $login = 'login';
        $credentials = [
            'user_type' => GuardManager::USER_TYPE_ENTREPRISE,
            'user_login' => $login,
            'user_external_id' => '123456',
            'service_id' => '654321',
        ];

        $entreprise = new Inscrit();

        $inscritRepository = $this->createMock(InscritRepository::class);
        $inscritRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with(['login' => $login])
            ->willReturn($entreprise)
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($inscritRepository)
        ;

        self::assertSame($entreprise, $externalSsoAuth->getUser($credentials, $userProvider));
    }

    public function testGetUserAgentByExternalIdAndService(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $userProvider = $this->createMock(UserProviderInterface::class);

        $serviceId = '654321';
        $idExterne = '123456';
        $credentials = [
            'user_type' => GuardManager::USER_TYPE_AGENT,
            'user_login' => null,
            'user_external_id' => $idExterne,
            'service_id' => $serviceId,
        ];

        $agent = new Agent();

        $agentRepository = $this->createMock(AgentRepository::class);
        $agentRepository
            ->expects(self::once())
            ->method('findOneBy')
            ->with([
                'idExterne' => $idExterne,
                'serviceId' => $serviceId,
            ])
            ->willReturn($agent)
        ;

        $em
            ->expects(self::once())
            ->method('getRepository')
            ->willReturn($agentRepository)
        ;

        self::assertSame($agent, $externalSsoAuth->getUser($credentials, $userProvider));
    }

    /**
     * @dataProvider urlRedirectProvider
     *
     * @param string $urlRequest
     */
    public function testOnAuthenticationSuccess(string $urlRequest): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $tokenInterface = $this->createMock(TokenInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $request = new Request();

        $guardManager
            ->expects(self::once())
            ->method('getRedirectUrl')
            ->with($request)
            ->willReturn($urlRequest)
        ;

        $result = $externalSsoAuth->onAuthenticationSuccess($request, $tokenInterface, null);

        self::assertSame(RedirectResponse::class, get_class($result));
        self::assertSame($urlRequest, $result->getTargetUrl());
    }

    public function testOnAuthenticationFailure(): void
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $guardManager = $this->createMock(GuardManager::class);
        $loggerInterface = $this->createMock(LoggerInterface::class);

        $externalSsoAuth = new ExternalSsoAuthenticator($em, $guardManager, $loggerInterface);

        $authException = $this->createMock(AuthenticationException::class);

        $request = new Request();

        $result = $externalSsoAuth->onAuthenticationFailure($request, $authException);

        self::assertSame(RedirectResponse::class, get_class($result));
        self::assertSame(Response::HTTP_FOUND, $result->getStatusCode());
        self::assertSame('/?page=Entreprise.FormulaireCompteEntreprise&action=creation', $result->getTargetUrl());
    }
}
