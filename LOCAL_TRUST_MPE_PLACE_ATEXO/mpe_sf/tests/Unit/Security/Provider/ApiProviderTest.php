<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Security\Provider;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Repository\AdministrateurRepository;
use App\Repository\AgentRepository;
use App\Repository\InscritRepository;
use App\Security\Provider\ApiProvider;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;

class ApiProviderTest extends TestCase
{
    public function testSupported()
    {
        $isSupport = $this->mockProvider()->supportsClass(Agent::class);
        $this->assertTrue($isSupport);
    }

    public function testNotSupported()
    {
        $isSupport = $this->mockProvider()->supportsClass(Consultation::class);
        $this->assertFalse($isSupport);
    }

    /**
     * @dataProvider getData
     */
    public function testLoadUserByUsernameAndPayload(array $payload, string $expected)
    {
        $user = $this->mockProvider()->loadUserByUsernameAndPayload('login', $payload);

        $this->assertInstanceOf($expected, $user);
    }

    public function testAuthenticationFailed()
    {
        $this->expectException(UserNotFoundException::class);

        $user = $this->mockProvider()->loadUserByUsernameAndPayload('login', []);
    }

    private function getData(): array
    {
        return [
            [['roles' => ['ROLE_ENTREPRISE']], Inscrit::class],
            [['roles' => ['ROLE_AGENT']], Agent::class],
            [['roles' => ['ROLE_SUPER_ADMIN']], Administrateur::class],
        ];
    }

    private function mockProvider(): ApiProvider
    {
        $inscritRepository = $this->createMock(InscritRepository::class);
        $inscritRepository->method('findOneBy')->willReturn(new Inscrit());

        $agentRepository = $this->createMock(AgentRepository::class);
        $agentRepository->method('findOneBy')->willReturn(new Agent());

        $adminRepository = $this->createMock(AdministrateurRepository::class);
        $adminRepository->method('findOneBy')->willReturn(new Administrateur());

        return new ApiProvider($inscritRepository, $agentRepository, $adminRepository);
    }
}
