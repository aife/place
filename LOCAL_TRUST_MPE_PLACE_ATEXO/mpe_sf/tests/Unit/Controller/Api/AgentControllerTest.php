<?php

namespace Tests\Unit\Controller\Api;

use App\Controller\Api\AgentController;
use App\Entity\Agent;
use App\Service\AgentService;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Tests\Unit\AtexoTestCase;

class AgentControllerTest extends AtexoTestCase
{
    public function testHabilitation()
    {
        $containerMock = $this->createMock(AbstractController::class);

        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $serializer = $this->createMock(Serializer::class);

        $parameterBag->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will(
                $this->returnCallback(
                    function ($parameter) {
                        static $callCount = [];
                        $callCount[$parameter] = !isset($callCount[$parameter]) ? 1 : ++$callCount[$parameter];
                        if ('format' === $parameter) {
                            return 'xml';
                        }
                        if ('SERVICE_METIER_EXEC' === $parameter) {
                            return 30;
                        }
                    }
                )
            );

        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn(new Agent());
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isGrantedFromAgentWs'))
            ->willReturn(true);

        $agentService = $this->createMock(AgentService::class);

        $agentService->expects($this->any())
            ->method($this->equalTo('getIdAgentFromIdSso'))
            ->will($this->returnValue(1))
        ;
        $agentService->expects($this->any())
            ->method($this->equalTo('getAgent'))
            ->willReturn(new Agent())
        ;

        $serializer->expects($this->any())
            ->method($this->equalTo('simpleSerialize'))
            ->willReturn(
                $this->returnValue($this->getXml())
            )
        ;

        $controller = new AgentController(
            $this->createMock(EntityManagerInterface::class),
            $agentTechniqueTokenService,
            $this->createMock(Serializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
            $agentService,
            $serializer,
            $this->createMock(StatistiqueService::class),
            $parameterBag
        );

        $response = $controller->habilitationsAction(new Request(['token' => 'tokenApi']));

        $this->checkResponse($response, 'xml');
    }

    public function getXml()
    {
        return '<?xml version="1.0" ?>
<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ' .
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">
    <reponse statutReponse="OK">
        <agentSSO>
            <id>11362</id>
            <identifiant>adminORME</identifiant>
            <habilitations>
                <habilitation>GESTIONAGENTPOLE</habilitation>
                <habilitation>GESTIONFOURNISSEURSENVOISPOSTAUX</habilitation>
                <habilitation>GESTIONBICLES</habilitation>
                <habilitation>CREERCONSULTATION</habilitation>
                <habilitation>MODIFIERCONSULTATION</habilitation>
                <habilitation>VALIDERCONSULTATION</habilitation>
                <habilitation>PUBLIERCONSULTATION</habilitation>
                <habilitation>SUIVRECONSULTATION</habilitation>
                <habilitation>SUIVRECONSULTATIONPOLE</habilitation>
                <habilitation>SUPPRIMERENVELOPPE</habilitation>
                <habilitation>SUPPRIMERCONSULTATION</habilitation>
                <habilitation>DEPOUILLERCANDIDATURE</habilitation>
                <habilitation>DEPOUILLEROFFRE</habilitation>
                <habilitation>MESSAGERIESECURISEE</habilitation>
                <habilitation>ACCESREGISTREDEPOTSPAPIER</habilitation>
                <habilitation>ACCESREGISTRERETRAITSPAPIER</habilitation>
                <habilitation>ACCESREGISTREQUESTIONSPAPIER</habilitation>
                <habilitation>GERERENCHERES</habilitation>
                <habilitation>SUIVREENCHERES</habilitation>
                <habilitation>SUIVIENTREPRISE</habilitation>
                <habilitation>ENVOIBOAMP</habilitation>
                <habilitation>ACCESCLASSEMENTLOT</habilitation>
                <habilitation>CONNECTEURSIS</habilitation>
                <habilitation>CONNECTEURMARCO</habilitation>
                <habilitation>REPONDREAUXQUESTIONS</habilitation>
                <habilitation>APPELPROJETFORMATION</habilitation>
                <habilitation>UTILISERCLIENTCAO</habilitation>
                <habilitation>NOTIFICATIONBOAMP</habilitation>
                <habilitation>ADMINISTRERCOMPTE</habilitation>
                <habilitation>GESTIONMAPA</habilitation>
                <habilitation>GESTIONTYPEVALIDATION</habilitation>
                <habilitation>APPROUVERCONSULTATION</habilitation>
                <habilitation>ADMINISTRERPROCEDURE</habilitation>
                <habilitation>RESTREINDRECREATION</habilitation>
                <habilitation>CREERLISTEMARCHES</habilitation>
                <habilitation>GESTIONCOMMISSIONS</habilitation>
                <habilitation>SUIVISEULCONSULTATION</habilitation>
                <habilitation>ATTRIBUTIONMARCHE</habilitation>
                <habilitation>FICHERECENSEMENT</habilitation>
                <habilitation>DECLARERINFRUCTUEUX</habilitation>
                <habilitation>DECLARERSANSSUITE</habilitation>
                <habilitation>CREERCONSULTATIONTRANSVERSE</habilitation>
                <habilitation>OUVRIRCANDIDATUREENLIGNE</habilitation>
                <habilitation>OUVRIRCANDIDATUREADISTANCE</habilitation>
                <habilitation>REFUSERENVELOPPE</habilitation>
                <habilitation>GERERADMISSIBILITE</habilitation>
                <habilitation>RESTAURERENVELOPPE</habilitation>
                <habilitation>OUVRIRANONYMATENLIGNE</habilitation>
                <habilitation>OUVRIROFFREENLIGNE</habilitation>
                <habilitation>GESTIONCOMPTEBOAMP</habilitation>
                <habilitation>GESTIONAGENTS</habilitation>
                <habilitation>GESTIONHABILITATIONS</habilitation>
                <habilitation>GERERMAPAINFERIEURMONTANT</habilitation>
                <habilitation>GERERMAPASUPERIEURMONTANT</habilitation>
                <habilitation>MODIFIERCONSULTATIONAVANTVALIDATION</habilitation>
                <habilitation>MODIFIERCONSULTATIONAPRESVALIDATION</habilitation>
                <habilitation>ACCESREPONSES</habilitation>
                <habilitation>TELECHARGEMENTGROUPEANTICIPEPLISCHIFFRES</habilitation>
                <habilitation>TELECHARGEMENTUNITAIREPLISCHIFFRES</habilitation>
                <habilitation>OUVRIROFFREADISTANCE</habilitation>
                <habilitation>CREERANNONCEINFORMATION</habilitation>
                <habilitation>SAISIEMARCHES</habilitation>
                <habilitation>VALIDATIONMARCHES</habilitation>
                <habilitation>PUBLICATIONMARCHES</habilitation>
                <habilitation>GERERSTATISTIQUESMETIER</habilitation>
                <habilitation>GERERARCHIVES</habilitation>
                <habilitation>ADMINISTRERPROCEDURESFORMALISEES</habilitation>
                <habilitation>CREERANNONCEATTRIBUTION</habilitation>
                <habilitation>ACCESREGISTRERETRAITSELECTRONIQUE</habilitation>
                <habilitation>ACCESREGISTREQUESTIONSELECTRONIQUE</habilitation>
                <habilitation>ACCESREGISTREDEPOTSELECTRONIQUE</habilitation>
                <habilitation>VALIDATIONSIMPLE</habilitation>
                <habilitation>VALIDATIONINTERMEDIAIRE</habilitation>
                <habilitation>VALIDATIONFINALE</habilitation>
                <habilitation>CREERSUITECONSULTATION</habilitation>
                <habilitation>HYPERADMIN</habilitation>
                <habilitation>DROITGESTIONSERVICES</habilitation>
                <habilitation>SUIVIACCES</habilitation>
                <habilitation>STATISTIQUESSITE</habilitation>
                <habilitation>STATISTIQUESQOS</habilitation>
                <habilitation>OUVRIRANONYMATADISTANCE</habilitation>
                <habilitation>GESTIONCOMPTEJAL</habilitation>
                <habilitation>GESTIONCENTRALEPUB</habilitation>
                <habilitation>GESTIONCOMPTEGROUPEMONITEUR</habilitation>
                <habilitation>OUVRIROFFRETECHNIQUEENLIGNE</habilitation>
                <habilitation>OUVRIROFFRETECHNIQUEADISTANCE</habilitation>
                <habilitation>ACTIVATIONCOMPTEENTREPRISE</habilitation>
                <habilitation>IMPORTERENVELOPPE</habilitation>
                <habilitation>SUIVISEULREGISTREDEPOTSPAPIER</habilitation>
                <habilitation>SUIVISEULREGISTRERETRAITSPAPIER</habilitation>
                <habilitation>SUIVISEULREGISTREQUESTIONSPAPIER</habilitation>
                <habilitation>SUIVISEULREGISTREDEPOTSELECTRONIQUE</habilitation>
                <habilitation>SUIVISEULREGISTRERETRAITSELECTRONIQUE</habilitation>
                <habilitation>SUIVISEULREGISTREQUESTIONSELECTRONIQUE</habilitation>
                <habilitation>MODIFIERCONSULTATIONMAPAINFERIEURMONTANTAPRESVALIDATION</habilitation>
                <habilitation>MODIFIERCONSULTATIONMAPASUPERIEURMONTANTAPRESVALIDATION</habilitation>
                <habilitation>MODIFIERCONSULTATIONPROCEDURESFORMALISEESAPRESVALIDATION</habilitation>
                <habilitation>GERERLESENTREPRISES</habilitation>
                <habilitation>PORTEESOCIETESEXCLUES</habilitation>
                <habilitation>PORTEESOCIETESEXCLUESTOUSORGANISMES</habilitation>
                <habilitation>MODIFIERSOCIETESEXCLUES</habilitation>
                <habilitation>SUPPRIMERSOCIETESEXCLUES</habilitation>
                <habilitation>RESULTATANALYSE</habilitation>
                <habilitation>GERERADRESSESSERVICE</habilitation>
                <habilitation>GERERMONSERVICE</habilitation>
                <habilitation>DOWNLOADARCHIVES</habilitation>
                <habilitation>CREERANNONCEEXTRAITPV</habilitation>
                <habilitation>CREERANNONCERAPPORTACHEVEMENT</habilitation>
                <habilitation>GESTIONCERTIFICATSAGENT</habilitation>
                <habilitation>CREERAVISPROGRAMMEPREVISIONNEL</habilitation>
                <habilitation>ANNULERCONSULTATION</habilitation>
                <habilitation>ENVOYERPUBLICITE</habilitation>
                <habilitation>LISTEMARCHESNOTIFIES</habilitation>
                <habilitation>SUIVREMESSAGE</habilitation>
                <habilitation>ENVOYERMESSAGE</habilitation>
                <habilitation>SUIVIFLUXCHORUSTRANSVERSAL</habilitation>
                <habilitation>GESTIONMANDATAIRE</habilitation>
                <habilitation>GERERNEWSLETTER</habilitation>
                <habilitation>GESTIONMODELESFORMULAIRE</habilitation>
                <habilitation>GESTIONADRESSESFACTURATIONJAL</habilitation>
                <habilitation>ADMINISTRERADRESSESFACTURATIONJAL</habilitation>
                <habilitation>REDACTIONDOCUMENTSREDAC</habilitation>
                <habilitation>VALIDATIONDOCUMENTSREDAC</habilitation>
                <habilitation>ANNUAIREACHETEUR</habilitation>
                <habilitation>REPRENDREINTEGRALEMENTARTICLE</habilitation>
                <habilitation>ADMINISTRERCLAUSES</habilitation>
                <habilitation>VALIDERCLAUSES</habilitation>
                <habilitation>ADMINISTRERCANEVAS</habilitation>
                <habilitation>VALIDERCANEVAS</habilitation>
                <habilitation>ADMINISTRERCLAUSESENTITEACHATS</habilitation>
                <habilitation>GENERERPIECESFORMATODT</habilitation>
                <habilitation>PUBLIERVERSIONCLAUSIEREDITEUR</habilitation>
                <habilitation>ADMINISTRERCLAUSESEDITEUR</habilitation>
                <habilitation>VALIDERCLAUSESEDITEUR</habilitation>
                <habilitation>ADMINISTRERCANEVASEDITEUR</habilitation>
                <habilitation>VALIDERCANEVASEDITEUR</habilitation>
                <habilitation>DECISIONSUIVISEUL</habilitation>
                <habilitation>OUVRIRCANDIDATUREHORSLIGNE</habilitation>
                <habilitation>OUVRIROFFREHORSLIGNE</habilitation>
                <habilitation>OUVRIROFFRETECHNIQUEHORSLIGNE</habilitation>
                <habilitation>OUVRIRANONYMATHORSLIGNE</habilitation>
                <habilitation>ESPACECOLLABORATIFGESTIONNAIRE</habilitation>
                <habilitation>ESPACECOLLABORATIFCONTRIBUTEUR</habilitation>
                <habilitation>GERERORGANISMES</habilitation>
                <habilitation>GERERASSOCIATIONSAGENTS</habilitation>
                <habilitation>HISTORIQUENAVIGATIONINSCRITS</habilitation>
                <habilitation>TELECHARGERACCORDSCADRES</habilitation>
                <habilitation>CREERANNONCEDECISIONRESILIATION</habilitation>
                <habilitation>CREERANNONCESYNTHESERAPPORTAUDIT</habilitation>
                <habilitation>GEREROPERATIONS</habilitation>
                <habilitation>TELECHARGERSIRETACHETEUR</habilitation>
                <habilitation>GERERREOUVERTURESMODIFICATION</habilitation>
                <habilitation>ACCEDERTOUSTELECHARGEMENTS</habilitation>
                <habilitation>CREERCONTRAT</habilitation>
                <habilitation>ModifierContrat</habilitation>
                <habilitation>CONSULTERCONTRAT</habilitation>
                <habilitation>GERERNEWSLETTERREDAC</habilitation>
                <habilitation>PROFILRMA</habilitation>
                <habilitation>AFFECTATIONVISIONRMA</habilitation>
                <habilitation>GERERGABARITEDITEUR</habilitation>
                <habilitation>GERERGABARIT</habilitation>
                <habilitation>GERERGABARITENTITEACHATS</habilitation>
                <habilitation>GERERGABARITAGENT</habilitation>
                <habilitation>GERERMESSAGESACCUEIL</habilitation>
                <habilitation>GEREROAGA</habilitation>
                <habilitation>DEPLACERSERVICE</habilitation>
                <habilitation>ACTIVERVERSIONCLAUSIER</habilitation>
                <habilitation>IDAGENT</habilitation>
            </habilitations>
        </agentSSO>
    </reponse>
</mpe>';
    }
}
