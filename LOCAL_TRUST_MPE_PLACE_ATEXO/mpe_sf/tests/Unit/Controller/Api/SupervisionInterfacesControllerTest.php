<?php

namespace Tests\Unit\Controller\Api;

use App\Controller\Api\SupervisionInterfacesController;
use App\Entity\Api\SupervisionInterface;
use App\Entity\SsoTiers;
use App\Entity\Tiers;
use App\Repository\Api\SupervisionInterfaceRepository;
use App\Repository\SsoTiersRepository;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

class SupervisionInterfacesControllerTest extends TestCase
{
    protected $xsd;

    public function setUp(): void
    {
        $this->xsd = realpath(__DIR__ . '/../../../../public/app/xsd/mpe.xsd');
        parent::setUp();
    }

    public function getController()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (SsoTiers::class == $repository) {
                        $repository = $this->createMock(SsoTiersRepository::class);

                        $entity = new SsoTiers();
                        $entity->setIdTiers(1);
                        $repository->expects($this->any())
                            ->method('checkToken')
                            ->willReturn($entity);

                        return $repository;
                    }

                    if (Tiers::class == $repository) {
                        $repository = $this->createMock(SsoTiersRepository::class);

                        $entity = new Tiers();
                        $entity->setLogin('login');
                        $repository->expects($this->any())
                            ->method('findAll')
                            ->willReturn([$entity]);

                        return $repository;
                    }

                    if (SupervisionInterface::class == $repository) {
                        $supervisionInterface = new SupervisionInterface();
                        $supervisionInterface->setWebserviceBatch('webserviceBatch');
                        $supervisionInterface->setCreatedDate(new \DateTime());
                        $supervisionInterface->setTotal(1);
                        $supervisionInterface->setTotalOk('1');
                        $supervisionInterface->setPoids(99999999999);
                        $supervisionInterface->setPoidsOk(99999999999);
                        $supervisionInterface->setId(1);
                        $supervisionInterface->setInterface('interface');
                        $supervisionInterface->setService('service');

                        $repository = $this->createMock(SupervisionInterfaceRepository::class);
                        $repository->expects($this->any())
                            ->method('getByDateAndInterface')
                            ->willReturn($supervisionInterface);

                        $repository->expects($this->any())
                            ->method('getSupervisionInterfaceByDateByService')
                            ->willReturn([$supervisionInterface]);

                        $repository->expects($this->any())
                            ->method('update')
                            ->willReturn($supervisionInterface);

                        return $repository;
                    }
                }
            );

        $logger = $this->createMock(LoggerInterface::class);

        $parameterBag = $this->createMock(ParameterBagInterface::class);

        $statistiqueService = new StatistiqueService($em, $parameterBag);
        $statistiqueService->setXsd($this->xsd);

        $request = $this->getRequest();

        $statistiqueService->create($request);

        return new SupervisionInterfacesController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
            $statistiqueService,
        );
    }

    public function getRequest()
    {
        $request = new Request(
            [
                'interface' => 'interface',
                'service' => 'service',
                'webserviceBatch' => 'webserviceBatch',
                'statut' => 'nok',
            ]
        );

        $request->headers->set('Authorization', 'Bearer 123456');

        return $request;
    }

    public function testPostStatistiqueAction()
    {
        $controller = $this->getController();
        $request = $this->getRequest();
        $xml = $controller->postStatistiqueAction($request);

        $expected = true;
        $dom = new \DOMDocument();
        $dom->loadXML($xml->getContent());
        $actual = $dom->schemaValidate($this->xsd);

        $this->assertSame(
            $expected,
            $actual,
            'XML response format is invalid against XSD : '
            .$this->xsd.PHP_EOL.$xml->getContent()
        );
    }

    public function testGetStatistiqueAction()
    {
        $controller = $this->getController();
        $request = $this->getRequest();
        $xml = $controller->recoveryIndicatorsAction($request);

        $expected = true;
        $dom = new \DOMDocument();
        $dom->loadXML($xml->getContent());
        $actual = $dom->schemaValidate($this->xsd);
        $this->assertSame(
            $expected,
            $actual,
            'XML response format is invalid against XSD : '
            .$this->xsd.PHP_EOL.$xml->getContent()
        );
    }
}
