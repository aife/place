<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Controller\Api\Certificat;

use App\Controller\Api\Certificat\SaveCertificatController;
use App\Entity\Agent;
use App\Entity\CertificatPermanent;
use App\Entity\Organisme;
use App\Repository\AgentRepository;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AgentTechniqueTokenService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Tests\Unit\AtexoTestCase;
use App\Repository\CertificatPermanentRepository;

class SaveCertificatControllerTest extends AtexoTestCase
{
    /**
     * @throws \Exception
     */
    public function testDualKeyNoData(): void
    {
        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn(new Agent());
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isTokenCanAccessWs'))
            ->willReturn(true);

        $em = $this->createMock(EntityManagerInterface::class);

        $controller = new SaveCertificatController(
            $em,
            $agentTechniqueTokenService,
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
        );

        $response = json_decode($controller->dualKey(new Request())->getContent(), true);
        $this->assertEquals(401, $response['code']);
        $this->assertEquals('Ticket invalide', $response['message']);
    }

    /**
     * @throws \Exception
     */
    public function testDualKeyTokenValidAgentNonAuthentifie(): void
    {
        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $agent = new Agent();
        $agent->setLogin('mamp');
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn($agent);
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isTokenCanAccessWs'))
            ->willReturn(false);

        $em = $this->createMock(EntityManagerInterface::class);

        $controller = new SaveCertificatController(
            $em,
            $agentTechniqueTokenService,
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
        );
        $request = new Request();
        $request->query->set('ticket', 'TOKEN_TEST');
        $response = $controller->dualKey($request);
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals(401, $result['code']);
        $this->assertEquals('Agent Technique non autorisé d\'accéder à ce WS', $result['message']);
    }

    /**
     * @throws \Exception
     */
    public function testDualKeyTokenInValidAgentAuthentifie(): void
    {
        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $agent = new Agent();
        $agent->setLogin('mamp');
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn($agent);
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isTokenCanAccessWs'))
            ->willReturn(false);

        $em = $this->createMock(EntityManagerInterface::class);

        $controller = new SaveCertificatController(
            $em,
            $agentTechniqueTokenService,
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
        );
        $request = new Request();
        $request->query->set('ticket', 'TOKEN_TEST');
        $response = $controller->dualKey($request);
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals(401, $result['code']);
        $this->assertEquals('Agent Technique non autorisé d\'accéder à ce WS', $result['message']);
    }

    /**
     * @throws \Exception
     */
    public function testDualKeyTokenValidAgentAuthentifieJsonVide(): void
    {
        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $agent = new Agent();
        $agent->setLogin('mamp');
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn($agent);
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isTokenCanAccessWs'))
            ->willReturn(true);

        $em = $this->createMock(EntityManagerInterface::class);

        $controller = new SaveCertificatController(
            $em,
            $agentTechniqueTokenService,
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
        );
        $request = new Request();
        $request->query->set('ticket', 'TOKEN_TEST');
        $response = $controller->dualKey($request);
        $content = $response->getContent();
        $result = json_decode($content, true);
        $this->assertEquals(401, $result['code']);
        $this->assertEquals(
            'Le json doit contenir ces quatres params: nomCleChiffrement, certificat, estCleChiffrementSecours,cnCleChiffrement',
            $result['message']
        );
    }

    /**
     * @throws \Exception
     */
    public function testDualKeyTokenValidAgentAuthentifieJsonAvecDonnees(): void
    {
        $agentTechniqueTokenService = $this->createMock(AgentTechniqueTokenService::class);
        $logger = $this->createMock(LoggerInterface::class);

        $agent = new Agent();
        $agent->setLogin('mamp');
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('getAgentFromToken'))
            ->willReturn($agent);
        $agentTechniqueTokenService->expects($this->any())
            ->method($this->equalTo('isTokenCanAccessWs'))
            ->willReturn(true);

        $em = $this->createMock(EntityManagerInterface::class);

        $controller = new SaveCertificatController(
            $em,
            $agentTechniqueTokenService,
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $logger,
        );
        $request = new Request();

        $request->initialize(['ticket', 'TOKEN_TEST'], [], [], [], [], [], '{
               "nomCleChiffrement":"TEST",
               "cnCleChiffrement":"TEST",
               "estCleChiffrementSecours": false,
               "certificat":"TEST"
            }');

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {

                    if (Agent::class == $repository) {
                        $repository = $this->createMock(AgentRepository::class);
                        $agentRepo = new Agent();
                        $agentRepo->setLogin('mamp');
                        $agentRepo->setOrganisme((new Organisme())->setAcronyme('e3r'));
                        $repository->expects($this->any())
                            ->method('find')
                            ->willReturn($agentRepo);

                        return $repository;
                    }

                    if (CertificatPermanent::class == $repository) {
                        $repository = $this->createMock(CertificatPermanentRepository::class);
                        $repository->expects($this->any())
                            ->method('getHighestId')
                            ->willReturn(1000);

                        return $repository;
                    }
                }
            );

        $response = $controller->dualKey($request);
        $content = $response->getContent();
        $result = json_decode($content, true);

        $this->assertEquals(200, $result['code']);
        $this->assertEquals('Certificat créé avec succès', $result['message']);
    }
}
