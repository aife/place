<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Controller\Entreprise;

use App\Controller\Entreprise\OrganismeController;
use App\Service\App\GraphicChart;
use App\Service\OrganismeService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class OrganismeControllerTest extends TestCase
{
    public function switchOrganismDataProvider()
    {
        return [
            [null, 'doesnotexist', false],
            [null, 'e3r', true],
            ['/somepage', 'doesnotexist', false],
            ['/somepage', 'e3r', true],
        ];
    }

    /**
     * @dataProvider switchOrganismDataProvider
     * @param $httpReferer
     * @param string $acronym
     * @param bool $orgExists
     */
    public function testSwitchOrganism($httpReferer, string $acronym, bool $orgExists): void
    {
        $request = Request::create('/', 'GET');
        if (!is_null($httpReferer)) {
            $request->headers->set('referer', [$httpReferer]);
        }
        $organismeService = $this->createMock(OrganismeService::class);
        $organismeService->method('exists')->willReturn($orgExists);

        $containerMock = $this->getContainerMock();

        $controller = new OrganismeController();
        $controller->setContainer($containerMock);

        $response = $controller->switchOrganism($request, $organismeService, $acronym);

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertEquals(
            ($httpReferer == null) ? '/entreprise' : $httpReferer,
            $response->headers->get('location')
        );

        $cookies = $response->headers->getCookies();

        if ($orgExists) {
            $this->assertEquals(3, sizeof($cookies));
        } else {
            $this->assertEmpty($cookies);
        }
    }

    /**
     * @return mixed|MockObject|Container
     */
    protected function getContainerMock()
    {
        $routerMock = $this->createMock(Router::class);
        $routerMock->method('generate')->willReturn('/entreprise');

        $containerMock = $this->createMock(Container::class);
        $containerMock->method('get')->willReturn($routerMock);
        return $containerMock;
    }
}
