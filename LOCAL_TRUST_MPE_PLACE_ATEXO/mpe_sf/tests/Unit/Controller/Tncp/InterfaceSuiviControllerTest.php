<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Unit\Controller\Tncp;

use App\Controller\Tncp\InterfaceSuiviController;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class InterfaceSuiviControllerTest extends TestCase
{
    /**
     * @dataProvider mapFiltersDataProvider
     * @param $inputData
     * @param $expectedResult
     * @return void
     * @throws ReflectionException
     */
    public function testMapFilters($inputData, $expectedResult): void
    {
        $reflectionClass = new \ReflectionClass(InterfaceSuiviController::class);
        $method = $reflectionClass->getMethod('mapFilters');
        $method->setAccessible(true);

        $controller = new InterfaceSuiviController(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(ParameterBagInterface::class),
            $this->createMock(JWTTokenManagerInterface::class),
            $this->createMock(LoggerInterface::class),
        );

        $result = $method->invoke($controller, $inputData);

        $this->assertSame($expectedResult, $result);
    }

    public function mapFiltersDataProvider(): array
    {
        return [
            [[], []],
            [
                ['dateEnvoi' => '17/01/2023 - 18/01/2023'],
                ['dateCreation' => ['after' => '17-01-2023', 'strictly_before' => '19-01-2023']]
            ],
            [
                ['dateEnvoi' => '18/01/2023 - 18/01/2023'],
                ['dateCreation' => ['after' => '18-01-2023', 'strictly_before' => '19-01-2023']],
            ],
            [
                ['dateEnvoi' => '18/01/2023 - 31/01/2023'],
                ['dateCreation' => ['after' => '18-01-2023', 'strictly_before' => '01-02-2023']],
            ],
        ];
    }
}
