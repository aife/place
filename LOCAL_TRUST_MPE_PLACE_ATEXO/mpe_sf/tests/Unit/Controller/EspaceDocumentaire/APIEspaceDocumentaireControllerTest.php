<?php

namespace Tests\Unit\Controller\EspaceDocumentaire;

use App\Controller\Api\EspaceDocumentaire\ApiEspaceDocumentaireController;
use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\Consultation\AutrePieceConsultation;
use App\Entity\DCE;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\HabilitationAgent;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TDumeNumero;
use App\Exception\ApiProblemForbiddenException;
use App\Repository\BloborganismeFileRepository;
use App\Repository\DCERepository;
use App\Repository\EnveloppeFichierRepository;
use App\Repository\EnveloppeRepository;
use App\Repository\OffreRepository;
use App\Repository\TCandidatureRepository;
use App\Repository\TDumeContexteRepository;
use App\Repository\TDumeNumeroRepository;
use App\Service\AgentTechniqueTokenService;
use App\Service\Api\Serializer;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\AtexoAgent;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\ContratService;
use App\Service\EspaceDocumentaire\Authorization;
use App\Service\EspaceDocumentaire\EspaceDocumentaireUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use App\Service\SearchAgent;
use App\Service\WebServices\WebServicesExec;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use App\Utils\Zip;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;
use App\Message\GenerationArchiveDocumentaire;

class APIEspaceDocumentaireControllerTest extends AtexoTestCase
{
    private $security;
    private $user;
    private $auth;

    public function setUp(): void
    {
        parent::setUp();
        $this->security = $this->createMock(Security::class);
        $organisme = new Organisme();
        $organisme->setAcronyme('brice-test');
        $this->user = new Agent();
        $this->user->setId(1);
        $this->user->setOrganisme($organisme);
        $this->security->expects($this->any())
            ->method('getUser')
            ->willReturn($this->user);

        $this->auth = $this->createMock(AuthorizationAgent::class);
        $this->auth->expects($this->any())
            ->method('isInTheScope')
            ->willReturn(true);
    }

    protected function setUpNotConnected()
    {
        $this->security = $this->createMock(Security::class);
        $this->security->expects($this->any())
            ->method('getUser')
            ->willReturn(null);
    }

    private function getControllerPlisNotConnected()
    {
        $this->setUpNotConnected();

        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', []);
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Vous n\'êtes pas autorisé à accéder à cette ressource !');
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($serializer);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth),
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetPlisNotConnectedAction()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller = $this->getControllerPlisNotConnected();
        $controller->getPlisAction($request, $espaceDocumentaireUtil);
    }

    protected function getControllerForPlusConnectedSansDepot()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $offre = new Offre();
        $offre->setOrganisme('pmi-pmi-1');
        $offre->setConsultationId(1600);
        $repositoryEnveloppe = $this->createMock(EnveloppeRepository::class);
        $arrayOffres = [];

        $repositoryOffre = $this->createMock(OffreRepository::class);
        $repositoryOffre->expects($this->any())
            ->method('getAllDepotFromConsultationId')
            ->willReturn($arrayOffres);

        $repositoryEnveloppe->expects($this->any())
            ->method('getInfoEnveloppes')
            ->willReturn($arrayOffres);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryHabilitationAgent,
                $repositoryOffre,
                $repositoryEnveloppe
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $this->security,
            $this->auth
        );
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucun dépôt n’a été effectué sur cette consultation.');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );


        $controller->setContainer($container);

        return $controller;
    }

    public function testGetPlisConnectedSansDepotAction()
    {
        $em = $this->createMock(EntityManager::class);
        $repository = $this->createMock(OffreRepository::class);
        $repository->expects($this->any())
            ->method('getAllDepotFromConsultationId')
            ->willReturn([]);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository);

        ob_start();

        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerForPlusConnectedSansDepot();
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller->getPlisAction($request, $espaceDocumentaireUtil);
        $this->assertEquals(367, ob_get_length());
        $this->assertStringContainsString('DEPOT', ob_get_contents());
        $this->assertStringContainsString('Aucun dépôt n’a été effectué sur cette consultation.', ob_get_contents());
        ob_end_clean();
    }

    protected function getControllerForPlisConnectedAvecDepot()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');

        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $arrayOffres = [
            [
                'nomEntrepriseInscrit' => 'ATEXO',
                'sousPli' => 0,
                'id' => 1545,
                'statutOffres' => 1,
                'numeroReponse' => '1',
            ],
            [
                'nomEntrepriseInscrit' => 'ATEXO',
                'sousPli' => 3,
                'id' => 1421,
                'statutOffres' => 1,
                'numeroReponse' => '1',
            ],
        ];

        $repositoryOffre = $this->createMock(OffreRepository::class);
        $repositoryOffre->expects($this->any())
            ->method('getAllDepotFromConsultationId')
            ->willReturn($arrayOffres);

        $repositoryEnveloppe = $this->createMock(EnveloppeRepository::class);
        $repositoryEnveloppe->expects($this->any())
            ->method('getInfoEnveloppes')
            ->willReturn($arrayOffres);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryAgent,
                $repositoryHabilitationAgent,
                $repositoryOffre,
                $repositoryEnveloppe
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $this->security,
            $this->auth
        );

        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);

        $translatorInterface = $this->createMock(TranslatorInterface::class);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    protected function getControllerDceNotConnected()
    {
        $this->setUpNotConnected();

        $em = $this->createMock(EntityManager::class);
        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $reference = 395;
        $session->set('contexte_authentification', []);
        $container = $this->createMock(ContainerInterface::class);
        $translatorInterface = $this->createMock(TranslatorInterface::class);

        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Vous n\'êtes pas autorisé à accuéder à cette ressource !');

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryption = new Encryption($parameterBag);
        $zip = new Zip($encryption);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $mountManager = $this->createMock(MountManager::class);
        $encryptions = new Encryption($parameterBag);

        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth),
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $contratService = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($serializer);
        $controller->setContainer($container);

        return $controller;
    }

    public function testGetDceNotConnectedAction()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller = $this->getControllerDceNotConnected($request);
        $controller->getDceAction($request, $espaceDocumentaireUtil);
    }

    private function getControllerDceConnectedAndNoDce()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $reference = 395;
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $consultation = new Consultation();
        $consultation->setOrganisme($organisme);
        $consultation->setId(1);
        $repositoryConsultation = $this->createMock(DCERepository::class);

        $listeDce = [];
        $repositoryConsultation->expects($this->any())
            ->method('getDce')
            ->willReturn($listeDce);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $repositoryConsultation);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization(
            $em,
            $searchAgent,
            $session,
            $container,
            $this->security,
            $this->auth
        );
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration, $serializer);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryption = new Encryption($parameterBag);
        $translatorInterface = $this->createMock(TranslatorInterface::class);

        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucun fichier n\'a été renseigné');
        $encryptions = new Encryption($parameterBag);

        $zip = new Zip($encryption);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $controller->setContainer($container);

        return $controller;
    }

    public function testGetDceConnectedAndNoDceAction()
    {
        ob_start();
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerDceConnectedAndNoDce();
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller->getDceAction($request, $espaceDocumentaireUtil);
        $this->assertEquals(389, ob_get_length());
        $this->assertStringContainsString('DCE', ob_get_contents());
        $this->assertStringContainsString('Aucun fichier n\'a été renseigné', ob_get_contents());
        ob_end_clean();
    }

    private function getControllerDceConnected()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $reference = 395;
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $consultation = new Consultation();
        $consultation->setOrganisme($organisme);
        $consultation->setId(1);
        $repositoryConsultation = $this->createMock(DCERepository::class);

        $dce = new DCE();
        $dce->setDce(1);
        $dce->setNomDce('test.zip');
        $repositoryConsultation->expects($this->any())
            ->method('getDce')
            ->willReturn($dce);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $repositoryConsultation);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $configuration = $this->createMock(AtexoConfiguration::class);

        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $mountManager = $this->createMock(MountManager::class);

        $mountManager->expects($this->any())
            ->method('getFileSize')
            ->willReturn(1210);

        $mountManager->expects($this->any())
            ->method('getAbsolutePath')
            ->willReturn(__DIR__ . '/../../data/test.zip');
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($configuration, $serializer);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $encryption = new Encryption($parameterBag);
        $translatorInterface = $this->createMock(TranslatorInterface::class);

        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn("Vous n'êtes pas autorisé à accéder à cette ressource !");

        $zip = new Zip($encryption);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $encryptions = new Encryption($parameterBag);

        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $controller->setContainer($container);

        return $controller;
    }

    public function testGetDceConnectedAction()
    {
        ob_start();
        $controller = $this->getControllerDceConnected();
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);

        $searchAgent = $this->createMock(SearchAgent::class);

        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller->getDceAction($request, $espaceDocumentaireUtil);
        $this->assertEquals(1207, ob_get_length());
        $this->assertStringContainsString('DCE', ob_get_contents());
        $this->assertStringContainsString('test.zip', ob_get_contents());
        ob_end_clean();
    }

    private function getControllerDumeAcheteurNotConnected()
    {
        $this->setUpNotConnected();

        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', []);
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Vous n\'êtes pas autorisé à accéder à cette ressource !');
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($serializer);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth),
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetDumeAcheteurNotConnectedAction()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerDumeAcheteurNotConnected();
        $controller->getDUMEAcheteurAction($request);
    }

    protected function getControllerForDumeAcheteurConnectedSansFichier()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $offre = new Offre();
        $offre->setOrganisme('pmi-pmi-1');
        $offre->setConsultationId(1600);

        $ids = [0 => ['blobId' => 1530, 'blobXmlId' => 1531]];
        $tdumeRepo = $this->createMock(TDumeContexteRepository::class);

        $file1 = new BloborganismeFile();
        $file1->setName('fywhw9hj.pdf');
        $file1->setOrganisme('pmi-min-1');
        $file1->setChemin('2019/11/07/pmi-min-1/files/');

        $file2 = new BloborganismeFile();
        $file1->setName('fywhw9hj.xml');
        $file1->setOrganisme('pmi-min-1');
        $file1->setChemin('2019/11/07/pmi-min-1/files/');
        $files = [0 => $file1, 1 => $file2];
        $blobRepo = $this->createMock(BloborganismeFileRepository::class);
        $tdumeRepo->expects($this->any())
            ->method('findBy')
            ->willReturn($files);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $tdumeRepo, $blobRepo);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Le DUME Acheteur n\'est pas encore publié');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetDumeAcheteurConnectedSansFichierPublieAction()
    {
        ob_start();
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerForDumeAcheteurConnectedSansFichier();
        $controller->getDumeAcheteurAction($request);
        $this->assertEquals(356, ob_get_length());
        $this->assertStringContainsString('DUME Acheteur', ob_get_contents());
        $this->assertStringContainsString('Le DUME Acheteur n\'est pas encore publié', ob_get_contents());
        ob_end_clean();
    }

    protected function prepareGetPiecesConsultationAction()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $consultation = $this->createMock(Consultation::class);
        $consultation->setAlloti(false);
        $repositoryConsultation = $this->createMock(ObjectRepository::class);
        $repositoryConsultation->expects($this->any())
            ->method('find')
            ->with(1600)
            ->willReturn($consultation);

        $offre = new Offre();
        $offre->setOrganisme('pmi-pmi-1');
        $offre->setConsultationId(6435);

        $dumeEntreprise = new TDumeNumero();
        $dumeEntreprise->setBlobId(1531);
        $dumeEntreprise->setBlobIdXml(1530);
        $dumeEntreprise->setIdDumeContexte(6435);
        $dumeEntreprise->setNumeroDumeNational('njdkjdhsfhds');

        $repositoryCandidature = $this->createMock(TCandidatureRepository::class);
        $repositoryCandidature->expects($this->any())
            ->method('getIdContexte')
            ->willReturn($dumeEntreprise);

        $repositoryCandidature = $this->createMock(TDumeNumeroRepository::class);
        $repositoryCandidature->expects($this->any())
            ->method('findOneBy')
            ->willReturn([0 => 6435]);

        $enveloppeCandidature = $enveloppeOffre = $enveloppeAnonymat = [];
        $repositoryEnveloppe = $this->createMock(EnveloppeFichierRepository::class);

        $repositoryEnveloppe->expects($this->any())
            ->method('getEnveloppeFichiersByTypeEnveloppeAndOffreId')
            ->willReturnOnConsecutiveCalls($enveloppeCandidature, $enveloppeOffre, $enveloppeAnonymat);

        $repositoryEnveloppeFermer = $this->createMock(EnveloppeFichierRepository::class);
        $repositoryEnveloppeFermer->expects($this->any())
            ->method('getEnveloppeFichiersFermerByTypeEnveloppeAndOffreId')
            ->willReturnOnConsecutiveCalls($enveloppeCandidature, $enveloppeOffre, $enveloppeAnonymat);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryAgent,
                $repositoryHabilitationAgent,
                $repositoryConsultation,
                $repositoryCandidature,
                $repositoryCandidature,
                $repositoryEnveloppeFermer,
                $repositoryEnveloppe,
                $repositoryEnveloppeFermer,
                $repositoryEnveloppe,
                $repositoryEnveloppeFermer,
                $repositoryEnveloppe
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Le DUME Acheteur n\'est pas encore publié');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls(
                $configuration,
                [],
                $serializer
            );
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    private function getControllerPubNotConnected()
    {
        $this->setUpNotConnected();

        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', []);
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Vous n\'êtes pas autorisé à accéder à cette ressource !');
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($serializer);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth),
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetPubNotConnectedAction()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerPubNotConnected();
        $controller->getPiecesDePubliciteAction($request, $espaceDocumentaireUtil);
    }

    private function getControllerAutrePieceNotConnected()
    {
        $this->setUpNotConnected();

        $searchAgent = $this->createMock(SearchAgent::class);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', []);
        $container = $this->createMock(ContainerInterface::class);
        $em = $this->createMock(EntityManager::class);
        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Vous n\'êtes pas autorisé à accéder à cette ressource !');
        $container->expects($this->any())
            ->method('get')
            ->willReturnOnConsecutiveCalls($serializer);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth),
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetAutrePieceNotConnectedAction()
    {
        $this->expectException(ApiProblemForbiddenException::class);
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller = $this->getControllerPubNotConnected();
        $controller->getAutresPiecesConsultationAction($request, $espaceDocumentaireUtil);
    }

    protected function getAucuneAutrePieceControllerForConnected()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $autrePieces = [];

        $repositoryAutrePiece = $this->createMock(ObjectRepository::class);
        $repositoryAutrePiece->expects($this->any())
                ->method('findBy')
                ->with(['consultationId' => 1600])
                ->willReturn($autrePieces);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
                ->method('getRepository')
                ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $repositoryAutrePiece);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucune pièce');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);

        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testGetAucuneAutrePieceConnectedAction()
    {
        ob_start();
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getAucuneAutrePieceControllerForConnected();
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller->getAutresPiecesConsultationAction($request, $espaceDocumentaireUtil);
        $this->assertEquals(375, ob_get_length());
        $this->assertStringContainsString('AUTRESPIECESCONSULTATION', ob_get_contents());
        $this->assertStringContainsString('Aucune pièce', ob_get_contents());
        ob_end_clean();
    }

    protected function getAutrePieceControllerForConnected()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $autrePieces = new ArrayCollection();
        $autrePiece = new AutrePieceConsultation();
        $class = new \ReflectionClass($autrePiece);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($autrePiece, 1);
        $autrePiece->setBlobId(1);
        $autrePiece->setConsultationId(1);
        $autrePieces->add($autrePiece);

        $repositoryAutrePiece = $this->createMock(ObjectRepository::class);
        $repositoryAutrePiece->expects($this->any())
            ->method('findBy')
            ->with(['consultationId' => 1600])
            ->willReturn($autrePieces);

        $blobOrganisme = new BloborganismeFile();
        $class = new \ReflectionClass($blobOrganisme);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($blobOrganisme, 1);

        $blobOrganisme->setName('test');
        $repositoryOrganismeBlobFile = $this->createMock(ObjectRepository::class);
        $repositoryOrganismeBlobFile->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($blobOrganisme);

        $echangeDocBlob = new EchangeDocBlob();
        $echangeDocBlob->setBlobOrganisme($blobOrganisme);

        $repositoryEchangeDocBlob = $this->createMock(ObjectRepository::class);
        $repositoryEchangeDocBlob->expects($this->any())
            ->method('findBy')
            ->with(['blobOrganisme' => $blobOrganisme->getId()])
            ->willReturn([$echangeDocBlob]);

        $mount = $this->createMock(MountManager::class);

        $mount->expects($this->any())
            ->method('getFileSize')
            ->willReturn(1000);

        $mount->expects($this->any())
            ->method('getAbsolutePath')
            ->willReturn(__DIR__ . '/../../data/test.zip');

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryAgent,
                $repositoryHabilitationAgent,
                $repositoryAutrePiece,
                $repositoryOrganismeBlobFile,
                $repositoryEchangeDocBlob
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucun dépôt n’a été effectué sur cette consultation.');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);

        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    protected function postAutrePieceControllerForConnected()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $bloborganismeFile = new BloborganismeFile();
        $class = new \ReflectionClass($bloborganismeFile);
        $property = $class->getProperty('id');
        $property->setAccessible(true);
        $property->setValue($bloborganismeFile, 1);

        $repositoryblobOrganisme = $this->createMock(ObjectRepository::class);
        $repositoryblobOrganisme->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($bloborganismeFile);

        $consultation = new Consultation();
        $classConsultation = new \ReflectionClass($consultation);
        $propertyConsultation = $classConsultation->getProperty('id');
        $propertyConsultation->setAccessible(true);
        $propertyConsultation->setValue($consultation, 1);

        $repositoryConsultation = $this->createMock(ObjectRepository::class);
        $repositoryConsultation->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1600])
            ->willReturn($consultation);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryHabilitationAgent,
                $repositoryblobOrganisme,
                $repositoryConsultation
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('ajout du document réussi');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $mountManager = $this->createMock(MountManager::class);
        $mountManager->expects($this->any())
            ->method('getFileSize')
            ->willReturn(1000);

        $mountManager->expects($this->any())
            ->method('getAbsolutePath')
            ->willReturn(__DIR__ . '/../../data/test.zip');
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $container->expects($this->any())
            ->method('get')
            ->willReturn(
                $configuration,
                $mountManager,
                $serializer
            );

        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testPostAutrePieceConnectedAction()
    {
        ob_start();
        $uploadedFile = new UploadedFile(__DIR__ . '/../../data/test.zip', 'test.zip');
        $request = new Request(
            [
                'idConsultation' => '1600',
            ],
            [
                'POST',
            ],
            [],
            [],
            [
                [
                    $uploadedFile,
                ],
            ],
            [],
            []
        );
        $controller = $this->postAutrePieceControllerForConnected();
        $espaceDocumentaireUtil = $this->createMock(EspaceDocumentaireUtil::class);
        $controller->postAutresPiecesConsultationAction($request, $espaceDocumentaireUtil);
        $this->assertEquals(375, ob_get_length());
        $this->assertStringContainsString('AUTRES_PIECES', ob_get_contents());
        $this->assertStringContainsString('ajout du document réussi', ob_get_contents());
        ob_end_clean();
    }

    protected function deleteAutrePieceControllerForConnected()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $autrePieceConsultation = new AutrePieceConsultation();

        $repositoryAutrePieceConsultation = $this->createMock(ObjectRepository::class);
        $repositoryAutrePieceConsultation->expects($this->any())
            ->method('findOneBy')
            ->with([
                'blobId' => 1600,
                'consultationId' => 1600,
            ])
            ->willReturn($autrePieceConsultation);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls(
                $repositoryHabilitationAgent,
                $repositoryAutrePieceConsultation
            );

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('delete du document réussi');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);

        $mount = $this->createMock(MountManager::class);
        $container->expects($this->any())
            ->method('get')
            ->willReturn(
                $configuration,
                $mount,
                $serializer
            );

        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setContainer($container);

        return $controller;
    }

    public function testDeleteAutrePieceConnectedAction()
    {
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $encrypte = new Encryption($parameterBag);
        $blobId = $encrypte->cryptId(1600);
        ob_start();
        $request = new Request(
            [
                'idConsultation' => '1600',
                'blobId' => $blobId,
            ],
            [
                'DELETE',
            ],
            [],
            [],
            [],
            [],
            []
        );
        $controller = $this->deleteAutrePieceControllerForConnected();

        $controller->deleteAutresPiecesConsultationAction($request);
        $this->assertEquals(375, ob_get_length());
        $this->assertStringContainsString('autre pièce consultation', ob_get_contents());
        $this->assertStringContainsString('delete du document réussi', ob_get_contents());
        ob_end_clean();
    }

    private function getControllerPlisDesAttributairesConnectedNoData()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $offre = new Offre();
        $offre->setOrganisme('pmi-pmi-1');
        $offre->setConsultationId(1600);

        $arrayOffres = [];

        $repositoryOffre = $this->createMock(OffreRepository::class);
        $repositoryOffre->expects($this->any())
            ->method('getAttributaire')
            ->willReturn($arrayOffres);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $repositoryOffre);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucun attributaire n\'a été désigné pour cette consultation');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $controller->setContainer($container);

        return $controller;
    }

    public function testPlisDesAttributairesConnectedNoData()
    {
        ob_start();
        $request = new Request(['idConsultation' => '1600'], [], [], [], [], [], []);
        $controller = $this->getControllerPlisDesAttributairesConnectedNoData();
        $controller->getPlisDesAttributaires($request);
        $this->assertEquals(378, ob_get_length());
        $this->assertStringContainsString('ATTRIBUTAIRES', ob_get_contents());
        $this->assertStringContainsString(
            'Aucun attributaire n\'a été désigné pour cette consultation',
            ob_get_contents()
        );
        ob_end_clean();
    }

    private function getControllerCreateDownload()
    {
        $organisme = new Organisme();
        $organisme->setAcronyme('pmi-pmi-1');
        $agent = new Agent();
        $agent->setOrganisme($organisme);
        $agent->setId(1);

        $searchAgent = $this->createMock(SearchAgent::class);
        $searchAgent->expects($this->any())
            ->method('getConsultationsByQuerySf')
            ->willReturn(['consultation' => ['ref' => 1]]);
        $session = new Session(new MockArraySessionStorage());
        $session->set('contexte_authentification', ['id' => 1, 'organisme' => $organisme]);

        $repositoryAgent = $this->createMock(ObjectRepository::class);
        $repositoryAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['id' => 1])
            ->willReturn($agent);

        $habilitationAgent = new HabilitationAgent();
        $habilitationAgent->setEspaceDocumentaireConsultation(true);
        $repositoryHabilitationAgent = $this->createMock(ObjectRepository::class);
        $repositoryHabilitationAgent->expects($this->any())
            ->method('findOneBy')
            ->with(['agent' => 1])
            ->willReturn($habilitationAgent);

        $offre = new Offre();
        $offre->setOrganisme('pmi-pmi-1');
        $offre->setConsultationId(1600);
        $repositoryEnveloppe = $this->createMock(EnveloppeRepository::class);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnOnConsecutiveCalls($repositoryHabilitationAgent, $repositoryEnveloppe);

        $container = $this->createMock(ContainerInterface::class);
        $perimetreAgent = new Authorization($em, $searchAgent, $session, $container, $this->security, $this->auth);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $parameterBag->expects($this->any())->method('get')->willReturn('a parameter');
        $encryptions = new Encryption($parameterBag);
        $zip = new Zip($encryptions);
        $translatorInterface = $this->createMock(TranslatorInterface::class);
        $translatorInterface->expects($this->any())
            ->method('trans')
            ->willReturn('Aucun attributaire n\'a été désigné pour cette consultation');

        $logger = $this->createMock(LoggerInterface::class);
        $loggerController = $this->createMock(Logger::class);
        $contratService = $this->createMock(ContratService::class);
        $mountManager = $this->createMock(MountManager::class);
        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $webServiceExec = $this->createMock(WebServicesExec::class);
        $serializer = new Serializer($container, $em, $logger, $contratService, $parameterBag, $webServiceExec);
        $configuration = $this->createMock(AtexoConfiguration::class);
        $configuration->expects($this->any())
            ->method('hasConfigOrganisme')
            ->willReturn(true);
        $container->expects($this->any())
            ->method('get')
            ->willReturn($configuration, $serializer);
        $atexoUtil = $this->createMock(AtexoUtil::class);
        $atexoUtil->expects($this->any())
            ->method('getArrayArbo')
            ->willReturn(['toto' => ['tata']]);

        $controller = new ApiEspaceDocumentaireController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $serializer,
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $perimetreAgent,
            $session,
            $encryptions,
            $translatorInterface,
            $zip,
            $atexoUtil,
            $mountManager,
            $loggerController,
            $this->security,
            $this->createMock(AtexoConfiguration::class),
            $this->createMock(AtexoFichierOrganisme::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $controller->setContainer($container);

        return $controller;
    }

    /**
     * @throws \JsonException
     */
    public function testCreateDownloadOKAction()
    {
        ob_start();
        $request = new Request(
            ['idConsultation' => '1600'],
            ['data' => ['0' => 'TOTO##TITI##TATA']],
            [],
            [],
            [],
            [],
            []
        );

        $message = new GenerationArchiveDocumentaire(
            '1600',
            '1',
            base64_encode('{"toto":["tata"]}')
        );
        $envelope = new Envelope($message);

        $messageBusInterface = $this->getMockBuilder(MessageBusInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $messageBusInterface->expects($this->exactly(1))
            ->method('dispatch')->with($message)->willReturn($envelope);


        $controller = $this->getControllerCreateDownload();
        $controller->createDownloadAction($request, $messageBusInterface);
        $this->assertEquals(340, ob_get_length());

        $this->assertStringContainsString('CREATEDOWNLOAD', ob_get_contents());
        $this->assertStringContainsString('message>OK</message', ob_get_contents());
        ob_end_clean();
    }
}
