<?php

namespace Tests\Unit\Controller\DonneesEssentielles;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\SsoTiers;
use App\Repository\OrganismeRepository;
use App\Repository\SsoTiersRepository;
use App\Controller\Api\DonneesEssentielles\ContratController;
use App\Controller\Api\DonneesEssentielles\ContratTokenlessController;
use App\Entity\ContratTitulaire;
use App\Model\DonneesEssentielles\Marche;
use App\Model\DonneesEssentielles\Marches;
use App\Entity\MarchePublie;
use App\Repository\ContratTitulaireRepository;
use App\Repository\MarchePublieRepository;
use App\Service\Api\Serializer as ApiSerializer;
use App\Service\Api\StatistiqueService;
use App\Service\AtexoAgent;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Service\AtexoInscrit;
use App\Service\AtexoUtil;
use App\Service\ModificationContrat\ModificationContratService;
use App\Service\OrganismeService;
use App\Service\WebServices\WebServicesExec;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use App\Repository\ServiceRepository;
use App\Service\AgentTechniqueTokenService;
use App\Service\ContratService;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Tests\Unit\AtexoTestCase;

class ContratControllerTest extends AtexoTestCase
{
    public function setUp(): void
    {
        $this->xsd = realpath(
            __DIR__ . '/../../../../public/app/xsd/marche_etendu.xsd'
        );
        $this->pivot_xsd = __DIR__ . '/../../../../public/app/xsd/pivot.xsd';
        $this->etendu_xsd = __DIR__ . '/../../../../public/app/xsd/marche_etendu.xsd';

        parent::setUp();
    }

    public function testFormatPivotAction()
    {
        $controller = new ContratController(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(StatistiqueService::class),
            $this->createMock(ContratService::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $request = new Request();

        $em = $this->createMock(EntityManager::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if (SsoTiers::class == $repository) {
                        $repository = $this->createMock(SsoTiersRepository::class);

                        $entity = new SsoTiers();
                        $repository->expects($this->any())
                            ->method('checkToken')
                            ->willReturn($entity);

                        return $repository;
                    }

                    if (ContratTitulaire::class == $repository) {
                        $repository = $this->createMock(ContratTitulaireRepository::class);

                        $entity = new ContratTitulaire();
                        $repository->expects($this->any())
                            ->method('getContracts')
                            ->willReturn([$entity]);

                        return $repository;
                    }
                }
            );

        $controller->setEm($em);

        $container = $this->createMock(ContainerInterface::class);
        $container->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($serviceName) {
                    static $callCount = [];
                    $callCount[$serviceName] = !isset($callCount[$serviceName]) ? 1 : ++$callCount[$serviceName];

                    if ('agent_technique_token' === $serviceName) {
                        $agentTechniqueService = $this->createMock(AgentTechniqueTokenService::class);

                        $agentTechniqueService->expects($this->any())
                            ->method($this->equalTo('getAgentFromToken'))
                            ->willReturn(new Agent());

                        return $agentTechniqueService;
                    }
                }
            ));

        $uuid = '12345678-1234-1234-1234-123456789012';
        $contrat = new ContratTitulaire();
        $contrat->setUuid($uuid);
        $contrat->setId(1);
        $contrat->setLieuExecution('test');
        $controller->setContainer($container);
        $marches = new Marches([]);

        $marche = $this->getMarche($uuid, $contrat);

        $marches->addMarche($marche);
        $marches->addMarche($marche);

        $service = $this->createMock(ContratService::class);
        $service->expects($this->any())
            ->method('listMarcheForContrat')
            ->willReturn($marches->getMarches());
        $service->expects($this->any())
            ->method('getContracts')
            ->willReturn([['contrat' => $contrat]]);

        $controller->setService($service);

        $response = $controller->formatPivotAction(
            $request
        );

        $xml = $response->getContent();
        $dom = new \DOMDocument();
        $dom->loadXML($xml);

        $actual = $dom->schemaValidate($this->pivot_xsd);

        $this->assertTrue($actual);
    }

    /**
     * @group test
     * @return void
     * @throws \Exception
     */
    public function testFormatEtenduAction()
    {
        $controller = new ContratController(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(StatistiqueService::class),
            $this->createMock(ContratService::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $request = new Request();

        $em = $this->createMock(EntityManager::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if ($repository == SsoTiers::class) {
                        $repository = $this->createMock(SsoTiersRepository::class);

                        $entity = new SsoTiers();
                        $repository
                            ->method('checkToken')
                            ->willReturn($entity);
                        return $repository;
                    }

                    if ($repository == ContratTitulaire::class) {
                        $repository = $this->createMock(ContratTitulaireRepository::class);

                        $entity = new ContratTitulaire();
                        $repository->expects($this->any())
                            ->method('getContracts')
                            ->willReturn([$entity]);
                        return $repository;
                    }
                }
            );

        $controller->setEm($em);

        $container = $this->createMock(ContainerInterface::class);
        $container->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($serviceName) {

                    static $callCount = array();
                    $callCount[$serviceName] = !isset($callCount[$serviceName]) ? 1 : ++$callCount[$serviceName];

                    if ($serviceName === 'agent_technique_token') {
                        $agentTechniqueService = $this->createMock(AgentTechniqueTokenService::class);

                        $agentTechniqueService->expects($this->any())
                            ->method($this->equalTo('getAgentFromToken'))
                            ->willReturn(new Agent());

                        return $agentTechniqueService;
                    }
                }
            ));

        $uuid = '12345678-1234-1234-1234-123456789012';
        $contrat = new ContratTitulaire();
        $contrat->setUuid($uuid);
        $contrat->setId(1);
        $contrat->setLieuExecution('test');
        $controller->setContainer($container);
        $marches = new Marches([]);

        $marche = $this->getMarche($uuid, $contrat, true);

        $marches->addMarche($marche);
        $marches->addMarche($marche);

        $service = $this->createMock(ContratService::class);
        $service->expects($this->any())
            ->method('listMarcheForContrat')
            ->willReturn($marches->getMarches());
        $service->expects($this->any())
            ->method('getContracts')
            ->willReturn([['contrat' => $contrat]]);

        $controller->setService($service);

        $response = $controller->formatEtenduAction(
            $request,
            $this->createMock(WebServicesExec::class),
            $this->createMock(DenormalizerInterface::class)
        );

        $xml = $response->getContent();
        $dom = new \DOMDocument();
        $dom->loadXML($xml);

        $actual = $dom->schemaValidate($this->etendu_xsd);

        $this->assertTrue($actual);
    }

    public function getMarche($uuid, ?ContratTitulaire $contrat, $isExtented = false)
    {
        $marche = new Marche();
        $marche->setUuid($uuid);
        $marche->setId(1234567);
        $marche->setNature('Marché');
        $nomAcheteur = 'NomAcheteur - ' . str_repeat('123456789-', 30);
        $atexoUtil = parent::getAtexoUtil();



        $acheteur = (new \App\Model\DonneesEssentielles\Marche\AcheteurAType())
            ->setId(12345678901234)
            ->setNom($nomAcheteur);
        if ($isExtented) {
            $organisme = new Organisme();
            $class = new ReflectionClass($organisme);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($organisme, 999999);
            $organisme->setDenominationOrg('test organisme');

            $repo = $this->createMock(OrganismeRepository::class);

            $repo->expects($this->any())
                ->method('findOneBy')
                ->with($this->anything())
                ->willReturn($organisme);

            $service = new Service();
            $class = new ReflectionClass($service);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($service, 999999);
            $service->setLibelle('test service');

            $repo = $this->createMock(ServiceRepository::class);

            $repo->expects($this->any())
                ->method('findOneBy')
                ->with($this->anything())
                ->willReturn($service);

            $acheteur
                ->setIdOrganisme($organisme->getId())
                ->setLibelleOrganisme($organisme->getDenominationOrg())
                ->setidEntiteAchat(
                    ($service instanceof Service) ? $service->getId() : ''
                )
                ->setLibelleEntiteAchat(
                    ($service instanceof Service) ? $service->getLibelle() : ''
                )
                ->setAccessChorus(
                    ($service instanceof Service) ? $service->getAccesChorus() : ''
                )
                ->setIdAgent(12345789)
                ->setPrenomAgent('Prenom')
                ->setNomAgent('Nom')
                ->setEmailAgent('email@agent.com')
                ->setLoginAgent('agentLogin');
        }

        $marche->setAcheteur(
            $acheteur
        );
        $marche->setDureeMois(12);
        $marche->setValeurGlobale(123);
        $marche->setObjet('objet');
        $marche->setLieuExecution(
            (new Marche\LieuExecutionAType())
                ->setCode('code')
                ->setTypeCode('Code postal')
                ->setNom('nom')
        );
        $marche->setTypeContrat(
            'MARCHE_PUBLIC'
        );
        $marche->setCodeCPV('00000000-1');
        $marche->setProcedure('Procédure adaptée');
        $marche->setDateNotification(new \DateTime());
        $marche->setDateSignature(new \DateTime());
        $marche->setDateDebutExecution(new \DateTime());
        $marche->setDatePublicationDonnees(new \DateTime());
        $marche->setDateTransmissionDonneesEtalab(new \DateTime());
        $marche->setMontantSubventionPublique(123);
        $marche->setMontant(0.01);
        $marche->setFormePrix('Ferme');
        $marche->setDonneesExecution(new \App\Model\DonneesEssentielles\Marche\DonneeExecution());

        $titulaireAType =
            (new \App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType())
            ->setId(1)
            ->setTypeIdentifiant('SIRET')
            ->setDenominationSociale('123456789')
        ;

        if ($isExtented) {
            $titulaireAType->setContact(
                (new Marche\ContactTitulaireType())
                    ->setId(42)
                    ->setPrenom('prenomContrat')
                    ->setNom('nomContrat')
                    ->setEmail('emailContrat')
            );
        }

        $marche->setTitulaires([$titulaireAType]);
        $marche->setModifications([]);

        $marche->setDonneesComplementaires(
            (new \App\Model\DonneesEssentielles\Marche\DonneesComplementaires())
                ->setClauseSociale(true)
                ->setProcedurePassation('proc')
                ->setNaturePassation('nature')
                ->setAccordCadreAvecMarcheSubsequent(false)
                ->setIdAccordCadre(1)
                ->setSiretPAAccordCadre('siret')
                ->setCodesCPVSec('')
                ->setNbTotalPropositionsRecu(0)

                ->setNbTotalPropositionsDemat(0)
                ->setClauseEnvironnementale(false)
                ->setCcagReference('')
                ->setConsultation(
                    (new \App\Model\DonneesEssentielles\Marche\ConsultationAType())->setId(1)->setNumero(1)
                )

                ->setLot(
                    (new \App\Model\DonneesEssentielles\Marche\LotAType())
                        ->setNumeroLot(1)->setIntituleLot('')
                )
                ->setNumerosContrat(new \App\Model\DonneesEssentielles\Marche\NumerosContratType())
                ->setContratsChapeaux(
                    (new \App\Model\DonneesEssentielles\Marche\ContratsChapeauxType())
                        ->setContratChapeauMultiAttributaires(
                            new \App\Model\DonneesEssentielles\Marche\ContratChapeauMultiAttributairesType()
                        )
                        ->setContratChapeauAcSad(new \App\Model\DonneesEssentielles\Marche\ContratChapeauAcSadType())
                )
        );

        return $marche;
    }

    public function testExtraireAvecCriteres()
    {
        $request = new Request();

        $serviceRepository = $this->createMock(ServiceRepository::class);
        $entity = new Service();
        $entity->setId(1);
        $serviceRepository->expects($this->any())
            ->method('findBy')
            ->willReturn($entity)
        ;

        $marchePublieRepository = $this->createMock(MarchePublieRepository::class);
        $entity = new MarchePublie();
        $entity->setServiceId(1);
        $marchePublieRepository->expects($this->once())
            ->method('getListeMarchesPublies')
            ->willReturn([$entity]);


        $contratTitulaireRepository = $this->createMock(ContratTitulaireRepository::class);
        $entity = new ContratTitulaire();
        $contratTitulaireRepository->expects($this->any())
            ->method('getMarchesByCriteria')
            ->willReturn([$entity]);

        $em = $this->createMock(EntityManager::class);
        $em
            ->expects($this->any())
            ->method('getRepository')
            ->withConsecutive(
                [Service::class],
                [MarchePublie::class],
                [ContratTitulaire::class]
            )
            ->willReturnOnConsecutiveCalls(
                $serviceRepository,
                $marchePublieRepository,
                $contratTitulaireRepository,
            )
        ;

        $container = $this->createMock(ContainerInterface::class);
        $marches = new Marches([]);
        $marches->addMarche(new Marche());
        $marches->addMarche(new Marche());

        $service = $this->createMock(ContratService::class);
        $service->expects($this->any())
            ->method('getFormatPivot')
            ->willReturn($marches);

        $controller = new ContratTokenlessController(
            $em,
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(StatistiqueService::class),
            $this->createMock(ContratService::class),
            $this->createMock(ParameterBagInterface::class),
        );

        $controller->setService($service);

        $idEntitePublique = 'pmi-min-1';
        $idEntiteAchat = 0;
        $inclureDescendances = 1;
        $annee = 2019;
        $nomAttributaire = false;
        $idCategorie = false;
        $montantMin = false;
        $montantMax = false;
        $lieuxExecution = false;
        $codesCpv = false;
        $dateNotifMin = false;
        $dateNotifMax = false;
        $motsCles = false;
        $concession = 0;

        $response = $controller->extraireAvecCriteres(
            $idEntitePublique,
            $idEntiteAchat,
            $inclureDescendances,
            $annee,
            $nomAttributaire,
            $idCategorie,
            $montantMin,
            $montantMax,
            $lieuxExecution,
            $codesCpv,
            $dateNotifMin,
            $dateNotifMax,
            $motsCles,
            $concession
        );

        $xml = $response->getContent();
        $dom = new \DOMDocument();
        $dom->loadXML($xml);
        $expected = true;

        $actual = $dom->schemaValidate($this->pivot_xsd);

        $this->assertSame($expected, $actual);
    }

    public function getMarcheServiceIdIsNull($uuid, ?ContratTitulaire $contrat, $isExtented = false)
    {
        $marche = new Marche();
        $marche->setUuid($uuid);
        $marche->setId(1234567);
        $marche->setNature('Marché');
        $nomAcheteur = 'NomAcheteur - ' . str_repeat('123456789-', 30);
        $atexoUtil = parent::getAtexoUtil();



        $acheteur = (new \App\Model\DonneesEssentielles\Marche\AcheteurAType())
            ->setId(12345678901234)
            ->setNom($nomAcheteur);
        if ($isExtented) {
            $organisme = new Organisme();
            $class = new ReflectionClass($organisme);
            $property = $class->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($organisme, 999999);
            $organisme->setDenominationOrg('test organisme');

            $repo = $this->createMock(OrganismeRepository::class);

            $repo->expects($this->any())
                ->method('findOneBy')
                ->with($this->anything())
                ->willReturn($organisme);



            $repo = $this->createMock(ServiceRepository::class);

            $service = $repo->expects($this->any())
                ->method('findOneBy')
                ->with($this->anything())
                ->willReturn(null);

            $acheteur
                ->setIdOrganisme($organisme->getId())
                ->setLibelleOrganisme($organisme->getDenominationOrg())
                ->setidEntiteAchat(
                    ($service instanceof Service) ? $service->getId() : null
                )
                ->setLibelleEntiteAchat(
                    ($service instanceof Service) ? $service->getLibelle() : null
                )
                ->setAccessChorus(
                    ($service instanceof Service) ? $service->getAccesChorus() : null
                )
                ->setIdAgent(12345789)
                ->setPrenomAgent('Prenom')
                ->setNomAgent('Nom')
                ->setEmailAgent('email@agent.com')
                ->setLoginAgent('agentLogin');
        }

        $marche->setAcheteur(
            $acheteur
        );
        $marche->setDureeMois(12);
        $marche->setValeurGlobale(123);
        $marche->setObjet('objet');
        $marche->setLieuExecution(
            (new Marche\LieuExecutionAType())
                ->setCode('code')
                ->setTypeCode('Code postal')
                ->setNom('nom')
        );
        $marche->setTypeContrat(
            'MARCHE_PUBLIC'
        );
        $marche->setCodeCPV('00000000-1');
        $marche->setProcedure('Procédure adaptée');
        $marche->setDateNotification(new \DateTime());
        $marche->setDateSignature(new \DateTime());
        $marche->setDateDebutExecution(new \DateTime());
        $marche->setDatePublicationDonnees(new \DateTime());
        $marche->setDateTransmissionDonneesEtalab(new \DateTime());
        $marche->setMontantSubventionPublique(123);
        $marche->setMontant(0.01);
        $marche->setFormePrix('Ferme');
        $marche->setDonneesExecution(new \App\Model\DonneesEssentielles\Marche\DonneeExecution());

        $titulaireAType =
            (new \App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType())
                ->setId(1)
                ->setTypeIdentifiant('SIRET')
                ->setDenominationSociale('123456789')
        ;

        if ($isExtented) {
            $titulaireAType->setContact(
                (new Marche\ContactTitulaireType())
                    ->setId(42)
                    ->setPrenom('prenomContrat')
                    ->setNom('nomContrat')
                    ->setEmail('emailContrat')
            );
        }

        $marche->setTitulaires([$titulaireAType]);
        $marche->setModifications([]);

        $marche->setDonneesComplementaires(
            (new \App\Model\DonneesEssentielles\Marche\DonneesComplementaires())
                ->setClauseSociale(true)
                ->setProcedurePassation('proc')
                ->setNaturePassation('nature')
                ->setAccordCadreAvecMarcheSubsequent(false)
                ->setIdAccordCadre(1)
                ->setSiretPAAccordCadre('siret')
                ->setCodesCPVSec('')
                ->setNbTotalPropositionsRecu(0)

                ->setNbTotalPropositionsDemat(0)
                ->setClauseEnvironnementale(false)
                ->setCcagReference('')
                ->setConsultation(
                    (new \App\Model\DonneesEssentielles\Marche\ConsultationAType())->setId(1)->setNumero(1)
                )

                ->setLot(
                    (new \App\Model\DonneesEssentielles\Marche\LotAType())
                        ->setNumeroLot(1)->setIntituleLot('')
                )
                ->setNumerosContrat(new \App\Model\DonneesEssentielles\Marche\NumerosContratType())
                ->setContratsChapeaux(
                    (new \App\Model\DonneesEssentielles\Marche\ContratsChapeauxType())
                        ->setContratChapeauMultiAttributaires(
                            new \App\Model\DonneesEssentielles\Marche\ContratChapeauMultiAttributairesType()
                        )
                        ->setContratChapeauAcSad(new \App\Model\DonneesEssentielles\Marche\ContratChapeauAcSadType())
                )
        );

        return $marche;
    }

    public function testFormatEtenduActionServiceIdIsNull()
    {
        $controller = new ContratController(
            $this->createMock(EntityManagerInterface::class),
            $this->createMock(AgentTechniqueTokenService::class),
            $this->createMock(ApiSerializer::class),
            $this->createMock(AtexoAgent::class),
            $this->createMock(AtexoEntreprise::class),
            $this->createMock(AtexoEtablissement::class),
            $this->createMock(AtexoInscrit::class),
            $this->createMock(OrganismeService::class),
            $this->createMock(ModificationContratService::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(StatistiqueService::class),
            $this->createMock(ContratService::class),
            $this->createMock(ParameterBagInterface::class),
        );
        $request = new Request();

        $em = $this->createMock(EntityManager::class);

        $em->expects($this->any())
            ->method('getRepository')
            ->willReturnCallback(
                function ($repository) {
                    if ($repository == SsoTiers::class) {
                        $repository = $this->createMock(SsoTiersRepository::class);

                        $entity = new SsoTiers();
                        $repository
                            ->method('checkToken')
                            ->willReturn($entity);
                        return $repository;
                    }

                    if ($repository == ContratTitulaire::class) {
                        $repository = $this->createMock(ContratTitulaireRepository::class);

                        $entity = new ContratTitulaire();
                        $repository->expects($this->any())
                            ->method('getContracts')
                            ->willReturn([$entity]);
                        return $repository;
                    }
                }
            );

        $controller->setEm($em);

        $container = $this->createMock(ContainerInterface::class);
        $container->expects($this->any())
            ->method('get')
            ->with($this->anything())
            ->will($this->returnCallback(
                function ($serviceName) {

                    static $callCount = array();
                    $callCount[$serviceName] = !isset($callCount[$serviceName]) ? 1 : ++$callCount[$serviceName];

                    if ($serviceName === 'agent_technique_token') {
                        $agentTechniqueService = $this->createMock(AgentTechniqueTokenService::class);

                        $agentTechniqueService->expects($this->any())
                            ->method($this->equalTo('getAgentFromToken'))
                            ->willReturn(new Agent());

                        return $agentTechniqueService;
                    }
                }
            ));

        $uuid = '12345678-1234-1234-1234-123456789012';
        $contrat = new ContratTitulaire();
        $contrat->setUuid($uuid);
        $contrat->setId(1);
        $contrat->setLieuExecution('test');
        $controller->setContainer($container);
        $marches = new Marches([]);

        $marche = $this->getMarcheServiceIdIsNull($uuid, $contrat, true);

        $marches->addMarche($marche);
        $marches->addMarche($marche);

        $service = $this->createMock(ContratService::class);
        $service->expects($this->any())
            ->method('listMarcheForContrat')
            ->willReturn($marches->getMarches());
        $service->expects($this->any())
            ->method('getContracts')
            ->willReturn([['contrat' => $contrat]]);

        $controller->setService($service);

        $response = $controller->formatEtenduAction(
            $request,
            $this->createMock(WebServicesExec::class),
            $this->createMock(DenormalizerInterface::class)
        );

        $xml = $response->getContent();
        $dom = new \DOMDocument();
        $dom->loadXML($xml);
        $actual = $dom->schemaValidate($this->etendu_xsd);
        $this->assertTrue($actual);
    }
}
