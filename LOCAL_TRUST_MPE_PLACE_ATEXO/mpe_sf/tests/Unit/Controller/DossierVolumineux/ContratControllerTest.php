<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace Tests\Unit\Controller\DossierVolumineux;

use App\Controller\Commun\DossierVolumineuxController;
use App\Service\AtexoUtil;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tests\Unit\AtexoTestCase;

class ContratControllerTest extends AtexoTestCase
{
    /**
     * @dataProvider getDataProvider
     */
    public function testGetRedirectRoute($route, $expected)
    {
        $controller = new DossierVolumineuxController(
            $this->createMock(TranslatorInterface::class),
            $this->createMock(AtexoUtil::class),
            $this->createMock(ParameterBagInterface::class)
        );
        $request = new Request();
        $request->attributes->set('_route', $route);
        $actual = $controller->getRedirectRoute($request);
        $this->assertSame($expected, $actual);
    }

    /**
     * @return array
     */
    public function getDataProvider()
    {
        yield ['/agent/download/id', 'dossier_volumineux_agent_gestion'];
        yield ['/entreprise/download/id', 'dossier_volumineux_entreprise_gestion'];
        yield [null, 'dossier_volumineux_entreprise_gestion'];
    }
}
