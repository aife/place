<?php

namespace Tests\Unit\Model\Messec;

use App\Model\Messec\Message;
use Tests\Unit\MpeEntityTestCase;

class MessageTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Message::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'piecesJointes',
            'identifiantPfEmetteur',
            'identifiantPfDestinataire',
            'nomPfEmetteur',
            'urlPfDestinataire',
            'urlPfEmetteurVisualisation',
            'urlPfDestinataireVisualisation',
            'urlPfReponse',
            'identifiantObjetMetierPlateFormeDestinataire',
            'identifiantObjetMetierPlateFormeEmetteur',
            'identifiantSousObjetMetier',
            'referenceObjetMetier',
            'typeMessage',
            'masquerOptionsEnvoi',
            'cartouche',
            'signatureAvisPassage',
            'nomCompletExpediteur',
            'emailExpediteur',
            'emailReponseExpediteur',
            'emailContactDestinatairePfEmetteur',
            'idMsgInitial',
            'contenu',
            'contenuHTML',
            'contenuText',
            'objet',
            'objetText',
            'reponseAttendue',
            'statut',
            'typesInterdits',
            'destsPfEmettreur',
            'destsPfDestinataire',
            'destinatairesPreSelectionnes',
            'destinatairesBrouillon',
            'emailsAlerteReponse',
            'dateReponseAttendue',
            'reponseLectureBloque',
            'dateReponse',
            'dateReponseAsDate',
            'destinatairesNotifies',
            'maxTailleFichiers',
            'templateMail',
            'templateFige',
            'nomPfDestinataire',
            'urlPfEmetteur',
        ];
    }
}
