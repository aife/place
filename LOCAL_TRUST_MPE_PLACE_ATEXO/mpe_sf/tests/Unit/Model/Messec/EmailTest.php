<?php

namespace Tests\Unit\Model\Messec;

use App\Model\Messec\Email;
use Tests\Unit\MpeEntityTestCase;

class EmailTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return Email::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'message',
            'typeDestinataire',
            'identifiantObjetMetier',
            'identifiantEntreprise',
            'identifiantContact',
            'nomEntreprise',
            'nomContact',
            'email',
            'statutEmail',
            'codeLien',
            'dateDemandeEnvoi',
            'dateEnvoi',
            'dateAR',
            'nombreEssais',
            'erreur',
            'identifiantEntrepriseAR',
            'identifiantContactAR',
            'nomEntrepriseAR',
            'nomContactAR',
            'emailContactAR',
            'reponse',
            'toutesLesReponses',
            'dateModificationBrouillon',
            'connecte',
            'dateRelance',
            'dateEchec',
            'favori',
            'dateDemandeEnvoiAsDate',
            'dateRelanceAsDate',
            'dateEchecAsDate',
            'dateEnvoiAsDate',
            'dateARAsDate',
            'dateModificationBrouillonAsDate',
        ];
    }
}
