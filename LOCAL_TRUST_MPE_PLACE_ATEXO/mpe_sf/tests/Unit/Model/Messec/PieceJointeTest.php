<?php

namespace Tests\Unit\Model\Messec;

use App\Model\Messec\PieceJointe;
use Tests\Unit\MpeEntityTestCase;

class PieceJointeTest extends MpeEntityTestCase
{
    protected function getTestedEntity()
    {
        return PieceJointe::class;
    }

    protected function getBddFieldNames(): array
    {
        return [
            'id',
            'idExterne',
            'reference',
            'nom',
            'chemin',
            'taille',
            'tailleFormatee',
            'dateCreation',
            'contentType',
        ];
    }
}
