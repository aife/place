<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Functional\Security;

use App\Entity\Inscrit;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AuthenticationTest extends WebTestCase
{

    /**
     * @var ObjectManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testPageInfoSite()
    {
        $client = static::createClient();

        $client->request('GET', '/entreprise/footer/info-site');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSelectorTextContains('html h1', 'InfoSite');

        $this->assertSelectorExists(
            'footer input[type=button][value=en]',
            'Un bouton vers la langue EN doit être présent dans le footer'
        );
    }

    public function testPageAccueilEntreprise()
    {
        $client = static::createClient();

        $client->request('GET', '/entreprise');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertSelectorTextContains('html h1', 'Bienvenue sur le PORTAIL D\'AVIS');
    }

    public function testLoginEntrepriseSucceeds()
    {
        /** @var Inscrit $mpUser */
        $mpUser = $this->entityManager
            ->getRepository(Inscrit::class)
            ->findOneBy(['login' => 'mp']);
        $mpUser->setTentativesMdp(0);
        $mpUser->setBloque('0');
        $mpUser->setTypeHash('sha1');
        $mpUser->setMdp(hash('sha1', 'mp'));
        $this->entityManager->persist($mpUser);
        $this->entityManager->flush();

        $client = static::createClient();
        $client->disableReboot();

        $crawler = $client->request('GET', '/entreprise');

        $this->assertSelectorExists(
            'form[name=loginForm]',
            'Le formulaire de connexion doit être présent'
        );

        $client->followRedirects();
        $form = $crawler->selectButton('Se connecter')->form();
        $form['loginform[_username]'] = 'mp';
        $form['loginform[_password]'] = 'mp';
        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorNotExists('.panel-user-infos.alert-danger');
        $this->assertSelectorTextContains('#user-infos', 'PASSET');
    }

    public function testLoginEntrepriseFails()
    {
        $client = static::createClient();
        $client->disableReboot();

        $crawler = $client->request('GET', '/entreprise');

        $client->followRedirects();
        $form = $crawler->selectButton('Se connecter')->form();
        $form['loginform[_username]'] = 'mauvaislogin';
        $form['loginform[_password]'] = 'mauvaismdp';
        $client->submit($form);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('.panel-user-infos.alert-danger', 'Identifiant ou mot de passe erroné');
    }
//
//    public function testLoginAgentSucceeds()
//    {
//        $client = static::createClient();
//        $client->disableReboot();
//
//        /** @var Crawler $crawler */
//        $crawler = $client->request('GET', '/?page=Agent.AgentHome');
//
//        $client->followRedirects();
//        $form = $crawler->selectButton('Se connecter')->form();
//        $form['loginform[_username]'] = 'mauvaislogin';
//        $form['loginform[_password]'] = 'mauvaismdp';
//        $client->submit($form);
//
//    }
}
