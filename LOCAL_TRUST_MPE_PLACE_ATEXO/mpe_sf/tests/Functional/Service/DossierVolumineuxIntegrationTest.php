<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Tests\Functional\Service;

use App\Service\CurrentUser;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client as guzzleClient;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AtexoUtilTest.
 */
class DossierVolumineuxIntegrationTest extends WebTestCase
{
    protected $client;
    protected $token;

    /**
     * @param $params
     * @dataProvider getTusProvider
     */
    public function testSuccessCreateSessionTus($params, $urlTus, $uuidReference)
    {
        $container = $this->createMock(ContainerInterface::class);
        $container->expects($this->any())
            ->method('getparameter')
            ->willReturn($urlTus);

        $currentUser = $this->createMock(CurrentUser::class);
        $validator = $this->createMock(ValidatorInterface::class);
        $validator->expects($this->any())
            ->method('validate')
            ->willReturn([]);

        $logger = $this->createMock(LoggerInterface::class);
        $objectManager = $this->createMock(EntityManager::class);

        $dossierVolumineuxService = new DossierVolumineuxService(
            $container,
            $currentUser,
            $objectManager,
            $validator,
            $logger
        );

        $result = $dossierVolumineuxService->callTusSession($params, $uuidReference);
        $this->assertArrayHasKey('uuid', $result);
    }

    /**
     * @return string
     */
    public function testGetToken()
    {
        //$response = $this->client->get('/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123');
        $response = $this->client->get('/api.php/ws/authentification/connexion/atexo_tus/dza12da3azd1');
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();

        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->token = $objet->__toString();

            return $this->token;
        }
    }

    /**
     * @param $token
     * @depends      testGetToken
     * @dataProvider getUuidTechnique
     */
    public function testUploadMetadataAction($params, $token)
    {
        $option = [
            'body' => [
                'date' => '2019-02-25T08:33:15Z',
                'taille' => 24696061952,
                'statut' => 'complet',
                'nom' => 'nom',
            ],
        ];
        $response = $this->client->post($this->wsBase.'/upload/metadata/'.$params.'/'.$token, $option);

        $json = $response->getBody()->read(1024);

        $res = json_decode($json);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(is_object($res));
    }

    /**
     * @return array
     */
    public function getUuidTechnique()
    {
        return [
            [
                'e6fd000c-3b3e-11e9-9ab2-0242ac110007',
            ],
        ];
    }

    /**
     * @return array
     */
    public function getTusProvider()
    {
        return [
            [
                [
                    'reference' => 'A-7298830a',
                    'nomDossierVolumineux' => 'DOSSIER-VOLUMINEUX',
                    'serverLogin' => 'atexo_tus',
                    'serverPassword' => 'dza12da3azd1',
                    'serverURL' => 'http://127.0.0.1/',
                    'statut' => 'init',
                ],
                'http://127.0.0.1/tus-api/',
                'uuidTechnique' => '7298ddfa-3c2c-11e9-8bf5-0242ac110007',
            ],
        ];
    }

    /**
     * @param $token
     * @depends      testGetToken
     * @dataProvider getUploadWsFile
     */
    public function testUploadWsAction($params, $token)
    {
        $option = [
            'body' => [
                $params['type'].'File' => 'moncontenuedefichier',
            ],
        ];

        $response = $this->client->post(
            sprintf(
                $this->wsBase.'/upload/%sfile/%s/%s/%s',
                $params['type'],
                $params['uuidTechnique'],
                $params['hash'],
                $token
            ),
            $option
        );

        $json = $response->getBody()->read(1024);

        $res = json_decode($json);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(is_object($res));
    }

    /**
     * @return array
     */
    public function getUploadWsFile()
    {
        return [
            [
                [
                    'uuidTechnique' => 'e6fd000c-3b3e-11e9-9ab2-0242ac110007',
                    'hash' => '2fa93acff31fab4600c2b7b7f6059e7f',
                    'type' => 'descripteur',
                ],
            ],
            [
                [
                    'uuidTechnique' => 'e6fd000c-3b3e-11e9-9ab2-0242ac110007',
                    'hash' => '2fa93acff31fab4600c2b7b7f6059e7f',
                    'type' => 'log',
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
        $this->wsBase = 'app.php/api/v1/dossier-volumineux';
    }
}
