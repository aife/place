<?php

namespace Tests\Functional\Service;

use App\Entity\Agent;
use App\Entity\Organisme;
use App\Service\Agent\ValidationEcoSip;
use App\Service\Publicite\EchangeConcentrateur;
use Monolog\Test\TestCase;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ValidationServiceTest.
 */
class ValidationEcoSipTest extends TestCase
{
    public function testGetParamesConnection()
    {
        $page = 'agent/annonces/validation-eco';
        $typeValidation = 'eco';
        // Organisme entity
        $organisme = new Organisme();
        $organisme->setAcronyme("pmi-min-1");
        //Agent
        $agent = new Agent();
        $agent->setId('11362');
        $agent->setLogin('adminORME');
        $agent->setOrganisme($organisme);

        $parameterBag = $this->createMock(ParameterBagInterface::class);
        $concentrateur = $this->createMock(EchangeConcentrateur::class);
        $requestStack = new RequestStack();
        $request = new Request([], [], [], [], [], [], [], json_encode([
            'test' => 'eforms'

        ]));
        $request->setLocale('fr');
        $requestStack->push($request);

        $validationEcoSip = new ValidationEcoSip($parameterBag, $concentrateur, $requestStack);
        $dataValidation = $validationEcoSip->getConnectionParams($page, $agent, $typeValidation);

        $this->assertSame($dataValidation['org'], $organisme->getAcronyme());
        $this->assertSame($dataValidation['page'], $page);
        $this->assertSame($dataValidation['lang'], $request->getLocale());
    }
}
