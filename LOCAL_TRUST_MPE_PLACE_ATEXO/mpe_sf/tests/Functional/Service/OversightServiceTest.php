<?php

namespace Tests\Functional\Service;

use App\Entity\SsoTiers;
use App\Entity\Tiers;
use App\Repository\SsoTiersRepository;
use App\Repository\TiersRepository;
use App\Service\OversightService;
use Tests\Functional\Controller\Api\ApiWebTestCase;

/**
 * Class OversightServiceTest.
 */
class OversightServiceTest extends ApiWebTestCase
{
    protected function setUp(): void
    {
        $this->userPasswordInDb = 'phpunit_test/phpunit_test0123';
        parent::setUp();
    }

    public function testGetTiersInformations()
    {
        $ticket = $this->getTicket();

        // SSoTiers entity
        $ssoTiers = new SsoTiers();
        $ssoTiers->setIdTiers(2);
        $ssoTiers->setIdSsoTiers($ticket);
        $ssoTiers->setIdFonctionnalite(4);

        // Tiers entity
        $tiers = new Tiers();
        $tiers->setLogin('phpunit_test');
        $tiers->setDenomination('phpunit_test');
        $tiers->setPassword('phpunit_test0123');
        $tiers->setFonctionnalite(4);

        $overSight = $this->createMock(OversightService::class);
        $overSight->expects($this->any())
            ->method('getTiersInformations')
            ->with($ticket)
            ->willReturn($tiers);

        $repoSsoTiers = $this->createMock(SsoTiersRepository::class);
        $repoSsoTiers->expects($this->any())
            ->method('findOneBy')
            ->with(['idSsoTiers' => $ticket])
            ->willReturn($ssoTiers);

        $repoTiers = $this->createMock(TiersRepository::class);
        $repoTiers->expects($this->any())
            ->method('findOneBy')
            ->with(['idTiers' => $ssoTiers->getIdTiers()])
            ->willReturn($tiers);

        $expectedTiers = $overSight->getTiersInformations($ticket);
        $this->assertInstanceOf(Tiers::class, $expectedTiers);
        $this->assertEquals($expectedTiers->getIdTiers(), $tiers->getIdTiers());
        $this->assertEquals($expectedTiers->getLogin(), $tiers->getLogin());
        $this->assertEquals($expectedTiers->getDenomination(), $tiers->getDenomination());
        $this->assertEquals($expectedTiers->getFonctionnalite(), $tiers->getFonctionnalite());
        $this->assertEquals($expectedTiers->getOrganisme(), $tiers->getOrganisme());
        $this->assertEquals($expectedTiers->getPassword(), $tiers->getPassword());
    }
}
