<?php

namespace Tests\Functional\Service;

use App\Service\AtexoSearchEntreprise;
use App\Service\AtexoUtil;
use App\Utils\AtexoConsultationCriteriaVo;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AtexoUtilTest.
 */
class AtexoSearchCaseTest extends KernelTestCase
{
    private $entityManager;
    private $service;
    private $resultat;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->container = new Container();

        $this->container->setParameter('REGLE_VALIDATION_TYPE_1', '1');
        $this->container->setParameter('REGLE_VALIDATION_TYPE_2', '2');
        $this->container->setParameter('REGLE_VALIDATION_TYPE_3', '3');
        $this->container->setParameter('REGLE_VALIDATION_TYPE_4', '4');
        $this->container->setParameter('REGLE_VALIDATION_TYPE_5', '5');
        $this->container->setParameter('TYPE_PROCEDURE_RESTREINTE', '2');
        $this->container->setParameter('TYPE_PROCEDURE_PUBLICITE', '1');
        $this->container->setParameter('BASE_ROOT_DIR', '/data/local-trust/mpe_place_test/');
        $this->container->setParameter(
            'PHP_LIB_PATH',
            '/opt/local-trust/apache/htdocs/mpe_place_test/mpe/protected/library/'
        );
        $this->container->setParameter('PF_URL', 'http://127.0.0.1/');
        $this->container->setParameter('DB_PREFIX', 'place_test_unitaire');
        $this->container->setParameter('PF_MAIL_FROM', 'nepasrepondre@atexo.com');
        $this->container->setParameter('HOSTSPEC', 'localhost');
        $this->container->setParameter('USERNAME', 'test_unitaire');
        $this->container->setParameter('PASSWORD', '123');
        $this->container->setParameter('PF_LONG_NAME', 'Atexo');
        $this->container->setParameter('PRECISION_LUCENE', '0.7');
        $this->container->setParameter('CLAUSES_SOCIALES_ACTIVER', '1');

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $logMock = $this->createMock(Logger::class);
        $sessionMock = $this->createMock(Session::class);
        $translatorMock = $this->createMock(TranslatorInterface::class);
        $serviceUtils = new AtexoUtil($this->container, $logMock, $sessionMock, $translatorMock);

        $this->service = new AtexoSearchEntreprise($this->container, $this->entityManager, $logMock, $serviceUtils);

        $this->resultat = 11;
    }

    /*public function testInjectBdd()
     {
         $bool = true;
         try {
             $path = "/opt/local-trust/apache/htdocs/mpe_place_test/mpe_sf/bin/console";
             shell_exec('php7 '.$path.' mpe:bdd:init test_unitaire 123');
         } catch (Exception $exception) {
             $bool = false;
         }

         $this->assertTrue($bool);
     }*/

    public function testCountConsultationEntreprise()
    {
        $criteriaVo = new AtexoConsultationCriteriaVo();
        $criteriaVo->setcalledFromPortail(true);
        $returnJustNumberOfElement = true;
        $returnReferences = $forAlertesEntreprises = false;
        $result = $this->service->getConsultationsByQuery($criteriaVo, $returnJustNumberOfElement);
        $this->assertEquals($this->resultat, $result[0]['total']);
    }

    /**
     * @dataProvider dataProviderTestSearchConsultationEntreprise
     *
     * @param $listKeyWord
     * @param $expectedResult
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function testSearchConsultationEntreprise($critere, $expectedResult)
    {
        $criteriaVo = new AtexoConsultationCriteriaVo();
        $criteriaVo->setcalledFromPortail(true);

        if (array_key_exists('id', $critere)) {
            $criteriaVo->setIdReference($critere['id']);
        }

        if (array_key_exists('referUser', $critere)) {
            $criteriaVo->setReferenceConsultation($critere['referUser']);
        }

        if (array_key_exists('referUserRestreint', $critere)) {
            $criteriaVo->setReferenceConsultation($critere['referUserRestreint']);
        }

        if (array_key_exists('procedure', $critere)) {
            $criteriaVo->setIdTypeProcedure($critere['procedure']);
        }

        if (array_key_exists('categorie', $critere)) {
            $criteriaVo->setCategorieConsultation($critere['categorie']);
        }

        if (array_key_exists('dlroStart', $critere) && array_key_exists('dlroEnd', $critere)) {
            $criteriaVo->setDateFinStart($critere['dlroStart']);
            $criteriaVo->setDateFinEnd($critere['dlroEnd']);
        }

        if (array_key_exists('lieux', $critere)) {
            $criteriaVo->setLieuxexecution($critere['lieux']);
        }

        if (array_key_exists('keyword', $critere)) {
            $criteriaVo->setKeyWordAdvancedSearch($critere['keyword']);
        }

        if (array_key_exists('cpv', $critere)) {
            $criteriaVo->setIdCodeCpv2($critere['cpv']);
        }

        if (array_key_exists('miseEnLigneStart', $critere) && array_key_exists('miseEnLigneEnd', $critere)) {
            $criteriaVo->setEnLigneDepuis($critere['miseEnLigneStart']);
            $criteriaVo->setEnLigneJusquau($critere['miseEnLigneEnd']);
        }

        if (array_key_exists('clauseSocial', $critere)) {
            $criteriaVo->setClauseSociale($critere['clauseSocial']);
        }

        if (array_key_exists('clauseEnv', $critere)) {
            $criteriaVo->setClauseEnv($critere['clauseEnv']);
        }

        if (array_key_exists('atelierP', $critere)) {
            $criteriaVo->setAtelierProtege($critere['atelierP']);
        }

        if (array_key_exists('siae', $critere)) {
            $criteriaVo->setSiae($critere['siae']);
        }

        if (array_key_exists('ess', $critere)) {
            $criteriaVo->setEss($critere['ess']);
        }

        if (array_key_exists('insertion', $critere)) {
            $criteriaVo->setSocialeInsertionActiviterEconomique($critere['insertion']);
        }

        if (array_key_exists('commerce', $critere)) {
            $criteriaVo->setSocialeCommerceEquitable($critere['commerce']);
        }

        $returnJustNumberOfElement = $returnReferences = $forAlertesEntreprises = false;
        $result = $this->service->getConsultationsByQuery(
            $criteriaVo,
            $returnJustNumberOfElement,
            $returnReferences,
            $forAlertesEntreprises
        );

        $this->assertEquals($expectedResult, count($result));
    }

    /**
     * @return array
     */
    public function dataProviderTestSearchConsultationEntreprise()
    {
        return [
            [
                ['id' => 1], 1,
            ],
            [
                ['id' => 20], 0,
            ],
            [
                ['referUser' => 'ATOM_CONS_0001'], 3,
            ],
            [
                ['referUserRestreint' => 'ATOM_CONS_0001'], 3,
            ],
            [
                ['procedure' => 3], 7,
            ],
            [
                ['referUser' => 'ATOM_CONS_0001', 'procedure' => 3], 2,
            ],
            [
                ['categorie' => 3], 3,
            ],
            [
                ['referUser' => 'ATOM_CONS_0001', 'procedure' => 3, 'categorie' => 3], 0,
            ],
            [
                ['referUser' => 'ATOM_CONS_0001', 'procedure' => 3, 'categorie' => 1], 2,
            ],
            [
                ['dlroStart' => '01/01/2019', 'dlroEnd' => '01/01/2119'], 11,
            ],
            [
               ['lieux' => ',,254,'], 8,
            ],
            [
                ['lieux' => ',,250,'], 3,
            ],
            [
                ['lieux' => ',,254,250,'], 10,
            ],
            [
                ['lieux' => '254'], 8,
            ],
            [
                ['lieux' => ',,244,'], 1,
            ],
            [
                ['keyword' => '"ATOM_CONS_0009"'], 1,
            ],
            [
                ['keyword' => '"ATOM_CONS_0009" "ATOM_CONS_0001"'], 0,
            ],
            [
                ['cpv' => '45500000##'], 1,
            ],
            [
                ['cpv' => '35000000##'], 1,
            ],
            [
                ['miseEnLigneStart' => '01/01/2018', 'miseEnLigneEnd' => '01/01/2119'], 10,
            ],
            [
                ['lieux' => ',,250,', 'miseEnLigneStart' => '01/01/2018', 'miseEnLigneEnd' => '01/01/2119'], 2,
            ],
            [
                ['clauseSocial' => '1'], 3,
            ],
            [
                ['clauseSocial' => '2'], 8,
            ],
            [
                ['clauseEnv' => '1'], 3,
            ],
            [
                ['clauseEnv' => '2'], 8,
            ],
            [
                ['clauseSocial' => '1', 'clauseEnv' => '2'], 2,
            ],
            [
                ['atelierP' => '1'], 4,
            ],
            [
                ['siae' => '1'], 2,
            ],
            [
                ['ess' => '1'], 2,
            ],
            [
                ['insertion' => '1'], 0,
            ],
            [
                ['commerce' => '1'], 0,
            ],
            [
                ['clauseSocial' => '1', 'atelierP' => '1'], 2,
            ],
        ];
    }
}
