<?php

namespace Tests\Functional\Controller\Api;

class TableauDeBordControllerTest extends ApiWebTestCase
{
    /**
     * Test récupération des consultations en cours au format xml.
     */
    public function testConsultationsEnCoursXml()
    {
        $url = $this->endpoint.'/consultations-en-cours.xml?'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Test récupération des consultations en cours au format json.
     */
    public function testConsultationsEnCoursJson()
    {
        $url = $this->endpoint.'/consultations-en-cours.json?'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    /**
     * Test récupération des questions au format xml.
     */
    public function testQuestionsXml()
    {
        $url = $this->endpoint.'/questions.xml?'.$this->extraParameters;

        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Test récupération des questions au format json.
     */
    public function testQuestionsJson()
    {
        $url = $this->endpoint.'/questions.json?'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    /**
     * Test récupération des contrats au format xml.
     */
    public function testContratsXml()
    {
        $url = $this->endpoint.'/contrats.xml?'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Test récupération des contrats au format json.
     */
    public function testContratsJson()
    {
        $url = $this->endpoint.'/contrats.json?'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/tableau-de-bord';
        $this->extraParameters = 'loginAgent='.$this->loginAgent.'&ticket='.$this->getTicket();
    }
}
