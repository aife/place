<?php

namespace Tests\Functional\Controller\Api;

use DOMDocument;
use GuzzleHttp\Client as guzzleClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ApiWebTestCase extends WebTestCase
{
    public const MODULE_EXEC = 'EXEC';
    /**
     * @var
     */
    private static $staticClient;
    protected $endpoint;
    protected $extraParameters;
    protected $userPasswordInDb = 'phpunit_test/phpunit_test0123';
    /**
     * @var guzzleClient
     */
    protected $client;
    /**
     * @var
     */
    protected $ticket;
    protected $loginAgent = 'adminORME';
    protected $xsd = __DIR__.'/WS_MPE_SF.xsd';
    private $urlAuth;

    /**
     * Executé avant le setUp, une seule fois.
     */
    public static function setUpBeforeClass(): void
    {
        $baseUrl = getenv('TEST_BASE_URL');
        if (!$baseUrl) {
            static::fail('No TEST_BASE_URL environmental variable set in phpunit.xml.');
        }
        self::$staticClient = new guzzleClient(
            [
                'base_url' => $baseUrl,
                'defaults' => [
                    'exceptions' => false,
                ],
            ]
        );
    }

    /**
     * Récupération d'un ticket  et test de sa validité.
     *
     * @return string
     */
    public function getTicket()
    {
        if (!empty($this->ticket)) {
            return $this->ticket;
        }
        $response = $this->client->get($this->urlAuth);

        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();
        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->assertNotEmpty(
                $objet->__toString(),
                "/!\ Erreur de récupération du token de WS. Vérifiez que le user/password "
                ."$this->userPasswordInDb existe bien en BDD (table Tiers)"
            );
            $this->ticket = $objet->__toString();

            return $this->ticket;
        }
    }

    /**
     * Test lors d'un ticket invalide.
     */
    public function testInvalidTicket()
    {
        $response = $this->client->get($this->urlAuth.'invalidAuth');
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();
        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->assertEmpty($objet->__toString());
        }
    }

    /**
     * Executé à chaque test.
     */
    protected function setUp(): void
    {
        $this->client = self::$staticClient;
        $this->urlAuth = '/api.php/ws/authentification/connexion/'.$this->userPasswordInDb;
    }

    /**
     * @param $response
     * @param $mode
     * @param int $statutCode
     */
    protected function checkResponse($response, $mode, $statutCode = 200)
    {
        $this->assertEquals($statutCode, $response->getStatusCode());
        $data = $response->getBody()->getContents();

        if ('xml' == $mode) {
            $this->assertNotEmpty($data);
            $this->assertTrue($this->hasValidSchema($data));
        } elseif ('json' == $mode) {
            $this->assertNotEmpty($data);
            $this->assertTrue($this->isValidJson($data));

            $json = json_decode($data);
            $this->assertNotNull($json->mpe->reponse);
        }

        return $data;
    }

    protected function hasValidSchema($xml)
    {
        $dom = new DOMDocument();
        $dom->loadXML($xml, LIBXML_NOBLANKS);
        $dom->xinclude();

        return $dom->schemaValidate($this->xsd);
    }

    protected function isValidJson($string)
    {
        json_decode($string);

        return JSON_ERROR_NONE == json_last_error();
    }

    protected function getService($id)
    {
        return self::$kernel->getContainer()
            ->get($id);
    }
}
