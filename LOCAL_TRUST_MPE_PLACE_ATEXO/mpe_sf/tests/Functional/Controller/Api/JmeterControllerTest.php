<?php

namespace Tests\Functional\Controller\Api;

class JmeterControllerTest extends ApiWebTestCase
{
    public function testScenarioNotExist()
    {
        $url = $this->endpoint.'fauxScenario'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testScenarioExist()
    {
        $url = $this->endpoint.'depotReponse'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testHashOk()
    {
        $url = $this->endpoint.'hash/depotReponse'.$this->extraParameters;

        $response = $this->client->get($url);

        $xmlReponse = $response->getBody()->getContents();
        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $this->assertNotEmpty($objet->reponse->sha256);
    }

    public function testHashKo()
    {
        $url = $this->endpoint.'hash/fauxScenario'.$this->extraParameters;

        $response = $this->client->get($url);

        $xmlReponse = $response->getBody()->getContents();
        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('KO', $objet->reponse['statutReponse']);
        $this->assertNotEmpty($objet->reponse->messageErreur);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/monitoring/jmeter/';
        $this->extraParameters = '?ticket='.$this->getTicket();
    }
}
