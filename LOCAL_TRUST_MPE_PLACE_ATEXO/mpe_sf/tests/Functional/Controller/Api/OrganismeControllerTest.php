<?php

namespace Tests\Functional\Controller\Api;

class OrganismeControllerTest extends ApiWebTestCase
{
    /**
     * Récupération des organismes au format XML.
     */
    public function testOrganismesXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Récupération des organismes au format XML.
     */
    public function testOrganismesWithExecModuleXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters.'&module='.self::MODULE_EXEC;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Récupération des organismes au format JSON.
     */
    public function testOrganismesJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/organismes';
        $this->extraParameters = '?page=1&limit=1&lastUpdateTimestamp=1548510000&ticket='.$this->getTicket();
    }
}
