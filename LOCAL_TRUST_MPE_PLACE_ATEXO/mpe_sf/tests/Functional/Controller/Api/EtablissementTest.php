<?php

namespace Tests\Functional\Controller\Api;

use GuzzleHttp\Client as guzzleClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EtablissementTest extends WebTestCase
{
    protected $client;
    protected $ticket;

    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
    }

    /**
     * @return string
     */
    public function testGetTicket()
    {
        $response = $this->client->get(
            '/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123'
        );

        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();
        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->ticket = $objet->__toString();

            return $this->ticket;
        }
    }

    /**
     * @param $ticket
     * @depends testGetTicket
     */
    public function testEtablissementJson($ticket)
    {
        $url = 'app.php/api/1/etablissements.json?page=1&limit=20&lastUpdateTimestamp=1448510000&ticket='.$ticket;
        $response = $this->client->get($url);

        $this->assertEquals(200, $response->getStatusCode());
        $jsonResponse = json_decode($response->getBody()->getContents());

        $this->assertNotEmpty($jsonResponse);
        $this->assertNotNull($jsonResponse->etablissements);
    }

    /**
     * @param $ticket
     * @depends testGetTicket
     */
    public function testEtablissementXml($ticket)
    {
        $xsd = __DIR__.'/../../WS_MPE_SF.xsd';
        $url = 'app.php/api/1/etablissements.xml?page=1&limit=20&lastUpdateTimestamp=1448510000&ticket='.$ticket;

        $response = $this->client->get($url);
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();

        if (200 == $response->getStatusCode() && !empty($xml)) {
            $dom = new \DOMDocument();
            $dom->loadXML($xml, LIBXML_NOBLANKS);
            $dom->xinclude();
            $valid = $dom->schemaValidate($xsd);

            $this->assertTrue(true == $valid);
        }
    }
}
