<?php

namespace Tests\Functional\Controller\Api;

class ContactControllerTest extends ApiWebTestCase
{
    protected $options;

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/contacts';
        $this->extraParameters = '?page=1&limit=20&lastUpdateTimestamp=1';
        $this->options = [
            'headers' => ['Authorization' => 'Bearer '.$this->getTicket()],
        ];
    }

    /**
     * Récupération des contacts au format JSON.
     */
    public function testContactJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json');
    }

    public function testContactWithExecModuleJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters.'&module='.self::MODULE_EXEC;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json');
    }

    /**
     * Récupération des contacts au format XML.
     */
    public function testContactXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml');
    }
}
