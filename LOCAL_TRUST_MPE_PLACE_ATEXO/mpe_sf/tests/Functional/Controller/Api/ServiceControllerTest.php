<?php

namespace Tests\Functional\Controller\Api;

class ServiceControllerTest extends ApiWebTestCase
{
    public function testGetJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    public function testGetWithExecModuleJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters.'&module='.self::MODULE_EXEC;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    public function testGetXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    public function testInvalidToken()
    {
        $url = $this->endpoint.'.json';
        $response = $this->client->get($url);
        $this->assertEquals(403, $response->getStatusCode());
    }

    public function testPostXml()
    {
        $content = file_get_contents(__DIR__.'/service_create.xml');

        $options = [
            'body' => $content,
        ];

        $response = $this->client->post($this->endpoint.'.xml?ticket='.$this->getTicket(), $options);

        $this->assertEquals(201, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $this->assertNotEmpty($objet->reponse->service->id);

        $xmlToUpdate = simplexml_load_string($content);
        $xmlToUpdate->envoi->service->id = $objet->reponse->service->id;

        $params = [];
        $params['ticket'] = $this->getTicket();
        $params['xml'] = $xmlToUpdate->asXML();

        return $params;
    }

    /**
     * @param $params
     * @depends testPostXml
     */
    public function testPutXml($params)
    {
        $options = [
            'body' => $params['xml'],
        ];

        $response = $this->client->put($this->endpoint.'.xml?ticket='.$this->getTicket(), $options);

        $this->assertEquals(200, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $source = simplexml_load_string($params['xml']);
        $this->assertEquals($objet->reponse->service->id, $source->envoi->service->id);
    }

    public function testPostJson()
    {
        $content = file_get_contents(__DIR__.'/service_create.json');

        $options = [
            'body' => $content,
        ];

        $response = $this->client->post($this->endpoint.'.json?ticket='.$this->getTicket(), $options);

        $this->assertEquals(201, $response->getStatusCode());
        $jsonResponse = $response->getBody()->getContents();
        $this->assertNotEmpty($jsonResponse);
        $objet = json_decode($jsonResponse);

        $this->assertNotEmpty($objet->mpe->reponse->service->id);

        $params = [];
        $params['ticket'] = $this->getTicket();

        $jsonToUpdate = json_decode($content);
        $jsonToUpdate->id = $objet->mpe->reponse->service->id;
        $params['json'] = json_encode($jsonToUpdate);

        return $params;
    }

    /**
     * @param $params
     * @depends testPostJson
     */
    public function testPutJson($params)
    {
        $options = [
            'body' => $params['json'],
        ];

        $response = $this->client->put($this->endpoint.'.json?ticket='.$this->getTicket(), $options);
        $this->assertEquals(200, $response->getStatusCode());
        $jsonResponse = $response->getBody()->getContents();
        $this->assertNotEmpty($jsonResponse);
        $objet = json_decode($jsonResponse);
        $source = json_decode($params['json']);

        $this->assertEquals($objet->mpe->reponse->service->id, $source->id);
    }

    public function testPatchXml()
    {
        $response = $this->client->patch(
            $this->endpoint.'.xml?ticket='.$this->getTicket()
            .'&deServiceId=1&versServiceId=2&organisme=pmi-min-1'
        );
        $this->checkResponse($response, 'xml');
    }

    public function testPatchJson()
    {
        $response = $this->client->patch(
            $this->endpoint.'.json?ticket='.$this->getTicket()
            .'&deServiceId=1&versServiceId=2&organisme=pmi-min-1'
        );

        $this->assertEquals(200, $response->getStatusCode());
        $response = $response->getBody()->getContents();
        $this->assertNotEmpty($response);
        $this->isValidJson($response);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/services';
        $this->extraParameters = '?page=1&limit=1&lastUpdateTimestamp=1448510000&ticket='.$this->getTicket();
    }
}
