<?php

namespace Tests\Functional\Controller\Api;

class EtablissementControllerTest extends ApiWebTestCase
{
    protected $options;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userPasswordInDb = 'phpunit_test/phpunit_test0123';
        $this->endpoint = '/app.php/api/v1/etablissements';
        $this->extraParameters = '?page=1&limit=20&lastUpdateTimestamp=1548510000';
        $this->options = [
            'headers' => ['Authorization' => 'Bearer '.$this->getTicket()],
        ];
    }

    /**
     * Récupération des établissements au format JSON.
     */
    public function testEtablissementJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json');
    }

    /**
     * Récupération des établissements au format JSON.
     */
    public function testEtablissementWithExecModuleJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters.'&module='.self::MODULE_EXEC;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json');
    }

    /**
     * Récupération des établissements au format XML.
     */
    public function testEtablissementXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Test creating new etablissement.
     *
     * @return array
     */
    public function testPostEtablissementXml()
    {
        $content = file_get_contents(__DIR__.'/etablissement_create.xml');
        $this->options['body'] = $content;
        $response = $this->client->post($this->endpoint.'.xml', $this->options);

        $xmlReponse = $this->checkResponse($response, 'xml', 201);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $this->assertNotEmpty($objet->reponse->etablissement->id);

        $xmlToUpdate = simplexml_load_string($content);
        $xmlToUpdate->envoi->etablissement->id = $objet->reponse->etablissement->id;

        $params = [];
        $params['token'] = $this->getTicket();
        $params['xml'] = $xmlToUpdate->asXML();

        return $params;
    }

    /**
     * Test Creating etablissement.
     *
     * @return array
     */
    public function testPostEtablissementJson()
    {
        $content = file_get_contents(__DIR__.'/etablissement_create.json');
        $this->options['body'] = $content;
        $response = $this->client->post($this->endpoint.'.json', $this->options);

        $this->assertEquals(201, $response->getStatusCode());
        $jsonResponse = $this->checkResponse($response, 'json', 201);
        $objet = json_decode($jsonResponse);
        $this->assertNotEmpty($objet->mpe->reponse->statutReponse);

        $params = [];
        $params['token'] = $this->getTicket();
        $params['json'] = json_encode($objet->mpe->reponse->Etablissement);

        return $params;
    }

    /**
     * @param $params
     * @depends testPostEtablissementXml
     */
    public function testPutEtablissementXml($params)
    {
        $this->options['body'] = $params['xml'];
        $response = $this->client->put($this->endpoint.'.xml', $this->options);
        $this->assertEquals(200, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $source = simplexml_load_string($params['xml']);
        $this->assertEquals($objet->reponse->etablissement->id, $source->envoi->etablissement->id);
    }

    /**
     * @param $params
     * @depends testPostEtablissementJson
     */
    public function testPutJson($params)
    {
        $this->options['body'] = $params['json'];
        $response = $this->client->put($this->endpoint.'.json', $this->options);
        $this->assertEquals(200, $response->getStatusCode());
        $jsonResponse = $response->getBody()->getContents();
        $this->assertNotEmpty($jsonResponse);
        $objet = json_decode($jsonResponse);
        $source = json_decode($params['json']);
        $this->assertEquals($objet->mpe->reponse->Etablissement->id, $source->id);
    }
}
