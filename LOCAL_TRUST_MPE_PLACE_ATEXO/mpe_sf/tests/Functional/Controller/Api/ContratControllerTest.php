<?php

namespace Tests\Functional\Controller\Api;

class ContratControllerTest extends ApiWebTestCase
{
    /**
     * Récupération des contrats au format JSON.
     */
    public function testContractJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    public function testContractWithExecModuleJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters.'&module='.self::MODULE_EXEC;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json');
    }

    /**
     * Récupération des contrats au format XML.
     */
    public function testContractXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url);
        $this->checkResponse($response, 'xml');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->endpoint = 'app.php/api/1/contrats';
        $this->extraParameters = '?page=1&limit=20&lastUpdateTimestamp=1&ticket='.$this->getTicket();
    }
}
