<?php

namespace Tests\Functional\Controller\Api;

use Ramsey\Uuid\Uuid;

class InscritControllerTest extends ApiWebTestCase
{
    protected $login;
    protected $email;

    protected function setUp(): void
    {
        parent::setUp();

        $uuid = Uuid::uuid1();
        $uuid = substr($uuid->toString(), 0, 20);
        $this->email = 'inscrit'.$uuid.'@atexo.com';
        $this->login = 'inscrit'.$uuid;
        $this->userPasswordInDb = 'phpunit_test/phpunit_test0123';
        $this->endpoint = '/app.php/api/v1/inscrits';
        $this->extraParameters = '?page=1&limit=20&lastUpdateTimestamp=1548510000';
        $this->options = [
            'headers' => ['Authorization' => 'Bearer '.$this->getTicket()],
        ];
    }

    /**
     * Récupération des inscrits au format JSON.
     */
    public function testInscritJson()
    {
        $url = $this->endpoint.'.json'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json');
    }

    /**
     * Récupération des inscrits au format XML.
     */
    public function testInscritXml()
    {
        $url = $this->endpoint.'.xml'.$this->extraParameters;
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml');
    }

    /**
     * Test creating new inscrit by using xml format.
     *
     * @return array
     */
    public function testPostInscritXml()
    {
        $content = file_get_contents(__DIR__.'/inscrit_create.xml');
        $content = str_replace('EMAILTOREPLACE', $this->email, $content);
        $content = str_replace('LOGINTOREPLACE', $this->login, $content);
        $this->options['body'] = $content;

        $response = $this->client->post($this->endpoint.'.xml', $this->options);
        $xmlReponse = $this->checkResponse($response, 'xml', 201);
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $this->assertNotEmpty($objet->reponse->inscrit->id);

        $xmlToUpdate = simplexml_load_string($content);
        $xmlToUpdate->envoi->inscrit->id = $objet->reponse->inscrit->id;

        $params = [];
        $params['token'] = $this->getTicket();
        $params['xml'] = $xmlToUpdate->asXML();

        return $params;
    }

    /**
     * Test Creating Inscrit bu using Json format.
     *
     * @return array
     */
    public function testPostInscritJson()
    {
        $content = file_get_contents(__DIR__.'/inscrit_create.json');
        $content = str_replace('EMAILTOREPLACE', $this->email, $content);
        $content = str_replace('LOGINTOREPLACE', $this->login, $content);
        $this->options['body'] = $content;

        $response = $this->client->post($this->endpoint.'.json', $this->options);
        $this->assertEquals(201, $response->getStatusCode());
        $jsonResponse = $this->checkResponse($response, 'json', 201);
        $objet = json_decode($jsonResponse);
        $this->assertNotEmpty($objet->mpe->reponse->statutReponse);

        $params = [];
        $params['token'] = $this->getTicket();
        $params['json'] = json_encode($objet->mpe->reponse->Inscrit);

        return $params;
    }

    /**
     * @param $params
     * @depends testPostInscritXml
     */
    public function testPutInscritXml($params)
    {
        $this->options['body'] = $params['xml'];
        $response = $this->client->put($this->endpoint.'.xml', $this->options);

        $this->assertEquals(200, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        $this->assertNotEmpty($xmlReponse);
        $objet = simplexml_load_string($xmlReponse);
        $this->assertEquals('OK', $objet->reponse['statutReponse']);
        $source = simplexml_load_string($params['xml']);
        $this->assertEquals($objet->reponse->inscrit->id, $source->envoi->inscrit->id);
    }

    /**
     * @param $params
     * @depends testPostInscritJson
     */
    public function testPutInscritJson($params)
    {
        $this->options['body'] = $params['json'];
        $response = $this->client->put($this->endpoint.'.json', $this->options);

        $this->assertEquals(200, $response->getStatusCode());
        $jsonResponse = $response->getBody()->getContents();
        $this->assertNotEmpty($jsonResponse);
        $objet = json_decode($jsonResponse);
        $source = json_decode($params['json']);
        $this->assertEquals($objet->mpe->reponse->Inscrit->id, $source->id);
    }
}
