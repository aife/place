<?php

namespace Tests\Functional\Api;

use GuzzleHttp\Client as guzzleClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AtexoRestActionConsultationTest.
 */
class AtexoRestActionConsultationTest extends WebTestCase
{
    protected $client;
    protected $token;
    protected $consultationId;

    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
    }

    /**
     * @return string
     */
    public function testGetToken()
    {
        $response = $this->client->get(
            '/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123'
        );
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();
        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->token = $objet->__toString();

            return $this->token;
        }
    }

    /**
     * @param $token
     * @depends testGetToken
     */
    public function testPostConsultation($token)
    {
        $xml = file_get_contents(__DIR__.'/consultation/xml_data_envoye_2018-FTV-DAHP-AOO-0340_PHASE_1_post.xml');
        $xml = str_replace('{{consultationId}}', 0, $xml);
        $xml = str_replace('{{consultationObjet}}', 'TEST APO - ALLOTISSEMENT 2', $xml);
        $option = [
            'body' => ['xml' => $xml],
        ];
        $response = $this->client->post('/api.php/ws/consultation/create?ticket='.$token, $option);
        $this->assertEquals(200, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        if (200 == $response->getStatusCode() && !empty($xmlReponse)) {
            $objet = simplexml_load_string($xmlReponse);
            $this->consultationId = $objet->reponse->consultation->id->__toString();
        }

        $params = [];
        $params['token'] = $token;
        $params['consultationId'] = $this->consultationId;

        return $params;
    }

    /**
     * @param $params
     * @depends testPostConsultation
     */
    public function testUpdateConsultation($params)
    {
        $xml = file_get_contents(__DIR__.'/consultation/xml_data_envoye_2018-FTV-DAHP-AOO-0340_PHASE_1_post.xml');
        $xml = str_replace('{{consultationId}}', $params['consultationId'], $xml);
        $xml = str_replace('{{consultationObjet}}', 'TEST APO - ALLOTISSEMENT 2 - update', $xml);
        $option = [
            'body' => ['xml' => $xml],
        ];
        $response = $this->client->post('/api.php/ws/consultation/update?ticket='.$params['token'], $option);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
