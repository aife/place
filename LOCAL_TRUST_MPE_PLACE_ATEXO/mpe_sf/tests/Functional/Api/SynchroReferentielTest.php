<?php

namespace Tests\Functional\Api;

use GuzzleHttp\Client as guzzleClient;
use PHPUnit\Framework\TestCase;

class SynchroReferentielTest extends TestCase
{
    protected $client;
    protected $token;
    protected $container;

    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
    }

    /**
     * @return string
     */
    public function testGetToken()
    {
        $response = $this->client->get('/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123');
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();

        $this->assertNotEmpty($xml);

        $objet = simplexml_load_string($xml);

        return $this->token = $objet->__toString();
    }

    /**
     * @param $token
     * @depends testGetToken
     */
    public function testWsSynchroReferentielOk($token)
    {
        $xsd = __DIR__.'/../../mpe/ressources/REST/WS_MPE.xsd';
        $response = $this->client->get('/app.php/api/1/referentiels/procedureContrats?token='.$token);

        try {
            $this->assertEquals(200, $response->getStatusCode());
            $xml = $response->getBody()->getContents();
            $this->assertNotEmpty($xml);

            $dom = new \DOMDocument();
            $this->assertTrue($dom->loadXML($xml));
            $valid = $dom->schemaValidate($xsd);

            $this->assertTrue(true == $valid);
        } catch (\Exception $e) {
        }
    }

    public function testInvalidToken()
    {
        $response = $this->client->get('/app.php/api/v1/referentiels/procedureContrats?token=invalid');
        $this->assertEquals(200, $response->getStatusCode());

        $xml = $response->getBody()->getContents();
        $this->assertNotEmpty($xml);
        $this->assertContains('Invalid token ', $xml);
    }
}
