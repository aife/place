<?php

namespace Tests\Functional\Api\ModificationContrat;

use Tests\Functional\Controller\Api\ApiWebTestCase;

class ModificationContratControllerTest extends ApiWebTestCase
{
    protected $options;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userPasswordInDb = 'phpunit_test/phpunit_test0123';
        $this->options = [
            'headers' => ['Authorization' => 'Bearer '.$this->getTicket()],
        ];
    }

    public function testInsertJson()
    {
        $url = '/app.php/api/v1/contrats/modification.json';
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json', 201);
    }
}
