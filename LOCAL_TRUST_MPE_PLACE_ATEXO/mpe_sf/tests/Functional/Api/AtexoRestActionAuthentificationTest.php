<?php

namespace Tests\Functional\Api;

use GuzzleHttp\Client as guzzleClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class SupervisionInterfacesControllerTest.
 */
class AtexoRestActionAuthentificationTest extends WebTestCase
{
    protected $client;

    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
    }

    public function testGetToken()
    {
        $response = $this->client->get('/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123');
        $this->assertEquals(200, $response->getStatusCode());
    }
}
