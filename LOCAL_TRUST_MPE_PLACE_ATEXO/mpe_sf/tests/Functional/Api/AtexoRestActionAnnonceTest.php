<?php

namespace Tests\Functional\Api;

use GuzzleHttp\Client as guzzleClient;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class AtexoRestActionAnnonceTest.
 */
class AtexoRestActionAnnonceTest extends WebTestCase
{
    protected $client;
    protected $token;
    protected $annonceId;
    protected $fichierId;

    protected function setUp(): void
    {
        $this->client = new guzzleClient(['base_url' => 'http://127.0.0.1']);
    }

    /**
     * @return string
     */
    public function testGetToken()
    {
        $response = $this->client->get('/api.php/ws/authentification/connexion/phpunit_test/phpunit_test0123');
        $this->assertEquals(200, $response->getStatusCode());
        $xml = $response->getBody()->getContents();
        if (200 == $response->getStatusCode() && !empty($xml)) {
            $objet = simplexml_load_string($xml);
            $this->token = $objet->__toString();

            return $this->token;
        }
    }

    /**
     * @param $token
     * @depends testGetToken
     */
    public function testPostAnnonce($token)
    {
        $xml = file_get_contents(__DIR__.'/annonce/xml_data_envoye_2018-FTV-DAHP-MAPA3-0296_ATTRIBUTION.xml');
        $xml = str_replace('{{annonceId}}', 0, $xml);
        $xml = str_replace('{{annonceObjet}}', 'essai test de la version  2017-008-02   vers Maximilien', $xml);
        $xml = str_replace('{{publiciteId}}', 0, $xml);

        $nameFile = fopen(__DIR__.'/annonce/AE_Lot2 - 20180927145400 - Signature 1.pdf', 'r');
        $option = [
            'body' => [
                'xml' => $xml,
                'name' => $nameFile,
            ],
        ];
        $response = $this->client->post('/api.php/ws/annonceservice/create?ticket='.$token, $option);
        $this->assertEquals(200, $response->getStatusCode());
        $xmlReponse = $response->getBody()->getContents();

        if (200 == $response->getStatusCode() && !empty($xmlReponse)) {
            $objet = simplexml_load_string($xmlReponse);
            $this->annonceId = $objet->reponse->annonce->id->__toString();
            $this->fichierId = $objet->reponse->annonce->fichiersPublicite->fichier->id->__toString();
        }

        $params = [];
        $params['token'] = $token;
        $params['annonceId'] = $this->annonceId;
        $params['fichierId'] = $this->fichierId;

        return $params;
    }

    /**
     * @param $params
     * @depends testPostAnnonce
     */
    public function testUpdateAnnonce($params)
    {
        $xml = file_get_contents(__DIR__.'/annonce/xml_data_envoye_2018-FTV-DAHP-MAPA3-0296_ATTRIBUTION.xml');
        $xml = str_replace('{{annonceId}}', $params['annonceId'], $xml);
        $xml = str_replace('{{annonceObjet}}', 'essai test de la version  2017-008-02   vers Maximilien update', $xml);
        $xml = str_replace('{{publiciteId}}', $params['fichierId'], $xml);

        $nameFile = fopen(__DIR__.'/annonce/AE_Lot2 - 20180927145400 - Signature 1.pdf', 'r');
        $option = [
            'body' => [
                'xml' => $xml,
                'name' => $nameFile,
            ],
        ];

        $response = $this->client->post('/api.php/ws/annonceservice/update?ticket='.$params['token'], $option);

        $this->assertEquals(200, $response->getStatusCode());
    }
}
