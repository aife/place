<?php

namespace Tests\Functional\Api\EspaceDocumentaire;

use Tests\Functional\Controller\Api\ApiWebTestCase;

class APIEspaceDocumentaireControllerTest extends ApiWebTestCase
{
    protected $options;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userPasswordInDb = 'phpunit_test/phpunit_test0123';
        $this->options = [
            'headers' => ['Authorization' => 'Bearer '.$this->getTicket()],
        ];
    }

    public function testGetPlisTokenInvalidJson()
    {
        $url = $this->endpoint.'.json';
        $url = '/app.php/api/v1/espace-documentaire/plis/1600.json';
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json', 403);
    }

    public function testGetPlisTokenValidJson()
    {
        $url = $this->endpoint.'.json';
        $url = '/app.php/api/v1/espace-documentaire/plis/1600.json';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json', 200);
    }

    public function testGetPlisTokenValidXml()
    {
        $url = $this->endpoint.'.xml';
        $url = '/app.php/api/v1/espace-documentaire/plis/1600.xml';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml', 200);
    }

    public function testRCTokenInvalidJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/rc/1600.json';
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json', 403);
    }

    public function testRCTokenValidJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/rc/1600.json';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json', 200);
    }

    public function testRCTokenValidXml()
    {
        $url = '/app.php/api/v1/espace-documentaire/rc/1600.xml';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml', 200);
    }

    public function testAutresPieceTokenInvalidJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/autres-pieces/1600.json';
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json', 403);
    }

    public function testAutresPieceTokenValidJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/autres-pieces/1600.json';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json', 200);
    }

    public function testAutresPieceTokenValidXml()
    {
        $url = '/app.php/api/v1/espace-documentaire/autres-pieces/1600.xml';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml', 200);
    }

    public function testAuthInvalidJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/authentification.json';
        $response = $this->client->get($url);
        $this->checkResponse($response, 'json', 403);
    }

    public function testAuthJson()
    {
        $url = '/app.php/api/v1/espace-documentaire/authentification.json';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'json', 200);
    }

    public function testAuthXml()
    {
        $url = '/app.php/api/v1/espace-documentaire/authentification.xml';
        $response = $this->client->get($url, $this->options);
        $this->checkResponse($response, 'xml', 200);
    }
}
