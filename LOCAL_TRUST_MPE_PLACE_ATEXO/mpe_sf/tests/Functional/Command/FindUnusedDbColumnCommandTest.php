<?php

namespace Tests\Functional\Command;

use App\Command\FindUnusedDbColumnCommand;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

class FindUnusedDbColumnCommandTest extends WebTestCase
{
    public function testOK()
    {
        $tester = $this->createCommandTester();
        $response = $tester->execute([]);
        $this->assertTrue($this->isJsonData($response));
    }

    protected function isJsonData($string)
    {
        json_decode($string);

        return JSON_ERROR_NONE == json_last_error();
    }

    protected function createCommandTester()
    {
        $kernel = static::$kernel = $this->createKernel();
        $kernel->boot();
        $application = new Application($kernel);
        $application->add(new FindUnusedDbColumnCommand());
        $command = $application->find('mpe:tools:find-unused-db-column');
        $command->setContainer($kernel->getContainer());

        return new CommandTester($command);
    }
}
