<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\EventSubscriber;

use Exception;
use DateTime;
use App\Entity\Inscrit;
use App\Service\InscritOperationsTracker;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class InscritTrackerSubscriber implements EventSubscriberInterface
{
    /**
     * @var string
     */
    private $currentUri;

    public function __construct(private readonly InscritOperationsTracker $inscritOperationsTracker, private readonly Security $security, private readonly array $pages = [])
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    public function onKernelController(ControllerEvent $event)
    {
        if (!($this->security->getUser() instanceof Inscrit)) {
            return;
        }

        foreach ($this->pages as $page) {
            $this->currentUri = $page['uri'];
            $methodName = $page['method'] ?? 'trackingActions';
            if (str_contains((string) $this->currentUri, $event->getRequest()->getPathInfo())) {
                $this->$methodName();
                break;
            }
        }
    }

    /**
     * @param array $data
     *
     * @throws Exception
     */
    private function trackingActions(string $detail = '-', $data = [])
    {
        $dateDebAction = new DateTime(date('Y-m-d H:i:s'));
        $dateFinAction = new DateTime(date('Y-m-d H:i:s'));
        $this->inscritOperationsTracker->trackingOperations($detail, $dateDebAction, $dateFinAction, $data);
    }
}
