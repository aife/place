<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;
use App\Security\GuardManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class LogoutSubscriber implements EventSubscriberInterface
{
    public function __construct(protected ParameterBagInterface $parameterBag, private readonly GuardManager $guardManager)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [LogoutEvent::class => 'onLogout'];
    }

    public function onLogout(LogoutEvent $logoutEvent): void
    {
        $redirectUri = $this->guardManager->getLogoutUrl($logoutEvent->getRequest());
        $logoutEvent->setResponse(new RedirectResponse($redirectUri));
    }
}
