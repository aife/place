<?php

namespace App\EventSubscriber;

use Exception;
use App\Entity\Autoformation\ModuleAutoformation;
use App\Service\AtexoUtil;
use App\Service\File\FileUploader;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;

class ModuleAutoformationSubscriber implements EventSubscriberInterface
{
    private const FORLDER_ROOT = 'module_formation';
    private const VIDEO = 'video';
    private const VIDEO_PICTURE = 'videoThumbnail';
    private const LOCALE_FR = 'fr';

    /**
     * @param FileUploader $fileUploader
     * @param RequestStack $requestStack
     * @param AtexoUtil $atexoUtil
     */
    public function __construct(
        private readonly FileUploader $fileUploader,
        private readonly RequestStack $requestStack,
        private readonly AtexoUtil $atexoUtil
    ) {
    }

    public function getSubscribedEvents(): array
    {
        return [
            Events::prePersist,
            Events::preUpdate,
            Events::postRemove,
            Events::postLoad
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     * @throws Exception
     */
    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof ModuleAutoformation) {
            return;
        }

        $this->uploadFile($entity);
    }

    public function postRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof ModuleAutoformation) {
            return;
        }

        $this->fileUploader->removeFolder($this->getFullTarget($entity));
    }

    /**
     * @param LifecycleEventArgs $args
     * @return void
     * @throws Exception
     */
    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof ModuleAutoformation) {
            return;
        }
        $request = $this->requestStack->getCurrentRequest();

        $videoPicture = $request->files->get(self::VIDEO_PICTURE);
        $video = $request->files->get(self::VIDEO);

        if ($videoPicture instanceof UploadedFile) {
            $this->fileUploader->remove($entity->getPathPicture());
        }

        if ($video instanceof UploadedFile) {
            $oldFolder = str_replace('/index.html', '', $entity->getPathVideo());
            $this->fileUploader->removeFolder($oldFolder);
        }

        $this->uploadFile($entity);
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if (!$entity instanceof ModuleAutoformation) {
            return;
        }

        $this->fileUploader->setTargetDirectory($this->getFullTarget($entity));
        $this->fileUploader->copyDirectoryToPublic(folderTarget: $this->getFullTarget($entity));
    }

    /**f
     * @param $str
     * @param string $charset
     * @return string
     */
    private function strWithoutAccents($str, $charset = 'utf-8'): string
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset);

        $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|Grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
        $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
        $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères

        return $str;
    }

    private function getFullTarget(ModuleAutoformation $moduleAutoformation): string
    {
        $target = self::FORLDER_ROOT . '/';

        foreach ($moduleAutoformation->getRubrique()->getLangueRubriques() as $langueRubrique) {
            if ($langueRubrique->getLangue()->getLangue() === self::LOCALE_FR) {
                $nameRubrique = strtolower($this->strWithoutAccents($langueRubrique->getNom())) ;
                $rubrique = implode('_', $this->atexoUtil->deleteSpecialCharacteresFromChaine($nameRubrique));
                $target .= $rubrique . '/';
                break;
            }
        }

        foreach ($moduleAutoformation->getLangueModuleAutoformations() as $langueModule) {
            if ($langueModule->getLangue()->getLangue() === self::LOCALE_FR) {
                $nameModule = strtolower($this->strWithoutAccents($langueModule->getNom()));
                $module = implode('_', $this->atexoUtil->deleteSpecialCharacteresFromChaine($nameModule));
                $target .= $module ;
                break;
            }
        }

        return $target;
    }

    /**
     * @param ModuleAutoformation $moduleAutoformation
     * @throws Exception
     */
    private function uploadFile(ModuleAutoformation $moduleAutoformation): void
    {
        $this->fileUploader->setTargetDirectory($this->getFullTarget($moduleAutoformation));
        $request = $this->requestStack->getCurrentRequest();
        $videoPicture = $request->files->get(self::VIDEO_PICTURE);
        $video = $request->files->get(self::VIDEO);

        if ($videoPicture instanceof UploadedFile) {
            $path = $this->fileUploader->upload($videoPicture);
            $moduleAutoformation->setPathPicture($path);
        }

        if ($video instanceof UploadedFile) {
            $path = $this->fileUploader->uploadAndDezip($video);
            $moduleAutoformation->setPathVideo($path);
            $moduleAutoformation->setUrlExterne('');
        }
    }
}
