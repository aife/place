<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\EventSubscriber\Tncp;

use DateTime;
use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\InterfaceSuivi;
use App\Event\ModificationConsultationEvent;
use App\Event\ModificationDceEvent;
use App\Event\PublicationConsultationEvent;
use App\Message\Tncp\Producer\MessageSuiviConsultation;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class SuiviConsultationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
    }

    public static function getSubscribedEvents()
    {
        return [
            PublicationConsultationEvent::class => 'onPublicationConsultation',
            ModificationConsultationEvent::class => 'onPublicationConsultation',
            ModificationDceEvent::class => 'onPublicationConsultation',
        ];
    }

    public function onPublicationConsultation(
        PublicationConsultationEvent
        | ModificationConsultationEvent
        | ModificationDceEvent $event
    ): void {
        $agentTechnique = $this->entityManager->getRepository(Agent::class)->findOneBy([
            'login' => $this->parameterBag->get('AGENT_TECHNIQUE_TNCP')
        ]);
        $interfaceSuivi = $this->createSuiviInterface($event);

        $suiviMessage = new MessageSuiviConsultation(
            $interfaceSuivi->getUuid(),
            $interfaceSuivi->getFlux(),
            $interfaceSuivi->getAction(),
            $event->getIdConsultation(),
            (int) $interfaceSuivi->getIdObjetDestination(),
            $this->parameterBag->get('PF_URL_REFERENCE'),
            $this->parameterBag->get('UID_PF_MPE'),
            $agentTechnique ? $this->tokenManager->create($agentTechnique) : '',
            new DateTime()
        );

        $this->messageBus->dispatch(new Envelope($suiviMessage));
    }

    private function createSuiviInterface(
        PublicationConsultationEvent
        | ModificationConsultationEvent
        | ModificationDceEvent $event
    ): InterfaceSuivi {
        $action = match (true) {
            $event instanceof PublicationConsultationEvent => InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION,
            $event instanceof ModificationConsultationEvent =>
                InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI,
            $event instanceof ModificationDceEvent => InterfaceSuivi::ACTION_MODIFICATION_DCE
        };
        $consultation = $this->entityManager->getRepository(Consultation::class)->find($event->getIdConsultation());
        $idDestination = $this->entityManager->getRepository(InterfaceSuivi::class)
            ->findLastIdDestinationByConsultation($event->getIdConsultation());
        if (! $consultation instanceof Consultation) {
            throw new Exception('The consultation with ID %s not found.');
        }
        $interfaceSuivi = new InterfaceSuivi();
        $interfaceSuivi->setFlux(InterfaceSuivi::TNCP_INTERFACE);
        $interfaceSuivi->setConsultation($consultation);
        $interfaceSuivi->setAction($action);
        $interfaceSuivi->setStatut(InterfaceSuivi::STATUS_EN_ATTENTE);
        $interfaceSuivi->setIdObjetDestination($idDestination);
        $interfaceSuivi->setDateCreation(new DateTime());
        $interfaceSuivi->setDateModification(new DateTime());
        $this->entityManager->persist($interfaceSuivi);
        $this->entityManager->flush();

        return $interfaceSuivi;
    }
}
