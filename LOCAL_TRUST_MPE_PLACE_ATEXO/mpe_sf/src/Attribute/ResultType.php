<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Attribute;

use Attribute;

#[Attribute(Attribute::TARGET_METHOD)]
class ResultType
{
    public final const URL = 'url';
    public final const STRING = 'string';
    public final const JSON_DECODE = 'json_decode';

    public function __construct(public string $type)
    {
    }
}
