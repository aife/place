<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Processor;

use App\Processor\Provider\ProviderInterface;
use Closure;
use Gedmo\Exception\RuntimeException;
use Symfony\Component\Config\Util\XmlUtils;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;
use Symfony\Component\DependencyInjection\Exception\EnvNotFoundException;

/**
 * Class ClientConfigEnvVarProcessor.
 */
class ClientConfigEnvVarProcessor implements EnvVarProcessorInterface
{
    /**
     * ClientConfigEnvVarProcessor constructor.
     */
    public function __construct(private readonly ContainerInterface $container, private readonly ProviderInterface $provider)
    {
    }

    /**
     * @return array|string[]
     */
    public static function getProvidedTypes()
    {
        return [
            'clientconfig' => 'string',
        ];
    }

    /**
     * @param string $prefix
     * @param string $name
     *
     * @return mixed
     */
    public function getEnv($prefix, $name, Closure $getEnv)
    {
        $i = strpos($name, ':');
        $env = null;
        $subPrefix = '';
        if (false !== $i) {
            $result = \explode(':', $name);
            $subPrefix = $result[0];
            $name = $result[1];
        }

        if ('clientconfig' === $prefix) {
            if (isset($_ENV[$name])) {
                $env = $_ENV[$name];
            } elseif (isset($_SERVER[$name]) && !str_starts_with($name, 'HTTP_')) {
                $env = $_SERVER[$name];
            } elseif (false !== $env = getenv($name)) {
                $env = getenv($name);
            } else {
                $env = $this->provider->get($name);
                if (null === $env) {
                    if (!$this->container->hasParameter("env($name)")) {
                        throw new EnvNotFoundException($name);
                    }

                    if (null === $env = $this->container->getParameter("env($name)")) {
                        return;
                    }
                }
            }
        }

        if (!is_scalar($env)) {
            throw new RuntimeException(sprintf('Non-scalar env var "%s" cannot be cast to %s.', $env, $prefix));
        }

        if ('string' === $subPrefix) {
            return (string) $env;
        }

        if ('bool' === $subPrefix) {
            return (bool) self::phpize($env);
        }

        if ('int' === $subPrefix) {
            if (!is_numeric($value = self::phpize($env))) {
                throw new RuntimeException(sprintf('Non-numeric env var "%s" cannot be cast to int.', $name));
            }

            return (int) $env;
        }

        if ('float' === $subPrefix) {
            if (!is_numeric($value = self::phpize($env))) {
                throw new RuntimeException(sprintf('Non-numeric env var "%s" cannot be cast to float.', $name));
            }

            return (float) $env;
        }

        if ('const' === $subPrefix) {
            if (!\defined($env)) {
                throw new RuntimeException(sprintf('Env var "%s" maps to undefined constant "%s".', $name, $env));
            }

            return \constant($env);
        }

        if ('base64' === $subPrefix) {
            return base64_decode($env);
        }

        if ('json' === $subPrefix) {
            $env = json_decode($env, true);
            if (JSON_ERROR_NONE !== json_last_error()) {
                throw new RuntimeException(sprintf('Invalid JSON in env var "%s": '.json_last_error_msg(), $name));
            }

            if (!\is_array($env)) {
                throw new RuntimeException(sprintf('Invalid JSON env var "%s": array expected, %s given.', $name, \gettype($env)));
            }

            return $env;
        }

        return $env;
    }

    private static function phpize($value)
    {
        if (!class_exists(XmlUtils::class)) {
            throw new RuntimeException('The Symfony Config component is required to cast env vars to "bool", "int" or "float".');
        }

        return XmlUtils::phpize($value);
    }
}
