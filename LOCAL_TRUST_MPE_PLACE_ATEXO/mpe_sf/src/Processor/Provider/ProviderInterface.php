<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Processor\Provider;

interface ProviderInterface
{
    public function get($name);

    public function getAll();
}
