<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Processor\Provider;

use Symfony\Contracts\Cache\CacheInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\ItemInterface;

class CacheProvider implements ProviderInterface
{
    private $cache = null;

    /**
     * CacheProvider constructor.
     *
     * @param $cacheProvider
     */
    public function __construct(private readonly ProviderInterface $dbProvider, private readonly CacheInterface $cacheProvider)
    {
    }

    /**
     * @param $name
     *
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public function get($name)
    {
        $this->getAll();

        return $this->cache[$name] ?? null;
    }

    /**
     * @return mixed|null
     *
     * @throws InvalidArgumentException
     */
    public function getAll()
    {
        if (null === $this->cache) {
            $key = 'configClient';
            $this->cache = $this->cacheProvider->get($key, function (ItemInterface $item) {
                $item->expiresAfter(3600);

                return $this->dbProvider->getAll();
            });
        }

        return $this->cache;
    }
}
