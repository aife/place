<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Processor\Provider;

use PDO;
use PDOException;
use Exception;

class DbProvider implements ProviderInterface
{
    protected ?array $dbParams = null;

    /**
     * @param $name
     * @return mixed
     * @throws Exception
     */
    public function get($name)
    {
        if (!isset($this->dbParams)) {
            $this->dbParams = $this->getAll();
        }

        return $this->dbParams[$name] ?? null;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getAll()
    {
        return $this->load();
    }

    /**
     * @return array
     * @throws Exception
     */
    private function load()
    {
        $mandatoryVariables = [
            'DB_PREFIX',
            'DATABASE_USERNAME',
            'DATABASE_PASSWORD',
        ];
        foreach ($mandatoryVariables as $mandatoryVariable) {
            if (empty(getenv($mandatoryVariable))) {
                throw new Exception('Variable d\'environnement manquante : ' . $mandatoryVariable);
            }
        }

        $database = 'MPE_DB';
        $port = empty(getenv('DATABASE_PORT')) ? '' : 'port=' . getenv('DATABASE_PORT') . ';';
        $prefix = getenv('DB_PREFIX') . '_';
        $user = getenv('DATABASE_USERNAME');
        $pass = getenv('DATABASE_PASSWORD');

        $dsn = 'mysql:host=' . getenv('DATABASE_HOST') . ';' . $port
            . 'dbname=' . $prefix . $database . ';charset=utf8';
        try {
            $pdo = new PDO($dsn, $user, $pass);

            $request = 'SELECT `parameter`, `value` FROM `configuration_client`';
            $stmt = $pdo->prepare($request);
            $stmt->execute();
            $result = $stmt->fetchAll($pdo::FETCH_KEY_PAIR);
        } catch (PDOException $exception) {
            // Si on n'a pas accès à la BDD, on retourne un  tableau vide.
            $result = [];
        } catch (Exception $exception) {
            throw new Exception(sprintf("Problem with PDO::fetchall %s", $exception->getMessage()));
        }
        if (!is_array($result)) {
            $type = gettype($result);
            throw new Exception("Problem with PDO::fetchall which returned $type instead of array");
        }

        return $result;
    }
}
