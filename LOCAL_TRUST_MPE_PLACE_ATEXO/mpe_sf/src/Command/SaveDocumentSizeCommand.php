<?php

namespace App\Command;

use App\Entity\DocumentServeurDocs;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class SaveDocumentSizeCommand.
 */
class SaveDocumentSizeCommand extends Command
{
    protected static $defaultName = 'docs:save-filesize';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Enregistrer le poids des documents MPE à télécharger')
            ->addArgument('type', InputArgument::OPTIONAL, 'Whether the file is for Agent or Enterprise');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;

        $filesUrlList = [
            'URL_DOCS_GUIDE_UTILISATEUR_ENTREPRISE',
            'URL_DOCS_GUIDE_AUTOFORMATION_ENTREPRISE',
        ];

        foreach ($filesUrlList as $fileUrlCode) {
            $output->writeln('Check if file exists in DB');

            $docUrl = $this->parameterBag->get($fileUrlCode);

            if (str_starts_with($docUrl, '/')) {
                $docUrl = $this->parameterBag->get('PF_URL_REFERENCE').ltrim($docUrl, '/');
            }
            $explode = explode('/', (string) $docUrl);

            $fileName = array_pop($explode);

            $fileTmpPath = '/tmp/'.$fileName;
            $fileHandle = fopen($docUrl, 'r');

            if (false === $fileHandle) {
                $output->writeln('Error while trying to get file : '.$docUrl);
                continue;
            }

            file_put_contents($fileTmpPath, $fileHandle);
            $fileSize = filesize($fileTmpPath);

            $documentServeurDoc = $this->em
                ->getRepository(DocumentServeurDocs::class)
                ->findOneBy(['nom' => pathinfo($fileUrlCode, PATHINFO_FILENAME)]);

            if (null === $documentServeurDoc) {
                $documentServeurDoc = new DocumentServeurDocs();
            }

            $output->writeln('Set new values for file in DB');

            $output->writeln(sprintf(
                '| %s | %s | %s | %s |',
                $fileUrlCode,
                $docUrl,
                $fileName,
                $fileSize
            ));

            $documentServeurDoc
                ->setNom(pathinfo($fileTmpPath, PATHINFO_FILENAME))
                ->setExtension(pathinfo($fileTmpPath, PATHINFO_EXTENSION))
                ->setPoids($fileSize);

            $output->writeln('Save file');

            $em->persist($documentServeurDoc);
        }

        $em->flush();
        return 0;
    }
}
