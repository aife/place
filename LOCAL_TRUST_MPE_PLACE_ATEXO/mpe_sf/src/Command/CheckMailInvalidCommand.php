<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\Email;
use App\Entity\Inscrit;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CheckMailInvalidCommand.
 */
class CheckMailInvalidCommand extends Command
{
    protected static $defaultName = 'mpe:check:mail';
    protected ?string $fileName = null;
    protected ?Email $emailConstraint = null;
    protected $limit;
    protected int $offset = 0;

    final const ARGUMENT_LIMIT = 'limit';
    final const ARGUMENT_MEMORY = 'memory';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ValidatorInterface $validator
    ) {
        parent::__construct();
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setDescription('Commande qui permet de vérifier la validité des mail valide')
            ->addArgument(
                'repertoirDest',
                InputArgument::REQUIRED,
                'Repertoire de destination de l\'export des compte au mail invalide'
            )
            ->addArgument(
                self::ARGUMENT_LIMIT,
                InputArgument::OPTIONAL,
                'limit le nombre de traitement valeur par défaut 1000'
            )
            ->addArgument(
                self::ARGUMENT_MEMORY,
                InputArgument::OPTIONAL,
                'Affiche la mémoire consommer'
            )
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if ($input->getArgument(self::ARGUMENT_MEMORY)) {
            $output->writeln(sprintf('Memory usage (currently) %dKB/ (max) %dKB', round(memory_get_usage(true) / 1024), memory_get_peak_usage(true) / 1024));
        }
        $repertoirDest = $input->getArgument('repertoirDest');
        $this->limit = 1000;

        if ($input->getArgument(self::ARGUMENT_LIMIT)) {
            $this->limit = $input->getArgument(self::ARGUMENT_LIMIT);
        }

        $this->emailConstraint = new Assert\Email();
        $this->emailConstraint->message = 'Invalid email address';
        $em = $this->em;

        $this->fileName = 'listeEmailInvalid_'.date('Y-m-d-H-i-s').'.csv';
        $fp = fopen($repertoirDest.$this->fileName, 'w+');
        $string = "id,email\n";
        fwrite($fp, $string);
        fclose($fp);

        $repository = $em->getRepository(Inscrit::class);
        foreach ($repository->findAllInscrits($this->limit) as $inscrit) {
            $this->checkMail($inscrit, $repertoirDest);
        }

        if ($input->getArgument(self::ARGUMENT_MEMORY)) {
            $output->writeln(sprintf('Memory usage (currently) %dKB/ (max) %dKB', round(memory_get_usage(true) / 1024), memory_get_peak_usage(true) / 1024));
        }
        return 0;
    }

    private function checkMail(Inscrit $inscrit, $repertoirDest)
    {
        $errors = $this->validator->validate(
            $inscrit->getEmail(),
            $this->emailConstraint
        );
        if (0 !== (is_countable($errors) ? count($errors) : 0)) {
            $string = $inscrit->getId().','.$inscrit->getEmail();
            $fp = fopen($repertoirDest.$this->fileName, 'a+');
            fwrite($fp, $string."\n");
            fclose($fp);
        }
    }
}
