<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class BlobOrphelinCommand.
 */
class BlobOrphelinCommand extends Command
{
    protected static $defaultName = 'blob_orphelin';
    public const ORGANISME = 'organisme';
    public const PIECE = 'piece';
    public const ID_BLOB = 'id_blob';
    public const FICHIER = 'fichier';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription('Permet de lister les blobs orphelins')
            ->addArgument(
                'maxLimit',
                InputArgument::OPTIONAL,
                'limit nbr blob a traiter'
            )

        ;
    }

    /**
     * Permet d'executer la commande console.
     *
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;
        $fileNameTrouve = rtrim($this->parameterBag->get('COMMON_TMP'), '/').'/'.'listeBlobTrouve_'.date('Y-m-d-H-i-s').'.csv';
        $fp1 = fopen($fileNameTrouve, 'a+');
        $fileName = rtrim($this->parameterBag->get('COMMON_TMP'), '/').'/'.'listeBlobOrphelin_'.date('Y-m-d-H-i-s').'.csv';
        $fp = fopen($fileName, 'a+');
        $ch = ' Date debut '.date('d/m/Y H:i:s')." \n";
        $ch .= " organisme | id | name | empalcement \n";
        fwrite($fp, $ch);
        fwrite($fp1, $ch);
        $dedans = true;
        $maxLimit = (int) $input->getArgument('maxLimit');
        $offset = 0;
        $limit = 20000;
        if (isset($maxLimit) && $maxLimit) {
            $limit = $maxLimit;
        }
        while ($dedans) {
            echo ' limit => '.$limit;
            echo '  offset => '.$offset." \n";
            $dedans = self::traitement($limit, $offset, $fp, $fp1, $em);
            $offset += $limit;
            if (isset($maxLimit)) {
                $dedans = false;
            }
        }
        $ch = ' Date Fin '.date('d/m/Y H:i:s')." \n";
        fwrite($fp, $ch);
        fclose($fp);
        fwrite($fp1, $ch);
        fclose($fp1);
        echo 'voir : '.$fileName.'et '.$fileNameTrouve;
        return 0;
    }

    public function traitement($limit, $offset, $fp, $fp1, EntityManagerInterface $em)
    {
        $SQL = "select * from blobOrganisme_file order by organisme limit $limit offset $offset ";
        $stmt = $em->getConnection()->prepare($SQL);
        $stmt->execute();
        $dedans = false;
        while ($info = $stmt->fetch()) {
            $dedans = true;
            $sql2 = self::verifyBlob($info['id'], $info[self::ORGANISME]);
            $stmt2 = $em->getConnection()->prepare($sql2);
            $rs2 = $stmt2->executeQuery()->fetchAllAssociative();
            echo 'id blob : '.$info['id'];
            if (0 == (is_countable($rs2) ? count($rs2) : 0)) {
                echo " ==> PAS TROUVE \n";
                $ch = $info[self::ORGANISME].'|'.$info['id'].'|'.$info['name'].'|'.rtrim($this->parameterBag->get('BASE_ROOT_DIR'), '/').'/'.$info[self::ORGANISME].'/files/'.$info['id'].'-'.$info['revision']." \n";
                fwrite($fp, $ch);
            } else {
                echo " ==> TROUVE \n";
                $chTrouve = $info[self::ORGANISME].'|'.$info['id'].'|'.$info['name'].'|'.rtrim($this->parameterBag->get('BASE_ROOT_DIR'), '/').'/'.$info[self::ORGANISME].'/files/'.$info['id'].'-'.$info['revision']." \n";
                fwrite($fp1, $chTrouve);
            }
            $em->clear();
        }
        $em->clear();

        return $dedans;
    }

    public function verifyBlob($idBlob, $organisme)
    {
        $sql2 = "select id from (
select id_blob_fiche_modificative AS id from t_chorus_fiche_modificative where organisme='$organisme' AND  (id_blob_fiche_modificative ='$idBlob' OR id_blob_fiche_modificative LIKE ('$idBlob#%') OR id_blob_fiche_modificative LIKE ('%#$idBlob#%') OR id_blob_fiche_modificative LIKE ('%#$idBlob'))
union 
select DN.blob_id AS id from t_dume_numero DN,t_dume_contexte DC where DN.blob_id ='$idBlob' AND DN.id_dume_contexte=DC.id AND DC.organisme='$organisme'
union
select DN.blob_id_xml AS id from t_dume_numero DN,t_dume_contexte DC where DN.blob_id_xml ='$idBlob' AND DN.id_dume_contexte=DC.id AND DC.organisme='$organisme'
union 
select id_blob AS id from Societes_Exclues where organisme_acronyme='$organisme' AND id_blob ='$idBlob'
union  
select id_blob AS id from Autres_Pieces_Mise_Disposition where org='$organisme' AND id_blob ='$idBlob'";

        $values = [
            't_chorus_fiche_modificative' => 'id_blob_piece_justificatives',
            'DCE' => 'Dce',
            'Complement' => 'Complement',
            'RG' => 'RG',
            'questions_dce' => 'id_fichier',
            'EchangePieceJointe' => self::PIECE,
            'Annonce_Press_PieceJointe' => self::PIECE,
            'AVIS' => 'Avis',
            'blocFichierEnveloppe' => 'id_blob_chiffre',
            'fichierEnveloppe' => self::ID_BLOB,
            't_document' => 'id_fichier',
            'blocFichierEnveloppe' => 'id_blob_dechiffre',
            'blocFichierEnveloppeTemporaire' => self::ID_BLOB,
            'Annonce_Press_PieceJointe' => self::PIECE,
            'DocumentExterne' => 'IdBlob',
            'Chorus_Fiche_Navette' => 'id_document',
            'decisionEnveloppe' => 'id_blob_pieces_notification',
            'decisionEnveloppe' => 'fichier_joint',
            'fichiers_liste_marches' => self::FICHIER,
            'programme_previsionnel' => self::FICHIER,
            't_synthese_rapport_audit' => self::FICHIER,
            'AdresseFacturationJal' => 'id_blob_logo',
            'Chorus_pj' => self::FICHIER,
            'AnnonceJALPieceJointe' => self::PIECE,
            'Offres' => 'id_pdf_echange_accuse',
            't_candidature_mps' => self::ID_BLOB,
            'Helios_cosignature' => self::ID_BLOB,
            'Helios_piece_publicite' => self::FICHIER,
            'Helios_pv_consultation' => self::FICHIER,
            'Helios_rapport_prefet' => self::FICHIER,
            'Helios_tableau_ar' => self::FICHIER,
            'Helios_teletransmission' => 'Piecej1',
            'Helios_teletransmission' => 'Piecej2',
        ];

        foreach ($values as $table => $field) {
            $sql2 .= "union select $field AS id from $table where organisme='$organisme'AND $field ='$idBlob'";
        }

        return $sql2.') AS T';
    }
}
