<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InitialisationConsultationFavorisCommand.
 */
class InitialisationConsultationFavorisCommand extends Command
{
    protected static $defaultName = 'mpe:initialisation:consultations:favories';
    protected ?OutputInterface $output = null;
    protected ?array $tab = null;

    public function __construct(private EntityManagerInterface $entityManager, private LoggerInterface $logger)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription("Commande qui permet d'initialiser les données sur les consultations favories")
            ->addArgument(
                'memory',
                InputArgument::OPTIONAL,
                'Affiche la mémoire consommée'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $memoryArg = $input->getArgument('memory');
        if ($memoryArg) {
            $output->writeln(sprintf(
                'Memory usage (currently) %dkB/ (max) %dkB',
                round(memory_get_usage(true) / 1024),
                memory_get_peak_usage(true) / 1024
            ));
        }

        $this->output = $output;
        $this->logger->info("Début de l'execution de la commande mpe:initialisation:consultation:favoris");
        $this->output->setDecorated(true);
        $this->tab = [
            'idEtatConsultation' => 5,
            'dateDebut' => '0000-00-00',
            'dateFin' => '2014-01-01',
        ];

        $this->initialisationFavorisParCreateur();
        $this->initialisationFavorisParInvitePermanent();
        $this->initialisationFavorisParInvitePonctuelAvecHabilitation();
        $this->initialisationFavorisParInvitePonctuel();

        if ($memoryArg) {
            $output->writeln(sprintf(
                'Memory usage (currently) %dkB/ (max) %dkB',
                round(memory_get_usage(true) / 1024),
                memory_get_peak_usage(true) / 1024
            ));
        }

        $this->logger->info("\nFin de l'execution de la commande mpe:initialisation:consultation:favoris ");
        return 0;
    }

    private function initialisationFavorisParCreateur()
    {
        $this->output->writeln("Début d'initialisation des consultations favories créées par l'agent\n");
        $listConsutlations = $this->entityManager->getRepository(Consultation::class)->findListReprise($this->tab);
        $progressBar = new ProgressBar($this->output, is_countable($listConsutlations) ? count($listConsutlations) : 0);
        $progressBar->start();
        $agentNotExiste = [];
        foreach ($listConsutlations as $consultation) {
            $progressBar->advance();
            try {
                $consultationFavoris = new ConsultationFavoris();
                $idCreateur = $consultation['idCreateur'];
                if (0 === $idCreateur) {
                    continue;
                }

                $check = $this->checkDoublonFavoris($idCreateur, $consultation['id']);
                if (false === $check) {
                    $agent = $this->entityManager->getRepository(Agent::class)->find($idCreateur);
                    $objectConsultation = $this->entityManager->getRepository(Consultation::class)
                        ->find($consultation['id']);
                    if (false === $this->creerConsultationFavoris($agent, $objectConsultation)) {
                        $agentNotExiste[] = "L'agent  avec l'id : ".
                            $idCreateur.
                            " n'existe pas en Base de données. Cet agent est le créateur de la consultation  id : "
                            .$consultation['id'].')';
                    }
                }
            } catch (Exception $exception) {
                $this->logger->error($exception->getMessage());
            }
        }
        $progressBar->finish();
        $this->entityManager->flush();

        foreach ($agentNotExiste as $log) {
            $this->logger->error($log);
        }
        $this->output->writeln("\nFin d'initialisation des consultations favories créés par l'agent\n");
    }

    private function initialisationFavorisParInvitePermanent()
    {
        $this->output->writeln("\nDébut d'initialisation des consultations favories pour un invité permanent par l'agent\n");
        $this->initialisationFavorisInvitePermanentMonEntite();
        $this->output->writeln("\nFin d'initialisation des consultations favories pour un invité permanent par l'agent\n");
    }

    private function initialisationFavorisInvitePermanentMonEntite()
    {
        $this->output->writeln("\nDébut d'initialisation des consultations favories pour un invité permanent par l'agent : phase 1 / Profile Invite Permanent Mon Entite\n");

        $listAgent = $this->entityManager
            ->getRepository(Agent::class)
            ->getAgentPermanent(['invite_permanent_mon_entite' => '1']);

        $progressBar = new ProgressBar($this->output, is_countable($listAgent) ? count($listAgent) : 0);
        $progressBar->start();
        foreach ($listAgent as $agent) {
            $progressBar->advance();

            $tab = $this->tab;
            $tab['service'] = $agent->getServiceId();
            $tab['acronymeOrg'] = $agent->getOrganisme()->getAcronyme();

            $listConsutlations = $this->entityManager
                ->getRepository(Consultation::class)
                ->findListReprise($tab);

            foreach ($listConsutlations as $consultation) {
                $check = $this->checkDoublonFavoris($agent->getId(), $consultation['id']);
                if (false === $check) {
                    $objectConsultation = $this->entityManager->getRepository(Consultation::class)
                        ->find($consultation['id']);
                    $this->creerConsultationFavoris($agent, $objectConsultation);
                }
            }
        }
        $progressBar->finish();
        $this->entityManager->flush();
        $this->output->writeln("\nFin d'initialisation des consultations favories pour un invité permanent par l'agent : phase 1 / Profile Invite Permanent Mon Entite\n");
    }

    private function initialisationFavorisParInvitePonctuel()
    {
        $this->output->writeln("\nDébut d'initialisation des consultations favories pour un invité ponctuel par l'agent : phase 3\n");
        $listConsutlations = $this->entityManager->getRepository(InterneConsultation::class)->findListConsultations($this->tab);

        $progressBar = new ProgressBar($this->output, is_countable($listConsutlations) ? count($listConsutlations) : 0);
        $progressBar->start();

        foreach ($listConsutlations as $consultation) {
            $progressBar->advance();
            $interneId = $consultation['interne_id'];
            $consultationId = $consultation['consultation_id'];
            $check = $this->checkDoublonFavoris($interneId, $consultationId);
            if (false === $check) {
                $agent = $this->entityManager->getRepository(Agent::class)->find($interneId);
                $objectConsultation = $this->entityManager->getRepository(Consultation::class)
                    ->find($consultationId);
                $this->creerConsultationFavoris($agent, $objectConsultation);
            }
        }

        $progressBar->finish();
        $this->entityManager->flush();

        $this->output->writeln("\nFin d'initialisation des consultations favories pour un invité ponctuel par l'agent : phase 3\n");
    }

    private function initialisationFavorisParInvitePonctuelAvecHabilitation()
    {
        $this->output->writeln("\nDébut d'initialisation des consultations favories pour un invité ponctuel par l'agent avec habilitation: phase 2\n");
        $listConsutlations = $this->entityManager->getRepository(InterneConsultationSuiviSeul::class)->findListConsultations($this->tab);

        $progressBar = new ProgressBar($this->output, is_countable($listConsutlations) ? count($listConsutlations) : 0);
        $progressBar->start();

        foreach ($listConsutlations as $consultation) {
            $progressBar->advance();
            $interneId = $consultation['interne_id'];
            $consultationId = $consultation['consultation_id'];
            $check = $this->checkDoublonFavoris($interneId, $consultationId);
            if (false === $check) {
                $agent = $this->entityManager->getRepository(Agent::class)->find($interneId);
                $objectConsultation = $this->entityManager->getRepository(Consultation::class)
                    ->findOneBy(['reference' => $consultationId]);
                $this->creerConsultationFavoris($agent, $objectConsultation);
            }
        }

        $progressBar->finish();
        $this->entityManager->flush();

        $this->output->writeln("\nFin d'initialisation des consultations favories pour un invité ponctuel par l'agent avec habilitation : phase 2\n");
    }

    private function checkDoublonFavoris($idAgent, $idConsultation)
    {
        $doublon = false;
        $consultationFavoris = $this->entityManager->getRepository(ConsultationFavoris::class)
            ->findOneBy(['agent' => $idAgent, 'consultation' => $idConsultation]);

        if ($consultationFavoris instanceof ConsultationFavoris) {
            $doublon = true;
        }

        return $doublon;
    }

    public function creerConsultationFavoris($agent, $objectConsultation)
    {
        $creationOk = false;
        if ($agent instanceof Agent && $objectConsultation instanceof Consultation) {
            $consultationFavoris = new ConsultationFavoris();
            $consultationFavoris->setAgent($agent);
            $consultationFavoris->setConsultation($objectConsultation);
            $this->entityManager->persist($consultationFavoris);
            $creationOk = true;
        }

        return $creationOk;
    }
}
