<?php

namespace App\Command;

use App\Message\UpdateMarchePublie;
use App\Service\CommandMonitoringService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\MarchePublie;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class RedressementDonneeJuridiqueCommand.
 */
class UpdateMarchePublieCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:update:marchePublie';
    protected function configure()
    {
        $this->setDescription("Commande qui permet d'activer la publication pour  
        les organisme/services pour une année données (on fait des insertion)")
            ->addArgument(
                'annee',
                InputArgument::OPTIONAL,
                " L'année pour laquelle on active la publication(valeur par defaut : l'année en cours)"
            );
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly MessageBusInterface $bus
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = null;
        $logger = $this->logger;
        $logger->info("Début de l'execution de la commade mpe:update:marchePublie ");
        $date = new DateTime('now');
        $annee = $date->format('Y');
        $anneeArg = $input->getArgument('annee');
        if ($anneeArg) {
            $annee = $anneeArg;
            $logger->info('Le parametre année =  ' . $annee);
        } else {
            $logger->info('Le parametre année par défaut est  ' . $annee);
        }

        if ($annee) {
            try {
                $em = $this->em;
                $resultInsert = $em->getRepository(MarchePublie::class)->insererMarchePublie($annee, $logger);
            } catch (Exception $e) {
                $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
                $logger->error("Erreur lors de l'execution de la comande  mpe:update:marchePublie" . PHP_EOL . $erreur);
                $this->logger->info('lancement message UpdateMarchePublie pour l\'annee :' . $annee);
                $this->bus->dispatch(new UpdateMarchePublie($annee));
            }
        }
        $logger->info("Fin de l'execution de la commade mpe:update:marchePublie ");
        return 0;
    }
}
