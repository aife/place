<?php

namespace App\Command;

use App\Entity\Entreprise;
use App\Service\AtexoEntreprise;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class RedressementDonneeJuridiqueCommand.
 */
class RedressementDonneeJuridiqueCommand extends Command
{
    protected static $defaultName = 'mpe:entreprise:redressement';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoEntreprise $atexoEntreprise,
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Commande qui permet de redresser les données d\'une entreprise via SGMAP ')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Limitation du redressement (valeur par defaut : 1)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = $input->getArgument('limit') ?: 1;
        $query = $this->em->getRepository(Entreprise::class)
            ->createQueryBuilder('e')
            ->where('e.formejuridique like :formejuridique')
            ->setParameter('formejuridique', '%?%')
            ->setMaxResults($limit)
            ->getQuery();

        $listEntreprise = $query->getResult();

        foreach ($listEntreprise as $entreprise) {
            $siren = $entreprise->getSiren();
            $atexoEntreprise = $this->atexoEntreprise;
            $atexoEntreprise->synchroEntrepriseAvecApiGouvEntreprise($siren, $entreprise);
        }
        return 0;
    }
}
