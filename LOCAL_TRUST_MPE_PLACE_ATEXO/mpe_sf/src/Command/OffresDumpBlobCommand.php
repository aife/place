<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Command;

use PDO;
use App\Entity\BloborganismeFile;
use App\Entity\Offre;
use App\Service\AtexoFichierOrganisme;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Generator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class OffresDumpBlobCommand.
 */
class OffresDumpBlobCommand extends Command
{
    protected static $defaultName = 'mpe:offres:dump-blob';
    private const LIMIT = 100;
    private const ORGANISME = 'organisme';

    private $commonTmp;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoFichierOrganisme $fichierOrganismeService
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Export sur le filesystem du champ xml_string de la table Offres');
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $memoryUsage = null;
        $io = new SymfonyStyle($input, $output);
        $this->commonTmp = $this->parameterBag->get('COMMON_TMP');

        $io->comment('Début de la migration du XML Réponse');

        try {
            $count = 0;
            $this->entityManager->getConnection()->beginTransaction();

            foreach ($this->findAll() as $offre) {
                $memoryUsage = memory_get_usage(true) / 1024 / 1024;

                if (0 === $count % self::LIMIT) {
                    $this->entityManager->getConnection()->commit();
                    $msg = "\nMémoire utilisée : ${memoryUsage}M";
                    $msg .= ' - Offres traitées : ' . $count;
                    $io->text($msg);
                    $io->text('Identifiant de la dernière offre traitée : ' . $offre['id']);
                    $this->entityManager->getConnection()->beginTransaction();
                }

                $this->traitement($io, $offre);
                ++$count;
            }
            $this->entityManager->getConnection()->commit();
            $io->text("Mémoire utilisée : ${memoryUsage}M");
            $io->text('Offres traitées : ' . $count);
        } catch (\Exception $exception) {
            $io->write('Erreur :' . $exception);
        }
        $io->comment('Fin de la migration du XML Réponse');
        return 0;
    }

    /**
     * @param $offre
     * @throws ConnectionException
     * @throws Exception
     */
    protected function traitement(SymfonyStyle $io, $offre)
    {
        try {
            $fileName = '%s-%s-%s.xml';
            $fileName = sprintf($fileName, $offre[self::ORGANISME], $offre['consultation_id'], $offre['id']);
            $tmpFilePath = $this->commonTmp . $fileName;

            file_put_contents($tmpFilePath, $offre['xml_string']);

            $this->fichierOrganismeService->moveTmpFile($fileName);
            $idBlob = $this->fichierOrganismeService->insertFile(
                $fileName,
                $tmpFilePath,
                $offre[self::ORGANISME],
                'xml_reponse'
            );
            $blobOrganisme = $this->entityManager->getRepository(BloborganismeFile::class)->findOneById($idBlob);

            $this->update($offre, $blobOrganisme);

            $this->entityManager->clear(BloborganismeFile::class);
            unlink($tmpFilePath);
        } catch (\Exception $exception) {
            $this->entityManager->getConnection()->rollBack();

            if ($offre instanceof Offre) {
                $offreId = $offre->getId();
            } else {
                $offreId = 'null';
            }
            $io->write(
                "Problème de migration du xml sur l'offre "
                . $offreId
                . ". Message d'erreur : "
                . $exception->getMessage()
            );
        }
    }

    /**
     * @param $offre
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function update($offre, BloborganismeFile $blobOrganisme)
    {
        $sql = 'update Offres set id_blob_xml_reponse = ? WHERE id = ? and organisme = ? ';
        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->bindValue(1, $blobOrganisme->getId());
        $stmt->bindValue(2, $offre['id']);
        $stmt->bindValue(3, $offre[self::ORGANISME]);
        $stmt->execute();
        unset($offre);
    }

    /**
     * @return Generator
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    private function findAll()
    {
        $sql = 'SELECT id, organisme, xml_string, consultation_id FROM Offres WHERE id_blob_xml_reponse IS NULL ';
        $sql .= "AND xml_string IS NOT NULL AND trim(xml_string) != '' LIMIT :offset, :limit";
        $stmt = $this->entityManager->getConnection()->prepare($sql);

        $limit = self::LIMIT;

        $offset = 0;

        while (true) {
            $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
            $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
            $results = $stmt->executeQuery()->fetchAllAssociative();

            if (0 === count($results)) {
                break;
            }

            foreach ($results as $result) {
                yield $result;
            }
            unset($results);
        }
    }
}
