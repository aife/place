<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use Exception;
use App\Entity\TMesRecherches;
use App\Service\DataTransformer\SearchCriteriaTransformer;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SearchCriteriaTransformerCommand extends Command
{
    protected static $defaultName = 'mpe:search-criteria:transform';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SearchCriteriaTransformer $criteriaTransformer,
        private readonly LoggerInterface $alertesentrepriseLogger
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Transform alert Criteria  from XML format to JSON.')
            ->addOption(
                'with-entity',
                'we',
                InputOption::VALUE_NONE,
                'Transform only alert with acronymOrg setted'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $output->writeln('Transforming alert Criteria from XML format to JSON ...');
            $criterias = $this->em->createQuery('SELECT a FROM ' . TMesRecherches::class . ' a');
            $alertsCount = 0;
            /**
             * @var TMesRecherches $critaria
             */
            foreach ($criterias->toIterable() as $critaria) {
                if ($critaria->getXmlCriteria()) {
                    try {
                        $output->writeln(sprintf('Transform alert with id %s.', $critaria->getId()));
                        $this->alertesentrepriseLogger->info(
                            sprintf('Alert-email: Transform alert with id %s.', $critaria->getId()),
                            ['alert-body' => unserialize($critaria->getXmlCriteria())]
                        );
                        $critaria->setCriteria(
                            $this->criteriaTransformer->transform(unserialize($critaria->getXmlCriteria()))
                        );
                        $critaria->setHashedCriteria(md5(json_encode($critaria->getCriteria(), JSON_THROW_ON_ERROR)));

                        $alertsCount++;
                        if (0 === $alertsCount % 20) {
                            $this->em->flush();
                            $this->em->clear();
                        }
                    } catch (\Exception $exception) {
                        $this->alertesentrepriseLogger->error(
                            sprintf('Alert-email: Transforming criteria alert with id %s failed.', $critaria->getId()),
                            [
                                'alert-body' => unserialize($critaria->getXmlCriteria()),
                                'error' => $exception->getMessage(),
                                'trace' => $exception->getTraceAsString()
                            ]
                        )
                        ;
                    }
                }
            }
            $output->writeln(sprintf('%s alertes de recherche sont traité.', $alertsCount));
            $this->em->flush();

            return 0;
        } catch (Exception $exception) {
            $this->alertesentrepriseLogger->error(
                'Alert-email: SearchCriteriaTransformerCommand has stopped with exception ',
                ['error' => $exception->getMessage(), 'trace' => $exception->getTraceAsString()]
            );

            return (int) $exception->getCode();
        }
    }
}
