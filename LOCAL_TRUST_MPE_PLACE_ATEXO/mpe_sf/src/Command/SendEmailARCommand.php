<?php

/**
 * Commande pour envoyer les email AR de facon asynchrone.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */

namespace App\Command;

use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Event\ResponseEvents;
use App\Event\ValidateResponseEvent;
use App\Listener\ResponseListener;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SendEmailARCommand extends Command
{
    protected static $defaultName = 'mpe:depot:email-ar';
    protected function configure()
    {
        $this->setDescription('Permet l\'envoi d\'email AR en asynchrone')
            ->addArgument(
                'reference_consultation',
                InputArgument::REQUIRED,
                'Reference de la consultation'
            )
            ->addArgument(
                'id_offre',
                InputArgument::REQUIRED,
                'Identifiant de l\'offre'
            )
            ->addArgument(
                'id_inscrit',
                InputArgument::REQUIRED,
                'Identifiant de linscrit'
            )
            ->addArgument(
                'id_candidature_mps',
                InputArgument::REQUIRED,
                'Identifiant candidature MPS'
            );
    }

    /**
     * SendEmailARCommand constructor.
     */
    public function __construct(
        protected EntityManagerInterface $entityManager,
        protected ResponseListener $responseListener,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $heureDebutExecution = date('H:i:s');
        $output->writeln('Started at '.$heureDebutExecution);

        $refConsultation = $input->getArgument('reference_consultation');
        $idOffre = $input->getArgument('id_offre');
        $idInscrit = $input->getArgument('id_inscrit');
        $idCandidatureMps = $input->getArgument('id_candidature_mps');

        $em = $this->entityManager;
        $consultation = $em->getRepository(Consultation::class)->find($refConsultation);
        $offre = $em->getRepository(Offre::class)->find($idOffre);
        $inscrit = $em->getRepository(Inscrit::class)->find($idInscrit);

        $validateResponseEvent = new ValidateResponseEvent(
            $consultation,
            $offre,
            $inscrit,
            $idCandidatureMps
        ); // candidature Mps en dur

        $eventDispatcher = new EventDispatcher();

        $eventDispatcher->addListener(
            ResponseEvents::ON_RESPONSE_VALIDATION,
            [$this->responseListener, ResponseEvents::CALLBACK[ResponseEvents::ON_RESPONSE_VALIDATION]]
        );

        $eventDispatcher->dispatch($validateResponseEvent, ResponseEvents::ON_RESPONSE_VALIDATION);

        $heureFinExecution = date('H:i:s');
        $output->writeln('Ended at '.$heureFinExecution);

        return 0;
    }
}
