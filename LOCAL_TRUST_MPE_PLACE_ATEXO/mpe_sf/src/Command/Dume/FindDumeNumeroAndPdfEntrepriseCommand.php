<?php

namespace App\Command\Dume;

/**
 * Command to find dume numero and pdf.
 *
 * Class FindDumeNumeroAndPdfEntrepriseCommand
 */
class FindDumeNumeroAndPdfEntrepriseCommand extends FindDumeNumeroAndPdfAbstractCommand
{
    protected static $defaultName = 'dume:entreprise:publish';

    protected string $type = 'entreprise';
}
