<?php

namespace App\Command\Dume;

/**
 * Command to find dume numero and pdf.
 *
 * Class FindDumeNumeroAndPdfAcheteurCommand
 */
class FindDumeNumeroAndPdfAcheteurCommand extends FindDumeNumeroAndPdfAbstractCommand
{
    protected static $defaultName = 'dume:acheteur:publish';

    protected string $type = 'acheteur';
}
