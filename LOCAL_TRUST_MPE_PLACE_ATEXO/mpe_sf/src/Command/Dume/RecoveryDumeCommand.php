<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Dume;

use App\Entity\TDumeContexte;
use App\Message\DumePublication;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RecoveryDumeCommand extends Command
{
    /**
     * Pour le dume acheteur
     */
    public const DEMANDE = 'demande';
    public const ACHETEUR = 'acheteur';

    /**
     * Pour le dume entreprise
     */
    public const REPONSE = 'reponse';
    public const ENTREPRISE = 'entreprise';

    public const VALID = 'valid';
    public const PUBLISH = 'publish';

    protected static $defaultName = 'dume:recovery';


    /**
     * @param LoggerInterface $logger
     * @param ParameterBagInterface $parameterBag
     * @param EntityManagerInterface $em
     */
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly MessageBusInterface $bus,
    ) {
        parent::__construct();
    }

    /**
     * configurer la commande
     */
    protected function configure()
    {
        $this
            ->setDescription(
                'Commande de validation et publication de dume'
            );
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $now = new \DateTime(date('Y-m-d H:i:s', strtotime(' -1 days')));
        $this->getAllValidationDume($output, $now);
        $this->getAllEnAttenteDePublciationDume($output, $now);
        $this->getAllPublciationDumeAcheteur($output, $now);
        $this->getAllPublciationDumeEntreprise($output, $now);

        return 0;
    }

    protected function getAllValidationDume(OutputInterface $output, \DateTime $now)
    {
        $this->logger->info('Début de la vérification des dume a valider');
        $listDumeAValider = $this->em->getRepository(TDumeContexte::class)
            ->getDumesPending(
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE'),
                $now
            );

        foreach ($listDumeAValider as $tDumeNumero) {
            if (self::DEMANDE === $tDumeNumero->getTypeDume()) {
                $this->validationDume(
                    self::ACHETEUR,
                    self::VALID,
                    $tDumeNumero,
                    $output
                );

                $this->validationDume(
                    self::ACHETEUR,
                    self::PUBLISH,
                    $tDumeNumero,
                    $output
                );
            }

            if (self::REPONSE === $tDumeNumero->getTypeDume()) {
                $this->validationDume(
                    self::ENTREPRISE,
                    self::VALID,
                    $tDumeNumero,
                    $output
                );

                $this->validationDume(
                    self::ENTREPRISE,
                    self::PUBLISH,
                    $tDumeNumero,
                    $output
                );
            }
        }
        $this->logger->info('Fin de la vérification des dume a valider');
    }

    protected function validationDume(string $type, string $etape, TDumeContexte $tDumeContexte, OutputInterface $output)
    {
        $this->logger->info('Début validation du dume '.$type.' /  contexte_lt_dume_id : '.$tDumeContexte->getContexteLtDumeId());
        $this->retryAgin($type, $etape, $tDumeContexte->getContexteLtDumeId(), $output);
        $this->logger->info('Fin validation du dume '.$type.' / contexte_lt_dume_id : '.$tDumeContexte->getContexteLtDumeId());
    }

    protected function retryAgin(string $type, string $etape, int $contexte_lt_dume_id, OutputInterface $output)
    {
        try {
            $isPublish = true;
            if ($etape === self::VALID) {
                $isPublish = false;
            }
            $return = $this->bus->dispatch(new DumePublication($type, $isPublish, $contexte_lt_dume_id));
        } catch (Exception $exception) {
            $this->logger->error('Error dispatch. Stack Trace : '.$exception->getTraceAsString());
        }

        return $return;
    }

    protected function getAllEnAttenteDePublciationDume(OutputInterface $output, \DateTime $now)
    {
        $this->logger->info('Début de la vérification des dume a valider');
        $listDumeAPublie = $this->em->getRepository(TDumeContexte::class)
            ->getDumesPending(
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'),
                $now
            );

        foreach ($listDumeAPublie as $tDumeContexte) {
            if (self::DEMANDE === $tDumeContexte->getTypeDume()) {
                $this->validationDume(
                    self::ENTREPRISE,
                    self::VALID,
                    $tDumeContexte,
                    $output
                );

                $this->publicationDume(
                    self::ACHETEUR,
                    self::PUBLISH,
                    $tDumeContexte,
                    $output
                );
            }

            if (self::REPONSE === $tDumeContexte->getTypeDume()) {
                $this->publicationDume(
                    self::ENTREPRISE,
                    self::VALID,
                    $tDumeContexte,
                    $output
                );

                $this->publicationDume(
                    self::ENTREPRISE,
                    self::PUBLISH,
                    $tDumeContexte,
                    $output
                );
            }
        }
        $this->logger->info('Fin de la vérification des dume a valider');
    }

    protected function publicationDume(string $type, string $etape, TDumeContexte $tDumeContexte, OutputInterface $output)
    {
        $this->logger->info('Début publication du dume '.$type.' /  contexte_lt_dume_id : '.$tDumeContexte->getContexteLtDumeId());
        $this->retryAgin($type, $etape, $tDumeContexte->getContexteLtDumeId(), $output);
        $this->logger->info('Fin publication du dume '.$type.' / contexte_lt_dume_id : '.$tDumeContexte->getContexteLtDumeId());
    }

    protected function getAllPublciationDumeAcheteur(OutputInterface $output, \DateTime $now)
    {
        $this->logger->info('Début de la vérification des dume a valider');
        $listDumeAPublie = $this->em->getRepository(TDumeContexte::class)
            ->getDumeARecupererAcheteur(
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_PUBLIE'),
                self::DEMANDE,
                $now
            );

        foreach ($listDumeAPublie as $tDumeContexte) {
            $this->publicationDume(
                self::ACHETEUR,
                self::VALID,
                $tDumeContexte,
                $output
            );

            $this->publicationDume(
                self::ACHETEUR,
                self::PUBLISH,
                $tDumeContexte,
                $output
            );
        }
        $this->logger->info('Fin de la vérification des dume a valider');
    }

    protected function getAllPublciationDumeEntreprise(OutputInterface $output, \DateTime $now)
    {
        $this->logger->info('Début de la vérification des dume a valider');
        $listDumeAPublie = $this->em->getRepository(TDumeContexte::class)
            ->getDumeARecupererEntreprise(
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'),
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_PUBLIE'),
                self::ENTREPRISE,
                $now,
            );

        foreach ($listDumeAPublie as $tDumeContexte) {
            $this->publicationDume(
                self::ENTREPRISE,
                self::VALID,
                $tDumeContexte,
                $output
            );

            $this->publicationDume(
                self::ENTREPRISE,
                self::PUBLISH,
                $tDumeContexte,
                $output
            );
        }
        $this->logger->info('Fin de la vérification des dume a valider');
    }
}
