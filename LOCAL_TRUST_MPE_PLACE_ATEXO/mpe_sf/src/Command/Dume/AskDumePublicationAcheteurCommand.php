<?php

namespace App\Command\Dume;

/**
 * Command to publish dume on dume nationale.
 *
 * Class AskDumePublicationAcheteurCommand
 */
class AskDumePublicationAcheteurCommand extends AskDumePublicationAbstractCommand
{
    protected static $defaultName = 'dume:acheteur:valid';

    protected string $type = 'acheteur';
}
