<?php

namespace App\Command\Dume;

use App\Command\MonitoredCommand;
use App\Message\DumePublication;
use App\Service\CommandMonitoringService;
use Exception;
use Doctrine\DBAL\ConnectionException;
use DateTime;
use App\Entity\BloborganismeFile;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Exception\DumeException;
use App\Service\AtexoDumeService;
use App\Service\DumeLoggerService;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Command to find dume numero and pdf.
 *
 * Class FindDumeNumeroAndPdfAbstractCommand
 */
abstract class FindDumeNumeroAndPdfAbstractCommand extends MonitoredCommand
{
    final public const TYPE_ACHETEUR = 'acheteur';
    final public const TYPE_ENTREPRISE = 'entreprise';
    private const MESSAG_BASE = 'lancement message DumePublication de type entreprise en 
                    mode publication pour le contexte_lt_dume_id : ';

    protected string $type = '';

    /**
     * FindDumeNumeroAndPdfAbstractCommand constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly DumeLoggerService $dumeLogger,
        private readonly AtexoDumeService $atexoDumeService,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly MessageBusInterface $bus
    ) {
        parent::__construct($commandMonitoringService);
    }

    /**
     * Configuration de la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    protected function configure()
    {
        $this->setDescription('Permet de récupérer le numéro du dume national d\'un dume ' . $this->type .
            ' et le fichier pdf en asynchrone')
            ->addArgument(
                'contexte_lt_dume_id',
                InputArgument::REQUIRED,
                'context send by lt_dume and store on table t_dume_contexte'
            );
    }

    /**
     * Permet d'executer la commande console.
     *
     * @return void
     *
     * @throws DumeException
     * @throws ServiceException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $type_dume = null;
        if (self::TYPE_ACHETEUR == $this->type) {
            $type_dume = $this->parameterBag->get('TYPE_DUME_ACHETEUR');
        } elseif (self::TYPE_ENTREPRISE == $this->type) {
            $type_dume = $this->parameterBag->get('TYPE_DUME_OE');
        }

        $logger = $this->dumeLogger;

        $contexteLtDumeId = $input->getArgument('contexte_lt_dume_id');
        $statutPublie = $this->parameterBag->get('STATUT_DUME_CONTEXTE_PUBLIE');
        $dumeContexte = $this->em
            ->getRepository(TDumeContexte::class)
            ->getWaitingOrPublish(
                $contexteLtDumeId,
                $type_dume,
                $this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'),
                $statutPublie
            );

        if (!empty($dumeContexte)) {
            foreach ($dumeContexte as $contexte) {
                try {
                    $message = self::MESSAG_BASE . $contexteLtDumeId;
                    $this->processDumeContext($contexte, $contexteLtDumeId, $output, $logger);
                } catch (ServiceException $e) {
                    $this->logger->info($message);

                    if (0 !== $e->getCode()) {
                        throw new DumeException('Issue with DUME service : ' . $e->getCode());
                    }

                    throw $e;
                } catch (RestRequestException $e) {
                    $this->logger->info($message);

                    throw new DumeException('Issue with DUME service : ' . $e->getCode());
                } catch (Exception $e) {
                    $this->logger->info($message);

                    throw new DumeException('Unknown issue : ' . $e->getMessage());
                }
            }
        } else {
            $logger->addInfo("ContexteLtDumeId not found : $contexteLtDumeId");

            throw new DumeException("ContexteLtDumeId not found : $contexteLtDumeId");
        }
        return 0;
    }

    public function processDumeContext($contexte, $contexteLtDumeId, $output, $logger)
    {
        $numeroDumeNational = null;
        $output->writeln('t_dume_contexte : ' . $contexte->getId());
        $output->writeln('contexte_lt_dume_id : ' . $contexteLtDumeId);

        $logger->addInfo('Beginning command dume:' . $this->type . ':publish');

        $client = $this->atexoDumeService->createConnexionLtDume();

        $logger->addInfo('Client created');
        $logger->addInfo('Start calling getPublishedDume function from mpe-dume library');
        $statutPublie = $this->parameterBag->get('STATUT_DUME_CONTEXTE_PUBLIE');
        if (self::TYPE_ACHETEUR == $this->type) {
            $numeroDumeNational = $client->acheteurService()->getPublishedDume($contexte->getContexteLtDumeId());
        } elseif (self::TYPE_ENTREPRISE == $this->type) {
            $numeroDumeNational = $client->entrepriseService()->getPublishedDume($contexte->getContexteLtDumeId());
        }

        $logger->addInfo('Finishing getPublishedDume function from mpe-dume library');

        $listNumeroSN = $numeroDumeNational->getNumerosDume();

        // Dans le cas d'une publication Acheteur standard (is_standard = 1),
        // nous ne devons pas faire la demande de récupération des éléments SN
        if (self::TYPE_ACHETEUR == $this->type && true === $numeroDumeNational->getIsStandard()) {
            $logger->addInfo('DUME Acheteur standard');
            $output->writeln('DUME Acheteur standard');

            $contexte->setStatus($statutPublie);
            $this->em->flush();

            return;
        }

        if (is_array($listNumeroSN) && !array_key_exists(0, $listNumeroSN)) {
            $logger->addInfo('Information - "numero DUME SN" not yet available');
            $this->logger->info(self::MESSAG_BASE . $contexteLtDumeId);

            throw new DumeException('numero DUME SN not yet available');
        } else {
            $numeroDumeSN = $listNumeroSN[0]['numeroDumeSN'];
            if (null === $numeroDumeSN) {
                $logger->addInfo('Information - "numero DUME SN" not yet available');
                $this->logger->info(self::MESSAG_BASE . $contexteLtDumeId);

                throw new DumeException('numero DUME SN not yet available');
            } else {
                $dumeNumero = $this->em->getRepository(TDumeNumero::class)
                    ->findOneBy(
                        [
                            'idDumeContexte' => $contexte,
                        ]
                    );

                if (self::TYPE_ACHETEUR == $this->type) {
                    if (null === $dumeNumero) {
                        $dumeNumero = new TDumeNumero();
                        $dumeNumero->setIdDumeContexte($contexte);

                        $contexte->setStatus($statutPublie);

                        $this->em->persist($contexte);
                        $logger->addInfo('TDumeNumero inserted and TDumeContexte status updated');
                    }

                    $dumeNumero->setNumeroDumeNational($numeroDumeSN);

                    $this->em->persist($dumeNumero);
                    $this->em->flush();
                } elseif (self::TYPE_ENTREPRISE == $this->type) {
                    $contexte->setStatus($statutPublie);

                    $logger->addInfo('TDumeContexte status updated');
                }

                $logger->addInfo('Numero DUME national found : ' . $numeroDumeSN);
                $output->writeln('Numero DUME national : ' . $numeroDumeSN);

                if (null == $dumeNumero->getBlobId()) {
                    $this->getAndSavePdf($client, $contexte, $dumeNumero, $numeroDumeSN);
                }

                if (null == $dumeNumero->getBlobIdXml()) {
                    $this->getAndSaveXml($client, $contexte, $dumeNumero, $numeroDumeSN);
                }

                if (null == $dumeNumero->getBlobId() && null == $dumeNumero->getBlobIdXml()) {
                    throw new DumeException('PDF and XML file not yet available');
                }

                $contexte->setStatus($statutPublie);

                $this->em->persist($contexte);
                $this->em->flush();

                $logger->addInfo('PDF file saved');
            }
        }
    }

    /**
     * @param $client
     * @param $contexte
     * @param $dumeNumero
     * @param $numeroDume
     *
     * @throws ConnectionException
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>*/
    private function getAndSavePdf($client, $contexte, $dumeNumero, $numeroDume)
    {
        try {
            $this->em->getConnection()->beginTransaction();
            $pdfContent = '';
            if (self::TYPE_ACHETEUR == $this->type) {
                $pdfContent = $client->acheteurService()->getPdf($numeroDume);
            } elseif (self::TYPE_ENTREPRISE == $this->type) {
                $pdfContent = $client->entrepriseService()->getPdf($numeroDume);
            }

            $blobOrganismeFile = $this->save($contexte, $numeroDume, $pdfContent, '.pdf');
            if (
                $blobOrganismeFile instanceof BloborganismeFile
                && $blobOrganismeFile->getId()
            ) {
                $dumeNumero->setBlobId($blobOrganismeFile->getId());
                $dumeNumero->setDateRecuperationPdf(new DateTime());

                $this->em->persist($dumeNumero);
                $this->em->flush();
            }
        } catch (Exception $exceptionPdf) {
            $erreur = 'Erreur : ' . $exceptionPdf->getMessage()
                . PHP_EOL . 'Trace: ' . $exceptionPdf->getTraceAsString();
            $this->dumeLogger->getLogger()->error(
                'Erreur lors de la récupération du fichier'
                . PHP_EOL . 'Type de dume  = ' . print_r($this->type, true)
                . PHP_EOL . $erreur
            );
        }
    }

    /**
     * @param $client
     * @param $contexte
     * @param $dumeNumero
     * @param $numeroDume
     *
     * @throws ConnectionException
     */
    private function getAndSaveXml($client, $contexte, $dumeNumero, $numeroDume)
    {
        try {
            $this->em->getConnection()->beginTransaction();

            if (self::TYPE_ACHETEUR == $this->type) {
                $xmlContent = $client->acheteurService()->getXml($numeroDume);
            } elseif (self::TYPE_ENTREPRISE == $this->type) {
                $xmlContent = $client->entrepriseService()->getXml($numeroDume);
            } else {
                $xmlContent = '';
            }
            $blobOrganismeFile = $this->save($contexte, $numeroDume, $xmlContent, '.xml');
            if (
                $blobOrganismeFile instanceof BloborganismeFile
                && $blobOrganismeFile->getId()
            ) {
                $dumeNumero->setBlobIdXml($blobOrganismeFile->getId());
                $dumeNumero->setDateRecuperationXml(new DateTime());

                $this->em->persist($dumeNumero);
                $this->em->flush();
            }
        } catch (Exception $exceptionXml) {
            $erreur = 'Erreur : ' . $exceptionXml->getMessage()
                . PHP_EOL . 'Trace: ' . $exceptionXml->getTraceAsString();
            $this->dumeLogger->getLogger()->error(
                'Erreur lors de la récupération du fichier'
                . PHP_EOL . 'Type de dume  = ' . print_r($this->type, true)
                . PHP_EOL . $erreur
            );
        }
    }

    /**
     * @param $tmp
     *
     * @return string|string[]
     */
    private function changeFileTmp($tmp): string|array
    {
        $file = str_replace('.tmp', '', $tmp);
        rename($tmp, $file);

        return $file;
    }

    /**
     * @param $contexte
     * @param $numeroDume
     * @param $content
     * @param string $extension
     *
     * @return BloborganismeFile
     *
     * @throws ConnectionException
     */
    private function save($contexte, $numeroDume, $content, $extension = '.xml')
    {
        $blobOrganismeFile = null;
        try {
            $organisme = $contexte->getOrganisme()->getAcronyme();
            $repertoire = date('Y') . '/' . date('m') . '/' . date('d') . '/' . $organisme . '/files/';

            $blobOrganismeFile = new BloborganismeFile();
            $blobOrganismeFile->setOrganisme($organisme);
            $blobOrganismeFile->setName($numeroDume . $extension);
            $blobOrganismeFile->setChemin($repertoire);
            $blobOrganismeFile->setStatutSynchro(0);
            $this->em->persist($blobOrganismeFile);
            $this->em->flush();
            if (
                $blobOrganismeFile instanceof  BloborganismeFile
                && $blobOrganismeFile->getId() !== null
            ) {
                $fileToSave = $this->getFileToSave($repertoire, $blobOrganismeFile->getId(), $content);
                $fileToSave = $this->changeFileTmp($fileToSave);

                $blobOrganismeFile->setHash(sha1_file($fileToSave));
                $this->em->persist($blobOrganismeFile);
                $this->em->flush();
                $this->em->getConnection()->commit();
            }
        } catch (Exception $exception) {
            $erreur = 'Erreur : ' . $exception->getMessage() . PHP_EOL . 'Trace: ' . $exception->getTraceAsString();
            $this->dumeLogger->getLogger()->error(
                'Erreur lors de la récupération du fichier'
                . PHP_EOL . 'Organisme = ' . print_r($organisme, true)
                . PHP_EOL . 'Numero dume = ' . print_r($numeroDume, true)
                . PHP_EOL . 'Type de fichier = ' . print_r($extension, true)
                . PHP_EOL . 'Chemin  = ' . print_r($fileToSave, true)
                . PHP_EOL . $erreur
            );
        }
        return $blobOrganismeFile;
    }

    /**
     * @param $content
     *
     * @return string
     */
    public function getFileToSave(string $repertoire, int $blobOrganismeFileId, $content)
    {
        $dir = str_replace('//', '/', $this->parameterBag->get('BASE_ROOT_DIR'));
        $mnts = $this->parameterBag->get('DOCUMENT_ROOT_DIR_MULTIPLE');
        if ('' !== $mnts) {
            $arrayMnts = explode(';', (string) $mnts);
            if (count($arrayMnts) > 0) {
                $dir = $arrayMnts[0];
            }
        }
        $fileToSave = $dir . $repertoire;

        if (!is_dir($fileToSave)) {
            mkdir($fileToSave, 0755, true);
        }
        $fileToSave = $fileToSave . $blobOrganismeFileId . '-0.tmp';

        file_put_contents($fileToSave, $content);

        return $fileToSave;
    }
}
