<?php

namespace App\Command\Dume;

use App\Command\MonitoredCommand;
use App\Entity\TDumeContexte;
use App\Message\DumePublication;
use App\Service\CommandMonitoringService;
use Exception;
use App\Exception\DumeException;
use App\Service\AtexoDumeService;
use App\Service\DumeLoggerService;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Command to publish dume on dume nationale.
 *
 * Class AskDumePublicationAbstractCommand
 */
abstract class AskDumePublicationAbstractCommand extends MonitoredCommand
{
    final public const CMD_PREFIX = 'dume';
    final public const ARGUMENT_CONTEXT_ID = 'contexte_lt_dume_id';
    final public const TYPE_ACHETEUR = 'acheteur';
    final public const TYPE_ENTREPRISE = 'entreprise';

    protected string $type = '';

    /**
     * FindDumeNumeroAndPdfAbstractCommand constructor.
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly DumeLoggerService $dumeLogger,
        private readonly AtexoDumeService $atexoDumeService,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly MessageBusInterface $bus
    ) {
        parent::__construct($commandMonitoringService);
    }

    /**
     * Configuration de la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    protected function configure()
    {
        $this->setDescription('Permet de faire la demande de publication du dume ' . $this->type .
            ' au dume national en asynchrone')
            ->addArgument(
                self::ARGUMENT_CONTEXT_ID,
                InputArgument::REQUIRED,
                'context send by lt_dume and store on table t_dume_contexte'
            )
        ;
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @return int
     *
     * @throws DumeException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = $this->dumeLogger->getLogger();

        $contexteLtDumeId = $input->getArgument(self::ARGUMENT_CONTEXT_ID);
        $cmd = self::CMD_PREFIX . $this->type . ':valid';

        if (self::TYPE_ACHETEUR == $this->type) {
            $type_dume = $this->parameterBag->get('TYPE_DUME_ACHETEUR');
        } elseif (self::TYPE_ENTREPRISE == $this->type) {
            $type_dume = $this->parameterBag->get('TYPE_DUME_OE');
        }

        $info = $this->em
            ->getRepository(TDumeContexte::class)
            ->findOneBy(
                [
                    'status' => $this->parameterBag->get('STATUT_DUME_CONTEXTE_VALIDE'),
                    'typeDume' => $type_dume,
                    'contexteLtDumeId' => $contexteLtDumeId,
                ]
            );

        if (null !== $info) {
            $this->publicationDume($output, $input, $info, $contexteLtDumeId, $cmd, $logger);
        } else {
            throw new DumeException("ContexteLtDumeId not found : $contexteLtDumeId");
        }
        return 0;
    }

    /**
     * @param $output
     * @param $input
     * @param $info
     * @param $contexteLtDumeId
     * @param $cmd
     * @param $logger
     * @param $queue
     *
     * @throws DumeException
     */
    protected function publicationDume($output, $input, $info, $contexteLtDumeId, $cmd, $logger)
    {
        $reponse = null;
        $message = 'Lancement message DumePublication';
        try {
            $output->writeln('t_dume_contexte : ' . $info->getId());
            $output->writeln('contexte_lt_dume_id : ' . $contexteLtDumeId);

            $logger->info('Beginning command ' . $cmd);

            $client = $this->atexoDumeService->createConnexionLtDume();

            $logger->info('Client created');
            $logger->info('Start calling publishDume function from mpe-dume library');

            if (self::TYPE_ACHETEUR == $this->type) {
                $reponse = $client->acheteurService()->publishDume($input->getArgument(self::ARGUMENT_CONTEXT_ID));
            } elseif (self::TYPE_ENTREPRISE == $this->type) {
                $reponse = $client->entrepriseService()->publishDume($input->getArgument(self::ARGUMENT_CONTEXT_ID));
            }

            $logger->info('Finishing publishDume function from mpe-dume library');

            $info->setStatus($this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'));

            $logger->info('TDumeContexte status updated');

            $output->writeln('status : ' . $reponse->getStatut());

            $this->logger->info($message);
            $this->bus->dispatch(new DumePublication($this->type, true));
            $this->logger->info('New message DumePublication de type ' . $this->type . ' ');

            $this->em->persist($info);
            $this->em->flush();
        } catch (ServiceException | RestRequestException $e) {
            $this->logger->info($message . ' suite a une exception :' . $e->getMessage());
            $this->bus->dispatch(new DumePublication($this->type, true));

            throw new DumeException('Issue with DUME service : ' . $e->getCode());
        } catch (Exception $e) {
            $this->logger->info($message . ' suite a une exception :' . $e->getMessage());
            $this->bus->dispatch(new DumePublication($this->type, true));

            throw new DumeException('Unknown issue : ' . $e->getMessage());
        }
    }
}
