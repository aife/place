<?php

namespace App\Command\Dume;

/**
 * Command to publish dume on dume nationale.
 *
 * Class AskDumePublicationEntrepriseCommand
 */
class AskDumePublicationEntrepriseCommand extends AskDumePublicationAbstractCommand
{
    protected static $defaultName = 'dume:entreprise:valid';

    protected string $type = 'entreprise';
}
