<?php

namespace App\Command\Dume;

use App\Command\MonitoredCommand;
use App\Message\DumePublication;
use App\Service\CommandMonitoringService;
use Exception;
use App\Entity\TDumeNumero;
use App\Entity\TMembreGroupementEntreprise;
use App\Exception\DumeException;
use App\Service\AtexoDumeService;
use App\Service\DumeLoggerService;
use AtexoDume\Exception\RestRequestException;
use AtexoDume\Exception\ServiceException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Command to publish dume on dume nationale.
 *
 * Class AskDumePublicationGroupementEntrepriseCommand
 */
class AskDumePublicationGroupementEntrepriseCommand extends MonitoredCommand
{
    protected static $defaultName = 'dume:entreprise:groupement-to-publish';
    /**
     * FindDumeNumeroAndPdfAbstractCommand constructor.
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly DumeLoggerService $dumeLogger,
        private readonly AtexoDumeService $atexoDumeService,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly MessageBusInterface $bus
    ) {
        parent::__construct($commandMonitoringService);
    }

    /**
     * Configuration de la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    protected function configure()
    {
        $this->setDescription('Permet de faire la demande de publication du groupement 
        dume entreprise au dume national en asynchrone')
            ->addArgument(
                'id_groupement_entreprise',
                InputArgument::REQUIRED,
                'id_groupement_entreprise to get all id_dume_context'
            )
        ;
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @return void
     *
     * @throws DumeException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = $this->dumeLogger;

        $idGroupementEntreprise = $input->getArgument('id_groupement_entreprise');
        $cmd = 'dume:entreprise:groupement-to-publish';

        if (null !== $idGroupementEntreprise) {
            $tMembreGroupementEntreprises = $this->em
                ->getRepository(TMembreGroupementEntreprise::class)
                ->findByIdGroupementEntreprise($idGroupementEntreprise);

            $mandataire = $this->em
                ->getRepository(TMembreGroupementEntreprise::class)
                ->findOneBy([
                    'idGroupementEntreprise' => $idGroupementEntreprise,
                    'idRoleJuridique' => $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE'),
                ]);

            $arrayIdDumeContexteGroupementFormat = [];
            $arrayIdDumeContexteGroupement = [];

            foreach ($tMembreGroupementEntreprises as $tMembreGroupementEntreprise) {
                $tDumeNumero = $this->em
                    ->getRepository(TDumeNumero::class)
                    ->findOneByNumeroDumeNational($tMembreGroupementEntreprise->getNumeroSN());

                if ($tDumeNumero) {
                    $arrayIdDumeContexteGroupementFormat['idsContexteGroupement'][] =
                        (int) $tDumeNumero->getIdDumeContexte()->getContexteLtDumeId();
                    $arrayIdDumeContexteGroupement[] = $tDumeNumero->getIdDumeContexte();
                }
            }

            $arrayIdDumeContexteGroupementFormat['siretMandataire'] =
                $mandataire->getEntreprise()->getSiren() . $mandataire->getEtablissement()->getCodeEtablissement();

            $logger->addInfo('arrayIdDumeContexteGroupementFormat to lt_dume' .
                var_export($arrayIdDumeContexteGroupementFormat, true));

            if (!empty($arrayIdDumeContexteGroupementFormat)) {
                try {
                    $output->writeln('idGroupementEntreprise : ' . $idGroupementEntreprise);

                    $logger->addInfo('Beginning command ' . $cmd);

                    $client = $this->atexoDumeService->createConnexionLtDume();

                    $logger->addInfo('Client created');
                    $logger->addInfo('Start calling publishGroupementDume function from mpe-dume library');

                    $reponse = $client
                        ->entrepriseService()
                        ->publishGroupementDume($arrayIdDumeContexteGroupementFormat);

                    $logger->addInfo('Finishing publishGroupementDume function from mpe-dume library');

                    $output->writeln('status : ' . $reponse['statut']);

                    foreach ($arrayIdDumeContexteGroupement as $tDumeContexte) {
                        $tDumeContexte->setStatus(
                            $this->parameterBag->get('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION')
                        );

                        $logger->addInfo('lancement message DumePublication 
                                            de type entreprise en mode publication');
                        $this->bus->dispatch(new DumePublication('entreprise', true));

                        $this->em->persist($tDumeContexte);
                        $this->em->flush();
                    }
                } catch (ServiceException | RestRequestException $e) {
                    $logger->addInfo('lancement message DumePublication de type entreprise en mode publication 
                    suite a une exception : ' . $e->getMessage());
                    $this->bus->dispatch(new DumePublication('entreprise', true));

                    throw new DumeException('Issue with DUME service : ' . $e->getCode());
                } catch (Exception $e) {
                    $logger->addInfo('lancement message DumePublication de type entreprise en mode publication 
                    suite a une exception : ' . $e->getMessage());
                    $this->bus->dispatch(new DumePublication('entreprise', true));

                    throw new DumeException('Unknown issue : ' . $e->getMessage());
                }
            } else {
                throw new DumeException("Any idDumeContexte for this groupement : $idGroupementEntreprise");
            }
        } else {
            throw new DumeException("idGroupementEntreprise not found : $idGroupementEntreprise");
        }
        return 0;
    }
}
