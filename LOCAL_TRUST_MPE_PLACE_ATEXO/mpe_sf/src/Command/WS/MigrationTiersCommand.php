<?php

namespace App\Command\WS;

use Doctrine\ORM\Exception\ORMException;
use Exception;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\Service;
use App\Entity\Tiers;
use App\Entity\WS\WebService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class MigrationTierCommand.
 */
class MigrationTiersCommand extends Command
{
    protected static $defaultName = 'mpe:migration:tiers';

    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription("Permet de migrer la table tiers dans la table agent avec l'ajout des habilitations");
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    /**
     * Permet d'executer la commande console.
     *
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $em = $this->em;
            $output->writeln('Start mpe:migration:tiers');
            $tiers = $em->getRepository(Tiers::class)->findAll();
            foreach ($tiers as $tier) {
                $agent = $em->getRepository(Agent::class)->findOneBy([
                    'login' => $tier->getLogin(),
                    'nom' => $tier->getLogin(),
                ]);

                if (null === $agent) {
                    $agent = new Agent();
                    $agent->setLogin($tier->getLogin());
                    $agent->setNom($tier->getLogin());
                    $agent->setPassword(
                        hash(
                            'sha256',
                            $this->parameterBag->get('SALT_POUR_MOT_DE_PASSE') . $tier->getPassword()
                        )
                    );
                    $agent->setIdProfilSocleExterne(0);
                    $agent->setTechnique(true);
                    if ($tier->getOrganisme()) {
                        $agent->setOrganisme($tier->getOrganisme());
                    }
                    $service = $em->getReference(Service::class, 0);
                    $agent->setService($service);
                    $em->persist($agent);
                    $em->flush();

                    $habilitationAgent = new HabilitationAgent();
                    $habilitationAgent->setAgent($agent);
                    $habilitationAgent->setAccesWs(1);
                    $habilitationAgent->setModuleAutoformation(0);
                    $habilitationAgent->setAdministrerOrganisme(0);
                    $em->persist($habilitationAgent);
                    $em->flush();
                }
                $webservices = $em->getRepository(WebService::class)->findAll();
                foreach ($webservices as $webservice) {
                    $agent->addWebservice($webservice);
                }
                $em->flush();
            }
            $output->writeln('End mpe:migration:tiers');
        } catch (Exception $e) {
            $erreur = 'Erreur MigrationTierCommand => ' . $e->getMessage();
            $output->writeln('Erreur : ' . $erreur);
        }

        return 0;
    }
}
