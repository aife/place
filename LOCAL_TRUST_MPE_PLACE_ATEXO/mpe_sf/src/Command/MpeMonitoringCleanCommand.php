<?php

namespace App\Command;

use App\Repository\CommandMonitoringRepository;
use App\Service\CommandMonitoringService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'mpe:monitoring:clean',
    description: 'Une commande pour le nettoyage de la table monitoring_command.',
)]
class MpeMonitoringCleanCommand extends MonitoredCommand
{
    public function __construct(
        private CommandMonitoringService $commandMonitoringService,
        private CommandMonitoringRepository $monitoringRepository,
        private LoggerInterface $cliLogger
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $beforeOneWeek = (new \DateTime('now'))->modify('-7 days');
            $this->monitoringRepository->clean($beforeOneWeek);
            $io->success('Les logs de plus d\'une semaine sont supprimés.');

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $io->error($exception->getMessage());
            $this->cliLogger->error(sprintf('mpe:monitoring:clean : %s', $exception->getMessage()));

            return Command::FAILURE;
        }
    }
}
