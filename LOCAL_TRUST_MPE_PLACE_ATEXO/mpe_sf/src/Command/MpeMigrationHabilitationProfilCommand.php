<?php

namespace App\Command;

use App\Entity\HabilitationProfil;
use App\Entity\HabilitationProfilHabilitation;
use App\Entity\ReferentielHabilitation;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'mpe:migration:profil-habilitation',
    description: 'Migration des profils habilitations V1 vers V2',
)]
class MpeMigrationHabilitationProfilCommand extends MonitoredCommand
{
    private $io = null;
    private $referentielHabilitations = [];

    public function __construct(
        private EntityManagerInterface $entityManager,
        private CommandMonitoringService $commandMonitoringService,
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->success('Migration des profils habilitations .....');
        if ($this->commandMonitoringService->isCommandLaunched($this->getName())) {
            $this->io->error('Command is launched ......');

            return Command::FAILURE;
        }
        $this->referentielHabilitations = $this->entityManager
            ->getRepository(ReferentielHabilitation::class)->findAll();
        $habilitationProfils = $this->entityManager->getRepository(HabilitationProfil::class)->findAll();

        foreach ($habilitationProfils as $habilitationProfil) {
            $this->migrateHabilitationProfil($habilitationProfil);
        }

        $this->entityManager->flush();
        $this->entityManager->clear();

        $this->io->success('Migration des habilitations terminée avec succès.');

        return Command::SUCCESS;
    }

    private function migrateHabilitationProfil(HabilitationProfil $habilitationProfil)
    {
        foreach ($this->referentielHabilitations as $referentielHabilitation) {
            $habilitation = $referentielHabilitation->getSlug();
            $this->io->success(
                sprintf(
                    'Migration de l\'habilitation %s pour le profil %s',
                    $habilitation,
                    $habilitationProfil->getLibelle()
                )
            );

            $isActive = false;

            $methodName = str_replace('_', '', ucwords($habilitation, '_'));
            if (method_exists($habilitationProfil, 'get' . $methodName)) {
                $isActive = $habilitationProfil->{'get' . $methodName}();
            } elseif (method_exists($habilitationProfil, 'is' . $methodName)) {
                $isActive = $habilitationProfil->{'is' . $methodName}();
            }

            if ($isActive) {
                $habilitationProfilHabilitation = new HabilitationProfilHabilitation();
                $habilitationProfilHabilitation
                    ->setHabilitationProfil($habilitationProfil)
                    ->setHabilitation($referentielHabilitation);

                $this->entityManager->persist($habilitationProfilHabilitation);
            }
        }
    }
}
