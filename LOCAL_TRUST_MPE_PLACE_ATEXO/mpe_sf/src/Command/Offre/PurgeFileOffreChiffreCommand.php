<?php

namespace App\Command\Offre;

use App\Entity\BloborganismeFile;
use App\Entity\EnveloppeFichierBloc;
use App\Entity\HistoriquePurge;
use App\Repository\HistoriquePurgeRepository;
use App\Service\HistoriquePurgeService;
use App\Utils\Filesystem\MountManager;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'mpe:offres-chiffre:purge',
    description: "Purge les offres chiffrées lorsqu'elles ont été déchiffrées.",
    aliases: ['mpe:oc:purge'],
    hidden: false
)]
class PurgeFileOffreChiffreCommand extends Command
{
    final public const LOGGER_CHANNEL = "cli_nas_purge_files";
    final public const DATE_FORMAT = "d/m/Y";

    final public const LOG_HEADERS = [
        HistoriquePurge::KEY_ID_BLOB_ORGANISME_FILE,
        HistoriquePurge::KEY_FILE_PATH,
        HistoriquePurge::KEY_CONSULTATION,
        HistoriquePurge::KEY_CONSULTATION_REFERENCE,
        HistoriquePurge::KEY_ORGANISME,
        HistoriquePurge::KEY_DATE_FIN,
        HistoriquePurge::KEY_DATE_DECHIFFREMENT,
        HistoriquePurge::KEY_ID_ENVELOPPE,
        HistoriquePurge::KEY_ID_FICHIER_ENVELOPPE,
        HistoriquePurge::KEY_CREATION_FICHIER,
        HistoriquePurge::KEY_TEXT,
    ];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $cliNasPurgeFilesLogger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly MountManager $mountManager,
        private readonly HistoriquePurgeRepository $historiquePurgeRepository,
        private readonly HistoriquePurgeService $historiquePurgeService,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('Cette commande permet de purger les offres déchiffrées sur le serveur.')
            ->addArgument(
                'statutConsultation',
                InputArgument::REQUIRED,
                'Liste de statuts de consultations séparé par des virgules
                -------------------------
                 2 : Consultation
                 3 : Ouverture et analyse
                 4 : Decision
                 5 : à archiver
                 6 : archive réalisé
                -------------------------
                 (Exemple: 3,5,6)
                 '
            )
            ->addArgument(
                'dateDechiffrement',
                InputArgument::REQUIRED,
                "Indiquez la date de Dechiffrement pour purger les fichiers des offres antérieur à la date sélectionnée
                sous le format dd-mm-YYYY
                (Exemple: 25-07-1990)
                "
            )
            ->addArgument(
                'acronymeOrganisme',
                InputArgument::REQUIRED,
                "Acronyme organisme. Mettre 'all' pour tous les organismes
                Valeur :
                - __acronyme_organisme__ 
                - all (all=tous les organismes)
                (Exemple: pmi-min-1)
                (Exemple: all)"
            )
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Mode utilisation command test/reel. Exemple: true|false (true=mode test, false=mode réel)',
                true
            )
            ->addOption(
                'limit',
                null,
                InputArgument::OPTIONAL,
                "Nombre d'offres chiffré à purger lors du lancement de cette commande",
                100
            )
            ->addOption(
                'step',
                null,
                InputArgument::OPTIONAL,
                "La définition du pas pour chaque itération pour la requête base de données",
                null
            )
            ->addOption(
                'stateList',
                null,
                InputArgument::OPTIONAL,
                "Code du statut des offres à repurger si nécessaire, par défaut ça sera tous les nouveaux,
                séparé par des virgules. 
                Exemple: 2,3 (1=Fichier supprimé ok, 2=Fichier non trouvé physiquement sur le serveur, 3=autre erreur)",
                0
            )
        ;
    }
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $statutConsultation = explode(',', $input->getArgument('statutConsultation'));
            if (empty($statutConsultation)) {
                $io->error(sprintf("Impossible de supprimer des offres car aucun statut n'a été renseigné"));

                return Command::FAILURE;
            }

            try {
                $dateDechiffrement = new \DateTimeImmutable($input->getArgument('dateDechiffrement'));
            } catch (Exception $exception) {
                $io->error(sprintf("Erreur de format de la date Dechiffrement : %s", $exception->getMessage()));

                return Command::FAILURE;
            }

            $acronyme = $input->getArgument('acronymeOrganisme');
            $dryRun = !in_array($input->getOption('dry-run'), [0, '0', 'false', false], true);
            $limit = $input->getOption('limit');
            $step = $input->getOption('step');
            $stateString = $input->getOption('stateList');

            $stateList = explode(',', $stateString);
            $stateList[] = HistoriquePurge::STATE_INITIAL;

            $io->title(
                sprintf(
                    "Debut de la commande de purge des fichiers physiques des offres chiffrés ayant été déchiffrés 
                 pour les consultations aux statuts: %s et %s pour les consultations ayant une Dechiffrement antiérieur à : %s.",
                    $input->getArgument('statutConsultation'),
                    $acronyme == 'all' ? "concernant TOUS les organismes" : sprintf("l'organisme %s", $acronyme),
                    $dateDechiffrement->format(self::DATE_FORMAT)
                )
            );

            $io->note(
                sprintf(
                    "Les résultats de la commande sont disponible dans ce dossier: %s. Faire un grep du channel '%s'",
                    $this->parameterBag->get('LOG_DIR'),
                    self::LOGGER_CHANNEL,
                )
            );

            if ($dryRun) {
                $io->warning("Commande lancé en mode TEST");
            } else {
                $io->caution("Commande lancé en mode REEL");
                $this->cliNasPurgeFilesLogger->info(
                    "Lancement de la commande de purge des offres chiffrés avec supression physique"
                );
            }

            if (!is_null($step)) {
                $offset = 0;
                while ($offset < $limit) {
                    $envFichierBlocDechiffre = $this->getDecryptedOffres(
                        $statutConsultation,
                        $acronyme,
                        $io,
                        $stateList,
                        $dateDechiffrement,
                        $offset,
                        $step,
                    );

                    $this->purgeOffres($io, $dryRun, $envFichierBlocDechiffre);

                    $offset += $step;
                }
            } else {
                $envFichierBlocDechiffre = $this->getDecryptedOffres(
                    $statutConsultation,
                    $acronyme,
                    $io,
                    $stateList,
                    $dateDechiffrement,
                    $limit,
                );

                $this->purgeOffres($io, $dryRun, $envFichierBlocDechiffre);
            }


            return Command::SUCCESS;
        } catch (\Exception $e) {
            $log = "ERROR Commande de purge des offres chiffrés avec supression physique : "
                . $e->getMessage() . " " . $e->getTraceAsString();
            $io->error($log);
            $this->cliNasPurgeFilesLogger->error($log);
            return Command::FAILURE;
        }
    }

    private function getDecryptedOffres(
        array $statutConsultation,
        string $acronyme,
        SymfonyStyle $io,
        array $stateList,
        DateTimeInterface $dateDechiffrement,
        int $limit,
        int $step = null,
    ): array {
        $io->text('Récupération des offres ayant un statut ouvert');

        $enveloppeFichierBlocDechiffre = $this->entityManager
            ->getRepository(EnveloppeFichierBloc::class)
            ->getEnveloppeFichierBlocOffreDecryptee(
                $statutConsultation,
                $acronyme,
                $stateList,
                $dateDechiffrement,
                $limit,
                $step,
            )
        ;

        $io->text(sprintf(
            "%s fichiers cryptés seront supprimés durant ce purge",
            count($enveloppeFichierBlocDechiffre)
        ));

        return $enveloppeFichierBlocDechiffre;
    }

    private function purgeOffres(
        SymfonyStyle $io,
        bool $dryRun,
        array $enveloppeFichierBlocDechiffre
    ): void {
        $io->section('Début purge');

        $logInfoCommand = [];

        /** @var EnveloppeFichierBloc $bloc */
        foreach ($enveloppeFichierBlocDechiffre as $bloc) {
            if ($bloc->getIdBlobChiffre() && $bloc->getOrganisme()) {
                $this->deleteBlobFileChiffre(
                    $io,
                    $dryRun,
                    $bloc,
                    $bloc->getIdBlobChiffre(),
                    $bloc->getOrganisme(),
                    $logInfoCommand
                );
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();

        $this->historiquePurgeService->displayLogList($io, self::LOG_HEADERS, $logInfoCommand);
        $this->historiquePurgeService->notifyResultsInConsole(
            $io,
            $logInfoCommand,
            count($enveloppeFichierBlocDechiffre),
            $dryRun,
            "offre(s) chiffré(s)"
        );

        $io->section('Fin purge');
    }

    private function deleteBlobFileChiffre(
        SymfonyStyle $io,
        bool $dryRun,
        EnveloppeFichierBloc $bloc,
        int $idBlobChiffre,
        string $organisme,
        array &$logInfoCommand
    ): void {
        $filePath = sprintf(
            "%s%s",
            $this->parameterBag->get('BASE_ROOT_DIR'),
            $this->mountManager->getFilePath($idBlobChiffre, $organisme, $this->entityManager)
        );

        $historiquePurge = $this->getPurgeLogIfExistsOrCreate($bloc, $filePath, $dryRun);

        $state = HistoriquePurge::STATE_INITIAL;
        $text = sprintf('Debut suppression du fichier suivant : %s', $filePath);
        $this->cliNasPurgeFilesLogger->info($text);
        $io->info($text);

        if (!file_exists($filePath)) {
            $text = sprintf(HistoriquePurge::DESCRIPTION_DELETED_NOT_FOUND, $filePath);
            $state = HistoriquePurge::STATE_ERROR_FILE_NOT_FOUND;

            $this->cliNasPurgeFilesLogger->info($text);
        } elseif ($idBlobChiffre) {
            if (true === $dryRun) {
                $state = HistoriquePurge::STATE_READY_TO_DELETE;
                $text = sprintf(HistoriquePurge::DESCRIPTION_AWAIT_MODE_REAL, $filePath);
            } else {
                try {
                    $deletionResult = $this->mountManager->deleteFile($idBlobChiffre, $organisme, $this->entityManager);

                    $text = $deletionResult
                        ? sprintf(HistoriquePurge::DESCRIPTION_DELETED_OK, $filePath)
                        : sprintf(HistoriquePurge::DESCRIPTION_DELETED_KO, $filePath);

                    $historiquePurge->setDeleted(true);
                    $state = HistoriquePurge::STATE_DELETED;
                } catch (Exception $e) {
                    $text = sprintf(HistoriquePurge::DESCRIPTION_DELETED_KO, $filePath . $e->getMessage());
                    $state = HistoriquePurge::PURGE_STATE_ERROR;

                    $io->error($text);
                    $this->cliNasPurgeFilesLogger->error($text);
                }
            }

            $io->info($text);
            $this->cliNasPurgeFilesLogger->info($text);
        }

        $organismeBlobFichier = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->getOrganismeBlobFichier($idBlobChiffre)
        ;

        $fichierEnveloppe = $bloc->getFichierEnveloppe();
        $enveloppe = $fichierEnveloppe?->getEnveloppe();
        $offre = $enveloppe?->getOffre();

        $details = [
            HistoriquePurge::KEY_ID_BLOB_ORGANISME_FILE => $idBlobChiffre,
            HistoriquePurge::KEY_FILE_PATH => $filePath,
            HistoriquePurge::KEY_CONSULTATION => $offre?->getConsultationId(),
            HistoriquePurge::KEY_CONSULTATION_REFERENCE =>
                $offre?->getConsultation()?->getReferenceUtilisateur(),
            HistoriquePurge::KEY_ORGANISME => $organismeBlobFichier?->getOrganisme(),
            HistoriquePurge::KEY_DATE_FIN =>
                $offre?->getConsultation()?->getDatefin()?->format(self::DATE_FORMAT),
            HistoriquePurge::KEY_DATE_DECHIFFREMENT =>
                $enveloppe?->getDateDebutDechiffrement()?->format(self::DATE_FORMAT),
            HistoriquePurge::KEY_ID_ENVELOPPE => $fichierEnveloppe?->getIdEnveloppe(),
            HistoriquePurge::KEY_ID_FICHIER_ENVELOPPE => $fichierEnveloppe?->getIdFichier(),
            HistoriquePurge::KEY_CREATION_FICHIER =>
                $offre?->getCreatedAt()?->format(self::DATE_FORMAT),
            HistoriquePurge::KEY_TEXT => $text
        ];

        $logInfoCommand[$state][] = $details;

        if (false === $dryRun) {
            $historiquePurge->setDescription($text);
            $historiquePurge->setState($state);
            $historiquePurge->setData(json_encode($details, JSON_THROW_ON_ERROR));
            $this->entityManager->persist($historiquePurge);

            if ($organismeBlobFichier) {
                $organismeBlobFichier->setDeletionDatetime(new DateTime());
            }
        }
    }

    private function getPurgeLogIfExistsOrCreate(
        EnveloppeFichierBloc $bloc,
        string $filePath,
        bool $dryRun
    ): HistoriquePurge {
        $historiquePurge = $this->historiquePurgeRepository->findBy(
            [
                'idBlocFichierEnveloppe' => $bloc->getIdBlocFichier(),
                'organisme' => $bloc->getOrganisme(),
            ]
        );
        if ($historiquePurge) {
            return $historiquePurge;
        }

        $description = sprintf(
            "Supression du fichier %s commencé le %s à %s. %s",
            $filePath,
            date('d-m-Y'),
            date('H:i:s'),
            HistoriquePurge::DESCRIPTION_INITIAL,
        );

        $historiquePurge = (new HistoriquePurge())
            ->setType(HistoriquePurge::TYPE_DELETE_PHYSIQUE_CRYPTEE)
            ->setDescription($description)
        ;

        if (false === $dryRun) {
            $historiquePurge->setIdBlocFichierEnveloppe($bloc->getIdBlocFichier());
            $historiquePurge->setOrganisme($bloc->getOrganisme());
        }

        return $historiquePurge;
    }
}
