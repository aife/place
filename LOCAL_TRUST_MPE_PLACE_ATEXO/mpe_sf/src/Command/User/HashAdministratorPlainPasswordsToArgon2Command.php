<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\User;

use App\Entity\Administrateur;
use App\Repository\AdministrateurRepository;
use App\Security\PasswordEncoder\CommonPasswordEncoder;
use App\Service\AdministratorService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class HashAdministratorPlainPasswordsToArgon2Command extends Command
{
    protected static $defaultName = 'mpe:hash-admin-plain-passwords-to-argon2';

    public function __construct(
        private AdministratorService $administratorService,
        string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Hasher les mots de passe administrateur en clair en argon2')
            ->setHelp(
                'Cette commande permet de Hasher les mots de passe administrateur en clair en argon2'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('converting...');

        return $this->administratorService->convertPasswordToArgon();
    }
}
