<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\User;

use App\Entity\Administrateur;
use App\Repository\AdministrateurRepository;
use App\Security\PasswordEncoder\CommonPasswordEncoder;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateAdministratorCommand extends Command
{
    protected static $defaultName = 'mpe:create-admin';

    public function __construct(
        private AdministrateurRepository $administrateurRepository,
        private CommonPasswordEncoder $commonPasswordEncoder,
        string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Créer un administrateur')
            ->setHelp(
                'Cette commande permet de créer un administrateur'
            )
            ->addOption('login', 'lo', InputOption::VALUE_REQUIRED)
            ->addOption('password', 'pa', InputOption::VALUE_REQUIRED)
            ->addOption('email', 'em', InputOption::VALUE_REQUIRED)
            ->addOption('nom', 'no', InputOption::VALUE_REQUIRED)
            ->addOption('prenom', 'pre', InputOption::VALUE_REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $administrateur = new Administrateur();
        $login = $input->getOption('login') ??
            throw new \InvalidArgumentException('l\'option --login n\'est pas renseignée');
        $password = $input->getOption('password') ??
            throw new \InvalidArgumentException('l\'option --password n\'est pas renseignée');
        $nom = $input->getOption('email') ??
            throw new \InvalidArgumentException('l\'option --email n\'est pas renseignée');
        $prenom = $input->getOption('nom') ??
            throw new \InvalidArgumentException('l\'option --nom n\'est pas renseignée');
        $email = $input->getOption('prenom') ??
            throw new \InvalidArgumentException('l\'option --prenom n\'est pas renseignée');

        $administrateur->setLogin($login);
        $administrateur->setOriginalLogin($login);
        $administrateur->setNom($nom);
        $administrateur->setPrenom($prenom);
        $administrateur->setEmail($email);
        $administrateur->setMdp(
            $this->commonPasswordEncoder->encodePassword(
                Administrateur::class,
                $password
            )
        );

        $this->administrateurRepository->persist($administrateur);
        $this->administrateurRepository->flush();

        return 0;
    }
}
