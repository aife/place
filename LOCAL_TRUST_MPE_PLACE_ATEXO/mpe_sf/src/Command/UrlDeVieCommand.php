<?php

namespace App\Command;

use App\Service\Superviseur\SuperviseurContrats;
use App\Service\Superviseur\SuperviseurDisk;
use App\Service\Superviseur\SuperviseurDume;
use App\Service\Superviseur\SuperviseurFacade;
use App\Service\Superviseur\SuperviseurInterfaces;
use App\Service\Superviseur\SuperviseurMol;
use App\Service\Superviseur\SuperviseurNas;
use App\Service\Superviseur\SuperviseurOpenOffice;
use App\Service\Superviseur\SuperviseurPhp;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class UrlDeVieCommand extends Command
{
    protected static $defaultName = 'mpe:url-de-vie';
    protected $fileName;
    protected $limit;
    protected int $offset = 0;
    protected $validator;
    protected $doctrine;

    public function __construct(
        private readonly ContainerInterface $container,
        private readonly ParameterBagInterface $parameterBag,
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Commande qui permet de vérifier le minimum requis');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $container = $this->container;
        $nas = new SuperviseurNas($container);
        $disk = new SuperviseurDisk($container);
        $interfaces = new SuperviseurInterfaces($container);
        $php = new SuperviseurPhp($container);
        $contrat = new SuperviseurContrats($container);
        $openOffice = new SuperviseurOpenOffice($container);
        $mol = new SuperviseurMol($container);
        $dume = new SuperviseurDume($container);
        $facade = new SuperviseurFacade($nas, $disk, $interfaces, $openOffice, $php, $contrat, $mol, $dume);
        $maxParallelProcesses = $this->parameterBag->get('max_parallel_processes');
        $pollingInterval = $this->parameterBag->get('polling_interval');

        $result = $facade->getResult($maxParallelProcesses, $pollingInterval);
        $rowsByServices = $this->getRowsByServices($result);

        $table = new Table($output);
        $table
            ->setHeaders([
                'Function',
                'Statut',
                'Messages',
            ])
            ->setRows($rowsByServices);

        $table->render();
        return 0;
    }

    protected function getRowsByServices($result)
    {
        $liste = [];
        foreach ($result['services'] as $nameService => $resultService) {
            $liste[] = new TableSeparator();
            $service = $resultService['service'];
            $status = $service['globalStatus'];
            $function = $service['function'];
            $liste[] = [$function, $status, $this->setBaliseByStatut($status, $service['messages'])];
        }

        foreach ($result['interfaces'] as $nameService => $resultService) {
            $liste[] = new TableSeparator();
            $interface = $resultService['interface'];
            $status = $interface['globalStatus'];
            $function = $interface['function'];
            $liste[] = [$function, $status, $this->setBaliseByStatut($status, $interface['messages'])];
        }

        return $liste;
    }

    protected function setBaliseByStatut($status, $messages)
    {
        $text = '';
        if (is_array($messages)) {
            foreach ($messages as $message) {
                if ('OK' == $status) {
                    $text .= '<info>' . $message['message'] . '</info>' . PHP_EOL;
                } else {
                    $text = '<error>' . $message['message'] . '</error>' . PHP_EOL;
                }
            }
        }

        return $text;
    }
}
