<?php

namespace App\Command;

use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Service\AtexoFichierOrganisme;
use DateTimeImmutable;
use Doctrine\DBAL\ConnectionException;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class PurgeOffresZabbixCommand.
 */
class PurgeOffresZabbixCommand extends Command
{
    protected static $defaultName = 'mpe:offres-zabbix:purge';
    protected const BATCH_SIZE = 100;
    protected const MAX_LOOPS = 2000;

    private bool $dryRun = true;
    private SymfonyStyle $io;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $cliLogger,
        private readonly AtexoFichierOrganisme $serviceBlobOrganisme,
        private readonly Stopwatch $stopwatch
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Suppression des offres de la consultation Zabbix')
            ->addArgument(
                'ref-consultation',
                InputArgument::REQUIRED,
                'Référence utilisateur de la consultation à nettoyer (ex: ZABBIX_2019)'
            )
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation de la suppression (activé par défaut)',
                true
            )
            ->addOption(
                'retention',
                null,
                InputArgument::OPTIONAL,
                'Nombre de jours de rétention des offres (30 par défaut)',
                30
            );
    }

    /**
     * @throws ConnectionException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);

        $this->stopwatch->start($this->getName());

        try {
            $this->io->title($this->getDescription());

            $refConsultation = $input->getArgument('ref-consultation');
            $this->dryRun = $input->getOption('dry-run');
            $retention = $input->getOption('retention');

            if ($this->dryRun) {
                $this->io->warning('Mode dry-run activé : les offres ne seront pas vraiment supprimées');
            }

            $beforeDate = new DateTimeImmutable($retention . ' days ago');
            $this->io->writeln(['Offres avant le ' . $beforeDate->format('d/m/Y'), '']);

            $consultationsZabbix = $this->entityManager->getRepository(Consultation::class)
                ->findBy(['referenceUtilisateur' => $refConsultation]);

            $this->io->writeln('Nombre de consultations Zabbix : ' . count($consultationsZabbix));

            $this->entityManager->getConnection()->beginTransaction();
            $nombreTotalOffresSupprimees = 0;

            /** @var Consultation $consultation */
            foreach ($consultationsZabbix as $consultation) {
                $this->io->writeln('');
                $this->io->section(
                    sprintf(
                        'Suppression des offres de la consultation "%s" (Organisme "%s")',
                        $consultation->getReferenceUtilisateur(),
                        $consultation->getAcronymeOrg()
                    )
                );

                $nombreTotalOffres = $this->entityManager->getRepository(Offre::class)
                    ->getConsultationOffres($consultation->getId(), null, 0, $beforeDate, true);

                $this->io->writeln("<info>Nombre total d'offres à supprimer : $nombreTotalOffres</>");

                $loops = 0;

                do {
                    $offres = $this->entityManager->getRepository(Offre::class)
                        ->getConsultationOffres($consultation->getId(), self::BATCH_SIZE, 0, $beforeDate);

                    if ($nbOffres = is_countable($offres) ? count($offres) : 0) {
                        $this->io->section("Nettoyage de $nbOffres offres...");

                        $this->clean($offres);

                        $nombreTotalOffresSupprimees += $nbOffres;
                        $offresRestantes = $nombreTotalOffres - $nombreTotalOffresSupprimees;
                        $this->io->writeln(["<info>Il reste $offresRestantes offres à supprimer</>", '']);
                    }

                    $this->entityManager->clear();

                    if (
                        $loops % 50 == 0 && !$this->dryRun
                        && $this->entityManager->getConnection()->isTransactionActive()
                    ) {
                        $this->entityManager->getConnection()->commit();
                    }
                } while ((is_countable($offres) ? count($offres) : 0) > 0 && ++$loops < self::MAX_LOOPS);

                if ($loops >= self::MAX_LOOPS) {
                    $this->io->warning(
                        sprintf(
                            'Nombre max de boucles (%s) dépassé. Toutes les offres n\'ont pas été traitées',
                            self::MAX_LOOPS
                        )
                    );
                }
            }

            if ($this->entityManager->getConnection()->isTransactionActive()) {
                if ($this->dryRun) {
                    $this->entityManager->getConnection()->rollBack();
                } else {
                    $this->entityManager->getConnection()->commit();
                }
            }

            $this->io->writeln('');
            $this->io->success([
                'Terminé : ' . $this->getName(),
                "$nombreTotalOffresSupprimees offres ont été supprimées.",
            ]);
        } catch (Exception $e) {
            if ($this->entityManager->getConnection()->isTransactionActive()) {
                $this->entityManager->getConnection()->rollBack();
            }

            $this->cliLogger->error($e->getMessage());
            $this->io->error(['Erreur lors de la suppression des offres : ', $e->getMessage()]);
            $this->io->warning(['La dernière transaction a été rollbackée, mais des blobs ont été supprimés.']);
            return 1;
        }

        $event = $this->stopwatch->stop($this->getName());

        $this->io->note([
            "Durée d'exécution : " . round($event->getDuration() / 1000, 1) . ' sec',
            "Mémoire utilisée : " . $event->getMemory() / 1024 / 1024 . 'M',
        ]);

        return 0;
    }

    public function clean(array $offres)
    {
        $this->cleanListeLotsCandidatures($offres);
        $this->cleanCandidatures($offres);
        $this->cleanBlobs($offres);
        $this->cleanFichiersEnveloppes($offres);
        $this->cleanEnveloppes($offres);
        $this->cleanOffres($offres);
    }

    public function cleanListeLotsCandidatures(array $offres)
    {
        $this->io->write('Nettoyage des TListeLotsCandidatures... ');

        $candidatures = $this->entityManager->getRepository(TCandidature::class)
            ->findBy(['idOffre' => $this->getOffresIds($offres)]);

        $candidaturesIds = array_map(fn (TCandidature $candidature) => $candidature->getId(), $candidatures);

        $this->entityManager->createQueryBuilder()
            ->delete('App:TListeLotsCandidature', 'c')
            ->where('c.candidature IN (:candidaturesIds)')
            ->setParameter('candidaturesIds', $candidaturesIds)
            ->getQuery()->execute();

        $this->io->writeln(count($candidaturesIds) . ' TListeLotsCandidatures supprimées');
    }

    public function cleanCandidatures(array $offres)
    {
        $this->io->write('Nettoyage des TCandidatures... ');

        $result = $this->entityManager->createQueryBuilder()
            ->delete('App:TCandidature', 'c')
            ->where('c.idOffre IN (:offresIds)')
            ->setParameter('offresIds', $this->getOffresIds($offres))
            ->getQuery()->execute();

        $this->io->writeln($result . ' TCandidatures supprimées');
    }

    public function cleanBlobs(array $offres)
    {
        $this->io->write('Nettoyage des blobs sur le filesystem... ');

        $fichiersEnveloppes = $this->getFichiersEnveloppes($offres);
        $deleted = 0;

        foreach ($fichiersEnveloppes as $fichierEnveloppe) {
            // Suppression du blob le cas échéant
            if (null !== $fichierEnveloppe->getBlob()) {
                $this->cleanBlob($fichierEnveloppe->getBlob()->getId(), $fichierEnveloppe->getOrganisme());
                $deleted++;
            }
            // Suppression du fichier signature associé le cas échéant
            if ($fichierEnveloppe->getIdBlobSignature()) {
                $this->cleanBlob($fichierEnveloppe->getIdBlobSignature(), $fichierEnveloppe->getOrganisme());
                $deleted++;
            }
        }
        $this->io->writeln($deleted . ' blobs supprimés');
    }

    public function cleanBlob(int $idBlob, string $organisme)
    {
        if (!$this->dryRun && $idBlob) {
            try {
                $this->serviceBlobOrganisme->deleteBlobFile($idBlob, $organisme);
            } catch (Exception $e) {
                $this->io->warning([
                    "Erreur lors de la suppression du blob $idBlob",
                    $e->getMessage()
                ]);
            }
        }
    }

    public function cleanFichiersEnveloppes(array $offres)
    {
        $this->io->write('Nettoyage des EnveloppeFichiers... ');

        $result = $this->entityManager->createQueryBuilder()
            ->delete('App:EnveloppeFichier', 'e')
            ->where('e.idEnveloppe IN (:enveloppesIds)')
            ->setParameter('enveloppesIds', $this->getEnveloppesIds($offres))
            ->getQuery()->execute();

        $this->io->writeln($result . ' EnveloppeFichiers supprimées');
    }

    public function cleanEnveloppes(array $offres)
    {
        $this->io->write('Nettoyage des Enveloppes... ');

        $result = $this->entityManager->createQueryBuilder()
            ->delete('App:Enveloppe', 'e')
            ->where('e.offreId IN (:offresIds)')
            ->setParameter('offresIds', $this->getOffresIds($offres))
            ->getQuery()->execute();

        $this->io->writeln($result . ' Enveloppes supprimées');
    }

    public function cleanOffres(array $offres)
    {
        $this->io->write('Nettoyage des Offres... ');

        $result = $this->entityManager->createQueryBuilder()
            ->delete('App:Offre', 'o')
            ->where('o.id IN (:offresIds)')
            ->setParameter('offresIds', $this->getOffresIds($offres))
            ->getQuery()->execute();

        $this->io->writeln($result . ' Offres supprimées');
    }

    protected function getOffresIds(array $offres): array
    {
        return array_map(fn (Offre $offre) => $offre->getId(), $offres);
    }

    protected function getFichiersEnveloppes(array $offres): array
    {
        return $this->entityManager->getRepository(EnveloppeFichier::class)
            ->findBy(['enveloppe' => $this->getEnveloppesIds($offres)]);
    }

    protected function getEnveloppesIds(array $offres): array
    {
        $enveloppes = $this->entityManager->getRepository(Enveloppe::class)
            ->findBy(['offre' => $this->getOffresIds($offres)]);

        return array_map(fn (Enveloppe $enveloppe) => $enveloppe->getIdEnveloppeElectro(), $enveloppes);
    }
}
