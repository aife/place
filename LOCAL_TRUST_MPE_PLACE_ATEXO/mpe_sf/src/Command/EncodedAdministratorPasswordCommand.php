<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Command;

use App\Entity\Administrateur;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class EncodedAdministratorPasswordCommand extends Command
{
    protected static $defaultName = 'mpe:admin:password-encoder';
    protected function configure()
    {
        $this->setDescription('Permet d\'encoder les mots de passe de la table Administrateur')
        ;
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly UserPasswordHasherInterface $userPasswordHasher,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;
        $encoder = $this->userPasswordHasher;

        $administrators = $em->getRepository(Administrateur::class)->findAll();

        foreach ($administrators as $administrator) {
            $password = $encoder->hashPassword($administrator, $administrator->getMdp());
            $administrator->setMdp($password);
        }

        $em->flush();
        return 0;
    }
}
