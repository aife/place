<?php

/**
 * Commande pour supprimer une archive documentaire.
 */

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteArchiveCommand extends Command
{
    protected static $defaultName = 'mpe:suppression-archive';
    protected function configure()
    {
        $this->setDescription('Permet la suppresion d\'une archive à partir du chemin de l\'archive')
            ->addArgument(
                'path_archive',
                InputArgument::REQUIRED,
                'Chemin de l\'archive'
            )
        ;
    }

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    /**
     * Permet d'executer la commande qui permet de supprimer un fichier ou un repertoire à partir de son chemin.
     *
     * Permet de supprimer une notification à partir de son id
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;
        $cheminArchive = $input->getArgument('path_archive');

        if (is_dir($cheminArchive)) {
            rmdir($cheminArchive);
            $output->writeln('Le dossier '.$cheminArchive.' a été bien supprimé');
        } elseif (file_exists($cheminArchive)) {
            unlink($cheminArchive);
            $output->writeln('Le fichier '.$cheminArchive.' a été bien supprimé');
        } else {
            $output->writeln($cheminArchive.' n\'existe pas !');
        }
        return 0;
    }
}
