<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\DonneeEssentielle;

use DateTime;
use App\Entity\ContratTitulaire;
use App\Entity\DonneesAnnuellesConcession;
use App\Entity\ModificationContrat;
use App\Service\AtexoDumeService;
use App\Service\ContratService;
use AtexoDume\Exception\ServiceException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

/**
 * Class Genera
 * @package App\Command
 */
class CheckStatusDECommand extends Command
{
    final public const ARGUMENT_ID_CONTRAT = 'idContrat';
    final public const FORMAT = 'xml';
    final public const STATUT_PUBLICATION_SN = '0';


    /**
     * @var string
     */
    protected static $defaultName = 'mpe:check-status:de';

    /**
     * GenerationDocumentRecuperationCommand constructor.
     * @param LoggerInterface $logger
     * @param HttpClientInterface $client
     * @param ParameterBagInterface $parameterBag
     * @param EntityManagerInterface $em
     * @param ContratService $contratService
     * @param AtexoDumeService $atexoDumeService
     */
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly HttpClientInterface $client,
        private readonly ParameterBagInterface $parameterBag,
        /**
         * @var
         */
        private readonly EntityManagerInterface $em,
        private readonly ContratService $contratService,
        private readonly AtexoDumeService $atexoDumeService
    ) {
        parent::__construct();
    }

    /**
     * configurer la commande
     */
    protected function configure()
    {
        $this
            ->setDescription('Check et mise a jour du status DE (status, erreur etc.)');
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info('Début du check des contrat');
        $contrats = $this->getListContrat();
        $client = $this->atexoDumeService
            ->createConnexionLtDume();

        foreach ($contrats as $contrat) {
            $numeroContrat = null;
            $nbModification = '00';
            $uuid = null;
            $statut = 0;
            if (array_key_exists('contrat', $contrat)) {
                $id = $contrat['contrat']->getIdContratTitulaire();
                $numeroContrat = substr($contrat['contrat']->getNumIdUniqueMarchePublic(), 0, -2);
                $uuid = $contrat['contrat']->getUuid();
                $statut = $contrat['contrat']->getStatutPublicationSn();
            }

            if ($statut == 1 && array_key_exists('modifications', $contrat)) {
                $uuid = $contrat['contrat']->getUuid();
                $nbModification = $contrat['modifications'][0]->getNumOrdre();
                if (10 > $nbModification) {
                    $nbModification = '0' . $nbModification;
                }
                $statut = $contrat['modifications'][0]->getStatutPublicationSn();
            }

            if ($nbModification && $numeroContrat && $uuid && $statut == 0) {
                try {
                    $reponse = $client->donneesEssentiellesService()
                        ->getStatutPublication($numeroContrat, $nbModification, $uuid);
                    $statut = $reponse['statut'];
                    $message = $reponse['message'];
                    $statSn = match ($statut) {
                        'ERREUR_SN' => $this->parameterBag->get('STATUT_NON_PUBLIE_SN'),
                        'PUBLIE' => $this->parameterBag->get('STATUT_PUBLIE_SN'),
                        default => $this->parameterBag->get('STATUT_EN_ATTENTE_SN'),
                    };

                    $this->changeStatusPublicationSn(
                        $id,
                        $nbModification,
                        $statSn,
                        $message
                    );
                } catch (ServiceException $exception) {
                    $this->logger->error(
                        'Le contrat id : ' . $id .
                        ' / uuid : ' . $uuid .
                        ' / numéro de contrat : ' . $numeroContrat .
                        ' / numéro de modification  : ' . $nbModification .
                        ' n\'a pas pu être publié !' .
                        PHP_EOL .
                        "Erreur: " .
                        $exception->getMessage() .
                        PHP_EOL .
                        "Trace: " .
                        $exception->getTraceAsString()
                    );

                    $statSn =  $this->parameterBag->get('STATUT_NON_PUBLIE_SN');

                    $this->changeStatusPublicationSn(
                        $id,
                        $nbModification,
                        $statSn,
                        $message
                    );

                } catch (Exception $exc) {
                    $this->logger->error(
                        'Le contrat id : ' . $id .
                        ' / uuid : ' . $uuid .
                        ' / numéro de contrat : ' . $numeroContrat .
                        ' / numéro de modification  : ' . $nbModification .
                        ' n\'a pas pu être publié !' .
                        PHP_EOL .
                        "Erreur: " .
                        $exc->getMessage() .
                        PHP_EOL .
                        "Trace: " .
                        $exc->getTraceAsString()
                    );

                    $statSn =  $this->parameterBag->get('STATUT_NON_PUBLIE_SN');
                    $this->changeStatusPublicationSn(
                        $id,
                        $nbModification,
                        $statSn,
                        $message
                    );
                }
            }
        }
        sleep($this->parameterBag->get('CONFIG_SLEEP'));
        $this->logger->info('Fin du check des contrat');
        return 0;
    }


    /**
     * @return array
     */
    private function getListContrat()
    {
        $dateNotifMin = new DateTime('now');
        $dateNotifMin->modify('-4 year');
        $dateNotifMax = new DateTime('now');
        $limit = $this->parameterBag->get('CONFIG_LIMIT_DE');
        $parameters = [
            'format' => self::FORMAT,
            'dateNotifMin' => $dateNotifMin,
            'dateNotifMax' => $dateNotifMax,
            'statutPublicationSn' => self::STATUT_PUBLICATION_SN,
            'siren' => null,
            'complement' => null,
            'dateDerniereModif' => null,
            'exclusionOrganisme' => null,
            'limit' => $limit,
        ];

        return $this->contratService->getContracts($parameters);
    }

    /**
     * @param $idContrat
     * @param $nbModification
     * @param $status
     * @param $message
     */
    private function changeStatusPublicationSn(
        $idContrat,
        $nbModification,
        $status,
        $message
    ) {
        $contratTitulaire = $this->em
            ->getRepository(ContratTitulaire::class)
            ->findOneBy([
                'idContratTitulaire' => $idContrat
            ]);

        if ($nbModification == '00') {
            if ($contratTitulaire) {
                $contratTitulaire->setStatutPublicationSn($status);

                //PUBLIE
                if ($status == $this->parameterBag->get('STATUT_PUBLIE_SN')) {
                    $contratTitulaire->setDatePublicationSn(new Datetime());
                }

                $contratTitulaire->setDateModificationSn(new DateTime());

                //ERREUR_SN
                if ($status ==  $this->parameterBag->get('STATUT_NON_PUBLIE_SN')) {
                    $contratTitulaire->setErreurSn($message);
                }

                $contratTitulaire->setDateModification(new DateTime());
                $this->em->flush();

                $this->logger->info('ContratTitulaire status_publication_sn updated');
            } else {
                $this->logger->info('ContratTitulaire non présent en base. IdContrat = ' . $contratTitulaire);
            }
        } elseif ($donneeAnnuelle = self::getDonneeAnnuelle($idContrat, $nbModification)) {
            // push StatusModificationSn donnee annuelle
            $donneeAnnuelle->setSuiviPublicationSn(true);
            $this->em->persist($donneeAnnuelle);
            $this->logger->info('DonneeAnnuelle suivi_publication_sn updated');
            $this->em->flush();
        } else {
            $modificationContrat = $this->em
                ->getRepository(ModificationContrat::class)
                ->findOneBy(
                    [
                        'idContratTitulaire' => $idContrat,
                        'numOrdre' => (int)$nbModification
                    ]
                );
            if ($modificationContrat) {
                $modificationContrat->setStatutPublicationSn($status);

                //PUBLIE
                if ($status == $this->parameterBag->get('STATUT_PUBLIE_SN')) {
                    $modificationContrat->setDatePublicationSn(new Datetime());
                }

                $modificationContrat->setDateModificationSn(new DateTime());

                //ERREUR_SN
                if ($status ==  $this->parameterBag->get('STATUT_NON_PUBLIE_SN')) {
                    $modificationContrat->setErreurSn($message);
                }

                $modificationContrat->setDateModification(new DateTime());
                $contratTitulaire->setDateModification(new DateTime());

                $this->em->flush();

                $this->logger->info('ModificationContrat status_publication_sn updated');
            } else {
                $this->logger->info('ModificationContrat non présent en base. IdContrat = ' .
                    $idContrat . ', $nbModification=' .
                    $nbModification);
            }
        }
    }


    /**
     * @param $idContrat
     * @param $nbModification
     * @param $em
     * @return mixed|null
     */
    public function getDonneeAnnuelle($idContrat, $nbModification)
    {
        if (str_contains((string) $nbModification, '_')) {
            $numOrdre = (int)substr($nbModification, strpos($nbModification, '_') + 1);
            $donneesExecution = $this->em
                ->getRepository(DonneesAnnuellesConcession::class)
                ->findBy([
                    self::ARGUMENT_ID_CONTRAT => $idContrat,
                    'numOrdre' => $numOrdre
                ]);
            if (count($donneesExecution) > 0) {
                return array_pop($donneesExecution);
            }
        }
        return null;
    }
}
