<?php

namespace App\Command;

use App\Entity\TDumeContexte;
use App\Entity\TCandidature;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class PurgerDepotBrouillonModificationDumeAcheteurCommand.
 */
class PurgerDepotBrouillonModificationDumeAcheteurCommand extends Command
{
    protected static $defaultName = 'offre:suppr-brouillons:modification-dume-acheteur';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription("Supprimer les dépôts à l'état brouillon et dont le DUME acheteur vient d'être modifié")
            ->addArgument(
                'id_consultation',
                InputArgument::REQUIRED,
                'id_consultation modified by an agent'
            );
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start update draft');

        $em = $this->em;

        $idConsultation = $input->getArgument('id_consultation');
        $status = (int) $this->parameterBag->get('STATUT_CANDIDATUE_DUME_BROUILLON');
        $typeDume = $this->parameterBag->get('TYPE_DUME_OE');
        $newStatus = $this->parameterBag->get('STATUT_DUME_CONTEXTE_REMPLACER');

        $output->writeln('Start update draft table t_dume_contexte');

        $dumeContexte = $this->em
            ->getRepository(TDumeContexte::class)
            ->getBrouillonApresModificationDumeAcheteur($idConsultation, $typeDume, $status);

        if ((is_countable($dumeContexte) ? count($dumeContexte) : 0) > 0) {
            foreach ($dumeContexte as $value) {
                $value->setStatus($newStatus);

                $em->persist($value);
                $em->flush();

                $candidature = $this->em
                    ->getRepository(TCandidature::class)
                    ->getBrouillonApresModificationDumeAcheteur(
                        $idConsultation,
                        $value->getId(),
                        $status,
                        $this->parameterBag->get('TYPE_CANDIDATUE_DUME'),
                        $this->parameterBag->get('TYPE_CANDIDATUE_DUME_ONLINE')
                    );

                if (1 == (is_countable($candidature) ? count($candidature) : 0)) {
                    $output->writeln('Start update draft table t_candidature');

                    foreach ($candidature as $item) {
                        $item->setTypeCandidature(null);
                        $item->setTypeCandidatureDume(null);
                        $item->setIdDumeContexte(null);

                        $em->persist($item);
                        $em->flush();
                    }

                    $output->writeln('End update draft table t_candidature');
                }
            }
        }

        $output->writeln('End update draft table t_dume_contexte');
        $output->writeln('End update draft');
        return 0;
    }
}
