<?php

namespace App\Command;

use App\Entity\Agent;
use App\Security\PasswordEncoder\SodiumPepperEncoder;
use App\Service\PradoPasswordEncoderInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EncodePasswordCommand extends Command
{
    protected static $defaultName = 'password:encode';
    private $password;
    private $pepper;
    private $io;
    private const SODIUM_PEPPER = 'SODIUM_PEPPER';
    public const SUCCESS = 0;

    public function __construct(private readonly SodiumPepperEncoder $passwordEncoder, private readonly ParameterBagInterface $parameterBag)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Encoder un mot de passe pour un agent technique.');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function interact(InputInterface $input, OutputInterface $output): void
    {
        $this->io->title('Encoder un mot de passe pour un agent technique');
        $this->password = $this->io->askHidden('Entrer un mot de passe', function ($password) {
            if (empty($password)) {
                throw new InvalidArgumentException('Le mot de passe ne peut pas être vide.');
            }

            return $password;
        });
        $addPepper = $this->io->confirm('Voulez-vous entrer un pepper?', false);
        if (true === $addPepper) {
            $this->pepper = $this->io->askHidden('Entrer un pepper', function ($pepper) {
                if (empty($pepper)) {
                    throw new InvalidArgumentException('Le pepper ne peut pas être vide.');
                }

                return $pepper;
            });
        } else {
            $this->pepper = $this->parameterBag->get(self::SODIUM_PEPPER);
        }

        if (empty($this->pepper)) {
            throw new InvalidArgumentException('Le pepper ne peut pas être vide.');
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $encoderPassword = $this->passwordEncoder->encodePassword(
            $this->password ,
            $this->pepper
        );

        $this->io->success('Le mot de passe encodé : ' . $encoderPassword);

        return self::SUCCESS;
    }
}

