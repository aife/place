<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use App\Entity\Etablissement;
use App\Entity\TmpSiretIncorrect;
use App\Service\AtexoUtil;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;

/**
 * Cette classe doit être supprimée une fois que tous les mails sont envoyés
 */
class SendEmailSiretIncorrectCommand extends Command
{
    public const LIMIT = 500;
    private array $data = [];
    private int $idInscrit = 0;

    protected static $defaultName = 'mpe:siret-invalid';

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Environment $twig,
        private readonly AtexoUtil $atexoUtil
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Permet de récuperer les sirets erronés ')
            ->addOption(
                'sendmail',
                null,
                InputOption::VALUE_REQUIRED,
                'Permet d\'envoyer par defaut des mails à des établissements dont le SIRET est erroné',
                0
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $sendmail = $input->getOption('sendmail');
        $this->findAndInsertSiretWrong();

        if ($sendmail > 0) {
            $this->sendMail($sendmail);
        }

        return Command::SUCCESS;
    }

    private function findAndInsertSiretWrong(): void
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);

        do {
            $this->data = $this->em
                ->getRepository(Etablissement::class)
                ->findAccountsEstablishmentsAndSiret($this->idInscrit, self::LIMIT)
            ;

            $this->insertData();
        } while (self::LIMIT == count($this->data));
    }

    private function insertData(): void
    {
        foreach ($this->data as $value) {
            $siret = $value['siret'];
            $idInscrit = $value['id'];
            $email = $value['email'];

            if (
                (
                    null != $siret
                    && !$this->atexoUtil->isValidSiret($siret)
                )
                && null === $this->getEmail($email)
            ) {
                $tmpSiretIncorrect = new TmpSiretIncorrect();
                $tmpSiretIncorrect->setSiret($siret);
                $tmpSiretIncorrect->setEmail($email);
                $tmpSiretIncorrect->setIsSendMessage(false);
                $tmpSiretIncorrect->setIdInscrit($idInscrit);
                $tmpSiretIncorrect->setCreateDate(new \DateTime('now'));

                $this->em->persist($tmpSiretIncorrect);
            }

            $this->idInscrit = $idInscrit;
        }

        try {
            $this->em->flush();
        } catch (\Exception $e) {
            $this->em->rollback();
            $this->logger->error($e->getMessage());
        }

        $this->em->clear();
    }

    private function getEmail(string $email): ?TmpSiretIncorrect
    {
        return $this->em->getRepository(TmpSiretIncorrect::class)->findOneByEmail($email);
    }

    private function sendMail(int $limit): void
    {
        $mailer = new \Swift_Mailer(new \Swift_SmtpTransport());
        $tmpSiretIncorrects = $this->em->getRepository(TmpSiretIncorrect::class)->findCompte($limit);
        $beforeEmail = $this->parameterBag->get('PF_SHORT_NAME') . ' - ' .  $this->parameterBag->get('PF_LONG_NAME');

        foreach ($tmpSiretIncorrects as $tmpSiretIncorrect) {
            $mail = $tmpSiretIncorrect->getEmail();
            if( !filter_var($mail, FILTER_VALIDATE_EMAIL) ){
                $this->logger->info("L'email suivant : ".$tmpSiretIncorrect->getEmail()." est incorrect.\n");
                continue;
            }
            $message = (new \Swift_Message(
                'Action nécessaire à effectuer sur votre compte PLACE (Plateforme des Achats de l’État)'
            ))
                ->setFrom([$this->parameterBag->get('PF_MAIL_FROM') => $beforeEmail])
                ->setDate(new \DateTime('now'))
                ->setTo($mail)
                ->setBody(
                    $this->twig->render(
                        'tmp/siret_incorrect.html.twig',
                        [
                            'path' => $this->parameterBag->get('PF_URL_REFERENCE'),
                        ]
                    ),
                    'text/html'
                );

            try {
                $mailer->send($message);
                $tmpSiretIncorrect->setIsSendMessage(true);
                $tmpSiretIncorrect->setSendDate(new \DateTime('now'));
            } catch (\Exception $e) {
                $this->logger->info("L'email suivant : ".$tmpSiretIncorrect->getEmail()." est incorrect.\n");
                continue;
            }
        }

        $this->em->flush();
        $this->em->clear();
    }
}
