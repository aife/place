<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Messenger;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanMessengerCommand extends Command
{
    protected static $defaultName = 'mpe:messenger:clean';

    public function __construct(
        private EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription(
                'Commande qui supprime les failed'
            )
            ->addOption(
                'retention',
                null,
                InputArgument::OPTIONAL,
                'Nombre de jours que l\'on garde les messages',
                7
            )
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation la commande',
                true
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->dryRun = (bool)$input->getOption('dry-run');
        $this->retention = $input->getOption('retention');
        $dateRetention = date('Y-m-d 00:00:00', strtotime(' - '.$this->retention.'days'));
        $rawSql = "DELETE
            FROM messenger_messages
            where  queue_name = 'failed'
            and created_at <= '".$dateRetention."'";
        $deletes = $this->em->getConnection()->prepare($rawSql);
        if (!$this->dryRun) {
            $deletes->execute([]);
        }

        return 0;
    }
}
