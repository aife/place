<?php
/**
 * Commande console de synchronisation asynchrone des documents.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */

namespace App\Command;

use App\Service\SynchronizeDocuments;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SynchronizeDocumentsCommand extends Command
{
    protected static $defaultName = 'synchronize:documents';

    public function __construct(
        private readonly SynchronizeDocuments $synchronizeDocuments
    ) {
        parent::__construct();
    }

    /**
     * Configuration de la commande console.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function configure()
    {
        $this->setDescription('Permet de synchroniser les documents d\'une entreprise ou etablissement en asynchrone.')
            ->addArgument(
                'reference_consultation',
                InputArgument::REQUIRED,
                'Reference de la consultation'
            )
            ->addArgument(
                'id_entreprise',
                InputArgument::REQUIRED,
                'Identifiant de l\'entreprise'
            )
            ->addArgument(
                'id_etablissement',
                InputArgument::OPTIONAL,
                'Identifiant de l\'etablissement'
            )
            ->addArgument(
                'id_offre',
                InputArgument::OPTIONAL,
                'Identifiant de l\'offre'
            )

        ;
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $heureDebutExecution = date('H:i:s');
        $idEntreprise = $input->getArgument('id_entreprise');
        $idEtablissement = $input->getArgument('id_etablissement');
        $consultationId = $input->getArgument('reference_consultation');
        $idOffre = $input->getArgument('id_offre');

        $resultat = $this->synchronizeDocuments
            ->synchronizeDocuments($consultationId, $idEntreprise, $idEtablissement, $idOffre);

        $heureFinExecution = date('H:i:s');
        $output->writeln(
            <<<EOT

Resultat :
$resultat

Heure de debut de l'execution : $heureDebutExecution
Heure de fin de l'execution   : $heureFinExecution
EOT
        );
        return 0;
    }
}
