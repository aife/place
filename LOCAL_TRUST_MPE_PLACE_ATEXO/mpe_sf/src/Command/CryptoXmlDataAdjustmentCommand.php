<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\Offre;
use Doctrine\ORM\OptimisticLockException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ParserLogCommand.
 */
class CryptoXmlDataAdjustmentCommand extends Command
{
    protected static $defaultName = 'mpe:crypto:xml-data-adjustment';
    const ARGUMENT_LIMIT = 'limit';

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription("Permet de parser un fichier de log pour réinsérer le XML du chiffrement d'un dépôt")
            ->addArgument(
                'pathFileLog',
                InputArgument::REQUIRED,
                'Chemin absolu du fichier log que l\'on doit parser.'.
                ' Exemple : /tmp/registreDepot_20181712.log'
            )
            ->addArgument(
                self::ARGUMENT_LIMIT,
                InputArgument::OPTIONAL,
                'Limite sur le nombre de dépôts à mettre à jour. Cela permet de jouer le script sur un échantillon de dépôts.'.
                ' Exemple : 10'
            );
    }

    /**
     * Permet d'executer la commande console.
     *
     * @throws OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;
        $pathFileLog = $input->getArgument('pathFileLog');
        $limit = null;
        if ($input->getArgument(self::ARGUMENT_LIMIT)) {
            $limit = $input->getArgument(self::ARGUMENT_LIMIT);
        }
        if (!file_exists($pathFileLog)) {
            $output->writeln("Le fichier de log n'existe pas !");
            exit(1);
        }

        try {
            $log = file_get_contents($pathFileLog);
            $tabs = preg_split("/[\s[xml_string:protected]]+/", (string) $log);
            $this->parseLogFile($tabs, $em, $limit, $output);
            $output->writeln("Vous trouverez l'ancienne valeur sauvegardée dans un fichier json dans le répertoire /tmp. Exemple : /tmp/backup_1729_1620.json");
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage());
        }
        return 0;
    }

    /**
     * @param $tabs
     * @param $em
     * @param $limit
     * @param $output
     */
    protected function parseLogFile($tabs, $em, $limit, $output)
    {
        $i = 1;
        foreach ($tabs as $data) {
            if (strpos($data, '=> <?xml version="1.0" encoding="ISO-8859-1"')) {
                $cleanChaine = str_replace(' => ', '', $data);
                $cleanChaine = utf8_encode(trim(str_replace('[nom_entreprise_inscrit:protecte', '', $cleanChaine)));

                try {
                    $this->persistXmlString($cleanChaine, $em);
                } catch (Exception $exception) {
                    $output->writeln("\n\n -------------------- XML invalide ! ---------------- \n\n Error :".$exception->getMessage()."\n\n XML=>".$cleanChaine);
                }
                if (!is_null($limit)) {
                    if ($i == $limit) {
                        break;
                    }
                    ++$i;
                }
            }
        }
    }

    /**
     * @param $cleanChaine
     * @param $em
     */
    protected function persistXmlString($cleanChaine, $em)
    {
        $xml = simplexml_load_string($cleanChaine);
        $idOffre = (string) $xml->attributes()['idReponseAnnonce']['0'];
        $idRef = (string) $xml->attributes()['reference']['0'];
        $offre = $this->em->getRepository(Offre::class)->findBy(['id' => $idOffre, 'consultationRef' => $idRef]);
        if ((is_countable($offre) ? count($offre) : 0) > 0) {
            if ($offre[0] instanceof Offre) {
                $backup = '/tmp/backup_'.$idOffre.'_'.$idRef.'.json';
                $flux = fopen($backup, 'w+');
                $tabBack = ['id' => $idOffre, 'consultationRef' => $idRef, 'xmlString' => $offre[0]->getXmlString()];
                fwrite($flux, json_encode($tabBack, JSON_THROW_ON_ERROR));
                fclose($flux);
                $offre[0]->setXmlString($cleanChaine);
                $this->em->persist($offre[0]);
                $this->em->flush();
            }
        }
    }
}
