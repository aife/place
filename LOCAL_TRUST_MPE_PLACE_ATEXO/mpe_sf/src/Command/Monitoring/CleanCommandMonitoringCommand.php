<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Monitoring;

use App\Entity\CommandMonitoring;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanCommandMonitoringCommand extends \App\Command\MonitoredCommand
{
    protected static $defaultName = 'mpe:monitoring:clean';

    public function __construct(
        private EntityManagerInterface $em,
        private LoggerInterface $logger,
        private CommandMonitoringService $commandMonitoringService
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function configure()
    {
        $this->setDescription(
            'Supprimer les logs sur la table command_monitoring qui datent de plus d\'un mois'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = null;
        try {
            $em = $this->em;
            $logger = $this->logger;
            $output->writeln('Start deleting commande monitoring');
            $repo = $em->getRepository(CommandMonitoring::class);
            //effacer les lignes qui datent de plus 1 mois.
            $repo->clean((new \DateTime('now'))->modify('-1 month'));
            $output->writeln('End deleting commande monitoring');
        } catch (\Exception $e) {
            $erreur = 'Erreur CleanCommandMonitoringCommand => ' . $e->getMessage();
            $logger->error($erreur);
            $output->writeln($erreur);

            return 1;
        }
        return 0;
    }
}
