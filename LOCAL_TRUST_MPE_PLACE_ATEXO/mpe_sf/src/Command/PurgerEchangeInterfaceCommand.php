<?php

namespace App\Command;

use DateTime;
use App\Entity\EchangesInterfaces;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class PurgerAccessTokenCommand.
 */
class PurgerEchangeInterfaceCommand extends Command
{
    protected static $defaultName = 'mpe:echangeinterface:purge';
    private ?InputInterface $input = null;
    private ?OutputInterface $output = null;

    protected function configure()
    {
        $desc = 'Nombre de jours de durée de vie (ex: 100) (valeur par defaut définie par la config produit ou client)';
        $this->setDescription('Supprimer les EchangeInterface qui datent')
            ->addArgument(
                'dureeVie',
                InputArgument::OPTIONAL,
                $desc
            )
            ->addOption(
                'nombre-enregistrements',
                'N',
                InputOption::VALUE_NONE,
                "Affiche le nombre d'enregistrements de la table (lancement à la main)",
                null
            )
        ;
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $this->input = $input;
        $logger = $this->logger;

        try {
            $dureeVie = $this->parameterBag->get('DUREE_VIE_ECHANGE_INTERFACE_JOURS');
            $dureeVieArg = $input->getArgument('dureeVie');
            if (!empty($dureeVieArg)) {
                $dureeVie = $dureeVieArg;
                if ((int) $dureeVie < 9) {
                    $msg = 'La duree de vie de la table echanges_interfaces doit être supérieure à 9 jours!';
                    throw new \Exception($msg);
                }
            }

            $this->displayCount();
            $output->writeln('Start deleting');
            $this->em->getRepository(EchangesInterfaces::class)->purge($dureeVie);
            $output->writeln('End deleting');
            $this->displayCount();
        } catch (Exception $e) {
            $erreur = 'Erreur PurgerEchangesInterfacesCommand => ' . $e->getMessage();
            $logger->error($erreur);
        }
        return 0;
    }

    public function displayCount()
    {
        if (true === $this->input->getOption('nombre-enregistrements')) {
            $countMsg = 'Nombre de lignes restant dans la table echange_interface: %s';
            $count = $this->em->getRepository(EchangesInterfaces::class)->countAllRecords();
            $count = sprintf($countMsg, $count);
            $this->output->writeln(sprintf($countMsg, $count));
        }
    }
}
