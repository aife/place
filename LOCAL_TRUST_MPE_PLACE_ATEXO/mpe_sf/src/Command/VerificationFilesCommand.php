<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use PDO;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Script de vérification des fichiers.
 */
class VerificationFilesCommand extends Command
{
    protected static $defaultName = 'mpe:verification:files';
    protected const LIMIT = 500;
    protected const OFFSET = 500;
    protected ?OutputInterface $output = null;
    private ?Filesystem $filesystem = null;
    private ?SymfonyStyle $io = null;
    private ?bool $view = null;

    protected function configure()
    {
        $this->setDescription('Lister les fichiers manquant (blobs)')
            ->addArgument(
                'yearMin',
                InputArgument::OPTIONAL,
                'année min sur lequel on fait la recherche',
                '2019'
            )
            ->addArgument(
                'yearMax',
                InputArgument::OPTIONAL,
                'année sur lequel on fait la recherche',
                '2020'
            )
            ->addArgument(
                'view',
                InputArgument::OPTIONAL,
                'Valeur qui permet d\'afficher les messages',
                false
            );
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
    ) {
        parent::__construct();
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $result = [];
        $logger = null;
        try {
            $this->output = $output;
            $logger = $this->logger;

            $directory = $this->parameterBag->get('BASE_ROOT_DIR');
            $tmp = $this->parameterBag->get('COMMON_TMP');
            $this->view = $view = (bool) $input->getArgument('view');
            $yearMin = $input->getArgument('yearMin');
            $yearMAx = $input->getArgument('yearMax');

            $this->io = $io = new SymfonyStyle($input, $output);
            $this->filesystem = new Filesystem();

            /**
             * Traitement des new ID.
             */
            $sql = 'SELECT blobOrganisme_file.id, blobOrganisme_file.chemin, blobOrganisme_file.organisme
                    From  fichierEnveloppe,
                        Enveloppe,
                         Offres,
                         blobOrganisme_file
                    WHERE Offres.id = Enveloppe.offre_id
            AND Offres.organisme = Enveloppe.organisme
            AND fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
            AND fichierEnveloppe.organisme = Enveloppe.organisme
            AND fichierEnveloppe.id_blob = blobOrganisme_file.id
            AND fichierEnveloppe.id_blob is not null
            AND Offres.statut_offres != \'99\'
            AND substr(Offres.date_depot, 1, 4) between :yearMin and  :yearMax  
            AND blobOrganisme_file.old_id is null
            LIMIT :offset, :limit';
            $stmt = $this->em->getConnection()->prepare($sql);
            $stmt->bindValue(':yearMin', $yearMin);
            $stmt->bindValue(':yearMax', $yearMAx);
            $limit = self::LIMIT;
            $offset = 0;
            $count = 0;
            $fileTmps = $tmp.'/file_absent_new_id_'.date('Yndhis').'.csv';
            while (true) {
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
                $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
                $results = $stmt->executeQuery()->fetchAllAssociative();
                if (0 === (is_countable($results) ? count($results) : 0)) {
                    break;
                }
                foreach ($results as $result) {
                    if ($result['chemin']) {
                        $file = $directory.$result['chemin'].$result['id'].'-0';
                    } else {
                        $file = $directory.$result['organisme'].'/files/'.$result['id'].'-0';
                    }

                    if (!$this->filesystem->exists($file)) {
                        $message = $file." ;\n";
                        $this->filesystem->appendToFile($fileTmps, $message);
                        if ($this->view) {
                            $this->io->error($message);
                        }
                    }
                    ++$count;
                }
                $memoryUsage = memory_get_usage(true) / 1024 / 1024;
                $msg = "\nMémoire utilisée : ${memoryUsage}M";
                $msg .= ' - Nombre blob au total checké : '.$count;
                if ($view) {
                    $io->text($msg);
                    $io->text('Identifiant du dernière blob_id traitée : '.$result['id']);
                }
                $offset += self::OFFSET;
                unset($results);
            }
            /**
             * Traitement des ancien ID.
             */
            $sqlOldId = "SELECT blobOrganisme_file.old_id, blobOrganisme_file.chemin, blobOrganisme_file.organisme
                    From  fichierEnveloppe,
                        Enveloppe,
                         Offres,
                         blobOrganisme_file
                    WHERE Offres.id = Enveloppe.offre_id
            AND Offres.organisme = Enveloppe.organisme
            AND fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
            AND fichierEnveloppe.organisme = Enveloppe.organisme
            AND fichierEnveloppe.id_blob = blobOrganisme_file.old_id
            AND fichierEnveloppe.id_blob is not null
            AND Offres.statut_offres != '99'
            AND substr(Offres.date_depot, 1, 4) between :yearMin and  :yearMax 
            AND blobOrganisme_file.old_id is not null
            LIMIT :offset, :limit";
            $stmt = $this->em->getConnection()->prepare($sqlOldId);
            $stmt->bindValue(':yearMin', $yearMin);
            $stmt->bindValue(':yearMax', $yearMAx);
            $limit = self::LIMIT;
            $offset = 0;
            $count = 0;
            $fileTmps = $tmp.'/file_absent_old_id_'.date('Yndhis').'.csv';
            while (true) {
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
                $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
                $results = $stmt->executeQuery()->fetchAllAssociative();
                if (0 === (is_countable($results) ? count($results) : 0)) {
                    break;
                }
                foreach ($results as $result) {
                    if ($result['chemin']) {
                        $file = $directory.$result['chemin'].$result['old_id'].'-0';
                    } else {
                        $file = $directory.$result['organisme'].'/files/'.$result['old_id'].'-0';
                    }

                    if (!$this->filesystem->exists($file)) {
                        $message = $file." ;\n";
                        $this->filesystem->appendToFile($fileTmps, $message);
                        if ($this->view) {
                            $this->io->error($message);
                        }
                    }
                    ++$count;
                }
                $memoryUsage = memory_get_usage(true) / 1024 / 1024;
                $msg = "\nMémoire utilisée : ${memoryUsage}M";
                $msg .= ' - Nombre blob au total checké : '.$count;
                if ($view) {
                    $io->text($msg);
                    $io->text('Identifiant du dernière blob_id traitée : '.$result['old_id']);
                }
                $offset += self::OFFSET;
                unset($results);
            }
        } catch (Exception $e) {
            $erreur = 'Erreur => '.$e->getMessage();
            $logger->error($erreur);
        }
        return 0;
    }
}
