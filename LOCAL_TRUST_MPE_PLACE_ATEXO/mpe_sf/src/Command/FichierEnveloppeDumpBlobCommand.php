<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Command;

use App\Entity\BloborganismeFile;
use App\Entity\EnveloppeFichier;
use App\Repository\EnveloppeFichierRepository;
use App\Service\AtexoFichierOrganisme;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class FichierEnveloppeDumpBlobCommand.
 */
class FichierEnveloppeDumpBlobCommand extends Command
{
    protected static $defaultName = 'mpe:fichierEnveloppe:dump-blob';
    private const FILE_ID = 'idFichier';
    private const ORGANISME = 'organisme';

    private $commonTmp;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoFichierOrganisme $fichierOrganismeService
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Export sur le filesystem du champ signature_fichier de la table fichierEnveloppe');
    }

    /**
     * @return void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->commonTmp = $this->parameterBag->get('COMMON_TMP');

        $io->comment("Début de la migration de 'signature_fichier'");

        try {
            $count = 0;
            $memoryUsage = null;
            $this->entityManager->getConnection()->beginTransaction();
            /** @var EnveloppeFichierRepository $repository */
            $repository = $this->entityManager->getRepository(EnveloppeFichier::class);

            foreach ($repository->progressiveFindAll() as $enveloppeFichier) {
                $memoryUsage = memory_get_usage(true) / 1024 / 1024;

                if (0 === $count % $repository::LIMIT) {
                    $this->entityManager->getConnection()->commit();
                    $msg = "\nMémoire utilisée : ${memoryUsage}M";
                    $msg .= ' - fichierEnveloppe traités : ' . $count;
                    $io->text($msg);
                    $io->text('Identifiant du dernier fichierEnveloppe traité : ' . $enveloppeFichier[self::FILE_ID]);
                    $this->entityManager->getConnection()->beginTransaction();
                }

                $this->traitement($io, $enveloppeFichier);

                if (!$this->entityManager->isOpen()) {
                    $this->entityManager->resetManager();
                }
                ++$count;
            }
            $this->entityManager->getConnection()->commit();
            $io->text("Mémoire utilisée : ${memoryUsage}M");
            $io->text('Offres traitées : ' . $count);
        } catch (\Exception $exception) {
            $io->write('Erreur :' . $exception);
        }
        $io->comment('Fin de la migration du XML Réponse');
        return 0;
    }

    /**
     * @throws ConnectionException
     * @throws Exception
     */
    protected function traitement(SymfonyStyle $io, array $enveloppeFichier)
    {
        try {
            $fileName = '%s-%s-%s.xml';
            $fileName = sprintf(
                $fileName,
                $enveloppeFichier[self::ORGANISME],
                $enveloppeFichier['idEnveloppe'],
                $enveloppeFichier[self::FILE_ID]
            );
            $tmpFilePath = $this->commonTmp . $fileName;
            file_put_contents($tmpFilePath, $enveloppeFichier['signatureFichier']);
            $this->fichierOrganismeService->moveTmpFile($fileName);

            $idBlob = $this->fichierOrganismeService->insertFile(
                $fileName,
                $tmpFilePath,
                $enveloppeFichier[self::ORGANISME]
            );
            $blobOrganisme = $this->entityManager->getRepository(BloborganismeFile::class)->findOneById($idBlob);

            $this->update($enveloppeFichier, $blobOrganisme);

            $this->entityManager->clear(BloborganismeFile::class);
            unlink($tmpFilePath);
        } catch (\Exception $exception) {
            $this->entityManager->getConnection()->rollBack();
            $id = null;

            if (isset($enveloppeFichier[self::FILE_ID])) {
                $id = $enveloppeFichier[self::FILE_ID];
            }
            $io->writeln(
                'Problème de migration du fichier_signature '
                . $id
                . ". Message d'erreur : "
                . $exception->getMessage()
            );
        }
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function update($fichierEnveloppe, BloborganismeFile $blobOrganisme)
    {
        $sql = 'update fichierEnveloppe set id_blob_signature = ? WHERE id_fichier = ? and organisme = ? ';
        $stmt = $this->entityManager->getConnection()->prepare($sql);
        $stmt->bindValue(1, $blobOrganisme->getId());
        $stmt->bindValue(2, $fichierEnveloppe[self::FILE_ID]);
        $stmt->bindValue(3, $fichierEnveloppe[self::ORGANISME]);
        $stmt->execute();
        unset($fichierEnveloppe);
    }
}
