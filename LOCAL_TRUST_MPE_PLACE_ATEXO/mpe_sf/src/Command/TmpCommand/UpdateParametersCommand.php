<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\TmpCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Yaml;

class UpdateParametersCommand extends Command
{
    public function __construct(private Filesystem $filesystem)
    {
        parent::__construct();
    }

    protected static $defaultName = 'mpe:update:parameters';

    protected function configure()
    {
        $this->setDescription('Command MUST only used on dev mode, help to find duplicated '
            . 'parameters in other parameters files and remove from parameters.yml');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listFiles = [
            './config/parameters_client.yml',
            './config/constantes_application.yml',
            './config/parameters_authentication.yml',
            './config/parameters_client.yml',
            './config/parameters_modules.yml',
            './config/parameters_technique.yml',
        ];
        $parameters = Yaml::parseFile('./config/parameters.yml');
        $parametersClient = [];
        foreach ($listFiles as $file) {
            $parametersClient += Yaml::parseFile($file)['parameters'];
        }

        foreach ($parameters['parameters'] as $key => $parameter) {
            if (
                array_key_exists($key, $parametersClient)
                || array_key_exists(str_replace(['env(', ')'], '', $key), $parametersClient)
            ) {
                unset($parameters['parameters'][$key]);
            }
        }
        $this->filesystem->dumpFile('./config/parameters.yml', Yaml::dump($parameters));

        return 0;
    }
}
