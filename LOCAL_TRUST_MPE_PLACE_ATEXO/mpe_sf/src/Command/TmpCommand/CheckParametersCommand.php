<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\TmpCommand;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Yaml\Yaml;

class CheckParametersCommand extends Command
{
    protected static $defaultName = 'mpe:check:parameters';

    protected function configure()
    {
        $this->setDescription('Command MUST only used on dev mode, help to find duplicated '
            . 'parameters in other parameters files and remove from parameters.yml');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $listFiles = [
            './config/parameters_authentication.yml',
            './config/parameters_modules.yml',
            './config/parameters.yml',
        ];
        $dotEnv = new Dotenv();
        $dotEnv->load('./.env');

        $parametersClient = [];
        foreach ($listFiles as $file) {
            $parametersClient += Yaml::parseFile($file)['parameters'];
        }

        $output->writeln('List of parameters not found on .env : ');
        foreach ($parametersClient as $key => $parameter) {
            if (false !== getenv($key) || false !== getenv(str_replace(['env(', ')'], '', $key))) {
                continue;
            }

            if (!str_starts_with($key, 'env(')) {
                $output->writeln($key);
            }
        }

        return 0;
    }
}
