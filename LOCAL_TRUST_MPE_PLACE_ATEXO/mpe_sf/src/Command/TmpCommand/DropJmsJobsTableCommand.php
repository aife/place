<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\TmpCommand;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DropJmsJobsTableCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected static $defaultName = 'mpe:drop-jmsjobs-table';

    protected function configure()
    {
        // Cette commande pourra être supprimée quand la table jms_jobs aura été supprimée de toutes les PF MPE.
        $this->setDescription(
            'Drops stackTrace of jms_jobs table to avoid error while running messenger:setup-transports command'
        );
    }

    /**
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $stmt = $this->em->getConnection()->prepare('select count(*) from jms_jobs;');
            $stmt->executeStatement();
        } catch (\Exception $e) {
            // La table n'existe pas, on ne fait rien.
            return 0;
        }

        // La table existe, on drop le champ s'il existe.
        $stmt = $this->em->getConnection()->prepare('alter table jms_jobs drop column if exists stackTrace;');
        $stmt->executeStatement();

        return 0;
    }
}
