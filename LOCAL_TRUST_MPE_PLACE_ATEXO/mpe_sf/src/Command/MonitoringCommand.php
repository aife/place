<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use App\Entity\ModuleSatelliteMonitoring;
use App\Service\CommandMonitoringService;
use App\Service\Monitoring\MonitoringInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Stopwatch\Stopwatch;

class MonitoringCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:monitor:services';

    public function __construct(
        #[TaggedIterator(MonitoringInterface::class)] private iterable $monitoringServices,
        private CommandMonitoringService $commandMonitoringService,
        private ParameterBagInterface $parameterBag,
        private Stopwatch $stopwatch,
        private EntityManagerInterface $em
    ) {
        parent::__construct($commandMonitoringService);
    }

    public function configure()
    {
        $this->setDescription('This command verify access to external modules.');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $starttime = date('Y-m-d H:i:s', strtotime('now'));
        $output->writeln('Command started will be stopped automatically after 5 minutes ....');
        $this->em->createQuery('DELETE ' . ModuleSatelliteMonitoring::class . ' sm')->execute();
        foreach ($this->monitoringServices as $service) {
            $process = new Process(
                [$this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/bin/console',
                    'mpe:monitor:service', get_class($service)]
            );
            $process->setTimeout(300);
            $process->start();
        }

        sleep(60 * 5);
        $endtime = date('Y-m-d H:i:s', strtotime('now'));

        $serviceMonitor = new ModuleSatelliteMonitoring();
        $serviceMonitor->setService('last_refresh');
        $serviceMonitor->setJsonResponse(
            ['last_refresh' => [
                'status' => 'Completed',
                'start' => $starttime,
                'end' => $endtime
            ]]
        );

        $this->em->persist($serviceMonitor);
        $this->em->flush();
        $output->writeln('Stopped.');

        return 0;
    }
}
