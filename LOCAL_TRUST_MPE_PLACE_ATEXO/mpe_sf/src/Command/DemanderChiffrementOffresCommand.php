<?php

/**
 * Commande console de chiffrement asynchrone des offres.
 */

namespace App\Command;

use App\Entity\Consultation;
use App\Entity\Offre;
use App\Service\AtexoChiffrementOffre;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DemanderChiffrementOffresCommand extends Command
{
    protected static $defaultName = 'offre:demande:chiffrement';
    public function __construct(
        protected ParameterBagInterface $parameterBag,
        protected EntityManagerInterface $em,
        protected AtexoChiffrementOffre $chiffrementOffreService
    ) {
        parent::__construct();
    }

    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription('Permet de demander le chiffrement d\'une ou plusieurs offres en asynchrone. 
            Si id_offre passé en parametre alors le chiffrement concerne cette offre 
            sinon les offres non chiffrees en base de donnees.')
            ->addArgument(
                'id_offre',
                InputArgument::OPTIONAL,
                'Identifiant de l\'offre'
            )
        ;
    }

    /**
     * Permet d'executer la commande console.
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $heureDebutExecution = date('H:i:s');
        $idOffre = $input->getArgument('id_offre');
        $res = 0;
        $resultat = '';
        if (!empty($idOffre)) {
            $code = $this->demanderChiffrementOffre($idOffre, $resultat);
            if (empty($code)) {
                $res = -1;
            }
        } else {
            $firstOffres = $this->em->getRepository(Offre::class)
                ->getOffresByStatut($this->parameterBag->get('STATUT_ENV_FERMETURE_EN_ATTENTE'));
            $secondOffres = $this->em->getRepository(Offre::class)
                ->getOffresByStatut($this->parameterBag->get('STATUT_ENV_EN_ATTENTE_CHIFFREMENT'));

            $uniqueOffres = $this->getUniqueOffres(array_merge($firstOffres, $secondOffres));

            $res = $this->chiffrerOffres($uniqueOffres, $resultat);
        }

        $heureFinExecution = date('H:i:s');
        $output->writeln(
            <<<EOT

Resultat :
$resultat
$res

Heure de debut de l'execution : $heureDebutExecution
Heure de fin de l'execution   : $heureFinExecution
EOT
        );

        return $res;
    }

    /**
     * demander chiffrement pour une liste des offres.
     */
    private function chiffrerOffres(array $offres, string &$resultat): int
    {
        $code = 0;
        if ($offres) {
            foreach ($offres as $offre) {
                if ($offre instanceof Offre) {
                    $result = $this->demanderChiffrementOffre($offre->getId(), $resultat);
                    if (empty($result)) {
                        $code = -1;
                    }
                }
            }
        } else {
            $resultat .= 'AUCUNE OFFRE A CHIFFRER';
        }

        return $code;
    }

    /**
     * Permet de chiffrer l'offre.
     */
    private function demanderChiffrementOffre(int $idOffre, string &$resultat): int
    {
        $offre = $this->em->getRepository(Offre::class)->find($idOffre);
        $code = 0;
        if ($offre instanceof Offre) {
            $consultation = $this->em->getRepository(Consultation::class)->find($offre->getConsultationId());
            if ($consultation instanceof Consultation) {
                $code = $this->chiffrementOffreService->demanderChiffrementOffre($offre);
                $resultat .= PHP_EOL . ("offre_id : $idOffre =======> demande de chiffrement "
                        . ((!empty($code)) ? "OK [code = $code]" : 'KO')) . PHP_EOL;
            }
        } else {
            $resultat .= "Offre_id = $idOffre non trouvee en base de donnees";
        }

        return $code;
    }

    private function getUniqueOffres(array $mergedOffres): array
    {
        $uniqueOffres = [];
        $idOffreExist = [];

        /** @var Offre $offre */
        foreach ($mergedOffres as $offre) {
            if (!in_array($offre->getId(), $idOffreExist, true)) {
                $uniqueOffres[] = $offre;
                $idOffreExist[] = $offre->getId();
            }
        }

        return $uniqueOffres;
    }
}
