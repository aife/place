<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\OptimisticLockException;
use App\Service\AtexoFichierOrganisme;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\TDossierFormulaire;
use App\Entity\TEditionFormulaire;
use App\Entity\TEnveloppeDossierFormulaire;
use App\Entity\TReponseElecFormulaire;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class CheckMailInvalidCommand.
 */
class RestoreReponseAofCommand extends Command
{
    protected static $defaultName = 'mpe:restore-reponse-aof';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private AtexoConfiguration $atexoConfiguration,
        private ParameterBagInterface $parameterBag,
        private AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Commande qui permet de récuperer une offre aof perdu')
            ->addArgument(
                'fileJson',
                InputArgument::REQUIRED,
                'Fichier qui contient json envoyé au moment du depot'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $fileJson = $input->getArgument('fileJson');
            $json = is_file($fileJson) ? file_get_contents($fileJson) : '';
            if (!$fileJson || empty($json)) {
                throw new Exception('Fichier non défini');
            }

            if ($this->atexoConfiguration->hasConfigPlateforme('interfaceModuleSub')) {
                $arrayInfoOffre = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                $idOffre = $arrayInfoOffre['offre']['idOffre'];
                if ($idOffre) {
                    $offre = $this->entityManager->getRepository(Offre::class)->find($idOffre);
                    $statutOffre = [$this->parameterBag->get('STATUT_ENV_FERMETURE_EN_ATTENTE'), $this->parameterBag->get('STATUT_ENV_FERMETURE_EN_COURS')];
                    if ($offre instanceof Offre && in_array($offre->getStatutOffres(), $statutOffre)) {
                        echo 'id Offre '.$offre->getId()."\n";
                        $reponseElecFormulaire = $this->entityManager->getRepository(TReponseElecFormulaire::class)
                            ->findOneBy([
                                'consultationRef' => $offre->getConsultationRef(),
                                'organisme' => $offre->getOrganisme(),
                                'idInscrit' => $offre->getInscrit(),
                            ]);

                        if ($reponseElecFormulaire instanceof TReponseElecFormulaire) {
                            echo 'id reponseElecFormulaire '.$reponseElecFormulaire->getIdReponseElecFormulaire()."\n";
                            $enveloppesInfo = $arrayInfoOffre['offre']['enveloppes'];
                            foreach ($enveloppesInfo as $enveloppeInfo) {
                                $idEnveloppe = $enveloppeInfo['idEnveloppe'];
                                $enveloppeFormulaire = $this->entityManager
                                    ->getRepository(TEnveloppeDossierFormulaire::class)
                                    ->findOneBy([
                                        'idEnveloppe' => $idEnveloppe,
                                        'organisme' => $offre->getOrganisme(),
                                        'idLot' => $enveloppeInfo['numeroLot'],
                                        'typeEnveloppe' => $enveloppeInfo['type'],
                                        'typeReponse' => $this->parameterBag->get('TYPE_DOSSIER_REPONSE_PRINCIPALE'),
                                    ]);

                                if ($enveloppeFormulaire instanceof TEnveloppeDossierFormulaire) {
                                    $dossier = $this->entityManager
                                        ->getRepository(TDossierFormulaire::class)
                                        ->findOneBy([
                                            'idDossierFormulaire' => $enveloppeFormulaire->getIdDossierFormulaire(),
                                            'idReponseElecFormulaire' => $reponseElecFormulaire->getIdReponseElecFormulaire(),
                                            'idLot' => $enveloppeInfo['numeroLot'],
                                            'typeEnveloppe' => $enveloppeInfo['type'],
                                            'formulaireDepose' => $this->parameterBag->get('FORMULAIRE_DEPOSE_DOSSIER_TRANSMIS'),
                                            'typeReponse' => $this->parameterBag->get('TYPE_DOSSIER_REPONSE_PRINCIPALE'),
                                        ]);
                                    if ($dossier instanceof TDossierFormulaire) {
                                        try {
                                            $enveloppe = $this->createEnveloppe(
                                                $offre,
                                                $idEnveloppe,
                                                $enveloppeFormulaire->getTypeEnveloppe(),
                                                $enveloppeFormulaire->getIdLot(),
                                                $enveloppeFormulaire->getOrganisme()
                                            );
                                            echo 'id enveloppe '.$enveloppe->getIdEnveloppeElectro()."\n";
                                            if ($enveloppe->getFichierEnveloppes()->isEmpty()) {
                                                $editions = $dossier->getTEditionFormulaires();
                                                if (is_countable($editions) ? count($editions) : 0) {
                                                    $numOrdre = 1;
                                                    foreach ($editions as $edition) {
                                                        if ($edition instanceof TEditionFormulaire) {
                                                            echo 'creation fichier '.$edition->getNomFichier()."\n";
                                                            $this->createFichierEnveloppeFromEdition(
                                                                $enveloppe->getUidResponse(),
                                                                $edition,
                                                                $enveloppe,
                                                                $enveloppe->getOrganisme(),
                                                                $numOrdre
                                                            );

                                                            ++$numOrdre;
                                                        }
                                                    }
                                                }
                                            } else {
                                                echo "Des fichiers existes dans l'enveloppe ".$enveloppe->getIdEnveloppeElectro()."\n";
                                            }
                                        } catch (Exception $e) {
                                            throw $e;
                                        }
                                    }
                                }
                            }
                            $offre->setStatutOffres($this->parameterBag->get('STATUT_ENV_FERME'));
                            $this->entityManager->persist($offre);
                            $this->entityManager->flush();
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $output->writeln(sprintf($e->getMessage()));
        }
        return 0;
    }

    /**
     * Permet de creer une enveloppe.
     *
     * @param string $uidResponse   unique ID du depot
     * @param int    $typeEnveloppe type d'enveloppe
     * @param int    $lot           numero du lot
     * @param null   $fictive
     *
     * @return Enveloppe
     *
     * @throws Exception
     */
    private function createEnveloppe(Offre $offre, $idEnveloppe, $typeEnveloppe, $lot, $acronymeEnveloppe)
    {
        if (null === $typeEnveloppe) {
            throw new Exception("Le type d'enveloppe est introuvable");
        }

        if (null === $lot) {
            throw new Exception('Le numero du lot est introuvable');
        }

        try {
            $enveloppe = $this->entityManager->getRepository(Enveloppe::class)->find($idEnveloppe);
            if (!$enveloppe instanceof Enveloppe) {
                $enveloppe = new Enveloppe();
                $enveloppe->setIdEnveloppeElectro($idEnveloppe);
                $enveloppe->setOrganisme($acronymeEnveloppe);
                $enveloppe->setUidResponse($offre->getUidResponse());
                $enveloppe->setOffre($offre);
                $enveloppe->setTypeEnv($typeEnveloppe);
                $enveloppe->setStatutEnveloppe($this->parameterBag->get('STATUT_ENV_FERME'));
                $enveloppe->setSousPli($lot);
                $enveloppe->setChampsOptionnels('');
                $enveloppe->setDonneesOuverture('');
                $enveloppe->setHorodatageDonneesOuverture('');
                $enveloppe->setNomAgentOuverture('');
                $this->entityManager->persist($enveloppe);
                $metadata = $this->entityManager->getClassMetaData(Enveloppe::class);
                $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
                $metadata->setIdGenerator(new AssignedGenerator());
                $this->entityManager->flush();
            }

            return $enveloppe;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $uidResponse
     * @param $edition
     * @param $enveloppe
     * @param $acronymeOrganisme
     * @param $numOrdre
     *
     * @throws OptimisticLockException
     */
    public function createFichierEnveloppeFromEdition($uidResponse, TEditionFormulaire $edition, $enveloppe, $acronymeOrganisme, $numOrdre)
    {
        try {
            $enveloppeFichier = new EnveloppeFichier();
            $enveloppeFichier->setUidResponse($uidResponse);
            $enveloppeFichier->setEnveloppe($enveloppe);
            $enveloppeFichier->setOrganisme($acronymeOrganisme);
            $enveloppeFichier->setNumOrdreFichier($numOrdre);
            $enveloppeFichier->setTypeFichier($edition->getType());
            $enveloppeFichier->setTypePiece($edition->getType());
            $enveloppeFichier->setTailleFichier(filesize($edition->getPath()));
            $enveloppeFichier->setNomFichier($edition->getNomFichier());
            $enveloppeFichier->setHash(sha1_file($edition->getPath()));
            $enveloppeFichier->setHash256(hash_file('sha256', $edition->getPath()));

            $enveloppeFichier->setIdTypePiece(0);
            $enveloppeFichier->setIsHash(1);

            $enveloppeFichier->setSignatureFichier('');
            $enveloppeFichier->setVerificationCertificat('');

            $idBlob = $this->atexoFichierOrganisme->insertFile($edition->getNomFichier(), $edition->getPath(), $acronymeOrganisme);
            $enveloppeFichier->setIdBlob($idBlob);
            $enveloppeFichier->setEnveloppe($enveloppe);
            $this->entityManager->persist($enveloppeFichier);
            $this->entityManager->flush();
        } catch (Exception $e) {
            throw $e;
        }
    }
}
