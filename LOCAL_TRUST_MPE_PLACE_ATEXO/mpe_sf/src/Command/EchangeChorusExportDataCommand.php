<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use Exception;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\ContratTitulaire;
use App\Service\AtexoEchangeChorus;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class EchangeChorusExportDataCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'mpe:echange-chorus:export-data-csv';

    private const FILE_EXTENSION = 'CSV';

    private const LABEL_RAISON_SOCIALE_ATTRIBUTAIRE = 'RaisonSocialeAttributaire';
    private const LABEL_SIRET_ATTRIBUTAIRE = 'SIRETAttributaire';
    private const LABEL_CODE_APE = 'CodeAPE';
    private const LABEL_FORME_JURIDIQUE = 'FormeJuridique';
    private const LABEL_PME = 'PME';
    private const LABEL_PAYS_TERRITOIRE = 'PaysTerritoire';
    private const LABEL_NUMERO_NATIONAL_ATTRIBUTAIRE = 'NumeroNationalAttributaire';

    private const LABEL_SIRET_ENTREPRISE = 'sirenEntreprise';
    private const LABEL_CODE_ETABLISSEMENT = 'codeEtablissement';
    private const LABEL_NUMERO_LONG_OEAP = 'numLongOEAP';

    public final const SUCCESS = 0;

    private const LIMIT_ROWS = 1000;
    private const OFFSET = 0;

    private const HEADER_LIST = [
        self::LABEL_NUMERO_LONG_OEAP,
        self::LABEL_RAISON_SOCIALE_ATTRIBUTAIRE,
        self::LABEL_SIRET_ATTRIBUTAIRE,
        self::LABEL_CODE_APE,
        self::LABEL_FORME_JURIDIQUE,
        self::LABEL_PME,
        self::LABEL_PAYS_TERRITOIRE,
        self::LABEL_NUMERO_NATIONAL_ATTRIBUTAIRE,
    ];

    /**
     * @var bool
     */
    private $dryRun;

    private ?OutputInterface $output = null;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoEchangeChorus $atexoEchangeChorus,
        private readonly LoggerInterface $chorusLogger,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Reprise de données côté Chorus pour les anciens contrats de type Accord-cadre')
            ->addArgument(
                'importFile',
                InputArgument::REQUIRED,
                'Fichier d\'import contenant les num contrat à alimenter'
            )
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation de génération de données d\'echange chorus',
                false
            )
        ;
    }

    /**
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $stopwatch = new Stopwatch();
        $stopwatch->start('exportChorusEchangeTime');

        $this->output = $output;
        $this->dryRun = $input->getOption('dry-run');

        $startTime = date('H:i:s');
        $output->writeln('******************** Début du script à ' . $startTime);
        $this->logger->info('EchangeChorusExportDataCommand commence à ' . $startTime);

        // Disable SQL logger because it can be a source of memory leaks during long-lived commands
        $connection = $this->em->getConnection();
        $connection->getConfiguration()->setSQLLogger(null);

        // get file path
        $importFile = $input->getArgument('importFile');

        // execute script
        $this->extractChorusEchangeData($importFile);

        $this->em->clear();

        // Force the run of garbage collection,
        gc_collect_cycles();

        $event = $stopwatch->stop('exportChorusEchangeTime');

        // Vérification mémoire
        $memoryUsage = $event->getMemory() / 1024 / 1024;
        $output->writeln("Script terminé avec comme consommation mémoire: ${memoryUsage}M");

        $duration = $event->getDuration();
        $output->writeln('******************** Durée du script ' . $duration . 'ms');
        $this->logger->info('EchangeChorusExportDataCommand a duré ' . $duration . 'ms');

        return self::SUCCESS;
    }

    private function extractChorusEchangeData(string $importedFilePath): void
    {
        if (!$importedFilePath || !file_exists($importedFilePath)) {
            $this->addErrorToLogAndOuput('Erreur : Le fichier ' . $importedFilePath .' n\'existe pas');
            return;
        }

        $fileInfo = pathinfo($importedFilePath);
        if ($fileInfo["extension"] === self::FILE_EXTENSION) {
            $this->addErrorToLogAndOuput('Erreur : Mauvais format de fichier, extension csv attendu');
            return;
        }

        // Récupére la liste des numero long oeap
        $numLongOeapList = $this->getListNumLongOeap($importedFilePath);

        // Récupére la liste des echanges chorus avec optimisation
        $chorusEchanges = $this->getChorusEchangeByLimitOffset($numLongOeapList);

        // Create export file
        $listFromDb = $this->createCsvFile($chorusEchanges);

        // Vérifie si les contrat sont trouvés
        $this->checkNonExistingContracts($numLongOeapList, $listFromDb);

        // Supprime le fichier si le mode dry-run n'est pas actif
        if (false === $this->dryRun) {
            $this->deleteImportedFile($importedFilePath);
        }

        // Unset in order to mark it as garbage-collectable
        unset($chorusEchanges);
    }

    private function getChorusEchangeByLimitOffset(array $numLongOeapList): array
    {
        $offset = self::OFFSET;
        $echangeChorusList = [];

        do {
            $echangeChorus = $this->em
                ->getRepository(ChorusEchange::class)
                ->getChorusEchangeByNumLongOeap($numLongOeapList, self::LIMIT_ROWS, $offset)
            ;
            $offset = self::LIMIT_ROWS + $offset;

            $echangeChorusList = array_merge($echangeChorusList, $echangeChorus);
        } while (count($echangeChorus) === self::LIMIT_ROWS);

        return $echangeChorusList;
    }

    private function getListNumLongOeap(string $csvFile): array
    {
        $line = [];

        try {
            $line = file_get_contents($csvFile);
        } catch (Exception $ex) {
            $this->addErrorToLogAndOuput('Erreur lors de l\'ouverture du fichier ');
        }

        return $line ? array_filter(array_map('trim', explode(';', $line))) : [];
    }

    public function createCsvFile(array $chorusEchanges): array
    {
        $fileExportPath =
            rtrim($this->parameterBag->get('COMMON_TMP'),'/')
            . '/'
            . 'export_chorus_echange_'. date('Y-m-d_H-i-s') . '.csv'
        ;

        $listFromDb = [];
        try {
            // open csv file for writing
            $file = fopen($fileExportPath, 'w');

            if (!$file) {
                $this->addErrorToLogAndOuput('Erreur lors de l\'ouverture du fichier pour la création du csv');
            }

            // Ajout des entêtes
            fputcsv($file, self::HEADER_LIST);

            foreach ($chorusEchanges as $echange) {
                $raisonSocialeAttributaire = $echange[self::LABEL_RAISON_SOCIALE_ATTRIBUTAIRE];
                $siretAttributaire = $this->atexoEchangeChorus->getSiretAttributaire(
                    $echange[self::LABEL_SIRET_ENTREPRISE],
                    $echange[self::LABEL_CODE_ETABLISSEMENT],
                );
                $codeApe = $this->atexoEchangeChorus->getCodeApe(trim($echange[self::LABEL_CODE_APE]));
                $formeJuridique = $echange[self::LABEL_FORME_JURIDIQUE];
                $pme = $echange[self::LABEL_PME] === ContratTitulaire::CATEGORIE_ENTREPRISE_PME ?: 0;
                $paysTerritoire = $echange[self::LABEL_PAYS_TERRITOIRE];
                $numeroNationalAttributaire = $echange[self::LABEL_NUMERO_NATIONAL_ATTRIBUTAIRE];
                $numLongOeap = $echange[self::LABEL_NUMERO_LONG_OEAP];

                $row = $this->setEchangeValues(
                    $raisonSocialeAttributaire,
                    $siretAttributaire,
                    $codeApe,
                    $formeJuridique,
                    $pme,
                    $paysTerritoire,
                    $numeroNationalAttributaire,
                    $numLongOeap
                );

                $listFromDb[] = $numLongOeap;

                // write in file
                fputcsv($file, $row);
            }

            fclose($file);

            $this->output->writeln(sprintf('Le fichier a été créé avec succès: %s', $fileExportPath));
            $this->logger->info(sprintf('EchangeChorusExportDataCommand: Fichier a été créé avec succès: %s', $fileExportPath));
        } catch (Exception $ex) {
            $this->addErrorToLogAndOuput(
                'EchangeChorusExportDataCommand: Erreur lors de la création du fichier csv, erreur '
                . $ex->getMessage()
            );
        }

        return $listFromDb;
    }

    private function checkNonExistingContracts(array $numLongOeapList, array $listFromDb): void
    {
        $nonExistingContracts = array_diff($numLongOeapList, $listFromDb);
        $warningMessage = sprintf(
            'EchangeChorusExportDataCommand : Certains contrats n\'ont pas été trouvé, voici la liste: %s',
            implode(', ', $nonExistingContracts)
        );

        if ($nonExistingContracts) {
            $this->output->writeln('-------------------------------------------------');
            $this->output->writeln($warningMessage);
            $this->output->writeln('-------------------------------------------------');
            $this->logger->warning($warningMessage);
        }
    }

    private function addErrorToLogAndOuput(string $errorMessage): void
    {
        $this->logger->error($errorMessage);
        $this->output->writeln($errorMessage);
    }

    private function deleteImportedFile(string $importedFilePath): void
    {
        try {
            unlink($importedFilePath);
        } catch (Exception $ex) {
            $this->addErrorToLogAndOuput(
                'Erreur lors de la suppression du fichier d\'import, erreur: '
                . $ex->getMessage());
        }
    }

    private function setEchangeValues(
        ?string $raisonSocialeAttributaire,
        ?string $siretAttributaire,
        ?string $codeApe,
        ?string $formeJuridique,
        ?string $pme,
        ?string $paysTerritoire,
        ?string $numeroNationalAttributaire,
        ?string $numLongOeap
    ): array {
        $this->checkDataExists($raisonSocialeAttributaire, self::LABEL_RAISON_SOCIALE_ATTRIBUTAIRE, $numLongOeap);
        $this->checkDataExists($siretAttributaire, self::LABEL_SIRET_ATTRIBUTAIRE, $numLongOeap);
        $this->checkDataExists($codeApe, self::LABEL_CODE_APE, $numLongOeap);
        $this->checkDataExists($formeJuridique, self::LABEL_FORME_JURIDIQUE, $numLongOeap);
        $this->checkDataExists($pme, self::LABEL_PME, $numLongOeap);
        $this->checkDataExists($paysTerritoire, self::LABEL_PAYS_TERRITOIRE, $numLongOeap);
        $this->checkDataExists($numeroNationalAttributaire, self::LABEL_NUMERO_NATIONAL_ATTRIBUTAIRE, $numLongOeap);

        return [
            $numLongOeap,
            $raisonSocialeAttributaire,
            $siretAttributaire,
            $codeApe,
            $formeJuridique,
            $pme,
            $paysTerritoire,
            $numeroNationalAttributaire
        ];
    }

    private function checkDataExists(?string $value, string $fieldName, string $numLongOeap): void
    {
        if (!$value) {
            $warningMessage = 'Impossible de récupérer la valeur de ';
            $warningMessage .= $fieldName;
            $warningMessage .= ' pour le contrat numéro: ';
            $warningMessage .= $numLongOeap;
            $this->logger->warning($warningMessage);
        }
    }
}
