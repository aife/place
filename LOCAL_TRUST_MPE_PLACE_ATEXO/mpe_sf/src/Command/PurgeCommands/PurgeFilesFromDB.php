<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\PurgeCommands;

use App\Entity\HistoriquePurge;
use App\Service\HistoriquePurgeService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'mpe:db-brouillon:purge',
    description: "Purge les fichiers brouillon de la base de données.",
    aliases: ['mpe:db:brouillon:purge'],
    hidden: false
)]
class PurgeFilesFromDB extends Command
{
    final public const LOGGER_CHANNEL = "cli_db_purge_files";

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $dbPurgeFilesLogger,
        private readonly ParameterBagInterface $parameterBag,
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp('Cette commande permet de purger les fichiers de la BDD déjà supprimé du NAS.')
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Mode utilisation command test/reel. Exemple: true|false (true=mode test, false=mode réel)',
                true
            )
            ->addOption(
                'limit',
                null,
                InputArgument::OPTIONAL,
                "Nombre de fichiers de fichiers à purger lors du lancement de cette commande.
                '(Exemple: 1000)
                ",
                1000
            )
            ->addOption(
                'step',
                null,
                InputArgument::OPTIONAL,
                "La définition du pas pour chaque itération pour la requête base de données",
                1000
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $io->title("Debut de la commande de purge des fichiers .brouilon de la base de données.");

            $io->note(
                sprintf(
                    "Les résultats de la commande sont disponible dans ce dossier: %s. Faire un grep du channel '%s'",
                    $this->parameterBag->get('LOG_DIR'),
                    self::LOGGER_CHANNEL,
                )
            );

            $dryRun = !in_array($input->getOption('dry-run'), [0, '0', 'false', false], true);
            $limit = $input->getOption('limit');
            $step = $input->getOption('step');

            if ($dryRun) {
                $io->warning("Commande lancé en mode TEST");
            } else {
                $io->caution("Commande lancé en mode REEL");
                $this->dbPurgeFilesLogger->info(
                    "Lancement de la commande de purge des offres chiffrés avec supression physique"
                );
            }

            $offset = 0;
            while ($offset < $limit) {
                $this->purgeBrouillonFichierEnveloppe($io, $step);
                $this->purgeBrouillonDCE($io, $step);

                $offset += $step;
            }

            return Command::SUCCESS;
        } catch (\Exception $exception) {
            $io->error(sprintf("Erreur de format de la date DLRO : %s", $exception->getMessage()));

            return Command::FAILURE;
        }
    }

    public function purgeBrouillonFichierEnveloppe(SymfonyStyle $io, int $step = null): void
    {
        $io->text('Récupération des fichiers à supprimer');

        $this->entityManager
            ->getRepository(HistoriquePurge::class)
            ->purgeBrouillonFichierEnveloppe($step);

        $io->text("purge des fichiers enveloppes");
    }

    public function purgeBrouillonDCE(SymfonyStyle $io, int $step = null): void
    {
        $io->text('Récupération des fichiers à supprimer');

        $this->entityManager
            ->getRepository(HistoriquePurge::class)
            ->purgeBrouillonDCE($step);

        $io->text("purge des dce");
    }
}
