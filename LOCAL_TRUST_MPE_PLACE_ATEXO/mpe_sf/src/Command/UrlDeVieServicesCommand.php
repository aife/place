<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UrlDeVieServicesCommand extends Command
{
    protected static $defaultName = 'mpe:url-de-vie:services';
    protected $fileName;
    protected $limit;
    protected int $offset = 0;
    protected $validator;
    protected ?object $doctrine = null;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ContainerInterface $container
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $helpMessage = 'Le nom du service tel que: SuperviseurNas, SuperviseurDisk, SuperviseurInterfaces, 
        SuperviseurOpenOffice, SuperviseurPhp, SuperviseurDume, SuperviseurContrats, SuperviseurMol';
        $this->setDescription('Commande qui permet de récupérer les résultats des services 
        suivants:' . PHP_EOL . ' Nas, Disk, interfaces, OpenOffice, PHP, Contrats')
            ->addArgument('service', InputArgument::OPTIONAL, $helpMessage)
            ->setHelp($helpMessage);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $service = $input->getArgument('service');
        if (!empty($service)) {
            $className = 'App\Service\Superviseur\\' . $service;
            $object = new ReflectionClass($className);
            $serviceObject = $object->newInstanceArgs([$this->container, $this->em]);
            $result = $serviceObject->getSpecificStatuses();
            $output->writeln(json_encode($result, JSON_THROW_ON_ERROR));
        }
        return 0;
    }
}
