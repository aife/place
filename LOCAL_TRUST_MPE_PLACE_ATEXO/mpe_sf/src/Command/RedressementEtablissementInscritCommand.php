<?php

namespace App\Command;

use App\Entity\Etablissement;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RedressementEtablissementInscritCommand extends Command
{
    protected static $defaultName = 'redressement:etablissement:inscrit';
    private $em;
    private $logger;
    private const LIMIT = 100;
    private const UPDATE = 'Update inscrit with id';

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Redressement des etablissements pour les inscrits qui ont 
            un id_etablissement egal 0');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $offset = 0;
            $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
            do {
                $listInscrits = $this->em->getRepository(Inscrit::class)->findBy(
                    ['idEtablissement' => 0],
                    null,
                    self::LIMIT,
                    $offset
                );

                $offset += self::LIMIT;
                $this->update($listInscrits, $output);

                $this->em->flush();
                $this->em->clear();
            } while (count($listInscrits) == self::LIMIT);
        } catch (\Exception $exception) {
            $this->logger->error('Command stopped with error.', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return $exception->getCode();
        }

        return 0;
    }

    private function update(array $listInscrits, OutputInterface $output): void
    {
        foreach ($listInscrits as $inscrit) {
            if ($inscrit->getSiret()) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->findOneBy([
                        'codeEtablissement' => $inscrit->getSiret(),
                        'idEntreprise' => $inscrit->getEntrepriseId()
                    ]);
                if (!$etablissement) {
                    $output->writeln(
                        sprintf('creating new Etablissement with SIRET : %s', $inscrit->getSiret())
                    );
                    $this->logger->info(
                        sprintf('creating new Etablissement with SIRET : %s', $inscrit->getSiret())
                    );
                    $etablissement = $this->createEtablissementFromInscrit($inscrit);
                }
                $inscrit->setEtablissement($etablissement);
                $output->writeln(sprintf(self::UPDATE . ' : %s', $inscrit->getId()));
                $this->logger->info(sprintf(self::UPDATE . ' : %s', $inscrit->getId()));
            } elseif (
                $inscrit->getEntreprise()->getSirenetranger()
                && 'france' !== strtolower($inscrit->getEntreprise()->getPays())
            ) {
                if (
                    is_countable(
                        $inscrit->getEntreprise()->getEtablissements()
                    )
                    && count($inscrit->getEntreprise()->getEtablissements())
                ) {
                    $inscrit->setEtablissement($inscrit->getEntreprise()->getEtablissements()[0]);
                } else {
                    $etablissement = $this->createEtablissementFromInscrit($inscrit);
                    $output->writeln(
                        sprintf(
                            'creating new foreign Etablissement with SIRET : %s',
                            $etablissement->getCodeEtablissement()
                        )
                    );
                    $this->logger->info(
                        sprintf(
                            'creating new foreign Etablissement with SIRET : %s',
                            $etablissement->getCodeEtablissement()
                        )
                    );
                    $inscrit->setEtablissement($etablissement);
                }
                $output->writeln(sprintf(self::UPDATE . ' : %s', $inscrit->getId()));
                $this->logger->info(sprintf(self::UPDATE . ' : %s', $inscrit->getId()));
            }
        }
    }

    private function createEtablissementFromInscrit(Inscrit $inscrit): Etablissement
    {
        $etablissement = new Etablissement();
        $entreprise = $inscrit->getEntreprise();
        $etablissement->setEntreprise($entreprise)
            ->setCodeEtablissement($inscrit->getSiret() ?? '00001')
            ->setEstSiege('0')
            ->setAdresse($inscrit->getAdresse() ?? $entreprise->getAdresse())
            ->setAdresse2($inscrit->getAdresse2() ?? $entreprise->getAdresse2())
            ->setCodePostal($inscrit->getCodepostal() ?? $entreprise->getCodepostal())
            ->setVille($inscrit->getVille())
            ->setPays($inscrit->getPays())
            ->setSaisieManuelle('1')
            ->setIdInitial(0)
            ->setDateCreation(new \DateTime())
            ->setDateModification(new \DateTime())
            ->setStatutActif('1')
            ->setInscritAnnuaireDefense('0')
            ->setIdExterne(0);

        $this->em->persist($etablissement);

        return $etablissement;
    }
}
