<?php

namespace App\Command;

use ReflectionMethod;
use Application\Service\Atexo\Controller\Atexo_Controller_Prado;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\PdoStore;
use Symfony\Component\Stopwatch\Stopwatch;

class MpePradoCliCommand extends Command
{
    protected static $defaultName = 'mpe:prado:cli';
    protected static $defaultDescription = 'Commande servant à lancer des commandes Prado';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $cliLogger
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('command-prado', InputArgument::REQUIRED, 'Nom de la commande Prado')
            ->addArgument(
                'command-prado-args',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'Arguments de la commande Prado'
            )
            ->addOption(
                'no-lock',
                null,
                InputOption::VALUE_NONE,
                'Désactiver le verrouillage lors de l\'exécution de la commande'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $stopwatch = new Stopwatch();
        $commandName = $input->getArgument('command-prado');
        $args = $input->getArgument('command-prado-args');
        $noLock = $input->getOption('no-lock');

        $_SERVER['argc']--;
        array_shift($_SERVER['argv']);

        $this->setCliEnvironment();

        $io->note(sprintf('Lancement de la commande %s %s', $commandName, implode(' ', $args)));
        $this->cliLogger->info(sprintf('Lancement de la commande %s %s', $commandName, implode(' ', $args)));

        $stopwatch->start($commandName);
        $this->launch($commandName, $io, empty($noLock));
        $event = $stopwatch->stop($commandName);

        $io->note(sprintf('Fin de la commande %s en %d ms. Mémoire utilisée %.2F MB', $commandName, $event->getDuration(), $event->getMemory() / 1024 / 1024));
        $this->cliLogger->info(sprintf('Fin de la commande %s en %s', $commandName, (string)$event));

        return 0;
    }

    private function setCliEnvironment()
    {
        error_reporting(E_ERROR | E_PARSE);

        Atexo_Controller_Prado::runCli(realpath(__DIR__ . '/../../legacy/protected/application-cli.xml'));
    }

    private function launch(string $commandName, SymfonyStyle $io, bool $lockCommand = true): void
    {
        if ($lockCommand) {
            $store = new PdoStore($this->entityManager->getConnection());
            $factory = new LockFactory($store);
            $lock = $factory->createLock($commandName);
        }

        if (!$lockCommand || $lock->acquire()) {
            $className = 'Application\Cli\Cli_' . $commandName;
            $cliFile = realpath(__DIR__ . '/../../legacy/protected/Cli/' . $commandName . '.php');

            include $cliFile;
            $class = new ReflectionMethod($className, 'run');
            $class->invoke(null);
        } else {
            $io->error('Commande vérouillée');
            $this->cliLogger->error('Commande vérouillée');
        }
    }
}
