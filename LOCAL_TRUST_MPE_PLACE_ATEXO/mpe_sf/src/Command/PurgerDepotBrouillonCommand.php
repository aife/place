<?php
/**
 * Created by PhpStorm.
 * User: Aso
 * Date: 23/12/2016
 * Time: 17:37
 */

namespace App\Command;

use Exception;
use App\Entity\Offre;
use App\Service\AtexoFichierOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class PurgerDepotBrouillonCommand
 * @package App\Command
 */
class PurgerDepotBrouillonCommand extends Command
{
    protected static $defaultName = 'offre:sup-brouillon';

    protected function configure()
    {
        $this->setDescription('Supprimer les dépots en etat brouillon');
    }

    /**
     * GenerationDocumentRecuperationCommand constructor.
     */
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $output->writeln('Start deleting');
            $status = (int)$this->parameterBag->get('STATUT_ENV_BROUILLON');
            $dureeVieDepotBrouillon = $this->parameterBag->get('duree_vie_depot_brouillon');
            $filesToDelete = $this->em
                ->getRepository(Offre::class)
                ->purge($status, $dureeVieDepotBrouillon);
            $this->deleteFile($filesToDelete);
            $output->writeln('End deleting');
        } catch (Exception $e) {
            $erreur = "Erreur PurgerDepotBrouillonCommand => ".$e->getMessage();
            $this->logger->error($erreur);
            return (int) $e->getCode();
        }

        return 0;
    }

    /**
     * Delete physical files (physical is relative)
     *
     */
    protected function deleteFile(array $filesToDelete)
    {
        foreach ($filesToDelete as $file) {
            $this->atexoFichierOrganisme
                ->deleteBlobFile($file['id_blob'], $file['organisme']);
        }
    }
}
