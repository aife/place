<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use DateTime;
use App\Entity\Api\SupervisionInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EchangesInterfacesCommand.
 */
class StatistiquesCommand extends Command
{
    protected static $defaultName = 'echanges_interfaces';
    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription('Supprime les données datant de plus de 15 jours en base de données ')
            ->addArgument(
                'date',
                InputArgument::OPTIONAL,
                'date uniquement pour le redressement de données (table supervision interface vide pour une date)'.
                'Format de la date (exemple : 2018-10-16)'
            )
        ;
    }

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    /**
     * Permet d'executer la commande console.
     *
     * @return int|void|null
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;
        $date = $input->getArgument('date');

        if (isset($date)) {
            $dateMin = new DateTime($date);
            $dateMax = new DateTime($date);
        } else {
            $dateMin = new DateTime();
            $dateMax = new DateTime();
        }

        $dateMin->modify('-1 days');
        $dateMin->setTime('00', '00', '00');
        $dateMax->setTime('00', '00', '00');

        $this->removeSupervisionInterfaces($em);
        return 0;
    }

    /**
     * @param $em
     * @param int $days
     *
     * @throws Exception
     */
    public function removeSupervisionInterfaces($em, $days = 15)
    {
        $date = new DateTime();
        $date->modify("-$days days");

        $supervisionRepository = $em->getRepository(SupervisionInterface::class);
        $supervisionsToBeRemoved = $supervisionRepository->getSupervisionInterfaceByDate($date);

        foreach ($supervisionsToBeRemoved as $item) {
            $em->remove($item);
        }

        $em->flush();
    }
}
