<?php

namespace App\Command\Migration;

use App\Entity\Consultation;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class MigrationReferenceConsultationInit.
 */
class MigrationReferenceConsultationInitCommand extends Command
{
    protected static $defaultName = 'mpe:migration:referenceConsultationInit';
    protected ?OutputInterface $output = null;
    protected $tab;

    public function __construct(private EntityManagerInterface $entityManager, private LoggerInterface $logger, private ParameterBagInterface $parameterBag)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Commande qui permet de migrer ReferenceConsultationInit à lancer une seule fois
             si non risque de perte de données')
            ->addArgument(
                'memory',
                InputArgument::OPTIONAL,
                'Affiche la mémoire consommée'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $memoryArg = $input->getArgument('memory');
        if ($memoryArg) {
            $output->writeln(sprintf(
                'Memory usage (currently) %dkB/ (max) %dkB',
                round(memory_get_usage(true) / 1024),
                memory_get_peak_usage(true) / 1024
            ));
        }

        $this->output = $output;
        $this->logger->info("Début de l'execution de la commande mpe:migration:referenceConsultationInit");
        $this->output->setDecorated(true);
        $data = $this->getData();
        $logs = "id consultation|referenceUtilisateur|referenceConsultationInit|id consultation Initiale|organisme\n\r";
        foreach ($data as $d) {
            $logs .= $d['idC1'].'|'.$d['referenceUtilisateurC1'].'|'.$d['referenceConsultationInitC1'].'|'.$d['idC2'].'|'.$d['organisme'];
            $consultationIniti = $this->entityManager->getRepository(Consultation::class)->find($d['idC2']);
            if ($consultationIniti instanceof Consultation && $consultationIniti->getOrganisme() == $d['organisme']) {
                $logs .= "|consultation Initiale trouvée\n\r";
                $qb = $this->entityManager->getRepository(Consultation::class)->createQueryBuilder('')
                    ->update(Consultation::class, 'c')
                    ->set('c.referenceConsultationInit', ':idC2')
                    ->Where('c.id =:idC1')
                    ->setParameter('idC2', $d['idC2'])
                    ->setParameter('idC1', $d['idC1'])
                    ->getQuery();
                $qb->execute();
            } else {
                $logs .= "|consultation Initiale PAS trouvée\n\r";
            }
        }
        if ($logs) {
            $fichierLog = $this->parameterBag->get('LOG_DIR').'/migrationReferenceConsultationInit_'.date('Ydm_His').'.log';
            file_put_contents($fichierLog, $logs);
            $output->writeln(sprintf(' **** VOIR FICHIER '.$fichierLog.' ****'));
        }
        if ($memoryArg) {
            $output->writeln(sprintf(
                'Memory usage (currently) %dkB/ (max) %dkB',
                round(memory_get_usage(true) / 1024),
                memory_get_peak_usage(true) / 1024
            ));
        }

        $this->logger->info("\nFin de l'execution de la commande mpe:migration:referenceConsultationInit ");
        return 0;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        $qb = $this->entityManager->getRepository(Consultation::class)->createQueryBuilder('c1')
            ->select('c1.acronymeOrg as organisme,c1.id as idC1,c1.reference as referenceC1 ,c1.referenceUtilisateur as referenceUtilisateurC1,
            c1.referenceConsultationInit as referenceConsultationInitC1 ,c2.id as idC2,c2.reference as referenceC2,
            c2.referenceUtilisateur as referenceUtilisateurC2')
            ->join(
                Consultation::class,
                'c2',
                'WITH',
                'c1.referenceConsultationInit=c2.reference and c1.organisme=c2.organisme'
            )
            ->where('c1.reference IS NOT NULL')
            ->andWhere('c1.reference!=0')
            ->andWhere('c1.idEtatConsultation not in (6,5)');

        return $qb->getQuery()->getResult();
    }
}
