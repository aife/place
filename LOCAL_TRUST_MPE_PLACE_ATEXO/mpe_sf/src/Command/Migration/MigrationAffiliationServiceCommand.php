<?php

namespace App\Command\Migration;

use Exception;
use App\Service\MigrationEntiteAchat\MigrationAffiliationServiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationAffiliationServiceCommand extends Command
{
    protected static $defaultName = 'mpe:migration:services:affiliation';
    use LockableTrait;

    public function __construct(
        private readonly MigrationAffiliationServiceService $migrationAffiliationService
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Migrer les AffiliationService des entités associées aux services d\'un organisme à un autre')
            ->addOption('cheminFichierServices', 'f', InputOption::VALUE_REQUIRED, 'chemin du fichier listant les identifiants uniques des services sources et destination')
            ->addOption('acronymeOrganismeSource', 's', InputOption::VALUE_REQUIRED, 'acronyme de l\'organisme source des services')
            ->addOption('acronymeOrganismeDestination', 'd', InputOption::VALUE_REQUIRED, 'acronyme de l\'organisme de destination des services')
            ->addOption('modeReel', 'r', InputOption::VALUE_OPTIONAL, 'si ce paramètre est passé en option quelque soit sa valeur, alors la migration s\'effectue en réél');
    }

    /**
     * @return int|void|null
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln("Cette commande s'exécute déjà dans un autre processus.");

            return 0;
        }

        $cheminFichierServices = $input->getOption('cheminFichierServices');

        $acronymeOrganismeSource = $input->getOption('acronymeOrganismeSource');
        $acronymeOrganismeDestination = $input->getOption('acronymeOrganismeDestination');
        $modeTest = (((bool) $input->getOption('modeReel')) == 1) ? false : true;
        $time_start = microtime(true);

        $output->writeln('Démarrage de la migration des AffiliationServices....');

        $res = $this->migrationAffiliationService->migrate($cheminFichierServices, $acronymeOrganismeSource, $acronymeOrganismeDestination, $modeTest);

        if ($res) {
            $output->writeln('L\'opération de migration des AffiliationServices a été réalisée avec succès. Les logs donnent tous les détails, veuillez les consulter pour plus d\'informations');
        } else {
            $output->writeln('L\'opération de migration n\'a pas été réalisée car une erreur s\'est produite. Veuillez consulter les logs pour plus de détail');
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $output->writeln('Total Execution Time: '.ceil($execution_time).'s');
        $this->release();

        return 0;
    }
}
