<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Migration;

use App\Doctrine\DatabaseToMigrate;
use App\Entity\Service;
use App\Service\Migration\AgentMigrationService;
use App\Service\Migration\CategorieINSEEMigrationService;
use App\Service\Migration\CertificatPermanentMigrationService;
use App\Service\Migration\CompteMoniteurMigrationService;
use App\Service\Migration\GroupeMoniteurMigrationService;
use App\Service\Migration\MigrationTableMappingService;
use App\Service\Migration\OrganismeMigrationService;
use App\Service\Migration\ProcedureEquivalenceMigrationService;
use App\Service\Migration\ServiceMigrationService;
use App\Service\Migration\TypeContratEtTypeProcedureMigrationService;
use App\Service\Migration\TypeProcedureOrganismeMigrationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class MigrationPlateformeCommand extends Command
{
    protected static $defaultName = 'mpe:migration:plateforme';

    protected SymfonyStyle $io;

    protected int $batchSize = 200;

    /**
     * 'Resource' => [
     *      'name' => 'Nom',
     *      'serviceMigrationServiceName' => 'variableMigrationService', // Nom du constructeur
     *      'attributesDetail' => [
     *          'attribut1',
     *          'attribut2',
     *      ],
     *      'identifierAttribute' => 'id',
     * ],
     */
    protected array $mainMigrationServices = [
        'Organisme' => [
            'name' => 'Organisme',
            'serviceMigrationServiceName' => 'organismeMigrationService',
            'attributesDetail' => [
                'acronyme',
                'denominationOrg',
            ],
            'identifierAttribute' => 'acronyme',
        ],
        'Service' => [
            'name' => 'Service',
            'serviceMigrationServiceName' => 'serviceMigrationService',
            'attributesDetail' => [
                'id',
                'libelle',
            ],
            'identifierAttribute' => 'id',
        ],
        'Agent' => [
            'name' => 'Agent',
            'serviceMigrationServiceName' => 'agentMigrationService',
            'attributesDetail' => [
                'id',
                'login',
            ],
            'identifierAttribute' => 'login',
        ],
        'CertificatPermanent' => [
            'name' => 'CertificatPermanent',
            'serviceMigrationServiceName' => 'certificatPermanentMigrationService',
            'attributesDetail' => [
                'id',
                'organisme',
                'titre',
            ],
            'identifierAttribute' => [
                'id',
                'organisme',
            ],
        ],
        'GroupeMoniteur' => [
            'name' => 'GroupeMoniteur',
            'serviceMigrationServiceName' => 'groupeMoniteurMigrationService',
            'attributesDetail' => [
                'id',
                'organisme',
            ],
            'identifierAttribute' => [
                'id',
                'organisme',
            ],
        ],
        'CompteMoniteur' => [
            'name' => 'CompteMoniteur',
            'serviceMigrationServiceName' => 'compteMoniteurMigrationService',
            'attributesDetail' => [
                'id',
                'organisme',
            ],
            'identifierAttribute' => [
                'id',
                'organisme',
            ],
        ],
        'TypeProcedureOrganisme' => [
            'name' => 'TypeProcedureOrganisme',
            'serviceMigrationServiceName' => 'typeProcedureOrganismeMigrationService',
            'attributesDetail' => [
                'idTypeProcedure',
                'organisme',
                'libelleTypeProcedure',
            ],
            'identifierAttribute' => [
                'idTypeProcedure',
                'organisme',
            ],
        ],
        'ProcedureEquivalence' => [
            'name' => 'ProcedureEquivalence',
            'serviceMigrationServiceName' => 'procedureEquivalenceMigrationService',
            'attributesDetail' => [
                'idTypeProcedure',
                'organisme',
            ],
            'identifierAttribute' => [
                'idTypeProcedure',
                'organisme',
            ],
        ],
        'TypeContratEtTypeProcedure' => [
            'name' => 'TypeContratEtTypeProcedure',
            'serviceMigrationServiceName' => 'contratEtTypeProcedureMigrationService',
            'attributesDetail' => [
                'idTypeContratEtProcedure',
                'organisme',
            ],
            'identifierAttribute' => 'idTypeContratEtProcedure',
        ],
    ];

    protected array $migrationServiceMapped = [
        OrganismeMigrationService::class,
        ServiceMigrationService::class,
        CategorieINSEEMigrationService::class,
        AgentMigrationService::class,
        CertificatPermanentMigrationService::class,
        TypeProcedureOrganismeMigrationService::class,
        ProcedureEquivalenceMigrationService::class,
        GroupeMoniteurMigrationService::class,
        CompteMoniteurMigrationService::class,
        TypeContratEtTypeProcedureMigrationService::class,
    ];

    protected function configure(): void
    {
        $this->setDescription('Permet de migrer les données essentielles d\'une plateforme vers une autre');
    }

    public function __construct(
        protected EntityManagerInterface $em,
        protected DatabaseToMigrate $databaseToMigrate,
        protected MigrationTableMappingService $tableMappingService,
        protected Stopwatch $stopwatch,
        protected PropertyAccessorInterface $propertyAccessor,
        protected OrganismeMigrationService $organismeMigrationService,
        protected ServiceMigrationService $serviceMigrationService,
        protected AgentMigrationService $agentMigrationService,
        protected CertificatPermanentMigrationService $certificatPermanentMigrationService,
        protected TypeProcedureOrganismeMigrationService $typeProcedureOrganismeMigrationService,
        protected ProcedureEquivalenceMigrationService $procedureEquivalenceMigrationService,
        protected TypeContratEtTypeProcedureMigrationService $contratEtTypeProcedureMigrationService,
        protected GroupeMoniteurMigrationService $groupeMoniteurMigrationService,
        protected CompteMoniteurMigrationService $compteMoniteurMigrationService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->stopwatch->start('migrate');
        $startTime = date('H:i:s');
        $output->writeln('Démarré à ' . $startTime);

        $this->initMigrationMappingTable();

        $this->organismeMigrationService->addConfiguredMapping();
        $this->agentMigrationService->addExistantMapping();

        foreach ($this->mainMigrationServices as $migrationService) {
            $this->migrationResource($migrationService);
        }

        $endExecutionTime = date('H:i:s');
        $output->writeln('Terminé à ' . $endExecutionTime);

        $this->displayTimeAndMemoryUsed();

        return 0;
    }

    protected function migrationResource($resourceDetail)
    {
        $resourceName = $resourceDetail['name'];
        $serviceMigrationServiceName = $resourceDetail['serviceMigrationServiceName'];
        $attributesDetail = $resourceDetail['attributesDetail'];
        $identifierAttribute = $resourceDetail['identifierAttribute'];

        $page = 0;

        $count = $this->$serviceMigrationServiceName->getResourceCount();
        $countDone = 0;
        $this->io->info(sprintf('Il y a %s %s(s) à migrer', $count, $resourceName));

        if (!$count) {
            return;
        }

        $healthChecks = $this->$serviceMigrationServiceName->healthCheck();
        $identifiersToIgnore = [];

        if (!empty($healthChecks)) {
            $this->io->newLine(3);
            $this->io->block('Health check');
            $this->io->title(sprintf('Health check (%s)', count($healthChecks)));
            $reports = $healthChecks['reports'];
            $identifiersToIgnore = $healthChecks['identifiersToIgnore'];
            foreach ($reports as $report) {
                $this->io->warning($report);
            }
        } else {
            $this->io->info(sprintf('Les données de %s sont saines', $resourceName));
        }

        do {
            $resources = $this->$serviceMigrationServiceName->extract(null, $this->batchSize, $identifiersToIgnore);
            $nbResources = count($resources);

            if ($nbResources) {
                $this->io->newLine(3);

                $table = $this->io->createTable();
                $table
                    ->setHeaderTitle(sprintf('%s %s(s) en cours de migration', $nbResources, $resourceName))
                    ->setHeaders($attributesDetail);

                /** @var Service $resource */
                foreach ($resources as $resource) {
                    $row = [];

                    foreach ($attributesDetail as $attribute) {
                        $row[] = $this->propertyAccessor->getValue($resource, $attribute);
                    }

                    $table->addRow($row);
                }

                $table->render();
            }

            $this->io->title(sprintf('Début de la migration des %s', $resourceName));

            foreach ($resources as $resource) {
                $resource = $this->$serviceMigrationServiceName->migrate($resource);
                $countDone++;
                if (is_array($identifierAttribute)) {
                    $identifier = [];

                    foreach ($identifierAttribute as $partialIdentifier) {
                        $identifier[] = $this->propertyAccessor->getValue($resource, $partialIdentifier);
                    }

                    $identifier = implode(';', $identifier);

                    $this->io->success(
                        sprintf(
                            '%s %s %s migré %s/%s',
                            $resourceName,
                            implode(';', $identifierAttribute),
                            $identifier,
                            $countDone,
                            $count
                        )
                    );
                } else {
                    $identifier = $this->propertyAccessor->getValue($resource, $identifierAttribute);
                    $this->io->success(
                        sprintf(
                            '%s %s %s migré %s/%s',
                            $resourceName,
                            $identifierAttribute,
                            $identifier,
                            $countDone,
                            $count
                        )
                    );
                }

                unset($resource);
            }
            unset($resources);
            $this->em->clear();
            $this->databaseToMigrate->clear();
            $page++;
        } while ($page <= floor($count / $this->batchSize));
        unset($healthChecks);
    }

    private function initMigrationMappingTable()
    {
        foreach ($this->migrationServiceMapped as $migrationService) {
            $tableName = $migrationService::MIGRATION_TABLE_NAME;
            $tableExists = $this->tableMappingService->verify($tableName);

            if (!$tableExists) {
                $this->io->info(sprintf('Table de mapping %s créée !', $tableName));
            }
        }
    }

    protected function displayTimeAndMemoryUsed(): void
    {
        $this->io->newLine(2);
        $this->io->success('Les données ont été importées.');
        $this->stopwatch->stop('migrate');
        $event = $this->stopwatch->getEvent('migrate');

        $durationMS = (int)($event->getDuration());
        list($seconds, $millis) = explode('.', $durationMS / 1000);

        $this->io->success(
            sprintf(
                "Transfert de donnée effectué en %d:%d:%d.%d\n%.2f Gb utilisé en pic d'usage.",
                ($seconds / 60) / 60,
                ($seconds / 60) % 60,
                $seconds % 60,
                $millis,
                $event->getMemory() * 0.000000007451
            )
        );
    }
}
