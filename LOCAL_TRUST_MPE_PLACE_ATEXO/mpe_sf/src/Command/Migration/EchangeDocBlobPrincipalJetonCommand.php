<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Migration;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EchangeDocBlobPrincipalJetonCommand extends Command
{
    protected static $defaultName = 'mpe:echange-doc-blob:init-parent-jeton-signature';
    private const LIMIT_ROWS = 1000;

    private const FIRST_ID = 0;

    protected function configure(): void
    {
        $this->setDescription('Ajout de relation parents enfants pour les documents de jetons de signature')
        ;
    }

    public function __construct(protected EntityManagerInterface $em)
    {
        parent::__construct();
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startTime = date('H:i:s');
        $output->writeln('Started at ' . $startTime);

        // Disable SQL logger because it can be a source of memory leaks during long-lived commands
        $connection = $this->em->getConnection();
        $connection->getConfiguration()->setSQLLogger(null);

        $limit = self::LIMIT_ROWS;
        $minimumId = self::FIRST_ID;

        do {
            [$batchCount, $minimumId] = $this->runScriptAddJetonToPrincipalByBatch($output, $limit, $minimumId);
        } while ($batchCount === $limit);

        $endExecutionTime = date('H:i:s');
        $output->writeln('Ended at ' . $endExecutionTime);
        return 0;
    }

    private function runScriptAddJetonToPrincipalByBatch(OutputInterface $output, int $limit, int $minimumId): array
    {
        $echangeDocs = $this->em->getRepository(EchangeDoc::class)->findBy(
            [],
            ['id' => 'ASC'],
            $limit,
            $minimumId
        );
        $nbEchangeDocs = count($echangeDocs);

        if ($nbEchangeDocs > 0) {
            $minimumId = $echangeDocs[$nbEchangeDocs - 1]->getId();

            /** @var EchangeDoc $echangeDoc */
            foreach ($echangeDocs as $echangeDoc) {
                if ($echangeDocBlobs = $echangeDoc->getEchangeDocBlobs()) {
                    $blobPrincipal = null;
                    /** @var EchangeDocBlob $echangeDocBlob */
                    foreach ($echangeDocBlobs as $echangeDocBlob) {
                        if ($echangeDocBlob->getCategoriePiece() === EchangeDocBlob::FICHIER_PRINCIPAL) {
                            $blobPrincipal = $echangeDocBlob;
                        }

                        if (
                            $blobPrincipal
                            && $echangeDocBlob->getCategoriePiece() === EchangeDocBlob::FICHIER_JETON
                            && !$echangeDocBlob->getDocPrincipal()
                        ) {
                            $echangeDocBlob->setDocPrincipal($blobPrincipal);
                            $this->em->persist($echangeDocBlob);
                        }
                    }
                }
                unset($echangeDoc);
            }

            // Check memory usage during fetch of jeton signature
            $memoryUsage = memory_get_usage(true) / 1024 / 1024;
            $output->writeln("Batch finished with memory: ${memoryUsage}M");

            // Unset in order to mark it as garbage-collectable
            unset($echangeDocs);

            $this->em->flush();
            $this->em->clear();

            // Force the run of garbage collection,
            gc_collect_cycles();
        }

        return [$nbEchangeDocs, $minimumId];
    }
}
