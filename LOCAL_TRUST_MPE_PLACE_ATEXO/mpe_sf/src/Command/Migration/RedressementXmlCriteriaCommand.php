<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Migration;

use Exception;
use __PHP_Incomplete_Class;
use App\Entity\TMesRecherches;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;

/**
 * Class RedressementXmlCriteriaCommand
 * @package App\Command\Migration
 */
class RedressementXmlCriteriaCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'mpe:redressement-xml-criteria';

    private const LIMIT = 100;

    /**
     * GenerationDocumentRecuperationCommand constructor.
     */
    public function __construct(
        private readonly LoggerInterface $alertesentrepriseLogger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em
    ) {
        spl_autoload_register(RedressementXmlCriteriaCommand::class . '::extendClass');

        parent::__construct();
    }

    /**
     * configurer la commande.
     */
    protected function configure()
    {
        $this
            ->setDescription('Commande qui permet de redresser les donnée de la table t_MesRecherches');
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $memoryUsage = null;
        $data = null;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->alertesentrepriseLogger->info('Alert-email: Début du redressement');
        $io = new SymfonyStyle($input, $output);
        $io->comment('Début du redressement');
        $offset = 0;
        $counter = 0;

        do {
            try {
                $data = $this->em->getRepository(TMesRecherches::class)->findAllWithLimit($offset, self::LIMIT);

                foreach ($data as $recherche) {
                    $this->traitementRecherche($input, $io, $this->em, $recherche);
                    $memoryUsage = memory_get_usage(true) / 1024 / 1024;

                    if (++$counter % 100 == 0) {
                        $msg = "\nMémoire utilisée : ${memoryUsage}M";
                        $msg .= ' - recherches traitées : ' . $counter;
                        $io->text($msg);
                        $io->text('Identifiant de la dernière recherche traitée : ' . $recherche->getId());

                        $this->em->flush(); // save unsaved changes
                        $this->em->clear(); // clear doctrine managed entities
                        gc_collect_cycles(); // PHP garbage collect
                    }
                }

                $this->em->flush(); // Executes all updates.
                $this->em->clear(); // Detaches all objects from Doctrine!
                gc_collect_cycles();

                $io->text("Mémoire utilisée : ${memoryUsage}M");
                $io->text('Recherches traitées : ' . $counter);
            } catch (Exception $exception) {
                $io->write('Erreur :' . $exception);
            }
        } while ((is_countable($data) ? count($data) : 0) == self::LIMIT);


        $io->comment('Fin du redressement');
        $this->alertesentrepriseLogger->info('Alert-email: Fin du redressement');

        return 0;
    }


    private function traitementRecherche(
        InputInterface $input,
        SymfonyStyle $io,
        EntityManager $em,
        TMesRecherches $objetRecherche
    ): void {
        if ('2018-07-19' <= $objetRecherche->getDateModification()) {
            $serializedObject = $objetRecherche->getxmlCriteria();
        } else {
            $serializedObject = Atexo_Util::utf8ToIso($objetRecherche->getxmlCriteria());
        }
        $class = Atexo_Consultation_CriteriaVo::class;
        try {
            $serializedObject = preg_replace_callback(
                '!s:\d+:"(.*?)";!s',
                fn($m) => 's:' . strlen($m[1]) . ':"' . $m[1] . '";',
                $serializedObject
            );
            $criteriaObject = unserialize($serializedObject, ['allowed_classes' => true]);
            $criteriaObject = self::castStdClass($class, $criteriaObject);
            $serialize = serialize($criteriaObject);
            $objetRecherche->setXmlCriteria($serialize);
            $objetRecherche->setDateModification(new \DateTime());
            $em->persist($objetRecherche);
            //}
        } catch (Exception $exception) {
            $this->alertesentrepriseLogger->error(sprintf('Alert-email: %s', $exception->getMessage()));
        }
    }

    private function castStdClass($destination, $stdObject)
    {
        if (is_string($destination)) {
            $destination = new $destination();
        }
        if (Atexo_Consultation_CriteriaVo::class == $stdObject::class) {
            $class = Atexo_Referentiel_ReferentielVo::class;
            $arrayObjet = $stdObject->getReferentielVo();
            $newArrayObjet = array();
            foreach ($arrayObjet as $objet) {
                if (__PHP_Incomplete_Class::class == $objet::class) {
                    $newArrayObjet[] = self::castStdClass($class, $objet);
                } else {
                    $newArrayObjet[] = $objet;
                }
            }
            $stdObject->setReferentielVo($newArrayObjet);
            $destination = $stdObject;
        } else {
            foreach ($stdObject as $property => &$value) {
                $set = 'set' . ucfirst(ltrim($property, '_'));
                if (method_exists($destination, $set)) {
                    if (is_array($value)) {
                        foreach ($value as $objet) {
                            if (is_object($objet)) {
                                $class = Atexo_Referentiel_ReferentielVo::class;
                                self::castStdClass($class, $objet);
                            }
                        }
                    } else {
                        $destination->$set($value);
                    }
                }
            }
        }
        return $destination;
    }

    public static function extendClass(string $classname): void
    {
        if ('Atexo_Consultation_CriteriaVo' === $classname) {  // just create a class that has some nice accessors to it
            eval("class $classname extends stdClass {}");
        }
    }
}
