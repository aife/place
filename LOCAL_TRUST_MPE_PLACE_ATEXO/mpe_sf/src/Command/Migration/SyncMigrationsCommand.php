<?php

namespace App\Command\Migration;

use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class SyncMigrationsCommand extends Command
{
    protected static $defaultName = 'mpe:migration:sync-migrations';
    protected ?SymfonyStyle $io = null;

    /**
     * SyncMigrationsCommand constructor.
     *
     * @param null $name
     */
    public function __construct(protected EntityManagerInterface $em, $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setDescription('Ensures that the metadata storage and versions name are synchronised with the latest version.')
            ->setHelp(
                <<<EOT
The <info>%command.name%</info> command updates metadata storage and versions name the latest version.

    <info>%command.full_name%</info>
EOT
            );
    }

    public function execute(
        InputInterface $input,
        OutputInterface $output
    ): int {
        $this->io = new SymfonyStyle($input, $output);
        $command = $this->getApplication()->find('doctrine:migrations:sync-metadata-storage');
        $greetInput = new ArrayInput([]);
        $returnCode = $command->run($greetInput, $output);
        if (0 == $returnCode) {
            try {
                $result = $this->updateVersionName();
                if ($result > 0) {
                    $this->io->success('Versions name synchronized : '.$result.' affected row'.($result > 1 ? 's' : ''));
                } else {
                    $this->io->success('Versions name are synchronized');
                }
            } catch (Exception $e) {
                $this->io->error('Versions name synchronization failed');
                $this->io->text($e->getMessage());
                $returnCode = 1;
            }
        }

        return $returnCode;
    }

    /**
     * @throws Exception
     */
    private function updateVersionName(): int
    {
        $sql = "UPDATE migration_manager SET version = CONCAT('App\\\Migrations\\\Version', version) WHERE version NOT LIKE 'App%';";

        return $this->em->getConnection()->executeStatement($sql);
    }
}
