<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Migration;

use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EchangeDocBlobPrincipalAnnexesCommand extends Command
{
    protected static $defaultName = 'mpe:echange-doc-blob:init-parent-annexes';
    protected function configure(): void
    {
        $this->setDescription('Ajout de relation parents enfants pour les documents principaux')
        ;
    }

    public function __construct(protected EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $startTime = date('H:i:s');
        $output->writeln('Started at ' . $startTime);

        $echangeDocs = $this->em->getRepository(EchangeDoc::class)->findAll();

        /** @var EchangeDoc $echangeDoc */
        foreach ($echangeDocs as $echangeDoc) {
            if ($echangeDocBlobs = $echangeDoc->getEchangeDocBlobs()) {
                $blobPrincipal = null;
                /** @var EchangeDocBlob $echangeDocBlob */
                foreach ($echangeDocBlobs as $echangeDocBlob) {
                    if ($echangeDocBlob->getCategoriePiece() === EchangeDocBlob::FICHIER_PRINCIPAL) {
                        $blobPrincipal = $echangeDocBlob;
                    }

                    if (
                        $blobPrincipal
                        && $echangeDocBlob->getCategoriePiece() === EchangeDocBlob::FICHIER_ANNEXE
                        && !$echangeDocBlob->getDocPrincipal()
                    ) {
                        $echangeDocBlob->setDocPrincipal($blobPrincipal);
                        $this->em->persist($echangeDocBlob);
                    }
                }
            }
        }

        $this->em->flush();

        $endExecutionTime = date('H:i:s');
        $output->writeln('Ended at ' . $endExecutionTime);

        return 0;
    }
}
