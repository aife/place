<?php

namespace App\Command\Migration;

use Exception;
use App\Service\MigrationEntiteAchat\MigrationEntiteAchatService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class MigrationEntiteAchatCommand extends Command
{
    protected static $defaultName = 'mpe:migration:services';
    use LockableTrait;

    public function __construct(
        private readonly MigrationEntiteAchatService $migrationEntiteAchat
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Migre les services et entités associées aux services d\'un organisme à un autre')
            ->addOption('cheminFichierServices', 'f', InputOption::VALUE_REQUIRED, 'chemin du fichier listant les identifiants uniques des services à migrer')
            ->addOption('acronymeOrganismeSource', 's', InputOption::VALUE_REQUIRED, 'acronyme de l\'organisme source des services à migrer')
            ->addOption('acronymeOrganismeDestination', 'd', InputOption::VALUE_REQUIRED, 'acronyme de l\'organisme de destination des services à migrer')
            ->addOption('modeReel', 'r', InputOption::VALUE_OPTIONAL, 'si ce paramètre est passé en option quelque soit sa valeur, alors la migration s\'effectue en réél');
    }

    /**
     * @return int|void|null
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln("Cette commande s'exécute déjà dans un autre processus.");

            return 0;
        }

        $cheminFichierServices = $input->getOption('cheminFichierServices');
        $cheminFichierServicesCorrespondances = $cheminFichierServices.'_correspondance_services';

        $acronymeOrganismeSource = $input->getOption('acronymeOrganismeSource');
        $acronymeOrganismeDestination = $input->getOption('acronymeOrganismeDestination');
        $modeTest = (((bool) $input->getOption('modeReel')) == 1) ? false : true;
        $time_start = microtime(true);

        $res = $this->migrationEntiteAchat->migrate($cheminFichierServices, $cheminFichierServicesCorrespondances, $acronymeOrganismeSource, $acronymeOrganismeDestination, $modeTest);

        if ($res) {
            $output->writeln('L\'opération de migration des entités d\'achat a été réalisée avec succès. Les logs donnent tous les détails, veuillez les consulter pour plus d\'informations');
        } else {
            $output->writeln('L\'opération de migration n\'a pas été réalisée car une erreur s\'est produite. Veuillez consulter les logs pour plus de détail');
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $output->writeln('Total Execution Time: '.ceil($execution_time).'s');
        $this->release();
        return 0;
    }
}
