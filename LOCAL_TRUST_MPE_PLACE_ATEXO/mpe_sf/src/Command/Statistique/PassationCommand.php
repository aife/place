<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Statistique;

use App\Command\MonitoredCommand;
use App\Entity\ConfigurationOrganisme;
use App\Service\AtexoConfiguration;
use App\Service\CommandMonitoringService;
use App\Service\Export\SuiviPassation;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class PurgeOffreCommand.
 */
class PassationCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:statistique:passation';
    const EXPORT_MARCHE_CONCLUS = 'export_marches_conclus';
    const EXPORT_PASSATION = 'export_passation';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly KernelInterface $kernel,
        private AtexoConfiguration $atexoConfiguration,
        private SuiviPassation $suiviPassation,
        private readonly CommandMonitoringService $commandMonitoringService,
    ) {
        parent::__construct($commandMonitoringService);
    }

    /**
     * @throws Exception
     */
    protected function configure()
    {
        $this->setDescription('Génération des statistiques de passation')
            ->addOption(
                'export',
                null,
                InputArgument::OPTIONAL,
                'Export à faire : export_marches_conclus, export_passation',
                false
            )->addOption(
                'organisme',
                null,
                InputArgument::OPTIONAL,
                'Organisme ciblé',
                false
            );
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $export = $input->getOption('export');
            $acronyme = $input->getOption('organisme');
            $organismes = $this->em->getRepository(ConfigurationOrganisme::class)->getOrganismeAcronymesWhereSuiviPassationActive($acronyme);
            foreach ($organismes as $organisme) {
                $acronyme = $organisme ['organisme'];
                $this->logger->info(' Début Export Organisme '.$acronyme.' : Export '.$export);
                $dirName = $this->parameterBag->get('STATISTIQUES_PASSATIONS_PATH').$acronyme.'/';
                if (!is_dir($dirName)) {
                    mkdir($dirName, 0777, true);
                }
                switch (strtolower($export)) {
                    case self::EXPORT_MARCHE_CONCLUS :
                        $this->exportMarchesConclus($acronyme, $dirName);
                        break;
                    case self::EXPORT_PASSATION :
                        $this->exportPassation($acronyme, $dirName);
                        break;
                    default :
                        $msg = 'Veuillez saisir l\'export à faire :'.
                            ' Export_Marches_Conclus,'.
                            ' Export_Passation';
                        $this->logger->info($msg);
                        $output->writeln($msg);

                        return -1;
                }
                $this->logger->info(' Fin Export Organisme '.$acronyme.' : Export '.$export);
            }
        } catch (\Exception $e) {
            $this->logger->error(
                ' Statistique '.$export.
                ' Organisme '.$acronyme.
                ' Erreur : '.$e->getMessage().
                ' trace : '.$e->getTraceAsString()
            );
        }

        return 0;
    }

    public function exportMarchesConclus($organisme, $dirName)
    {
        $fileCsv = $dirName.'Export_Marches_Conclus.csv';
        $this->suiviPassation->getEnteteExportMarchesConclus($fileCsv);
        $this->suiviPassation->generateExportMarchesConclus($organisme, $fileCsv);
    }

    public function exportPassation($organisme, $dirName)
    {
        $fileCsv = $dirName.'Export_Passation.csv';
        $this->suiviPassation->getEnteteExportSuiviPassation($fileCsv);
        $this->suiviPassation->generateExportPassation($organisme, $fileCsv);
    }

}
