<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Statistique;

use App\Command\MonitoredCommand;
use App\Entity\ConfigurationOrganisme;
use App\Message\ExportPassation;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ExportPassationCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:export:passation';

    const EXPORT_MARCHE_CONCLUS = 'export_marches_conclus';
    const EXPORT_PASSATION = 'export_passation';

    /**
     * @param LoggerInterface $logger
     * @param EntityManagerInterface $em
     * @param MessageBusInterface $bus
     */
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $em,
        private readonly MessageBusInterface $bus,
        private readonly CommandMonitoringService $commandMonitoringService,
    ) {
        parent::__construct($this->commandMonitoringService);
    }

    /**
     * configurer la commande
     */
    protected function configure()
    {
        $this
            ->setDescription(
                'Commande d\'export des statistiques passation'
            )->addOption(
                'export',
                null,
                InputArgument::OPTIONAL,
                'Export à faire : Export_Marches_Conclus, Export_Passation ',
                false
            );
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $export = $input->getOption('export');
            $organismes = $this->em->getRepository(ConfigurationOrganisme::class)
                ->getOrganismeAcronymesWhereSuiviPassationActive();
            foreach ($organismes as $organisme) {
                $acronyme = $organisme ['organisme'];
                switch (strtolower($export)) {
                    case self::EXPORT_MARCHE_CONCLUS:
                        $this->retryAgain(self::EXPORT_MARCHE_CONCLUS, $acronyme);
                        break;
                    case self::EXPORT_PASSATION:
                        $this->retryAgain(self::EXPORT_PASSATION, $acronyme);
                        break;
                    default:
                        $this->retryAgain(self::EXPORT_MARCHE_CONCLUS, $acronyme);
                        $this->retryAgain(self::EXPORT_PASSATION, $acronyme);
                }
            }
        } catch (\Exception $e) {
            $this->logger->error(
                ' Export statistique ' .
                ' Organisme ' . $acronyme .
                ' Erreur : ' . $e->getMessage() .
                ' trace : ' . $e->getTraceAsString()
            );
        }

        return 0;
    }

    protected function retryAgain(string $export, string $acronyme)
    {
        try {
            $this->bus->dispatch(new ExportPassation($export, $acronyme));
        } catch (\Exception $exception) {
            $this->logger->error(
                'Error dispatch. Stack Trace : '
                . $exception->getMessage() . ' ' . $exception->getTraceAsString()
            );
        }
    }
}
