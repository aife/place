<?php

namespace App\Command\EspaceDocumentaire;

use App\Command\MonitoredCommand;
use App\Message\DeleteArchive;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use DateTime;
use RuntimeException;
use Exception;
use App\Entity\Agent;
use App\Entity\Agent\NotificationAgent;
use App\Entity\Consultation;
use App\Service\EspaceDocumentaire\EspaceDocumentaireService;
use App\Utils\Encryption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class GenerationArchiveDocumentaireCommand.
 */
class GenerationArchiveDocumentaireCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:generation-archive-documentaire';
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Encryption $encryption,
        private readonly EspaceDocumentaireService $espaceDocumentaireService,
        private readonly MessageBusInterface $bus,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly LoggerInterface $espacedocLogger,
        $name = null
    ) {
        parent::__construct($commandMonitoringService, $name);
    }

    protected function configure()
    {
        $this->setDescription('génération de l\'archie documentaire')
            ->addArgument(
                'consultationId',
                InputArgument::REQUIRED,
                'id de la consultation'
            )
            ->addArgument(
                'agentId',
                InputArgument::REQUIRED,
                'id de l\'agent '
            )
            ->addArgument(
                'json',
                InputArgument::REQUIRED,
                'liste des fichiers qui constitue l\'archive (sous forme de JSON)'
            )
            ->addArgument(
                'type',
                InputArgument::OPTIONAL,
                'type Notification : 0 : Espace documentaire, 1 : téléchargement des plis',
                '0'
            )
            ->addArgument(
                'supprimerApres',
                InputArgument::OPTIONAL,
                'nombre jour pour supprimer le fichier'
            );
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $description = null;
        $libelleNotifcation = null;
        $output->writeln('Début de création de l\'archive');
        $json = $input->getArgument('json');
        $agentId = $input->getArgument('agentId');
        $type = $input->getArgument('type');
        $consultationId = $input->getArgument('consultationId');
        $em = $this->em;

        $espaceDocumentaireService = $this->espaceDocumentaireService;
        $agent = $em->getRepository(Agent::class)->find($agentId);
        $consultation = $em->getRepository(Consultation::class)->find($consultationId);

        $notification = new NotificationAgent();
        $notification->setIdAgent($agentId);
        $notification->setIdType(1);
        $notification->setOrganisme($agent->getOrganisme());
        if ('0' === $type) {
            $libelleNotifcation = '@@LABEL_NOTIFICATION_ARCHIVE@@';
            $description = '@@DEFINE_NOTIFICATION_ARCHIVE_ERREUR@@';
        } elseif ('1' === $type) {
            $libelleNotifcation = '';
            $description = '@@DEFINE_NOTIFICATION_TELECHARGEMENT_PLIS_SELECTIONNES_ERREUR@@';
        }
        $path = '?page=Agent.DetailConsultation&id=' . $consultationId;

        try {
            $date = (new DateTime())
                ->format('YmdHis');

            $result = $espaceDocumentaireService->genereteArchive(
                $consultationId,
                $json,
                $date
            );

            if (!in_array(false, $result['resultatArchive'])) {
                try {
                    if (
                        !is_dir($concurrentDirectory = $this->parameterBag->get('PATH_ARCHIVE_ESPACE_DOCUMENTAIRE'))
                        && !mkdir($concurrentDirectory)
                    ) {
                        throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
                    }
                } catch (Exception $exc) {
                    $this->espacedocLogger->warning($exc->getMessage());
                }
                $pathArchive = $this->parameterBag->get('PATH_ARCHIVE_ESPACE_DOCUMENTAIRE')
                    . $result['archiveName'];
                rename(
                    $result['pathArchive'],
                    $pathArchive
                );

                $path = 'agent/download-archive/'
                    . $this->encryption->cryptId($agentId)
                    . '/'
                    . $this->encryption->cryptId($consultationId)
                    . '/'
                    . $result['archiveName']
                    . '/'
                    . $type
                ;
                if ('0' === $type) {
                    $libelleNotifcation = '@@LABEL_NOTIFICATION_ARCHIVE_OK@@';
                } elseif ('1' === $type) {
                    $libelleNotifcation = '@@DEFINE_NOTIFICATION_TELECHARGEMENT_PLIS_SELECTIONNES@@';
                }
                $this->espacedocLogger->info('lancement message DeleteArchive avec le path : ' . $pathArchive);
                $this->bus->dispatch(new DeleteArchive($pathArchive));
            } else {
                $notification->setDescription($description);
            }
        } catch (Exception $exception) {
            if ('1' === $type) {
                $libelleNotifcation = '';
            }
            $path = '?page=Agent.DetailConsultation&id=' . $consultationId;
            $notification->setDescription($description);
            $this->espacedocLogger->error('Erreur lors de l\'execution de la 
                                           commande mpe:generation-archive-documentaire. Message : '
                . $exception->getMessage() . PHP_EOL . 'Trace: ' . $exception->getTraceAsString());
        }

        $notification->setReferenceObjet($consultation->getReferenceUtilisateur());
        $notification->setUrlNotification($path);
        $notification->setLibelleNotifcation($libelleNotifcation);
        $notification->setNotificationLue(0);
        $notification->setNotificationActif(1);
        $datetime = new DateTime('now');
        $notification->setDateCreation($datetime);

        $em->persist($notification);
        $em->flush();

        $output->writeln('FIN de création de l\'archive');

        return 0;
    }
}
