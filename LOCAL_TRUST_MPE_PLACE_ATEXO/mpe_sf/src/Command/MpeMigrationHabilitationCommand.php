<?php

namespace App\Command;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationTypeProcedure;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ReferentielHabilitation;
use App\Entity\TypeProcedure;
use App\Enum\Habilitation\HabilitationSlug;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\Query\Expr\Join;

#[AsCommand(
    name: 'mpe:migration:habilitation',
    description: 'Migration des habilitations V1 vers V2',
)]
class MpeMigrationHabilitationCommand extends MonitoredCommand
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private CommandMonitoringService $commandMonitoringService,
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('Migration des habilitations .....');
        if ($this->commandMonitoringService->isCommandLaunched($this->getName())) {
            $io->error('Command is launched ......');

            return Command::FAILURE;
        }
        $this->entityManager->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->entityManager->createNativeQuery(
            'TRUNCATE table habilitation_type_procedure;',
            new ResultSetMapping()
        )->execute();
        $habilitationReferentiels = $this->entityManager->getRepository(ReferentielHabilitation::class)->findAll();
        foreach ($habilitationReferentiels as $referentiel) {
            $habilitation = $referentiel->getSlug();
            $referentielId = $referentiel->getId();

            $pageSize = 1000;
            $currentPage = 1;

            do {
                $habilitationAgents = $this->entityManager->getRepository(HabilitationAgent::class)
                    ->createQueryBuilder('ha')
                    ->join(Agent::class, 'a', Join::WITH, 'ha.agent = a.id AND a.deletedAt is NULL')
                    ->setMaxResults($pageSize)
                    ->setFirstResult(($currentPage - 1) * $pageSize)
                    ->getQuery()->getResult();

                $referentiel = $this->entityManager->getRepository(ReferentielHabilitation::class)
                    ->find($referentielId);
                foreach ($habilitationAgents as $habilitationAgent) {
                    $isActive = false;
                    $simpilfieMapa = 0;
                    $methodName = str_replace('_', '', ucwords($habilitation, '_'));
                    if (method_exists($habilitationAgent, 'get' . $methodName)) {
                        $isActive = $habilitationAgent->{'get' . $methodName}();
                    } elseif (method_exists($habilitationAgent, 'is' . $methodName)) {
                        $isActive = $habilitationAgent->{'is' . $methodName}();
                    }

                    if (
                        in_array(
                            $habilitation,
                            [
                                HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE->value,
                                HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE->value,
                                HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE->value
                            ]
                        )
                    ) {
                        $isActive = $habilitationAgent->getCreerSuiteConsultation();
                    }

                    if (HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES->value === $habilitation) {
                        if ($habilitationAgent->getGererMapaSuperieurMontant()) {
                            $isActive = true;
                            $simpilfieMapa += 2;
                        }

                        if ($habilitationAgent->getGererMapaInferieurMontant()) {
                            $isActive = true;
                            $simpilfieMapa += 1;
                        }
                    }

                    if ($isActive) {
                        if (
                            in_array(
                                $habilitation,
                                [
                                    HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT->value,
                                    HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE->value
                                ]
                            )
                        ) {//MAPA > 90
                            $agentTypeProcedures = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                                ->findBy(
                                    [
                                        'mapa' => 1,
                                        'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_SUP_90'),
                                        'organisme' => $habilitationAgent->getAgent()
                                                                         ?->getOrganisme()
                                                                         ?->getAcronyme(),
                                    ]
                                );
                            $this->saveHabilitationWithTypeProcedure(
                                $agentTypeProcedures,
                                $referentielId,
                                $habilitationAgent
                            );
                        } elseif (
                            in_array(
                                $habilitation,
                                [
                                    HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT->value,
                                    HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE->value
                                ]
                            )
                        ) {//MAPA < 90
                            $agentTypeProcedures = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                                ->findBy(
                                    [
                                        'mapa'            => '1',
                                        'idMontantMapa'   => $this->parameterBag->get('MONTANT_MAPA_INF_90'),
                                        'organisme'       => $habilitationAgent->getAgent()
                                            ?->getOrganisme()
                                            ?->getAcronyme(),
                                    ]
                                );
                            $this->saveHabilitationWithTypeProcedure(
                                $agentTypeProcedures,
                                $referentielId,
                                $habilitationAgent
                            );
                        } elseif (
                            in_array(
                                $habilitation,
                                [
                                    HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES->value,
                                    HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE->value
                                ]
                            )
                        ) {
                            //procédures formalisées
                            $agentTypeProcedures = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                                ->findBy(
                                    [
                                        'activerMapa' => '1',
                                        'organisme' => $habilitationAgent->getAgent()
                                            ?->getOrganisme()
                                            ?->getAcronyme()
                                    ]
                                );
                            $this->saveHabilitationWithTypeProcedure(
                                $agentTypeProcedures,
                                $referentielId,
                                $habilitationAgent
                            );
                        } elseif (HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES->value === $habilitation) {
                            $criteria = [
                                'procedureSimplifie' => '1',
                                'organisme' => $habilitationAgent->getAgent()
                                    ?->getOrganisme()
                                    ?->getAcronyme()
                            ];
                            if ($simpilfieMapa > 0 && $simpilfieMapa < 3) {
                                $criteria['idMontantMapa'] = $simpilfieMapa;
                            }
                            $agentTypeProcedures = $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                                ->findBy($criteria);
                            $this->saveHabilitationWithTypeProcedure(
                                $agentTypeProcedures,
                                $referentielId,
                                $habilitationAgent
                            );
                        } else {
                            $habilitationProcedure = new HabilitationTypeProcedure();
                            $habilitationProcedure->setHabilitation($referentiel)
                                ->setAgent($habilitationAgent->getAgent());
                            $tempObjets[] = $habilitationProcedure;
                            $this->entityManager->persist($habilitationProcedure);
                        }
                    }
                }

                $this->entityManager->flush();
                $this->entityManager->clear();
                $this->entityManager->detach($habilitationAgent);
                foreach ($tempObjets as $tempObject) {
                    $this->entityManager->detach($tempObject);
                }

                $tempObjets = [];
                gc_enable();
                gc_collect_cycles();
                $currentPage++;
            } while (count($habilitationAgents) > 0);
            $io->success(sprintf('Page %s migration des habilitations terminée avec succès.', $habilitation) . (memory_get_peak_usage(true) / 1024 / 1024) . " MiB\n\n");
        }

        $this->entityManager->flush();
        $this->entityManager->clear();

        $io->success('Migration des habilitations terminée avec succès.');

        return Command::SUCCESS;
    }

    protected function saveHabilitationWithTypeProcedure(
        array $agentTypeProcedures,
        int $referentielId,
        HabilitationAgent $habilitationAgent
    ) {
        $tempObjets = [];
        $referentiel = $this->entityManager->getRepository(ReferentielHabilitation::class)
            ->find($referentielId);
        foreach ($agentTypeProcedures as $agentTypeProcedure) {
            $habilitationProcedure = new HabilitationTypeProcedure();
            $habilitationProcedure->setHabilitation($referentiel)
                ->setTypeProcedure($agentTypeProcedure->getIdTypeProcedure())
                ->setOrganism($habilitationAgent->getAgent()?->getOrganisme())
                ->setAgent($habilitationAgent->getAgent());
            $tempObjets[] = $habilitationProcedure;
            $this->entityManager->persist($habilitationProcedure);
        }
        $this->entityManager->flush();
        $this->entityManager->clear();
        $this->entityManager->detach($habilitationAgent);
        foreach ($tempObjets as $tempObject) {
            $this->entityManager->detach($tempObject);
        }

        $tempObjets = [];
        gc_enable();
        gc_collect_cycles();
    }
}
