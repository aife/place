<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Rss;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RssController
 * @package Atexo\AppBundle\Controller\Rss
 */
class RssController extends AbstractController
{
    #[Route(path: '/rssTr.xml', name: 'rss')]
    public function rssAction(Request $request) : Response
    {
        $file = $this->getParameter('DIR_FLUX_RSS') . $request->getPathInfo();
        return $this->file($file, null, ResponseHeaderBag::DISPOSITION_INLINE);
    }
}
