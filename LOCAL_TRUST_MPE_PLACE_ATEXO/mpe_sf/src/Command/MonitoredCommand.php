<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use Throwable;
use App\Service\CommandMonitoringService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class MonitoredCommand extends Command
{
    public function __construct(
        private readonly CommandMonitoringService $commandMonitoringService,
        string $name = null
    ) {
        parent::__construct($name);
    }

    public function run(InputInterface $input, OutputInterface $output)
    {
        $commandLog = $this->commandMonitoringService->createCommandLog($this->getName(), $input->getArguments());
        try {
            $return = parent::run($input, $this->commandMonitoringService->getOutputMonitoredCommand());
            $this->commandMonitoringService->setSuccess($commandLog);
        } catch (Throwable $e) {
            $this->commandMonitoringService->setFailure($commandLog, $e->getMessage());
            throw $e;
        }

        return $return;
    }
}
