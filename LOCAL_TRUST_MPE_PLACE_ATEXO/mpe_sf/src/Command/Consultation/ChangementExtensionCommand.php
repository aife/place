<?php

namespace App\Command\Consultation;

use App\Entity\BloborganismeFile;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use DateTime;
use App\Service\AtexoFichierOrganisme;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Class PurgeOffreCommand.
 */
class ChangementExtensionCommand extends Command
{
    protected static $defaultName = 'mpe:changement:extension';

    private ?OutputInterface $output = null;

    /**
     * @var bool pour les test
     */
    private $dryRun;

    /**
     * @var resource
     */
    private $fopen;

    /**
     * @var string
     */
    private $baseRootDir;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly AtexoFichierOrganisme $serviceBlobOrganisme,
        private readonly KernelInterface $kernel,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * @throws Exception
     */
    protected function configure()
    {
        $this->setDescription('Permet de changer l\'extension des fichiers')
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation le changement de l\'extension',
                true
            )
            ->addOption(
                'dateDebut',
                null,
                InputArgument::OPTIONAL,
                'date de début pour la récupèration',
                (new DateTime(date('Y-m-d 00:00:00', strtotime('-5 day'))))->format('Y-m-d H:i:s')
            )
            ->addOption(
                'dateFin',
                null,
                InputArgument::OPTIONAL,
                'date de début pour la récupèration',
                (new DateTime(date('Y-m-d 23:59:59', strtotime('-0 day'))))->format('Y-m-d H:i:s')
            );
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $helper = $this->getHelper('question');
            $em = $this->em;

            $this->output = $output;
            $this->output->writeln('Début de la migration des extension');
            $dateDebut = $input->getOption('dateDebut');
            $dateFin = $input->getOption('dateFin');
            $this->baseRootDir = $this->kernel->getRootDir();
            $this->dryRun = $input->getOption('dry-run');

            $blobs = $em->getRepository(BloborganismeFile::class)
                ->getListBlob($dateDebut, $dateFin);

            $msg = 'Nombre de fichier entre le '.$dateDebut.' et '.$dateFin.' qui seront traiter : '.(is_countable($blobs) ? count($blobs) : 0);
            $this->output->writeln($msg);

            $tmp = $this->baseRootDir.'tmp/changementExtension_'.date('YmdHis').'.txt';
            $this->fopen = fopen($tmp, 'w+');
            fwrite($this->fopen, $msg."\n");

            if (!$this->dryRun) {
                $question = new ConfirmationQuestion(
                    'Attention cette action sera irréversible. Voulez-vous vraiment continuer ?',
                    false,
                    '/^(y|j)/i'
                );
                if (!$helper->ask($input, $this->output, $question)) {
                    return 0;
                }
            }
            $this->migrationExtension($blobs, $this->parameterBag->get('EXTENSION_NON_CHIFFRE'));

            /**
             * Reprise des données jeton !
             */
            $blobs = $em->getRepository(BloborganismeFile::class)
                ->getListBlobJetonOffreChiffre($dateDebut, $dateFin);

            $msg = 'Nombre de fichier jeton entre le '.$dateDebut.' et '.$dateFin.' qui seront traiter : '.(is_countable($blobs) ? count($blobs) : 0);
            $this->output->writeln($msg);

            fwrite($this->fopen, $msg."\n");

            $this->migrationExtension($blobs, $this->parameterBag->get('EXTENSION_SIGNATURE'));

            $this->output->writeln('Fin de la migration des extension');
        } catch (Exception $e) {
            $erreur = 'Erreur sur la migration des extensions => '.$e->getMessage();
            $this->logger->error($erreur);
        }
        return 0;
    }

    /**
     * @param $blob
     * @param $extension
     */
    private function TraitementDryRun($blob, $extension)
    {
        $file = $this->baseRootDir.$blob['chemin'].
            $blob['id'].'-0'.$blob['extension'];

        $fileRename = $this->baseRootDir.$blob['chemin'].
            $blob['id'].'-0'.$extension;

        $this->output->writeln('Fichier qui sera  renomer  : '.$file.' en '.$fileRename);
        $this->output->writeln('Commande  : mv '.$file.' '.$fileRename);
        fwrite($this->fopen, 'Commande  : mv '.$file.' '.$fileRename."\n");
    }

    /**
     * @param $blob
     * @param $extension
     *
     * @throws Exception
     */
    private function ChangementExtension($blob, $extension)
    {
        if (!$this->dryRun && $blob['id']) {
            $this->TraitementDryRun($blob, $extension);
            $this->serviceBlobOrganisme->changeExtensionBlob($blob['id'], $blob['organisme'], $extension);
        }
    }

    /**
     * @param $blobs
     * @param string $extension
     *
     * @throws Exception
     */
    private function migrationExtension($blobs, $extension = '.non_chiffre')
    {
        foreach ($blobs as $blob) {
            if ($this->dryRun) {
                $this->TraitementDryRun($blob, $extension);
            } else {
                $this->ChangementExtension($blob, $extension);
            }
        }
    }
}
