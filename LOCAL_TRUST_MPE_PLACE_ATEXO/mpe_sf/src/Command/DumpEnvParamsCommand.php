<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DumpEnvParamsCommand extends Command
{
    protected static $defaultName = 'mpe:dump:envvars';

    public function __construct(private readonly ParameterBagInterface $parameterBag, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Dump environment variables.')
            ->setHelp('This command dumps Symfony environment variables (keys and values)')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->parameterBag->all() as $key => $param) {
            if (str_starts_with($key, "env(")) {
                echo $output->writeln(sprintf('%s  =>  "%s"', $key, $param));
            }
        }
        return 0;
    }
}
