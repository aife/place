<?php

namespace App\Command;

use DateTime;
use App\Service\AtexoFichierOrganisme;
use App\Entity\CandidatureMps;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TListeLotsCandidature;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class PurgeOffreCommand.
 */
class PurgeOffreCommand extends Command
{
    protected static $defaultName = 'mpe:offre:purge';
    private $limit;
    private $retention;
    private ?string $date = null;
    private ?OutputInterface $output = null;
    private $dryRun;

    final protected const PHRASE_HEAD = '[table => blobOrganisme et blobOrganismeFile] / '
        . 'Suppression de la donnée suivante dans la table blobOrganisme et blobOrganismeFile : ';

    public function __construct(
        private EntityManagerInterface $entityManager,
        private LoggerInterface $logger,
        private AtexoFichierOrganisme $atexoFichierOrganisme,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Supprimer les offres d\'un organisme')
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation de la suppression (activé par défaut)',
                true
            )
            ->addOption(
                'retention',
                null,
                InputArgument::OPTIONAL,
                'Nombre de jours de rétention des offres (5 par défaut)',
                '5'
            )
            ->addOption(
                'limit',
                null,
                InputArgument::OPTIONAL,
                'Limite : nombre d\'offres par batch (5 par défaut)',
                '5'
            )
        ;
    }

    /**
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->output = $output;
            $this->output->writeln('Début suppression');
            $this->limit = $input->getOption('limit');
            $this->retention = $input->getOption('retention');
            $this->dryRun = $input->getOption('dry-run');
            $format = 'Y-m-d';
            $this->date = (new DateTime(date($format, strtotime('-' . $this->retention . ' day'))))
                ->format($format);

            $organismes = $this->entityManager->getRepository(Organisme::class)->findByTagPurge(true);
            foreach ($organismes as $organisme) {
                $acronyme = $organisme->getAcronyme();
                if (empty($acronyme)) {
                    continue;
                }

                $this->output->write("\n");
                $phrase = 'Phase 1 : purge des candidatures MPS';
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->output->write("\n");
                $this->cleanTableCandidatureMps($organisme);

                $this->output->write("\n");
                $phrase = 'Phase 2 : purge des offres sans date de création';
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->output->write("\n");
                $this->purgeBaseSansDateReference($organisme);

                $this->output->write("\n");
                $phrase = 'Phase 3 : purge des offres avec date de création';
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->output->write("\n");
                $this->purgeBaseDateReference($organisme);
                $this->output->write("\n");
            }

            if (false === $this->dryRun) {
                $this->entityManager->flush();
            }

            $this->output->writeln('Fin suppression');
        } catch (Exception $e) {
            $erreur = 'Erreur PurgerAccessTokenCommand => ' . $e->getMessage();
            $this->logger->error($erreur);
        }
        return 0;
    }

    public function purgeBaseSansDateReference(string $organisme)
    {
        $phrase = 'Traitement des offres sans date de création pour l\'organisme : ' . $organisme;
        $this->logger->info($phrase);
        $this->output->writeln($phrase);
        $this->output->write("\n");

        $offres = $this->entityManager->getRepository(Offre::class)
            ->listOffresASupprimerSansDate($organisme, $this->limit);
        if (!empty($offres)) {
            $this->clean($offres, $organisme);
        }
    }

    public function purgeBaseDateReference(string $organisme)
    {
        $phrase = 'Traitement des offres avec date création pour l\'organisme : ' . $organisme;
        $this->logger->info($phrase);
        $this->output->writeln($phrase);
        $this->output->write("\n");
        $offres = $this->entityManager->getRepository(Offre::class)
            ->listeOffresASupprimer($organisme, $this->date, $this->limit);
        $this->clean($offres, $organisme);
    }

    public function clean(array $offres, string $organisme)
    {
        $tabOffres = [];
        foreach ($offres as $offre) {
            $tabOffres[] = $offre->getId();
        }

        if ($tabOffres) {
            $this->cleanTableCandidature($tabOffres, $organisme);
            $this->cleanEnveloppe($tabOffres, $organisme);
        }

        foreach ($offres as $offre) {
            $phrase = '[table => offres ] / Suppression de l\'offre :' . $offre->getId();
            $this->logger->info($phrase);
            $this->output->writeln($phrase);
            $this->output->write("\n");
            $this->entityManager->remove($offre);
        }
    }

    public function cleanTableCandidature(array $offres, string $organisme)
    {
        $candidatures = $this->entityManager->getRepository(TCandidature::class)
            ->listeCandidaturesASupprimer($offres, $organisme, $this->limit);

        if ($candidatures) {
            foreach ($candidatures as $candidature) {
                $this->cleanListeLotsCandidature($candidature->getId(), $organisme);
                $phrase = '[table => t_candidature_mps ] / Suppression de la candidature suivante :' .
                    $candidature->getId();
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->output->write("\n");
                $this->entityManager->remove($candidature);
            }
        }
    }

    /**
     * @param $organisme
     */
    public function cleanTableCandidatureMps($organisme)
    {
        $candidaturesMps = $this->entityManager->getRepository(CandidatureMps::class)
            ->listeCandidaturesMpsASupprimer($organisme, $this->date, $this->limit);
        if ($candidaturesMps) {
            foreach ($candidaturesMps as $candidatureMps) {
                $phrase = '[table => t_candidature ] / Suppression de la candidature mps suivante : ' .
                    $candidatureMps->getIdCandidature();
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->entityManager->remove($candidatureMps);

                $phrase = self::PHRASE_HEAD;
                $phrase .= $candidatureMps->getIdBlob();
                $this->logger->info($phrase);
                $this->output->writeln($phrase);

                $this->cleanBlob($candidatureMps->getIdBlob(), $organisme);
            }
        }
    }

    public function cleanListeLotsCandidature(int $idCanditure, string $organisme)
    {
        $lotsCandidatures = $this->entityManager->getRepository(TListeLotsCandidature::class)
            ->listeLotsCandidatureASupprimer($idCanditure, $organisme);
        foreach ($lotsCandidatures as $lotsCandidature) {
            $phrase = '[table => t_liste_lots_candidature] / Suppression des lots de candidatures suivante :' .
                $idCanditure;
            $this->logger->info($phrase);
            $this->output->writeln($phrase);
            $this->output->write("\n");
            $this->entityManager->remove($lotsCandidature);
        }
    }

    /**
     * @Todo : Lors du merge avec les dossier volumineux, il faudra probablement faire une adaptation
     */
    public function cleanEnveloppe(array $offres, string $organisme)
    {
        $enveloppes = $this->entityManager->getRepository(Enveloppe::class)
            ->listeEnveloppesASupprimer($offres, $organisme);
        foreach ($enveloppes as $enveloppe) {
            $this->cleanFichierEnveloppe($enveloppe->getIdEnveloppeElectro(), $organisme);

            $phrase = "[table => enveloppe] / Suppression de l'enveloppe : " . $enveloppe->getIdEnveloppeElectro();
            $this->logger->info($phrase);
            $this->output->writeln($phrase);
            $this->output->write("\n");
            $this->entityManager->remove($enveloppe);
        }
    }

    public function cleanFichierEnveloppe(int $idEnveloppe, string $organisme)
    {
        $fichierEnveloppes = $this->entityManager->getRepository(EnveloppeFichier::class)
            ->listeFichiersEnveloppeASupprimer($idEnveloppe, $organisme);
        foreach ($fichierEnveloppes as $fichierEnveloppe) {
            $phrase = self::PHRASE_HEAD;
            $phrase .= $fichierEnveloppe->getIdBlob();
            $this->logger->info($phrase);
            $this->output->writeln($phrase);
            if (null !== $fichierEnveloppe->getBlob()) {
                $this->cleanBlob($fichierEnveloppe->getBlob()->getId(), $organisme);
            }

            if ($fichierEnveloppe->getIdBlobSignature()) {
                $phrase = self::PHRASE_HEAD;
                $phrase .= $fichierEnveloppe->getIdBlobSignature();
                $this->logger->info($phrase);
                $this->output->writeln($phrase);
                $this->cleanBlob($fichierEnveloppe->getIdBlobSignature(), $organisme);
            }
            $phrase = '[table => fichierEnveloppe] / Suppression du fichier enveloppe : ' .
                $fichierEnveloppe->getIdFichier();
            $this->logger->info($phrase);
            $this->output->writeln($phrase);
            $this->output->write("\n");
            $this->entityManager->remove($fichierEnveloppe);
        }
    }

    public function cleanBlob(int $idBlob, string $organisme)
    {
        $file = $this->parameterBag->get('BASE_ROOT_DIR') . $organisme . '/files/' . $idBlob . '-0';
        $phrase = 'Suppression du fichier suivant :' . $file;
        $this->logger->info($phrase);
        $this->output->writeln($phrase);
        $this->output->write("\n");

        if (false === $this->dryRun && $idBlob) {
            $this->atexoFichierOrganisme->deleteBlobFile($idBlob, $organisme);
        }

        $this->output->write("\n");
    }
}
