<?php

namespace App\Command;

use App\Repository\ConsultationRepository;
use App\Service\Publicite\EchangeConcentrateur;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SuiviPubliciteCommand extends Command
{
    final public const STATUS_MAPPING = [
        'ENVOI_PLANIFIER'                       => 'PUBLICATION_PLANNED',
        'EN_COURS_DE_PUBLICATION'               => 'TO_BE_PUBLISHED',
        'PUBLIER'                               => 'PUBLISHED',
        'ARRETER'                               => 'PUBLICATION_STOP',
        'REJETER_CONCENTRATEUR'                 => 'PUBLICATION_REJECTED_CONCENTRATEUR',
        'REJETER_SUPPORT'                       => 'PUBLICATION_REJECTED_SUPPORT',
        'REJETER_CONCENTRATEUR_VALIDATION_ECO'  => 'PUBLICATION_REJECTED_VALIDATION_ECO',
        'REJETER_CONCENTRATEUR_VALIDATION_SIP'  => 'PUBLICATION_REJECTED_VALIDATION_SIP'
    ];

    protected static $defaultName = 'mpe:suivi:publicite';
    protected static $defaultDescription = 'Synchronize publication statut';

    public function __construct(
        private readonly EchangeConcentrateur $echangeConcentrateur,
        private readonly LoggerInterface $logger,
        private readonly ConsultationRepository $consultationRepository,
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->addOption('startDate', null, InputOption::VALUE_REQUIRED, 'Start date (yyyy-MM-dd hh:mm:ss)')
            ->addOption('endDate', null, InputOption::VALUE_REQUIRED, 'End date (yyyy-MM-dd hh:mm:ss)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info("Début de l'execution de la commande mpe:suivi:publicite");

        $startDate = $input->getOption('startDate');
        if (empty($startDate)) {
            $date = new \DateTime();
            $date->modify('-2 week');
            $startDate = $date->format('Y-m-d H:i:s');
        }

        $endDate = $input->getOption('endDate');
        if (empty($endDate)) {
            $date = new \DateTime();
            $endDate = $date->format('Y-m-d H:i:s');
        }

        $accessToken = $this->echangeConcentrateur->getToken();
        $rawConsultations = $this->echangeConcentrateur->getConsultationSynchronize($accessToken, $startDate, $endDate);
        $this->logger->info('Récupération de ' . count($rawConsultations) . ' consultations depuis le concentrateur');

        $consultationUpdated = 0;
        foreach ($rawConsultations as $value) {
            if (!empty($value['idConsultation']) && !empty(self::STATUS_MAPPING[$value['statut']])) {
                $consultation = $this->consultationRepository->find($value['idConsultation']);
                if (!empty($consultation)) {
                    if (empty($consultation->getDateMiseEnLigneCalcule())
                        || $consultation->getDateMiseEnLigneCalcule() === '0000-00-00 00:00:00'
                    ) {
                        if ($consultation->getRegleMiseEnLigne() == $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP')
                            && !empty($value['dateSoumission']))
                        {
                            $consultation->setDateMiseEnLigneCalcule(new \DateTime($value['dateSoumission']));
                        } elseif ($consultation->getRegleMiseEnLigne() == $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP')
                            && !empty($value['datePublication'])) {
                            $consultation->setDateMiseEnLigneCalcule(new \DateTime($value['datePublication']));
                        }  elseif ($consultation->getRegleMiseEnLigne() == $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ENTITE_COORDINATRICE')
                            && $consultation->getDateMiseEnLigneSouhaitee() >= date('Y-m-d')
                        ) {
                            $dateMiseEnLigne = isset($value['dateMiseEnLigneCalcule']) ? new \DateTime($value['dateMiseEnLigneCalcule']) : null;
                            $consultation->setDateMiseEnLigneCalcule($dateMiseEnLigne);
                        }
                        $consultation->setEtatPublication($this->parameterBag->get(self::STATUS_MAPPING[$value['statut']]));
                    }
                    $consultationUpdated++;
                }
            }
        }
        $this->entityManager->flush();
        $this->logger->info($consultationUpdated . ' consultation(s) mise(s) à jour');

        $this->logger->info("Fin de l'execution de la commande mpe:suivi:publicite");

        return Command::SUCCESS;
    }
}
