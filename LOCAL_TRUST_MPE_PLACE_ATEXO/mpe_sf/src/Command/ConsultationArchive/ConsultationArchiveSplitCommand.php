<?php

namespace App\Command\ConsultationArchive;

use Exception;
use App\Service\ConsultationArchive\ConsultationArchiveSplitService;
use App\Utils\Filesystem\Adapter\Local;
use App\Utils\Filesystem\Adapter\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationArchiveSplitCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'consultation:archive:split';

    public function __construct(private ParameterBagInterface $parameterBag, private ConsultationArchiveSplitService $archiveSplitService)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Split les fichiers et alimente la table consultation_archive_bloc')
            ->addOption(
                'poidsMaximum',
                'p',
                InputOption::VALUE_OPTIONAL,
                'poids maximum à splitter en Go',
                '25'
            );
    }

    /**
     * @return int|void|null
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!$this->lock()) {
            $output->writeln("Cette commande s'exécute déjà dans un autre process.");
            return 0;
        }

        $poidsMaximum = $input->getOption('poidsMaximum');
        // poids maximum en octets
        $poidsMaximum = $poidsMaximum * 1024 ** 3;
        $time_start = microtime(true);
        $path = $this->parameterBag->get('FICHIERS_ARCHIVE_DIR');
        $adapter = new Local($path);
        $filesystem = new Filesystem($adapter);

        $chunk = $this->archiveSplitService->getMaxBlocSize();
        $this->archiveSplitService->setChunk($chunk);
        $this->archiveSplitService->setTimeOut($this->parameterBag->get('PROCESS_TIMEOUT'));
        $this->archiveSplitService->setOutput($output);
        $this->archiveSplitService->setFilesystem($filesystem);
        $this->archiveSplitService->setPath($path);
        $this->archiveSplitService->setPoidsMaximum($poidsMaximum);
        $datas = $this->archiveSplitService->findByStatusNonFragmente();
        $this->archiveSplitService->populate($datas);

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $output->writeln('Total Execution Time: ' . ceil($execution_time) . 's');
        $this->release();

        return 0;
    }
}
