<?php

namespace App\Command\ConsultationArchive;

use Exception;
use App\Service\ConsultationArchive\ConsultationArchiveFichierService;
use App\Utils\Filesystem\Adapter\Local;
use App\Utils\Filesystem\Adapter\Filesystem;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationArchiveFichierCommand extends Command
{
    protected static $defaultName = 'consultation:archive:fichier';

    public function __construct(private ParameterBagInterface $parameterBag, private ConsultationArchiveFichierService $archiveFichierService)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Alimente la table consultation_archive_fichiers')
            ->addOption('fromfile', 'f', InputOption::VALUE_NONE, 'launch with file')
        ;
    }

    /**
     * @return int|void|null
     *
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $time_start = microtime(true);
        $path = $this->parameterBag->get('FICHIERS_ARCHIVE_DIR');
        $adapter = new Local($path);
        $filesystem = new Filesystem($adapter);

        $this->archiveFichierService->setOutput($output);
        $this->archiveFichierService->setFilesystem($filesystem);

        $option = $input->getOption('fromfile');
        if ($option) {
            $this->archiveFichierService->populateWithFile();
        } else {
            $this->archiveFichierService->populate();
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $output->writeln('Total Execution Time: ' . ceil($execution_time) . 's');

        return 0;
    }
}
