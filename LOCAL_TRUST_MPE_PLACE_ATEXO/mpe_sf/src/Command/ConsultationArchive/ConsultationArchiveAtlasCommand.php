<?php

namespace App\Command\ConsultationArchive;

use App\Service\ConsultationArchive\ConsultationArchiveAtlasService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConsultationArchiveAtlasCommand extends Command
{
    protected static $defaultName = 'consultation:archive:atlas';

    public function __construct(private ConsultationArchiveAtlasService $archiveAtlasService)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Alimente la table consultation_archive_atlas');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $time_start = microtime(true);

        $this->archiveAtlasService->setOutput($output);
        $this->archiveAtlasService->populate();
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        $output->writeln('Total Execution Time: '.ceil($execution_time).'s');
        return 0;
    }
}
