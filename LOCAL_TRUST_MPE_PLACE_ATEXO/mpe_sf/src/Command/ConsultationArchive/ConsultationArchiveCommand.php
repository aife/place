<?php

namespace App\Command\ConsultationArchive;

use App\Service\ConsultationArchive\ConsultationArchiveService;
use DateTime;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsultationArchiveCommand.
 */
class ConsultationArchiveCommand extends Command
{
    protected static $defaultName = 'consultation:archive';

    public function __construct(
        private readonly ConsultationArchiveService $consultationArchiveService
    ) {
        parent::__construct();
    }


    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setDescription('Alimente les tables consultation_archive et consultation_archive_bloc')
        ;
    }

    /**
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $timeStart = microtime(true);
        /** @var ConsultationArchiveService $service */
        $service = $this->consultationArchiveService;
        try {
            $limit = 3000;
            $offset = 0;
            $count = 0;
            $dateFormat = 'Y/m/s h:i:s';
            $countConsultationsArchivees = $service->getCountConsultationsArchivees();
            $debut = new DateTime();
            $output->writeln(str_repeat('-', 80));
            $output->writeln($debut->format($dateFormat));
            $output->writeln($countConsultationsArchivees." consultations au status 'ARCHIVEE' dans MPE");
            $output->writeln(str_repeat('-', 80));
            $service->setNewdate($debut);
            $service->statHead($debut);
            while (true) {
                ++$count;
                $consultations = $service->getConsultationsArchivees($offset, $limit);
                $service->populate($consultations);
                $offset += $limit;
                $date = new DateTime();
                $output->writeln($date->format($dateFormat));
                $msg = 'Mémoire utilisée: '.(memory_get_usage() / 1024).' Ko';
                $output->writeln($msg);
                $msg = "Consultations archivées : $offset traitées / $countConsultationsArchivees";
                $output->writeln($msg);
                $timeEnd = microtime(true);
                $executionTime = ($timeEnd - $timeStart);
                $output->writeln("Temps d'exécution: ".ceil($executionTime).' secondes');
                $output->writeln(str_repeat('-', 80));
                if ($offset > $countConsultationsArchivees) {
                    break;
                }
                unset($consultations);
            }
            $msg = 'Memory usage after: '.(memory_get_usage() / 1024).' KB';
            $output->writeln($msg);
            $msg = "$offset traités";
            $output->writeln($msg);
            $fin = new DateTime();
            $output->writeln($fin->format($dateFormat));

            $interval = $debut->diff($fin);
            $output->writeln('temps écoulé: '.$interval->format('%i minutes %s secondes'));
        } catch (Exception $e) {
            $output->writeln('General error: '.$e->getMessage());
        }
        return 0;
    }
}
