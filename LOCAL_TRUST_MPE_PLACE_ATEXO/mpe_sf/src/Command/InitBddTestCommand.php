<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CheckMailInvalidCommand.
 */
class InitBddTestCommand extends Command
{
    protected static $defaultName = 'mpe:bdd:init';
    protected $fileName;
    protected $emailConstraint;
    protected $limit;
    protected int $offset = 0;
    protected $validator;
    protected $doctrine;

    protected function configure()
    {
        $this->setDescription('Commande qui va initialise la base de donnée de donnée de test')
            ->addArgument('user', InputArgument::REQUIRED, 'User de la bdd')
            ->addArgument('pwd', InputArgument::REQUIRED, 'Mdp de la bdd')
            ->addArgument('folderDump', InputArgument::OPTIONAL, 'Chemin du dossier des données SQL (dump + data)')
            ->addArgument('pathMysql', InputArgument::OPTIONAL, 'Chemin du bin mysql')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $folderDump = '/opt/local-trust/apache/htdocs/mpe_place_test/mpe_sf/fixture/';
        $folderDumpArg = $input->getArgument('folderDump');
        if ($folderDumpArg) {
            $folderDump = $folderDumpArg;
        }

        $pathMysql = '/opt/local-trust/mysql/bin/mysql';
        $pathMysqlArg = $input->getArgument('pathMysql');
        if ($pathMysqlArg) {
            $pathMysql = $pathMysqlArg;
        }
        $user = $input->getArgument('user');
        $pwd = $input->getArgument('pwd');

        $cmd = $pathMysql.' -u'.$user.' -p'.$pwd;
        shell_exec($cmd.' -D place_test_unitaire_MPE_DB -e "DROP DATABASE place_test_unitaire_MPE_DB"');
        shell_exec($cmd.' < '.$folderDump.'place_test_MPE_DB_structure.sql');
        shell_exec($cmd.' < '.$folderDump.'place_test_MPE_DB_data.sql');
        shell_exec($cmd.' < '.$folderDump.'place_test_MPE_DB_consultation.sql');

        $out = shell_exec('pwd');

        $update = trim($out).'/../mpe/protected/var/scripts-sql/update/';
        if (is_dir($update)) {
            $files = scandir($update);
            foreach ($files as $file) {
                $path = $update.$file;
                $ext = pathinfo($path, PATHINFO_EXTENSION);
                if ('sql' === $ext) {
                    shell_exec($cmd.' place_test_unitaire_MPE_DB --force < '.$update.$file);
                }
            }
        }
        return 0;
    }
}
