<?php

namespace App\Command;

use App\Entity\Etablissement;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RedressementDonneeInscritCommand.
 */
class RedressementDonneeInscritCommand extends Command
{
    protected static $defaultName = 'mpe:inscrit:redressementEtablissement';

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Commande qui permet de redresser la colonne id_etablissement dans la table inscrit');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $doctrine = $this->em;

        $this->redressement($this->em);

        $listInscrits = $this->getListeInscritNoExistEtablissement($this->em);
        if ($listInscrits) {
            foreach ($listInscrits as $inscrit) {
                $inscrit->setIdEtablissement(0);
                $this->em->persist($inscrit);
                $this->em->flush();
            }
        }

        $this->redressement($this->em);
        return 0;
    }

    /**
     * @param $doctrine
     */
    private function redressement($doctrine)
    {
        $this->putRedressement($this->em, '1');
        $this->putRedressement($this->em, '0');
    }

    /**
     * @param $doctrine
     * @param $siege
     */
    private function putRedressement($doctrine, $siege)
    {
        $listInscrits = $this->getListeInscritARedresser($this->em);
        if ($listInscrits) {
            foreach ($listInscrits as $inscrit) {
                $this->redressementDonneeViaBDD($this->em, $inscrit, $siege);
            }
        }
    }

    /**
     * Récupéretion des inscrit a redresser.
     *
     * @param $doctrine
     * @param $limit
     *
     * @return mixed
     */
    protected function getListeInscritARedresser($doctrine)
    {
        $query = $this->em->getRepository(Inscrit::class)
            ->createQueryBuilder('i')
            ->where('i.idEtablissement = 0')
            ->OrWhere('i.idEtablissement is null')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * @param $doctrine
     *
     * @return mixed
     */
    protected function getListeInscritNoExistEtablissement($doctrine)
    {
        $query = $this->em->getRepository(Inscrit::class)
            ->createQueryBuilder('i')
            ->leftJoin(Etablissement::class, 'e', Join::WITH, 'e.idEtablissement = i.idEtablissement')
            ->Where('e.idEtablissement is null')
            ->andWhere('i.idEtablissement != 0')
            ->getQuery();

        return $query->getResult();
    }

    /**
     * Redressement via les données en BDD !
     *
     * @param $doctrine
     * @param $container
     * @param $limit
     */
    protected function redressementDonneeViaBDD($doctrine, Inscrit $inscrit, $estSiege = null)
    {
        $query = $this->em->getRepository(Etablissement::class)
            ->createQueryBuilder('e')
            ->where('e.idEntreprise = :idEntreprise')
            ->setParameter('idEntreprise', $inscrit->getEntrepriseId());
        if (null !== $estSiege) {
            $query = $query->andWhere('e.estSiege = :estSiege')
                ->setParameter('estSiege', $estSiege);
        }
        $query = $query->andWhere('e.statutActif = :statutActif')
            ->setParameter('statutActif', '1')
            ->setMaxResults('1')
            ->orderBy('e.idEtablissement', 'asc')
            ->getQuery();

        $etablissements = $query->getResult();

        if ((is_countable($etablissements) ? count($etablissements) : 0) > 0) {
            $inscrit->setEtablissement($etablissements[0]);
            $this->em->persist($inscrit);
            $this->em->flush();
        }
    }
}
