<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[AsCommand(
    name: 'mpe:purge-files:organisme',
    description: "Supprime l'ensemble des documents d'un organisme."
)]
class DeleteOrganismeFilesCommand extends Command
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $cliNasPurgeFileslogger,
        private readonly ParameterBagInterface $parameterBag,
        private bool $dryRun = true
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument('acronymeOrganisme', InputArgument::REQUIRED, "Acronyme de l'organisme")
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Mettre à false pour une véritable suppression',
                true
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $acronyme = $input->getArgument('acronymeOrganisme');
            $this->dryRun = !in_array($input->getOption('dry-run'), [0, '0', 'false', false], true);

            if ($this->dryRun) {
                $io->warning("Commande lancée en DRY RUN");
                $this->cliNasPurgeFileslogger->info("[DRY RUN] Lancement de la commande de suppression des documents d'un organisme");
            } else {
                $io->warning("Commande lancée en mode REEL");
                $this->cliNasPurgeFileslogger->info("Lancement de la commande de suppression des documents d'un organisme");
            }

            $isFileDeleted = false;
            $baseDir = $this->parameterBag->get('BASE_ROOT_DIR');


            $directDirectory = $baseDir . $acronyme;
            if (is_dir($directDirectory)) {
                $allFiles = $this->getRecursiveFiles($directDirectory, []);
                if (!empty($allFiles)) {
                    $isFileDeleted = true;
                    $this->deleteFiles($allFiles);
                    $io->title("Fichiers supprimés pour le répertoire {$acronyme} : ");
                    $io->listing($allFiles);
                    $this->cliNasPurgeFileslogger->info("Fichiers supprimés pour le répertoire {$acronyme} : ");
                    $this->logFiles($allFiles);
                }
                if (!$this->dryRun) {
                    rmdir($directDirectory);
                }
            }


            $scan = scandir($baseDir);
            foreach ($scan as $value) {
                if (!in_array($value, [".", ".."])) {
                    $filePath = $baseDir . '/' . $value;
                    if (is_dir($filePath) && strlen($value) == 4 && str_starts_with($value, '20')) {
                        $allFiles = $this->findOrganismeDirectory($filePath, $acronyme, []);
                        if (!empty($allFiles)) {
                            $isFileDeleted = true;
                            $io->title("Fichiers supprimés pour le répertoire {$value} : ");
                            $io->listing($allFiles);
                            $this->cliNasPurgeFileslogger->info("Fichiers supprimés pour le répertoire {$value} : ");
                            $this->logFiles($allFiles);
                        }
                    }
                }
            }

            if (!$isFileDeleted) {
                $io->title("Aucun fichiers à supprimer");
                $this->cliNasPurgeFileslogger->info("Aucun fichiers à supprimer");
            }

            $this->deleteBlobOrganismeFIles($acronyme);

            return Command::SUCCESS;
        } catch (\Exception $e) {
            $log = "ERROR Commande de suppression des documents d'un organisme : "
                . $e->getMessage() . " " . $e->getTraceAsString();
            $io->error($log);
            $this->cliNasPurgeFilesLogger->error($log);
            return Command::FAILURE;
        }
    }

    private function getRecursiveFiles(string $baseDir, array $filesToDelete): array
    {
        $scan = scandir($baseDir);

        foreach ($scan as $value) {
            if (!in_array($value, [".", ".."])) {
                $filePath = $baseDir . '/' . $value;
                if (is_file($filePath)) {
                    $filesToDelete[] = $filePath;
                } elseif (is_dir($filePath)) {
                    $filesToDelete = $this->getRecursiveFiles($filePath, $filesToDelete);
                }
            }
        }

        return $filesToDelete;
    }

    private function findOrganismeDirectory(string $baseDir, string $organisme, array $filesToDelete): array
    {
        $scan = scandir($baseDir);

        foreach ($scan as $value) {
            if (!in_array($value, [".", ".."])) {
                $filePath = $baseDir . '/' . $value;
                if (is_dir($filePath)) {
                    if ($value == $organisme) {
                        $filesToDelete = $this->getRecursiveFiles($filePath, $filesToDelete);
                        $this->deleteFiles($filesToDelete);
                        if (!$this->dryRun) {
                            rmdir($filePath);
                        }
                    } else {
                        $filesToDelete = $this->findOrganismeDirectory($filePath, $organisme, $filesToDelete);
                    }
                }
            }
        }

        return $filesToDelete;
    }

    private function logFiles(array $filesToDelete): void
    {
        foreach ($filesToDelete as $file) {
            $this->cliNasPurgeFileslogger->info(" * {$file}");
        }
    }

    private function deleteFiles(array $filesToDelete): void
    {
        if (!$this->dryRun) {
            foreach ($filesToDelete as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
    }

    private function deleteBlobOrganismeFIles(string $organisme): void
    {
        if (!$this->dryRun) {
            $now = new \DateTime();

            $this->entityManager->createQueryBuilder()
                ->update('App:BloborganismeFile', 'b')
                ->set('b.deletionDatetime', ':delDate')
                ->where('b.organisme = :organisme')
                ->setParameter('organisme', $organisme)
                ->setParameter('delDate', $now->format('Y-m-d H:i:s'))
                ->getQuery()->execute();
        }
    }
}
