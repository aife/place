<?php

namespace App\Command\Legacy;

use ReflectionMethod;
use Throwable;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\Util\TLogger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Lock\LockFactory;
use Symfony\Component\Lock\Store\SemaphoreStore;

class PradoCommand extends Command
{
    protected static $defaultName = 'prado:cli';
    public function __construct(private ParameterBagInterface $parameterBag, string $name = null)
    {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->addArgument('arguments', InputArgument::IS_ARRAY, '');
    }

    public function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $return_code = null;
        $store = new SemaphoreStore();
        $factory = new LockFactory($store);

        $command =  array_shift($_SERVER['argv']);

        $isPrado = false;
        if ('install' != $_SERVER['argv'][1]) {
            $isPrado = true;
        }
        if ($isPrado) {
            if ($this->parameterBag->get('TIMEZONE') !== date_default_timezone_get()) {
                date_default_timezone_set($this->parameterBag->get('TIMEZONE'));
            }
        } else {
            chdir(realpath(__DIR__ . '/../../../../'));
        }

        $command = ucfirst($_SERVER['argv'][1]);

        $file = 'Cli_' . $command;

        $className = 'Application\Cli\\' . $file;

        $lockFile = $factory->createLock($file);

        if ($lockFile->acquire()) {
            $return_code = 0;
            $cliFile = $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/legacy/protected/Cli/' . $command . '.php';

            if (is_file($cliFile)) {
                try {
                    $class = new ReflectionMethod($className, 'run');
                    if ($isPrado) {
                        Prado::log("Lancement commande $command ", TLogger::INFO, 'cli');
                    }

                    $time_start = microtime(true);
                    $class->invoke(null);
                    $time_end = microtime(true);
                    $time = round($time_end - $time_start, 4);
                    if ($isPrado) {
                        Prado::log(
                            "Fin commande $command in " . Atexo_Util::GetElapsedTime($time),
                            TLogger::INFO,
                            'cli'
                        );
                    }
                } catch (Throwable $t) {
                    if ($isPrado) {
                        Prado::log(
                            "Script $className interrompu ! Error : " . $t,
                            TLogger::FATAL,
                            'cli'
                        );
                    } else {
                        echo $t;
                    }
                    $return_code = 1;
                }

            } else {
                $lockFile->release();
            }
        }

        return (int) $return_code;
    }
}
