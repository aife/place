<?php

namespace App\Command;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use App\Entity\HistorisationMotDePasse;
use App\Entity\Inscrit;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InitHistorisationMotDePasseCommand.
 */
class InitHistorisationMotDePasseCommand extends Command
{
    protected static $defaultName = 'mpe:entreprise:initHistorisationMDP';
    protected function configure()
    {
        $this->setDescription('Commande qui permet d\'initialiser les mot de passe historisé ');
    }

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $inscrits = $this->em->getRepository(Inscrit::class)->findAll();

        $nbElements = is_countable($inscrits) ? count($inscrits) : 0;
        $nbInsertions = 0;

        foreach ($inscrits as $inscrit) {
            $this->initHistorisation($this->em, $inscrit);
            ++$nbInsertions;
        }

        $output->writeln(
            'Insertion de '.$nbInsertions.' lignes historisations pour un total de '.$nbElements.' inscrits'
        );
        return 0;
    }

    private function initHistorisation(EntityManagerInterface $em, Inscrit $inscrit)
    {
        try {
            $historisation = new HistorisationMotDePasse();
            $historisation->setAncienMotDePasse($inscrit->getMdp());
            $dt = new DateTime('01/01/1900 00:00:00');
            $historisation->setDateModification($dt);
            $historisation->setInscrit($inscrit);
            $em->persist($historisation);
            $em->flush($historisation);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
