<?php

/**
 * Created by PhpStorm.
 * User: Amal
 * Date: 25/04/2019
 * Time: 17:37.
 */

namespace App\Command;

use App\Service\CommandMonitoringService;
use DateTime;
use App\Entity\SsoAgent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Exception;

/**
 * Class PurgerSsoAgentCommand.
 */
class PurgerSsoAgentCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:ssoagent:purge';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function configure()
    {
        $this->setDescription('Supprimer les SsoAgent qui datent');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = null;
        try {
            $em = $this->em;
            $logger = $this->logger;
            $output->writeln('Start deleting');
            $dureeVieSsoAgent = $this->parameterBag->get('DUREE_VIE_SSO_AGENT');
            $em->getRepository(SsoAgent::class)->purge($dureeVieSsoAgent);
            $output->writeln('End deleting');
        } catch (Exception $e) {
            $erreur = 'Erreur PurgerSsoAgentCommand => ' . $e->getMessage();
            $logger->error($erreur);
        }
        return 0;
    }
}
