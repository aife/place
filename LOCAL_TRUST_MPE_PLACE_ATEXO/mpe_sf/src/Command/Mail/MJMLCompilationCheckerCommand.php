<?php

namespace App\Command\Mail;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Process;

class MJMLCompilationCheckerCommand extends Command
{
    protected static $defaultName = 'mpe:mjml:check';
    private const PRADO_ROOT = 'mpe_sf/legacy';
    private const SF_ROOT = 'mpe_sf';
    private const MJML_EXTENSION = '.mjml.twig';
    private const GIT_UNTRACKED_STATUS = 'Untracked files';
    private const GIT_NOT_STAGED_STATUS = 'Changes not staged for commit';

    protected string|bool $rootDir;

    public function __construct(string $rootDir = '')
    {
        if (empty($rootDir)) {
            $rootDir = realpath(__DIR__.'/../../../..');
        }
        $this->rootDir = rtrim($rootDir, '/').'/';

        parent::__construct();
    }

    /**
     * Configure command.
     */
    protected function configure()
    {
        $this->setDescription('Test si tous les templates sont compilés');
    }

    /**
     * Execute the command.
     *
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $missingFile = $this->checkTemplate();

        if (!empty($missingFile)) {
            $io->error($missingFile);

            return 1;
        } else {
            $io->success('Tous les templates sont présents');

            return 0;
        }
        return 0;
    }

    /**
     * Check if mjml is compiled and result is on git.
     */
    protected function checkTemplate(): array
    {
        $missingFiles = [];

        $finder = new Finder();
        $finder
            ->in($this->rootDir.self::PRADO_ROOT)
            ->in($this->rootDir.self::SF_ROOT)
            ->files()
            ->name('*'.self::MJML_EXTENSION);
        if ($finder->hasResults()) {
            foreach ($finder as $file) {
                $mjmlTemplate = $file->getRealPath();
                $htmlTemplate = preg_replace('/\.mjml\.twig$/', '.html.twig', $mjmlTemplate);

                // Check presence templaete
                if (!file_exists($htmlTemplate)) {
                    $missingFiles[] = "Template html manquant pour le template $mjmlTemplate";
                    continue;
                }

                // Check fichier sur git
                $error = $this->gitFileStatus($htmlTemplate);
                if (null !== $error) {
                    $missingFiles[] = $error;
                }
            }
        }

        return $missingFiles;
    }

    /**
     * Check file if file is added and staged.
     */
    protected function gitFileStatus(string $file): ?string
    {
        $pattern = '/^[a-z\-\/_.0-9]+('.
            str_replace('/', '\/', self::SF_ROOT).'\/|'.
            str_replace('/', '\/', self::PRADO_ROOT).'\/)/i';

        $git_path = preg_replace($pattern, '${1}', $file);
        $process = new Process(['cd', $this->rootDir, " && git status $git_path"]);
        $process->run();
        $fileStatus = $process->getOutput();
        if (strstr($fileStatus, self::GIT_UNTRACKED_STATUS)) {
            return "Le template $git_path n'est pas suivi par git";
        }
        if (strstr($fileStatus, self::GIT_NOT_STAGED_STATUS)) {
            return "Les modifications au template $git_path ne sont pas validées dans git";
        }

        return null;
    }
}
