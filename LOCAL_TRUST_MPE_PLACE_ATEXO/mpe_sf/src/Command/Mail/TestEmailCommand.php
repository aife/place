<?php

namespace App\Command\Mail;

use App\Service\Email\AtexoMailManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestEmailCommand extends Command
{
    protected static $defaultName = 'mpe:email:test';
    private $targetMail;

    /**
     * TestEmailCommand constructor.
     */
    public function __construct(private readonly iterable $mailServices, private ContainerInterface $container)
    {
        parent::__construct();
    }

    /**
     * Configure command.
     */
    protected function configure()
    {
        $this->setDescription('Test les emails')
            ->addArgument('targetMail', InputArgument::REQUIRED, 'Email cible')
            ->addArgument('service', InputArgument::OPTIONAL, 'Service à tester');
    }

    /**
     * Execute the command.
     *
     * @return int|void|null
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->targetMail = $input->getArgument('targetMail');
        $service = $input->getArgument('service');
        if ($service) {
            $this->testService($service);
        } else {
            $this->sendAll();
        }
        return 0;
    }

    /**
     * Test given service.
     */
    protected function testService(string $service)
    {
        $serviceInstance = $this->container->get($service);
        $this->send($serviceInstance);
    }

    /**
     * Test all services.
     */
    protected function sendAll()
    {
        foreach ($this->mailServices as $service) {
            $this->send($service);
        }
    }

    /**
     * Call sendMail method in given service.
     */
    protected function send(AtexoMailManager $service)
    {
        $testVars = $service->getTestVariables();
        if ($testVars) {
            $service->sendMail($this->targetMail, $testVars);
        }
    }
}
