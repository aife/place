<?php

/**
 * Commande pour envoyer les email AR de facon asynchrone.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */

namespace App\Command\Mail;

use App\Entity\Consultation;
use App\Entity\TMesRecherches;
use App\Listener\ResponseListener;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\Helper\EmailHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class SendEmailTestAlertEntrepriseCommand extends Command
{
    protected static $defaultName = 'test:mpe:alerte:entreprise';
    protected function configure()
    {
        $this->setDescription('TEST :  Envoie un mail de teste')
            ->addArgument(
                'ids',
                InputArgument::REQUIRED,
                'liste des ids. exemple : X,X,X'
            )
            ->addArgument(
                'idRecheche',
                InputArgument::REQUIRED,
                'id recherche'
            )
            ->addArgument(
                'mail',
                InputArgument::REQUIRED,
                'mail de l expediteur'
            )
        ;
    }

    /**
     * SendEmailARCommand constructor.
     */
    public function __construct(
        private readonly MailerInterface $mailer,
        protected EntityManagerInterface $entityManager,
        protected ResponseListener $responseListener,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EmailHelper $helper,
        private readonly PlateformeVirtuelleService $plateformeVirtuelle,
        private readonly TranslatorInterface $translator,
        string $name = null
    ) {
        parent::__construct($name);
    }

    /**
     * Permet d'executer la commande console.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $ids = $input->getArgument('ids');
        $idRecheche = $input->getArgument('idRecheche');
        $mail = $input->getArgument('mail');

        $em = $this->entityManager;

        $consultationsCollection = $em->getRepository(Consultation::class)->findBy([
            'id' => explode(',', $ids),
        ]);
        $context = $this->getContext($consultationsCollection);
        $alertMessage = $em->getRepository(TMesRecherches::class)->find($idRecheche);


        $emailCriteria['criteria'] = $this->helper->buildHtmlCriteriaForEmail(
            $alertMessage->getCriteria()
        );
        $context = array_merge($context, $this->getAlertContext($alertMessage));
        $typeAvis = $alertMessage->getTypeAvis() === $this->parameterBag->get('TYPE_AVIS_CONSULTATION')
            ? $this->translator->trans('TEXT_NOUVELLE_CONSULTATION')
            : $this->translator->trans('TEXT_NOUVELLE_ANNONCE');
        $subject = $context['pfLongName'].' - '.$typeAvis.' : '.count($consultationsCollection)
            .' - '.$alertMessage->getDenomination();
        $email = new TemplatedEmail();
        $email->from($context['from'])->to($mail)
            ->subject($subject)
            ->htmlTemplate('email/alert-entreprise.html.twig')
            ->textTemplate('email/alert-entreprise.txt.twig')
            ->context(array_merge($context, $emailCriteria));

        $this->mailer->send($email);


        return 0;
    }

    private function getContext(iterable $consultations): array
    {
        $context = [];
        $context['titleColor'] = $this->parameterBag->get('MAIL_COULEUR_TITRE');
        $context['imageBandeau'] = $this->helper->getImageBandeau();
        $context['urlImageBandeau'] = $this->helper->getUrlImageBandeau();
        $urlLocal = 'https://mpe-docker.local-trust.com';
        $urlExterne = 'https://simap-prep.b2g-test.etat.lu';
        $context['urlImageBandeau'] = str_replace($urlLocal, $urlExterne,  $context['urlImageBandeau']);
        $context['pfLongName'] = $this->parameterBag->get('PF_LONG_NAME');
        $context['from'] = $this->parameterBag->get('PF_MAIL_FROM');
        $context['pfUrlReference'] = $this->parameterBag->get('PF_URL_REFERENCE');
        $context['printPdfUrl'] = $context['pfUrlReference'] . '?page=Entreprise.GetMailFormatPdf&amp;idAlerte='
            . 'alerteId_' . uniqid();
        $context['consultations'] = $consultations;

        return $context;
    }

    private function getAlertContext(TMesRecherches $alert): array
    {
        $context = [];
        $context['alertName'] =  $alert->getDenomination();
        if (null !== $alert->getPlatformeVirtuelleId()) {
            $platform = $this->plateformeVirtuelle->getPlateformeVirtuelleById($alert->getPlatformeVirtuelleId());
            $context['from'] = $platform->getNoReply();
            $context['pfLongName'] = $platform->getFromPfName();
            $context['pfUrlReference'] = $platform->getProtocole() . '://' . $platform->getDomain();
            $context['imageBandeau'] = $this->helper->getImageBandeau($platform->getCodeDesign());
            $context['printPdfUrl'] = $context['pfUrlReference'] . '?page=Entreprise.GetMailFormatPdf&amp;idAlerte='
                . 'alerteId_' . uniqid();
        }

        return $context;
    }

}
