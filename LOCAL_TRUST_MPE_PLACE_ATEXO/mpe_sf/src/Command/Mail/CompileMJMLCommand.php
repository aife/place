<?php

namespace App\Command\Mail;

use App\Service\Email\MjmlManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class CompileMJMLCommand.
 */
class CompileMJMLCommand extends Command
{
    protected static $defaultName = 'mpe:mjml:compile';
    /**
     * CompileMJMLCommand constructor.
     */
    public function __construct(private readonly MjmlManager $mjmlManager)
    {
        parent::__construct();
    }

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setDescription('Compile un template MJML')
            ->setHelp('Valide et compile un template MJML')
            ->addArgument('file', InputArgument::REQUIRED, 'MJML template');
    }

    /**
     * Execute current command.
     *
     * @return int|void|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        if ($this->mjmlManager->compileMjml($input->getArgument('file'))) {
            $io->success(['Le fichier '.$input->getArgument('file').' a bien été compilé']);
        } else {
            $io->error(['Une erreur est survenue']);
        }
        return 0;
    }
}
