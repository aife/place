<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use App\Service\DataTransformer\SearchCriteriaTransformer;
use Exception;
use App\Entity\TMesRecherches;
use App\Message\AlertMessage;
use App\Repository\TMesRecherchesRepository;
use App\Service\CommandMonitoringService;
use App\Service\WebservicesMpeConsultations;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class AlertEntrepriseCommand extends MonitoredCommand
{
    use LockableTrait;

    final public const WEEKLY_ALERT = 'hebdo';
    final public const MAX_RESULTS_ITEMS = 100;
    protected static $defaultName = 'mpe:alert:entreprise';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly WebservicesMpeConsultations $wsConsultations,
        private readonly LoggerInterface $alertesentrepriseLogger,
        private readonly MessageBusInterface $messageBus,
        private readonly Stopwatch $stopwatch,
        private readonly CommandMonitoringService $commandMonitoringService
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function configure()
    {
        $this->setDescription('send alerts to Enterprises');
        $this->addOption(
            self::WEEKLY_ALERT,
            'hb',
            InputOption::VALUE_NONE,
            'Get consultations of last week'
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            if (!$this->lock()) {
                $output->writeln('Le script est en cours d\'execution ...');

                return 0;
            }
            $this->stopwatch->start('commandEvent', 'command');
            $output->writeln('Alerts Enterprises start sending');
            $criteriaRepository = $this->entityManager->getRepository(TMesRecherches::class);
            $criteria = $criteriaRepository->findSearchCriteriaByTypeCreateur(
                TMesRecherchesRepository::TYPE_CREATEUR_ENTREPRISE
            );
            $resultCount = 0;

            foreach ($criteria as $key => $criterion) {
                try {
                    $criterion = $this->filterKeywordsSearch($criterion);
                    $this->stopwatch->start('searchEvent' . $key, 'search');
                    $response = $this->wsConsultations->getContent(
                        'searchConsultationsForEntreprise',
                        array_merge(
                            $criterion->getCriteria(),
                            $this->getValidationDateCriterion($input->getOptions()[self::WEEKLY_ALERT])
                        )
                    );

                    if ($output->isDebug()) {
                        $this->alertesentrepriseLogger->info(
                            'Alert-email: Result of search with this criteria :',
                            [
                                'criteria' => array_merge(
                                    $criterion->getCriteria(),
                                    $this->getValidationDateCriterion($input->getOptions()[self::WEEKLY_ALERT])
                                ),
                                'result' => $response->{'hydra:member'}
                            ]
                        );
                    }

                    if (0 !== $response->{'hydra:totalItems'}) {
                        $consultations = $response->{'hydra:member'};
                        $alertsWithEmails = $criteriaRepository->findAlertsEmailsInscritsByHashedCriteria(
                            $criterion->getHashedCriteria()
                        );

                        if (is_countable($alertsWithEmails) && count($alertsWithEmails)) {
                            $this->messageBus->dispatch(new AlertMessage($consultations, $alertsWithEmails));
                        }
                        $resultCount += count($alertsWithEmails);
                        if ($output->isDebug()) {
                            $this->alertesentrepriseLogger->info(
                                'Alert-email: This result will be sent to this list of emails:',
                                ['emails:' => array_column($alertsWithEmails, 'email')]
                            );
                        }
                    }
                    $searchEvent = $this->stopwatch->stop('searchEvent' . $key);
                    $output->writeln(sprintf('Max memory usage and duration of %s', (string)$searchEvent));
                    $this->alertesentrepriseLogger->info(
                        sprintf(
                            'Alert-email: %s - max memory usage and duration of %s',
                            self::$defaultName,
                            (string)$searchEvent
                        ),
                        [
                            'criteria' => array_merge(
                                $criterion->getCriteria(),
                                $this->getValidationDateCriterion($input->getOptions()[self::WEEKLY_ALERT])
                            )
                        ]
                    );
                } catch (\Exception $exception) {
                    $this->alertesentrepriseLogger->error(
                        sprintf(
                            'Alert-email: Alert with hashed criteria %s returned with error',
                            $criterion->getHashedCriteria()
                        ),
                        [
                            'criteria' => array_merge(
                                $criterion->getCriteria(),
                                $this->getValidationDateCriterion($input->getOptions()[self::WEEKLY_ALERT])
                            ),
                            'error' => $exception->getMessage(),
                            'trace' => $exception->getTraceAsString()
                        ]
                    );
                }
            }
            $output->writeln(sprintf('%s alerts has returned search result', $resultCount));
            $output->writeln('Alert Enterprise Command finished');
            $commandEvent = $this->stopwatch->stop('commandEvent');
            $output->writeln(sprintf('Max memory usage and duration of %s', (string) $commandEvent));
            $this->alertesentrepriseLogger->info(
                sprintf(
                    'Alert-email: %s - max memory usage and duration of %s',
                    self::$defaultName,
                    (string) $commandEvent
                )
            );
            $this->release();

            return 0;
        } catch (Exception $exception) {
            $this->alertesentrepriseLogger->error(
                'Alert-email: Alert Enterprise command has stopped with error',
                ['error' => $exception->getMessage(), 'trace' => $exception->getTraceAsString()]
            );
            $output->writeln('Alert Enterprise command has stopped with error');
            $this->release();

            return (int) $exception->getCode();
        }
    }

    private function getValidationDateCriterion(bool $isHebdo): array
    {
        $criterion = [];
        $criterion['dateMiseEnLigneCalcule']['after'] = $isHebdo ? date("Y-m-d", strtotime(" -7 days"))
            : date("Y-m-d", strtotime("yesterday"));
        $criterion['dateMiseEnLigneCalcule']['before'] = date("Y-m-d", strtotime("today"));
        $criterion['itemsPerPage'] = self::MAX_RESULTS_ITEMS;

        return $criterion;
    }

    private function filterKeywordsSearch(TMesRecherches $criterion): TMesRecherches
    {
        if (!isset($criterion->getCriteria()['search_full'])) {
            return $criterion;
        }
        $keywords = [];

        foreach ($criterion->getCriteria()['search_full'] as $keyword) {
            $keyword = str_replace(SearchCriteriaTransformer::SPECIAL_CHARS_FILTER, '', $keyword);
            if (false === strstr(SearchCriteriaTransformer::SPECIAL_WORDS_FILTER, $keyword)) {
                $keywords[] = $keyword;
            }
        }

        $criterion->setCriteria(array_replace($criterion->getCriteria(), ['search_full' => $keywords]));

        return $criterion;
    }
}
