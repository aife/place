<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use SimpleXMLElement;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class SaveDocumentSizeCommand.
 */
class restoreFileHashCommand extends Command
{
    protected static $defaultName = 'docs:restore-file-hash';
    protected bool $message = false;
    protected int $numberFile = 1;
    protected int|bool $move = 0;
    protected $folder;
    protected $destFolder;

    public function __construct(private EntityManagerInterface $entityManager, private ParameterBagInterface $parameterBag)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $folderArg = 'folder';
        $folderArgDesc = 'Répertoire à analyser';
        $destFolder = 'destFolder';
        $message = 'message';
        $this->setDefinition([
            new InputOption($folderArg, '', InputOption::VALUE_REQUIRED, $folderArgDesc),
            new InputOption(
                $destFolder,
                '',
                InputOption::VALUE_REQUIRED,
                'Repertoire de destination de l\'export'
            ),
            new InputOption(
                'move',
                '',
                InputOption::VALUE_OPTIONAL,
                'Active le déplacement du fichier dans le dossier traiter'
            ),
            new InputOption($message, '', InputOption::VALUE_OPTIONAL, $folderArgDesc),
        ])
            ->setDescription('Script de vérification du hash')
            ->addArgument(
                $folderArg,
                InputArgument::REQUIRED,
                $folderArgDesc
            )
            ->addArgument(
                $destFolder,
                InputArgument::REQUIRED,
                'Repertoire de destination de l\'export'
            )
            ->addArgument(
                'move',
                InputArgument::OPTIONAL,
                'Active le déplacement du fichier dans le dossier traiter'
            )
            ->addArgument($message, InputArgument::OPTIONAL, 'Affiche message');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $message = $input->getArgument('message');
        if ($message) {
            $io->comment('Début de l\'analyse');
        }
        $this->folder = $input->getArgument('folder');
        if ($message) {
            $this->message = true;
        }

        if ('move' == $input->getArgument('move')) {
            $this->move = true;
        }

        $this->destFolder = $input->getArgument('destFolder');

        try {
            $repertoires = scandir($this->folder);
            $cpt = (is_countable($repertoires) ? count($repertoires) : 0) - 2;
            if ($this->message) {
                $io->block('Nombre fichier à traité :'.$cpt);
            }

            if ($cpt > 0) {
                $fp = fopen($this->destFolder.'listeFichierDechiffrer_'.
                    date('Y-m-d-H-i-s').'.csv', 'w+');
                $string = "idFichier,Hash,FichierSource,FichierFinal\n";
                fwrite($fp, $string);
                $fpC = fopen($this->destFolder.'listeFichierChiffrer_'.
                    date('Y-m-d-H-i-s').'.csv', 'w+');
                $string = "idOffre,Hash,FichierSource,FichierFinal\n";
                fwrite($fpC, $string);
                $this->parsingFolder($repertoires, $io, $output, $fp, $fpC);
                fclose($fp);
                fclose($fpC);
            }
        } catch (Exception $exception) {
            $io->write('Erreur :'.$exception->getMessage());
        }

        if ($message) {
            $io->comment('Fin de l\'analyse');
        }
        return 0;
    }

    private function parsingFolder($repertoires, SymfonyStyle $io, $output, $fp, $fpC)
    {
        foreach ($repertoires as $repertoire) {
            if ('.' != $repertoire && '..' != $repertoire) {
                $sourceDirectory = $this->folder.$repertoire;
                $files = scandir($sourceDirectory);
                $cpt = (is_countable($files) ? count($files) : 0) - 3;

                if ($cpt > 0) {
                    $repertoireTraiter = $sourceDirectory.'/traiter';

                    if (!is_dir($repertoireTraiter)) {
                        if (mkdir($repertoireTraiter, 0777)) {
                            chmod($repertoireTraiter, 0777);
                            $io->newLine(3);
                            $io->error('Impossible de créer le repertoire : '.$repertoireTraiter);
                            exit(1);
                        }
                    }

                    $io->section('Dechiffrer : traitement des fichiers dans le repertoire :'.$sourceDirectory);
                    $this->numberFile = $cpt;
                    $this->parsingFilesDechiffrer($files, $io, $sourceDirectory, $output, $fp);
                    $io->newLine(3);

                    $io->section('Chiffrer : traitement des fichiers dans le repertoire :'.$sourceDirectory);
                    $this->parsingFilesChiffrer($files, $io, $sourceDirectory, $output, $fpC);
                    $io->newLine(3);
                }
            }
        }
    }

    private function parsingFilesDechiffrer($files, SymfonyStyle $io, $repertoire, $output, $fp)
    {
        $progressBar = new ProgressBar($output, $this->numberFile);
        $progressBar->start();
        foreach ($files as $file) {
            if ('.' != $file && '..' != $file && 'traiter' != $file) {
                if ($this->message) {
                    $io->block('File => '.$repertoire.'/'.$file);
                }
                if (file_exists($repertoire.'/'.$file)) {
                    $hashCalcule = strtolower(sha1_file($repertoire.'/'.$file));

                    if ($this->message) {
                        $io->block('Hash :'.$hashCalcule);
                    }

                    $fichierEnveloppes = $this->entityManager
                        ->getRepository(EnveloppeFichier::class)
                        ->findByHash($hashCalcule);

                    if ((is_countable($fichierEnveloppes) ? count($fichierEnveloppes) : 0) > 0) {
                        foreach ($fichierEnveloppes as $fichierEnveloppe) {
                            if ($this->message) {
                                $io->block($fichierEnveloppe->getIdFichier());
                            }

                            if ($fichierEnveloppe->getBlob()) {
                                $string = $fichierEnveloppe->getIdFichier().','.
                                    $hashCalcule.','.
                                    $repertoire.'/'.$file.','.
                                    $this->parameterBag->get('BASE_ROOT_DIR').$fichierEnveloppe->getOrganisme().'/files/'.$fichierEnveloppe->getBlob()->getId().'-0';
                                fwrite($fp, $string."\n");
                            }
                        }
                    } else {
                        if ($this->message) {
                            $io->newLine(3);
                            $io->error('Hash suivant '.$hashCalcule.' du fichier '.$file.' non trouver !');
                        }
                    }
                } else {
                    $io->newLine(3);
                    $io->error('File manquant');
                }

                $progressBar->advance();
            }
        }

        $progressBar->finish();
    }

    private function parsingFilesChiffrer($files, SymfonyStyle $io, $repertoire, $output, $fp)
    {
        $progressBar = new ProgressBar($output, $this->numberFile);
        $progressBar->start();
        $baseRoot = $this->parameterBag->get('BASE_ROOT_DIR');
        $filesDirectory = '/files/';
        foreach ($files as $file) {
            if ('.' != $file && '..' != $file && 'traiter' != $file) {
                if ($this->message) {
                    $io->block('File => '.$repertoire.'/'.$file);
                }

                if (file_exists($repertoire.'/'.$file)) {
                    $hashCalcule = strtolower(sha1_file($repertoire.'/'.$file));

                    if ($this->message) {
                        $io->block('Hash :'.$hashCalcule);
                    }
                    $offres = $this->entityManager
                        ->getRepository(Offre::class)
                        ->searchHash($hashCalcule);
                    if ((is_countable($offres) ? count($offres) : 0) > 0) {
                        foreach ($offres as $offre) {
                            if ($this->message) {
                                $io->block($offre->getId());
                            }

                            if ($offre->getXmlString()) {
                                $xml = new SimpleXMLElement($offre->getXmlString());

                                foreach ($xml->Enveloppes->Enveloppe as $enveloppe) {
                                    foreach ($enveloppe->Fichier as $fichiers) {
                                        foreach ($fichiers->BlocsChiffrement as $BlocsChiffrement) {
                                            if ('1' == $BlocsChiffrement->attributes()->nbrBlocs) {
                                                if (strtolower($BlocsChiffrement->BlocChiffrement->attributes()->empreinte) == $hashCalcule) {
                                                    $nomBloc = (string) $BlocsChiffrement->BlocChiffrement->attributes()->nomBloc;
                                                    $string = $offre->getId().','.
                                                        $hashCalcule.','.
                                                        $repertoire.'/'.$file.','.
                                                        $baseRoot.
                                                        $offre->getOrganisme().$filesDirectory.
                                                        $nomBloc.'-0';
                                                    fwrite($fp, $string."\n");
                                                }
                                            } else {
                                                foreach ($BlocsChiffrement as $blocsChiffrement) {
                                                    if (strtolower($blocsChiffrement->attributes()->empreinte) == $hashCalcule) {
                                                        $nomBloc = (string) $BlocsChiffrement->BlocChiffrement->attributes()->nomBloc;
                                                        $string = $offre->getId().','.
                                                            $hashCalcule.','.
                                                            $repertoire.'/'.$file.','.
                                                            $baseRoot.
                                                            $offre->getOrganisme().$filesDirectory.
                                                            $nomBloc.'-0';
                                                        fwrite($fp, $string."\n");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($this->message) {
                            $io->error('Hash suivant '.$hashCalcule.' du fichier '.$file.' non trouver !');
                        }
                    }

                    try {
                        if (true == $this->move) {
                            rename($repertoire.'/'.$file, $repertoire.'/traiter/'.$file);
                        }
                    } catch (Exception $exception) {
                        $io->error('Error  :'.$exception->getMessage());
                    }
                } else {
                    if (!is_dir($file)) {
                        $io->error('File manquant');
                    }
                }

                $progressBar->advance();
            }
        }

        $progressBar->finish();
    }
}
