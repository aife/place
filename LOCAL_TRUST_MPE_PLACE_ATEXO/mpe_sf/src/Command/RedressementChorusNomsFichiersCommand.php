<?php

namespace App\Command;

use App\Entity\Chorus\ChorusNomsFichiers;
use App\Entity\ContratTitulaire;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class RedressementChorusNomsFichiersCommand.
 */
class RedressementChorusNomsFichiersCommand extends Command
{
    protected static $defaultName = 'mpe:chorus:redressement';
    protected function configure()
    {
        $this->setDescription('Commande qui permet de redresser la table chorus_noms_fichiers');
    }

    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $em = $this->em;

        $io = new SymfonyStyle($input, $output);

        $listChorusFichiers = $em->getRepository(ContratTitulaire::class)->getListeChorusEchangeInvalid();
        $nbElements = is_countable($listChorusFichiers) ? count($listChorusFichiers) : 0;

        if ($nbElements > 0) {
            $reponse = $io->choice(
                'Il y a plusieurs ligne à traiter, vous voulez les traiter ?',
                ['tout d\'un coup (attention: cela fait tout d\'un coup !)',
                        'unitairement (pour chaque ligne vous serez solicité)',
                        'aucune',
                    ],
                'aucune'
            );

            if ('tout d\'un coup (attention: cela fait tout d\'un coup !)' === $reponse) {
                $i = 1;
                $progressBar = new ProgressBar($output, $nbElements);
                foreach ($listChorusFichiers as $listChorusFichier) {
                    $this->insertionChorusNomFichier($listChorusFichier, $i, $em);
                    $progressBar->advance();
                    ++$i;
                }
                $progressBar->finish();
            } elseif ('unitairement (pour chaque ligne vous serez solicité)' === $reponse) {
                $this->redressementUnitaire($listChorusFichiers, $io, $em);
            } else {
                $io->error('Aucune traitement effectuer !');
            }
        } else {
            $io->success('Aucune donnée a triater.');
        }
        return 0;
    }

    private function insertionChorusNomFichier($data, $i, $em)
    {
        $chorusNomFichier = new ChorusNomsFichiers();
        $chorusNomFichier->setIdEchange($data['idContratTitulaire']);
        $chorusNomFichier->setAcronymeOrganisme($data['organisme']);
        $chorusNomFichier->setDateAjout(date('Y-m-d H:i:s'));
        $chorusNomFichier->setNumeroOrdre('1');
        $chorusNomFichier->setTypeFichier('FSO');
        $chorusNomFichier->setNomFichier('INSERTION_MANUELLE_'.date('YmdHis').'_'.$i);

        $em->persist($chorusNomFichier);
        $em->flush($chorusNomFichier);
    }

    private function redressementUnitaire($listChorusFichiers, $io, $em)
    {
        $i = 1;
        foreach ($listChorusFichiers as $listChorusFichier) {
            $io->comment('id_echange : '.$listChorusFichier['idContratTitulaire']);
            $io->comment('numero_ordre : 1');
            $io->comment('nom_fichier: INSERTION_MANUELLE_'.date('YmdHis').'_'.$i);
            $io->comment('acronyme_organisme:'.$listChorusFichier['organisme']);
            $io->comment('type_fichier: FSO');
            $io->comment('date_ajout:'.date('Y-m-d H:i:s'));

            if ($io->confirm('Voulez-vous rajouter la ligne  ci-dessous dans la table chorus_noms_fichiers ?')) {
                $this->insertionChorusNomFichier($listChorusFichier, $i, $em);
                $io->success('insersion ok');
            }

            ++$i;
        }
    }
}
