<?php

namespace App\Command;

use App\Service\CommandMonitoringService;
use DateTime;
use App\Entity\AccessToken;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class PurgerAccessTokenCommand.
 */
class PurgerAccessTokenCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:accesstoken:purge';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct($commandMonitoringService);
    }

    protected function configure()
    {
        $desc = 'Nombre de jours de durée de vie (ex: 100) (valeur par defaut définie par la config produit ou client)';
        $this->setDescription('Supprimer les AccessToken qui datent')
            ->addArgument(
                'dureeVie',
                InputArgument::OPTIONAL,
                $desc
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = null;
        try {
            $em = $this->em;
            $logger = $this->logger;
            $output->writeln('Start deleting');
            $dureeVieAccessToken = $this->parameterBag->get('DUREE_VIE_ACCESS_TOKEN_JOURS');
            $dureeVieArg = $input->getArgument('dureeVie');
            if (!empty($dureeVieArg)) {
                $dureeVieAccessToken = $dureeVieArg;
                if ((int) $dureeVieAccessToken < 1) {
                    $msg = 'La duree de vie de la table access_token doit être supérieure à 1 jour!';
                    throw new \Exception($msg);
                }
            }
            $em->getRepository(AccessToken::class)->purge($dureeVieAccessToken);
            $output->writeln('End deleting');
        } catch (\Exception $e) {
            $erreur = 'Erreur PurgerAccessTokenCommand => ' . $e->getMessage();
            $logger->error($erreur);
        }
        return 0;
    }
}
