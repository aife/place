<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class DedoublonnageXMLTraductionCommand.
 */
class DedoublonnageXMLTraductionCommand extends Command
{
    protected static $defaultName = 'mpe:traduction:dedoublonnage';
    protected function configure()
    {
        $this->setDescription('Commande qui permet de dédoublonner les fichiers de langues')
            ->addArgument('lang', InputArgument::OPTIONAL, 'choix de la langue (valeur par defaut : fr)');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $lang = 'fr';
        if ($input->getArgument('lang')) {
            $lang = $input->getArgument('lang');
        }

        $traductionFile = __DIR__.'/../../../../../mpe/protected/config/messages/messages.default.'.$lang.'.xml';

        $xml = simplexml_load_string(file_get_contents($traductionFile));
        $json = json_encode($xml, JSON_THROW_ON_ERROR);
        $array = json_decode($json, true, 512, JSON_THROW_ON_ERROR);

        $arr = $array['file']['body']['trans-unit'];
        $nbElementDoublon = count(array_diff_key($arr, array_unique($arr, SORT_REGULAR)));

        $bool = false;
        if ($nbElementDoublon > 0) {
            $bool = true;
        }

        $output->writeln($bool);
        return 0;
    }
}
