<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command;

use App\Entity\ModuleSatelliteMonitoring;
use App\Service\CommandMonitoringService;
use App\Service\Monitoring\MonitoringInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\Attribute\TaggedIterator;

class ServiceMonitoringCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:monitor:service';

    public function __construct(
        #[TaggedIterator(MonitoringInterface::class)] private iterable $monitoringServices,
        private CommandMonitoringService $commandMonitoringService,
        private EntityManagerInterface $em
    ) {
        parent::__construct($commandMonitoringService);
    }

    public function configure()
    {
        $this->setDescription('Monitoring one service');
        $this->addArgument('serviceName', InputArgument::REQUIRED);
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $serviceName = $input->getArgument('serviceName');
        foreach ($this->monitoringServices as $service) {
            if ($serviceName === get_class($service)) {
                $info = $service->check();
                $serviceMonitor = new ModuleSatelliteMonitoring();
                $serviceMonitor->setService($serviceName);
                $serviceMonitor->setJsonResponse($info);
                $this->em->persist($serviceMonitor);
                $this->em->flush();
            }
        }

        return 0;
    }
}
