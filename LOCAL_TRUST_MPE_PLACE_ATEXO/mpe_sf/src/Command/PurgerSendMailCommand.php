<?php

namespace App\Command;

use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use DateTime;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class PurgerSendMailCommand.
 */
class PurgerSendMailCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:sendmail:purge';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly CommandMonitoringService $commandMonitoringService
    ) {
        parent::__construct($commandMonitoringService);
    }

    /**
     * Configuration de la commande console.
     */
    protected function configure()
    {
        $this->setDescription('Permet de purger le dossier SendMail');
    }

    /**
     * Permet d'executer la commande console.
     *
     * @throws ORMException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $logger = null;
        try {
            $em = $this->em;
            $logger = $this->logger;
            $output->writeln('Start mpe:sendmail:purge');
            $dureeVieBackupSendMail = $this->parameterBag->get('DUREE_VIE_BACKUP_SEND_MAIL');
            $output->writeln('DUREE_VIE_BACKUP_SEND_MAIL => ' . $dureeVieBackupSendMail);
            $saveMailDir = rtrim($this->parameterBag->get('BASE_ROOT_DIR'), '/') . '/sendMail/';
            $cmd = 'find ' . $saveMailDir . ' -mindepth  1 -atime +' . $dureeVieBackupSendMail . ' -exec rm -rf {} \;';
            $output->writeln('cmd =>' . $cmd);
            system($cmd);
            $output->writeln('End mpe:sendmail:purge');
        } catch (\Exception $e) {
            $erreur = 'Erreur PurgerAccessTokenCommand => ' . $e->getMessage();
            $output->writeln('Erreur : ' . $erreur);
            $logger->error($erreur);
        }
        return 0;
    }
}
