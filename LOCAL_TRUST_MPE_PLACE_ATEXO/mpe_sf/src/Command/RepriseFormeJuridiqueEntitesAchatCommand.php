<?php

/**
 * Commande console de recuperation des exercices d'un etablissement
 *     => Recupere les exercices
 *     => Enregistre en base de donnees.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since ESR2018
 *
 * @copyright Atexo 2018
 */

namespace App\Command;

use App\Entity\Entreprise;
use App\Entity\Service;
use App\Service\AtexoService;
use App\Service\AtexoUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RepriseFormeJuridiqueEntitesAchatCommand extends Command
{
    protected static $defaultName = 'entiteAchat:synchronisation:formeJuridique';
    protected function configure()
    {
        $this->setDescription('Recupere les exercices de l\'etablissement via WS externe SGMAP')
        ;
    }

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoUtil $atexoUtil,
        private readonly AtexoService $atexoService,
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $log = '';
        $heureDebutExecution = date('H:i:s');
        $log .= "la récuperation de toute les entités d'achat ayant un siren\r\n";
        $entreprise_repo = $this->em->getRepository(Service::class);
        $entitesAchat = $entreprise_repo->getServicesAvecSiren();
        foreach ($entitesAchat as $entiteAchat) {
            if ($entiteAchat instanceof Service) {
                $siren = $entiteAchat->getSiren();
                $log .= "Début Traitement de l'entité d'achat dont l'id = " . $entiteAchat->getId() . ' et sigle = ' . $entiteAchat->getSigle() . "\r\n";
                $log .= 'La vérification de la validité du siren =  ' . $siren . "\r\n";
                $sirenValid = $this->atexoUtil->isValidSiren($siren);
                if ($sirenValid) {
                    $log .= "Le siren est valide \r\n";
                    $log .= "La synchronisation avec Api Gouv Entreprise \r\n";
                    $atexoEntiteAchat = $this->atexoService;
                    $serviceSgmap = $atexoEntiteAchat->recupererServiceApiGouvEntreprise($siren, $entiteAchat->getId());
                    if ($serviceSgmap instanceof Entreprise) {
                        $log .= "L'entité d'achat existe  ";
                        $entiteAchat->setFormeJuridique($serviceSgmap->getFormejuridique());
                        $entiteAchat->setFormeJuridiqueCode($serviceSgmap->getFormejuridiqueCode());
                        $log .= "Mettre à jour l'enté d'achat\r\n";
                        $entreprise_repo->updateService($entiteAchat);
                    } else {
                        $log .= "L'entité d'achat n'existe pas \r\n";
                    }
                } else {
                    $log .= "Le siren n'est pas valide \r\n";
                }
                $log .= "Fin Traitement de l'entité d'achat dont l'id = " . $entiteAchat->getId() . ' et sigle = ' . $entiteAchat->getSigle() . "\r\n";
            }
        }

        $output->writeln('cout : ' . (is_countable($entitesAchat) ? count($entitesAchat) : 0));
        $heureFinExecution = date('H:i:s');
        $output->writeln(<<<EOT

LOG : $log
Heure de début de l'execution : $heureDebutExecution
Heure de fin de l'execution   : $heureFinExecution
EOT
        );
        return 0;
    }
}
