<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Redressement;

use App\Entity\Organisme;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContrat;
use App\Entity\TypeContratEtTypeProcedure;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RedressementTypeContratEtProcedureCommand extends Command
{
    /**
     * @var string
     */
    protected static $defaultName = 'mpe:redressement:type_contrat_et_procedure';

    /**
     * @var
     */
    private $em;


    /**
     * GenerationDocumentRecuperationCommand constructor.
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
        parent::__construct();
    }

    /**
     * configurer la commande.
     */
    protected function configure()
    {
        $this
            ->setDescription(
                'Commande qui permet de redresser les procédure manquantes dans la table t_type_contrat_et_procedure.'
            );
    }

    /**
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $helper = $this->getHelper('question');
        /**
         * Saisie de l'id type contrat
         */
        $questionContrat = new Question(
            "Saisir l'id du type contrat ? (information qui se trouve dans la table t_type_contrat) : "
        );

        $questionContrat->setNormalizer(function ($value) {
            return $value ? trim($value) : '';
        });
        $questionContrat->setValidator(function ($idTypeContrat) {
            if ('' === $idTypeContrat) {
                throw new \Exception('Vous devez saisir un id');
            } elseif (!preg_match('/^\d+$/', $idTypeContrat)) {
                throw new \Exception('Cela doit être un entier');
            }

            $typeContrat = $this->em->getRepository(TypeContrat::class)->findOneByIdTypeContrat($idTypeContrat);
            if (!$typeContrat instanceof TypeContrat) {
                throw new \Exception('Type de contrat inconnue');
            }

            return $idTypeContrat;
        });
        $questionContrat->setMaxAttempts(20);
        $idTypeContrat = $helper->ask($input, $output, $questionContrat);




        /**
         * Saisie de l'organisme
         */
        $questionOrganisme = new Question(
            "Saisir le trigramme l'organisme ? (information qui se trouve dans la table Organisme) : "
        );

        $questionOrganisme->setNormalizer(function ($value) {
            return $value ? trim($value) : '';
        });
        $questionOrganisme->setValidator(function ($value) {
            if ('' === $value) {
                throw new \Exception('Vous devez saisir le trigramme');
            }

            $organisme = $this->em->getRepository(Organisme::class)->findOneByAcronyme($value);
            if (!$organisme instanceof Organisme) {
                throw new \Exception('Organisme inconnue');
            }

            return $value;
        });
        $questionOrganisme->setMaxAttempts(20);
        $trigramme = $helper->ask($input, $output, $questionOrganisme);
        $questionConfirmation = sprintf(
            "Voulez-vous lancer le redressement de la table t_type_contrat_et_procedure pour le type de contrat %s et l'organisme : %s ? ",
            $idTypeContrat,
            $trigramme
        );
        $confirmation = new ConfirmationQuestion(
            $questionConfirmation,
            false,
            '/^(y|j)/i'
        );
        if ($helper->ask($input, $output, $confirmation)) {
            //check type_procedure_organisme
            $typeProcedureOrganismes = $this->em->getRepository(TypeProcedureOrganisme::class)
            ->findBy([
                'organisme' => $trigramme
            ]);


            if (count($typeProcedureOrganismes) === 0) {
                return $io->error('Aucune procédure connue dans la table Type_Procedure_Organisme');
            }

            $io->section('Début de la mise à jour de la table t_type_contrat_et_procedure');
            foreach ($typeProcedureOrganismes as $typeProcedureOrganisme) {
                $typeContratEtTypeProcedure = $this->em->getRepository(TypeContratEtTypeProcedure::class)->findOneBy([
                    'idTypeContrat' => $idTypeContrat,
                    'organisme' => $trigramme,
                    'idTypeProcedure' => $typeProcedureOrganisme->getIdTypeProcedure()
                ]);
                if ($typeContratEtTypeProcedure instanceof TypeContratEtTypeProcedure) {
                    $io->warning(
                        sprintf(
                            'Association de la procédure : %s (%s), de l\'organsime %s et du type de contrat %s déjà présent.',
                            $typeProcedureOrganisme->getIdTypeProcedure(),
                            $typeProcedureOrganisme->getLibelleTypeProcedure(),
                            $trigramme,
                            $idTypeContrat
                        )
                    );
                    continue;
                }

                /**
                 * Insertion des données
                 */
                $insertionTypeContratEtTypeProcedure = new TypeContratEtTypeProcedure();
                $insertionTypeContratEtTypeProcedure->setIdTypeContrat($idTypeContrat);
                $insertionTypeContratEtTypeProcedure->setOrganisme($trigramme);
                $insertionTypeContratEtTypeProcedure->setIdTypeProcedure($typeProcedureOrganisme);

                $this->em->persist($insertionTypeContratEtTypeProcedure);
                $this->em->flush();

                $io->success(sprintf(
                    'Insertion de la procédure : %s (%s), de l\'organsime %s et du type de contrat %s.',
                    $typeProcedureOrganisme->getIdTypeProcedure(),
                    $typeProcedureOrganisme->getLibelleTypeProcedure(),
                    $trigramme,
                    $idTypeContrat
                ));
            }
            return $io->success('Mise à jour terminer');
        }

        return $io->error('Annulation de la reprise de données');
    }
}
