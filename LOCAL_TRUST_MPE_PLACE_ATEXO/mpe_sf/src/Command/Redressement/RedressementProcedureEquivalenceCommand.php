<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Redressement;

use App\Entity\Organisme;

use App\Entity\ProcedureEquivalence;
use App\Entity\TypeProcedure;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class RedressementProcedureEquivalenceCommand extends Command
{
    protected static $defaultName = 'mpe:redressement:procedure_equivalence';
    private $dryRun;
    private $organismeReference;

    public function __construct(
        private EntityManagerInterface $em
    ) {
        parent::__construct();
    }

    /**
     * configurer la commande.
     */
    protected function configure()
    {
        $this
            ->setDescription(
                'Commande qui permet de redresser les procédure manquantes dans la table procedure equivalence.'
            )
            ->addOption(
                'organisme-reference',
                null,
                InputArgument::OPTIONAL,
                'Organisme de ',
                'a1a'
            )
            ->addOption(
                'dry-run',
                null,
                InputArgument::OPTIONAL,
                'Dry run : Simulation l\'ajout des procédures équivalences',
                true
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->dryRun = (bool)$input->getOption('dry-run');
        $this->organismeReference = $input->getOption('organisme-reference');
        $io->block('Analyse des Procédure équivalence manquante');
        $io->info('Organisme de référence : '.$this->organismeReference);
        $organismes = $this->em->getRepository(Organisme::class)->findAll();
        $typeProcedures = $this->em->getRepository(TypeProcedure::class)->findAll();
        $io->progressStart(count($organismes));
        $table = [];
        foreach ($organismes as $organisme) {
            foreach ($typeProcedures as $typeProcedure) {
                $proceduresEquivalence = $this->em->getRepository(ProcedureEquivalence::class)->findOneBy(
                    [
                        'idTypeProcedure' => $typeProcedure->getIdTypeProcedure(),
                        'organisme' => $organisme->getAcronyme(),
                    ]
                );

                if (!$proceduresEquivalence instanceof ProcedureEquivalence) {
                    $proceduresEquivalenceReference = $this->em->getRepository(ProcedureEquivalence::class)->findOneBy(
                        [
                            'idTypeProcedure' => $typeProcedure->getIdTypeProcedure(),
                            'organisme' => $this->organismeReference,
                        ]
                    );

                    if ($proceduresEquivalenceReference instanceof ProcedureEquivalence) {

                        $table[] = [
                            $typeProcedure->getIdTypeProcedure(),
                            $typeProcedure->getLibelleTypeProcedure(),
                            $organisme->getAcronyme(),
                        ];

                        if (!$this->dryRun) {
                            $newProceduresEquivalence = clone $proceduresEquivalenceReference;
                            $newProceduresEquivalence->setOrganisme($organisme->getAcronyme());
                            $this->em->detach($newProceduresEquivalence);
                            $this->em->persist($newProceduresEquivalence);
                            $this->em->flush();
                        }

                    }
                }
            }

            $io->progressAdvance();
            $this->em->clear();
        }

        $io->progressFinish();
        $io->success('Résumer des insertions des procédures équivalences');

        $io->table([
            'Id type procédure',
            'Type procédure',
            'Organisme',
        ], $table);

        return 0;
    }
}
