<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use PDO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class FindUnusedDbColumnCommand.
 */
class FindUnusedDbColumnCommand extends Command
{
    protected static $defaultName = 'mpe:tools:find-unused-db-column';
    public const STATUT = 'statut';

    private $dbName;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Permet de trouver les colonnes non utilisées en base');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->initDbManager();
        $stmt = $this->em->getConnection()
            ->prepare("SELECT TABLE_NAME AS tableName, TABLE_ROWS as nbLignes FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_TYPE='BASE TABLE' AND TABLE_SCHEMA = '" . $this->dbName . "'");

        $stmt->execute();
        $finalArray = [];
        while ($table = $stmt->fetch(PDO::FETCH_OBJ)) {
            $tableName = $table->tableName;

            $sql = ' SELECT * FROM `' . $tableName . '` PROCEDURE ANALYSE()';

            $stmtDescribe = $this->em->getConnection()->prepare($sql);
            $stmtDescribe->execute();

            while ($colonnes = $stmtDescribe->fetch(PDO::FETCH_ASSOC)) {
                // retire ce champs car ce n'est pas pertinent pour notre analyse (et peut être très volumineux).
                unset($colonnes['Optimal_fieldtype']);
                $temp = $colonnes;
                $temp['client'] = $this->dbName;
                $temp['table'] = $tableName;
                $tempColonne = explode('.', (string) $colonnes['Field_name']);

                $temp['colonne'] = end($tempColonne);

                if (0 === intval($table->nbLignes)) {
                    $temp[self::STATUT] = 'Table vide';
                } elseif ($colonnes['Min_value'] === $colonnes['Max_value']) {
                    $temp[self::STATUT] = 'Suspect(MinValue=MaxValue)';
                } else {
                    $temp[self::STATUT] = 'OK';
                }

                $finalArray[] = $temp;
            }
        }
        $output->write(json_encode($finalArray, JSON_THROW_ON_ERROR));
        return 0;
    }

    private function initDbManager()
    {
        $this->dbName = $this->parameterBag->get('DATABASE_NAME');
    }
}
