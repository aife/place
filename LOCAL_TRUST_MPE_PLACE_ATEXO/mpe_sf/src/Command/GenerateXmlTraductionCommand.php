<?php

namespace App\Command;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use DOMXPath;
use DOMDocument;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class GenerateXmlTraductionCommand.
 */
class GenerateXmlTraductionCommand extends Command
{
    protected static $defaultName = 'mpe:traduction:generate-xlsx2xml';
    const ARGUMENT_LANGUE = 'langue';
    const ARGUMENT_CHEMIN = 'chemin';

    private $repertoireTraduction = null;

    protected function configure()
    {
        $this->setDefinition([
            new InputOption(
                self::ARGUMENT_LANGUE,
                '',
                InputOption::VALUE_REQUIRED,
                'Langue de traduction'
            ),
            new InputOption(
                self::ARGUMENT_CHEMIN,
                '',
                InputOption::VALUE_REQUIRED,
                'Chemin vers le fichier'
            ),
        ])
            ->setDescription("Permet de générer les traductions à partir d'un XLSX vers XML")
            ->addArgument(
                self::ARGUMENT_LANGUE,
                InputArgument::REQUIRED,
                " L'année pour laquelle on active la publication(valeur par defaut : l'année en cours)"
            )
            ->addArgument(
                self::ARGUMENT_CHEMIN,
                InputArgument::REQUIRED,
                ' Chemin vers le fichier'
            );
    }

    public function __construct(
        private readonly ParameterBagInterface $parameterBag
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->repertoireTraduction = $this->parameterBag->get('ATEXO_MPE_PATH').
            '/protected/config/messages';

        $pathFile = $input->getArgument(self::ARGUMENT_CHEMIN);
        $langue = $input->getArgument(self::ARGUMENT_LANGUE);
        $this->checkParam($output, $langue, $pathFile);
        $spreadsheet = $this->getSpreadsheet($pathFile);
        $data = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        //Suppression des headers
        unset($data[1]);
        $this->createXml($output, $data, $langue);
        return 0;
    }

    /**
     * Récupération du fichier via la librairie PHPSpreadsheet.
     *
     * @param $pathFile
     * @param string $format
     *
     * @return Spreadsheet
     *
     * @throws Exception
     */
    private function getSpreadsheet($pathFile, $format = 'Xlsx')
    {
        $reader = IOFactory::createReader($format);

        return $reader->load($pathFile);
    }

    private function createXml($output, $data, $langue)
    {
        $fileName = $this->repertoireTraduction.'/messages.default.'.$langue.'.xml';
        $dom = new DOMDocument();
        $dom->load($fileName);

        $xpath = new DOMXPath($dom);

        /*
         * Pour chaque ligne de mon fichier excel
         */
        foreach ($data as $key => $row) {
            $b = trim($row['B']);
            $a = trim($row['A']);

            // Si la clé est vide, on le log et on passe au suivant (non traitable)
            if (empty($a)) {
                $output->writeln('Ligne n°'.trim($key).' aucune clé');
                continue;
            }
            if (preg_match('/[^A-Z0-9_]/', $a)) {
                $output->writeln('La clé '.$a." n'est pas au format attendu");
                continue;
            }
            $node = $xpath->query("//trans-unit[@id = '".$a."']/target")->item(0);

            // Si la clé n'est pas connu dans notre référentiel, on le log et on passe au suivant
            // On ne veut pas ajouter des nouvelles clés automatiquement.
            if (!$node) {
                $output->writeln('La clé '.$a." n'est pas connu dans le référentiel");
                continue;
            }

            $target = $dom->createElement('target');
            $lib = '<![CDATA['.$b.']]>';
            $libTarget = $dom->createTextNode($b);
            $target->appendChild($libTarget);

            $node->parentNode->replaceChild($target, $node);
        }

        if (!$dom->save($fileName)) {
            $output->writeln("Le fichier n'est pas été enregistré");
        }

        return true;
    }

    /**
     * Vérifie les parcamètres d'entrées.
     *
     * @param $output
     * @param $langue
     * @param $pathFile
     *
     * @return bool
     */
    private function checkParam($output, $langue, $pathFile)
    {
        if (!in_array($langue, $this->getAvailableLanguage())) {
            $output->writeln("Cette langue n'est pas disponible");
            exit(1);
        }
        if (!file_exists($pathFile)) {
            $output->writeln("Le fichier source n'existe pas");
            exit(2);
        }

        $file_parts = pathinfo($pathFile);
        if ('xlsx' != strtolower($file_parts['extension'])) {
            $output->writeln("Le fichier n'est pas au format Xlsx");
            exit(3);
        }

        return true;
    }

    private function getAvailableLanguage()
    {
        $repertoires = scandir($this->repertoireTraduction);

        $languages = [];
        $matches = preg_grep('/messages.default.([a-z]{2}).xml/i', $repertoires);
        foreach ($matches as $match) {
            $temp = explode('.', (string) $match);
            $languages[] = $temp[2];
        }

        return $languages;
    }
}
