<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Tncp;

use App\Command\MonitoredCommand;
use Exception;
use App\Entity\Consultation;
use App\Event\PublicationConsultationEvent;
use App\Service\CommandMonitoringService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class PublishConsultationOnBusCommand extends MonitoredCommand
{
    protected static $defaultName = 'mpe:tncp:publish-consultation';

    public function __construct(
        private CommandMonitoringService $commandMonitoringService,
        private readonly EntityManagerInterface $entityManager,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly LoggerInterface $logger
    ) {
        parent::__construct($this->commandMonitoringService);
    }

    protected function configure()
    {
        $this->setDescription('Find unpublished consultations on TNCP and publish it.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $noPublishedConsultations = $this->entityManager->getRepository(Consultation::class)
                ->getUnpublishedConsultation();

            foreach ($noPublishedConsultations as $consultation) {
                $this->logger->info(sprintf('Consultation with id %s is published to bus.', $consultation['id']));
                $event = new PublicationConsultationEvent($consultation['id'], (int) $consultation['codeExterne']);
                $this->dispatcher->dispatch($event, PublicationConsultationEvent::class);
            }
            $output->writeln(sprintf('%s consultations is published on bus.', is_countable($noPublishedConsultations) ? count($noPublishedConsultations) : 0));

            return 0;
        } catch (Exception $exception) {
            $this->logger->error(
                'SearchCriteriaTransformerCommand has stopped with exception ',
                ['error' => $exception->getMessage(), 'trace' => $exception->getTraceAsString()]
            );

            return (int) $exception->getCode();
        }
    }
}
