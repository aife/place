<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Command\Tncp;

use App\Message\Tncp\Producer\PublishConsultation;
use DateTimeImmutable;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\MessageBusInterface;

class QueueTesterCommand extends Command
{
    protected static $defaultName = 'tncp:queue:test:sending';

    public function __construct(
        private readonly MessageBusInterface $messageBus
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Test dispatching messages to TNCP Kafka Queue');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $message = new PublishConsultation(
            self::$defaultName ?: '',
            'La valeur : ' . (new DateTimeImmutable())->format('c')
        );

        $this->messageBus->dispatch(new Envelope($message));

        return 0;
    }
}
