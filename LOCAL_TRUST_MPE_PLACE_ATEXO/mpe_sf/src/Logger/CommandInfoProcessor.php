<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Logger;

use Monolog\Processor\ProcessorInterface;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class CommandInfoProcessor implements ProcessorInterface, EventSubscriberInterface
{
    public function __construct(private ?string $commandName = null)
    {
    }

    public function __invoke(array $record): array
    {
        if (!empty($this->commandName)) {
            $record['extra']['Context']['command-name'] = $this->commandName;
            $record['extra']['Context']['user-type'] = 'Command';
        }

        return $record;
    }

    public function addCommandData(ConsoleEvent $event): void
    {
        $this->commandName = $event->getCommand()->getName();

        if ($event->getInput()->hasArgument('command-prado')) {
            $this->commandName = $event->getInput()->getArgument('command-prado');
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::COMMAND => ['addCommandData', 1],
        ];
    }
}
