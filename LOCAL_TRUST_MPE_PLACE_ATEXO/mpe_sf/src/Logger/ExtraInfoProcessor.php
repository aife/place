<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Logger;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Service\Version\VersionService;
use Monolog\Processor\ProcessorInterface;
use Symfony\Component\Security\Core\Security;

class ExtraInfoProcessor implements ProcessorInterface
{
    public function __construct(private readonly Security $security, private readonly VersionService $versionService)
    {
    }

    public function __invoke(array $record): array
    {
        $context = [];
        $user = $this->security->getUser();
        $context['user-type'] = match (true) {
            $user instanceof Agent => 'Agent',
            $user instanceof Inscrit => 'Inscrit',
            $user instanceof Administrateur => 'Admin',
            default => 'Anonyme'
        };

        if ($user instanceof Agent || $user instanceof Inscrit) {
            $context['user-login'] = $user->getLogin();
        }

        $context['mpe-version'] = $this->versionService->getApplicationVersion();
        if (!empty($record['extra']['Context'])) {
            $record['extra']['Context'] = array_merge($context, $record['extra']['Context']);
        } else {
            $record['extra']['Context'] = $context;
        }

        return $record;
    }
}
