<?php

namespace App\Service;

use App\Traits\ConfigProxyTrait;
use Zend\Http\Client\Adapter\Curl;
use Exception;
use App\Entity\Consultation;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Classe contenant des fonctions utiles.
 *
 * @author Amal EL BEKKAOUI <amal@atexo.com>
 * @copyright Atexo 2016
 *
 * @version 0
 *
 * @since   0
 */
class AtexoCandidatureMps
{
    use ConfigProxyTrait;

    /**
     * @var
     */
    private $container;

    /**
     * @var
     */
    private $session;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Constructeur de la classe.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     */
    public function __construct(ContainerInterface $container, SessionInterface $session)
    {
        $this->setContainer($container);
        $this->setSession($session);
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     */
    public function setSession($session)
    {
        $this->session = $session;
    }


    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->getContainer()->getParameter($name);
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getFromSession($name)
    {
        return $this->getSession()->get('contexte_authentification/' . $name);
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    protected function get($id)
    {
        return $this->getContainer()->get($id);
    }

    /**
     * methode qui permet de factoriser la construction de la variable appel_offre_id.
     */
    public function getAppelOffreId($consultation)
    {
        $consRefUser = null;
        $consRef = null;
        $consOrg = null;
        if ($consultation instanceof Consultation) {
            $consRefUser = $consultation->getReferenceUtilisateur();
            $consRef = $consultation->getId();
            $consOrg = $consultation->getAcronymeOrg();
        }

        return str_replace('.', '_', str_replace(' ', '', $this->get('atexo.atexo_util')->replaceCharactersMsWordWithRegularCharacters(substr($consRefUser, 0, 230) . '_' . $consRef . '_' . $consOrg . '_' . $this->container->getParameter('PF_SHORT_NAME'))));
    }
}
