<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface SimpleFileUploaderInterface
{
    public function upload(UploadedFile $file, string $to, ?string $fileName): void;
}
