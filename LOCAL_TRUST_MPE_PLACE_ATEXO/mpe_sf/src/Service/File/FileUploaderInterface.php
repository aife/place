<?php

namespace App\Service\File;

use Symfony\Component\HttpFoundation\File\UploadedFile;

interface FileUploaderInterface
{
    public function upload(UploadedFile $file): string;
    public function uploadAndDezip(UploadedFile $file): string;
    public function remove(string $fileName): void;
    public function removeFolder(string $dir): void;
    public function setTargetDirectory(string $target): void;
    public function getPath(string $fileName): string;
    public function getTargetDirectory(): string;
    public function getOriginalName(UploadedFile $file): string;
}