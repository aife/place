<?php

namespace App\Service\File;

use Exception;
use ZipArchive;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader implements FileUploaderInterface
{
    private readonly Filesystem $filesystem;

    /**
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger
    ) {
        $this->filesystem = new Filesystem();
    }

    private ?string $targetDirectory = null;

    public function upload(UploadedFile $file): string
    {
        $originalFilename = $this->getOriginalName($file) . '.' . $file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $originalFilename);
        } catch (FileException $e) {
            throw $e;
        }

        return $this->getPath($originalFilename);
    }

    /**
     * @throws Exception
     */
    public function uploadAndDezip(UploadedFile $file): string
    {

        try {
            $originalFilename = $this->getOriginalName($file);
            $fullName  = $originalFilename . '.' . $file->guessExtension();
            $path = $this->getTargetDirectory() . '/' . $fullName;
            $zip = new ZipArchive();

            $file->move($this->getTargetDirectory(), $fullName);

            $res = $zip->open($path);

            if (true === $res) {
                $zip->extractTo($this->getTargetDirectory());
                $zip->close();
                $this->remove($this->getPath($fullName));
            } else {
                throw new Exception('Echec de l\'extraction du fichier' . $fullName);
            }
        } catch (FileException $e) {
            throw $e;
        }

        return $this->targetDirectory . '/' . $originalFilename . '/index.html';
    }

    public function remove(string $fileName): void
    {
        $target = $this->parameterBag->get('BASE_ROOT_DIR') . $fileName;
        unlink($target);
    }

    public function removeFolder(string $dir): void
    {
        $target = $this->parameterBag->get('BASE_ROOT_DIR') . $dir;

        if (is_dir($target)) {
            $this->filesystem->remove($target);
        }
    }

    public function setTargetDirectory(string $target): void
    {
        $this->targetDirectory = $target;
    }

    public function getPath(string $fileName): string
    {
        return  $this->targetDirectory . '/' . $fileName;
    }

    public function getTargetDirectory(): string
    {
        return $this->parameterBag->get('BASE_ROOT_DIR') . $this->targetDirectory;
    }

    public function getOriginalName(UploadedFile $file): string
    {
        return pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
    }

    public function copyDirectoryToPublic(string $sourceDirectory = null, string $folderTarget = ''): void
    {
        try {
            $sourceDirectory = $sourceDirectory ?? $this->getTargetDirectory();
            $directoryPublic = $this->parameterBag->get('APP_BASE_ROOT_DIR') . 'mpe_sf/public/' . $folderTarget;

            $this->filesystem->mirror($sourceDirectory, $directoryPublic);
        } catch (Exception $exception) {
            $this->logger->alert($exception->getMessage());
        }
    }
}
