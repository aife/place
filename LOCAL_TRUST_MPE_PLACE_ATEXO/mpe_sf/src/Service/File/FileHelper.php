<?php

namespace App\Service\File;

use App\Entity\BloborganismeFile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @todo This class serves no purpose and should be deleted once the new DCE File upload is merged.
 */
class FileHelper
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function getFileByBlobId(int $blobId): ?string
    {
        $blobOrganismeFile = $this->entityManager->getRepository(BloborganismeFile::class)->find($blobId);
        if ($blobOrganismeFile instanceof BloborganismeFile) {
            $dir = $this->parameterBag->get('BASE_ROOT_DIR') .
                $blobOrganismeFile->getOrganisme() .
                "/files/";
            $filename = $blobOrganismeFile->getId() . '-0';
            if (file_exists($dir . $filename)) {
                return $dir . $filename;
            }

            $chemin = null;
            if ($blobOrganismeFile->getChemin() !== null) {
                $chemin = $blobOrganismeFile->getChemin();
                $dir = $this->parameterBag->get('BASE_ROOT_DIR') .
                    $blobOrganismeFile->getChemin();
            }

            if (file_exists($dir . $filename)) {
                return $dir . $filename;
            }

            $mnts = $this->parameterBag->get('DOCUMENT_ROOT_DIR_MULTIPLE');
            if ($mnts !== '') {
                $arrayMnts = explode(';', $mnts);
                foreach ($arrayMnts as $mnt) {
                    $dir = $mnt . $chemin;
                    if (file_exists($dir . $filename)) {
                        return $dir . $filename;
                    }
                }
            }
        }

        return null;
    }
}
