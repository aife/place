<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\File;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SimpleFileUploader implements SimpleFileUploaderInterface
{
    public function __construct(private LoggerInterface $logger)
    {
    }

    public function upload(UploadedFile $file, string $to, ?string $fileName): void
    {
        $fileName = $fileName ?? pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

        try {
            $file->move($to, $fileName);
        } catch (FileException $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
