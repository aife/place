<?php

/*
 * Class AtexoCertificat : classe de gestion des certificats
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Service;

use App\Entity\EnveloppeFichier;
use Psr\Container\ContainerInterface;

class AtexoCertificat
{
    /**
     * @var
     */
    private $fileCertificat;

    /**
     * AtexoCertificat constructor.
     *
     * @param $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        /**
         * @var
         */
        private ContainerInterface $container
    )
    {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getFileCertificat()
    {
        return $this->fileCertificat;
    }

    /**
     * @param mixed $fileCertificat
     */
    public function setFileCertificat($fileCertificat)
    {
        $this->fileCertificat = $fileCertificat;
    }

    /**
     * @param $certifFile
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getCnEmetteurCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer', 'CN');
    }

    /**
     * @param $certifFile
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getCnCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'CN');
    }

    /**
     * Parse un certificat x509 depuis un fichier et retourne la valeur d'un champ précisé en param.
     *
     * @param string $certificatFile Fichier sur disque du certificat x509
     * @param string $categorie      Indique la catégorie. Choix possible : subject, issuer, validTo_time_t
     * @param string $param          (optionnel) Indique le paramètre à afficher dans la catégorie
     *                               ou la catégorie si pas renseigné. Choix possible : C, L, OU, O, emailAddress, CN
     *
     * @return string La valeur du paramètre (retourne N/A si pas de valeur)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    protected function parseCertificatFromFile($certificatFile, $categorie, $param = '')
    {
        if (is_file($certificatFile)) {
            return self::parseCertificat(file_get_contents($certificatFile), $categorie, $param);
        } else {
            return 'N/A';
        }
    }

    /**
     * Parse un certificat x509 et retourne la valeur d'un champ précisé en param.
     *
     * @param string $certificat Certificat x509 (contenu dans une string)
     * @param string $categorie  Indique la catégorie. Choix possible : subject, issuer, validTo_time_t
     * @param string $param      (optionnel) Indique le paramètre à afficher dans la catégorie
     *                           ou la catégorie si pas renseigné. Choix possible : C, L, OU, O, emailAddress, CN
     *
     * @return string La valeur du paramètre (retourne N/A si pas de valeur)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function parseCertificat($certificat, $categorie, $param = '')
    {
        $x509 = openssl_x509_parse($certificat);
        if (is_array($x509)) {
            if ('' != $param) {
                $paramValue = $x509[$categorie][$param];
            } else {
                $paramValue = $x509[$categorie];
            }
            if (is_array($paramValue)) {
                $paramValue = str_replace("\x00", '', implode(', ', $paramValue));
            }

            return utf8_decode($paramValue);
        } else {
            return 'N/A';
        }
    }

    /**
     * @param bool $isFichierSigne
     * @param null $typeSignature
     *
     * @return string
     */
    public function getCnSubject(EnveloppeFichier $unFichier, $isFichierSigne = false, $typeSignature = null)
    {
        if ($isFichierSigne) {
            if ('XML' == $typeSignature) {
                $this->putCertificatInFile($unFichier, $typeSignature);
            } else {
                $this->putCertificatInFile($unFichier);
            }
            $cnCeritifcat = $this->getCnCertificatFromFile($this->fileCertificat);
            $this->unlinkCertificatFile();

            return $cnCeritifcat;
        }

        return '';
    }

    /**
     * @param bool $isFichierSigne
     * @param null $typeSignature
     *
     * @return string
     */
    public function getCnEmmeteur(EnveloppeFichier $unFichier, $isFichierSigne = false, $typeSignature = null)
    {
        if ($isFichierSigne) {
            if ('XML' == $typeSignature) {
                $this->putCertificatInFile($unFichier, $typeSignature);
            } else {
                $this->putCertificatInFile($unFichier);
            }
            $cnCeritifcat = $this->getCnEmetteurCertificatFromFile($this->fileCertificat);
            $this->unlinkCertificatFile();

            return $cnCeritifcat;
        }

        return '';
    }

    /**
     * @param null $typeSignature
     */
    private function putCertificatInFile(EnveloppeFichier $unFichier, $typeSignature = null)
    {
        $signature = null;
        $atexoBlobOrganisme = $this->container->get(AtexoFichierOrganisme::class);
        $contentSignature = $atexoBlobOrganisme->getContentSignatureFichier($unFichier);
        if ('' == $contentSignature) {
            $signature = $unFichier->getSignatureFichier();
        }

        if ('XML' == $typeSignature) {
            if ($signature) {
                $signature = base64_decode(trim($signature));
            }
        } else {
            if ($signature) {
                $signature = "-----BEGIN PKCS7-----\n".trim($signature)."\n-----END PKCS7-----";
            }
        }
        if ('' != $signature && null != $signature) {
            $this->fileCertificat = $this->container->get('atexo.authentication.atexo_crypto')
                ->getCertificatFromSignature($signature, false, true, $typeSignature);
        }
    }

    /**
     * @return void
     */
    private function unlinkCertificatFile()
    {
        if (is_file($this->fileCertificat)) {
            unlink($this->fileCertificat);
        }
    }
}
