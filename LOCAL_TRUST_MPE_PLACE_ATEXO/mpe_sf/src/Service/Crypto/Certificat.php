<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Crypto;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Certificat
{
    public function __construct(private readonly HttpClientInterface $client, private readonly LoggerInterface $cryptoLogger, private readonly ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCertificatDetails(string $certFilePath): array
    {
        $headers = [];
        $response = null;
        $urlCheckCertificat = $this->parameterBag->get('URL_CRYPTO') . '/signature/checkCertificat';
        $file = file_get_contents($certFilePath);

        if (empty($file)) {
            throw new FileNotFoundException('File ' . $certFilePath . ' not found');
        }

        $formFields = [
            'certificatBase64' => base64_encode($file),
            'originePlateforme' => $this->parameterBag->get('ORIGINE_PLATEFORME'),
            'origineOrganisme' => $this->parameterBag->get('ORIGINE_ORGANISME'),
            'origineContexteMetier' => 'MPE_chiffrement',
        ];

        $headers[] = 'Accept: application/json';

        try {
            $response = $this->client->request(
                'POST',
                $urlCheckCertificat,
                [
                    'headers' => $headers,
                    'body' => $formFields,
                ]
            );

            return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->cryptoLogger->error($response->getContent(false));
            throw $e;
        }
    }
}
