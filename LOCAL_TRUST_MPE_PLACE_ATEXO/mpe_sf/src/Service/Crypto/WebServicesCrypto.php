<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Crypto;

use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use Exception;
use App\Exception\UnexpectedResponseStatusException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class WebServicesCrypto
{
    public final const RETRYABLE_STATUS_CODE = [
        Response::HTTP_LOCKED,
        Response::HTTP_TOO_EARLY,
        Response::HTTP_TOO_MANY_REQUESTS,
        Response::HTTP_INTERNAL_SERVER_ERROR,
        Response::HTTP_BAD_GATEWAY,
        Response::HTTP_SERVICE_UNAVAILABLE,
        Response::HTTP_GATEWAY_TIMEOUT,
        Response::HTTP_INSUFFICIENT_STORAGE,
        Response::HTTP_NOT_EXTENDED
    ];

    public function __construct(
        public HttpClientInterface $httpClient,
        public LoggerInterface $cryptoLogger,
        public ParameterBagInterface $parameterBag,
    ) {
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     */
    public function prepAndSend(
        string $url,
        array $expectedCodes = [Response::HTTP_OK],
        ?string $verb = null,
        ?string $requestBody = null,
        bool $returnData = false,
        string $contentType = 'application/json',
        string $acceptType = 'application/json',
        array $headers = [],
        bool $retry = true
    ): bool|string {
        $verb = $this->getRequestVerb($verb);

        $this->cryptoLogger->info(
            sprintf(
                "WebServicesCrypto - Debut prepAndSend avec comme url ws : %s et verbe http : %s",
                $url,
                $verb
            )
        );

        $headers['Content-Type'] = $contentType;
        $headers['Accept'] = $acceptType;

        $options = [
            'headers' => $headers,
            'body' => $requestBody
        ];

        $this->cryptoLogger->info(
            sprintf(
                "WebServicesCrypto - body info Content-Type: %s, Accept: %s",
                $contentType,
                $acceptType,
            )
        );

        // Retryable Request
        $response = $this->executeRequestWithRetry(
            $verb,
            $url,
            $options,
            $retry
        );

        $statusCode = $response->getStatusCode();
        $responseBody = $response->getContent();

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($url, $statusCode, $responseBody);
        }

        $this->cryptoLogger->info(
            sprintf(
                "WebServicesCrypto - Fin WS prepAndSend avec comme url ws : %s et verbe http : %s",
                $url,
                $verb
            )
        );

        return true === $returnData ? $responseBody : true;
    }

    public function multipartRequestSend(
        string $url,
        array $expectedCodes = [Response::HTTP_OK, Response::HTTP_CREATED],
        string $verb = 'PUT',
        ?string $uploadedFilePath = null,
        bool $retry = true
    ): string {
        $verb = $this->getRequestVerb($verb);

        $this->cryptoLogger->info(
            sprintf(
                "WebServicesCrypto - Debut multipartRequestSend avec comme url ws : %s et verbe http : %s",
                $url,
                $verb
            )
        );

        // Uploading file in form body
        $formFields = [
            'description' => 'Sending file to check crypto',
            'fichier' => DataPart::fromPath($uploadedFilePath),
        ];

        $this->cryptoLogger->info(
            sprintf("WebServicesCrypto - Fichier de signature uploadé : %s ", $uploadedFilePath)
        );

        $formData = new FormDataPart($formFields);

        $headers = $formData->getPreparedHeaders()->toArray();

        $options = [
            'headers' => $headers,
            'body' => $formData->bodyToIterable(),
        ];

        // Retryable Request
        $response = $this->executeRequestWithRetry($verb, $url, $options, $retry);

        $statusCode = $response->getStatusCode();
        $responseBody = $response->getContent();

        if (!in_array($statusCode, $expectedCodes)) {
            $this->handleError($url, $statusCode, $responseBody);
        }

        $this->cryptoLogger->info(
            sprintf(
                "WebServicesCrypto - Fin multipartRequestSend avec comme url ws : %s et verbe http : %s",
                $url,
                $verb
            )
        );
        
        return $responseBody;
    }

    private function executeRequestWithRetry(
        string $verb,
        string $url,
        array $options,
        bool $retryCrypto = true
    ): ?ResponseInterface {
        $response = null;

        try {
            $max_tries = (int)$this->parameterBag->get('PARAM_HTTP_CLIENT_MAX_RETRY');
            $delay = (int)$this->parameterBag->get('PARAM_HTTP_CLIENT_DELAY') / 1000;

            $this->cryptoLogger->info(
                sprintf(
                    "WebServicesCrypto - Retry call API avec nombre max d'essai: %s et délai de: %ss",
                    $max_tries,
                    $delay,
                )
            );

            if ($retryCrypto) {
                $retry = 0;
                do {
                    if ($retry > 0) {
                        sleep($delay);

                        $this->cryptoLogger->warning(
                            sprintf(
                                "WebServicesCrypto - Essai de retry appel API numéro : %s",
                                $retry + 1
                            )
                        );
                    }

                    $response = $this->httpClient->request($verb, $url, $options);

                    $retry++;
                } while (
                    in_array($response->getStatusCode(), self::RETRYABLE_STATUS_CODE)
                    && $retry < $max_tries
                );
            } else {
                $response = $this->httpClient->request($verb, $url, $options);
            }
        } catch (\Exception $e) {
            $this->cryptoLogger->error($e->getMessage());
        } catch (TransportExceptionInterface $e) {
            $this->cryptoLogger->error($e->getMessage());
        }

        return $response;
    }

    private function getRequestVerb(string $verb): string
    {
        return match ($verb) {
            'POST_MP', 'POST_BIN' => 'POST',
            'PUT_MP', 'PUT_BIN' => 'PUT',
            default => $verb
        };
    }

    private function handleError(string $url, int $statusCode, $responseBody): never
    {
        $errorData = json_decode($responseBody, null, 512, JSON_THROW_ON_ERROR);

        $errorMessage = (empty($errorData->message))
            ? UnexpectedResponseStatusException::UNEXPECTED_STATUS
            : $errorData->message
        ;

        $logMessage = $errorMessage . " lors de l'appel au WS Crypto sur l'url %s qui retourne ce codeStatus %s";
        $this->cryptoLogger->error(sprintf($logMessage, $url, $statusCode));

        throw new UnexpectedResponseStatusException($errorMessage);
    }
}
