<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Crypto;
use Exception;
use HttpException;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Node\Param;
use Prado\Prado;
use Prado\Util\TLogger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\Translation\TranslatorInterface;

class CurlSend
{
    /**
     * CurlSend constructor.
     */
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    /**
     * permet d envoyer au serveur d horodatage la requete time stamping et de recuperer la reponse
     * retourne un tableau contenant le statut de la requete OPENSSL (0 si OK) et le chemin du fichier contenant le jeton d horodatage si OK ou l'erreur si échec.
     *
     * @param $req_file
     */
    public function sendToTsServer($req_file): array
    {
        $formvars = [];
        $submit_url = $this->parameterBag->get('TS_SERVER_ADDR');
        $trustDir = $this->parameterBag->get('TRUST_DIR');
        $formvars['platform_id'] = $this->parameterBag->get('PLATFORM_ID');

        $fp = fopen($req_file, 'r');
        $request = base64_encode(fread($fp, filesize($req_file)));
        $formvars['request'] = $request;
        fclose($fp);

        // initialisation de la session curl
        $ch = curl_init($submit_url);

        // options de transfert curl
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_CAPATH, $trustDir);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $formvars);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->parameterBag->get('CURLOPT_CONNECTTIMEOUT'));

        // envoi des parametres et recuperation de la reponse
        $pnp_result_page = curl_exec($ch);

        // erreur curl
        $errorNumber = curl_errno($ch);
        if (0 != $errorNumber) {
            $errorMsg = '';
            $errorMsg = curl_error($ch);
            $fp = fopen($this->parameterBag->get('COMMON_TMP').'tsr_error', 'w');
            fwrite($fp, $errorMsg);
            fclose($fp);
            $error = "CONNEXION_SERVEUR_HORODATAGE_ECHEC \n $errorMsg";

            return [1, $error];
        }

        // fermeture de la session curl
        curl_close($ch);

        $tab = explode('.tsq', (string) $req_file);
        $response_file = $tab[0].'.tsr';

        if (preg_match('/##DEBUT JETON HORODATAGE##/', (string) $pnp_result_page)) {
            $tab1 = explode('##DEBUT JETON HORODATAGE##', (string) $pnp_result_page);
            $tab2 = explode('##FIN JETON HORODATAGE##', $tab1[1]);
            $response = $tab2[0];
            $fp = fopen($response_file, 'w+');
            fwrite($fp, base64_decode($response));
            fclose($fp);

            return [0, $response_file];
        } else {
            $tab1 = explode('##DEBUT ERREUR##', (string) $pnp_result_page);
            $tab2 = explode('##FIN ERREUR##', $tab1[1]);
            $error = $tab2[0];

            return [2, $error];
        }
    }

    /**
     * @param $requestfile_path
     * @throws Exception
     */
    public function sendToServerLuxTrust($requestfile_path): array
    {
        if (!file_exists($requestfile_path)) {
            throw new HttpException('The Requestfile was not found');
        }

        $submit_url = $this->parameterBag->get('TS_SERVER_ADDR');
        $ch = curl_init($submit_url);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->parameterBag->get('CURLOPT_TIMEOUT'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($requestfile_path));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/timestamp-query']);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSLCERT, $this->parameterBag->get('PATH_FILE_CERTIFICAT'));
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $this->parameterBag->get('PASSWORD_CERTIFICATE'));
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
        //definition du proxy si existe
        $urlProxy = $this->parameterBag->get('URL_PROXY');
        $portProxy = $this->parameterBag->get('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $binary_response_string = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $tab = explode('.tsq', (string) $requestfile_path);
        $response_file = $tab[0].'.tsr';
        $fp = fopen($response_file, 'w+');
        fwrite($fp, $binary_response_string);
        fclose($fp);
        // erreur curl
        $errorNumber = curl_errno($ch);
        $error = "";
        if (0 != $errorNumber) {
            $errorMsg = curl_error($ch);
            $error = "CONNEXION_SERVEUR_HORODATAGE_ECHEC \n $errorMsg";

            return [1, $error];
        }

        curl_close($ch);

        if (200 != $status || !strlen((string) $binary_response_string)) {
            return [2, $error];
        } else {
            return [0, $response_file];
        }
    }

    /**
     * @param $postData
     */
    public function sendDataToModuleValidation($postData): bool|string
    {
        $submit_url = $this->parameterBag->get('URL_MODULE_VALIDATION_SIGNATURE_LOCAL');
        $ch = curl_init($submit_url);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->parameterBag->get('CURLOPT_TIMEOUT'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, $this->parameterBag->get('TUNNEL_PROXY'));

        //definition du proxy si existe
        $urlProxy = $this->parameterBag->get('URL_PROXY');
        $portProxy = $this->parameterBag->get('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;

        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $postResult = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur Curl : '.curl_error($ch);
        }

        return $postResult;
    }
}
