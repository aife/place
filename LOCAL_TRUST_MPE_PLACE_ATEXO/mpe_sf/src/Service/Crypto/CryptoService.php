<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Crypto;

use Exception;
use JsonException;
use DateTime;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\Om\BaseCommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Blob\Atexo_Blob_DbManagement;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Service;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use AtexoCrypto\Dto\InfosHorodatage;
use AtexoCrypto\Dto\InfosSignature;
use AtexoCrypto\Exception\ServiceException;
use Prado\Util\TLogger;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CryptoService
{
    /**
     * CryptoService constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private LoggerInterface $cryptoLogger,
        private readonly ContainerInterface $container
    ) {
    }

    public function generateCaSerialFile(): bool
    {
        $serial = $this->generateUniqueSerial();
        if ($fp = @fopen(
            $this->parameterBag->get('BASE_ROOT_DIR') . $this->parameterBag->get(
                'COMMON_DIRECTORY'
            ) . $this->parameterBag->get('CA_SERIAL'),
            'w'
        )) {
            if (!@fwrite($fp, $serial)) {
                @fclose($fp);

                return false;
            }
            return @fclose($fp);
        } else {
            return false;
        }
    }

    public function generateUniqueSerial(): string
    {
        $ko = true;
        do {
            $serial = '';
            for ($i = 1; $i <= 8; ++$i) {
                $number = random_int(0, 15);
                if (10 == $number) {
                    $serial .= 'A';
                } elseif (11 == $number) {
                    $serial .= 'B';
                } elseif (12 == $number) {
                    $serial .= 'C';
                } elseif (13 == $number) {
                    $serial .= 'D';
                } elseif (14 == $number) {
                    $serial .= 'E';
                } elseif (15 == $number) {
                    $serial .= 'F';
                } else {
                    $serial .= $number;
                }
            }
            if (!$this->isSerialExistInOffre($serial)) {
                $ko = false;
            }
        } while ($ko);

        return $serial;
    }

    public function isSerialExistInOffre(string $serial): bool
    {
        try {
            $c = new Criteria();
            $connexionCom = Propel::getConnection(
                $this->parameterBag->get('COMMON_DB') . $this->parameterBag->get('CONST_READ_ONLY')
            );
            $c->add(BaseCommonOffresPeer::UNTRUSTEDSERIAL, $serial);
            $offre = CommonOffresPeer::doSelect($c, $connexionCom);

            return (count($offre) > 0);
        } catch (Exception) {
            return false;
        }
    }

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function timeStampFile(string $infile): array|bool
    {
        //-- Horodatage du DCE --
        $result = false;
        if (is_file($infile)) {
            $result = $this->horodatageViaCrypto($infile);
        }

        return $result;
        //-- FIN Horodatage du DCE --
    }

    /**
     * @param $data
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function timeStampData($data): array|bool
    {
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $infile = $this->parameterBag->get('COMMON_TMP') . '/file_' . $token . session_name() . session_id() . time();
        if (!Atexo_Util::write_file($infile, $data)) {
            throw new Exception(
                'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get('COMMON_TMP') . '/'
            );
        }

        $timeStampVars = [];
        if (is_file($infile)) {
            $timeStampVars = $this->horodatageViaCrypto($infile);
        }

        return $timeStampVars;
    }

    /**
     * Récupère le timestamp à partir d'un jeton d'horodatage.
     *
     * @param $jetonHorodatage
     * @param string $format
     * ####################################################################################################
     * @todo :  Potentiellement du code MORT
     * ####################################################################################################*/
    public function retrieveDateFromHorodatage($jetonHorodatage, $format = 'iso'): false|string
    {
        if ('fr' == $format) {
            $formatDate = '%d/%m/%Y %H:%M';
        } else {
            $formatDate = '%Y-%m-%d %H:%M';
        }
        $timeStamp = '';
        $timeStampFileName = $this->parameterBag->get('COMMON_TMP') . 'horodatage' . session_id() . time();
        $timeStampFileNameText = $this->parameterBag->get('COMMON_TMP') . 'horodatage' . session_id() . time(
            ) . 'text_envoi_differe.tsr';
        if (!Atexo_Util::write_file($timeStampFileName, base64_decode($jetonHorodatage))) {
            return '';
        }

        // On vérifie le fichier
        $cmdTestHorodatage = $this->parameterBag->get('OPENSSL_TS') . ' ts -reply -in ' . $timeStampFileName . ' -text';

        system($cmdTestHorodatage . ' > ' . $timeStampFileNameText . ' 2>&1', $result);
        if (0 != $result) {
            //TODO message d'erreur
            return '';
        } else {
            $fp = fopen($timeStampFileNameText, 'r');
            $ts = fread($fp, filesize($timeStampFileNameText));
            fclose($fp);
            $msgs = explode("\n", (string) $ts);
            foreach ($msgs as $msg) {
                $msgTitle = explode('Time stamp:', $msg);
                if ($msgTitle[1]) {
                    $timeStamp = strftime($formatDate, strtotime(Atexo_Util::removeMilliSecond($msgTitle[1])));
                }
            }
        }
        unlink($timeStampFileName);
        unlink($timeStampFileNameText);

        return $timeStamp;
    }

    /**
     * @param $idFichier
     * @param $hash
     * @param null $organisme
     */
    public function verifyHashValidity($idFichier, $hash, $organisme = null): bool
    {
        $organisme = (empty($organisme)) ? Atexo_CurrentUser::getCurrentOrganism() : $organisme;
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idFichier, $organisme);
        $file = $blobResource['blob']['pointer'];
        $result = false;
        if ($file) {
            $dbManagement = new Atexo_Blob_DbManagement();
            $fileResult = $this->parameterBag->get('COMMON_TMP') . session_id() . time(
                ) . '_' . $idFichier . '_' . $organisme . '_sha1sum';
            system('sha1sum ' . $file . ' > ' . $fileResult . ' 2>&1', $verif);
            $this->cryptoLogger->info(
                ' Commande verification sha1sum ' . $file . ' > ' . $fileResult . ' 2>&1',
                TLogger::INFO,
                'Atexo'
            );
            if (0 === $verif) {
                $resultContent = $dbManagement->db_read_file($fileResult);
                $computedHash = substr($resultContent, 0, 40);
                if (0 == strcmp(strtoupper($computedHash), strtoupper($hash))) {
                    $result = true;
                    Atexo_Util::deleteLocalfile($fileResult);
                }
            }
        }

        return $result;
    }

    /**
     * * Verifie :
     * 1) la validité de la chaine de certificat
     * 2) Vérification la révoquation du certificat
     * 3) verification des dates.
     *
     * @param $signature
     * @param false $isFile
     * @param null $typeSignature
     * @param false $certificat
     * @return int[]
     */
    public function verifierCertificatDeSignature(
        $signature,
        $isFile = false,
        $typeSignature = null,
        $certificat = false
    ): array {
        if ('XML' == $typeSignature) {
            try {
                if ($certificat) {
                    $certificat = $signature;
                } else {
                    $certificat = (new Atexo_Crypto())->getCertificatFromSignature($signature, false, false, 'XML');
                }
                //Initialisation
                $arrayResultatVerification = [1, 1, 1];
                $cerificatFileName = $this->parameterBag->get('COMMON_TMP') . '/certificat_' . session_name(
                    ) . session_id() . time();
                $certificat = "-----BEGIN CERTIFICATE-----\n" . $certificat . "\n-----END CERTIFICATE-----";
                if (!Atexo_Util::write_file($cerificatFileName, $certificat)) {
                    throw new Exception(
                        'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get(
                            'COMMON_TMP'
                        ) . '/'
                    );
                }

                //verification de la chaine de certificat
                $arrayResultatVerification[0] = $this->verifyChaineCertf($cerificatFileName);

                //Verification de la Révocation
                $arrayResultatVerification[1] = $this->verifyRevocationCertif($cerificatFileName);

                //Dates de validité
                $arrayResultatVerification[2] = $this->verifyDateValiditeCertif($cerificatFileName);

                @unlink($cerificatFileName);

                return $arrayResultatVerification;
            } catch (Exception) {
                echo 'erreur dans la verification du certificat de signature.';
                exit;
            }
        } else {
            try {
                //Initialisation
                $arrayResultatVerification = [1, 1, 1];
                if ($isFile) {
                    $cerificatFileName = $this->getCertificatFromSignatureFile($signature);
                } else {
                    $cerificatFileName = $this->getCertificatFromSignature($signature);
                }

                //verification de la chaine de certificat
                $arrayResultatVerification[0] = $this->verifyChaineCertf($cerificatFileName);

                //Verification de la Révocation
                $arrayResultatVerification[1] = $this->verifyRevocationCertif($cerificatFileName);

                //Dates de validité
                $arrayResultatVerification[2] = $this->verifyDateValiditeCertif($cerificatFileName);

                @unlink($cerificatFileName);

                return $arrayResultatVerification;
            } catch (Exception) {
                echo 'erreur dans la verification du certificat de signature.';
                exit;
            }
        }
    }

    public function getCertificatFromSignature(
        $signature,
        $onErrorThrowException = true,
        $file = false,
        $typeSignature = null
    ) {
        if ('XML' == $typeSignature) {
            $certificatFileName = $this->parameterBag->get('COMMON_TMP') . '/certificat_' . session_name() . session_id(
                ) . time();

            $pos1 = strpos($signature, 'ds:X509Certificate');
            $pos2 = strpos($signature, '/ds:X509Certificate');
            $certificat = substr($signature, $pos1, $pos2 - $pos1);

            $pos1 = strpos($certificat, '>');
            $pos2 = strpos($certificat, '<');
            $certificat = substr($certificat, $pos1 + 1, $pos2 - $pos1 - 1);
            if ($file) {
                $certificat = "-----BEGIN CERTIFICATE-----\n" . $certificat . "\n-----END CERTIFICATE-----";
                if (!Atexo_Util::write_file($certificatFileName, $certificat)) {
                    throw new Exception(
                        'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get(
                            'COMMON_TMP'
                        ) . '/' . ' type signature XML, et certificatFileName est : ' . $certificatFileName . ' et certificat est : ' . $certificat
                    );
                }

                return $certificatFileName;
            } else {
                return $certificat;
            }
        } else {
            $signatureFileName = $this->parameterBag->get('COMMON_TMP') . '/signature_' . session_name() . session_id(
                ) . time();
            if (!Atexo_Util::write_file($signatureFileName, $signature)) {
                throw new Exception(
                    'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get(
                        'COMMON_TMP'
                    ) . '/' . ' type signature p7s, et signatureFileName est : ' . $signatureFileName . ' et signature est : ' . $signature
                );
            }
            $certificat = $this->getCertificatFromSignatureFile($signatureFileName, $onErrorThrowException);
            @unlink($signatureFileName);

            return $certificat;
        }
    }

    public function getCertificatFromSignatureFile($signatureFileName, $onErrorThrowException = true)
    {
        $certificatFileName = $this->parameterBag->get('COMMON_TMP') . '/certificat_' . session_name() . session_id(
            ) . time();
        $commande = $this->parameterBag->get('OPENSSL') . ' smime -verify -in ' . escapeshellarg(
                $signatureFileName
            ) . ' -noverify -nosigs -signer ' .
            $certificatFileName . ' -inform PEM -content /dev/null';
        system($commande, $resultat);
        //      @unlink($certificatFileName);
        if (0 == $resultat) {
            return $certificatFileName;
        } else {
            $content = file_get_contents($signatureFileName);
            $content = str_replace("-----BEGIN PKCS7-----\n", '', $content);
            $content = str_replace("\n-----END PKCS7-----", '', $content);
            if (!Atexo_Util::write_file($signatureFileName . '_der', $content)) {
                throw new Exception(
                    'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get(
                        'COMMON_TMP'
                    ) . '/'
                );
            }
            $commande = $this->parameterBag->get('OPENSSL') . ' smime -verify -in ' . escapeshellarg(
                    $signatureFileName . '_der'
                ) . ' -noverify -nosigs -signer ' . $certificatFileName . ' -inform DER -content /dev/null';
            system($commande, $resultat);
            if (0 == $resultat) {
                return $certificatFileName;
            }
        }
        if ($onErrorThrowException) {
            return 'Error';
            //throw new Exception($commande." Impossible d'obtenir le certificat à partir de la signature");
        } else {
            return 'Error';
        }
    }

    public function verifySignatureValidityFromFiles($file, $signatureFile)
    {
        $signer = $this->parameterBag->get('COMMON_TMP') . session_id() . time() . '_signer';

        $verifyNocerts = $this->parameterBag->get('OPENSSL') . ' smime -verify -noverify -inform PEM -signer ' . $signer
            . ' -in  ' . escapeshellarg($signatureFile) . ' -content ' . escapeshellarg(
                str_replace(' ', '\ ', $file)
            ) . ' -out /dev/null'; // faire plutôt un shellescapeargs()
        $command = $verifyNocerts;
        $destination = $this->parameterBag->get('COMMON_TMP') . session_id() . time() . '_verify_error';
        system($command . '  > ' . $destination . ' 2>&1', $result);
        // unlink($pkcs7File);
        if (0 != $result) {
            $content = file_get_contents($signatureFile);

            $content = str_replace("-----BEGIN PKCS7-----\n", '', $content);
            $content = str_replace("\n-----END PKCS7-----", '', $content);
            if (!Atexo_Util::write_file($signatureFile . '_der', $content)) {
                throw new Exception(
                    'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get(
                        'COMMON_TMP'
                    ) . '/'
                );
            }
            $verifyNocerts = $this->parameterBag->get(
                    'OPENSSL'
                ) . ' smime -verify -noverify -inform DER -signer ' . $signer
                . ' -in  ' . escapeshellarg($signatureFile . '_der') . ' -content ' . escapeshellarg(
                    str_replace(' ', '\ ', $file)
                ) . ' -out /dev/null'; // faire plutôt un shellescapeargs()
            $command = $verifyNocerts;
            $destination = $this->parameterBag->get('COMMON_TMP') . session_id() . time() . '_verify_error';
            system($command . '  > ' . $destination . ' 2>&1', $result);
        }

        return $result;
    }

    /**
     * Verifie l'horodatage du fichier.
     *
     * @param : $jeton : jeton d'horodatage : en base64 ou un fichier
     */
    public function verifyHorodatage($jeton, $fileOrString)
    {
        $file = null;
        $deleteJeton = false;
        $deleteFile = false;
        if (!is_file($jeton)) {
            $donneesJeton = $jeton;
            $jeton = $this->parameterBag->get('COMMON_TMP') . '/jeton_' . session_name() . session_id() . time(
                ) . '.tsr';
            Atexo_Util::write_file($jeton, base64_decode($donneesJeton));
            $deleteJeton = true;
        }
        if (!is_file($fileOrString)) {
            $string = $fileOrString;
            $file = $this->parameterBag->get('COMMON_TMP') . '/file_' . session_name() . session_id() . time() . '';
            Atexo_Util::write_file($file, $string);
            $deleteFile = true;
        }
        $verify = $this->parameterBag->get(
                'OPENSSL_TS'
            ) . ' ts -verify -data ' . $file . ' -in ' . $jeton . ' -CApath ' . $this->parameterBag->get('TRUST_DIR');
        $verifyTsrTsvErrorFile = $this->parameterBag->get('COMMON_TMP') . '/verify_tsr_tsv_error';
        system($verify . '  > ' . $verifyTsrTsvErrorFile . ' 2>&1', $tsVerify);
        if ($deleteJeton && is_file($jeton)) {
            unlink($jeton);
        }
        if ($deleteFile && is_file($file)) {
            unlink($file);
        }

        if (0 != $tsVerify) {
            return false;
        } elseif (is_file($verifyTsrTsvErrorFile)) {
            unlink($verifyTsrTsvErrorFile);
        }

        return true;
    }

    /**
     * Permet de retourner le hash du chemin d'un fichier identifié par $idBlob.
     */
    public function hashBlobFile($idBlob)
    {
        $Org = Atexo_CurrentUser::getCurrentOrganism();
        $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $Org);
        $blob_file = $blobResource['blob']['pointer'];
        if (is_file($blob_file)) {
            return sha1_file($blob_file);
        }
    }

    private function generateTSR($infile, $PathOrgOpensslConfig)
    {
        // Reponse ts
        $pathOrgTmp = $this->parameterBag->get('COMMON_TMP');
        $reply = $this->parameterBag->get(
                'OPENSSL_TS'
            ) . ' ts -reply -queryfile ' . $infile . '.tsq -config ' . $PathOrgOpensslConfig .
            ' -signer ' . $this->parameterBag->get('TS_CERT') . ' -inkey ' . $this->parameterBag->get(
                'TS_PKEY'
            ) . " -out $infile.tsr -section " .
            $this->parameterBag->get('TSA_SECTION') . $this->parameterBag->get('OPENSSL_ENGINE');
        system($reply . '  > ' . $pathOrgTmp . 'tsr_error 2>&1', $tsReply);
        if (0 != $tsReply) {
            $fp = fopen($pathOrgTmp . 'tsr_error', 'r');
            fread($fp, filesize($pathOrgTmp . 'tsr_error'));
            fclose($fp);
            $this->cryptoLogger->info("création du jeton d'horodatage tsr reply " . $reply, TLogger::ERROR, 'Atexo');

            return [$tsReply, null];
        }
    }

    /**
     * @return bool|int|void
     * @throws Exception
     */
    public function generateCompanyPkiOpensslConfigFile()
    {
        if (!is_file($this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_CONFIG_TEMPLATE'))) {
            throw new Exception('Fichier de configuration modele introuvable');
        }
        $strTemplate = file_get_contents($this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_CONFIG_TEMPLATE'));
        $strTemplate = str_replace('__DIR__', $this->parameterBag->get('DIR_PKI_ENTREPRISE'), $strTemplate);
        $strTemplate = str_replace('__CERTS_DB__', $this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_DB'), $strTemplate);
        $strTemplate = str_replace(
            '__CERT_PATH__',
            $this->parameterBag->get('BASE_ROOT_DIR') . $this->parameterBag->get(
                'COMMON_DIRECTORY'
            ) . $this->parameterBag->get('MARCHES_CA_CERT'),
            $strTemplate
        );
        $strTemplate = str_replace(
            '__CERT_PKEY__',
            $this->parameterBag->get('BASE_ROOT_DIR') . $this->parameterBag->get(
                'COMMON_DIRECTORY'
            ) . $this->parameterBag->get('MARCHES_CA_KEY'),
            $strTemplate
        );

        return file_put_contents($this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_CONFIG'), $strTemplate);
    }

    public function generateCompanyPkiOpensslDbFile(): bool
    {
        if (!is_file($this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_DB'))) {
            return touch($this->parameterBag->get('PKI_ENTREPRISE_OPENSSL_DB'));
        }

        return true;
    }

    /**
     * @param $certificatFilePath
     */
    public function getSerialFromCertif($certificatFilePath): false|string
    {
        $command = $this->parameterBag->get('OPENSSL') . ' x509 -in ' . escapeshellarg(
                $certificatFilePath
            ) . ' -noout -serial';
        $verificationResultatFile = $this->parameterBag->get('COMMON_TMP') . '/' . 'chaineVerif_' . session_name(
            ) . session_id() . time();
        system($command . ' > ' . $verificationResultatFile . ' 2>&1', $res);
        if (!$res) {
            return $verificationResultatFile;
        }

        return false;
    }

    /**
     * @param $cerificatFileName
     */
    public function verifyChaineCertf($cerificatFileName): int
    {
        //verification de la chaine de certificat
        $commande = $this->parameterBag->get('OPENSSL') . ' verify -CApath ' . $this->parameterBag->get(
                'TRUST_DIR'
            ) . '  ' . escapeshellarg($cerificatFileName);
        $verificationResultatFile = $this->parameterBag->get('COMMON_TMP') . '/' . 'chaineVerif_' . session_name(
            ) . session_id() . time();
        system($commande . ' > ' . $verificationResultatFile . ' 2>&1');

        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $pos1 = strpos(str_replace(' ', '', $resultatVerification), (string)str_replace(' ', '', $cerificatFileName));
        $pos2 = strpos(str_replace(' ', '', $resultatVerification), 'OK');
        if (trim((str_replace(' ', '', $resultatVerification))) == trim(
                (str_replace(' ', '', $cerificatFileName) . ':OK')
            ) || (false !== $pos1 && false !== $pos2)) {
            $verif = 0;
        } else {
            //verification de la chaine de certificat dans TRUST_DIR_ALTERNATE
            $commandeAlternate = $this->parameterBag->get('OPENSSL') . ' verify -CApath ' . $this->parameterBag->get(
                    'TRUST_DIR_ALTERNATE'
                ) . '  ' . escapeshellarg($cerificatFileName);
            $verificationResultatAlternateFile = $this->parameterBag->get(
                    'COMMON_TMP'
                ) . '/' . 'chaineVerifAlternate_' . session_name() . session_id() . time();
            system($commandeAlternate . ' > ' . $verificationResultatAlternateFile . ' 2>&1');

            $fpAlternate = fopen($verificationResultatAlternateFile, 'r');
            $resultatVerificationAlternate = fread($fpAlternate, filesize($verificationResultatAlternateFile));
            fclose($fpAlternate);
            $pos1 = strpos(
                str_replace(' ', '', $resultatVerificationAlternate),
                (string)str_replace(' ', '', $cerificatFileName)
            );
            $pos2 = strpos(str_replace(' ', '', $resultatVerificationAlternate), 'OK');
            if (trim((str_replace(' ', '', $resultatVerificationAlternate))) == trim(
                    (str_replace(' ', '', $cerificatFileName) . ':OK')
                ) || (false !== $pos1 && false !== $pos2)) {
                $verif = 0;
            } else {
                $verif = 2;
            }
            unlink($verificationResultatAlternateFile);
        }

        unlink($verificationResultatFile);

        return $verif;
    }

    /**
     * @param $cerificatFileName
     */
    public function verifyRevocationCertif($cerificatFileName): int
    {
        //Verification de la Révocation
        $verificationResultatFile = $this->parameterBag->get('COMMON_TMP') . '/' . 'revoqueVerif_' . session_name(
            ) . session_id() . time();
        $commande = $this->parameterBag->get('OPENSSL') . ' verify -CApath ' . $this->parameterBag->get(
                'TRUST_DIR'
            ) . ' -crl_check ' . escapeshellarg($cerificatFileName);
        system($commande . ' > ' . $verificationResultatFile . ' 2>&1');
        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $pos1 = strpos(str_replace(' ', '', $resultatVerification), (string)str_replace(' ', '', $cerificatFileName));
        $pos2 = strpos(str_replace(' ', '', $resultatVerification), 'OK');
        if (strstr($resultatVerification, 'certificate revoked')) {
            $resVerif = 2;
        } elseif (trim((str_replace(' ', '', $resultatVerification))) == trim(
                (str_replace(' ', '', $cerificatFileName) . ':OK')
            ) || (false !== $pos1 && false !== $pos2)) {
            $resVerif = 0;
        } else {
            //verification de la Révocation dans TRUST_DIR_ALTERNATE
            $commandeAlternate = $this->parameterBag->get('OPENSSL') . ' verify -CApath ' . $this->parameterBag->get(
                    'TRUST_DIR_ALTERNATE'
                ) . ' -crl_check ' . escapeshellarg($cerificatFileName);
            $verificationResultatAlternateFile = $this->parameterBag->get(
                    'COMMON_TMP'
                ) . '/' . 'revoqueAlternate_' . session_name() . session_id() . time();
            system($commandeAlternate . ' > ' . $verificationResultatAlternateFile . ' 2>&1');
            $fpAlternate = fopen($verificationResultatAlternateFile, 'r');
            $resultatVerificationAlternate = fread($fpAlternate, filesize($verificationResultatAlternateFile));
            fclose($fpAlternate);
            $pos1 = strpos(
                str_replace(' ', '', $resultatVerificationAlternate),
                (string)str_replace(' ', '', $cerificatFileName)
            );
            $pos2 = strpos(str_replace(' ', '', $resultatVerificationAlternate), 'OK');
            if (trim((str_replace(' ', '', $resultatVerificationAlternate))) == trim(
                    (str_replace(' ', '', $cerificatFileName) . ':OK')
                ) || (false !== $pos1 && false !== $pos2)) {
                $resVerif = 0;
            } elseif (strstr($resultatVerificationAlternate, 'certificate revoked')) {
                $resVerif = 2;
            } else {
                $resVerif = 1;
            }
            unlink($verificationResultatAlternateFile);
        }

        unlink($verificationResultatFile);

        return $resVerif;
    }

    /**
     * @param $cerificatFileName
     */
    public function verifyDateValiditeCertif($cerificatFileName): int
    {
        //Dates de validité
        $verificationResultatFile = $this->parameterBag->get('COMMON_TMP') . '/' . 'datesVerif_' . session_name(
            ) . session_id() . time();
        $commande = $this->parameterBag->get('OPENSSL') . ' x509 -in ' . escapeshellarg(
                $cerificatFileName
            ) . ' -enddate -startdate -noout';
        system($commande . ' > ' . $verificationResultatFile . ' 2>&1');

        $fp = fopen($verificationResultatFile, 'r');
        $resultatVerification = fread($fp, filesize($verificationResultatFile));
        fclose($fp);
        $resultatVerification = explode("\n", (string) $resultatVerification);
        unlink($verificationResultatFile);

        if ($resultatVerification[1]) {
            $dateDebut = explode('=', $resultatVerification[1]);
            $dateDebut = date('Y-m-d H:i:s', strtotime($dateDebut[1]));

            $dateFin = explode('=', $resultatVerification[0]);
            $dateFin = date('Y-m-d H:i:s', strtotime($dateFin[1]));

            if ($dateDebut <= date('Y-m-d H:i:s') && $dateFin >= date('Y-m-d H:i:s')) {
                $resVerif = 0;
            } else {
                $resVerif = 2;
            }
        } else {
            $resVerif = 1;
        }

        return $resVerif;
    }

    /**
     * @param $date
     * @return mixed|string
     */
    public function formatDate($date)
    {
        $arrayDate = explode('.', (string) $date);
        if ($arrayDate[1]) {
            $arrayDateSansP = explode(' ', $arrayDate[1]);
            $newDate = $arrayDate[0] . ' ' . $arrayDateSansP[1] . ' ' . $arrayDateSansP[2];
        } else {
            $newDate = $date;
        }

        return $newDate;
    }

    /**
     * @param $idFichierBlob
     * @param $idOffre
     * @param $fichierEnveloppe
     * @param false $calledFrom
     * @return int|void
     * @throws ServiceException
     * @throws Atexo_Config_Exception
     * @throws JsonException
     */
    public function verifySignatureValidity($idFichierBlob, $idOffre, $fichierEnveloppe, $calledFrom = false)
    {
        $verifHash = null;
        $verifIntegrite = null;
        if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
            $organisme = $fichierEnveloppe->getOrganisme();
            $signatureValide = 0;
            if ($this->parameterBag->get('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                $signatureInfo = $fichierEnveloppe->getSignatureInfos();
                if ($signatureInfo) {
                    $infoSignature = json_decode($signatureInfo, true, 512, JSON_THROW_ON_ERROR);
                    if (isset($infoSignature['signatureValide'])) {
                        $signatureValide = 'true' == $infoSignature['signatureValide'] ? 0 : -1;
                    } else {
                        $signatureValide = -2;
                    }
                    if ('entreprise' == $calledFrom) {
                        return $signatureValide;
                    }
                }
            }

            $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
            if ($offre) {
                if ((!empty($offre->getHorodatageHashFichiers()) || !empty(
                        $offre->getIdBlobHorodatageHash()
                        )) && $this->parameterBag->get('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                    //Verification de l'integrité de la signature
                    $verifHash = true;
                    $verifIntegrite = $this->verifyRejeuSignature($fichierEnveloppe);
                } elseif ($idFichierBlob) {
                    //Verification du hash
                    $responsesXml = $offre->getXmlString();
                    $hashSignedFile = (new Atexo_Entreprise_Reponses())->getEmprienteFichierFromXmlReponse(
                        $responsesXml,
                        $fichierEnveloppe
                    );
                    $verifHash = $this->verifyHashValidity($idFichierBlob, $hashSignedFile, $organisme);
                    $this->cryptoLogger->info(
                        ' Verification du hash resultat ' . $verifHash . ' du fichier idFichier ' . $idFichierBlob . ' hashSignedFile ' . $hashSignedFile,
                        TLogger::INFO,
                        'Atexo'
                    );
                    $verifIntegrite = $this->verifyRejeuHorodatage($offre->getXmlString(), $offre->getHorodatage());
                }

                if (-2 == $signatureValide) {
                    return -2;
                } elseif (0 == $signatureValide && $verifHash && $verifIntegrite) {
                    return 0;
                } else {
                    return -1;
                }
            }
        }
    }

    /**
     * @param $xmlReponsesAnnonce
     * @param $jetonHorodatage
     * @throws Exception
     */
    public function verifyRejeuHorodatage($xmlReponsesAnnonce, $jetonHorodatage): array|bool
    {
        $pathOrgTmp = $this->parameterBag->get('COMMON_TMP');
        $sessId = session_id();
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $infile = $this->parameterBag->get('COMMON_TMP') . '/file_' . $token . session_name() . $sessId . time();
        $infileTsr = $infile . '.tsr';

        if (!Atexo_Util::write_file($infile, $xmlReponsesAnnonce)) {
            throw new Exception(
                'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get('COMMON_TMP') . '/'
            );
        }

        if (!Atexo_Util::write_file($infileTsr, base64_decode($jetonHorodatage))) {
            throw new Exception(
                'Impossible de créer un fichier dans le répertoire : ' . $this->parameterBag->get('COMMON_TMP') . '/'
            );
        }
        $verify = $this->parameterBag->get('OPENSSL_TS')
            . " ts -verify -data $infile -in $infile.tsr -CApath " . $this->parameterBag->get('TRUST_DIR');
        $tsvErrorFile = $pathOrgTmp . $sessId . '_tsv_error';
        system($verify . '  > ' . $tsvErrorFile . ' 2>&1', $tsVerify);
        if (0 != $tsVerify) {
            $this->cryptoLogger->info(" verification du jeton d'horodatage verify " . $verify, TLogger::ERROR, 'Atexo');
            if (is_file($infile)) {
                unlink($infile);
            }
            if (is_file($infileTsr)) {
                unlink($infileTsr);
            }

            return [$tsVerify, null];
        } else {
            if (is_file($tsvErrorFile)) {
                unlink($tsvErrorFile);
            }

            return true;
        }
    }

    /**
     * Permet de verifier si le mode applet est activé pour l'agent
     */
    public function isModeAppletAgentActive(): bool
    {
        return !$this->parameterBag->get('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE') || Atexo_Module::isEnabled('ModeApplet');
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param $idAgent
     * @param $organisme
     * @param bool $enLigne
     * @throws Atexo_Config_Exception
     */
    public function getTelechargementCrypto(array $offres, $idAgent, $organisme, $enLigne = true): false|string
    {
        $this->cryptoLogger = Atexo_LoggerManager::getLogger('crypto');
        $params = [];
        $params['MpeURLPrivee'] = $this->parameterBag->get('MPE_WS_URL');
        $params['ServeurCryptoURLPublic'] = Atexo_MultiDomaine::replaceDomain($this->parameterBag->get('URL_CRYPTO'));
        $params['MpeFilePath'] = $this->parameterBag->get('BASE_ROOT_DIR') . $organisme
            . $this->parameterBag->get('FILES_DIR');
        $params['MpeLogin'] = $this->parameterBag->get('MPE_WS_LOGIN_SRV_CRYPTO');
        $params['MpePassword'] = $this->parameterBag->get('MPE_WS_PASSWORD_SRV_CRYPTO');
        $params['Format'] = $this->parameterBag->get('FORMAT_RETOUR_CRYPTO');
        $params['EnLigne'] = $enLigne;

        $service = new Atexo_Crypto_Service(
            Atexo_MultiDomaine::replaceDomain($this->parameterBag->get('URL_CRYPTO')),
            $this->cryptoLogger,
            $this->parameterBag->get('ENABLE_SIGNATURE_SERVER_V2'),
            null,
            Atexo_Util::getSfService(WebServicesCrypto::class)
        );

        try {
            return $service->demandeDechiffrement($offres, $idAgent, null, $params);
        } catch (ServiceException $e) {
            $this->cryptoLogger->error(
                "Erreur Lors d'appel au ws de dechiffrement" . $e->errorCode . ' - ' . $e->etatObjet . ' - ' . $e->message
            );
        }

        return false;
    }

    /**
     * Permet de verifier  la validité de la signature
     *
     * @param $fichierEnveloppe
     * @return bool
     * @throws Atexo_Config_Exception
     * @throws ServiceException
     */
    public function verifyRejeuSignature($fichierEnveloppe)
    {
        $this->cryptoLogger = Atexo_LoggerManager::getLogger('crypto');
        $signatureValide = false;
        try {
            if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                $service = new Atexo_Crypto_Service(
                    Atexo_MultiDomaine::replaceDomain($this->parameterBag->get('URL_CRYPTO')),
                    $this->cryptoLogger,
                    0, // ENABLE_SIGNATURE_SERVER_V2 pas prise en compte au niveau du depot
                    $this->parameterBag->get('UID_PF_MPE'),
                    Atexo_Util::getSfService(WebServicesCrypto::class)
                );
                $organisme = $fichierEnveloppe->getOrganisme();
                $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
                $filePath = $this->parameterBag->get('COMMON_TMP') . '/file_' . $token . session_name() . session_id(
                    ) . time();
                Atexo_Blob::copyBlob($fichierEnveloppe->getIdBlob(), $filePath, $organisme);
                if (is_file($filePath)) {
                    $typeSignature = $fichierEnveloppe->getTypeSignatureFichier();
                    if ($typeSignature === $this->parameterBag->get('PADES_SIGNATURE')) {
                        $this->cryptoLogger->info('Appele au ws de vérification de la signature de type =>' . $typeSignature);
                        $result = $service->verifierSignaturePades($filePath);
                    } else {
                        $typeSignature = $this->parameterBag->get('INCONNUE_SIGNATURE');
                        $hash = sha1_file($filePath);
                        $hash256 = hash_file('sha256', $filePath);
                        $this->cryptoLogger->info('Appele au ws de vérification de la signature de type =>' . $typeSignature);
                        $result = $service->verifierSignature(
                            $hash,
                            $fichierEnveloppe->getSignatureFichier(),
                            $typeSignature,
                            $hash256
                        );
                    }
                    if ($result instanceof InfosSignature) {
                        $signatureValide = boolval($result->getSignatureValide());
                    }
                }
            }
        } catch (ServiceException $e) {
            $this->cryptoLogger->error(
                "Erreur Lors d'appel au ws de dechiffrement " . $e->errorCode . ' - ' . $e->etatObjet . ' - ' . $e->message
            );
        }

        return $signatureValide;
    }

    /**
     * @param $resources
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function horodatageViaCrypto($resources): array|false
    {
        $resultat = false;
        $hash256 = null;
        if (file_exists($resources)) {
            $hash256 = hash_file('sha256', $resources);
        } elseif (!empty($resources)) {
            $hash256 = hash('sha256', $resources);
        }

        if (null !== $hash256) {
            try {
                $cryptoService = $this->container->get('atexo_crypto.crypto');
                $infosHorodatage = $cryptoService->demandeHorodatage($hash256);
                $untrusteddateOffre = '';

                if ($infosHorodatage instanceof InfosHorodatage) {
                    $untrusteddateOffre = (new DateTime(
                        date('Y-m-d H:i:s', $infosHorodatage->getHorodatage())
                    ))->format('Y-m-d H:i:s');
                    $resultat['horodatage'] = $infosHorodatage->getJetonHorodatageBase64();
                    $resultat['untrustedDate'] = $untrusteddateOffre;
                }
            } catch (Exception $exception) {
                $this->cryptoLogger->info('Exception : ' . $exception->getMessage() . 'Atexo');
            }
        } else {
            $this->cryptoLogger->info('Horodatage : error  Atexo');
        }

        return $resultat;
    }
}
