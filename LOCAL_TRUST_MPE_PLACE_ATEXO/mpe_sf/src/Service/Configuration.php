<?php

namespace App\Service;

use App\Entity\ConfigurationPlateforme;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Configuration
 * @deprecated
 * @package App\Service
 */
class Configuration
{
    /**
     * Configuration constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em
    ) {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getConfigurationPlateforme()
    {
        return $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
    }

    /**
     * @param $organisme
     */
    public function getConfigurationOrganisme($organisme)
    {
        return;
    }

    /**
     * @param $parameter
     *
     * @return mixed
     */
    public function getParameter($parameter)
    {
        return $this->container->getParameter($parameter);
    }
}
