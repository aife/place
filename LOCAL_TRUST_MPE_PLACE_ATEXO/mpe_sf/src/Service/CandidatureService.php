<?php

namespace App\Service;

use App\Entity\Inscrit;
use App\Entity\Consultation;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;

class CandidatureService
{
    final public const PHASE_DUME = 'dume';
    final public const PHASE_MPS = 'mps';
    final public const PHASE_STD = 'std';
    final public const PHASE_ROLE = 'role';
    final public const CHOICE_ONLINE = 'online';
    final public const CHOICE_FOURNIR = 'fournir';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly Security $security,
        private $parameters = []
    ) {
    }

    /**
     * @param $options
     *
     * @return object|null
     */
    public function findOneByOptions($options)
    {
        return $this->em->getRepository(TCandidature::class)->findOneBy($options);
    }

    /**
     * @return TCandidature|object|null
     *
     * @throws Exception
     */
    public function changeTypeCandidatureStandard(Request $request)
    {
        try {
            return $this->changeTypeCandidature($request, self::PHASE_STD, $request->get('phase'));
        } catch (Exception $e) {
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->logger->error(
                "Erreur lors de l'enregistrement de la candidature : function changeTypeCandidature, " . PHP_EOL . $erreur
            );
            throw $e;
        }
    }

    /**
     * @return TCandidature|object|null
     *
     * @throws Exception
     */
    public function changeTypeCandidatureDume(Request $request)
    {
        try {
            return $this->changeTypeCandidature($request, self::PHASE_DUME, $this->parameters['TYPE_CANDIDATUE_DUME'], $request->get('choice'));
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return TCandidature|object|null
     *
     * @throws Exception
     */
    public function changeRoleInscrit(Request $request)
    {
        try {
            return $this->changeTypeCandidature($request, self::PHASE_ROLE, '', '', $request->get('role'));
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $type
     * @param string $phase
     * @param string $choice
     * @param string $role
     *
     * @return TCandidature|object|null
     *
     * @throws Exception
     */
    public function changeTypeCandidature(Request $request, $type, $phase = '', $choice = '', $role = '')
    {
        try {
            $consultationId = $request->get('consultation');
            $organismeAcronyme = $request->get('organisme');
            $orgs = $request->getSession()->get('organismes_eligibles') ?? [];
            $consultation = $this->em->getRepository(Consultation::class)->getConsultationForDepot($consultationId, $orgs);
            if (!$consultation instanceof Consultation || $consultation->getOrganisme()->getAcronyme() != $organismeAcronyme) {
                $this->logger->error("Erreur: tentative de changement d'une consultation hors-ligne");
                throw new NotFoundHttpException();
            }
            $candidature = $this->findOneByOptions([
                'organisme' => $organismeAcronyme,
                'consultation' => $consultationId,
                'idInscrit' => $request->get('idInscrit'),
                'idEntreprise' => $request->get('idEntreprise'),
                'idEtablissement' => $request->get('idEtablissement'),
                'status' => $this->parameters['STATUT_ENV_BROUILLON'],
            ]);

            if ($candidature instanceof TCandidature) {
                if (self::PHASE_DUME == $type) {
                    $candidature->setTypeCandidatureDume($choice);
                } elseif (self::PHASE_STD == $type) {
                    $candidature->setTypeCandidature($phase);
                    $candidature->setTypeCandidatureDume('');
                } elseif (!empty($role)) {
                    $candidature->setRoleInscrit($role);
                }
            } else {
                $candidature = $this->createCandidatureFromConsultation(
                    $consultationId,
                    $organismeAcronyme,
                    $phase,
                    $choice,
                    $role
                );
            }

            $this->em->persist($candidature);
            $this->em->flush();

            return $candidature;
        } catch (Exception $e) {
            $erreur = 'Erreur : ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->logger->error(
                "Erreur lors de l'enregistrement de la candidature : function  " . $type . ', ' . PHP_EOL . $erreur
            );
            throw $e;
        }
    }

    /**
     * @param $consulationId
     * @param $organismeAcronyme
     * @param string $phase
     * @param string $choice
     * @param string $role
     *
     * @throws Exception
     */
    public function createCandidatureFromConsultation($consulationId, $organismeAcronyme, $phase = '', $choice = '', $role = ''): TCandidature
    {
        if (!isset($this->parameters['STATUT_ENV_BROUILLON'])) {
            throw new Exception('Missing parameter STATUT_ENV_BROUILLON');
        }

        $user = $this->security->getUser();

        if (!$user instanceof Inscrit) {
            throw new Exception('Connected user must be of type Inscrit');
        }

        $candidature = new TCandidature();

        $consultation = $this->em->getRepository(Consultation::class)->find($consulationId);
        $organisme = $this->em->getRepository(Organisme::class)->find($organismeAcronyme);
        $user = $this->security->getUser();

        $candidature->setConsultation($consultation);
        $candidature->setOrganisme($organisme);
        $candidature->setIdInscrit($user->getId());
        $candidature->setIdEntreprise($user->getEntrepriseId());
        $candidature->setIdEtablissement($user->getIdEtablissement());
        $candidature->setStatus($this->parameters['STATUT_ENV_BROUILLON']);

        if (self::PHASE_DUME == $phase) {
            $candidature->setTypeCandidature(self::PHASE_DUME);
            if (self::CHOICE_ONLINE == $choice) {
                $candidature->setTypeCandidatureDume(self::CHOICE_ONLINE);
            } elseif (self::CHOICE_FOURNIR == $choice) {
                $candidature->setTypeCandidatureDume(self::CHOICE_FOURNIR);
            }
        } elseif (self::PHASE_MPS == $phase) {
            $candidature->setTypeCandidature(self::PHASE_MPS);
        } elseif (self::PHASE_STD == $phase) {
            $candidature->setTypeCandidature(self::PHASE_STD);
        }

        if (!empty($role)) {
            $candidature->setRoleInscrit($role);
        }

        return $candidature;
    }
}
