<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service;

use App\Entity\Entreprise;
use App\Entity\TListeLotsCandidature;
use Doctrine\ORM\EntityManagerInterface;

class LotCandidature
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function getNamesAndIdsForCandidats(int $numLot, int $idConsultation): array
    {
        $data = $this->em->getRepository(TListeLotsCandidature::class)
            ->getCandidatsByLotAndConsultation($numLot, $idConsultation);

        $entreprisesId = array_keys($data);

        if (count($entreprisesId) > 0) {
            $data = $this->em->getRepository(Entreprise::class)->getNameAndIdEntrepriseByIds($entreprisesId);
        }

        return $data;
    }

    public function getRaisonSocialAndSiretForEntreprise(int $numLot, int $idConsultation, array $statutOffres): array
    {
        $result = [];
        $repository = $this->em->getRepository(TListeLotsCandidature::class);

        $candidatsByLotAndConsultation = $repository->getCandidatsByLotAndConsultation(
            $numLot,
            $idConsultation,
            $statutOffres
        );

        $listeLotsCandidature = $repository->findBy(['consultation' => $idConsultation]);

        if ((is_countable($candidatsByLotAndConsultation) ? count($candidatsByLotAndConsultation) : 0) > 0) {
            foreach ($candidatsByLotAndConsultation as $entrepriseId => $entrepriseData) {
                $infoSoumissionaire = $this->em->getRepository(Entreprise::class)
                    ->getRaisonSocialAndSiretEntrepriseByIdAndEtablissement(
                        $entrepriseId,
                        $entrepriseData['idEtablissement']
                    )
                ;

                foreach ($listeLotsCandidature as $listeLot) {
                    if (
                        $listeLot instanceof TListeLotsCandidature
                        && $listeLot->getIdEntreprise() == $entrepriseId
                        && $entrepriseData['numLot'] == $listeLot->getNumLot()
                        && $listeLot->getCandidature() !== null
                        && $listeLot->getCandidature()->getIdOffre() !== null
                    ) {
                        $infoSoumissionaire['enveloppe'] = ['numero' =>
                            $listeLot->getCandidature()->getIdOffre()->getNumeroReponse()
                        ];
                        break;
                    }
                }

                $result[] = $infoSoumissionaire;
            }
            return $result;
        }

        return $candidatsByLotAndConsultation;
    }

    public function getLotsCandidature(int $idConsultation, int $idEntreprise = null, int $idOffre = null): array
    {
        $repository = $this->em->getRepository(TListeLotsCandidature::class);


        return $repository->getLotsUsedByCandidatureForEntreprise($idConsultation, $idEntreprise, $idOffre);
    }
}
