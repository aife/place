<?php

namespace App\Service\Dume;

use Application\Service\Atexo\Atexo_Config;
use AtexoDume\Client\Client;
use AtexoDume\Dto\ReponseValidPubDume;
use Stringable;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use ZipArchive;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Utils\Filesystem\MountManager;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DumeService
{
    final public const DUME_IMAGE_PUBLIE = 'consultation_depot/assets/images/logo-dume-publie.png';
    final public const DUME_IMAGE_NON_PUBLIE = 'consultation_depot/assets/images/logo-dume-non-publie.png';
    final public const PHASE_DUME = 'dume';
    final public const CHOICE_ONLINE = 'online';
    final public const DUME_TYPE_XML = 'xml';
    final public const CMD_ENTREPRISE_GROUPEMENT_TO_PUBLISH = 'dume:entreprise:groupement-to-publish';
    final public const CMD_ENTREPRISE_VALID = 'dume:entreprise:valid';
    final public const CMD_DELAY = 0;

    private readonly null|string|Stringable|UserInterface $user;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $dumeLogger,
        private readonly TranslatorInterface $translator,
        TokenStorageInterface $tokenStorage,
        private readonly MountManager $mountManager,
        private $parameters = []
    ) {
        $this->user = $tokenStorage->getToken()?->getUser();
    }

    /**
     * Récupération du DumeNumero à partir d'un id DumeContexte.
     *
     * @param $dumeContexteId
     *
     * @return object|null
     */
    public function getDumeNumeroFromIdDumeContexte($dumeContexteId)
    {
        return $this->em->getRepository(TDumeNumero::class)->findOneBy(['idDumeContexte' => $dumeContexteId]);
    }

    /**
     *  Récupération du DumeNumero d'un groupement.
     *
     * @param $offreId
     * @param $entrepriseId
     *
     * @return mixed
     */
    public function getDumeNumeroFromGroupement($offreId, $entrepriseId)
    {
        return $this->em->getRepository(TDumeNumero::class)->getDumeNumeroFromGroupement($offreId, $entrepriseId);
    }

    /**
     * Récupération d'un DumeContexte.
     *
     * @param $id
     *
     * @return object|null
     */
    public function getDumeContexteFromId($id)
    {
        return $this->em->getRepository(TDumeContexte::class)->findOneBy(['id' => $id]);
    }

    /**
     * Récupération des informations DUME d'une consultation.
     *
     * @param $consultation
     * @param null $contexte
     *
     * @return array
     */
    public function getInfoDume($consultation, $contexte = null)
    {
        $infoDume = [];
        if (
            !($contexte instanceof TDumeContexte) &&
            $consultation instanceof Consultation && '1' == $consultation->getDumeDemande()
        ) {
            $contexte = $this->getDumeContexteWaitingOrValidOrPublish($consultation);
        }
        if ($contexte instanceof TDumeContexte) {
            if ($contexte->isStandard()) {
                $infoDume['image'] = self::DUME_IMAGE_NON_PUBLIE;
                $infoDume['message'] = $this->translator->trans('DEFINE_DUME_ACHETEUR_NON_PUBLIE');
            } else {
                $infoDume['image'] = self::DUME_IMAGE_PUBLIE;
                $infoDume['message'] = $this->translator->trans('DEFINE_DUME_ACHETEUR_PUBLIE');
            }
        }

        return $infoDume;
    }

    /**
     * Récupération des DumeContexte à partir de statut.
     *
     * @return mixed
     */
    public function getDumeContexteWaitingOrValidOrPublish(Consultation $consultation)
    {
        return $this->em
            ->getRepository(TDumeContexte::class)
            ->getWaitingOrValidOrPublish(
                $consultation->getId(),
                $consultation->getAcronymeOrg(),
                $this->parameters['TYPE_DUME_ACHETEUR'],
                $this->parameters['STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'],
                $this->parameters['STATUT_DUME_CONTEXTE_VALIDE'],
                $this->parameters['STATUT_DUME_CONTEXTE_PUBLIE'],
                $this->dumeLogger,
                $this->translator->trans('MSG_ERREUR_TECHNIQUE')
            );
    }

    /**
     * Récupération du Dume valide d'un dépot.
     *
     * @param $isModuleEnable
     * @param $codeEtablissement
     * @param $tvaIntracommunautaire
     * @param $user
     *
     * @return array
     */
    public function getDepotDumeValid(
        $isModuleEnable,
        Entreprise $entreprise,
        $codeEtablissement,
        $tvaIntracommunautaire,
        Consultation $consultation,
        $user
    ) {
        $tabMultiDepot = ['isDumePublier' => false];

        if ($isModuleEnable) {
            $candidature = $this->em
                ->getRepository(TCandidature::class)
                ->getDumePublier(
                    $consultation->getId(),
                    $consultation->getOrganisme()->getAcronyme(),
                    [
                        $this->parameters['STATUT_DUME_CONTEXTE_PUBLIE'],
                    ],
                    $this->parameters['TYPE_CANDIDATUE_DUME'],
                    $this->parameters['TYPE_CANDIDATUE_DUME_ONLINE'],
                    $entreprise->getSiren(),
                    $codeEtablissement,
                    $tvaIntracommunautaire,
                    $user
                );

            if (
                ($candidature instanceof TCandidature) && (
                    $candidature->getIdDumeContexte()->getStatus() ==
                    $this->parameters['STATUT_DUME_CONTEXTE_PUBLIE']
                )
            ) {
                $dumeNumero = $this->getDumeNumeroFromIdDumeContexte($candidature->getIdDumeContexte()->getId());

                if ($dumeNumero instanceof TDumeNumero) {
                    $tabMultiDepot['isDumePublier'] = true;
                    $tabMultiDepot['numSN'] = $dumeNumero->getNumeroDumeNational();
                }
            }
        }

        return $tabMultiDepot;
    }

    /**
     * Récupération du DumeContexte acheteur.
     *
     * @param $consultationId
     * @param $acronymeOrganisme
     *
     * @return mixed
     */
    public function getDumeContexteAcheteurPublie($consultationId, $acronymeOrganisme)
    {
        return $this->getDumeContexte(
            $consultationId,
            $acronymeOrganisme,
            $this->parameters['STATUT_DUME_CONTEXTE_PUBLIE'],
            $this->parameters['TYPE_DUME_ACHETEUR']
        );
    }

    /**
     * Récupération du Dume brouillon.
     *
     * @param $consultationId
     * @param $acronymeOrganisme
     * @param $user
     * @param $etablissementId
     *
     * @return mixed
     */
    public function getDumeContexteEntrepriseBrouillon($consultationId, $acronymeOrganisme, $user, $etablissementId)
    {
        return $this->getDumeContexteEntreprise(
            $consultationId,
            $acronymeOrganisme,
            $user->getEntrepriseId(),
            $etablissementId,
            $this->parameters['STATUT_ENV_BROUILLON'],
            $user->getId()
        );
    }

    /**
     * Récupération de tous les dumes d'une entreprise pour une consultation.
     *
     * @param $consultationId
     * @param $acronymeOrganisme
     * @param $user
     * @param $etablissementId
     *
     * @return mixed
     */
    public function getDumeContexteEntrepriseExistant($consultationId, $acronymeOrganisme, $user, $etablissementId)
    {
        return $this->getDumeContexteEntreprise(
            $consultationId,
            $acronymeOrganisme,
            $user->getEntrepriseId(),
            $etablissementId
        );
    }

    /**
     * Fonction générique de récupération des dumes entreprises.
     *
     * @param $consultationId
     * @param $acronymeOrganisme
     * @param $entrepriseId
     * @param $etablissementId
     * @param null $statut
     * @param null $userId
     *
     * @return mixed
     */
    public function getDumeContexteEntreprise(
        $consultationId,
        $acronymeOrganisme,
        $entrepriseId,
        $etablissementId,
        $statut = null,
        $userId = null
    ) {
        return $this->getDumeContexte(
            $consultationId,
            $acronymeOrganisme,
            $statut,
            $this->parameters['TYPE_DUME_OE'],
            $userId,
            $entrepriseId,
            $etablissementId
        );
    }

    /**
     * Fonction générique de récupération des dumes.
     *
     * @param $consultationId
     * @param $acronymeOrganisme
     * @param $statut
     * @param $type
     * @param null $userId
     * @param null $entrepriseId
     * @param null $etablissementId
     *
     * @return mixed
     */
    public function getDumeContexte(
        $consultationId,
        $acronymeOrganisme,
        $statut,
        $type,
        $userId = null,
        $entrepriseId = null,
        $etablissementId = null
    ) {
        return $this->em
            ->getRepository(TDumeContexte::class)
            ->getIdContextDume(
                $consultationId,
                $acronymeOrganisme,
                $statut,
                $type,
                $userId,
                $entrepriseId,
                $etablissementId
            );
    }

    /**
     * Création persistance d'un DumeNumero.
     *
     * @param $contextDumeId
     * @param $numeroSN
     *
     * @return TDumeNumero
     *
     */
    public function createDumeNumero($contextDumeId, $numeroSN): TDumeNumero
    {
        $dumeNumero = new TDumeNumero();
        $dumeNumero->setIdDumeContexte($contextDumeId);
        $dumeNumero->setNumeroDumeNational($numeroSN);

        $this->em->persist($dumeNumero);
        $this->em->flush();

        return $dumeNumero;
    }

    /**
     * Persistance d'un DumeContexte.
     *
     * @param $contexteLtDumeId
     *
     * @return TDumeContexte
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createDumeContexte(
        Consultation $consultation,
        Organisme $organisme,
        $contexteLtDumeId
    ) {
        $dumeContexte = new TDumeContexte();

        $dumeContexte->setConsultation($consultation);
        $dumeContexte->setOrganisme($organisme);
        $dumeContexte->setContexteLtDumeId($contexteLtDumeId);
        $dumeContexte->setTypeDume($this->parameters['TYPE_DUME_OE']);
        $dumeContexte->setStatus($this->parameters['STATUT_ENV_BROUILLON']);

        $this->em->persist($dumeContexte);
        $this->em->flush();

        return $dumeContexte;
    }

    /**
     * Fonction générique de téléchargement d'un DUME.
     *
     * @param $type
     * @param $offreId
     * @param null $dumeNumeroId
     *
     * @return array
     */
    public function downloadDume($type, $offreId, $dumeNumeroId = null)
    {
        $offre = $this->em->getRepository(Offre::class)->find($offreId);
        $consultation = $this->em->getRepository(Consultation::class)->find($offre->getConsultationId());

        if (!is_object($this->user)) {
            throw new AuthenticationCredentialsNotFoundException(
                $this->translator->trans('TEXT_PERMISSION_NON_ACCORDEE')
            );
        }
        $fileArray = [];
        if ($offre->getStatutOffres() != $this->parameters['STATUT_ENV_BROUILLON']) {
            $candidature = $this->em
                ->getRepository(TCandidature::class)
                ->findOneBy([
                    'idOffre' => $offre->getId(),
                    'idInscrit' => $this->user->getId(),
                    'idEntreprise' => $this->user->getEntrepriseId(),
                    'idEtablissement' => $this->user->getIdEtablissement(),
                ]);

            if (
                self::PHASE_DUME == $candidature->getTypeCandidature() &&
                self::CHOICE_ONLINE == $candidature->getTypeCandidatureDume()
            ) {
                if (null !== $dumeNumeroId) {
                    $dumeNumeroAndBlob = $this->em->getRepository(TDumeNumero::class)->find($dumeNumeroId);
                } else {
                    $dumeNumeroAndBlob = $this->getDumeNumeroFromIdDumeContexte($candidature->getIdDumeContexte());
                }

                if (self::DUME_TYPE_XML == $type) {
                    $blobId = $dumeNumeroAndBlob->getBlobIdXml();
                } else {
                    $blobId = $dumeNumeroAndBlob->getBlobId();
                }

                $absoluteFilePath = $this->mountManager->getAbsolutePath(
                    $blobId,
                    $consultation->getOrganisme(),
                    $this->em
                );
                $relativeFilePath = $this->mountManager->getFilePath($blobId, $consultation->getOrganisme(), $this->em);

                $fileSystem = $this->mountManager->getFilesystem('nas');
                if (!$fileSystem->has($relativeFilePath)) {
                    throw new NotFoundHttpException();
                }

                $fileArray = [
                    'fileName' => $blobId . '-0',
                    'filePath' => $absoluteFilePath,
                    'dumeNumero' => $dumeNumeroAndBlob->getNumeroDumeNational(),
                ];
            }
        }

        return $fileArray;
    }

    /**
     * Permet de retourner le zip DUME.
     *
     * @param $idNumero
     * @param $acronymeOrganisme
     * @param $format
     *
     * @return array()
     */
    public function downloadZipDUMEById($idNumero, $acronymeOrganisme, $format)
    {
        try {
            $result = [];

            $dumeNumeroAndBlob = $this->em
                ->getRepository(TDumeNumero::class)
                ->findOneBy(['id' => $idNumero]);
            if ($dumeNumeroAndBlob instanceof TDumeNumero) {
                $dirArchive = $this->parameters['COMMON_TMP'];
                $blobId = null;
                if ('pdf' == $format) {
                    $blobId = $dumeNumeroAndBlob->getBlobId();
                } elseif ('xml' == $format) {
                    $blobId = $dumeNumeroAndBlob->getBlobIdXml();
                }

                $absoluteFilePath = $this->mountManager->getAbsolutePath($blobId, $acronymeOrganisme, $this->em);
                $relativeFilePath = $this->mountManager->getFilePath($blobId, $acronymeOrganisme, $this->em);

                $fileSystem = $this->mountManager->getFilesystem('nas');
                if (!$fileSystem->has($relativeFilePath)) {
                    throw new NotFoundHttpException();
                }

                $archive = new ZipArchive();
                $archiveName = $dirArchive . 'DUME_ACHETEUR_' . $blobId . session_id() . time();

                if (true === $archive->open($archiveName, ZipArchive::CREATE)) {
                    $archive->addFile(
                        $absoluteFilePath,
                        $dumeNumeroAndBlob->getNumeroDumeNational() . '.' . $format
                    );
                    $downloadName = $this->translator->trans('DEFINE_DUME_ACHETEUR');
                }
                $archive->close();
                $result['archiveName'] = $archiveName;
                $result['downloadName'] = $downloadName;
            }
        } catch (Exception $e) {
            $this->dumeLogger->info(' Erreur lors de telechargement du Zip DUME Acheteur '
                . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $result;
    }

    public function validationDume(TDumeContexte $dumeContext, string $from = 'DEMANDE_VALIDATION'): array
    {
        $results = ['hasError' => false, 'message' => ''];

        $consultation = $dumeContext->getConsultation();

        if ('DEMANDE_VALIDATION' == $from) {
            $serviceIndisponibleMessage = $this->translator->trans(
                'DUME_ACHETEUR_DEMANDE_VALIDATION_SERVICE_INDISPONIBLE'
            );
        } elseif ('VALIDATION' == $from) {
            $serviceIndisponibleMessage = $this->translator->trans('DUME_ACHETEUR_VALIDATION_SERVICE_INDISPONIBLE');
        } else {
            $serviceIndisponibleMessage = $this->translator->trans('DUME_ACHETEUR_SERVICE_INDISPONIBLE');
        }

        $resultsValidateDume = $this->validateDume($dumeContext);
        $infoDume = $this->getInfoDumeApi($dumeContext);

        if (!empty($infoDume)) {
            $standard = 0;
            if (true == $infoDume['default']) {
                $standard = 1;
            }
            $dumeContext->setIsStandard($standard);
            $this->em->flush();
        }

        if ($resultsValidateDume instanceof \AtexoDume\Dto\ReponseValidPubDume) {
            if ($resultsValidateDume->getStatut() == Atexo_Config::getParameter('STATUT_VALIDATION_DUME_OK')) {
                $results = ['hasError' => false, 'message' => ''];
                $dumeContext->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE'));
                $this->em->flush();
                $this->dumeLogger->info(
                    'fct validerDumeAcheteur consultation ' . $consultation->getId()
                    . ' : Mettre à jour le statut du t_dume_contexte à valide  id ' . $dumeContext->getId()
                );
            } else {
                $serviceIndisponibleMessage = (new Atexo_Config())->toPfEncoding($resultsValidateDume->getMessage());
                $results = ['hasError' => true, 'message' => $serviceIndisponibleMessage];
            }
        } else {
            $results = ['hasError' => true, 'message' => $serviceIndisponibleMessage];
        }

        return $results;
    }

    public function validateDume(TDumeContexte $dumeContext): ?ReponseValidPubDume
    {
        $results = null;
        $consultation = $dumeContext->getConsultation();

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $this->dumeLogger,
                null
            );
            $this->dumeLogger->info('DEBUT fct validerDume : appel à la library mpe_dume : consultation '
                . $consultation->getId() . ' , idContexte  ' . $dumeContext->getId());

            if ($dumeContext->getContexteLtDumeId()) {
                $this->dumeLogger->info(
                    'fct validerDume : validate dume acheteur encours  pour consultation ' . $consultation->getId()
                    . ' =>  params : idContextLtDume ' . $dumeContext->getId()
                );
                $results = $c->acheteurService()->validateDume($dumeContext->getContexteLtDumeId());
                $this->dumeLogger->info(
                    'fct validerDume : validate dume acheteur  effectue pour consultation ' . $consultation->getId()
                    . ' et idContexte  ' . $dumeContext->getId(
                    ) . ' =>  retour :' . print_r(
                        $results,
                        true
                    )
                );
            }

            $this->dumeLogger->info(
                'FIN fct validerDume : appel à la library mpe_dume : consultation ' . $consultation->getId()
                . ' , idContexte  ' . $dumeContext->getId() . '=> return ' . print_r(
                    $results,
                    true
                )
            );
        } catch (Exception $e) {
            $this->dumeLogger->error(
                'FIN fct validerDume : appel à la library mpe_dume : consultation ' . $consultation->getId()
                . ' , idContexte  ' . $dumeContext->getId(
                ) . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString()
            );
        }

        return $results;
    }

    public function getInfoDumeApi(TDumeContexte $dumeContext): array
    {
        $results = [];

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $this->dumeLogger,
                null
            );
            $this->dumeLogger->info('DEBUT fct get : appel à la library mpe_dume : idContexteDume '
                . $dumeContext->getId());
            $results = $c->acheteurService()->getInfoDume($dumeContext->getContexteLtDumeId());
            $this->dumeLogger->info('FIN fct get : appel à la library mpe_dume : idContexteDume '
                . $dumeContext->getId() . ' => return ' . $results);
        } catch (Exception $e) {
            $this->dumeLogger->error(
                'FIN fct get : appel à la library mpe_dume : idContexteDume '
                . $dumeContext->getId() . ' => exception ' . $e->getMessage() . ' ' . $e->getTraceAsString()
            );
        }

        return $results;
    }
}
