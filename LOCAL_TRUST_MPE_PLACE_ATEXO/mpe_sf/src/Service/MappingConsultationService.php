<?php

namespace App\Service;

use Exception;
use App\Entity\Consultation;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MappingConsultationService
{
    public function __construct(private readonly ContainerInterface $container, private readonly EntityManagerInterface $em, private readonly LoggerInterface $logger)
    {
    }

    /**
     * Récupération de l'id de la consultation pour la gestion post-ESR2020 ( reference vs id de la table consultation).
     *
     * @param int  $reference correspond à la reference ou l'id de la consultation
     * @param null $acronyme  acronyme de l'organisme
     * @param null $code      code d'accès à la consultation restreinte
     *
     * @return int|null
     */
    public function getConsultationId($reference, $acronyme = null, $code = null)
    {
        $consultationId = null;
        try {
            $consultations = $this->em->getRepository(Consultation::class)
                ->getMappedConsultation(
                    $reference,
                    $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE'),
                    $acronyme,
                    $code
                );
            if (is_array($consultations)) {
                if (count($consultations) > 1) {
                    $consultationId = -1;
                } elseif ($consultations[0] instanceof Consultation) {
                    $consultationId = $consultations[0]->getId();
                }
            }
        } catch (Exception $e) {
            $message = '[MappingConsultationService] Erreur lors de la récupération de la consultation';
            $message .= ' ref : '.$reference;
            $message .= ' acronyme : '.$acronyme;
            $message .= ' code : '.$code;
            $message .= ' => '.$e->getMessage();
            $this->logger->error($message);
        }

        return $consultationId;
    }
}
