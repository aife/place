<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Agent;
use App\Exception\AgentTechniqueNotFoundException;
use App\Service\Version\VersionService;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class WebServicesRedaction
{
    private string $encoding = 'iso-8859-1';
    private const AGENT_TECHNIQUE_LOGIN = 'redac';

    final public const MODULE_MPE = 'MPE';
    final public const MODULE_EXEC = 'EXEC';
    final public const MODULE_RECENSEMENT = 'RECENSEMENT';

    final public const VALUE_EXISTS = '1';

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly AgentService $agentService,
        private readonly AtexoUtil $atexoUtil,
        private readonly CurrentUser $currentUser,
        private readonly OrganismeService $organismeService,
        private readonly TranslatorInterface $translation,
        private readonly AtexoConfiguration $configuration,
        private readonly VersionService $versionService,
        private readonly EntityManagerInterface $entityManager,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getAuthResponse(Agent $user): array
    {
        $login = rawurlencode($user->getLogin());
        $pwd = $user->getPassword();

        $response = $this->httpClient->request(
            Request::METHOD_GET,
            $this->parameterBag->get('URL_RSEM_REST_AUTENTIFICATION') . $login . '/' . $pwd,
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                ],
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de token Redac'
            );
        }

        if (!($content = $response->getContent())) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de token Redac: null body'
            );
        }

        $crawler = new Crawler($content);
        if ($crawler->filter('code')->count() > 0) {
            throw new HttpException(
                $crawler->filter('code')->text(),
                $crawler->filter('message')->text()
            );
        }

        return [
            'token' => $crawler->filter('ticket')->text(),
        ];
    }

    public function initialiseContext(Agent $user, string $ticket): array
    {
        $xmlInitialisation = $this->generateXmlInitialisation($user);

        $response = $this->httpClient->request(
            Request::METHOD_PUT,
            $this->parameterBag->get('URL_RSEM_REST_INITIALISATION') . '?ticket=' . $ticket,
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                ],
                'body' => $xmlInitialisation
            ]
        );

        if ($response->getStatusCode() !== Response::HTTP_OK) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de token Redac'
            );
        }

        if (!($content = $response->getContent())) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de token Redac: null body'
            );
        }

        $crawler = new Crawler($content);
        if ($crawler->filter('code')->count() > 0) {
            throw new HttpException(
                $crawler->filter('code')->text(),
                $crawler->filter('message')->text()
            );
        }

        return [
            'identifiant' => $crawler->filter('identifiant')->text(),
        ];
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     */
    public function getListeDocument(string $consultationId, string $consultationOrg, string $ticket): array
    {
        $response = $this->httpClient->request(
            Request::METHOD_GET,
            $this->parameterBag->get('URL_RSEM_GET_LISTE_DOCUMENT'),
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                ],
                'query' => [
                    'identifiantConsultation' => base64_encode(sprintf(
                        '%s#%s',
                        $consultationOrg,
                        $consultationId
                    )),
                    'ticket' => $ticket,
                ],
            ]
        );

        if ($response->getStatusCode() != '200') {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de la liste des documents Redac'
            );
        }

        $content = $response->getContent();

        if (!$content) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de la liste des documents Redac: null body'
            );
        }

        $crawler = new Crawler($content);
        if ($crawler->filter('code')->count() > 0) {
            $code = $crawler->filter('code')->text();
            $message = $crawler->filter('message')->text();
            throw new HttpException($code, $message);
        }

        $documentElements = $crawler->filterXPath('//document');
        $docList = [];

        $documentElements->each(function (Crawler $documentCrawler, $index) use (&$docList) {
            $documentData = [
                'identifiant' => $documentCrawler->filter('identifiant')->text(),
                'nom' => $documentCrawler->filter('nom')->text(),
                'statut' => $documentCrawler->filter('statut')->text(),
                'titre' => $documentCrawler->filter('titre')->text(),
                'type' => $documentCrawler->filter('type')->text(),
            ];

            $docList[] = $documentData;
        });

        return $docList;
    }

    /**
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function downloadRedacDocument(
        string $idDocument,
        \DateTimeInterface $dateFin,
        string $organisme,
        string $ticket
    ): string {
        $url = sprintf(
            '%s?numPieceJointe=0&convertirEnPdf=true&dateLimitePlis=%s%s',
            $this->parameterBag->get('URL_RSEM_GET_CONTENT_DOCUMENT_REDAC'),
            strtotime($dateFin->format('Y-m-d H:i:s')) . '000',
            (!empty($organisme) ? '&acronymeOrganisme=' . $organisme : '')
        );

        $response = $this->httpClient->request(
            Request::METHOD_GET,
            $url,
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                ],
                'query' => [
                    'identifiantDocument' => $idDocument,
                    'ticket' => $ticket,
                ],
            ]
        );

        if (!$response->getContent()) {
            throw new HttpException(
                $response->getStatusCode(),
                'Problème lors de la récupération de la liste des documents Redac: null body'
            );
        }

        return $response->getContent();
    }

    protected function setDataXML(Agent $agent = null): false|string
    {
        return $this->generateXmlInitialisation($agent);
    }

    protected function generateXmlInitialisation(Agent $agent)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);

        // Créer les éléments XML
        $redactionElement = $this->addElement($domDocument, $domDocument, 'redaction');
        $initialisationElement = $this->addElement($domDocument, $redactionElement, 'initialisation');

        // Ajouter les informations utilisateur et PF
        $this->addInfosUserAndPF($domDocument, $initialisationElement, $agent);
        $this->addInfosProduct($domDocument, $initialisationElement, $agent);

        // TODO Ajouter les informations de consultation si disponibles


        return $domDocument->saveXml();
    }

    private function addElement($domDocument, $masterTag, $elementName, $content = '')
    {
        $element = $domDocument->createElement($elementName, $content);
        $masterTag->appendChild($element);

        return $element;
    }

    private function addInfosUserAndPF($domDocument, $initialisation, $agent)
    {
        // Création des éléments
        $this->addElement($domDocument, $initialisation, 'plateforme', $this->parameterBag->get('UID_PF_MPE'));
        $this->addElement($domDocument, $initialisation, 'version', $this->versionService->getApplicationVersion());

        $utilisateur = $this->addElement($domDocument, $initialisation, 'utilisateur');
        $this->addUserInformation($domDocument, $utilisateur, $agent);

        $habilitations = $this->addElement($domDocument, $utilisateur, 'habilitations');
        $this->addHabilitations($domDocument, $habilitations);

        $configuration = $this->addElement($domDocument, $utilisateur, 'configuration');
        $this->addConfiguration($domDocument, $configuration);

        return $domDocument;
    }

    private function addUserInformation($domDocument, $utilisateur, $agent)
    {
        $this->addElement($domDocument, $utilisateur, 'nom', utf8_encode($agent->getNom()));
        $this->addElement($domDocument, $utilisateur, 'prenom', utf8_encode($agent->getPrenom()));
        $this->addElement($domDocument, $utilisateur, 'courriel', $agent->getEmail());
        $this->addElement($domDocument, $utilisateur, 'identifiantExterne', $agent->getLogin());
        $this->addElement($domDocument, $utilisateur, 'plateformeUuid', $this->parameterBag->get('UID_PF_MPE'));
        $api = $this->addElement($domDocument, $utilisateur, 'api');
        $this->addApiInformation($domDocument, $api);
    }

    private function addApiInformation($domDocument, $api)
    {
        $url = rtrim($this->parameterBag->get('PF_URL_REFERENCE'), "/");
        $agentTechnique = $this->entityManager->getRepository(Agent::class)
            ->findAgentTechniqueByLogin(self::AGENT_TECHNIQUE_LOGIN);
        if (null === $agentTechnique) {
            throw new AgentTechniqueNotFoundException(
                sprintf('Agent Technique with login %s not found.', self::AGENT_TECHNIQUE_LOGIN)
            );
        }
        $token = $this->tokenManager->create($agentTechnique);
        $this->addElement($domDocument, $api, 'url', $url);
        $this->addElement($domDocument, $api, 'token', $token);
        $this->addElement($domDocument, $api, 'refreshToken', '');
    }

    private function addHabilitations($domDocument, $habilitations)
    {
        $habilitationRoles = [
            'RedactionDocumentsRedac',
            'ValidationDocumentsRedac',
            'AdministrerClauses',
            'AdministrerCanevas',
            'ValiderClauses',
            'ValiderCanevas',
            'ValiderClausesEditeur',
            'ValiderCanevasEditeur',
            'AdministrerClausesEditeur',
            'AdministrerCanevasEditeur',
            'GenererPiecesFormatOdt',
            'PublierVersionClausierEditeur',
            'AdministrerClausesEntiteAchats',
            'ReprendreIntegralementArticle',
            'GererGabaritEditeur',
            'GererGabarit',
            'GererGabaritEntiteAchats',
            'ActiverVersionClausier',
            'document',
            'administrationDocumentsModeles'
        ];

        foreach ($habilitationRoles as $role) {
            if ($this->currentUser->checkHabilitation($role)) {
                $this->addElement($domDocument, $habilitations, 'habilitation', 'ROLE_' . $role);
            }
        }
    }

    private function addConfiguration($domDocument, $configuration)
    {
        $configurationValue = $this->configuration->isModuleEnabled('PlateformeEditeur') ? 'true' : 'false';
        $this->addElement($domDocument, $configuration, 'plateformeEditeur', $configurationValue);
    }

    private function addInfosProduct($domDocument, $initialisation, $agent)
    {
        $produits = $this->addElement($domDocument, $initialisation, 'produits');
        $produit = $this->addElement($domDocument, $produits, 'produit');
        $this->addElement($domDocument, $produit, 'nom', self::MODULE_MPE);
        $this->addElement($domDocument, $produit, 'url', $this->parameterBag->get('PF_URL_REFERENCE'));

        if ($confOrganisme = $this->configuration->getConfigurationOrganisme($agent->getOrganisme())) {
            if ($confOrganisme->getModuleExec()) {
                $this->addProduit($domDocument, $produits, self::MODULE_EXEC, self::VALUE_EXISTS);
            }

            if ($confOrganisme->isModuleRecensementProgrammation()) {
                $this->addProduit(
                    $domDocument,
                    $produits,
                    self::MODULE_RECENSEMENT,
                    self::VALUE_EXISTS)
                ;
            }
        }
    }

    public function addProduit(DOMDocument $domDocument, mixed $produits, string $moduleName, string $urlValue): void
    {
        $produit = $this->addElement($domDocument, $produits, 'produit');
        $this->addElement($domDocument, $produit, 'nom', $moduleName);
        $this->addElement($domDocument, $produit, 'active', $urlValue);
    }
}
