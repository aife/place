<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */


namespace App\Service;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class PradoPasswordEncoder implements PradoPasswordEncoderInterface
{
    public function __construct(private PasswordHasherFactoryInterface $encoderFactory)
    {
    }

    /**
     * @inheritDoc
     */
    public function encodePassword(string $userClassName, string $plainPassword): string
    {
        $hasher = $this->encoderFactory->getPasswordHasher($userClassName);

        return $hasher->hash($plainPassword);
    }

    /**
     * @inheritDoc
     */
    public function isPasswordValid(string $userClassName, string $encodedPassword, string $plainPassword): bool
    {
        $hasher = $this->encoderFactory->getPasswordHasher($userClassName);

        return $hasher->verify($encodedPassword, $plainPassword);
    }
}
