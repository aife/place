<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Consultation;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationReferentielService
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function getListReferentielsConsultation(
        array $referentielsConsultation,
        Consultation $consultation
    ): array {
        $list = [];
        foreach ($referentielsConsultation as $referentiel) {
            $codeLibelle = $referentiel?->getLtReferentiel()?->getCodeLibelle();
            if ($consultation->isAlloti() && !empty($referentiel->getLot())) {
                $list['list'][Consultation::LOTS][$referentiel->getLot()][$codeLibelle] = [
                    'valeurPrincipale'  => $referentiel->getValeurPrincipaleLtReferentiel(),
                    'valeurSecondaire'  => $referentiel->getValeurSecondaireLtReferentiel()
                ];
            } else {
                $list['list'][Consultation::REFERENTIELS][$codeLibelle] = [
                    'valeurPrincipale'  => $referentiel->getValeurPrincipaleLtReferentiel(),
                    'valeurSecondaire'  => $referentiel->getValeurSecondaireLtReferentiel()
                ];
            }
        }

        return $list;
    }

    /**
     * Note : Cette méthode sera à supprimer aprés la migration des données des référentiels de consultation vers
     * les données complémentaires
     */
    public function getReferentielValuesSettedInDonneeComplementaire(
        array $referentielsConsultation,
        Consultation\DonneeComplementaire $donneeComplementaire,
        string $numeroLot = null
    ): Consultation\DonneeComplementaire {
        $codeLibelleToSet = [
            $this->parameterBag->get('INFOS_CMPLT_LOT') => 'setInformationComplementaire',
            $this->parameterBag->get('AUTRE_INFOS') => 'setAutresInformations',
            $this->parameterBag->get('MODALITES_VISITES_LIEUX_REUNIONS') => 'setModaliteVisiteLieuxReunion',
            $this->parameterBag->get('CONDITION_PARTICIPATION') => 'setConditionParticipation',
            $this->parameterBag->get('POUVOIR_ADJUDICATEUR') => 'setAutrePouvoirAdjudicateur',
            $this->parameterBag->get('FORMULE_SIGNATURE') => 'setFormuleSignature',
        ];

        foreach ($referentielsConsultation as $referentiel) {
            if ($numeroLot && $referentiel->getLot() === $numeroLot) {
                continue;
            }

            $codeLibelle = $referentiel?->getLtReferentiel()?->getCodeLibelle();
            $valueReferentiel = $referentiel->getValeurPrincipaleLtReferentiel();

            if ($codeLibelle && $valueReferentiel && isset($codeLibelleToSet[$codeLibelle])) {
                $setterMethod = $codeLibelleToSet[$codeLibelle];
                $donneeComplementaire->$setterMethod($valueReferentiel);
            }
        }

        return $donneeComplementaire;
    }
}
