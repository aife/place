<?php

namespace App\Service;

use Swift_Mime_MimePart;
use Swift_Message;
use Twig\Environment;

/**
 * @author Mohamed BLAL
 *
 * Class SwiftMessageFactory
 */
class SwiftMessageFactory
{
    /**
     * Le fichier définissant le tamplate de l'email.
     *
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $subject;

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    private ?string $cc = null;

    /**
     * content to display over the email, as an associative array.
     *
     * @var array
     */
    private $content;

    /**
     * @var mixed
     */
    private $attachment;

    /**
     * SwiftMessageFactory constructor.
     *
     * @param Environment
     */
    public function __construct(
        /**
         * Template twig Engine.
         */
        private readonly Environment $templateEngine
    )
    {
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param $template
     *
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param $to
     *
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param $subject
     *
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param $from
     *
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return string
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param string $cc
     */
    public function setCc($cc)
    {
        $this->cc = $cc;
    }

    /**
     * @return mixed
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @param mixed $attachment
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;
    }

    /**
     * Aurait pu être static.
     *
     * @return $this
     */
    public function getInstance()
    {
        return $this;
    }

    /**
     * @return Swift_Mime_MimePart
     */
    public function create()
    {
        $message = (new Swift_Message())
            ->setSubject($this->subject)
            ->setFrom($this->from)
            ->setTo($this->to)
            ->setCc($this->cc)
            ->setCharset('utf-8') //TODO a dynamisé
            ->setContentType('text/html') //TODO a dynamisé
            ->setBody($this->templateEngine->render($this->template, $this->content));

        return $message;
    }
}
