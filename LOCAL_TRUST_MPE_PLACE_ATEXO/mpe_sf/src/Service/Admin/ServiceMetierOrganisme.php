<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Admin;

use App\Entity\OrganismeServiceMetier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ServiceMetierOrganisme
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $parameterBag
    ) {
    }

    public function getAccessibleServices(string $organisme, int $calledFromSocle): array
    {
        $arrayServicesMetiers = [];
        $rows = $this->em->getRepository(OrganismeServiceMetier::class)
            ->findByOrganismeOrServiceMetier(
                $organisme,
                $this->parameterBag->get('SERVICE_METIER_MPE'),
                $calledFromSocle
            );
        foreach ($rows as $row) {
            if (!isset($arrayServicesMetiers[$row['idServiceMetier']])) {
                $servicesMetiers['checked'] = false;
                $servicesMetiers['activate'] = true;
                if ($row['ServiceActive']) {
                    $servicesMetiers['checked'] = true;
                }
                if (
                    (!$calledFromSocle && $row['idServiceMetier'] == $this->parameterBag->get('SERVICE_METIER_MPE'))
                    || $row['idServiceMetier'] == $this->parameterBag->get('SERVICE_METIER_SOCLE')
                ) {
                    $servicesMetiers['activate'] = false;
                    $servicesMetiers['checked'] = true;
                }

                $servicesMetiers['id_service_metier'] = $row['idServiceMetier'];
                $servicesMetiers['sigle'] = $row['sigle'];
                $servicesMetiers['denomination'] = $row['denomination'];
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            } else {
                $servicesMetiers['id_service_metier'] = $row['idServiceMetier'];
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            }
        }

        return $arrayServicesMetiers;
    }
}
