<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Admin;

use App\Service\File\SimpleFileUploader;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class HandleUploadLogoOrganisme
{
    public function __construct(private SimpleFileUploader $simpleFileUploader, private LoggerInterface $logger)
    {
    }

    public function upload(Form $form, string $path): void
    {
        /** @var UploadedFile $brochureFile */
        $logoSmallFile = $form->get('logoSmall')->getData();
        /** @var UploadedFile $brochureFile */
        $logoBigFile = $form->get('logoBig')->getData();

        try {
            if ($logoBigFile) {
                $this->simpleFileUploader->upload($logoBigFile, $path, 'logo-organisme-grand.jpg');
            }

            if ($logoSmallFile) {
                $this->simpleFileUploader->upload($logoSmallFile, $path, 'logo-organisme-petit.jpg');
            }
        } catch (FileException $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
