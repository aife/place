<?php

namespace App\Service\Authorization;

use Doctrine\DBAL\Exception as DBALException;
use App\Service\AtexoConfiguration;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Service\SearchAgent;
use App\Service\WebservicesMpeConsultations;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class AuthorizationAgent.
 */
class AuthorizationAgent
{
    public final const CONSULTATION_IS_NOT_IN_PERIMETRE =
        'La consultation id %d n\'existe pas ou ne fait pas partie du perimètre de l\'agent %d ';

    /**
     * AuthorizationAgent constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SearchAgent $searchAgent,
        private readonly SessionInterface $session,
        private readonly ContainerInterface $container,
        private readonly Security $security,
        private readonly WebservicesMpeConsultations $wsConsultations,
    ) {
    }

    public function isInTheScope(int $consultationId): bool
    {
        $wsResponse = $this->wsConsultations->getContent('searchConsultationsById', $consultationId);

        return  $wsResponse == Response::HTTP_OK;
    }

    /**
     * @param $reference
     *
     * @return bool
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function isAuthorized($reference, $checkModule = [])
    {
        $auth = false;

        /** @var Agent $agent */
        $agent = $this->security->getUser();

        if ($agent) {
            $contexteAuthentification = ['id' => $agent->getId()];

            if ($this->checkModule($agent, $contexteAuthentification, $checkModule)) {
                $auth = $this->isInTheScope($reference);
            }
        }

        return $auth;
    }

    protected function checkModule($infoAgent, $contexteAuthentification, $checkModule = [])
    {
        $authorization = true;
        if ((is_countable($checkModule) ? count($checkModule) : 0) > 0) {
            if (array_key_exists('EchangesDocuments', $checkModule)) {
                $authorization = $this->checkHabilitationEchangesDocument($infoAgent, $contexteAuthentification);
            }

            if (array_key_exists('EspaceDocumataires', $checkModule)) {
                $authorization = $this->checkHabilitationEspaceDocumentaire($infoAgent, $contexteAuthentification);
            }
        }

        return $authorization;
    }

    /**
     * Cette méthode permet de vérifier l'habilitation du module Echanges documents.
     *
     * @param $infoAgent
     * @param $contexteAuthentification
     *
     * @return bool
     */
    protected function checkHabilitationEchangesDocument($infoAgent, $contexteAuthentification)
    {
        $authorization = false;
        $habilitationAgent = $this->em
            ->getRepository(HabilitationAgent::class)
            ->findOneBy(['agent' => $contexteAuthentification['id']]);

        $echangesDocuments = $this->container->get(AtexoConfiguration::class)
            ->hasConfigOrganisme($infoAgent->getOrganisme()->getAcronyme(), 'echangesDocuments');

        if (true === $echangesDocuments && true === $habilitationAgent->isAccesEchangeDocumentaire()) {
            $authorization = true;
        }

        return $authorization;
    }

    /**
     * Cette méthode permet de vérifier l'habilitation du module Espace documentaire.
     *
     * @param $infoAgent
     * @param $contexteAuthentification
     *
     * @return bool
     */
    protected function checkHabilitationEspaceDocumentaire($infoAgent, $contexteAuthentification)
    {
        $authorization = false;
        $habilitationAgent = $this->em
            ->getRepository(HabilitationAgent::class)
            ->findOneBy(['agent' => $contexteAuthentification['id']]);

        $espaceDocumentaire = $this->container->get(AtexoConfiguration::class)
            ->hasConfigOrganisme($infoAgent->getOrganisme()->getAcronyme(), 'espaceDocumentaire');

        if (true === $espaceDocumentaire && true === $habilitationAgent->isEspaceDocumentaireConsultation()) {
            $authorization = true;
        }

        return $authorization;
    }
}
