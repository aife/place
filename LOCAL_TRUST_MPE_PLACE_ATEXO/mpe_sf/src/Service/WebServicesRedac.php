<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use DOMDocument;
use App\Attribute\ResultType;
use App\Entity\Agent;
use App\Security\ClientOpenIdConnect;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;

class WebServicesRedac extends AbstractService
{
    public final const CLIENT_ID = 'front';
    public final const PRODUCT_MPE = 'MPE';
    public final const DOCUMENTS_LIST_API = '/documents-modeles/';
    public final const DOCUMENT_API = '/documents-modeles/%s/download';
    public final const URL_API = '/clausier-api/';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    protected function authRedac(): ?string
    {
        return $this->getToken();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function getDocumentsList(string $platform, ?string $organism, ?int $service)
    {
        $url = $this->getRedacBaseUrl() . self::DOCUMENTS_LIST_API
            . sprintf(
                '?plateforme=%s&organisme=%s&service=%s&produit=%s',
                $platform,
                $organism,
                $service,
                self::PRODUCT_MPE
            );

        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ];

        return $this->request('GET', $url, $options)->getContent();
    }

    protected function getDocumentByCode(string $code, string $platform, ?string $organism, ?int $service)
    {
        $url = $this->getRedacBaseUrl() . self::DOCUMENT_API
            . '?plateforme=%s&organisme=%s&service=%s&produit=%s';

        $url = sprintf($url, $code, $platform, $organism, $service, self::PRODUCT_MPE);
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ];

        return $this->request('GET', $url, $options)->getContent();
    }

    protected function getTemplateDepouillement()
    {
        $url = rtrim($this->parameterBag->get('URL_CENTRALE_REFERENTIELS'), '/')
           . '/' . $this->parameterBag->get('URL_API_DEPOUILLEMENT_TEMPLATE');
        $options = [];

        return $this->request('GET', $url, $options)->getContent();
    }

    private function getToken(): ?string
    {
        return $this->getAuthenticationToken(
            self::OPEN_ID_REDAC,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD')
        )[AbstractService::ACCESS_TOKEN];
    }

    private function getRedacBaseUrl(): string
    {
        return $this->parameterBag->get('URL_RSEM') . self::URL_API;
    }
}
