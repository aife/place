<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use AtexoCrypto\Dto\InfosSignature;

class CertificateTransformer
{
    public string $fileName;
    public string $jetonName;
    public bool $isValidCertificate;
    public array $certificateFor;
    public array $certificateBy;
    public ?string $validFrom;
    public ?string $validTo;
    public ?string $controlDate;
    public bool $isPeriodValid;
    public bool $isChainValid;
    public string $certificateReference;
    public bool $isRevocation;
    public bool $isRepudiation;
    public ?string $certificateFormat;
    public ?string $dateSignature;
    public ?bool $isTimestamp;
    public ?bool $eidas;
    public ?bool $timestamp;

    public static function transform(
        string $fileName,
        string $jetonName,
        InfosSignature | array  | null $certificates,
        bool $surchargeReferentielsModule = false
    ): ?array {
        if (null === $certificates) {
            return null;
        }
        $certificates = !is_array($certificates) ? [$certificates] : $certificates;
        $result = [];

        foreach ($certificates as $certificate) {
            $transformedCertificate = new self();
            $transformedCertificate->fileName = $fileName;
            $transformedCertificate->jetonName = $jetonName;
            $transformedCertificate->certificateFor = self::formatCertificateString(
                $certificate->getSignataireComplet()
            );
            $transformedCertificate->certificateBy = self::formatCertificateString($certificate->getEmetteur());
            $transformedCertificate->validFrom = $certificate->getDateValiditeDu();
            $transformedCertificate->validTo = $certificate->getDateValiditeAu();
            $transformedCertificate->controlDate = (new \DateTime())->format('d/m/Y h:i');
            $transformedCertificate->isPeriodValid = boolval($certificate->getPeriodiciteValide());
            $transformedCertificate->isChainValid = self::isChainValid($certificate, $surchargeReferentielsModule);
            $transformedCertificate->certificateReference = (is_array($certificate->getRepertoiresChaineCertification())
                && array_key_exists('nom', $certificate->getRepertoiresChaineCertification()[0]))
                ? $certificate->getRepertoiresChaineCertification()[0]['nom'] : '';
            $transformedCertificate->isRevocation = 0 !== $certificate->getAbsenceRevocationCRL();
            $transformedCertificate->isRepudiation = boolval($certificate->getSignatureValide());
            $transformedCertificate->certificateFormat = $certificate->getFormatSignature();
            $transformedCertificate->dateSignature = $certificate->getDateIndicative();
            $transformedCertificate->isTimestamp = 1 === $certificate->getJetonHorodatage();
            $transformedCertificate->timestamp = $certificate->getDateHorodatage() ? $certificate->getDateHorodatage()
                : null;
            $transformedCertificate->isValidCertificate = self::isValidCertificate($transformedCertificate);
            $transformedCertificate->eidas = $certificate->getQualifieEIDAS();
            $transformedCertificate->dateRevocation = $certificate->getDateRevocation();

            $result[] = $transformedCertificate;
        }
        return $result;
    }

    private static function isValidCertificate(self $certificate): bool
    {
        if (
            !$certificate->isPeriodValid
            || !$certificate->isChainValid
            || $certificate->isRevocation
            || !$certificate->isRepudiation
        ) {
            return false;
        }

        return true;
    }

    private static function formatCertificateString(?string $certificateString): array
    {
        if (empty($certificateString)) {
            return [];
        }

        $infos = explode(',', $certificateString);
        foreach ($infos as $key => $info) {
            if (!preg_match('/^[A-Z]{1,2}=.+/', $info)) {
                unset($infos[$key]);
            }
        }

        return $infos;
    }

    private static function isChainValid(InfosSignature $certificate, bool $surchargeReferentielsModule): bool
    {
        if (true === $surchargeReferentielsModule) {
            if (
                is_array($certificate->getRepertoiresChaineCertification())
                && array_key_exists('statut', $certificate->getRepertoiresChaineCertification()[0])
                && $certificate->getRepertoiresChaineCertification()[0]['statut'] === 'OK'
            ) {
                return true;
            }
        } else {
            return $certificate->getSignatureValide();
        }

        return false;
    }
}
