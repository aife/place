<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Repository\ResetPasswordRequestRepository;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use DateTime;
use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Entity\ResetPasswordRequest as ResetPasswordRequestEntity;
use App\Security\FormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Twig\Environment;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\Response;

class ResetPasswordRequest
{
    private const USER_TYPE_AGENT = 'agent';
    private const USER_TYPE_ENTREPRISE = 'entreprise';

    /**
     * ResetPasswordRequest constructor.
     * @param TranslatorInterface $translator ,
     */
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Environment $twig,
        private readonly TranslatorInterface $translator,
        private readonly AtexoUtil $atexoUtil,
        private readonly GuardAuthenticatorHandler $guardAuthenticatorHandler,
        private readonly FormAuthenticator $formAuthenticator,
        private readonly ResetPasswordRequestRepository $resetPasswordRequestRepository
    ) {
    }

    public function processSendingPasswordResetEmail(string $email, string $calledFrom)
    {
        $user = $this->getUserByEmail($email, $calledFrom);

        if (
            ($user instanceof Agent && $user->getActif() == '1') ||
            ($user instanceof Inscrit && '0' === $user->getBloque())
        ) {
            $jeton = $this->getJetonUpdatePassword($user->getId(), $calledFrom);
            $this->save($user->getId(), $calledFrom, $user->getEmail(), $jeton);
            $this->sendMail($user, $jeton, $calledFrom);
        }
    }

    /**
     * @return Agent|Inscrit|null
     */
    public function validateTokenAndFetchUser(?ResetPasswordRequestEntity $resetPasswordRequest)
    {
        $user = null;

        if (
            !empty($resetPasswordRequest) &&
            $resetPasswordRequest->getModificationFaite() === '0' &&
            $resetPasswordRequest->getDateFinValidite() > date('Y-m-d H:i:s')
        ) {
            $user = $this->getUserByEmail($resetPasswordRequest->getEmail(), $resetPasswordRequest->getTypeUser());
        }

        return $user;
    }

    public function getResetPasswordRequestByJeton(string $jeton): ?ResetPasswordRequestEntity
    {
        return $this->em->getRepository(ResetPasswordRequestEntity::class)->findOneBy([
            'jeton' => $jeton,
        ]);
    }

    public function logIn(Request $request, string $calledFrom, Agent|Inscrit $user): Response
    {
        $providerKey = $calledFrom . '_provider';

        return $this->guardAuthenticatorHandler
            ->authenticateUserAndHandleSuccess(
                $user,
                $request,
                $this->formAuthenticator,
                $providerKey
            );
    }

    /**
     * @return Agent|Inscrit|null
     */
    private function getUserByEmail(string $email, string $calledFrom)
    {
        $user = null;

        if ($calledFrom == self::USER_TYPE_AGENT) {
            $user = $this->em->getRepository(Agent::class)->findOneBy([
                'email' => $email,
            ]);
        } elseif ($calledFrom == self::USER_TYPE_ENTREPRISE) {
            $user = $this->em->getRepository(Inscrit::class)->findOneBy([
                'email' => $email,
            ]);
        }

        return $user;
    }

    private function sendMail($user, string $jeton, string $typeUser)
    {
        $mailer = new Swift_Mailer(new Swift_SmtpTransport());
        $beforeEmail = $this->parameterBag->get('PF_SHORT_NAME') . ' - ' .
            $this->parameterBag->get('PF_LONG_NAME');
        $validateToken = $this->parameterBag->get('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD');

        $message = (new Swift_Message())
            ->setFrom([$this->parameterBag->get('PF_MAIL_FROM') => $beforeEmail])
            ->setSubject($this->translator->trans('DEMANDE_RE_INITIALISER_MOT_PASSE'))
            ->setDate(new DateTime('now'))
            ->setTo($user->getEmail())
            ->setBody(
                $this->twig->render(
                    'email/reset-password.html.twig',
                    [
                        'jeton' => $jeton,
                        'user' => $typeUser,
                        'plateforme' => $beforeEmail,
                        'couleur_titre' => $this->parameterBag->get('MAIL_COULEUR_TITRE'),
                        'validateToken' => $validateToken,
                        'login' =>  $user->getLogin(),
                    ]
                ),
                'text/html'
            );

        $mailer->send($message);
    }

    private function getJetonUpdatePassword(int $idUser, string $typeUser): string
    {
        do {
            $jeton = sha1($idUser . $typeUser . date('Y-m-d H:i:s'));

            $resetPasswordRequest = $this->getResetPasswordRequestByJeton($jeton);
        } while (!empty($resetPasswordRequest));

        return $jeton;
    }

    private function save(int $idUser, string $typeUser, string $email, string $jeton): void
    {
        $resetPasswordRequest = new ResetPasswordRequestEntity();
        $dateDebut = date('Y-m-d H:i:s');

        $resetPasswordRequest->setIdUser($idUser);
        $resetPasswordRequest->setTypeUser($typeUser);
        $resetPasswordRequest->setEmail($email);
        $resetPasswordRequest->setJeton($jeton);
        $resetPasswordRequest->setDateDemandeModification($dateDebut);
        $dateFin = $this->atexoUtil->addIntervalToDate(
            $dateDebut,
            $this->parameterBag->get('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD')
        );
        $resetPasswordRequest->setDateFinValidite($dateFin);

        $this->em->persist($resetPasswordRequest);
        $this->em->flush();
    }

    public function validateAllResetPasswordRequest(int $idUser)
    {
        $resetPasswordRequests = $this->resetPasswordRequestRepository->findBy([
            'idUser' => $idUser,
        ]);

        foreach ($resetPasswordRequests as $resetPasswordRequest) {
            $resetPasswordRequest->setModificationFaite('1');
        }

        $this->em->flush();
    }
}
