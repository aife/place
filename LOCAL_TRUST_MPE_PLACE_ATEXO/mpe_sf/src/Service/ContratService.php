<?php

namespace App\Service;

use App\Entity\GeolocalisationN1;
use App\Model\DonneesEssentielles\Marches;
use App\Service\DataTransformer\ClausesTransformer;
use Exception;
use App\Model\DonneesEssentielles\Marche\TitulairesAType\TitulaireAType;
use App\Model\DonneesEssentielles\Marche\ContactTitulaireType;
use DateTime;
use App\Model\DonneesEssentielles\Marche\ModificationsAType;
use App\Model\DonneesEssentielles\Marche\ModificationsAType\ModificationAType;
use App\Model\DonneesEssentielles\Marche\NumerosContratType;
use App\Model\DonneesEssentielles\Marche\ContratTransverse;
use App\Model\DonneesEssentielles\Marche\EntiteEligible;
use App\DataTransformer\Output\ContratTitulaireOutputDataTransformer;
use App\Dto\Output\ContratTitulaireOutput;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\CategoriesConsiderationsSociales;
use App\Entity\ConsLotContrat;
use App\Entity\Consultation;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\DonneesAnnuellesConcession;
use App\Entity\TechniqueAchat;
use App\Model\DonneesEssentielles\Marche;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\GeolocalisationN2;
use App\Entity\InvitationConsultationTransverse;
use App\Entity\Lot;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TypeContrat;
use App\Entity\TypeContratConcessionPivot;
use App\Entity\TypeProcedureConcessionPivot;
use App\Entity\TypeProcedurePivot;
use App\Entity\ValeurReferentiel;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\AchatResponsableType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ClausesEnvironnementalesConditionExecutionType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ClausesEnvironnementalesSpecificationTechniqueType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ClausesSocialesConditionExecutionType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ClausesSocialesSpecificationTechniqueType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ConsiderationsEnvironnementalesType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ConsiderationsSocialesType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\CriteresEnvironnementauxAttributionType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\CriteresSociauxAttributionType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\MarcheReserveType;
use App\Model\DonneesEssentielles\Marche\AchatResponsable\ObjetInsertionType;
use App\Model\DonneesEssentielles\Marche\AcheteurAType;
use App\Model\DonneesEssentielles\Marche\ConsultationAType;
use App\Model\DonneesEssentielles\Marche\ContratChapeauAcSadType;
use App\Model\DonneesEssentielles\Marche\ContratChapeauMultiAttributairesType;
use App\Model\DonneesEssentielles\Marche\ContratsChapeauxType;
use App\Model\DonneesEssentielles\Marche\DonneeAnnuelle;
use App\Model\DonneesEssentielles\Marche\DonneesAnnuelles;
use App\Model\DonneesEssentielles\Marche\DonneesComplementaires;
use App\Model\DonneesEssentielles\Marche\LieuExecutionAType;
use App\Model\DonneesEssentielles\Marche\LotAType;
use App\Model\DonneesEssentielles\Marche\Tarif;
use App\Model\DonneesEssentielles\Marche\Tarifs;
use App\Model\DonneesEssentielles\Marche\TitulairesAType;
use Application\Propel\Mpe\CommonConsultation;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Consultation\ClausesN1;
use App\Entity\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN1 as ReferentielClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2 as ReferentielClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3 as ReferentielClausesN3;

/**
 * Class ContratService.
 */
class ContratService
{
    private $categoriesConsiderationsSociales;
    private $isConcession;
    private ?ClausesN1 $clausesSociales = null;
    private ?ClausesN1 $clausesEnvironnementales = null;

    /**
     * @return mixed
     */
    public function getIsConcession()
    {
        return $this->isConcession;
    }

    /**
     * @param mixed $isConcession
     */
    public function setIsConcession($isConcession)
    {
        $this->isConcession = $isConcession;
    }

    /**
     * ContratService constructor.
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly TranslatorInterface $translator,
        private readonly EntityManagerInterface $em,
        /**
         * @var
         */
        private readonly AtexoUtil $atexoUtil,
        private readonly AtexoConfiguration $moduleStateChecker,
        private readonly LoggerInterface $logger,
        private readonly ClausesTransformer $clausesTransformer
    ) {
    }

    public function getCategoriesConsiderationsSociales()
    {
        if (is_null($this->categoriesConsiderationsSociales)) {
            $this->categoriesConsiderationsSociales = $this->em
                ->getRepository(CategoriesConsiderationsSociales::class)
                ->getAllFormatedCategories();
        }
        return $this->categoriesConsiderationsSociales;
    }

    /**
     * @param $parameters
     * @param $orgServicesAllowed
     *
     * @return array
     */
    public function getContracts($parameters, $orgServicesAllowed = [])
    {
        $query = $this->em
            ->getRepository(ContratTitulaire::class)
            ->getContracts(
                $parameters['dateNotifMin'],
                $parameters['dateNotifMax'],
                $parameters['siren'],
                $parameters['complement'],
                $parameters['dateDerniereModif'],
                $parameters['statutPublicationSn'],
                $parameters['exclusionOrganisme'],
                $parameters['limit'],
                $orgServicesAllowed
            );

        $contrats = [];

        foreach ($query as $currentReview) {
            $contrats[] = $currentReview;
        }

        return $contrats;
    }

    /**
     * @return mixed
     */
    public function isConcession(ContratTitulaire $contrat)
    {
        $typeContrat = $this->em
            ->getRepository(TypeContrat::class)
            ->find($contrat->getIdTypeContrat());

        return $typeContrat->isConcession();
    }

    public function getTypeContrat(): string
    {
        return $this->getIsConcession() ? 'CONTRAT_DE_CONCESSION' : 'MARCHE_PUBLIC';
    }

    /**
     * Retourne une collection de marche pour un contrat
     * c'est à dire le contrat initiale et la liste des modifications.
     *
     * @param $contrat
     * @param bool $formatExted
     * @param int  $statutPublicationSn
     *
     * @return array
     */
    public function listMarcheForContrat($contrat, $formatExted = false, $statutPublicationSn = null)
    {
        $tabContrat = null;
        $marches = [];
        $modificationContrats = null;
        if (is_array($contrat)) {
            $tabContrat = $contrat['contrat'];
            if (array_key_exists('modifications', $contrat)) {
                $modificationContrats = $contrat['modifications'];
            }
        }

        $marcheInitiale = $this->getFormatPivot($tabContrat, $formatExted);
        if ($formatExted) {
            try {
                $marcheInitiale = $this->getExtendedFormatFromPivot($tabContrat, $marcheInitiale);
                $marcheInitiale->setContratTransverse($this->getEntitesEligibles($tabContrat));
            } catch (Exception $e) {
                $this->logger->error(
                    sprintf(
                        "Erreur sur le contrat id_contrat_titulaire '%s' : %s",
                        $tabContrat->getIdContratTitulaire(),
                        $e->getMessage()
                    )
                );
                $marcheInitiale = null;
            }
        }
        if (!empty($marcheInitiale)) {
            if (is_null($statutPublicationSn) || $tabContrat->getStatutPublicationSn() == $statutPublicationSn) {
                $marches[] = $marcheInitiale;
            }

            $listModifications = $this->addModificationContratForMarche(
                $modificationContrats,
                $marcheInitiale,
                $tabContrat
            );
            foreach ($listModifications as $listModification) {
                $marches[] = $listModification;
            }
        }


        return $marches;
    }

    public function listMarcheForContratCriteres($contrat, $formatExted = false)
    {
        $tabContrat = null;
        $marches = null;
        $modificationContrats = null;
        if (is_array($contrat)) {
            $tabContrat = $contrat['contrat'];
            if (array_key_exists('modifications', $contrat)) {
                $modificationContrats = $contrat['modifications'];
            }
        }

        $marcheInitiale = $this->getFormatPivot($tabContrat);
        if ($formatExted) {
            $marcheInitiale = $this->getExtendedFormatFromPivot($tabContrat, $marcheInitiale);
        }

        $marches[] = $marcheInitiale;

        $listModifications = $this->addModificationContratForMarche(
            $modificationContrats,
            $marcheInitiale,
            $tabContrat
        );

        foreach ($listModifications as $listModification) {
            $marches[] = $listModification;
        }

        $donneesAnnuelles = self::addDonneesAnnuelles($contrat, $marcheInitiale);
        if (!empty($donneesAnnuelles)) {
            foreach ($donneesAnnuelles as $donneeAnnuelle) {
                $marches[] = $donneeAnnuelle;
            }
        }

        return $marches;
    }

    /**
     * @param $contrat
     * @param $marche
     *
     * @return mixed|null
     */
    public function addDonneesAnnuelles($contrat, $marcheInitiale)
    {
        if (is_array($contrat)) {
            $tabContrat = $contrat['contrat'];
            if (array_key_exists('ajoutDonneeAnnuelles', $contrat)) {
                return $this->setMarcheConcession($tabContrat, $marcheInitiale);
            }
        }

        return null;
    }

    /**
     * @param $marcheInitiale
     *
     * @return array
     */
    public function setMarcheConcession(ContratTitulaire $contrat, $marcheInitiale)
    {
        $marches = [];
        $donneesAnnuelleConcessions = $this->em
            ->getRepository(DonneesAnnuellesConcession::class)
            ->findBy(['idContrat' => $contrat->getIdContratTitulaire()]);

        $numOrdre = null;

        foreach ($donneesAnnuelleConcessions as $ind => $donneesAnnuelleConcession) {
            $donneeAnnuelles = new DonneesAnnuelles();
            $donneeAnnuelle = new DonneeAnnuelle();
            $marche = clone $marcheInitiale;
            $donneeAnnuelle->setDatePublicationDonneesExecution(
                $donneesAnnuelleConcession->getDateSaisie()->format('Y-m-d')
            );
            $donneeAnnuelle->setDepensesInvestissement($donneesAnnuelleConcession->getValeurDepense());
            $numOrdre = $donneesAnnuelleConcession->getNumOrdre();
            $tarifs = new Tarifs();
            foreach ($donneesAnnuelleConcession->getDonneesAnnuellesConcessionTarifs() as $donneesTarifs) {
                $tarif = new Tarif();
                $tarif->setTarif($donneesTarifs->getMontant());
                $tarif->setIntituleTarif($donneesTarifs->getIntituleTarif());
                $tarifs->setTarif($tarif);
                $donneeAnnuelle->setTarifs($tarifs);
            }
            $donneeAnnuelles->setDonneesAnnuelle($donneeAnnuelle);

            $marche
                ->setId(self::getId($contrat, $numOrdre, true))
                ->setDateDebutExecution($contrat->getDateDebutExecution())
                ->setDonneesExecution($donneeAnnuelles)
                ->setDateSignature($contrat->getDateNotification())
                ->setMontantSubventionPublique(
                    $this->atexoUtil->formatterMontant(
                        $contrat->getMontantSubventionPublique(),
                        2,
                        true
                    )
                );
            $marches[] = $marche;
            unset($marche);
        }

        return $marches;
    }

    public function getFormatExtendu($contrat)
    {
        $marcheInitiale = $this->getFormatPivot($contrat);
        $marche = $this->getExtendedFormatFromPivot($contrat, $marcheInitiale);

        return $marche;
    }

    /**
     * @param $id
     * @param null $numOrdre
     * @param bool $donneeAnnuelle
     */
    public function getId($contrat, $numOrdre = null, $donneeAnnuelle = false): bool|string
    {
        $id = $contrat->getNumIdUniqueMarchePublic();
        $accronyme = $contrat->getOrganisme();
        if (true === $this->moduleStateChecker->hasConfigOrganisme($accronyme, 'numDonneesEssentiellesManuel')) {
            $res = $id;
        } else {
            if (true === $donneeAnnuelle) {
                $res = substr($id, 0, -2);
                $res .= '_' . str_pad($numOrdre, 2, 0, STR_PAD_LEFT);
            } elseif (!empty($numOrdre)) {
                $res = substr($id, 0, -2);
                $res .= str_pad($numOrdre, 2, 0, STR_PAD_LEFT);
            } else {
                $res = substr($id, 0, -2) . '00';
            }
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function getNature(ContratTitulaire $contrat, $isConcession)
    {
        if ($isConcession) {
            if (!empty($contrat->getIdTypeContratConcessionPivot())) {
                $typeContratConcessionPivot = $this->em
                    ->getRepository(TypeContratConcessionPivot::class)
                    ->find($contrat->getIdTypeContratConcessionPivot());

                return $this->translator->trans($typeContratConcessionPivot->getLibelle());
            }

            return '';
        } else {
            return $this->translator->trans($contrat->getLibelleTypeContratPivot());
        }
    }

    /**
     * @return Marche
     *
     * @throws Exception
     */
    public function getFormatPivot(ContratTitulaire $contrat, bool $extendedFormat = false)
    {
        $this->isConcession = $this->isConcession($contrat);

        $acheteur = new AcheteurAType();
        $acheteur
            ->setId($contrat->getSiretFormater())
            ->setNom(
                $this->atexoUtil->atexoHtmlEntitiesDecode($contrat->getNomEntiteAcheteur())
            );

        $lieuExecution = new LieuExecutionAType();

        if (!empty($contrat->getNomLieuPrincipalExecution())) {
            $geolocalisation = $this->em
                ->getRepository(GeolocalisationN2::class)
                ->findOneBy(['denomination1' => $contrat->getNomLieuPrincipalExecution()]);
        } elseif ($contrat->getLieuExecution()) {
            $ids = explode(',', $contrat->getLieuExecution());
            $id = null;
            foreach ($ids as $id) {
                if (!empty($id)) {
                    break;
                }
            }
            if ($id) {
                $geolocalisation = $this->em
                    ->getRepository(GeolocalisationN2::class)
                    ->findOneBy(['id' => $id]);
            }
        }

        $arrayFormePrix = $contrat->getFormes();
        $formePrix = $contrat->getFormePrix() ? $arrayFormePrix[$contrat->getFormePrix()] : '';

        if (isset($geolocalisation)) {
            $type = 'Code département';
            $geolocalisationN1 = $this->em
                ->getRepository(GeolocalisationN1::class)
                ->findOneBy(
                    [
                        'id' => $geolocalisation->getIdGeolocalisationN1()
                    ]
                );

            if ($geolocalisationN1 instanceof GeolocalisationN1) {
                if (
                    $geolocalisationN1->getIdGeolocalisationN0() === 3
                    || $geolocalisationN1->getIdGeolocalisationN0() === 2
                ) {
                    $type = $geolocalisationN1->getDenomination1();
                }
            }

            if ($type === 'Pays') {
                $type = 'Code pays';
            }

            $lieuExecution
                ->setNom($geolocalisation->getDenomination1())
                ->setCode($geolocalisation->getDenomination2())
                ->setTypeCode($type);
        }

        if (!empty($contrat->getProcedurePassationPivot())) {
            $procedurePassation = $this->em
                ->getRepository(TypeProcedurePivot::class)
                ->find($contrat->getProcedurePassationPivot());
        }

        $titulaires = new TitulairesAType();
        $titulaire = new TitulaireAType();

        if (
            null !== $contrat->getIdTitulaire()
            && null !== $contrat->getIdTitulaireEtab()
            && 0 != $contrat->getIdTitulaireEtab()
        ) {
            $entreprise = $this->em
                ->getRepository(Entreprise::class)
                ->find($contrat->getIdTitulaire());

            $etablissement = $this->em
                ->getRepository(Etablissement::class)
                ->find($contrat->getIdTitulaireEtab());

            if (null !== $entreprise && null !== $etablissement) {
                $typeIdentifiant = 'SIRET';
                $siren = $entreprise->getSiren();

                if (empty($siren)) {
                    $typeIdentifiant = 'HORS-UE';
                    $siren = $entreprise->getSirenetranger();
                }
                $titulaire
                    ->setId($siren . $etablissement->getCodeEtablissement())
                    ->setDenominationSociale($entreprise->getNom())
                    ->setTypeIdentifiant($typeIdentifiant);

                // Ajout des informations du contact provenant de la table t_contact_contrat avec l'id inscrit
                // Sachant que les données du contact peuvent ne pas correspondre avec celui de l'inscrit
                if (
                    $extendedFormat
                    && ($contactContrat = $contrat->getContact())
                ) {
                    $contactTitulaire = new ContactTitulaireType();
                    $contactTitulaire
                        ->setId($contactContrat->getIdInscrit())
                        ->setEmail($contactContrat->getEmail())
                        ->setNom($contactContrat->getNom())
                        ->setPrenom($contactContrat->getPrenom())
                    ;

                    $titulaire->setContact($contactTitulaire);
                }

                $titulaires->setTitulaire([$titulaire]);
            }
        }
        $marche = new Marche();

        $marche
            ->setId($this->getId($contrat))
            ->setUuid($contrat->getUuid())
            ->setAcheteur($acheteur)
            ->setCodeCPV($contrat->getCodeCpv1())
            ->setDateNotification(
                $contrat->getDateNotification() ?: new DateTime()
            )
            ->setDatePublicationDonnees(
                $contrat->getDateNotification() ?: new DateTime()
            )
            ->setDureeMois($contrat->getDureeInitialeContrat())
            ->setFormePrix($formePrix)
            ->setLieuExecution($lieuExecution)
            ->setNature($this->getNature($contrat, $this->isConcession))
            ->setObjet($this->atexoUtil->atexoHtmlEntitiesDecode($this->atexoUtil
                ->truncate($contrat->getObjetContrat())))
            ->setProcedure($this->getProcedure($contrat, $this->isConcession))
            ->setTitulaires($titulaires->getTitulaire())
            ->setTypeContrat($this->getTypeContrat());

        if ($this->isConcession) {
            $marche
                ->setDateDebutExecution($contrat->getDateDebutExecution())
                ->setDateSignature($contrat->getDateNotification())
                ->setMontantSubventionPublique(
                    $this->atexoUtil->formatterMontant(
                        $contrat->getMontantSubventionPublique(),
                        2,
                        true
                    )
                );
            $marche->setValeurGlobale($this->atexoUtil->formatterMontant($contrat->getMontantContrat(), 2, true));
        } else {
            $marche->setMontant($this->atexoUtil->formatterMontant($contrat->getMontantContrat(), 2, true));
        }

        return $marche;
    }

    /**
     * @param $contrat
     * @param $marcheInitiale
     *
     * @return array
     */
    private function addModificationContratForMarche($modificationsContrat, $marcheInitiale, $contrat)
    {
        $marches = [];
        $marche = clone $marcheInitiale;

        if (isset($modificationsContrat)) {
            $historiesModifications = [];
            foreach ($modificationsContrat as $modificationContrat) {
                $modifications = new ModificationsAType();
                $modification = new ModificationAType();
                $modification
                    ->setObjetModification($this->atexoUtil->truncate($modificationContrat->getObjetModification()))
                    ->setDateSignatureModification($modificationContrat->getDateSignature())
                    ->setDatePublicationDonneesModification($modificationContrat->getDateModification());

                if (null != $modificationContrat->getIdEtablissement()) {
                    $etablissementModification = $this->em
                        ->getRepository(Etablissement::class)
                        ->find($modificationContrat->getIdEtablissement());

                    $entrepriseModification = $this->em
                        ->getRepository(Entreprise::class)
                        ->find($etablissementModification->getIdEntreprise());

                    $titulaireModification =
                        new Marche\ModificationsAType\ModificationAType\TitulairesAType\TitulaireAType();
                    $titulaireModification
                        ->setTypeIdentifiant('SIRET')
                        ->setId(
                            $entrepriseModification->getSiren() . $etablissementModification->getCodeEtablissement()
                        )
                        ->setDenominationSociale($entrepriseModification->getNom());

                    $titulairesModification = new Marche\ModificationsAType\ModificationAType\TitulairesAType();
                    $titulairesModification
                        ->setTypeIdentifiant('SIRET');
                    $titulairesModification->setId(
                        $entrepriseModification->getSiren() . $etablissementModification->getCodeEtablissement()
                    );
                    $titulairesModification->setDenominationSociale($entrepriseModification->getNom());
                    $titulairesModification->addToTitulaire($titulaireModification);

                    $modification->setTitulaires([$titulairesModification]);
                }

                if (null != $modificationContrat->getDureeMarche()) {
                    $modification->setDureeMois($modificationContrat->getDureeMarche());
                }

                if (null != $modificationContrat->getMontant()) {
                    if ($this->isConcession($contrat)) {
                        $modification->setValeurGlobale(
                            $this->atexoUtil->formatterMontant(
                                $modificationContrat->getMontant(),
                                2,
                                true
                            )
                        );
                        $marche->setValeurGlobale(null);
                    } else {
                        $modification->setMontant(
                            $this->atexoUtil->formatterMontant(
                                $modificationContrat->getMontant(),
                                2,
                                true
                            )
                        );
                    }
                }
                $historiesModifications[] = $modification;
                foreach ($historiesModifications as $Historiesmodification) {
                    $modifications->addToModification($Historiesmodification);
                }

                $marche->setModifications([$modification]);

                $numOrdre = $modificationContrat->getNumOrdre();
                $marche->setId(self::getId($contrat, $numOrdre));

                if (0 == $modificationContrat->getStatutPublicationSn()) {
                    $marches[] = $marche;
                }
                $marche = clone $marcheInitiale;
            }
        }

        return $marches;
    }

    /**
     * @return string
     */
    public function getProcedure(ContratTitulaire $contrat, $isConcession)
    {
        if ($isConcession) {
            if (!empty($contrat->getIdTypeProcedureConcessionPivot())) {
                $ProcedureConcessionPivot = $this->em
                    ->getRepository(TypeProcedureConcessionPivot::class)
                    ->find($contrat->getIdTypeProcedureConcessionPivot());

                return $this->translator->trans($ProcedureConcessionPivot->getLibelle());
            } else {
                return '';
            }
        } else {
            if (!empty($contrat->getProcedurePassationPivot())) {
                $procedurePassation = $this->em
                    ->getRepository(TypeProcedurePivot::class)
                    ->find($contrat->getProcedurePassationPivot());
            }

            return isset($procedurePassation) ? $this->translator->trans($procedurePassation->getLibelle()) : '';
        }
    }

    /**
     * @param ContratTitulaire $contrat
     *
     * @return Marche
     */
    public function getExtendedFormatFromPivot($contrat, $marche)
    {
        $donneesComplementaires = new DonneesComplementaires();

        $nature = $this->em
            ->getRepository(TypeContrat::class)
            ->find($contrat->getIdTypeContrat());

        $donneesComplementaires
            ->setProcedurePassation($contrat->getLibelleTypeProcedureMpe())
            ->setNaturePassation($this->translator->trans($nature->getLibelleTypeContrat()))
            ->setAccordCadreAvecMarcheSubsequent($contrat->isAcMarcheSubsequent())
            ->setCodesCPVSec(trim(str_replace('#', ',', $contrat->getCodeCpv2()), ','))
            ->setNbTotalPropositionsDemat($contrat->getNbTotalPropositionsDematLot())
            ->setNbTotalPropositionsRecu($contrat->getNbTotalPropositionsLot());

        // faire un traitement spécial pour  ClauseSociale et ClauseEnvironnementale
        $donneesComplementaires->setAchatResponsable($this->getAchatResponsableType($contrat));
        $donneesComplementaires->setClauses($this->getAchatResponsablesType($contrat));

        $consLotContrat = $this->em
            ->getRepository(ConsLotContrat::class)
            ->findOneBy([
                'idContratTitulaire' => $contrat->getIdContratTitulaire(),
            ]);

        $consultationDE = new ConsultationAType();

        if ($consLotContrat) {
            $consultation = $this->em
                ->getRepository(Consultation::class)
                ->findOneBy([
                    'id' => $consLotContrat->getConsultationId(),
                    'organisme' => $consLotContrat->getOrganisme(),
                ]);
            if ($consultation) {
                $consultationDE->setId($consultation->getId())
                    ->setNumero($consultation->getReferenceUtilisateur())
                    ->setCodeExterne($consultation->getCodeExterne());

                $donneesComplementaires->setConsultation($consultationDE);

                if ($consultation->getAlloti()) {
                    $donneesComplementaires->setLot($this->getLot($consLotContrat));
                }
            }
        } elseif (!empty($contrat->getReferenceConsultation())) {
            //Ceci est une bidouille pour MPE-9205 afin d'avoir le numero manuel pour contrat ex-nihilo
            $consultationDE->setNumero($contrat->getReferenceConsultation());
            $donneesComplementaires->setConsultation($consultationDE);
        }

        $donneesComplementaires->setNumeroEJ($this->formatNumeroEJ($contrat->getNumEJ()));

        if ('1' == $nature->getMarcheSubsequent()) {
            $donneesComplementaires->setSiretPAAccordCadre($contrat->getSiretPaAccordCadre());
        }

        $ccag = $this->getCcag($contrat->getCcagApplicable());

        if ($ccag) {
            $donneesComplementaires->setCcagReference($ccag->getLibelleValeurReferentiel());
        }

        $donneesComplementaires->setNumerosContrat($this->getNumerosContrat($contrat));

        $service = $this->em->getRepository(Service::class)->findOneBy(
            ['id' => $contrat->getServiceId(), 'organisme' => $contrat->getOrganisme()]
        );
        $organisme = $this->em->getRepository(Organisme::class)->findOneBy(
            ['acronyme' => $contrat->getOrganisme()]
        );

        $agent = $this->em->getRepository(Agent::class)->find($contrat->getIdAgent());

        $contratsChapeaux = $this->getContratsChapeaux($contrat, $donneesComplementaires);
        $donneesComplementaires->setContratsChapeaux($contratsChapeaux);

        $serviceId = ($service instanceof Service) ? $service->getId() : '';
        $oldServiceId = ($service instanceof Service) ? $service->getOldId() : '';
        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
            $serviceId = empty($serviceId) ? '0' : $service->getOldId();
        }
        $marche
            ->getAcheteur()
            ->setIdOrganisme($organisme->getId())
            ->setLibelleOrganisme($organisme->getDenominationOrg())
            ->setidEntiteAchat($serviceId)
            ->setOldIdEntiteAchat($oldServiceId)
            ->setLibelleEntiteAchat(
                ($service instanceof Service) ? $service->getLibelle() : $organisme->getDenominationOrg()
            )
            ->setAccessChorus(
                ($service instanceof Service) ? $service->getAccesChorus() : '0'
            )
            ->setIdAgent($agent->getId())
            ->setPrenomAgent($agent->getPrenom())
            ->setNomAgent($agent->getNom())
            ->setEmailAgent($agent->getEmail())
            ->setLoginAgent($agent->getLogin());
        $marche->setDonneesComplementaires($donneesComplementaires);

        return $marche;
    }

    protected function getLot($consLotContrat)
    {
        $categorieLot = $this->em
            ->getRepository(Lot::class)
            ->getIntituleAndNumLot(
                $consLotContrat->getConsultationId(),
                $consLotContrat->getOrganisme(),
                $consLotContrat->getLot()
            );
        $lot = new LotAType();
        if (isset($categorieLot[0])) {
            $lot->setIntituleLot($this->atexoUtil->atexoHtmlEntitiesDecode($categorieLot[0]['intitule_lot']))
                ->setNumeroLot((int)$categorieLot[0]['num_lot']);
        }
        return $lot;
    }

    protected function getCcag($id)
    {
        return $this->em
            ->getRepository(ValeurReferentiel::class)
            ->findOneBy([
                'id' => $id,
                'idReferentiel' => ValeurReferentiel::REFERENTIEL_CCAG,
            ]);
    }

    protected function getNumerosContrat($contrat)
    {
        $numerosContrat = new NumerosContratType();
        $numerosContrat->setIdTechnique($contrat->getId());
        $numerosContrat->setReferenceLibre($contrat->getReferenceLibre());
        $numerosContrat->setNumeroCourt($contrat->getNumeroContrat());
        $numerosContrat->setNumeroLong($contrat->getNumLongOEAP());

        return $numerosContrat;
    }

    /**
     * Retourne l'objet ContratsChapeaux qui alimente les données essentielles.
     *
     * @param $contrat
     * @param $donneesComplementaires
     *
     * @return ContratsChapeauxType
     */
    protected function getContratsChapeaux($contrat, &$donneesComplementaires)
    {
        $contratsChapeaux = new ContratsChapeauxType();
        $contratChapeauMultiAttributaires = new ContratChapeauMultiAttributairesType();
        if ($contrat->getIdChapeau()) {
            $chapeau = $this->em
                ->getRepository(ContratTitulaire::class)
                ->find($contrat->getIdChapeau());
            if ($chapeau instanceof ContratTitulaire) {
                $contratChapeauMultiAttributaires->setIdTechniqueContratChapeauMultiAttributaires($chapeau->getId());
                $contratChapeauMultiAttributaires->setReferenceLibreChapeauMultiAttributaires(
                    $chapeau->getReferenceLibre()
                );
                $contratChapeauMultiAttributaires->setNumeroCourtContratChapeauMultiAttributaires(
                    $chapeau->getNumeroContrat()
                );
            }
        }
        $contratsChapeaux->setContratChapeauMultiAttributaires($contratChapeauMultiAttributaires);

        $contratChapeauAcSad = new ContratChapeauAcSadType();
        if ($contrat->getLienACSAD()) {
            $accordCadre = $this->em
                ->getRepository(ContratTitulaire::class)
                ->find($contrat->getLienACSAD());

            if ($accordCadre instanceof ContratTitulaire) {
                if ($contrat->isAcMarcheSubsequent()) {
                    $donneesComplementaires->setIdAccordCadre($accordCadre->getNumIdUniqueMarchePublic());
                }
                $contratChapeauAcSad->setIdTechniqueContratChapeauAcSad($accordCadre->getId());
                $contratChapeauAcSad->setIdContratChapeauAcSad($accordCadre->getNumIdUniqueMarchePublic());
                $contratChapeauAcSad->setReferenceLibreContratChapeauAcSad($accordCadre->getReferenceLibre());
                $contratChapeauAcSad->setNumeroCourtContratChapeauAcSad($accordCadre->getNumeroContrat());
                $contratChapeauAcSad->setNumeroLongContratChapeauAcSad($accordCadre->getNumLongOEAP());
            }
        }

        $contratsChapeaux->setContratChapeauAcSad($contratChapeauAcSad);

        return $contratsChapeaux;
    }

    /**
     * Utilisé pour la génération XML côté entreprise.
     *
     * @param $modificationsContrat
     * @param $marcheInitiale
     *
     * @return array
     */
    public function setModificationContratForMarche($modificationsContrat, $marcheInitiale)
    {
        $marche = $marcheInitiale;

        if (isset($modificationsContrat)) {
            foreach ($modificationsContrat as $modificationContrat) {
                $modification = new ModificationAType();
                $modification
                    ->setObjetModification($modificationContrat->getObjetModification())
                    ->setDateSignatureModification($modificationContrat->getDateSignature())
                    ->setDatePublicationDonneesModification($modificationContrat->getDateModification());

                if (null != $modificationContrat->getIdEtablissement()) {
                    $etablissementModification = $this->em
                        ->getRepository(Etablissement::class)
                        ->find($modificationContrat->getIdEtablissement());

                    $entrepriseModification = $this->em
                        ->getRepository(Entreprise::class)
                        ->find($etablissementModification->getIdEntreprise());

                    $titulaireModification
                        = new Marche\ModificationsAType\ModificationAType\TitulairesAType\TitulaireAType();
                    $titulaireModification
                        ->setTypeIdentifiant('SIRET')
                        ->setId(
                            $entrepriseModification->getSiren() . $etablissementModification->getCodeEtablissement()
                        )
                        ->setDenominationSociale($entrepriseModification->getNom());

                    $titulairesModification = new Marche\ModificationsAType\ModificationAType\TitulairesAType();
                    $titulairesModification
                        ->setTypeIdentifiant('SIRET');
                    $titulairesModification->setId(
                        $entrepriseModification->getSiren() . $etablissementModification->getCodeEtablissement()
                    );
                    $titulairesModification->setDenominationSociale($entrepriseModification->getNom());
                    $titulairesModification->addToTitulaire($titulaireModification);

                    $modification->setTitulaires([$titulairesModification]);
                }

                if (null != $modificationContrat->getDureeMarche()) {
                    $modification->setDureeMois($modificationContrat->getDureeMarche());
                }

                if (null != $modificationContrat->getMontant()) {
                    $modification->setMontant(
                        $this->atexoUtil->formatterMontant($modificationContrat->getMontant(), 2, true)
                    );
                }
                $marche->addToModifications($modification);

                $numOrdre = $modificationContrat->getNumOrdre();
                if ($modificationContrat->getNumOrdre() < 10) {
                    $numOrdre = '0' . $numOrdre;
                }

                $contrat = $modificationContrat->getIdContratTitulaire();
                if (
                    true === $this->moduleStateChecker->hasConfigOrganisme(
                        $contrat->getOrganisme(),
                        'numDonneesEssentiellesManuel'
                    )
                ) {
                    $NumIdUniqueMarchePublic = $contrat->getNumIdUniqueMarchePublic();
                } else {
                    $NumIdUniqueMarchePublic = substr($marche->getId(), 0, -2) . $numOrdre;
                }

                $marche->setId($NumIdUniqueMarchePublic);
            }
        }

        return $marche;
    }

    public function setMarcheOrConcession(ContratTitulaire $contrat, $marcheInitiale)
    {
        $marches = [];
        $donneesAnnuelleConcessions = $this->em
            ->getRepository(DonneesAnnuellesConcession::class)
            ->findBy(['idContrat' => $contrat->getIdContratTitulaire()]);

        $numOrdre = null;
        $marche = $marcheInitiale;

        foreach ($donneesAnnuelleConcessions as $ind => $donneesAnnuelleConcession) {
            $donneeAnnuelles = new DonneesAnnuelles();
            $donneeAnnuelle = new DonneeAnnuelle();

            $donneeAnnuelle->setDatePublicationDonneesExecution(
                $donneesAnnuelleConcession->getDateSaisie()->format('Y-m-d')
            );
            $donneeAnnuelle->setDepensesInvestissement($donneesAnnuelleConcession->getValeurDepense());
            $numOrdre = $donneesAnnuelleConcession->getNumOrdre();
            $tarifs = new Tarifs();
            foreach ($donneesAnnuelleConcession->getDonneesAnnuellesConcessionTarifs() as $donneesTarifs) {
                $tarif = new Tarif();
                $tarif->setTarif($donneesTarifs->getMontant());
                $tarif->setIntituleTarif($donneesTarifs->getIntituleTarif());
                $tarifs->setTarif($tarif);
                $donneeAnnuelle->setTarifs($tarifs);
            }
            $donneeAnnuelles->setDonneesAnnuelle($donneeAnnuelle);
            $marche->setDonneesExecution($donneeAnnuelles);
        }

        $marche
            ->setDateDebutExecution($contrat->getDateDebutExecution())
            ->setDateSignature($contrat->getDateNotification())
            ->setMontantSubventionPublique(
                $this->atexoUtil->formatterMontant(
                    $contrat->getMontantSubventionPublique(),
                    2,
                    true
                )
            );

        return $marche;
    }

    /**
     * formatage du numéro EJ (on retire X_  et #.
     *
     * @param $numero
     *
     * @return mixed
     */
    protected function formatNumeroEJ($numero)
    {
        if (empty($numero)) {
            return '';
        }

        $numero = preg_replace('([0-9]+_)', '', $numero);
        $numero = str_replace('#', '', $numero);

        return $numero;
    }

    protected function getAchatResponsableType(ContratTitulaire $contrat): AchatResponsableType
    {
        $this->getClauses($contrat);
        $achatResponsable = new AchatResponsableType();
        $achatResponsable->setConsiderationsSociales($this->getConsiderationsSocialesType());
        $achatResponsable->setConsiderationsEnvironnementales($this->getConsiderationsEnvironnementalesType());

        return $achatResponsable;
    }

    /**
     * @return \stdClass[] $clauses
     */
    public function getAchatResponsablesType(ContratTitulaire $contrat): array
    {
        $data = [];
        $clauses = $this->getOuPutClauses($contrat);

        if (empty($clauses)) {
            return $data;
        }

        foreach ($clauses as $clauseN1) {
            $objectClausesN1 = $this->createObjectStdClass($clauseN1);
            foreach ($clauseN1['clausesN2'] as $clauseN2) {
                $objectClausesN2 = $this->createObjectStdClass($clauseN2);
                $objectClausesN1->clausesN2[] = $objectClausesN2;
                foreach ($clauseN2['clausesN3'] as $clauseN3) {
                    $objectClausesN3 = $this->createObjectStdClass($clauseN3);

                    $objectClausesN2->clausesN3[] = $objectClausesN3;
                }
            }

            $data[] = $objectClausesN1;
        }

        return $data;
    }

    public function getOuPutClauses(ContratTitulaire $contrat): array
    {
        return $this->clausesTransformer->getNestedClauses($contrat);
    }

    /**
     * @param string[] $clause
     */
    protected function createObjectStdClass(array $clause): \stdClass
    {
        $object = new \stdClass();
        $object->iri = $clause['iri'] ?? '';
        $object->label = $clause['label'] ?? '';
        $object->slug = $clause['slug'] ?? '';

        return $object;
    }

    protected function getConsiderationsSocialesType(): ConsiderationsSocialesType
    {
        $considerationsSociales = $this->getInitConsiderationsSocialesType();

        foreach ($this->clausesSociales?->getClausesN2() as $clauseN2) {
            if ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::CONDITION_EXECUTION) {
                $considerationsSociales->setClausesSocialesConditionExecution(
                    (new ClausesSocialesConditionExecutionType())->setValue(true)
                );
            } elseif (
                $clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::SPECIFICATION_TECHNIQUE
            ) {
                $considerationsSociales->setClausesSocialesSpecificationTechnique(
                    (new ClausesSocialesSpecificationTechniqueType())->setValue(true)
                );
            } elseif (
                $clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::CRITERE_ATTRIBUTION_MARCHE
            ) {
                $considerationsSociales->setCriteresSociauxAttribution(
                    (new CriteresSociauxAttributionType())->setValue(true)
                );
            } elseif ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::MARCHE_RESERVE) {
                $considerationsSociales->setMarcheReserve($this->getMarcheReserveType($clauseN2));
            } elseif ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::INSERTION) {
                $considerationsSociales->setObjetInsertion((new ObjetInsertionType())->setValue(true));
            }
        }

        return $considerationsSociales;
    }

    protected function getInitConsiderationsSocialesType(): ConsiderationsSocialesType
    {
        $considerationsSociales = new ConsiderationsSocialesType();

        $considerationsSociales->setValue(
            $this->clausesSociales instanceof ClausesN1
        );

        $considerationsSociales->setClausesSocialesConditionExecution(
            (new ClausesSocialesConditionExecutionType())->setValue(false)
        );
        $considerationsSociales->setClausesSocialesSpecificationTechnique(
            (new ClausesSocialesSpecificationTechniqueType())->setValue(false)
        );
        $considerationsSociales->setCriteresSociauxAttribution((
            new CriteresSociauxAttributionType())->setValue(false));
        $considerationsSociales->setObjetInsertion((new ObjetInsertionType())->setValue(false));

        $considerationsSociales->setMarcheReserve($this->getInitMarcheReserveType());

        return $considerationsSociales;
    }

    protected function getMarcheReserveType(ClausesN2 $clausesN2): MarcheReserveType
    {
        $marcheReserveType = $this->getInitMarcheReserveType();

        foreach ($clausesN2->getClausesN3() as $clauseN3) {
            if ($clauseN3->getReferentielClauseN3()->getSlug() === ReferentielClausesN3::CLAUSE_SOCIALE_SIAE) {
                $marcheReserveType->setSIAE(true);
            } elseif ($clauseN3->getReferentielClauseN3()->getSlug() === ReferentielClausesN3::CLAUSE_SOCIALE_EESS) {
                $marcheReserveType->setEESS(true);
            } elseif (
                $clauseN3->getReferentielClauseN3()->getSlug()
                === ReferentielClausesN3::CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE
            ) {
                $marcheReserveType->setESATEA(true);
            }
        }

        return $marcheReserveType;
    }

    protected function getInitMarcheReserveType(): MarcheReserveType
    {
        $marcheReserveType = new MarcheReserveType();

        $marcheReserveType->setEESS(false);
        $marcheReserveType->setSIAE(false);
        $marcheReserveType->setESATEA(false);

        return $marcheReserveType;
    }

    protected function getCategoriesSociales($listeClause)
    {
        $temp = [];
        $categories = $this->getCategoriesConsiderationsSociales();
        foreach ($listeClause as $idClause) {
            if (!empty($categories[$idClause])) {
                $temp[] = $categories[$idClause];
            }
        }

        return implode(',', $temp);
    }

    protected function getConsiderationsEnvironnementalesType(): ConsiderationsEnvironnementalesType
    {
        $considerationsEnvironnementales = $this->getInitConsiderationsEnvironnementalesType();
        foreach ($this->clausesEnvironnementales?->getClausesN2() as $clauseN2) {
            if ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::SPECIFICATIONS_TECHNIQUES) {
                $considerationsEnvironnementales->setClausesEnvironnementalesSpecificationTechnique(
                    new ClausesEnvironnementalesSpecificationTechniqueType(true)
                );
            } elseif ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::CONDITIONS_EXECUTIONS) {
                $considerationsEnvironnementales->setClausesEnvironnementalesConditionExecution(
                    new ClausesEnvironnementalesConditionExecutionType(true)
                );
            } elseif ($clauseN2->getReferentielClauseN2()->getSlug() === ReferentielClausesN2::CRITERES_SELECTIONS) {
                $considerationsEnvironnementales->setCriteresEnvironnementauxAttribution(
                    new CriteresEnvironnementauxAttributionType(true)
                );
            }
        }

        return $considerationsEnvironnementales;
    }

    protected function getInitConsiderationsEnvironnementalesType(): ConsiderationsEnvironnementalesType
    {
        $considerationsEnvironnementales = new ConsiderationsEnvironnementalesType();

        $considerationsEnvironnementales->setValue(
            $this->clausesEnvironnementales instanceof ClausesN1
        );
        $considerationsEnvironnementales->setClausesEnvironnementalesConditionExecution(
            new ClausesEnvironnementalesConditionExecutionType(false)
        );
        $considerationsEnvironnementales->setClausesEnvironnementalesSpecificationTechnique(
            new ClausesEnvironnementalesSpecificationTechniqueType(false)
        );
        $considerationsEnvironnementales->setCriteresEnvironnementauxAttribution(
            new CriteresEnvironnementauxAttributionType(false)
        );

        return $considerationsEnvironnementales;
    }

    /**
     * retourne tous les sous services d'un service.
     */
    public function getSubServices($serviceId, $justIds = false): array
    {
        $service = $this->em->getRepository(Service::class)->find($serviceId);

        if ($service) {
            $result = [];
            $this->getAllChilds($service->getId(), $result);

            if ($justIds) {
                $idsArray = [];

                foreach ($result as $value) {
                    $idsArray[] = $value->getId();
                }

                return $idsArray;
            }

            return $result;
        } else {
            return [];
        }
    }

    public function getAllChilds($serviceId, &$arrayChilds)
    {
        if (is_array($arrayChilds)) {
            $levelOneChilds = $this->getLevelOneChilds($serviceId);

            if (!empty($serviceId)) {
                $purchaseEntity = $this->em->getRepository(Service::class)
                    ->findOneBy([
                        'id' => $serviceId
                    ]);
                $arrayChilds[] = $purchaseEntity;
            }

            if (is_array($levelOneChilds) && 0 != count($levelOneChilds)) {
                foreach ($levelOneChilds as $child) {
                    $this->getAllChilds($child->getId(), $arrayChilds);
                }
            }
        }

        return $arrayChilds;
    }

    /**
     * retourne un tableau des entités filles de l'entité passée en paramètre.
     */
    public function getLevelOneChilds($idPurchaseEntity): array
    {
        $resServices = [];

        $resAffServices = $this->em
            ->getRepository(AffiliationService::class)
            ->getDistinctByIdPoleAndOrganisme($idPurchaseEntity);

        if (is_array($resAffServices) && count($resAffServices) > 0) {
            foreach ($resAffServices as $affService) {
                $service = $this->em
                    ->getRepository(Service::class)
                    ->findOneBy([
                        'id' => $affService->getServiceId()
                    ]);

                $resServices[] = $service;
            }
        }

        return $resServices;
    }

    /**
     * retourne un tableau ordonné des id des parents du service $serviceId.
     *
     * @param int  $serviceId
     * @param bool $includeCurrService si true, le service $serviceId est inclus dans le résultat
     *
     * @return array(id)
     */
    public function getArParentsId($serviceId, $org, $includeCurrService = false)
    {
        $listParent = [];
        $idParent = $this->getParent($serviceId, $org);

        while ($idParent) {
            $listParent[] = $idParent;
            $idParent = $this->getParent($idParent, $org);
        }

        $listParent[] = 0;

        if ($includeCurrService && !in_array($serviceId, $listParent)) {
            $listParent = [...[
                $serviceId,
            ], ...$listParent];
        }

        return $listParent;
    }

    /**
     * retourne l'id du service parent ou NULL s'il n'y en a pas.
     *
     * @param $serviceId
     * @param $org
     */
    public function getParent($serviceId, $org): bool|int
    {
        $service = $this->em
            ->getRepository(Service::class)
            ->findOneBy([
                'id' => $serviceId
            ]);

        if ($service) {
            return $service->getId();
        } else {
            return false;
        }
    }

    /**
     * retourne un tableau de fils.
     *
     * @param array $listChilds
     * @param $org
     * @param bool $withLibelle
     *
     * @return mixed
     */
    public function getAllChildsWithPath($listChilds, $org, $withLibelle = false)
    {
        $childArrayWithPath = [];
        array_shift($listChilds);

        if (is_array($listChilds)) {
            foreach ($listChilds as $child) {
                $childArrayWithPath['id'] = $child->getId();
                $childArrayWithPath['libelle'] = $this->getServiceById($child->getId(), $org);
                $parents = $this->getAllParents($child->getId(), $org);

                $childArrayWithPath['path'][$child->getId()] = $this->atexoUtil
                        ->implodeArrayWithSeparator(
                            $childArrayWithPath['libelle'],
                            $parents,
                            ' / '
                        ) . (
                            ($withLibelle) ? ' - ' . $child->getLibelle() : ''
                        );
            }
        }

        return $childArrayWithPath['path'];
    }

    /**
     * retourne le nom service en fonction de l'id du service.
     *
     * @param int $idService l'id du service
     *
     * @return string le nom du service
     */
    public function getServiceById($idService, $org)
    {
        if (0 != $idService) {
            $service = $this->em
                ->getRepository(Service::class)
                ->findOneBy(['organisme' => $org]);
        } else {
            $service = $this->em
                ->getRepository(Organisme::class)
                ->findOneBy(['acronyme' => $org]);
        }

        return $service->getSigle();
    }

    /**
     * retourne un tableau ordonné de tous les parents du service $service.
     *
     * @param $serviceId
     * @param $org
     *
     * @return array(id, libelle)
     */
    public function getAllParents($serviceId, $org)
    {
        $listParent = [];
        $idParent = $this->getParent($serviceId, $org);
        $index = 0;

        while ($idParent) {
            $listParent[$index]['id'] = $idParent;

            $entityParent = $this->em
                ->getRepository(Service::class)
                ->find($idParent);

            if ($entityParent) {
                $listParent[$index]['libelle'] = $entityParent->getSigle();
            }

            ++$index;
            if ($serviceId === $idParent) {
                break;
            }
            $idParent = $this->getParent($idParent, $org);
        }

        $listParent[$index]['id'] = 0;
        $listParent[$index]['libelle'] = '';

        if (count($listParent) > 0) {
            return $listParent;
        } else {
            return [];
        }
    }

    /**
     * @param $marche
     */
    public function displayMontant(ContratTitulaire $contrat, $marche)
    {
        $marche->setMontant($this->getMontantMarche($contrat->getIdContratTitulaire()));
    }

    private function getEntitesEligibles($contrat)
    {
        $contratTransverse = new ContratTransverse();

        $entites = [];
        //Ajouter Entite Eligible aux contrats dans le WS format etendu
        $entiteEligible = $this->em->getRepository(InvitationConsultationTransverse::class)
            ->findIfContratExists($contrat->getIdContratTitulaire());

        if (!empty($entiteEligible)) {
            $contratTransverse->setValue(true);
            foreach ($entiteEligible as $key => $value) {
                $entite = new EntiteEligible();
                $entite->setId($value['id']);
                $entite->setAcronyme($value['acronyme']);
                $entite->setDenomination($value['denominationOrg']);
                $entites[] = $entite;
            }

            $contratTransverse->setEntiteEligible($entites);
        } else {
            $contratTransverse->setValue(false);
        }

        return $contratTransverse;
    }

    /**
     * @param $idContratTitulaire
     *
     * @return int
     */
    public function getMontantMarche($idContratTitulaire)
    {
        $montant = 0;
        try {
            $contrat = $this->em->getRepository(ContratTitulaire::class)
                ->findOneBy(['idContratTitulaire' => $idContratTitulaire]);

            //check on type Contrat
            $typeContrat = $this->em->getRepository(TypeContrat::class)->findOneBy(
                ['idTypeContrat' => $contrat->getIdTypeContrat()]
            );

            //case1
            if ($typeContrat->getAvecChapeau() && $typeContrat->getAvecMontantMax()) {
                //recherche le contrat chapeau lié à ce contrat
                $contratChapeau = $this->em->getRepository(ContratTitulaire::class)->findOneBy(
                    ['idContratTitulaire' => $contrat->getChapeau()]
                );

                if (!empty($contratChapeau)) {
                    $montant = $contratChapeau->getMontantMaxEstime();
                }
            }

            //case 2
            if (!$typeContrat->getAvecChapeau() && $typeContrat->getAvecMontantMax()) {
                $montant = $contrat->getMontantMaxEstime();
            }

            //case 3 & 4
            if (
                ($typeContrat->getAvecChapeau() && !$typeContrat->getAvecMontantMax()) ||
                (!$typeContrat->getAvecChapeau() && !$typeContrat->getAvecMontantMax())
            ) {
                $montant = $contrat->getMontant();
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $montant;
    }

    public function getTechniqueAchatLibelle(Consultation|CommonConsultation $consultation): ?string
    {
        if (!(bool) $consultation->getTypeMarche()) {
            return null;
        }

        $typeContrat = $this->em->getRepository(TypeContrat::class)->find($consultation->getTypeMarche());
        if ($typeContrat && $typeContrat->getTechniqueAchat()) {
            return $consultation->getAutreTechniqueAchat() ?: $typeContrat->getTechniqueAchat()->getLibelle();
        }

        return null;
    }

    public function getTechniqueAchatLibelleByTypeContratId(int $typeContratId): ?string
    {
        $typeContrat = $this->em->getRepository(TypeContrat::class)->find($typeContratId);
        if ($typeContrat && $typeContrat->getTechniqueAchat()) {
            return $typeContrat->getTechniqueAchat()->getLibelle();
        }

        return null;
    }

    private function getClauses(ContratTitulaire $contratTitulaire): void
    {
        $this->clausesSociales = null;
        $this->clausesEnvironnementales = null;
        foreach ($contratTitulaire->getClausesN1() as $clause) {
            if ($clause->getReferentielClauseN1()->getSlug() === ReferentielClausesN1::CLAUSES_SOCIALES) {
                $this->clausesSociales = $clause;
            } elseif (
                $clause->getReferentielClauseN1()->getSlug() === ReferentielClausesN1::CLAUSES_ENVIRONNEMENTALES
            ) {
                $this->clausesEnvironnementales = $clause;
            }
        }
    }

    /**
     * Permet de verifier si un contrat est notifie et l'EJ commande
     *
     * @param $contrat : objet contrat
     * @return bool
     */
    public function isContratNotifieEtEjCommande($contrat)
    {
        try {
            return strstr($contrat->getStatutEJ(), $this->container->getParameter('CHORUS_STATUT_EJ_COMMANDE'))
                && $contrat->getStatutContrat() == $this->container->getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
        } catch (Exception $e) {
            $this->logger->error("Erreur : " . $e->getMessage() . " \n\nTrace : " . $e->getTraceAsString() . " \n\nMethode = CommonTContratTitulaire::isContratNotifieEtEjCommande");
        }
    }

    /**
     * Precise si le contrat est accord cadre/SAD
     *
     * @return bool : true si AC/SAD false sinon
     */
    public function isContratAcSad($contrat)
    {
        try {
            $typeContrat = $this->em->getRepository(TypeContrat::class)->find($contrat->getIdTypeContrat());
            if ($typeContrat) {
                return $typeContrat->getModeEchangeChorus() == $this->container->getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER');
            }
        } catch (\Exception $e) {
            $this->logger->error("Erreur lors de la determination du type de contrat : " . $e->getMessage());
        }
    }
    /**
     * Precise si le contrat est accord cadre/SAD
     *
     * @return bool : true si AC/SAD false sinon
     */
    public function isContratAcSadFromType($typeContrat)
    {
        try {
            $typeContrat = $this->getTypeContratFromTypeExec($typeContrat);
            if ($typeContrat) {
                return $typeContrat->getModeEchangeChorus() == $this->container->getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER');
            }
        } catch (\Exception $e) {
            $this->logger->error("Erreur lors de la determination du type de contrat : " . $e->getMessage());
        }
    }

    public function getTypeContratFromTypeExec($typeContrat)
    {
        try {
            $typeContrat = $this->em->getRepository(TypeContrat::class)->findOneBy([
                'libelleTypeContrat' => 'TYPE_CONTRAT_' . $typeContrat
            ]);
           return $typeContrat;
        } catch (\Exception $e) {
            $this->logger->error("Erreur lors de la determination du type de contrat : " . $e->getMessage() . ' ' . $e->getTraceAsString());
        }
    }
    public function normalizeForExec(mixed $contrats, DenormalizerInterface $normalizer, Marches $marches): void
    {
        foreach ($contrats as $contrat) {
            $contrat['id'] = $contrat['idMarche'];
            $contrat['lieuExecution'] = $contrat['lieuxExecutions'][0];
            $contrat['codeCPV'] = $contrat['cpv']['codePrincipal'];
            $contrat['dureeMois'] = $contrat['dureeMaximaleMarche'];
            if (!$contrat['dateNotification']) {
                $contrat['dateNotification'] = (new \DateTime())->format('Y-m-d');
            }
            $contrat['datePublicationDonnees'] = $contrat['dateNotification'];

            $contrat['donneescomplementaires']['procedurePassation'] = $contrat['procedurePassation'];
            $contrat['donneescomplementaires']['naturePassation'] = $contrat['naturePassation'];
            $contrat['donneescomplementaires']['accordCadreAvecMarcheSubsequent'] = $contrat['accordCadreAvecMarcheSubsequent'];
            $contrat['donneescomplementaires']['codesCPVSec'] = $contrat['cpv']['codeSecondaire1'];
            $contrat['donneescomplementaires']['nbTotalPropositionsDemat'] = $contrat['nbTotalPropositionsRecues'];
            $contrat['donneescomplementaires']['nbTotalPropositionsRecu'] = $contrat['nbTotalPropositionsRecues'];
            $contrat['donneescomplementaires']['consultation']['id'] = $contrat['consultation']['id'];
            $contrat['donneescomplementaires']['consultation']['numero'] = $contrat['consultation']['numero'];
            $contrat['donneescomplementaires']['consultation']['codeExterne'] = $contrat['consultation']['codeExterne'];
            $contrat['donneescomplementaires']['lot']['codeExterne'] = $contrat['lot']['intituleLot'];
            $contrat['donneescomplementaires']['lot']['numeroLot'] = $contrat['lot']['numeroLot'];
            $contrat['donneescomplementaires']['NumeroEJ'] = $contrat['numEj'];
            $contrat['donneescomplementaires']['ccagReference'] = $contrat['ccagApplicable'];
            $numeroContrat = new Marche\NumerosContratType();
            $numeroContrat->setIdTechnique($contrat['id'] ?? '')
                ->setNumeroCourt($contrat['numero'] ?? '')
                ->setReferenceLibre($contrat['referenceLibre'] ?? '')
                ->setNumeroLong($contrat['numeroLong'] ?? '');
            $contrat['donneescomplementaires']['numerosContrat'] = $numeroContrat;
            $contratChapeauxMulti = new Marche\ContratChapeauMultiAttributairesType();
            $contratsChapeaux = new Marche\ContratsChapeauxType();
            $contratChapeauxSadAc = new Marche\ContratChapeauAcSadType();
            $contratChapeauxSadAc->setIdTechniqueContratChapeauAcSad($contrat['contratChapeauAcSad']['id'] ?? '')
                ->setIdContratChapeauAcSad($contrat['contratChapeauAcSad']['numIdUniqueMarchePublic'] ?? '')
                ->setReferenceLibreContratChapeauAcSad($contrat['contratChapeauAcSad']['referenceLibre'] ?? '')
                ->setNumeroCourtContratChapeauAcSad($contrat['contratChapeauAcSad']['numeroCourt'] ?? '')
                ->setNumeroLongContratChapeauAcSad($contrat['contratChapeauAcSad']['numeroLong'] ?? '');

            $contratChapeauxMulti->setIdTechniqueContratChapeauMultiAttributaires($contrat['contratChapeauMultiAttributaires']['id'] ?? '')
                ->setReferenceLibreChapeauMultiAttributaires($contrat['contratChapeauMultiAttributaires']['referenceLibre'] ?? '')
                ->setNumeroCourtContratChapeauMultiAttributaires($contrat['contratChapeauMultiAttributaires']['numeroCourt'] ?? '');

            $contratsChapeaux->setContratChapeauMultiAttributaires($contratChapeauxMulti);
            $contratsChapeaux->setContratChapeauAcSad($contratChapeauxSadAc);
            $contrat['donneescomplementaires']['contratsChapeaux'] = $contratsChapeaux;

            $marche = $normalizer->denormalize($contrat, Marche::class);
            $titulaires = $normalizer->denormalize($contrat['titulaires'], TitulaireAType::class . '[]');
            $marche->setTitulaires($titulaires);
            $marches->addMarche($marche);
        }
    }
}
