<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\ModificationContrat;

use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\ModificationContrat;
use App\Service\AtexoEntreprise;
use App\Service\AtexoEtablissement;
use App\Traits\ApiTrait;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ModificationContratService.
 */
class ModificationContratService
{
    use ApiTrait;

    /**
     * ModificationContratService constructor.
     *
     * @param EntityManagerInterface $em                 Entity manager
     * @param ValidatorInterface     $validator          Validator
     * @param AtexoEntreprise        $atexoEntreprise    Service Entreprise
     * @param AtexoEtablissement     $atexoEtablissement Service Etablissement
     * @param TranslatorInterface    $translator         Translator
     * @param Authorization          $authorization      Service Authorization
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ValidatorInterface $validator,
        private readonly AtexoEntreprise $atexoEntreprise,
        private readonly AtexoEtablissement $atexoEtablissement,
        private readonly TranslatorInterface $translator,
        private readonly Authorization $authorization,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * Creation de l'entité ModificationContrat depuis le flux xml.
     *
     * @param $nodeFlux Flux xml représentant une modification de contrat
     *
     * @return ModificationContrat Entité Modification de contrat
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function createEntity($nodeFlux)
    {
        $siret = null;
        $typeIdentifiant = null;
        $denominationSociale = null;
        $agent = $this->em->getRepository(Agent::class)->find($nodeFlux['idAgent']);
        $contrat = $this->em->getRepository(ContratTitulaire::class)->find($nodeFlux['idContrat']);
        if (empty($contrat)) {
            throw new Exception('Contrat inexistant : ' . $nodeFlux['idContrat']);
        }
        $modifications = $this->em->getRepository(ModificationContrat::class)->findByIdContratTitulaire(
            $nodeFlux['idContrat']
        );
        $etablissement = null;
        $entreprise = null;
        $nodeTitulaire = $this->getNodeTitulaire($nodeFlux);
        if (!empty($nodeTitulaire)) {
            $typeIdentifiant = $this->getTypeIdentifiant($nodeTitulaire);
            $siret = $this->getSiret($nodeTitulaire);
            $denominationSociale = $this->getDenominationSociale($nodeTitulaire);
            $entreprise = $this->getEntreprise($siret, $typeIdentifiant);
            if (!empty($entreprise)) {
                $etablissement = $this->getEtablissement($siret, $entreprise);
            }
        }

        return $this->insertModificationContrat(
            $nodeFlux,
            $modifications,
            $agent,
            $contrat,
            $siret,
            $typeIdentifiant,
            $denominationSociale,
            $this->parameterBag,
            $denominationSociale,
            $entreprise
        );
    }

    /**
     * Extraction du noeud titulaire.
     *
     * @param $nodeFlux Flux xml représentant une modification de contrat
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function getNodeTitulaire($nodeFlux)
    {
        if (isset($nodeFlux['titulaires'])) {
            if (
                isset($nodeFlux['titulaires']['titulaire'])
                && (
                    isset($nodeFlux['titulaires']['titulaire']['id'])
                    && isset($nodeFlux['titulaires']['titulaire']['typeIdentifiant'])
                    && isset($nodeFlux['titulaires']['titulaire']['denominationSociale']))
            ) {
                return $nodeFlux['titulaires']['titulaire'];
            }
        }

        return null;
    }

    /**
     * Extraction du typeIdentifiant (Siret...).
     *
     * @param array $nodeTitulaire Noeud Titulaire
     *
     * @return mixed
     */
    public function getTypeIdentifiant(array $nodeTitulaire)
    {
        return $nodeTitulaire['typeIdentifiant'];
    }

    /**
     * Extraction du siret.
     *
     * @param array $nodeTitulaire Noeud Titulaire
     *
     * @return mixed
     */
    public function getSiret(array $nodeTitulaire)
    {
        return $nodeTitulaire['id'];
    }

    /**
     * Extraction de la denomination sociale.
     *
     * @param array $nodeTitulaire Noeud Titulaire
     *
     * @return mixed
     */
    public function getDenominationSociale(array $nodeTitulaire)
    {
        return $nodeTitulaire['denominationSociale'];
    }

    /**
     * Récupération de l'objet entreprise.
     *
     * @param string $siret           Siret
     * @param string $typeIdentifiant Conditionne la recherche par siret ou siren étranger
     *
     * @return Atexo_Entreprise_EtablissementVo|object|null
     */
    public function getEntreprise(string $siret, string $typeIdentifiant)
    {
        $siren = null;
        if ('SIRET' == $typeIdentifiant) {
            $this->validateSiret($siret);
            $siren = substr($siret, 0, 9);
            $criterias = ['siren' => $siren];
        } else {
            $criterias = ['sirenetranger' => $siret];
        }

        $entreprise = $this->em->getRepository(Entreprise::class)->findOneBy($criterias);
        if (empty($entreprise) && !empty($siren)) {
            $entreprise = $this->atexoEntreprise->synchroEntrepriseAvecApiGouvEntreprise($siren, $entreprise);
        }

        return $entreprise;
    }

    /**
     * Validation du Siret.
     *
     * @param string $siret Siret
     *
     * @return void
     */
    public function validateSiret(string $siret)
    {
        $siretConstraint = new Assert\Luhn();
        $siretConstraint->message = $this->translator->trans('SIRET_INVALIDE');
        $errors = $this->validator->validate(
            $siret,
            $siretConstraint
        );
        $this->errors($errors);
    }

    /**
     * Formatte le message d'erreur.
     *
     * @param $errors liste d'erreurs
     *
     * @return void
     */
    private function errors($errors)
    {
        if ((is_countable($errors) ? count($errors) : 0) > 0) {
            $msg = "L'erreur suivante est survenue ";
            if ((is_countable($errors) ? count($errors) : 0) > 1) {
                $msg = 'Les erreurs suivantes sont survenues ';
            }
            $msg .= "lors de l'insertion de la modification de contrat: ";
            $count = 0;
            foreach ($errors as $error) {
                $msg .= $error->getMessage();
                ++$count;
                if ($count < (is_countable($errors) ? count($errors) : 0)) {
                    $msg .= ', ';
                } else {
                    $msg .= '!';
                }
            }
            throw new ValidatorException($msg);
        }
    }

    /**
     * Recupère l'établissement.
     *
     * @param string     $siret      Siret
     * @param Entreprise $entreprise Objet représentant l'entreprise
     *
     * @return Etablissement|object|null
     */
    public function getEtablissement(string $siret, Entreprise $entreprise)
    {
        $criterias = [
            'idEntreprise' => $entreprise->getId(),
            'codeEtablissement' => substr($siret, 9),
        ];
        $etablissement = $this->em->getRepository(Etablissement::class)->findOneBy($criterias);
        if (empty($etablissement)) {
            $etablissement = $this->atexoEtablissement->synchroEtablissementAvecApiGouvEntreprise($siret);
        }

        return $etablissement;
    }

    /**
     * Création d'une modification de contrat.
     *
     * @param $nodeFlux            xml représentant une modification de contrat
     * @param array $modifications liste des modifications du contrat
     * @param Agent $agent Entité
     *                                          Agent
     * @param ContratTitulaire $contrat Entité contrat
     * @param Entreprise|null $entreprise Entité entreprise
     * @param Etablissement|null $etablissement Entité établissement
     * @param $siret               Siret
     * @param $typeIdentifiant     Conditionne la recherche par siret ou siren
     *                             étranger
     * @param $denominationSociale Dénomination Sociale
     *
     * @return ModificationContrat
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Atexo_Config_Exception
     */
    public function insertModificationContrat(
        $nodeFlux,
        array $modifications,
        Agent $agent,
        ContratTitulaire $contrat,
        $siret,
        $typeIdentifiant,
        $denominationSociale,
        ParameterBagInterface $parameterBag,
        Entreprise $entreprise = null,
        Etablissement $etablissement = null
    ) {
        $modificationContrat = new ModificationContrat();
        $numeroOrdre = count($modifications) + 1;
        $modificationContrat->setNumOrdre($numeroOrdre);
        $modificationContrat->setDateCreation(new DateTime());
        $modificationContrat->setDateModification(new DateTime());
        $modificationContrat->setIdAgent($agent);
        $modificationContrat->setIdEtablissement($etablissement);
        $modificationContrat->setIdContratTitulaire($contrat);
        $modificationContrat->setObjetModification($nodeFlux['objetModification']);
        $modificationContrat->setIdContratTitulaire($contrat);
        $dateSignature = Datetime::createFromFormat(
            ('Y-m-d'),
            substr($nodeFlux['dateSignatureModification'], 0, 10)
        );
        $modificationContrat->setDateSignature($dateSignature);
        if (isset($nodeFlux['montant'])) {
            $modificationContrat->setMontant($nodeFlux['montant']);
        }
        if (isset($nodeFlux['dureeMois'])) {
            $modificationContrat->setDureeMarche($nodeFlux['dureeMois']);
        }
        $this->validate($modificationContrat);
        $numero = substr($contrat->getNumIdUniqueMarchePublic(), 0, -2);
        $numero .= str_pad($numeroOrdre, 2, '0', STR_PAD_LEFT);
        $contrat->setNumIdUniqueMarchePublic($numero);
        if (!empty($entreprise)) {
            $entreprise->setNom($denominationSociale);
            if ('SIRET' != $typeIdentifiant) {
                $entreprise->setSirenetranger($siret);
            }
            $this->em->persist($entreprise);
            $contrat->setIdTitulaire($entreprise->getId());
        }
        if (!empty($etablissement)) {
            $contrat->setIdTitulaireEtab($etablissement->getIdEtablissement());
        }
        $this->em->persist($contrat);
        $this->em->persist($modificationContrat);
        $this->em->flush();

        $validSnStatus = [
            $parameterBag->get('STATUT_EN_ATTENTE_SN'),
            $parameterBag->get('STATUT_PUBLIE_SN'),
            $parameterBag->get('STATUT_NON_PUBLIE_SN')
        ];
        $isEnabled = $this
            ->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        return $modificationContrat;
    }

    /**
     * Validation de la modification de contrat.
     *
     * @param ModificationContrat $modificationContrat Entité modification
     *
     * @return void
     */
    private function validate(ModificationContrat $modificationContrat)
    {
        $errors = $this->validator->validate($modificationContrat);
        $this->errors($errors);
    }

    /**
     * Validation des champs obligatoires.
     *
     * @param array $arrayInfo tableau à valider
     *
     * @return array
     */
    public function validateInfo(array $arrayInfo, $formUpdate = false)
    {
        $arrayErreur = [];
        $mandatoryFields = ['objetModification', 'idContrat', 'idAgent', 'dateSignatureModification'];
        foreach ($mandatoryFields as $mandatoryField) {
            if (!isset($arrayInfo[$mandatoryField]) || empty($arrayInfo[$mandatoryField])) {
                $arrayErreur[] = sprintf('Le champ "%s" est manquant!', $mandatoryField);
            }
        }

        return $arrayErreur;
    }
}
