<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\ModificationContrat;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Exception\ApiProblemForbiddenException;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class Authorisation.
 */
class Authorization
{
    /**
     * Authorization agent constructor.
     *
     * @param EntityManager      $em
     * @param ContainerInterface $container
     */
    public function __construct(private readonly EntityManagerInterface $em, private readonly AtexoConfiguration $config)
    {
    }

    /**
     * @return bool
     */
    public function isAgentAuthorized(Agent $agent)
    {
        $auth = false;
        if (!empty($agent)) {
            $habilitationAgent = $this->em
                ->getRepository(HabilitationAgent::class)
                ->findOneBy(['agent' => $agent->getId()]);
            if (true === (bool) $habilitationAgent->getExecVoirContrats()) {
                $auth = true;
            }
        }
        if (false === $auth) {
            throw new ApiProblemForbiddenException();
        }

        return $auth;
    }
}
