<?php

namespace App\Service;

use App\Service\ApiGateway\ApiEntreprise;
use Exception;
use DateTime;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\MessageAccueil;
use App\Exception\ApiProblemAlreadyExistException;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemException;
use App\Service\Entreprise\EntrepriseAPIService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Classe contenant des fonctions utiles.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 0
 *
 * @since   0
 */
class AtexoEntreprise
{
    /**
     * @var
     */
    private ?ContainerInterface $container = null;
    protected $config;
    protected $util;

    public final const ADRESSE = 'adresse';

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getUtil(): AtexoUtil
    {
        return $this->util;
    }

    /**
     * @param mixed $util
     */
    public function setUtil($util)
    {
        $this->util = $util;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Constructeur de la classe.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     */
    public function __construct(
        ContainerInterface $container,
        protected EntityManagerInterface $em,
        AtexoConfiguration $config,
        AtexoUtil $util,
        private readonly EntrepriseAPIService $entrepriseAPIService,
        private readonly AtexoEtablissement $etablissementService,
        private readonly ParameterBagInterface $parameterBag,
        private readonly TranslatorInterface $translator
    ) {
        $this->setContainer($container);
        $this->setConfig($config);
        $this->setUtil($util);
    }

    public function createFromInscription(array $entrepriseBody): array
    {
        $establishments = [];

        if (empty($entrepriseBody['entreprise']['siren'])) { // Entreprise étrangère
            $entrepriseBody['entreprise']['capitalSocial'] = '';
            $entreprise = $this->createEntity($entrepriseBody['entreprise']);
            foreach ($entrepriseBody['etablissements'] as $key => $etablissement) {
                $etablissement['idEntreprise'] = $entreprise->getId();
                $etablissement['estSiege'] = isset($etablissement['estSiege']) && $etablissement['estSiege'] === true;
                $etablissement['codeEtablissement'] = str_pad($key + 1, 5, '0', STR_PAD_LEFT);
                $establishment = $this->etablissementService->createEntity($etablissement);
                $establishments[] = $establishment;
            }
        } else { // Entreprise française
            $entreprise = $this->em->getRepository(Entreprise::class)
                ->getEntrepriseBySiren($entrepriseBody['entreprise']['siren']);

            if (!empty($entreprise)) {
                $params = ['idEntreprise' => $entreprise->getId()];
                $establishments = $this->em->getRepository(Etablissement::class)->findBy($params);

                if (!empty($entrepriseBody['entreprise']['siret'])) {
                    foreach ($establishments as $establishment) {
                        if ($establishment['codeEtablissement'] == $entrepriseBody['entreprise']['siret']) {
                            return ['entreprise' => $entreprise, 'establishments' => $establishments];
                        }
                    }
                    $establishmentFromApi = $this->entrepriseAPIService->callEstablishment(
                        $entrepriseBody['entreprise']['siren'] . $entrepriseBody['entreprise']['siret']
                    );
                    if (!empty($establishmentFromApi)) {
                        $establishment = $this->etablissementService->loadFromApiGouvEntreprise(
                            $establishmentFromApi['data']
                        );
                        $establishment->setIdEntreprise($entreprise->getId());
                        $establishment = $this->etablissementService->createOrUpdateEntity($establishment, 'create');
                        $establishments[] = $establishment;
                    }
                }
            } else {
                $entrepriseFromAPI = $this->entrepriseAPIService->callEntreprise(
                    $entrepriseBody['entreprise']['siren']
                );

                $entreprise = $this->loadFromApiGouvEntreprise($entrepriseFromAPI['data']);
                $entreprise = $this->createOrUpdateEntity($entreprise, 'create');
                $establishment = $this->etablissementService->loadFromApiGouvEntreprise($entrepriseFromAPI['data']);
                $establishment->setIdEntreprise($entreprise->getId());
                $establishment = $this->etablissementService->createOrUpdateEntity($establishment, 'create');
                $establishments[] = $establishment;
            }
        }

        return ['entreprise' => $entreprise, 'establishments' => $establishments];
    }

    /**
     * Permet de remplir l'objet entreprise avec les donnees de l'API entreprise.
     *
     * @param array $data : données API entreprise
     *
     * @return Entreprise : objet contenant les informations
     */
    public function loadFromApiGouvEntreprise(array $data, Entreprise $entreprise = null): Entreprise
    {
        if (empty($entreprise)) {
            $entreprise = new Entreprise();
        }
        $entreprise->setSiren($data['siren']);

        $activitePrincipale = $data['activite_principale'] ?? null;
        $entreprise->setCodeape($activitePrincipale['code'] ?? '');
        $capitalSocial = $data['capital']['montant'] ?? 'N.C.';
        $entreprise->setCapitalSocial($capitalSocial);
        $entreprise->setFormejuridique($data['forme_juridique']['libelle'] ?? 'Autre');
        $entreprise->setFormejuridiqueCode($data['forme_juridique']['code'] ?? '');
        $entreprise->setNomCommercial($data['nom_commercial']);

        $raisonSociale = ' ';
        if ($data['type'] == ApiEntreprise::TYPE_PERSONNE_PHYSIQUE) {
            $personnePhysiqueAttributs = $data['personne_physique_attributs'];
            $raisonSociale = $personnePhysiqueAttributs['prenom_1'] . ' ' . $personnePhysiqueAttributs['nom_naissance'];
        } elseif ($data['type'] == ApiEntreprise::TYPE_PERSONNE_MORALE) {
            $raisonSociale = $data['personne_morale_attributs']['raison_sociale'] ?? null;
        }

        $entreprise->setNom($raisonSociale);
        $entreprise->setRaisonSociale($raisonSociale);
        $entreprise->setMandataires($data['mandataires_sociaux']);
        $entreprise->setNicSiege(
            ((strlen((string) $data['siret_siege_social']) > 9) ? substr($data['siret_siege_social'], 9, 5) : '')
        );
        $entreprise->setSiretSiegeSocial(($data['siret_siege_social']));

        if ($trancheEffectifSalarie = $data['tranche_effectif_salarie']['code'] ?? '') {
            $criteresRef = [
                'libelle2'      => $trancheEffectifSalarie,
                'idReferentiel' => $this->getParameter('ID_REFERENTIEL_TRANCHE_EFFECTIF')
            ];
            $idCodeEffectif = $this->get(Referentiel::class)->getIdValeurReferentiel($criteresRef);
            if ($idCodeEffectif) {
                $entreprise->setIdCodeEffectif($idCodeEffectif);
            }
        }
        if (!empty($data['categorie_entreprise'])) {
            $entreprise->setCategorieEntreprise($data['categorie_entreprise']);
        }
        $entreprise->setPaysadresse($this->getParameter('DENOMINATION1_GEON2_FRANCE'));
        $entreprise->setAcronymePays($this->getParameter('DENOMINATION2_GEON2_FRANCE'));
        $entreprise->setPaysenregistrement($this->getParameter('DENOMINATION2_GEON2_FRANCE'));
        $entreprise->setEtatAdministratif($data['etat_administratif']);
        $entreprise->setSaisieManuelle(0);
        $entreprise->setUpdateDateApiGouv(new DateTime());

        return $entreprise;
    }

    /**
     * Permet de recuperer une entreprise selon le siren.
     *
     * @param int       $siren             le siren d'enetreprise
     * @param bool      $withEtablissement si on veut entreprise avec etablissement
     * @param connexion $connexion         l'objet de la connexion
     * @param int       $idObjet           l'id de l'objet pour le stoquer dans l'historique
     *
     * @return mix Atexo_Entreprise_EntrepriseVo l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererEntrepriseApiGouvEntreprise($siren, $withEtablissement = false, $idObjet = null)
    {
        $token = $this->parameterBag->get('API_ENTREPRISE_TOKEN');

        $atexoInterface = $this->get(ApiGouvEntrepriseSynchro::class);
        $paramsWs = [
            'token' => $token,
            'api_gouv_url_entreprise' => $this->getContainer()->getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
            'api_gouv_url_etablissement' => $this->getContainer()->getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $atexoInterface->setParameters($paramsWs);

        $resultat = $atexoInterface->getEntrepriseBySiren($siren, $withEtablissement);
        $loggerWs = $this->get('mpe.logger');

        if (is_array($resultat) && $atexoInterface->isResponseSuccess($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'entreprise");
            $historiqueSynchro = $this->get(HistoriqueSynchro::class);
            $historiqueSynchro->enregistrerHistroique('ENTREPRISE', $siren, $jeton, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'entreprise");
            $entreprise = $resultat[1];

            return $entreprise;
        }

        return false;
    }

    /**
     * Permet de retourner le code ape (NAF) de l'etablissement siege.
     *
     * @param array $etablissements liste des etablissements
     * @retrun String code ape
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getCodeApeEtablissementSiege($etablissements)
    {
        if (is_array($etablissements)) {
            foreach ($etablissements as $etablissement) {
                if ($etablissement instanceof Etablissement) {
                    if ($etablissement->getEstSiege()) {
                        return $etablissement->getCodeape();
                    }
                }
            }
        }

        return null;
    }

    /**
     * Permet de synchroniser une entreprise identifiee par son siren avec Api Gouv Entreprise.
     *
     * @param string     $siren      : siren de l'entreprise sur 9 caracteres
     * @param Entreprise $entreprise : objet entreprise
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function synchroEntrepriseAvecApiGouvEntreprise($siren, $entreprise = null)
    {
        $loggerWs = $this->get('mpe.logger');
        $loggerWs->info("Debut de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");
        try {
            $idEntreprise = null;
            if ($entreprise instanceof Entreprise && !empty($entreprise->getId())) {
                $idEntreprise = $entreprise->getId();
            } else {
                $entreprise = new Entreprise();
                $loggerWs->info("L'entreprise dont le siren = $siren n'existe pas en base de donnees");
            }

            $entrepriseApiGouvEntreprise = self::recupererEntrepriseApiGouvEntreprise($siren, true, $idEntreprise);
            if ($entrepriseApiGouvEntreprise instanceof Entreprise) {
                $loggerWs->info("L'objet existe dans la BD Api Gouv Entreprise");
                $listeEtablissements = $entrepriseApiGouvEntreprise->getEtablissements();
                if (is_array($listeEtablissements) && !empty($listeEtablissements)) {
                    //Enregistrement du code APE dans l'entreprise $entrepriseApiGouvEntreprise
                    $loggerWs->info("La récuperation du codeApe a partir de l'etablisssement siége");
                    $codeApe = self::getCodeApeEtablissementSiege($listeEtablissements);
                    $loggerWs->info("Le codeApe = '" . $codeApe . "'");
                    $entrepriseApiGouvEntreprise->setCodeape($codeApe);
                }

                $loggerWs->info("Debut de la suppression des anciens dirigeants de l'entreprise ");
                $responsableEngagement = $this->get(AtexoResponsableEngagement::class);
                $responsableEngagement->deleteLeaders($entreprise->getId());
                $loggerWs->info("Fin de la suppression des anciens dirigeants de l'entreprise ");
                $loggerWs->info("remplir l'objet Entreprise par l'objet Api Gouv Entreprise");

                $entreprise = self::fillInfosEse($entrepriseApiGouvEntreprise, $entreprise);

                $entreprise->setSaisieManuelle('0');
                $entreprise->setPaysenregistrement(
                    $this->getUtil()->toHttpEncoding($this->getParameter('DENOMINATION2_GEON2_FRANCE'))
                );
                $entreprise->setPaysadresse($this->getParameter('DENOMINATION1_GEON2_FRANCE'));
                $entreprise->setAcronymePays($this->getParameter('DENOMINATION2_GEON2_FRANCE'));
                $entreprise->setUpdateDateApiGouv(new DateTime());
                $loggerWs->info("Debut de l'enregistrement de l'entreprise");
                $entreprise_repo = $this->em->getRepository(Entreprise::class);
                $entreprise_repo->updateEntreprise($entreprise);
                $loggerWs->info("Fin de l'enregistrement de l'entreprise");
                $loggerWs->info("L'entreprise synchronise avec Api Gouv Entreprise est mis a jour avec SUCCES sur MPE");

                //Enregistrer l'etablissement siege
                $loggerWs->info("Debut enregistrement de l'etablisssement siége");
                if ($entreprise instanceof Entreprise && !empty($entreprise->getId())) {
                    $atexoEtablissement = $this->get(AtexoEtablissement::class);
                    foreach ($listeEtablissements as $etablissementVo) {
                        if ($etablissementVo instanceof Etablissement) {
                            if ($etablissementVo->getEstSiege()) {
                                $etablissementVo->setIdEntreprise($entreprise->getId());
                                $etablissementVo->setEntreprise($entreprise);
                                $monetablissement = $atexoEtablissement->enregistrerEtablissementApiGouvEntreprise(
                                    $etablissementVo
                                );
                                $repositoryEtab = $this->em->getRepository(Etablissement::class);
                                $etabDb = $repositoryEtab->getEtablissementByCodeAndIdEntreprise(
                                    $etablissementVo->getCodeEtablissement(),
                                    $etablissementVo->getIdEntreprise()
                                );
                                if ($etabDb instanceof Etablissement) {
                                    $entreprise->setEtablissementSiege($etabDb);
                                }
                            }
                        }
                    }
                }
                $loggerWs->info("Fin enregistrement de l'etablisssement siége");
                $loggerWs->info("Fin de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");

                return $entreprise;
            } else {
                $loggerWs->error("une erreur est survenue lors de la synchronisation de l'entreprise avec Api Gouv Entreprise");
            }
        } catch (Exception $e) {
            $msg = "Erreur lors de la synchronisation avec Api Gouv Entreprise  : siren = $siren \n\nErreur: " . $e->getMessage();
            $msg .= "\n\nTrace: " . $e->getTraceAsString();
            $loggerWs->error($msg);
        }
        $loggerWs->info("Fin de la synchronisation de l'entreprise dont le siren = $siren avec Api Gouv Entreprise");
    }

    public function fillInfosEse($from, $to, $ecraserInfo = true)
    {
        if ($from instanceof Entreprise && $to instanceof Entreprise) {
            if (null !== $from->getSiren() || $ecraserInfo) {
                $to->setSiren($from->getSiren());
            }
            if (null !== $from->getNicSiege()) {
                $to->setNicSiege($from->getNicSiege());
            }
            if (null !== $from->getPaysenregistrement()) {
                $to->setPaysenregistrement($from->getPaysenregistrement());
            }
            if (null !== $from->getSirenetranger()) {
                $to->setSirenetranger($from->getSirenetranger());
            }
            if (null !== $from->getPaysadresse()) {
                $to->setPaysadresse($from->getPaysadresse());
            }
            if (null !== $from->getAcronymePays()) {
                $to->setAcronymePays($from->getAcronymePays());
            }
            if (null !== $from->getNom() || $ecraserInfo) {
                $to->setNom($from->getNom());
            }
            if (null !== $from->getAdresse()) {
                $to->setAdresse($from->getAdresse());
            }
            if (null !== $from->getAdresse2()) {
                $to->setAdresse2($from->getAdresse2());
            }
            if (null !== $from->getCodepostal()) {
                $to->setCodepostal($from->getCodepostal());
            }
            if (null !== $from->getVilleadresse()) {
                $to->setVilleadresse($from->getVilleadresse());
            }
            if (null !== $from->getTelephone()) {
                $to->setTelephone($from->getTelephone());
            }
            if (null !== $from->getFax()) {
                $to->setFax($from->getFax());
            }
            if (null !== $from->getEntrepriseEA()) {
                $to->setEntrepriseEA($from->getEntrepriseEA());
            }
            if (null !== $from->getEntrepriseSIAE()) {
                $to->setEntrepriseSIAE($from->getEntrepriseSIAE());
            }
            if (null !== $from->getCodeape() || $ecraserInfo) {
                $to->setCodeape($from->getCodeape());
            }
            if (null !== $from->getLibelleApe() || $ecraserInfo) {
                $to->setLibelleApe($from->getLibelleApe());
            }
            if (null !== $from->getFormejuridique() || $ecraserInfo) {
                $to->setFormejuridique($from->getFormejuridique());
            }

            if (null !== $from->getDateCreation()) {
                $to->setDateCreation($from->getDateCreation());
            }
            if (null !== $from->getDateModification()) {
                $to->setDateModification($from->getDateModification());
            }

            if (null !== $from->getRegion()) {
                $to->setRegion($from->getRegion());
            }
            if (null !== $from->getProvince()) {
                $to->setProvince($from->getProvince());
            }
            if (null !== $from->getTelephone2()) {
                $to->setTelephone2($from->getTelephone2());
            }
            if (null !== $from->getTelephone3()) {
                $to->setTelephone3($from->getTelephone3());
            }
            if (null !== $from->getCnss()) {
                $to->setCnss($from->getCnss());
            }
            if (null !== $from->getCapitalSocial() || $ecraserInfo) {
                $to->setCapitalSocial($from->getCapitalSocial());
            }
            if (null !== $from->getNumTax()) {
                $to->setNumTax($from->getNumTax());
            }
            if (null !== $from->getRcNum()) {
                $to->setRcNum($from->getRcNum());
            }
            if (null !== $from->getRcVille()) {
                $to->setRcVille($from->getRcVille());
            }
            if (null !== $from->getDomainesActivites()) {
                $to->setDomainesActivites($from->getDomainesActivites());
            }
            if (null !== $from->getSiteInternet()) {
                $to->setSiteInternet($from->getSiteInternet());
            }
            if (null !== $from->getDescriptionActivite()) {
                $to->setDescriptionActivite($from->getDescriptionActivite());
            }
            if (null !== $from->getActiviteDomaineDefense()) {
                $to->setActiviteDomaineDefense($from->getActiviteDomaineDefense());
            }
            if (null !== $from->getAdressesElectroniques()) {
                $to->setAdressesElectroniques($from->getAdressesElectroniques());
            }
            if (null !== $from->getMoyensTechnique()) {
                $to->setMoyensTechnique($from->getMoyensTechnique());
            }
            if (null !== $from->getMoyensHumains()) {
                $to->setMoyensHumains($from->getMoyensHumains());
            }
            if (null !== $from->getVisibleBourse()) {
                $to->setVisibleBourse($from->getVisibleBourse());
            }
            if (null !== $from->getTypeCollaboration()) {
                $to->setTypeCollaboration($from->getTypeCollaboration());
            }
            if (null !== $from->getCompteActif()) {
                $to->setCompteActif($from->getCompteActif());
            }
            if (null !== $from->getIfu()) {
                $to->setIfu($from->getIfu());
            }
            if (null !== $from->getSaisieManuelle()) {
                $to->setSaisieManuelle($from->getSaisieManuelle());
            }
            if (null !== $from->getSiretSiegeSocial()) {
                $to->setSiretSiegeSocial($from->getSiretSiegeSocial());
            }

            if (null !== $from->getCreatedFromDecision()) {
                $to->setCreatedFromDecision($from->getCreatedFromDecision());
            }

            if (null !== $from->getInscrits()) {
                foreach ($from->getInscrits() as $oneInscrit) {
                    $to->addInscrit($oneInscrit);
                }
            }
            if (null !== $from->getMandataires()) {
                $responsableEngagement = $this->get(AtexoResponsableEngagement::class);
                $responsableEngagement->setArrayCommonResponsableengagementInEntreprise($from->getMandataires(), $to);
            }

            if (null !== $from->getEtablissementSiege()) {
                $to->setEtablissementSiege($from->getEtablissementSiege());
            }

            if (null !== $from->getIdCodeEffectif()) {
                $to->setIdCodeEffectif($from->getIdCodeEffectif());
            }
            if (null !== $from->getCategorieEntreprise()) {
                $to->setCategorieEntreprise($from->getCategorieEntreprise());
            }

            return $to;
        }
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->parameterBag->get($name);
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    protected function get($id)
    {
        return $this->getContainer()->get($id);
    }

    /**
     * @param array $node
     *
     * @return Entreprise
     */
    public function createEntity(array $arrayInfoEntreprise)
    {
        $entreprise = $this->serializeEntreprise($arrayInfoEntreprise);

        return $this->createOrUpdateEntity($entreprise, 'create');
    }

    /**
     * @return Entreprise
     *
     * @throws Exception
     */
    public function updateEntity(array $arrayInfoEntreprise)
    {
        $entreprise = $this->serializeEntreprise($arrayInfoEntreprise);

        return $this->createOrUpdateEntity($entreprise, 'update');
    }

    /**
     * @return Entreprise|object|null
     */
    public function createOrUpdateEntity(Entreprise $node, $mode)
    {
        try {
            $message = null;
            $entreprise = null;
            if (!empty($node->getIdExterne())) {
                $entreprise = $this->em->getRepository(Entreprise::class)
                    ->findOneByIdExterne($node->getIdExterne());
            } elseif (!empty($node->getId())) {
                $entreprise = $this->em->getRepository(Entreprise::class)
                    ->find($node->getId());
            } else {
                if (!empty($node->getSiren())) {
                    $entreprise = $this->em->getRepository(Entreprise::class)
                        ->findOneBy(['siren' => $node->getSiren()]);
                    $message = 'une entreprise avec le siren ' . $node->getSiren() . ' existe déjà';
                } elseif (!empty($node->getSirenetranger())) {
                    $entreprise = $this->em->getRepository(Entreprise::class)
                        ->findOneBy(
                            [
                                'sirenetranger' => $node->getSirenetranger(),
                                'paysenregistrement' => $node->getPaysenregistrement(),
                            ]
                        );
                    $message = 'Une entreprise avec un identifiant national '
                        . $node->getSirenetranger()
                        . ' existe déjà pour le pays ' . $node->getPaysenregistrement();
                }
                if ($entreprise instanceof Entreprise) {
                    throw new ApiProblemAlreadyExistException($message);
                }
            }
            if (!$entreprise instanceof Entreprise) {
                $entreprise = new Entreprise();
            }

            if ($node->getSiren()) {
                $entreprise->setSiren($node->getSiren());
            }

            if ($node->getSirenetranger()) {
                $entreprise->setSirenetranger($node->getSirenetranger());
            }

            $entreprise->setTelephone($node->getTelephone());
            $entreprise->setFax($node->getFax());
            $entreprise->setSiteInternet($node->getSiteInternet());
            $entreprise->setEffectif($node->getEffectif());
            $entreprise->setEmail($node->getEmail());
            $entreprise->setIdExterne($node->getIdExterne());
            $entreprise->setVille($node->getVilleadresse());
            $entreprise->setCodepostal($node->getCodepostal());
            $entreprise->setPays($node->getPaysadresse());
            $entreprise->setAcronymePays($node->getAcronymePays());
            $entreprise->setAdresse($node->getAdresse());
            $entreprise->setFormejuridique($node->getFormejuridique());
            $entreprise->setCodeape($node->getCodeape());
            $entreprise->setLibelleApe($node->getLibelleApe());
            $entreprise->setCapitalSocial($node->getCapitalSocial());
            $entreprise->setRaisonSociale($node->getRaisonSociale());
            $entreprise->setNom($node->getRaisonSociale());
            $entreprise->setPaysenregistrement($node->getPaysenregistrement());
            $entreprise->setAcronymePays($node->getAcronymePays());
            $entreprise->setIdCodeEffectif($node->getIdCodeEffectif());
            $entreprise->setCategorieEntreprise($node->getCategorieEntreprise());
            $entreprise->setEtatAdministratif($node->getEtatAdministratif());
            $entreprise->setSaisieManuelle($node->getSaisieManuelle() ?? 1);
            if (!empty($node->getUpdateDateApiGouv())) {
                $entreprise->setUpdateDateApiGouv($node->getUpdateDateApiGouv()->format('Y:m:d H:i:s'));
            }

            if ('create' === $mode) {
                $dateTime = new DateTime();
                $date = $dateTime->format('Y:m:d H:i:s');
                $entreprise->setDateCreation($date);
            }
            $this->em->beginTransaction();
            $this->em->persist($entreprise);
            $this->em->flush();
            $this->em->commit();

            return $entreprise;
        } catch (ApiProblemException $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw $e;
        } catch (Exception $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    /**
     * @param $arrayInfoEntreprise
     *
     * @return Entreprise
     */
    public function serializeEntreprise($arrayInfoEntreprise)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $entreprise = $serializer->denormalize($arrayInfoEntreprise, Entreprise::class);

            if (
                array_key_exists('etablisssements', $arrayInfoEntreprise)
                && array_key_exists('etablissement', $arrayInfoEntreprise['etablisssements'])
            ) {
                $etablissements = $arrayInfoEntreprise['etablisssements']['etablissement'];

                foreach ($etablissements as $ets) {
                    $etablissement = $serializer->denormalize($ets, Etablissement::class);
                    $etablissement->setEstSiege($ets['siege']);
                    $etablissement->setIdEtablissement($ets['id']);
                    $entreprise->addEtablissement($etablissement);
                }
            }

            return $entreprise;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param bool $formUpdate
     *
     * @return array
     */
    public function validateInfo(array $arrayInfoEntreprise, $formUpdate = false)
    {
        $arrayErreur = [];
        $entreprise = $this->serializeEntreprise($arrayInfoEntreprise);
        if (
            !empty($entreprise->getSiren())
                && !  $this->getUtil()->isValidSiren($entreprise->getSiren())
        ) {
            $arrayErreur[] = 'Siren non valide';
        }

        if (empty($entreprise->getId())) {
            if (
                empty($entreprise->getSirenetranger())
                && empty($entreprise->getSiren())
            ) {
                $arrayErreur[] = 'Siren ou identifiant national manquant.';
            } elseif (
                !empty($entreprise->getSirenetranger())
                && empty($entreprise->getPaysenregistrement())
            ) {
                $arrayErreur[] = "Pour une entreprise étrangère, vous devez renseigner le champs 'paysenregistrement'.";
            }
        }

        if ($entreprise->getEmail() && !filter_var($entreprise->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $arrayErreur[] = 'Email non valide';
        }

        if ($formUpdate) {
            $this->entrepriseExist($entreprise, $arrayErreur);
        }

        return $arrayErreur;
    }

    /**
     * @param Entreprise $enttreprise
     * @param $arrayErreur
     */
    public function entrepriseExist(Entreprise $entreprise, &$arrayErreur)
    {
        $entrepriseRepo = $this->em->getRepository(Entreprise::class);
        $id = $entreprise->getId();

        if (empty($id)) {
            $arrayErreur[] = 'id vide';
        } elseif (!$entrepriseRepo->find($entreprise->getId())) {
            $arrayErreur[] = "Aucune entreprise avec l'id " . $id;
        }
    }

    /**
     * @param $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode(Entreprise $entreprise, Serializer $serializer, $mode)
    {
        $entrepriseNormalize = $serializer->normalize($entreprise, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($entrepriseNormalize, $mode);

        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', 'entreprise>', $content);
        }

        return $content;
    }

    public function sirenEtrangerAlreadyExists(string $sirenEtranger): bool
    {
        $entreprise = $this->em->getRepository(Entreprise::class)->findOneBy(['sirenetranger' => $sirenEtranger]);

        return !empty($entreprise);
    }

    public function controlEntrepriseCreationFromInscription(array $body): ?string
    {
        $errorMessage = null;
        if (empty($body['entreprise']) || !is_array($body['entreprise'])) {
            return 'Body entreprise is empty or not an array';
        } elseif (empty($body['etablissements']) || !is_array($body['etablissements'])) {
            return 'Body etablissements is empty or not an array';
        }

        if (empty($body['entreprise']['siren'])) {
            if (empty($body['entreprise']['raisonSociale']) || !is_string($body['entreprise']['raisonSociale'])) {
                $errorMessage = 'Body["entreprise"] raisonSociale is empty or not a string';
            } elseif (
                empty($body['entreprise']['formejuridique']) || !is_string($body['entreprise']['formejuridique'])
            ) {
                $errorMessage = 'Body["entreprise"] formejuridique is empty or not a string';
            } elseif (
                empty($body['entreprise']['paysenregistrement'])
                || !is_string($body['entreprise']['paysenregistrement'])
            ) {
                $errorMessage = 'Body["entreprise"] paysenregistrement is empty or not a string';
            } elseif (
                empty($body['entreprise']['categorieEntreprise'])
                || !is_string($body['entreprise']['categorieEntreprise'])
            ) {
                $errorMessage = 'Body["entreprise"] categorieEntreprise is empty or not a string';
            } elseif (
                empty($body['entreprise']['sirenEtranger'])
                || !is_string($body['entreprise']['sirenEtranger'])
            ) {
                $errorMessage = 'Body["entreprise"] sirenEtranger is empty or not a string';
            } elseif ($this->sirenEtrangerAlreadyExists($body['entreprise']['sirenEtranger'])) {
                $errorMessage = 'Entreprise already exists with this sirenEtranger';
            }

            if (empty($errorMessage)) {
                foreach ($body['etablissements'] as $key => $etablissement) {
                    if (empty($etablissement['adresse']) || !is_string($etablissement['adresse'])) {
                        $errorMessage = "Body[\"etablissements\"][{$key}] adresse is empty or not a string";
                        break;
                    } elseif (empty($etablissement['ville']) || !is_string($etablissement['ville'])) {
                        $errorMessage = "Body[\"etablissements\"][{$key}] ville is empty or not a string";
                        break;
                    } elseif (empty($etablissement['pays']) || !is_string($etablissement['pays'])) {
                        $errorMessage = "Body[\"etablissements\"][{$key}] pays is empty or not a string";
                        break;
                    }
                }
            }
        } elseif (!is_numeric($body['entreprise']['siren'])) {
            $errorMessage = 'Body["entreprise"] siren is not numeric';
        }

        return $errorMessage;
    }

    public function getHomeBasicMessages(): array
    {
        $rawLangMessages = [
            'TEXT_INFORMATION_SAE',
            'MSG_ENTREPRISE',
            'TEXT_REMETTRE_SOUS_FORME_ELECTRONIQUE',
            'TEXT_ABONNER_FLUX_RSS_PUBLICATION_AVIS',
            'TEXT_CONSULTER_INFORMATIONS_MARCHE_PUBLIC',
            'TEXT_DISPOSER_ESPACE_ECHANGES_DOCUMENTS',
            'TEXT_FONCTIONS_SUPPLEMENTAIRES_PLATEFORME_MARCHES_PUBLICS'
        ];

        $langMessages = [];
        foreach ($rawLangMessages as $rawLangMessage) {
            $langMessages[$rawLangMessage] = $this->translator->trans($rawLangMessage);
        }

        $configHomeMessage = $this->getParameter('CONFIG_PLATEFORME_POUR_MESSAGE_ACCUEIL');
        $homeMessage = $this->em->getRepository(MessageAccueil::class)
            ->findOneBy(['destinataire' => 'entreprise', 'config' => $configHomeMessage]);
        $homeMessage = !empty($homeMessage) ? $homeMessage->getContenu() : '';

        return ['langMessages' => $langMessages, 'homeMessage' => $homeMessage];
    }

    public function explodeDataAdress(array $data): array
    {
        if (
            !empty($data)
            && isset($data[self::ADRESSE])
            && is_countable($data[self::ADRESSE])
            && count($data[self::ADRESSE]) > 1
        ) {
            $adresses = $data[self::ADRESSE];

            $data[self::ADRESSE] = $adresses['rue'] ?? '';
            $data['pays'] = $adresses['pays'] ?? '';
            $data['ville'] = $adresses['ville'] ?? '';
            $data['codePostal'] = $adresses['codePostal'] ?? '';
            $data['acronymePays'] = $adresses['acronymePays'] ?? '';
        }

        return $data;
    }

    public function getEntrepriseBySiren(string $siren): ?Entreprise
    {
        $entreprise = $this->em->getRepository(Entreprise::class)
            ->getEntrepriseBySiren($siren);

        return $entreprise;
    }
}
