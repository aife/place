<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\App;

use Exception;
use App\Entity\Configuration\PlateformeVirtuelle;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class GraphicChart : gestion de la charte graphique.
 *
 * @author Sébastien Lepers <sebastien.lepers@atexo.com>
 */
class GraphicChart
{
    public final const DESIGN_COOKIE = 'design';
    public final const NO_DESIGN_COOKIE = 'no-design';

    /**
     * GraphicChart constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(private readonly RequestStack $requestStack, private readonly EntityManagerInterface $em, private readonly LoggerInterface $logger)
    {
    }

    /**
     * @return void
     *
     * @throws Exception
     */
    public function setDesignCookie()
    {
        $request = $this->requestStack->getCurrentRequest();

        if (
            $request->cookies->has(self::NO_DESIGN_COOKIE)
            || $request->cookies->has(self::DESIGN_COOKIE)
        ) {
            return;
        }

        $currentDomain = $request->getHost();

        $plateformeVirtuelleRepo = $this->em->getRepository(PlateformeVirtuelle::class);

        /** @var PlateformeVirtuelle $plateformeVirtuelle */
        $plateformeVirtuelle = $plateformeVirtuelleRepo->findOneBy(['domain' => $currentDomain]);

        // La gestion des cookies et des headers doit être compatible SF, Prado et la classe Atexo_Controller_Front
        // => pas possible d'utiliser les bonnes pratiques de SF... On utilise les vieilles méthodes...

        if (null === $plateformeVirtuelle || null === $plateformeVirtuelle->getCodeDesign()) {
            $this->setCookie(self::NO_DESIGN_COOKIE, 1);

            return;
        }

        $this->setCookie(self::DESIGN_COOKIE, $plateformeVirtuelle->getCodeDesign());
        $this->setCookie(self::NO_DESIGN_COOKIE);

        header('Status: 302 Moved Temporarily', false, 302);
        header('Location: ' . $request->getUri());

        if (getenv('UNIT_TESTING')) {
            throw new Exception('In unit tests, this exception is expected');
        } else {
            exit();
        }
    }

    public function setCookie(string $name, string $value = '', int $expires = 0, string $path = '/')
    {
        setcookie($name, $value, ['expires' => $expires, 'path' => $path, 'domain' => '', 'secure' => true, 'httponly' => true]);
    }
}
