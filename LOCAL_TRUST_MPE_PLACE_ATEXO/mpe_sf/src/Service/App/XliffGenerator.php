<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\App;

use Exception;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Symfony\Component\Config\Util\XmlUtils;
use Symfony\Component\Translation\Util\XliffUtils;

class XliffGenerator
{
    public function __construct(protected LoggerInterface $logger)
    {
    }

    protected function getBaseXml($language = 'fr')
    {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><xliff></xliff>');
        $language = strtoupper($language);

        $xml->addAttribute('version', '1.2');

        $fileNode = $xml->addChild('file');
        $fileNode->addAttribute('datatype', 'plaintext');
        $fileNode->addAttribute('original', 'file.ext');
        $fileNode->addAttribute('source-language', $language);
        $fileNode->addAttribute('target-language', "$language-$language");

        $fileNode->addChild('body');

        return $xml;
    }

    public function getXliff(array $data, $language = 'fr'): SimpleXMLElement
    {
        $xml = $this->getBaseXml($language);

        foreach ($data as $key => $value) {
            $transUnitChild = $this->addTransUnitNode($xml, $key);

            if (!$this->isValidXml($key, $value)) {
                // Si la traduction contient des caractères incompatibles avec le XML,
                // on loggue et on utilise la clé comme traduction pour ne pas bloquer les autres traductions.
                $this->logger->debug(
                    self::class.' - Traduction incompatible avec le XML : '
                    .$key.' = '.$value
                );
                $transUnitChild->addChild('target', $key);
            } else {
                $transUnitChild->addChild('target', $value);
            }
        }

        return $xml;
    }

    protected function addTransUnitNode($xml, $key)
    {
        $transUnitChild = $xml->file->body->addChild('trans-unit');
        $transUnitChild->addAttribute('id', $key);
        $transUnitChild->addChild('source', $key);

        return $transUnitChild;
    }

    public function isValidXml($key, $value): bool
    {
        try {
            $xmlElement = $this->getBaseXml();
            $xmlElement->addAttribute('xmlns', 'urn:oasis:names:tc:xliff:document:1.2');
            $transUnitChild = $this->addTransUnitNode($xmlElement, $key);
            $transUnitChild->addChild('target', $value);
            $errors = XliffUtils::validateSchema(XmlUtils::parse($xmlElement->asXML()));

            if ($errors) {
                $this->logger->error(XliffUtils::getErrorsAsString($errors));

                return false;
            }
            if (empty($transUnitChild->target) && !empty($value)) {
                $this->logger->error("Traduction incompatible en XML : '$key' => $value");

                return false;
            }
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->logger->error($exception->getTraceAsString());

            return false;
        }

        return true;
    }
}
