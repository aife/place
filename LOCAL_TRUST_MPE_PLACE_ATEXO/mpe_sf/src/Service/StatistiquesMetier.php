<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use Exception;
use App\Entity\Consultation;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Service qui permet de calculer les statisqtiques pour tout les organismes actives.
 *
 * Class StatistiquesMetier
 */
class StatistiquesMetier
{
    /**
     * StatistiquesMetier constructor.
     */
    public function __construct(public LoggerInterface $logger, public ParameterBagInterface $parameterBag, public EntityManagerInterface $em)
    {
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger): StatistiquesMetier
    {
        $this->logger = $logger;

        return $this;
    }

    public function getParameterBag(): ParameterBagInterface
    {
        return $this->parameterBag;
    }

    public function setParameterBag(ParameterBagInterface $parameterBag): StatistiquesMetier
    {
        $this->parameterBag = $parameterBag;

        return $this;
    }

    public function getEm(): EntityManager
    {
        return $this->em;
    }

    public function setEm(EntityManager $em): StatistiquesMetier
    {
        $this->em = $em;

        return $this;
    }

    /**
     * @param $dateDebut
     * @param $dateFin
     *
     * @return array
     */
    public function renvoiStatistiquesPrado($dateDebut, $dateFin)
    {
        $resultatsOffres = $resultsConsultations = $resultsRetraits = [];
        $categories = ['travaux', 'fournitures', 'services'];
        foreach ($categories as $categorie) {
            try {
                $resultsConsultations[$categorie] = $this->getStatistiquesConsultation(
                    $this->getParameterParCategorie($categorie),
                    $dateDebut ?? date('Y').'-01-01',
                    $dateFin ?? date('yy-m-d')
                );
            } catch (Exception $e) {
                $this->getLogger()->critical('Une erreur s\'est produite\n
                 Function : getStatistiquesConsultation \n
                 Type de statistiques: '.$categorie.' '.$e->getMessage());
            }
        }
        foreach ($categories as $categorie) {
            try {
                $resultsRetraits[$categorie] = $this->getStatistiquesRetraits(
                    $this->getParameterParCategorie($categorie),
                    $dateDebut ?? date('Y').'-01-01',
                    $dateFin ?? date('yy-m-d')
                );
            } catch (Exception $e) {
                $this->getLogger()->critical('Une erreur s\'est produite\n
                 Function : getNbTelechargementParCategorie \n
                 Type de statistiques: '.$categorie.' '.$e->getMessage());
            }
        }
        foreach ($categories as $categorie) {
            try {
                $resultatsOffres[$categorie] = $this->getStatistiquesOffres(
                    $this->getParameterParCategorie($categorie),
                    $dateDebut ?? date('Y').'-01-01',
                    $dateFin ?? date('yy-m-d')
                );
            } catch (Exception $e) {
                $this->getLogger()->critical('Une erreur s\'est produite\n
                 Function : getNbReponseElectroniqueParCategorie \n
                 Type de statistiques: '.$categorie.' '.$e->getMessage());
            }
        }

        return [
            'resultatsTravaux' => $resultsConsultations['travaux'],
            'resultatsFournitures' => $resultsConsultations['fournitures'],
            'resultatsServices' => $resultsConsultations['services'],
            'resultatsRetraitsTravaux' => $resultsRetraits['travaux'],
            'resultatsRetraitsFournitures' => $resultsRetraits['fournitures'],
            'resultatsRetraitsServices' => $resultsRetraits['services'],
            'resultatsOffresTravaux' => $resultatsOffres['travaux'],
            'resultatsOffresFournitures' => $resultatsOffres['fournitures'],
            'resultatsOffresServices' => $resultatsOffres['services'],
        ];
    }

    /**
     * @param $resultPerCategory
     *
     * @return int[]
     */
    public function calculStatistiques($resultPerCategory): array
    {
        $results = [
            'mapaInf' => 0,
            'mapaSupp' => 0,
            'totalMapa' => 0,
            'ao' => 0,
            'mn' => 0,
            'pn' => 0,
            'dc' => 0,
            'autre' => 0,
            'total' => 0,
            'sad' => 0,
            'accordcadre' => 0,
            'totalAccordCadreSad' => 0,
        ];

        foreach ($resultPerCategory as $row) {
            if (1 == $row['mapa']) {
                if ($row['id_montant_mapa'] == $this->getParameterBag()->get('MONTANT_MAPA_INF_90')) {
                    $results['mapaInf'] += $row['nombreConsultation'];
                } else {
                    $results['mapaSupp'] += $row['nombreConsultation'];
                }
                $results['totalMapa'] += $row['nombreConsultation'];
            } else {
                if ('1' == $row['ao']) {
                    $results['ao'] += $row['nombreConsultation'];
                    $results['total'] += $row['nombreConsultation'];
                } elseif ('1' == $row['mn']) {
                    $results['mn'] += $row['nombreConsultation'];
                    $results['total'] += $row['nombreConsultation'];
                } elseif ('1' == $row['pn']) {
                    $results['pn'] += $row['nombreConsultation'];
                    $results['total'] += $row['nombreConsultation'];
                } elseif ('1' == $row['dc']) {
                    $results['dc'] += $row['nombreConsultation'];
                    $results['total'] += $row['nombreConsultation'];
                } elseif ('1' == $row['sad']) {
                    $results['sad'] += $row['nombreConsultation'];
                    $results['totalAccordCadreSad'] += $row['nombreConsultation'];
                } elseif ('1' == $row['accordcadre']) {
                    $results['accordcadre'] += $row['nombreConsultation'];
                    $results['totalAccordCadreSad'] += $row['nombreConsultation'];
                } elseif ('1' == $row['autre']) {
                    $results['autre'] += $row['nombreConsultation'];
                    $results['total'] += $row['nombreConsultation'];
                }
            }
        }

        return $results;
    }

    /**
     * @param $category
     * @param $dateDebut
     * @param $dateFin
     * @param ContainerInterface $container
     * @param EntityManager      $em
     *
     * @return int[]
     */
    public function getStatistiquesConsultation($category, $dateDebut, $dateFin): array
    {
        $repository = $this->getEm()->getRepository(Consultation::class);
        $nbProcedureParCategorie = $repository->getNbProcedureParCategorie(
            $category,
            $this->getParameterBag()->get('TYPE_AVIS_CONSULTATION'),
            $dateDebut,
            $dateFin
        );

        return $this->calculStatistiques($nbProcedureParCategorie);
    }

    /**
     * @param $category
     * @param $dateDebut
     * @param $dateFin
     *
     * @return int[]
     */
    public function getStatistiquesRetraits($category, $dateDebut, $dateFin): array
    {
        $repository = $this->getEm()->getRepository(Consultation::class);
        $nbTelechargementParCategorie = $repository->getNbTelechargementParCategorie(
            $category,
            $this->getParameterBag()->get('TYPE_AVIS_CONSULTATION'),
            $dateDebut
        );

        return $this->calculStatistiques($nbTelechargementParCategorie);
    }

    /**
     * @param $category
     * @param $dateDebut
     * @param $dateFin
     *
     * @return int[]
     */
    public function getStatistiquesOffres($category, $dateDebut, $dateFin): array
    {
        $repository = $this->getEm()->getRepository(Consultation::class);
        $nbReponseElectroniqueParCategorie = $repository->getNbReponseElectroniqueParCategorie(
            $category,
            $this->getParameterBag()->get('TYPE_AVIS_CONSULTATION'),
            $dateDebut,
            $dateFin
        );

        return $this->calculStatistiques($nbReponseElectroniqueParCategorie);
    }

    /**
     * @param $category
     */
    public function getParameterParCategorie($category): string
    {
        $categories = [
            'travaux' => $this->getParameterBag()->get('TYPE_PRESTATION_TRAVAUX'),
            'fournitures' => $this->getParameterBag()->get('TYPE_PRESTATION_FOURNITURES'),
            'services' => $this->getParameterBag()->get('TYPE_PRESTATION_SERVICES'),
        ];

        return $categories[$category];
    }
}
