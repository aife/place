<?php

/**
 * @todo : Classe a supprimer dans la prochaine version mais avant faire l'analyse de la suppression
 */

namespace App\Service;

use DateTime;
use Exception;
use App\Controller\Api\AbstractWebserviceController;
use App\Entity\EchangesInterfaces;
use App\Entity\SsoTiers;
use App\Entity\Tiers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class OversightService
{
    private array $dataOverSight;

    /**
     * EchangesInterfacesService constructor.
     *
     * @param EntityManager $em
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function __construct(private ContainerInterface $container, private EntityManagerInterface $em, private RequestStack $requestStack)
    {
        $request = $this->requestStack->getCurrentRequest();
        $dateDebutExecution = (new DateTime())->format('Y:m:d H:i:s');
        $ticket = AbstractWebserviceController::getToken($request);
        $tiersInformations = $this->getTiersInformations($ticket);

        $this->dataOverSight = [
            'dateDebutExecution' => $dateDebutExecution,
            'codeRetour' => 0,
            'poids' => 0,
            'nbrFlux' => 1,
            'nomInterface' => (isset($tiersInformations)) ? $tiersInformations->getLogin() : null,
            'variableEntree' => json_encode($request->query->all(), JSON_THROW_ON_ERROR),
        ];
    }

    /**
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @param $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param EntityManager $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return RequestStack
     */
    public function getRequestStack()
    {
        return $this->requestStack;
    }

    public function setRequestStack(Request $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getDataOverSight(): array
    {
        return $this->dataOverSight;
    }

    public function setDataOverSight(array $dataOverSight)
    {
        $this->dataOverSight = $dataOverSight;

        return $this;
    }

    /**
     * Save informations on echanges_interface table at the beginning of Webservice's call.
     */
    public function setStartOversight(array $dataOversight)
    {
        $echangesInterfaces = new EchangesInterfaces();
        $echangesInterfaces->setService($dataOversight['service']);
        $echangesInterfaces->setNomBatch($dataOversight['nomBatch']);
        $echangesInterfaces->setDebutExecution($dataOversight['dateDebutExecution']);
        $echangesInterfaces->setCodeRetour($dataOversight['codeRetour']);
        $echangesInterfaces->setPoids($dataOversight['poids']);
        $echangesInterfaces->setNbFlux($dataOversight['nbrFlux']);
        $echangesInterfaces->setNomInterface($dataOversight['nomInterface']);
        $echangesInterfaces->setVariablesEntree($dataOversight['variableEntree']);

        return $echangesInterfaces;
    }

    /**
     * Save informations on echanges_interface table at the end of Webservice's call.
     *
     * @param $echangeInterfaces
     * @param $returnCode
     *
     * @throws Exception
     */
    public function setEndOversight($echangeInterfaces, $returnCode)
    {
        $dateFinExecution = (new DateTime())->format('Y:m:d H:i:s');
        $echangeInterfaces->setFinExecution($dateFinExecution);
        $echangeInterfaces->setCodeRetour($returnCode);
    }

    /**
     * Get Tiers Information from SsoTiers and Tiers.
     *
     * @param $ticket
     *
     * @return mixed
     */
    public function getTiersInformations($ticket)
    {
        $tiers = null;
        $ssoTiersRepository = $this->em->getRepository(SsoTiers::class);

        if (!empty($ticket)) {
            $ssoTiers = $ssoTiersRepository->findOneBy(['idSsoTiers' => $ticket]);
            if (!empty($ssoTiers)) {
                $idTiers = $ssoTiers->getIdTiers();
                $tiersRepository = $this->em->getRepository(Tiers::class);
                $tiers = $tiersRepository->findOneBy(['idTiers' => $idTiers]);
            }
        }

        return $tiers;
    }
}
