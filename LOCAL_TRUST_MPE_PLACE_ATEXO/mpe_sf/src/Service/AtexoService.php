<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Agent;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class AtexoChiffremetOffre de gestion des chiffrements des offres.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @since ESR2018
 *
 * @copyright Atexo 2018
 */
class AtexoService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ApiGouvEntrepriseSynchro $apiGouvEntrepriseSynchro,
        private readonly LoggerInterface $logger,
        private readonly HistoriqueSynchro $historiqueSynchro
    ) {
    }

    /**
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->parameterBag->get($name);
    }

    /**
     * Permet de recuperer un service selon le siren.
     *
     * @param int       $siren     le siren de service
     * @param int       $idObjet   l'id de l'objet pour le stoquer dans l'historique
     *
     * @return mix Atexo_Entreprise_EntrepriseVo l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2018
     *
     * @copyright Atexo 2018
     */
    public function recupererServiceApiGouvEntreprise($siren, int $idObjet)
    {
        $token = $this->parameterBag->get('API_ENTREPRISE_TOKEN');

        $atexoInterface = $this->apiGouvEntrepriseSynchro;
        $paramsWs = [
            'token' => $token,
            'api_gouv_url_entreprise' => $this->parameterBag->get('URL_API_ENTREPRISE_UNITE_LEGALE'),
            'api_gouv_url_etablissement' => $this->parameterBag->get('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $atexoInterface->setParameters($paramsWs);
        $resultat = $atexoInterface->getEntrepriseBySiren($siren, false);
        $loggerWs = $this->logger;

        if (is_array($resultat) && $atexoInterface->isResponseSuccess($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'entite d'achat");
            $historiqueSynchro = $this->historiqueSynchro;
            $historiqueSynchro->enregistrerHistorique('SERVICE', $siren, $jeton, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'entite d'achat");
            $entreprise = $resultat[1];

            return $entreprise;
        }

        return false;
    }

    /**
     * @return Agent
     */
    public function getAgent(int $id): ?Agent
    {
        return $this->em->getRepository(Agent::class)->find($id);
    }

    public function getInscrit(int $id): ?Inscrit
    {
        return $this->em->getRepository(Inscrit::class)->find($id);
    }
}
