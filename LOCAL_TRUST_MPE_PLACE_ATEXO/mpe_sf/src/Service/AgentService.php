<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationTypeProcedure;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Agent\AgentServiceMetier;
use App\Entity\Service;
use App\Entity\SsoAgent;
use App\Filter\AgentHabilitationFilter;
use App\Form\Habilitation\HabilitationAgentType;
use App\Repository\Agent\AgentServiceMetierRepository;
use App\Repository\AgentRepository;
use App\Repository\HabilitationAgentRepository;
use App\Repository\HabilitationTypeProcedureRepository;
use App\Repository\InvitePermanentTransverseRepository;
use App\Repository\Organisme\ServiceMetierRepository;
use Application\Service\Atexo\Atexo_Util;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AgentService
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBagInterface,
        private readonly EntityManagerInterface $em,
        private readonly AtexoAgent $atexoAgent,
        private readonly AgentRepository $agentRepository,
        private readonly AgentServiceMetierRepository $agentServiceMetierRepository,
        private readonly HabilitationAgentRepository $agentHabilitationRepository,
        private readonly HabilitationTypeProcedureRepository $habilitationTypeProcedureRepository,
        private readonly InvitePermanentTransverseRepository $invitePermanentTransverseRepository
    ) {
    }

    /**
     * @param $idAgent
     *
     * @return Agent|object|null
     */
    public function getAgent($idAgent)
    {
        return $this->em->getRepository(Agent::class)->find($idAgent);
    }

    /**
     * Retourne l'id de l'agent si le Sso est valide, sinon false.
     *
     * @param $idSso
     *
     * @return bool or idAgent
     */
    public function getIdAgentFromIdSso($idSso, $idServiceMetierConsommateur)
    {
        $ssoAgent = $this->em->getRepository(SsoAgent::class)->checkToken($idSso, $idServiceMetierConsommateur);
        return empty($ssoAgent) ? false : $ssoAgent->getIdAgent();
    }

    /**
     * destroy le Sso si valide
     *
     * @param integer $idSso
     * @param integer $idServiceMetierConsommateur
     *
     * @return void
     */
    public function destroySso($idSso, $idServiceMetierConsommateur)
    {
        $ssoAgent = $this->em->getRepository(SsoAgent::class)->checkToken($idSso, $idServiceMetierConsommateur);
        if ($ssoAgent instanceof SsoAgent) {
            $ssoAgent->setDateLastRequest(date('Y-m-d H:i:s'));
            $this->em->persist($ssoAgent);
            $this->em->flush();
        }
    }

    /**
     * @param $idAgent
     * @param $organisme
     * @param $idService
     */
    public function getUrlSSoExec(CurrentUser $currentUser)
    {
        return $this->parameterBagInterface->get('URL_EXEC_SSO') . $this->getSsoForSocle($currentUser);
    }

    public function getSsoForSocle(CurrentUser $currentUser)
    {
        $serviceMetierExec = $this->parameterBagInterface->get('SERVICE_METIER_EXEC');
        $sso = $this->getSso($currentUser, $serviceMetierExec);

        if (empty($sso) && $currentUser->isAgent()) {
            $sso = $this->dumpSession($currentUser);
            $ssoAgent = new SsoAgent();
            $ssoAgent->setIdSso($sso);
            $ssoAgent->setIdAgent($currentUser->getIdAgent());
            $ssoAgent->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoAgent->setOrganisme($currentUser->getAcronymeOrga());
            $ssoAgent->setDateLastRequest(Atexo_Util::dateDansFutur(
                date('Y-m-d H:i:s'),
                $this->parameterBagInterface->get('NBR_HEURE_MAINTENANCE_CNX')
            ));
            $ssoAgent->setServiceId($serviceMetierExec);
            $this->em->persist($ssoAgent);
            $this->em->flush();
        }

        return $sso;
    }

    /**
     * @param $idServiceMetier
     *
     * @return false|string
     */
    public function getSso(CurrentUser $currentUser, $idServiceMetier): ?string
    {
        $sso = false;
        $ssoAgent = $this->em
            ->getRepository(SsoAgent::class)
            ->getSsoAgentValid(
                $currentUser->getIdAgent(),
                $currentUser->getAcronymeOrga(),
                $idServiceMetier
            );
        if ($ssoAgent instanceof SsoAgent) {
            $sso = $ssoAgent->getIdSso();
        }

        return $sso;
    }

    public function dumpSession(CurrentUser $currentUser): string
    {
        $sess = [];
        $id = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $agent = $this->em->getRepository(Agent::class)->find($currentUser->getIdAgent());
        $sess['loginAgent'] = $agent->getLogin();
        $sess['organisme'] = $agent->getOrganisme();
        $sess_string = serialize($sess);
        $sess_file_name = $this->parameterBagInterface->get('PATH_SESSION_SSO') . $id;
        $fp = fopen($sess_file_name, 'w');
        fwrite($fp, $sess_string);
        fclose($fp);

        return $id;
    }

    /**
     * @param Agent $agent
     * @param bool $concactSigle
     * @return string
     */
    public function getLabelService(Agent $agent, bool $concactSigle = true): string
    {
        $format = '%s - %s';
        $labelService =  $agent->getOrganisme()->getDenominationOrg();
        if ($concactSigle) {
            $labelService = sprintf(
                $format,
                $agent->getOrganisme()->getSigle(),
                $agent->getOrganisme()->getDenominationOrg()
            );
        }
        if ($agent->getServiceId() != null) {
            $serviceAgent = $this->em->getRepository(Service::class)
                ->findOneBy([
                    'id' => $agent->getServiceId(),
                    'organisme' => $agent->getOrganisme()?->getAcronyme(),
                ]);
            if ($serviceAgent instanceof Service) {
                $labelService = $concactSigle ? $serviceAgent->getCheminComplet() : $serviceAgent->getLibelle();
            }
        }

        return $labelService;
    }

    /**
     * @param Agent $agent
     * @return string|null
     */
    public function getSirenService(Agent $agent): ?string
    {
        $siren = '';
        if ($agent->getServiceId() != null) {
            $serviceAgent = $this->em->getRepository(Service::class)
                ->findOneBy([
                    'id' => $agent->getServiceId(),
                    'organisme' => $agent->getOrganisme()->getAcronyme(),
                ]);
            if ($serviceAgent instanceof Service) {
                $siren = $serviceAgent->getSiren();
            }
        }
        return $siren;
    }

    /**
     * @param Agent $agent
     * @return string|null
     */
    public function getComplementService(Agent $agent): ?string
    {
        $complement = '';
        if ($agent->getServiceId() != null) {
            $serviceAgent = $this->em->getRepository(Service::class)
                ->findOneBy([
                    'id' => $agent->getServiceId(),
                    'organisme' => $agent->getOrganisme()->getAcronyme(),
                ]);
            if ($serviceAgent instanceof Service) {
                $complement = $serviceAgent->getComplement();
            }
        }
        return $complement;
    }

    public function isAgentInArray(Agent $agent, array $agents): bool
    {
        /* @var Agent $value */
        foreach ($agents as $value) {
            if ($value->getId() === $agent->getId()) {
                return true;
            }
        }

        return false;
    }

    public function duplicate($idAgent): array
    {
        /** @var Agent $agent */
        $agent = $this->agentRepository->find($idAgent);

        if (empty($agent)) {
            return ['agent' => null, 'password' => null];
        }

        /** @var Agent $newAgent */
        $newAgent = clone $agent;
        $password = $this->generatePassword(10);
        $newAgent
            ->setActif('0')
            ->setIdExterne('0')
            ->setTypeHash(Agent::ARGON_TYPE)
            ->setLogin('login_copie_' . rand(0, 1000))
            ->setHabilitation(null)
            ->setHabilitationTypeProcedures(new ArrayCollection());
        $this->atexoAgent->updateNewAgentPasswordByArgon2($newAgent, $password);

        $habilitationLegacy = $agent->getHabilitation();
        $habilitations = $agent->getHabilitationTypeProcedures();

        $invitePermanentTransverseList = $this->invitePermanentTransverseRepository->findBy(['agent' => $agent]);

        $agentServiceMetierList = $this->agentServiceMetierRepository->findBy(['idAgent' => $agent->getId()]);

        $this->em->persist($newAgent);
        $this->em->flush();

        $newAgent
            ->setNom($agent->getNom() . '_copie_' . $newAgent->getId())
            ->setPrenom($agent->getPrenom() . '_copie_' . $newAgent->getId())
            ->setEmail($agent->getEmail() . '_copie_' . $newAgent->getId())
            ->setLogin($agent->getLogin() . '_copie_' . $newAgent->getId());

        if (!empty($habilitationLegacy)) {
            $newHabilitationLegacy = clone $habilitationLegacy;
            $newHabilitationLegacy->setAgent($newAgent);
            $this->em->persist($newHabilitationLegacy);
        }

        /** @var HabilitationTypeProcedure $habilitation */
        foreach ($habilitations as $habilitation) {
            $newHabilitation = clone $habilitation;
            $newHabilitation->setAgent($newAgent);
            $this->em->persist($newHabilitation);
        }

        /** @var InvitePermanentTransverse $invitePermanentTransverse */
        foreach ($invitePermanentTransverseList as $invitePermanentTransverse) {
            $newInvitePermanentTransverse = clone $invitePermanentTransverse;
            $newInvitePermanentTransverse->setAgent($newAgent);
            $this->em->persist($newInvitePermanentTransverse);
        }

        /** @var AgentServiceMetier $agentServiceMetier */
        foreach ($agentServiceMetierList as $agentServiceMetier) {
            $newAgentServiceMetier = clone $agentServiceMetier;
            $newAgentServiceMetier->setIdAgent($newAgent->getId());
            $this->em->persist($newAgentServiceMetier);
        }

        $this->em->flush();

        return [
            'agent' => $newAgent,
            'password' => $password,
        ];
    }

    private function generatePassword($car): string
    {
        $string = '';
        $chaine = 'abcdefghijklmnpqrstuvwxy';
        mt_srand((float)microtime() * 1_000_000);
        for ($i = 0; $i < $car; ++$i) {
            $string .= $chaine[random_int(0, mt_getrandmax()) % strlen($chaine)];
        }

        return $string;
    }
}
