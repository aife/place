<?php

namespace App\Service;

use App\Entity\CategorieConsultation;
use App\Entity\Consultation;
use App\Entity\Lot;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class LotService permettant la gestion des lots.
 *
 * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
 */
class LotService
{
    public final const SEPARATOR = '#';

    /**
     * LotService constructor.
     *
     * @param Session $session
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SessionInterface $session,
        private readonly ClausesMigrationService $clausesMigrationService,
        private readonly ClausesService $clausesService
    ) {
    }

    public function getLot(int $lotId): ?Lot
    {
        return $this->em->getRepository(Lot::class)->find($lotId);
    }

    public function importLot($data)
    {
        $lot = new Lot();

        $data = $this->cleanUserDatas($data);

        // GET organisme / consultation
        $lot->setOrganisme($data['acronyme']);
        $consultation = $this->em->getRepository(Consultation::class)->find($data['consultation']);
        $lot->setConsultation($consultation);

        // field on excel file
        $lot->setLot($data['row']['number']);
        $lot->setDescription($data['row']['name']);
        $lot->setDescriptionDetail($data['row']['description']);
        $categorieConsultation = $this->em->getRepository(CategorieConsultation::class)->find($data['row']['category']);
        $lot->setCategorie($categorieConsultation);
        $lot->setCodeCpv1($data['row']['code_cpv']);
        $lot->setCodeCpv2($data['row']['code_cpv_2']);

        //Clause sociale
        $lot->setClauseSociale($data['row']['social_condition']);
        $lot->setClauseSocialeConditionExecution($data['row']['execution_social_condition']);
        $lot->setClauseSocialeInsertion($data['row']['attribution_social_condition']);
        $lot->setClauseSocialeAteliersProteges($data['row']['esat_ea_social_condition']);
        $lot->setClauseSocialeSiae($data['row']['siae_social_condition']);
        $lot->setClauseSocialeEss($data['row']['eess_social_condition']);
        $lot->setMarcheInsertion($data['row']['marche_insertion']);
        $lot->setClauseSpecificationTechnique($data['row']['clause_specification_technique']);

        //Clause Environnement
        $lot->setClauseEnvironnementale($data['row']['environment']);
        $lot->setClauseEnvSpecsTechniques($data['row']['technical_environment']);
        $lot->setClauseEnvCondExecution($data['row']['execution_environment']);
        $lot->setClauseEnvCriteresSelect($data['row']['attribution_environment']);

        // Migration vers les nouvelles clauses
        $this->clausesMigrationService->migrateClauses($lot);

        $this->em->persist($lot);
        $this->em->flush();
    }

    /**
     * Remove dirty things from users datas.
     */
    protected function cleanUserDatas(array $data): array
    {
        foreach ($data['row'] as &$el) {
            if (is_string($el)) {
                $el = trim($el);
            }
        }

        return $data;
    }

    /**
     * @param $consultation
     *
     * @return bool
     */
    public function canImport($consultationId)
    {
        $return = true;
        $contexte = $this->session->get('contexte_authentification');
        if (isset($contexte['consultationsAutorisees'])) {
            $return = (bool) in_array($consultationId, $contexte['consultationsAutorisees']);
        }
        return $return;
    }

    public function getExplodedCodeCpv(?string $codeCpv, string $separator): array
    {
        if (empty($codeCpv)) {
            return [];
        }
        $trimedCodeCpv = trim($codeCpv, $separator);

        return $trimedCodeCpv ? explode($separator, $trimedCodeCpv) : [];
    }

    public function getConcatenatedCodeCpv(object $lotInput): ?string
    {
        if (!$lotInput->codeCpvSecondaire1 && !$lotInput->codeCpvSecondaire2 && !$lotInput->codeCpvSecondaire3) {
            return null;
        }

        $codeCpv2 = trim(implode(self::SEPARATOR, [
            $lotInput->codeCpvSecondaire1,
            $lotInput->codeCpvSecondaire2,
            $lotInput->codeCpvSecondaire3
        ]), self::SEPARATOR);

        return self::SEPARATOR . $codeCpv2 . self::SEPARATOR;
    }

    public function removeLotsFromConsultation(Consultation $consultation): void
    {
        $lots = $consultation->getLots();
        foreach ($lots as $lot) {
            $this->clausesService->deleteClausesRelatedTo($lot);
            $this->em->remove($lot);
        }
    }
}
