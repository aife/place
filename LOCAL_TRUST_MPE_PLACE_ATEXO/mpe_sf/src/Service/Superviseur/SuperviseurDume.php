<?php

namespace App\Service\Superviseur;

use App\Service\AtexoConfiguration;
use App\Entity\TDumeContexte;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class SuperviseurDume extends SuperviseurAbstract
{
    public array $specificStatus = ['DumeInterface'];
    public string $nameService = 'interface';

    public function __construct(protected ContainerInterface $container, protected ?EntityManagerInterface $em = null)
    {
    }

    /**
     * @return mixed
     */
    public function checkDumeInterface()
    {
        $result = [];
        if ($this->container->get(AtexoConfiguration::class)->hasConfigPlateforme('InterfaceDume')) {
            $url = $this->container->getParameter('URL_PF_DUME_API');
            $connectTimeout = $this->container->getParameter('CURLOPT_CONNECTTIMEOUT_MS');
            $timeout = $this->container->getParameter('CURLOPT_TIMEOUT_MS');
            $httpCode = $this->connectToInterface($url, false, $connectTimeout, $timeout);

            $result['globalStatus'] = 'OK';
            $result['function'] = 'Dume';

            if (200 === $httpCode) {
                $this->setSpecificStatuses($result, 'curl', 'OK', '', 'Connexion OK');
            } else {
                $result['globalStatus'] = 'KO';
                $result['messages'][] = [
                    'message' => 'Impossible de se connecter au Dume',
                ];
            }
            $dumesEnAttente = $this->em->getRepository(TDumeContexte::class)->getDumesEnAttente();
            $this->setSpecificStatuses(
                $result,
                'DumeEnAttente',
                'OK',
                $dumesEnAttente,
                'Nombre de DUME en attente de publication: '.$dumesEnAttente
            );
        }

        return $result;
    }
}
