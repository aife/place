<?php

namespace App\Service\Superviseur;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class SuperviseurJmsJob extends SuperviseurAbstract
{
    public array $specificStatus = ['JmsJob'];
    public string $nameService = 'service';

    public function __construct(protected ContainerInterface $container, protected ?EntityManagerInterface $em = null)
    {
    }

    /**
     * Display jms jobs states.
     */
    public function checkJmsJob()
    {
        $result = [];
        $jmsStatuses =
            [
                'pending' => [
                    'message' => 'Nombre de jobs JMS en attente: ',
                    'status' => 'OK',
                ],
                'canceled' => [
                    'message' => 'Nombre de jobs JMS annulés: ',
                    'status' => 'Warning',
                ],
                'running' => [
                    'message' => 'Nombre de jobs JMS en cours: ',
                    'status' => 'OK',
                ],
                'finished' => [
                    'message' => 'Nombre de jobs JMS finis: ',
                    'status' => 'OK',
                ],
                'failed' => [
                    'message' => 'Nombre de jobs JMS erronés: ',
                    'status' => 'KO',
                ],
                'terminated' => [
                    'message' => 'Nombre de jobs JMS terminés: ',
                    'status' => 'OK',
                ],
            ];
        $result['globalStatus'] = 'OK';
        $result['function'] = 'jmsJob';

        foreach ($jmsStatuses as $fct => $jmsStatuse) {
            $nbrJob = $this->getJmsJobByStatus($fct);
            $this->setSpecificStatuses($result, $fct, $jmsStatuse['status'], $nbrJob, $jmsStatuse['message'].$nbrJob);
        }

        return $result;
    }

    /**
     * Return the number of JMS on filtering by status.
     *
     * @param $status
     *
     * @return mixed
     */
    public function getJmsJobByStatus($status)
    {
        $checkJms = $this->em->createQuery('SELECT count(j.id)
                FROM JMSJobQueueBundle:Job j 
                WHERE j.state = :state 
                ORDER BY j.id ASC')
            ->setParameter('state', $status)
            ->getOneOrNullResult();

        return current($checkJms);
    }
}
