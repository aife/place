<?php

namespace App\Service\Superviseur;

use CURLFile;
use Psr\Container\ContainerInterface;

class SuperviseurOpenOffice extends SuperviseurAbstract
{
    public array $specificStatus = ['OpenOffice'];
    public string $nameService = 'service';

    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * @return mixed
     */
    public function checkOpenOffice()
    {
        $result = [];
        $url = $this->container->getParameter('URL_OOO');
        // mettre un document odt dans Common_tmp
        $fileName = 'example.odt';
        $basePath = $this->container->getParameter('COMMON_TMP');
        $pathDir = $basePath.session_id().time().'modeleOdt/';

        if (!is_writable($basePath)) {
            $result['globalStatus'] = 'KO';
            $result['function'] = 'openOffice';
            $message = 'Le répertoire : '.$basePath.' - ('.
                substr(sprintf('%o', fileperms($basePath)), -4).") n'est pas accessible en écriture";
            $this->setSpecificStatuses($result, 'droitEcriture_COMMON_TMP', 'KO', '', $message);
            $result['messages'][] = [
                'message' => $message,
            ];
        } else {
            if (mkdir($pathDir)) {
                $filePath = $pathDir.$fileName;
                $file = file_put_contents($filePath, $this->randomString(40000));

                $result['globalStatus'] = 'OK';
                $result['function'] = 'openOffice';

                if (file_exists($filePath) && $file) {
                    $postData = [];
                    $postData['odtFile'] = new CURLFile($filePath);
                    $postData['extFile'] = 'pdf';
                    $proxyUrl = $this->container->getParameter('ATX_PDF_PROXY_URL');
                    $proxyPort = $this->container->getParameter('ATX_PDF_PROXY_PORT');
                    $ch = curl_init();

                    if (!empty($proxyUrl) && !empty($proxyPort)) {
                        curl_setopt($ch, CURLOPT_PROXY, $proxyUrl.':'.$proxyPort);
                    }
                    $atxPdfServeurIndex = $this->container->getParameter('ATX_PDF_SERVEUR_INDEX');
                    curl_setopt($ch, CURLOPT_URL, (!empty($atxPdfServeurIndex)) ? $atxPdfServeurIndex : $url);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    $postResult = curl_exec($ch);
                    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    $pathPdfFile = $pathDir.'pdfFile.pdf';
                    $pdfFile = file_put_contents($pathPdfFile, $postResult);
                    if ('200' != $httpCode && '201' != $httpCode) {
                        $result['globalStatus'] = 'KO';
                        $result['messages'][] = [
                            'message' => 'Impossible de se connecter au serveur OpenOffice',
                        ];
                    } elseif ($pdfFile && 1 !== (int) $postResult && !curl_errno($ch)) {
                        $result['specificStatuses'] =
                            [
                                'specificStatus' => [
                                    'function' => 'curl',
                                    'status' => 'OK',
                                    'value' => true,
                                    'message' => 'Génération de document: URL: '.$pathPdfFile,
                                ],
                            ];
                    } else {
                        $result['globalStatus'] = 'KO';
                        $result['messages'][] = [
                            'message' => 'Echec de génération de document: URL :'.$pathPdfFile,
                        ];

                        $result['specificStatuses'] =
                            [
                                'specificStatus' => [
                                    'function' => 'curl',
                                    'status' => 'KO',
                                    'value' => false,
                                    'message' => 'Echec de génération de document: URL :'.$pathPdfFile,
                                ],
                            ];
                    }

                    curl_close($ch);
                    unlink($filePath);
                    unlink($pathDir.'pdfFile.pdf');
                }
            } else {
                $result['globalStatus'] = 'KO';
                $result['messages'][] = [
                    'message' => 'Impossible de créer le répertoire modeleODT',
                ];
            }
        }

        return $result;
    }
}
