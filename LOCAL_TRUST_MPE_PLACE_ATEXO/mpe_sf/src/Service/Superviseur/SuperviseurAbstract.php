<?php

namespace App\Service\Superviseur;

use App\Entity\ConfigurationOrganisme;

abstract class SuperviseurAbstract
{
    protected static array $state = [];

    public string $host;

    public string $port;

    public string $name;

    public static array $resultDefaultSpecific = ['function' => '', 'status' => 'NA', 'msg' => '', 'msgInfos' => ''];

    public static array $resultDefaultService = ['function' => '', 'globalStatus' => 'NA', 'messages' => []];

    public static array $resultService = [];

    public static array $resultSpecific = [];

    public function getSpecificStatuses()
    {
        static::setTabService($this->nameService);
        $resultCheckService = static::getTabService();
        $listeInterface = [];

        foreach ($this->specificStatus as $methodToCheck) {
            $caller = 'check'.ucfirst($methodToCheck);
            $currentFunctionResult = $this->$caller();

            $result = array_merge($resultCheckService[$this->nameService], $currentFunctionResult);
            if ('interface' === $this->nameService) {
                $listeInterface[] = $result;
            } else {
                $resultCheckService[$this->nameService] = $result;
            }
        }
        if ('interface' === $this->nameService) {
            $resultCheckService[$this->nameService] = $listeInterface;
        }

        return $resultCheckService;
    }

    /**
     * @return array
     */
    public static function getState()
    {
        return static::$state;
    }

    public static function setTabService($nameService)
    {
        static::$resultService = [$nameService => static::$resultDefaultService];
    }

    /**
     * @return array
     */
    public static function getTabService()
    {
        return static::$resultService;
    }

    public static function setTabSpecific($nameFunction)
    {
        static::$resultSpecific = [$nameFunction => static::$resultDefaultSpecific];
        static::$resultSpecific[$nameFunction]['function'] = $nameFunction;
    }

    /**
     * @return array
     */
    public static function getTabSpecific()
    {
        return static::$resultSpecific;
    }

    /**
     * Generate a random String.
     *
     * @param int $length
     *
     * @return string
     */
    public function randomString($length = 6)
    {
        $str = '';
        $characters = array_merge(range('A', 'Z'), range('a', 'z'), range('0', '9'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; ++$i) {
            $rand = random_int(0, $max);
            $str .= $characters[$rand];
        }

        return $str;
    }

    /**
     * Fonction qui permet de vérifier la connexion vers une interface donnée.
     *
     * Cette partie va être revue ultérieurement pour être remplacé par du guzzle
     *
     * @param $url
     *
     * @return bool|mixed
     */
    public function connectToInterface($url, $ssl = false, $connectTimeout = null, $timeout = null)
    {
        if (!empty($url)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if (!empty($connectTimeout)) {
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, $connectTimeout);
            }
            if (!empty($timeout)) {
                curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
            }
            if ($ssl) {
                // vérifier certificat SSL
                $this->verifySsl($ch, 2, 1);
            }

            $jnlpResult = curl_exec($ch);
            $verifySslResult = curl_getinfo($ch, CURLINFO_SSL_VERIFYRESULT);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $urlProxy = $this->container->getParameter('URL_PROXY');
            $portProxy = $this->container->getParameter('PORT_PROXY');
            $curlOptProxy = $urlProxy.':'.$portProxy;
            if ('' !== $urlProxy) {
                curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
            }

            if (0 === $verifySslResult) {
                if ($httpCode) {
                    return $httpCode;
                }
            } else {
                if ($ssl) {
                    // ne pas vérifier la partie SSL
                    $this->verifySsl($ch, 0, 0);
                }

                $checkSslResult = curl_exec($ch);
                $verifySslWithoutCa = curl_getinfo($ch, CURLINFO_SSL_VERIFYRESULT);

                if (0 === $verifySslWithoutCa) {
                    return false;
                } else {
                    return curl_getinfo($ch, CURLINFO_HTTP_CODE);
                }
            }
        }

        return false;
    }

    /**
     * Vérifier les paramètres SSL.
     *
     * @param $ch
     * @param $host
     * @param $peer
     */
    public function verifySsl(&$ch, $host, $peer)
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $host);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $peer);
    }

    /**Fonction permettant de définir une liste de status
     * @param $result
     * @param $function
     * @param $status
     * @param $value
     * @param $message
     */
    public function setSpecificStatuses(&$result, $function, $status, $value, $message)
    {
        $result['specificStatuses'][] =
            [
                'specificStatus' => [
                    'function' => $function,
                    'status' => $status,
                    'value' => $value,
                    'message' => $message,
                ],
            ];
    }

    /**
     * Fonction permettant de tester la connexion vers une Interface donnée.
     *
     * @param $url
     * @param $function
     *
     * @return mixed
     */
    public function checkInterface($url, $function, $ssl = false, $connectTimeout = null, $timeout = null)
    {
        $result = [];
        $httpCode = $this->connectToInterface($url, $ssl, $connectTimeout, $timeout);
        $result['globalStatus'] = 'OK';
        $result['function'] = $function;
        if (200 === $httpCode) {
            $this->setSpecificStatuses($result, 'curl', 'OK', '', 'Serveur '.$function.' Ok');
        } else {
            $result['globalStatus'] = 'KO';
            $result['messages'][] = [
                'message' => 'Impossible de se connecter au serveur '.$function,
            ];
        }

        return $result;
    }

    /**
     * @param $config
     *
     * @return bool
     */
    public function hasConfigOrg($config)
    {
        /**
         * @var ConfigurationOrganisme $orgConfig
         */
        $orgConfig = $this
            ->em
            ->getRepository(ConfigurationOrganisme::class)
            ->findOneBy([$config => 1]);

        return $orgConfig instanceof ConfigurationOrganisme;
    }
}
