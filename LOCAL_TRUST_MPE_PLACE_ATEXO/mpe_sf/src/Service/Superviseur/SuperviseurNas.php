<?php

namespace App\Service\Superviseur;

use Psr\Container\ContainerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class SuperviseurNas extends SuperviseurAbstract
{
    public array $specificStatus = ['NASWriteSpeed'];
    public string $nameService = 'service';

    public function __construct(protected ContainerInterface $container)
    {
    }

    /**
     * Displays the write time on NAS.
     *
     * @return mixed
     */
    public function checkNASWriteSpeed()
    {
        $result = [];
        $stopwatch = new Stopwatch();
        $stopwatch->start('NASWriteSpeed');

        $fileName = 'page.txt';
        $pathDir = $this->container->getParameter('COMMON_TMP');

        if (!is_writable($pathDir)) {
            $result['globalStatus'] = 'KO';
            $result['function'] = 'NASWriteSpeed';
            $message = 'Le répertoire : '.$pathDir.' - ('.substr(sprintf('%o', fileperms($pathDir)), -4).") n'est pas accessible en écriture";
            $this->setSpecificStatuses($result, 'droitEcriture_COMMON_TMP', 'KO', '', $message);
            $result['messages'][] = [
                'message' => $message,
            ];
        } else {
            $filePath = $pathDir.$fileName;
            $file = file_put_contents($filePath, $this->randomString(500000));

            $hashFile = sha1(file_get_contents($filePath));
            $fileSize = filesize($filePath);

            if ($fileSize) {
                $fileSize = number_format($fileSize / 1024, 2);
            }

            $nasDir = $this->container->getParameter('FICHIERS_ARCHIVE_DIR');
            $fileNasPath = $nasDir.$fileName;
            $copied = copy($filePath, $fileNasPath);
            $hashfileNas = sha1(file_get_contents($fileNasPath));

            $event = $stopwatch->stop('NASWriteSpeed');
            $duration = $event->getDuration();

            $result['globalStatus'] = 'OK';
            $result['function'] = 'NASWriteSpeed';

            if ($duration && $copied) {
                $result['specificStatuses'] =
                [
                    'specificStatus' => [
                        'function' => 'ResponseTime',
                        'status' => 'OK',
                        'value' => $duration,
                        'message' => 'Un fichier de '.$fileSize.' Ko a été écrit en '.$duration.' ms',
                    ],
                ];
            } else {
                $result['globalStatus'] = 'KO';
                $result['messages'][] = ['message' => 'Impossible de copier le fichier vers le répertoire NAS'];
            }

            if ($hashfileNas !== $hashFile) {
                $result['globalStatus'] = 'KO';
                $result['specificStatuses']['specificStatus']['message'] = 'Les hash des fichiers sont différents entre Fichier NAS et COMMON_TMP';
                $result['messages'][] = [
                'message' => $result['specificStatuses']['specificStatus']['message'],
                ];
            }

            // Delete NAS file
            if ($file) {
                unlink($filePath);
                unlink($fileNasPath);
            }
        }

        return $result;
    }
}
