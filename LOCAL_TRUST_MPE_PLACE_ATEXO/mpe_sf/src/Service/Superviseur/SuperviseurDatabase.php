<?php

namespace App\Service\Superviseur;

use Exception;
use Doctrine\ORM\EntityManagerInterface;

class SuperviseurDatabase extends SuperviseurAbstract
{
    public array $specificStatus = ['connect', 'select'];

    public string $nameService = 'mysql';

    public function __construct(protected EntityManagerInterface $doctrine, int $timeout = 0)
    {
        $this->timeout = $timeout;
    }

    public function checkConnect()
    {
        static::setTabSpecific('connect');
        $result = static::getTabSpecific();
        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $this->timeout);
        try {
            $this->doctrine->getConnection()->connect();
            $this->doctrine->getConnection()->close();
            $result['connect']['status'] = 'OK';
        } catch (Exception $e) {
            $result['connect']['status'] = 'KO';
            $result['connect']['msg'] = $e->getMessage();
        }
        ini_set('default_socket_timeout', $defaultTimeout);

        return $result;
    }

    public function checkSelect()
    {
        static::setTabSpecific('select');
        $result = static::getTabSpecific();
        $defaultTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $this->timeout);
        try {
            $entityManager = $this->doctrine->getManager()->getConnection();
            $query = $entityManager->prepare('SELECT count(organisme) as nb_consultation FROM consultation LIMIT 10');
            $resultQuery = $query->executeQuery()->fetchAllAssociative();
            $this->doctrine->getConnection()->close();
            $result['select']['status'] = 'OK';
            $result['select']['msgInfos'] = 'Nombre de consultations : '.$resultQuery[0]['nb_consultation'];
        } catch (Exception $e) {
            $result['select']['status'] = 'KO';
            $result['select']['msg'] = $e->getMessage();
        }
        ini_set('default_socket_timeout', $defaultTimeout);

        return $result;
    }
}
