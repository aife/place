<?php

namespace App\Service\Superviseur;

use Psr\Container\ContainerInterface;

class SuperviseurPhp extends SuperviseurAbstract
{
    public array $specificStatus = ['Phpversion', 'Ssl'];

    public string $nameService = 'service';

    public function __construct(protected ContainerInterface $container)
    {
    }

    public function checkMemory()
    {
        static::setTabSpecific('memory');
        $result = static::getTabSpecific();

        $result['memory']['status'] = 'OK';
        $result['memory']['msgInfos'] = $this->convert(memory_get_usage());

        $memUsage = $this->getServerMemoryUsage(false);
        $result['memory']['msgInfos'] = sprintf(
            'Memory usage: %s / %s (%s%%)',
            $this->getNiceFileSize($memUsage['total'] - $memUsage['free']),
            $this->getNiceFileSize($memUsage['total']),
            number_format($this->getServerMemoryUsage(true), 2, '.', '')
        );

        return $result;
    }

    public function checkOpcache()
    {
        static::setTabSpecific('opcache');
        $result = static::getTabSpecific();

        if (!function_exists('opcache_get_status') || !opcache_get_status()) {
            $result['opcache']['status'] = 'KO';
            $result['opcache']['msg'] = 'Il semblerait que opcache soit Off';
        } else {
            $result['opcache']['status'] = 'OK';
            $result['opcache']['msg'] = '';
        }

        return $result;
    }

    public function checkPhpversion()
    {
        $result = [];
        $result['globalStatus'] = 'OK';
        $result['function'] = 'Php';

        $result['specificStatuses'] =
            [
                [
                    'specificStatus' => [
                        'function' => 'phpVersion',
                        'status' => 'OK',
                        'value' => phpversion(),
                        'message' => 'PHP V'.phpversion(),
                    ],
                ],
                [
                    'specificStatus' => [
                        'function' => 'phpIniPath',
                        'status' => 'OK',
                        'value' => php_ini_loaded_file(),
                        'message' => 'Chemin vers le php.ini:'.php_ini_loaded_file(),
                    ],
                ],
            ];

        // Check installed packages
        $packages = ['fpm', 'cli', 'dev', 'common', 'bcmath', 'curl', 'gd', 'ldap', 'mbstring',
            'mysql', 'soap', 'xml', 'xsl', 'zip', ];
        $extraPackages = ['pear', 'apcu', 'msgpack'];

        preg_match("#^\d.\d#", PHP_VERSION, $match);
        $version = $match[0];

        foreach ($packages as $package) {
            $packageName = 'php'.$version.'-'.$package;
            $cmd = exec('dpkg --get-selections |grep '.$packageName);
            $result = $this->getPackageInformations($cmd, $packageName, $result);
        }

        foreach ($extraPackages as $package) {
            $packageName = 'php-'.$package;
            $cmd = exec('dpkg --get-selections |grep '.$packageName);
            $result = $this->getPackageInformations($cmd, $packageName, $result);
        }

        // check configuration
        $configurations = [
            'maxInputTime' => 'max_input_time',
            'maxExecutionTime' => 'max_execution_time',
            'memoryLimit' => 'memory_limit',
            'fileUploads' => 'file_uploads',
            'postMaxSize' => 'post_max_size',
            'uploadMaxFilesize' => 'upload_max_filesize',
            'maxFileUploads' => 'max_file_uploads',
            'dateTimezone' => 'date.timezone',
            'opcacheEnableCli' => 'opcache.enable_cli',
            'opcacheEnable' => 'opcache.enable',
        ];

        foreach ($configurations as $fct => $configName) {
            $configValue = ini_get($configName);
            if (false !== $configValue) {
                $this->setSpecificStatuses($result, $fct, 'OK', $configValue, $configName.' = '.$configValue);
            } else {
                $result['globalStatus'] = 'KO';
                $result['messages'][] = [
                    'message' => 'configuration '.$configName.' est inexistante',
                ];
                $this->setSpecificStatuses(
                    $result,
                    $fct,
                    'KO',
                    '0',
                    'configuration '.$configName.' est inexistante'
                );
            }
        }

        return $result;
    }

    /**
     *  Return informations of installed packages.
     *
     * @param $cmd
     * @param $packageName
     * @param $result
     *
     * @return mixed
     */
    public function getPackageInformations($cmd, $packageName, $result)
    {
        if (!empty($cmd)) {
            $this->setSpecificStatuses($result, $packageName, 'OK', '1', $packageName.' est installé');
        } else {
            $result['globalStatus'] = 'KO';
            $result['messages'][] = [
                'message' => $packageName.' est manquant',
            ];

            $this->setSpecificStatuses($result, $packageName, 'KO', '0', $packageName.' est manquant');
        }

        return $result;
    }

    public function checkSsl()
    {
        $result = [];
        $url = $this->container->getParameter('PF_URL_REFERENCE');
        $connectTimeout = $this->container->getParameter('CURLOPT_CONNECTTIMEOUT_MS');
        $timeout = $this->container->getParameter('CURLOPT_TIMEOUT_MS');

        $httpCode = $this->connectToInterface($url, true, $connectTimeout, $timeout);

        $result['globalStatus'] = 'OK';
        $result['function'] = 'SSL';

        if (301 === $httpCode || 302 === $httpCode) {
            $this->setSpecificStatuses($result, 'SSL', 'OK', '', 'Certificat OK. URL: '.$url);
        } else {
            $result['globalStatus'] = 'KO';
            $result['messages'][] = [
                'message' => 'Certificat KO. URL: '.$url,
            ];
            $this->setSpecificStatuses($result, 'SSL', 'KO', '', 'Certificat KO. URL: '.$url);
        }

        return $result;
    }

    public function convert($size)
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];

        return @round($size / 1024 ** ($i = floor(log($size, 1024))), 2).' '.$unit[$i];
    }

    public function getServerMemoryUsage($getPercentage = true)
    {
        $memoryTotal = null;
        $memoryFree = null;

        if (stristr(PHP_OS, 'win')) {
            // Get total physical memory (this is in bytes)
            $cmd = 'wmic ComputerSystem get TotalPhysicalMemory';
            @exec($cmd, $outputTotalPhysicalMemory);

            // Get free physical memory (this is in kibibytes!)
            $cmd = 'wmic OS get FreePhysicalMemory';
            @exec($cmd, $outputFreePhysicalMemory);

            if ($outputTotalPhysicalMemory && $outputFreePhysicalMemory) {
                // Find total value
                foreach ($outputTotalPhysicalMemory as $line) {
                    if ($line && preg_match('/^[0-9]+$/', (string) $line)) {
                        $memoryTotal = $line;
                        break;
                    }
                }

                // Find free value
                foreach ($outputFreePhysicalMemory as $line) {
                    if ($line && preg_match('/^[0-9]+$/', (string) $line)) {
                        $memoryFree = $line;
                        $memoryFree *= 1024;  // convert from kibibytes to bytes
                        break;
                    }
                }
            }
        } else {
            if (is_readable('/proc/meminfo')) {
                $stats = @file_get_contents('/proc/meminfo');

                if (false !== $stats) {
                    // Separate lines
                    $stats = str_replace(["\r\n", "\n\r", "\r"], "\n", $stats);
                    $stats = explode("\n", $stats);

                    // Separate values and find correct lines for total and free mem
                    foreach ($stats as $statLine) {
                        $statLineData = explode(':', trim($statLine));

                        // Total memory
                        if (2 == count($statLineData) && 'MemTotal' == trim($statLineData[0])) {
                            $memoryTotal = trim($statLineData[1]);
                            $memoryTotal = explode(' ', $memoryTotal);
                            $memoryTotal = $memoryTotal[0];
                            $memoryTotal *= 1024;  // convert from kibibytes to bytes
                        }

                        // Free memory
                        if (2 == count($statLineData) && 'MemFree' == trim($statLineData[0])) {
                            $memoryFree = trim($statLineData[1]);
                            $memoryFree = explode(' ', $memoryFree);
                            $memoryFree = $memoryFree[0];
                            $memoryFree *= 1024;  // convert from kibibytes to bytes
                        }
                    }
                }
            }
        }

        if (is_null($memoryTotal) || is_null($memoryFree)) {
            return null;
        } else {
            if ($getPercentage) {
                return 100 - ($memoryFree * 100 / $memoryTotal);
            } else {
                return [
                    'total' => $memoryTotal,
                    'free' => $memoryFree,
                ];
            }
        }
    }

    public function getNiceFileSize($bytes, $binaryPrefix = true)
    {
        if ($binaryPrefix) {
            $unit = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
            if (0 == $bytes) {
                return '0 '.$unit[0];
            }

            return @round($bytes / 1024 ** ($i = floor(log($bytes, 1024))), 2)
                .' '.($unit[$i] ?? 'B');
        } else {
            $unit = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
            if (0 == $bytes) {
                return '0 '.$unit[0];
            }

            return @round($bytes / 1000 ** ($i = floor(log($bytes, 1000))), 2)
                .' '.($unit[$i] ?? 'B');
        }
    }
}
