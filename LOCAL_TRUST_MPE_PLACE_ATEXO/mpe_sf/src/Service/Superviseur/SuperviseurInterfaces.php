<?php

namespace App\Service\Superviseur;

use App\Service\AtexoConfiguration;
use App\Entity\Enveloppe;
use App\Entity\Offre;
use Psr\Container\ContainerInterface;

class SuperviseurInterfaces extends SuperviseurAbstract
{
    public array $specificStatus =
        [
            'CryptoInterface',
            'SignatureInterface',
            'Sgmap',
            'SgmapBaseCentrale',
            'Chorus',
            'Atlas',
            'Arcade',
            'Referentiels',
            'Sub',
        ];
    public string $nameService = 'interface';
    protected $em;

    public function __construct(protected ContainerInterface $container)
    {
        $this->em = $this->container->get('doctrine')->getManager();
    }

    /**
     * Vérifier la connexion vers l'interface Signature.
     *
     * @return mixed
     */
    public function checkSignatureInterface()
    {
        $url = $this->container->getParameter('URL_CRYPTO_OUTIL_TEST');

        return $this->checkInterface($url, 'Signature', false, 60);
    }

    /**
     * Vérifier la connexion vers l'interface Crypto.
     *
     * @return mixed
     */
    public function checkCryptoInterface()
    {
        $result = [];
        $url = $this->container->getParameter('URL_CRYPTO_OUTIL_TEST');
        $connectTimeout = $this->container->getParameter('CURLOPT_CONNECTTIMEOUT_MS');
        $timeout = $this->container->getParameter('CURLOPT_TIMEOUT_MS');
        $httpCode = $this->connectToInterface($url, false, $connectTimeout, $timeout);
        $result['globalStatus'] = 'OK';
        $result['function'] = 'Crypto';

        if (200 === $httpCode) {
            $statutAttenteChiffrement = $this->container->getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT');
            $statutEncoursChiffrement = $this->container->getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS');
            $em = $this->container->get('doctrine')->getManager();
            $delaiValidite = $this->container->getParameter('delai_validite');

            // récuperer offres en attente ou en cours de chiffrement
            $nombresOffresEnAttente = $em->getRepository(Offre::class)
                ->getOffresEnAttenteChiffrement($statutAttenteChiffrement, $statutEncoursChiffrement, $delaiValidite);
            $this->setSpecificStatuses(
                $result,
                'chiffrementsEnAttente',
                'WARNING',
                (int) $nombresOffresEnAttente,
                $nombresOffresEnAttente . ' chiffrements en attente'
            );

            // récupérer les offres en  cours de déchiffrement
            $statutEnCoursDechiffrement = $this->container->getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS');
            $nbrOffresEnCoursDechiffrement = $em->getRepository(Offre::class)
                ->getOffresEnCoursDechiffrement($statutEnCoursDechiffrement, $delaiValidite);

            // recupérer les enveloppes en cours de dechiffrement
            $nbrEnveloppesEnCoursDechiffrement = $em->getRepository(Enveloppe::class)
                ->getEnveloppesEnCoursDechiffrement($statutEnCoursDechiffrement, $delaiValidite);
            $nbrEnAttenteDechiffrement = $nbrOffresEnCoursDechiffrement + $nbrEnveloppesEnCoursDechiffrement;
            $this->setSpecificStatuses(
                $result,
                'dechiffrementsEnAttente',
                'WARNING',
                $nbrEnAttenteDechiffrement,
                $nbrEnAttenteDechiffrement . ' déchiffrements en attente'
            );
        } else {
            $result['globalStatus'] = 'KO';
            $result['messages'][] = [
                'message' => 'Impossible de se connecter au serveur Crypto',
            ];
        }

        return $result;
    }

    /**
     * Vérifier la connexion vers l'interface SGMAP.
     *
     * @return mixed
     */
    public function checkSgmap()
    {
        $result = [];
        if ($this->container->get(AtexoConfiguration::class)->hasConfigPlateforme('getSynchronisationSgmap')) {
            $url = $this->container->getParameter('URL_API_GOUV_ENTREPRISE');
            $result = $this->checkInterface($url, 'API_GOUV_ENTREPRISE', false, 60);
        }

        return $result;
    }

    /**
     * Vérifier la connexion vers l'interface Chorus.
     *
     * @return mixed
     */
    public function checkChorus()
    {
        $result = [];
        if ($this->hasConfigOrg('interfaceChorusPmi')) {
            $url = $this->container->getParameter('URL_ACCES_CHORUS');
            $result = $this->checkInterface($url, 'Chorus', false, 60);
        }

        return $result;
    }

    /**
     * Vérifier la connexion vers l'interface Arcade.
     *
     * @return mixed
     */
    public function checkArcade()
    {
        $result = [];
        if ($this->hasConfigOrg('interfaceArchiveArcadePmi')) {
            $url = $this->container->getParameter('URL_RECEPTION_ARCADE');
            $result = $this->checkInterface($url, 'Arcade', false, 60);
        }

        return $result;
    }

    /**
     * Vérifier la connexion vers l'interface Atlas.
     *
     * @return mixed
     */
    public function checkAtlas()
    {
        $result = [];
        if ($this->hasConfigOrg('interfaceArchiveArcadePmi')) {
            $url = $this->container->getParameter('URL_ARCHIVAGE_EXTERNE_ENVOI');
            $result = $this->checkInterface($url, 'Atlas', false, 60);
        }

        return $result;
    }

    /**
     * Vérifier la connexion vers l'interface Refrentiels.
     *
     * @return mixed
     */
    public function checkReferentiels()
    {
        $urlReference = $this->container->getParameter('PF_URL_REFERENCE');
        $urlReferentiel = $this->container->getParameter('URL_REFERENTIEL');
        $cheminFichierConfig = $this->container->getParameter('PATH_FILE_CPV_CONFIG');

        // on contruit une fake URL pour tester la connexion
        $url = $urlReference . $urlReferentiel .
            'clef=ll&locale=fr&cheminFichierConfigXML=' . $cheminFichierConfig . '&urlBase=ff';

        return $this->checkInterface($url, 'Referentiels', false, 60);
    }

    /**
     * Vérifier la connexion vers l'interface SUB.
     *
     * @return mixed
     */
    public function checkSub()
    {
        $result = [];
        if ($this->container->get(AtexoConfiguration::class)->hasConfigPlateforme('interfaceModuleSub')) {
            $urlSub = $this->container->getParameter('URL_SUB');
            $result['globalStatus'] = 'OK';
            $result['function'] = 'SUB';

            if ($urlSub) {
                $url = $urlSub . 'sub/login-tiers.sub';

                return $this->checkInterface($url, 'SUB', false, 60);
            } else {
                // SUB non configuré
                $result['globalStatus'] = 'KO';
                $result['messages'][] = [
                    'message' => 'SUB n\'est pas configuré',
                ];
            }
        }

        return $result;
    }
}
