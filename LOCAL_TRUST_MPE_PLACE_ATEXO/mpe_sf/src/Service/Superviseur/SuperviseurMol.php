<?php

namespace App\Service\Superviseur;

use App\Service\AtexoConfiguration;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

class SuperviseurMol extends SuperviseurAbstract
{
    public array $specificStatus = ['MolInterface'];
    public string $nameService = 'interface';

    public function __construct(protected ContainerInterface $container, protected ?EntityManagerInterface $em = null)
    {
    }

    /**
     * @return mixed
     */
    public function checkMolInterface()
    {
        $result = [];
        $confPf = $this->container->get(AtexoConfiguration::class);
        if ($confPf->hasConfigPlateforme('publiciteMarchesEnLigne')
            &&
            $confPf->hasConfigPlateforme('publiciteFormatXml')
        ) {
            $connectTimeout = $this->container->getParameter('CURLOPT_CONNECTTIMEOUT_MS');
            $timeout = $this->container->getParameter('CURLOPT_TIMEOUT_MS');

            $url = $this->container->getParameter('mol_adresse');
            $httpCode = $this->connectToInterface($url, false, $connectTimeout, $timeout);

            $urlConcentrateur = $this->container->getParameter('URL_CONCENTRATEUR_WS');
            $httpCodeConcentrateur = $this->connectToInterface($urlConcentrateur, false, $connectTimeout, $timeout);

            $result['globalStatus'] = 'OK';
            $result['function'] = 'MOL';

            $annonceEnAttente = $this->getAnnoncesEnAttente();

            if (200 === $httpCode && 200 === $httpCodeConcentrateur) {
                $this->setSpecificStatuses($result, 'curl', 'OK', '', 'Connexion OK');
            } else {
                $result['globalStatus'] = 'KO';
                $result['messages'][] = [
                    'message' => 'Impossible de se connecter au MOL',
                ];
            }
            $this->setSpecificStatuses(
                $result,
                'publicationsEnAttente',
                'WARNING',
                $annonceEnAttente,
                'Nombre de publicité en attente de publication: '.$annonceEnAttente
            );
        }

        return $result;
    }

    /**
     * Cette fonction récupère le nombre d'annonces en attente.
     *
     * @TODO cette fonction devra être refaite dès qu'on aura ajouté l'entité AnnonceBoamp
     *
     * @return mixed
     * @throws DBALException
     */
    public function getAnnoncesEnAttente()
    {
        $connection = $this->em->getConnection();
        $statement = $connection->prepare('select count(*) from AnnonceBoamp where statut_destinataire != :statut ');
        $statement->bindValue('statut', 'publié');
        $results = $statement->executeQuery()->fetchFirstColumn();

        return $results[0];
    }
}
