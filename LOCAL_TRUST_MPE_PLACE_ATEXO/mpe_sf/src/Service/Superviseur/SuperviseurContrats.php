<?php

namespace App\Service\Superviseur;

use App\Service\AtexoConfiguration;
use DateTime;
use App\Entity\ContratTitulaire;
use Psr\Container\ContainerInterface;

class SuperviseurContrats extends SuperviseurAbstract
{
    public array $specificStatus = ['Contrats'];
    public string $nameService = 'interface';

    public function __construct(protected ContainerInterface $container)
    {
    }

    public function checkContrats()
    {
        $result = [];
        if ($this->container->get(AtexoConfiguration::class)->hasConfigPlateforme('InterfaceDume')) {
            $em = $this->container->get('doctrine')->getManager();
            $dateNotifMin = new DateTime('now');
            $dateNotifMin->modify('-1 year');
            $dateNotifMax = new DateTime('now');

            // Get non published contracts
            $contrats = $em->getRepository(ContratTitulaire::class)->getContracts(
                $dateNotifMin,
                $dateNotifMax,
                null,
                null,
                null,
                0,
                null,
                null
            );
            $nbrContrats = iterator_count($contrats);

            $result['globalStatus'] = 'OK';
            $result['function'] = 'contratsStatutSnAttente';
            $result['messages'][] = [
                'message' => 'Nombre de contrats en attente de publication au SN: '.$nbrContrats,
            ];
            $this->setSpecificStatuses(
                $result,
                'enAttentePublicationSN',
                'Warning',
                $nbrContrats,
                'Nombre de contrats en attente de publication au SN: '.$nbrContrats
            );

            $url = $this->container->getParameter('URL_PF_DUME_API');
            $connectTimeout = $this->container->getParameter('CURLOPT_CONNECTTIMEOUT_MS');
            $timeout = $this->container->getParameter('CURLOPT_TIMEOUT_MS');
            $httpCode = $this->connectToInterface($url, false, $connectTimeout, $timeout);

            if (200 === $httpCode) {
                $this->setSpecificStatuses($result, 'ping', 'OK', 1, 'Connexion OK');
            } else {
                $result['globalStatus'] = 'KO';
                $this->setSpecificStatuses($result, 'ping', 'KO', 0, 'Connexion Ko');
                $result['messages'][] = [
                    'message' => 'Impossible de se connecter au LT DUME',
                ];
            }
        }

        return $result;
    }
}
