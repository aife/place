<?php

namespace App\Service\Superviseur;

use ReflectionClass;
use InvalidArgumentException;
use Symfony\Component\Process\Process;

class SuperviseurFacade
{
    protected array $services;

    public function __construct(
        protected SuperviseurNas $nas,
        private readonly SuperviseurDisk $disk,
        private readonly SuperviseurInterfaces $interfaces,
        private readonly SuperviseurOpenOffice $openOffice,
        private readonly SuperviseurPhp $php,
        private readonly SuperviseurContrats $contrats,
        private readonly SuperviseurMol $mol,
        private readonly SuperviseurDume $dume
    ) {
        $this->services = func_get_args();
    }

    public function getResult($maxParallelProcesses, $pollingInterval)
    {
        // Utile pour ajouter les autres fonctionnalités tels que service, interface
        $globalResult = [
            'services' => [],
            'interfaces' => [],
        ];
        $i = 0;
        $runningProcesses = [];
        $directory = getcwd();

        if (str_ends_with($directory, '/web')) {
            $command = 'php ../bin/console mpe:url-de-vie:services ';
        } else {
            $command = 'php bin/console mpe:url-de-vie:services ';
        }

        foreach ($this->services as $id => $service) {
            $class = new ReflectionClass($service);
            $className = $class->getShortName();
            $processCmd = $command . $className;
            ${'process' . $i} = new Process($processCmd);
            $runningProcesses[] = ${'process' . $i};
            ++$i;
        }

        $this->runParallels($runningProcesses, $maxParallelProcesses, $globalResult, $pollingInterval);

        return $globalResult;
    }

    public function runParallels(array $processes, $maxParallel, &$globalResult, $poll = 1000)
    {
        $this->validateProcesses($processes);
        $processesQueue = $processes;
        $maxParallel = min(abs($maxParallel), count($processesQueue));
        $currentProcesses = array_splice($processesQueue, 0, $maxParallel);

        foreach ($currentProcesses as $process) {
            $process->mustRun();
        }

        do {
            usleep($poll);
            foreach ($currentProcesses as $index => $process) {
                if (!$process->isRunning()) {
                    unset($currentProcesses[$index]);
                    if (count($processesQueue) > 0) {
                        $nextProcess = array_shift($processesQueue);
                        $nextProcess->start();
                        $currentProcesses[] = $nextProcess;
                    }
                }

                if ('terminated' === $process->getStatus()) {
                    $resultService = json_decode($process->getOutput(), JSON_OBJECT_AS_ARRAY, 512, JSON_THROW_ON_ERROR);
                    if (isset($resultService['service'])) {
                        $globalResult['services'][]['service'] = $resultService['service'];
                    }

                    if (isset($resultService['interface'])) {
                        foreach ($resultService['interface'] as $interface) {
                            $globalResult['interfaces'][]['interface'] = $interface;
                        }
                    }
                }
            }
        } while (count($processesQueue) > 0 || count($currentProcesses) > 0);
    }

    /**
     * @param Process[] $processes
     */
    protected function validateProcesses(array $processes)
    {
        if (empty($processes)) {
            throw new InvalidArgumentException('Impossible de lancer 0 commandes');
        }
        foreach ($processes as $process) {
            if (!($process instanceof Process)) {
                throw new InvalidArgumentException('Le Process dans le tableau doit être une instance de Symfony Process');
            }
        }
    }
}
