<?php

namespace App\Service\Superviseur;

use Psr\Container\ContainerInterface;

class SuperviseurDisk extends SuperviseurAbstract
{
    public array $specificStatus = ['DirectoryAccess'];
    public string $nameService = 'service';

    public function __construct(protected ContainerInterface $container)
    {
    }

    public function checkDirectoryAccess()
    {
        $result = [];
        $listeDir = ['COMMON_TMP', 'LOG_DIR', 'COMMON_TMP_SHARED', 'FICHIERS_ARCHIVE_DIR'];

        if (!empty($this->container->getParameter('DOCUMENT_ROOT_DIR_MULTIPLE'))) {
            $mnt = explode(';', (string) $this->container->getParameter('DOCUMENT_ROOT_DIR_MULTIPLE'));
            $listeDir = array_merge($listeDir, $mnt);
        }

        $fileName = 'page.txt';
        $result['globalStatus'] = 'OK';
        $result['function'] = 'disk';

        foreach ($listeDir as $id => $dir) {
            $pathDir = $this->container->getParameter($dir);
            $filePath = $pathDir.$fileName;
            $file = @file_put_contents($filePath, $this->randomString(40000));

            $result['specificStatuses'][$id] =
                [
                    'specificStatus' => [
                    'function' => 'droitEcriture_'.$dir,
                    'value' => '',
                    ],
                ];

            if (!is_writable($pathDir) && false === $file) {
                $result['globalStatus'] = 'KO';
                $result['specificStatuses'][$id]['specificStatus']['status'] = 'KO';
                $result['specificStatuses'][$id]['specificStatus']['message'] = 'Le répertoire : '.$pathDir.' - ('.substr(sprintf('%o', fileperms($pathDir)), -4).") n'est pas accessible en écriture";
                $result['messages'][] = [
                    'message' => $result['specificStatuses'][$id]['specificStatus']['message'],
                ];
            } else {
                $result['specificStatuses'][$id]['specificStatus']['status'] = 'OK';
                $result['specificStatuses'][$id]['specificStatus']['message'] = 'Le répertoire : '.$pathDir.' - ('.substr(sprintf('%o', fileperms($pathDir)), -4).') est accessible en écriture';
            }

            if ($file) {
                unlink($filePath);
            }
        }

        return $result;
    }
}
