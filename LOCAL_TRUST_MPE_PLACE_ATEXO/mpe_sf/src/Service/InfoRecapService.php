<?php

namespace App\Service;

use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\EnveloppeFichier;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Service\Dume\DumeService;
use App\Utils\Encryption;
use Application\Service\Atexo\Atexo_Util;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class InfoRecapService
{
    public final const PHASE_DUME = 'dume';

    public final const CHOICE_ONLINE = 'online';

    private $user;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoConfiguration $configuration,
        private readonly GeolocalisationService $geolocalisationService,
        TokenStorageInterface $tokenStorage,
        private readonly DumeService $dumeService,
        private readonly Environment $templating,
        private readonly Encryption $encryption,
        private $parameters = [],
    ) {
        $this->user = $tokenStorage->getToken()->getUser();
    }

    /**
     * récupération du lien des détails des lots.
     */
    public function getLienDetailLots(Consultation $consultation): string
    {
        return $this->parameters['URL_MPE'] .
            '?page=' . Atexo_Util::getTypeUserCalledForPradoPages() . '.PopUpDetailLots&orgAccronyme=' .
            $consultation->getOrganisme() .
            '&id=' .
            $consultation->getId();
    }

    /**$récupération de l'url de reception
     * @param $idPdfEchangeAccuse
     * @param $organismeAcronyme
     * @return string
     */
    public function getUrlReception($idPdfEchangeAccuse, $organismeAcronyme): string
    {
        return $this->parameters['PF_URL_MPE']
            . '?page=Entreprise.DownloadRecapReponse&idFile='
            . $idPdfEchangeAccuse
            . '&orgAcronyme='
            . $organismeAcronyme
            . '&callFrom=entreprise';
    }

    /**
     * Récupération du récapiculatif de l'offre pour les contenus transmis.
     *
     * @param $lieuxExecutions
     * @param $inscritDepotOffre
     * @param $entreprise
     *
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getOffreInfosRecap(
        Offre $offre,
        Consultation $consultation,
        $lieuxExecutions,
        $inscritDepotOffre,
        $entreprise
    ): string {
        $listeFichiers = $this->em
            ->getRepository(EnveloppeFichier::class)
            ->getListeFichiersDepot(
                $offre,
                $this->parameters['type_enveloppe']
            );
        $dumeNumero = null;
        $arrayDumeNumero = [];
        if ($offre->getStatutOffres() != $this->parameters['STATUT_ENV_BROUILLON']) {
            $candidature = $this->em
                ->getRepository(TCandidature::class)
                ->findOneBy([
                    'idOffre' => $offre->getId(),
                ]);
            if (null !== $candidature) {
                $dumeNumero = $this->dumeService->getDumeNumeroFromIdDumeContexte($candidature->getIdDumeContexte());

                if (null != $dumeNumero && null != $dumeNumero->getBlobId()) {
                    $arrayDumeNumero = [
                        'dumeNumeroAndBlob' => (
                            self::PHASE_DUME == $candidature->getTypeCandidature() &&
                            self::CHOICE_ONLINE == $candidature->getTypeCandidatureDume()
                        ),
                    ];
                }
            }

            if (
                null != $dumeNumero && $candidature &&
                'dume' == $candidature->getTypeCandidature() &&
                'online' == $candidature->getTypeCandidatureDume()
            ) {
                $arrayDumeNumero['numeroSN'] = $dumeNumero->getNumeroDumeNational();
                $arrayDumeNumero['filePdf'] = $dumeNumero->getBlobId();
                $arrayDumeNumero['fileXml'] = $dumeNumero->getBlobIdXml();
            }

            $entrepriseId = $this->user->getEntrepriseId();
            if ($this->configuration->hasConfigPlateforme('groupement') && $entrepriseId) {
                $listeDumeGroupement = $this->dumeService->getDumeNumeroFromGroupement($offre->getId(), $entrepriseId);
                if ($listeDumeGroupement) {
                    $arrayDumeNumero['dumeGroupements'] = $listeDumeGroupement;
                }
            }
            $arrayDumeNumero['candidature'] = $candidature;
        }

        $idPdfEchangeAccuse = $offre->getIdPdfEchangeAccuse()
            ? $this->encryption->cryptId($offre->getIdPdfEchangeAccuse())
            : null ;
        $info = [
            'consultation' => $consultation,
            'offre' => $offre,
            'lieuxExecutions' => $lieuxExecutions,
            'user' => $inscritDepotOffre,
            'entreprise' => $entreprise,
            'listeFichiers' => $listeFichiers,
            'lienDetailLots' => ($consultation->getAlloti()) ? $this->getLienDetailLots($consultation) : '',
            'isDetailConsultation' => 1,
            'reception' => $this->getUrlReception($idPdfEchangeAccuse, $consultation->getAcronymeOrg()),
        ];

        $info = array_merge($info, $arrayDumeNumero);

        return $this->templating->render('consultation/consultation-depot-confirmation-infos-recap.html.twig', $info);
    }

    /**
     * Récupération du bloc récapiculatif DUME pour les contenus transmis.
     *
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getDumeInfosRecap(Consultation $consultation, Inscrit $inscrit, Entreprise $entreprise): string
    {
        $htmlContent = '';
        $candidatures = $this->em
            ->getRepository(TCandidature::class)
            ->getCandidaturesByRole(
                $consultation->getId(),
                $consultation->getOrganisme()->getAcronyme(),
                $inscrit->getId(),
                $entreprise->getId(),
                2,
                $this->parameters['STATUT_DUME_CONTEXTE_VALIDE'],
                $this->parameters['TYPE_CANDIDATUE_DUME'],
                $this->parameters['TYPE_CANDIDATUE_DUME_ONLINE']
            );

        if (!empty($candidatures) && (is_countable($candidatures) ? count($candidatures) : 0)) {
            $infoCandidaturesCoTraitant = [
                'candidaturesCoTraitant' => $candidatures,
                'entreprise' => $entreprise,
                'user' => $inscrit,
            ];

            $htmlContent = $this->templating->render(
                'consultation/consultation-depot-dume-confirmation-infos-recap.html.twig',
                $infoCandidaturesCoTraitant
            );
        }

        return $htmlContent;
    }

    /**
     *  Récupération des contenus transmis par l'entreprise lors de ces dépots pour une consultation
     * se trouve en bas de la page du détail de la consultation.
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getDepotContenusTransmis(Consultation $consultation): string
    {
        $entreprise = $this->user->getEntreprise();
        $htmlContent = '';
        $organismeAcronyme = $consultation->getOrganisme()->getAcronyme();

        $htmlContent .= $this->getDumeInfosRecap($consultation, $this->user, $entreprise);

        $lieuxExecutions = $this->geolocalisationService->getLieuxExecution($consultation);

        //L'offre pour récupérer toutes les informations du contenus transmis
        $offres = $this->em->getRepository(Offre::class)
            ->getOffresForContenusTransmis(
                $consultation->getId(),
                $organismeAcronyme,
                $this->user->getId(),
                $entreprise->getId(),
                $this->parameters['STATUT_ENV_BROUILLON']
            );

        if (is_array($offres) && count($offres) > 0) {
            /**
             * @var Offre $offre
             */
            foreach ($offres as $offre) {
                $htmlContent .= $this->getOffreInfosRecap(
                    $offre,
                    $consultation,
                    $lieuxExecutions,
                    $this->user,
                    $entreprise
                );
            }
        }

        return $htmlContent;
    }
}
