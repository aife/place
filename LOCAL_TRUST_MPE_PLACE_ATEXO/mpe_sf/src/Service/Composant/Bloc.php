<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Composant;


use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Bloc
{
    /**
     * Footer constructor.
     * @param Environment $twig
     * @param ParameterBagInterface $parameterBag
     * @param TranslatorInterface $translator
     * @param SessionInterface $session
     * @param LoggerInterface $logger
     */
    public function __construct(private readonly Environment $twig, private readonly ParameterBagInterface $parameterBag, private readonly TranslatorInterface $translator, private readonly SessionInterface $session, private readonly LoggerInterface $logger)
    {
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getTemplate(string $template): string
    {
        $locale = $this->session->get('_locale');
        if ($locale === null) {
            $locale = $this->parameterBag->get('locale');
        }
        $this->translator->setLocale($locale);

        return $this->twig->render($template, [
            'lang' => $locale
        ]);
    }
}
