<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Footer;

use App\Exception\CantReadFileException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Footer
{
    public final const ENTREPRISE = 'entreprise';
    public final const AGENT = 'agent';

    /**
     * Footer constructor.
     */
    public function __construct(
        private readonly Environment $twig,
        private readonly ParameterBagInterface $parameterBag,
        private readonly TranslatorInterface $translator,
        private readonly SessionInterface $session,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getTemplate(string $productName, string $calledFrom = self::ENTREPRISE): string
    {
        $version = '';
        $calledFromEntreprise = $calledFrom === self::ENTREPRISE;
        $template = $calledFromEntreprise ? 'entreprise/menu/footer.html.twig' : 'footer/footer-agent.html.twig';

        if ($this->parameterBag->get('SPECIFIQUE_PLACE')) {
            $template = 'ecrans/footer-place.html.twig';
            $version = $this->getVersion();
        }
        $locale = $this->session->get('_locale');
        if ($locale === null) {
            $locale = $this->parameterBag->get('locale');
        }
        $this->translator->setLocale($locale);

        return $this->twig->render($template, [
            'version' => $version,
            'msg_accessibility' => $this->getMsgAccessibility($calledFrom),
            'called_from' => $calledFrom,
            'product_name' => $productName,
            'lang' => $locale
        ]);
    }

    public function getMsgAccessibility(string $parameter): string
    {
        $msg = $this->translator->trans('TOTALEMENT_CONFORME');

        if ($parameter == self::ENTREPRISE) {
            $msg = $this->translator->trans('PARTIELLEMENT_CONFORME') ;
        } elseif ($parameter == self::AGENT) {
            $msg = $this->translator->trans('NON_CONFORME') ;
        }

        return $msg;
    }

    private function getVersion(): string
    {
        $version = '';
        $fileName = '.version_footer';
        $filesystem = new Filesystem();
        $versionFilePath = $this->parameterBag->get('APP_BASE_ROOT_DIR') . $fileName;
        $file = true;
        if (!$filesystem->exists($versionFilePath)) {
            $this->logger->warning('File ' . $fileName . ' not found');
            $file = false;
        }

        if ($file) {
            $versionFileContent = file_get_contents($versionFilePath, true);

            if (false === $versionFileContent) {
                throw new CantReadFileException(sprintf('Impossible de lire le fichier : %s', $versionFilePath));
            }

            $contents = explode(PHP_EOL, $versionFileContent);

            foreach ($contents as $key => $line) {
                if (preg_match('#^version#', $line)) {
                    $values = explode('=', $line);
                    $version = $values[1] ?? '';

                    break;
                }
            }
        }

        return $version;
    }
}
