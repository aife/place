<?php

namespace App\Service\Footer;

use App\Service\ConfigurationOrganismeService;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Security\Core\Security;

class PrerequisTechnique
{
    private const MODE_APPLET = 'ModeApplet';

    public function __construct(private readonly ContainerBagInterface $parameters, private readonly Security $security, private readonly ConfigurationOrganismeService $confOrganismeService)
    {
    }

    public function getModeServeurCrypto(string $calledFrom): bool
    {
        $user = $this->security->getUser();

        $bool = true;
        if (
            !$this->parameters->get('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
            || (
                $calledFrom === 'agent'
                && $user
                && $this->confOrganismeService->moduleIsEnabled($user->getOrganisme(), self::MODE_APPLET)
            )
        ) {
            $bool = false;
        }

        return $bool;
    }
}
