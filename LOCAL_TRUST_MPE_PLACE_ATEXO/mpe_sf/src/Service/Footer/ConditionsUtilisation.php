<?php

namespace App\Service\Footer;

use Symfony\Contracts\Translation\TranslatorInterface;

class ConditionsUtilisation
{
    private readonly string $cguEntrepriseTrans;
    private readonly string $cguAgentTrans;

    public function __construct(private readonly TranslatorInterface $translator)
    {
        $this->cguEntrepriseTrans = $this->translator->trans('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE');
        $this->cguAgentTrans = $this->translator->trans('CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT');
    }

    /**
     * Permet de gerer la visibilite des CGU specifiques.
     */
    public function visibiliteBlocCguSpecifique(string $calledFrom): bool
    {
        return ('entreprise' == $calledFrom &&
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE' != $this->cguEntrepriseTrans)
                || ('agent' == $calledFrom &&
                'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' !=  $this->cguAgentTrans);
    }

    /**
     * Permet de recuperer le contenu specifique a afficher selon les cas.
     */
    public function getLibelleBlocCguSpecifique(string $calledFrom): string
    {
        if (
            'entreprise' == $calledFrom &&
            'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_ENTREPRISE' != $this->cguEntrepriseTrans
        ) {
            return $this->cguEntrepriseTrans;
        } elseif (
            'agent' == $calledFrom &&
            'CONTENU_CONDITIONS_UTILISATIONS_SPECIFIQUES_AGENT' !=  $this->cguAgentTrans
        ) {
            return $this->cguAgentTrans;
        }

        return '';
    }
}
