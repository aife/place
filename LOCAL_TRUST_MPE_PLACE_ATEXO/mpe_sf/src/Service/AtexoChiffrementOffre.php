<?php

namespace App\Service;

use App\Entity\CertificatChiffrement;
use App\Entity\Consultation;
use App\Entity\Offre;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\InfosHorodatage;
use AtexoCrypto\Exception\ServiceException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Gestion des chiffrements des offres.
 */
class AtexoChiffrementOffre
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $cryptoLogger,
        private readonly AtexoCrypto $crypto,
        private readonly EntityManagerInterface $em
    ) {
    }

    /**
     * Permet de chiffrer l'offre.
     */
    public function demanderChiffrementOffre(Offre $offre): bool
    {
        $result = false;
        $this->cryptoLogger->info("Le statut de l'offre  est : " . $offre->getStatutOffres());
        if (
            $offre->getStatutOffres() == $this->getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')
            || $offre->getStatutOffres() == $this->getParameter('STATUT_ENV_FERMETURE_EN_ATTENTE')
        ) {
            $result = $this->chiffrement($offre);
        } else {
            $this->cryptoLogger->info('Demande de chiffrement offre:  idOffre = ' . $offre->getId()
                . ' a un statut invalide : ( ' . $offre->getStatutOffres()
                . ') ( les offres autorisée sont les offres avec statut en attente 
                de chiffrement ou en attente de fermeture)');
        }

        return $result;
    }

    /**
     * Chiffrement et horodatage.
     */
    private function chiffrement(Offre $offre): bool
    {
        $idConsultation = null;
        $result = false;
        $infosHorodatage = null;
        try {
            $consultation = $this->em->getRepository(Consultation::class)->find($offre->getConsultationId());
            $idConsultation = $consultation->getId();
            $this->cryptoLogger->info(
                "Début de la récuperation des certificat non double de la consultation 
                dont : idConsultation = $idConsultation , organisme = "
                . $offre->getOrganisme()
            );
            $certificats = $this->em->getRepository(CertificatChiffrement::class)
                ->findCertificats($idConsultation, $consultation->getOrganisme());
            $consultation->setCertificatChiffrementsNonDouble($certificats);
            $this->cryptoLogger->info(
                "Fin de la récuperation des certificat non double de la consultation 
                dont : idConsultation = $idConsultation , organisme = "
                . $offre->getOrganisme()
            );
            $this->cryptoLogger->info("Debut demande de chiffrement offre: idConsultation = $idConsultation , idOffre = "
                . $offre->getId() . ' , organisme = ' . $offre->getOrganisme());

            if ($consultation->getChiffrementOffre()) {
                $this->cryptoLogger->info("mettre à jour le statut de l'offre à  : STATUT_ENV_CHIFFREMENT_EN_COURS");
                $offre->setStatutOffres($this->getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS'));
            } else {
                $this->cryptoLogger->info("mettre à jour le statut de l'offre à  : STATUT_ENV_FERMETURE_EN_COURS");
                $offre->setStatutOffres($this->getParameter('STATUT_ENV_FERMETURE_EN_COURS'));
            }

            $this->em->flush();

            $params = [
                'mpe_url_privee' => $this->getParameter('url_mpe_ws'),
                'serveur_crypto_url_public' => $this->getParameter('URL_CRYPTO'),
                'mpe_file_path' => $this->getParameter('BASE_ROOT_DIR')
                    . (new DateTime())->format('Y/m/d') . '/' . $consultation->getOrganisme() . '/files/',
                'mpe_login' => $this->getParameter('login_serveur_crypto'),
                'mpe_password' => $this->getParameter('mp_serveur_crypto'),
                'fonction' => $this->getParameter('FONCTION_CHIFFREMENT'),
            ];
            $infosAppelServeurCrypto = "idConsultation = $idConsultation " . PHP_EOL . 'idOffre = ' . $offre->getId()
                . PHP_EOL . 'organisme = ' . $offre->getOrganisme() . PHP_EOL
                . 'Parametres:' . var_export($params, true);
            if ($consultation->getChiffrementOffre()) {
                $this->cryptoLogger->info('consultation avec chiffrement OUI');
            } else {
                $this->cryptoLogger->info('consultation avec chiffrement NON');
            }
            $this->cryptoLogger->info('Appel de la methode de chiffrement avec les parametres : '
                . PHP_EOL . $infosAppelServeurCrypto);
            $infosHorodatage = $this->crypto->demandeChiffrement($consultation, $offre, $params);
            $this->cryptoLogger->info('Demande de chiffrement OK.' . PHP_EOL . 'Infos demande de chiffrement : '
                . var_export($infosHorodatage, true));
            //Mise a jour de l'offre
            $offre->setEtatChiffrement($this->getParameter('ETAT_CHIFFREMENT_OK'));
        } catch (ServiceException $e) {
            $erreur = '[code_erreur = ' . $e->errorCode . '] ' . PHP_EOL . ' [etat = ' . $e->etatObjet . '] ' . PHP_EOL
                . ' [message_erreur = ' . $e->message . '] ' . PHP_EOL
                . " [infos_appel_serveur_crypto = $infosAppelServeurCrypto]";
            $this->cryptoLogger->error("Erreur lors de la demande de chiffrement de l'offre : $erreur");
            //Mise a jour de l'offre
            $offre->setErreurChiffrement($erreur);
            switch ($e->errorCode) {
                case 'ERROR_REST':
                    $offre->setEtatChiffrement($this->getParameter('ETAT_CHIFFREMENT_KO_ERROR_REST'));
                    break;
                case 'ERROR_HORODATAGE':
                    $offre->setEtatChiffrement($this->getParameter('ETAT_CHIFFREMENT_KO_ERROR_HORODATAGE'));
                    break;
                case 'ERROR_JSON':
                    $offre->setEtatChiffrement($this->getParameter('ETAT_CHIFFREMENT_KO_ERROR_JSON'));
                    break;
                default:
            }
        } catch (Exception $e) {
            $erreur = '[Erreur = ' . $e->getMessage() . '] ';
            $this->cryptoLogger->error("Erreur lors de la demande de chiffrement de l'offre : $erreur "
                . PHP_EOL . ' [Trace = ' . $e->getTraceAsString() . ']');
            //Mise a jour de l'offre
            $offre->setErreurChiffrement($erreur);
            $offre->setEtatChiffrement($this->getParameter('ETAT_CHIFFREMENT_KO_ERROR_UNKNOWN'));
        }

        // Persist et code retour
        try {
            $this->em->flush();
            if ($infosHorodatage instanceof InfosHorodatage) {
                $result = true;
            }
            $this->cryptoLogger->info(
                "Fin demande de chiffrement offre: idConsultation = $idConsultation , idOffre = "
                . $offre->getId() . ' , organisme = ' . $offre->getOrganisme()
            );
        } catch (Exception $e) {
            $erreur = '[Erreur = ' . $e->getMessage() . '] ';
            $this->cryptoLogger->error("Erreur lors du persist de l'offre : $erreur "
                . PHP_EOL . ' [Trace = ' . $e->getTraceAsString() . ']');
        }

        return $result;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $parameter The parameter name
     *
     * @return mixed
     */
    private function getParameter(string $parameter)
    {
        return $this->parameterBag->get($parameter);
    }
}
