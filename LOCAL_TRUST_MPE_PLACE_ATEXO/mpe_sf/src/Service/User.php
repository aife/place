<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\Inscrit;
use Symfony\Component\Security\Core\Security;

class User
{

    /**
     * User constructor.
     */
    public function __construct(private readonly Security $security)
    {
    }

    /**
     * @return Agent|Inscrit|Administrateur|null
     */
    public function getUser()
    {
        return $this->security->getUser();
    }
}
