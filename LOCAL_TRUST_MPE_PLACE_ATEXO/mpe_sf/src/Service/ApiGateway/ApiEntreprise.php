<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\ApiGateway;

use App\Service\Entreprise\EntrepriseAPIService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ApiEntreprise
{
    final public const CONTEXT = 'context';
    final public const OBJECT = 'object';
    final public const PLATEFORME_CLIENT_SIRET = 'recipient';
    final public const CONTENT_TYPE_JSON = 'application/json';
    final public const NODE_RESPONSE_DATA = 'data';
    final public const NODE_LINKS = 'links';
    final public const NODE_META = 'meta';

    final public const TYPE_PERSONNE_MORALE = 'personne_morale';
    final public const TYPE_PERSONNE_PHYSIQUE = 'personne_physique';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function getRequestOptions(): array
    {
        return [
            'headers' => $this->getHeaderParams(),
            'query' => $this->getQueryParams()
        ];
    }

    public function getQueryParams(): array
    {
        return [
            self::CONTEXT => EntrepriseAPIService::REQUEST_MANDATORY_PARAMS[self::CONTEXT],
            self::OBJECT => EntrepriseAPIService::REQUEST_MANDATORY_PARAMS[self::OBJECT],
            self::PLATEFORME_CLIENT_SIRET => $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT')
        ];
    }

    public function getHeaderParams(): array
    {
        return [
            'Content-Type' => self::CONTENT_TYPE_JSON,
            'Authorization' => 'Bearer ' . $this->parameterBag->get('API_ENTREPRISE_TOKEN'),
        ];
    }

    public function mergeMultipleResponseData(array $responses): array
    {
        $mergedResult = [];
        foreach ($responses as $response) {
            foreach ($response as $key => $value) {
                $mergedResult[$key] = match ($key) {
                    self::NODE_RESPONSE_DATA,
                    self::NODE_LINKS,
                    self::NODE_META => array_merge($value, $mergedResult[$key] ?? []),
                    default => [],
                };
            }
        }

        return $mergedResult;
    }
}
