<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\ApiGateway;

use Exception;
use App\Service\Entreprise\EntrepriseAPIService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesSynchro
{
    final public const MSG_ERROR = "Echec lors de la synchronisation avec le WS Exercices via Api Gateway!";
    final public const MSG_SUCCESS = "Synchronisation avec le WS Exercices via Api Gateway réussie.";

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntrepriseAPIService $apiService,
        private readonly LoggerInterface $logger,
        private readonly HttpClientInterface $httpClient,
    ) {
    }

    public function synchroApiGatewayExercices(string $siren, string $nic): void
    {
        try {
            $siret = $siren . $nic;
            $url = str_replace('{siret}', $siret, $this->parameterBag->get('URL_API_ENTREPRISE_CHIFFRES_AFFAIRES'));

            $response = $this->httpClient->request(
                Request::METHOD_GET,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->parameterBag->get('API_ENTREPRISE_TOKEN'),
                    ],
                    'query' => $this->apiService->getWebServiceQueryParams()
                ]
            );

            if ($response->getStatusCode() != Response::HTTP_OK) {
                $this->logger->warning(self::MSG_ERROR);
            } else {
                $this->logger->info(self::MSG_SUCCESS);
            }
        } catch (Exception $e) {
            $this->logger->error(
                sprintf(self::MSG_ERROR . " %s", $e->getMessage())
            );
        }
    }
}
