<?php

/**
 * Class AtexoCrypto : classe de gestion de la crypto.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */

namespace App\Service;

use Psr\Container\ContainerInterface;

class AtexoCrypto
{
    /**
     * AtexoCrypto constructor.
     *
     * @param $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(private ContainerInterface $container, private readonly AtexoUtil $atexoUtil)
    {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }
}
