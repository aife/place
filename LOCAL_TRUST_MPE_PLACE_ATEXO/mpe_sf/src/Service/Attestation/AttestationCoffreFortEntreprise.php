<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Attestation;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\BlobFichier;
use App\Entity\Entreprise;
use App\Entity\Justificatifs;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class AttestationCoffreFortEntreprise
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly IriConverterInterface $iriConverter,
        private readonly LoggerInterface $logger
    ) {
    }

    public function getAttestations(Entreprise $entreprise): array
    {
        $this->logger->info(sprintf(
            "Début recherche justificatifs coffre fort de l'entreprise ayant pour siren %s",
            $entreprise->getSiren()
        ));

        $attestations = $this->getAttestationCoffreFort($entreprise);

        $this->logger->info(sprintf(
            "Fin recherche justificatifs coffre fort de l'entreprise ayant pour siren %s",
            $entreprise->getSiren()
        ));

        return $attestations;
    }

    private function getAttestationCoffreFort(Entreprise $entreprise): array
    {
        $justificatifs =
            $this->entityManager
                ->getRepository(Justificatifs::class)
                ->findBy(['idEntreprise' => $entreprise->getId()])
        ;

        $data = [];
        /** @var Justificatifs $justificatif */
        foreach ($justificatifs as $justificatif) {
            $blobFile = $this->entityManager->getRepository(BlobFichier::class)
                ->find($justificatif->getJustificatif())
            ;

            $documentCoffreFort = [
                'intituleJustificatif' => $justificatif->getIntituleJustificatif(),
                'nomJustificatif' => $justificatif->getNom(),
                'file' => $blobFile ? $this->iriConverter->getIriFromItem($blobFile) : ''
            ];

            if (!$blobFile) {
                $messageErreur = sprintf(
                    "Le fichier n'a pas été trouvé pour le document justificatif coffre-fort id=%s et nom=%s",
                    $justificatif->getId(),
                    $justificatif->getIntituleJustificatif()
                );
                $documentCoffreFort['errorMessage'] = $messageErreur;

                $this->logger->error($messageErreur);
            } else {
                $this->logger->info(sprintf(
                    "Le fichier a été trouvé pour le document justificatif coffre-fort id=%s et nom=%s",
                    $justificatif->getId(),
                    $justificatif->getIntituleJustificatif()
                ));
            }

            $data[] = $documentCoffreFort;
        }

        return $data;
    }
}
