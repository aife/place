<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Attestation;

use DateTime;
use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\BlobFichier;
use App\Entity\DerniersAppelsValidesWsSgmapDocuments;
use App\Entity\DocumentType;
use App\Entity\Entreprise;
use App\Entity\EntrepriseDocument;
use App\Entity\EntrepriseDocumentVersion;
use App\Service\AtexoUtil;
use App\Service\Blob\BlobFileService;
use App\Service\Entreprise\EntrepriseAPIService;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AttestationApiEntreprise
{
    private const VIGILANCE_URSSAF_CODE = 'attestation_sociale_vg';
    private const FISCALE_DGFIP_CODE = 'attestation_fiscale';

    private const DATETIME_FORMAT = 'Y-m-d H:i:s';

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly IriConverterInterface $iriConverter,
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $logger,
        private readonly EntrepriseAPIService $entrepriseAPIService,
        private readonly Security $security,
        private readonly BlobFileService $blobFileService,
        private readonly AtexoUtil $atexoUtil,
    ) {
    }

    #[ArrayShape([self::FISCALE_DGFIP_CODE => "array", self::VIGILANCE_URSSAF_CODE => "array"])]
    public function getAttestations(Entreprise $entreprise): array
    {
        $data = [];
        if (!$entreprise->getSiren()) {
            $sirenNotFound = sprintf(
                "Impossible de récupérer les documents fiscaux via l'api car l'entreprise id=%s n'a pas de SIREN",
                $entreprise->getId()
            );
            $this->logger->error($sirenNotFound);

            $data['errorMessage'] = $sirenNotFound;

            return $data;
        }

        return [
            self::FISCALE_DGFIP_CODE => $this->getAttestationFiscale($entreprise),
            self::VIGILANCE_URSSAF_CODE => $this->getAttestationVigilanceUrsaaf($entreprise)
        ];
    }

    private function getAttestationFiscale(Entreprise $entreprise): array
    {
        return $this->getDocumentByDocumentType($entreprise, $this->parameterBag->get('TYPE_ATTESTATION_FISCALE'));
    }

    private function getAttestationVigilanceUrsaaf(Entreprise $entreprise): array
    {
        return $this->getDocumentByDocumentType(
            $entreprise,
            $this->parameterBag->get('TYPE_ATTESTATION_SOCIALE_VIGILANCE')
        );
    }

    private function getDocumentByDocumentType(Entreprise $entreprise, string $typeDocument): array
    {
        $data = [];

        $this->logger->info(
            sprintf(
                "Debut récupération document pour l'entreprise %s ayant pour type de document : %s",
                $entreprise->getSiren(),
                $typeDocument,
            )
        );

        /** @var EntrepriseDocument $documentEntreprise */
        $documentEntreprise = $this->entityManager->getRepository(EntrepriseDocument::class)->findOneBy(
            [
                'idEntreprise' => $entreprise->getId(),
                'idTypeDocument' => $typeDocument,
            ]
        );

        /** @var DocumentType $documentType */
        $documentType = $this->entityManager->getRepository(DocumentType::class)->findOneBy(
            [
                'idDocumentType' => $typeDocument
            ]
        );

        if (!$documentEntreprise) {
            $documentEntreprise = $this->createNewDocumentIfNotExist(
                $entreprise,
                $documentType
            );
        }

        /** @var EntrepriseDocumentVersion $documentEntrepriseVersion */
        $documentEntrepriseVersion =
            $this->entityManager->getRepository(EntrepriseDocumentVersion::class)->findOneBy(
                ['idDocument' => $documentEntreprise->getIdDocument()],
                ['idVersionDocument' => 'desc']
            )
        ;

        $documentData = [
            'labelTypeDocument' => $documentType->getNomDocumentType(),
            'codeTypeDocument' => $documentType->getCode(),
        ];

        // On vérifie s'il existe un document récent afin d'éviter de relancer une synchro ApiGouvEntreprise
        if ($this->isTimeLimitToSynchronizeIsExceeded($entreprise, $documentType) || !$documentEntrepriseVersion) {
            // Faire la synchro avec la route documentType
            $resultWs = $this->entrepriseAPIService->getAttestationByDocumentType(
                $documentType,
                $entreprise->getSiren()
            );

            if (isset($resultWs['errors'])) {
                $documentData['errorMessage'] = $resultWs['errors'];

                return $documentData;
            } elseif (isset($resultWs['data']) && isset($resultWs['data']['document_url'])) {
                $urlApiGouv = $resultWs['data']['document_url'];
                $documentEntrepriseVersion = $this->addNewVersionDocument(
                    $entreprise,
                    $documentType,
                    $documentEntrepriseVersion,
                    $urlApiGouv,
                    $resultWs,
                    $documentEntreprise,
                );
            }
        }

        $documentEntreprise->setIdDerniereVersion($documentEntrepriseVersion->getIdVersionDocument());
        $this->entityManager->persist($documentEntreprise);
        $this->entityManager->flush();

        if ($documentEntrepriseVersion) {
            // Get Blob File
            /** @var BlobFichier $blobFile */
            $blobFile = $this->entityManager->getRepository(BlobFichier::class)->find(
                $documentEntrepriseVersion->getIdBlob()
            );

            if ($blobFile && $documentType) {
                $documentData['file'] = $this->iriConverter->getIriFromItem($blobFile);

                return $documentData;
            }
        }

        $this->logger->info(
            sprintf(
                "Fin récupération document pour l'entreprise %s ayant pour type de document : %s",
                $entreprise->getSiren(),
                $typeDocument,
            )
        );

        return $data;
    }

    private function isTimeLimitToSynchronizeIsExceeded(Entreprise $entreprise, DocumentType $documentType): bool
    {
        $this->logger->info('Debut verification date de dernier appel valide du WS');

        /** @var DerniersAppelsValidesWsSgmapDocuments $dernierAppelValideWs */
        $dernierAppelValideWs =
            $this->entityManager->getRepository(DerniersAppelsValidesWsSgmapDocuments::class)->findOneBy(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
        ;

        $dateDernierAppelValide = $dernierAppelValideWs?->getDateDernierAppelValide()->format(self::DATETIME_FORMAT);
        $parameterDelaiSynchro = $this->parameterBag->get('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION');

        if ($dateDernierAppelValide) {
            $this->logger->info(sprintf("Date de dernier appel valide du WS : %s", $dateDernierAppelValide));

            // On vérifie si le dernier document récupéré par le WS est un document récent
            // Le nombre de jours de différence à prendre en compte est indiqué dans le paramétre d'environnement
            // => 'DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION'

            $dateNow = (new DateTime())->modify($parameterDelaiSynchro);
            $dateDiff = $dateNow->diff($dernierAppelValideWs->getDateDernierAppelValide());

            if (0 === $dateDiff->days) {
                $this->logger->info(
                    sprintf(
                        "Le dernier appel valide du WS date de moins de %s jours donc pas de synchro",
                        $dateDiff->days
                    )
                );

                return false;
            }
        }

        $this->logger->info(
            "Le dernier appel valide dépasse le paramétre de délai de synchro donc lancement ApiGouvEntreprise"
        );

        $this->logger->info('Fin verification date de dernier appel valide du WS');

        return true;
    }

    private function addNewVersionDocument(
        Entreprise $entreprise,
        DocumentType $documentType,
        ?EntrepriseDocumentVersion $documentEntrepriseVersion,
        string $url,
        array $content,
        EntrepriseDocument $documentEntreprise
    ): ?EntrepriseDocumentVersion {

        // Sauvegarder le fichier en local via l'url récupéré par l'api entreprise
        $blobFile = $this->blobFileService->uploadExternalFileToBlobFile(
            $documentType,
            $url,
            $content,
            $documentType->getNomDocumentType()
        );

        if ($blobFile) {
            // Mettre à jour la table stockant la dernière date d'appel au ws
            $this->saveDateDernierAppelValideWs($entreprise, $documentType);

            // Créer nouveau versionDocument
            return $this->createNewVersionDocument($blobFile, $documentEntreprise, $url, $content);
        }

        return $documentEntrepriseVersion;
    }

    private function saveDateDernierAppelValideWs(Entreprise $entreprise, DocumentType $documentType)
    {
        $this->logger->info('Enregistrement nouvelle date de dernier appel valide du WS');

        /** @var DerniersAppelsValidesWsSgmapDocuments $dernierAppelValideWs */
        $dernierAppelValideWs =
            $this->entityManager->getRepository(DerniersAppelsValidesWsSgmapDocuments::class)->findOneBy(
                [
                    'idEntreprise' => $entreprise,
                    'idTypeDocument' => $documentType
                ]
            )
        ;

        if (!$dernierAppelValideWs) {
            $dernierAppelValideWs = new DerniersAppelsValidesWsSgmapDocuments();
            $dernierAppelValideWs->setIdTypeDocument($documentType->getIdDocumentType());
            $dernierAppelValideWs->setIdEntreprise($entreprise->getId());
            $dernierAppelValideWs->setIdentifiant($entreprise->getSiren() ?? '');
        }

        if ($user = $this->security->getUser()) {
            $dernierAppelValideWs->setIdAgent($user->getId());
        }

        $newDate = (new DateTime());
        $dernierAppelValideWs->setDateDernierAppelValide($newDate);

        $this->entityManager->persist($dernierAppelValideWs);
        $this->entityManager->flush();

        $this->logger->info(
            sprintf(
                "Date dernier appel valide du WS enregistre pour l'entreprise : %s",
                $entreprise->getId()
            )
        );
    }

    private function createNewVersionDocument(
        BlobFichier $blobFile,
        EntrepriseDocument $documentEntreprise,
        string $url,
        array $content
    ): EntrepriseDocumentVersion {
        $this->logger->info(
            sprintf("Enregistrement nouvelle version document pour blobfileId = %s", $blobFile->getId())
        );

        $versionDocument = new EntrepriseDocumentVersion();
        $versionDocument->setDocument($documentEntreprise);
        $versionDocument->setDateRecuperation(new DateTime());
        $versionDocument->setHash(md5(json_encode($content, JSON_THROW_ON_ERROR)));
        $versionDocument->setIdBlob($blobFile->getId());
        $versionDocument->setTailleDocument($this->blobFileService->getFileSize($blobFile));
        $versionDocument->setExtensionDocument($this->atexoUtil->getExtension($url));

        $this->entityManager->persist($versionDocument);
        $this->entityManager->flush();

        $this->logger->info(
            sprintf("Fin enregistrement nouvelle version document pour blobfileId = %s", $blobFile->getId())
        );

        return $versionDocument;
    }

    private function createNewDocumentIfNotExist(
        Entreprise $entreprise,
        DocumentType $documentType
    ): EntrepriseDocument {
        $this->logger->info("Enregistrement nouveau document EntrepriseDocument");

        $etablissment = $entreprise->getEtablissements()?->first()
            ? $entreprise->getEtablissements()?->first() ->getIdEtablissement() :
            0
        ;
        $documentName = $this->getFormatedDocumentName($documentType->getNomDocumentType(), $entreprise->getSiren());

        $documentEntreprise = new EntrepriseDocument();
        $documentEntreprise->setIdEntreprise($entreprise->getId());
        $documentEntreprise->setEntreprise($entreprise);
        $documentEntreprise->setIdEtablissement($etablissment);
        $documentEntreprise->setNomDocument($documentName);
        $documentEntreprise->setIdTypeDocument($documentType->getIdDocumentType());

        $this->entityManager->persist($documentEntreprise);
        $this->entityManager->flush();

        $this->logger->info("Fin enregistrement nouveau document EntrepriseDocument");

        return $documentEntreprise;
    }

    private function getFormatedDocumentName(string $documentTypeName, string $siren): string
    {
        return sprintf("%s_%s", str_replace(' ', '_', $documentTypeName), $siren);
    }
}
