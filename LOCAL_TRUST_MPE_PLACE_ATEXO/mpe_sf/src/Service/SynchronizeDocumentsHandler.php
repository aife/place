<?php

namespace App\Service;

use App\Message\SynchronizeDocumentsMessage;
use App\Event\DepotValidationSynchronizeDocumentsEvent;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class SynchronizeDocumentsHandler d'initialisation de la synchronisation des documents SGMAP.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class SynchronizeDocumentsHandler
{
    /**
     * SynchronizeDocumentsHandler constructor.
     *
     * @param LoggerInterface $logger
     * @param MessageBusInterface $bus
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly MessageBusInterface $bus
    ) {
    }

    /**
     * Permet d'initialiser la synchronisation des documents entreprise et/ou etablissement.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function synchronizeDocuments(DepotValidationSynchronizeDocumentsEvent $event): void
    {
        $reference = null;
        $idEntreprise = null;
        $idEtablissement = null;
        try {
            $reference = $event->getConsultation()->getReferenceUtilisateur();
            $idEntreprise = $event->getIdEntreprise();
            $idEtablissement = $event->getIdEtablissement();
            $idOffre = $event->getIdOffre();

            $this->logger->info(
                "Dispatch SynchronizeDocumentsMessage pour la commande synchronize:documents 
                dans  avec les parametres [reference=$reference],
                 [idEntreprise=$idEntreprise], [idEtablissement=$idEtablissement]"
            );
            $this->bus->dispatch(new SynchronizeDocumentsMessage(
                $reference,
                $idEntreprise,
                $idEtablissement,
                $idOffre
            ));
        } catch (\Exception $e) {
            $erreur = "Erreur lors de la commande synchronize:documents dans le dispatch SynchronizeDocumentsMessage 
            avec les parametres [reference=$reference], [idEntreprise=$idEntreprise], 
            [idEtablissement=$idEtablissement]";
            $erreur .= PHP_EOL . 'Exception ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->logger->error($erreur);
        }
    }
}
