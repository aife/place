<?php

namespace App\Service;

use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\RG;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentService
{
    final const PAGE_RG = 'EntrepriseDownloadReglement';
    final const PAGE_DCE = 'EntrepriseDemandeTelechargementDce';
    final const PAGE_COMPLEMENT = 'EntrepriseDownloadComplement';
    final const LIEN_GENERIQUE = 'index.php?page=Entreprise.[PAGE]&id=[CONSULTATIONID]&orgAcronyme=[ACRONYME]';

    public function __construct(private readonly EntityManagerInterface $em, private readonly AtexoUtil $utilitaire, private readonly AtexoTelechargementConsultation $telechargementConsultationService, private readonly TranslatorInterface $translator, private $parameters = [])
    {
    }

    /**
     * @return array
     */
    public function getLienRg(Consultation $consultation)
    {
        $consultationId = $consultation->getId();
        $organismeOrganisme = $consultation->getAcronymeOrg();
        $rg = $this->em
            ->getRepository(RG::class)
            ->getReglement($consultationId, $organismeOrganisme);

        $lien = [];

        if ($rg instanceof RG) {
            $lien = $this->getInfoslien(
                base64_encode($consultationId),
                $organismeOrganisme,
                $rg->getRg(),
                self::PAGE_RG,
                $this->translator->trans('DEFINE_REGLEMENT_CONSULTATION')
            );
        }

        return $lien;
    }

    /**
     * @return array
     */
    public function getLienDCE(Consultation $consultation)
    {
        $consultationId = $consultation->getId();
        $organismeOrganisme = $consultation->getAcronymeOrg();
        $dce = $this->em
            ->getRepository(DCE::class)
            ->getDce($consultationId, $organismeOrganisme);

        $lien = [];

        if ($dce instanceof DCE) {
            $lien = $this->getInfoslien(
                htmlspecialchars($consultationId),
                $organismeOrganisme,
                $dce->getDce(),
                self::PAGE_DCE,
                $this->translator->trans('DEFINE_DOSSIER_CONSULTATION')
            );
        }

        return $lien;
    }

    /**
     * @return array
     */
    public function getLienComplement(Consultation $consultation)
    {
        $consultationId = $consultation->getId();
        $organismeOrganisme = $consultation->getAcronymeOrg();

        $complement = $this->em
            ->getRepository(Complement::class)
            ->getComplement($consultationId, $consultation->getAcronymeOrg());

        $lien = [];
        if ($complement instanceof Complement) {
            $lien = $this->getInfoslien(
                base64_encode(htmlspecialchars($consultationId)),
                $organismeOrganisme,
                $complement->getComplement(),
                self::PAGE_COMPLEMENT,
                $this->translator->trans('DEFINE_SAVOIR_PLUS_CONSULTATION')
            );
        }

        return $lien;
    }

    /**
     * @return string
     */
    public function getLienRcByIdConsultation(string $consultationId)
    {
        $lien = '';
        $consultation = $this->em->getRepository(Consultation::class)->find($consultationId);

        if (empty($consultation)) {
            throw new NotFoundHttpException('La consultation avec l\'id:'.$consultationId.'n\'existe pas');
        }

        $data = $this->getLienRg($consultation);

        if (count($data) > 0) {
            $lien = $data['lien'];
        }

        return $lien;
    }

    /**
     * @param $consultationIdEncoded
     * @param $organismeAcronyme
     * @param $blobId
     * @param $page
     * @param $labelTaille
     *
     * @return array
     */
    private function getInfoslien($consultationIdEncoded, $organismeAcronyme, $blobId, $page, $labelTaille)
    {
        $lien = [];
        $lienRelatif = str_replace(
            ['[PAGE]', '[CONSULTATIONID]', '[ACRONYME]'],
            [$page, $consultationIdEncoded, $organismeAcronyme],
            self::LIEN_GENERIQUE
        );
        $lien['lien'] = $this->parameters['URL_MPE'].$lienRelatif;

        $tailleRg = $this->utilitaire
            ->arrondirSizeFile(
                $this->telechargementConsultationService
                    ->getFileSize(
                        $blobId,
                        $organismeAcronyme
                    )
                / 1024
            );
        $lien['taille'] = $labelTaille.' - '.$tailleRg;

        return $lien;
    }
}
