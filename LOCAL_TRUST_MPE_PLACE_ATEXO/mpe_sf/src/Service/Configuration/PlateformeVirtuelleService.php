<?php

namespace App\Service\Configuration;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\Configuration\PlateformeVirtuelleOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Class PlateformeVirtuelleService.
 */
class PlateformeVirtuelleService
{
    private array $plateformesVirtuelles = [];

    /**
     * PlateformeVirtuelleService constructor.
     */
    public function __construct(private readonly EntityManagerInterface $em, private readonly SessionInterface $session, private readonly ParameterBagInterface $param, private readonly RequestStack $requestStack)
    {
    }

    public function getAcronymesOrgEligible(string $domaine = null)
    {
        $domaine ??= $this->requestStack->getCurrentRequest()?->getHttpHost() ?? $this->param->get('PF_URL_REFERENCE');
        if (!$acronymOrgas = $this->session->get('organismes_eligibles')) {
            $acronymOrgas = $this->em->getRepository(PlateformeVirtuelleOrganisme::class)
                ->getListAcronymeOrgEligible($domaine);

            $this->session->set('organismes_eligibles', $acronymOrgas);
        }

        return $acronymOrgas;
    }

    /**
     * Récupération d'une PlateformeVirtuelle à partir de son ID.
     *
     * @return object|null
     */
    public function getPlateformeVirtuelleById(int $id)
    {
        if (!isset($this->plateformesVirtuelles[$id])) {
            $this->plateformesVirtuelles[$id] = $this->em->getRepository(PlateformeVirtuelle::class)->find($id);
        }

        return $this->plateformesVirtuelles[$id];
    }

    /**
     * @return array
     */
    public function getAcronymesOrgEligibleByDomaineId(int $id)
    {
        $res = [];
        if (!empty($id)) {
            $res = $this->em->getRepository(PlateformeVirtuelleOrganisme::class)
                ->getListAcronymeOrgEligibleById($id);
        }

        return $res;
    }
}
