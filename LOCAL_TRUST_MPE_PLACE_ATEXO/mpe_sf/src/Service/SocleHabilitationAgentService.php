<?php

namespace App\Service;

use Exception;
use App\Entity\Agent;
use App\Entity\SocleHabilitationAgent;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SocleHabilitationAgentService
{
    /**
     * OrganismeService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(private readonly ContainerInterface $container, private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @param $idAgent
     *
     * @return SocleHabilitationAgent
     *
     * @throws Exception
     */
    public function createForAdmin(Agent $agent)
    {
        try {
            $socleHabilitationAgent = new SocleHabilitationAgent();
            $socleHabilitationAgent->setAgent($agent);
            $socleHabilitationAgent->setGestionAgentPoleSocle(1);
            $socleHabilitationAgent->setGestionAgentsSocle(1);
            $socleHabilitationAgent->setDroitGestionServicesSocle(1);
            $this->em->persist($socleHabilitationAgent);
            $this->em->flush();

            return $socleHabilitationAgent;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
