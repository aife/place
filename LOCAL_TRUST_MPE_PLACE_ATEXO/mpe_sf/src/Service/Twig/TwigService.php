<?php
/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\Twig;

use Twig\Error\RuntimeError;
use Twig\Environment;

/**
 * Class TwigService.
 */
class TwigService
{
    /**
     * TwigService constructor.
     *
     * @param Environment $_twig test
     *
     * @return void
     */
    public function __construct(private readonly Environment $_twig)
    {
    }

    /**
     * Escapes a string.
     *
     * @param string $str      String to clean
     * @param string $strategy The escaping strategy
     *
     * @return string
     *
     * @throws RuntimeError
     */
    public function twigEscapeFilter(string $str, string $strategy = 'html')
    {
        if (function_exists('twig_escape_filter')) {
            return twig_escape_filter($this->_twig, $str, $strategy);
        }

        return $str;
    }
}
