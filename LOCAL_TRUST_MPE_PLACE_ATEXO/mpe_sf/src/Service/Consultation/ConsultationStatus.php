<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use RuntimeException;
use App\Entity\Consultation;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationStatus
{
    public final const DATE_FORMAT = 'Y-m-d H:i:s';
    public final const EMPTY_DATE = '0000-00-00';
    public final const EMPTY_DATE_TIME = self::EMPTY_DATE . ' 00:00:00';
    public final const STATUS = [
        'ELABORATION'       => '0',
        'PREPARATION'       => '1',
        'CONSULTATION'      => '2',
        'OUVERTURE_ANALYSE' => '3',
        'DECISION'          => '4'
    ];

    public final const STATUS_MULTI_COD = 'CONSULTATION / OUVERTURE_ANALYSE / DECISION';
    public final const STATUS_MULTI_OD = 'OUVERTURE_ANALYSE / DECISION';

    public final const STATUS_COLORS = [
        '#FBC02D', //ELABORATION
        '#FF7D4D', //PREPARATION
        '#2196F3', //CONSULTATION
        '#1976D2', // CONSULTATION / OUVERTURE_ANALYSE / DECISION
        '#01D380', // OUVERTURE_ANALYSE
        '#00B7A5', // OUVERTURE_ANALYSE / DECISION
        '#12856D' // DECISION
    ];

    public function __construct(
        private readonly ParameterBagInterface $parameter
    ) {
    }

    public function getConsultationStatus(Consultation $consultation): array
    {
        $idStatus = $consultation->getIdEtatConsultation();
        $dateMiseEnligneCalcule = $consultation->getDateMiseEnLigneCalcule();
        $endDate = $consultation->getDatefin();
        $status = [];

        if ($idStatus > self::STATUS['OUVERTURE_ANALYSE']) {
            $status[] = $idStatus;
        } elseif (
            ($endDate > date(self::DATE_FORMAT) || str_contains((string) $endDate, self::EMPTY_DATE))
            && (
                !$dateMiseEnligneCalcule
                || strstr($dateMiseEnligneCalcule, self::EMPTY_DATE)
                || $dateMiseEnligneCalcule > date(self::DATE_FORMAT)
            )
        ) {
            $status[] = ('1' == $consultation->getEtatEnAttenteValidation())
                ? self::STATUS['PREPARATION']
                : self::STATUS['ELABORATION']
            ;
        } elseif (
            $endDate > date(self::DATE_FORMAT)
            && $dateMiseEnligneCalcule
            && !str_contains((string) $dateMiseEnligneCalcule, self::EMPTY_DATE)
            && $dateMiseEnligneCalcule <= date(self::DATE_FORMAT)
        ) {
            $status[] = self::STATUS['CONSULTATION'];
        } elseif ($endDate <= date(self::DATE_FORMAT) && !$idStatus) {
            $status[] = self::STATUS['OUVERTURE_ANALYSE'];
        }

        if (is_countable($status) && count($status) == 0) {
            throw new RuntimeException('Consultation without status');
        }

        $multiStatus = $this->getMultiStatus($consultation, $status);

        return (is_countable($multiStatus) && count($multiStatus) > 0)
            ? $multiStatus
            : $status;
    }

    private function getMultiStatus(Consultation $consultation, array $status): array
    {
        $multiStatus = [];

        if (
            $consultation->getDecisionPartielle() == $this->parameter->get('DECISION_PARTIELLE_OUI')
            && $status[0] == self::STATUS['OUVERTURE_ANALYSE']
        ) {
            array_push($multiStatus, self::STATUS['OUVERTURE_ANALYSE'], self::STATUS['DECISION']);
        }

        if (
            $status[0] !== self::STATUS['PREPARATION']
            && $status[0] !== self::STATUS['ELABORATION']
            && $consultation->getDepouillablePhaseConsultation() ==
            $this->parameter->get('CONSULTATION_DEPOUIABLE_PHASE_1')
        ) {
            array_push($multiStatus, self::STATUS['OUVERTURE_ANALYSE'], self::STATUS['DECISION']);
            if ($consultation->getDatefin() > date(self::DATE_FORMAT)) {
                array_unshift($multiStatus, self::STATUS['CONSULTATION']);
            }
        }

        return $multiStatus;
    }
}
