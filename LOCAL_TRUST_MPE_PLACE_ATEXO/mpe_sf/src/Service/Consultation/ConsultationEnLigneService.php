<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Service\CurrentUser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

class ConsultationEnLigneService
{
    public function __construct(
        public ParameterBagInterface $parameterBag,
        protected CurrentUser $currentUser
    ) {
    }

    public function hasHabilitationModifierConsultationApresValidation(?TypeProcedureOrganisme $typeProcedure): bool
    {
        if (null === $typeProcedure) {
            return false;
        }

        if ($typeProcedure->getMapa() === $this->parameterBag->get('IS_MAPA')) {
            return ($typeProcedure->getIdMontantMapa() == $this->parameterBag->get('MONTANT_MAPA_INF_90')
                    && $this->hasHabilitationAgent('ModifierConsultationMapaInferieurMontantApresValidation'))
                || ($typeProcedure->getIdMontantMapa() == $this->parameterBag->get('MONTANT_MAPA_SUP_90')
                    && $this->hasHabilitationAgent('ModifierConsultationMapaSuperieurMontantApresValidation'));
        }
        return $this->hasHabilitationAgent('ModifierConsultationProceduresFormaliseesApresValidation');
    }

    protected function hasHabilitationAgent(string $habilitation, string $prefixMethod = 'get'): bool
    {
        return $this->currentUser && $this->currentUser->isAgent()
            && $this->currentUser->checkHabilitation($this->convertToCamelCase($habilitation), $prefixMethod);
    }

    protected function convertToCamelCase(string $code): string
    {
        return ucfirst((new CamelCaseToSnakeCaseNameConverter())->denormalize($code));
    }
}
