<?php

namespace App\Service\Consultation;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\TDumeContexte;
use App\Service\AtexoConfiguration;
use App\Service\Dume\DumeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationValidationService
{
    public final const STATUS_DUME_CONTEXT_VALID = 2;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly TranslatorInterface $translator,
        private readonly AtexoConfiguration $atexoConfiguration,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly DumeService $dumeService
    ) {
    }

    public function consultationValidate(Consultation $consultation): array
    {
        $errors = [];
        if (
            $consultation->getDumeDemande() != 0
            && !empty($consultation->getDumeDemande())
            && 0 == $consultation->getTypeFormulaireDume()
        ) {
            $dumeContext = $this->entityManager->getRepository(TDumeContexte::class)->findOneBy(
                ['consultation' => $consultation->getId()]
            );

            if (!$dumeContext) {
                $errors[] = $this->translator->trans('TEXT_FORMULAIRE_DUME_N_ONT_PAS_TOUS_ETE_RENSEIGNES');
            } else {
                $resultDumeValidation = $this->dumeService->validationDume($dumeContext);

                if ($resultDumeValidation['hasError']) {
                    $errors[] = $resultDumeValidation['message'];
                }
            }
        }

        if (true === $consultation->isAlloti() && $consultation->getLots()->isEmpty()) {
            $errors[] = $this->translator->trans('DEFINE_VOUS_DEVEZ_AJOUTER_AU_MOINS_UN_LOT');
        }

        if ($consultation->getDonneeComplementaireObligatoire() || $consultation->getDonneePubliciteObligatoire()) {
            $organisme = null;
            if ($this->security->getUser() instanceof Agent) {
                $organisme = $this->security->getUser()->getOrganisme();
            }
            if (empty($consultation->getDonneeComplementaire()?->getDureeMarche())) {
                $errors[] = $this->translator->trans('DEFINE_DUREE_MARCHE_OU_DELAI_EXECUTION');
            }
            if (
                empty($consultation->getDonneeComplementaire()?->getIdCcagReference())
                && false === $consultation->isAlloti()
            ) {
                $errors[] = $this->translator->trans('DEFINE_CCAG_REFERENCE');
            }
            if ($consultation->getDonneePubliciteObligatoire()) {
                if (empty($consultation->getDonneeComplementaire()?->getActiviteProfessionel())) {
                    $errors[] = $this->translator->trans('DEFINE_ACTIVITE_PROFESSIONEL');
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getPrm())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_CORRESPONDANT_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ->getConsultationComptePub()?->getDenomination())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getAdresse())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getCp())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getVille())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getTelephone())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_TELEPHONE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getEmail())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_EMAIL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (empty($consultation->getDonneeComplementaire()?->getConsultationComptePub()?->getUrl())) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()
                        ?->getInstanceRecoursOrganisme())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                    ?->getConsultationComptePub()
                        ?->getInstanceRecoursAdresse())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()?->getInstanceRecoursCp())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                    ?->getConsultationComptePub()
                    ?->getInstanceRecoursVille())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()?->getInstanceRecoursUrl())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                    ?->getConsultationComptePub()
                        ?->getFactureDenomination())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()?->getFactureAdresse())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()?->getFactureCp())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE'
                    );
                }
                if (
                    empty($consultation->getDonneeComplementaire()
                        ?->getConsultationComptePub()?->getFactureVille())
                ) {
                    $errors[] = $this->translator->trans(
                        'DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE'
                    );
                }
            }
            if ($consultation->getDonneePubliciteObligatoire() && !$this->parameterBag->get('SPECIFIQUE_PLACE')) {
                if (empty($consultation->getDonneeComplementaire()?->getActiviteProfessionel())) {
                    $errors[] = $this->translator->trans('DEFINE_ACTIVITE_PROFESSIONEL');
                }
                if (empty($consultation->getDonneeComplementaire()?->getEconomiqueFinanciere())) {
                    $errors[] = $this->translator->trans('DEFINE_ECONOMIQUE_FINANCIERE');
                }
                if (empty($consultation->getDonneeComplementaire()?->getTechniquesProfessionels())) {
                    $errors[] = $this->translator->trans('DEFINE_TECHNIQUES_PROFESSIONELS');
                }
                if (empty($consultation->getDonneeComplementaire()?->getMotsCles())) {
                    $errors[] = $this->translator->trans('MOTS_CLES_DESCRIPTEURS_CONSULTATION');
                }
            }
            if (
                $this->atexoConfiguration->isModuleEnabled('Publicite', $organisme) &&
                !$this->parameterBag->get('SPECIFIQUE_PLACE') && $consultation->getDonneePubliciteObligatoire()
            ) {
                if (null === $consultation->getDonneeComplementaire()?->getVisiteObligatoire()) {
                    $errors[] = $this->translator->trans('MANDATORY_VISIT');
                }
            }
            if (
                $this->atexoConfiguration->isModuleEnabled('Publicite', $organisme)
                && !$this->parameterBag->get('SPECIFIQUE_PLACE')
                &&  $consultation->getDonneePubliciteObligatoire()
            ) {
                if (empty($consultation->getTypeMarche()->getTechniqueAchat())) {
                    $errors[] = $this->translator->trans('TECHNIQUE_ACHAT');
                }
                if (null === $consultation->getDonneeComplementaire()?->getAttributionSansNegociation()) {
                    $errors[] = $this->translator->trans('POSSIBILTY_ATTRIBUTION_WITHOUT_NEGOCIATION');
                }
                if (empty($consultation->getDonneeComplementaire()?->getCatalogueElectronique())) {
                    $errors[] = $this->translator->trans('ELECTRONIC_CATALOG');
                }
            }
            if (
                $this->atexoConfiguration->isModuleEnabled('DonneesComplementaires', $organisme) &&
                $this->atexoConfiguration->isModuleEnabled('DonneesRedac', $organisme)
            ) {
                if (empty($consultation->getDonneeComplementaire()?->getDelaiValiditeOffres())) {
                    $errors[] = $this->translator->trans('FCSP_DELAIS_VALIDITE_OFFRES');
                }
                if (empty($consultation->getDonneeComplementaire()?->getIdCritereAttribution())) {
                    $errors[] = $this->translator->trans('DEFINE_CRITERE_ATTRIBUTION');
                }

                if (!is_null($consultation->getDonneeComplementaire())) {
                    if (
                        count($consultation->getDonneeComplementaire()->getTranches()) > 0
                        && empty($consultation->getDonneeComplementaire()->getIdFormePrix())
                        && false === $consultation->isAlloti()
                    ) {
                        foreach ($consultation->getDonneeComplementaire()->getTranches() as $tranche) {
                            if ($tranche->getIdFormePrix() === null) {
                                $errors[] = $this->translator->trans('TEXT_FORME_DE_PRIX');
                                break;
                            }
                        }
                    } elseif (
                        empty($consultation->getDonneeComplementaire()->getIdFormePrix())
                        &&  count($consultation->getDonneeComplementaire()->getTranches()) === 0
                        && false === $consultation->isAlloti()
                    ) {
                        $errors[] = $this->translator->trans('TEXT_FORME_DE_PRIX');
                    }
                }
            }
        }
        if (
            empty($consultation->getEnvCandidature())
            &&
            empty($consultation->getEnvOffre())
            &&
            empty($consultation->getEnvAnonymat())
        ) {
            $errors[] = $this->translator->trans('DEFINE_CONSTITUTION_DOSSIERS_REPONSES');
        }
        return $errors;
    }
}
