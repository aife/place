<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\Consultation;
use App\Entity\ProcedureEquivalence;
use App\Entity\ProcedureEquivalenceDume;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ConsultationProcedureEquivalence
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $parameterBag
    ) {
    }

    public function setAlloti(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        if (
            str_contains($procedureEquivalence->getEnvOffreTypeUnique(), '-')
            || str_contains($procedureEquivalence->getEnvOffreTypeMultiple(), '-')
        ) {
            $enabledParamProcedure = $this->getActivate($procedureEquivalence->getEnvOffreTypeMultiple());
            $consultation->setAlloti($enabledParamProcedure);

            return $consultation;
        }

        return $consultation;
    }

    public function setCodeCpv(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getAfficherCodeCpv());
        $consultationRef = new Consultation();
        $defaultValueProperty = $consultationRef->getCodeCpv1();
        if ($enabledParamProcedure == 0 && $defaultValueProperty === null) {
            $consultation->setCodeCpv1(null);
            $consultation->setCodeCpv2(null);
        } else {
            $consultation->setCodeCpv1($consultation->getCodeCpv1());
            $consultation->setCodeCpv2($consultation->getCodeCpv2());
        }

        return $consultation;
    }

    public function setDumeDemande(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        if (str_starts_with($procedureEquivalence->getDumeDemande(), '+')) {
            return $consultation;
        }

        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getDumeDemande());
        $consultation->setDumeDemande($enabledParamProcedure);

        return $consultation;
    }

    public function setTypeFormulaireDume(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getTypeFormulaireDumeStandard());
        $value = $enabledParamProcedure == 1 ? 0 : 1;
        $consultation->setTypeFormulaireDume($value);
        return $consultation;
    }

    public function setTypeProcedureDume(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $procedureType = $this->em->getRepository(ProcedureEquivalenceDume::class)->findOneBy([
            'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
            'organisme' => $consultation->getOrganisme()->getAcronyme(),
            'selectionner' => '1'
        ]);
        if ($procedureType instanceof ProcedureEquivalenceDume) {
            $consultation->setTypeProcedureDume($procedureType->getIdTypeProcedureDume());
        }

        return $consultation;
    }

    public function setAutoriserReponseElectronique(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $reponseAutorisee = $this->getActivate($procedureEquivalence->getElecResp());
        $reponseRefusee = $this->getActivate($procedureEquivalence->getNoElecResp());
        $reponseObligatoire = $this->getActivate($procedureEquivalence->getRepObligatoire());

        //Réponse électronique
        if ($reponseAutorisee === 1 || $reponseObligatoire === 1) {
            $consultation->setAutoriserReponseElectronique('1');
        } elseif ($reponseRefusee === 0) {
            $consultation->setAutoriserReponseElectronique('0');
        }

        return $consultation;
    }

    public function setReponseObligatoire(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getRepObligatoire());
        $consultation->setReponseObligatoire((string) $enabledParamProcedure);
        return $consultation;
    }

    public function setPoursuivreAffichage(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $value = ($procedureEquivalence->getPoursuiteDateLimiteRemisePli() == 0)
            ? $procedureEquivalence->getDelaiPoursuiteAffichage()
            : 0;
        $consultation->setPoursuivreAffichage($value);
        return $consultation;
    }

    public function setPoursuivreAffichageUnite(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        if ($procedureEquivalence->getDelaiPoursuivreAffichageUnite()) {
            $consultation->setPoursuivreAffichageUnite($procedureEquivalence->getDelaiPoursuivreAffichageUnite());
        }
        return $consultation;
    }

    public function setSignatureOffre(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        if ($this->getActivate($procedureEquivalence->getSignatureDisabled())) {
            $value = 0;
        } elseif ($this->getActivate($procedureEquivalence->getSignatureEnabled())) {
            $value = 1;
        } elseif ($this->getActivate($procedureEquivalence->getSignatureAutoriser())) {
            $value = 2;
        } else {
            $value = null;
        }

        $consultation->setSignatureOffre($value);
        return $consultation;
    }

    public function setModeOuvertureReponse(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getModeOuvertureReponse());
        $consultation->setModeOuvertureReponse((string) $enabledParamProcedure);
        return $consultation;
    }

    public function setChiffrementOffre(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getCipherEnabled());
        $consultation->setChiffrementOffre((string) $enabledParamProcedure);
        return $consultation;
    }

    public function setEnvCandidature(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getEnvCandidature());
        $consultation->setEnvCandidature((string) $enabledParamProcedure);
        return $consultation;
    }

    public function setEnvOffre(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $consultation->setEnvOffre((string) $this->getActivate($procedureEquivalence->getEnvOffre()));
        $consultation
            ->setSignatureActeEngagement((string) $this->getActivate($procedureEquivalence->getSignaturePropre()));
        $consultation->setAnnexeFinanciere((bool) $this->getActivate($procedureEquivalence->getAnnexeFinanciere()));

        return $consultation;
    }

    public function setEnvAnonymat(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getEnvAnonymat());
        $consultation->setEnvAnonymat((string) $enabledParamProcedure);
        return $consultation;
    }

    public function setDateValidation(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $value = $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE');

        if ($this->getActivate($procedureEquivalence->getMiseEnLigne2())) {
            $value = $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE');
        } elseif ($this->getActivate($procedureEquivalence->getMiseEnLigne3())) {
            $value = $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP');
        } elseif ($this->getActivate($procedureEquivalence->getMiseEnLigne4())) {
            $value = $this->parameterBag->get('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP');
        }

        $consultation->setRegleMiseEnLigne($value);

        return $consultation;
    }

    public function setDonneeComplementaire(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getDonneesComplementaireOui());

        $consultation->setDonneeComplementaireObligatoire($enabledParamProcedure);

        return $consultation;
    }

    public function setPartialDceDownload(
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $enabledParamProcedure = $this->getActivate($procedureEquivalence->getPartialDceDownload());
        $consultation->setPartialDceDownload($enabledParamProcedure);

        return $consultation;
    }

    public function getActivate($value): int
    {
        $integer = 0;
        $value = trim((string) $value);
        if ((!strnatcasecmp($value, '1')) || (!strnatcasecmp($value, '+1')) || (!strnatcasecmp($value, '-1'))) {
            $integer = (int) 1;
        } elseif ((!strnatcasecmp($value, '0')) || (!strnatcasecmp($value, '+0')) || (!strnatcasecmp($value, '-0'))) {
            $integer = (int) 0;
        }

        return $integer;
    }
}
