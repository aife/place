<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Service\Agent\AgentInviterService;
use App\Service\Agent\ConsultationSimplifieeService;
use App\Service\Agent\Guests;
use App\Service\AgentService;
use Doctrine\ORM\OptimisticLockException;
use Exception;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Agent;
use App\Entity\AvisType;
use App\Entity\BloborganismeFile;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Entreprise;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Etablissement;
use App\Entity\GeolocalisationN2;
use App\Entity\Inscrit;
use App\Entity\Offre;
use App\Entity\TDossierFormulaire;
use App\Entity\TEditionFormulaire;
use App\Entity\TEnveloppeDossierFormulaire;
use App\Entity\TReponseElecFormulaire;
use App\Exception\ConsultationNotFoundException;
use App\Exception\UidNotFoundException;
use App\Service\Agent\ReferenceTypeService;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoUtil;
use App\Service\Configuration\PlateformeVirtuelleService;
use App\Service\CurrentUser;
use App\Service\InscritOperationsTracker;
use App\Service\InterfaceSub;
use App\Service\ObscureDataManagement;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Entity\Consultation\ConsultationFavoris;

/**
 * Class ConsultationService.
 */
class ConsultationService
{
    final public const CONSULTATION_TYPE_CLOTURES = 'clotures';
    public const DEFAULT_TYPE_AVIS_ANNONCE_INFORMATION = 2;
    public const DEFAULT_TYPE_AVIS_ANNONCE_ATTRIBUTION = 4;
    public const DEFAULT_TYPE_AVIS_PROJET_ACHAT = 9;

    final public const CODES_NUTS_SEPARATOR = '#';

    final public const PARAMETRE_PROCEDURE_LIST_FOR_WS_CONSULTATION = [
        'Alloti',
        'CodeCpv',
        'DumeDemande',
        'TypeFormulaireDume',
        'TypeProcedureDume',
        'AutoriserReponseElectronique',
        'ReponseObligatoire',
        'PoursuivreAffichage',
        'PoursuivreAffichageUnite',
        'SignatureOffre',
        'ModeOuvertureReponse',
        'ChiffrementOffre',
        'EnvCandidature',
        'EnvOffre',
        'EnvAnonymat',
        'DateValidation',
        'DonneeComplementaire',
        'PartialDceDownload',
    ];

    final public const TYPE_REPONSE_OBLIGATOIRE = 'OBLIGATOIRE';
    final public const TYPE_REPONSE_AUTORISEE = 'AUTORISEE';
    final public const TYPE_REPONSE_REFUSEE = 'REFUSEE';
    final public const TYPE_REPONSE_REQUISE = 'REQUISE';
    final public const TYPE_REPONSE_NON_REQUISE = 'NON_REQUISE';
    final public const SIGNATURE_OFFRE_REQUISE = 1;
    final public const SIGNATURE_OFFRE_AUTORISEE = 2;
    final public const AUTORISER_REPONSE_ELECTRONIQUE = 1;

    /**
     * ConsultationService constructor.
     */
    public function __construct(
        protected PlateformeVirtuelleService $plateformeVirtuelleService,
        protected TranslatorInterface $translator,
        protected EntityManagerInterface $em,
        protected ParameterBagInterface $parameterBag,
        protected ObscureDataManagement $obscureDataManagement,
        protected CurrentUser $currentUser,
        protected LoggerInterface $logger,
        protected InterfaceSub $interfaceSub,
        protected AtexoUtil $atexoUtil,
        protected AtexoFichierOrganisme $atexoFichierOrganisme,
        protected AtexoEnveloppeFichier $atexoEnveloppeFichierService,
        protected InscritOperationsTracker $inscritOperationTracker,
        protected AtexoConfiguration $moduleStateChecker,
        protected ReferenceTypeService $referenceTypeService,
        protected readonly FavorisService $favorisService,
        private AgentInviterService $agentInviterService,
        private readonly AgentService $agentService,
        private readonly Guests $guestsService
    ) {
    }

    /**
     * @param $consultationId
     */
    public function getConsultation($consultationId, $type = null, $autoriserDepotHorsDLRO = false): ?Consultation
    {
        $translated = $this->translator->trans('CONSULTATION_NON_DISPO');
        if (empty($consultationId)) {
            throw new ConsultationNotFoundException(404, $translated);
        }
        $orgs = $this->plateformeVirtuelleService->getAcronymesOrgEligible() ?? [];

        $consultation = $this->em->getRepository(Consultation::class)
            ->getConsultationForDepot($consultationId, $orgs);

        if (
            !($consultation instanceof Consultation)
            && $type === self::CONSULTATION_TYPE_CLOTURES
            && $this->currentUser->getIdInscrit() != ''
        ) {
            $consultation = $this->em->getRepository(Consultation::class)
                ->getClosedConsultations($consultationId, $this->currentUser->getIdInscrit(), $orgs);

            if (!($consultation instanceof Consultation) && $autoriserDepotHorsDLRO) {
                $consultation = $this->em->getRepository(Consultation::class)->find($consultationId);
            }
        }

        $typeAvis = [
            self::DEFAULT_TYPE_AVIS_ANNONCE_INFORMATION,
            self::DEFAULT_TYPE_AVIS_ANNONCE_ATTRIBUTION,
            self::DEFAULT_TYPE_AVIS_PROJET_ACHAT,
        ];

        if (!($consultation instanceof Consultation) && in_array($type, $typeAvis)) {
            $consultation = $this->em->getRepository(Consultation::class)
                ->getConsultationAvis($consultationId, $type, $orgs);
        }

        if (!($consultation instanceof Consultation)) {
            $tab['id'] = $consultationId;
            if (!empty($orgs)) {
                $tab['organisme'] = $orgs;
            }
            $consultation = $this->em->getRepository(Consultation::class)
                ->findOneBy($tab);

            throw new ConsultationNotFoundException(404, $translated);
        }

        return $consultation;
    }

    /**
     * @param $offre
     *
     * @throws OptimisticLockException
     */
    public function createEnveloppeDossierFormulaire($offre)
    {
        $reponseElecFormulaire = $this->em
            ->getRepository(TReponseElecFormulaire::class)
            ->findOneBy([
                'consultationId' => $offre->getConsultationId(),
                'organisme' => $offre->getOrganisme(),
                'idInscrit' => $offre->getInscritId(),
            ]);

        if ($reponseElecFormulaire instanceof TReponseElecFormulaire) {
            $enveloppes = $offre->getEnveloppes();

            foreach ($enveloppes as $enveloppe) {
                $dossier = $this->em
                    ->getRepository(TDossierFormulaire::class)
                    ->findOneBy([
                        'idReponseElecFormulaire' => $reponseElecFormulaire->getIdReponseElecFormulaire(),
                        'idLot' => $enveloppe->getSousPli(),
                        'typeEnveloppe' => $enveloppe->getTypeEnv(),
                        'formulaireDepose' => $this->parameterBag->get('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION'),
                        'typeReponse' => $this->parameterBag->get('TYPE_DOSSIER_REPONSE_PRINCIPALE'),
                    ]);

                if ($dossier instanceof TDossierFormulaire) {
                    //Copie du dossier dans la table "t_enveloppe_dossier_formulaire"
                    $enveloppeDossier = new TEnveloppeDossierFormulaire();

                    $this->logger->info("Enveloppe #" . $enveloppe->getIdEnveloppeElectro() . "#");

                    $enveloppeDossier->setIdEnveloppe($enveloppe->getIdEnveloppeElectro());
                    $enveloppeDossier->setIdDossierFormulaire($dossier->getIdDossierFormulaire());
                    $enveloppeDossier->setOrganisme($enveloppe->getOrganisme());
                    $enveloppeDossier->setIdLot($dossier->getIdLot());
                    $enveloppeDossier->setTypeEnveloppe($dossier->getTypeEnveloppe());
                    $enveloppeDossier->setLibelleForrmulaire($dossier->getLibelleForrmulaire());
                    $enveloppeDossier->setCleExterneDispositif($dossier->getCleExterneDispositif());
                    $enveloppeDossier->setCleExterneDossier($dossier->getCleExterneDossier());
                    $enveloppeDossier->setStatutValidation($dossier->getStatutValidation());
                    $enveloppeDossier->setDateCreation($dossier->getDateCreation());
                    $enveloppeDossier->setDateModif($dossier->getDateModif());
                    $enveloppeDossier->setDateValidation($dossier->getDateValidation());
                    $enveloppeDossier->setStatutGenerationGlobale($dossier->getStatutGenerationGlobale());
                    $enveloppeDossier->setTypeReponse($dossier->getTypeReponse());
                    $enveloppeDossier->setCleExterneFormulaire($dossier->getCleExterneFormulaire());

                    $this->em->persist($enveloppeDossier);

                    //Flaguer le dossier et mettre le champ "formulaire_depose" a 1
                    $dossier->setFormulaireDepose($this->parameterBag->get('FORMULAIRE_DEPOSE_DOSSIER_TRANSMIS'));

                    $this->em->persist($dossier);
                    //deverouiller le dossier chez SUB
                    $this->interfaceSub->unlockDossier($dossier->getCleExterneDossier(), $this->currentUser->getCurrentUser()->getLogin(), null);
                }
            }
        }
    }


    /**
     * Permet de creer une enveloppe
     *
     * @param string $uidResponse unique ID du depot
     * @param integer $typeEnveloppe type d'enveloppe
     * @param integer $lot numero du lot
     * @param null $fictive
     * @return Enveloppe
     * @throws Exception
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @since ESR2017
     * @copyright Atexo 2017*/
    public function createEnveloppe($uidResponse, $typeEnveloppe, $lot, $acronymeEnveloppe, $fictive = null, $flush = true, $offre = null)
    {
        if (empty($uidResponse)) {
            $this->logger->error("[uid_response = $uidResponse] L'unique ID du depot est introuvable");

            throw new UidNotFoundException("L'unique ID du depot est introuvable");
        }

        if ($typeEnveloppe === null) {
            $this->logger->error("[uid_response = $uidResponse] Le type d'enveloppe est introuvable");

            throw new Exception("Le type d'enveloppe est introuvable");
        }

        if ($lot === null) {
            $this->logger->error("[uid_response = $uidResponse] Le numero du lot est introuvable");

            throw new Exception("Le numero du lot est introuvable");
        }

        if (empty($acronymeEnveloppe)) {
            $this->logger->error("[uid_response = $uidResponse] L'acronyme de l'organisme est introuvable");

            throw new Exception("L'acronyme de l'organisme est introuvable");
        }

        try {
            $enveloppe = new Enveloppe();
            $enveloppe->setOrganisme($acronymeEnveloppe);
            $enveloppe->setUidResponse($uidResponse);
            $enveloppe->setOffre($offre);
            $enveloppe->setTypeEnv($typeEnveloppe);
            $enveloppe->setStatutEnveloppe(1); //ferme
            $enveloppe->setSousPli($lot);
            $enveloppe->setChampsOptionnels('');
            $enveloppe->setDonneesOuverture('');
            $enveloppe->setHorodatageDonneesOuverture('');
            $enveloppe->setNomAgentOuverture('');

            if ($fictive) {
                $enveloppe->setStatutEnveloppe($this->parameterBag->get('STATUT_ENV_OUVERTE'));
                $enveloppe->setCryptage('0');
                $enveloppe->setEnveloppeFictive('1');
            }

            $this->em->persist($enveloppe);
            if (true === $flush) {
                $this->em->flush();
            }

            return $enveloppe;
        } catch (Exception $e) {
            $error = "[uid_response = $uidResponse] Erreur creation enveloppe: uidResponse=$uidResponse, typeEnveloppe=$typeEnveloppe, lot=$lot, org=$acronymeEnveloppe, fictive=$fictive" . PHP_EOL;
            $this->logger->error($error . PHP_EOL . "Erreur = " . $e->getMessage() . PHP_EOL . "Trace = " . $e->getTraceAsString());
        }
    }

    /**
     * @param $uidResponse
     * @param $edition
     * @param $enveloppe
     * @param $acronymeOrganisme
     * @param $numOrdre
     *
     * @throws OptimisticLockException
     */
    public function createFichierEnveloppeFromEdition($uidResponse, $edition, $enveloppe, $acronymeOrganisme, $numOrdre)
    {
        $enveloppeFichier = new EnveloppeFichier();
        $enveloppeFichier->setUidResponse($uidResponse);
        $enveloppeFichier->setEnveloppe($enveloppe);
        $enveloppeFichier->setOrganisme($acronymeOrganisme);
        $enveloppeFichier->setNumOrdreFichier($numOrdre);
        $enveloppeFichier->setTypeFichier($edition->getType());
        $enveloppeFichier->setTypePiece($edition->getType());
        $enveloppeFichier->setNomFichier($edition->getNomFichier());
        $enveloppeFichier->setHash(sha1_file($edition->getPath()));
        $enveloppeFichier->setHash256(hash_file("sha256", $edition->getPath()));
        $enveloppeFichier->setIdTypePiece(0);
        $enveloppeFichier->setIsHash(1);
        $enveloppeFichier->setVerificationCertificat('');

        $idBlob = $this->atexoFichierOrganisme->insertFile($edition->getNomFichier(), $edition->getPath(), $acronymeOrganisme);
        $blob = $this->em->getRepository(BloborganismeFile::class)->findById($idBlob);
        $enveloppeFichier->setBlob($blob);

        $this->em->persist($enveloppeFichier);

        $enveloppe->addFichierEnveloppe($enveloppeFichier);

        $this->em->persist($enveloppe);
        $this->em->flush();
    }

    /**
     * Permet de recuperer l'offre.
     *
     * @param int    $offreId     identifiant de l'offre
     * @param string $uidResponse unique ID du depot
     *
     * @return Offre|null
     *
     * @throws Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2017
     *
     * @copyright Atexo 2017*/
    public function getOffre($offreId, $uidResponse)
    {
        if (empty($offreId)) {
            $this->logger->error("[uid_response = $uidResponse] L'ID de l'offre est vide");

            throw new Exception("L'ID de l'offre est vide");
        }

        if (empty($uidResponse)) {
            throw new UidNotFoundException("L'unique ID du depot est vide");
        }

        return $this->em
            ->getRepository(Offre::class)
            ->findOneBy(['id' => $offreId, 'uidResponse' => $uidResponse]);
    }

    /**
     * Suppimer Enveloppe et fichier.
     */
    public function supprimerEnveloppeEtFichier($offre)
    {
        foreach ($offre->getEnveloppes() as $enveloppe) {
            if ($enveloppe instanceof Enveloppe) {
                $fichiers = $enveloppe->getFichierEnveloppes();

                if (!empty($fichiers)) {
                    foreach ($fichiers as $fichier) {
                        if ($fichier instanceof EnveloppeFichier) {
                            $this->atexoEnveloppeFichierService->deleteEnveloppeFichier($fichier->getIdFichier());
                        }
                    }
                }

                $this->em->remove($enveloppe);
                $offre->removeEnveloppe($enveloppe);
            }
        }
    }

    /**
     *  Permet de supprimer non valides lors du depot (eventuellement supprimes).
     *
     * @param string $ids         liste des IDS enveloppes separes par une virgule
     * @param string $uidResponse unique ID correspondant au depot
     *
     * @throws Exception
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016*/
    public function supprimerFichiersNonValides($ids, $uidResponse, $offre, $consultation)
    {
        try {
            $listeIdsObscurcis = explode(',', $ids);
            $listeIds = [];
            if (!empty($listeIdsObscurcis)) {
                foreach ($listeIdsObscurcis as $idObscurci) {
                    $listeIds[] = $this->obscureDataManagement->getDataFromObscuredValue($idObscurci, $uidResponse);
                }
            }
            if (is_array($listeIds) && !empty($listeIds)) {
                $listeEnveloppes = $offre->getEnveloppes();
                if (!empty($listeEnveloppes)) {
                    foreach ($listeEnveloppes as $enveloppe) {
                        if ($enveloppe instanceof Enveloppe) {
                            $fichiers = $enveloppe->getFichierEnveloppes();
                            if (!empty($fichiers)) {
                                foreach ($fichiers as $fichier) {
                                    if ($fichier instanceof EnveloppeFichier) {
                                        if (!in_array($fichier->getIdFichier(), $listeIds)) {
                                            $this->logger->info('supprimerFichiersNonValides fichier id => ' . $fichier->getIdFichier());
                                            $this->removeEnveloppeFichier($fichier->getIdFichier(), $consultation, $uidResponse, 'DESCRIPTION43');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * delete enveloppe fichier et eventuellement le fichier signature associe.
     *
     * @param int          $idFichier
     * @param Consultation $consultation
     * @param string       $uidResponse
     * @param bool         $cleDescription
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function removeEnveloppeFichier($idFichier, $consultation = null, $uidResponse = null, $cleDescription = false)
    {
        $listeFichiers = null;
        $fichierSignature = null;
        try {
            $afficherTrace = 0;
            $fichier = $this
                ->em
                ->getRepository(EnveloppeFichier::class)
                ->findOneBy(['idFichier' => $idFichier]);
            if ($fichier instanceof EnveloppeFichier) {
                $offreFichier = $fichier->getEnveloppe()->getOffre();
                $statutOffres = $offreFichier->getStatutOffres();
                $idOffres = $offreFichier->getId();
                if ($statutOffres != $this->parameterBag->get('STATUT_ENV_BROUILLON')) {
                    $this->logger->error("[uid_response = $uidResponse] Erreur suppression du fichier: [idFichier = $idFichier] " . PHP_EOL
                        . 'Message Erreur: statut invalide pour la suppression du fichier :' . $idFichier . ", statut de l'offre  est : " . $statutOffres
                        . ' id offre du fichier ' . $idOffres . '. Offre en cours ' . $offreFichier->getId());
                } else {
                    if ($fichier->getIdFichierSignature() != $fichier->getIdFichier()) {
                        $fichierSignature = $this
                            ->em
                            ->getRepository(EnveloppeFichier::class)
                            ->findOneBy(['idFichier' => $fichier->getIdFichierSignature()]);
                    }
                    //Debut tracage de l'action
                    $enveloppe = $fichier->getEnveloppe();

                    if ($enveloppe instanceof Enveloppe) {
                        $offreId = $enveloppe->getOffreId();

                        if (null === $consultation && $offreId) {
                            $offre = $this
                                ->em
                                ->getRepository(Offre::class)
                                ->find($offreId);
                            if ($offre instanceof Offre) {
                                if ($offre->getConsultation() instanceof Consultation) {
                                    $consultation = $offre->getConsultation();
                                }
                            }
                        }

                        if (false == $cleDescription) {
                            $cleDescription = $this
                                ->inscritOperationTracker
                                ->getCleDescription('DELETE', $fichier->getTypeFichier());
                            $afficherTrace = 1;
                        }

                        [$idValeurReferentielle, $description] = $this
                            ->inscritOperationTracker
                            ->getInfosValeursReferentielles($cleDescription);

                        if ('SIG' == $fichier->getTypeFichier()) {
                            $fichierSource = $this
                                ->em
                                ->getRepository(EnveloppeFichier::class)
                                ->findOneBy(
                                    [
                                        'idFichierSignature' => $fichier->getIdFichier(),
                                        'organisme' => $fichier->getOrganisme(),
                                    ]
                                );

                            if ($fichierSource instanceof EnveloppeFichier) {
                                $listeFichiers = $fichierSource->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo(
                                    $fichierSource->getTailleFichier()
                                ) . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ')';

                                //Debut Suppression des infos de signature dans le fichier source (n'est pas inclus dans le traitement de la trace des utilisateurs)
                                $fichierSource->setSignatureInfos(null);
                                $fichierSource->setIdFichierSignature(null);

                                $this->em->persist($fichierSource);
                                $this->em->flush($fichierSource);
                                //Fin Suppression des infos de signature dans le fichier source (n'est pas inclus dans le traitement de la trace des utilisateurs)
                            }

                            $listeFichiersSignature = $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo(
                                $fichier->getTailleFichier()
                            ) . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ')';
                        } else {
                            $listeFichiers = $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo(
                                $fichier->getTailleFichier()
                            ) . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ')';

                            $listeFichiersSignature = '';
                        }

                        $description ??= '';
                        $this->inscritOperationTracker
                            ->completeDescription(
                                $description,
                                $listeFichiers,
                                $enveloppe->getTypeEnv(),
                                $fichier->getTypeFichier(),
                                $listeFichiersSignature
                            );

                        $this->inscritOperationTracker->tracerOperationsInscrit(new \DateTime(), new \DateTime(), $idValeurReferentielle, $description, $consultation, '', false, null, $afficherTrace, null, $offreId);
                    }
                    //Fin tracage de l'action

                    //Suppression du fichier
                    $this->atexoEnveloppeFichierService->deleteEnveloppeFichier($idFichier);

                    if ($fichierSignature instanceof EnveloppeFichier) {
                        //Suppression de la signature du fichier
                        $this
                            ->atexoEnveloppeFichierService
                            ->deleteEnveloppeFichier($fichierSignature->getIdFichier());
                    }
                }
            }
        } catch (Exception $e) {
            $this->logger->error(
                "[uid_response = $uidResponse] Erreur suppression du fichier: [idFichier = $idFichier] "
                . PHP_EOL .
                'Message Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString()
            );
        }
    }

    /**
     * @param bool $getOffre
     *
     * @return array
     *
     * @throws Exception
     */
    public function validerFichiersTraitement(int $id, Request $request, $getOffre = true, $offre = null)
    {
        $listeFichiersSignValides = null;
        $nbrFichiersValides = null;
        $listeFichiersNonSignes = null;
        $listeFichiersSignIncertaines = null;
        $listeFichiersSignInvalides = null;
        $listeFichiersNonVerifiable = null;
        $reponse = $request->get('reponse');
        $uidResponse = $reponse['uid_response'];
        $session = $request->getSession();
        $session->set($session->get('token_contexte_authentification_' . $id) . "/$uidResponse/infos_validation_depot_"
            . $id . $this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse) . '/date_debut', new \DateTime());

        $consultation = $this->getConsultation($id, self::CONSULTATION_TYPE_CLOTURES, true);
        $acronymeOrganisme = $consultation->getAcronymeOrg();
        $consultationId = $consultation->getId();
        $idInscrit = $this->currentUser->getCurrentUser()->getId();

        if (true == $getOffre) {
            $offre = $this->getOffre($this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse), $uidResponse);
        }

        try {
            if ($this->moduleStateChecker->hasConfigPlateforme('interfaceModuleSub')) {
                $reponseElecFormulaire = $this->em
                    ->getRepository(TReponseElecFormulaire::class)
                    ->findOneBy([
                        'consultationId' => $consultationId,
                        'organisme' => $acronymeOrganisme,
                        'idInscrit' => $idInscrit,
                    ]);

                $this->supprimerEnveloppeEtFichier();
                if ($reponseElecFormulaire instanceof TReponseElecFormulaire) {
                    $dossierFormulaires = $reponseElecFormulaire->getTdossierformulaires();

                    if (count($dossierFormulaires)) {
                        foreach ($dossierFormulaires as $dossier) {
                            if ($dossier instanceof TDossierFormulaire) {
                                if (
                                    $dossier->getFormulaireDepose() == $this->parameterBag->get('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION') &&
                                    $dossier->getTypeReponse() == $this->parameterBag->get('TYPE_DOSSIER_REPONSE_PRINCIPALE')
                                ) {
                                    $enveloppe = $this->createEnveloppe(
                                        $uidResponse,
                                        $dossier->getTypeEnveloppe(),
                                        $dossier->getIdLot(),
                                        $acronymeOrganisme,
                                        null,
                                        true,
                                        $offre
                                    );

                                    $editions = $dossier->getTEditionFormulaires();

                                    if (count($editions)) {
                                        $numOrdre = 1;

                                        foreach ($editions as $edition) {
                                            if ($edition instanceof TEditionFormulaire) {
                                                $this->createFichierEnveloppeFromEdition(
                                                    $uidResponse,
                                                    $edition,
                                                    $enveloppe,
                                                    $acronymeOrganisme,
                                                    $numOrdre
                                                );

                                                ++$numOrdre;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                //Supprimer les enveloppes non valides (supprimes) lors du depot
                $this->supprimerFichiersNonValides($reponse['fichiers'], $uidResponse, $offre, $consultation);
            }

            $infoFiles = [];
            $infoFiles['statutFileDepot'] = 'OK';
            $detailsFiles = ' Pas de signature présente';

            if (
                $consultation instanceof Consultation &&
                '0' != (string) $consultation->getSignatureOffre()
            ) {
                $infoFiles['signatureOffre'] = (string) $consultation->getSignatureOffre();
                $listeEnveloppes = $offre->getEnveloppes();

                if (!empty($listeEnveloppes)) {
                    $listeFichiersNonSignes = '';
                    $listeFichiersNonVerifiable = '';
                    $listeFichiersSignInvalides = '';
                    $listeFichiersSignIncertaines = '';
                    $listeFichiersSignValides = '';
                    $detailsFiles = '';
                    $nbrFichiersValides = 0;

                    foreach ($listeEnveloppes as $enveloppe) {
                        if ($enveloppe instanceof Enveloppe) {
                            $fichiers = $enveloppe->getFichierEnveloppes();

                            if (!empty($fichiers)) {
                                foreach ($fichiers as $fichier) {
                                    if ($fichier instanceof EnveloppeFichier && 'SIG' != $fichier->getTypeFichier()) {
                                        if (empty($fichier->getTailleFichier()) || '0' == (string) $fichier->getTailleFichier()) {
                                            $infoFiles['emptyFile'][] = $fichier->getNomFichier();
                                            $infoFiles['statutFileDepot'] = 'KO';
                                        }
                                        if (!empty($fichier->getSignatureInfos())) {
                                            $infosSignatures = json_decode($fichier->getSignatureInfos(), true, 512, JSON_THROW_ON_ERROR);

                                            if (
                                                is_array($infosSignatures) && !empty($infosSignatures) &&
                                                array_key_exists('signatureValide', $infosSignatures) &&
                                                null !== $infosSignatures['signatureValide'] &&
                                                array_key_exists('emetteur', $infosSignatures) &&
                                                null !== $infosSignatures['emetteur'] &&
                                                array_key_exists('periodiciteValide', $infosSignatures) &&
                                                null !== $infosSignatures['periodiciteValide']
                                            ) {
                                                if (
                                                    $this->moduleStateChecker
                                                    ->hasConfigPlateforme('surchargeReferentiels')
                                                ) {
                                                    if (
                                                        array_key_exists('repertoiresChaineCertification', $infosSignatures) &&
                                                        null != $infosSignatures['repertoiresChaineCertification'] &&
                                                        array_key_exists('statut', $infosSignatures['repertoiresChaineCertification'][0]) &&
                                                        $infosSignatures['repertoiresChaineCertification'][0]['statut']
                                                        == $this->parameterBag->get('statut_referentiel_certificat_ko')
                                                    ) {
                                                        $infoFiles['infoSignature']['infoSignatureInvalide'][] = $fichier->getNomFichier();
                                                        $infoFiles['statutFileDepot'] = 'KO';
                                                        $listeFichiersSignInvalides .= $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                            . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                    } elseif (
                                                        !array_key_exists('repertoiresChaineCertification', $infosSignatures) ||
                                                        null == $infosSignatures['repertoiresChaineCertification'] ||
                                                        (
                                                            null != $infosSignatures['repertoiresChaineCertification'] &&
                                                            array_key_exists('statut', $infosSignatures['repertoiresChaineCertification'][0]) &&
                                                            $infosSignatures['repertoiresChaineCertification'][0]['statut']
                                                            != $this->parameterBag->get('statut_referentiel_certificat_ok')
                                                        )
                                                    ) {
                                                        $infoFiles['infoSignature']['infoSignatureUnknown'][] = $fichier->getNomFichier();
                                                        $infoFiles['statutFileDepot'] = 'KO';
                                                        $listeFichiersSignIncertaines .= $fichier->getNomFichier() . ' ('
                                                            . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier()) . ' '
                                                            . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                    }
                                                } else {
                                                    if (
                                                        array_key_exists('chaineDeCertificationValide', $infosSignatures) &&
                                                        2 == $infosSignatures['chaineDeCertificationValide']
                                                    ) {
                                                        $infoFiles['infoSignature']['infoSignatureInvalide'][] = $fichier->getNomFichier();
                                                        $infoFiles['statutFileDepot'] = 'KO';
                                                        $listeFichiersSignInvalides .= $fichier->getNomFichier() . ' ('
                                                            . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                            . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                    } elseif (
                                                        !array_key_exists('chaineDeCertificationValide', $infosSignatures) ||
                                                        0 != $infosSignatures['chaineDeCertificationValide']
                                                    ) {
                                                        $infoFiles['infoSignature']['infoSignatureUnknown'][] = $fichier->getNomFichier();
                                                        $infoFiles['statutFileDepot'] = 'KO';
                                                        $listeFichiersSignIncertaines .= $fichier->getNomFichier() . ' ('
                                                            . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                            . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                    }
                                                }

                                                if (
                                                    array_key_exists('absenceRevocationCRL', $infosSignatures) &&
                                                    2 == $infosSignatures['absenceRevocationCRL']
                                                ) {
                                                    $infoFiles['infoSignature']['infoSignatureInvalide'][] = $fichier->getNomFichier();
                                                    $infoFiles['statutFileDepot'] = 'KO';
                                                    $listeFichiersSignInvalides .= $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                } elseif (
                                                    !array_key_exists('absenceRevocationCRL', $infosSignatures) ||
                                                    0 != $infosSignatures['absenceRevocationCRL']
                                                ) {
                                                    $infoFiles['infoSignature']['infoSignatureUnknown'][] = $fichier->getNomFichier();
                                                    $infoFiles['statutFileDepot'] = 'KO';
                                                    $listeFichiersSignIncertaines .= $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                }

                                                if (
                                                    !array_key_exists('periodiciteValide', $infosSignatures) ||
                                                    !$infosSignatures['periodiciteValide']
                                                ) {
                                                    $infoFiles['infoSignature']['infoSignatureInvalide'][] = $fichier->getNomFichier();
                                                    $infoFiles['statutFileDepot'] = 'KO';
                                                    $listeFichiersSignInvalides .= $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                }

                                                if (
                                                    array_key_exists('signatureValide', $infosSignatures) &&
                                                    false == $infosSignatures['signatureValide']
                                                ) {
                                                    $infoFiles['infoSignature']['infoSignatureInvalide'][] = $fichier->getNomFichier();
                                                    $infoFiles['statutFileDepot'] = 'KO';
                                                    $listeFichiersSignInvalides .= $fichier->getNomFichier() . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                } elseif (
                                                    !array_key_exists('signatureValide', $infosSignatures) ||
                                                    true != $infosSignatures['signatureValide']
                                                ) {
                                                    $infoFiles['infoSignature']['infoSignatureUnknown'][] = $fichier->getNomFichier();
                                                    $infoFiles['statutFileDepot'] = 'KO';
                                                    $listeFichiersSignIncertaines .= $fichier->getNomFichier()
                                                        . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                }

                                                if (
                                                    array_key_exists('etat', $infosSignatures) &&
                                                    '0' == (string) $infosSignatures['etat']
                                                ) {
                                                    $listeFichiersSignValides .= $fichier->getNomFichier()
                                                        . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier())
                                                        . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                                    ++$nbrFichiersValides;
                                                }
                                            }
                                        } else {
                                            if ($fichier->getIdFichierSignature()) {
                                                $infoFiles['infoSignature']['infoSignatureNonVerifiable'][] = $fichier->getNomFichier();
                                                $infoFiles['statutFileDepot'] = 'KO';
                                                $listeFichiersNonVerifiable .= $fichier->getNomFichier()
                                                    . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier()) . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                            } else {
                                                $infoFiles['infoSignature']['infoSignatureVide'][] = $fichier->getNomFichier();
                                                $infoFiles['statutFileDepot'] = 'KO';
                                                $listeFichiersNonSignes .= $fichier->getNomFichier()
                                                    . ' (' . $this->atexoUtil->convertAndFormatToKo($fichier->getTailleFichier()) . ' ' . $this->translator->trans('TEXT_KILO_OCTET') . ') | ';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (!empty($listeFichiersSignValides)) {
                    $detailsFiles .= $this->parameterBag->get('separateur_statistiques_fichiers_validation_depot')
                        . $nbrFichiersValides . ' ' . $this->translator->trans('DEFINE_PIECES_CORRECTEMENT_SIGNEES')
                        . ' : ' . $listeFichiersSignValides;
                }

                if (
                    array_key_exists('infoSignature', $infoFiles) && array_key_exists(
                        'infoSignatureVide',
                        $infoFiles['infoSignature']
                    ) && !empty($infoFiles['infoSignature']['infoSignatureVide'])
                ) {
                    $detailsFiles .= $this->parameterBag->get('separateur_statistiques_fichiers_validation_depot')
                        . count($infoFiles['infoSignature']['infoSignatureVide']) . ' '
                        . $this->translator->trans('DEFINE_PIECES_NON_SIGNEES') . ' : ' . $listeFichiersNonSignes;
                }

                if (
                    array_key_exists('infoSignature', $infoFiles) && array_key_exists(
                        'infoSignatureUnknown',
                        $infoFiles['infoSignature']
                    ) && !empty($infoFiles['infoSignature']['infoSignatureUnknown'])
                ) {
                    $detailsFiles .= $this->parameterBag->get('separateur_statistiques_fichiers_validation_depot')
                        . count($infoFiles['infoSignature']['infoSignatureUnknown']) . ' '
                        . $this->translator->trans('DEFINE_PIECES_SIGNEES_DONT_SIGNATURE_INCERTAINE') . ' : '
                        . $listeFichiersSignIncertaines;
                }

                if (
                    array_key_exists('infoSignature', $infoFiles) && array_key_exists(
                        'infoSignatureInvalide',
                        $infoFiles['infoSignature']
                    ) && !empty($infoFiles['infoSignature']['infoSignatureInvalide'])
                ) {
                    $detailsFiles .= $this->parameterBag->get('separateur_statistiques_fichiers_validation_depot')
                        . count($infoFiles['infoSignature']['infoSignatureInvalide']) . ' ' . $this->translator->trans('DEFINE_PIECES_SIGNEES_DONT_SIGNATURE_INVALIDE') . ' : ' . $listeFichiersSignInvalides;
                }

                if (
                    array_key_exists('infoSignature', $infoFiles)
                    && array_key_exists('infoSignatureNonVerifiable', $infoFiles['infoSignature'])
                    && !empty($infoFiles['infoSignature']['infoSignatureNonVerifiable'])
                ) {
                    $detailsFiles .= $this->parameterBag->get('separateur_statistiques_fichiers_validation_depot')
                        . count($infoFiles['infoSignature']['infoSignatureNonVerifiable']) . ' '
                        . $this->translator->trans('DEFINE_PIECES_SIGNEES_DONT_SIGNATURE_NON_VERIFIABLE')
                        . ' : ' . $listeFichiersNonVerifiable;
                }
            }
            //Debut tracage de l'etat des fichiers
            [$idValeurReferentielle, $description] = $this->inscritOperationTracker
                ->getInfosValeursReferentielles('DESCRIPTION32');
            $session->set($session->get('token_contexte_authentification_' . $id)
                . "/$uidResponse/infos_validation_depot_" . $id
                . $this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse)
                . '/id_valeur_referentielle_info_tracker', $idValeurReferentielle);
            $session->set($session->get('token_contexte_authentification_' . $id)
                . "/$uidResponse/infos_validation_depot_" . $id
                . $this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse)
                . '/description_info_tracker', str_replace(
                    ' [_liste_fichiers_ajoutes_]',
                    $detailsFiles,
                    $description
                ));
            $session->set($session->get('token_contexte_authentification_' . $id)
                . "/$uidResponse/infos_validation_depot_"
                . $id . $this->obscureDataManagement->getDataFromObscuredValue($reponse['offre_id'], $uidResponse)
                . '/detail_files_info_tracker', $detailsFiles);
            //Fin tracage de l'etat des fichiers
        } catch (Exception $e) {
            throw $e;
        }

        return $infoFiles;
    }

    /**
     * Permer de récupérer le SIRET (établissement FR) ou TVA Intracommunautaire (établissement ETRANGER)
     * le typeOE d'une entreprise et l'id de l'établissement.
     *
     * @param $user
     *
     * @return array
     */
    public function getSiretAndTypeOE($user)
    {
        $etablissement = $this->em
            ->getRepository(Inscrit::class)
            ->find($user->getId())
            ->getIdEtablissement();

        $entreprise = $this->em
            ->getRepository(Entreprise::class)
            ->find($user->getEntrepriseId());

        $sirenEtranger = $entreprise->getSirenetranger();

        if (null == $sirenEtranger) {
            //Find the SIRET : concat SIREN and CodeEtablissement
            $siren = $entreprise->getSiren();

            $codeEtablissement = $this->em
                ->getRepository(Etablissement::class)
                ->findOneBy(['idEtablissement' => $etablissement])
                ->getCodeEtablissement();

            $siret = $siren . $codeEtablissement;

            $typeOE = $this->parameterBag->get('TYPE_FR_CODE_SN');
        } else {
            //Find the SIRET : tvaIntracommunautaire
            $siret = $this->em
                ->getRepository(Etablissement::class)
                ->findOneBy(['idEtablissement' => $etablissement])
                ->getTvaIntracommunautaire();

            $typeOE = $this->parameterBag->get('TYPE_ETRANGER_CODE_SN');
        }

        return ['siret' => $siret, 'typeOE' => $typeOE, 'etablissement' => $etablissement];
    }

    /**
     * fonction qui retourne le status actuel de la consultation en fonction des informations passe en parametre.
     * @return int|mixed|string
     */
    public function getStatus(Consultation $consultation, bool $calledFromPortail)
    {
        $statut = $idEtatConsultation = 0;
        if (!$calledFromPortail) {
            $idEtatConsultation = $consultation->getIdEtatConsultation();
        }

        $dateMiseEnligneCalcule = $consultation->getDateMiseEnLigneCalcule();
        $dateFin = $consultation->getDatefin();

        if ($idEtatConsultation > 0) {
            $statut = $idEtatConsultation;
        } elseif (
            ($dateMiseEnligneCalcule === null
                || strstr($dateMiseEnligneCalcule->format('Y-m-d'), '0001-11-30')
                || $dateMiseEnligneCalcule->format('Y-m-d') > date('Y-m-d H:i:s'))
        ) {
            if ('1' == $consultation->getEtatEnAttenteValidation()) {
                $statut = $this->parameterBag->get('STATUS_PREPARATION');
            } else {
                $statut = $this->parameterBag->get('STATUS_ELABORATION');
            }
        } elseif (
            $dateFin->format('Y-m-d') > date('Y-m-d H:i:s')
            && $dateMiseEnligneCalcule
            && !strstr($dateMiseEnligneCalcule->format('Y-m-d'), '0001-11-30')
            && $dateMiseEnligneCalcule->format('Y-m-d') <= date('Y-m-d H:i:s')
        ) {
            $statut = $this->parameterBag->get('STATUS_CONSULTATION');
        } elseif (
            $dateFin && $dateFin->format('Y-m-d') <= date('Y-m-d H:i:s')
            && !$idEtatConsultation
        ) {
            $statut = $this->parameterBag->get('STATUS_OUVERTURE_ANALYSE');
        }

        return $statut;
    }

    public function clotureConsultation(Request $request): ?string
    {
        $clotures = null;
        if ($request->get(ConsultationService::CONSULTATION_TYPE_CLOTURES) !== null) {
            $clotures = ConsultationService::CONSULTATION_TYPE_CLOTURES;
        }

        return $clotures;
    }

    public function isFavoriByAgent(Consultation $consultation, Agent $agent): bool
    {
        $consultationFavoris = $this->em
            ->getRepository(ConsultationFavoris::class)
            ->getFavorisForAgent(
                $consultation->getId(),
                $agent->getId()
            );

        if ($consultationFavoris instanceof ConsultationFavoris) {
            return true;
        }

        return false;
    }

    public function setDefaultValuesForInitialization(Consultation $consultation): Consultation
    {
        $consultation->setConsStatut('0');
        $consultation->setDatefin(new \DateTime('0000-00-00 00:00:00'));
        $consultation->setEtatEnAttenteValidation('0');
        $consultation->setIdRpa(null);
        $consultation->setNumeroPhase(1);
        $consultation->setTypeAcces(Consultation::TYPE_ACCES_PUBLIC);
        $consultation->setEnvolActivation(false);

        return $consultation;
    }

    public function setFavorisForInitialization(Consultation $consultation, Agent $agent): void
    {
        $agentsToAdd = [];
        if ($agent->getAlerteMesConsultations() === '1') {
            $agentsToAdd[] = $agent;
        }

        $agentsToAdd = $this->addFavorisGuestsMonEntitee($agent, $agentsToAdd);
        $agentsToAdd = $this->addFavorisGuestsEntiteeDependante($agent, $agentsToAdd);
        $agentsToAdd = $this->addFavorisGuestsEntiteeTransverse($agent, $agentsToAdd);

        foreach ($agentsToAdd as $value) {
            $this->favorisService->addConsultationFavoris($value, $consultation);
        }
    }
    private function addFavorisGuestsMonEntitee(Agent $agent, array $agentsInProcess): array
    {
        $listes = $this->agentInviterService->getPermanentGuestsMonEntitee(
            $agent->getOrganisme(),
            $agent->getService()?->getId()
        );

        if (!empty($listes)) {
            $agents = $this->em->getRepository(Agent::class)->findBy([
                'id' => $listes,
            ]);

            foreach ($agents as $value) {
                if ('1' === $value->getAlerteConsultationsMonEntite() && !$this->agentService->isAgentInArray($value, $agentsInProcess)) {
                    $agentsInProcess[] = $value;
                }
            }
        }

        return $agentsInProcess;
    }

    private function addFavorisGuestsEntiteeDependante(Agent $agent, array $agentsInProcess): array
    {
        $listes = $this->agentInviterService->getPermanentGuestsEntiteeDependante(
            $agent->getOrganisme(),
            $agent->getService()?->getId()
        );

        if (!empty($listes)) {
            $agents = $this->em->getRepository(Agent::class)->findBy([
                'id' => $listes,
            ]);
            foreach ($agents as $value) {
                if ('1' === $value->getAlerteConsultationsDesEntitesDependantes() && !$this->agentService->isAgentInArray($value, $agentsInProcess)) {
                    $agentsInProcess[] = $value;
                }
            }
        }

        return $agentsInProcess;
    }

    private function addFavorisGuestsEntiteeTransverse(Agent $agent, array $agentsInProcess): array
    {
        $listes = $this->agentInviterService->getPermanentGuestsEntiteeTransverse(
            $agent->getOrganisme(),
            $agent->getService()?->getId()
        );

        if (!empty($listes)) {
            $agents = $this->em->getRepository(Agent::class)->findBy([
                'id' => $listes,
            ]);
            foreach ($agents as $value) {
                if ('1' === $value->getAlerteConsultationsMesEntitesTransverses() && !$this->agentService->isAgentInArray($value, $agentsInProcess)) {
                    $agentsInProcess[] = $value;
                }
            }
        }

        return $agentsInProcess;
    }

    public function addFavorisInvitePonctuel(int $idConsultation, string $orgAcronym, array $agentsInProcess): array
    {
        $agentIds = [];
        $agentIds = $this->guestsService->getInterneConsultation($idConsultation, $orgAcronym, $agentIds);
        $agentIds = $this->guestsService->getInterneConsultationSuiviSeul($idConsultation, $orgAcronym, $agentIds);

        if (!empty($agentIds)) {
            $agents = $this->em->getRepository(Agent::class)->findBy([
                'id' => $agentIds,
            ]);
            foreach ($agents as $agent) {
                if (
                    '1' === $agent->getAlerteConsultationsInvitePonctuel() &&
                    !$this->agentService->isAgentInArray($agent, $agentsInProcess)
                ) {
                    $agentsInProcess[] = $agent;
                }
            }
        }
        $this->em->flush();

        return $agentsInProcess;
    }

    public function setAvisTypeForInitialization(Consultation $consultation): Consultation
    {
        $avisType = $this->em->getRepository(AvisType::class)->findOneBy(['abbreviation' => 'AAPC']);
        $consultation->setAvisType($avisType);

        return $consultation;
    }

    public function setLieuxExecutionForInitialization(Consultation $consultation, array $departements): Consultation
    {
        $consultation->setLieuExecution($this->getLieuExecution($departements));

        return $consultation;
    }

    public function getLieuExecution(?array $departements): string
    {
        $lieuExecution = ',,';

        if (empty($departements)) {
            return $lieuExecution;
        }

        $geolocalisationsN2 = $this->em->getRepository(GeolocalisationN2::class)
            ->findBy(['denomination2' => $departements]);

        foreach ($geolocalisationsN2 as $value) {
            $lieuExecution .= $value->getId() . ',';
        }

        return $lieuExecution;
    }

    public function getDepartmentsWithCodeNuts(Consultation $consultation): array
    {
        $explode = explode(self::CODES_NUTS_SEPARATOR, $consultation->getCodesNuts());

        if (!$explode) {
            return [];
        }

        return array_values(array_filter($explode)) ?? [];
    }

    public function getDepartmentsWithLieuExecution($data, bool $withName = false, bool $withCodeInterface = false): array
    {
        if (!$data) {
            return [];
        }
        $lieux = ($data instanceof  Consultation) ? $data->getLieuExecution() : $data;
        $lieux = explode(',', $lieux);
        $lieux = array_filter($lieux);

        if (empty($lieux)) {
            return [];
        }

        $departments = [];
        $geolocalisationsN2 = $this->em->getRepository(GeolocalisationN2::class)
            ->findBy(['id' => $lieux]);

        foreach ($geolocalisationsN2 as $value) {
            if ($withName) {
                $departments[$value->getDenomination2()] = $value->getDenomination1();
            } elseif ($withCodeInterface) {
                $departments[] = $value->getCodeInterface();
            } else {
                $departments[] = $value->getDenomination2();
            }
        }

        return $departments;
    }

    public function setMessecForInitialization(Consultation $consultation): Consultation
    {
        $configurationPlteforme = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
        $consultation->setVersionMessagerie(1);
        if ($configurationPlteforme->isMessagerieV2()) {
            $consultation->setVersionMessagerie(2);
        }

        return $consultation;
    }

    public function setPFVForInitialization(Consultation $consultation): Consultation
    {
        $plateformeVirtuelle = $this->em
            ->getRepository(PlateformeVirtuelle::class)->findOneBy(
                ['domain' => $_SERVER['SERVER_NAME']]
            );
        if ($plateformeVirtuelle instanceof PlateformeVirtuelle) {
            $consultation->setPlateformeVirtuelle($plateformeVirtuelle);
        }

        return $consultation;
    }

    public function setEmptyValuesForInitialization(Consultation $consultation): Consultation
    {
        $consultation
            ->setTitre('')
            ->setResume('')
            ->setDetailConsultation('')
            /*
             * Les tirages de plan sont utilisés dans l'onglet DCE !
             */
            ->setTiragePlan(0)
            ->setTirageDescriptif('')
            /*
             * Donnée lié au module : ConsultationEchantillonsDemandes
             * + onglet donnée complémentaire !
             * + consultation allotie
             */
            ->setEchantillon('')
            ->setReunion('')
            ->setVisitesLieux('')
            /*
             * Avis pub
             */
            ->setUrlConsultationAvisPub('')
            ->setIsEnvoiPubliciteValidation('')
            ->setAnnexeFinanciere(false)
        ;

        return $consultation;
    }

    public function setDatesForInitialization(Consultation $consultation): Consultation
    {
        $now = new DateTime();
        $defaultDatetime = new DateTime('0000-00-00');
        $defaultString = '0000-00-00 00:00:00';

        $consultation->setDatedebut($now)
            ->setDatevalidation($defaultString)
            ->setDatemiseenligne($defaultDatetime)
            ->setDatefinSad($defaultDatetime)
            ->setDateMiseEnLigneCalcule(null);

        return $consultation;
    }

    public function setMontantMarcheForInitialization(Consultation $consultation, float $montantMarche): Consultation
    {
        $donneeComplementaire = $consultation->getDonneeComplementaire() ?? new DonneeComplementaire();

        $donneeComplementaire->setMontantMarche($montantMarche);

        if (!$consultation->getDonneeComplementaire()) {
            $this->em->persist($donneeComplementaire);
            $consultation->setDonneeComplementaire($donneeComplementaire);
        }

        return $consultation;
    }

    public function setJustificationNonAlloti(Consultation $consultation, string $justificationNonAlloti): Consultation
    {
        $donneeComplementaire = $consultation->getDonneeComplementaire() ?? new DonneeComplementaire();

        $donneeComplementaire->setJustificationNonAlloti($justificationNonAlloti);

        if (!$consultation->getDonneeComplementaire()) {
            $this->em->persist($donneeComplementaire);
            $consultation->setDonneeComplementaire($donneeComplementaire);
        }

        return $consultation;
    }

    /**
     * Nous vérifions ici si l'agent est invité permanent.
     * Si l’agent créateur ne possède pas cette habilitation,
     * alors l’agent est automatiquement inséré comme invité ponctuel de la consultation avec droits de modification.
     */
    public function setInvitePonctuelForInitialization(Consultation $consultation, Agent $agent): void
    {
        $habilitations = $agent->getHabilitation();

        if (!(bool)$habilitations?->getInvitePermanentMonEntite()) {
            $interneConsultation = new InterneConsultation();
            $interneConsultation->setAgent($agent);
            $interneConsultation->setOrganisme($consultation->getOrganisme());

            $this->em->persist($interneConsultation);
            $this->em->flush();

            $interneConsultation->setConsultation($consultation);
        }
    }

    public function setReferenceUtilisateurForInitialization(
        Consultation $consultation,
        Agent $agent,
        ?string $referenceFromWs
    ): Consultation {
        $consultation = $this->referenceTypeService->setReferenceType($consultation, $agent, true);
        if (empty($consultation->getReferenceUtilisateur())) {
            $consultation->setReferenceUtilisateur($referenceFromWs);
        }

        return $consultation;
    }

    public function setConsultationWithParametreProcedure(
        Consultation $consultationFromWs,
        ProcedureEquivalenceAgentService $procedureEquivalenceAgentService
    ): Consultation {
        return $procedureEquivalenceAgentService->initializeConsultationWithParametreProcedure(
            $consultationFromWs,
            self::PARAMETRE_PROCEDURE_LIST_FOR_WS_CONSULTATION
        );
    }

    public function getTypeResponse(Consultation $consultation): string
    {
        $typeResponse = self::TYPE_REPONSE_REFUSEE;

        if ($consultation->getReponseObligatoire()) {
            $typeResponse = self::TYPE_REPONSE_OBLIGATOIRE;
        } elseif ($consultation->getAutoriserReponseElectronique()) {
            $typeResponse = self::TYPE_REPONSE_AUTORISEE;
        }

        return $typeResponse;
    }

    public function getSignatureElectronique(Consultation $consultation): string
    {
        $typeResponse = self::TYPE_REPONSE_NON_REQUISE;
        $signatureOffre = $consultation->getSignatureOffre();
        $autoriserReponseElectronique = $consultation->getAutoriserReponseElectronique();

        if (
            self::SIGNATURE_OFFRE_REQUISE == $signatureOffre
            && self::AUTORISER_REPONSE_ELECTRONIQUE == $autoriserReponseElectronique
        ) {
            $typeResponse = self::TYPE_REPONSE_REQUISE;
        } elseif (
            self::SIGNATURE_OFFRE_AUTORISEE == $signatureOffre
            && self::AUTORISER_REPONSE_ELECTRONIQUE  == $autoriserReponseElectronique
        ) {
            $typeResponse = self::TYPE_REPONSE_AUTORISEE;
        }

        return $typeResponse;
    }
}
