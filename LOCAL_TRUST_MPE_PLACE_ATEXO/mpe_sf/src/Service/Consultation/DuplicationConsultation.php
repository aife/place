<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\ConsultationTags;
use App\Entity\Lot;
use App\Entity\Traduction;
use App\Exception\ApiContentNotCompliantToConsultationValidationRulesException;
use App\Service\ClausesService;
use App\Service\WebServices\ApiEndpointLibrary;
use App\Service\WebServices\DuplicationService;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use PHPUnit\Exception;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class DuplicationConsultation
{
    public function __construct(
        private readonly DuplicationService $duplicationService,
        private readonly ClausesService $clausesService,
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $entity,
        private readonly IriConverterInterface $iriConverter,
        private readonly Security $security,
        private readonly FlashBagInterface $flashBag,
        private readonly TranslatorInterface $translator
    ) {
    }

    /**
     * @throws ReflectionException
     * @throws JsonException
     */
    public function duplicate(Consultation $consultation, Request $request): ?string
    {
        /** @var Agent $agent */
        $agent = $this->security->getUser();

        $this->logger->info(sprintf("Début Duplication consultation : avec l'agent: %s", $agent->getLogin()));

        $donneComplementaireIdRessource = null;
        if ($donneComplementaire = $consultation->getDonneeComplementaire()) {
            $donneComplementaireIdRessource = $this->duplicateDonneeComplementaire($donneComplementaire);
        }

        $query = [
            ApiEndpointLibrary::KEY_DUPLICATE => true,
        ];

        $consultationIdRessource = null;
        $this->logger->info(
            sprintf(
                "Duplication consultation : Début duplication CONSULTATION id: %s",
                $consultation->getId()
            )
        );

        try {
            // Duplication consultation
            $newConsultation = $this->duplicationService->duplicateObjectAndGetNewObject(
                $consultation->getId(),
                ApiEndpointLibrary::URI_CONSULTATIONS,
                $this->duplicationService->getKeysToRemove($consultation),
                $this->duplicationService->getKeysToAdd($consultation, $agent, $donneComplementaireIdRessource),
                $query
            );

            $consultationIdRessource = $this->duplicationService->getIriResourceFromWsResult($newConsultation);
        } catch (ApiContentNotCompliantToConsultationValidationRulesException $e) {
            $this->logger->error(
                sprintf(
                    "Duplication consultation : ERREUR duplication CONSULTATION id: %s",
                    $consultation->getId()
                )
            );

            $this->logger->error(sprintf("Duplication consultation : message erreur %s", $e->getMessage()));

            // Redirection vers la consultation d'origine avec message d'erreur
            $errorMessage = $this->translator->trans("TEXT_MESSAGE_ERROR_DUPLICATION");
            $this->flashBag->add('error', $errorMessage);

            return null;
        } catch (Exception $e) {
            $this->logger->error(
                sprintf(
                    "Duplication consultation : ERREUR duplication CONSULTATION id: %s",
                    $consultation->getId()
                )
            );

            $this->logger->error(sprintf("Duplication consultation : message erreur %s", $e->getMessage()));

            // Redirection vers la consultation d'origine avec message d'erreur
            $errorMessage = $this->translator->trans("TEXT_MESSAGE_ERROR_DUPLICATION");
            $this->flashBag->add('error', $errorMessage);

            return null;
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : Fin duplication CONSULTATION, nouveau id: %s",
                $consultationIdRessource
            )
        );

        // IRI de référence de la nouvelle consultation dupliqué
        $newConsultationObject = $this->iriConverter->getItemFromIri($consultationIdRessource);

        // Duplication lot si la consultation est allotie
        if ($consultation->isAlloti()) {
            $this->duplicateLots($consultation->getLots(), $consultationIdRessource, $request);
        }

        $this->duplicateConsultationTags($consultation, $newConsultationObject);

        $this->flashBag->add(
            'success',
            $this->translator->trans('DUPLICATION_CONSULTATION_SUCCESS')
        );

        return $newConsultationObject?->getId();
    }

    /**
     * @throws ReflectionException
     * @throws JsonException
     */
    public function duplicateDonneeComplementaire(Consultation\DonneeComplementaire $donneComplementaire): ?string
    {
        $this->logger->info(
            sprintf(
                "Duplication consultation : Début duplication Donne Complementaire id: %s",
                $donneComplementaire->getIdDonneeComplementaire()
            )
        );

        $newDonneeComplementaire = $this->duplicationService->duplicateObjectAndGetNewObject(
            $donneComplementaire->getIdDonneeComplementaire(),
            ApiEndpointLibrary::URI_DONNE_COMPLEMENTAIRES,
            ApiEndpointLibrary::KEYS_TO_REMOVE_DONNEE_COMPLEMENTAIRE,
        );

        $donneComplementaireIdRessource = $this->duplicationService->getIriResourceFromWsResult(
            $newDonneeComplementaire
        );

        $this->logger->info(
            sprintf(
                "Duplication consultation : Fin duplication Donne Complementaire, nouveau id: %s",
                $donneComplementaireIdRessource
            )
        );

        // Duplication critere attribution et sous critere attribution
        $critereAttribution = $donneComplementaire->getCriteresAttributionCollection();
        $this->duplicateCritereAttribution($critereAttribution, $donneComplementaireIdRessource);

        // Duplication forme de prix
        $this->duplicateFormePrix($donneComplementaire, $donneComplementaireIdRessource);

        // Duplication Compte pub
        $this->duplicateConsultationComptePub($donneComplementaire, $donneComplementaireIdRessource);

        return $donneComplementaireIdRessource;
    }

    private function duplicateCritereAttribution(
        Collection $critereAttributionCollection,
        string $donneComplementaireIdRessource
    ): void {
        $addKeys = [ApiEndpointLibrary::KEY_DONNEE_COMPLEMENTAIRE => $donneComplementaireIdRessource];

        /** @var Consultation\CritereAttribution $critereAttribution */
        foreach ($critereAttributionCollection as $critereAttribution) {
            $this->logger->info(
                sprintf(
                    "Duplication consultation : Début duplication CritereAttribution id: %s",
                    $critereAttribution->getId()
                )
            );

            $newCritereAttribution = $this->duplicationService->duplicateObjectAndGetNewObject(
                $critereAttribution->getId(),
                ApiEndpointLibrary::URI_CRITERE_ATTRIBUTION,
                ApiEndpointLibrary::KEYS_TO_REMOVE_CRITERE,
                $addKeys,
            );

            $newCritereRessource = $this->duplicationService->getIriResourceFromWsResult($newCritereAttribution);

            $this->logger->info(
                sprintf(
                    "Duplication consultation : Fin duplication CritereAttribution, nouveau id: %s",
                    $newCritereRessource
                )
            );

            // Duplicate Sous Critere Attribution
            if ($sousCritereAttribution = $critereAttribution->getSousCriteresCollection()) {
                $this->duplicateSousCritereAttribution($sousCritereAttribution, $newCritereRessource);
            }
        }
    }

    private function duplicateSousCritereAttribution(Collection $sousCriteres, string $sousCritereRessource): void
    {
        $addKeys = [
            ApiEndpointLibrary::KEY_CRITERE_ATTRIBUTION => $sousCritereRessource,
            ApiEndpointLibrary::KEY_LOT => null,
        ];

        /** @var Consultation\SousCritereAttribution $sousCritereAttribution */
        foreach ($sousCriteres as $sousCritereAttribution) {
            $this->logger->info(
                sprintf(
                    "Duplication consultation : Début duplication SousCritereAttribution id: %s",
                    $sousCritereAttribution->getIdSousCritereAttribution()
                )
            );

            $newSousCritereRessource = $this->duplicationService->duplicateObjectAndGetNewObject(
                $sousCritereAttribution->getIdSousCritereAttribution(),
                ApiEndpointLibrary::URI_SOUS_CRITERE_ATTRIBUTION,
                ApiEndpointLibrary::KEYS_TO_REMOVE_SOUS_CRITERE,
                $addKeys,
            );

            $newSousCritereRessource = $this->duplicationService->getIriResourceFromWsResult($newSousCritereRessource);

            $this->logger->info(
                sprintf(
                    "Duplication consultation : Fin duplication SousCritereAttribution, nouveau id: %s",
                    $newSousCritereRessource
                )
            );
        }
    }

    /**
     * @throws ReflectionException
     * @throws JsonException
     */
    public function duplicateLots(Collection $lots, string $consultationIdResource, Request $request): void
    {
        /** @var Lot $lot */
        foreach ($lots as $lot) {
            $this->logger->info(
                sprintf(
                    "Duplication consultation : Début duplication Lot %s id: %s de la consultation %s",
                    $lot->getLot(),
                    $lot->getId(),
                    $consultationIdResource
                )
            );

            $descriptionTraductionId = $lot->getIdTrDescription()
                ? $this->duplicateTraductionDescription($lot->getIdTrDescription(), $request)
                : null
            ;

            $descriptionTraductionDetailId = $lot->getIdTrDescriptionDetail()
                ? $this->duplicateTraductionDescription($lot->getIdTrDescriptionDetail(), $request)
                : null
            ;

            $donneComplementaireIdRessource = null;
            if ($donneComplementaire = $lot->getDonneeComplementaire()) {
                $donneComplementaireIdRessource = $this->duplicateDonneeComplementaire($donneComplementaire);
            }

            $addKeys = [
                ApiEndpointLibrary::KEY_CONSULTATION => $consultationIdResource,
                ApiEndpointLibrary::KEY_DONNEE_COMPLEMENTAIRE => $donneComplementaireIdRessource,
                ApiEndpointLibrary::KEY_CLAUSES => $this->clausesService->getFormatedClausesForApiInjection($lot),
                ApiEndpointLibrary::KEY_TRADUCTION_DESCRIPTION => $descriptionTraductionId,
                ApiEndpointLibrary::KEY_TRADUCTION_DESCRIPTION_DETAIL => $descriptionTraductionDetailId,
            ];

            try {
                $newLot = $this->duplicationService->duplicateObjectAndGetNewObject(
                    $lot->getId(),
                    ApiEndpointLibrary::URI_LOT,
                    ApiEndpointLibrary::KEYS_TO_REMOVE_LOT,
                    $addKeys
                );

                $iriRessource = $this->duplicationService->getIriResourceFromWsResult($newLot);

                $this->logger->info(
                    sprintf(
                        "Duplication consultation : Fin duplication Lot, nouveau id: %s",
                        $iriRessource
                    )
                );
            } catch (Exception $e) {
                $this->logger->error(
                    sprintf(
                        "Duplication consultation : ERREUR duplication Lot %s id: %s de la consultation %s",
                        $lot->getLot(),
                        $lot->getId(),
                        $consultationIdResource
                    )
                );

                $this->logger->error(sprintf("Duplication consultation : Message erreur %s", $e->getMessage()));
            }
        }
    }

    private function duplicateTraductionDescription(int $idLibelle, Request $request): ?int
    {
        $descriptionTraduction = $this->entity->getRepository(Traduction::class)->findOneBy([
            'idLibelle' => $idLibelle,
            'langue' => $request->getLocale(),
        ]);

        $newDescription = (new Traduction())
            ->setLangue($descriptionTraduction->getLangue())
            ->setLibelle($descriptionTraduction->getLibelle())
        ;

        $this->entity->persist($newDescription);
        $this->entity->flush();

        return $newDescription->getIdLibelle();
    }

    public function duplicateConsultationTags(Consultation $consultation, ?Consultation $newConsultation): void
    {
        if (!$newConsultation || !($tags = $consultation->getTags())) {
            return;
        }

        foreach ($tags as $tag) {
            $newTag = (new ConsultationTags())
                ->setConsultation($newConsultation)
                ->setTagCode($tag->getTagCode())
            ;

            $this->entity->persist($newTag);
        }

        $this->entity->flush();
    }

    public function duplicateFormePrix(
        Consultation\DonneeComplementaire $donneComplementaire,
        string $donneComplementaireIdRessource
    ): void {
        /** @var Consultation\DonneeComplementaire $newDonneComplementaire */
        $newDonneComplementaire = $this->iriConverter->getItemFromIri($donneComplementaireIdRessource);

        if (!$newDonneComplementaire || !($formePrixId = $donneComplementaire->getIdFormePrix())) {
            return;
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : Début duplication FormePrix id: %s",
                $formePrixId
            )
        );

        $newFormePrix = $this->duplicationService->createFormePrix($formePrixId);

        $newDonneComplementaire->setIdFormePrix($newFormePrix->getIdFormePrix());

        $this->entity->persist($newDonneComplementaire);
        $this->entity->flush();

        $this->logger->info(
            sprintf(
                "Duplication consultation : Fin duplication FormePrix, nouveau id: %s",
                $newFormePrix->getIdFormePrix()
            )
        );
    }

    public function duplicateConsultationComptePub(
        Consultation\DonneeComplementaire $donneComplementaire,
        string $donneComplementaireIdRessource
    ): void {
        /** @var Consultation\DonneeComplementaire $newDonneComplementaire */
        $newDonneComplementaire = $this->iriConverter->getItemFromIri($donneComplementaireIdRessource);

        if (!$newDonneComplementaire || !($comptePub = $donneComplementaire->getConsultationComptePub())) {
            return;
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : Début duplication ConsultationComptePub id: %s",
                $comptePub->getId()
            )
        );

        $newComptePub = $this->duplicationService->duplicateComptePub($comptePub, $newDonneComplementaire);

        $this->logger->info(
            sprintf(
                "Duplication consultation : Fin duplication ConsultationComptePub, nouveau id: %s",
                $newComptePub->getId()
            )
        );
    }
}
