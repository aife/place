<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Offre;
use App\Entity\OffrePapier;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CommentService
{
    final public const SEPARATOR = "-";

    public function __construct(
        private readonly EntityManagerInterface $entity,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    public function getCommentByContrat(ContratTitulaire $contratTitulaire): string
    {
        /** @var Etablissement $etablissement */
        $etablissement = $this->entity->getRepository(Etablissement::class)->find(
            $contratTitulaire->getIdTitulaireEtab()
        );

        $comment = sprintf("\n%s", $this->getNomTitulaireContrat($contratTitulaire));

        $codePostal = $etablissement?->getCodePostal() ?? self::SEPARATOR;
        $ville = $etablissement?->getVille() ?? self::SEPARATOR;

        if ($codePostal || $ville) {
            $comment .= sprintf(
                " (%s%s%s)",
                $codePostal,
                ($codePostal && $ville) ? ' - ' : '',
                $ville
            );
        }

        return $comment;
    }
    private function getNomTitulaireContrat(ContratTitulaire $contratTitulaire): ?string
    {
        $entreprise = $this->entity->getRepository(Entreprise::class)->findOneBy(
            ['id' => $contratTitulaire->getIdContratTitulaire()]
        );

        if ($entreprise instanceof Entreprise) {
            $nomTitulaireContrat = $entreprise->getNom() ?? '';
        } else {
            $nomTitulaireContrat = $this->getNomEntrepriseOffre($contratTitulaire);
        }

        return $nomTitulaireContrat;
    }

    private function getNomEntrepriseOffre(ContratTitulaire $contratTitulaire): string
    {
        if ($contratTitulaire->getTypeDepotReponse() == $this->parameterBag->get('TYPE_ENVELOPPE_ELECTRONIQUE')) {
            /** @var Offre $offre */
            $offre = $this->entity->getRepository(Offre::class)->findOneBy(
                [
                    'id' => $contratTitulaire->getIdOffre(),
                    'organisme' => $contratTitulaire->getOrganisme(),
                ]
            );

            $nomTitulaireContrat = $offre?->getNomEntrepriseInscrit();
        } else {
            /** @var OffrePapier $offre */
            $offre = $this->entity->getRepository(OffrePapier::class)->findOneBy(
                [
                    'id' => $contratTitulaire->getIdOffre(),
                    'organisme' => $contratTitulaire->getOrganisme(),
                ]
            );

            $nomTitulaireContrat = $offre?->getNomEntreprise();
        }

        return $nomTitulaireContrat ?? self::SEPARATOR;
    }
}
