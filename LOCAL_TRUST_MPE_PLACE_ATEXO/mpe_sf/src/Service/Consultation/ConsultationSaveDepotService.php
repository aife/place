<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Message\SendEmailAR;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ConsultationSaveDepotService
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly MessageBusInterface $bus
    ) {
    }

    /**
     * @param $params
     */
    public function addJobEmailAR(
        string $referenceConsultation,
        string $idOffre,
        string $idInscrit,
        string $idCandidatureMps
    ): array {
        try {
            $errors = [];

            $this->bus->dispatch(new SendEmailAR(
                $referenceConsultation,
                $idOffre,
                $idInscrit,
                $idCandidatureMps
            ));
        } catch (Exception $exceptionJob) {
            $erreur =
                "Une erreur est survenu lors de l'enregistrement de la tache asynchrones  pour la consultation=> " .
                $referenceConsultation . ' Erreur => ' . $exceptionJob->getMessage() . '   ' .
                $exceptionJob->getTraceAsString();
            $this->logger->error($erreur);
            $errors[] = $erreur;

            $this->em->rollback();
        }

        return $errors;
    }
}
