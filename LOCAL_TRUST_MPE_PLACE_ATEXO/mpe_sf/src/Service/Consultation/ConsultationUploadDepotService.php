<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\Enveloppe;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class ConsultationUploadDepotService
{
    public function __construct(private readonly LoggerInterface $depotLogger, private readonly EntityManagerInterface $em)
    {
    }

    /**
     * Permet de recuperer une enveloppe.
     *
     * @param string $uidResponse unique ID de la reponse
     * @param int $offreId identifiant de l'offre
     * @param int $typeEnveloppe type d'enveloppe
     * @param int $lot numero du lot
     * @param string $org organisme
     *
     *
     * @throws Exception
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2017
     *
     * @copyright Atexo 2017*/
    public function getEnveloppe($uidResponse, $offreId, $typeEnveloppe, $lot, $org): ?Enveloppe
    {
        if (empty($offreId)) {
            $this->depotLogger->error("[uid_response = $uidResponse] L'ID de l'offre est introuvable.");

            throw new Exception("L'ID de l'offre est introuvable");
        }

        if (null === $typeEnveloppe) {
            $this->depotLogger->error("[uid_response = $uidResponse] Le type d'enveloppe est introuvable");

            throw new Exception("Le type d'enveloppe est introuvable");
        }

        if (null === $lot) {
            $this->depotLogger->error("[uid_response = $uidResponse] Le numero du lot est introuvable");

            throw new Exception('Le numero du lot est introuvable');
        }

        if (empty($org)) {
            $this->depotLogger->error("[uid_response = $uidResponse] L'organisme est introuvable");

            throw new Exception("L'organisme est introuvable");
        }

        return $this->em
            ->getRepository(Enveloppe::class)
            ->findOneBy([
                'typeEnv' => $typeEnveloppe,
                'sousPli' => $lot,
                'organisme' => $org,
                'offreId' => $offreId,
            ]);
    }
}
