<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\BloborganismeFile;
use App\Repository\BloborganismeFileRepository;
use App\Service\AtexoFichierOrganisme;
use App\Utils\Filesystem\MountManager;
use League\Flysystem\FileExistsException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

final class UploadDceHelper
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly BloborganismeFileRepository $bloborganismeFileRepository,
        private readonly MountManager $mountManager,
        private readonly AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
    }

    public function getFileByBlobId(int $blobId): ?string
    {
        $blobOrganismeFile = $this->bloborganismeFileRepository->find($blobId);

        if (empty($blobOrganismeFile)) {
            return null;
        }

        $dir = $this->parameterBag->get('BASE_ROOT_DIR') . $blobOrganismeFile->getOrganisme() . '/files/';
        $filename = $blobOrganismeFile->getId() . '-0';

        if ($blobOrganismeFile->getExtension()) {
            $filename .= $blobOrganismeFile->getExtension();
        }

        if (file_exists($dir . $filename)) {
            return $dir . $filename;
        }

        $chemin = null;
        if (($chemin = $blobOrganismeFile->getChemin()) !== null) {
            $dir = $this->parameterBag->get('BASE_ROOT_DIR') . $chemin;
        }

        if (file_exists($dir . $filename)) {
            return $dir . $filename;
        }

        $mnts = $this->parameterBag->get('DOCUMENT_ROOT_DIR_MULTIPLE');
        if ($mnts !== '') {
            $arrayMnts = explode(';', $mnts);
            foreach ($arrayMnts as $mnt) {
                $dir = $mnt . $chemin;

                if (file_exists($dir . $filename)) {
                    return $dir . $filename;
                }
            }
        }

        return null;
    }

    public function normalizeTmpPath($tmpFilePath)
    {
        $res = str_replace($this->parameterBag->get('COMMON_TMP'), '', $tmpFilePath);
        $mnts = $this->parameterBag->get('DOCUMENT_ROOT_DIR_MULTIPLE');
        if (!empty($mnts)) {
            $arrayMnts = explode(';', $mnts);
            foreach ($arrayMnts as $mnt) {
                $mount = substr($mount, 0, -1);
                $res = str_replace('//', '/', $res);
                $res = str_replace($mnt, '', $res);
            }
        } else {
            $res = str_replace($this->parameterBag->get('BASE_ROOT_DIR'), '', $res);
        }

        $upload_dir = ini_get('upload_tmp_dir');

        return str_replace($upload_dir, '', $res);
    }

    /**
     * @throws FileExistsException
     */
    public function moveTmpFile($filePath, Request $request = null)
    {
        $filePath = $this->normalizeTmpPath($filePath);

        if (!empty($request)) {
            $serverTmpName = sprintf('%s-%s', $request->server->get('SERVER_ADDR'), $filePath);
        } else {
            $serverTmpName = $filePath;
        }

        $dir = $this->parameterBag->get('BASE_ROOT_DIR');
        $mnts = $this->parameterBag->get('DOCUMENT_ROOT_DIR_MULTIPLE');
        if (!empty($mnts)) {
            $arrayMnts = explode(';', (string)$mnts);
            if (count($arrayMnts) > 0) {
                $dir = $arrayMnts[0];
            }
        }
        if ($this->mountManager->has('common_tmp://'.$filePath)) {
            $filePath = 'common_tmp://'.$filePath;
        } elseif ($this->mountManager->has('upload_tmp_dir://'.$filePath)) {

            $filePath = 'upload_tmp_dir://'.$filePath;
        } elseif ($this->mountManager->has('nas://'.$filePath)) {
            $filePath = 'nas://'.$filePath;
        } else {
            if (!empty($mnts)) {
                $arrayMnts = explode(';', (string)$mnts);
                if (count($arrayMnts) > 0) {
                    foreach ($arrayMnts as $key => $mount) {
                        if ($key === 0) {
                            continue;
                        }
                        $mount = substr($mount, 0, -1);
                        if ($this->mountManager->has($mount.'://'.$filePath)) {
                            $filePath = $mount.'://'.$filePath;
                            break;
                        }
                    }
                }
            }
        }

        $pathTmpNas = Uuid::uuid4()->toString().'/'.basename($serverTmpName);
        if ($this->mountManager->copy($filePath, 'nas://tmp/'.$pathTmpNas)) {
            $this->atexoFichierOrganisme->setServerTmpName($pathTmpNas);

            return $dir.'tmp/'.$pathTmpNas;
        }

        return false;
    }
}
