<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Service\Agent\Guests;
use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class FavorisService.
 */
class FavorisService
{
    /**
     * FavorisService constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger
    ) {
    }

    public function addConsultationFavoris(Agent $agent, Consultation $consultation): void
    {
        try {
            $consultationFavorisExist = $this->entityManager
                ->getRepository(ConsultationFavoris::class)
                ->findOneBy(['consultation' => $consultation->getId(), 'agent' => $agent->getId()]);

            if (!$consultationFavorisExist instanceof ConsultationFavoris) {
                $consultationFavoris = new ConsultationFavoris();
                $consultationFavoris->setAgent($agent)
                    ->setConsultation($consultation);
                $this->entityManager->persist($consultationFavoris);
            }
        } catch (Exception $exception) {
            $this->logger->error('Error : ' . $exception->getTraceAsString());
        }
    }
}
