<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use App\Entity\CandidatureMps;
use App\Entity\Consultation;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\TCandidature;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TReponseElecFormulaire;
use App\Exception\ConsultationNotAuthorizedException;
use App\Exception\UidNotFoundException;
use App\Service\AtexoConfiguration;
use App\Service\AtexoGroupement;
use App\Service\CandidatureService;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use App\Service\Dume\DumeService;
use App\Service\InscritOperationsTracker;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationDepotService
{
    public final const PHASE_DUME = 'dume';
    public final const CHOICE_ONLINE = 'online';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly SessionInterface $session,
        private readonly AtexoConfiguration $moduleStateChecker,
        private readonly TranslatorInterface $translator,
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $em,
        private readonly CandidatureService $candidatureService,
        private readonly DumeService $dumeService,
        private readonly DossierVolumineuxService $dossiersVolumineuxService,
        private readonly InscritOperationsTracker $inscritOperationTracker
    ) {
    }

    public function checkRepondreApresCloture(Consultation $consultation): bool
    {
        if (
            false === $this->moduleStateChecker->hasConfigPlateforme('entrepriseRepondreConsultationApresCloture')
            && $consultation->getDatefin() < new DateTime()
        ) {
            throw new ConsultationNotAuthorizedException(
                401,
                $this->translator->trans('NON_AUTORISER_DEPOT_REPONSE')
            );
        }

        return true;
    }

    public function checkConsultationRestreinte(Consultation $consultation): bool
    {
        if (
            $this->parameterBag->get('type_acces_procedure')['restreinte'] == $consultation->getTypeAcces() &&
            $consultation->getCodeProcedure() !=
            $this->session->get('contexte_authentification_' . $consultation->getId())['codeAcces']
        ) {
            throw new ConsultationNotAuthorizedException(
                401,
                $this->translator->trans('ACCES_NON_AUTORISE_A_LA_CONSULTATION')
            );
        }

        return true;
    }

    /**
     * Permet de créer une offre.
     *
     * @param $user
     * @param string $uidResponse unique ID du depot
     *
     * @throws UidNotFoundException
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2017
     *
     * @copyright Atexo 2017
     */
    public function createOffre($user, Consultation $consultation, string $uidResponse): Offre
    {
        if (empty($uidResponse)) {
            $this->logger->error("[uid_response = $uidResponse] L'unique ID du depot est introuvable");

            throw new UidNotFoundException("L'unique ID du depot est introuvable");
        }

        $offre = $this->initDefaultFieldsOffre();

        $offre->setUidResponse($uidResponse);
        $consultation->addOffre($offre);
        $user->addOffre($offre);

        $this->em->persist($offre);
        $this->em->flush();

        return $offre;
    }

    public function initDefaultFieldsOffre(): Offre
    {
        $offre = new Offre();

        return $offre
            ->setSignatureenvxml('')
            ->setStatutOffres($this->parameterBag->get('STATUT_ENV_BROUILLON'))
            ->setHorodatage('')
            ->setUntrustedserial(uniqid())
            ->setEnvoiComplet(0)
            ->setEtatChiffrement(0)
            ->setHorodatageEnvoiDiffere('')
            ->setSignatureenvxmlEnvoiDiffere('')
            ->setXmlString('')
            ->setNumeroReponse(0)
            ;
    }

    /**
     * @param $user
     * @param $phase
     * @param $choice
     *
     *
     * @throws Exception
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function updateTypeCandidatureAndRelationListLotCandidature(
        Consultation $consultation,
        $user,
        $phase,
        $choice
    ): TCandidature {
        /** @var ?TCandidature $candidature */
        $candidature = $this->em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $consultation->getAcronymeOrg(),
                'consultation' => $consultation->getId(),
                'idInscrit' => $user->getId(),
                'idEntreprise' => $user->getEntrepriseId(),
                'idEtablissement' => $user->getIdEtablissement(),
                'status' => $this->parameterBag->get('STATUT_ENV_BROUILLON'),
            ]);

        if (null != $candidature) {
            $candidature->setTypeCandidature($phase);
            $candidature->setTypeCandidatureDume($choice);
        } else {
            $candidature = $this->candidatureService->createCandidatureFromConsultation(
                $consultation->getId(),
                $consultation->getAcronymeOrg(),
                $phase,
                $choice
            );
        }

        $this->em->persist($candidature);
        $this->em->flush();

        $listLotsCandidature = $this->em
            ->getRepository(TListeLotsCandidature::class)
            ->findBy([
                'organisme' => $consultation->getAcronymeOrg(),
                'consultation' => $consultation->getId(),
                'idInscrit' => $user->getId(),
                'idEntreprise' => $user->getEntrepriseId(),
                'idEtablissement' => $user->getIdEtablissement(),
                'status' => $this->parameterBag->get('STATUT_ENV_BROUILLON'),
            ]);

        if (null != $listLotsCandidature) {
            foreach ($listLotsCandidature as $value) {
                $value->setCandidature($candidature);

                $this->em->persist($candidature);
            }

            $this->em->flush();
        }

        return $candidature;
    }

    /**
     * get candidature MPS.
     *
     * @param $uidResponse
     * @param $consultationId
     * @param $user
     */
    public function getCandidatureMps($uidResponse, $consultationId, Consultation $consultation, $user): ?CandidatureMps
    {
        $candidatureMPS = null;
        $idCandidatureMps = $this->session->get(
            $this->session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/id_candidature_mps"
        );
        $typeCandidature = $this->session->get(
            $this->session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/type_candidature_mps"
        );

        if (
            $this->moduleStateChecker
                ->hasConfigOrganisme($consultation->getAcronymeOrg(), 'marchePublicSimplifie')
            && $consultation->getMarchePublicSimplifie()
            && 1 == $typeCandidature /*MPS interne*/
            && $idCandidatureMps
        ) {
            $candidatureMPS = $this->em
                ->getRepository(CandidatureMps::class)
                ->getCandidatureMpsBrouillon($idCandidatureMps, $consultation, $user);
        }

        return $candidatureMPS;
    }

    /**
     * @param $phase
     * @param $choice
     * @param $user
     */
    public function getConsultationCandidature($phase, $choice, Consultation $consultation, $user, Offre $offre): array
    {
        $candidatureDume = [];
        $candidatureGroupement = [];
        $candidature = $this->em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $consultation->getAcronymeOrg(),
                'consultation' => $consultation->getId(),
                'idInscrit' => $user->getId(),
                'idEntreprise' => $user->getEntrepriseId(),
                'idEtablissement' => $user->getIdEtablissement(),
                'status' => $this->parameterBag->get('STATUT_ENV_BROUILLON'),
            ]);
        if ($candidature instanceof TCandidature) {
            $conditionGroupement = false;
            $idOffre = $offre->getId();
            $idEntreprise = $user->getEntrepriseId();
            if ($this->moduleStateChecker->hasConfigPlateforme('groupement')) {
                //Information du groupement
                $groupement = $this->em
                    ->getRepository(TGroupementEntreprise::class)
                    ->findOneBy([
                        'candidature' => $candidature,
                    ]);

                if ($idEntreprise && null !== $groupement) {
                    $groupement->setIdOffre($idOffre);
                    $this->em->flush();
                    $conditionGroupement = true;
                }
            }
            if (
                $this->moduleStateChecker->hasConfigPlateforme('interfaceDume')
                && self::PHASE_DUME == $phase
                && self::CHOICE_ONLINE == $choice
            ) {
                //Récupérer le TDumeNumero lié au contexteLtDumeId de la candidature
                $dumeNumero =
                    $this->dumeService->getDumeNumeroFromIdDumeContexte(
                        $candidature->getIdDumeContexte()->getId()
                    );

                $candidatureDume = [
                    'contexteLtDumeId' => $candidature->getIdDumeContexte()->getContexteLtDumeId(),
                    'numeroSN' => $dumeNumero->getNumeroDumeNational(),
                ];
                if ($conditionGroupement) {
                    $listeDumeGroupement = $this->dumeService->getDumeNumeroFromGroupement($idOffre, $idEntreprise);
                    if ($listeDumeGroupement) {
                        $candidatureGroupement['dumeGroupements'] = $listeDumeGroupement;
                    }
                }
            }
        }

        return [
            $candidatureDume,
            $candidatureGroupement,
        ];
    }

    /**
     * @param $user
     */
    public function getConsultationLots(Consultation $consultation, $user, string $msgSelectionLot = ''): array
    {
        $locale = $this->session->get('_locale') ?? $this->parameterBag->get('locale');
        $lots = [];
        if ($consultation->getAlloti()) {
            $listLotsCandidature = $this->em
                ->getRepository(TListeLotsCandidature::class)
                ->getListeLotByUserAndConsultationAndWithCandidature(
                    $user,
                    $consultation,
                    $this->parameterBag->get('STATUT_ENV_BROUILLON')
                );

            foreach ($listLotsCandidature as $lotsCandidature) {
                $lots[] = $lotsCandidature->getNumLot();
            }

            $msgSelectionLot = $this->translator->trans('DEFINE_MESSAGE_LISTE_LOTS_MODIFIABLE_PAGE_DETAIL', [], null, $locale);
        }

        return [$lots, $msgSelectionLot];
    }

    /**
     * paramètre initialisation du groupement.
     *
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function initGroupement(string $uidResponse, int $consultationId, AtexoGroupement $atexoGroupement): array
    {
        $this->session->set($this->session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/groupement", null);
        $this->session->set($this->session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/groupementOui", 0);
        $this->session->set($this->session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/declarerGroupement", 0);

        $this->session->set(
            $this->session->get('token_contexte_authentification_' . $consultationId)
                . "/$uidResponse/idTypeGroupement",
            $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE')
        );

        return $atexoGroupement->getParamsConfig();
    }

    public function getDossierVolumineux(Consultation $consultation): array
    {
        $paramsDossiersVolumineux = ['dossiersVolumineux' => ''];

        if ($consultation->isEnvolActivation()) {
            /** Récupération des dossiers volumineux pour l'inscrit courant. */
            $dossiersVolumineux = $this->dossiersVolumineuxService->getActiveDVforCurrentUser();
            $paramsDossiersVolumineux = ['dossiersVolumineux' => $dossiersVolumineux];
        }

        return $paramsDossiersVolumineux;
    }

    /**
     * @param $dateDebutValidation
     * @throws Exception
     */
    public function addTraceInscrit($dateDebutValidation, Consultation $consultation, ?Offre $offre): void
    {
        [$idValeurReferentielle, $description] = $this->inscritOperationTracker
            ->getInfosValeursReferentielles('DESCRIPTION12');

        if ($consultation instanceof Consultation) {
            $description = str_replace(
                '[__ref_utilisateur__]',
                $consultation->getReferenceUtilisateur(),
                $description
            );
        }

        $idOffre = ($offre instanceof Offre) ? $offre->getId() : null;

        $this->inscritOperationTracker->tracerOperationsInscrit(
            $dateDebutValidation,
            new DateTime(),
            $idValeurReferentielle,
            $description,
            $consultation,
            '',
            false,
            null,
            1,
            null,
            $idOffre
        );
    }

    /**
     * @param $user
     * @param $uidResponse
     * @param $id
     */
    public function getConsultationEnveloppesLots(
        $user,
        $uidResponse,
        $id,
        Consultation $consultation,
        string $msgSelectionLot = ''
    ): array {
        $lots = [];
        $enveloppes = [];
        $arrayDossiers = [];
        $fromCandidatureMPS = false;

        if ($this->moduleStateChecker->hasConfigPlateforme('interfaceModuleSub')) {
            $consultationId = $consultation->getId();
            $organisme = $consultation->getAcronymeOrg();
            $reponseElecFormulaire = $this->em->getRepository(TReponseElecFormulaire::class)
                ->findOneBy(['consultationId' => $consultationId, 'organisme' => $organisme, 'idInscrit' => $user]);

            if ($reponseElecFormulaire instanceof TReponseElecFormulaire) {
                $dossierFormulaires = $reponseElecFormulaire->getTdossierformulaires();

                foreach ($dossierFormulaires as $dossier) {
                    if (
                        $dossier->getFormulaireDepose() ==
                        $this->parameterBag->get('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION')
                        && $dossier->getTypeReponse() ==
                        $this->parameterBag->get('TYPE_DOSSIER_REPONSE_PRINCIPALE')
                    ) {
                        $arrayDossiers[$dossier->getTypeEnveloppe()][$dossier->getIdLot()] = $dossier;
                        $lots[] = $dossier->getIdLot();
                    }
                }
                if ($consultation->getAlloti()) {
                    $enveloppes = $this->em->getRepository(Lot::class)->findLots($consultationId, $organisme, $lots);
                } else {
                    $enveloppes = [$consultation];
                }
            }
        } elseif (
            $consultation->getAlloti()
            && $candidatureMPS =
            $this->getCandidatureMps($uidResponse, $id, $consultation, $user) instanceof CandidatureMps
        ) {
            /** @var $candidatureMPS CandidatureMps */
            $lots = explode(',', (string) $candidatureMPS->getListeLots());
            if (!empty($lots)) {
                $fromCandidatureMPS = true;
                $msgSelectionLot = $this->translator->trans('DEFINE_MESSAGE_LISTE_LOTS_NON_MODIFIABLE');
                $enveloppes = $this->em->getRepository(Lot::class)
                    ->findLots($candidatureMPS->getConsultationId(), $candidatureMPS->getOrganisme(), $lots);
            }
        } else {
            $enveloppes = ($consultation->getAlloti()) ? $consultation->getLots() : [$consultation];
        }

        return [$enveloppes, $arrayDossiers, $fromCandidatureMPS, $msgSelectionLot];
    }
}
