<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Consultation;

use Exception;
use LengthException;
use App\Exception\DataNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchAgentService
{
    private const NAMES = 'names';
    private const TOTALS = 'totals';
    private const TOTAL_CONSULTATION = 'total_consultation';
    private const STATUS_KEY_TRAD = 'status_key_trad';

    public function __construct(private readonly TranslatorInterface $translator, private readonly LoggerInterface $logger)
    {
    }

    /**
     * Get stats for widgets based on the form search.
     * We return an array with 2 keys :
     * One for the name, and the other one for the total
     *
     * @throws Exception
     */
    public function getDataWidgetsStats(object $apiResponse, array $paramUrlFromForm): array
    {
        $response = [];
        if (isset($apiResponse->{'hydra:member'})) {
            $hydraMember = $apiResponse->{'hydra:member'};
            $isTypeProcedure = ($paramUrlFromForm['groupBy'] === 'typeProcedure');
            if ($paramUrlFromForm['groupBy'] == 'categorie' || $isTypeProcedure) {
                $response = $this->getDataWidgetsStatsForCategorieOrTypeProcedure(
                    $hydraMember,
                    $isTypeProcedure
                );
            } else {
                try {
                    $response = $this->getDataWidgetsStatsForStatutCalcule($hydraMember);
                } catch (\Exception $exception) {
                    $this->logger->error($exception->getMessage());
                }
            }
        }

        return $response;
    }

    private function getDataWidgetsStatsForCategorieOrTypeProcedure(array $items, bool $TypeProcedure = false): array
    {
        $response = [];
        $sumCategorie = [];
        foreach ($items as $item) {
            $totalCategorie[$item->{'libelle'}][] = $item->{'total'};
            $sumCategorie = array_map(fn($val) => array_sum($val), $totalCategorie);
        }

        if ($TypeProcedure) {
            arsort($sumCategorie);
        }

        $response[self::NAMES] = array_keys($sumCategorie);
        $response[self::TOTALS] = array_values($sumCategorie);

        return $response;
    }

    /**
     * @throws Exception
     */
    private function getDataWidgetsStatsForStatutCalcule(array $items): array
    {
        try {
            $response = [];
            $statusLabels = array_flip(ConsultationStatus::STATUS);
            $response[self::TOTAL_CONSULTATION] = 0;
            foreach ($items as $item) {
                $response[self::TOTALS][] = $item->{'total'};
                $response[self::TOTAL_CONSULTATION] +=  $item->{'total'};
                $status = explode(',', (string) $item->{'statutCalcule'});

                if (is_countable($status) && count($status) > 1) {
                    $labels = [];
                    $keysTrad = [];
                    foreach ($status as $statusValue) {
                        $labels[] = $this->translator->trans('STATUS_LABEL_' . $statusLabels[$statusValue]);
                        $keysTrad[] = $statusLabels[$statusValue];
                    }
                    $response[self::NAMES][] = implode(' / ', $labels);
                    $response[self::STATUS_KEY_TRAD][] = implode(' / ', $keysTrad);
                } else {
                    $response[self::NAMES][] = $this->translator->trans(
                        'STATUS_LABEL_' . $statusLabels[$item->{'statutCalcule'}]
                    );
                    $response[self::STATUS_KEY_TRAD][] = $statusLabels[$item->{'statutCalcule'}];
                }
            }

            return $this->sortStatutCalcule($response);
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    private function sortStatutCalcule(array $items): array
    {
        $sortedStatusNames = $this->getSortStatusNames();

        if (count($sortedStatusNames) !== count(ConsultationStatus::STATUS_COLORS)) {
            throw new LengthException(
                'Le tableau des statuts et le tableau des couleurs ne sont pas de la même taille'
            );
        }
        if (array_key_exists(self::TOTAL_CONSULTATION, $items) && 0 === $items[self::TOTAL_CONSULTATION]) {
            $items[self::NAMES] = [];
            $items[self::TOTALS] = [];
            $items[self::STATUS_KEY_TRAD] = [];
        } elseif (empty($items[self::NAMES]) && empty($items[self::TOTALS])) {
            throw new DataNotFoundException(
                Response::HTTP_NOT_FOUND,
                'Le tableau des statuts récupérés ou le tableau des totaux récupérés n\'existe pas'
            );
        }

        $data = [];
        $index = 0;

        $names = $items[self::NAMES];
        $totals = $items[self::TOTALS];
        $data[self::TOTAL_CONSULTATION] = $items[self::TOTAL_CONSULTATION];
        $statusKeyTrad = $items[self::STATUS_KEY_TRAD];

        foreach ($sortedStatusNames as $keyName => $nameStatus) {
            if (false !== $key = array_search($nameStatus, $names)) {
                $currentValue = $totals[$index];
                $currentSatutName = $names[$index];
                $totals[$index] = $totals[$key];
                $totals[$key] = $currentValue;
                $currentKeyTrad = $statusKeyTrad[$index];
                $statusKeyTrad[$index] = $statusKeyTrad[$key];
                $statusKeyTrad[$key] = $currentKeyTrad;
                $data[self::TOTALS] = $totals;
                $data[self::NAMES][$index] = $nameStatus;
                $data[self::STATUS_KEY_TRAD] = $statusKeyTrad;
                $names[$key] = $currentSatutName;
                $names[$index] = $nameStatus;
                $data['colors'][] = ConsultationStatus::STATUS_COLORS[$keyName];
                $index++;
            }
        }

        return $data;
    }

    public function getSortStatusNames(): array
    {
        $statutCOD = $this->translator->trans('STATUS_LABEL_CONSULTATION') . ' / '
            .  $this->translator->trans('STATUS_LABEL_OUVERTURE_ANALYSE') . ' / '
            .  $this->translator->trans('STATUS_LABEL_DECISION');

        $statutOD = $this->translator->trans('STATUS_LABEL_OUVERTURE_ANALYSE') . ' / '
            . $this->translator->trans('STATUS_LABEL_DECISION');

        return [
            $this->translator->trans('STATUS_LABEL_ELABORATION'),
            $this->translator->trans('STATUS_LABEL_PREPARATION'),
            $this->translator->trans('STATUS_LABEL_CONSULTATION'),
            $statutCOD,
            $this->translator->trans('STATUS_LABEL_OUVERTURE_ANALYSE'),
            $statutOD,
            $this->translator->trans('STATUS_LABEL_DECISION'),
        ];
    }
}
