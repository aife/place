<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Attribute\ResultType;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebservicesInterfaceSuivi extends AbstractService
{
    private const URI = '/interface-suivis';

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    private function getToken()
    {
        return $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . 'api/v2';
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchMessages(array $query = [])
    {
        $url = $this->getMpeWsBaseUrl() . self::URI;
        $token = $this->getToken();

        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => $query,
        ];

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }
}
