<?php

namespace App\Service;

use App\Traits\ConfigProxyTrait;
use Laminas\Http\Client\Adapter\Curl;
use Exception;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Classe pour gerer l'interface Sub.
 *
 * @author Khalid BENAMAR <kbe@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 0
 *
 * @since   0
 */
class InterfaceSub
{
    use ConfigProxyTrait;

    private $container;
    private $logger = null;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Initialise un objet InterfaceSub.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     *
     * @param array $config
     *                      Exemple:
     *                      $ws = new InterfaceSub($params);
     */
    public function __construct(ContainerInterface $container)
    {
        $this->setContainer($container);
        $this->logger = $container->get('mpe.logger');
    }

    /**
     * Effectue la requete vers le WS SGMAP
     * et retourne le resultat apres traitement du statut.
     *
     * @param string $url    : Url du WS
     * @param array  $params : parametres eventuel a passer au WS
     * @param int    $type   : type d'acces 1 pour entreprise, 2 pour etablissement, ... voir constante de la classe
     *
     * @return array : liste des parametres
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function unlockDossier($clesExterneDossier, $loginInscrit, $consultationRef)
    {
        try {
            $returnAPI = [];
            $config = [
                'adapter' => Curl::class,
                'curloptions' => [
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_TIMEOUT => 30,
                ],
            ];
            if ($configProxy = self::getConfigProxyForLaminasHttpClient($this->container)) {
                $config = array_merge($config, $configProxy);
            }
            $url = $this->container->getParameter('URL_SUB_REST_UNLOCK_DOSSIER');
            $client = new Client($url, $config);
            $client->setAuth($this->container->getParameter('LOGIN_APPLI_ACCES_SUB'), $this->container->getParameter('PASSWORD_APPLI_ACCES_SUB'));
            $paramRequest = [];
            $paramRequest['iddossier'] = $clesExterneDossier;
            $paramRequest['logincompte'] = $loginInscrit;
            $client->setParameterGet($paramRequest);
            $client->setMethod(Request::METHOD_GET);
            $this->logger->info('URL SUB appele ', [$url]);
            $this->logger->info('- Parametre(s) envoye(s) ', $paramRequest);
            $reponseAPI = $client->send();
            $contentAPI = $reponseAPI->getBody();
            $statut = $reponseAPI->getStatusCode();
            $this->logger->info('- Status code reponse ', [$statut]);
            $this->logger->info('- Reponse recu ', [$contentAPI]);
            /*try {
                $returnAPI = json_decode($contentAPI, true);
            } catch(\Exception $e) {
                $this->logger->error("Erreur lors du decodage du json ",  array($e->getMessage()));
            }*/
        } catch (Exception $e) {
            $this->logger->error("Erreur lors de la connexion au web service code [$url] : ", [$e->getMessage()]);
        }
    }
}
