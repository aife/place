<?php

/**
 * Created by PhpStorm.
 * User: fma
 * Date: 19/03/19
 * Time: 09:47.
 */

namespace App\Service;

use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

class AtexoFavoris
{
    /**
     * AtexoAlertes constructor.
     *
     * @param $container
     */
    public function __construct(
        private ContainerInterface $container,
        private EntityManagerInterface $em,
        private readonly LoggerInterface $logger
    ){
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @param $reference
     * @param $user
     *
     * @return array
     */
    public function addFavoris($consultationId, $user)
    {
        $statut = ['statut' => true, 'reponse' => ''];
        try {
            $consultationFavoris = $this->em
                ->getRepository(ConsultationFavoris::class)
                ->findOneBy(['consultation' => $consultationId, 'agent' => $user]);

            if (null === $consultationFavoris) {
                $agent = $this->em
                    ->getRepository(Agent::class)
                    ->find($user);

                $consultation = $this->em
                    ->getRepository(Consultation::class)
                    ->findOneBy(['id' => $consultationId]);

                $consultationFavoris = new ConsultationFavoris();
                $consultationFavoris->setAgent($agent);
                $consultationFavoris->setConsultation($consultation);

                $this->em->persist($consultationFavoris);
                $this->em->flush();

                $statut['reponse'] = 'La consultation a bien été ajouter à vos favoris.';
            } else {
                $statut['statut'] = false;
                $statut['reponse'] = 'La consultation est déjà dans vos favoris.';
            }
        } catch (Exception $exception) {
            $statut['statut'] = false;
            $statut['reponse'] = "Problème technique sur l'ajout d'une consultation favoris.";
            $this->logger->info(
                "Problème technique sur l'ajout d'une consultation favoris :  [Reference :" .
                $consultationId . ', User: ' .
                $user . ']. Message error : ' . $exception->getTraceAsString()
            );
        }

        return $statut;
    }

    public function deleteFavoris($consultationId, $user)
    {
        $statut = ['statut' => true, 'reponse' => ''];
        try {
            $consultationFavoris = $this->em
                ->getRepository(ConsultationFavoris::class)
                ->findOneBy(['consultation' => $consultationId, 'agent' => $user]);

            if (null !== $consultationFavoris) {
                $this->em->remove($consultationFavoris);
                $this->em->flush();
                $statut['reponse'] = 'La consultation a bien été supprimer de vos favoris.';
            }
        } catch (Exception $exception) {
            $statut['statut'] = false;
            $statut['reponse'] = "Problème technique sur la suppression d'une consultation favoris.";
            $this->logger->info(
                "Problème technique sur la suppression d'une consultation favoris :  [Reference :" .
                $consultationId . ', User: ' .
                $user . ']. Message error : ' . $exception->getTraceAsString()
            );
        }

        return $statut;
    }

    public function deleteFavorisByConsultation(int $consultationId): void
    {
        try {
            $consultationsFavoris = $this->em
                ->getRepository(ConsultationFavoris::class)
                ->findBy(['id_consultation' => $consultationId]);


            foreach ($consultationsFavoris as $favoris) {
                $this->em->remove($favoris);
            }

            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
