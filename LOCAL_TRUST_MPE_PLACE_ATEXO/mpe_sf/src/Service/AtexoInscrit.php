<?php

namespace App\Service;

use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Exception;
use Error;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use DateTime;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Message;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\DataNotFoundException;
use App\Model\ApiProblem;
use App\Security\FormAuthenticator;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Classe contenant des fonctions utiles.
 *
 * @author Nahed BEN SALEM <nahed.bensalem@atexo.com>
 * @copyright Atexo 2019
 *
 * @version 0
 *
 * @since   0
 */
class AtexoInscrit
{
    /**
     * @var
     */
    private ?ContainerInterface $container = null;
    protected $config;
    protected $util;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getUtil()
    {
        return $this->util;
    }

    /**
     * @param mixed $util
     */
    public function setUtil($util)
    {
        $this->util = $util;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    public function __construct(
        ContainerInterface $container,
        protected EntityManagerInterface $em,
        AtexoConfiguration $config,
        AtexoUtil $util,
        private GuardAuthenticatorHandler $guardAuthenticatorHandler,
        private FormAuthenticator $formAuthenticator,
        private UserPasswordHasherInterface $userPasswordEncoder,
        private ParameterBagInterface $parameterBag,
        private TranslatorInterface $translator,
        private Environment $twig
    ) {
        $this->setContainer($container);
        $this->setConfig($config);
        $this->setUtil($util);
    }

    /**
     *
     * @return Inscrit
     */
    public function createFromInscription(array $inscritBody, Entreprise $entreprise, Etablissement $etablissement)
    {
        $inscritBody['entrepriseId'] = $entreprise->getId();
        $inscritBody['idEtablissement'] = $etablissement->getIdEtablissement();
        $inscritBody['siret'] = $etablissement->getCodeEtablissement();
        $inscritBody['inscritAnnuaireDefense'] = 0;
        $user = new Inscrit();
        $inscritBody['mdp'] = $this->userPasswordEncoder->hashPassword($user, $inscritBody['mdp']);

        if ($this->isValidationRequiredForRegistration()) {
            $inscritBody['bloque'] = $this->parameterBag->get('ETAT_COMPTE_INSCRIT_INVALIDE');
            $inscritBody['uid'] = md5(uniqid(random_int(0, mt_getrandmax()), true));
        }

        $inscrit = $this->createEntity($inscritBody);

        return $inscrit;
    }

    /**
     * @param $arrayInfoInscrit
     * @param bool $formUpdate
     *
     * @return array
     *
     * @throws Exception
     */
    public function validateInfo(array $arrayInfoInscrit, $formUpdate = false)
    {
        $arrayErreur = [];
        $denormalizedInscrit = $this->serializeInscrit($arrayInfoInscrit);
        $inscritRepo = $this->em->getRepository(Inscrit::class);
        $checkLoginEmail = $inscritRepo->createQueryBuilder('ins')
            ->where('ins.id != :id')
            ->andWhere('ins.login = :login OR ins.email = :email')
            ->setParameter('id', $arrayInfoInscrit['id'] ?? 0)
            ->setParameter('login', $denormalizedInscrit->getLogin())
            ->setParameter('email', $denormalizedInscrit->getEmail())
            ->getQuery()
            ->execute();

        $siret = $denormalizedInscrit->getSiret();
        if (!empty($siret) && !preg_match('/^[0-9]{5}$/', $siret)) {
            $arrayErreur[] = 'Siret non valide';
        }

        if (!$formUpdate && !empty($checkLoginEmail)) {
            $arrayErreur[] = 'Le login ou email existe déjà';
        }
        if (!filter_var($denormalizedInscrit->getEmail(), FILTER_VALIDATE_EMAIL)) {
            $arrayErreur[] = 'Email non valide';
        }

        if ($formUpdate) {
            $this->inscritExist($arrayInfoInscrit, $arrayErreur);
        }

        if (
            empty($denormalizedInscrit->getProfil()) || ($denormalizedInscrit->getProfil() != $this->getContainer()
                    ->getParameter('PROFIL_INSCRIT_ATES')
                && $denormalizedInscrit->getProfil() != $this->getContainer()->getParameter('PROFIL_INSCRIT_USER'))
        ) {
            $arrayErreur[] = 'Profil manque ou invalide, valeur reçue : ' . $denormalizedInscrit->getProfil()
                . '. Les valeurs possible : '
                . $this->getContainer()->getParameter('PROFIL_INSCRIT_USER') . ' , '
                . $this->getContainer()->getParameter('PROFIL_INSCRIT_ATES');
        }

        return $arrayErreur;
    }

    /**
     * @param $arrayInfoInscrit
     *
     * @return Inscrit
     */
    public function serializeInscrit($arrayInfoInscrit)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $inscrit = $serializer->denormalize($arrayInfoInscrit, Inscrit::class);
            if (array_key_exists('motDePasse', $arrayInfoInscrit)) {
                $inscrit->setMdp($arrayInfoInscrit['motDePasse']);
            }
            if (array_key_exists('typeHash', $arrayInfoInscrit)) {
                $inscrit->setTypeHash($arrayInfoInscrit['typeHash']);
            }
            if (array_key_exists('actif', $arrayInfoInscrit)) {
                $inscrit->setBloque($arrayInfoInscrit['actif'] ? '0' : '1');
            }

            // On enregistre les 5 derniers chiffres du siret en
            if (array_key_exists('siret', $arrayInfoInscrit)) {
                $inscrit->setSiret(substr($arrayInfoInscrit['siret'], -5));
            }

            return $inscrit;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function inscritExist(array $arrayInfoInscrit, array &$arrayErreur): void
    {
        $inscritRepo = $this->em->getRepository(Inscrit::class);
        $id = $arrayInfoInscrit['id'];
        $idExterne = $arrayInfoInscrit['idExterne'];

        if (empty($id) && empty($idExterne)) {
            $arrayErreur[] = 'id vide';
        } elseif (
            !empty($id) &&
            !$inscritRepo->find($id) &&
            !empty($idExterne) &&
            !$inscritRepo->findByIdExterne($idExterne)
        ) {
            $arrayErreur[] = "Aucun inscrit avec l'id " . $id;
        }
    }

    /**
     * @param array $node
     *
     * @return Inscrit
     */
    public function createEntity(array $arrayInfoInscrit)
    {
        $inscrit = $this->serializeInscrit($arrayInfoInscrit);

        return $this->createOrUpdateEntity($inscrit, 'create');
    }

    /**
     * @return Inscrit|Inscrit[]|mixed|object|null
     * @throws Exception
     */
    public function updateEntity(array $arrayInfoInscrit)
    {
        $inscrit = $this->serializeInscrit($arrayInfoInscrit);

        return $this->createOrUpdateEntity($inscrit, 'update');
    }

    public function createOrUpdateEntity(Inscrit $inputInscrit, $mode)
    {
        try {
            $inscrit = null;
            $etablissement = null;

            try {
                $inputInscritId = $inputInscrit->getId();
            } catch (Error) {
                $inputInscritId = null;
            }

            // Look for existing Etablissement
            if (!empty($inputInscrit->getIdEtablissement())) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->find($inputInscrit->getIdEtablissement());
            }

            if (!$etablissement instanceof Etablissement) {
                $message = "L'inscrit n'est attaché à aucun établissement";
                if (!empty($inputInscrit->getIdEtablissement())) {
                    $message = "Aucun établissement avec l'id " . $inputInscrit->getIdEtablissement();
                }
                throw new ApiProblemInvalidArgumentException($message);
            }
            if (empty($inputInscrit->getEntrepriseId())) {
                $inputInscrit->setEntrepriseId($etablissement->getIdEntreprise());
            }
            // Look for existing Inscrit
            if ('0' != $inputInscrit->getIdExterne()) {
                $inscrit = $this->em->getRepository(Inscrit::class)->findOneByIdExterne($inputInscrit->getIdExterne());
            } elseif ('update' === $mode && !empty($inputInscritId)) {
                $inscrit = $this->em->getRepository(Inscrit::class)
                    ->findByIdOrOldIdAndEntrepriseId($inputInscritId, $inputInscrit->getEntrepriseId());
            }
            if ('update' === $mode && $inscrit instanceof Inscrit) {
                $inscrit = $this->updateInscrit($inputInscrit);
            } elseif ('create' === $mode) {
                $inscrit = new Inscrit();
                $inscrit = $this->createInscrit($inscrit, $etablissement, $inputInscrit, $mode);
            } else {
                throw new DataNotFoundException(404, ApiProblem::TYPE_NOT_FOUND);
            }

            return $inscrit;
        } catch (ApiProblemException | DataNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            if ($this->em->getConnection()->isTransactionActive()) {
                $this->em->rollback();
            }
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    /**
     * @param $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode(Inscrit $inscrit, Serializer $serializer, $mode)
    {
        $inscritNormalize = $serializer->normalize($inscrit, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($inscritNormalize, $mode);

        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', 'inscrit>', $content);
        }

        return $content;
    }

    /**
     * @param $inscrit
     * @param $etablissement
     * @param $mode
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    protected function createInscrit($inscrit, $etablissement, Inscrit $node, $mode)
    {
        $inscrit->setLogin($node->getLogin())
                ->setMdp($node->getMdp())
                ->setTypeHash($node->getTypeHash())
                ->setIdExterne($node->getIdExterne())
                ->setEmail($node->getEmail())
                ->setNom($node->getNom())
                ->setPrenom($node->getPrenom())
                ->setTelephone($node->getTelephone())
                ->setBloque($node->getBloque())
                ->setUid($node->getUid())
                ->setCodePostal($node->getAdresse()['codePostal'])
                ->setVille($node->getAdresse()['ville'])
                ->setPays($node->getAdresse()['pays'])
                ->setAdresse($node->getAdresse()['rue'])
                ->setEntrepriseId($node->getEntrepriseId())
                ->setIdEtablissement($node->getIdEtablissement())
                ->setSiret($node->getSiret())
                ->setInscritAnnuaireDefense($node->getInscritAnnuaireDefense())
                ->setProfil($node->getProfil());

        $inscrit->setEtablissement($etablissement);
        $inscrit->setEntreprise($etablissement->getEntreprise());
        $inscrit->setEntrepriseId($etablissement->getEntreprise()->getId());

        // set DateCreation and DateModification
        $dateTime = new DateTime();
        $date = $dateTime->format('Y:m:d H:i:s');
        if ('create' === $mode) {
            $inscrit->setDateCreation($date);
        }
        $inscrit->setDateModification($date);

        $this->em->beginTransaction();
        $this->em->persist($inscrit);
        $this->em->flush();
        $this->em->commit();

        return $inscrit;
    }

    protected function updateInscrit(Inscrit $inscrit)
    {
        return $this->em->getRepository(Inscrit::class)
            ->updateInscrit($inscrit);
    }

    /**
     * permet la gestion de la suppression d'un agent.
     * On renseigne la date deleted_at, on passe le nombre de tentative de mdp à 3
     * On historise cette suppression dans la table historisque_suppression_agent.
     *
     * @param bool $callByWS
     *
     * @return |null
     *
     * @throws Exception
     */
    public function deleteEntity(array $arrayInfo, $callByWS = true)
    {
        $inputInscrit = $this->serializeInscrit($arrayInfo);

        try {
            $inputInscritId = $inputInscrit->getId();
        } catch (Error) {
            $inputInscritId = null;
        }

        try {
            $inscrit = null;
            if ('0' != $inputInscrit->getIdExterne()) {
                $inscrit = $this->em->getRepository(Inscrit::class)->findOneByIdExterne($inputInscrit->getIdExterne());
            } elseif (null !== $inputInscritId) {
                $inscrit = $this->em->getRepository(Inscrit::class)
                    ->findByIdOrOldIdAndEntrepriseId($inputInscritId, $inputInscrit->getEntrepriseId());
            }
            if (!$inscrit instanceof Inscrit) {
                throw new DataNotFoundException(404, ApiProblem::TYPE_NOT_FOUND);
            }

            $inscrit->setDeletedAt(new \DateTime("now"));
            $inscrit->setDeleted(true);
            $inscrit->setDateModification((new \DateTime("now"))->format('Y-m-d h:i:s'));
            $inscrit->setLogin($inscrit->getId() . '_' . $inscrit->getLogin());
            $inscrit->setEmail($inscrit->getId() . '_' . $inscrit->getEmail());
            $inscrit->setNom( 'nom_inscrit_' . $inscrit->getId());
            $inscrit->setPrenom('prenom_inscrit_' . $inscrit->getId());
            $inscrit->setMdp('mdp_' . $inscrit->getId());

            $this->em->flush($inscrit);

            return $inscrit;
        } catch (ApiProblemException | DataNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->em->rollback();
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    public function logIn(Request $request, Inscrit $user): Response
    {
        return $this->guardAuthenticatorHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $this->formAuthenticator,
            'inscrit_provider'
        );
    }

    public function emailAlreadyExists(string $email): bool
    {
        $inscrit = $this->em->getRepository(Inscrit::class)->findOneBy(['email' => $email]);

        return !empty($inscrit);
    }

    public function loginAlreadyExists(string $login): bool
    {
        $inscrit = $this->em->getRepository(Inscrit::class)->findOneBy(['login' => $login]);

        return !empty($inscrit);
    }

    public function sendCreationEmail(Inscrit $inscrit, Entreprise $entreprise): void
    {
        $inscritBody = [];
        $mailer = new Swift_Mailer(new Swift_SmtpTransport());
        $beforeEmail = $this->parameterBag->get('PF_SHORT_NAME') . ' - ' .
            $this->parameterBag->get('PF_LONG_NAME');

        $validationRequired = $this->isValidationRequiredForRegistration();
        if ($validationRequired) {
            $inscritBody['bloque'] = $this->parameterBag->get('ETAT_COMPTE_INSCRIT_INVALIDE');
            $inscritBody['uid'] = md5(uniqid(random_int(0, mt_getrandmax()), true));
        }

        $renderData = [
            'plateforme'            => $beforeEmail,
            'couleur_titre'         => $this->parameterBag->get('MAIL_COULEUR_TITRE'),
            'firstname'             => $inscrit->getPrenom(),
            'lastname'              => $inscrit->getNom(),
            'email'                 => $inscrit->getEmail(),
            'login'                 => $inscrit->getLogin(),
            'uid'                   => $inscrit->getUid(),
            'entrepriseName'        => $entreprise->getNom(),
            'validationRequired'    => $validationRequired
        ];
        $twigTemplate = 'email/creation-inscrit.html.twig';
        if ($this->isSpecificEmailEnabledForRegistration()) {
            $renderData['entrepriseAddress'] = $entreprise->getAdresse();
            $renderData['entrepriseCity'] = $entreprise->getVille();
            $renderData['entreprisePhone'] = $entreprise->getTelephone();
            $renderData['entrepriseCnss'] = $entreprise->getCnss();
            $twigTemplate = 'email/creation-inscrit-specific.html.twig';
        }

        $message = (new Swift_Message())
            ->setFrom([$this->parameterBag->get('PF_MAIL_FROM') => $beforeEmail])
            ->setSubject($this->translator->trans('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR'))
            ->setDate(new DateTime('now'))
            ->setTo($inscrit->getEmail())
            ->setBody($this->twig->render($twigTemplate, $renderData), 'text/html');

        $mailer->send($message);
    }

    public function sendCreationEmailForAdmins(Inscrit $inscrit, Entreprise $entreprise): void
    {
        $mailer = new Swift_Mailer(new Swift_SmtpTransport());
        $beforeEmail = $this->parameterBag->get('PF_SHORT_NAME') . ' - ' .
            $this->parameterBag->get('PF_LONG_NAME');

        $admins = $this->em->getRepository(Inscrit::class)
            ->findBy(['entrepriseId' => $entreprise->getId(), 'profil' => Inscrit::PROFIL_ATES]);

        foreach ($admins as $admin) {
            $mainMessage = $this->translator->trans('DEFINE_CORPS_MAIL_CREATION_COMPTE_UTILISATEUR_TO_ADMIN');
            $mainMessage = str_replace(
                '[__NOM_PRENOM_UTILISATEUR__]',
                $inscrit->getPrenom() . ' ' . $inscrit->getNom(),
                $mainMessage
            );
            $mainMessage = str_replace('[__PROFIL__]', $this->translator->trans('DEFINE_UES_SIMPLE'), $mainMessage);
            $mainMessage = str_replace('[__PF__]', $this->parameterBag->get('PF_SHORT_NAME'), $mainMessage);
            $mainMessage = str_replace('[__NOM_SOCIETE__]', $entreprise->getNom(), $mainMessage);

            $message = (new Swift_Message())
                ->setFrom([$this->parameterBag->get('PF_MAIL_FROM') => $beforeEmail])
                ->setSubject($this->translator->trans('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR'))
                ->setDate(new DateTime('now'))
                ->setTo($admin->getEmail())
                ->setBody(
                    $this->twig->render(
                        'email/creation-inscrit-admin.html.twig',
                        [
                            'plateforme'    => $beforeEmail,
                            'couleur_titre' => $this->parameterBag->get('MAIL_COULEUR_TITRE'),
                            'firstname'     => $admin->getPrenom(),
                            'lastname'      => $admin->getNom(),
                            'message'       => $mainMessage
                        ]
                    ),
                    'text/html'
                );

            $mailer->send($message);
        }
    }

    public function isValidationRequiredForRegistration(): bool
    {
        $validationRequired = $this->em->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme()->getEntrepriseValidationEmailInscription();

        return $validationRequired === '1';
    }

    public function isSpecificEmailEnabledForRegistration(): bool
    {
        $specificEmailEnabled = $this->em->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme()->getMailActivationCompteInscritEntreprise();

        return $specificEmailEnabled === '1';
    }

    /**
     * @return string
     */
    public function activateInscritByUid(string $uid)
    {
        $inscrit = $this->em->getRepository(Inscrit::class)->createQueryBuilder('ins')
            ->where('ins.uid = :uid OR ins.uid = :validateuid')
            ->setParameter('uid', $uid)
            ->setParameter('validateuid', "valide_{$uid}")
            ->getQuery()->execute();


        if (!empty($inscrit[0])) {
            $inscrit = $inscrit[0];

            if ($inscrit->getUid() == $uid) {
                $inscrit->setUid("valide_{$uid}");
                $inscrit->setBloque($this->parameterBag->get('ETAT_COMPTE_INSCRIT_VALIDE'));
                $this->em->flush();

                return $this->translator->trans('MESSAGE_CONFIRMATION_VALIDATION_COMPTE_INSCRIT');
            } elseif ($inscrit->getUid() == "valide_{$uid}") {
                return $this->translator->trans('DEFINE_COMPTE_DEJA_VALIDE');
            }
        }

        return 'No inscrit found';
    }

    public function controlInscritCreationFromInscription(array $body): ?string
    {
        $errorMessage = null;
        if (empty($body['inscrit']) || !is_array($body['inscrit'])) {
            $errorMessage = 'Body inscrit is empty or not an array';
        } elseif (empty($body['inscrit']['prenom']) || !is_string($body['inscrit']['prenom'])) {
            $errorMessage = 'Body["inscrit"] prenom is empty or not a string';
        } elseif (empty($body['inscrit']['nom']) || !is_string($body['inscrit']['nom'])) {
            $errorMessage = 'Body["inscrit"] nom is empty or not a string';
        } elseif (empty($body['inscrit']['email']) || !filter_var($body['inscrit']['email'], FILTER_VALIDATE_EMAIL)) {
            $errorMessage = 'Body["inscrit"] email is empty or not valid';
        } elseif (empty($body['inscrit']['telephone']) || !is_string($body['inscrit']['telephone'])) {
            $errorMessage = 'Body["inscrit"] telephone is empty or not a string';
        } elseif (empty($body['inscrit']['login']) || !is_string($body['inscrit']['login'])) {
            $errorMessage = 'Body["inscrit"] login is empty or not a string';
        } elseif (empty($body['inscrit']['mdp']) || !is_string($body['inscrit']['mdp'])) {
            $errorMessage = 'Body["inscrit"] mdp is empty or not a string';
        } elseif ($this->emailAlreadyExists($body['inscrit']['email'])) {
            $errorMessage = 'Inscrit already exists with this email';
        } elseif ($this->loginAlreadyExists($body['inscrit']['login'])) {
            $errorMessage = 'Inscrit already exists with this login';
        }

        return $errorMessage;
    }
}
