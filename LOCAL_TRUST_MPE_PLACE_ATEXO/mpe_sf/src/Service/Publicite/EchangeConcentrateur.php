<?php

namespace App\Service\Publicite;

use App\Entity\Jal;
use App\Exception\AgentTechniqueNotFoundException;
use App\Service\OrganismeService;
use App\Service\WebservicesConcentrateur;
use Application\Propel\Mpe\CommonConsultation;
use Exception;
use DateTime;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\GeolocalisationN2;
use App\Entity\Lot;
use App\Entity\Tranche;
use App\Entity\TypeProcedure;
use App\Exception\ConnexionWsException;
use App\Service\ClientWs\ClientWs;
use App\Service\ContratService;
use App\Service\DonneeComplementaireService;
use Doctrine\ORM\EntityManagerInterface;
use Gesdinet\JWTRefreshTokenBundle\Generator\RefreshTokenGeneratorInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

/**
 * Class EchangeConcentrateur.
 */
class EchangeConcentrateur
{
    final public const PREFIX_BALISE = 'PT_';
    final public const STATUT_EN_ATTENTE = 'EN_ATTENTE';

    public const AGENT_TECHNIQUE_LOGIN = 'agent_concentrateur';
    public const AGENT_TECHNIQUE_TTL_REFRESH_TOKEN = 7200;
    public const ERROR_MSG_CONCENTRATEUR = "Problème s'est produit lors de la communication avec le concentrateur ";

    /**
     * EchangeConcentrateur constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $publiciteLogger,
        private readonly EntityManagerInterface $em,
        private readonly ClientWs $clientWS,
        private readonly DonneeComplementaireService $donneeComplService,
        private readonly Environment $twig,
        private readonly ContratService $contratService,
        private readonly OrganismeService $organismeService,
        private readonly JWTTokenManagerInterface $tokenManager,
        private readonly RefreshTokenGeneratorInterface $refreshTokenGenerator,
        private readonly WebservicesConcentrateur $webservicesConcentrateur
    ) {
    }

    /**
     * @return string|null
     *
     * @throws ConnexionWsException
     */
    public function getToken(int $consultationId = null, string $typeValidation = null)
    {
        $endpoint = $this->parameterBag->get('PF_URL_REFERENCE');
        $endpoint .= 'concentrateur-annonces/rest/v2/oauth/token?grant_type=client_credentials';
        if (!empty($consultationId)) {
            $endpoint .= '&idConsultation=' . $consultationId;
        }
        if (!empty($typeValidation)) {
            $endpoint .= '&typeValidation=' . $typeValidation;
        }
        $login = $this->parameterBag->get('UID_PF_MPE');
        $password = $this->parameterBag->get('PASSWORD_CONCENTRATEUR_PUB');
        $msgErreur = self::ERROR_MSG_CONCENTRATEUR;
        if (!empty($login) && !empty($password)) {
            try {
                $options = ['auth' => [$login, $password]];
                $response = $this->clientWS->post($endpoint, $options);
            } catch (Exception $e) {
                $this->publiciteLogger->error(
                    $msgErreur . $endpoint . ' : ' . $e->getMessage() . ' ' . $e->getTraceAsString()
                );
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $msgErreur);
            }
            $codesOk = [
                Response::HTTP_OK,
                Response::HTTP_CREATED,
                Response::HTTP_ACCEPTED,
            ];
            if (!in_array($response->getStatusCode(), $codesOk)) {
                $this->publiciteLogger->error(
                    $msgErreur . $endpoint
                    . ' code status ' . $response->getStatusCode()
                );
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $msgErreur);
            }
            try {
                $res = $response->getBody()->getContents();
                $tokenJson = json_decode($res, true, 512, JSON_THROW_ON_ERROR);

                return $tokenJson['access_token'];
            } catch (Exception $e) {
                $this->publiciteLogger->error(
                    $msgErreur . $endpoint
                    . ' : ' . $e->getMessage() . ' ' . $e->getTraceAsString()
                );
                $errorMessage = 'Problème lors du formatage du retour du concentrateur';
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $errorMessage);
            }
        }

        return null;
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    public function generateXmlAnnonce(int $idConsultation)
    {
        try {
            $xml = '';
            $consultation = $this->em->getRepository(Consultation::class)->find($idConsultation);
            if ($consultation instanceof Consultation) {
                $params = [];
                $params['consultation'] = $consultation;
                $codeNatureMarche = match ($consultation->getCategorie()) {
                    $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX') => 'WORKS',
                    $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES') => 'SUPPLIES',
                    $this->parameterBag->get('TYPE_PRESTATION_SERVICES') => 'SERVICES',
                };
                $params['codeNatureMarche'] = $codeNatureMarche;
                $donnesCompl = $consultation->getDonneeComplementaire();
                if ($donnesCompl instanceof DonneeComplementaire) {
                    $params['donneeComplCons'] = $donnesCompl;
                    $organisme = $consultation->getOrganisme();
                    $tel = $this->formatTelephone($organisme->getTel());
                    $organisme->setTel($tel ?? '');

                    /** @var Agent $agentCreateur */
                    $agentCreateur = null;
                    if (0 !== $consultation->getIdCreateur() && null !== $consultation->getIdCreateur()) {
                        $agentCreateur = $this->em->getRepository(Agent::class)->find($consultation->getIdCreateur());
                        if (!is_null($agentCreateur)) {
                            $tel = $this->formatTelephone($agentCreateur->getNumTel());
                            $agentCreateur->setNumTel($tel);
                            $params['agentCreateur'] = $agentCreateur;
                        }
                    }

                    $params['infoComptePub'] = $this->donneeComplService
                        ->getPubConsultation($donnesCompl->getIdDonneeComplementaire(), $agentCreateur);
                    $params['formeJuridique'] = $this->donneeComplService->getFormeJuridique($donnesCompl);
                }

                $lots = [];
                if ($consultation->getAlloti()) {
                    $lots = $this->em->getRepository(Lot::class)->findLotsConsultation(
                        $consultation->getId(),
                        $consultation->getAcronymeOrg()
                    );
                }
                $params['lots'] = $lots;
                $params['donneeComplService'] = $this->donneeComplService;
                $params['urlDetailConsultation'] = $this->donneeComplService->getUrlConsultation($consultation);
                $params['organisme'] = $consultation->getOrganisme();
                $params['addInfo'] = $this->donneeComplService->getAddInfo($consultation, $donnesCompl);
                $params['procedure'] = $this->getTypeProcedure($consultation);

                $pfUid = str_replace('_', '', $this->parameterBag->get('UID_PF_MPE'));
                $pfUid = preg_replace('/[^A-Za-z]+/', '', $pfUid);
                if (strlen((string) $pfUid) > 8) {
                    $pfUid = str_replace('MPE', '', $pfUid);
                }
                $pfUid = substr($pfUid, 0, 8);

                $pfUid = strtoupper($pfUid);
                $params['pfUid'] = $pfUid;

                $params['consultationUid'] = $this->getConsultationUid($consultation);

                $params['codesNuts'] = $this->donneeComplService->getArrayCodesNuts($consultation);
                $lieuxExecution = array_filter(explode(',', $consultation->getLieuExecution()));
                if (isset(array_values($lieuxExecution)[0])) {
                    $geolocalisations = $this->em->getRepository(GeolocalisationN2::class)
                        ->getLieuxExecution([array_values($lieuxExecution)[0]]);
                    if (is_array($geolocalisations) && isset($geolocalisations[0])) {
                        $params['lieuExecution'] = $geolocalisations[0]['denomination2'];
                    }
                }

                $hasTranche = false;
                $trancheRepository = $this->em->getRepository(Tranche::class);

                if ($consultation->isAlloti()) {
                    foreach ($consultation->getLots() as $lot) {
                        if (null !== $lot->getDonneeComplementaire()) {
                            $tranches = $trancheRepository->findBy(
                                [
                                    'idDonneeComplementaire' => $lot->getDonneeComplementaire()
                                        ->getIdDonneeComplementaire()
                                ]
                            );

                            if (is_countable($tranches) && count($tranches) > 0) {
                                $hasTranche = true;
                                break;
                            }
                        }
                    }
                } elseif (null !== $consultation->getDonneeComplementaire()) {
                    $tranches = $trancheRepository->findBy(
                        [
                            'idDonneeComplementaire' => $consultation
                                                            ->getDonneeComplementaire()
                                                            ->getIdDonneeComplementaire()
                        ]
                    );

                    $hasTranche =  (is_countable($tranches) && count($tranches) > 0);
                }

                $params['hasTranche'] = $hasTranche;
                $params['techniqueAchat'] = $this->contratService->getTechniqueAchatLibelle($consultation);

                $xml = $this->twig->render('publicite/annonce.joue.xml.twig', $params);
            }

            return $xml;
        } catch (Exception $e) {
            $this->publiciteLogger->error(
                'Problème lors de la génération du xml : ' . $e->getMessage() . $e->getTraceAsString()
            );
            throw $e;
        }
    }

    /**
     * @return string
     */
    public function getTypeProcedure(Consultation $consultation)
    {
        $baliseTP = '';
        $typeProcedure = $consultation->getProcedureType();
        if ($typeProcedure instanceof TypeProcedure) {
            $baliseTP = sprintf('<%s/>', self::PREFIX_BALISE . $typeProcedure->getValueBindingsub());
        }

        return $baliseTP;
    }

    /**
     * @return bool
     *
     * @throws Exception
     */
    public function envoyerAnnonces(int $idConsultation, $platform)
    {
        $envoiOk = false;
        $endpoint = $this->parameterBag->get('PF_URL_REFERENCE');
        $endpoint .= 'concentrateur-annonces/concentrateur-annonces/rest/v2/annonces?idConsultation='
            . $idConsultation . '&idPlatform=' . $platform;
        $token = $this->getToken();
        $msgErreur = "Problème s'est produit lors de la communication avec le concentrateur ";
        if (!empty($token)) {
            $xml = $this->generateXmlAnnonce($idConsultation);
            $consultation = $this->em->getRepository(Consultation::class)->find($idConsultation);
            $json = [
                'statut'    => self::STATUT_EN_ATTENTE,
                'xml'       => $xml,
                'organisme' => $consultation->getOrgDenomination()
            ];
            try {
                $options = [];
                $options['headers'] = ['Authorization' => 'Bearer ' . $token];
                $options['json'] = $json;
                $response = $this->clientWS->put($endpoint, $options);
            } catch (Exception $e) {
                if (!empty($this->publiciteLogger)) {
                    $this->publiciteLogger->error($msgErreur . $endpoint . ' : ' . $e->getMessage() . ' ' .
                        $e->getTraceAsString())
                    ;
                }
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $msgErreur);
            }
            $codesOk = [
                Response::HTTP_OK,
                Response::HTTP_CREATED,
                Response::HTTP_ACCEPTED,
            ];
            if (!in_array($response->getStatusCode(), $codesOk)) {
                $this->publiciteLogger->error(
                    $msgErreur . $endpoint
                    . ' code status ' . $response->getStatusCode()
                );
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $msgErreur);
            }
            try {
                $res = $response->getBody()->getContents();
                $json = json_decode($res, true, 512, JSON_THROW_ON_ERROR);
                $envoiOk = self::STATUT_EN_ATTENTE == $json['statut'];
            } catch (Exception $e) {
                $this->publiciteLogger->error(
                    $msgErreur . $endpoint
                    . ' : ' . $e->getMessage() . ' ' . $e->getTraceAsString()
                );
                $errorMessage = 'Problème lors du formatage du retour du concentrateur';
                throw new ConnexionWsException(Response::HTTP_INTERNAL_SERVER_ERROR, $errorMessage);
            }
        }

        return $envoiOk;
    }

    public function getConsultationUid(Consultation $consultation): string
    {
        $year = 2000;
        if ($consultation->getDatedebut() instanceof DateTime) {
            $year = $consultation->getDatedebut()->format('Y');
        }
        $consultationUid = $year . '-';
        $consultationUid .= str_pad($consultation->getId(), 6, '0', STR_PAD_LEFT);

        return $consultationUid;
    }

    public function formatTelephone(?string $telephone): ?string
    {
        if (empty($telephone)) {
            return null;
        }

        $indicatifNational = '+33';

        if (preg_match('/(\+\d{1,3})\s/', $telephone, $matches, PREG_OFFSET_CAPTURE)) {
            $indicatifNational = $matches[1][0] ?? '';
        }

        if (!empty($indicatifNational)) {
            $telephone = str_replace($indicatifNational, '', $telephone);
            $telephone = ltrim($telephone, '0');
        }

        $telephone = str_replace([' ', '-', '.', '/'], '', $telephone);
        $telephone = $indicatifNational . ' ' . $telephone;

        // Si le format final ne correspond pas au format autorisé par la XSD
        // ou si longueur > 100
        if (
            !preg_match('/^(\+\d{1,3}\s\d+$)/', $telephone)
            || strlen($telephone) > 100
        ) {
            $telephone = null;
        }

        return $telephone;
    }

    public function getDataEnvoi(Agent $agent): array
    {
        $data = [
            'envoi' => []
        ];

        $agentTechnique = $this->em->getRepository(Agent::class)
            ->findAgentTechniqueByLogin(self::AGENT_TECHNIQUE_LOGIN)
        ;

        if (empty($agentTechnique)) {
            throw new AgentTechniqueNotFoundException(
                sprintf('Agent Technique with login %s not found.', self::AGENT_TECHNIQUE_LOGIN)
            );
        }

        $data['envoi']['agent'] = [
            'id'                        => $agent->getId(),
            'identifiant'               => $agent->getLogin(),
            'nom'                       => $agent->getNom(),
            'prenom'                    => $agent->getPrenom(),
            'plateforme'                => $this->parameterBag->get('UID_PF_MPE'),
            'acronymeOrganisme'         => $agent->getAcronymeOrganisme(),
            'email'                     => $agent->getEmail(),
            'telephone'                 => $agent->getNumTel(),
            'fax'                       => $agent->getNumFax(),
            'sigleUrl'                  => '',
            'nomCourantAcheteurPublic'  => $this->organismeService->getNomCourantAcheteurPublic($agent)
        ];

        $refreshToken = $this->refreshTokenGenerator->createForUserWithTtl(
            $agentTechnique,
            self::AGENT_TECHNIQUE_TTL_REFRESH_TOKEN
        );

        $data['envoi']['agent']['api'] = [
            'url'           => rtrim($this->parameterBag->get('PF_URL_REFERENCE'), "/"),
            'token'         => $this->tokenManager->create($agentTechnique),
            'refreshToken'  => $refreshToken->getRefreshToken()
        ];
        $data['envoi']['agent']['organisme'] = [
            'id'                    => $agent->getOrganisme()?->getId() ?? '',
            'acronyme'              => $agent->getOrganisme()?->getAcronyme() ?? '',
            'denominationOrganisme' => $agent->getOrganisme()?->getDenominationOrg() ?? '',
            'siren'                 => $agent->getOrganisme()?->getSiren() ?? '',
            'complement'            => $agent->getOrganisme()?->getComplement() ?? ''
        ];
        $data['envoi']['agent']['service'] = [
            'id'            => $agent->getService()?->getId() ?? '',
            'libelle'       => $agent->getService()?->getLibelle() ?? '',
            'siren'         => $agent->getService()?->getSiren() ?? '',
            'complement'    => $agent->getService()?->getComplement() ?? ''
        ];

        return $data;
    }

    public function getJal(CommonConsultation|Consultation $cons): array
    {
        if ($cons instanceof CommonConsultation) {
            $jals = $this->em->getRepository(Jal::class)->getJallForLux(
                $cons->getOrganisme(),
                $cons->getServiceId()
            );
        } else {
            $jals = $this->em->getRepository(Jal::class)->getJallForLux(
                $cons->getAcronymeOrg(),
                $cons->getService()?->getId()
            );
        }
        $jalsList = [];
        foreach ($jals as $jal) {
            $jalsList[] = [
                'nom' => $jal->getNom(),
                'email' => $jal->getEmail(),
            ];
        }
        return $jalsList;
    }

    public function addAgent(string $token, Agent $agent): void
    {
        if (empty($token)) {
            $this->publiciteLogger->error(
                "Erreur lors de l'ajout d'un agent dans le ws concentrateur-annonces/rest/v2/agents: le token est vide"
            );
        }

        try {
            $this->webservicesConcentrateur->getContent(
                'addAgent',
                $token,
                $this->getDataEnvoi($agent)
            );
        } catch (Exception $e) {
            $this->publiciteLogger->error(
                sprintf(
                    "%s concentrateur-annonces/rest/v2/agents : %s %s",
                    self::ERROR_MSG_CONCENTRATEUR,
                    $e->getMessage(),
                    $e->getTraceAsString()
                )
            );
        }
    }

    public function getConsultationSynchronize(string $token, string $dateDebut, string $dateFin): array
    {
        $endpoint = $this->parameterBag->get('PF_URL_REFERENCE');
        $endpoint .= 'concentrateur-annonces/rest/v2/type-avis-annonces/consultation/synchronize';
        $endpoint .= '?idPlatform=' . $this->parameterBag->get('UID_PF_MPE');
        $endpoint .= '&dateFin=' . htmlspecialchars($dateFin) . '&dateDebut=' . htmlspecialchars($dateDebut);

        return $this->getConcentrateurWSCall($token, $endpoint);
    }

    public function getConsultationAll(string $token, int $consultationId): array
    {
        $endpoint = $this->parameterBag->get('PF_URL_REFERENCE');
        $endpoint .= "concentrateur-annonces/rest/v2/type-avis-annonces/consultation/{$consultationId}/all";
        $endpoint .= '?idPlatform=' . $this->parameterBag->get('UID_PF_MPE');

        return $this->getConcentrateurWSCall($token, $endpoint);
    }

    private function getConcentrateurWSCall(string $token, string $endpoint): array
    {
        try {
            $options = [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token
                ]
            ];
            $response = $this->clientWS->get($endpoint, $options);

            $content = $response->getBody()->getContents();
            $decodedResponse = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->publiciteLogger->error(
                sprintf(
                    "%s %s : %s %s",
                    self::ERROR_MSG_CONCENTRATEUR,
                    $endpoint,
                    $e->getMessage(),
                    $e->getTraceAsString()
                )
            );
            throw $e;
        }

        return $decodedResponse;
    }
}
