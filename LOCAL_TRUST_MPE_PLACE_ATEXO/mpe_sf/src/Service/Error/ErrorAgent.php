<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Error;

use Twig\Environment;

class ErrorAgent
{
    /**
     * ErrorAgent constructor.
     */
    public function __construct(private readonly Environment $twig)
    {
    }

    public function render()
    {
        return $this->twig->render('menuAgent/error.html.twig');
    }
}
