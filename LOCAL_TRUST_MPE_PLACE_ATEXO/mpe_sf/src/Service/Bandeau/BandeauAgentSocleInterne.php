<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau;

use Twig\Environment;

class BandeauAgentSocleInterne
{
    /**
     * BandeauAgent constructor.
     */
    public function __construct(private readonly Environment $twig)
    {
    }

    public function render(): string
    {
        return $this->twig->render('menuAgent/bandeau-agent-socle-interne.html.twig');
    }
}
