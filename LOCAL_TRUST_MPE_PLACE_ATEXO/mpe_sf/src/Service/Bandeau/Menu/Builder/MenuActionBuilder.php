<?php

namespace App\Service\Bandeau\Menu\Builder;

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

use App\Service\Bandeau\Menu\Action\DuplicationConsultation;
use App\Service\Bandeau\Menu\Action\EchangeDoc;
use App\Service\Bandeau\Menu\Action\EspaceDoc;
use App\Service\Bandeau\Menu\Action\MessagerieSecurise;
use App\Service\Bandeau\Menu\Action\Publicite;
use App\Service\Bandeau\Menu\Action\Redaction;
use App\Service\Bandeau\Menu\Action\Spaser;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuActionBuilder extends AbstractMenuBuilder
{
    final public const MENU_ID = 'menu_agent_action';

    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Redaction $redaction,
        protected Publicite $publicite,
        protected MessagerieSecurise $messagerie,
        protected EspaceDoc $espaceDoc,
        protected EchangeDoc $echangeDoc,
        protected Spaser $spaser,
        CurrentUser $currentUser,
        protected readonly DuplicationConsultation $duplicationConsultation,
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'redaction',
                'publicite',
                'messagerie',
                'espaceDoc',
                'echangeDoc',
                'spaser',
                'duplicationConsultation',
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => $this::MENU_ID,
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
