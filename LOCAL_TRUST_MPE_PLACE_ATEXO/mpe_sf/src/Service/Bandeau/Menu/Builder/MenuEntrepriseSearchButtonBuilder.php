<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\Button\Entreprise\Access;
use App\Service\Bandeau\Menu\Button\Entreprise\AccessExterne;
use App\Service\Bandeau\Menu\Button\Entreprise\AjoutPanier;
use App\Service\Bandeau\Menu\Button\Entreprise\ReglementConsultation;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuEntrepriseSearchButtonBuilder extends AbstractMenuBuilder
{
    public const MENU_ID = 'menu_entreprise_button';

    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Access $access,
        protected ReglementConsultation $reglementConsultation,
        protected AccessExterne $accessExterne,
        protected AjoutPanier $ajoutPanier,
        CurrentUser $currentUser
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'access',
                'reglementConsultation',
                'accessExterne',
                'ajoutPanier'
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => $this::MENU_ID,
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
