<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\Administration\Submenus\AdministrationMetier;
use App\Service\Bandeau\Menu\Administration\Submenus\ClausesPersonnalisees;
use App\Service\Bandeau\Menu\Administration\Submenus\ClausierClient;
use App\Service\Bandeau\Menu\Administration\Submenus\ClausierEditeur;
use App\Service\Bandeau\Menu\Administration\Submenus\Parametrage;
use App\Service\Bandeau\Menu\Administration\Submenus\Statistiques;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuAdministrationBuilder extends AbstractMenuBuilder
{
    public final const MENU_ID = 'menu_administration';

    /**
     * MenuAdministrationBuilder constructor.
     */
    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Parametrage $parametrage,
        protected AdministrationMetier $administrationMetier,
        protected ClausesPersonnalisees $clausesPersonnalisees,
        protected ClausierClient $clausierClient,
        protected ClausierEditeur $clausierEditeur,
        protected Statistiques $statistiques,
        CurrentUser $currentUser
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                [
                    'parametrage' => 7,
                    'administrationMetier' => 5,
                ],
                [
                    'clausierEditeur' => 4,
                    'clausierClient' => 4,
                    'clausesPersonnalisees' => 4,
                ],
                ['statistiques' => 12],
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        if (!$this->currentUser->isConnected()) {
            return $this->factory->createItem('root');
        }

        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => $this::MENU_ID,
                ],
            ]
        );

        $rowIndex = 0;

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) use (&$rowIndex) {
                if (is_array($menuItem)) {
                    $row = $params['menu']->addChild('row-' . ++$rowIndex, ['attributes' => ['class' => 'row']]);

                    foreach ($menuItem as $menuItemInBloc => $columns) {
                        if (is_null($columns) || !is_numeric($columns)) {
                            $columns = 4;
                        }

                        $options = array_merge(
                            $params['options'],
                            [
                                'attributes' => [
                                    'class' => "col-md-$columns col-12 mb-2",
                                ],
                            ]
                        );

                        if ($item = $this->$menuItemInBloc->mergeOptions($options)->getItem()) {
                            $row->addChild($item);
                        }
                    }
                } else {
                    if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                        $params['menu']->addChild($item);
                    }
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
