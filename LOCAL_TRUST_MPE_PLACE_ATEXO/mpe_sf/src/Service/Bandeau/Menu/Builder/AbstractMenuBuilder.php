<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\CachedMenuRenderer;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

abstract class AbstractMenuBuilder
{
    public const MENU_ID = 'abstract_menu';

    public function __construct(
        protected CurrentUser $currentUser,
        protected FactoryInterface $factory,
        private readonly SessionInterface $session,
        private readonly CacheInterface $cache,
        protected array $menuItems
    ) {
    }

    public function createMenu(array $options): ItemInterface
    {
        if (!$this->currentUser->isConnected()) {
            return $this->factory->createItem('root');
        }

        $sessionKey = CachedMenuRenderer::SESSION_KEY . '_' . $this->currentUser->getIdAgent();
        $bandeauAgentCacheHash = $this->session->get($sessionKey);

        if (!empty($bandeauAgentCacheHash)
            && $this->cache->hasItem(CachedMenuRenderer::CACHE_KEY_PREFIX . $bandeauAgentCacheHash)
        ) {
            return $this->factory->createItem('root');
        }

        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => 'main-menu-navigation',
                    'class' => 'nav navbar-nav',
                    'data-menu' => 'menu-navigation',
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
