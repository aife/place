<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\Button\Decision;
use App\Service\Bandeau\Menu\Button\Details;
use App\Service\Bandeau\Menu\Button\Modification;
use App\Service\Bandeau\Menu\Button\Reponse;
use App\Service\Bandeau\Menu\Button\Validation;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuButtonBuilder extends AbstractMenuBuilder
{
    public final const MENU_ID = 'menu_agent_button';

    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Details $details,
        protected Modification $modifier,
        protected Reponse $reponse,
        protected Decision $decision,
        protected Validation $valider,
        CurrentUser $currentUser,
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'details',
                'modifier',
                'reponse',
                'decision',
                'valider',
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => $this::MENU_ID,
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
