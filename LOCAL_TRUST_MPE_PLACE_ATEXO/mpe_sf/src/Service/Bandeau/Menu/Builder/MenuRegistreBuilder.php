<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\Registre\Depot;
use App\Service\Bandeau\Menu\Registre\Question;
use App\Service\Bandeau\Menu\Registre\Retrait;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuRegistreBuilder extends AbstractMenuBuilder
{
    public final const MENU_ID = 'menu_agent_register';

    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Retrait $retrait,
        protected Question $question,
        protected Depot $depot,
        CurrentUser $currentUser,
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'retrait',
                'question',
                'depot',
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => $this::MENU_ID,
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
