<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\SocleInterne\Submenus\Parametrage;
use App\Service\Bandeau\Menu\Agent\Submenus\Accueil;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuAgentSocleInterneBuilder extends AbstractMenuBuilder
{
    public final const MENU_ID = 'menu_agent_socle';

    /**
     * MenuAgentBuilder constructor.
     */
    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Accueil $accueil,
        CurrentUser $currentUser,
        protected Parametrage $parametrage
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'accueil',
                'parametrage'
            ]
        );
    }

    public function createMenu(array $options): ItemInterface
    {
        if (!$this->currentUser->isConnected()) {
            return $this->factory->createItem('root');
        }

        $menu = $this->factory->createItem(
            'root',
            [
                'childrenAttributes' => [
                    'id' => 'main-menu-navigation',
                    'class' => 'nav navbar-nav',
                    'data-menu' => 'menu-navigation',
                ],
            ]
        );

        array_walk(
            $this->menuItems,
            function ($menuItem, $key, $params) {
                if ($item = $this->$menuItem->mergeOptions($params['options'])->getItem()) {
                    $params['menu']->addChild($item);
                }
            },
            ['menu' => $menu, 'options' => $options]
        );

        return $menu;
    }
}
