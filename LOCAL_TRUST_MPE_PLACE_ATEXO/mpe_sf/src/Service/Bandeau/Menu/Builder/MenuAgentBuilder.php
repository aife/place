<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Builder;

use App\Service\Bandeau\Menu\Agent\Submenus\Accueil;
use App\Service\Bandeau\Menu\Agent\Submenus\Commission;
use App\Service\Bandeau\Menu\Agent\Submenus\OutilsAide;
use App\Service\Bandeau\Menu\Agent\Submenus\Passation;
use App\Service\Bandeau\Menu\Agent\Submenus\PreparationRedaction;
use App\Service\Bandeau\Menu\Agent\Submenus\Programmation;
use App\Service\Bandeau\Menu\Agent\Submenus\Recensement;
use App\Service\Bandeau\Menu\Agent\Submenus\Sourcing;
use App\Service\Bandeau\Menu\Agent\Submenus\SourcingImage;
use App\Service\Bandeau\Menu\Agent\Submenus\StrategieAchat;
use App\Service\Bandeau\Menu\Agent\Submenus\SuiviContrats;
use App\Service\Bandeau\Menu\Agent\Submenus\ValidationEcoSip;
use App\Service\CurrentUser;
use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Cache\CacheInterface;

class MenuAgentBuilder extends AbstractMenuBuilder
{
    public final const MENU_ID = 'menu_agent';

    /**
     * MenuAgentBuilder constructor.
     */
    public function __construct(
        FactoryInterface $factory,
        SessionInterface $session,
        CacheInterface $cache,
        protected Accueil $accueil,
        protected Recensement $recensement,
        protected Programmation $programmation,
        protected Sourcing $sourcing,
        protected StrategieAchat $strategieAchat,
        protected PreparationRedaction $preparationRedaction,
        protected Passation $passation,
        protected SuiviContrats $suiviContrats,
        protected OutilsAide $outilsAide,
        protected ValidationEcoSip $validationEcoSip,
        CurrentUser $currentUser,
        protected SourcingImage $sourcingImage,
        protected Commission $commission,
    ) {
        parent::__construct(
            $currentUser,
            $factory,
            $session,
            $cache,
            [
                'accueil',
                'recensement',
                'programmation',
                'sourcing',
                'sourcingImage',
                'strategieAchat',
                'preparationRedaction',
                'passation',
                'suiviContrats',
                'outilsAide',
                'validationEcoSip',
                'commission'
            ]
        );
    }
}
