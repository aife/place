<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Registre;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Retrait extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_REGISTRE_RETRAIT';
    public final const ICON = 'ft-upload';
    public final const SEPARATOR = true;

    private int $idConsultation = 0;

    public function getItem(): ?ItemInterface
    {
        $this->idConsultation = $this->options['idConsulation'];
        $this->options['uri'] = '/index.php?page=Agent.GestionRegistres&id='
            . $this->idConsultation . '&type=1';
        $this->init();
        $this->menu->setExtra('count', $this->options['retraitCount']);

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return true;
    }

    public function canClick(string $label): bool
    {
        return $this->hasHabilitationAgent('acces_registre_retraits_papier')
            || $this->hasHabilitationAgent('acces_registre_retraits_electronique')
            || $this->hasHabilitationAgent('suivi_seul_registre_retraits_papier')
            || $this->hasHabilitationAgent('suivi_seul_registre_retraits_electronique')
            ;
    }
}
