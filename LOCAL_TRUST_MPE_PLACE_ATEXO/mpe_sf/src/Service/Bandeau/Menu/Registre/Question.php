<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Registre;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Question extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_REGISTRE_QUESTION';
    public final const ICON = 'ft-help-circle';
    public final const SEPARATOR = true;

    private int $idConsultation = 0;

    public function getItem(): ?ItemInterface
    {
        $this->idConsultation = $this->options['idConsulation'];
        $this->options['uri'] = '/index.php?page=Agent.GestionRegistres&id='
            . $this->idConsultation . '&type=3';
        $this->init();
        $this->menu->setExtra('count', $this->options['questionCount']);

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return true;
    }

    public function canClick(string $label): bool
    {
        return $this->hasHabilitationAgent('acces_registre_questions_papier')
            || $this->hasHabilitationAgent('acces_registre_questions_electronique')
            || $this->hasHabilitationAgent('suivi_seul_registre_questions_papier')
            || $this->hasHabilitationAgent('suivi_seul_registre_questions_electronique')
            ;
    }
}
