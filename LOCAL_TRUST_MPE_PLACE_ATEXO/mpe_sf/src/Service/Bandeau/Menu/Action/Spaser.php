<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Spaser extends AbstractActionSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'MENU_ACTION_SPASER';
    final public const ICON = 'la la-leaf';
    final public const SEPARATOR = true;

    public function getItem(): ?ItemInterface
    {
        $this->options['uri'] = $this->router->generate(
            'agent_spaser_objet_metier_consultation',
            [
                "id" => $this->options['idConsulation']
            ]
        );

        $this->init();
        $this->menu?->setExtra('target', '_blank');

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasConfigurationOrganisme('acces_module_spaser')
            && $this->hasHabilitationAgent('gestion_spaser_consultations', self::PREFIX_METHOD_GETTER_IS)
        ;
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasConfigurationOrganisme('acces_module_spaser')
            && $this->hasHabilitationAgent('gestion_spaser_consultations', self::PREFIX_METHOD_GETTER_IS)
        ;
    }
}
