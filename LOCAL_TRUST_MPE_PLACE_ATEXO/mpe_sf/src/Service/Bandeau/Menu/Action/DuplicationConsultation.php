<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class DuplicationConsultation extends AbstractActionSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'MENU_BUTTON_DUPLICATION_CONSULTATION';
    final public const ICON = 'ft-copy';

    public function getItem(): ?ItemInterface
    {
        $consultation = $this->options['consultation'];

        $this->options['uri'] = $this->router->generate(
            'agent_consultations_duplication',
            ['consultation' => $consultation->getId()]
        );
        $this->options['linkAttributes']['target'] = '_blank';

        $this->init();

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasHabilitationAgent('duplication_consultations', 'is');
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasHabilitationAgent('duplication_consultations', 'is');
    }
}
