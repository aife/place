<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class EspaceDoc extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_ACTION_ESPACE_DOC';
    public final const ICON = 'ft-folder';

    public function getItem(): ?ItemInterface
    {
        $idConsultation = $this->options['idConsulation'];
        $url = $this->router->generate('agent_espace_documentaire_consultation', ['id' => $idConsultation]);
        $this->options['uri'] = 'javascript:window.open(\''
            . $url . '\',\'850px\',\'800px\',\'yes\'); javascript:void(0);';
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasConfigurationOrganisme('espace_documentaire');
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasHabilitationAgent('espace_documentaire_consultation', 'is');
    }
}
