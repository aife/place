<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Entity\Consultation;
use App\Entity\ProcedureEquivalence;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Publicite extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_ACTION_PUBLICITE';
    public final const ICON = 'ft-file-text';
    public final const SEPARATOR = true;

    private int $idConsultation = 0;
    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->idConsultation = $this->options['idConsulation'];
        $this->consultation = $this->options['consultation'];

        $this->options['uri'] = '/index.php?page=Agent.PubliciteConsultation&id='
            . base64_encode($this->idConsultation) . '&fullWidth=true';
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
       return  $this->hasHabilitationPublicite($this->consultation->getIdTypeProcedureOrg(), $this->consultation->getOrganisme()->getAcronyme());
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return !$this->isReadOnlyUser($this->idConsultation) && $this->hasHabilitationAgent('publier_consultation');
    }

    protected function hasHabilitationPublicite($idTypeProcedureOrg, $organisme): bool
    {
        $isHabilitation = false;
        if (null === $idTypeProcedureOrg) {
            return $isHabilitation;
        }

        $procedureEquivalence = $this->em->getRepository(ProcedureEquivalence::class)->findOneBy([
            'idTypeProcedure' => $idTypeProcedureOrg,
            'organisme' => $organisme,
        ]);

        if (
            !empty($procedureEquivalence)
            && ('+1' === $procedureEquivalence->getProcedurePublicite()
            || '1' === $procedureEquivalence->getProcedurePublicite())
        ) {
            $isHabilitation = true;
        }

        return $isHabilitation;
    }
}
