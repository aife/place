<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class MessagerieSecurise extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_ACTION_MESSAGERIE_SECURISE';
    public final const ICON = 'ft-mail';
    public final const SEPARATOR = true;

    private int $idConsultation = 0;

    public function getItem(): ?ItemInterface
    {
        $this->idConsultation = $this->options['idConsulation'];
        $this->options['uri'] = '/index.php?page=Agent.DetailConsultation&id='
        . $this->idConsultation . '&suivi=1';
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return true;
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return !$this->isReadOnlyUser($this->idConsultation) && ($this->hasHabilitationAgent('suivre_message')
            || $this->hasHabilitationAgent('envoyer_message'));
    }
}
