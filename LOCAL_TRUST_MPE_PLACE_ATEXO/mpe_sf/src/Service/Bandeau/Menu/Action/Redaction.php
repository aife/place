<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Redaction extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_ACTION_REDACTION';
    public final const ICON = 'ft-edit';

    private bool $redactionActive = false;
    private int $idConsultation = 0;

    public function getItem(): ?ItemInterface
    {
        $this->idConsultation = $this->options['idConsulation'];
        $this->redactionActive = $this->options['redactionActive'] ?? false;

        $this->options['uri'] = $this->routeBuilder
            ->getRoute('agent_consultations_redaction_pieces_get_by_id', ['id' => $this->idConsultation]);
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return true === $this->redactionActive && $this->hasConfigurationOrganisme('interface_module_rsem');
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return !$this->isReadOnlyUser($this->idConsultation)
            && ($this->hasHabilitationAgent('redaction_documents_redac')
                || $this->hasHabilitationAgent('validation_documents_redac'));
    }
}
