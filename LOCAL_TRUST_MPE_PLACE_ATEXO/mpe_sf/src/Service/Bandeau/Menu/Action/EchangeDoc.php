<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Action;

use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class EchangeDoc extends AbstractActionSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'MENU_ACTION_ECHANGE_DOC';
    final public const ICON = 'ft-external-link';
    final public const SEPARATOR = true;

    public function getItem(): ?ItemInterface
    {
        $idConsultation = $this->options['idConsulation'];
        $this->options['uri'] = $this->router->generate(
            'echange_documentaire_index',
            ['consultationId' => $this->encryption->cryptId($idConsultation)]
        );

        $this->init();
        $this->menu?->setExtra('target', '_blank');

        return $this->menu;
    }

    public function canSee(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasConfigurationOrganisme('echanges_documents');
    }

    public function canClick(string $label = self::TRANSLATION_PREFIX): bool
    {
        return $this->hasHabilitationAgent('acces_echange_documentaire', 'is');
    }
}
