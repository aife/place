<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Commission extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_COMMISSION';
    public final const ICON = 'fa-calendar';

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('GESTION_DES_SEANCES');
        $this->addLink(
            'FORMULAIRE_SEANCE',
            '/?page=Commission.FormulaireSeance&creation'
        );

        $this->addLink(
            'SUIVI_SEANCE',
            '/?page=Commission.SuiviSeance'
        );


        $this->addSubmenu('ADMINISTRATION');
        $this->addLink(
            'LISTE_COMMISSIONS',
            '/?page=Commission.ListeCommissions'
        );

        $this->addLink(
            'LISTE_INTERVENANTS_EXTERNES',
            '/?page=Commission.ListeIntervenantsExternes'
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        switch ($label) {
            case $this::TRANSLATION_PREFIX:
            case 'GESTION_DES_SEANCES':
            case 'FORMULAIRE_SEANCE':
            case 'ADMINISTRATION':
            case 'SUIVI_SEANCE':
            case 'LISTE_COMMISSIONS':
            case 'LISTE_INTERVENANTS_EXTERNES':
                return $this->hasConfigurationOrganisme('cao');
            default:
                return false;
        }
    }

    public function canClick(string $label): bool
    {
        switch ($label) {
            case $this::TRANSLATION_PREFIX:
            case 'GESTION_DES_SEANCES':
            case 'FORMULAIRE_SEANCE':
            case 'ADMINISTRATION':
            case 'SUIVI_SEANCE':
            case 'LISTE_COMMISSIONS':
            case 'LISTE_INTERVENANTS_EXTERNES':
                return $this->hasConfigurationOrganisme('cao');
            default:
                return false;
        }
    }
}
