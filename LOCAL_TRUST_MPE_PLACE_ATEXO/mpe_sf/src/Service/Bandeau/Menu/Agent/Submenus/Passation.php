<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Passation extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_PASSATION';
    public final const ICON = 'fa-globe';

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('CONSULTATIONS');

        $fullWidth = $this->parameterBag->get('ACTIVE_ELABORATION_V2') ? '&fullWidth=true' : '';
        $this->addLink('CONSULTATIONS_CREER', '/?page=Agent.FormulaireConsultation' . $fullWidth);
        $this->addLink(
            'CONSULTATIONS_SIMPLIFIEE_CREER',
            $this->router->generate('atexo_consultation_simplifie_create')
        );
        $this->addForm(
            'CONSULTATIONS_RECHERCHE_RAPIDE',
            '/',
            'keyWord',
            'CONSULTATIONS_RECHERCHE_RAPIDE_OK',
            ['page' => 'Agent.TableauDeBord']
        );
        $this->addLink('CONSULTATIONS_TOUTES', '/agent/consultation/recherche?search=true');
        $this->addLink('CONSULTATIONS_RMA_TOUTES', '/agent/consultation/recherche?rma=1&keyWord=&statut%5B%5D=2&statut%5B%5D=3&statut%5B%5D=4&search=');
        $this->addLink('RECHERCHE_AVANCEE', '/agent/consultation/recherche');
        $this->addLink('MES_RECHERCHES_SAUVEGARDEES', '/index.php?page=Agent.AgentGestionRecherches');
        $this->addLink('GERER_MES_RECHERCHES', '/?page=Agent.AgentGestionRecherches');

        $this->addSubmenu('AUTRES_ANNONCES');
        $this->addLink('CREER', '/?page=Agent.FormulaireAnnonce');
        $this->addLink(
            'TOUTES_LES_ANNONCES_D_INFORMATION',
            '/?page=Agent.TableauDeBordAnnonce&typeAnnonce='
            . $this->parameterBag->get('TYPE_AVIS_INFORMATION')
        );
        $this->addLink(
            'TOUS_LES_EXTRAITS_DE_PV',
            '?page=Agent.TableauDeBordAnnonce&typeAnnonce='
            . $this->parameterBag->get('TYPE_AVIS_EXTRAIT_PV')
        );
        $this->addLink(
            'TOUTES_LES_ANNONCES_D_ATTRIBUTION',
            '/?page=Agent.TableauDeBordAnnonce&typeAnnonce='
            . $this->parameterBag->get('TYPE_AVIS_ATTRIBUTION')
        );
        $this->addLink(
            'TOUS_LES_RAPPORTS_D_ACHEVEMENT',
            '/?page=Agent.TableauDeBordAnnonce&typeAnnonce='
            . $this->parameterBag->get('TYPE_AVIS_RAPPORT_ACHEVEMENT')
        );
        $this->addLink(
            'TOUTES_LES_DECISIONS_DE_RESILIATION',
            '/?page=Agent.TableauDeBordAnnonce&typeAnnonce='
            . $this->parameterBag->get('TYPE_AVIS_DECISION_RESILIATION')
        );
        $this->addLink('ANNONCE_DE_PROGRAMME_PREVISIONNEL', '/?page=Agent.ListeProgrammePrevisionnel');
        $this->addLink('ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT', '/?page=Agent.ListeSyntheseRapportAudit');
        $this->addLink('AUTRES_RECHERCHE_AVANCEE', '/?page=Agent.TableauDeBordAnnonce&AS=1&annonce');
        $this->addLink('AUTRES_GERER_MES_RECHERCHES', '/index.php?page=Agent.AgentGestionRecherches');

        $this->addSubmenu('OPERATION');
        $this->addLink('OPERATION_CREER', '/?page=Agent.FormulaireOperation&idOperation');
        $this->addLink('TOUTES_LES_OPERATIONS', '/?page=Agent.SearchOperations');

        $this->addSubmenu('ENCHERES', [], !$this->canSee('OPERATION'));
        $this->addLink('ENCHERES_CREER', '/?page=Agent.GererEnchere');
        $this->addLink('SUIVRE', '/?page=Agent.RechercherEncheres');

        $this->addSubmenu('DOSSIER_VOLUMINEUX');
        $this->addLink('DOSSIER_VOLUMINEUX_URI', '/?page=Agent.AgentDossierVolumineux');
        $this->addLink('PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL', '/');

        $this->addSubmenu('ARCHIVES');
        $this->addForm(
            'ARCHIVES_RECHERCHE_RAPIDE',
            '/',
            'keyWord',
            'ARCHIVES_RECHERCHE_RAPIDE_OK',
            ['page' => 'Agent.ArchiveTableauDeBord']
        );
        $this->addLink('ARCHIVES_TOUTES_LES_CONSULTATIONS', '/?page=Agent.ArchiveTableauDeBord&AS=0');
        $this->addLink('RECHERCHE_AVANCEE_ARCHIVES', '/?page=Agent.ArchiveTableauDeBord&AS=1&searchAnnCons&archive');
        $this->addLink('TELECHARGEMENTS_DES_ARCHIVES', '/?page=Agent.downloadArchives');
        $this->addLink('MES_TELECHARGEMENT_ASYNCHRONES', '/?page=Agent.MesTelechargements');
        $this->addLink('TOUT_LES_TELECHARGEMENTS_ASYNCHRONES', '/?page=Agent.TousLesTelechargement');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'CONSULTATIONS', 'CONSULTATIONS_CREER', 'CONSULTATIONS_RECHERCHE_RAPIDE',
            'CONSULTATIONS_TOUTES',
            'RECHERCHE_AVANCEE', 'AUTRES_ANNONCES', 'TOUTES_LES_ANNONCES_D_ATTRIBUTION', 'AUTRES_RECHERCHE_AVANCEE',
            'ARCHIVES', 'ARCHIVES_RECHERCHE_RAPIDE', 'MES_TELECHARGEMENT_ASYNCHRONES',
            'TOUT_LES_TELECHARGEMENTS_ASYNCHRONES', 'RECHERCHE_AVANCEE_ARCHIVES', 'ARCHIVES_TOUTES_LES_CONSULTATIONS'
            => true,
            'CONSULTATIONS_SIMPLIFIEE_CREER' => $this->hasConfigurationOrganisme('consultation_simplifiee'),
            'CONSULTATIONS_RMA_TOUTES' => $this->hasConfigurationOrganisme('profil_rma'),
            'GERER_MES_RECHERCHES', 'AUTRES_GERER_MES_RECHERCHES' =>
            $this->hasConfigurationOrganisme('recherches_favorites_agent'),
            'CREER' => $this->hasConfigurationPlateforme('creer_autre_annonce'),
            'TOUTES_LES_ANNONCES_D_INFORMATION' =>
            $this->hasConfigurationPlateforme('autre_annonce_information'),
            'TOUS_LES_EXTRAITS_DE_PV' =>
            $this->hasConfigurationPlateforme('autre_annonce_extrait_pv'),
            'TOUS_LES_RAPPORTS_D_ACHEVEMENT' =>
            $this->hasConfigurationPlateforme('autre_annonce_rapport_achevement'),
            'TOUTES_LES_DECISIONS_DE_RESILIATION' =>
            $this->hasConfigurationPlateforme('autre_annonce_decision_resiliation'),
            'ANNONCE_DE_PROGRAMME_PREVISIONNEL' =>
            $this->hasConfigurationPlateforme('autre_annonce_programme_previsionnel'),
            'ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT' =>
            $this->hasConfigurationPlateforme('autre_annonce_synthese_rapport_audit'),
            'OPERATION', 'OPERATION_CREER', 'TOUTES_LES_OPERATIONS' =>
            $this->hasConfigurationOrganisme('gestion_operations'),
            'ENCHERES', 'ENCHERES_CREER', 'SUIVRE' =>
            $this->hasConfigurationOrganisme('encheres'),
            'DOSSIER_VOLUMINEUX' =>
                $this->hasConfigurationPlateforme('menu_agent_complet') ||
                $this->hasConfigurationOrganisme('module_envol'),
            'DOSSIER_VOLUMINEUX_URI' => $this->hasConfigurationOrganisme('module_envol'),
            'PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL' =>
                $this->hasConfigurationPlateforme('menu_agent_complet') &&
                !$this->hasConfigurationOrganisme('module_envol'),
            'TELECHARGEMENTS_DES_ARCHIVES' => $this->hasConfigurationOrganisme('archivage_consultation_sur_pf'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'CONSULTATIONS_RECHERCHE_RAPIDE', 'CONSULTATIONS_TOUTES', 'RECHERCHE_AVANCEE', 'CREER',
            'TOUTES_LES_ANNONCES_D_INFORMATION', 'TOUS_LES_EXTRAITS_DE_PV', 'TOUS_LES_RAPPORTS_D_ACHEVEMENT',
            'TOUTES_LES_ANNONCES_D_ATTRIBUTION', 'TOUTES_LES_DECISIONS_DE_RESILIATION',
            'ANNONCE_DE_PROGRAMME_PREVISIONNEL', 'ANNONCE_DE_SYNTHESE_DE_RAPPORT_D_AUDIT', 'AUTRES_RECHERCHE_AVANCEE',
            'TOUTES_LES_OPERATIONS', 'PLUS_D_INFORMATION_SUR_LE_MODULE_ENVOL' => true,
            'CONSULTATIONS_CREER' => $this->hasHabilitationAgent('gerer_mapa_inferieur_montant')
                || $this->hasHabilitationAgent('gerer_mapa_superieur_montant')
                || $this->hasHabilitationAgent('administrer_procedures_formalisees'),
            'CONSULTATIONS_SIMPLIFIEE_CREER' => $this->hasHabilitationAgent('administrer_procedures_simplifiees')
                && $this->hasConfigurationOrganisme('consultation_simplifiee'),
            'CONSULTATIONS_RMA_TOUTES' => $this->hasHabilitationAgent('profil_rma'),
            'OPERATION_CREER' => $this->hasHabilitationAgent('gerer_operations'),
            'ENCHERES_CREER' => $this->hasHabilitationAgent('gerer_encheres'),
            'SUIVRE' => $this->hasHabilitationAgent('suivre_encheres'),
            'TELECHARGEMENTS_DES_ARCHIVES' => $this->hasHabilitationAgent('download_archives'),
            'ARCHIVES_RECHERCHE_RAPIDE', 'RECHERCHE_AVANCEE_ARCHIVES', 'MES_TELECHARGEMENT_ASYNCHRONES',
            'ARCHIVES_TOUTES_LES_CONSULTATIONS' => $this->hasHabilitationAgent('gerer_archives'),
            'TOUT_LES_TELECHARGEMENTS_ASYNCHRONES' =>
            $this->hasHabilitationAgent('acceder_tous_telechargements'),
            'DOSSIER_VOLUMINEUX_URI' => $this->hasHabilitationAgent('gestion_envol', 'is'),
            'GERER_MES_RECHERCHES', 'AUTRES_GERER_MES_RECHERCHES' =>
            $this->hasConfigurationOrganisme('recherches_favorites_agent'),
            default => false,
        };
    }
}
