<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\AgentService;
use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SuiviContrats extends AbstractSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'MENU_AGENT_SUIVI_CONTRATS';
    final public const ICON = 'fa-briefcase';

    public function getItem(): ?ItemInterface
    {
        $option = [];
        $this->init();

        $this->addSubmenu('CONTRATS_ET_DONNEES_ESSENTIELLES');

        if ($this->parameterBag->get('ACTIVE_EXEC_V2')) {
            $this->addLink(
                'ACCORD_CADRE_ET_SAD',
                $this->routeBuilder->getRoute('agent_web_component_contrat_list_type_sad')
            );
            $this->addForm(
                'CONTRATS_RECHERCHE_RAPIDE',
                '/',
                'keyWord',
                'CONTRATS_RECHERCHE_RAPIDE_OK',
                ['page' => 'Agent.ResultatRechercheContrat']
            );
            $this->addLink(
                'TOUS_LES_CONTRATS',
                $this->routeBuilder->getRoute('agent_web_component_contrat_list')
            );
            $this->addLink(
                'RECHERCHE_AVANCEE',
                $this->routeBuilder->getRoute('agent_web_component_contrat_list', ['advanceSearch' => true])
            );
            $this->addLink(
                'SAISIR_UN_CONTRAT',
                $this->routeBuilder->getRoute('agent_web_component_contrat_create')
            );
        } else {
            $this->addLink('ACCORD_CADRE_ET_SAD', '/?page=Agent.ResultatRechercheContrat&searchAcSad');
            $this->addForm(
                'CONTRATS_RECHERCHE_RAPIDE',
                '/',
                'keyWord',
                'CONTRATS_RECHERCHE_RAPIDE_OK',
                ['page' => 'Agent.ResultatRechercheContrat']
            );
            $this->addLink('TOUS_LES_CONTRATS', '/?page=Agent.ResultatRechercheContrat');
            $this->addLink('RECHERCHE_AVANCEE', '/?page=Agent.ResultatRechercheContrat&advanceSearch');
            $this->addLink('SAISIR_UN_CONTRAT', '/?page=Agent.FormulaireContrat');

            $urlExec = $this->agentService->getUrlSSoExec($this->currentUser);
            $option['extras'] = ['target' => '_blank'];
            $this->addSubMenu('EXECUTION_DES_CONTRATS');
            $this->addLink('ACCEDER_AU_MODULE_EXEC', $urlExec, $option);
            $this->addLink('PLUS_INFO_SUR_LE_MODULE_EXEC', '/');
        }

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'CONTRATS_ET_DONNEES_ESSENTIELLES', 'CONTRATS_RECHERCHE_RAPIDE', 'ACCORD_CADRE_ET_SAD', 'TOUS_LES_CONTRATS', 'RECHERCHE_AVANCEE', 'SAISIR_UN_CONTRAT' => true,
            'EXECUTION_DES_CONTRATS' => $this->hasConfigurationPlateforme('menu_agent_complet') ||
                $this->hasConfigurationOrganisme('module_exec'),
            'ACCEDER_AU_MODULE_EXEC' => $this->hasConfigurationOrganisme('module_exec'),
            'PLUS_INFO_SUR_LE_MODULE_EXEC' => $this->hasConfigurationPlateforme('menu_agent_complet') &&
                !$this->hasConfigurationOrganisme('module_exec'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'CONTRATS_RECHERCHE_RAPIDE', 'ACCORD_CADRE_ET_SAD', 'TOUS_LES_CONTRATS', 'RECHERCHE_AVANCEE', 'PLUS_INFO_SUR_LE_MODULE_EXEC' => true,
            'SAISIR_UN_CONTRAT' => $this->hasHabilitationAgent('creer_contrat'),
            default => false,
        };
    }
}
