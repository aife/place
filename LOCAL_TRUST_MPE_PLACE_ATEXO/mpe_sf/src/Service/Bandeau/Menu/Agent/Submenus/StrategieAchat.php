<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class StrategieAchat extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_STRATEGIE_ACHAT';
    public final const ICON = 'fa-map';

    public function getItem(): ?ItemInterface
    {
        $this->options['uri'] = $this->parameterBag->get('PF_URL') . 'agent/recensement/nukema';
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX => $this->hasConfigurationPlateforme('menu_agent_complet') || (
                    $this->hasConfigurationOrganisme('module_recensement_programmation') &&
                    (
                        $this->hasHabilitationAgent('strategie_achat_gestion') ||
                        $this->hasHabilitationAgent('recensement_programmation_administration')
                    )
                ),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return false;
    }
}
