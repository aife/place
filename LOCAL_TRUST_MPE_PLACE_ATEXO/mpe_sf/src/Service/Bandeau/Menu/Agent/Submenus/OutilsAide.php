<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class OutilsAide extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_OUTILS_AIDE';
    public final const ICON = 'fa-wrench';

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('OUTILS_DE_SIGNATURE');
        $this->addLink('SIGNER_UN_DOCUMENT', $this->router->generate('agent_signer_document'));
        $this->addLink('VERIFIER_LA_SIGNATURE', $this->router->generate('verification_signature'));
        $this->addSubmenu('OUTILS_INFORMATIQUES');

        $this->addLink(
            'DECHIFFREMENT_EN_LIGNE',
            $this->parameterBag->get('URL_DOCS_OUTILS_HORS_LIGNE')
            .'/outil-dechiffrement-hors-ligne.zip'
        );

        if ($this->hasConfigurationOrganisme('cms_actif')) {
            $this->addLink(
                'DECHIFFREMENT_EN_LIGNE',
                $this->router->generate('aides_dechiffrement_hors_ligne', [ 'calledFrom' =>'agent'])
            );
        }

        $this->addLink('AUTRES_OUTILS', '/agent/aide/outils-de-formation-informatiques');
        $this->addLink('CERTIFICATS', '/?page=Agent.Certificats&callFrom=agent');
        $this->addSubmenu('SE_PREPARER_A_SE_DEPOUILLER');
        $this->addLink('TESTER_LA_CONFIGURATION_DE_MON_POSTE', '/?page=Agent.DiagnosticPoste&callFrom=agent');
        $this->addSubmenu('ANNUAIRE_ACHETEUR_PUBLIC');
        $this->addLink('ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER', '/?page=Agent.SearchAchteursPublics');
        $this->addSubmenu('ANNUAIRE_FOURNISSEUR');
        $this->addLink('ANNUAIRE_FOURNISSEUR_RECHERCHER', '/?page=Agent.SearchCompany');
        $this->addSubmenu('AIDE');
        $this->addLink('GUIDE_UTILISATION', '/agent/aide/guide-utilisation');
        $this->addLink('ASSISTANCE_TELEPHONIQUE', '/agent/aide/assistance');
        $this->addLink('DOCUMENTS_DE_REFERENCE', '/?page=Agent.DocumentsDeReference&callFrom=agent');
        $this->addLink('ABREVIATIONS_GLOSSAIRES', '/agent/aide/glossaire');
        $this->addLink('OUTILS_DE_FORMATION', '/agent/aide/outils-de-formation');
        $this->addLink('MODULE_AUTO_FORMATION', '/?page=Agent.AgentAutoformation');
        $this->addLink(
            'TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME',
            '/?page=Agent.TableauVersions',
            ['remove_prefix' => true]
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX,
            'OUTILS_DE_SIGNATURE',
            'SIGNER_UN_DOCUMENT',
            'VERIFIER_LA_SIGNATURE',
            'OUTILS_INFORMATIQUES',
            'DECHIFFREMENT_EN_LIGNE',
            'AUTRES_OUTILS',
            'SE_PREPARER_A_SE_DEPOUILLER',
            'TESTER_LA_CONFIGURATION_DE_MON_POSTE',
            'ANNUAIRE_FOURNISSEUR',
            'ANNUAIRE_FOURNISSEUR_RECHERCHER',
            'AIDE', 'GUIDE_UTILISATION',
            'ASSISTANCE_TELEPHONIQUE',
            'ABREVIATIONS_GLOSSAIRES',
            'OUTILS_DE_FORMATION' => true,
            'CERTIFICATS' => $this->hasConfigurationPlateforme('module_certificat'),
            'ANNUAIRE_ACHETEUR_PUBLIC',
            'ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER' => $this->hasConfigurationPlateforme(
                'annuaire_acheteurs_publics'
            ),
            'DOCUMENTS_DE_REFERENCE' => $this->hasConfigurationPlateforme('documents_reference'),
            'MODULE_AUTO_FORMATION' => $this->hasConfigurationPlateforme('modules_autoformation'),
            'TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME' =>
                'TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME' !== $this->translateLabel(
                    'TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME'
                ),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'SIGNER_UN_DOCUMENT',
            'TEXT_DECOUVREZ_DERNIERES_FONCTIONNALITES_PLATEFORME',
            'VERIFIER_LA_SIGNATURE', 'DECHIFFREMENT_EN_LIGNE',
            'AUTRES_OUTILS', 'CERTIFICATS',
            'TESTER_LA_CONFIGURATION_DE_MON_POSTE',
            'GUIDE_UTILISATION',
            'ASSISTANCE_TELEPHONIQUE',
            'ABREVIATIONS_GLOSSAIRES',
            'OUTILS_DE_FORMATION',
            'DOCUMENTS_DE_REFERENCE',
            'ANNUAIRE_ACHETEUR_PUBLIC',
            'MODULE_AUTO_FORMATION' => true,
            'ANNUAIRE_FOURNISSEUR_RECHERCHER' => $this->hasHabilitationAgent('suivi_entreprise'),
            'ANNUAIRE_ACHETEUR_PUBLIC_RECHERCHER' => $this->hasHabilitationAgent('annuaire_acheteur'),
            default => false,
        };
    }
}
