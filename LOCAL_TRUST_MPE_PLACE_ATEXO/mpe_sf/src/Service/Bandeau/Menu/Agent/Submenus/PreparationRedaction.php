<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class PreparationRedaction extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_PREPARATION_REDACTION';
    public final const ICON = 'fa-pencil-alt';

    public function getItem(): ?ItemInterface
    {
        $this->init();
        $this->addSubmenu('BASE_DCE');
        $this->addLink('RECHERCHE_AVANCEE', '/?page=Agent.Nukema', [
            'linkAttributes' => ['target' => '_blank']
        ]);
        $this->addLink('PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', '/', [
            'linkAttributes' => ['target' => '_blank']
        ]);
        $this->addSubmenu('MES_CLAUSES_PERSONNALISEES');
        $this->addLink('MES_CLAUSES_PERSONNALISEES_CLAUSES', '/agent/redaction/parametrage/clauses');
        $this->addLink(
            'MES_CLAUSES_PERSONNALISEES_GABARITS',
            '/?page=Agent.RedacClausier&cp=AdministrerGabaritsClauseAgent'
        );
        $this->addSubmenu('REDACTION_DES_PIECES');
        $this->addLink('REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC', '/');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX => $this->hasConfigurationPlateforme('menu_agent_complet')
                || $this->hasConfigurationOrganisme('base_dce')
                || $this->hasConfigurationOrganisme('interface_module_rsem'),
            'BASE_DCE' => $this->hasConfigurationPlateforme('menu_agent_complet') ||
                $this->hasConfigurationOrganisme('base_dce'),
            'RECHERCHE_AVANCEE' => $this->hasConfigurationOrganisme('base_dce'),
            'PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE' => $this->hasConfigurationPlateforme('menu_agent_complet') &&
                !$this->hasConfigurationOrganisme('base_dce'),
            'MES_CLAUSES_PERSONNALISEES', 'MES_CLAUSES_PERSONNALISEES_CLAUSES', 'MES_CLAUSES_PERSONNALISEES_GABARITS' => $this->hasConfigurationOrganisme('interface_module_rsem'),
            'REDACTION_DES_PIECES', 'REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC' => $this->hasConfigurationPlateforme('menu_agent_complet') &&
                !$this->hasConfigurationOrganisme('interface_module_rsem'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'RECHERCHE_AVANCEE', 'PLUS_D_INFO_SUR_LE_MODULE_BASE_DCE', 'REDACTION_DES_PIECES_IMAGAGE_DU_MODULE_REDAC', 'REDACTION_DES_PIECES_PLUS_D_INFO_SUR_LE_MODULE_REDAC', 'MES_CLAUSES_PERSONNALISEES_CLAUSES' => true,
            'MES_CLAUSES_PERSONNALISEES_GABARITS' => $this->hasHabilitationAgent('gerer_gabarit_agent'),
            default => false,
        };
    }
}
