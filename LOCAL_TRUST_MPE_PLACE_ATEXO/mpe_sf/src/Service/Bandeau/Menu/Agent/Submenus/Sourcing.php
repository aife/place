<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Sourcing extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_SOURCING';
    public final const ICON = 'fa-search';
    public final const HAS_SUBMENU = false;

    public function getItem(): ?ItemInterface
    {
        $this->options['uri'] = $this->parameterBag->get('PF_URL') . 'agent/sourcing/nukema';
        $this->options['linkAttributes']['target'] = '_blank';

        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX => $this->hasConfigurationOrganisme('module_sourcing'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return false;
    }
}
