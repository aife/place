<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Accueil extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'TEXT_ACCUEIL';
    public final const ICON = 'fa-home';
    public final const HAS_SUBMENU = false;

    public function getItem(): ?ItemInterface
    {
        $this->options['uri'] =
            $this->hasConfigurationPlateforme('socle_interne')
                ? $this->router->generate('atexo_agent_accueil_socle_interne_index')
                : $this->router->generate('atexo_agent_accueil_index')
        ;

        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return $label === $this::TRANSLATION_PREFIX;
    }

    public function canClick(string $label): bool
    {
        return false;
    }
}
