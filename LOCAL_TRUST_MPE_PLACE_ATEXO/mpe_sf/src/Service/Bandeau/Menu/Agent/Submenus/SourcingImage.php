<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 *
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class SourcingImage extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_SOURCING';
    public final const ICON = 'fa-search';

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('TEXT_SOUS_MENU');
        $this->addLink('LIEN_INFO', '/');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'TEXT_SOUS_MENU', 'LIEN_INFO' => $this->hasConfigurationPlateforme('menu_agent_complet')
                && !$this->hasConfigurationOrganisme('module_sourcing'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return false;
    }
}
