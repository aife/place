<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Agent\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class ValidationEcoSip extends AbstractSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'VALIDATION';
    final public const ICON = 'fa-check-circle';

    public function getItem(): ?ItemInterface
    {
        $this->init();
        $this->addSubmenu('VALIDATION');
        $this->addLink('VALIDATION_ECO', $this->routeBuilder->getRoute('agent_annonces_validation_eco'));
        $this->addLink('VALIDATION_SIP', $this->routeBuilder->getRoute('agent_annonces_validation_sip'));

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        if (!$this->hasConfigurationOrganisme('module_eco_sip')) {
            return false;
        }

        return match ($label) {
            $this::TRANSLATION_PREFIX,
            'VALIDATION_ECO_SIP' => $this->hasHabilitationAgent('gestion_validation_eco', 'is') ||
                                    $this->hasHabilitationAgent('gestion_validation_sip', 'is'),
            'VALIDATION_ECO' => $this->hasHabilitationAgent('gestion_validation_eco', 'is'),
            'VALIDATION_SIP' => $this->hasHabilitationAgent('gestion_validation_sip', 'is'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        if (!$this->hasConfigurationOrganisme('module_eco_sip')) {
            return false;
        }

        return match ($label) {
            'VALIDATION_ECO' => $this->hasHabilitationAgent('gestion_validation_eco', 'is'),
            'VALIDATION_SIP' => $this->hasHabilitationAgent('gestion_validation_sip', 'is'),
            default => false,
        };
    }
}
