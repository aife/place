<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\SocleInterne\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Parametrage extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_SOCLE_INTERNE';
    public final const ICON = '';

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('PORTAIL');
        $this->addLink('ADMINISTRATEUR_AGENT', '/?page=Agent.GestionAgents&all=1');

        $this->addSubmenu('SERVICE');
        $this->addLink('GESTION_SERVICE', '/?page=Agent.GestionEntitesAchat');
        $this->addLink('GESTION_AGENTS', '/?page=Agent.GestionAgents&all=0');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        switch ($label) {
            case $this::TRANSLATION_PREFIX:
            case 'PORTAIL':
            case 'ADMINISTRATEUR_AGENT':
            case 'SERVICE':
            case 'GESTION_SERVICE':
            case 'GESTION_AGENTS':
                return true;
            default:
                return false;
        }
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'GESTION_SERVICE' => $this->hasHabilitationAgent('gestion_agent_pole'),
            'ADMINISTRATEUR_AGENT' => $this->hasHabilitationAgent('hyper_admin'),
            'GESTION_AGENTS' => $this->hasHabilitationAgent('gestion_agents'),
            default => false,
        };
    }
}
