<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Details extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_BUTTON_DETAILS';
    public final const ICON = 'ft-search';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = '/index.php?page=Agent.DetailConsultation&id='
            . $this->consultation->getId();
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return true;
    }

    public function canClick(string $label): bool
    {
        return true;
    }
}
