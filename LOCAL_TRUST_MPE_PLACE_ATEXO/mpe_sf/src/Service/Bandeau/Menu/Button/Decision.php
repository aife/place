<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use App\Service\Consultation\ConsultationStatus;
use Knp\Menu\ItemInterface;

class Decision extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_BUTTON_DECISION';
    public final const ICON = 'ft-award';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = '/index.php?page=Agent.ouvertureEtAnalyse&id='
            . $this->consultation->getId() . '&decision&fullWidth=true';
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return in_array(ConsultationStatus::STATUS['DECISION'], $this->consultation->statutCalcule);
    }

    public function canClick(string $label): bool
    {
        return !$this->isReadOnlyUser($this->consultation->getId())
            && ($this->hasHabilitationAgent('attribution_marche')
                || $this->hasHabilitationAgent('decision_suivi_seul'));
    }
}
