<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button\Entreprise;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class AccessExterne extends AbstractActionSubmenu implements SubmenuInterface
{
    public const TRANSLATION_PREFIX = 'TEXT_ACCEDER_CONSULTATION_PROFIL_ACHETEUR';
    public const ICON = 'ft-log-in';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = $this->consultation->getUrlConsultationExterne();
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return $this->consultation->getConsultationExterne() == '1'
            && !empty($this->consultation->getUrlConsultationExterne());
    }

    public function canClick(string $label): bool
    {
        return true;
    }
}
