<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button\Entreprise;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use App\Service\DocumentService;
use Knp\Menu\ItemInterface;
use Symfony\Contracts\Service\Attribute\Required;


class ReglementConsultation extends AbstractActionSubmenu implements SubmenuInterface
{
    private $documentService;

    /**
     * @param mixed $documentService
     * @return ReglementConsultation
     */
    #[Required]
    public function setDocumentService(DocumentService  $documentService)
    {
        $this->documentService = $documentService;
        return $this;
    }
    public const TRANSLATION_PREFIX = 'TELECHARGER_REGLEMENT_CONSULTATION';
    public const ICON = 'ft-download';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = $this->documentService->getLienRcByIdConsultation($this->consultation->getId());
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return !empty($this->documentService->getLienRcByIdConsultation($this->consultation->getId()));
    }

    public function canClick(string $label): bool
    {
        return true;
    }
}
