<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button\Entreprise;

use App\Entity\Consultation;
use App\Entity\PanierEntreprise;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class AjoutPanier extends AbstractActionSubmenu implements SubmenuInterface
{
    public const TRANSLATION_PREFIX = 'DEFINE_AJOUTER_PANIER';
    public const ICON = 'ft-shopping-cart';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        if ($this->currentUser->isConnected()) {
            $url = "../../index.php?page=Entreprise.popUpGestionPanier&id=".base64_encode($this->consultation->getId())."&orgAcronyme={$this->consultation->getAcronymeOrg()}&action=ajout";
            $this->options['uri'] = "javascript:window.open('"
                . $url . "','700px','80px','yes'); javascript:void(0);";

        } else {
            $this->options['uri'] = '../../index.php?page=Entreprise.EntrepriseHome&goto='.urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie');
        }

        $this->init();
        $this->menu->setExtra('isHref', true);

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return true;
    }

    public function canClick(string $label): bool
    {
        if ($this->currentUser->isConnected()) {
            $panierRepository = $this->em->getRepository(PanierEntreprise::class);
            $data = $panierRepository->getPanierEntreprise(
                $this->consultation->getOrganisme(),
                $this->consultation->getId(),
                $this->currentUser->getIdEntreprise(),
                $this->currentUser->getIdInscrit()
            );
            if (!empty($data)) {
                return false;
            }
        }

        return true;
    }
}
