<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button\Entreprise;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Access extends AbstractActionSubmenu implements SubmenuInterface
{
    public const TRANSLATION_PREFIX = 'DEFINE_ACCEDER_CONSULTATION';
    public const ICON = 'ft-log-in';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = "/entreprise/consultation/{$this->consultation->getId()}?orgAcronyme={$this->consultation->getAcronymeOrg()}";
        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return $this->consultation->getConsultationExterne() == '0';
    }

    public function canClick(string $label): bool
    {
        return true;
    }
}
