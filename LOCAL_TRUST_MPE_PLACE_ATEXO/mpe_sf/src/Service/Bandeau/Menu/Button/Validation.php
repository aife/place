<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button;

use App\Entity\Consultation;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use App\Service\Consultation\ConsultationStatus;
use Knp\Menu\ItemInterface;

class Validation extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_BUTTON_VALIDER';
    public final const ICON = 'ft-check-circle';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        $this->options['uri'] = '/index.php?page=Agent.ValiderConsultation&id='
            . $this->consultation->getId();
        $this->init();
        $this->menu?->setExtra('regleMiseEnLigne', $this->consultation->getRegleMiseEnLigne());
        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return in_array(ConsultationStatus::STATUS['PREPARATION'], $this->consultation->statutCalcule);
    }

    public function canClick(string $label): bool
    {
        return $this->registre->consultationHasValidationHabilitation($this->consultation, $this->currentUser);
    }
}
