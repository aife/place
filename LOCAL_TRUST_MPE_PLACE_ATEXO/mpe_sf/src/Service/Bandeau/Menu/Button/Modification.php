<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Button;

use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Service\Bandeau\Menu\Submenus\AbstractActionSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use App\Service\Consultation\ConsultationStatus;
use Knp\Menu\ItemInterface;

class Modification extends AbstractActionSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_BUTTON_MODIFIER';
    public final const ICON = 'ft-edit-2';

    private ?Consultation $consultation = null;

    public function getItem(): ?ItemInterface
    {
        $this->consultation = $this->options['consultation'];
        if (
            $this->parameterBag->get('ACTIVE_ELABORATION_V2')
            && (
                array_intersect($this->consultation->statutCalcule, [
                    ConsultationStatus::STATUS['ELABORATION'],
                    ConsultationStatus::STATUS['PREPARATION'],
                ])
                || 1 == $this->parameterBag->get('PERMETTRE_ACCES_FORMULAIRE_AMONT_EN_MODIFICATION_APRES_VALIDATION')
            )
        ) {
            $this->options['uri'] = $this->router
                ->generate('agent_consultation_modification', ['uuid' => $this->consultation->getUuid()]);
            if (
                ($this->getStatus($this->consultation) !== $this->parameterBag->get('STATUS_PREPARATION')
                && $this->getStatus($this->consultation) !== $this->parameterBag->get('STATUS_ELABORATION'))
                || (0 == $this->parameterBag->get('PERMETTRE_ACCES_FORMULAIRE_AMONT_EN_MODIFICATION_APRES_VALIDATION')
                    && 1 == $this->consultation->getEtatValidation())
            ) {
                $this->options['uri'] = $this->router
                    ->generate(
                        'agent_consultation_modification_en_ligne',
                        ['uuid' => $this->consultation->getUuid()]
                    );
            }
        } else {
            $this->options['uri'] = '/index.php?page=Agent.FormulaireConsultation&id='
                . base64_encode($this->consultation->getId());
        }

        $this->init();

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return !empty(array_intersect($this->consultation->statutCalcule, [
            ConsultationStatus::STATUS['ELABORATION'],
            ConsultationStatus::STATUS['PREPARATION'],
            ConsultationStatus::STATUS['CONSULTATION'],
        ]));
    }

    public function canClick(string $label): bool
    {
        if ($this->isReadOnlyUser($this->consultation->getId())) {
            return false;
        }

        if (
            !empty(array_intersect($this->consultation->statutCalcule, [
                ConsultationStatus::STATUS['ELABORATION'],
                ConsultationStatus::STATUS['PREPARATION'],
            ]))
        ) {
            return $this->hasHabilitationAgent('modifier_consultation_avant_validation');
        }

        if (in_array(ConsultationStatus::STATUS['CONSULTATION'], $this->consultation->statutCalcule)) {
            return $this->consultationEnLigneService
                ->hasHabilitationModifierConsultationApresValidation($this->consultation->typeProcedureOrg);
        }

        return false;
    }

    public function getStatus(Consultation $consultation, $calledFromPortail = false)
    {
        $statut = null;
        if ($consultation instanceof Consultation) {
            if (!$calledFromPortail) {
                $idEtatConsultation = $consultation->getIdEtatConsultation();
            } else {
                $idEtatConsultation = 0;
            }
        } else {
            throw new \Exception('Parametre pour la fonction getStatus errone : attendu Objet Consultation ou CommonConsultation ');
        }


        $dateMiseEnligneCalcule = null;
        if ($consultation->getDateMiseEnLigneCalcule()) {
            $dateMiseEnligneCalcule = $consultation->getDateMiseEnLigneCalcule()->format('Y-m-d');
        }
        $dateFin = $consultation->getDatefin();
        if ($idEtatConsultation > 0) {
            $statut = $idEtatConsultation;
        } elseif (
            (!$dateMiseEnligneCalcule || strstr($dateMiseEnligneCalcule, '0000-00-00')
            || $dateMiseEnligneCalcule > date('Y-m-d H:i:s'))
        ) {
            if ('1' == $consultation->getEtatEnAttenteValidation()) {
                $statut = $this->parameterBag->get('STATUS_PREPARATION');
            } else {
                $statut = $this->parameterBag->get('STATUS_ELABORATION');
            }
        } elseif (
            $dateFin > date('Y-m-d H:i:s')
            && $dateMiseEnligneCalcule
            && !strstr($dateMiseEnligneCalcule, '0000-00-00')
            && $dateMiseEnligneCalcule <= date('Y-m-d H:i:s')
        ) {
            $statut = $this->parameterBag->get('STATUS_CONSULTATION');
        } elseif ($dateFin <= date('Y-m-d H:i:s') && !$idEtatConsultation) {
            $statut = $this->parameterBag->get('STATUS_OUVERTURE_ANALYSE');
        }

        return $statut;
    }
}
