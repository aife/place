<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Submenus;

use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Service\AgentService;
use App\Service\Consultation\ConsultationEnLigneService;
use App\Service\CurrentUser;
use App\Service\GestionRegistres;
use App\Service\Routing\RouteBuilderInterface;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AbstractSubmenu
{
    public const TRANSLATION_PREFIX = 'CHANGE_ME';
    public const ICON = '';
    public const HAS_SUBMENU = true;

    final public const PREFIX_METHOD_GETTER_IS = 'is';

    public ?ItemInterface $menu = null;

    protected ?ItemInterface $currentSubmenu = null;

    protected ?ItemInterface $currentRow = null;

    private ?ConfigurationPlateforme $configurationPlateforme = null;

    private ?ConfigurationOrganisme $configurationOrganisme = null;

    protected array $options = [];

    /**
     * @var bool Affichage sous forme de cards Bootstrap 4
     */
    public bool $bootstrapCards = false;

    /**
     * @var int Nombre de columns pour l'affichage sous forme de cards Bootstrap
     */
    public int $bootstrapCardsColumns = 3;

    /**
     * MenuAgentBuilder constructor.
     */
    public function __construct(
        protected FactoryInterface $factory,
        protected EntityManagerInterface $em,
        public ParameterBagInterface $parameterBag,
        protected CurrentUser $currentUser,
        protected AgentService $agentService,
        protected UrlGeneratorInterface $router,
        protected Encryption $encryption,
        protected GestionRegistres $registre,
        protected RouteBuilderInterface $routeBuilder,
        protected TranslatorInterface $translator,
        protected ConsultationEnLigneService $consultationEnLigneService
    ) {
        if (!$this->bootstrapCards) {
            $this->options = [
                'uri' => '#',
                'attributes' => ['class' => 'nav-item'],
                'linkAttributes' => ['class' => 'nav-link'],
            ];
        }
    }

    abstract public function getItem(): ?ItemInterface;

    abstract public function canSee(string $label): bool;

    abstract public function canClick(string $label): bool;

    public function mergeOptions($options): self
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    public function init(): void
    {
        if (!$this->canSee($this::TRANSLATION_PREFIX)) {
            return;
        }

        if ($this::HAS_SUBMENU && !$this->bootstrapCards) {
            $this->options = array_merge(
                [
                    'attributes' => [
                        'class' => 'dropdown nav-item',
                        'data-menu' => 'dropdown',
                    ],
                    'linkAttributes' => [
                        'class' => 'dropdown-toggle nav-link',
                        'data-toggle' => 'dropdown',
                    ],
                    'childrenAttributes' => [
                        'class' => 'dropdown-custom',
                        'role' => 'menu',
                    ],
                ],
                $this->options
            );
        }

        $this->options = $this->addSpecificCssClass('', $this->options);

        $this->menu = $this->factory->createItem($this::TRANSLATION_PREFIX, $this->options);

        if ($this->bootstrapCards) {
            $this->currentSubmenu = $this->addDiv('card');
            $this->currentSubmenu = $this->addDiv('card-content');
            $this->currentSubmenu = $this->addDiv('card-body');

            $this->addSubmenu(
                $this::TRANSLATION_PREFIX,
                ['extras' => ['tag' => 'h3']]
            )->setLabel($this->menu->getLabel());
            $this->menu->setLabel('');

            $this->addRow(1);
            $this->addColumns($this->bootstrapCardsColumns);
            $this->setColumn(1);
        }

        if (!empty($this::ICON)) {
            $this->menu->setExtra('icon', $this::ICON);
        }
    }

    public function addElement(string $label, array $options = [], $forceCanSee = false): ?ItemInterface
    {
        $this->prefixUri($options);

        if ($forceCanSee || $this->canSee($label)) {
            $item = $this->currentSubmenu ?: $this->menu;

            if (!$this->bootstrapCardsColumns || 5 == $item->getLevel()) {
                $options = $this->addSpecificCssClass($label, $options);
            }

            $label = (array_key_exists('remove_prefix', $options) && true === $options['remove_prefix'])
                ? $label
                : $this->getPrefixedLabel($label);

            return $item->addChild($label, $options);
        }

        return null;
    }

    public function addSubmenu(
        string $label,
        array $options = [],
        bool $changeColumn = true
    ): ?ItemInterface {
        $options = array_merge(
            [
                'attributes' => [
                    'class' => 'dropdown nav-item',
                    'data-menu' => 'dropdown',
                ],
                'extras' => [
                    'tag' => $this->bootstrapCards ? 'h4' : 'h3',
                ],
            ],
            $options
        );

        if (!$this->bootstrapCards && !is_null($this->currentSubmenu) && $changeColumn) {
            $this->currentSubmenu = $this->currentSubmenu->getParent();
        }

        if (!$changeColumn) {
            $options['attributes']['class'] .= ' mt-5';
        }

        $element = $this->addElement($label, $options);

        if (!$this->bootstrapCards && $changeColumn) {
            $this->currentSubmenu = $element;
        }

        return $element;
    }

    public function addLink(string $label, string $uri, array $options = []): ?ItemInterface
    {
        if ($this->canClick($label)) {
            $options = array_merge(
                $options,
                [
                    'uri' => $uri,
                ]
            );
        } else {
            unset($options['uri']);
        }

        return $this->addElement($label, $options);
    }

    /**
     * Ajout d'un formulaire de recherche rapide.
     */
    public function addForm(
        string $label,
        string $uri,
        string $inputName,
        string $submitLabel = 'OK',
        array $hiddenInputs = [],
        array $options = []
    ): ?ItemInterface {
        $options = array_merge(
            $options,
            [
                'uri' => $uri,
                'extras' => [
                    'form' => true,
                    'formInputName' => $inputName,
                    'formPlaceHolder' => $this::TRANSLATION_PREFIX . '_' . $label,
                    'formSubmitLabel' => $this::TRANSLATION_PREFIX . '_' . $submitLabel,
                    'formHiddenInputs' => $hiddenInputs,
                    'formAttributes' => [
                        'id' => strtolower($this::TRANSLATION_PREFIX . '_' . $label),
                    ],
                ],
            ]
        );

        if (!$this->canClick($label)) {
            $options['extras']['fieldsetAttributes']['disabled'] = 'disabled';
        }

        return $this->addElement($label, $options);
    }

    public function addRow(int $rowIndex = 1): ?ItemInterface
    {
        if (!$this->bootstrapCards) {
            throw new Exception('L\'ajout de ligne n\'est possible que si bootstrapCards vaut true');
        }
        $rowOptions = ['attributes' => ['class' => 'row'], 'extras' => ['row' => true]];
        $row = $this->addElement('row-' . $rowIndex, $rowOptions, true);
        if (!is_null($row)) {
            $row->setLabel('');
        }
        $this->currentRow = $row;

        return $row;
    }

    public function addColumns(int $number = 1): void
    {
        if (!$this->bootstrapCards) {
            throw new Exception('L\'ajout de colonnes n\'est possible que si bootstrapCards vaut true');
        }
        $this->currentSubmenu = $this->currentRow;

        for ($i = 1; $i <= $number; ++$i) {
            $this->addColumn($i, $number);
        }
    }

    public function addColumn(int $columnIndex = 1, int $totalColumns = 3): ?ItemInterface
    {
        if (!$this->bootstrapCards) {
            throw new Exception('L\'ajout de colonne n\'est possible que si bootstrapCards vaut true');
        }
        $columnWidth = floor(12 / $totalColumns);
        $columnOptions = [
            'attributes' => ['class' => "col-md-$columnWidth col-12 mb-2"],
            'extras' => ['column' => true],
        ];
        $column = $this->addElement('column-' . $columnIndex, $columnOptions, true);
        if (!is_null($column)) {
            $column->setLabel('');
        }

        return $column;
    }

    public function setColumn(int $number = 1): void
    {
        if (!$this->bootstrapCards) {
            throw new Exception('La gestion de colonnes n\'est possible que si bootstrapCards vaut true');
        }
        if (!is_null($this->currentRow)) {
            $this->currentSubmenu = $this->currentRow->getChild($this::TRANSLATION_PREFIX . '_column-' . $number);
        }
    }

    public function addDiv(string $class, int $divIndex = 1): ?ItemInterface
    {
        $divOptions = [
            'attributes' => ['class' => $class],
        ];
        $div = $this->addElement('div-' . $divIndex, $divOptions, true);
        if (!is_null($div)) {
            $div->setLabel('');
        }

        return $div;
    }

    public function addSeparator(int $hrIndex = 1): ?ItemInterface
    {
        $divOptions = [
            'extras' => ['hr' => true],
        ];
        $hr = $this->addElement('hr-' . $hrIndex, $divOptions, true);
        if (!is_null($hr)) {
            $hr->setLabel('');
        }

        return $hr;
    }

    public function getPrefixedLabel(string $label): string
    {
        return $this::TRANSLATION_PREFIX . '_' . $label;
    }

    public function getCssClassFromLabel(string $label = ''): string
    {
        return trim(strtolower(str_replace('_', '-', $this::TRANSLATION_PREFIX . '_' . $label)), '-');
    }

    public function addSpecificCssClass(string $label = '', array $options = []): array
    {
        $options['attributes']['class'] = ((isset($options['attributes']['class'])) ?
                $options['attributes']['class'] . ' '
                : '') . $this->getCssClassFromLabel($label);

        return $options;
    }

    public function addCssClassToOptions(string $className = '', array $options = []): array
    {
        $options['attributes']['class'] ??= '';
        $classesArray = explode(' ', (string) $options['attributes']['class']);
        if (!in_array($className, $classesArray)) {
            $classesArray[] = $className;
        }
        $options['attributes']['class'] = trim(implode(' ', $classesArray));

        return $options;
    }

    public function convertToCamelCase(string $code): string
    {
        return ucfirst((new CamelCaseToSnakeCaseNameConverter())->denormalize($code));
    }

    public function getGetterMethodName($code, $className): string
    {
        if (method_exists($className, 'is' . $this->convertToCamelCase($code))) {
            return 'is' . $this->convertToCamelCase($code);
        }

        return 'get' . $this->convertToCamelCase($code);
    }

    public function hasConfigurationPlateforme(string $module): bool
    {
        if (is_null($this->configurationPlateforme)) {
            $this->configurationPlateforme = $this->em->getRepository(ConfigurationPlateforme::class)
                ->getConfigurationPlateforme();
        }

        $getterMethod = $this->getGetterMethodName($module, ConfigurationPlateforme::class);

        return '1' === $this->configurationPlateforme->$getterMethod()
            || true === $this->configurationPlateforme->$getterMethod();
    }

    public function hasConfigurationOrganisme(string $module): bool
    {
        if (is_null($this->configurationOrganisme)) {
            $this->configurationOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
                ->findOneBy(['organisme' => $this->currentUser->getOrganisme()]);
        }

        $getterMethod = $this->getGetterMethodName($module, ConfigurationOrganisme::class);

        return $this->configurationOrganisme && ('1' === $this->configurationOrganisme->$getterMethod()
                || true === $this->configurationOrganisme->$getterMethod());
    }

    public function hasHabilitationAgent(string $habilitation, string $prefixMethod = 'get'): bool
    {
        return $this->currentUser && $this->currentUser->isAgent()
            && $this->currentUser->checkHabilitation($this->convertToCamelCase($habilitation), $prefixMethod);
    }

    public function prefixUri(array &$options): void
    {
        if (isset($options['uri']) && str_starts_with($options['uri'], '/')) {
            $pURL = $this->parameterBag->get('PF_URL_AGENT') != "" ?
                $this->parameterBag->get('PF_URL_AGENT') :
                $this->parameterBag->get('PF_URL');
            $options['uri'] = $pURL . ltrim($options['uri'], '/');
        }
    }

    protected function hasHabilitationModifierConsultationApresValidation(?TypeProcedureOrganisme $typeProcedure): bool
    {
        if (null === $typeProcedure) {
            return false;
        }

        if ($typeProcedure->getMapa() === $this->parameterBag->get('IS_MAPA')) {
            return ($typeProcedure->getIdMontantMapa() === $this->parameterBag->get('MONTANT_MAPA_INF_90')
                && $this->hasHabilitationAgent('ModifierConsultationMapaInferieurMontantApresValidation'))
                || ($typeProcedure->getIdMontantMapa() === $this->parameterBag->get('MONTANT_MAPA_SUP_90')
                && $this->hasHabilitationAgent('ModifierConsultationMapaSuperieurMontantApresValidation'));
        }

        return $this->hasHabilitationAgent('ModifierConsultationProceduresFormaliseesApresValidation');
    }

    public function isReadOnlyUser(int $consultationId): bool
    {
        return $this->em->getRepository(InterneConsultationSuiviSeul::class)
            ->isReadOnlyUser($consultationId, $this->currentUser);
    }

    public function translateLabel(string $label): ?string
    {
        return $this->translator->trans($label);
    }
}
