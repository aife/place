<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Submenus;

use Knp\Menu\ItemInterface;

interface SubmenuInterface
{
    public function getItem(): ?ItemInterface;

    public function canSee(string $label): bool;

    public function canClick(string $label): bool;
}
