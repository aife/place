<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Submenus;

use Knp\Menu\ItemInterface;

abstract class AbstractActionSubmenu extends AbstractSubmenu
{
    public const SEPARATOR = false;

    public function init(): void
    {

        if (!$this->canSee($this::TRANSLATION_PREFIX)) {
            if ($this->menu instanceof ItemInterface && null !== $this->menu->getParent()) {
                $this->menu = null;
            }
            return;
        }

        if (!$this->canClick($this::TRANSLATION_PREFIX)) {
            unset($this->options['uri']);
        }
        $this->menu = $this->factory->createItem($this::TRANSLATION_PREFIX, $this->options);

        if (!empty($this::ICON)) {
            $this->menu->setExtra('icon', $this::ICON);
        }
        $this->menu->setExtra('separator', $this::SEPARATOR);
    }
}
