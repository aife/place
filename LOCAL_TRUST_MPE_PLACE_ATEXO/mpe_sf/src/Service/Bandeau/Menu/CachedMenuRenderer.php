<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\MatcherInterface;
use Knp\Menu\Renderer\TwigRenderer;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface as CacheItemInterface;
use Twig\Environment;

class CachedMenuRenderer extends TwigRenderer
{
    public final const CACHE_KEY_PREFIX = 'bandeau-agent-html-';
    public final const SESSION_KEY = self::CACHE_KEY_PREFIX . 'hash';
    public final const CACHE_LIFETIME = 1200;

    public function __construct(
        Environment $environment,
        string $template,
        MatcherInterface $matcher,
        private readonly CacheInterface $cache,
        private readonly SessionInterface $session,
        private readonly Security $security,
        array $defaultOptions = []
    ) {
        parent::__construct($environment, $template, $matcher, $defaultOptions);
    }

    public function render(ItemInterface $item, array $options = []): string
    {
        if (!$this->security->isGranted('ROLE_AGENT')) {
            return '';
        }

        $bandeauAgentCacheHash = $this->session->get($this->getSessionKey());

        if (!empty($bandeauAgentCacheHash)) {
            $html = $this->cache->get(
                self::CACHE_KEY_PREFIX . $bandeauAgentCacheHash,
                function (CacheItemInterface $cacheItem) use ($item, $options) {
                    $cacheItem->expiresAfter(self::CACHE_LIFETIME);

                    return parent::render($item, $options);
                }
            );
            $bandeauAgentCacheHash = md5($html);
            $this->session->set($this->getSessionKey(), $bandeauAgentCacheHash);
        } else {
            $html = parent::render($item, $options);
            $bandeauAgentCacheHash = md5($html);
            $this->session->set($this->getSessionKey(), $bandeauAgentCacheHash);
            $this->cache->get(
                self::CACHE_KEY_PREFIX . $bandeauAgentCacheHash,
                function (CacheItemInterface $cacheItem) use ($html) {
                    $cacheItem->expiresAfter(self::CACHE_LIFETIME);

                    return $html;
                }
            );
        }

        return $html;
    }

    public function getSessionKey(): string
    {
        $sessionKey = self::SESSION_KEY . '_' . $this->security->getUser()?->getId();

        return $sessionKey;
    }
}
