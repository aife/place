<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Parametrage extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_PARAMETRAGE';
    public final const ICON = '';
    public final const HAS_SUBMENU = false;
    public bool $bootstrapCards = true;
    public int $bootstrapCardsColumns = 3;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('PLATEFORME');
        $this->addLink('ADMINISTRATION_MESSACE_ACCUEIL', $this->router->generate('message_accueil_index'));
        $this->addLink('GESTION_ORGANISME', '/agent/organisme/recherche?search=true');
        $this->addLink('ADMINISTRATEUR_AGENT', '/?page=Agent.GestionAgents&all=1');
        $this->addLink('PLATEFORME_GESTION_SERVICE', '/?page=Agent.GestionEntitesAchat');
        $this->addLink('GESTION_DOMAINE_ACTIVITE', '/?page=Agent.GestionNomenclature');
        $this->addLink('PLATEFORME_SUIVI_ACCES', '/?page=Agent.SuiviAcces&all=1');
        $this->addLink('AUTOFORMATION_AGENT', '/?page=Agent.ModuleAutoformation&access=agent');
        $this->addLink('AUTOFORMATION_ENTREPRISE', '/?page=Agent.ModuleAutoformation&access=entreprise');
        $this->addLink('LISTE_SIRET_ACHETEUR', '/?page=Agent.DownloadFileListeSiretAcheteur');

        if ($this->canSee('PLATEFORME') && $this->canSee('ENTITE_PUBLIQUE')) {
            $this->addSeparator();
        }
        $this->addSubmenu('ENTITE_PUBLIQUE');
        $this->addLink('CATEGORIE_MAPPA', '/?page=Agent.GestionMAPA');
        $this->addLink('PARAMETRAGE_PROCEDURES', '/?page=Agent.ParametrerProcedures');
        $this->addLink('REGLES_VALIDATION', '/?page=Agent.GestionReglesValidation');
        $this->addLink('COMPTES_JAL', '/?page=Agent.GestionJal&entitePublic=1');
        $this->addLink('SUIVI_ACCES', '/?page=Agent.SuiviAcces&all=0');
        $this->addLink('DOCUMENT_MODELE', $this->router->generate('admin_redac_document_template_index'));

        $this->setColumn(2);

        $this->addSubmenu('SERVICE');
        $this->addLink('GESTION_SERVICE', '/?page=Agent.GestionEntitesAchat');
        $this->addLink('GESTION_AGENTS', '/?page=Agent.GestionAgents&all=0');
        $this->addLink('GESTION_HABILITATIONS', '/?page=Agent.GestionAgents');
        $this->addLink('GESTION_COMPTES_BOAMP', '/?page=Agent.GestionBoamp');
        $this->addLink('GESTION_COMPTES_MONITEUR', '/?page=Agent.GestionMoniteur');
        $this->addLink('SERVICE_COMPTES_JAL', '/?page=Agent.GestionJal');
        $this->addLink('ADRESSE_FACTURATION_JAL', '/?page=Agent.GestionFacturation');
        $this->addLink('COMPTE_CENTRALE_PUBLICATION', '/?page=Agent.GestionCentralePub');
        $this->addLink('GESTION_FAVORIS_PUBLICITE', '/?page=Agent.GestionFavorisPublicite');
        $this->addLink('CLES_CHIFFREMENT', '/?page=Agent.GestionBiCle');
        $this->addLink('FOURNISSEUR_DOCUMENT', '/?page=Agent.GestionFournisseursDocs');
        $this->addLink('GESTION_MANDATAIRE', '/?page=Agent.GestionMandataires');
        $this->addLink('PARAMETRAGE_ADRESSE', '/?page=Agent.GestionDesAdresses');
        $this->addLink('GERER_INSCRIPTION_FOURNISSEUR', '/?page=Agent.AgentInscriptionCompany');

        $this->setColumn(3);

        $this->addSubmenu('NEWSLETTER');
        $this->addLink('MESSAGE_AGENTS', '/?page=Agent.NewsletterMessagesAgents');
        $this->addLink('MESSAGE_REDACTEURS', '/?page=Agent.NewsletterMessagesAgents&Redac');
        if ($this->canSee('NEWSLETTER') && $this->canSee('CERTIFICAT_ELECTRONIQUE')) {
            $this->addSeparator();
        }
        $this->addSubmenu('CERTIFICAT_ELECTRONIQUE');
        $this->addLink('GESTION_CERTIFICAT_FOURNISSEUR', '/?page=Agent.SearchCertificatsFournisseurs');
        $this->addLink('GESTION_CERTIFICAT_AGENT', '/?page=Agent.GestionCertificatsAgents&all=1');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'PARAMETRAGE', 'ENTITE_PUBLIQUE', 'PARAMETRAGE_PROCEDURES', 'REGLES_VALIDATION',
            'COMPTES_JAL', 'SUIVI_ACCES', 'SERVICE', 'GESTION_SERVICE', 'GESTION_AGENTS', 'GESTION_HABILITATIONS',
            'SERVICE_COMPTES_JAL', 'CLES_CHIFFREMENT', 'FOURNISSEUR_DOCUMENT' => true,
            'PLATEFORME', 'PLATEFORME_SUIVI_ACCES', 'ADMINISTRATION_MESSACE_ACCUEIL' =>
                $this->hasHabilitationAgent('hyper_admin'),
            'ADMINISTRATION_MESSACE_ACCUEIL' => $this->hasHabilitationAgent('gerer_messages_accueil'),
            'ADMINISTRATEUR_AGENT' => !$this->hasConfigurationPlateforme('socle_interne') &&
            !$this->hasConfigurationPlateforme('socleexterneppp') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'GESTION_ORGANISME' => $this->hasConfigurationPlateforme('gestion_organismes') &&
            !$this->hasConfigurationPlateforme('socle_interne') &&
            !$this->hasConfigurationPlateforme('socleexterneppp') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'PLATEFORME_GESTION_SERVICE' => $this->hasConfigurationPlateforme('gestion_organisme_par_agent') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'GESTION_DOMAINE_ACTIVITE' => $this->hasConfigurationPlateforme('consultation_domaines_activites') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'CATEGORIE_MAPPA' => $this->hasConfigurationPlateforme('procedure_adaptee'),
            'GESTION_COMPTES_BOAMP', 'GESTION_COMPTES_MONITEUR' =>
                $this->hasConfigurationPlateforme('publicite_format_xml'),
            'ADRESSE_FACTURATION_JAL' => $this->hasConfigurationPlateforme('gestion_adresses_facturation_jal'),
            'COMPTE_CENTRALE_PUBLICATION' => $this->hasConfigurationOrganisme('centrale_publication'),
            'GESTION_FAVORIS_PUBLICITE' => $this->hasConfigurationPlateforme('publicite')
                && (int)$this->parameterBag->get('SPECIFIQUE_PLACE') != 1,
            'GESTION_MANDATAIRE' => $this->hasConfigurationOrganisme('gestion_mandataire'),
            'PARAMETRAGE_ADRESSE' => $this->hasConfigurationPlateforme('gerer_adresses_service'),
            'GERER_INSCRIPTION_FOURNISSEUR', 'GESTION_CERTIFICAT_FOURNISSEUR' =>
                $this->hasConfigurationPlateforme('gestion_entreprise_par_agent'),
            'NEWSLETTER', 'MESSAGE_AGENTS' => $this->hasConfigurationPlateforme('gestion_newsletter'),
            'MESSAGE_REDACTEURS' => $this->hasConfigurationPlateforme('gestion_newsletter') &&
            $this->hasConfigurationOrganisme('interface_module_rsem'),
            'CERTIFICAT_ELECTRONIQUE' => $this->hasConfigurationPlateforme('gestion_entreprise_par_agent') ||
            $this->hasConfigurationPlateforme('gerer_certificats_agent'),
            'GESTION_CERTIFICAT_AGENT' => $this->hasConfigurationPlateforme('gerer_certificats_agent'),
            'AUTOFORMATION_AGENT', 'AUTOFORMATION_ENTREPRISE' => $this->hasHabilitationAgent('hyper_admin') &&
                $this->hasConfigurationPlateforme('modules_autoformation'),
            'LISTE_SIRET_ACHETEUR' => $this->hasConfigurationOrganisme('extraction_siret_acheteur'),
            'DOCUMENT_MODELE' => $this->hasConfigurationOrganisme('module_administration_document'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'GESTION_FAVORIS_PUBLICITE' => true,
            'ADMINISTRATEUR_AGENT', 'GESTION_DOMAINE_ACTIVITE', 'PLATEFORME_GESTION_SERVICE' =>
                 $this->hasHabilitationAgent('hyper_admin'),
            'PLATEFORME_SUIVI_ACCES', 'SUIVI_ACCES' => $this->hasHabilitationAgent('suivi_acces'),
            'REGLES_VALIDATION' => $this->hasHabilitationAgent('gestion_type_validation'),
            'COMPTES_JAL' => $this->hasHabilitationAgent('gestion_compte_jal') ||
            $this->hasHabilitationAgent('administrer_adresses_facturation_jal'),
            'GESTION_SERVICE' => $this->hasHabilitationAgent('gestion_agent_pole'),
            'GESTION_AGENTS' => $this->hasHabilitationAgent('gestion_agents'),
            'GESTION_HABILITATIONS' => $this->hasHabilitationAgent('gestion_habilitations'),
            'SERVICE_COMPTES_JAL' => $this->hasHabilitationAgent('gestion_compte_jal'),
            'CLES_CHIFFREMENT' => $this->hasHabilitationAgent('gestion_bi_cles'),
            'FOURNISSEUR_DOCUMENT' => $this->hasHabilitationAgent('gestion_fournisseurs_envois_postaux'),
            'PARAMETRAGE_PROCEDURES' => $this->hasHabilitationAgent('administrer_procedure'),
            'ADMINISTRATION_MESSACE_ACCUEIL' => $this->hasHabilitationAgent('gerer_messages_accueil'),
            'GESTION_ORGANISME' => $this->hasHabilitationAgent('hyper_admin') &&
            $this->hasHabilitationAgent('gerer_organismes'),
            'CATEGORIE_MAPPA' => $this->hasHabilitationAgent('gestion_mapa'),
            'GESTION_COMPTES_BOAMP' => $this->hasHabilitationAgent('gestion_compte_boamp'),
            'GESTION_COMPTES_MONITEUR' => $this->hasHabilitationAgent('gestion_compte_groupe_moniteur'),
            'ADRESSE_FACTURATION_JAL' => $this->hasHabilitationAgent('gestion_adresses_facturation_jal'),
            'COMPTE_CENTRALE_PUBLICATION' => $this->hasHabilitationAgent('gestion_centrale_pub'),
            'GESTION_MANDATAIRE' => $this->hasHabilitationAgent('gestion_mandataire'),
            'PARAMETRAGE_ADRESSE' => $this->hasHabilitationAgent('gerer_adresses_service'),
            'GERER_INSCRIPTION_FOURNISSEUR', 'GESTION_CERTIFICAT_FOURNISSEUR' =>
                $this->hasHabilitationAgent('gerer_les_entreprises'),
            'MESSAGE_AGENTS' => $this->hasHabilitationAgent('gerer_newsletter'),
            'MESSAGE_REDACTEURS' => $this->hasHabilitationAgent('gerer_newsletter_redac'),
            'GESTION_CERTIFICAT_AGENT' => $this->hasHabilitationAgent('gestion_certificats_agent'),
            'AUTOFORMATION_AGENT', 'AUTOFORMATION_ENTREPRISE' => $this->hasHabilitationAgent('module_autoformation'),
            'LISTE_SIRET_ACHETEUR' => $this->hasHabilitationAgent('telecharger_siret_acheteur'),
            'DOCUMENT_MODELE' => $this->hasHabilitationAgent('administration_documents_modeles', 'is'),
            default => false,
        };
    }
}
