<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class AdministrationMetier extends AbstractSubmenu implements SubmenuInterface
{
    final public const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_ADMINISTRATION_METIER';
    final public const ICON = '';
    public bool $bootstrapCards = true;
    public int $bootstrapCardsColumns = 2;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('ACTIONS_SPECIFIQUES');
        $this->addLink('TELECHARGEMENT_PLIS_CHIFFRES', '/?page=Agent.TelechargementPlisChiffres');
        $this->addLink('SUPPRESSION_PLIS_REFUSES', '/?page=Agent.SuppressionPliRefuse');

        $hrLevel = 1;
        if ($this->canSee('ACTIONS_SPECIFIQUES') && $this->canSee('ACTIONS_ANNEXES')) {
            $this->addSeparator($hrLevel);
            $hrLevel++;
        }

        $this->addSubmenu('ACTIONS_ANNEXES');
        $this->addLink(
            'PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS',
            '/?page=Agent.PublicationListeMarches',
            []
        );
        $this->addLink('GESTION_FICHIER_A_PUBLIER', '/?page=Agent.ListeDesMarches');

        $this->addLink('MESURES_DEMATERIALISATION', '/?page=Agent.StatistiquesMesureAvancement');
        $this->addLink('ECHANGE_CHORUS', '/?page=Agent.TableauBordEchangesChorus&search=true');

        if (
            ($this->canSee('ACTIONS_SPECIFIQUES') || $this->canSee('ACTIONS_ANNEXES'))
            && $this->canSee('ADMINISTRATION_SPASER')
        ) {
            $this->addSeparator($hrLevel);
        }

        $option['linkAttributes'] = ['target' => '_blank'];
        $this->addSubmenu('ADMINISTRATION_SPASER');
        $this->addLink(
            'CONFIGURATION_SPASER',
            $this->router->generate('agent_spaser_objet_metier_admin_spaser'),
            $option
        );
        $this->addLink(
            'SAISIE_INDICATEUR_TRANSVERSE',
            $this->router->generate('agent_spaser_objet_metier_transverse'),
            $option
        );

        $this->setColumn(2);

        $this->addSubmenu('SOCIETES_EXCLUES');
        $this->addLink('SOCIETES_EXCLUES_CREER', '/?page=Agent.FormulaireSocietesExclues');
        $this->addLink(
            'SOCIETES_EXCLUES_RECHERCHE_AVANCEE',
            '/?page=Agent.AgentRechercherSocietesExclues&search=1'
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'ACTIONS_SPECIFIQUES', 'TELECHARGEMENT_PLIS_CHIFFRES',
            'SUPPRESSION_PLIS_REFUSES' => true, 'ACTIONS_ANNEXES'
            => $this->hasConfigurationPlateforme('article_133_generation_pf') ||
            $this->hasConfigurationPlateforme('statistiques_mesure_demat') ||
            $this->hasConfigurationOrganisme('interface_chorus_pmi'),
            'PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS' =>
                (int) $this->parameterBag->get('SPECIFIQUE_PLACE') != 1
                && $this->hasConfigurationPlateforme('article_133_generation_pf'),
            'GESTION_FICHIER_A_PUBLIER' => $this->hasConfigurationPlateforme('article_133_upload_fichier'),
            'MESURES_DEMATERIALISATION' => $this->hasConfigurationPlateforme('statistiques_mesure_demat'),
            'ECHANGE_CHORUS' => $this->hasConfigurationOrganisme('interface_chorus_pmi'),
            'SOCIETES_EXCLUES', 'SOCIETES_EXCLUES_CREER', 'SOCIETES_EXCLUES_RECHERCHE_AVANCEE'
            => $this->hasConfigurationPlateforme('menu_agent_societes_exclues'),
            'ADMINISTRATION_SPASER', 'CONFIGURATION_SPASER', 'SAISIE_INDICATEUR_TRANSVERSE'
            => ($this->hasConfigurationOrganisme('acces_module_spaser')
                && $this->hasHabilitationAgent('gestion_spaser_consultations', self::PREFIX_METHOD_GETTER_IS)),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'MESURES_DEMATERIALISATION', 'SOCIETES_EXCLUES_RECHERCHE_AVANCEE' => true,
            'TELECHARGEMENT_PLIS_CHIFFRES'
            => $this->hasHabilitationAgent('telechargement_groupe_anticipe_plis_chiffres'),
            'SUPPRESSION_PLIS_REFUSES' => $this->hasHabilitationAgent('supprimer_enveloppe'),
            'PUBLICATION_DONNEES_ESSENTIELLES_MARCHES_CONCLUS', 'GESTION_FICHIER_A_PUBLIER'
            => $this->hasHabilitationAgent('publication_marches'),
            'ECHANGE_CHORUS' => $this->hasHabilitationAgent('suivi_flux_chorus_transversal'),
            'SOCIETES_EXCLUES_CREER' => $this->hasHabilitationAgent('portee_societes_exclues') ||
            $this->hasHabilitationAgent('portee_societes_exclues_tous_organismes'),
            'CONFIGURATION_SPASER', 'SAISIE_INDICATEUR_TRANSVERSE'
            => ($this->hasConfigurationOrganisme('acces_module_spaser')
                && $this->hasHabilitationAgent('gestion_spaser_consultations', self::PREFIX_METHOD_GETTER_IS)),
            default => false,
        };
    }
}
