<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class ClausesPersonnalisees extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_CLAUSES_PERSONNALISEES';
    public final const ICON = '';
    public bool $bootstrapCards = true;
    public int $bootstrapCardsColumns = 1;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('CLAUSES_PERSO_ENTITE_ACHAT');
        $this->addLink('CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', '/?page=Agent.RedacClausier&cp=ParametrageClause&fullWidth=true');
        $this->addLink(
            'CLAUSES_PERSO_ENTITE_ACHAT_GABARIT',
            '/?page=Agent.RedacClausier&cp=AdministrerGabaritsClauseEntiteAchat&fullWidth=true'
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'CLAUSES_PERSO_ENTITE_ACHAT', 'CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES', 'CLAUSES_PERSO_ENTITE_ACHAT_GABARIT' => $this->hasConfigurationOrganisme('interface_module_rsem'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'CLAUSES_PERSO_ENTITE_ACHAT_CLAUSES' => $this->hasHabilitationAgent('administrer_clauses_entite_achats'),
            'CLAUSES_PERSO_ENTITE_ACHAT_GABARIT' => $this->hasHabilitationAgent('gerer_gabarit_entite_achats'),
            default => false,
        };
    }
}
