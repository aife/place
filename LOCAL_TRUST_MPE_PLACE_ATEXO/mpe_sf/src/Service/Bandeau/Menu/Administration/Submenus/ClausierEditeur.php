<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class ClausierEditeur extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_CLAUSIER_EDITEUR';
    public final const ICON = '';
    public bool $bootstrapCards = true;
    public int $bootstrapCardsColumns = 2;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('CLAUSES_EDITEURS');
        $this->addLink('CLAUSES_EDITEURS_RECHERCHER', '/?page=Agent.RedacClausier&cp=RechercherClauseEditeur&fullWidth=true');
        $this->addLink('CLAUSES_EDITEURS_CREER', '/?page=Agent.RedacClausier&cp=CreationClauseEditeur&fullWidth=true');
        $this->addLink('CLAUSES_EDITEURS_EXPORTER', '/?page=Agent.RedacClausier&cp=exportClauseEditeurAction&fullWidth=true');

        if ($this->canSee('CLAUSES_EDITEURS') && $this->canSee('CLAUSES_EDITEURS_CANEVAS')) {
            $this->addSeparator();
        }

        $this->addSubmenu('CLAUSES_EDITEURS_CANEVAS');
        $this->addLink('CLAUSES_EDITEURS_CANEVAS_RECHERCHER', '/?page=Agent.RedacClausier&cp=RechercherCanevasEditeur&fullWidth=true');
        $this->addLink('CLAUSES_EDITEURS_CANEVAS_CREER', '/?page=Agent.RedacClausier&cp=CreationCanevasEditeur&fullWidth=true');

        $this->setColumn(2);

        $this->addSubmenu('CLAUSES_EDITEURS_VERSION');
        $this->addLink('CLAUSES_EDITEURS_VERSION_CREER', '/?page=Agent.RedacClausier&cp=PublicationClausier&fullWidth=true');
        $this->addLink('CLAUSES_EDITEURS_VERSION_HISTORIQUE', '/?page=Agent.RedacClausier&cp=HistoriqueClausier&fullWidth=true');

        if ($this->canSee('CLAUSES_EDITEURS_VERSION') && $this->canSee('CLAUSES_EDITEURS_GABARIT')) {
            $this->addSeparator();
        }

        $this->addSubmenu('CLAUSES_EDITEURS_GABARIT');
        $this->addLink(
            'CLAUSES_EDITEURS_GABARIT_MODIFIER',
            '/?page=Agent.RedacClausier&cp=AdministrerGabaritsClauseEditeur&fullWidth=true'
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'CLAUSES_EDITEURS', 'CLAUSES_EDITEURS_VERSION',
            'CLAUSES_EDITEURS_VERSION_HISTORIQUE', 'CLAUSES_EDITEURS_GABARIT', 'CLAUSES_EDITEURS_GABARIT_MODIFIER'
            => $this->hasConfigurationOrganisme('interface_module_rsem'),
            'CLAUSES_EDITEURS_RECHERCHER', 'CLAUSES_EDITEURS_CREER', 'CLAUSES_EDITEURS_EXPORTER',
            'CLAUSES_EDITEURS_CANEVAS', 'CLAUSES_EDITEURS_CANEVAS_RECHERCHER',
            'CLAUSES_EDITEURS_CANEVAS_CREER', 'CLAUSES_EDITEURS_VERSION_CREER'
            => $this->hasConfigurationPlateforme('plateforme_editeur')
                && $this->hasConfigurationOrganisme('interface_module_rsem'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'CLAUSES_EDITEURS_VERSION_HISTORIQUE' => $this->hasHabilitationAgent('administrer_clauses_editeur'),
            'CLAUSES_EDITEURS_RECHERCHER', 'CLAUSES_EDITEURS_CREER', 'CLAUSES_EDITEURS_EXPORTER',
            'CLAUSES_EDITEURS_CANEVAS_RECHERCHER', 'CLAUSES_EDITEURS_CANEVAS_CREER'
            => $this->hasHabilitationAgent('administrer_clauses_editeur')
                || $this->hasHabilitationAgent('valider_clauses_editeur'),
            'CLAUSES_EDITEURS_GABARIT_MODIFIER' => $this->hasHabilitationAgent('gerer_gabarit_editeur'),
            'CLAUSES_EDITEURS_VERSION_CREER' => $this->hasHabilitationAgent('publier_version_clausier_editeur'),
            default => false,
        };
    }
}
