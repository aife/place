<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class Statistiques extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_STATISTIQUES';
    public final const ICON = '';
    public bool $bootstrapCards = true;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('STATISTIQUES_PLATEFORME');
        $this->addLink('STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', '/?page=Agent.StatistiquesRechercheAvance&allOrgs');
        $this->addLink('STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', '/?page=Agent.StatistiquesMetier&allOrgs');
        $this->addLink('STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', '/?page=Agent.StatistiquesMapa&allOrgs');
        $this->addLink(
            'STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE',
            '/?page=Agent.StatistiquesEntreprise&allOrgs'
        );
        $this->addLink('STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', '/?page=Agent.StatistiquesAvisPublies&allOrgs');
        $this->addLink(
            'STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE',
            '/?page=Agent.StatistiquesBourseCotraitance&allOrgs'
        );

        $this->setColumn(2);

        $this->addSubmenu('STATISTIQUES_ENTITE_PUBLIQUE');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', '/?page=Agent.StatistiquesRechercheAvance');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', '/?page=Agent.StatistiquesMetier');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE', '/?page=Agent.StatistiquesEntreprise');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', '/?page=Agent.StatistiquesAvisPublies');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', '/?page=Agent.StatistiquesMapa');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', '/?page=Agent.StatistiqueExportSuiviPassation');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', '/?page=Agent.StatistiqueExportMarchesConclus');
        $this->addLink('STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS', '/?page=Agent.StatistiqueExportAvenants');

        $this->setColumn(3);

        $this->addSubmenu('STATISTIQUES_REDAC');
        $this->addLink(
            'STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME',
            '/?page=Agent.RapportStatistiqueUtilisateurOrganisme'
        );
        $this->addLink(
            'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION',
            '/?page=Agent.RapportStatistiqueDocConsultation'
        );
        $this->addLink('STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', '/?page=Agent.RapportStatistiqueDocOrganisme');
        $this->addLink(
            'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE',
            '/?page=Agent.RapportStatistiqueDocProcedures'
        );
        $this->addLink('STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', '/?page=Agent.RapportStatistiqueProcNature');
        $this->addLink('STATISTIQUES_REDAC_DOCUMENT_CANEVAS', '/?page=Agent.RapportStatistiqueDetailDocCanevas');
        $this->addLink('STATISTIQUES_REDAC_DOCUMENT_LIBRES', '/?page=Agent.RapportStatistiqueDetailDocLibres');
        $this->addLink('STATISTIQUES_REDAC_DOCUMENTS', '/?page=Agent.RapportStatistiqueDocProcMarche');

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'STATISTIQUES_ENTITE_PUBLIQUE', 'STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE' => true,
            'STATISTIQUES_PLATEFORME', 'STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', 'STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', 'STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE' => $this->hasHabilitationAgent('hyper_admin'),
            'STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', 'STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', 'STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS' => $this->hasConfigurationOrganisme('suivi_passation'),
            'STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES' => $this->hasConfigurationPlateforme('procedure_adaptee') && $this->hasHabilitationAgent('hyper_admin'),
            'STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES' => $this->hasConfigurationPlateforme('publicite_format_xml') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE' => $this->hasConfigurationPlateforme('bourse_cotraitance') &&
            $this->hasHabilitationAgent('hyper_admin'),
            'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES' => $this->hasConfigurationPlateforme('publicite_format_xml'),
            'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES' => $this->hasConfigurationPlateforme('procedure_adaptee'),
            'STATISTIQUES_REDAC', 'STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', 'STATISTIQUES_REDAC_DOCUMENT_CANEVAS', 'STATISTIQUES_REDAC_DOCUMENT_LIBRES', 'STATISTIQUES_REDAC_DOCUMENTS' => $this->hasConfigurationOrganisme('interface_module_rsem') &&
            $this->hasHabilitationAgent('hyper_admin'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'STATISTIQUES_PLATEFORME_RECHERCHE_AVANCEE', 'STATISTIQUES_PLATEFORME_SUIVI_DONNEES_METIER', 'STATISTIQUES_PLATEFORME_SUIVI_PROCEDURES_ADAPTEES', 'STATISTIQUES_PLATEFORME_SUIVI_DONNEES_ENTREPRISE', 'STATISTIQUES_PLATEFORME_SUIVI_AVIS_PUBLIES', 'STATISTIQUES_PLATEFORME_SUIVI_BOURSE_COTRAITRANCE', 'STATISTIQUES_REDAC_DETAILS_UTILISATEURS_ORGANISME', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_CONSULTATION', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_ORGANISME', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_TYPE_PROCEDURE', 'STATISTIQUES_REDAC_DETAIL_DOCUMENTS_NATURE_ACHAT', 'STATISTIQUES_REDAC_DOCUMENT_CANEVAS', 'STATISTIQUES_REDAC_DOCUMENT_LIBRES', 'STATISTIQUES_REDAC_DOCUMENTS' => $this->hasHabilitationAgent('hyper_admin'),
            'STATISTIQUES_ENTITE_PUBLIQUE_RECHERCHE_AVANCEE', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_METIERS', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_DONNES_ENTREPRISE', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_AVIS_PUBLIES', 'STATISTIQUES_ENTITE_PUBLIQUE_SUIVI_PROCEDURE_ADAPTEES', 'STATISTIQUES_ENTITE_PUBLIQUE_PASSATION', 'STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_CONCLUS', 'STATISTIQUES_ENTITE_PUBLIQUE_MARCHES_AVENANTS' => $this->hasHabilitationAgent('gerer_statistiques_metier'),
            default => false,
        };
    }
}
