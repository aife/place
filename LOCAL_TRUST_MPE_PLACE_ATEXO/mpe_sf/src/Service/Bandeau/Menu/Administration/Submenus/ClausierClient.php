<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Bandeau\Menu\Administration\Submenus;

use App\Service\Bandeau\Menu\Submenus\AbstractSubmenu;
use App\Service\Bandeau\Menu\Submenus\SubmenuInterface;
use Knp\Menu\ItemInterface;

class ClausierClient extends AbstractSubmenu implements SubmenuInterface
{
    public final const TRANSLATION_PREFIX = 'MENU_AGENT_ADMINISTRATION_CLAUSIER_CLIENT';
    public final const ICON = '';
    public bool $bootstrapCards = true;
    public int $bootstrapCardsColumns = 2;

    public function getItem(): ?ItemInterface
    {
        $this->init();

        $this->addSubmenu('CLAUSES_CLIENT');
        $this->addLink('CLAUSES_CLIENT_RECHERCHER', '/?page=Agent.RedacClausier&cp=RechercherClause&fullWidth=true');
        $this->addLink('CLAUSES_CLIENT_CREER', '/?page=Agent.RedacClausier&cp=CreationClause&fullWidth=true');
        $this->addLink('CLAUSES_CLIENT_EXPORTER', '/?page=Agent.RedacClausier&cp=exportClauseAction&fullWidth=true');
        if ($this->canSee('CLAUSES_CLIENT') && $this->canSee('CLAUSES_CLIENT_CANEVAS')) {
            $this->addSeparator();
        }
        $this->addSubmenu('CLAUSES_CLIENT_CANEVAS');
        $this->addLink('CLAUSES_CLIENT_CANEVAS_RECHERCHER', '/?page=Agent.RedacClausier&cp=RechercherCanevas&fullWidth=true');
        $this->addLink('CLAUSES_CLIENT_CANEVAS_CREER', '/?page=Agent.RedacClausier&cp=CreationCanevas&fullWidth=true');

        $this->setColumn(2);

        $this->addSubmenu('CLAUSES_CLIENT_GABARIT');
        $this->addLink(
            'CLAUSES_CLIENT_GABARIT_MODIFIER',
            '/?page=Agent.RedacClausier&cp=AdministrerGabaritsClausierClient&fullWidth=true'
        );

        return $this->menu;
    }

    public function canSee(string $label): bool
    {
        return match ($label) {
            $this::TRANSLATION_PREFIX, 'CLAUSES_CLIENT', 'CLAUSES_CLIENT_RECHERCHER', 'CLAUSES_CLIENT_CREER', 'CLAUSES_CLIENT_EXPORTER', 'CLAUSES_CLIENT_CANEVAS', 'CLAUSES_CLIENT_CANEVAS_RECHERCHER', 'CLAUSES_CLIENT_CANEVAS_CREER', 'CLAUSES_CLIENT_GABARIT', 'CLAUSES_CLIENT_GABARIT_MODIFIER' => $this->hasConfigurationOrganisme('interface_module_rsem'),
            default => false,
        };
    }

    public function canClick(string $label): bool
    {
        return match ($label) {
            'CLAUSES_CLIENT_RECHERCHER', 'CLAUSES_CLIENT_CREER', 'CLAUSES_CLIENT_EXPORTER', 'CLAUSES_CLIENT_CANEVAS_RECHERCHER', 'CLAUSES_CLIENT_CANEVAS_CREER' => $this->hasHabilitationAgent('administrer_clauses') ||
            $this->hasHabilitationAgent('valider_clauses'),
            'CLAUSES_CLIENT_GABARIT_MODIFIER' => $this->hasHabilitationAgent('gerer_gabarit'),
            default => false,
        };
    }
}
