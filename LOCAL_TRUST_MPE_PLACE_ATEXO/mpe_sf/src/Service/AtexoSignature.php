<?php

namespace App\Service;

use AtexoCrypto\Exception\ServiceException;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AtexoSignature de gestion des signatures.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @since 2017-develop
 *
 * @copyright Atexo 2017
 */
class AtexoSignature
{
    /**
     * AtexoSignature constructor.
     *
     * @param $container
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function __construct(private ContainerInterface $container, private readonly LoggerInterface $logger)
    {
    }

    /**
     * @return mixed
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Permet de recuperer le jnlp de signature.
     *
     * @return string le contenu de jnlp
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function getJnlpToSignDocument()
    {
        $this->logger->info('La recuperation de jnlp');
        $cryptoService = $this->getContainer()->get('atexo_crypto.crypto');
        try {
            return $cryptoService->jnlpSignature($this->getContainer()->getParameter('URL_CRYPTO'));
        } catch (ServiceException $e) {
            $this->logger->error("Erreur Lors d'appel au ws de dechiffrement".$e->errorCode.' - '.$e->etatObjet.' - '.$e->message);
        }

        return false;
    }
}
