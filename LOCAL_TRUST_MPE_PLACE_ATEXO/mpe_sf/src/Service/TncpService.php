<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\InterfaceSuivi;

class TncpService
{
    public function __construct(private WebservicesInterfaceSuivi $interfaceSuivi)
    {
    }

    public function getConsultationStatus(int $idConsultation): array
    {
        $status = [
            InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION => 'NON_REALISE',
            InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI => 'NON_REALISE',
            InterfaceSuivi::ACTION_MODIFICATION_DCE => 'NON_REALISE'
        ];

        $succeed = [];
        $listSuivi = $this->interfaceSuivi->getContent('searchMessages', [
            'consultation' => $idConsultation,
            'order' => ['dateCreation' => 'asc'],
            'pagination' => false
        ]);
        foreach ($listSuivi->{'hydra:member'} as $suivi) {
            list($status, $succeed) = $this->getStatusByAction(
                InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION,
                $suivi,
                $status,
                $succeed
            );
            list($status, $succeed) = $this->getStatusByAction(
                InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI,
                $suivi,
                $status,
                $succeed
            );
            list($status, $succeed) = $this->getStatusByAction(
                InterfaceSuivi::ACTION_MODIFICATION_DCE,
                $suivi,
                $status,
                $succeed
            );
        }

        return ['status' => $status, 'date' => $succeed];
    }

    private function getStatusText(int $status): string
    {
        return match ($status) {
            InterfaceSuivi::STATUS_EN_ATTENTE => InterfaceSuivi::STATUS_EN_ATTENTE_TEXT,
            InterfaceSuivi::STATUS_EN_COURS => InterfaceSuivi::STATUS_EN_COURS_TEXT,
            InterfaceSuivi::STATUS_FINI => InterfaceSuivi::STATUS_FINI_TEXT,
            InterfaceSuivi::STATUS_ERREUR => InterfaceSuivi::STATUS_ERREUR_TEXT,
            default => 'NON_REALISE',
        };
    }


    private function getStatusByAction(string $action, \stdClass $suivi, array $status, array $succeed): array
    {
        if ($action === $suivi->action || $action === $suivi->action . 'E_PLI') {
            $status[$action] = $this->getStatusText($suivi->statut);
            if (InterfaceSuivi::STATUS_FINI == $suivi->statut) {
                $succeed[$action] = (new \DateTime($suivi->dateCreation))
                    ->format('d/m/Y H:i:s');
            }
        }

        return [$status, $succeed];
    }
}
