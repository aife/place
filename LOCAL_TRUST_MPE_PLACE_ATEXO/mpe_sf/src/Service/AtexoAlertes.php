<?php

namespace App\Service;

use App\Entity\ConfigurationPlateforme;
use App\Entity\TMesRecherches;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class AtexoAlertes.
 */
class AtexoAlertes
{
    /**
     * AtexoAlertes constructor.
     *
     * @param $container
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly SessionInterface $session,
        private readonly CurrentUser $currentUser
    ) {
        $this->typeAvisConsulation = $this->parameterBag->get('TYPE_AVIS_CONSULTATION');
        $this->maxResults = $this->parameterBag->get('NBRE_MAX_MENUS_RECHERCHES_FAVORITES');
        $this->idCreateur = $this->session->get('contexte_authentification/id');
        $this->typeCreateur = $this->parameterBag->get('TYPE_CREATEUR_ENTREPRISE');
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->parameterBag;
    }


    public function listeAlertes(): array
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();

        $lists = [];
        if (
            $configurationPlateforme->getRecherchesFavorites()
            && $this->currentUser->isEntreprise()
            && $this->currentUser->isConnected()
        ) {
            $lists = $this->em
                ->getRepository(TMesRecherches::class)
                ->retreiveRecherches(
                    $this->currentUser->getIdInscrit(),
                    $this->parameterBag->get('TYPE_CREATEUR_ENTREPRISE'),
                    $this->parameterBag->get('TYPE_AVIS_CONSULTATION'),
                    $this->parameterBag->get('NBRE_MAX_MENUS_RECHERCHES_FAVORITES'),
                    true
                );
        }

        return $lists;
    }


    public function listeAvis(): array
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();

        $lists = [];
        if (
            $configurationPlateforme->getRecherchesFavorites()
            && $this->currentUser->isEntreprise()
            && $this->currentUser->isConnected()
        ) {
            $lists = $this->em
                ->getRepository(TMesRecherches::class)
                ->retreiveRecherches(
                    $this->currentUser->getIdInscrit(),
                    $this->parameterBag->get('TYPE_CREATEUR_ENTREPRISE'),
                    $this->parameterBag->get('TYPE_AVIS_CONSULTATION'),
                    $this->parameterBag->get('NBRE_MAX_MENUS_RECHERCHES_FAVORITES'),
                    true,
                    false,
                    false
                );
        }

        return $lists;
    }

    private function getConfigurationPlateforme(): ConfigurationPlateforme
    {
        return $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
    }
}
