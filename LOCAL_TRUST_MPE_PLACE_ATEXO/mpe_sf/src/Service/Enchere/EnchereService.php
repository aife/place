<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Enchere;

use App\Entity\EnchereEntreprisePmi;
use App\Entity\EnchereOffre;
use Doctrine\ORM\EntityManagerInterface;

class EnchereService
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function getData(string $idEnchere, string $type = 'prix'): array
    {
        $entreprises = $this->entityManager
            ->getRepository(EnchereEntreprisePmi::class)
            ->findBy([
                'idEnchere' => $idEnchere
            ]);
        $encheres = [];

        foreach ($entreprises as $entreprise) {
            $data = [];
            $enchereOffres = $this->entityManager
                ->getRepository(EnchereOffre::class)
                ->findBy([
                    'idEnchere' => $idEnchere,
                    'idEnchereEntreprise' => $entreprise->getId()
                ], ['date' => 'ASC']);
            foreach ($enchereOffres as $offre) {
                $tsOffre = $offre->getDate()->format('Y-m-d\TH:i:s.u');
                if ($type == 'prix') {
                    $data[$tsOffre] = floor($offre->getValeurtc());
                } else {
                    $data[$tsOffre] = floor($offre->getValeurngc());
                }
            }
            $encheres[$entreprise->getNom()] = $data;
        }
        return $encheres;
    }
}
