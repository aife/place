<?php

namespace App\Service;

use App\Entity\MarchePublie;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MarchePublieService
{
    /**
     * OrganismeService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(private readonly ContainerInterface $container, private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @param $annee
     * @param $acronyme
     * @param int $idService
     */
    public function create($annee, $acronyme, $idService = 0)
    {
        $marchePublie = $this->em->getRepository(MarchePublie::class)->getMarchePublieByYear($acronyme, $idService, $annee);

        if (!$marchePublie instanceof MarchePublie) {
            $marchePublie = new MarchePublie();
            $marchePublie->setOrganisme($acronyme);
            $marchePublie->setServiceId($idService);
            $marchePublie->setNumeroMarcheAnnee($annee);
            $marchePublie->setIsPubliee('1');
            $this->em->persist($marchePublie);
            $this->em->flush();
        }

        return $marchePublie;
    }
}
