<?php

namespace App\Service;

use Exception;
use App\Entity\BlobFichier;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Classe contenant les fonctions gestion fichier.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2016
 */
class AtexoFichier
{
    /**
     * AtexoFichier constructor.
     *
     * @param $container
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function __construct(private ContainerInterface $container, private EntityManagerInterface $em, private readonly MountManager $mountManager)
    {
    }

    /**
     * insertion d'un fichier Blob.
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     *
     * @param null $subfolder
     *
     * @return int
     *
     * @throws Exception
     */
    public function insertFile(
        string $fileName,
        string $pathFile,
        string $folder = 'common',
        $subfolder = null,
        $extension = ''
    ) {
        try {
            $mountManager = $this->getMountManager();
            $fileSystem = $mountManager->getFilesystem('nas');
            if ($fileSystem->has($pathFile)) {
                $repertoire = $fileSystem->getRepertoire($folder);
                $em = $this->getContainer()->get('doctrine')->getManager();

                $blobFile = new BlobFichier();
                $blobFile->setName($fileName);
                $blobFile->setStatutSynchro(0);
                $blobFile->setDossier($subfolder);

                $dir = '/'.$repertoire;

                if ($subfolder) {
                    $dir .= '/'.$folder;
                }
                $blobFile->setChemin($dir);
                $blobFile->setExtension($extension);

                if (!$fileSystem->has($dir)) {
                    $fileSystem->createDir($dir);
                }
                $path = $dir.$blobFile->getId().'-0'.$extension;
                error_clear_last();
                if ($fileSystem->copy($pathFile, $path)) {
                    $hash = $fileSystem->sha1File($path);
                    $blobFile->setHash($hash);
                    $em->persist($blobFile);
                    $em->flush($blobFile);

                    return $blobFile->getId();
                } else {
                    $details = ' Error : '.print_r(error_get_last(), true);
                    $msg = 'Erreur insertion fichier au niveau du disque => '.$details;
                    throw new Exception($msg);
                }
            } else {
                throw new Exception('chemin du fichier incorrecte '.$pathFile);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Methode spécifique pour l'arbore.
     *
     * @param string $fileName
     * @param string $pathFile
     * @param string $folder
     *
     * @return int
     *
     * @throws Exception
     */
    public function insertDossierVolumineuxFile(string $content, DossierVolumineux $dossierVolumineux, string $prefix)
    {
        try {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $yy = date('Y');
            $mm = date('m');
            $dd = date('d');
            $folder = sprintf($this->getContainer()
                    ->getParameter('DIR_DOSSIERS_VOLUMINEUX').'/%s/%s/%s/', $yy, $mm, $dd);
            $file = sprintf('%s_%s.txt', $prefix, $dossierVolumineux->getUuidReference());

            if (!is_dir($folder) && !mkdir($folder, 0777, true) && !is_dir($folder)) {
                throw new Exception('Impossible de créer le dossier : '.$folder);
            }

            $filename = sprintf('%s/%s/%s/', $yy, $mm, $dd).$file;

            $fp = fopen($folder.$file, 'w+');
            fwrite($fp, $content);
            fclose($fp);

            $blobFile = new BlobFichier();
            $blobFile->setName($filename);
            $blobFile->setStatutSynchro(0);
            $em->persist($blobFile);

            $dossierVolumineux->setBlobLogfile($blobFile);
            $em->persist($dossierVolumineux);
            $em->flush();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Création d'un répertoire.
     *
     * @param $path
     *
     * @return bool
     */
    public function createDir($path)
    {
        $fileSystem = $this->mountManager->getFilesystem('nas');
        $isCreated = false;
        if (!$fileSystem->has($path)) {
            $isCreated = $fileSystem->createDir($path);
        }

        return $isCreated;
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getMountManager(): MountManager
    {
        return $this->mountManager;
    }
}
