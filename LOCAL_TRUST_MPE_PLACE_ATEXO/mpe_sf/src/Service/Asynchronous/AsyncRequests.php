<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Asynchronous;

use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\Response\TraceableResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class AsyncRequests
{
    final public const UNALLOWED_REQUESTS_NOT_FOUND = [
        '/insee/sirene/unites_legales/',
        '/siege_social',
    ];

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @throws TransportExceptionInterface
     */
    public function getMultiResponseByUrl(
        array $urlList,
        string $method,
        array $options
    ): array {
        return $this->getStreamMultiplexingResponses(
            $this->constructResponsesByUrl($urlList, $method, $options)
        );
    }

    public function constructResponsesByUrl(array $urlList, string $method, array $options): array
    {
        if (!$urlList) {
            return [];
        }

        $responses = [];
        foreach ($urlList as $url) {
            $responses[] = $this->httpClient->request($method, $url, $options);
        }

        return $responses;
    }

    /**
     * @param array|TraceableResponse[] $responses
     *
     * @throws TransportExceptionInterface
     */
    public function getStreamMultiplexingResponses(array $responses): array
    {
        $this->logger->info("Début récupération Multi Reponses en Async.");

        $data = [];
        $isNotAllowed = false;
        foreach ($responses as $response) {
            try {
                if ($response->getStatusCode() === Response::HTTP_OK) {
                    $data[] = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);

                    $this->logger->info(
                        sprintf("Récupération réussi de la request pour le ws: %s", $response->getInfo('url'))
                    );
                } else {
                    $this->logger->error(
                        sprintf("Requête en échec de la request pour le ws: %s", $response->getInfo('url'))
                    );
                    foreach (self::UNALLOWED_REQUESTS_NOT_FOUND as $value) {
                        if (str_contains($response->getInfo('url'), $value)) {
                            $isNotAllowed = true;
                        }
                    }
                }
            } catch (Exception $e) {
                $this->logger->error(
                    sprintf(
                        "Erreur récupération réponse ws: %s %s",
                        $response->getInfo('url'),
                        $e->getMessage()
                    )
                );
            }
        }

        $this->logger->info("Fin récupération Multi Reponses en Async.");

        return $isNotAllowed ? [] : $data;
    }
}
