<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelService
{
    public final const EXCEL_EXTENSION = '.xlsx';

    /**
     * @param string $name
     * @param array $values
     * ['headers' => [], 'informations => []]
     * Contains excel file headers (Column letter => Header) and the content.
     *
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function generateBasicExcelFile(string $name, array $values): array
    {
        $fileName = $name . self::EXCEL_EXTENSION;
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $columnLetterList = [];

        foreach ($values['headers'] as $columnLetter => $columnValue) {
            $sheet->setCellValue($columnLetter . '1', $columnValue);
            $columnLetterList[] = $columnLetter;
        }

        $numberColumn = 2;
        foreach ($values['informations'] as $arrayInfoSociete) {
            $i = 0;
            foreach ($arrayInfoSociete as $information) {
                $sheet->setCellValue($columnLetterList[$i] . $numberColumn, $information);
                $i++;
            }
            $numberColumn++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($temp_file);

        if (file_get_contents($temp_file) == '' || !file_get_contents($temp_file)) {
            throw new Exception('Erreur lors de la génération du fichier excel :' . $name);
        }

        return pathinfo($temp_file);
    }
}
