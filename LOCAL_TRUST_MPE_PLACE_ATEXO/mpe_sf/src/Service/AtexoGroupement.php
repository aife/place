<?php

namespace App\Service;

use Exception;
use DateTime;
use Doctrine\ORM\Exception\ORMException;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TGroupementEntreprise;
use App\Entity\TMembreGroupementEntreprise;
use App\Entity\TRoleJuridique;
use App\Entity\TTypeGroupementEntreprise;
use App\Event\DepotValidationSynchronizeDocumentsEvent;
use App\Event\ResponseEvents;
use App\Listener\ResponseListener;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class AtexoGroupement
{
    /**
     * AtexoGroupement constructor.
     */
    public function __construct(
        private ContainerInterface $container,
        private EntityManagerInterface $em,
        private readonly AtexoConfiguration $atexoConfiguration,
        private readonly AtexoEntreprise $atexoEntreprise,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security
    ) {
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Permet d'initialiser les paramettres du twig.
     *
     * @return array paramettre
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016
     */
    public function getParamsConfig(): array
    {
        $params = [];
        $configGroupement = $this->container->get(AtexoConfiguration::class)->hasConfigPlateforme('groupement');

        if ($configGroupement) {
            $params['idTypeGroupementSolidaire'] = $this->parameterBag->get('ID_TYPE_GROUPEMENT_SOLIDAIRE');
            $params['idTypeGroupementConjointNonSolidaire'] = $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
            $params['idTypeGroupementConjointSolidaire'] = $this->parameterBag->get('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
            $params['idRoleJuridiqueMandataire'] = $this->parameterBag->get('ID_ROLE_JURIDIQUE_MANDATAIRE');
            $params['groupement'] = null;
            $params['moduleGroupement'] = 1;
        }

        return $params;
    }

    /**
     * Permet de sauvgarder le groupement entreprise.
     *
     * @param string $uidResponse
     *
     * @return void
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since develop
     *
     * @copyright Atexo 2016*/
    public function saveGroupementEntreprise(Request $request, int $idOffre, $consultation, $uidResponse): void
    {
        $groupement = null;
        $consultationId = $consultation->getId();

        $session = $request->getSession();
        $groupementOui = $session->get(
            $session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/groupementOui"
        );
        $declarerGroupementOui = $session->get(
            $session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/declarerGroupement"
        );
        $idType = $session->get(
            $session->get('token_contexte_authentification_' . $consultationId)
            . "/$uidResponse/idTypeGroupement"
        );
        $em = $this->em;

        if ($groupementOui) {
            if ($declarerGroupementOui) {
                $groupement = $session->get($session->get('token_contexte_authentification_' . $consultationId) . "/$uidResponse/groupement");
            }

            if (!$groupement instanceof TGroupementEntreprise) {
                $groupement = new TGroupementEntreprise();
            }

            $type = $em->getReference(TTypeGroupementEntreprise::class, $idType);

            if ($type instanceof TTypeGroupementEntreprise) {
                $groupement->setIdTypeGroupement($type);
            }

            if ($groupement instanceof TGroupementEntreprise) {
                $groupement->setIdOffre($idOffre);
                $groupement->setDateCreation(new DateTime(date('Y-m-d H:i:s')));
                $roles = [];

                foreach ($groupement->getMembresGroupement() as $menbre) {
                    if ($menbre instanceof TMembreGroupementEntreprise) {
                        if (
                            !isset($roles[$menbre->getIdRoleJuridique()->getIdRoleJuridique()]) ||
                            !$roles[$menbre->getIdRoleJuridique()->getIdRoleJuridique()] instanceof TRoleJuridique
                        ) {
                            $roles[$menbre->getIdRoleJuridique()->getIdRoleJuridique()] = $em->getReference(TRoleJuridique::class, $menbre->getIdRoleJuridique()->getIdRoleJuridique());
                        }

                        $menbre->setIdRoleJuridique($roles[$menbre->getIdRoleJuridique()->getIdRoleJuridique()]);
                        $idEntrepriseMembre = $menbre->getEntreprise()->getId();
                        $entreprise = $em->getReference(Entreprise::class, $idEntrepriseMembre);
                        $menbre->setEntreprise($entreprise);
                        $idEtablissementMembre = null;

                        if ($menbre->getEtablissement() instanceof Etablissement) {
                            $idEtablissementMembre = $menbre->getEtablissement()->getIdEtablissement();

                            $etablissement = $em->getReference(
                                Etablissement::class,
                                $idEtablissementMembre
                            );

                            $menbre->setEtablissement($etablissement);
                        }

                        $menbre->setGroupement($groupement);
                        $this->addSynchronisationDocument(
                            $consultation,
                            $idEntrepriseMembre,
                            $idEtablissementMembre
                        );

                        foreach ($menbre->getSousMembres() as $ssMenbre) {
                            if ($ssMenbre instanceof TMembreGroupementEntreprise) {
                                if (
                                    !isset($roles[$ssMenbre->getIdRoleJuridique()->getIdRoleJuridique()]) ||
                                    !$roles[$ssMenbre->getIdRoleJuridique()->getIdRoleJuridique()] instanceof TRoleJuridique
                                ) {
                                    $roles[$ssMenbre->getIdRoleJuridique()->getIdRoleJuridique()] = $em->getReference(TRoleJuridique::class, $ssMenbre->getIdRoleJuridique()->getIdRoleJuridique());
                                }

                                $ssMenbre->setIdRoleJuridique($roles[$ssMenbre->getIdRoleJuridique()->getIdRoleJuridique()]);
                                $idEntrepriseSsMembre = $ssMenbre->getEntreprise()->getId();
                                $entreprise = $em->getReference(Entreprise::class, $idEntrepriseSsMembre);
                                $ssMenbre->setEntreprise($entreprise);
                                $idEtablissementSsMembre = null;

                                if ($ssMenbre->getEtablissement() instanceof Etablissement) {
                                    $idEtablissementSsMembre = $ssMenbre->getEtablissement()->getIdEtablissement();

                                    $etablissement = $em->getReference(
                                        Etablissement::class,
                                        $idEtablissementSsMembre
                                    );

                                    $ssMenbre->setEtablissement($etablissement);
                                }

                                $ssMenbre->setGroupement($groupement);
                                $this->addSynchronisationDocument(
                                    $consultation,
                                    $idEntrepriseSsMembre,
                                    $idEtablissementSsMembre
                                );
                            }
                        }
                    }
                }

                $em->persist($groupement);
                $em->flush();
            }
        }
    }

    /**
     * @param $reference
     * @param $idEntreprise
     * @param $idEtablissement
     */
    public function addSynchronisationDocument($reference, $idEntreprise, $idEtablissement): void
    {
        $eventDispatcher = new EventDispatcher();
        $listener = $this->get(ResponseListener::class);
        $synchronizeDocumentsEvent = new DepotValidationSynchronizeDocumentsEvent($reference, $idEntreprise, $idEtablissement);
        $eventDispatcher->addListener(ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS, [$listener, ResponseEvents::CALLBACK[ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS]]);
        $eventDispatcher->dispatch($synchronizeDocumentsEvent, ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS);
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    protected function get($id): object
    {
        return $this->container->get($id);
    }

    /**
     * @param $session
     *
     * @return mixed
     */
    public function getCandidature($contexteAuthentification): mixed
    {
        $consultation = $contexteAuthentification['reference_consultation'];
        $organisme = $contexteAuthentification['organisme_consultation'];

        /**
         * @Inscrit
         */
        $inscrit = $this->security->getUser();

        $inscritId = $contexteAuthentification['id'];
        $entrepriseId = $contexteAuthentification['entreprise_id'];
        $etablissementId = $contexteAuthentification['etablissement_id'];

        if ($inscrit instanceof Inscrit) {
            $inscritId = $inscrit->getId();
            $entrepriseId = $inscrit->getEntrepriseId();
            $etablissementId = $inscrit->getIdEtablissement();
        }

        $candidature = $this->em->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $organisme,
                'consultation' => $consultation,
                'idInscrit' => $inscritId,
                'idEntreprise' => $entrepriseId,
                'idEtablissement' => $etablissementId,
                'status' => $this->parameterBag->get('STATUT_ENV_BROUILLON'),
            ]);

        if (null == $candidature) {
            $consultation = $this->container->get('doctrine')
                ->getRepository(Consultation::class)
                ->find($consultation);

            $organisme = $this->container->get('doctrine')
                ->getRepository(Organisme::class)
                ->find($organisme);

            $candidature = new TCandidature();

            $candidature->setConsultation($consultation);
            $candidature->setOrganisme($organisme);
            $candidature->setIdInscrit($inscritId);
            $candidature->setIdEntreprise($entrepriseId);
            $candidature->setIdEtablissement($etablissementId);
            $candidature->setStatus($this->parameterBag->get('STATUT_ENV_BROUILLON'));

            $this->em->persist($candidature);
            $this->em->flush();
        }

        return $candidature;
    }

    /**
     * Permet de récupérer le groupement.
     *
     * @param $contexteAuthentification
     *
     * @return array
     */
    public function getGroupement($contexteAuthentification): array
    {
        $candidature = $this->getCandidature($contexteAuthentification);

        $groupement = $this->em->getRepository(TGroupementEntreprise::class)
            ->findOneBy([
                'candidature' => $candidature,
            ]);

        return [
            'groupement' => $groupement,
            'candidature' => $candidature,
        ];
    }

    /**
     * Permet de supprimer le groupement.
     *
     * @param $idGroupementEntreprise
     */
    public function supprimerGroupement($idGroupementEntreprise): void
    {
        $groupement = $this->em
            ->getRepository(TGroupementEntreprise::class)
            ->findOneBy([
                'idGroupementEntreprise' => $idGroupementEntreprise,
            ]);

        $this->em->remove($groupement);
        $this->em->flush();
    }

    /**
     * Permet de supprimer les membres et sous-membres d'un groupement.
     *
     * @param $idGroupementEntreprise
     */
    public function supprimerMembresGroupement($idGroupementEntreprise)
    {
        $membresGroupement = $this->em
            ->getRepository(TMembreGroupementEntreprise::class)
            ->findBy([
                'idGroupementEntreprise' => $idGroupementEntreprise,
            ]);

        foreach ($membresGroupement as $membreGroupement) {
            if (
                $membreGroupement->getIdRoleJuridique()->getIdRoleJuridique()
                != $this->parameterBag->get('ID_ROLE_JURIDIQUE_SOUS_TRAITANT')
            ) {
                $sousMembres = $this->em->getRepository(TMembreGroupementEntreprise::class)
                    ->findBy([
                        'idMembreParent' => $membreGroupement->getIdMembreGroupementEntreprise(),
                    ]);

                foreach ($sousMembres as $sousMembre) {
                    $this->em->remove($sousMembre);
                }

                $this->em->remove($membreGroupement);
            }
        }

        $this->em->flush();
    }

    /**
     * @param TGroupementEntreprise $groupement
     * @param $idRoleJuridique
     * @param string $siren
     * @param string $siret
     * @param int|null $idEntrepriseParent
     * @param array $foreignCountry
     *
     * @return string
     *
     * @throws ORMException
     */
    public function ajouterMembreAuGroupement(
        TGroupementEntreprise $groupement,
        $idRoleJuridique,
        string $siren,
        string $siret,
        int $idEntrepriseParent = null,
        $foreignCountry = []
    ): string {
        $msgErreur = '';
        $membreExiste = false;

        if (0 == count($foreignCountry)) {
            $entreprise = $this->em->getRepository(Entreprise::class)
            ->getEntrepriseBySiren($siren);
        } else {
            $entreprise = $this->em->getRepository(Entreprise::class)
                ->getEntrepriseBySirenEtrangerAndCountry($foreignCountry);
        }

        if ($entreprise instanceof Entreprise) {
            $membreExiste = $this->em->getRepository(TMembreGroupementEntreprise::class)
                ->isMembreGroupementEntreprise($groupement, $entreprise->getId());
        } else {
            $entreprise = new Entreprise();
            $entreprise->setSiren($siren);
        }

        if (!$membreExiste) {
            $coTraitant = $this->getCoSousTraitant($idRoleJuridique, $entreprise, $siret, $groupement);

            if ($coTraitant instanceof TMembreGroupementEntreprise) {
                if ($idRoleJuridique == $this->parameterBag->get('ID_ROLE_JURIDIQUE_CO_TRAITANT')) {
                    $groupement->addMembresGroupement($coTraitant);
                } elseif ($idRoleJuridique == $this->parameterBag->get('ID_ROLE_JURIDIQUE_SOUS_TRAITANT')) {
                    $membreParent = $this->em->getRepository(TMembreGroupementEntreprise::class)
                        ->findOneBy([
                            'idGroupementEntreprise' => $groupement->getIdGroupementEntreprise(),
                            'idEntreprise' => $idEntrepriseParent,
                        ]);

                    if ($membreParent instanceof TMembreGroupementEntreprise) {
                        $coTraitant->setIdMembreParent($membreParent);
                    }
                }

                $this->em->persist($coTraitant);
                $this->em->persist($groupement);
                $this->em->flush();
            } elseif (!($entreprise instanceof Entreprise && $entreprise->getId())) {
                $msgErreur = 'ENTREPRISE_N_EXISTE_PAS';
            }
        } else {
            $msgErreur = 'SIRET_EXISTE_DEJA';
        }

        return $msgErreur;
    }

    /**
     * @param $idRoleJuridique
     * @param $siret
     * @param $groupement
     *
     * @return TMembreGroupementEntreprise|null
     *
     * @throws ORMException
     */
    public function getCoSousTraitant(
        $idRoleJuridique,
        Entreprise &$entreprise,
        $siret,
        $groupement
    ) {
        $etablissement = null;
        $membreGroupement = null;

        if (!$entreprise->getId()) {
            $configSgmap = $this->atexoConfiguration->hasConfigPlateforme('synchronisationSgmap');
            $configSyncroCreation = $this->parameterBag
                ->get('ACTIVER_SYNCHRONISATION_SGMAP_LORS_DE_CREATION_ENTREPRISE');

            if ($configSgmap && $configSyncroCreation) {
                $entreprise = $this->atexoEntreprise->synchroEntrepriseAvecApiGouvEntreprise($entreprise->getSiren());

                if ($entreprise instanceof Entreprise) {
                    if ($entreprise->getNicsiege() != $siret) {
                        $atexoEtablissement = $this->container->get(AtexoEtablissement::class);
                        $etablissement =
                            $atexoEtablissement->synchroEtablissementAvecApiGouvEntreprise($entreprise->getSiren() . $siret);

                        if ($etablissement instanceof Etablissement) {
                            $entreprise->addEtablissement($etablissement);
                        }
                    }
                }
            }
        }

        if ($entreprise instanceof Entreprise && $entreprise->getId()) {
            if (!$etablissement instanceof Etablissement) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->getEtablissementByCodeAndIdEntreprise($siret, $entreprise->getId());

                if (!$etablissement instanceof Etablissement) {
                    $etablissement = $this->em->getRepository(Etablissement::class)
                        ->getEtablissementSiegeByIdEntreprise($entreprise->getId());
                }
            }

            $membreGroupement = new TMembreGroupementEntreprise();
            $membreGroupement->setIdEntreprise($entreprise->getId());
            $membreGroupement->setEntreprise($entreprise);

            if ($etablissement instanceof Etablissement) {
                $membreGroupement->setIdEtablissement($etablissement->getIdEtablissement());
                $membreGroupement->setEtablissement($etablissement);
            }

            $role = $this->em->getReference(TRoleJuridique::class, $idRoleJuridique);
            $membreGroupement->setIdRoleJuridique($role);
            $membreGroupement->setIdGroupementEntreprise($groupement->getIdGroupementEntreprise());
            $membreGroupement->setGroupement($groupement);
        }

        return $membreGroupement;
    }

    public function getGroupementByOffreId(int|string $offreId): ?TGroupementEntreprise
    {
        $groupement = $this->em->getRepository(TGroupementEntreprise::class)
            ->findOneBy([
                'idOffre' => $offreId,
            ]);

        return $groupement;
    }
}
