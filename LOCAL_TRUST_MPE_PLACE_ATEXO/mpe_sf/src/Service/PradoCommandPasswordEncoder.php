<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class PradoCommandPasswordEncoder implements PradoPasswordEncoderInterface
{
    public function __construct(private PasswordHasherFactoryInterface $hasherFactory)
    {
    }

    /**
     * @inheritDoc
     */
    public function encodePassword(string $userClassName, string $plainPassword): string
    {
        return $this->hasherFactory->getPasswordHasher($userClassName)
            ->hash($plainPassword);
    }

    /**
     * @inheritDoc
     */
    public function isPasswordValid(string $userClassName, string $encodedPassword, string $plainPassword): bool
    {
        return $this->hasherFactory->getPasswordHasher($userClassName)
            ->verify($encodedPassword, $plainPassword);
    }
}
