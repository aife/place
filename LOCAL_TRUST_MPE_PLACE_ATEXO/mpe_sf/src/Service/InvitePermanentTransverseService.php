<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;

class InvitePermanentTransverseService
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function removeInvitePermanentTransverseByAgent(Agent $agent, $flush = false): void
    {
        $invitePermanentTransverseRepository = $this->em->getRepository(InvitePermanentTransverse::class);
        $invitesPermanentTransverse = $invitePermanentTransverseRepository->findBy(
            ['agent' => $agent]
        );

        foreach ($invitesPermanentTransverse as $invite) {
            $this->em->remove($invite);
        }

        if ($flush) {
            $this->em->flush();
        }
    }

    public function createInvitePermanentTransverse(Agent $agent, array $serviceIds, bool $flush = false): void
    {
        foreach ($serviceIds as $serviceId) {
            $service = null;
            if (!empty($serviceId)) {
                $service = $this->em->getRepository(Service::class)->find($serviceId);
            }
            $invitePermanentTransverse = new InvitePermanentTransverse();
            $invitePermanentTransverse->setAgent($agent);
            $invitePermanentTransverse->setService($service);
            $invitePermanentTransverse->setAcronyme($agent->getOrganisme());
            $this->em->persist($invitePermanentTransverse);
        }

        if ($flush) {
            $this->em->flush();
        }
    }
}
