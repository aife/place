<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\JWT;

use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;

class Jwt
{
    public function __construct(private readonly JWTEncoderInterface $jwtEncoder)
    {
    }

    /**
     * @return bool
     */
    public function tokenIsValid(string $token)
    {
        $isValid = true;

        try {
            $this->jwtEncoder->decode($token);
        } catch (JWTDecodeFailureException) {
            $isValid = false;
        }

        return $isValid;
    }
}
