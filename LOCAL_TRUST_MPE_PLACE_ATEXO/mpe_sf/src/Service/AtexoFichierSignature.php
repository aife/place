<?php

namespace App\Service;

use Exception;
use App\Entity\EnveloppeFichier;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AtexoFichierSignature de gestion des signatures de fichiers.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class AtexoFichierSignature
{
    /**
     * AtexoFichierSignature constructor.
     *
     * @param $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        private ContainerInterface $container,
        private LoggerInterface $logger,
        private AtexoUtil $atexoUtil,
        private AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Permet de verifier la signature d'un fichier.
     *
     * @param $fichier
     * @param string $pdfPath
     *
     * @return string json contenant le resultat de la verification de la signature
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function verifierSignature($fichier, $pdfPath): string
    {
        $hashFichier = null;
        $contentSignature = null;
        $hash256 = null;
        $idFichier = null;
        try {
            if ($fichier instanceof EnveloppeFichier) {
                $idFichier = $fichier->getIdFichier();
                $this->logger->info("Debut verification signature fichier : id = $idFichier");
                $atexoBlobOrganisme = $this->atexoFichierOrganisme;
                $contentSignature = $atexoBlobOrganisme->getContentSignatureFichier($fichier);
                if ('' == $contentSignature) {
                    $contentSignature = $fichier->getSignatureFichier();
                }
                $hashFichier = $fichier->getHash();
                $hash256 = $fichier->getHash256();
            }

            //TODO type signature
            $this->logger->info("Appel de la methode verifierSignatureFichier avec les parametres [Hash = $hashFichier ], [Longueur signature = ".strlen((string) $contentSignature).' caracteres], [type_signature = INCONNUE]');
            if ('PADES' == $fichier->getTypeSignatureFichier()) {
                $infosSignature = $this->getContainer()->get('atexo_crypto.crypto')->verifierSignaturePdf($pdfPath, 'PADES');
            } else {
                $infosSignature = $this->getContainer()->get('atexo_crypto.crypto')
                    ->verifierSignatureHash($hashFichier, $contentSignature, 'INCONNUE', $hash256);
            }

            if (is_array($infosSignature)) {
                $infosSignature = $infosSignature[0];
            }

            $infosSignature = $this->atexoUtil->encodeToJson((array) $infosSignature);

            $infosSignature = str_replace('\u0000*\u0000', '', $infosSignature);
            $messageResultat = <<<EOT

Resultat verification signature : $infosSignature

Fin verification signature fichier : id = $idFichier
EOT;

            $this->logger->info($messageResultat);

            return (!empty($infosSignature)) ? $infosSignature : '';
        } catch (Exception $e) {
            $this->logger->info("Erreur lors de la verification de la signature: [idFichier = $idFichier], ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());

            return '';
        }
    }
}
