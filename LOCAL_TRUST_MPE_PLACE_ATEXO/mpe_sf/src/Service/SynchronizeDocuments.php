<?php

namespace App\Service;

use Exception;
use App\WebService\AtexoSgmapWebService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SynchronizeDocumentsHandler d'initialisation de la synchronisation des documents SGMAP.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class SynchronizeDocuments
{
    /**
     * SynchronizeDocumentsHandler constructor.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        /**
         * @var
         */
        private readonly ContainerInterface $container,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * Permet d'initialiser la synchronisation des documents entreprise et/ou etablissement.
     *
     * @param DepotValidationSynchronizeDocumentsEvent $event
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function synchronizeDocuments($reference, $idEntreprise, $idEtablissement, $idOffre = ''): string
    {
        $statusCodeWsEntreprise = null;
        $urlSynchroDocs = null;
        try {
            $urlSynchroDocs = $this->container
                    ->getParameter('PF_URL_REFERENCE') . $this->container
                    ->getParameter('atexo_uri_service_synchro_documents');
            //print_r($urlSynchroDocs);exit;
            $paramForLog = $erreur = "[reference=$reference], [idEntreprise=$idEntreprise], 
            [idEtablissement=$idEtablissement], [idOffre=$idOffre],";
            $resultat = '';

            if (!empty($idEntreprise)) {
                $urlSynchroDocsEntreprise = str_replace(
                    '{__id__}',
                    $idEntreprise,
                    str_replace(
                        '{__ref__}',
                        $reference,
                        str_replace('{__offre__}', "/$idOffre", $urlSynchroDocs)
                    )
                );
                $atexoSynchroDocumentsEntreprise = new AtexoSgmapWebService(
                    $urlSynchroDocsEntreprise,
                    $this->container->getParameter('sgmap_type_document')['entreprise'],
                    $this->logger,
                    $this->container
                );
                $resultat .= "\nLa synchronisation de l'entreprise " . $paramForLog;
                $this->logger->info("Initialisation de l'appel du ws de synchronisation des documents 
                ENTREPRISE avec les parametres : $paramForLog");

                $atexoSynchroDocumentsEntreprise->request($urlSynchroDocsEntreprise, 'GET', []);
                $statusCodeWsEntreprise = $atexoSynchroDocumentsEntreprise->getStatusCode();

                $this->logger->info("Fin appel du ws. Statut Code = $statusCodeWsEntreprise");
                $resultat .= "\nFin de la synchronisation de l'entreprise.Statut Code = " . $statusCodeWsEntreprise;
            }

            if (!empty($idEtablissement)) {
                $urlSynchroDocsEtablissement = str_replace(
                    '{__id__}',
                    $idEtablissement,
                    str_replace(
                        '{__ref__}',
                        $reference,
                        str_replace('{__offre__}', "/$idOffre", $urlSynchroDocs)
                    )
                );
                $resultat .= "\nLa synchronisation de l'etablissement " . $paramForLog;
                $atexoSynchroDocumentsEtablissement = new AtexoSgmapWebService(
                    $urlSynchroDocsEtablissement,
                    $this->container->getParameter('sgmap_type_document')['etablissement'],
                    $this->logger,
                    $this->container
                );

                $this->logger->info("Initialisation de l'appel du ws de synchronisation des documents 
                ETABLISSEMENT avec les parametres : $paramForLog");

                $atexoSynchroDocumentsEtablissement->request($urlSynchroDocsEtablissement, 'GET', []);
                $statusCodeWsEtablissemnt = $atexoSynchroDocumentsEtablissement->getStatusCode();

                $this->logger->info("Fin appel du ws. Statut Code = $statusCodeWsEtablissemnt");
                $resultat .= "\nFin de la synchronisation de l'etablissment.Statut Code = " . $statusCodeWsEntreprise;
            }
            $resultat .= "\nFin de la synchronisation ";

            return $resultat;
        } catch (Exception $e) {
            $erreur = "Erreur lors de l'appel du ws avec les parametres [reference=$reference], 
            [idEntreprise=$idEntreprise], [idEtablissement=$idEtablissement], [idOffre=$idOffre], 
            [urlSynchroDocs=$urlSynchroDocs]";
            $erreur .= PHP_EOL . 'Exception ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
            $this->logger->error($erreur);

            return $erreur;
        }
    }
}
