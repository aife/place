<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use DateTime;
use App\Entity\Administrateur;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class RgpdService
{
    final const ENTREPRISE = 'entreprise';
    final const AGENT = 'agent';

    public function __construct(private readonly EntityManagerInterface $em, private readonly Security $security, private readonly AtexoService $atexoService, private readonly SessionInterface $session)
    {
    }

    public function getUserConnected(): ?UserInterface
    {
        return $this->security->getUser() ??
            (isset($this->session->get('symfonySessionToPrado')['id']) ?
                $this->atexoService->getAgent(
                    (int)$this->session->get('symfonySessionToPrado')['id']
                )
                : $this->getSessionUser()
            );
    }

    protected function getSessionUser(): ?UserInterface
    {
        $user = null;
        $session = $this->session->get('contexte_authentification');
        if ($session['type_user'] === self::ENTREPRISE) {
            $user = $this->atexoService->getInscrit($session['id']);
        } elseif ($session['type_user'] === self::AGENT) {
            $user = $this->atexoService->getAgent($session['id']);
        }
        return $user;
    }

    public function saveRgpd(Request $request): UserInterface
    {
        $user = $this->getUserConnected();

        $user
            ->setDateValidationRgpd(new DateTime())
            ->setRgpdCommunicationPlace($request->request->getBoolean('communicationPlaceRgpd'))
            ->setRgpdEnquete($request->request->getBoolean('enqueteRgpd'));

        if ($user instanceof Inscrit) {
            $user->setRgpdCommunication($request->request->getBoolean('communicationRgpd'));
        }

        $this->em->flush();

        return $user;
    }

    public function displayPopUpRgpd(): bool
    {
        $user = $this->getUserConnected();

        /** @var ConfigurationPlateforme $configurationPlateforme */
        $configurationPlateforme = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme()
        ;

        if (
            !$user
            || $user instanceof Administrateur
            || !$configurationPlateforme->isRecueilConsentementRgpd()
        ) {
            return false;
        }

        if ($user->getDateValidationRgpd() instanceof DateTime) {
            $dateValidation = $user->getDateValidationRgpd();
            $currentDate = new DateTime();

            $dateDiff = $currentDate->diff($dateValidation);

            if ((int) $dateDiff->format('%y') < 1) {
                return false;
            }
        }

        return true;
    }
}
