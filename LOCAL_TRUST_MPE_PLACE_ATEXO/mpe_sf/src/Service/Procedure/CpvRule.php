<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class CpvRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (
            !$this->configuration->isModuleEnabled('AffichageCodeCpv', $organism)
            || !$this->isSelected($procedure, 'getAfficherCodeCpv')
        ) {
            $form->cpv = false;

            return $form;
        }

        $cpvModule = new ConsultationModule();
        $cpvOui = new ConsultationInput();
        $cpvOui->display = $this->isSelected($procedure, 'getAfficherCodeCpv');
        $cpvOui->mandatory = $this->isSelected($procedure, 'getCodeCpvObligatoire');
        $cpvModule->yes = $cpvOui;

        $form->cpv = $cpvModule;

        return $form;
    }
}
