<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Service\Consultation\ConsultationProcedureEquivalence;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProcedureEquivalenceAgentService
{
    public function __construct(
        protected EntityManagerInterface $em,
        protected ParameterBagInterface $parameterBag,
        private ConsultationProcedureEquivalence $consultationProcedureEquivalence
    ) {
    }

    /**
     * @param $value
     */
    public function getActivate($value): int
    {
        $integer = (int) 0;
        $value = trim((string) $value);
        if ((!strnatcasecmp($value, '1')) || (!strnatcasecmp($value, '+1')) || (!strnatcasecmp($value, '-1'))) {
            $integer = (int) 1;
        } elseif ((!strnatcasecmp($value, '0')) || (!strnatcasecmp($value, '+0')) || (!strnatcasecmp($value, '-0'))) {
            $integer = (int) 0;
        }

        return $integer;
    }

    public function setValueProcedure(
        string $key,
        Consultation $consultation,
        ProcedureEquivalence $procedureEquivalence
    ): Consultation {
        $setter = 'set' . $key;
        $getter = 'get' . $key;
        $consultation->$setter(
            $this->getActivate($procedureEquivalence->$getter())
        );

        return $consultation;
    }

    public function initializeConsultationWithParametreProcedure(
        Consultation $consultation,
        array $parametersName
    ): Consultation {

        $typeProcedure = $this->em->getRepository(TypeProcedureOrganisme::class)->findOneBy([
            'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
            'organisme' => $consultation->getOrganisme()->getAcronyme()
        ]);
        if ($typeProcedure) {
            $consultation->setIdRegleValidation($typeProcedure->getIdTypeValidation());
        }

        $procedureEquivalence = $this->em->getRepository(ProcedureEquivalence::class)->findOneBy([
            'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
            'organisme' => $consultation->getOrganisme()->getAcronyme()
        ]);

        if ($procedureEquivalence) {
            $consultationProcedureEquivalence = $this->consultationProcedureEquivalence;
            foreach ($parametersName as $method) {
                $setter = 'set' . $method;
                $consultation = $consultationProcedureEquivalence->$setter(
                    $consultation,
                    $procedureEquivalence
                );
            }
        }

        return $consultation;
    }
}
