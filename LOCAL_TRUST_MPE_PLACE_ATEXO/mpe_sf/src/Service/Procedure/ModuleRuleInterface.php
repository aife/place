<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use Symfony\Component\DependencyInjection\Attribute\Autoconfigure;

#[Autoconfigure(tags: [self::class])]
interface ModuleRuleInterface
{
    public static function getRuleName(): string;
    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm;
    public function isActive(Organisme $organism): bool;
}
