<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Service\AtexoConfiguration;

class ProcedureOpenRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(
        private AtexoConfiguration $configuration,
        private TypeProcedureOrganismeRepository $procedureOrganismeRepository
    ) {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $procedureOrganism = $this->procedureOrganismeRepository->findOneBy(
            ['idTypeProcedure' => $procedure->getIdTypeProcedure(), 'organisme' => $procedure->getOrganisme()]
        );
        $form->procedureOpen = $this->configuration->isModuleEnabled('Publicite', $organism)
            && $procedureOrganism->getMapa();

        return $form;
    }
}
