<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class FormulaireDumeRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (
            !$this->configuration->isModuleEnabled('InterfaceDume', $organism)
        ) {
            $form->formulaireDume = false;

            return $form;
        }

        $formulaireDumeModule = new ConsultationModule();
        $formulaireDumeOui = new ConsultationInput();
        $formulaireDumeOui->selected = $this->isSelected($procedure, 'getTypeFormulaireDumeSimplifie');
        $formulaireDumeOui->display = $this->canSee($procedure, 'getTypeFormulaireDumeSimplifie');
        $formulaireDumeOui->fixed = $this->isFixed($procedure, 'getTypeFormulaireDumeSimplifie');

        $formulaireDumeNon = new ConsultationInput();
        $formulaireDumeNon->selected = $this->isSelected($procedure, 'getTypeFormulaireDumeStandard');
        $formulaireDumeNon->display = $this->canSee($procedure, 'getTypeFormulaireDumeStandard');
        $formulaireDumeNon->fixed = $this->isFixed($procedure, 'getTypeFormulaireDumeStandard');

        $formulaireDumeModule->yes = $formulaireDumeOui;
        $formulaireDumeModule->no = $formulaireDumeNon;

        $form->formulaireDume = $formulaireDumeModule;

        return $form;
    }
}
