<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class ReglementConsultationRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $rcModule = new ConsultationModule();
        $rcOui = new ConsultationInput();
        $rcOui->selected = $this->isSelected($procedure, 'getReglementCons');
        $rcOui->display = $this->canSee($procedure, 'getReglementCons');
        $rcOui->fixed = $this->isFixed($procedure, 'getReglementCons');
        $rcModule->yes = $rcOui;

        $form->reglementConsultation = $rcModule;

        return $form;
    }
}
