<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Repository\ConfigurationClientRepository;

class AutresInformationsRule implements ModuleRuleInterface
{
    public function __construct(private ConfigurationClientRepository $clientRepository)
    {
    }

    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm
    {
        $form->autresInformations = (null !== $this->clientRepository->findOneBy([
            'parameter' => 'MODULE_AUTRES_INFORMATIONS'
        ]));

        return $form;
    }

    public static function getRuleName(): string
    {
        return 'AutresInformationsRule';
    }

    public function isActive(Organisme $organism): bool
    {
        return null !== $this->clientRepository->findOneBy(['parameter' => 'MODULE_AUTRES_INFORMATIONS']);
    }
}
