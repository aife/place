<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class JoRule implements ModuleRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm
    {
        $form->jo = $this->configuration->isModuleEnabled('typageJo2024', $organism, 'is');

        return $form;
    }

    public static function getRuleName(): string
    {
        return 'JoRule';
    }

    public function isActive(Organisme $organism): bool
    {
        return $this->configuration->isModuleEnabled('typageJo2024', $organism, 'is');
    }
}
