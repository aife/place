<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Repository\ConfigurationClientRepository;
use App\Service\AtexoConfiguration;

class VisiteLieuxReunionRule implements ModuleRuleInterface
{
    public function __construct(private ConfigurationClientRepository $clientRepository)
    {
    }

    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm
    {
        $form->visiteLieuxReunion = (null !== $this->clientRepository->findOneBy([
            'parameter' => 'MODULE_VISITE_LIEUX_REUNION'
        ]));

        return $form;
    }

    public static function getRuleName(): string
    {
        return 'VisiteLieuxReunionRule';
    }

    public function isActive(Organisme $organism): bool
    {
        return (null !== $this->clientRepository->findOneBy(['parameter' => 'MODULE_VISITE_LIEUX_REUNION']));
    }
}
