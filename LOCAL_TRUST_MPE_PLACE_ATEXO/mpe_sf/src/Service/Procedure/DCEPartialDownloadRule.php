<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class DCEPartialDownloadRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $dcePartialDlModule = new ConsultationModule();
        $dcePartialDlOui = new ConsultationInput();
        $dcePartialDlOui->selected = $this->isSelected($procedure, 'getPartialDceDownload');
        $dcePartialDlOui->display = $this->canSee($procedure, 'getPartialDceDownload');
        $dcePartialDlOui->fixed = $this->isFixed($procedure, 'getPartialDceDownload');
        $dcePartialDlModule->yes = $dcePartialDlOui;

        $form->dcePartialDownload = $dcePartialDlModule;

        return $form;
    }
}
