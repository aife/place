<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class TypeProcedureDumeRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (
            !$this->configuration->isModuleEnabled('InterfaceDume', $organism)
            || (
                !$this->canSee($procedure, 'getTypeProcedureDume')
            )
        ) {
            $form->typeProcedureDume = false;

            return $form;
        }

        $typeProcedureDumeModule = new ConsultationModule();
        $typeProcedureDumeOui = new ConsultationInput();
        $typeProcedureDumeOui->selected = $this->isSelected($procedure, 'getTypeProcedureDume');
        $typeProcedureDumeOui->display = $this->canSee($procedure, 'getTypeProcedureDume');
        $typeProcedureDumeOui->fixed = $this->isFixed($procedure, 'getTypeProcedureDume');
        $typeProcedureDumeModule->yes = $typeProcedureDumeOui;
        $form->typeProcedureDume = $typeProcedureDumeModule;

        return $form;
    }
}
