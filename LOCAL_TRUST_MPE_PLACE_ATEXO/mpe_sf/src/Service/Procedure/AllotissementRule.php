<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class AllotissementRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $allotissementModule = new ConsultationModule();
        $allotissementOui = new ConsultationInput();
        $allotissementOui->selected = $this->isSelected($procedure, 'getEnvOffreTypeMultiple');
        $allotissementOui->display = $this->canSee($procedure, 'getEnvOffreTypeMultiple');
        $allotissementOui->fixed = $this->isFixed($procedure, 'getEnvOffreTypeMultiple');

        $allotissementNon = new ConsultationInput();
        $allotissementNon->selected = $this->isSelected($procedure, 'getEnvOffreTypeUnique');
        $allotissementNon->display = $this->canSee($procedure, 'getEnvOffreTypeUnique');
        $allotissementNon->fixed = $this->isFixed($procedure, 'getEnvOffreTypeUnique');

        $allotissementModule->yes = $allotissementOui;
        $allotissementModule->no = $allotissementNon;

        $form->allotissement = $allotissementModule;

        return $form;
    }
}
