<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class DumeRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (
            !$this->configuration->isModuleEnabled('InterfaceDume', $organism)
            || !$this->canSee($procedure, 'getDumeDemande')
        ) {
            $form->dume = false;

            return $form;
        }

        $dumeModule = new ConsultationModule();
        $dumeOui = new ConsultationInput();
        $dumeOui->selected = $this->isSelected($procedure, 'getDumeDemande');
        $dumeOui->display = $this->canSee($procedure, 'getDumeDemande');
        $dumeOui->fixed = $this->isFixed($procedure, 'getDumeDemande');
        $dumeModule->yes = $dumeOui;

        $form->dume = $dumeModule;

        return $form;
    }
}
