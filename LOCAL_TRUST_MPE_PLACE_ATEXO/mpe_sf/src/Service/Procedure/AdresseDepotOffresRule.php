<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AdresseDepotOffresRule implements ModuleRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration, private ParameterBagInterface $parameterBag)
    {
    }

    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm
    {
        $form->adresseDepotOffres = $this->configuration->isModuleEnabled('ConsultationAdresseDepotOffres', $organism)
            && false === (bool) $this->parameterBag->get('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE');

        return $form;
    }

    public static function getRuleName(): string
    {
        return 'AdresseDepotOffresRule';
    }

    public function isActive(Organisme $organism): bool
    {
        return $this->configuration->isModuleEnabled('ConsultationAdresseDepotOffres', $organism)
            && false === (bool) $this->parameterBag->get('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE');
    }
}
