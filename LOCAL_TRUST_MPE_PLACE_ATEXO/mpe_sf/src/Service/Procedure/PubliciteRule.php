<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class PubliciteRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (!$this->configuration->isModuleEnabled('Publicite', $organism)) {
            $form->publicite = false;

            return $form;
        }

        $publiciteModule = new ConsultationModule();
        $publiciteOui = new ConsultationInput();
        $publiciteOui->selected = $this->isSelected($procedure, 'getAutoriserPubliciteOui');
        $publiciteOui->display = $this->canSee($procedure, 'getAutoriserPubliciteOui');
        $publiciteOui->fixed = $this->isFixed($procedure, 'getAutoriserPubliciteOui');

        $publiciteNon = new ConsultationInput();
        $publiciteNon->selected = $this->isSelected($procedure, 'getAutoriserPubliciteNon');
        $publiciteNon->display = $this->canSee($procedure, 'getAutoriserPubliciteNon');
        $publiciteNon->fixed = $this->isFixed($procedure, 'getAutoriserPubliciteNon');

        $publiciteModule->yes = $publiciteOui;
        $publiciteModule->no = $publiciteNon;

        $form->publicite = $publiciteModule;

        return $form;
    }
}
