<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\ProcedureEquivalence;

class ProcedureRule
{
    public function isSelected(ProcedureEquivalence $procedure, string $procedureName): bool
    {
        return (bool) (int) $procedure->$procedureName();
    }

    public function canSee(ProcedureEquivalence $procedure, string $procedureName): bool
    {
        return !str_starts_with($procedure->$procedureName(), '-');
    }

    public function isFixed(ProcedureEquivalence $procedure, string $procedureName): bool
    {
        return !str_starts_with($procedure->$procedureName(), '+')
            && !str_starts_with($procedure->$procedureName(), '-');
    }
}
