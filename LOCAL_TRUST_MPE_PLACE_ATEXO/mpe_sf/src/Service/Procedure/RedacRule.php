<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;
use App\Service\AtexoConfiguration;

class RedacRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function __construct(private AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        if (!$this->configuration->isModuleEnabled('InterfaceModuleRsem', $organism)) {
            $form->redac = false;

            return $form;
        }

        $redacModule = new ConsultationModule();
        $redacOui = new ConsultationInput();
        $redacOui->selected = $this->isSelected($procedure, 'getDonneesComplementaireOui');
        $redacOui->display = $this->canSee($procedure, 'getDonneesComplementaireOui');
        $redacOui->fixed = $this->isFixed($procedure, 'getDonneesComplementaireOui');

        $redacNon = new ConsultationInput();
        $redacNon->selected = $this->isSelected($procedure, 'getDonneesComplementaireNon');
        $redacNon->display = $this->canSee($procedure, 'getDonneesComplementaireNon');
        $redacNon->fixed = $this->isFixed($procedure, 'getDonneesComplementaireNon');

        $redacModule->yes = $redacOui;
        $redacModule->no = $redacNon;

        $form->redac = $redacModule;

        return $form;
    }
}
