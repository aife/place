<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class AccesConsultationEntreprise extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $accesConsultationEntrepriseApresDateModule = new ConsultationModule();
        $accesConsultationEntrepriseApresDateOui = new ConsultationInput();
        $accesConsultationEntrepriseApresDateOui->selected = $this->isSelected($procedure, 'getDelaiDateLimiteRemisePli');
        $accesConsultationEntrepriseApresDateOui->display = $this->canSee($procedure, 'getDelaiDateLimiteRemisePli');
        $accesConsultationEntrepriseApresDateOui->fixed = $this->isFixed($procedure, 'getDelaiDateLimiteRemisePli');
        $accesConsultationEntrepriseApresDateOui->parameters = [
            'delai' => $procedure->getDelaiPoursuiteAffichage(),
            'unit'  => $procedure->getDelaiPoursuivreAffichageUnite()
        ];

        $accesConsultationEntrepriseApresDateNon = new ConsultationInput();
        $accesConsultationEntrepriseApresDateNon->selected = $this->isSelected($procedure, 'getPoursuiteDateLimiteRemisePli');
        $accesConsultationEntrepriseApresDateNon->display = $this->canSee($procedure, 'getPoursuiteDateLimiteRemisePli');
        $accesConsultationEntrepriseApresDateNon->fixed = $this->isFixed($procedure, 'getPoursuiteDateLimiteRemisePli');

        $accesConsultationEntrepriseApresDateModule->yes = $accesConsultationEntrepriseApresDateOui;
        $accesConsultationEntrepriseApresDateModule->no = $accesConsultationEntrepriseApresDateNon;

        $form->accesConsultationEntrepriseApresDate = $accesConsultationEntrepriseApresDateModule;

        return $form;
    }
}
