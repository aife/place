<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class AccesRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $accesModule = new ConsultationModule();
        $accesOui = new ConsultationInput();
        $accesOui->selected = $this->isSelected($procedure, 'getProcedureRestreinte');
        $accesOui->display = $this->canSee($procedure, 'getProcedureRestreinte');
        $accesOui->fixed = $this->isFixed($procedure, 'getProcedureRestreinte');

        $accesNon = new ConsultationInput();
        $accesNon->selected = $this->isSelected($procedure, 'getProcedurePublicite');
        $accesNon->display = $this->canSee($procedure, 'getProcedurePublicite');
        $accesNon->fixed = $this->isFixed($procedure, 'getProcedurePublicite');

        $accesModule->yes = $accesOui;
        $accesModule->no = $accesNon;

        $form->acces = $accesModule;

        return $form;
    }
}
