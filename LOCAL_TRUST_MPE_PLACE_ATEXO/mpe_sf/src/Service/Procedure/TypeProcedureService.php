<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class TypeProcedureService
{
    public function __construct(
        private TypeProcedureOrganismeRepository $procedureOrganismeRepository,
        private CacheInterface $cache,
        private readonly ParameterBagInterface $parameterBag,
    ) {
    }

    public function getTypeProcedureByIdAndOrganism(int $id, string $acronymOrg): ?TypeProcedureOrganisme
    {
        return $this->cache->get(
            'type_procedure_org_' . $id . '_' . $acronymOrg,
            function (ItemInterface $item) use ($id, $acronymOrg) {
                $item->expiresAfter(3600);
                return $this->procedureOrganismeRepository->findOneBy([
                    'idTypeProcedure' => $id,
                    'organisme' => $acronymOrg
                ]);
            }
        );
    }

    public function getTypeProcedureMapaInfByOrganisme(string $organisme): array
    {
        return $this->procedureOrganismeRepository->findBy([
            'organisme' => $organisme,
            'mapa' => '1',
            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_INF_90'),
            'activerMapa' => '1',
            'procedureSimplifie' => '0',
        ]);
    }

    public function getTypeProcedureMapaSupByOrganisme(string $organisme): array
    {
        return $this->procedureOrganismeRepository->findBy([
            'organisme' => $organisme,
            'mapa' => '1',
            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_SUP_90'),
            'activerMapa' => '1',
            'procedureSimplifie' => '0',
        ]);
    }

    public function getTypeProcedureFormaliseeByOrganisme(string $organisme): array
    {
        return $this->procedureOrganismeRepository->findBy([
            'organisme' => $organisme,
            'mapa' => '0',
            'procedureSimplifie' => '0',
        ]);
    }

    public function getTypeProcedureSimplifieeByOrganisme(string $organisme): array
    {
        return $this->procedureOrganismeRepository->findBy([
            'organisme' => $organisme,
            'procedureSimplifie' => '1',
        ]);
    }
}
