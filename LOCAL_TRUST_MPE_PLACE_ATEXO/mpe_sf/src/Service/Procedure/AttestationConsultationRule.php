<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Model\ConsultationForm;
use App\Service\AtexoConfiguration;

class AttestationConsultationRule implements ModuleRuleInterface
{
    public function __construct(private readonly AtexoConfiguration $configuration)
    {
    }

    public function apply(ConsultationForm $form, Organisme $organism): ConsultationForm
    {
        $form->attestationConsultation = $this->configuration->isModuleEnabled(
            'CaseAttestationConsultation',
            $organism
        );

        return $form;
    }

    public static function getRuleName(): string
    {
        return 'AttestationConsultationRule';
    }

    public function isActive(Organisme $organism): bool
    {
        return $this->configuration->isModuleEnabled('CaseAttestationConsultation', $organism);
    }
}
