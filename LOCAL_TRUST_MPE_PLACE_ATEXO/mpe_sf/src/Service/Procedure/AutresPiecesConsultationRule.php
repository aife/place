<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Procedure;

use App\Entity\Organisme;
use App\Entity\ProcedureEquivalence;
use App\Model\ConsultationForm;
use App\Model\ConsultationInput;
use App\Model\ConsultationModule;

class AutresPiecesConsultationRule extends ProcedureRule implements ProcedureRuleInterface
{
    public function apply(ConsultationForm $form, ProcedureEquivalence $procedure, Organisme $organism): ConsultationForm
    {
        $autresPiecesModule = new ConsultationModule();
        $autresPiecesOui = new ConsultationInput();
        $autresPiecesOui->selected = $this->isSelected($procedure, 'getAutrePieceCons');
        $autresPiecesOui->display = $this->canSee($procedure, 'getAutrePieceCons');
        $autresPiecesOui->fixed = $this->isFixed($procedure, 'getAutrePieceCons');
        $autresPiecesModule->yes = $autresPiecesOui;

        $form->autresPiecesConsultation = $autresPiecesModule;

        return $form;
    }
}
