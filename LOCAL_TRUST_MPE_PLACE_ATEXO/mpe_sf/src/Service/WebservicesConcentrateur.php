<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Attribute\ResultType;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebservicesConcentrateur extends AbstractService
{
    private const URI_ADD_TOKEN = 'agents';
    private const URI_CONFIG_ANNONCES = 'annonces/configuration';
    private const TIMEOUT = 10;

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('URL_CONCENTRATEUR_ANNONCE') . 'rest/v2/';
    }

    protected function createToken(string $token, array $body = [])
    {
        $url = $this->getMpeWsBaseUrl() . self::URI_ADD_TOKEN;

        $options = [
            'timeout' => self::TIMEOUT,
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => json_encode($body),
        ];

        $this->request(Request::METHOD_POST, $url, $options)->getContent();

        return $token;
    }

    protected function configAnnonces(string $token, array $body = [])
    {
        $url = $this->getMpeWsBaseUrl() . self::URI_CONFIG_ANNONCES;

        $options = [
            'timeout' => self::TIMEOUT,
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => json_encode($body),
        ];

        return $this->request(Request::METHOD_POST, $url, $options)->getContent();
    }
}
