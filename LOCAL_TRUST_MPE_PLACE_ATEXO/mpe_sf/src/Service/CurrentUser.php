<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service;

use App\Entity\Agent;
use App\Entity\Inscrit;
use App\Service\Agent\HabilitationTypeProcedureService;
use Stringable;
use LogicException;
use App\Entity\HabilitationAgent;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;

class CurrentUser
{
    /**
     * CurrentUser constructor.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        private readonly TokenStorageInterface $securityStorage,
        private readonly AuthorizationCheckerInterface $securityChecker,
        private readonly ParameterBagInterface $parameterBag,
        protected HabilitationTypeProcedureService $habilitationTypeProcedureService
    ) {
    }

    /**
     * Permet de recuperer l'utilisateur courant en session.
     *
     * @return null|string|Stringable|UserInterface
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @since ESR2016
     * @copyright Atexo 2016*/
    public function getCurrentUser()
    {
        if (!$this->securityStorage) {
            throw new LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->securityStorage->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    /**
     * Permet de verifier si l'utilisateur courant est connecte.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function isConnected(): bool
    {
        return null !== $this->getCurrentUser();
    }

    /**
     * Verifie si l'utilisateur a un role donne.
     *
     * @param $role
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function hasRole(string $role): bool
    {
        if ($this->securityChecker->isGranted($role)) {
            return true;
        }

        return false;
    }

    /**
     * Verifier si l'utilisateur a une habilitation : Les habilitations sont chargees
     * dans le repository de l'agent et inscrit au moment de l'authentification
     * dans la methode : loadUserByUsername($username).
     *
     * @param $habilitation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     *
     * @deprecated ne plus utilisé cette fonction !
     */
    public function hasHabilitation($habilitation): bool
    {
        if ($this->getCurrentUser()) {
            $userHabilitations = $this->getCurrentUser()->getHabilitations();
            if (in_array($habilitation, $userHabilitations)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Les habilitations sont chargees dans le repository de l'agent et inscrit au moment de l'authentification
     * dans la methode : loadUserByUsername($username).
     */
    public function checkHabilitation(string $habilitation, string $prefixMethod = 'get'): bool
    {
        if ($this->getCurrentUser()) {
            if (
                $this->parameterBag->get('ACTIVE_HABILITATION_V2') &&
                $this->getCurrentUser() instanceof Agent
            ) {
                $slugger = new AsciiSlugger();
                $slug = $slugger->slug($habilitation)->snake()->lower()->toString();
                /** @var Agent $agent */
                $agent = $this->getCurrentUser();

                if ($this->habilitationTypeProcedureService->isHabilitationManaged($slug)) {
                    return $this->habilitationTypeProcedureService->checkHabilitation($agent, $slug);
                }
            }

            $userHabilitations = $this->getCurrentUser()->getHabilitation();

            $getMethod = $prefixMethod . $habilitation;
            $isMethod = 'is' . $habilitation;
            if ($userHabilitations instanceof HabilitationAgent) {
                if (method_exists($userHabilitations, $getMethod)) {
                    if ($userHabilitations->$getMethod()) {
                        return true;
                    }
                }

                if (method_exists($userHabilitations->getRecensementProgrammationStrategieAchat(), $isMethod)) {
                    if ($userHabilitations->getRecensementProgrammationStrategieAchat()->$isMethod()) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Verifie si l'utilisateur courant est un agent.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function isAgent(): bool
    {
        return $this->hasRole('ROLE_AGENT');
    }

    /**
     * Verifie si l'utilisateur courant a un role entreprise.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function isEntreprise(): bool
    {
        return $this->hasRole('ROLE_ENTREPRISE');
    }

    /**
     * Permet de verifier si l'utilisateur courant est agent ou inscrit d'entreprise (non authentifie en anonyme).
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function isUser(): bool
    {
        return $this->hasRole('ROLE_USER');
    }

    /**
     * Verifie si l'utilisateur courant est un inscrit entreprise.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function isInscrit(): bool
    {
        return $this->isEntreprise();
    }

    /**
     * Permet de recuperer le login de l'utilisateur connecte.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getUserName(): string
    {
        if ($this->getCurrentUser()) {
            return $this->getCurrentUser()->getUsername();
        }

        return '';
    }

    /**
     * Permet de recuperer l'email de l'utilisateur connecte.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getUserMail(): string
    {
        if ($this->getCurrentUser()) {
            return $this->getCurrentUser()->getEmail();
        }

        return '';
    }

    /**
     * Permet de recuperer l'id de l'inscrit connecte.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdInscrit(): string
    {
        if ($this->getCurrentUser() && $this->isInscrit()) {
            return $this->getCurrentUser()->getId();
        }

        return '';
    }

    /**
     * Permet de recuperer l'id de l'agent connecte.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdAgent(): string
    {
        if ($this->getCurrentUser() && $this->isAgent()) {
            return $this->getCurrentUser()->getId();
        }

        return '';
    }

    /**
     * Permet de recuperer l'id de l'entreprise correspondant a l'inscrit connecte.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdEntreprise(): string
    {
        if ($this->getCurrentUser() && $this->isInscrit()) {
            return $this->getCurrentUser()->getEntrepriseId();
        }

        return '';
    }

    /**
     * Permet de recuperer l'id de l'etablissement correspondant a l'inscrit connecte.
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdEtablissement(): string
    {
        if ($this->getCurrentUser() && $this->isInscrit()) {
            return $this->getCurrentUser()->getIdEtablissement();
        }

        return '';
    }

    /**
     * Permet de recuperer l'id du service de la personne connectee.
     *
     *
     * @author camille DUVERGER
     */
    public function getServiceId(): ?int
    {
        $res = 0;
        if ($this->getCurrentUser()) {
            $res = $this->getCurrentUser()->getServiceId();
        }

        return $res;
    }

    /**
     * Permet de recuperer l'acronyme de l'organisme de la personne connectee.
     *
     * @return string
     *
     * @author camille DUVERGER
     */
    public function getAcronymeOrga()
    {
        $res = '';
        if ($this->getCurrentUser() && $this->isAgent()) {
            $res = $this->getCurrentUser()->getAcronymeOrganisme();
        }

        return $res;
    }

    public function getOrganisme()
    {
        $org = null;

        if ($this->getCurrentUser() && $this->isAgent()) {
            $org = $this->getCurrentUser()->getOrganisme();
        }

        return $org;
    }
}
