<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Entreprise;

use Doctrine\Persistence\ObjectRepository;
use App\Entity\MessageAccueil;
use Doctrine\ORM\EntityManagerInterface;

class Accueil
{
    private readonly ObjectRepository $msgDb;

    private ?MessageAccueil $messageAccueil = null;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->msgDb = $entityManager->getRepository(MessageAccueil::class);
        $this->messageAccueil = $this->loadMessageAccueil();
    }

    public function getMsgAccueil(): string
    {
        return $this->messageAccueil ? $this->messageAccueil->getContenu() : '';
    }

    public function getMsgDesign(): string
    {
        if (! $this->messageAccueil instanceof MessageAccueil) {
            return '';
        }

        $cssClass = "bloc-message form-bloc-conf alert-";

        if ($this->messageAccueil->getTypeMessage() === 'avertissement') {
            $cssClass .= 'warning';
        } elseif ($this->messageAccueil->getTypeMessage() === 'erreur') {
            $cssClass .= 'danger';
        } else {
            $cssClass .= $this->messageAccueil->getTypeMessage();
        }
        return $cssClass;
    }

    public function loadMessageAccueil(): ?MessageAccueil
    {
        return $this->msgDb->findOneBy(['destinataire' => 'entreprise', 'config' => '']);
    }
}
