<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Entreprise\Footer;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class Renderer
{
    private ?Request $request = null;

    public function __construct(
        private readonly Environment $twig,
        RequestStack $requestStack
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(): string
    {
        $design = (is_null($this->request->cookies)) ? '' : $this->request->cookies->get('design', '');

        return ($this->twig->load('entreprise/menu/footer.html.twig'))->render(
            [
                'isAgent' => false,
                'design' => $design,
            ]
        );
    }
}
