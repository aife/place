<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Entreprise;

use App\Entity\ConfigurationPlateforme;
use App\Entity\LiensMenuEntreprisePersonnalise;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\EntityManagerInterface;

class Menu
{
    /* Ensemble des éléments du menu */

    private ?array $allItems = null;
    private readonly array $allConnexions;

    /* Éléments du menu activés */

    private ?array $itemsMenu = null;
    private $connexionsMenu;

    public function __construct(
        private readonly AtexoConfiguration $configuration,
        private readonly EntityManagerInterface $entityManager
    ) {
        $this->allConnexions = [
            'standard' => true,
        ];

        $this->connexionsMenu = $this->moduleFiltering($this->allConnexions, 'entreprise/menu/connexion/');
    }

    /* Initialisation des différents menus */

    private function moduleFiltering($allModules, $basePath): array
    {
        $arrayModules = array_keys(array_filter($allModules));
        array_walk(
            $arrayModules,
            function (&$module) use ($basePath) {
                $module = $basePath . $module . '.html.twig';
            }
        );
        return $arrayModules;
    }

    public function getMenu(): array
    {
        if (!$this->allItems || !$this->itemsMenu) {
            $this->allItems = [
                'panier' => $this->isPanier(),
                'annonces' => true,
                'dossiers_volumineux' => true,
                'bourse_cotraitance' => $this->isBourseCoTraitance(),
                'entites_achat' => true,
                'outils_signature' => true,
                'aide' => true,
                'preparer_repondre' => true,
                'societes_exclues' => $this->isSocietesExclues(),
                'liens_menu_personnalise' => (bool)$this->getLiensMenuEntreprisePersonalise(),
            ];
            $this->itemsMenu = $this->moduleFiltering($this->allItems, 'entreprise/menu/items/');
        }
        return $this->itemsMenu;
    }

    public function getConnectButtons(): array
    {
        return $this->connexionsMenu;
    }

    public function isSocleExterne(): bool
    {
        return '1' === $this->getConfigurationPlateforme()->getSocleExternePpp();
    }

    protected function getConfigurationPlateforme(): ConfigurationPlateforme
    {
        return $this->configuration->getConfigurationPlateforme();
    }

    /**********************************************/
    /*     Fonctions de personnalisation du menu  */
    /**********************************************/

    private function isPanier(): bool
    {
        return (bool) $this->getConfigurationPlateforme()->getPanierEntreprise();
    }

    private function isBourseCoTraitance(): bool
    {
        return (bool) $this->getConfigurationPlateforme()->getBourseALaSousTraitance();
    }

    public function isSocietesExclues(): bool
    {
        return (bool) $this->getConfigurationPlateforme()->getMenuAgentSocietesExclues();
    }

    public function getLiensMenuEntreprisePersonalise(): array
    {
        return
            $this->entityManager
                ->getRepository(LiensMenuEntreprisePersonnalise::class)
                ->findBy(
                    [],
                    ['displayOrder' => 'ASC']
                )
            ;
    }

    /**********************************************/
    /*             Informations du menu           */
    /**********************************************/

    public function getUrlInscription(): string
    {
        return "index.php?page=Entreprise.EntrepriseHome&goto=" . urlencode("entreprise") . "&inscription";
    }

    public function getUrlStandardConnexion(): string
    {
        return '';
    }
}
