<?php

namespace App\Service\Entreprise;

use App\Service\ApiGateway\ApiEntreprise;
use App\Service\Asynchronous\AsyncRequests;
use Exception;
use App\Entity\ConfigurationPlateforme;
use App\Entity\DocumentType;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EntrepriseAPIService
{
    final public const REQUEST_MANDATORY_PARAMS = [
        'context'   => 'MPS',
        'object'    => 'Synchronisation donnees Entreprise, Etablissement ou Exercices',
    ];

    /**
     * EntrepriseAPIService constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger,
        private readonly AsyncRequests $asyncRequests,
        private readonly ApiEntreprise $apiEntreprise,
    ) {
    }

    public function getEntreprise(string $siren, ?string $siret): array
    {
        $entreprise = $this->getEntrepriseIfExists($siren);
        if (!empty($entreprise) && !empty($siret)) {
            foreach ($entreprise['etablissements'] as $establishment) {
                if ($establishment['codeEtablissement'] == $siret) {
                    return $entreprise;
                }
            }
            $establishmentFromApi = $this->callEstablishment($siren . $siret);
            if (!empty($establishmentFromApi)) {
                $entreprise['etablissements'][] = $this->formatEtablishment($establishmentFromApi);
            }
        }

        if (empty($entreprise)) {
            $entrepriseFromApi = $this->callEntreprise($siren);
            $entreprise = $this->formatEntreprise($entrepriseFromApi);
        }

        return $entreprise;
    }

    public function callEntreprise(string $siren): array
    {
        $entrepriseEndpoints = [
            str_replace(
                '{siren}',
                $siren,
                $this->parameterBag->get('URL_API_ENTREPRISE_UNITE_LEGALE')
            ),
            str_replace(
                '{siren}',
                $siren,
                $this->parameterBag->get('URL_API_ENTREPRISE_MANDATAIRES_SOCIAUX')
            ),
            str_replace(
                '{siren}',
                $siren,
                $this->parameterBag->get('URL_API_ENTREPRISE_NUMERO_TVA')
            ),
            str_replace(
                '{siren}',
                $siren,
                $this->parameterBag->get('URL_API_ENTREPRISE_KBIS')
            ),
            str_replace(
                '{siren}',
                $siren,
                $this->parameterBag->get('URL_API_ENTREPRISE_SIEGE_SOCIAL')
            ),
        ];

        $multipleReponses = $this->asyncRequests->getMultiResponseByUrl(
            $entrepriseEndpoints,
            Request::METHOD_GET,
            $this->apiEntreprise->getRequestOptions()
        );

        return $this->apiEntreprise->mergeMultipleResponseData($multipleReponses);
    }

    public function callEstablishment(string $completeSiret): array
    {
        $url = $this->parameterBag->get('URL_API_ENTREPRISE_ETABLISSEMENT');
        $token = $this->parameterBag->get('API_ENTREPRISE_TOKEN');

        $response = $this->httpClient->request('GET', $url . $completeSiret, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => [
                'context'   => self::REQUEST_MANDATORY_PARAMS['context'],
                'object'    => self::REQUEST_MANDATORY_PARAMS['object'],
                'recipient' => $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT')
            ]
        ]);

        if ($response->getStatusCode() != Response::HTTP_OK) {
            return [];
        }

        $content = $response->getContent();

        return json_decode($content, true, 512, JSON_THROW_ON_ERROR);
    }

    public function getEntrepriseIfExists(string $siren): array
    {
        $entreprise = [];

        $rawEntreprise = $this->em->getRepository(Entreprise::class)->getEntrepriseBySiren($siren);
        if (!empty($rawEntreprise)) {
            $entreprise = [
                'nom'                   => $rawEntreprise->getNom(),
                'siren'                 => $rawEntreprise->getSiren(),
                'codeApe'               => $rawEntreprise->getCodeApe(),
                'acronymePays'          => $rawEntreprise->getAcronymePays(),
                'formeJuridique'        => $rawEntreprise->getFormeJuridique(),
                'categorieEntreprise'   => $rawEntreprise->getCategorieEntreprise()
            ];

            $params = ['idEntreprise' => $rawEntreprise->getId()];
            $rawEstablishments = $this->em->getRepository(Etablissement::class)->findBy($params);
            $establishments = [];
            foreach ($rawEstablishments as $rawEstablishment) {
                $establishments[] = [
                    'codeEtablissement'         => $rawEstablishment->getCodeEtablissement(),
                    'estSiege'                  => $rawEstablishment->getEstSiege() == 1,
                    'inscritAnnuaireDefense'    => $rawEstablishment->getInscritAnnuaireDefense() == 1,
                    'adresse'                   => $rawEstablishment->getAdresse(),
                    'codePostal'                => $rawEstablishment->getCodePostal(),
                    'ville'                     => $rawEstablishment->getVille()
                ];
            }
            $entreprise['etablissements'] = $establishments;
        }

        return $entreprise;
    }

    public function formatEntreprise(array $entrepriseApi): array
    {
        $entrepriseData = $entrepriseApi['data'];

        $activitePrincipale = $entrepriseData['activite_principale'] ?? null;

        $raisonSociale = ' ';
        if ($entrepriseData['type'] == ApiEntreprise::TYPE_PERSONNE_PHYSIQUE) {
            $personnePhysiqueAttributs = $entrepriseData['personne_physique_attributs'];
            $raisonSociale = $personnePhysiqueAttributs['prenom_1'] . ' ' . $personnePhysiqueAttributs['nom_naissance'];
        } elseif ($entrepriseData['type'] == ApiEntreprise::TYPE_PERSONNE_MORALE) {
            $raisonSociale = $entrepriseData['personne_morale_attributs']['raison_sociale'] ?? null;
        }

        $entreprise = [
            'nom'                   => $raisonSociale,
            'siren'                 => $entrepriseData['siren'],
            'codeApe'               => str_replace('.', '', $activitePrincipale['code'] ?? ''),
            'acronymePays'          => $entrepriseData['adresse']['code_pays_etranger'] ?? null,
            'formeJuridique'        => $entrepriseData['forme_juridique']['libelle'] ?? null,
            'categorieEntreprise'   => $entrepriseData['categorie_entreprise']
        ];

        $establishments = [[
            'codeEtablissement'         => substr($entrepriseData['unite_legale']['siret_siege_social'], 9),
            'estSiege'                  => true,
            'inscritAnnuaireDefense'    => false,
            'adresse'                   => $entrepriseData['adresse']['acheminement_postal']['l4'],
            'codePostal'                => $entrepriseData['adresse']['code_postal'],
            'ville'                     => $entrepriseData['adresse']['libelle_commune']
        ]];

        $entreprise['etablissements'] = $establishments;

        return $entreprise;
    }

    public function formatEtablishment(array $establishmentApi): array
    {
        $etablissement = $establishmentApi['data'];
        $adresse = sprintf(
            "%s %s %s",
            $etablissement['adresse']['numero_voie'],
            $etablissement['adresse']['type_voie'],
            $etablissement['adresse']['libelle_voie']
        );

        $establishment = [
            'codeEtablissement'         => substr($etablissement['siret'], 9),
            'estSiege'                  => $etablissement['siege_social'],
            'inscritAnnuaireDefense'    => false,
            'adresse'                   => $adresse,
            'codePostal'                => $etablissement['adresse']['code_postal'],
            'ville'                     => $etablissement['adresse']['libelle_commune']
        ];

        return $establishment;
    }

    public function getWebServiceQueryParams(): array
    {
        return array_merge(
            self::REQUEST_MANDATORY_PARAMS,
            ['recipient' => $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT')]
        );
    }

    public function getAttestationByDocumentType(DocumentType $documentType, string $siren): array
    {
        $url = $this->parameterBag->get('URL_API_GOUV_ENTREPRISE') . $documentType->getUri();
        $endpoint = str_replace('{siren}', $siren, $url);

        $this->logger->info(
            sprintf(
                "Début du call get attestation avec l'url: %s pour le type de document %s",
                $endpoint,
                $documentType->getCode()
            )
        );

        $response = null;
        try {
            $response = $this->httpClient->request('GET', $endpoint, [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' .  $this->parameterBag->get('API_ENTREPRISE_TOKEN'),
                ],
                'query' => [
                    'context'   => self::REQUEST_MANDATORY_PARAMS['context'],
                    'object'    => self::REQUEST_MANDATORY_PARAMS['object'],
                    'recipient' => $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT')
                ],

            ]);
        } catch (Exception $e) {
            $this->logger->info(
                sprintf(
                    "Erreur lors du call call get attestation avec l'url: %s pour le type de document %s,
                    message_erreur = %s",
                    $endpoint,
                    $documentType->getCode(),
                    $e->getMessage()
                )
            );
        }

        $this->logger->info(
            sprintf(
                "Fin de call get attestation avec l'url: %s pour le type de document %s",
                $endpoint,
                $documentType->getCode()
            )
        );

        if (null === $response) {
            return [];
        }

        if (
            $response->getStatusCode() != Response::HTTP_OK
            && $response->getStatusCode() != Response::HTTP_NOT_FOUND
        ) {
            $this->logger->info(
                sprintf(
                    "Erreur lors du call get attestation avec l'url: %s pour le type de document %s,
                    code erreur = %s",
                    $endpoint,
                    $documentType->getCode(),
                    $response->getStatusCode()
                )
            );
        }

        $content = $response->getContent(false);

        try {
            $contentDecoded = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->logger->info(
                sprintf(
                    "Erreur lors du decode, message_erreur = %s",
                    $e->getMessage()
                )
            );

            return [];
        }

        return $contentDecoded;
    }
}
