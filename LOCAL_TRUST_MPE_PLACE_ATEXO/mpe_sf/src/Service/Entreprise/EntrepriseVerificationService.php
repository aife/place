<?php

namespace App\Service\Entreprise;

use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use App\Service\AtexoUtil;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EntrepriseVerificationService
{
    final const ETAT_ADMINISTRATIF_FERME = 'F';
    final const ETAT_ADMINISTRATIF_ACTIF = 'A';

    /**
     * @var mixed
     */
    private $entreprise;

    /**
     * @var mixed
     */
    private $etablissement;

    /**
     * VerificationService constructor.
     */
    public function __construct(
        private readonly AtexoUtil $atexoUtil,
        private readonly SessionInterface $session,
        private readonly TranslatorInterface $translator,
        private readonly TokenStorageInterface $token,
        private readonly AtexoConfiguration $configuration
    ) {
        if (!empty($this->token->getToken())) {
            $this->currentUser = $this->token->getToken()->getUser();
        }
    }

    /**
     * @return array
     */
    public function getInitialParams()
    {
        $params = [];
        if ($this->currentUser instanceof Inscrit) {
            $this->entreprise = $this->currentUser->getEntreprise();
            $this->etablissement = $this->currentUser->getEtablissement();

            if ($this->etablissement instanceof Etablissement) {
                $adresse = $this->etablissement->getAdresse();
                if (!empty($this->etablissement->getAdresse2())) {
                    $adresse = $adresse.' - '.$this->etablissement->getAdresse2();
                }
                $params = [
                    'isSirenVisible' => false,
                    'typeAlert' => '',
                    'raisonSocial' => $this->entreprise->getNom(),
                    'adresse' => $adresse,
                    'codePostalVille' => $this->etablissement->getCodePostal().' - '.
                        $this->etablissement->getVille(),
                    'siren' => $this->entreprise->getSiren(),
                    'siret' => $this->entreprise->getSiren().' '.$this->etablissement->getCodeEtablissement(),
                    'msg' => '',
                ];
            }
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getParamsModal()
    {
        $params = $this->getInitialParams();
        if (!empty($params)) {
            $params['isSirenVisible'] = true;
        }

        $params['msg'] = '';
        if ('' != $this->getSiretAlert()) {
            $params['typeAlert'] = $this->getSiretAlert();
            $params['msg'] = $this->translator->trans('ENTREPRISE_VERIFICATION_SIRET_ERREUR');
            if ('warning' === $params['typeAlert']) {
                $params['msg'] = $this->translator->trans('ENTREPRISE_VERIFICATION_SIRET_AVERTISSEMENT');
            }
            if ('ETABLISSEMENT_FERME' === $params['typeAlert']) {
                $params['msg'] = $this->translator->trans('ETABLISSEMENT_FERME');
                $params['typeAlert'] = 'warning';
            }
            if ('ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO' === $params['typeAlert']) {
                $params['msg'] = $this->translator->trans('ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO');
                $params['typeAlert'] = 'warning';
            }
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getParamsDepot()
    {
        $params = $this->getInitialParams();
        if (!empty($params)) {
            //Régle de gestion 17: Pas SIREN
            if (empty(trim($this->entreprise->getSiren()))) {
                $params = array_merge(
                    $params,
                    [
                    'typeAlert' => 'info',
                    'msg' => 'ENTREPRISE_VERIFICATION_SIRET_VIDE_INFO',
                    'isSirenVisible' => false,
                    ]
                );
            } else {
                //Regle de gestion 18: AVEC SIREN SIRET NON CONFORME
                if (!$this->atexoUtil->isValidSiret(
                    $this->entreprise->getSiren().$this->etablissement->getCodeEtablissement()
                )
                ) {
                    $params = array_merge(
                        $params,
                        [
                        'msg' => 'ENTREPRISE_VERIFICATION_SIRET_ERREUR',
                        'typeAlert' => 'danger disable-siret',
                        'isSirenVisible' => true,
                        ]
                    );
                } else { //cas de SIRET conforme
                    //Régle de gestion 19 Entreprise NON Synchronisé
                    if ($this->entreprise->getSaisieManuelle()) {
                        $params = array_merge(
                            $params,
                            [
                            'typeAlert' => 'info',
                            'msg' => 'ENTREPRISE_NON_SYNCHRO',
                            'isSirenVisible' => true,
                            ]
                        );
                    } elseif ($this->etablissement->getSaisieManuelle()) {
                        //Régle de gestion 20 Entreprise Synchronisé, Siret Non Synchronisé
                        $params = array_merge(
                            $params,
                            [
                            'typeAlert' => 'warning',
                            'msg' => 'ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO',
                            'isSirenVisible' => true,
                            ]
                        );
                    } else {
                        //Régle de gestion 21 Etat Administratif fermé
                        if (self::ETAT_ADMINISTRATIF_FERME === $this->etablissement->getEtatAdministratif()) {
                            $params = array_merge(
                                $params,
                                [
                                    'typeAlert' => 'warning',
                                    'msg' => 'ETABLISSEMENT_FERME',
                                    'isSirenVisible' => true,
                                ]
                            );
                        } elseif (self::ETAT_ADMINISTRATIF_ACTIF === $this->etablissement->getEtatAdministratif()) {
                            //Régle de gestion 22: Etablissement non fermé
                            $params = array_merge(
                                $params,
                                [
                                'typeAlert' => 'info',
                                'msg' => 'ETABLISSEMENT_NON_FERME',
                                'isSirenVisible' => true,
                                ]
                            );
                        }
                    }
                }
            }
        } else {
            $params['isEtablissementExists'] = false;
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getSiretAlert()
    {
        $siretAlert = '';
        $user = $this->token->getToken();
        if (!empty($user)) {
            $this->currentUser = $user->getUser();
        }

        if (empty($this->entreprise)) {
            $this->getInitialParams();
        }

        if ($this->entreprise instanceof Entreprise) {
            if (!empty(trim($this->entreprise->getSiren()))) {
                if (false === $this->atexoUtil->isValidSiret($this->entreprise->getSiren().$this->etablissement->getCodeetablissement())) {
                    $siretAlert = 'danger';
                } elseif ($this->configuration->hasConfigPlateforme('synchronisationSgmap')) {
                    if (true === $this->etablissement->getSaisieManuelle() || empty($this->etablissement->getEtatAdministratif())) {
                        $siretAlert = 'ENTREPRISE_SYNCHRO_ETABLISSEMENT_NON_SYNCHRO';
                    } elseif ('F' === $this->etablissement->getEtatAdministratif()) {
                        $siretAlert = 'ETABLISSEMENT_FERME';
                    }
                }
            }
        }

        return $siretAlert;
    }
}
