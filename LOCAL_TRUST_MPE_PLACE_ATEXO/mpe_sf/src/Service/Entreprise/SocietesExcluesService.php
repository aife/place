<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Entreprise;

use Exception;
use App\Entity\SocietesExclues;
use App\Service\ExcelService;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class SocietesExcluesService
{
    final const FILENAME = 'societes-exclues';

    final const HEADER_FILE = [
        "A" => "Organisme",
        "B" => "Société exclue",
        "C" => "Motif d'exclusion",
        "D" => "Type d'exclusion",
        "E" => "Date de début d'exclusion",
        "F" => "Date de fin d'exclusion",
        "G" => "Portée"
    ];

    final const EXLUSION_TEMPORAIRE = 'Exclusion temporaire';
    final const EXLUSION_DEFINITIVE = 'Exclusion définitive';
    final const PORTEE_PARTIELLE = 'Portée partielle';
    final const PORTEE_TOTALE = 'Portée totale';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ExcelService $excelService,
        private readonly LoggerInterface $logger
    ){
    }

    public function getDataForFileSocietesExclues(): array
    {
        $data = ['headers' => self::HEADER_FILE];
        $societesExclues = $this->em->getRepository(SocietesExclues::class)->findAll();
        foreach ($societesExclues as $societeExclue) {
            $infoSociete = [
                $societeExclue->getOrganismeAcronyme(),
                $societeExclue->getRaisonSociale(),
                $societeExclue->getMotif(),
                $societeExclue->getTypeExclusion() ? self::EXLUSION_TEMPORAIRE : self::EXLUSION_DEFINITIVE,
                $societeExclue->getDateDebutExclusion(),
                $societeExclue->getDateFinExclusion(),
                $societeExclue->getTypeExclusion() ? self::PORTEE_PARTIELLE : self::PORTEE_TOTALE,
            ];
            $data['informations'][] = $infoSociete;
        }

        return $data;
    }

    public function getFileSocietesExclues(): ?array
    {
        try {
            $data = $this->getDataForFileSocietesExclues();

            return $this->excelService->generateBasicExcelFile(self::FILENAME, $data);
        } catch (Exception $e) {
            $this->logger->error(
                "Erreur = " . $e->getMessage() . PHP_EOL . "Trace = " . $e->getTraceAsString()
            );
            return null;
        }
    }
}
