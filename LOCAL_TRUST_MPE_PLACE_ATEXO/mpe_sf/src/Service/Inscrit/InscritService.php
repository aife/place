<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Inscrit;

use App\Entity\Inscrit;

class InscritService
{
    public function getRgpdLabels(Inscrit $inscrit): array
    {
        $rgpdLabels = [];

        $rgpdLabels[] = $inscrit->isRgpdCommunicationPlace() ? Inscrit::COMMUNICATION_PLACE : null;
        $rgpdLabels[] = $inscrit->getRgpdCommunication() ? Inscrit::COMMUNICATION_SIA : null;
        $rgpdLabels[] = $inscrit->isRgpdEnquete() ? Inscrit::COMMUNICATION_ENQUETE : null;

        return array_values(array_filter($rgpdLabels));
    }
}
