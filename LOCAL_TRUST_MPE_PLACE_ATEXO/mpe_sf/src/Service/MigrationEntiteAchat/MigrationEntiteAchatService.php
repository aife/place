<?php

namespace App\Service\MigrationEntiteAchat;

use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationEntiteAchatService
{
    private ?string $cheminFichierServices = null;

    private ?string $cheminFichierServicesCorrespondances = null;

    private ?string $acronymeOrganismeSource = null;

    private ?string $acronymeOrganismeDestination = null;

    private ?bool $modeTest = null;

    /**
     * MigrationEntiteAchatService constructor.
     *
     * @param EntityManager|null $em
     *
     * @throws Exception
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface $logger,
    ) {

    }

    /**
     * @param string $cheminFichierServices                : chemin vers le fichier qui contient les identifiants des entités d'achats à migrer
     * @param string $cheminFichierServicesCorrespondances : chemin vers le fichier qui contient les correspondances entre les identifiants sources et cibles
     * @param string $acronymeOrganismeSource              : acronyme de l'organisme source qui contient les entités d'achats à migrer
     * @param string $acronymeOrganismeDestination         : acronyme de l'organisme cible vers lequel déplacer les entités d'achats migrées
     * @param bool   $modeTest                             : par défaut, vrai (l'exécution ne fait rien en BD mais loggue uniquement),
     *
     * @return bool : false si le fichier $cheminFichierServices n'existe ou vide ou que $acronymeOrganismeSource/$acronymeOrganismeDestination n'existe pa ou qu'un service au moins n'a pas été migré correctement
     */
    public function migrate(
        string $cheminFichierServices,
        string $cheminFichierServicesCorrespondances,
        string $acronymeOrganismeSource,
        string $acronymeOrganismeDestination,
        bool $modeTest = true
    ) {
        $this->logger->info('Début de la migration des entités d\'achat');
        $info = "\nChemin du fichier source : ".$cheminFichierServices."\n";
        $info = "\nChemin du fichier correspondances : ".$cheminFichierServicesCorrespondances."\n";
        $info .= 'Organisme Source: '.$acronymeOrganismeSource."\n";
        $info .= 'Organisme Destination: '.$acronymeOrganismeDestination."\n";
        $info .= 'Lancement du script en mode : '.(true == $modeTest ? 'test' : 'reel')."\n";

        $this->cheminFichierServices = $cheminFichierServices;
        $this->cheminFichierServicesCorrespondances = $cheminFichierServicesCorrespondances;

        $this->acronymeOrganismeSource = $acronymeOrganismeSource;
        $this->acronymeOrganismeDestination = $acronymeOrganismeDestination;
        $this->modeTest = $modeTest;
        $res = true;

        $this->logger->info($info);

        if (!$this->validateParameters()) {
            $res = false;
        } else {
            $arrayServicesId = $this->transformFichierTableau();
            foreach ($arrayServicesId as $service) {
                $newService = $this->migrerService($service, $this->acronymeOrganismeSource, $this->acronymeOrganismeDestination, $this->modeTest);
                if (!($newService instanceof Service)) {
                    $res = false;
                }
            }
        }

        return $res;
    }

    private function validateParameters()
    {
        if (!file_exists($this->cheminFichierServices)) {
            $this->logger->error("Le fichier passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeSource)) {
            $this->logger->error("L'acronyme de l'organisme source passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeDestination)) {
            $this->logger->error("L'acronyme de l'organisme destination passé en paramètre n'existe pas");

            return false;
        }

        return true;
    }

    private function organismExist(string $acronymeOrganisme)
    {
        $criterias = ['acronyme' => $acronymeOrganisme];
        $organisme = $this->em->getRepository(Organisme::class)->findBy($criterias);
        if (count($organisme) > 0 && $organisme[0] instanceof Organisme) {
            return true;
        }

        return false;
    }

    private function transformFichierTableau()
    {
        $arrayServicesId = [];
        $row = 0;

        if (($handle = fopen($this->cheminFichierServices, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, "\t")) !== false) {
                $d = intval(trim($data[0]));
                var_export($d."\n");
                if ($d > 0) {
                    $arrayServicesId[] = $d;
                    ++$row;
                }
            }
        }
        $this->logger->info("Le nombre d'entités d'achat à migrer est : ".$row);

        return $arrayServicesId;
    }

    /**
     * Enregistre dans le fichier nommé $cheminFichierServices."_correspondance_services" la correspondance entre l'identifiant de l'ancien service et du nouveau service.
     *
     * @param Service $oldService Ancien service
     * @param Service $newService Nouveau service
     */
    private function saveToFile(Service $oldService, Service $newService)
    {
        $this->logger->info('Ajout dans le fichier de correspondance ancien/nouveau service: '.$this->cheminFichierServicesCorrespondances.' : '.$oldService->getId().';'.$newService->getId());
        $fp = fopen($this->cheminFichierServicesCorrespondances, 'a+');
        fwrite($fp, $oldService->getId().';'.$newService->getId()."\n");
        fclose($fp);
    }

    /**
     * @param $idService identifiant technique du service à migrer
     * @param $acronymeSource acronyme de l'organisme source
     * @param $acronymeDestination acronyme de l'organisme de destination
     * @param $modeTest indique s'il s'agit d'un dryRun
     *
     * @return bool|Service false si migration en BD KO, objet de type Service si OK
     */
    public function migrerService($idService, $acronymeSource, $acronymeDestination, $modeTest): bool|Service
    {
        $this->logger->info('Démarrage du traitement du service : '.$idService);

        $serviceAmigrer = $this->getService($idService, $acronymeSource);
        if ($serviceAmigrer) {
            try {
                $this->logger->info('Le service a bien été trouvé en BD : '.$serviceAmigrer->getId().' '.$serviceAmigrer->getSigle().' '.$serviceAmigrer->getAcronymeOrg());
                if (!$modeTest) {
                    $nouveauService = $serviceAmigrer->copier($acronymeDestination);
                    if ($nouveauService instanceof Service) {
                        $this->saveToFile($serviceAmigrer, $nouveauService);
                        $this->logger->info('Le nouveau service a bien été créé en BD : '.$nouveauService->getId().' '.$nouveauService->getSigle().' '.$nouveauService->getAcronymeOrg());
                        $this->logger->info('Fin du traitement du service : '.$serviceAmigrer->getId());

                        return $nouveauService;
                    } else {
                        $this->logger->error('Un problème est survenu pendant la copie du service '.$idService.". Le processus pour ce service va s'arrêter");
                        $this->logger->error('Fin du traitement du service : '.$idService);

                        return false;
                    }
                } else {
                    $this->logger->info('La commande est lancée en mode test, les opérations ne sont pas réalisées effectivement mais juste simulées');

                    return false;
                }
            } catch (Exception $ex) {
                $this->logger->critical('Un exception a été levée pendant la migration du service '.$idService.". Le message d'erreur est le suivant : ".$ex->getMessage());
                $this->logger->critical('Fin du traitement du service : '.$idService);

                return false;
            }
        } else {
            $this->logger->error('Le service '.$idService." n'est pas retrouvé en BD pour l'organisme source ".$acronymeSource);
            $this->logger->error('Fin du traitement du service : '.$idService);

            return false;
        }
    }

    /**
     * @param $idService
     * @param $acronyme
     */
    private function getService($idService, $acronyme): bool|Service
    {
        $criterias = ['organisme' => $acronyme, 'id' => $idService];

        $service = $this->em->getRepository(Service::class)->findBy($criterias);
        if (count($service) > 0 && $service[0] instanceof Service) {
            return $service[0];
        }

        return false;
    }
}
