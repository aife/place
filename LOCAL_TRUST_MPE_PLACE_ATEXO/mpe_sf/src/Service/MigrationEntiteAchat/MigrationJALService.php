<?php

namespace App\Service\MigrationEntiteAchat;

use App\Entity\Jal;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationJALService
{
    private ?string $cheminFichierJALCorrespondances = null;

    private ?string $cheminFichierServices = null;

    private ?string $acronymeOrganismeSource = null;

    private ?string $acronymeOrganismeDestination = null;

    private ?bool $modeTest = null;

    /**
     * MigrationJALService constructor.
     *
     * @param EntityManager|null $em
     *
     * @throws Exception
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface $logger,
    ) {

    }

    /**
     * @param string $cheminFichierServices        : chemin vers le fichier qui contient les identifiants des entités d'achats à migrer avec la correspondance avec les nouvelles entités créés
     * @param string $acronymeOrganismeSource      : acronyme de l'organisme source qui contient les JALs à migrer
     * @param string $acronymeOrganismeDestination : acronyme de l'organisme cible vers lequel copier les JALs migrés
     * @param bool   $modeTest                     : par défaut, vrai (l'exécution ne fait rien en BD mais loggue uniquement),
     *
     * @return bool|array : false si le fichier $cheminFichierServices n'existe ou vide ou que $acronymeOrganismeSource/$acronymeOrganismeDestination n'existe pas
     *                    true si les opérations de migrations se sont bien déroulées et sont complètes,  false sinon
     */
    public function migrate(
        string $cheminFichierServices,
        string $acronymeOrganismeSource,
        string $acronymeOrganismeDestination,
        bool $modeTest = true
    ): bool|array {
        $this->logger->info('Début de la migration des JALs');
        $info = "\nChemin du fichier: ".$cheminFichierServices."\n";
        $info .= 'Organisme Source: '.$acronymeOrganismeSource."\n";
        $info .= 'Organisme Destination: '.$acronymeOrganismeDestination."\n";
        $info .= 'Lancement du script en mode : '.(true == $modeTest ? 'test' : 'reel')."\n";

        $this->cheminFichierServices = $cheminFichierServices;
        $this->cheminFichierJALCorrespondances = $cheminFichierServices.'_correspondance_jals';
        $this->acronymeOrganismeSource = $acronymeOrganismeSource;
        $this->acronymeOrganismeDestination = $acronymeOrganismeDestination;
        $this->modeTest = $modeTest;
        $res = true;

        $this->logger->info($info);

        if (!$this->validateParameters()) {
            $res = false;
        } else {
            $arrayServicesId = $this->transformFichierTableau();
            foreach ($arrayServicesId as $idServiceSource => $idServiceDestination) {
                if (!($this->migrerJAL($idServiceSource, $idServiceDestination, $this->acronymeOrganismeSource, $this->acronymeOrganismeDestination, $this->modeTest))) {
                    $res = false;
                }
            }
        }

        return $res;
    }

    private function validateParameters()
    {
        if (!file_exists($this->cheminFichierServices)) {
            $this->logger->error("Le fichier passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeSource)) {
            $this->logger->error("L'acronyme de l'organisme source passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeDestination)) {
            $this->logger->error("L'acronyme de l'organisme destination passé en paramètre n'existe pas");

            return false;
        }

        return true;
    }

    private function organismExist(string $acronymeOrganisme)
    {
        $criterias = ['acronyme' => $acronymeOrganisme];
        $organisme = $this->em->getRepository(Organisme::class)->findBy($criterias);
        if (count($organisme) > 0 && $organisme[0] instanceof Organisme) {
            return true;
        }

        return false;
    }

    private function transformFichierTableau()
    {
        $arrayServicesId = [];
        $row = 0;

        if (($handle = fopen($this->cheminFichierServices, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                $s = intval(trim($data[0]));
                $d = intval(trim($data[1]));
                if ($s > 0 && $d > 0) {
                    $arrayServicesId[$s] = $d;
                    ++$row;
                }
            }
        }
        $this->logger->info("Le nombre d'entités d'achat à migrer est : ".$row);

        return $arrayServicesId;
    }

    /**
     * @param $idServiceSource identifiant technique du service source
     * @param $idServiceDestination identifiant technique du service destination
     * @param $acronymeSource acronyme de l'organisme source
     * @param $acronymeDestination acronyme de l'organisme de destination
     * @param $modeTest indique s'il s'agit d'un dryRun
     *
     * @return bool false si migration en BD KO, true si OK
     */
    public function migrerJAL($idServiceSource, $idServiceDestination, $acronymeSource, $acronymeDestination, $modeTest)
    {
        $this->logger->info('Démarrage du traitement des services : '.$idServiceSource.'-'.$acronymeSource.'/'.$idServiceDestination.'-'.$acronymeDestination);

        $serviceAmigrer = $this->getService($idServiceSource, $acronymeSource);
        $serviceMigre = $this->getService($idServiceDestination, $acronymeDestination);

        $res = true;

        if ($serviceAmigrer && $serviceMigre) {
            try {
                $this->logger->info('Les services ont bien été trouvés en BD : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                if (!$modeTest) {
                    $allJALs = ($this->em->getRepository(Jal::class))->getAllJALs($idServiceSource, $acronymeSource);
                    if ((is_countable($allJALs) ? count($allJALs) : 0) > 0) {
                        foreach ($allJALs as $jal) {
                            $this->logger->info('JAL trouvé pour copie : '.$jal->getId().' '.$jal->getServiceId().' '.$jal->getOrganisme());
                            $nouveauJAL = $this->copier($jal, $idServiceDestination, $acronymeDestination);
                            if ($nouveauJAL instanceof Jal) {
                                $this->logger->info('Le nouveau JAL a bien été créé en BD : '.$nouveauJAL->getId().' '.' '.$nouveauJAL->getServiceId().' '.$nouveauJAL->getOrganisme());
                                $this->logger->info('Fin du traitement du JAL : '.$jal->getId());
                            } else {
                                $this->logger->error('Un problème est survenu pendant la copie du JAL '.$jal->getId().' '.$jal->getOrganisme().'Fin du traitement du JAL. Passage au JAL suivant');
                                $res = false;
                            }
                        }
                    } else {
                        $this->logger->info('Aucun JALs à migrer pour le : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                    }
                    $this->saveToFile($serviceAmigrer, $serviceMigre);

                    return $res;
                } else {
                    $this->logger->info('La commande est lancée en mode test, les opérations ne sont pas réalisées effectivement mais juste simulées');

                    return false;
                }
            } catch (Exception $ex) {
                $this->logger->critical('Un exception a été levée pendant la migration des JALs pour les services '.$idServiceSource.'/'.$idServiceDestination.". Le message d'erreur est le suivant : ".$ex->getMessage());
                $this->logger->critical('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

                return false;
            }
        } else {
            $this->logger->error('Les services '.$idServiceSource.'/'.$idServiceDestination." n'ont pas été retrouvé en BD");
            $this->logger->error('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

            return false;
        }
    }

    /**
     * @param $idService
     * @param $acronyme
     */
    private function getService($idService, $acronyme): bool|Service
    {
        $criterias = ['acronymeOrg' => $acronyme, 'id' => $idService];

        $service = $this->em->getRepository(Service::class)->findBy($criterias);
        if (count($service) > 0 && $service[0] instanceof Service) {
            return $service[0];
        }

        return false;
    }

    /**
     * Enregistre dans le fichier nommé $cheminFichierServices."_correspondance_jals" la correspondance entre l'identifiant de l'ancien service et du nouveau service
     * le nombre de JALs de l'ancien service, le nombre de JALs du nouveau service.
     *
     * @param Service $oldService Ancien service
     * @param Service $newService Nouveau service
     */
    private function saveToFile(Service $oldService, Service $newService)
    {
        $this->logger->info('Ajout dans le fichier de correspondance ancien/nouveau service: '.$this->cheminFichierJALCorrespondances.' : '.$oldService->getId().';'.$newService->getId());
        $allJALSources = ($this->em->getRepository(Jal::class))->getAllJALs($oldService->getId(), $oldService->getAcronymeOrg());
        $allJALDestinations = ($this->em->getRepository(Jal::class))->getAllJALs($newService->getId(), $newService->getAcronymeOrg());

        $fp = fopen($this->cheminFichierJALCorrespondances, 'a+');
        fwrite($fp, $oldService->getId().';'.$newService->getId().';'.(is_countable($allJALSources) ? count($allJALSources) : 0).';'.(is_countable($allJALDestinations) ? count($allJALDestinations) : 0)."\n");
        fclose($fp);
    }

    /**
     * Cette fonction permet de copier le JAL lui attribuant un nouveau service et un nouvel organisme de destination passés en paramètres.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|JAL false si le clone n'a pas réussi, sinon le nouvel objet JAL créé
     *
     * @throws \Exception
     */
    public function copier(Jal $jal, int $idServiceDestination, string $organismeDestination): bool|\App\Entity\JAL
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)
            ->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $nouveauJAL = clone $jal;
            $nouveauJAL->setServiceId($idServiceDestination);
            $nouveauJAL->setOrganisme($organismeDestination);
            $this->em->persist($nouveauJAL);
            $this->em->flush();

            return $nouveauJAL;
        }

        return false;
    }
}
