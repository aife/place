<?php

namespace App\Service\MigrationEntiteAchat;

use App\Entity\Agent\TireurPlan;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationTireurPlanService
{
    private ?string $cheminFichierTireurPlanCorrespondances = null;

    private ?string $cheminFichierServices = null;

    private ?string $acronymeOrganismeSource = null;

    private ?string $acronymeOrganismeDestination = null;

    private ?bool $modeTest = null;

    /**
     * MigrationTireurPlanService constructor.
     *
     * @param EntityManager|null $em
     *
     * @throws Exception
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface $logger,
    ) {

    }

    /**
     * @param string $cheminFichierServices        : chemin vers le fichier qui contient les identifiants des entités d'achats à migrer avec la correspondance avec les nouvelles entités créés
     * @param string $acronymeOrganismeSource      : acronyme de l'organisme source qui contient les TireurPlans à migrer
     * @param string $acronymeOrganismeDestination : acronyme de l'organisme cible vers lequel copier les TireurPlans migrés
     * @param bool   $modeTest                     : par défaut, vrai (l'exécution ne fait rien en BD mais loggue uniquement),
     *
     * @return bool|array : false si le fichier $cheminFichierServices n'existe ou vide ou que $acronymeOrganismeSource/$acronymeOrganismeDestination n'existe pas
     *                    true si les opérations de migrations se sont bien déroulées et sont complètes,  false sinon
     */
    public function migrate(
        string $cheminFichierServices,
        string $acronymeOrganismeSource,
        string $acronymeOrganismeDestination,
        bool $modeTest = true
    ): bool|array {
        $this->logger->info('Début de la migration des TireurPlans');
        $info = "\nChemin du fichier: ".$cheminFichierServices."\n";
        $info .= 'Organisme Source: '.$acronymeOrganismeSource."\n";
        $info .= 'Organisme Destination: '.$acronymeOrganismeDestination."\n";
        $info .= 'Lancement du script en mode : '.(true == $modeTest ? 'test' : 'reel')."\n";

        $this->cheminFichierServices = $cheminFichierServices;
        $this->cheminFichierTireurPlanCorrespondances = $cheminFichierServices.'_correspondance_TireurPlans';
        $this->acronymeOrganismeSource = $acronymeOrganismeSource;
        $this->acronymeOrganismeDestination = $acronymeOrganismeDestination;
        $this->modeTest = $modeTest;
        $res = true;

        $this->logger->info($info);

        if (!$this->validateParameters()) {
            $res = false;
        } else {
            $arrayServicesId = $this->transformFichierTableau();
            foreach ($arrayServicesId as $idServiceSource => $idServiceDestination) {
                if (!($this->migrerTireurPlan($idServiceSource, $idServiceDestination, $this->acronymeOrganismeSource, $this->acronymeOrganismeDestination, $this->modeTest))) {
                    $res = false;
                }
            }
        }

        return $res;
    }

    private function validateParameters()
    {
        if (!file_exists($this->cheminFichierServices)) {
            $this->logger->error("Le fichier passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeSource)) {
            $this->logger->error("L'acronyme de l'organisme source passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeDestination)) {
            $this->logger->error("L'acronyme de l'organisme destination passé en paramètre n'existe pas");

            return false;
        }

        return true;
    }

    private function organismExist(string $acronymeOrganisme)
    {
        $criterias = ['acronyme' => $acronymeOrganisme];
        $organisme = $this->em->getRepository(Organisme::class)->findBy($criterias);
        if (count($organisme) > 0 && $organisme[0] instanceof Organisme) {
            return true;
        }

        return false;
    }

    private function transformFichierTableau()
    {
        $arrayServicesId = [];
        $row = 0;

        if (($handle = fopen($this->cheminFichierServices, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                $s = intval(trim($data[0]));
                $d = intval(trim($data[1]));
                if ($s > 0 && $d > 0) {
                    $arrayServicesId[$s] = $d;
                    ++$row;
                }
            }
        }
        $this->logger->info("Le nombre d'entités d'achat à migrer est : ".$row);

        return $arrayServicesId;
    }

    /**
     * @param $idServiceSource identifiant technique du service source
     * @param $idServiceDestination identifiant technique du service destination
     * @param $acronymeSource acronyme de l'organisme source
     * @param $acronymeDestination acronyme de l'organisme de destination
     * @param $modeTest indique s'il s'agit d'un dryRun
     *
     * @return bool false si migration en BD KO, true si OK
     */
    public function migrerTireurPlan($idServiceSource, $idServiceDestination, $acronymeSource, $acronymeDestination, $modeTest)
    {
        $this->logger->info('Démarrage du traitement des services : '.$idServiceSource.'-'.$acronymeSource.'/'.$idServiceDestination.'-'.$acronymeDestination);

        $serviceAmigrer = $this->getService($idServiceSource, $acronymeSource);
        $serviceMigre = $this->getService($idServiceDestination, $acronymeDestination);

        $res = true;

        if ($serviceAmigrer && $serviceMigre) {
            try {
                $this->logger->info('Les services ont bien été trouvés en BD : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                if (!$modeTest) {
                    $allTireurPlans = ($this->em->getRepository(TireurPlan::class))->getAllTireurPlans($idServiceSource, $acronymeSource);
                    if ((is_countable($allTireurPlans) ? count($allTireurPlans) : 0) > 0) {
                        foreach ($allTireurPlans as $tireurPlan) {
                            $this->logger->info('TireurPlan trouvé pour copie : '.$tireurPlan->getId().' '.$tireurPlan->getServiceId().' '.$tireurPlan->getOrganisme());
                            $nouveauTireurPlan = $tireurPlan->copier($idServiceDestination, $acronymeDestination);
                            if ($nouveauTireurPlan instanceof TireurPlan) {
                                $this->logger->info('Le nouveau TireurPlan a bien été créé en BD : '.$nouveauTireurPlan->getId().' '.' '.$nouveauTireurPlan->getServiceId().' '.$nouveauTireurPlan->getOrganisme());
                                $this->logger->info('Fin du traitement du nouveau TireurPlan : '.$nouveauTireurPlan->getId());
                            } else {
                                $this->logger->error('Un problème est survenu pendant la copie du TireurPlan '.$nouveauTireurPlan->getId().' '.$nouveauTireurPlan->getOrganisme().'Fin du traitement du TireurPlan. Passage au TireurPlan suivant');
                                $res = false;
                            }
                        }
                    } else {
                        $this->logger->info('Aucun TireurPlans à migrer pour le : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                    }
                    $this->saveToFile($serviceAmigrer, $serviceMigre);

                    return $res;
                } else {
                    $this->logger->info('La commande est lancée en mode test, les opérations ne sont pas réalisées effectivement mais juste simulées');

                    return false;
                }
            } catch (Exception $ex) {
                $this->logger->critical('Un exception a été levée pendant la migration des TireurPlans pour les services '.$idServiceSource.'/'.$idServiceDestination.". Le message d'erreur est le suivant : ".$ex->getMessage());
                $this->logger->critical('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

                return false;
            }
        } else {
            $this->logger->error('Les services '.$idServiceSource.'/'.$idServiceDestination." n'ont pas été retrouvé en BD");
            $this->logger->error('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

            return false;
        }
    }

    /**
     * @param $idService
     * @param $acronyme
     */
    private function getService($idService, $acronyme): bool|Service
    {
        $criterias = ['acronymeOrg' => $acronyme, 'id' => $idService];

        $service = $this->em->getRepository(Service::class)->findBy($criterias);
        if (count($service) > 0 && $service[0] instanceof Service) {
            return $service[0];
        }

        return false;
    }

    /**
     * Enregistre dans le fichier nommé $cheminFichierServices."_correspondance_TireurPlans" la correspondance entre l'identifiant de l'ancien service et du nouveau service
     * le nombre de TireurPlans de l'ancien service, le nombre de TireurPlans du nouveau service.
     *
     * @param Service $oldService Ancien service
     * @param Service $newService Nouveau service
     */
    private function saveToFile(Service $oldService, Service $newService)
    {
        $this->logger->info('Ajout dans le fichier de correspondance ancien/nouveau service: '.$this->cheminFichierTireurPlanCorrespondances.' : '.$oldService->getId().';'.$newService->getId());
        $allTireurPlanSources = ($this->em->getRepository(TireurPlan::class))->getAllTireurPlans($oldService->getId(), $oldService->getAcronymeOrg());
        $allTireurPlanDestinations = ($this->em->getRepository(TireurPlan::class))->getAllTireurPlans($newService->getId(), $newService->getAcronymeOrg());

        $fp = fopen($this->cheminFichierTireurPlanCorrespondances, 'a+');
        fwrite($fp, $oldService->getId().';'.$newService->getId().';'.(is_countable($allTireurPlanSources) ? count($allTireurPlanSources) : 0).';'.(is_countable($allTireurPlanDestinations) ? count($allTireurPlanDestinations) : 0)."\n");
        fclose($fp);
    }
}
