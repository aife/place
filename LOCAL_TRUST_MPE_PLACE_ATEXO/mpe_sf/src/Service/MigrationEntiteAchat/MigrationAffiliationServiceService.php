<?php

namespace App\Service\MigrationEntiteAchat;

use App\Entity\AffiliationService;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MigrationAffiliationServiceService
{
    private ?string $cheminFichierAffiliationServiceCorrespondances = null;

    private ?string $cheminFichierServices = null;

    private ?string $acronymeOrganismeSource = null;

    private ?string $acronymeOrganismeDestination = null;

    private ?bool $modeTest = null;

    /**
     * @var array
     */
    private $arrayServicesId;

    /**
     * MigrationRPAService constructor.
     *
     * @param EntityManager|null $em
     *
     * @throws Exception
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected LoggerInterface $logger
    ) {

    }

    /**
     * @param string $cheminFichierServices        : chemin vers le fichier qui contient les identifiants des entités d'achats à migrer avec la correspondance avec les nouvelles entités créés
     * @param string $acronymeOrganismeSource      : acronyme de l'organisme source qui contient les RPAs à migrer
     * @param string $acronymeOrganismeDestination : acronyme de l'organisme cible vers lequel copier les RPAs migrées
     * @param bool   $modeTest                     : par défaut, vrai (l'exécution ne fait rien en BD mais loggue uniquement),
     *
     * @return bool|array : false si le fichier $cheminFichierServices n'existe ou vide ou que $acronymeOrganismeSource/$acronymeOrganismeDestination n'existe pas
     *                    true si les opérations de migrations se sont bien déroulées et sont complètes,  false sinon
     */
    public function migrate(
        string $cheminFichierServices,
        string $acronymeOrganismeSource,
        string $acronymeOrganismeDestination,
        bool $modeTest = true
    ): bool|array {
        $this->logger->info('Début de la migration des AffiliationServices');
        $info = "\nChemin du fichier: ".$cheminFichierServices."\n";
        $info .= 'Organisme Source: '.$acronymeOrganismeSource."\n";
        $info .= 'Organisme Destination: '.$acronymeOrganismeDestination."\n";
        $info .= 'Lancement du script en mode : '.(true == $modeTest ? 'test' : 'reel')."\n";

        $this->cheminFichierServices = $cheminFichierServices;
        $this->cheminFichierAffiliationServiceCorrespondances = $cheminFichierServices.'_correspondance_affiliation_services';
        $this->acronymeOrganismeSource = $acronymeOrganismeSource;
        $this->acronymeOrganismeDestination = $acronymeOrganismeDestination;
        $this->modeTest = $modeTest;
        $res = true;

        $this->logger->info($info);

        if (!$this->validateParameters()) {
            $res = false;
        } else {
            $this->arrayServicesId = $this->transformFichierTableau();
            foreach ($this->arrayServicesId as $idServiceSource => $idServiceDestination) {
                if (!($this->migrerAffiliationService($idServiceSource, $idServiceDestination, $this->acronymeOrganismeSource, $this->acronymeOrganismeDestination, $this->modeTest))) {
                    $res = false;
                }
            }
        }

        return $res;
    }

    private function validateParameters()
    {
        if (!file_exists($this->cheminFichierServices)) {
            $this->logger->error("Le fichier passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeSource)) {
            $this->logger->error("L'acronyme de l'organisme source passé en paramètre n'existe pas");

            return false;
        }
        if (!$this->organismExist($this->acronymeOrganismeDestination)) {
            $this->logger->error("L'acronyme de l'organisme destination passé en paramètre n'existe pas");

            return false;
        }

        return true;
    }

    private function organismExist(string $acronymeOrganisme)
    {
        $criterias = ['acronyme' => $acronymeOrganisme];
        $organisme = $this->em->getRepository(Organisme::class)->findBy($criterias);
        if (count($organisme) > 0 && $organisme[0] instanceof Organisme) {
            return true;
        }

        return false;
    }

    private function transformFichierTableau()
    {
        $arrayServicesId = [];
        $row = 0;

        if (($handle = fopen($this->cheminFichierServices, 'r')) !== false) {
            while (($data = fgetcsv($handle, 1000, ';')) !== false) {
                $s = intval(trim($data[0]));
                $d = intval(trim($data[1]));
                if ($s > 0 && $d > 0) {
                    $arrayServicesId[$s] = $d;
                    ++$row;
                }
            }
        }
        $this->logger->info("Le nombre d'entités d'achat à migrer est : ".$row);

        return $arrayServicesId;
    }

    /**
     * @param $idServiceSource identifiant technique du service source
     * @param $idServiceDestination identifiant technique du service destination
     * @param $acronymeSource acronyme de l'organisme source
     * @param $acronymeDestination acronyme de l'organisme de destination
     * @param $modeTest indique s'il s'agit d'un dryRun
     *
     * @return bool false si migration en BD KO, true si OK
     */
    public function migrerAffiliationService($idServiceSource, $idServiceDestination, $acronymeSource, $acronymeDestination, $modeTest)
    {
        $this->logger->info('Démarrage du traitement des services : '.$idServiceSource.'-'.$acronymeSource.'/'.$idServiceDestination.'-'.$acronymeDestination);

        $serviceAmigrer = $this->getService($idServiceSource, $acronymeSource);
        $serviceMigre = $this->getService($idServiceDestination, $acronymeDestination);

        $res = true;

        if ($serviceAmigrer && $serviceMigre) {
            try {
                $this->logger->info('Les services ont bien été trouvés en BD : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                if (!$modeTest) {
                    $serviceParentSource = ($this->em->getRepository(Service::class))->getServiceParent($idServiceSource, $acronymeSource);
                    if ($serviceParentSource instanceof Service) {
                        $this->logger->info("Démarrage de la copie de l'affiliation service  : ".$serviceParentSource->getId().'/'.$idServiceSource);
                        $serviceParentDestination = $this->getService($this->arrayServicesId[$serviceParentSource->getId()], $acronymeDestination);
                        $newAffiliationService = new AffiliationService();
                        $newAffiliationService->setOrganisme($this->getOrganisme($acronymeDestination));
                        $newAffiliationService->setServiceParentId($serviceParentDestination);
                        $newAffiliationService->setServiceId($serviceMigre);
                        $this->em->persist($newAffiliationService);
                        $this->em->flush();
                        $this->logger->info('Nouvel affiliation service créé pour : '.$serviceParentDestination->getId().'/'.$serviceMigre->getId());
                        $this->saveToFile($serviceAmigrer, $serviceParentSource, $newAffiliationService);
                    } else {
                        $this->logger->info('Aucun ancetre de rattachement à migrer pour le : '.$serviceAmigrer->getId().'/'.$serviceMigre->getId());
                    }

                    return $res;
                } else {
                    $this->logger->info('La commande est lancée en mode test, les opérations ne sont pas réalisées effectivement mais juste simulées');

                    return false;
                }
            } catch (Exception $ex) {
                $this->logger->critical('Un exception a été levée pendant la migration des AffiliationService pour les services '.$idServiceSource.'/'.$idServiceDestination.". Le message d'erreur est le suivant : ".$ex->getMessage());
                $this->logger->critical('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

                return false;
            }
        } else {
            $this->logger->error('Les services '.$idServiceSource.'/'.$idServiceDestination." n'ont pas été retrouvé en BD");
            $this->logger->error('Fin du traitement pour : '.$idServiceSource.'/'.$idServiceDestination);

            return false;
        }
    }

    /**
     * @param $idService
     * @param $acronyme
     */
    private function getService($idService, $acronyme): bool|Service
    {
        $criterias = ['acronymeOrg' => $acronyme, 'id' => $idService];

        $service = $this->em->getRepository(Service::class)->findBy($criterias);
        if (count($service) > 0 && $service[0] instanceof Service) {
            return $service[0];
        }

        return false;
    }

    /**
     * @param $acronymeOrganisme
     */
    private function getOrganisme($acronymeOrganisme): bool|Organisme
    {
        $criterias = ['acronyme' => $acronymeOrganisme];

        $organisme = $this->em->getRepository(Organisme::class)->findBy($criterias);
        if (count($organisme) > 0 && $organisme[0] instanceof Organisme) {
            return $organisme[0];
        }

        return false;
    }

    /**
     * Enregistre dans le fichier nommé $cheminFichierServices."_correspondance_affiliation_services" la correspondance entre l'ancien pole/service et le nouveau pole/service.
     *
     * @param Service            $serviceAmigrer        Ancien service
     * @param Service            $serviceParentSource   Parent ancien service
     * @param AffiliationService $newAffiliationService Objet AffiliationService nouvellement créé
     */
    private function saveToFile(Service $serviceAmigrer, Service $serviceParentSource, AffiliationService $newAffiliationService)
    {
        $this->logger->info('Ajout dans le fichier de correspondance ancien/nouveau affiliationService: '.$this->cheminFichierAffiliationServiceCorrespondances.' : '.$serviceParentSource->getId().';'.$serviceAmigrer->getId());

        $fp = fopen($this->cheminFichierAffiliationServiceCorrespondances, 'a+');
        fwrite($fp, $serviceParentSource->getId().'/'.$serviceAmigrer->getId().';'.$newAffiliationService->getServiceParentId()->getId().';'.$newAffiliationService->getServiceId()->getId()."\n");
        fclose($fp);
    }
}
