<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Blob;

use App\Entity\BloborganismeFile;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;

class BlobOrganismeFileService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager
    ) {
    }

    public function getFile(string|int $blobId, bool $isId = false): ?array
    {
        $blobId = $isId ? $blobId : $this->encryption->decryptId($blobId);

        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->find($blobId);

        return $this->getNameAndRealPath($blobFile);
    }

    public function getRealPathFileByName(string $name): ?array
    {
        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->findOneByName($name);

        return $this->getNameAndRealPath($blobFile);
    }

    public function getNameAndRealPath(?BloborganismeFile $blobFile): ?array
    {
        if (!($blobFile instanceof BloborganismeFile)) {
            return null;
        }

        $fichier = $this->mountManager->getAbsolutePath(
            $blobFile->getId(),
            $blobFile->getOrganisme(),
            $this->entityManager
        );

        if (file_exists($fichier)) {
            return ['name' => $blobFile->getName(), 'realPath' => $fichier];
        }

        return null;
    }
}
