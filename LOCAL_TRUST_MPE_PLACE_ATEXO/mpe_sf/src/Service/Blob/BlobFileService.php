<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Blob;

use Exception;
use DateTime;
use App\Entity\BlobFichier;
use App\Entity\DocumentType;
use App\Service\AtexoFichier;
use App\Service\AtexoUtil;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BlobFileService
{
    public function __construct(
        private readonly MountManager $mountManager,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Filesystem $filesystem,
        private readonly EntityManagerInterface $entityManager,
        private readonly AtexoUtil $atexoUtil
    ) {
    }

    public function getNameAndRealPath(?BlobFichier $blobFile): ?array
    {
        if ($blobFile instanceof BlobFichier) {
            $fichier = $this->mountManager->getAbsolutePathBlobFichier($blobFile->getId());

            if (file_exists($fichier)) {
                return ['name' => $blobFile->getName(), 'realPath' => $fichier];
            }
        }

        return null;
    }

    public function uploadExternalFileToBlobFile(
        DocumentType $documentType,
        string $url,
        array $content,
        string $nomDocument
    ): ?BlobFichier {
        try {
            $this->logger->info("Début d'insertion d'un nouveau fichier de blob");

            $fileContent = file_get_contents($url);

            $fileName = md5(uniqid(random_int(0, mt_getrandmax()), true));

            $tempUploadedFile = $this->writeTmpFile($fileName, $fileContent);

            return $this->insertFile(
                fileName: $nomDocument,
                tempFilePath: $tempUploadedFile->getRealPath(),
                content: $content,
                extension: $tempUploadedFile->getExtension()
            );
        } catch (Exception $e) {
            $this->logger->error(
                sprintf("Erreur d'insertion d'un nouveau fichier de blob %s", $e->getMessage())
            );
        }

        return null;
    }

    private function writeTmpFile($documentName, $content): ?UploadedFile
    {
        $pathTmpTemplate = null;
        $tpmFile = null;
        try {
            $pathTmpTemplate = $this->parameterBag->get('COMMON_TMP') . $documentName;
            $this->filesystem->dumpFile($pathTmpTemplate, $content);
            $tpmFile = new UploadedFile($pathTmpTemplate, $documentName);
        } catch (Exception $e) {
            $this->logger->error(sprintf("Problème lors de la création du fichier temp %s", $pathTmpTemplate));
        }

        return $tpmFile;
    }

    public function insertFile(
        string $fileName,
        string $tempFilePath,
        array $content = [],
        string $folder = 'common',
        string $extension = '',
        string $subfolder = null
    ): ?BlobFichier {
        try {
            if (file_exists($tempFilePath)) {
                $pathSubFolder = (new DateTime())->format('Y/m/d') . '/' . $folder . '/files/';
                $destinationFolder = $this->parameterBag->get('BASE_ROOT_DIR') . $pathSubFolder;

                $blobFile = new BlobFichier();
                $blobFile->setName($fileName);
                $blobFile->setStatutSynchro(0);
                $blobFile->setDossier(null);

                $dir = '/' . $destinationFolder;
                if ($subfolder) {
                    $dir .=  '/' . $folder;
                }

                $blobFile->setChemin($pathSubFolder);
                $blobFile->setExtension($extension);
                $blobFile->setHash(md5(json_encode($content, JSON_THROW_ON_ERROR)));

                $this->entityManager->persist($blobFile);
                $this->entityManager->flush($blobFile);

                $destinationFilePath = sprintf(
                    "%s%s%s%s",
                    $dir,
                    $blobFile->getId(),
                    '-0',
                    $extension
                );

                $this->filesystem->copy($tempFilePath, $destinationFilePath);
                if (file_exists($destinationFilePath)) {
                    return $blobFile;
                } else {
                    $this->logger->error(
                        sprintf(
                            "Erreur insertion fichier au niveau du disque => %s vers %s",
                            $tempFilePath,
                            $destinationFilePath,
                        )
                    );
                }
            } else {
                $this->logger->error(sprintf("Le fichier temp %s n'existe pas", $tempFilePath));
            }
        } catch (Exception $e) {
            $this->logger->error(sprintf("Erreur lors de la création du BlobFile %s", $tempFilePath));
        }

        return null;
    }

    public function getFileSize(BlobFichier $blobFile): ?string
    {
        $fileNameAndPath = $this->getNameAndRealPath($blobFile);

        if (!$fileNameAndPath) {
            return null;
        }

        $filePath = $fileNameAndPath['realPath'];

        return is_file($filePath) ? $this->atexoUtil->getSizeName(@filesize($filePath)) : null;
    }
}
