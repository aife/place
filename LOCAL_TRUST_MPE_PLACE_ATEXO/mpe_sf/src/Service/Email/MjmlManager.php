<?php

namespace App\Service\Email;

use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class MjmlManager
{
    private const MJML_COMMAND = 'mjml';

    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    /**
     * Call mjml to compile mjml file.
     */
    public function compileMjml(string $filePath): bool
    {
        $inputFile = pathinfo($filePath);
        $fileName = $inputFile['basename'];
        if (!preg_match('/\.mjml\.twig$/', $fileName)) {
            $error = 'File must be named *.mjml.twig';
            $this->logger->error($error);
            throw new InvalidArgumentException($error);
        }
        $path = $inputFile['dirname'];
        if (file_exists($filePath)) {
            $process = new Process([
                self::MJML_COMMAND,
                $filePath,
                '-o',
                $path.'/'.str_replace('mjml', 'html', $fileName),
            ]);
            $process->run();

            $return = true;
            if (!$process->isSuccessful()) {
                $this->logger->error($process->getErrorOutput());
                throw new ProcessFailedException($process);
            }

            return $return;
        } else {
            $error = 'File '.$filePath.' not found';
            $this->logger->error($error);
            throw new InvalidArgumentException($error);
        }
    }
}
