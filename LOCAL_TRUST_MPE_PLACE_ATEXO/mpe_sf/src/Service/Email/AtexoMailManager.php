<?php

namespace App\Service\Email;

use Swift_Message;
use Swift_Attachment;
use Exception;
use App\Service\MailSender;
use Html2Text\Html2Text;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Yaml\Yaml;
use Twig\Environment;

class AtexoMailManager
{
    private const TEST_FILE = __DIR__ . '/../../Resources/config/mail/test_data.yml';
    private const TEXT_PLAIN_MIME = 'text/plain';
    private const HTML_MIME = 'text/html';
    private ?string $html = null;

    /**
     * AtexoMailManager constructor.
     */
    public function __construct(private readonly Environment $templating, private readonly MailSender $mailSender, private readonly LoggerInterface $logger, private readonly string $template = '', private array $options = [], private readonly string $encoding = '')
    {
    }

    /**
     * Send email without template. To support for exemple.
     */
    public function sendSimpleEmail(string $to, string $subject, string $text)
    {
        $message = new Swift_Message($subject);
        $message->setBody($text)
            ->setFrom($this->options['from'])
            ->setTo($to)
            ->setCharset($this->encoding)
            ->setContentType(self::TEXT_PLAIN_MIME);

        $this->mailSender->send($message);
    }

    /**
     * Send email.
     */
    public function sendMail(string $to, array $variables, bool $signed = false, ?string $attachement = null): bool
    {
        $this->generateHtml($variables);

        $html2text = new Html2Text($this->html);
        if ($this->checkParams() && filter_var($to, FILTER_VALIDATE_EMAIL)) {
            $message = new Swift_Message($this->options['subject']);
            $message
                ->setFrom(
                    $this->options['from'],
                    ($this->options['fromLabel'] ?? null)
                )
                ->setTo($to)
                ->setBody($this->html)
                ->setContentType(self::HTML_MIME)
                ->addPart($html2text->getText(), self::TEXT_PLAIN_MIME);

            if ($attachement && file_exists($attachement)) {
                $message->attach(Swift_Attachment::fromPath($attachement));
            }

            try {
                $this->mailSender->send($message, $signed);
            } catch (Exception $e) {
                $this->logger->error('Une erreur est survenue', [
                    'error' =>$e->getMessage(),
                    'trace' => $e->getTraceAsString()
                ]);
                $this->logger->error('Impossible d\'envoyer un email à ' . $to, $variables);

                return false;
            }

            return true;
        }

        return false;
    }

    public function generateHtml(array $variables = []): AtexoMailManager
    {
        $this->html = $this->templating->render(
            $this->template,
            $variables
        );

        return $this;
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        return $this->html;
    }

    /**
     * Get test variables values.
     */
    public function getTestVariables(): ?array
    {
        if (file_exists(self::TEST_FILE)) {
            $testVars = Yaml::parseFile(self::TEST_FILE);

            return $testVars[$this->getTemplateBaseName()];
        }
        return null;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): AtexoMailManager
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get clean template name.
     */
    protected function getTemplateBaseName(): string
    {
        $nameElements = explode(':', $this->template);
        $element = end($nameElements);

        return str_replace('.html.twig', '', $element);
    }

    /**
     * Check params.
     */
    protected function checkParams(): bool
    {
        $error = null;
        if (!array_key_exists('from', $this->options)) {
            $error = 'Missing \'from\' parameter';
        }
        if (!filter_var($this->options['from'], FILTER_VALIDATE_EMAIL)) {
            $error = '\'from\' parameter in not a valid email';
        }
        if (!array_key_exists('subject', $this->options)) {
            $error = 'Missing \'subject\' option';
        }

        if (null != $error) {
            $this->logger->error($error);
            throw new InvalidArgumentException($error);
        } else {
            return true;
        }
    }
}
