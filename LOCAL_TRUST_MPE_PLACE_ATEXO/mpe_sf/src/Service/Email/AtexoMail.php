<?php

namespace App\Service\Email;

interface AtexoMail
{
    public function sendMail(string $to, array $variables);

    public function getTestVariables();
}
