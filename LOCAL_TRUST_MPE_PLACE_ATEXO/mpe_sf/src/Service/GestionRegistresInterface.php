<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

interface GestionRegistresInterface
{
    public function getAll(int $idConsultation): array;
}
