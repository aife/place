<?php

namespace App\Service;

use App\Entity\Entreprise;
use App\Entity\Responsableengagement;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Classe contenant des fonctions utiles.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 0
 *
 * @since   0
 */
class AtexoResponsableEngagement
{
    /**
     * @var
     */
    private $container;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Constructeur de la classe.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     */
    public function __construct(ContainerInterface $container, protected EntityManagerInterface $em)
    {
        $this->setContainer($container);
    }

    /**
     * permet de transformer un tableau de diri la liste des dirigeants d'une entreprise.
     *
     * @param array dirigeants
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setArrayCommonResponsableengagementInEntreprise($tabDirigeant, &$entreprise)
    {
        if ($entreprise instanceof Entreprise) {
            if (is_array($tabDirigeant)) {
                foreach ($tabDirigeant as $dirigeant) {
                    $commonResonsable = $this->transformerLeaderObjet($dirigeant);
                    if ($commonResonsable) {
                        //$commonResonsable->setEntreprise($entreprise);
                        $entreprise->addResponsableengagement($commonResonsable);
                    }
                }
            }
        }
    }

    /**
     * permet de supprimer les dirigeant d'une entreprise.
     *
     * @param array leaderTab retourner depuis le WS
     * @param idEntreprise
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function transformerLeaderObjet($leaderTab)
    {
        if (is_array($leaderTab)) {
            $dirigeant = new Responsableengagement();
            $dirigeant->setNom($leaderTab['nom']);
            $dirigeant->setPrenom($leaderTab['prenom']);
            $dirigeant->setQualite($leaderTab['fonction']);

            return $dirigeant;
        } else {
            return false;
        }
    }

    /**
     * permet de supprimer les dirigeant d'une entreprise.
     *
     * @param array idEntreprise
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function deleteLeaders($idEntreprise)
    {
        if ($idEntreprise) {
            $repo = $this->em->getRepository('Responsableengagement');
            $repo->deleteResponsableByIdEntreprise($idEntreprise);
        }
    }
}
