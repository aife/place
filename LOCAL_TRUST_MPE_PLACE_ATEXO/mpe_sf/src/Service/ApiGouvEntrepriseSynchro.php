<?php

namespace App\Service;

use App\Traits\ConfigProxyTrait;
use Exception;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Classe pour gerer l'interface de synchronisation entre LT API et Api Gouv Entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 0
 *
 * @since   0
 */
class ApiGouvEntrepriseSynchro
{
    use ConfigProxyTrait;

    private $container;

    private string $token = '';
    private string $api_gouv_url_host = '';
    private string $api_gouv_url_entreprise = '';
    private string $api_gouv_url_etablissement = '';

    private $logger = null;
    final const ENTREPRISE = 1;
    final const ETABLISSEMENT = 2;
    final const EXERCICE = 3;
    final const ATTESTATION_FISCALE = 4;
    final const ATTESTATION_SOCIALE = 5;
    final const ELIGIBILITE_COTISATION_RETRAITE = 6;
    final const CERTIFICAT_QUALIBAT = 7;
    final const API = 8;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Initialise un objet ApiGouvEntrepriseSynchro.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     *
     * @param array $config
     */
    public function __construct(
        ContainerInterface $container,
        private readonly HttpClientInterface $httpClient,
        private readonly ParameterBagInterface $parameterBag
    ) {
        $this->setContainer($container);
        $this->logger = $container->get('mpe.logger');
    }

    public function setParameters($config)
    {
        foreach ($config as $attribut => $value) {
            $nameAtt = $attribut;
            if (property_exists($this, $attribut)) {
                $this->$nameAtt = $value;
                $this->logger->info('Property = ' . $nameAtt . ' value = ' . $value);
            } else {
                $this->logger->error("Erreur le paramètre \"$nameAtt\" (valeur=\"$value\") n'existe pas");
            }
        }
    }

    /**
     * Permet de recuperer les entreprises et retourne le json
     * exemple :
     *     $sirenAtexo = '440909562';
     *     $result = $ws->getEntrepriseBySiren($sirenAtexo);.
     *
     * @param string $siren : siren de l'entreprise
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getEntrepriseBySiren($siren, $withEtablissement = false)
    {
        $this->logger->info("Recuperation donnees entreprise Api Gouv Entreprise : siren => $siren");
        $urlApi = $this->api_gouv_url_host . $this->api_gouv_url_entreprise;
        $token = $this->token;
        $params = $this->getParametresRequetes($siren);
        //TODO Add parmeters
        $returnEntreprise = $this->sendToApiGouvEntreprise($urlApi . $siren, $token, self::ENTREPRISE, $params);

        if ($withEtablissement && static::isResponseSuccess($returnEntreprise) && $returnEntreprise[1] instanceof Entreprise) {
            $siret = $returnEntreprise[1]->getSiretSiegeSocial();
            $this->logger->info("Recuperation de l'etablissement siège de l'entreprise dont le siret => " . $siret);
            $returnEtablissement = $this->getEtablissementBySiret($siret);

            if ($returnEtablissement[1] instanceof Etablissement) {
                $this->logger->info("L'etablissement siège de l'entreprise existe ");
                $returnEntreprise[1]->addEtablissement($returnEtablissement[1]);
            }
            $this->logger->info("Fin de recuperation de l'etablissement siège de l'entreprise dont le siret => " . $siret);
        }

        return $returnEntreprise;
    }

    /**
     * Permet de recuperer les Etablissements et retourne le json
     * exemple :
     * $siretAtexo = '440909562000033';
     * $result = $ws->getEtablissementBySiret($siretAtexo);.
     *
     * @param string $siret : siret de l'etablissement
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getEtablissementBySiret($siret)
    {
        $token = $this->token;
        $apiUrl = $this->api_gouv_url_host . $this->api_gouv_url_etablissement;

        //TODO Add parmeters
        $params = $this->getParametresRequetes($siret);
        $this->logger->info("Recuperation donnees etablissement Api Gouv Entreprise : siret => $siret");

        return $this->sendToApiGouvEntreprise($apiUrl . $siret, $token, self::ETABLISSEMENT, $params);
    }

    /**
     * Permet de recuperer les exercices d'un etablissement donne
     * exemple :
     * $siretAtexo = '44090956200033';
     * $result = $ws->getExerciceBySiret($siretAtexo);.
     *
     * @param string $siret : siret dans le cas de l'etablissement
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 0
     *
     * @since   0
     *
     * @copyright Atexo 2015
     */
    public function getExerciceBySiret($siret)
    {
        $this->logger->info("Recuperation donnees exercices etablissement Api Gouv Entreprise : siret => $siret");

        return $this->sendToApiGouvEntreprise($this->url_host . $this->url_exercice . $siret, self::EXERCICE);
    }

    /**
     * Permet de recuperer l'Attestation fiscales de l'entreprise
     * exemple :
     * $siren = '4409095620';
     * $result = $ws->getAttestationFiscale($siren);.
     *
     * @param string $siren : siren de l'entreprise
     *
     * @return array un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getAttestationFiscale($siren)
    {
        $this->logger->info("Recuperation de l'attestation fiscale de l'entreprise dont le siren => $siren");

        return $this->sendToApiGouvEntreprise($this->url_host . $this->url_attestation_fiscale . $siren, self::ATTESTATION_FISCALE);
    }

    /**
     * Permet de retourner le type d'attestation sociale en fonction du type.
     *
     * @param string $type : le type d'attestation sociale
     *
     * @return string le type d'attestation sociale
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getTypeAttestationSociale($type)
    {
        return match ($type) {
            $this->type_attestation_sociale_marche_public => 'AMP_UR',
            $this->type_attestation_sociale_vigilence => 'AVG_UR',
            default => '',
        };
    }

    public function getEligibiliteCotisationRetraite($siret)
    {
        $this->logger->info("Recuperation données éligibilité cotisation retraite Api Gouv Entreprise : siret => $siret");
        $fullurl = $this->url_host . $this->url_eligibilite_cotisation_retraite . $siret;
        $data = $this->sendToApiGouvEntreprise($fullurl, self::ELIGIBILITE_COTISATION_RETRAITE);

        return $data;
    }

    public function getCertificatQualibat($siret)
    {
        $this->logger->info("Récupération donnée Certificat Qualibat depuis Api Gouv Entreprise, siret = $siret");
        $fullurl = $this->url_host . $this->url_certificat_qualibat . $siret;
        $data = $this->sendToApiGouvEntreprise($fullurl, self::CERTIFICAT_QUALIBAT);

        return $data;
    }

    /**
     * Effectue la requete vers le WS Api Gouv Entreprise
     * et retourne le resultat apres traitement du statut.
     *
     * @param string $url    : Url du WS
     * @param array  $params : parametres eventuel a passer au WS
     * @param int    $type   : type d'acces 1 pour entreprise, 2 pour etablissement, ... voir constante de la classe
     *
     * @return array : liste des parametres
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    private function sendToApiGouvEntreprise($url, $token, $type = 1, $params = [])
    {
        try {
            $returnAPI = [];
            $config = [
                'curloptions' => [
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_SSL_VERIFYHOST => false,
                    CURLOPT_TIMEOUT => 30,
                ],
            ];

            if ($configProxy = self::getConfigProxyForLaminasHttpClient($this->container)) {
                $config = array_merge($config, $configProxy);
            }

            $paramRequest = [];
            $paramRequest['token'] = $token;
            if (is_array($params) && !empty($params)) {
                if (!empty($params['get']) && is_array($params['get'])) {
                    foreach ($params['get'] as $keyG => $valueG) {
                        $paramRequest[$keyG] = $valueG;
                    }
                }
            }

            $response = $this->httpClient->request(
                Request::METHOD_GET,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                    ],
                    'query' => $paramRequest
                ]
            );

            $this->logger->info('URL Api Gouv Entreprise appele ', [$url]);
            $this->logger->info('- Parametre(s) envoye(s) ', $paramRequest);

            $contentAPI = null;
            if (($statut = $response->getStatusCode()) == Response::HTTP_OK) {
                $contentAPI = $response->getContent() ? $response->getContent() : null;
            }

            $this->logger->info('- Status code reponse ', [$statut]);
            $this->logger->info('- Reponse recu ', [$contentAPI]);

            try {
                $returnAPI = json_decode($contentAPI, true, 512, JSON_THROW_ON_ERROR);
            } catch (Exception $e) {
                $this->logger->error('Erreur lors du decodage du json ', [$e->getMessage()]);
            }

            return $this->retourWs($statut, $returnAPI, $type);
        } catch (Exception $e) {
            $this->logger->error("Erreur lors de la connexion au web service code [$url] : ", [$e->getMessage()]);
        }
    }

    /**
     * Permet de gerer le retour du web service Api Gouv Entreprise
     * Dans les retours:
     *    ===> En cas d'erreur, le 3e element du tableau contient le code d'erreur
     *    ===> En cas d'erreur, le 2e element du tableau contient les details de l'erreur.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return array|int : un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    private function retourWs($statut, $reponse, $type = 1): array|int
    {
        $return = [];
        $reponseEncodee = json_encode($reponse, JSON_UNESCAPED_SLASHES);
        switch ($statut) {
            case '401':
                //Json invalide
                $this->logger->error('Json invalide');
                $return = ['cas_e401', $reponse, $statut];
                break;
            case '404':
                $this->logger->error('Donnee non trouvee');
                $reponse['errors'] = 'Donnée non trouvée';
                $return = ['cas_e404', $reponse, $statut];
                break;
            case '200':
                if (!empty($reponse['reponse']['gateway_error']) && true === $reponse['reponse']['gateway_error']) {
                    $this->logger->error('Gateway error');
                    $return = ['cas3', $reponse, $statut];
                } else {
                    switch ($type) {
                        case self::ENTREPRISE:
                            $this->logger->info('cas4 : Entreprise trouvee');
                            $return = ['cas4', $this->preRemplirEntreprise($reponse), $reponseEncodee];
                            break;
                        case self::ETABLISSEMENT:
                            $this->logger->info('cas5 : Etablissement trouve');
                            $return = ['cas4', $this->preRemplirEtablissement($reponse), $reponseEncodee];
                            break;
                        case self::EXERCICE:
                            $this->logger->info('cas6 : Exercice trouve');
                            $return = ['cas6', $reponseEncodee];
                            break;
                        case self::ATTESTATION_FISCALE:
                            $this->logger->info('cas7 : Attestation fiscale trouvee');
                            $return = ['cas7', $reponseEncodee];
                            break;
                        case self::ATTESTATION_SOCIALE:
                            $this->logger->info('cas8 : Attestation sociale trouvee');
                            $return = ['cas8', $reponseEncodee];
                            break;
                        case self::ELIGIBILITE_COTISATION_RETRAITE:
                            $this->logger->info("cas9 : Status d'éligibilité cotisation retraite trouvé");
                            $return = ['cas9', $reponseEncodee];
                            break;
                        case self::CERTIFICAT_QUALIBAT:
                            $this->logger->info('cas10 : Certificat qualibat trouvé');
                            $return = ['cas10', $reponseEncodee];
                            break;
                        case self::API:
                            $this->logger->info('cas11 : API trouvé');
                            $return = ['cas11', $reponseEncodee];
                            // no break
                        default:
                    }
                }
                break;
            case '422':
                $this->logger->error($reponse['base'][0]);
                $return = ['cas_e422', $reponse, $statut];
                break;
            case '503':
                if (self::ATTESTATION_SOCIALE == $type) {
                    $this->logger->error(" La plateforme Acoss n'est pas disponible actuellement");
                }
                if (self::ELIGIBILITE_COTISATION_RETRAITE == $type) {
                    $this->logger->error('La plateforme PROBTP est indisponible');
                }
                if (self::CERTIFICAT_QUALIBAT == $type) {
                    $this->logger->error('La plateforme Qualibat est indisponible');
                }
                $return = ['cas_e503', $reponse, $statut];
                break;
            default:
                $this->logger->error('Erreur inconnue');
                $return = ['cas_e_default', $reponse];
                break;
        }

        return $return;
    }

    /**
     * Permet de retourner si la communication avec ws Api Gouv Entreprise est reussi selon les cas retourner.
     *
     * @param array $reponse : la reponse
     *
     * @return bool true si reponse avec succes, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public static function isResponseSuccess($reponse)
    {
        $cas_success = ['cas4', 'cas5', 'cas6', 'cas7', 'cas8', 'cas9', 'cas10', 'cas11'];

        return is_array($reponse) && in_array($reponse[0], $cas_success);
    }

    /**
     * Permet de remplir l'objet entrepriseVo avec les donnees du web service.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return Atexo_Entreprise_EntrepriseVo : objet contenant les informations
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function preRemplirEntreprise($reponse)
    {
        $atexoEntreprise = $this->getContainer()->get(AtexoEntreprise::class);
        $this->logger->info("Remplir l'Entreprise par les donnees de json recu");
        $entreprise = $atexoEntreprise->loadFromApiGouvEntreprise(($reponse['entreprise']));

        return $entreprise;
    }

    /**
     * Permet de remplir l'objet etablissementVo avec les donnees du web service.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return Atexo_Entreprise_EtablissementVo : objet contenant les informations
     *
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function preRemplirEtablissement($reponse)
    {
        $atexoEtablissement = $this->getContainer()->get(AtexoEtablissement::class);
        $this->logger->info("Remplir l'Etablissement par les donnees de json recu");
        $etablissement = $atexoEtablissement->loadFromApiGouvEntreprise(($reponse['etablissement']));

        return $etablissement;
    }

    /**
     * Permet de construire les parametres de la requete.
     *
     * @param string $identifiant                : siren ou siret
     * @param string $type                       : type de document
     * @param string $refUtilisateurConsultation : reference de la consultation
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR2015
     *
     * @copyright Atexo 2016
     */
    public function getParametresRequetes($identifiant)
    {
        $params = [];
        $params['context'] = 'MPS';
        $params['object'] = 'Synchronisation donnees Entreprise, Etablissement ou Service';
        $params['recipient'] = $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT');

        return ['get' => $params];
    }
}
