<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use ReflectionException;
use JsonException;
use BadMethodCallException;
use App\Attribute\ResultType;
use App\Entity\Agent;
use App\Exception\MethodMustOverrideException;
use App\Exception\UnauthorizedPublicMethod;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use ReflectionClass;
use ReflectionMethod;
use RuntimeException;

class AbstractService
{
    public const CONTENT_TYPE_FORM = 'application/x-www-form-urlencoded';
    public const CONTENT_TYPE_JSON = 'application/json';
    public const CONTENT_TYPE_PATCH = 'application/merge-patch+json';
    public const OPEN_ID_RECENSEMENT = '/auth/realms/recensement/protocol/openid-connect/token';
    public const OPEN_ID_SOURCING = '/auth/realms/anm/protocol/openid-connect/token';
    public const OPEN_ID_SPASER = '/auth/realms/spaser/protocol/openid-connect/token';
    public const OPEN_ID_REDAC = '/auth/realms/redac/protocol/openid-connect/token';
    public const OPEN_ID_EXEC = '/auth/realms/exec/protocol/openid-connect/token';
    private const GRANT_TYPE = 'password';
    private const JSON_DECODE = 'json_decode';
    public const ACCESS_TOKEN = 'access_token';
    public const REFRESH_TOKEN = 'refresh_token';

    private string $endUserMessage;
    protected bool $throw = true;
    private const URL = 'url';
    private const WS_TIMEOUT = 10;

    public function __construct(
        private HttpClientInterface $httpClient,
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag
    ) {
        $class = new ReflectionClass(get_class($this));
        $publicMethods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        if (is_countable($publicMethods) && count($publicMethods) > 3) {
            throw new UnauthorizedPublicMethod(sprintf('Adding public method not Allowed for %s.', $this::class));
        }
    }

    /**
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function request(string $method, string $url, array $options = []): ?ResponseInterface
    {
        $response = null;
        $defaultOptions = [
            'timeout' => $this->parameterBag->get('timeOutService'),
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
            ],
        ];

        $options = $options + $defaultOptions;
        try {
            $this->logger->info('Début d\'appel au webservice ' . $url);
            $response = $this->httpClient->request($method, $url, $options);
            $this->logResponseByStatusCode($response, $url);
        } catch (TimeoutExceptionInterface $e) {
            $this->logger->error(sprintf('Timeout lors de l\'appel au webservice %s', $url), [
                'exception_message' => $e->getMessage(),
                'exception_trace' => $e->getTraceAsString(),
            ]);
            $this->endUserMessage = 'Timeout lors de l\'appel au webservice %s';
            if (true === $this->throw) {
                throw $e;
            }

            return null;
        } catch (TransportExceptionInterface $e) {
            $this->logger->error(sprintf('MPE ne peut pas se connecter au WS : %s ', $url), [
                'exception_message' => $e->getMessage(),
                'exception_trace' => $e->getTraceAsString(),
            ]);
            $this->endUserMessage = 'Une erreur réseau s\'est produite lors de la connexion de MPE au service %s.';
            if (true === $this->throw) {
                throw $e;
            }

            return null;
        }

        return $response;
    }

    protected function getAuthenticationToken(
        string $urlIdentite,
        string $clientId,
        string $userName,
        string $password,
        int $timeout = self::WS_TIMEOUT
    ): array {
        return $this->getAuthenticationTokenResponse(
            $urlIdentite,
            $clientId,
            $userName,
            $password,
            $timeout
        );
    }

    protected function getAuthenticationTokenResponse(
        string $urlIdentite,
        string $clientId,
        string $userName,
        string $password,
        int $timeout = self::WS_TIMEOUT
    ): array {
        $url = $this->parameterBag->get('IDENTITE_BASE_URL') . $urlIdentite;
        $headers = [
            'Content-Type' => self::CONTENT_TYPE_FORM
        ];
        $body = [
            'grant_type'    => self::GRANT_TYPE,
            'client_id'     => $clientId,
            'username'      => $userName,
            'password'      => $password
        ];

        $response = $this->request('POST', $url, [
            'headers'   => $headers,
            'body'      => $body,
            'timeout'   => $timeout
        ]);

        if (null === $response) {
            return [
                self::ACCESS_TOKEN => null,
                'refresh_token' => null
            ];
        }

        return $response->toArray($this->throw);
    }

    protected function getAgentContext(
        string $contextEndpoint,
        Agent $agent,
        string $accessToken,
        string $refreshToken = ''
    ): string {
        $reflector = new ReflectionMethod($this, 'setDataXML');
        if ($reflector->getDeclaringClass()->getName() === AbstractService::class) {
            throw new MethodMustOverrideException('The function setDataXml must be overridden
             in services using getAgentContext function.');
        }

        $response = $this->request(
            'POST',
            $contextEndpoint,
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                    'Authorization' => 'Bearer ' . $accessToken,
                    'RefreshToken' => $refreshToken
                ],
                'body' => $this->setDataXML($agent),
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    public function getEndUserExceptionMessage(string $serviceName): string
    {
        return sprintf($this->endUserMessage, $serviceName);
    }

    /**
     * @param bool $throw not thrown httpclient exceptions if false
     */
    protected function setHttpClientThrow(bool $throw): void
    {
        $this->throw = $throw;
    }

    private function logResponseByStatusCode(ResponseInterface $response, string $url): void
    {
        switch (true) {
            case (200 <= $response->getStatusCode() && 300 > $response->getStatusCode()):
                $this->logger->info(sprintf('Fin d\'appel au webservice %s avec une reponse valide', $url));
                $this->endUserMessage = '';
                break;
            case (300 <= $response->getStatusCode() && 400 > $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice avec problème de redirection %s', $url)
                );
                $this->endUserMessage = 'Le service %s a retourné une redirection';
                break;
            case (400 === $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : les options sont invalides', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Les options de la requête envoyée au service %s sont invalides.';
                break;
            case (401 === $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : Identifiant ou token invalide', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Identifiant ou token invalide pour accéder au service %s.';
                break;
            case (403 === $response->getStatusCode()):
                $this->logger->error(
                    sprintf(
                        'Fin d\'appel au webservice %s : Vous n\'êtes pas autorisé à accéder à cette ressource',
                        $url
                    ),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Vous n\'êtes pas autorisé à accéder à cette ressource du service %s ';
                break;
            case (404 === $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : cette ressource n\'existe pas', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'La ressource n\'existe pas dans le service %s ';
                break;
            case (404 <= $response->getStatusCode() && 500 > $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : Erreur client retournée par le WS', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Le service %s a retourné une erreur client';
                break;
            case (500 <= $response->getStatusCode() && 600 > $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : Le serveur a retourné une erreur', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Le service %s a retourné une erreur serveur.';
                break;
            case (100 <= $response->getStatusCode() && 200 > $response->getStatusCode()):
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : Le WS a retourné une information', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Le service %s a retourné une information technique.';
                break;
            default:
                $this->logger->error(
                    sprintf('Fin d\'appel au webservice %s : Le WS retourne un code de statut invalide', $url),
                    $this->getResponseErrorAndCode($response)
                );
                $this->endUserMessage = 'Le service %s a retourné une reponse invalide.';
                break;
        }
    }

    protected function setDataXML(Agent $agent): false|string
    {
        return '';
    }

    /**
     * @throws ReflectionException
     * @throws JsonException
     */
    public function getContent(string $functionName, mixed ...$funtionArgs): mixed
    {
        if (!method_exists($this, $functionName)) {
            throw new BadMethodCallException(
                sprintf('%s function doesn\'t exist in %s.', $functionName, $this::class)
            );
        }

        $result = $this->$functionName(...$funtionArgs);

        $reflectionMethod = new ReflectionMethod($this, $functionName);
        $attributes = $reflectionMethod->getAttributes(ResultType::class);
        if (null === $attributes || (is_countable($attributes) && 0 === count($attributes))) {
            return $result;
        }

        if (self::URL === $attributes[0]->newInstance()->type) {
            return $this->checkValidationUrl($result);
        }

        if (self::JSON_DECODE === $attributes[0]->newInstance()->type) {
            return $this->decodeJson($result);
        }

        return $result;
    }

    private function checkValidationUrl(string $functionContent): string
    {
        if (false === filter_var($functionContent, FILTER_VALIDATE_URL)) {
            throw new RuntimeException('This function must return valid URL.');
        }

        return $functionContent;
    }

    /**
     * @throws JsonException
     */
    private function decodeJson(string $functionContent): mixed
    {
        return json_decode($functionContent, null, 512, JSON_THROW_ON_ERROR);
    }

    private function getResponseErrorAndCode(ResponseInterface $response): array
    {
        return [
            'response_error' => $response->getContent(false),
            'response_code' => $response->getStatusCode()
        ];
    }
}
