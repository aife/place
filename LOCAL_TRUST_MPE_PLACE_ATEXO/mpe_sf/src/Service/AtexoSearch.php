<?php

namespace App\Service;

use Exception;
use Prado;
use TLogger;
use App\Entity\ConfigurationPlateforme;
use App\Service\Messagerie\MessagerieService;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SearchLucene;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AtexoSearch
{
    protected array $queryParams = [];
    protected $criteriaVo;
    protected $isEnabled;

    public final const MOTS_CLES_INTERDITS = " and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf 
                                ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ";
    /**
     * AtexoSearch constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        protected ContainerInterface $container,
        protected EntityManagerInterface $em,
        protected LoggerInterface $logger,
        protected AtexoUtil $serviceUtile,
        private readonly MessagerieService $messagerieService,
        protected SessionInterface $session
    ) {
    }

    /** ********************************************************************
     *      Bloc Refonte entreprise.
     *  ********************************************************************
     */

    /**
     * @param $this ->criteriaVo
     * @param $queryParams
     */
    protected function otherConditions($gestionOperations = false, $referenceByReferentiel = null)
    {
        $texteRecherche = null;
        $otherConditions = '';
        if ('tout' != $this->criteriaVo->getTypeSearch()) {
            if ($this->criteriaVo->getIdReference()) {
                $otherConditions .= ' AND consultation.id=:idReference';
                $this->queryParams[':idReference'] = $this->criteriaVo->getIdReference();
            }
            if ($this->criteriaVo->getReferenceConsultation()) {
                $otherConditions .= ' AND consultation.reference_utilisateur LIKE :referUser ';
                $this->queryParams[':referUser'] = '%'.$this->criteriaVo->getReferenceConsultation().'%';
            }
            if ($this->criteriaVo->getReferenceConsultationRestreinte()) {
                $otherConditions .= ' AND consultation.reference_utilisateur =:referUser ';
                $this->queryParams[':referUser'] = $this->criteriaVo->getReferenceConsultationRestreinte();
            }

            // Debut du filtre des consultations à archivées
            if ($this->criteriaVo->getConsultationAArchiver()) {
                //Exclure les consultations SAD  AC
                $exclusionIdSadAc = <<<QUERY

AND id_type_procedure_org NOT IN (
SELECT
	Type_Procedure_Organisme.id_type_procedure
FROM
	Type_Procedure_Organisme
WHERE
	Type_Procedure_Organisme.organisme = :organismeAgent
	AND (( Type_Procedure_Organisme.sad = '1') OR (Type_Procedure_Organisme.accord_cadre ='1'))

)
QUERY;

                //Condition Préparation
                $conditionPreparation = <<<QUERY

 (
ID_ETAT_CONSULTATION = 0
AND ( DATEFIN > CURDATE() OR DATEFIN = '0000-00-00 00:00:00')
AND (
	 DATE_MISE_EN_LIGNE_CALCULE = ''
	 OR DATE_MISE_EN_LIGNE_CALCULE = '0000-00-00 00:00:00'
	 OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()
	 OR DATE_MISE_EN_LIGNE_CALCULE IS NULL
)
AND DATEDEBUT != '' AND DATEDEBUT != '0000-00-00 00:00:00'
AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH )
 )
QUERY;
                //Condition Ouverture et Analyse
                $conditionOuvertureAnalyse = <<<QUERY

OR (
	ID_ETAT_CONSULTATION = 0 AND DATEFIN != ''
	AND DATEFIN != '0000-00-00 00:00:00'
	AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH)
)
QUERY;
                $conditionDecision = <<<QUERY

OR (
	ID_ETAT_CONSULTATION = :status_decision
	AND  DATE_DECISION != ''
	AND DATE_DECISION != '0000-00-00 00:00:00'
	AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL :duree_rappel_archivage MONTH)
 )
QUERY;
                $this->queryParams[':duree_rappel_archivage'] = $this->container->getParameter('DUREE_RAPPEL_ARCHIVAGE');
                $this->queryParams[':status_decision'] = $this->container->getParameter('STATUS_DECISION');

                //Condition Decision
                $conditionCommun = <<<QUERY

DEPOUILLABLE_PHASE_CONSULTATION = '0'
AND ID_TYPE_AVIS = :type_avis_consultation
$exclusionIdSadAc
QUERY;
                $this->queryParams[':type_avis_consultation'] = $this->container->getParameter('TYPE_AVIS_CONSULTATION');

                $otherConditions .= <<<QUERY
 AND ( $conditionCommun  ) AND (  $conditionPreparation  $conditionOuvertureAnalyse  $conditionDecision  )
QUERY;
            }
            // Fin du filtre des consultations à archivées

            if (true === $this->criteriaVo->getcalledFromPortail()) {
                if ($this->criteriaVo->getAcronymeOrganisme()) {
                    $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
                    $this->queryParams[':acronymeOrg'] = $this->criteriaVo->getAcronymeOrganisme();
                }

                $idsService = $this->criteriaVo->getIdsService();
                if (!$idsService && $this->criteriaVo->getIdService() && $this->criteriaVo->getInclureDescendance()) {
                    $idsService = Atexo_EntityPurchase::retrieveAllChildrenServices($this->criteriaVo->getIdService(), $this->criteriaVo->getAcronymeOrganisme());
                }

                if ('' != $idsService) {//On inclut la descendance dans la recherche
                    if ('0' === $this->criteriaVo->getIdService()) {
                        $otherConditions .= ' AND ((consultation.service_id IN '.$idsService.' ) OR consultation.service_id IS NULL) ';
                    } else {
                        $otherConditions .= ' AND (consultation.service_id IN '.$idsService.' )';
                    }
                } else {
                    if ('' != $this->criteriaVo->getIdService()) {
                        if ('0' === $this->criteriaVo->getIdService()) {
                            $otherConditions .= ' AND ((consultation.service_id IS NULL) OR  (consultation.service_associe_id IS NULL))';
                        } else {
                            $otherConditions .= ' AND ((consultation.service_id=:serviceId) OR  (consultation.service_associe_id=:serviceId))';
                            $this->queryParams[':serviceId'] = $this->criteriaVo->getIdService();
                        }
                    }
                }

                if ($this->criteriaVo->getTypeAcces() != $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                    if ($this->isEnabled->getPanierEntreprise() &&
                        true === $this->criteriaVo->getcalledFromPortail() &&
                        $this->criteriaVo->getForPanierEntreprise()) {
                        $otherConditions .= ' AND ( consultation.type_acces = :typeProc1 OR consultation.type_acces = :typeProc2 )';
                        $this->queryParams[':typeProc1'] = $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE');
                        $this->queryParams[':typeProc2'] = $this->container->getParameter('TYPE_PROCEDURE_PUBLICITE');
                    } else {
                        $otherConditions .= ' AND consultation.type_acces=:typeProc ';
                        $this->queryParams[':typeProc'] = $this->container->getParameter('TYPE_PROCEDURE_PUBLICITE');
                    }
                }

                if (trim($this->criteriaVo->getOrgDenomination())) {
                    $otherConditions .= self::searchOrgDenominationLikeMotcle(trim($this->criteriaVo->getOrgDenomination()), $this->queryParams);
                }
                if ($this->criteriaVo->getDenominationAdapte()) {
                    $denominationAdapte = array_map('addslashes', $this->criteriaVo->getDenominationAdapte());
                    $list = implode("','", $denominationAdapte);
                    $otherConditions .= " AND denomination_adapte in( '".$list."') ";
                }
            } else {
                if (Atexo_Module::isEnabled('OrganisationCentralisee', $this->criteriaVo->getAcronymeOrganisme())) {
                    $idsService = $this->criteriaVo->getIdsService();
                    if ('' != $idsService) {
                        $otherConditions .= ' AND (consultation.service_id IN '.$idsService.' )';
                    }
                }
            }

            if ($this->criteriaVo->getDateFinStart()) {
                $otherConditions .= ' AND consultation.datefin >= :dateFinStart ';
                $this->queryParams[':dateFinStart'] = $this->serviceUtile
                        ->frnDate2iso($this->criteriaVo->getDateFinStart()).' 00:00:00';
            }

            if ($this->criteriaVo->getDateFinEnd()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $this->queryParams[':dateFinEnd'] = $this->serviceUtile
                        ->frnDate2iso($this->criteriaVo->getDateFinEnd()).' 23:59:59';
            }

            if ($this->criteriaVo->getDateTimeFin()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $this->queryParams[':dateFinEnd'] = $this->criteriaVo->getDateTimeFin();
            }

            $cpvConditions = '';
            if ($this->criteriaVo->getIdCodeCpv2()) {
                $codesCPV = explode('#', (string) $this->criteriaVo->getIdCodeCpv2());
                if (is_array($codesCPV) && $codesCPV) {
                    $indexCodeCpv = 0;
                    foreach ($codesCPV as $cpv) {
                        //si jamais le code CPV est codé sur 10 car. on enlève les 2 derniers. ex : 30000000-0
                        if (10 == strlen($cpv)) {
                            $cpv = substr($cpv, 0, -2);
                        }
                        if ($cpv) {
                            $cpvConditions .= <<<QUERY

OR consultation.code_cpv_1 like :code_cpv_1$indexCodeCpv
OR consultation.code_cpv_2 like :codeCpv$indexCodeCpv

OR CategorieLot.code_cpv_1 like :code_cpv_1$indexCodeCpv
OR CategorieLot.code_cpv_2 like :codeCpv$indexCodeCpv
QUERY;
                            $this->queryParams[':codeCpv'.$indexCodeCpv] = '%#'.rtrim($cpv, '0').'%';
                            $this->queryParams[':code_cpv_1'.$indexCodeCpv] = rtrim($cpv, '0').'%';
                        }
                        ++$indexCodeCpv;
                    }
                }
            }

            if ($cpvConditions) {
                $otherConditions .= " AND (0 $cpvConditions)";
            }

            if ($this->criteriaVo->getCategorieConsultation()) {
                $otherConditions .= ' AND consultation.categorie=:categorie ';
                $this->queryParams[':categorie'] = $this->criteriaVo->getCategorieConsultation();
            }

            if ($this->criteriaVo->getIdTypeProcedure()) {
                if ($this->criteriaVo->getCalledFromHelios() || false === $this->criteriaVo->getcalledFromPortail()) {
                    $otherConditions .= ' AND consultation.id_type_procedure_org=:idtypeProc ';
                    $this->queryParams[':idtypeProc'] = $this->criteriaVo->getIdTypeProcedure();
                } else {
                    $otherConditions .= ' AND consultation.id_type_procedure=:idtypeProc ';
                    $this->queryParams[':idtypeProc'] = $this->criteriaVo->getIdTypeProcedure();
                }
            }

            if ($this->isEnabled->getLieuxExecution()) {
                if ($this->criteriaVo->getLieuxexecution()) {
                    $arrayLieuExecution = explode(',', (string) $this->criteriaVo->getLieuxexecution());
                    $indiceLieuExecution = 1;
                    $lieuExecQuery = '';
                    foreach ($arrayLieuExecution as $unLieuExec) {
                        if ($unLieuExec) {
                            $lieuExecQuery .= ((1 == $indiceLieuExecution) ? '' : ' OR ').' consultation.lieu_execution LIKE :lieuExec'.$indiceLieuExecution.' ';
                            $this->queryParams[':lieuExec'.$indiceLieuExecution] = '%,'.$unLieuExec.',%';
                            ++$indiceLieuExecution;
                        }
                    }
                    $otherConditions .= ($lieuExecQuery) ? ' AND ('.$lieuExecQuery.')' : '';
                }
            }

            //codes nuts
            if ($this->isEnabled->getCodeNutLtReferentiel()) {
                $codesNutsQuery = '';
                if ($this->criteriaVo->getCodesNuts()) {
                    $listeCodesNuts = explode('#', (string) $this->criteriaVo->getCodesNuts());
                    if ($listeCodesNuts[1]) {
                        $indiceCodesNuts = 1;
                        foreach ($listeCodesNuts as $codesNuts) {
                            if ($codesNuts) {
                                //Recherche des descendants complets
                                $codesNutsQuery .= ((1 == $indiceCodesNuts) ? '' : ' OR ').' consultation.codes_nuts LIKE :codesNuts'.$indiceCodesNuts.' ';
                                $this->queryParams[':codesNuts'.$indiceCodesNuts] = '%#'.$codesNuts.'%';

                                $indiceCodesNutsAsc = 1;
                                //Recherche des ascendants directs
                                while (strlen((string) $codesNuts) > $this->container->getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_CODE_NUT')) {
                                    //Lorsque l'ascendant cherché se trouve entre 2 codes
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscMedium'.$indiceCodesNuts.$indiceCodesNutsAsc.' ';
                                    $this->queryParams[':codesNutsAscMedium'.$indiceCodesNuts.$indiceCodesNutsAsc] =
                                        '%#'.$this->supprimerDernierCaractere($codesNuts).'#%';

                                    //Lorsque l'ascendant cherché se trouve au à l'extremité, ce cas inclus lorsque l'ascendant direct est le seul selectionné
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscLast'.$indiceCodesNuts.$indiceCodesNutsAsc.' ';
                                    $this->queryParams[':codesNutsAscLast'.$indiceCodesNuts.$indiceCodesNutsAsc] =
                                        '%#'.$this->supprimerDernierCaractere($codesNuts);

                                    $codesNuts = $this->supprimerDernierCaractere($codesNuts);
                                    ++$indiceCodesNutsAsc;
                                }

                                ++$indiceCodesNuts;
                            }
                        }
                        if ($codesNutsQuery) {
                            $otherConditions .= ' AND ('.$codesNutsQuery.')';
                        }
                    }
                }
            }
            //fin codes nuts
        }

        if ($this->criteriaVo->getTypeAcces() == $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            $otherConditions .= ' AND consultation.type_acces=:typeProc ';
            $this->queryParams[':typeProc'] = $this->criteriaVo->getTypeAcces();
            $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
            $this->queryParams[':acronymeOrg'] = $this->criteriaVo->getAcronymeOrganismeProcRest();
            $otherConditions .= ' AND consultation.code_procedure=:codeProc ';
            $this->queryParams[':codeProc'] = $this->criteriaVo->getCodeAccesProcedureRestreinte();
        }

        if ($this->criteriaVo->getIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis=:typeAvis ';
            $this->queryParams[':typeAvis'] = $this->criteriaVo->getIdTypeAvis();
        }

        if ($this->criteriaVo->getNotIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis!=:notTypeAvis ';
            $this->queryParams[':notTypeAvis'] = $this->criteriaVo->getNotIdTypeAvis();
        }

        if ($this->criteriaVo->getPublicationEurope()) {
            $otherConditions .= ' AND publication_europe=:publicationEurop ';
            $this->queryParams[':publicationEurop'] = $this->criteriaVo->getPublicationEurope();
        }

        //Début recherche date mise en ligne calculée
        if ($this->criteriaVo->getEnLigneDepuis()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule>=:dateMiseEnLigneCalculeStar';
            $this->queryParams[':dateMiseEnLigneCalculeStar'] = $this->serviceUtile
                    ->frnDate2iso($this->criteriaVo->getEnLigneDepuis()).' 00:00:00';
        }

        if ($this->criteriaVo->getEnLigneJusquau()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule<=:dateMiseEnLigneCalculeEnd ';
            $this->queryParams[':dateMiseEnLigneCalculeEnd'] = $this->serviceUtile
                    ->frnDate2iso($this->criteriaVo->getEnLigneJusquau()).' 23:59:59';
        }
        //Fin recherche date mise en ligne calculée

        $etatPublicationStatus = "{$this->container->getParameter('TO_BE_PUBLISHED')},{$this->container->getParameter('PUBLISHED')},{$this->container->getParameter('TO_BE_UPDATED')},{$this->container->getParameter('UPDATED')}";
        $otherConditions .= " AND etat_publication IN ({$etatPublicationStatus})";

        $rechercheExacte = false;
        $rechercheApproximative = false;
        $searchChaqueMot = true;

        /**
         * Tout le code dans le IF est mort ce n'est plus utiliser
         * MAis avant de le supprimer faut voir pour le reprendre et l'adapter
         */
        if ($this->criteriaVo->getKeyWordAdvancedSearch()) {
            $texteRecherche = str_replace('&quot;', '"', $this->criteriaVo->getKeyWordAdvancedSearch());
            $arrayWorrd = explode('"', (string) $texteRecherche);
            if ($this->criteriaVo->getSearchModeExact()) {

                if (3 == count($arrayWorrd)) {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                }
                $rechercheExacte = true;
            } else {
                if (3 != count($arrayWorrd)) {
                    $rechercheApproximative = true;
                    $precision = $this->container->getParameter('PRECISION_LUCENE');
                    if (false === $this->criteriaVo->getcalledFromPortail()) {
                        $organisme = $this->criteriaVo->getAcronymeOrganisme();
                        if ($this->criteriaVo->getFromArchive()) {
                            $pathDirIndexLucene = $this->container->getParameter('BASE_ROOT_DIR').$organisme.'/'.
                                $this->container->getParameter('DIR_INDEX_LUCENE_ARCHIVE');
                        } else {
                            $pathDirIndexLucene = $this->container->getParameter('BASE_ROOT_DIR').$organisme.'/'.
                                $this->container->getParameter('DIR_INDEX_LUCENE_AGENT');
                        }
                        //$searchLucene = new Atexo_Consultation_SearchLucene($pathDirIndexLucene);
                        $arrayIdToFilter = null; //$searchLucene->searchByOrganisme($this->criteriaVo->getAcronymeOrganisme(), $keyWord, $precision);
                        if (!empty($arrayIdToFilter)) {
                            $strIdToFilter = implode("', '", $arrayIdToFilter);
                            $strIdToFilter = "('".$strIdToFilter."')";
                        } else {
                            $strIdToFilter = '(0)';
                        }
                        // Que les résultats de Lucene :
                        $otherConditions .= " AND (consultation.id  IN $strIdToFilter ";
                    } else {
                        //$searchLucene = new Atexo_Consultation_SearchLucene(Atexo_Config::getParameter("DIR_INDEX_LUCENE_PORTAIL"));
                        $arrayIdToFilter = null; //$searchLucene->searchInAllOrganisme($keyWord,$precision);
                        if (!empty($arrayIdToFilter)) {
                            $otherConditions .= ' AND ((0 ';
                            foreach ($arrayIdToFilter as $org => $oneRef) {
                                $strIdToFilter = implode("', '", $oneRef);
                                $strIdToFilter = "('".$strIdToFilter."')";
                                $otherConditions .= " OR (consultation.organisme = '".$org."' AND consultation.id  IN ".$strIdToFilter.') ';
                            }
                            $otherConditions .= ' )';
                        } else {
                            // Si Lucene ne retourne aucuns reusltats :
                            $otherConditions .= ' AND (0 ';
                        }
                    }
                } else {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                    $rechercheExacte = true;
                }
            }
        }

        if ($rechercheExacte || $rechercheApproximative || $this->criteriaVo->getKeyWordRechercheRapide() || $this->criteriaVo->getKeyWordAdvancedSearch()) {
            if ($this->criteriaVo->getKeyWordRechercheRapide()) {
                $texteRecherche = $this->criteriaVo->getKeyWordRechercheRapide();
            }

            if ($texteRecherche) {
                $querySelectKeyWord = $this->searchLikeMotcle(
                    $texteRecherche,
                    $this->criteriaVo->getcalledFromPortail(),
                    $searchChaqueMot
                );

                $otherConditions .= ' '.($rechercheApproximative ? ' OR ' : '  AND').
                    ' ( '.' 0 '.(($querySelectKeyWord) ? ' OR '.$querySelectKeyWord : '').' ) '
                    .($rechercheApproximative ? ' )' : '');
            }
        }

        if ($this->isEnabled->getConsultationDomainesActivites()) {
            if ($this->criteriaVo->getDomaineActivite()) {
                $domainesActivites = explode('#', trim($this->criteriaVo->getDomaineActivite(), '#'));
                if (is_array($domainesActivites) && count($domainesActivites) >= 1) {
                    $conditions = [];
                    foreach ($domainesActivites as $activite) {
                        $conditions[] = "( consultation.`domaines_activites` like '%#".
                            $this->quote($activite)."%' OR  consultation.`domaines_activites` like '".$this->quote($activite)."%' )";
                    }
                    $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
                }
            }
        }

        // Domaines d'activités Lt-Référentiel
        if ($this->isEnabled->getConsultationDomainesActivitesLtReferentiel()) {
            $domainesActivitesLtRef = '';
            if ($this->criteriaVo->getDomaineActivite()) {
                $listeDomainesActivitesLtRef = explode('#', (string) $this->criteriaVo->getDomaineActivite());
                if ($listeDomainesActivitesLtRef[1]) {
                    $indiceDomainesActivitesLtRef = 1;
                    foreach ($listeDomainesActivitesLtRef as $domainesActivites) {
                        if ($domainesActivites) {
                            $domainesActivitesLtRef .= ((1 == $indiceDomainesActivitesLtRef) ? '' : ' OR ').' consultation.domaines_activites LIKE :domaines_activites'.$indiceDomainesActivitesLtRef.' ';
                            $this->queryParams[':domaines_activites'.$indiceDomainesActivitesLtRef] = '%#'.rtrim($domainesActivites, '0').'%';
                            ++$indiceDomainesActivitesLtRef;
                        }
                    }
                    if ($domainesActivitesLtRef) {
                        $otherConditions .= ' AND ('.$domainesActivitesLtRef.')';
                    }
                }
            }
        }

        if ($this->criteriaVo->getQualification()) {
            $qualifications = explode('#', trim($this->criteriaVo->getQualification(), '#'));
            if (is_array($qualifications) && count($qualifications) >= 1) {
                $conditions = [];
                foreach ($qualifications as $qualification) {
                    $conditions[] = "(  consultation.`qualification` like '%#".$this->quote($qualification)."#%' OR  consultation.`qualification` like '".$this->quote($qualification)
                        ."#%' OR  consultation.`qualification` like '%#".$this->quote($qualification)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        if ($this->criteriaVo->getAgrements()) {
            $agrements = explode(',', trim($this->criteriaVo->getAgrements(), ','));
            if (is_array($agrements) && count($agrements) >= 1) {
                $conditions = [];
                foreach ($agrements as $agrement) {
                    $conditions[] = "(  consultation.`agrements` like '%,".$this->quote($agrement).",%' OR  consultation.`agrements` like '".
                        $this->quote($agrement).",%' OR  consultation.`agrements` like '%,".$this->quote($agrement)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        //jointure inner join on Organisme
        if (true === $this->criteriaVo->getcalledFromPortail() && $this->criteriaVo->getTypeOrganisme()) {
            $otherConditions .= " AND Organisme.categorie_insee='".$this->quote($this->criteriaVo->getTypeOrganisme())."'";
        }

        /*
        // @todo : Classe a reprendre !!!!
        //$referenceByReferentiel = Atexo_Referentiel_Referentiel::searchConsultation($this->criteriaVo->getReferentielVo(), $this->criteriaVo->getAcronymeOrganisme());
         */
        if ($referenceByReferentiel) {
            $otherConditions .= ' AND (consultation.id  IN ( '.implode(',', $referenceByReferentiel).' ) )';
        }

        // les alertes metiers
        if (0 != $this->criteriaVo->getIdAlerteConsultation() &&
            0 != $this->criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND alerte_metier.cloturee ='".
                $this->quote($this->criteriaVo->getAlerteConsultationCloturee())."' ";
            $otherConditions .= " AND alerte_metier.id_alerte ='".
                $this->quote($this->criteriaVo->getIdAlerteConsultation())."'";
        } elseif (0 == $this->criteriaVo->getIdAlerteConsultation() &&
            2 == $this->criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND consultation.alerte='1' ";
        }

        // Les clauses achat responsable
        if ($this->isEnabled->getConsultationClause()) {
            $clausesN1In = [];
            $clausesN1NotIn = [];
            $clausesN3In = [];
            $clausesN3NotIn = [];

            if ('' != $this->criteriaVo->getClauseEnv()) {
                $this->criteriaVo->getClauseEnv() == 1 ? ($clausesN1In[] = 2) : ($clausesN1NotIn[] = 2);
            }
            if (0 != $this->criteriaVo->getClauseSociale()) {
                $this->criteriaVo->getClauseSociale() == 1 ? ($clausesN1In[] = 1) : ($clausesN1NotIn[] = 1);
            }
            if (0 != $this->criteriaVo->getAtelierProtege()) {
                $this->criteriaVo->getAtelierProtege() == 1 ? ($clausesN3In[] = 7) : ($clausesN3NotIn[] = 7);
            }
            if (0 != $this->criteriaVo->getSiae()) {
                $this->criteriaVo->getSiae() == 1 ? ($clausesN3In[] = 8) : ($clausesN3NotIn[] = 8);
            }
            if (0 != $this->criteriaVo->getEss()) {
                $this->criteriaVo->getEss() == 1 ? ($clausesN3In[] = 9) : ($clausesN3NotIn[] = 9);
            }
            if ('' != $this->criteriaVo->getSocialeCommerceEquitable()) {
                $this->criteriaVo->getSocialeCommerceEquitable() == 1 ? ($clausesN3In[] = 4) : ($clausesN3NotIn[] = 4);
            }
            if (0 != $this->criteriaVo->getSocialeInsertionActiviterEconomique()) {
                $this->criteriaVo->getSocialeInsertionActiviterEconomique() == 1 ? ($clausesN3In[] = 1) : ($clausesN3NotIn[] = 1);
            }

            if (!empty($clausesN3In)) {
                $otherConditions .= " AND consultation.id in (
                            select consultation_id from CategorieLot where CategorieLot.id in (
                                select lot_id from consultation_clauses_n1 where consultation_clauses_n1.lot_id is not null AND consultation_clauses_n1.id in (
                                    select clause_n1_id from consultation_clauses_n2 where consultation_clauses_n2.id in (
                                        select clause_n2_id from consultation_clauses_n3 where referentiel_clause_n3_id in (:clauses_n3)
                                        )
                                    )
                                )
                            ) OR consultation.id in (
                                select consultation_id from consultation_clauses_n1 where consultation_id is not null AND consultation_clauses_n1.id in (
                                    select clause_n1_id from consultation_clauses_n2 where consultation_clauses_n2.id in (
                                        select clause_n2_id from consultation_clauses_n3 where referentiel_clause_n3_id in (:clauses_n3)
                                        )
                                    )
                            )
                            ";
                $listeClausesN3 = implode(", ", $clausesN3In);
                $this->queryParams[':clauses_n3'] = $listeClausesN3;
            }
            if (!empty($clausesN1In)) {
                $otherConditions .= " AND consultation.id in (
                            select consultation_id from CategorieLot where CategorieLot.id in (
                                select lot_id from consultation_clauses_n1 where consultation_clauses_n1.lot_id is not null AND referentiel_clause_n1_id in (:clauses_n1)
                                )
                            ) OR consultation.id in (
                                select consultation_id from consultation_clauses_n1 where consultation_id is not null AND referentiel_clause_n1_id in (:clauses_n1)
                            )
                            ";
                $listeClausesN1 = implode(", ", $clausesN1In);
                $this->queryParams[':clauses_n1'] = $listeClausesN1;
            }
            if (!empty($clausesN3NotIn)) {
                $otherConditions .= " AND consultation.id not in (
                            select consultation_id from CategorieLot where CategorieLot.id in (
                                select lot_id from consultation_clauses_n1 where consultation_clauses_n1.lot_id is not null AND consultation_clauses_n1.id in (
                                    select clause_n1_id from consultation_clauses_n2 where consultation_clauses_n2.id in (
                                        select clause_n2_id from consultation_clauses_n3 where referentiel_clause_n3_id in (:clauses_not_n3)
                                        )
                                    )
                                )
                            ) AND consultation.id not in (
                                select consultation_id from consultation_clauses_n1 where consultation_id is not null AND consultation_clauses_n1.id in (
                                    select clause_n1_id from consultation_clauses_n2 where consultation_clauses_n2.id in (
                                        select clause_n2_id from consultation_clauses_n3 where referentiel_clause_n3_id in (:clauses_not_n3)
                                        )
                                    )
                            )
                            ";
                $listeClausesN3 = implode(", ", $clausesN3NotIn);
                $this->queryParams[':clauses_not_n3'] = $listeClausesN3;
            }
            if (!empty($clausesN1NotIn)) {
                $otherConditions .= " AND consultation.id not in (
                            select consultation_id from CategorieLot where CategorieLot.id in (
                                select lot_id from consultation_clauses_n1 where consultation_clauses_n1.lot_id is not null AND referentiel_clause_n1_id in (:clauses_not_n1)
                                )
                            ) AND consultation.id not in (
                                select consultation_id from consultation_clauses_n1 where consultation_id is not null AND referentiel_clause_n1_id in (:clauses_not_n1)
                            )
                            ";
                $listeClausesN1 = implode(", ", $clausesN1NotIn);
                $this->queryParams[':clauses_not_n1'] = $listeClausesN1;
            }
        }

        //Recherche dans le panier de l'entreprise
        if (true === $this->criteriaVo->getcalledFromPortail() && $this->criteriaVo->getForPanierEntreprise()) {
            $arrayIdConsPanier = $this->searchPanier();

            $conditionForPanier = ' AND (0 ';
            if (!empty($arrayIdConsPanier)) {
                foreach ($arrayIdConsPanier as $org => $oneRef) {
                    $listeRefCons = implode("', '", $oneRef);
                    $listeRefCons = "('".$listeRefCons."')";
                    $conditionForPanier .= " OR (consultation.organisme = '".
                        $this->quote($org)."' AND consultation.id  IN ".$listeRefCons.') ';
                }
            }
            $conditionForPanier .= ' )';
            $otherConditions .= $conditionForPanier;
        }

        //jointure inner join on Organisme
        if ($this->criteriaVo->getExclureConsultationExterne()) {
            $otherConditions .= " AND (consultation.ref_org_partenaire is NULL OR  consultation.ref_org_partenaire = '')";
            $otherConditions .= ' AND Organisme.id_client_ANM!=:id_client_ANM';
            $this->queryParams[':id_client_ANM'] = 1;
        }

        if (false !== $this->criteriaVo->getAfficherDoublon()) {
            $otherConditions .= ' AND consultation.doublon=:doublon';
            $this->queryParams[':doublon'] = $this->criteriaVo->getAfficherDoublon();
        }

        if (true == $this->criteriaVo->getPublieHier()) {
            $date = strftime(
                '%Y-%m-%d %H:%M:%S',
                mktime(0, 0, 0, date('m'), date('d') - 1, date('y'))
            );
            $otherConditions .= " AND date_mise_en_ligne_calcule >= '".$date."' ";
        }

        if (true == $this->criteriaVo->getInformationMarche()) {
            $otherConditions .= " AND (id_type_avis = '".
                $this->container->getParameter('TYPE_AVIS_INFORMATION')."' Or id_type_avis = '".
                $this->container->getParameter('TYPE_AVIS_CONSULTATION')."' )";
        }

        if (true == $this->criteriaVo->getWithDce()) {
            $otherConditions .= ' AND consultation.id  IN ( SELECT distinct(DCE.consultation_id) FROM DCE, consultation WHERE DCE.organisme=consultation.organisme'
                ." AND DCE.consultation_id = consultation.id  AND DCE.dce != '0')";
        }

        if (true == $this->criteriaVo->getConsultaionPublier()) {
            $otherConditions .= " AND (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' "
                .' AND date_mise_en_ligne_calcule<=now()) ';
        }
        if (true == $this->criteriaVo->getConsultationAnnulee()) {
            $otherConditions .= " AND consultation_annulee = '1'";
        }

        /**
         * @todo : A voir comment implémenté ca !!
         * Atexo_Module::isEnabled('gestionOperations',$this->criteriaVo->getAcronymeOrganisme())
         */
        if ($gestionOperations && $this->criteriaVo->getIdOperation()) {
            $otherConditions .= " AND idOperation = '".$this->criteriaVo->getIdOperation()."'";
        }

        //marche public simplifie
        //Nous n'avons pas mis le test Atexo_Module::isEnabled('MarchePublicSimplifie',
        // $this->criteriaVo->getAcronymeOrganisme()) car ce bloc est appele depuis un Web Service
        if ($this->criteriaVo->getMps()) {
            $otherConditions .= ' AND consultation.marche_public_simplifie=:marche_public_simplifie ';
            if ('1' === $this->criteriaVo->getMps()) {//Consultation taggué MPS
                $this->queryParams[':marche_public_simplifie'] = '1';
            } elseif ('2' === $this->criteriaVo->getMps()) {//Consultation non taggué MPS
                $this->queryParams[':marche_public_simplifie'] = '0';
            }
        }

        if ($this->criteriaVo->getBourseCotraitance()) {
            $idEntreprise = $this->criteriaVo->getIdEntreprise();
            $comp = ('1' === $this->criteriaVo->getBourseCotraitance()) ?
                ' IN ' : (('2' === $this->criteriaVo->getBourseCotraitance()) ? ' NOT IN ' : '');
            if ($comp) {
                $otherConditions .= <<<QUERY
 AND consultation.id  $comp
	(
		SELECT
			DISTINCT(t_bourse_cotraitance.reference_consultation)
		FROM
			t_bourse_cotraitance
		WHERE
		t_bourse_cotraitance.id_Entreprise = $idEntreprise
	)
QUERY;
            }
        }

        if ($this->criteriaVo->getVisionRma()) {
            if ('' != $this->criteriaVo->getIdServiceRma()) {
                $otherConditions .= ' AND ((consultation.service_id=:serviceId) OR  (consultation.service_associe_id=:serviceId))';
                $this->queryParams[':serviceId'] = $this->criteriaVo->getIdServiceRma();
            }
            if ($this->criteriaVo->getAcronymeOrganisme()) {
                $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
                $this->queryParams[':acronymeOrg'] = $this->criteriaVo->getAcronymeOrganisme();
            }
        }
        $orgs = $this->session->get('organismes_eligibles');
        if ($orgs) {
            $orgEligibles = implode("','", $orgs);
            $otherConditions .= " AND (consultation.organisme in ('".$orgEligibles."'))";
        }

        return $otherConditions;
    }

    /**
     * Only for entreprise search on place
     * display or not Tncp according to key word from an url
     *
     * @return string
     */
    public function addEntrepriseConsultationExterneCondition()
    {
        $consultationExterneCondition = "";
        if ($this->criteriaVo->getConsultationExterneFilter() && !$this->criteriaVo->getConsultationExterne()) {
            $consultationExterneCondition = " AND consultation.consultation_externe = :consultationExterieur ";
            $this->queryParams[':consultationExterieur'] = '0';
        }

        return $consultationExterneCondition;
    }

    /**
     * @return string
     */
    protected function stateConditions()
    {
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $stateCondition = ''; //si getEtatConsultation() vide alors => recupérer les consultations quelques soient leurs états
        if ('' !== $this->criteriaVo->getEtatConsultation() || '' !== $this->criteriaVo->getSecondConditionOnEtatConsultation()) {
            //si getEtatConsultation() égale à 'DECISION' alors => recupérer toutes les consultations avec un id_etat_consultation != 0 (c'est à dire qu'un agent a positionné la consultation à un état donné)
            $stateCondition .= $this->getSqlStatusDecision(
                $this->criteriaVo,
                $conditionEtatPreparation,
                $queryParams
            );
            /*if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_DECISION') ||
                $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_DECISION')) {
                $stateCondition .= " AND ( consultation.id_etat_consultation=:EtatConsultation OR ( depouillable_phase_consultation=:phaseSAD AND NOT (" . $conditionEtatPreparation . "))" . ")";
                $this->queryParams[":EtatConsultation"] = $this->container->getParameter('STATUS_DECISION');
                $this->queryParams[":phaseSAD"] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
            }*/

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_OUVERTURE_ANALYSE')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_OUVERTURE_ANALYSE')) {
                $stateCondition .= ' AND ( (consultation.id_etat_consultation=:EtatConsultation OR (consultation.id_etat_consultation=:Decision AND  ';
                $this->queryParams[':EtatConsultation'] = $this->container->getParameter('STATUS_OUVERTURE_ANALYSE');
                $this->queryParams[':Decision'] = '0';
                $this->queryParams[':phaseSAD'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
                $stateCondition .= " ( datefin <= now()  AND datefin != '0000-00-00 00:00:00')  ) )";
                $stateCondition .= ' OR ( depouillable_phase_consultation=:phaseSAD AND NOT ( '.$conditionEtatPreparation."  ) AND (datefin<=now() AND datefin != '0000-00-00 00:00:00') ) )";
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_CONSULTATION')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_CONSULTATION')) {
                if ($this->criteriaVo->getcalledFromPortail()) {
                    //utilise pour afficher le contenu synthetique d'une consultation cloturee
                    if ($this->criteriaVo->getAvecConsClotureesSansPoursuivreAffichage()) {
                        $stateCondition .= ' AND ( datefin < now() AND ( ';
                    } else {
                        if ('1' == $this->container->getParameter('ANNONCES_MARCHES') || $this->criteriaVo->getSansCritereActive()) {
                            $stateCondition .= ' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND ( ';
                        } elseif ($this->criteriaVo->getOrganismeTest()) {
                            $listOrganismeInactifs = (new CommonOrganismePeer())->retrieveAcronymeAllInactifsOrganismes();
                            $list = implode("','", $listOrganismeInactifs);
                            $stateCondition .= " AND organisme IN ('".$list."') AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND (consultation.id_etat_consultation='0' OR  consultation.poursuivre_affichage > 0) AND consultation_annulee='0' AND ( ";
                        } else {
                            $stateCondition .= " AND Organisme.active = '".$this->criteriaVo->getOrgActive()."' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND (consultation.id_etat_consultation='0' OR  consultation.poursuivre_affichage > 0) AND consultation_annulee='0' AND ( ";
                        }
                    }
                } else {
                    $stateCondition .= " AND ( datefin > now() AND consultation.id_etat_consultation='0' AND ( ";
                }
                if ('' != $this->criteriaVo->getPeriodicite()) {
                    $dateMinMiseEnligne = strftime('%Y-%m-%d %H:%M:%S', mktime(0, 0, 0, date('m'), date('d') - $this->criteriaVo->getPeriodicite(), date('y')));
                    $dateMaxMiseEnligne = date('Y-m-d');
                    $stateCondition .= " date_mise_en_ligne_calcule <'".$dateMaxMiseEnligne."' ";
                    $stateCondition .= " AND date_mise_en_ligne_calcule >=  '".$dateMinMiseEnligne."' ";
                    $stateCondition .= " AND date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00'";
                    $stateCondition .= ' ))';
                } else {
                    $stateCondition .= " (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' ";
                    $stateCondition .= ' AND date_mise_en_ligne_calcule<=now())) )';
                }
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_PREPARATION')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_PREPARATION')) {
                $stateCondition .= ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='1' ";
            }

            if ($this->criteriaVo->getEtatConsultation() === $this->container->getParameter('STATUS_ELABORATION')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_ELABORATION')) {
                $stateCondition .= ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='0' ";
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_A_ARCHIVER')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_A_ARCHIVER')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
                $this->queryParams[':EtatConsultation'] = $this->container->getParameter('STATUS_A_ARCHIVER');
                $this->queryParams[':notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_ARCHIVE_REALISEE')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_ARCHIVE_REALISEE')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
                $this->queryParams[':EtatConsultation'] = $this->container->getParameter('STATUS_ARCHIVE_REALISEE');
                $this->queryParams[':notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_APRES_DECISION')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_APRES_DECISION')) {
                $stateCondition .= ' AND (consultation.id_etat_consultation>=:EtatConsultation1 AND consultation.id_etat_consultation<=:EtatConsultation2) AND depouillable_phase_consultation=:notSad ';
                $this->queryParams[':EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
                $this->queryParams[':EtatConsultation2'] = $this->container->getParameter('STATUS_ARCHIVE_REALISEE');
                $this->queryParams[':notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_AVANT_ARCHIVE')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_AVANT_ARCHIVE')) {
                $stateCondition .= ' AND ( consultation.id_etat_consultation<=:EtatConsultation1 ) ';
                $this->queryParams[':EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
                if ($this->criteriaVo->getVisionRma()) {
                    $stateCondition .= " AND NOT ( $conditionEtatPreparation  ) ";
                }
            }

            if ($this->criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_JUSTE_DECISION')
                || $this->criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_JUSTE_DECISION')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation1 AND depouillable_phase_consultation=:notSad ';
                $this->queryParams[':EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
                $this->queryParams[':notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }
        }

        return $stateCondition;
    }

    /**
     * @param $queryForPole
     * @param $returnJustNumberOfElement
     * @param $returnReferences
     *
     * @return string
     */
    protected function getSqlTrie($queryForPole, $returnJustNumberOfElement, $returnReferences)
    {
        $finalQuery = $queryForPole;
        if (!$returnJustNumberOfElement && !$returnReferences) {
            if ($this->criteriaVo->getSortByElement()) {
                $finalQuery .= ' ORDER BY '.$this->criteriaVo->getSortByElement().' '.
                    $this->criteriaVo->getSensOrderBy().', ordre ASC '.
                    (('datefin' != strtolower($this->criteriaVo->getSortByElement())) ? ' , datefin DESC ' : ' ');
            } elseif ($this->criteriaVo->getOrganismeTest()) {
                $finalQuery .= ' ORDER BY datefin DESC ';
            } else {
                $finalQuery .= ' ORDER BY ordre ASC , datefin DESC ';
            }

            if ($this->criteriaVo->getLimit()) {
                $finalQuery .= ' LIMIT '.$this->criteriaVo->getOffset().','.$this->criteriaVo->getLimit();
            }
        }

        return $finalQuery;
    }

    /**
     * @param $motCle
     * @param $calledFromPortail
     * @param $searchChaqueMot
     *
     * @return string
     */
    protected function searchLikeMotcle($motCle, $calledFromPortail, $searchChaqueMot)
    {
        $queryWhere = ' reference_utilisateur LIKE :referUser ';
        $this->queryParams[':referUser'] = '%'.$this->quote($motCle).'%';
        if ($searchChaqueMot) {
            $arrayCar = ['«', '»', ',', ';', '.', '&', '!', '?', "'", '"', '(', ')', '[', ']', '{', '}', '°', '+', '*', '/', '\\', '|', ':', '%'];
            $motCle = str_replace($arrayCar, ' ', $motCle);
            $arrayMotsCle = explode(' ', (string) $motCle);
        } else {
            $arrayMotsCle = ['0' => $motCle];
        }


        $indice = 0;
        foreach ($arrayMotsCle as $unMotCle) {
            $unMotCle = html_entity_decode(trim($this->quote($unMotCle)));
            if ($unMotCle && !strstr(self::MOTS_CLES_INTERDITS, $unMotCle) && strlen($unMotCle) >= 2) {
                $queryWhere .= ' OR '
                    .' intitule like :unMotCle'.$indice
                    .' OR '
                    .' objet like :unMotCle'.$indice
                    .(($calledFromPortail) ? '' : ' OR champ_supp_invisible like :unMotCle'.$indice)
                    .' OR description LIKE :unMotCle'.$indice
                    .' OR description_detail LIKE :unMotCle'.$indice;
                $this->queryParams[':unMotCle'.$indice] = '%'.$unMotCle.'%';
                ++$indice;
            }
        }

        return $queryWhere;
    }

    /**
     * @param $chaine
     *
     * @return string
     */
    protected function quote($chaine)
    {
        return addslashes($chaine);
    }

    /**
     * @return array
     *
     * @throws DBALException
     */
    protected function searchPanier()
    {
        $params = [];
        $query = ' SELECT PE.consultation_id, PE.organisme ';
        $query .= ' FROM Panier_Entreprise PE ';
        if ($this->criteriaVo->getAvecRetrait()) {
            $query .= ' , Telechargement T ';
        }
        if ($this->criteriaVo->getAvecQuestion()) {
            $query .= ' , questions_dce Q ';
        }
        if ($this->criteriaVo->getAvecDepot()) {
            $query .= ' , Offres O ';
        }
        $query .= ' WHERE PE.id_inscrit = :idInscrit ';
        if ($this->criteriaVo->getAvecRetrait()) {
            $query .= ' AND PE.consultation_id = T.consultation_id AND PE.organisme = T.organisme AND PE.id_inscrit = T.id_inscrit ';
        }
        if ($this->criteriaVo->getAvecQuestion()) {
            $query .= ' AND PE.consultation_id = Q.consultation_id AND PE.organisme = Q.organisme AND PE.id_inscrit = Q.id_inscrit ';
        }
        if ($this->criteriaVo->getAvecDepot()) {
            $query .= ' AND PE.consultation_id = O.consultation_id AND PE.organisme = O.organisme AND PE.id_inscrit = O.inscrit_id ';
        }
        if ($this->criteriaVo->getAvecEchange()) {
            $queryMessV1 = '  PE.`consultation_id` IN ( SELECT E.consultation_id FROM Echange E, ';
            $queryMessV1 .= ' EchangeDestinataire ED, Inscrit I  WHERE PE.organisme = E.organisme  ';
            $queryMessV1 .= ' AND ( ED.mail_destinataire = I.email  OR E.email_expediteur = I.email )';
            $queryMessV1 .= '  AND I.id = PE.id_inscrit AND E.id = ED.id_echange AND E.organisme = ED.organisme ';
            $queryMessV1 .= '  ) ';
            $idsConsultation = $this->messagerieService
                ->getlistConsultationsPanierWithEchangeMessec2(
                    $this->criteriaVo->getIdInscrit(),
                    $this->criteriaVo->getEmailInscrit()
                );
            $queryMessV2 = '';
            if (!empty($idsConsultation) && is_array($idsConsultation)) {
                $listCons = "'".implode("','", $idsConsultation)."'";
                $queryMessV2 = " PE.consultation_id in ( $listCons )";
            }
            $query .= 'AND ( ('.$queryMessV1.') '.($queryMessV2 ? 'OR ( '.$queryMessV2.') ' : '').')';
        }
        if ($this->criteriaVo->getSansRetrait()) {
            $query .= ' AND PE.`consultation_id` NOT IN ( SELECT T.consultation_id FROM Telechargement T  ';
            $query .= ' WHERE T.`id_inscrit` =  PE.`id_inscrit` AND T.`organisme` =  PE.`organisme` ) ';
        }
        if ($this->criteriaVo->getSansQuestion()) {
            $query .= ' AND  PE.`consultation_id` NOT IN ( SELECT Q.consultation_id FROM questions_dce Q  ';
            $query .= ' WHERE Q.id_inscrit =  PE.`id_inscrit` AND Q.`organisme` =  PE.`organisme` ) ';
        }
        if ($this->criteriaVo->getSansDepot()) {
            $query .= ' AND  PE.`consultation_id` NOT IN ( SELECT O.consultation_id FROM Offres O  ';
            $query .= ' WHERE O.inscrit_id =  PE.`id_inscrit` AND O.`organisme` =  PE.`organisme` ) ';
        }
        if ($this->criteriaVo->getSansEchange()) {
            $queryMessV1 = '  PE.`consultation_id` NOT IN ( SELECT E.consultation_id FROM Echange E, ';
            $queryMessV1 .= ' EchangeDestinataire ED, Inscrit I  WHERE PE.organisme = E.organisme  ';
            $queryMessV1 .= ' AND ( ED.mail_destinataire = I.email  OR E.email_expediteur = I.email )';
            $queryMessV1 .= '  AND I.id = PE.id_inscrit AND E.id = ED.id_echange AND E.organisme = ED.organisme ';
            $queryMessV1 .= '  ) ';
            $idsConsultation = $this->messagerieService
                ->getlistConsultationsPanierWithEchangeMessec2(
                    $this->criteriaVo->getIdInscrit(),
                    $this->criteriaVo->getEmailInscrit()
                );
            $queryMessV2 = '';
            if (!empty($idsConsultation) && is_array($idsConsultation)) {
                $listCons = "'".implode("','", $idsConsultation)."'";
                $queryMessV2 = " PE.consultation_id not in ( $listCons )";
            }
            $query .= 'AND ( ('.$queryMessV1.') '.($queryMessV2 ? 'AND ( '.$queryMessV2.') ' : '').')';
        }

        $params['idInscrit'] = $this->criteriaVo->getIdInscrit();
        $stmt = $this->em->getConnection()->executeQuery($query, $params);
        try {
            $list = $stmt->fetchAllAssociative();
        } catch (Exception $e) {
            Prado::log(
                'Erreur PaniersEntreprises.php'.$e->getMessage().$e->getTraceAsString(),
                TLogger::ERROR,
                'PaniersEntreprises.php'
            );
        }
        $arrayResult = [];
        $arrayVerif = [];
        $i = 0;
        foreach ($list as $row) {
            $key = $row['consultation_id'].'_'.$row['organisme'];
            if (!in_array($key, $arrayVerif)) {
                $arrayVerif[] = $row['consultation_id'].'_'.$row['organisme'];
                $arrayResult[$i][$row['organisme']] = $row['consultation_id'];
            }
            ++$i;
        }
        $arrayFinal = [];
        if (is_array($arrayResult) && count($arrayResult)) {
            foreach ($arrayResult as $key => $result) {
                foreach ($result as $org => $ref) {
                    $arrayFinal[$org][] = $ref;
                }
            }
        }

        return $arrayFinal;
    }

    /** ********************************************************************
     *      Bloc Refonte Agent.
     *  ********************************************************************
     */

    /**
     * @param $queryParams
     *
     * @return string
     *
     * @throws Atexo_Consultation_Exception
     */
    public function otherConditionsAgent(Atexo_Consultation_CriteriaVo $criteriaVo, &$queryParams)
    {
        $texteRecherche = null;
        $this->isEnabled = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        $otherConditions = '';
        if ('tout' != $criteriaVo->getTypeSearch()) {
            if ($criteriaVo->getIdReference()) {
                $otherConditions .= ' AND consultation.id =:idReference';
                $queryParams['idReference'] = $criteriaVo->getIdReference();
            }
            if ($criteriaVo->getReferenceConsultation()) {
                $otherConditions .= ' AND consultation.reference_utilisateur LIKE :referUser ';
                $queryParams['referUser'] = '%'.$criteriaVo->getReferenceConsultation().'%';
            }
            if ($criteriaVo->getReferenceConsultationRestreinte()) {
                $otherConditions .= ' AND consultation.reference_utilisateur =:referUser ';
                $queryParams['referUser'] = $criteriaVo->getReferenceConsultationRestreinte();
            }

            // Debut du filtre des consultations à archivées
            $otherConditions .= $this->getSqlConsultationAArchiver($criteriaVo, $queryParams);
            // Fin du filtre des consultations à archivées

            if (Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
                $idsService = $criteriaVo->getIdsService();

                if ('' != $idsService) {
                    if (is_null(Atexo_CurrentUser::getUserSf()->getServiceId())) {
                        $otherConditions .= " AND (consultation.service_id is null
                         OR consultation.service_id IN {$idsService} )";
                    } else {
                        $otherConditions .= ' AND (consultation.service_id IN '.$idsService.' )';
                    }
                }
            }

            if ($criteriaVo->getDateFinStart()) {
                $otherConditions .= ' AND consultation.datefin >= :dateFinStart ';
                $queryParams['dateFinStart'] = $this->serviceUtile
                        ->frnDate2iso($criteriaVo->getDateFinStart()).' 00:00:00';
            }

            if ($criteriaVo->getDateFinEnd()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $queryParams['dateFinEnd'] = $this->serviceUtile
                        ->frnDate2iso($criteriaVo->getDateFinEnd()).' 23:59:59';
            }

            if ($criteriaVo->getDateTimeFin()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $queryParams['dateFinEnd'] = $criteriaVo->getDateTimeFin();
            }

            $cpvConditions = '';
            if ($criteriaVo->getIdCodeCpv2()) {
                $codesCPV = explode('#', (string) $criteriaVo->getIdCodeCpv2());
                if (is_array($codesCPV) && $codesCPV) {
                    $indexCodeCpv = 0;
                    foreach ($codesCPV as $cpv) {
                        //si jamais le code CPV est codé sur 10 car. on enlève les 2 derniers. ex : 30000000-0
                        if (10 == strlen($cpv)) {
                            $cpv = substr($cpv, 0, -2);
                        }
                        if ($cpv) {
                            $cpvConditions .= <<<QUERY

OR consultation.code_cpv_1 like :codeCpv1$indexCodeCpv
OR consultation.code_cpv_2 like :codeCpv$indexCodeCpv

OR CategorieLot.code_cpv_1 like :codeCpv1$indexCodeCpv
OR CategorieLot.code_cpv_2 like :codeCpv$indexCodeCpv
QUERY;
                            $queryParams['codeCpv'.$indexCodeCpv] = '%#'.rtrim($cpv, '0').'%';
                            $queryParams['codeCpv1'.$indexCodeCpv] = rtrim($cpv, '0').'%';
                        }
                        ++$indexCodeCpv;
                    }
                }
            }
            if ($cpvConditions) {
                $otherConditions .= " AND (0 $cpvConditions)";
            }

            if ($criteriaVo->getCategorieConsultation()) {
                $otherConditions .= ' AND consultation.categorie=:categorie ';
                $queryParams['categorie'] = $criteriaVo->getCategorieConsultation();
            }
            if ($criteriaVo->getIdTypeProcedure()) {
                if ($criteriaVo->getCalledFromHelios() || false === $criteriaVo->getcalledFromPortail()) {
                    $otherConditions .= ' AND consultation.id_type_procedure_org=:idtypeProc ';
                    $queryParams['idtypeProc'] = $criteriaVo->getIdTypeProcedure();
                } else {
                    $otherConditions .= ' AND consultation.id_type_procedure=:idtypeProc ';
                    $queryParams['idtypeProc'] = $criteriaVo->getIdTypeProcedure();
                }
            }

            if ($this->isEnabled->getLieuxExecution()) {
                if ($criteriaVo->getLieuxexecution()) {
                    $arrayLieuExecution = explode(',', (string) $criteriaVo->getLieuxexecution());
                    $indiceLieuExecution = 1;
                    $lieuExecQuery = '';
                    foreach ($arrayLieuExecution as $unLieuExec) {
                        if ($unLieuExec) {
                            $lieuExecQuery .= ((1 == $indiceLieuExecution) ? '' : ' OR ').
                                ' consultation.lieu_execution LIKE :lieuExec'.
                                $indiceLieuExecution.' ';
                            $queryParams['lieuExec'.$indiceLieuExecution] = '%,'.$unLieuExec.',%';
                            ++$indiceLieuExecution;
                        }
                    }
                    $otherConditions .= ($lieuExecQuery) ? ' AND ('.$lieuExecQuery.')' : '';
                }
            }
            //codes nuts
            if ($this->isEnabled->getCodeNutLtReferentiel()) {
                $codesNutsQuery = '';
                if ($criteriaVo->getCodesNuts()) {
                    $listeCodesNuts = explode('#', (string) $criteriaVo->getCodesNuts());
                    if ($listeCodesNuts[1]) {
                        $indiceCodesNuts = 1;
                        foreach ($listeCodesNuts as $codesNuts) {
                            if ($codesNuts) {
                                //Recherche des descendants complets
                                $codesNutsQuery .= ((1 == $indiceCodesNuts) ? '' : ' OR ').
                                    ' consultation.codes_nuts LIKE :codesNuts'.$indiceCodesNuts.' ';
                                $queryParams['codesNuts'.$indiceCodesNuts] = '%#'.$codesNuts.'%';

                                $indiceCodesNutsAsc = 1;
                                //Recherche des ascendants directs
                                while (strlen((string) $codesNuts) > $this->container->getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_CODE_NUT')) {
                                    //Lorsque l'ascendant cherché se trouve entre 2 codes
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscMedium'.
                                        $indiceCodesNuts.
                                        $indiceCodesNutsAsc.' ';
                                    $queryParams['codesNutsAscMedium'.$indiceCodesNuts.
                                    $indiceCodesNutsAsc] = '%#'.$this->supprimerDernierCaractere($codesNuts).'#%';

                                    //Lorsque l'ascendant cherché se trouve au à l'extremité, ce cas inclus lorsque l'ascendant direct est le seul selectionné
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscLast'.
                                        $indiceCodesNuts.$indiceCodesNutsAsc.' ';
                                    $queryParams['codesNutsAscLast'.$indiceCodesNuts.$indiceCodesNutsAsc] = '%#'.
                                        $this->supprimerDernierCaractere($codesNuts);

                                    $codesNuts = $this->supprimerDernierCaractere($codesNuts);
                                    ++$indiceCodesNutsAsc;
                                }

                                ++$indiceCodesNuts;
                            }
                        }
                        if ($codesNutsQuery) {
                            $otherConditions .= ' AND ('.$codesNutsQuery.')';
                        }
                    }
                }
            }
            //fin codes nuts
        }

        if ($criteriaVo->getTypeAcces() == $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            $otherConditions .= ' AND consultation.type_acces=:typeProc ';
            $queryParams['typeProc'] = $criteriaVo->getTypeAcces();
            $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
            $queryParams['acronymeOrg'] = $criteriaVo->getAcronymeOrganismeProcRest();
            $otherConditions .= ' AND consultation.code_procedure=:codeProc ';
            $queryParams['codeProc'] = $criteriaVo->getCodeAccesProcedureRestreinte();
        }

        if ($criteriaVo->getIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis=:typeAvis ';
            $queryParams['typeAvis'] = $criteriaVo->getIdTypeAvis();
        }

        if ($criteriaVo->getNotIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis!=:notTypeAvis ';
            $queryParams['notTypeAvis'] = $criteriaVo->getNotIdTypeAvis();
        }

        if ($criteriaVo->getPublicationEurope()) {
            $otherConditions .= ' AND publication_europe=:publicationEurop ';
            $queryParams['publicationEurop'] = $criteriaVo->getPublicationEurope();
        }
        //Début recherche date mise en ligne calculée
        if ($criteriaVo->getEnLigneDepuis()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule>=:dateMiseEnLigneCalculeStar';
            $queryParams['dateMiseEnLigneCalculeStar'] = $this->serviceUtile
                    ->frnDate2iso($criteriaVo->getEnLigneDepuis()).' 00:00:00';
        }
        if ($criteriaVo->getEnLigneJusquau()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule<=:dateMiseEnLigneCalculeEnd ';
            $queryParams['dateMiseEnLigneCalculeEnd'] = $this->serviceUtile
                    ->frnDate2iso($criteriaVo->getEnLigneJusquau()).' 23:59:59';
        }
        //Fin recherche date mise en ligne calculée

        if ($criteriaVo->getEtatPublicationToBePublished() || $criteriaVo->getEtatPublicationPublished()
            || $criteriaVo->getEtatPublicationToBeUpdated() || $criteriaVo->getEtatPublicationUpdated()) {
            $etatPublicationCondition = '';
            if ($criteriaVo->getEtatPublicationToBePublished()) {
                $etatPublicationCondition = (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".
                    $this->container->getParameter('TO_BE_PUBLISHED')."' ";
            }
            if ($criteriaVo->getEtatPublicationPublished()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".
                    $this->container->getParameter('PUBLISHED')."' ";
            }
            if ($criteriaVo->getEtatPublicationToBeUpdated()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".
                    $this->container->getParameter('TO_BE_UPDATED')."' ";
            }
            if ($criteriaVo->getEtatPublicationUpdated()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".
                    $this->container->getParameter('UPDATED')."' ";
            }
            if ($etatPublicationCondition) {
                $otherConditions .= ' AND ('.$etatPublicationCondition.' ) ';
            }
        }

        // Recherche Key Word Advanced Search
        $rechercheExacte = false;
        $rechercheApproximative = false;
        $searchChaqueMot = true;
        if ($criteriaVo->getKeyWordAdvancedSearch()) {
            $texteRecherche = str_replace('&quot;', '"', $criteriaVo->getKeyWordAdvancedSearch());
            $keyWord = strtolower($texteRecherche);

            $arrayWorrd = explode('"', (string) $texteRecherche);
            if ($criteriaVo->getSearchModeExact()) {
                if (3 == count($arrayWorrd)) {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                }
                $rechercheExacte = true;
            } else {
                if (3 != count($arrayWorrd)) {
                    $rechercheApproximative = true;
                    $precision = $this->container->getParameter('PRECISION_LUCENE');
                    if (false === $criteriaVo->getcalledFromPortail()) {
                        $organisme = $criteriaVo->getAcronymeOrganisme();
                        if ($criteriaVo->getFromArchive()) {
                            $pathDirIndexLucene = $this->container->getParameter('BASE_ROOT_DIR').$organisme.'/'.
                                $this->container->getParameter('DIR_INDEX_LUCENE_ARCHIVE');
                        } else {
                            $pathDirIndexLucene = $this->container->getParameter('BASE_ROOT_DIR').$organisme.'/'.
                                $this->container->getParameter('DIR_INDEX_LUCENE_AGENT');
                        }
                        $searchLucene = new Atexo_Consultation_SearchLucene($pathDirIndexLucene);
                        $arrayIdToFilter = $searchLucene->searchByOrganisme(
                            $criteriaVo->getAcronymeOrganisme(),
                            $keyWord,
                            $precision
                        );
                        if (!empty($arrayIdToFilter)) {
                            $strIdToFilter = implode("', '", $arrayIdToFilter);
                            $strIdToFilter = "('".$strIdToFilter."')";
                        } else {
                            $strIdToFilter = '(0)';
                        }
                        // Que les résultats de Lucene :
                        $otherConditions .= " AND (consultation.id  IN $strIdToFilter ";
                    } else {
                        $searchLucene = new Atexo_Consultation_SearchLucene($this->container
                            ->getParameter('DIR_INDEX_LUCENE_PORTAIL'));
                        $arrayIdToFilter = $searchLucene->searchInAllOrganisme($keyWord, $precision);
                        if (!empty($arrayIdToFilter)) {
                            $otherConditions .= ' AND ((0 ';
                            foreach ($arrayIdToFilter as $org => $oneRef) {
                                $strIdToFilter = implode("', '", $oneRef);
                                $strIdToFilter = "('".$strIdToFilter."')";
                                $otherConditions .= " OR (consultation.organisme = '".$org.
                                    "' AND consultation.id IN ".$strIdToFilter.') ';
                            }
                            $otherConditions .= ' )';
                        } else {
                            // Si Lucene ne retourne aucuns reusltats :
                            $otherConditions .= ' AND (0 ';
                        }
                    }
                } else {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                    $rechercheExacte = true;
                }
            }
        }

        if ($rechercheExacte || $rechercheApproximative || $criteriaVo->getKeyWordRechercheRapide()) {
            if ($criteriaVo->getKeyWordRechercheRapide()) {
                $texteRecherche = $criteriaVo->getKeyWordRechercheRapide();
            }

            if ($texteRecherche) {
                $querySelectKeyWord = $this->searchLikeMotcleAgent(
                    $texteRecherche,
                    $criteriaVo->getcalledFromPortail(),
                    $searchChaqueMot,
                    $queryParams
                );
                $otherConditions .= ' '.($rechercheApproximative ? ' OR ' : '  AND').' ( '.' 0 '.
                    (($querySelectKeyWord) ? ' OR '.$querySelectKeyWord : '').' ) '.
                    ($rechercheApproximative ? ')' : '');
            }
        }
        // Fin

        if ($this->isEnabled->getConsultationDomainesActivites()) {
            if ($criteriaVo->getDomaineActivite()) {
                $domainesActivites = explode('#', trim($criteriaVo->getDomaineActivite(), '#'));
                if (is_array($domainesActivites) && count($domainesActivites) >= 1) {
                    $conditions = [];
                    foreach ($domainesActivites as $activite) {
                        $conditions[] = "( consultation.`domaines_activites` like '%#".(new Atexo_Db())->quote($activite).
                            "%' OR  consultation.`domaines_activites` like '".(new Atexo_Db())->quote($activite)."%' )";
                    }
                    $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
                }
            }
        }

        // Domaines d'activités Lt-Référentiel
        if ($this->isEnabled->getConsultationDomainesActivitesLtReferentiel()) {
            $domainesActivitesLtRef = '';
            if ($criteriaVo->getDomaineActivite()) {
                $listeDomainesActivitesLtRef = explode('#', (string) $criteriaVo->getDomaineActivite());
                if ($listeDomainesActivitesLtRef[1]) {
                    $indiceDomainesActivitesLtRef = 1;
                    foreach ($listeDomainesActivitesLtRef as $domainesActivites) {
                        if ($domainesActivites) {
                            $domainesActivitesLtRef .= ((1 == $indiceDomainesActivitesLtRef) ? '' : ' OR ').
                                ' consultation.domaines_activites LIKE :domaines_activites'.
                                $indiceDomainesActivitesLtRef.' ';
                            $queryParams['domaines_activites'.$indiceDomainesActivitesLtRef] = '%#'.
                                rtrim($domainesActivites, '0').'%';
                            ++$indiceDomainesActivitesLtRef;
                        }
                    }
                    if ($domainesActivitesLtRef) {
                        $otherConditions .= ' AND ('.$domainesActivitesLtRef.')';
                    }
                }
            }
        }

        if ($criteriaVo->getQualification()) {
            $qualifications = explode('#', trim($criteriaVo->getQualification(), '#'));
            if (is_array($qualifications) && count($qualifications) >= 1) {
                $conditions = [];
                foreach ($qualifications as $qualification) {
                    $conditions[] = "(  consultation.`qualification` like '%#".(new Atexo_Db())->quote($qualification).
                        "#%' OR  consultation.`qualification` like '".(new Atexo_Db())->quote($qualification)
                        ."#%' OR  consultation.`qualification` like '%#".(new Atexo_Db())->quote($qualification)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        if ($criteriaVo->getAgrements()) {
            $agrements = explode(',', trim($criteriaVo->getAgrements(), ','));
            if (is_array($agrements) && count($agrements) >= 1) {
                $conditions = [];
                foreach ($agrements as $agrement) {
                    $conditions[] = "(  consultation.`agrements` like '%,".(new Atexo_Db())->quote($agrement).
                        ",%' OR  consultation.`agrements` like '".
                        (new Atexo_Db())->quote($agrement).",%' OR  consultation.`agrements` like '%,".
                        (new Atexo_Db())->quote($agrement)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        //jointure inner join on Organisme
        /*if ($criteriaVo->getcalledFromPortail() === true && $criteriaVo->getTypeOrganisme()) {
            $otherConditions .= " AND Organisme.categorie_insee='" .
                Atexo_Db::quote($criteriaVo->getTypeOrganisme()) . "'";
        }*/

        $referenceByReferentiel = Atexo_Referentiel_Referentiel::searchConsultation(
            $criteriaVo->getReferentielVo(),
            $criteriaVo->getAcronymeOrganisme()
        );
        if ($referenceByReferentiel) {
            $otherConditions .= ' AND (consultation.id IN ( '.
                implode(',', $referenceByReferentiel).' ) )';
        }
//      // les alertes metiers

        if (0 != $criteriaVo->getIdAlerteConsultation() && 0 != $criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND alerte_metier.cloturee ='".
                (new Atexo_Db())->quote($criteriaVo->getAlerteConsultationCloturee())."' ";
            $otherConditions .= " AND alerte_metier.id_alerte ='".
                (new Atexo_Db())->quote($criteriaVo->getIdAlerteConsultation())."'";
        } elseif (0 == $criteriaVo->getIdAlerteConsultation() && 2 == $criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND consultation.alerte='1' ";
        }

        // Les clauses achat responsable
        if ($this->isEnabled->getConsultationClause()) {
            $hasConditionCC = false;
            $arrayCond = [];

            if (0 != $criteriaVo->getClauseSociale()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale='".$criteriaVo->getClauseSociale()."'";
            }

            if (0 != $criteriaVo->getAtelierProtege()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_ateliers_proteges='".
                    ($criteriaVo->getAtelierProtege() == $this->container
                        ->getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getSiae()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_siae='".
                    ($criteriaVo->getSiae() == $this->container
                        ->getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getEss()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_ess='".
                    ($criteriaVo->getEss() == $this->container
                        ->getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getClauseEnv()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_environnementale='".$criteriaVo->getClauseEnv()."'";
            }

            if ('' != $criteriaVo->getSocialeCommerceEquitable()) {
                $not = '';
                if (2 == $criteriaVo->getSocialeCommerceEquitable()) {
                    $not = ' NOT ';
                }

                $otherConditions .= <<<QUERY
AND
(
	( 
	    consultation.clause_specification_technique $not like '%:1:"4"%' 
	    OR consultation.clause_sociale_condition_execution $not like '%:1:"4"%' 
	    OR consultation.clause_sociale_insertion $not like '%:1:"4"%'
	)
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( catLot.clause_specification_technique $not like '%:1:"4"%' 
	    OR catLot.clause_sociale_condition_execution $not like '%:1:"4"%' 
	    OR catLot.clause_sociale_insertion $not like '%:1:"4"%')
	)<>0
)
QUERY;
            }

            if ('' != $criteriaVo->getSocialeInsertionActiviterEconomique()) {
                $not = '';
                if (2 == $criteriaVo->getSocialeInsertionActiviterEconomique()) {
                    $not = ' NOT ';
                }
                $otherConditions .= <<<QUERY
AND
(
	( 
	    consultation.clause_specification_technique $not like '%:1:"1"%' 
	    OR consultation.clause_sociale_condition_execution $not like '%:1:"1"%' 
	    OR consultation.clause_sociale_insertion $not like '%:1:"1"%'
	)
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( catLot.clause_specification_technique $not like '%:1:"1"%' 
	    OR catLot.clause_sociale_condition_execution $not like '%:1:"1"%' 
	    OR catLot.clause_sociale_insertion $not like '%:1:"1"%')
	)<>0
)
QUERY;
            }

            if ($hasConditionCC) {
                $conditionCCConsultation = 'consultation.'.implode(' AND consultation.', $arrayCond);
                $conditionCCLots = 'catLot.'.implode(' AND catLot.', $arrayCond);
                $otherConditions .= <<<QUERY

AND
(
	( $conditionCCConsultation )
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( $conditionCCLots )
	)<>0
)
QUERY;
            }
        }
        //Recherche dans le panier de l'entreprise
        /*if ($criteriaVo->getcalledFromPortail() === true && $criteriaVo->getForPanierEntreprise()) {
            $arrayIdConsPanier = Atexo_Entreprise_PaniersEntreprises::searchPanier($criteriaVo);

            $conditionForPanier = " AND (0 ";
            if (!empty($arrayIdConsPanier)) {
                foreach ($arrayIdConsPanier as $org => $oneRef) {
                    $listeRefCons = implode("', '", $oneRef);
                    $listeRefCons = "('" . $listeRefCons . "')";
                    $conditionForPanier .= " OR (consultation.organisme = '" . Atexo_Db::quote($org) .
                        "' AND consultation.id IN " . $listeRefCons . ") ";
                }
            }
            $conditionForPanier .= " )";
            $otherConditions .= $conditionForPanier;
        }*/
        //jointure inner join on Organisme
        if ($criteriaVo->getExclureConsultationExterne()) {
            $otherConditions .= " AND (consultation.ref_org_partenaire is NULL OR  consultation.ref_org_partenaire = '')";
            $otherConditions .= ' AND Organisme.id_client_ANM!=:id_client_ANM';
            $queryParams['id_client_ANM'] = 1;
        }

        if (false !== $criteriaVo->getAfficherDoublon()) {
            $otherConditions .= ' AND consultation.doublon=:doublon';
            $queryParams['doublon'] = $criteriaVo->getAfficherDoublon();
        }

        if (true == $criteriaVo->getPublieHier()) {
            $date = strftime(
                '%Y-%m-%d %H:%M:%S',
                mktime(0, 0, 0, date('m'), date('d') - 1, date('y'))
            );
            $otherConditions .= " AND date_mise_en_ligne_calcule >= '".$date."' ";
        }

        if (true == $criteriaVo->getInformationMarche()) {
            $otherConditions .= " AND (id_type_avis = '".
                $this->container->getParameter('TYPE_AVIS_INFORMATION')."' Or id_type_avis = '".
                $this->container->getParameter('TYPE_AVIS_CONSULTATION')."' )";
        }

        if (true == $criteriaVo->getWithDce()) {
            $otherConditions .= ' AND consultation.id IN ( SELECT distinct(DCE.consultation_id) FROM DCE, consultation WHERE DCE.organisme=consultation.organisme'
                ." AND DCE.consultation_id = consultation.id AND DCE.dce != '0')";
        }

        if (true == $criteriaVo->getConsultaionPublier()) {
            $otherConditions .= " AND (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' "
                .' AND date_mise_en_ligne_calcule<=now()) ';
        }

        if (true == $criteriaVo->getConsultationAnnulee()) {
            $otherConditions .= " AND consultation_annulee = '1'";
        }

        if (Atexo_Module::isEnabled('gestionOperations', $criteriaVo->getAcronymeOrganisme())) {
            if ($criteriaVo->getIdOperation()) {
                $otherConditions .= " AND idOperation = '".$criteriaVo->getIdOperation()."'";
            }
        }

        //marche public simplifie
        //Nous n'avons pas mis le test
        // Atexo_Module::isEnabled('MarchePublicSimplifie', $criteriaVo->getAcronymeOrganisme())
        // car ce bloc est appele depuis un Web Service
        if ($criteriaVo->getMps()) {
            $otherConditions .= ' AND consultation.marche_public_simplifie=:marche_public_simplifie ';
            if ('1' === $criteriaVo->getMps()) {//Consultation taggué MPS
                $queryParams['marche_public_simplifie'] = '1';
            } elseif ('2' === $criteriaVo->getMps()) {//Consultation non taggué MPS
                $queryParams['marche_public_simplifie'] = '0';
            }
        }

        if ($criteriaVo->getBourseCotraitance()) {
            $idEntreprise = $criteriaVo->getIdEntreprise();
            $comp = ('1' === $criteriaVo->getBourseCotraitance()) ? ' IN ' : (('2' === $criteriaVo->getBourseCotraitance()) ? ' NOT IN ' : '');
            if ($comp) {
                $otherConditions .= <<<QUERY
 AND consultation.id $comp
	(
		SELECT
			DISTINCT(t_bourse_cotraitance.reference_consultation)
		FROM
			t_bourse_cotraitance
		WHERE
		t_bourse_cotraitance.id_Entreprise = $idEntreprise
	)
QUERY;
            }
        }

        if ($criteriaVo->getVisionRma()) {
            if ('' != $criteriaVo->getIdServiceRma()) {
                $otherConditions .= ' AND ((consultation.service_id=:serviceId) OR  (consultation.service_associe_id=:serviceId))';
                $queryParams['serviceId'] = $criteriaVo->getIdServiceRma();
            }
            if ($criteriaVo->getAcronymeOrganisme()) {
                $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
                $queryParams['acronymeOrg'] = $criteriaVo->getAcronymeOrganisme();
            }
        }

        $orgs = $this->session->get('organismes_eligibles');
        if ($orgs) {
            $orgEligibles = implode("','", $orgs);
            $otherConditions .= " AND (consultation.organisme in ('".$orgEligibles."'))";
        }

        return $otherConditions;
    }

    /**
     * @param $criteriaVo
     * @param $container
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlConsultationAArchiver($criteriaVo, &$queryParams)
    {
        $otherConditions = '';
        if ($criteriaVo->getConsultationAArchiver()) {
            $queryParams['duree_rappel_archivage'] = $this->container->getParameter('DUREE_RAPPEL_ARCHIVAGE');
            $queryParams['status_decision'] = $this->container->getParameter('STATUS_DECISION');
            $queryParams['type_avis_consultation'] = $this->container->getParameter('TYPE_AVIS_CONSULTATION');

            //Exclure les consultations SAD  AC
            $exclusionIdSadAc = $this->getSqlExclureConsultationsSadAc();

            //Condition Préparation
            $conditionPreparation = $this->getSqlConditionPreparation();

            //Condition Ouverture et Analyse
            $conditionOuvertureAnalyse = $this->getSqlConditionOuvertureAnalyse();

            //Condition Decision
            $conditionDecision = $this->getSqlConditionDecision();

            //Condition Commun
            $conditionCommun = <<<QUERY

DEPOUILLABLE_PHASE_CONSULTATION = '0'
AND ID_TYPE_AVIS = :type_avis_consultation
$exclusionIdSadAc
QUERY;

            $otherConditions .= <<<QUERY
 AND ( $conditionCommun  ) AND (  $conditionPreparation  $conditionOuvertureAnalyse  $conditionDecision  )
QUERY;
        }

        return $otherConditions;
    }

    public function getSqlExclureConsultationsSadAc()
    {
        return <<<QUERY

AND id_type_procedure_org NOT IN (
SELECT
	Type_Procedure_Organisme.id_type_procedure
FROM
	Type_Procedure_Organisme
WHERE
	Type_Procedure_Organisme.organisme = :organismeAgent
	AND (( Type_Procedure_Organisme.sad = '1') OR (Type_Procedure_Organisme.accord_cadre ='1'))

)
QUERY;
    }

    public function getSqlConditionPreparation()
    {
        return <<<QUERY

 (
ID_ETAT_CONSULTATION = 0
AND ( DATEFIN > CURDATE() OR DATEFIN = '0000-00-00 00:00:00')
AND (
	 DATE_MISE_EN_LIGNE_CALCULE = ''
	 OR DATE_MISE_EN_LIGNE_CALCULE = '0000-00-00 00:00:00'
	 OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()
	 OR DATE_MISE_EN_LIGNE_CALCULE IS NULL
)
AND DATEDEBUT != '' AND DATEDEBUT != '0000-00-00 00:00:00'
AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH )
 )
QUERY;
    }

    public function getSqlConditionOuvertureAnalyse()
    {
        return <<<QUERY

OR (
	ID_ETAT_CONSULTATION = 0 AND DATEFIN != ''
	AND DATEFIN != '0000-00-00 00:00:00'
	AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH)
)
QUERY;
    }

    public function getSqlConditionDecision()
    {
        return <<<QUERY

OR (
	ID_ETAT_CONSULTATION = :status_decision
	AND  DATE_DECISION != ''
	AND DATE_DECISION != '0000-00-00 00:00:00'
	AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL :duree_rappel_archivage MONTH)
 )
QUERY;
    }

    /**
     * @param $criteriaVo
     * @param $queryParams
     * @param $container
     *
     * @return string
     */
    public function stateConditionsAgent($criteriaVo, &$queryParams)
    {
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $stateCondition = ''; //si getEtatConsultation() vide alors => recupérer les consultations quelques soient leurs états
        if ('' !== $criteriaVo->getEtatConsultation() || '' !== $criteriaVo->getSecondConditionOnEtatConsultation()) {
            //si getEtatConsultation() égale à 'DECISION' alors => recupérer toutes les consultations avec un
            // id_etat_consultation != 0 (c'est à dire qu'un agent a positionné la consultation à un état donné)
            $stateCondition .= $this->getSqlStatusDecision($criteriaVo, $conditionEtatPreparation, $queryParams);
            $stateCondition .= $this->getSqlStatusOuvertureAnalyse($criteriaVo, $conditionEtatPreparation, $queryParams);
            $stateCondition .= $this->getSqlStatusConsultation($criteriaVo);
            $stateCondition .= $this->getSqlStatusPreparation($criteriaVo, $conditionEtatPreparation);
            $stateCondition .= $this->getSqlStatusElaboration($criteriaVo, $conditionEtatPreparation);
            $stateCondition .= $this->getSqlStatusAArchiver($criteriaVo, $queryParams);
            $stateCondition .= $this->getSqlStatusArchiveRealisee($criteriaVo, $queryParams);
            $stateCondition .= $this->getSqlStatusApresDecision($criteriaVo, $queryParams);
            $stateCondition .= $this->getSqlStatusAvantArchive($criteriaVo, $conditionEtatPreparation, $queryParams);
            $stateCondition .= $this->getSqlStatusJusteDecision($criteriaVo, $queryParams);
        }

        return $stateCondition;
    }

    /**
     * @param $chaine
     */
    public function supprimerDernierCaractere($chaine): bool|string
    {
        if (strlen((string) $chaine) > $this->container->getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_CODE_NUT')) {
            $newChaine = substr($chaine, 0, -1);
        } else {
            $newChaine = $chaine;
        }

        return $newChaine;
    }

    /**
     * @param $criteriaVo
     * @param $stateCondition
     * @param $conditionEtatPreparation
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusDecision($criteriaVo, $conditionEtatPreparation, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_DECISION') ||
            $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_DECISION')) {
            $stateCondition = ' AND ( consultation.id_etat_consultation=:EtatConsultation OR ( depouillable_phase_consultation=:phaseSAD AND NOT ('.
                $conditionEtatPreparation.'))'.')';
            $queryParams['EtatConsultation'] = $this->container->getParameter('STATUS_DECISION');
            $queryParams['phaseSAD'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $conditionEtatPreparation
     *
     * @return string
     */
    public function getSqlStatusOuvertureAnalyse($criteriaVo, $conditionEtatPreparation, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_OUVERTURE_ANALYSE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_OUVERTURE_ANALYSE')) {
            $queryParams['EtatConsultation'] = $this->container->getParameter('STATUS_OUVERTURE_ANALYSE');
            $queryParams['Decision'] = '0';
            $queryParams['phaseSAD'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
            $stateCondition = ' AND ( (consultation.id_etat_consultation=:EtatConsultation OR (consultation.id_etat_consultation=:Decision AND  ';
            $stateCondition .= " ( datefin <= now()  AND datefin != '0000-00-00 00:00:00')  ) )";
            $stateCondition .= ' OR ( depouillable_phase_consultation=:phaseSAD AND NOT ( '.$conditionEtatPreparation."  ) AND (datefin<=now() AND datefin != '0000-00-00 00:00:00') ) )";
        }

        return $stateCondition;
    }

    /**
     * STATUS_CONSULTATION.
     *
     * @param $criteriaVo
     *
     * @return string
     */
    public function getSqlStatusConsultation($criteriaVo)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_CONSULTATION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_CONSULTATION')) {
            if ($criteriaVo->getcalledFromPortail()) {
                //utilise pour afficher le contenu synthetique d'une consultation cloturee
                if ($criteriaVo->getAvecConsClotureesSansPoursuivreAffichage()) {
                    $stateCondition .= ' AND ( datefin < now() AND ( ';
                } else {
                    if ('1' == $this->container->getParameter('ANNONCES_MARCHES') || $criteriaVo->getSansCritereActive()) {
                        $stateCondition .= ' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND ( ';
                    } elseif ($criteriaVo->getOrganismeTest()) {
                        $listOrganismeInactifs = (new CommonOrganismePeer())->retrieveAcronymeAllInactifsOrganismes();
                        $list = implode("','", $listOrganismeInactifs);
                        $stateCondition .= " AND organisme IN ('".$list."') AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND consultation.id_etat_consultation='0' AND consultation_annulee='0' AND ( ";
                    } else {
                        $stateCondition .= " AND Organisme.active = '".$criteriaVo->getOrgActive()."' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND consultation.id_etat_consultation='0' AND consultation_annulee='0' AND ( ";
                    }
                }
            } else {
                $stateCondition .= " AND ( datefin > now() AND consultation.id_etat_consultation='0' AND ( ";
            }

            if ('' != $criteriaVo->getPeriodicite()) {
                $dateMinMiseEnligne = date('Y-m-d H:i:s', mktime(0, 0, 0, date('m'), date('d') - $criteriaVo->getPeriodicite(), date('y')));
                $dateMaxMiseEnligne = date('Y-m-d');
                $stateCondition .= " date_mise_en_ligne_calcule <'".$dateMaxMiseEnligne."' ";
                $stateCondition .= " AND date_mise_en_ligne_calcule >=  '".$dateMinMiseEnligne."' ";
                $stateCondition .= " AND date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00'";
                $stateCondition .= ' ))';
            } else {
                $stateCondition .= " (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' ";
                $stateCondition .= ' AND date_mise_en_ligne_calcule<=now())) )';
            }
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $conditionEtatPreparation
     *
     * @return string
     */
    public function getSqlStatusPreparation($criteriaVo, $conditionEtatPreparation)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_PREPARATION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_PREPARATION')) {
            $stateCondition = ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='1' ";
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $conditionEtatPreparation
     *
     * @return string
     */
    public function getSqlStatusElaboration($criteriaVo, $conditionEtatPreparation)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() === $this->container->getParameter('STATUS_ELABORATION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_ELABORATION')) {
            $stateCondition = ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='0' ";
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusAArchiver($criteriaVo, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_A_ARCHIVER')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_A_ARCHIVER')) {
            $stateCondition = ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
            $queryParams['EtatConsultation'] = $this->container->getParameter('STATUS_A_ARCHIVER');
            $queryParams['notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusArchiveRealisee($criteriaVo, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_ARCHIVE_REALISEE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_ARCHIVE_REALISEE')) {
            $stateCondition = ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
            $queryParams['EtatConsultation'] = $this->container->getParameter('STATUS_ARCHIVE_REALISEE');
            $queryParams['notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusApresDecision($criteriaVo, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_APRES_DECISION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_APRES_DECISION')) {
            $stateCondition = ' AND (consultation.id_etat_consultation>=:EtatConsultation1 AND consultation.id_etat_consultation<=:EtatConsultation2) AND depouillable_phase_consultation=:notSad ';
            $queryParams['EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
            $queryParams['EtatConsultation2'] = $this->container->getParameter('STATUS_ARCHIVE_REALISEE');
            $queryParams['notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $conditionEtatPreparation
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusAvantArchive($criteriaVo, $conditionEtatPreparation, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_AVANT_ARCHIVE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_AVANT_ARCHIVE')) {
            $stateCondition = ' AND ( consultation.id_etat_consultation<=:EtatConsultation1 ) ';
            $queryParams['EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
            if ($criteriaVo->getVisionRma()) {
                $stateCondition .= " AND NOT ( $conditionEtatPreparation  ) ";
            }
        }

        return $stateCondition;
    }

    /**
     * @param $criteriaVo
     * @param $queryParams
     *
     * @return string
     */
    public function getSqlStatusJusteDecision($criteriaVo, &$queryParams)
    {
        $stateCondition = '';
        if ($criteriaVo->getEtatConsultation() == $this->container->getParameter('STATUS_JUSTE_DECISION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == $this->container->getParameter('STATUS_JUSTE_DECISION')) {
            $stateCondition = ' AND consultation.id_etat_consultation=:EtatConsultation1 AND depouillable_phase_consultation=:notSad ';
            $queryParams['EtatConsultation1'] = $this->container->getParameter('STATUS_DECISION');
            $queryParams['notSad'] = $this->container->getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
        }

        return $stateCondition;
    }

    public function searchLikeMotcleAgent($motCle, $calledFromPortail, $searchChaqueMot, &$queryParams)
    {
        $queryWhere = ' reference_utilisateur LIKE :referUser ';
        $queryParams['referUser'] = '%'.(new Atexo_Db())->quote($motCle).'%';
        if ($searchChaqueMot) {
            $arrayCar = ['«', '»', ',', ';', '.', '&', '!', '?', "'", '"', '(', ')', '[', ']', '{', '}', '°', '+', '*', '/', '\\', '|', ':', '%'];
            $motCle = str_replace($arrayCar, ' ', $motCle);
            $arrayMotsCle = explode(' ', (string) $motCle);
        } else {
            $arrayMotsCle = ['0' => $motCle];
        }

        $indice = 0;
        foreach ($arrayMotsCle as $unMotCle) {
            $unMotCle = trim($this->quote($unMotCle));
            if ($unMotCle && !strstr(self::MOTS_CLES_INTERDITS, $unMotCle) && strlen($unMotCle) >= 2) {
                $queryWhere .= ' OR '
                    .' intitule like :unMotCle'.$indice
                    .' OR '
                    .' objet like :unMotCle'.$indice
                    .(($calledFromPortail) ? '' : ' OR champ_supp_invisible like :unMotCle'.$indice)
                    .' OR description LIKE :unMotCle'.$indice
                    .' OR description_detail LIKE :unMotCle'.$indice;
                $queryParams['unMotCle'.$indice] = '%'.$unMotCle.'%';
                ++$indice;
            }
        }

        return $queryWhere;
    }

    /**
    * Permet de supprimer les caracteres et les mots interdits dans une chaine
    **/
    public function searchOrgDenominationLikeMotcle(string $inputText, array &$queryParams): string
    {
        $words = [];
        $keywords = $this->serviceUtile->deleteSpecialCharacteresFromChaine($inputText);
        foreach ($keywords as $keyword) {
            if ($keyword && !strstr(self::MOTS_CLES_INTERDITS, (string) $keyword) && strlen((string) $keyword) >= 2) {
                $words[] = $keyword;
            }
        }

        if (empty($words)) {
            return '';
        }

        $chaineDenominationOrg = implode('%', $words);
        $queryWhere = ' AND consultation.organisme in (SELECT acronyme FROM Organisme WHERE denomination_org like :orgDenomination)';
        $queryParams['orgDenomination'] = '%' . $this->quote($chaineDenominationOrg) . '%';

        return $queryWhere;
    }
}
