<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Spaser;

class SpaserUtils
{
    public const OBJET_METIER_TYPE_CONSULTATION = 'Consultation';
    public const OBJET_METIER_TYPE_CONTRAT = 'ContratTitulaire';
}
