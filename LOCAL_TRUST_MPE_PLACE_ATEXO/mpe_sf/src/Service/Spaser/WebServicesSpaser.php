<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Spaser;

use App\Entity\Agent;
use App\Entity\ContratTitulaire;
use App\Service\AbstractService;
use App\Entity\Consultation;
use Exception;
use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesSpaser extends AbstractService
{
    final protected const CLIENT_ID = 'spaser-api';
    final protected const SPASER_KEY_REDIRECT_URL = 'redirectUrl';
    final protected const SPASER_KEY_RESULT = 'result';
    final protected const API_PATH_NAME = 'api';

    final public const TYPE_ROUTE_SAISIE_INDICATEUR = 'indicateur';
    final public const TYPE_ROUTE_SAISIE_INDICATEUR_TRANSVERSE = 'indicateur-transverse';
    final public const TYPE_ROUTE_ADMIN_SPASER = 'administration';
    final public const TYPE_ROUTE_DASHBOARD = 'dashboard';

    final protected const CONTEXT_ENDPOINT = 'authorization-contexts/init';
    final protected const INDICATEUR_PROGRESS_ENDPOINT = 'indicateurs-progress';

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     */
    protected function getSpaserAuthUrl(Agent $agent, string $typeRoute, ?object $objetMetier = null): string
    {
        $this->logger->info("Début appel WS Spaser");
        $response = $this->getAuthResponse();

        $token = $response[AbstractService::ACCESS_TOKEN];
        $refreshToken = $response[AbstractService::REFRESH_TOKEN];

        $objetMetierType = $objetMetier ? (new \ReflectionClass($objetMetier))->getShortName() : null;

        /** @var Consultation|ContratTitulaire $objetMetier */
        return sprintf(
            "%s%s",
            $this->parameterBag->get('URL_SPASER'),
            $this->initSpaserContext($agent, $objetMetier, $typeRoute, $token, $refreshToken, $objetMetierType)
        );
    }

    protected function getAuthResponse(): string|array|null
    {
        $this->logger->info("Récupération du token avec le WS Identity");

        return $this->getAuthenticationTokenResponse(
            self::OPEN_ID_SPASER,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD'),
        );
    }

    /**
     * @param Agent $agent
     * @param object|null $objetMetier
     * @param string $typeRoute
     * @param string $token
     * @param string $refreshToken
     * @param string|null $objetMetierType
     * @return string
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function initSpaserContext(
        Agent $agent,
        ?object $objetMetier,
        string $typeRoute,
        string $token,
        string $refreshToken,
        ?string $objetMetierType = null,
    ): string {
        $contextEndpoint = sprintf(
            "%s%s/%s",
            $this->parameterBag->get('URL_SPASER'),
            self::API_PATH_NAME,
            self::CONTEXT_ENDPOINT,
        );

        $dateFin = null;
        if ($objetMetier instanceof Consultation && $objetMetier->getDateFin()) {
            $dateFin = $objetMetier->getDateFin()->format('Y-m-d h:i:s');
        }
        if ($objetMetier instanceof ContratTitulaire && $objetMetier->getDateAttribution()) {
            $dateFin = $objetMetier->getDateAttribution()->format('Y-m-d h:i:s');
        }

        /** @var Consultation|ContratTitulaire $objetMetier */
        $body = [
            "token" => $token,
            "refreshToken" => $refreshToken,
            "agentId" => $agent->getId(),
            "plateformUid" => $this->parameterBag->get('UID_PF_MPE'),
            "organisme" => $agent->getOrganisme()?->getAcronyme(),
            "context" => [
                "objet_metier_type" => $objetMetierType,
                "objet_metier_external_id" => $objetMetier?->getId(),
                "objet_metier_date_fin" => $dateFin,
                "agent_firstname" => $agent->getPrenom(),
                "agent_lastname" => $agent->getNom(),
                "type_route" => $typeRoute,
            ]
        ];

        $this->logger->info(
            sprintf(
                "Appel au WS Spaser pour récupérer l'url de redirection (indicateur|dashboard|admin): %s",
                $contextEndpoint
            )
        );

        try {
            $response = $this->request(
                Request::METHOD_POST,
                $contextEndpoint,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'body' => json_encode($body, JSON_THROW_ON_ERROR)
                ]
            );
        } catch (Exception $exception) {
            $this->logger->error(
                sprintf(
                    "Erreur lors de l'appel au WS Spaser lors de la création du contexte %s %s",
                    $contextEndpoint,
                    $exception->getMessage()
                )
            );

            return '';
        }

        if (
            null === $response
            || Response::HTTP_CREATED !== $response->getStatusCode()
        ) {
            $this->logger->error(
                sprintf(
                    "Echec lors de la récupération du lien de redirection via le WS spaser pour le endpoint %s",
                    $contextEndpoint
                )
            );

            return '';
        }

        $this->logger->info(
            sprintf(
                "Appel au WS réussi pour le endpoint %s",
                $contextEndpoint
            )
        );

        $contentValue = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        return $contentValue[self::SPASER_KEY_REDIRECT_URL] ?? '';
    }


    /**
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws TransportExceptionInterface
     */
    protected function getSaisieIndicateurProgress(
        Agent $agent,
        string $objetMetierType,
        array $objetMetierList,
    ): array {
        $this->logger->info("Début appel WS Spaser pour récupérer la progression de la saisie des indicateurs.");
        $response = $this->getAuthResponse();

        $token = $response[AbstractService::ACCESS_TOKEN];
        $refreshToken = $response[AbstractService::REFRESH_TOKEN];

        // Initialiser spaser
        $this->initSpaserContext(
            $agent,
            null,
            self::TYPE_ROUTE_SAISIE_INDICATEUR,
            $token,
            $refreshToken,
            $objetMetierType,
        );

        /** @var Consultation|ContratTitulaire $objetMetier */
        return $this->getSpaserIndicateurProgress(
            $agent,
            $objetMetierType,
            $objetMetierList,
            $token,
            $refreshToken
        );
    }

    protected function getSpaserIndicateurProgress(
        Agent $agent,
        string $objetMetierType,
        array $objetMetierList,
        string $token,
        string $refreshToken,
    ): array {
        $contextEndpoint = sprintf(
            "%s%s/%s",
            $this->parameterBag->get('URL_SPASER'),
            self::API_PATH_NAME,
            self::INDICATEUR_PROGRESS_ENDPOINT,
        );
        $this->logger->info(
            sprintf(
                "Début appel WS Spaser de la progression des saisies indicateurs de type: %s, endpoint: %s",
                $objetMetierType,
                $contextEndpoint,
            )
        );

        $body = [
            "token" => $token,
            "refreshToken" => $refreshToken,
            "agentId" => $agent->getId(),
            "plateformUid" => $this->parameterBag->get('UID_PF_MPE'),
            "organisme" => $agent->getOrganisme()?->getAcronyme(),
            "objetMetierType" => $objetMetierType,
            "collection" => $objetMetierList
        ];

        $this->logger->info(
            sprintf(
                "Appel au WS Spaser pour récupérer l'avancement des saisies indicateur de %s: %s",
                $objetMetierType,
                $contextEndpoint
            )
        );

        try {
            $response = $this->request(
                Request::METHOD_GET,
                $contextEndpoint,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $token,
                        'RefreshToken' => $refreshToken
                    ],
                    'body' => json_encode($body, JSON_THROW_ON_ERROR)
                ]
            );
        } catch (Exception $exception) {
            $this->logger->error(
                sprintf(
                    "Erreur lors de l'appel au WS Spaser de récupération des progression des indicateurs %s %s",
                    $contextEndpoint,
                    $exception->getMessage()
                )
            );

            return [];
        }

        if (
            null === $response
            || Response::HTTP_OK !== $response->getStatusCode()
        ) {
            $this->logger->error(
                sprintf(
                    "Echec lors de la récupération des progression des saisies indicateurs pour le endpoint %s",
                    $contextEndpoint
                )
            );

            return [];
        }

        $this->logger->info(
            sprintf(
                "Appel au WS réussi pour le endpoint %s",
                $contextEndpoint
            )
        );

        $contentValue = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);

        return $contentValue[self::SPASER_KEY_RESULT] ?? [];
    }
}
