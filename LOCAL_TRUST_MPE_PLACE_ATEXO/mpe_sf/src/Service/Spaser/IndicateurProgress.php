<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Spaser;

use App\Service\AbstractService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Security;

class IndicateurProgress extends AbstractService
{
    public function __construct(
        private readonly WebServicesSpaser $webServicesSpaser,
        private readonly Security $security,
        private readonly LoggerInterface $logger,
    ) {
    }

    public function getIndicateurProgression(string $objetMetierType, array $objetMetierIds): array
    {
        $reponseWs = [];

        try {
            $reponseWs = $this->webServicesSpaser->getContent(
                'getSaisieIndicateurProgress',
                $this->security->getUser(),
                $objetMetierType,
                $objetMetierIds,
            );
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        return $reponseWs;
    }
}
