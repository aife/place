<?php

namespace App\Service;

use App\Entity\Consultation;
use App\Entity\TBourseCotraitance;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class TBourseCotraitanceService
{
    /**
     * OrganismeService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(private EntityManagerInterface $em, private CurrentUser $currentUser)
    {
    }

    public function getCasGroupement(Consultation $consultation)
    {
        $bourses = $this->em->getRepository(TBourseCotraitance::class)->findBy(['consultation' => $consultation]);

        if (empty($bourses)) {
            //aucun candidat inscrit
            $cas = 'cas1';
        } else {
            //cas par défaut
            $cas = 'cas3';

            if ($this->currentUser->isConnected() && $this->currentUser->isInscrit()) {
                foreach ($bourses as $bourse) {
                    if ($bourse->getEntreprise()->getId() == $this->currentUser->getIdEntreprise()) {
                        if (count($bourses) == 1) {
                            //l'utilisateur connecté est inscrit seul
                            $cas = 'cas2';
                        } else {
                            //l'utilisateur connecté fait parti des inscrits
                            $cas = 'cas4';
                        }
                        break;
                    }
                }
            }
        }

        return $cas;
    }

    public function gererActionBourseCotraitance(Consultation $consultation): ?string
    {
        $groupement = $this->getCasGroupement($consultation);
        switch ($groupement) {
            case 'cas1':
                return 'nouveau';
            case 'cas2':
                return 'inscrit';
            case 'cas3':
                return 'nouveau';
            case 'cas4':
                return 'inscrit';
            default:
                return null;
        }
    }
}
