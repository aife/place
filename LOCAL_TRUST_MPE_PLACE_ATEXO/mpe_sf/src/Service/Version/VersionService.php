<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Version;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class VersionService
{
    public final const VERSION_FILE_NAME = '.version';
    public final const VERSION_LABEL_KEY = 'version=';
    public final const VERSION_DATE_KEY = 'date=';
    public final const VERSION_BACK_TO_LINE = "\n";

    public function __construct(
        /** ParameterBagInterface */
        private readonly ParameterBagInterface $parameterBag,
        private readonly KernelInterface $kernel,
    )
    {
    }

    private function getVersionFilePath(): string
    {
        return $this->kernel->getProjectDir() . '/../' . self::VERSION_FILE_NAME;
    }

    private function getContentVersionFile(): array
    {
        return file($this->getVersionFilePath());
    }

    public function getApplicationVersion(): string
    {
        return
            str_replace([self::VERSION_LABEL_KEY, self::VERSION_BACK_TO_LINE], '', $this->getContentVersionFile()[0])
            ?:
            $this->parameterBag->get('MPE_VERSION')
            ;
    }

    public function getApplicationVersionDate(): string
    {
        return
            str_replace(self::VERSION_DATE_KEY, '', $this->getContentVersionFile()[1])
                ?:
                $this->parameterBag->get('MPE_VERSION')
            ;
    }
}
