<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Consultation;
use App\Entity\Offre;
use App\Utils\EntityPurchase;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class GestionRegistres implements GestionRegistresInterface
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityPurchase $purchase
    ) {
    }

    /**
     * @throws EntityNotFoundException
     */
    public function getAll(int $idConsultation): array
    {
        /**
         * @var Consultation $consultation
         */
        $consultation = $this->entityManager->getRepository(Consultation::class)->find($idConsultation);

        if (!$consultation instanceof Consultation) {
            throw new EntityNotFoundException(
                sprintf('La consultation id %d n\'existe pas', $idConsultation)
            );
        }

        return [
            'retraits' => count($consultation->getTelechargements()),
            'depots' => count($consultation->getOffres()->filter(
                fn(Offre $offre) => $offre->getStatutOffres() != $this->parameterBag->get('STATUT_ENV_BROUILLON')
            )),
            'questions' => count($consultation->getQuestions())
        ];
    }

    public function consultationHasValidationHabilitation(Consultation $consultation, CurrentUser $user): bool
    {
        if ($consultation->getDatefin() < date('Y-m-d H:i:s')
            || $consultation->getEtatEnAttenteValidation() === "0") {
            return false;
        }

        if ($consultation->getIdRegleValidation() === $this->parameterBag->get('REGLE_VALIDATION_TYPE_5')) {
            return (
                (!$consultation->getDateValidationIntermediaire()
                    || str_starts_with($consultation->getDateValidationIntermediaire(), '0000-00-00'))
                && $user->checkHabilitation('ValidationIntermediaire')
                )
                ||
                (
                    (!$consultation->getDatevalidation()
                        || str_starts_with($consultation->getDatevalidation(), '0000-00-00'))
                    && $consultation->getDateValidationIntermediaire()
                    && !str_starts_with($consultation->getDateValidationIntermediaire(), '0000-00-00')
                    && $user->checkHabilitation('ValidationFinale')
                )
                ;
        }

        if ($consultation->getIdRegleValidation() === $this->parameterBag->get('REGLE_VALIDATION_TYPE_4')) {
            return (
                    (!$consultation->getDateValidationIntermediaire()
                        || str_starts_with($consultation->getDateValidationIntermediaire(), '0000-00-00'))
                    && $user->checkHabilitation('ValidationIntermediaire')
            )
            ||
            (
                (!$consultation->getDatevalidation()
                        || str_starts_with($consultation->getDatevalidation(), '0000-00-00'))
                && $consultation->getDateValidationIntermediaire()
                && !str_starts_with($consultation->getDateValidationIntermediaire(), '0000-00-00')
                && $user->checkHabilitation('ValidationFinale')
                && $this->purchase->isEntityAbleToValidate(
                    $consultation->getServiceValidation(),
                    $user->getServiceId(),
                    $user->getOrganisme()
                )
            );
        }

        if ($consultation->getIdRegleValidation() === $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')) {
            return (!$consultation->getDatevalidation()
                    || str_starts_with($consultation->getDatevalidation(), '0000-00-00'))
                && $user->checkHabilitation('ValidationFinale')
                && $this->purchase->isEntityAbleToValidate(
                    $consultation->getServiceValidation(),
                    $user->getServiceId(),
                    $user->getOrganisme()
                );
        }

        if ($consultation->getIdRegleValidation() === $this->parameterBag->get('REGLE_VALIDATION_TYPE_2')) {
            return (!$consultation->getDatevalidation()
                    || str_starts_with($consultation->getDatevalidation(), '0000-00-00'))
                && $user->checkHabilitation('ValidationFinale')
            ;
        }

        return (!$consultation->getDatevalidation()
                || str_starts_with($consultation->getDatevalidation(), '0000-00-00'))
            && $user->checkHabilitation('ValidationSimple');
    }
}
