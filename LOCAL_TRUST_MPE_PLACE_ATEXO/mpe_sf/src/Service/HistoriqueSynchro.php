<?php

namespace App\Service;

use App\Entity\HistoriqueSynchronisationSgmap;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Classe pour gerer l'interface de synchronisation entre LT API et SGMAP.
 */
class HistoriqueSynchro
{
    /**
     * Initialise un objet HistoriqueSynchro.
     */
    public function __construct(protected EntityManagerInterface $em)
    {
    }

    /**
     * Permet d'enregistrer l'historique de synchronisation avec Api Gouv Entreprise.
     */
    public function enregistrerHistorique(string $typeObject, int $code, string $jeton, int $idObjet): void
    {
        $historique = new HistoriqueSynchronisationSgmap();
        $historique->setTypeObjet($typeObject);
        $historique->setCode($code);
        $historique->setJeton($jeton);
        $historique->setIdObjet($idObjet);
        $historique_repo = $this->em->getRepository(HistoriqueSynchronisationSgmap::class);
        $historique_repo->updateHistorique($historique);
    }
}
