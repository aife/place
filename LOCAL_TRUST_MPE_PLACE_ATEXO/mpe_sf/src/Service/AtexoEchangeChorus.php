<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\TypeContrat;
use App\Exception\ApiProblemForbiddenException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\ApiProblemValidationException;
use App\Repository\Chorus\ChorusEchangeRepository;
use App\Repository\Chorus\ChorusFicheModificativeRepository;
use App\Service\WebServices\WebServicesExec;
use Doctrine\Common\Annotations\AnnotationException;
use Exception;
use DateTime;
use App\Entity\Agent;
use App\Entity\Chorus\ChorusEchange;
use App\Entity\Chorus\ChorusFicheModificative;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Exception\ApiProblemInternalException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Service\Chorus\FicheModificative;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AtexoEchangeChorus
{
    private const SIZE_SIREN = 14;
    private const SIZE_CODE_APE = 5;

    /**
     * Constructeur de la classe
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @version 0
     * @since   0
     * @copyright Atexo 2015
     */
    public function __construct(
        protected ContainerInterface $container,
        protected EntityManagerInterface $em,
        protected ParameterBagInterface $parameterBag,
        protected ContratService $contratService,
        protected FicheModificative $ficheModificativeService,
        protected WebServicesExec $webServicesExec,
        protected ChorusEchangeRepository $chorusEchangeRepository,
        protected ChorusFicheModificativeRepository $ficheModificativeRepository
    ) {
    }

    /**
     * @param array $arrayInfochorusEchange
     * @return ChorusEchange
     * @throws ExceptionInterface
     * @throws AnnotationException
     * @throws \JsonException
     * @throws \ReflectionException
     */
    public function createEntity(array $arrayInfochorusEchange): ChorusEchange
    {
        $agent = null;
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $contrat = $this->webServicesExec->getContent(
                'getContratByUuid',
                $arrayInfochorusEchange['uuidContrat']
            );

            if (array_key_exists('agent', $arrayInfochorusEchange)) {
                $agent = $serializer->denormalize($arrayInfochorusEchange['agent'], Agent::class);
                $arrayIdExterne = explode("_", $agent->getIdExterne());
                $agent->setId(end($arrayIdExterne));
            }

            if (array_key_exists('uuidContrat', $arrayInfochorusEchange)) {
                $lastEchange = $this->em->getRepository(ChorusEchange::class)->findOneBy(
                    [
                        'uuidExterneExec' => $arrayInfochorusEchange['uuidContrat']
                    ],
                    ['id' => 'DESC']
                );
            } else {
                throw new ApiProblemInternalException();
            }

            if (!empty($this->chorusEchangeRepository->getPendingEchangeByContrat($arrayInfochorusEchange['uuidContrat']))) {
                throw new ApiProblemValidationException(
                    sprintf(
                        'Un échange en cours de traitement côte CHORUS pour ce contrat %s.',
                        $arrayInfochorusEchange['uuidContrat']
                    )
                );
            }

            if (
                !empty($this->chorusEchangeRepository
                ->getFailedEchangeByContrat($arrayInfochorusEchange['uuidContrat']))
            ) {
                throw new ApiProblemValidationException(
                    sprintf(
                        'Un échange est en statut brouillon ou en erreur de génération pour ce contrat %s.',
                        $arrayInfochorusEchange['uuidContrat']
                    )
                );
            }
            $chorusEchange = $this->createOrUpdateEchangeChorus($arrayInfochorusEchange);
            $chorusEchange = $this->setDonneesContrat($chorusEchange, $contrat);
            $chorusEchange = $this->setDataFromLastEchange($chorusEchange, $lastEchange);
            $chorusEchange = $this->setDataCalcul($arrayInfochorusEchange, $chorusEchange, $lastEchange, $contrat);

            $chorusEchange->setNomCreateur($agent->getNom());
            $chorusEchange->setPrenomCreateur($agent->getPrenom());
            $chorusEchange->setIdCreateur($agent->getId());

            $chorusEchange->setIdsBlobEnv('');
            $this->em->beginTransaction();
            $this->em->persist($chorusEchange);
            $this->em->flush();

            if (array_key_exists('acte', $arrayInfochorusEchange)) {
                $chorusFicheModificative = $this->setChorusFicheModificative($arrayInfochorusEchange, $chorusEchange);
                $this->ficheModificativeService->generatePdfFicheModificative(
                    $chorusFicheModificative,
                    $contrat['numEj'] ?? '',
                    $contrat['referenceLibre'] ?? '',
                    $contrat['montant'] ?? '',
                    $contrat['id']
                );
            }

            $this->updateStatus($chorusEchange);

            $this->em->flush();
            $this->em->commit();

            return $chorusEchange;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @return array|string|string[]
     * @throws ExceptionInterface
     */
    public function getNode(ChorusEchange $chorusEchange, Serializer $serializer, string $mode): array|string
    {
        $chorusEchangeNormalize = $serializer->normalize($chorusEchange, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($chorusEchangeNormalize, $mode);

        if ($mode == 'xml') {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace("response>", 'EchangeChorus>', $content);
        }


        return $content;
    }

    public function validateInfo(array $arrayInfoChorusEchange, bool $formUpdate = false): array
    {
        if (isset($arrayInfoChorusEchange['uuidContrat'])) {
            $lastEchange = $this->em->getRepository(ChorusEchange::class)->findOneBy(
                [
                    'uuidExterneExec' => $arrayInfoChorusEchange['uuidContrat']
                ],
                ['id' => 'DESC']
            );
            if ($lastEchange instanceof ChorusEchange) {
                $arrayFluxPossiblePourExec = [
                    $this->parameterBag->get('TYPE_FLUX_CHORUS_FEN111'),
                    $this->parameterBag->get('TYPE_FLUX_CHORUS_FEN211')
                ];
                if (
                    !in_array($lastEchange->getTypeFlux(), $arrayFluxPossiblePourExec)
                    || !in_array($lastEchange->getTypeFluxAEnvoyer(), $arrayFluxPossiblePourExec)
                ) {
                    throw new ApiProblemInvalidArgumentException("Flux non autorisé");
                }
            } else {
                throw new ApiProblemInvalidArgumentException("Aucun echange initial");
            }
        } else {
            throw new ApiProblemInvalidArgumentException("uuidContrat n'existe pas dans le xml");
        }
        return [];
    }

    /**
     * @param $arrayInfochorusEchange
     * @param ChorusEchange $chorusEchange
     * @param array $contrat
     * @return ChorusEchange
     */
    private function setDonneesContrat(ChorusEchange $chorusEchange, array $contrat): ChorusEchange
    {
        if (!$contrat) {
            throw new ApiProblemInternalException();
        }
        $chorusEchange->setIdDecision($contrat['id']);
        $chorusEchange->setOrganisme($contrat['organisme']);

        $titulaire = $this->em->getRepository(Entreprise::class)->find($contrat['contactTitulaire']['idEntreprise']);
        if ($titulaire) {
            $chorusEchange->setCodePaysTitulaire($titulaire->getPaysenregistrement());
            $chorusEchange->setNumeroSirenTitulaire($titulaire->getSiren());
            $chorusEchange->setRaisonSocialeAttributaire($titulaire->getNom());
            $chorusEchange->setFormeJuridique($titulaire->getFormejuridique());
            $chorusEchange->setPme(
                $titulaire->getCategorieEntreprise() === ContratTitulaire::CATEGORIE_ENTREPRISE_PME
            );
            $chorusEchange->setPaysTerritoire($titulaire->getPays());
            $chorusEchange->setNumeroNationalAttributaire($titulaire->getSirenetranger());
            $chorusEchange->setCodeApe($this->getCodeApe(trim($titulaire->getCodeape())));
        }

        $etabTitulaire = $this->em->getRepository(Etablissement::class)->find($contrat['idEtablissementTitulaire']);
        if ($etabTitulaire) {
            $chorusEchange->setNumeroSiretTitulaire($etabTitulaire->getCodeEtablissement());
        }

        if ($titulaire && $etabTitulaire) {
            $chorusEchange->setSiretAttributaire(
                $this->getSiretAttributaire($titulaire->getSiren(), $etabTitulaire->getCodeEtablissement())
            );
        }
        $chorusEchange->setDateNotification(
            isset($contrat['datePrevisionnelleNotification']) ?
                date_format(date_create($contrat['datePrevisionnelleNotification']), 'Y-m-d')
                : null
        );
        $chorusEchange->setDateFinMarche(
            isset($contrat['datePrevisionnelleFinMarche']) ?
                date_format(date_create($contrat['datePrevisionnelleFinMarche']), 'Y-m-d')
                : null
        );
        $chorusEchange->setDateNotificationReelle(
            isset($contrat['dateNotification']) ?
                date_format(date_create($contrat['dateNotification']), 'Y-m-d')
                : null
        );
        $chorusEchange->setDateFinMarcheReelle(
            isset($contrat['dateFin']) ?
                date_format(date_create($contrat['dateFin']), 'Y-m-d')
                : null
        );

        $chorusEchange->setCpv1(isset($contrat['cpv']['codePrincipal']) ? $contrat['cpv']['codePrincipal'] : '');
        $chorusEchange->setCpv2(isset($contrat['cpv']['codeSecondaire1']) ? $contrat['cpv']['codeSecondaire1'] : '');

        $chorusEchange->setObjetContrat($contrat['objet']);
        $chorusEchange->setIntituleContrat($contrat['intitule']);
        $chorusEchange->setMontantHt($contrat['montant']);

        $chorusEchange->setNomAgent($contrat['createur']['nom']);
        $chorusEchange->setPrenomAgent($contrat['createur']['prenom']);

        return $chorusEchange;
    }

    private function updateStatus($chorusEchange): void
    {
        $chorusEchange->setStatutEchange($this->parameterBag->get('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));
    }

    public function setDataFromLastEchange(ChorusEchange $chorusEchange, $lastEchange): ChorusEchange
    {
        if ($lastEchange instanceof ChorusEchange) {
            $chorusEchange->setTypeContrat($lastEchange->getTypeContrat());
            $chorusEchange->setIdOa($lastEchange->getIdOa());
            $chorusEchange->setIdGa($lastEchange->getIdGa());
            $chorusEchange->setIdTypeMarche($lastEchange->getIdTypeMarche());
            $chorusEchange->setIdTypeGroupement($lastEchange->getIdTypeGroupement());
            $chorusEchange->setIdRegroupementComptable($lastEchange->getIdRegroupementComptable());
            $chorusEchange->setSiren($lastEchange->getSiren());
            $chorusEchange->setSiret($lastEchange->getSiret());
            $chorusEchange->setIdActeJuridique($lastEchange->getIdActeJuridique());
            $chorusEchange->setCpv1($lastEchange->getCpv1());
            $chorusEchange->setCpv2($lastEchange->getCpv2());
            $chorusEchange->setCodeCpvLibelle1($lastEchange->getCodeCpvLibelle1());
            if ($lastEchange->getCodeCpvLibelle2() !== null) {
                $chorusEchange->setCodeCpvLibelle2($lastEchange->getCodeCpvLibelle2());
            }
            $chorusEchange->setIdTypeProcedure($lastEchange->getIdTypeProcedure());
            $chorusEchange->setIdFormePrix($lastEchange->getIdFormePrix());
            $chorusEchange->setNbrEntreprisesCotraitantes($lastEchange->getNbrEntreprisesCotraitantes());
            $chorusEchange->setSousTraitanceDeclaree($lastEchange->getSousTraitanceDeclaree());
            $chorusEchange->setCarteAchat($lastEchange->getCarteAchat());
            $chorusEchange->setClauseSociale($lastEchange->getClauseSociale());
            $chorusEchange->setClauseEnvironnementale($lastEchange->getClauseEnvironnementale());
            $chorusEchange->setNbrPropositionRecues($lastEchange->getNbrPropositionRecues());
            $chorusEchange->setNbrPropositionDematerialisees($lastEchange->getNbrPropositionDematerialisees());
            $chorusEchange->setMontantHt($lastEchange->getMontantHt());
            $chorusEchange->setCodePaysTitulaire($lastEchange->getCodePaysTitulaire());
            $chorusEchange->setNumeroSiretTitulaire($lastEchange->getNumeroSiretTitulaire());
            $chorusEchange->setNumeroSirenTitulaire($lastEchange->getNumeroSirenTitulaire());
            $chorusEchange->setCodesPaysCoTitulaire($lastEchange->getCodesPaysCoTitulaire());
            $chorusEchange->setNumeroSiretCoTitulaire($lastEchange->getNumeroSiretCoTitulaire());
            $chorusEchange->setNumeroSirenCoTitulaire($lastEchange->getNumeroSirenCoTitulaire());
            $chorusEchange->setCcagReference($lastEchange->getCcagReference());
            $chorusEchange->setPourcentageAvance($lastEchange->getPourcentageAvance());
            $chorusEchange->setTypeAvance($lastEchange->getTypeAvance());
            $chorusEchange->setConditionsPaiement($lastEchange->getConditionsPaiement());
            $chorusEchange->setIdentifiantAccordCadre($lastEchange->getIdentifiantAccordCadre());
            $chorusEchange->setTypeFluxAEnvoyer($lastEchange->getTypeFluxAEnvoyer());
            $chorusEchange->setTypeFlux($lastEchange->getTypeFlux());
            $chorusEchange->setIdentifiantAccordCadreChapeau($lastEchange->getIdentifiantAccordCadreChapeau());
            $chorusEchange->setUuidExterneExec($lastEchange->getUuidExterneExec());
        }

        return $chorusEchange;
    }

    public function updateEntity(array $arrayInfochorusEchange): ChorusEchange
    {
        if (
            !array_key_exists('id', $arrayInfochorusEchange)
            ||
            empty($arrayInfochorusEchange['id'])
        ) {
            throw new ApiProblemValidationException('The request body must contain an ID.');
        }

        $echangeChorus = $this->chorusEchangeRepository->find($arrayInfochorusEchange['id']);
        if (!$echangeChorus instanceof ChorusEchange) {
            throw new ApiProblemValidationException(
                sprintf('Echange chorus with id %s not found.', $arrayInfochorusEchange['id'])
            );
        }

        if (!empty($this->chorusEchangeRepository->getPendingEchangeByContrat($echangeChorus->getUuidExterneExec()))) {
            throw new ApiProblemValidationException(
                sprintf(
                    'Un échange en cours de traitement côte CHORUS pour ce contrat %s.',
                    $echangeChorus->getUuidExterneExec()
                )
            );
        }

        if (
            !empty($this->chorusEchangeRepository
            ->getFailedEchangeByContrat($echangeChorus->getUuidExterneExec(), $echangeChorus->getId()))
        ) {
            throw new ApiProblemValidationException(
                sprintf(
                    'Un échange est en statut brouillon ou en erreur de génération pour ce contrat %s.',
                    $echangeChorus->getUuidExterneExec()
                )
            );
        }

        if (array_key_exists('acte', $arrayInfochorusEchange)) {
            $oldFichiers = $this->ficheModificativeRepository->findBy(['idEchange' => $echangeChorus->getId()]);
            foreach ($oldFichiers as $fichier) {
                $this->em->remove($fichier);
            }
            $this->em->flush();
            $contrat = $this->webServicesExec->getContent(
                'getContratByUuid',
                $echangeChorus->getUuidExterneExec()
            );
            $chorusFicheModificative = $this->setChorusFicheModificative($arrayInfochorusEchange, $echangeChorus);
            $this->ficheModificativeService->generatePdfFicheModificative(
                $chorusFicheModificative,
                $contrat['numEj'] ?? '',
                $contrat['referenceLibre'] ?? '',
                $contrat['montant'] ?? '',
                $contrat['id']
            );
        }

        $echangeChorus->setErreurRejet(null)
            ->setRetourChorus($this->parameterBag->get('CHORUS_RETOUR_SANS_OBJET'))
            ->setStatutEchange($this->parameterBag->get('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));
        $this->em->flush();

        return $echangeChorus;
    }

    private function setDataCalcul($arrayInfochorusEchange, $chorusEchange, $lastEchange, $contrat)
    {
        if (
            !array_key_exists('id', $arrayInfochorusEchange)
            ||
            empty($arrayInfochorusEchange['id'])
        ) {
            if ($lastEchange) {
                $numOrdre = $lastEchange->getNumOrdre();
                $chorusEchange->setNumOrdre(1 + intval($numOrdre));
            }
            $isStatutContratNotifie = in_array(strtolower($contrat['statut']), ['notifie', 'encours', 'clos']);
            $isStatutEJCommande = strstr($contrat['statutEJ'], $this->parameterBag->get('CHORUS_STATUT_EJ_COMMANDE'));
            if (
                !$this->contratService->isContratAcSadFromType($contrat['type']['codeExterne'])
                && ($isStatutContratNotifie && $isStatutEJCommande)
            ) {
                $chorusEchange->setTypeEnvoi($this->parameterBag->get('TYPE_ENVOI_CHORUS_ACTE_MODIF'));
            }
        }

        return $chorusEchange;
    }

    private function createOrUpdateEchangeChorus(array $arrayInfochorusEchange): ChorusEchange
    {
        $date = new DateTime();
        $dateFormated = $date->format('Y-m-d h:i:s');

        if (
            !array_key_exists('id', $arrayInfochorusEchange)
            ||
            empty($arrayInfochorusEchange['id'])
        ) {
            $chorusEchange = new ChorusEchange();
            $chorusEchange->setDateCreation($dateFormated);
            $chorusEchange->setStatutEchange('brouillon');
        } else {
            $chorusEchange = $this->em->getRepository(ChorusEchange::class)->find($arrayInfochorusEchange['id']);
            if (!$chorusEchange) {
                throw new ApiProblemInternalException();
            }
        }
        $chorusEchange->setDateModification($dateFormated);

        return $chorusEchange;
    }

    public function getSiretAttributaire(?string $sirenEntreprise, ?string $codeEtablissement): ?string
    {
        if (!$sirenEntreprise && !$codeEtablissement) {
            return null;
        }

        $sirenEntreprise = $sirenEntreprise && is_numeric($sirenEntreprise) ? $sirenEntreprise : '';
        $codeEtablissement = $codeEtablissement && is_numeric($codeEtablissement) ? $codeEtablissement : '';

        $sirentAttributaire = $sirenEntreprise . $codeEtablissement;

        return
            $sirentAttributaire
            && strlen($sirentAttributaire) <= self::SIZE_SIREN
                ? $sirentAttributaire
                : null;
    }

    /**
     * @return ChorusFicheModificative
     */
    private function setChorusFicheModificative(array $arrayInfochorusEchange, ChorusEchange $chorusEchange)
    {
        $chorusFicheModificative = new ChorusFicheModificative();

        $dateFinMarcheModifie = null;
        if (array_key_exists('nouvelleFinContrat', $arrayInfochorusEchange['acte'])) {
            $dateFinMarcheModifie = new DateTime($arrayInfochorusEchange['acte']['nouvelleFinContrat']);
        }
        $chorusFicheModificative->setDateFinMarcheModifie(
            $dateFinMarcheModifie
        );
        $chorusFicheModificative->setTauxTva(
            $arrayInfochorusEchange['acte']['tauxTVA'] ?? null
        );

        $chorusFicheModificative->setOrganisme(
            $chorusEchange->getOrganisme()
        );

        $chorusFicheModificative->setMontantActe(
            $arrayInfochorusEchange['acte']['montantHTChiffre'] ?? null
        );

        $chorusFicheModificative->setMontantMarche(
            $arrayInfochorusEchange['acte']['montantTTCChiffre'] ?? null
        );

        $chorusFicheModificative->setIdEchange(
            $chorusEchange->getId()
        );

        $visaPrefet = '0';
        if (array_key_exists('visaPrefet', $arrayInfochorusEchange['acte'])) {
            if ($arrayInfochorusEchange['acte']['visaPrefet'] === 'OUI') {
                $visaPrefet = '1';
            }
            if ($arrayInfochorusEchange['acte']['visaPrefet'] === 'NON') {
                $visaPrefet = '2';
            }
        }
        $chorusFicheModificative->setVisaPrefet(
            $visaPrefet
        );

        $visaACCF = '0';
        if (array_key_exists('visaACCF', $arrayInfochorusEchange['acte'])) {
            if ($arrayInfochorusEchange['acte']['visaACCF'] === 'OUI') {
                $visaACCF = '1';
            }
            if ($arrayInfochorusEchange['acte']['visaACCF'] === 'NON') {
                $visaACCF = '2';
            }
        }
        $chorusFicheModificative->setVisaAccf(
            $visaACCF
        );

        $dateCreation = null;
        if (array_key_exists('dateCreation', $arrayInfochorusEchange['acte'])) {
            $dateCreation = new DateTime($arrayInfochorusEchange['acte']['dateCreation']);
        }
        $chorusFicheModificative->setDateCreation(
            $dateCreation
        );

        $dateModification = null;
        if (array_key_exists('dateModification', $arrayInfochorusEchange['acte'])) {
            $dateModification = new DateTime($arrayInfochorusEchange['acte']['dateModification']);
        }
        $chorusFicheModificative->setDateModification(
            $dateModification
        );

        $dateNotificationPrev = null;
        if (array_key_exists('dateNotificationPrevisionnelle', $arrayInfochorusEchange['acte'])) {
            $dateNotificationPrev = new \DateTime($arrayInfochorusEchange['acte']['dateNotificationPrevisionnelle']);
        }
        $chorusFicheModificative->setDatePrevueNotification($dateNotificationPrev);

        $chorusFicheModificative->setIdBlobFicheModificative(
            null
        );

        $chorusFicheModificative->setIdBlobPieceJustificatives(
            null
        );

        $typeFournisseur = $arrayInfochorusEchange['acte']['typeFournisseurEntreprise'];
        $siret = $siren = $nom = null;

        if ($typeFournisseur === 'TITULAIRE') {
            $typeFournisseur = 1;
            if (array_key_exists('titulaire', $arrayInfochorusEchange['acte'])) {
                if (array_key_exists('siret', $arrayInfochorusEchange['acte']['titulaire'])) {
                    $siret = $arrayInfochorusEchange['acte']['titulaire']['siret'] . '#';
                }
                if (array_key_exists('fournisseur', $arrayInfochorusEchange['acte']['titulaire'])) {
                    if (array_key_exists('siren', $arrayInfochorusEchange['acte']['titulaire']['fournisseur'])) {
                        $siren = $arrayInfochorusEchange['acte']['titulaire']['fournisseur']['siren'] . '#';
                    }
                    if (array_key_exists('nom', $arrayInfochorusEchange['acte']['titulaire']['fournisseur'])) {
                        $nom = $arrayInfochorusEchange['acte']['titulaire']['fournisseur']['nom'] . '#';
                    }
                }
            }
        } elseif ($typeFournisseur === 'CO_TRAITANT') {
            $typeFournisseur = 2;
            if (array_key_exists('coTraitant', $arrayInfochorusEchange['acte'])) {
                foreach ($arrayInfochorusEchange['acte']['coTraitant'] as $coTraitant) {
                    if (array_key_exists('siret', $coTraitant)) {
                        $siret = $siret . $coTraitant['siret'] . '#';
                    }
                    if (array_key_exists('siren', $coTraitant)) {
                        $siren = $siren . $coTraitant['siren'] . '#';
                    }
                    if (array_key_exists('nom', $coTraitant)) {
                        $nom = $nom . $coTraitant['nom'] . '#';
                    }
                }
            }
        } elseif ($typeFournisseur === 'SOUS_TRAITANT') {
            $typeFournisseur = 3;
            if (array_key_exists('sousTraitant', $arrayInfochorusEchange['acte'])) {
                if (array_key_exists('siret', $arrayInfochorusEchange['acte']['sousTraitant'])) {
                    $siret = $arrayInfochorusEchange['acte']['sousTraitant']['siret'] . '#';
                }
                if (array_key_exists('siren', $arrayInfochorusEchange['acte']['sousTraitant'])) {
                    $siren = $arrayInfochorusEchange['acte']['sousTraitant']['siren'] . '#';
                }
                if (array_key_exists('nom', $arrayInfochorusEchange['acte']['sousTraitant'])) {
                    $nom = $arrayInfochorusEchange['acte']['sousTraitant']['nom'] . '#';
                }
            }
        }

        $chorusFicheModificative->setNomFournisseur(
            $nom
        );

        $chorusFicheModificative->setSirenFournisseur(
            $siren
        );

        $chorusFicheModificative->setSiretFournisseur(
            $siret
        );

        $chorusFicheModificative->setTypeFournisseur(
            $typeFournisseur
        );

        $chorusFicheModificative->setRemarque(
            $arrayInfochorusEchange['acte']['commentaire'] ?? null
        );

        $chorusFicheModificative->setTypeModification(
            $arrayInfochorusEchange['acte']['type'] ?? null
        );

        $chorusFicheModificative->setDateFinMarche(
            $arrayInfochorusEchange['acte']['datefinMarche'] ?? null
        );

        $nbSousTraitant = 0;
        if (array_key_exists('sousTraitant', $arrayInfochorusEchange['acte'])) {
            $nbSousTraitant =
                is_countable($arrayInfochorusEchange['acte']['sousTraitant']) ?
                    count($arrayInfochorusEchange['acte']['sousTraitant']) : 0;
        }
        $chorusFicheModificative->setNombreFournisseurCotraitant($nbSousTraitant);
        $this->em->persist($chorusFicheModificative);
        $this->em->flush();

        return $chorusFicheModificative;
    }

    public function setFournisseur(
        array $acte,
        ChorusFicheModificative $chorusFicheModificative
    ): ChorusFicheModificative {
        $typeFournisseur = $acte['typeFournisseurEntreprise'];
        $siret = $siren = $nom = null;
        $nbSousTraitant = 0;

        if ($typeFournisseur === 'TITULAIRE') {
            $typeFournisseur = 1;
            if (isset($acte['titulaire'])) {
                $titulaire = $acte['titulaire'];
                if (isset($titulaire['siret'])) {
                    $siret = $titulaire['siret'] . '#';
                }
                if (isset($titulaire['fournisseur'])) {
                    $fournisseur = $titulaire['fournisseur'];
                    if (isset($fournisseur['siren'])) {
                        $siren = $fournisseur . '#';
                    }
                    if (isset($fournisseur['nom'])) {
                        $nom = $fournisseur['nom'] . '#';
                    }
                }
            }
        } elseif ($typeFournisseur === 'CO_TRAITANT') {
            $typeFournisseur = 2;
            if (isset($acte['coTraitant'])) {
                $coTraitants = $acte['coTraitant'];
                foreach ($coTraitants as $coTraitant) {
                    if (isset($coTraitant['siret'])) {
                        $siret = $siret . $coTraitant['siret'] . '#';
                    }
                    if (isset($coTraitant['siren'])) {
                        $siren = $siren . $coTraitant['siren'] . '#';
                    }
                    if (isset($coTraitant['nom'])) {
                        $nom = $nom . $coTraitant['nom'] . '#';
                    }
                }
            }
        } elseif ($typeFournisseur === 'SOUS_TRAITANT') {
            $typeFournisseur = 3;
            if (isset($acte['sousTraitant'])) {
                $sousTraitant = $acte['sousTraitant'];
                if (isset($sousTraitant['siret'])) {
                    $siret = $sousTraitant['siret'] . '#';
                }
                if (isset($sousTraitant['siren'])) {
                    $siren = $sousTraitant['siren'] . '#';
                }
                if (isset($sousTraitant['nom'])) {
                    $nom = $sousTraitant['nom'] . '#';
                }
                $nbSousTraitant = is_countable($sousTraitant) ? count($sousTraitant) : 0;
            }
        }

        $chorusFicheModificative->setNomFournisseur($nom);

        $chorusFicheModificative->setSirenFournisseur($siren);

        $chorusFicheModificative->setSiretFournisseur($siret);

        $chorusFicheModificative->setTypeFournisseur($typeFournisseur);

        $chorusFicheModificative->setNombreFournisseurCotraitant($nbSousTraitant);

        return $chorusFicheModificative;
    }

    public function getCodeApe(?string $codeApe): ?string
    {
        return $codeApe && strlen($codeApe) <= self::SIZE_CODE_APE ? $codeApe : null;
    }
}
