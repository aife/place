<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Utah;

use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Service;
use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\EtatConsultation;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Service\Consultation\ConsultationService;
use App\Service\Version\VersionService;
use App\Utils\Utils;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class UtahService
{
    private $callFrom;

    private ?Request $request = null;

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly SessionInterface $session,
        private readonly LoggerInterface $utahLogger,
        private readonly EntityManagerInterface $entityManager,
        private readonly UtahCommunicationService $utahCommunicationService,
        private readonly ConsultationService $consultationService,
        private readonly TranslatorInterface $translator,
        private readonly Utils $utils,
        private readonly VersionService $versionService,
        private readonly WebServicesUtah $webServicesUtah
    ) {
    }

    /**
     * Permet d'initialiser le bouton d'acces a l'application UTAH.
     */
    public function initialiser(): void
    {
        if (!$this->parameterBag->get('UTILISER_FAQ') && !$this->parameterBag->get('UTILISER_UTAH')) {
            return;
        }

        if (!$this->security->getUser() && !$this->parameterBag->get('UTILISER_UTAH_MODE_DECONNECTER')) {
            return;
        }

        $this->preparerDonneesPourContexteMetier();
    }


    /**
     * Permet de preparer les donnees necessaires pour la generation des donnees du contexte metier.
     */
    public function preparerDonneesPourContexteMetier(): void
    {
        $contexteAuthentification = [];
        $this->session->remove($this->session->getId() . '_consultation');
        $ref = null;
        $org = null;
        try {
            $cas = null;
            $uri = $this->getRequest()->getRequestUri();
            if (preg_match('/page/', $uri)) {
                $page = $this->getRequest()->get('page');
                switch ($page) {
                    case 'Entreprise.EntrepriseDetailsConsultation':
                    case 'Entreprise.EntrepriseDemandeTelechargementDce':
                    case 'Entreprise.EntrepriseFormulairePoserQuestion':
                        [$cas, $ref, $org] = self::setDataWithGet();
                        break;
                    case 'Entreprise.FormulaireReponseConsultationMPS':
                    case 'Entreprise.FormulaireReponseConsultation':
                        $cas = 'contexte_metier_consultation';
                        $ref = $this->utils->atexoHtmlEntities($_GET['id']);
                        $org = $this->utils->atexoHtmlEntities($_GET['org']);
                        break;
                    case 'Agent.DetailConsultation':
                    case 'Agent.ouvertureEtAnalyse':
                    case 'Agent.GestionRegistres':
                    case 'Agent.ArchiveArborescence':
                    case 'Agent.ArchiveMetadonnee':
                    case 'Agent.ArchiveRappelTableauDeBord':
                    case 'Agent.ValiderConsultation':
                    case 'Agent.TraduireConsultation':
                    case 'Agent.TableauDecisionChorus':
                    case 'Agent.TableauDeBord':
                    case 'Agent.resultatAnalyse':
                    case 'Agent.EnvoiCourrierElectroniqueAnnulationConsultation':
                    case 'Agent.EnvoiCourrierElectroniqueChoixDestinataire':
                    case 'Agent.EnvoiCourrierElectroniqueComplementsFormulaires':
                    case 'Agent.EnvoiCourrierElectroniqueDemandeComplement':
                    case 'Agent.EnvoiCourrierElectroniqueInvitationConcourir':
                    case 'Agent.EnvoiCourrierElectroniqueNotification':
                    case 'Agent.EnvoiCourrierElectroniqueModifConsultation':
                    case 'Agent.EnvoiCourrierElectroniquePress':
                    case 'Agent.EnvoiCourrierElectroniqueReponseQuestion':
                    case 'Agent.EnvoiCourrierElectroniqueSimple':
                    case 'Agent.FormeEnvoiCourrierElectroniqueJAL':
                    case 'Agent.DonneesComplementairesConsultation':
                    case 'Agent.DetailDce':
                    case 'Agent.popUpAjoutRegistrePapier':
                    case 'Agent.popUpAvisMembresCommission':
                    case 'Agent.PopUpEvaluationOffre':
                    case 'Agent.popUpImporterEnveloppe':
                    case 'Agent.popUpRenseignerStatut':
                    case 'Agent.DetailAnalyse':
                    case 'Agent.PopUpRenseignerDecision':
                    case 'Agent.ConsultationDetailReponse':
                    case 'Agent.ConsultationsDetailAccordCadre':
                    case 'Agent.HistoriqueConsultation':
                        [$cas, $ref, $org] = self::setData();
                        break;
                    case 'Agent.SuiteConsultation':
                        $cas = 'contexte_metier_consultation';
                        $ref = $this->utils->atexoHtmlEntities($_GET['id']);
                        if ($this->security->getUser()) {
                            $org = $this->utils->atexoHtmlEntities($_GET['org']);
                        }
                        break;
                    case 'Agent.PopupUpdateDce':
                        $cas = 'contexte_metier_consultation';
                        $ref = base64_decode($_GET['id']);
                        if ($this->security->getUser()) {
                            $org = $this->security->getUser()->getOrganisme()->getAcronyme();
                        }
                        break;
                    case 'Agent.PubliciteConsultation':
                        $cas = 'contexte_metier_consultation';
                        $ref = base64_decode($this->utils->atexoHtmlEntities($_GET['id']));
                        if ($this->security->getUser()) {
                            $org = $this->security->getUser()->getOrganisme()->getAcronyme();
                        }
                        break;
                    case 'Agent.FormulaireConsultation':
                    case 'Agent.GererEnchere':
                    case 'Agent.PopupConcentrateur':
                    case 'Agent.PopupChoixTypeAvis':
                    case 'Agent.PubliciteConsultationMol':
                    case 'Agent.RedactionPiecesConsultation':
                    case 'Agent.ChangingConsultation':
                        [$cas, $ref, $org] = self::setData(true);
                        break;
                    case 'Agent.PopupAjoutLot':
                    case 'Agent.PopupDetailLot':
                        $cas = 'contexte_metier_consultation';
                        $ref = $this->utils->atexoHtmlEntities($_GET['id']);
                        $org = $this->utils->atexoHtmlEntities($_GET['orgAccronyme']);
                        break;
                    case 'Agent.popUpDateAnnulation':
                        $cas = 'contexte_metier_consultation';
                        $ref = $this->utils->atexoHtmlEntities($_GET['consultationId']);
                        if ($this->security->getUser()) {
                            $org = $this->security->getUser()->getOrganisme()->getAcronyme();
                        }
                        break;
                }
            }

            if (str_starts_with($uri, '/entreprise/consultation')) {
                if (empty($ref) && empty($org)) {
                    $session = $this->request->getSession()->all();
                    foreach ($session as $key => $value) {
                        if (str_starts_with($key, 'contexte_authentification_') && is_array($value) && !empty($value)) {
                            $contexteAuthentification = $value;
                        }
                    }

                    if (is_array($contexteAuthentification)) {
                        $ref = $contexteAuthentification['reference_consultation'];
                        $org = $contexteAuthentification['organisme_consultation'];
                    }
                }

                $this->session->set($this->session->getId() . '_consultation', $ref);
            }

            if ('contexte_metier_consultation' === $cas) {
                if (!empty($ref) && !empty($org)) {
                    $this->session->set($this->session->getId() . '_consultation', $ref);
                }
            }
        } catch (Exception $e) {
            $this->utahLogger->error(
                'Erreur lors de la creation des contextes UTAH: '
                . PHP_EOL
                . 'Exception: '
                . $e->getMessage()
                . PHP_EOL
                . 'Trace: '
                . $e->getTraceAsString()
            );
        }
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): UtahService
    {
        $this->request = $request;

        return $this;
    }

    /**
     *
     */
    private function setDataWithGet(): array
    {
        $cas = 'contexte_metier_consultation';
        $ref = $this->utils->atexoHtmlEntities($_GET['id']);
        $org = $this->utils->atexoHtmlEntities($_GET['orgAcronyme']);

        return [$cas, $ref, $org];
    }

    private function setData(bool $withDecode = false): array
    {
        $org = '';
        $cas = 'contexte_metier_consultation';
        $reference = $_GET['id'];
        if ($withDecode) {
            $reference = base64_decode($_GET['id']);
        }
        $ref = $this->utils->atexoHtmlEntities($reference);
        if ($this->security->getUser()) {
            $org = $this->security->getUser()->getOrganisme()->getAcronyme();
        }

        return [$cas, $ref, $org];
    }

    /**
     * Permet de gerer l'acces au formulaire de l'application UTAH.
     * @param $javaVersion
     */
    public function accederFormulaireUtah(RequestEvent $event, $javaVersion)
    {

        if (!$this->parameterBag->get('UTILISER_FAQ') && !$this->parameterBag->get('UTILISER_UTAH')) {
            return;
        }

        if ($this->security->getUser() || $this->parameterBag->get('UTILISER_UTAH_MODE_DECONNECTER')) {
            //Creation des contextes
            $context = $this->creerContextes($javaVersion);
            //Recuperation du jeton et ouverture du formulaire UTAH
            $jsonContexte = $this->encodeToJson($context);

            $url = $this->utahCommunicationService->utahUsedAuthentificationKeycloak() ?
                $this->getKeycloackUrl($jsonContexte) : $this->getUrl($event, $jsonContexte);


            $response = new RedirectResponse($url);

            $event->setResponse($response);
        }
    }

    /**
     * Permet de creer les contextes a envoyer a l'application UTAH.
     * @param false $javaVersion
     * @return array|void
     */
    public function creerContextes($javaVersion = false)
    {
        try {
            $contexteApplicative = $this->creerContexteApplicatif();
            $contexteTechnique = $this->creerContexteTechnique($javaVersion);
            $contexteMetier = $this->creerContexteMetier();

            $contextes = [];
            $contextes['applicatif'] = $contexteApplicative->toArray();
            $contextes['technique'] = $contexteTechnique->toArray();
            $contextes['metier'] = $contexteMetier->toArray();

            return $contextes;
        } catch (Exception $e) {
            $this->utahLogger->error(
                'Erreur lors de la creation des contextes UTAH: '
                . PHP_EOL
                . 'Exception: '
                . $e->getMessage()
                . PHP_EOL
                . 'Trace: '
                . $e->getTraceAsString()
            );
        }
    }

    /**
     * Permet de creer un contexte applicatif.
     * @return ContexteUtahService|void
     */
    private function creerContexteApplicatif()
    {
        try {
            $contexteApplicative = new ContexteUtahService();

            $nomUtilisateur = null;
            $idUtilisateur = null;
            $emailUtilisateur = null;
            $entiteUtilisateur = null;
            $createUtilisateur = null;
            if ($this->security->getUser()) {
                $user = $this->security->getUser();
                $nomUtilisateur = $user->getNom() . ' ' . $user->getPrenom();
                $createUtilisateur = $user->getDateCreation();
                $idUtilisateur = $user->getId();
                $emailUtilisateur = $user->getEmail();

                if ($user instanceof Agent) {
                    $entitePublique = $user->getOrganisme()->getSigle();
                    $entiteAchat = $user->getOrganisme()->getDenominationOrg();

                    $organismeIdInitial = $user->getOrganisme()->getIdInitial();
                    $agentId = $user->getId();
                    $agentExterneId = $user->getIdExterne();
                    $serviceIdExterne = $user->getService()?->getIdExterne();
                    $nomService = $user->getService()?->getLibelle();

                    $contexteApplicative->add(
                        'entiteIdSocle',
                        'entiteIdSocle',
                        $organismeIdInitial ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'serviceIdSocle',
                        'serviceIdSocle',
                        $serviceIdExterne ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'service',
                        'service',
                        $nomService ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'id',
                        'Id Agent',
                        $agentId ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'idSocle',
                        'Agent id externe',
                        $agentExterneId ?? null,
                        'string'
                    );

                    $entiteUtilisateur = [
                        'entite_publique' => $entitePublique,
                        'entite_achat' => $entiteAchat,
                    ];
                } elseif ($user instanceof Inscrit) {
                    $entreprise = $user->getEntreprise();
                    $etab = $user->getEtablissement();
                    $cp = null;
                    if ($etab instanceof Etablissement) {
                        $cp = $etab->getCodePostal();
                    }

                    $raisonSocialIdSocle = $user->getIdInitial();
                    $inscritId = $user->getId();
                    $inscriIntialId = $user->getIdInitial();

                    $contexteApplicative->add(
                        'raisonSocialIdSocle',
                        'Entreprise id initial',
                        $raisonSocialIdSocle ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'idEntreprise',
                        'Id Inscrit',
                        $inscritId ?? null,
                        'string'
                    );
                    $contexteApplicative->add(
                        'idSocleEntreprise',
                        'Id Socle',
                        $inscriIntialId ?? null,
                        'string'
                    );

                    $entiteUtilisateur = [
                        'nom' => $entreprise->getNom(),
                        'siren' => $entreprise->getSiren(),
                        'code_postal' => $cp,
                    ];
                }
            }


            $contexteApplicative->setNom('applicatif');
            $contexteApplicative->setLabel('applicatif');
            $contexteApplicative->setDescription('Contexte applicatif de la demande');
            $contexteApplicative
                ->setMaxVisible($this->parameterBag->get('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_APPLICATIF'));

            $contexteApplicative->add(
                'application',
                'Plate-forme',
                $this->parameterBag->get('PF_SHORT_NAME'),
                'string'
            );

            $contexteApplicative->add(
                'PF_UID',
                'Plate-forme-uid',
                $this->parameterBag->get('UID_PF_MPE'),
                'string'
            );

            $contexteApplicative->add(
                'version',
                'Version',
                $this->versionService->getApplicationVersion(),
                'string'
            );

            $contexteApplicative->add(
                'type_acteur',
                "Type d'utilisateur",
                $this->getCallFrom(),
                'string',
                false
            );

            $contexteApplicative->add(
                'utilisateur_nom',
                'Nom de l\'utilisateur',
                $nomUtilisateur,
                'string'
            );

            $contexteApplicative->add(
                'utilisateur_id',
                'ID de l\'utilisateur',
                $idUtilisateur,
                'integer',
                false
            );

            $contexteApplicative->add(
                'utilisateur_email',
                'Courriel de l\'utilisateur',
                $emailUtilisateur,
                'mailto'
            );

            if ($createUtilisateur) {
                $contexteApplicative->add(
                    'utilisateur_date_creation',
                    'Date de création',
                    $createUtilisateur,
                    'datetime'
                );
            }

            if ($this->getCallFrom() === 'agent_place') {
                $contexteApplicative->add(
                    'utilisateur_entite',
                    'Organisme',
                    $entiteUtilisateur,
                    'organisme_achat'
                );
            } else {
                $contexteApplicative->add(
                    'utilisateur_entite',
                    'Entreprise',
                    $entiteUtilisateur,
                    'entreprise'
                );
            }
            $contexteApplicative->add(
                'utiliser_utah',
                'utiliser_utah',
                $this->parameterBag->get('UTILISER_UTAH'),
                'boolean',
                false
            );
            $contexteApplicative->add(
                'url_utah',
                'url_utah',
                $this->parameterBag->get('URL_UTAH'),
                'string',
                false
            );

            if (
                $this->utahCommunicationService->utahUsedAuthentificationKeycloak()
                && $utahAuthResponse = $this->webServicesUtah->getContent('getAuthResponse', [])
            ) {
                $contexteApplicative->add(
                    WebServicesUtah::ACCESS_TOKEN_LIBELLE,
                    WebServicesUtah::ACCESS_TOKEN_LIBELLE,
                    $utahAuthResponse[WebServicesUtah::ACCESS_TOKEN_LIBELLE],
                    'string',
                    false
                );
            }

            return $contexteApplicative;
        } catch (Exception $e) {
            $this->utahLogger
                ->error(
                    'Erreur lors de creation du contexte applicatif: '
                    . PHP_EOL . 'Exception: '
                    . $e->getMessage()
                    . PHP_EOL
                    . 'Trace: '
                    . $e->getTraceAsString()
                );
        }
    }

    /**
     * @return string
     */
    public function getCallFrom()
    {
        $callfrom = 'entreprise_place';
        if ($this->callFrom === 'agent') {
            $callfrom = 'agent_place';
        }

        return $callfrom;
    }

    /**
     * @param $callFrom
     * @return $this
     */
    public function setCallFrom($callFrom)
    {
        $this->callFrom = $callFrom;

        return $this;
    }

    /**
     * Permet de creer un contexte Technique.
     * @param false $javaVersion
     * @return ContexteUtahService|void
     */
    public function creerContexteTechnique($javaVersion = false)
    {
        try {
            $javaVersion = $javaVersion ?: $this->translator->trans('UNKNOWN');

            $contexteTechnique = new ContexteUtahService();

            $contexteTechnique->setNom('technique');
            $contexteTechnique->setLabel('technique');
            $contexteTechnique->setDescription('Technique');
            $contexteTechnique
                ->setMaxVisible($this->parameterBag->get('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_TECHNIQUE'));

            $contexteTechnique->add(
                'os',
                "Système d'exploitation",
                $this->utils->getOsVisiteur(),
                'string',
                true,
                true,
                null,
                $_SERVER['HTTP_USER_AGENT']
            );

            $infosBrowser = $this->utils->getInfosBrowserVisiteur();
            $contexteTechnique->add(
                'navigateur',
                'Navigateur',
                $infosBrowser['name'] . ' ' . $infosBrowser['version'],
                'string',
                true,
                true,
                null,
                $_SERVER['HTTP_USER_AGENT']
            );

            $contexteTechnique->add(
                'java_ver',
                'Version de Java',
                $javaVersion,
                'string'
            );

            return $contexteTechnique;
        } catch (Exception $e) {
            $this->utahLogger->error(
                'Erreur lors de creation du contexte technique: '
                . PHP_EOL . 'Exception: '
                . $e->getMessage() . PHP_EOL
                . 'Trace: ' . $e->getTraceAsString()
            );
        }
    }

    /**
     * Permet de creer un contexte metier.
     * @return ContexteUtahService|void
     */
    public function creerContexteMetier()
    {
        try {
            $contexteMetier = new ContexteUtahService();

            $contexteMetier->setNom('metier');
            $contexteMetier->setLabel('metier');
            $contexteMetier->setDescription('Métier');
            $contexteMetier->setMaxVisible($this->parameterBag->get('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_METIER'));

            $consultationId = $this->session->get($this->session->getId() . '_consultation');
            $consultation = $this->entityManager
                ->getRepository(Consultation::class)
                ->findOneById($consultationId);

            if ($consultation instanceof Consultation) {
                $contexteMetier->add(
                    'ref_consult',
                    'Réf. de la consultation',
                    $consultation->getReferenceUtilisateur(),
                    'string'
                );

                $contexteMetier->add(
                    'id_consult',
                    'ID de la consultation',
                    $consultation->getId(),
                    'integer',
                    false
                );

                $objetEtatCons = $this->entityManager
                    ->getRepository(EtatConsultation::class)
                    ->findOneBy(
                        [
                            'idEtat' => $this->consultationService->getStatus($consultation, $this->isCallFrom()),
                        ]
                    );
                if ($objetEtatCons instanceof EtatConsultation) {
                    $contexteMetier->add(
                        'etape',
                        'Étape',
                        $this->translator->trans($objetEtatCons->getCodeEtat()),
                        'string'
                    );
                }

                $contexteMetier->add(
                    'allotissement',
                    'Allotissement',
                    ($consultation->getAlloti()) ? true : false,
                    'boolean'
                );
                $contexteMetier->add(
                    'rdp',
                    'Date limite de remise des plis',
                    $consultation->getDateFin()?->format('y-m-d h:i:s'),
                    'datetime'
                );
            }

            return $contexteMetier;
        } catch (Exception $e) {
            $this->utahLogger->error(
                'Erreur lors de creation du contexte metier: '
                . PHP_EOL . 'Exception: '
                . $e->getMessage()
                . PHP_EOL
                . 'Trace: '
                . $e->getTraceAsString()
            );
        }
    }

    /**
     * @return bool
     */
    public function isCallFrom()
    {
        $bool = false;
        if ($this->callFrom === 'entreprise') {
            $bool = true;
        }

        return $bool;
    }

    /**
     * Permet d'encoder en json les donnees a envoyer a l'applicatio UTAH.
     * @param $data
     * @return false|string|void
     */
    private function encodeToJson($data)
    {
        try {
            return json_encode($this->utils->utf8Converter($data), JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $jsonLastError = match (json_last_error()) {
                JSON_ERROR_NONE => ' - Aucune erreur',
                JSON_ERROR_DEPTH => ' - Profondeur maximale atteinte',
                JSON_ERROR_STATE_MISMATCH => ' - Inadéquation des modes ou underflow',
                JSON_ERROR_CTRL_CHAR => ' - Erreur lors du contrôle des caractères',
                JSON_ERROR_SYNTAX => ' - Erreur de syntaxe ; JSON malformé',
                JSON_ERROR_UTF8 => ' - Caractères UTF-8 malformés, probablement une erreur d\'encodage',
                default => ' - Erreur inconnue',
            };
            $this->utahLogger->error(
                "json last error = $jsonLastError "
                . PHP_EOL . 'Exception: '
                . $e->getMessage()
                . PHP_EOL
                . 'Trace: ' . $e->getTraceAsString()
            );
        }
    }

    private function getKeycloackUrl(string $jsonContexte): string
    {
        $url = $this->utahCommunicationService->getUrlAssistance();

        $key_assistance = 'URL_UTAH_RECUPERATION_TOKEN';
        $contentTypeJson = 'application/json';
        if ($this->parameterBag->get('UTILISER_FAQ')) {
            $key_assistance = 'URL_FAQ_RECUPERATION_TOKEN';
            $contentTypeJson = 'application/ld+json';
            $rawData = json_encode(['data' => $jsonContexte], JSON_THROW_ON_ERROR);
        }

        $params = ['context' => base64_encode($jsonContexte)];

        $url .= sprintf("?%s", http_build_query($params));

        return $url;
    }

    private function getUrl(RequestEvent $event, string $jsonContexte): string
    {
        $token = $this->utahCommunicationService->recupererToken($jsonContexte);
        $expode = explode('&Utah=', $event->getRequest()->getRequestUri());
        $url = $expode[0];

        if (false !== $token && !empty($token)) {
            $url = $this->utahCommunicationService->getUrlAssistance() . '?token=' . $token;
        } else {
            $this->session->getFlashBag()->add(
                'error',
                $this->translator->trans('MESSAGE_ERREUR_CONNEXION_APPLICATION_UTAH') . '<center>' .
                $this->parameterBag->get('NUMERO_TELEPHONE_INDISPO_UTAH') . '</center>'
            );
        }

        return $url;
    }
}
