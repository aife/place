<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Utah;

use App\Service\AbstractService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesUtah extends AbstractService
{
    private const CLIENT_ID = 'utah-api';

    public final const ACCESS_TOKEN_LIBELLE = 'access_token';
    public final const REFRESH_TOKEN_LIBELLE = 'refresh_token';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $utahLogger
    ) {
        parent::__construct($httpClient, $utahLogger, $parameterBag);
    }

    protected function getAuthResponse(): string|array|null
    {
        return $this->getAuthenticationTokenResponse(
            $this->parameterBag->get('URL_UTAH_KEYCLOAK_OPEN_ID'),
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD'),
        );
    }
}
