<?php

namespace App\Service\Utah;

use Application\Service\Atexo\Atexo_MultiDomaine;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Laminas\Http\Client;
use Laminas\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Classe de gestion de  l'Utilitaire de Transmission et suivi d'Avancement Hotline.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class UtahCommunicationService
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly LoggerInterface $utahLogger,
        private readonly WebServicesUtah $webServicesUtah
    ) {
    }

    /**
     * Permet de recuperer le token d'acces a l'application UTAH.
     *
     * @param string $contexte : contexte en json
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     *
     * @return bool
     */
    public function recupererToken($contexte)
    {
        $params = '[uri : ' . $this->parameterBag->get('URL_FAQ_RECUPERATION_TOKEN') . "] , [contextes: $contexte]";
        $this->utahLogger->info('Debut requete http POST pour recuperation du token');
        $this->utahLogger->info("Params de la requete : $params");
        try {
            $client = new Client();
            $key_assistance = 'URL_UTAH_RECUPERATION_TOKEN';
            $rawData = $contexte;

            // info: le content-type application/ld+json est bloqué par les régles du CSP
            $contentTypeJson = 'application/json';

            if ($this->parameterBag->get('UTILISER_FAQ')) {
                $key_assistance = 'URL_FAQ_RECUPERATION_TOKEN';
                $rawData = json_encode(['data' => $contexte, 'ip' => ''], JSON_THROW_ON_ERROR);
            }

            // Get utah auth token keycloak
            if ($this->utahUsedAuthentificationKeycloak()
                && $utahAuthResponse = $this->webServicesUtah->getContent('getAuthResponse', $params)
            ) {
                $client->setHeaders([
                    WebServicesUtah::ACCESS_TOKEN_LIBELLE => $utahAuthResponse['access_token'],
                    WebServicesUtah::REFRESH_TOKEN_LIBELLE => $utahAuthResponse['refresh_token']
                ]);
            }

            $client->setUri($this->parameterBag->get($key_assistance));
            $client->setMethod(Request::METHOD_POST);
            $client->setEncType($contentTypeJson);
            $client->setRawBody($rawData);

            $reponse = $client->send();
            $statusCode = $reponse->getStatusCode();
            $responseBody = $reponse->getBody();

            if (
                !empty($responseBody)
                && ($statusCode && in_array($statusCode, [Response::HTTP_CREATED, Response::HTTP_OK]))
            ) {
                $decodeReponse = json_decode($responseBody, null, 512, JSON_THROW_ON_ERROR);
                if (is_object($decodeReponse)) {
                    if (!empty($decodeReponse->token)) {
                        $token = $decodeReponse->token;
                        $this->utahLogger->info("Token recupere avec SUCCES: $token");

                        return $token;
                    } else {
                        $this->utahLogger->info('Echec recuperation du token. Details: ' . $decodeReponse->error);
                    }
                }
            } else {
                $this->utahLogger->info('Echec recuperation du token. La reponse du ws est vide.');
            }
            $this->utahLogger->info('Fin requete http POST pour recuperation du token');
        } catch (Exception $e) {
            $this->utahLogger->error(
                'Erreur lors de la recuperation du token UTAH'
                . PHP_EOL . "Params: $params"
                . PHP_EOL . 'Exception: '
                . $e->getMessage() . PHP_EOL
                . 'Trace: ' . $e->getTraceAsString()
            );
        }
        $this->utahLogger->info('Fin requete http POST pour recuperation du token');

        return false;
    }

    public function getUrlAssistance()
    {
        $option = [];
        $key_assistance = 'URL_UTAH';
        if ($this->parameterBag->get('URL_FAQ')) {
            $key_assistance = 'URL_FAQ';
        }

        $option['slash'] = true;
        return str_replace(
            $this->parameterBag->get('PF_URL_REFERENCE'),
            Atexo_MultiDomaine::getDomaine($this->parameterBag->get('PF_URL_REFERENCE'), $option),
            $this->parameterBag->get($key_assistance)
        );
    }

    public function utahUsedAuthentificationKeycloak(): bool
    {
        return (bool)$this->parameterBag->get('UTAH_AUTHENTICATION_WITH_KEYCLOAK');
    }
}
