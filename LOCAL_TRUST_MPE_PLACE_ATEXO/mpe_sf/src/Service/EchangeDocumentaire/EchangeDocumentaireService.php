<?php

namespace App\Service\EchangeDocumentaire;

use App\Repository\ReferentielSousTypeParapheurRepository;
use Exception;
use Doctrine\ORM\Exception\ORMException;
use ZipArchive;
use Doctrine\ORM\OptimisticLockException;
use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\EchangeDocumentaire\EchangeDoc;
use App\Entity\EchangeDocumentaire\EchangeDocApplicationClient;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\EchangeDocumentaire\EchangeDocHistorique;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceActes;
use App\Entity\EchangeDocumentaire\EchangeDocTypePieceStandard;
use App\Message\GenerateArchive;
use App\Service\CurrentUser;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EchangeDocumentaireService
{
    private const CHEMIN = 'chemin';

    final public const PASTELL = 'pastell';
    final public const CHEMINEMENT_SIGNATURE = 'cheminement_signature';
    final public const CHEMINEMENT_GED = 'cheminement_ged';
    final public const CHEMINEMENT_SEA = 'cheminement_sae';
    final public const CHEMINEMENT_TDT = 'cheminement_tdt';
    final public const TYPAGE_ANNEXE = 'typage_annexe';

    /**
     * EchangeDocumentaireService constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameter,
        private readonly EntityManagerInterface $em,
        private readonly CurrentUser $currentUser,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager,
        private readonly LoggerInterface $espacedocLogger,
        private readonly TranslatorInterface $translator,
        private readonly MessageBusInterface $bus,
        private readonly ReferentielSousTypeParapheurRepository $sousTypeParapheurRepository,
    ) {
    }

    /**
     * @return mixed
     */
    public function getAuthorizedApplicationClient()
    {
        $resultat = $this->em->getRepository(EchangeDocApplicationClient::class)
            ->getAuthorizedApplicationClientAvailable($this->currentUser->getCurrentUser()->getOrganisme());

        if (empty($resultat)) {
            $resultat = $this->em->getRepository(EchangeDocApplicationClient::class)
                ->getAuthorizedApplicationClientAll();
        }

        return $resultat;
    }

    public function createEchangeDocumentaire(array $data, int $consultationId): EchangeDoc
    {
        try {
            $echangeDoc = $this->newEchangeDoc($data, $consultationId);
            $this->persist($echangeDoc);

            foreach ($data['fichierPrincipaux'] as $files) {
                $echangeDocBlobPrincipal =
                    $this->newEchangeDocBlob($files['principal'], EchangeDocBlob::FICHIER_PRINCIPAL);
                $actes = $echangeDoc->getEchangeDocApplicationClient()->getEchangeDocApplication()->isFluxActes();
                $typagePrincipal = $files['typage'];

                if (true === $actes) {
                    $echangeDocTypePieceActes = $this->em
                        ->getRepository(EchangeDocTypePieceActes::class)
                        ->find($typagePrincipal);
                    $echangeDocBlobPrincipal->setEchangeTypeActes($echangeDocTypePieceActes);
                } else {
                    $echangeDocTypePieceStandard = $this->em
                        ->getRepository(EchangeDocTypePieceStandard::class)
                        ->find($typagePrincipal);
                    $echangeDocBlobPrincipal->setEchangeTypeStandard($echangeDocTypePieceStandard);
                }

                $echangeDocBlobPrincipal->setEchangeDoc($echangeDoc);
                $this->persist($echangeDocBlobPrincipal);

                $this->addJetonFiles($files, $echangeDoc, $echangeDocBlobPrincipal);
                $typeAnnexe = $files[self::TYPAGE_ANNEXE] ?? [];
                $this->addAnnexeFiles($files, $typeAnnexe, $echangeDoc, $actes, $echangeDocBlobPrincipal);
            }

            $this->em->flush();
            return $echangeDoc;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @throws ORMException
     */
    public function addJetonFiles(
        array $files,
        EchangeDoc $echangeDoc,
        EchangeDocBlob $echangeDocBlobPrincipal
    ): void {
        if (array_key_exists('jetons', $files)) {
            foreach ($files['jetons'] as $fichierJeton) {
                $echangeDocBlobJeton = $this->newEchangeDocBlob($fichierJeton, EchangeDocBlob::FICHIER_JETON);
                $echangeDocBlobJeton->setEchangeDoc($echangeDoc);
                $echangeDocBlobJeton->setDocPrincipal($echangeDocBlobPrincipal);
                $this->persist($echangeDocBlobJeton);
            }
        }
    }

    /**
     * @param $entity
     *
     * @throws ORMException
     */
    public function persist($entity): void
    {
        $this->em->persist($entity);
    }

    /**
     * @throws ORMException
     */
    public function addAnnexeFiles(
        array $files,
        array $typages,
        EchangeDoc $echangeDoc,
        bool $actes,
        EchangeDocBlob $echangeDocBlobPrincipal
    ): void {
        if (array_key_exists(self::TYPAGE_ANNEXE, $files)) {
            foreach ($files[self::TYPAGE_ANNEXE] as $key => $typageAnnexes) {
                $fichierAnnexe = key($typageAnnexes);
                $echangeDocBlobAnnexes = $this->newEchangeDocBlob($fichierAnnexe, EchangeDocBlob::FICHIER_ANNEXE);
                $echangeDocBlobAnnexes = $this->setEchangeDocTypePieceActesOrPieceStandard(
                    $typages,
                    $fichierAnnexe,
                    $actes,
                    $echangeDocBlobAnnexes
                );
                $echangeDocBlobAnnexes->setEchangeDoc($echangeDoc);
                $echangeDocBlobAnnexes->setDocPrincipal($echangeDocBlobPrincipal);
                $this->persist($echangeDocBlobAnnexes);
            }
        }
    }

    public function setEchangeDocTypePieceActesOrPieceStandard(
        array $typages,
        string $fichierAnnexe,
        bool $actes,
        EchangeDocBlob $echangeDocBlobAnnexes
    ): EchangeDocBlob {
        foreach ($typages as $key => $typageAnnexe) {
            if (array_key_exists($fichierAnnexe, $typageAnnexe)) {
                $typage = $typageAnnexe[$fichierAnnexe];
                if (true === $actes) {
                    $echangeDocTypePieceActes = $this->em
                        ->getRepository(EchangeDocTypePieceActes::class)
                        ->find($typage);
                    $echangeDocBlobAnnexes->setEchangeTypeActes($echangeDocTypePieceActes);
                } else {
                    $echangeDocTypePieceStandard = $this->em
                        ->getRepository(EchangeDocTypePieceStandard::class)
                        ->find($typage);
                    $echangeDocBlobAnnexes->setEchangeTypeStandard($echangeDocTypePieceStandard);
                }
                break;
            }
        }

        return $echangeDocBlobAnnexes;
    }

    private function newEchangeDoc($data, $consultationId): EchangeDoc
    {
        $echangeDocApplicationClient =
            $this->em->getRepository(EchangeDocApplicationClient::class)->find($data['application']);

        $agent = null;
        if (!$echangeDocApplicationClient?->getEchangeDocApplication()->isEnvoiDic()) {
            $agent = $this->currentUser->getCurrentUser();
        }

        $consultation =
            $this->em->getRepository(Consultation::class)->find($consultationId);
        if (!$echangeDocApplicationClient instanceof EchangeDocApplicationClient) {
            throw new Exception('No application client found');
        }
        if (!$consultation instanceof Consultation) {
            throw new Exception('No consultation found');
        }
        $echangeDoc = new EchangeDoc();
        $echangeDoc->setObjet($data['objet']);
        $echangeDoc->setDescription($data['description']);

        if (array_key_exists(self::PASTELL, $data)) {
            if (array_key_exists(self::CHEMINEMENT_SIGNATURE, $data[self::PASTELL])) {
                $signature = ('false' === $data[self::PASTELL][self::CHEMINEMENT_SIGNATURE]) ? false : true;
                $echangeDoc->setCheminementSignature($signature);
            }

            if (array_key_exists(self::CHEMINEMENT_GED, $data[self::PASTELL])) {
                $ged = ('false' === $data[self::PASTELL][self::CHEMINEMENT_GED]) ? false : true;
                $echangeDoc->setCheminementGed($ged);
            }

            if (array_key_exists(self::CHEMINEMENT_SEA, $data[self::PASTELL])) {
                $sea = ('false' === $data[self::PASTELL][self::CHEMINEMENT_SEA]) ? false : true;
                $echangeDoc->setCheminementSae($sea);
            }

            if (array_key_exists(self::CHEMINEMENT_TDT, $data[self::PASTELL])) {
                $tdt = ('false' === $data[self::PASTELL][self::CHEMINEMENT_TDT]) ? false : true;
                $echangeDoc->setCheminementTdt($tdt);
            }
        }

        if ($data['contrat']) {
            if ($this->parameter->get('ACTIVE_EXEC_V2')) {
                $echangeDoc->setUuidExterneExec($data['contrat']);
            } else {
                // TODO : EXEC - A supprimer quand le module EXEC sera activé
                $contratTitulaire = $this->em->getRepository(ContratTitulaire::class)->find($data['contrat']);
                $echangeDoc->setContratTitulaire($contratTitulaire);
            }
        }

        if ($data['sousTypeParapheur']) {
            $echangeDoc->setReferentielSousTypeParapheur(
                $this->sousTypeParapheurRepository->find($data['sousTypeParapheur'])
            );
        }

        $echangeDoc->setEchangeDocApplicationClient($echangeDocApplicationClient);
        $echangeDoc->setAgent($agent);
        $echangeDoc->setConsultation($consultation);
        if ($echangeDocApplicationClient->getEchangeDocApplication()->isEnvoiDic()) {
            $echangeDoc->setStatut($this->parameter->get('ECHANGE_DOC_STATUT_GENERATION_DIC'));
        } else {
            $echangeDoc->setStatut($this->parameter->get('ECHANGE_DOC_STATUT_A_ENVOYER'));
        }

        return $echangeDoc;
    }

    public function newEchangeDocBlob(
        string $stringFileToFormat,
        int $categoriePiece,
        ?array $fichierInfo = null
    ): EchangeDocBlob {
        $fichierInfo ??= $this->fileFormat($stringFileToFormat);
        $blobOrganismeFile = $this->em->getRepository(BloborganismeFile::class)->find($fichierInfo['idBlob']);
        if (!$blobOrganismeFile) {
            throw new Exception('No blobOrganismeFile found');
        }
        $echangeDocBlob = new EchangeDocBlob();
        $echangeDocBlob->setChemin($fichierInfo['chemin']);
        $echangeDocBlob->setCategoriePiece($categoriePiece);
        $poids = 0;
        $chucksum = null;
        if (null === $fichierInfo['chemin']) {
            $poids = $this->mountManager->getFileSize(
                $fichierInfo['idBlob'],
                $blobOrganismeFile->getOrganisme(),
                $this->em
            );

            $file = $this->mountManager->getAbsolutePath(
                $fichierInfo['idBlob'],
                $blobOrganismeFile->getOrganisme(),
                $this->em
            );

            $chucksum = md5_file($file);
        } else {
            $fichier = $this->mountManager->getAbsolutePath(
                $fichierInfo['idBlob'],
                $blobOrganismeFile->getOrganisme(),
                $this->em
            );

            if (file_exists($fichier)) {
                $dceZip = new ZipArchive();
                if (true === $dceZip->open($fichier)) {
                    $destination = $this->parameter->get('COMMON_TMP') . 'tmp_' . date('Ymdhis');
                    $fileExtract = $fichierInfo['chemin'];
                    $dceZip->extractTo($destination, $fileExtract);
                    $file = $destination . '/' . $fileExtract;
                    if (file_exists($file)) {
                        $poids = filesize($file);
                        $chucksum = md5_file($file);
                        $dceZip->close();
                        unlink($file);
                    }
                }
            }
        }

        $echangeDocBlob->setPoids($poids);
        $echangeDocBlob->setChecksum($chucksum);
        $fichierInfo['checksum'] = null;

        $echangeDocBlob->setBlobOrganisme($blobOrganismeFile);

        return $echangeDocBlob;
    }

    private function fileFormat($stringFileToFormat)
    {
        $tmp = explode('##', (string) $stringFileToFormat);

        $fichierInfo = [];
        if (str_contains((string) $stringFileToFormat, '##DCE##')) {
            $fichierInfo['idBlob'] = $this->encryption->decryptId($tmp[count($tmp) - 2]);
            $fichierInfo['chemin'] = $this->encryption->decryptId(end($tmp));
        } else {
            $fichierInfo['idBlob'] = $this->encryption->decryptId(end($tmp));
            $fichierInfo['chemin'] = null;
        }

        return $fichierInfo;
    }

    /**
     * @param string $codeStatut
     * @param null   $messageFonctionnel
     * @param null   $messageTechnique
     *
     *
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function changeStatut(
        EchangeDoc $echange,
        Agent $agent,
        $codeStatut = 'ENVOYE',
        $messageFonctionnel = null,
        $messageTechnique = null
    ): bool|DBALException|Exception {
        //update the statut and dateModification
        $echange->setStatut($codeStatut);

        //save in the table echange_doc_historique le nouveau statut
        $echangeDocHistorique = new EchangeDocHistorique();
        $echangeDocHistorique->setStatut($echange->getStatut());
        $echangeDocHistorique->setEchangeDoc($echange);
        $echangeDocHistorique->setMessageFonctionnel($messageFonctionnel);
        $echangeDocHistorique->setMessageTechnique($messageTechnique);
        $echangeDocHistorique->setAgent($agent);

        $reponse = true;
        try {
            $this->em->persist($echange);
            $this->em->persist($echangeDocHistorique);
            $this->em->flush();
        } catch (DBALException $e) {
            $this->espacedocLogger->error('Problème lors de la mise à jour du status de l\'échange : ' .
                $echange->getId() . 'Stack Erreur : ' . $e->getMessage());

            return $e;
        }

        return $reponse;
    }

    /**
     * @param Request $request
     * @param int $id
     * @param int $echangeId
     *
     * @return StreamedResponse
     *
     * @throws Exception
     */
    public function getFile(Request $request, int $id, int $echangeId): StreamedResponse
    {
        $result = null;
        $errorMessage = sprintf(
            'Impossible de récupérer le fichier blob ayant pour id=%s et idEchange=%s',
            $id,
            $echangeId,
        );

        try {
            $documentInfo = $this->em->getRepository(EchangeDocBlob::class)
                ->getDocumentIfExists($id, $echangeId);


            if (!empty($documentInfo)) {
                $filePath = $this->mountManager->getAbsolutePath(
                    $documentInfo['blobOrganismeId'],
                    $documentInfo['organisme'],
                    $this->em
                );

                $this->espacedocLogger->info('Récupération du chemin du fichier.');
                if (file_exists($filePath)) {
                    $this->espacedocLogger->info('Fichier existe et il va être renvoyé en streaming.');

                    if (empty($documentInfo[self::CHEMIN]) || null === $documentInfo[self::CHEMIN]) {
                        $result = $this->returnStreamedFile($request, $filePath, $documentInfo['name']);
                    } else {
                        $this->espacedocLogger->info('Le fichier existe dans un ZIP.');
                        $zipFile = new ZipArchive();

                        if (true === $zipFile->open($filePath)) {
                            $destination = $this->parameter->get('COMMON_TMP') . 'tmp_' . date('Ymdhis');
                            $zipFile->extractTo($destination);
                            $file = $destination . '/' . $documentInfo[self::CHEMIN];

                            if (file_exists($file) && is_file($file)) {
                                $zipFile->close();
                                //get the name of the file
                                $result =
                                    $this->returnStreamedFile($request, $file, basename($documentInfo[self::CHEMIN]));
                            }
                        }
                    }
                }
            }
        } catch (Exception $exception) {
            $this->espacedocLogger->info($errorMessage);
            throw new Exception($errorMessage);
        }

        if (!$result instanceof StreamedResponse) {
            $this->espacedocLogger->info($errorMessage);
            throw new Exception($errorMessage);
        }

        return $result;
    }

    /**
     * @param $request
     * @param $fichier
     * @param $name
     *
     * @return StreamedResponse
     */
    private function returnStreamedFile($request, $fichier, $name)
    {
        $response = new StreamedResponse(
            function () use ($fichier) {
                echo file_get_contents($fichier);
            }
        );

        $response->headers->set('Content-Type', "'" . mime_content_type($fichier) . "'");
        $response->headers->set('Cache-Control', '');
        $response->headers->set('Content-Length', strlen((string) file_get_contents($fichier)));
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));

        $name = preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $name);
        $contentDisposition = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $name
        );

        $response->headers->set('Content-Disposition', $contentDisposition);
        $response->prepare($request);

        return $response;
    }

    public function createAutomatedEchanges(int $consultationId): void
    {
        $consultation = $this->em->getRepository(Consultation::class)->find($consultationId);
        $applications = $this->em->getRepository(EchangeDocApplicationClient::class)
            ->getAvailabledApplicationClientForAutomatedEchanges($consultation->getOrganisme());

        try {
            foreach ($applications as $application) {
                $data = [
                    'application'   => $application->getId(),
                    'objet'         => $this->translator->trans('AUTOMATED_CONSULTATION_ARCHIVE'),
                    'description'   => $this->translator->trans('AUTOMATED_CONSULTATION_ARCHIVE_DESCRIPTION')
                ];
                $echangeDoc = $this->createEchangeDocumentaire($data, $consultationId);

                $this->bus->dispatch(new GenerateArchive($echangeDoc));
            }
        } catch (Exception $e) {
            $this->espacedocLogger->error($e->getMessage());
        }
    }
}
