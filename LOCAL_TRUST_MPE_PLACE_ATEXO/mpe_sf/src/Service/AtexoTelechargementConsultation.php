<?php

namespace App\Service;

use App\Entity\AnnonceBoamp;
use App\Entity\Avis;
use App\Entity\ConfigurationPlateforme;
use App\Entity\DCE;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Service  symfony de stokage.
 *
 * @author Fernando Lozano  <fernando.lozano@atexo.com>
 * @copyright Atexo 2017
 */
class AtexoTelechargementConsultation
{
    private ?object $atexoFichierOrganisme = null;

    /**
     * AtexoTelechargementConsultation constructor.
     *
     *                                      constructeur
     */
    public function __construct(private ContainerInterface $container, private TranslatorInterface $translator, private AtexoFichierOrganisme $atexoFichierOrganisme2)
    {
        $this->atexoFichierOrganisme = $this->atexoFichierOrganisme2;
    }

    /**************************     Setters /  Modification d'information  ************************************/

    /**
     * @author Fernando Lozano <fernando.lozano@atexo.com>
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /*************************     Getters  /  recuperation  d'information  ***********************************/

    /**
     * @return mixed
     *
     * @author Fernando Lozano <fernando.lozano@atexo.com>
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $idref
     * @param $organisme
     *
     *                  Fernando Lozano
     */
    public function getFileSize($idref, $organisme): bool|int
    {
        $size = 'false';
        if (!empty($organisme) and !empty($idref)) {
            $size = $this->atexoFichierOrganisme->getFileSize($idref, $organisme);
        }

        return $size;
    }

    public function getFileSizeFormatted($idref, $organisme)
    {
        $size = $this->getFileSize($idref, $organisme);

        return ($size < 1024) ? str_replace(
            '.',
            ',',
            round($size, 2)
        ).' '.$this->translator->trans('TEXT_KILO_OCTET') :
            str_replace(
                '.',
                ',',
                round($size / 1024, 2)
            ).' '.$this->translator->trans('TEXT_MEGA_OCTET');
    }

    public function atexoHtmlEntities($str)
    {
        return htmlspecialchars($str, ENT_NOQUOTES | ENT_QUOTES, $this->getParameter('HTTP_ENCODING'));
    }

    /**
     * Convertit une datetime ISO en date française.
     *
     * @param $_isoDate date au format yyyy-mm-dd hh:mm:ss
     *
     * @return date au format dd/mm/yyyy
     */
    public function iso2frnDateTime($_isoDate, $withTime = true, $withTimeSeconds = false)
    {
        if (!$_isoDate || '0000-00-00 00:00:00' == $_isoDate) {
            return;
        }
        if (10 == strlen((string) $_isoDate)) {
            return self::iso2frnDate($_isoDate);
        }

        [$a, $m, $j_et_heure] = explode('-', (string) $_isoDate);
        [$j, $heure] = explode(' ', $j_et_heure);
        [$h, $min, $sec] = explode(':', $heure);
        $date = "$j/$m/$a";
        if ($withTime) {
            $date = "$j/$m/$a $h:$min";
        }
        if ($withTimeSeconds) {
            $sec = empty($sec) ? '00' : $sec;
            $date = "$j/$m/$a $h:$min:$sec";
        }

        return $date;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->getContainer()->getParameter($name);
    }

    /**
     * Permet de determiner le nom à donner à l'avis de publicité à envoyer à la presse.
     *
     * @param $consultationId : la référence de la consultation
     * @param $idAvis : l'identifiant de l'avis de publicité (allusion à la table avis_pub)
     *
     * @return : nom de l'avis presse
     */
    public function displayAvisPubliciteFormatLibre($consultationId, $organisme)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $listeFormAvis = $doctrine->getRepository(Avis::class)
            ->retrieveListAvisEnvoye($consultationId, $organisme);
        $isEnabled = $doctrine->getRepository(ConfigurationPlateforme::class)->getConfigurationPlateforme();
        $newFormLibre = ['id' => '',
            'libelleType' => '',
            'dateCreation' => '',
            'destinataire' => '',
            'satatus' => '',
            'dateEnvoi' => '',
            'datePub' => '',
            'typeFormat' => '',
            'url' => '',
            'taille' => '',
            'type' => '',
        ];
        $dataSource = [];
        $index = 0;
        foreach ($listeFormAvis as $oneAvis) {
            if ($oneAvis->getType() == $this->getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT') ||
                $oneAvis->getType() == $this->getParameter('TYPE_FORMAT_PDF_TED') ||
                $oneAvis->getType() == $this->getParameter('TYPE_FORMAT_AVIS_IMPORTES')
            ) {
                if ($oneAvis->getTypeDocGenere() == $this->getParameter('TYPE_DOC_GENERE_AVIS')) {
                    if ($isEnabled->getPubliciteOpoce()) {
                        if ($oneAvis->getType() == $this->getParameter('TYPE_FORMAT_PDF_TED')) {
                            $newFormLibre['libelleType'] = $this->translator->trans('TEXT_AVIS_EUROPEEN_FICHIER_JOINT');
                        } elseif ($oneAvis->getType() == $this->getParameter('TYPE_FORMAT_PDF_TED_HORS_PF')) {
                            $newFormLibre['libelleType'] = $this->translator->trans('TEXT_AVIS_EUROPEEN_FICHIER_JOINT');
                        } elseif ($oneAvis->getType() == $this->getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                            $newFormLibre['libelleType'] = $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_PUB');
                        } else {
                            $newFormLibre['libelleType'] = $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_PRESSE');
                        }
                    } else {
                        $newFormLibre['libelleType'] = $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_PUBLICITE').
                            ' - '.strtoupper($oneAvis->getLangue());
                    }
                } else {
                    $newFormLibre['libelleType'] = $this->translator->trans('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE');
                }

                $newFormLibre['typeFormat'] = $this->getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT');

                $taille = $this->getFileSize($oneAvis->getAvis(), $organisme) / 1024;
                $newFormLibre['taille'] = $taille;
            } else {
                $newFormLibre['libelleType'] = $this->translator->trans('TEXT_URL_ACCES_DIRECT');
                $newFormLibre['typeFormat'] = $this->getParameter('TYPE_FORMAT_LIBRE_URL');
                $newFormLibre['typeFormat'] = $oneAvis->getUrl();
            }

            $newFormLibre['libelleType'] = $oneAvis->getId();

            $newFormLibre['datePub'] = $oneAvis->getDateCreation();
            $newFormLibre['satatus'] = $oneAvis->getStatut();

            $dataSource[$index] = $newFormLibre;
            ++$index;
        }

        // Ajouter les liens PDF au BOAMP dans le data source
        $listLienPdfBoamp = $doctrine->getRepository(AnnonceBoamp::class)
            ->retrieveAnnonceByRefCons($consultationId, $organisme);
        $dataSourceBoamp = [];
        $indexX = 0;

        foreach ($listLienPdfBoamp as $oneAnnonce) {
            if (!empty($oneAnnonce->getLienBoamp())) {
                $dataSourceBoamp['libelleType'][$indexX] = $this->translator->trans('NOM_LIEN_AVIS_BOAMP');
                $dataSourceBoamp['typeFormat'][$indexX] = $this->getParameter('TYPE_FORMAT_URL_BOAMP');
                $dataSourceBoamp['url'][$indexX] = $oneAnnonce->getLienBoamp();
                $dataSourceBoamp['status'][$indexX] = $oneAnnonce->getStatutDestinataire();

                ++$indexX;
            }
        }

        // Ajouter des liens de publication de l'OPOCE
        $listeLiensAvisPub = Atexo_Publicite_AvisPub::retrieveFormXmlDestPubByRefCons($consultationId, $organisme);
        $dataLiensAvisPub = [];

        $increment = 0;

        foreach ($listeLiensAvisPub as $lienPub) {
            $dataLiensAvisPub['libelleType'][$increment] = $this->translator->trans('NOM_LIEN_AVIS_TED');
            $dataLiensAvisPub['typeFormat'][$increment] = $this->getParameter('TYPE_FORMAT_URL_TED');
            $dataLiensAvisPub['url'][$increment] = $lienPub['lien_pub'];
            $dataLiensAvisPub['status'][$increment] = $lienPub['statut_destinataire'];
            ++$increment;
        }

        // Publicite SUB BOAMP
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
            Atexo_Config::getParameter('CONST_READ_ONLY'));

        $dataSourceBoampSUB = [];
        $supportPubBoamp = CommonTSupportPublicationQuery::create()
            ->findOneByCode(Atexo_Config::getParameter('CODE_SUPPORT_PUBLICATION_BOAMP'), $connexion);
        if ($supportPubBoamp instanceof CommonTSupportPublication) {
            $listeAnnoncesSub = Atexo_Publicite_AnnonceSub::getSupportAnnonceByRefConsAndSupportPub(
                $consultationId,
                $supportPubBoamp->getId(),
                Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE'),
                $connexion
            );
            $i = 0;
            if (is_array($listeAnnoncesSub) && count($listeAnnoncesSub)) {
                foreach ($listeAnnoncesSub as $annonceSub) {
                    if ($annonceSub instanceof CommonTSupportAnnonceConsultation) {
                        $dataSourceBoampSUB[$i] = $this->getObjectFormatLibreVo(
                            prado::localize('NOM_LIEN_AVIS_BOAMP'),
                            Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP'),
                            Atexo_Config::getParameter('URL_SUPPORT_PUBLICATION_BOAMP').$annonceSub->getNumeroAvis()
                        );
                        ++$i;
                    }
                }
            }
        }
    }

    /**
     * @param $idAvis
     *
     * @return int
     */
    public function getAvisFileSize($idAvis)
    {
        $avis = $this
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Avis::class)
            ->find($idAvis);

        $dir = $this->getAvisFilePath($avis->getId());
        $filename = $this->getAvisFileName($avis->getId());
        $filePath = $dir.$filename;

        return @filesize($filePath);
    }

    /**
     * @param $idAvis
     *
     * @return string
     */
    public function getAvisFileName($idAvis)
    {
        $avis = $this
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Avis::class)
            ->find($idAvis)
        ;

        return $avis->getAvis().'-0';
    }

    /**
     * @param $idAvis
     *
     * @return string
     */
    public function getAvisFilePath($idAvis)
    {
        $avis = $this
            ->getContainer()
            ->get('doctrine')
            ->getRepository(Avis::class)
            ->find($idAvis);
        $path = $this->getContainer()->get('kernel')->getBaseRootDir().$avis->getOrganisme().'/files/';

        if (null !== $avis->getChemin()) {
            $path = $this->getContainer()->get('kernel')->getBaseRootDir().$avis->getChemin().'/files/';
        }

        return $path;
    }

    /**
     * @param $dce
     * @param $organisme
     *
     * @return string
     */
    public function getTailleDCE($dce, $organisme)
    {
        $atexoUtil = $this->getContainer()->get('atexo.atexo_util');

        if ($dce instanceof DCE) {
            return $dce->getTailleDce() ?
                $atexoUtil->getSizeName($dce->getTailleDce()) :
                $atexoUtil->arrondirSizeFile(
                    $this->getFileSize($dce->getDce(), $organisme) / 1024
                );
        }

        return '';
    }
}
