<?php

namespace App\Service;

use DateTime;
use Exception;
use Doctrine\ORM\OptimisticLockException;
use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Entity\Organisme\ServiceMetier;
use App\Entity\Service;
use App\Exception\ApiProblemAlreadyExistException;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\ApiProblemOrganismePoolException;
use App\Service\Agent\AgentServiceMetierService;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrganismeService
{
    /**
     * OrganismeService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly EntityManagerInterface $em,
        private readonly MarchePublieService $marchePublieService,
        private readonly ConfigurationOrganismeService $configurationOrganismeService,
        private readonly AtexoAgent $agentService,
        private readonly SocleHabilitationAgentService $socleHabilitationAgentService,
        private readonly AgentServiceMetierService $agentServiceMetierService,
        private readonly LoggerInterface $logger
    ) {
    }

    public function exists(string $acronymeOrganisme): bool
    {
        $organisme = $this->em->getRepository(Organisme::class)
            ->findOneBy(['acronyme' => $acronymeOrganisme]);

        return !empty($organisme);
    }

    /**
     * Permet la création d'un organisme depuis le pool.
     */
    public function create(Organisme $organisme): Organisme
    {
        try {
            $this->em->beginTransaction();
            $administrateur = $this->em->getRepository(Administrateur::class)
                ->getRandomAvailableOrganisme();

            //Si l'organisme n'existe pas dans le pool, on en prend un au hasard.
            if (!$administrateur instanceof Administrateur) {
                throw new ApiProblemOrganismePoolException();
            }
            $acronyme = $administrateur->getLogin();
            $sigle = $organisme->getSigle();
            $criteria = [
                'acronyme' => $acronyme,
            ];
            $organismeExists = $this->em->getRepository(Organisme::class)->findOneBy($criteria);
            if (!empty($organismeExists)) {
                $msg = sprintf(
                    "L'organisme %s existe déjà.",
                    $acronyme
                );

                throw new ApiProblemAlreadyExistException($msg);
            }
            $organisme->setAcronyme($acronyme);
            $organisme->setSigle($sigle);
            $datetimeCreation = new DateTime();
            $organisme->setDateCreation($datetimeCreation);
//            // dans l'ancien fonctionnement on pouvait mettre à jour cette valeur
//            // cependant le xsd actuel ne le permet pas donc nous mettons à 1 par défaut
            $organisme->setActive('1');

            $administrateur->setOrganisme($organisme);
            $organisme->addAdministrateur($administrateur);
            $this->em->persist($organisme);
            $this->em->persist($administrateur);
            $this->em->flush();

            $idServiceMetierMpe = $this->container->getParameter('SERVICE_METIER_MPE');
            $idServiceMetierSocle = $this->container->getParameter('SERVICE_METIER_SOCLE');

            $this->marchePublieService->create($datetimeCreation->format('Y'), $acronyme);

            $this->configurationOrganismeService
                ->copierConfigurationOrganisme($this->container->getParameter('ORGANISME_REFERENCE'), $acronyme);

            $serviceMetierSocle = $this->em->getRepository(ServiceMetier::class)->find($idServiceMetierSocle);

            $isSocle = false;
            $idServiceMetierToUse = $idServiceMetierMpe;
            if ($serviceMetierSocle instanceof ServiceMetier and 'SOCLE' == $serviceMetierSocle->getSigle()) {
                $isSocle = true;
                $idServiceMetierToUse = $idServiceMetierSocle;
            }

            $this->em->getRepository(Organisme::class)->syncTable(
                $this->container->getParameter('ORGANISME_REFERENCE'),
                $acronyme,
                $idServiceMetierToUse,
                $this->container->getParameter('PATH_INIT_SCRIPT_ORG')
            );
            $nbOrganisme = $this->em->getRepository(Organisme::class)->getNumberOfCreatedOrganisme();

            // Si c'est le premier organisme, on créé un hyperadmin
            if (1 == $nbOrganisme) {
                $hyperAdmin = $this->agentService->createHyperAdmin($organisme);
                $this->socleHabilitationAgentService->createForAdmin($hyperAdmin);
                $this->agentServiceMetierService->createForAdmin(
                    $hyperAdmin->getId(),
                    $idServiceMetierMpe,
                    1
                );
                if ($isSocle) {
                    $this->agentServiceMetierService->createForAdmin(
                        $hyperAdmin->getId(),
                        $idServiceMetierSocle,
                        12
                    );
                }
            }

            $admin = $this->agentService->createAdmin($organisme, $administrateur->getMdp());
            $this->socleHabilitationAgentService->createForAdmin($admin);
            $this->agentServiceMetierService->createForAdmin(
                $admin->getId(),
                $idServiceMetierMpe,
                1
            );
            if ($isSocle) {
                $this->agentServiceMetierService->createForAdmin(
                    $admin->getId(),
                    $idServiceMetierSocle,
                    12
                );
            }

            $this->em->commit();

            return $organisme;
        } catch (ApiProblemOrganismePoolException | ApiProblemAlreadyExistException $e) {
            $this->em->rollback();
            throw $e;
        } catch (UniqueConstraintViolationException $e) {
            $this->em->rollback();
            $msg = "Impossible de créer l'organisme %s. ";
            $msg .= 'Des données rattachées à cet acronyme existent déjà en base de données.';
            $msg = sprintf($msg, $acronyme);
            $this->logger->error($e->getMessage());
            throw new ApiProblemAlreadyExistException($msg);
        } catch (Exception $ex) {
            $this->em->rollback();
            throw $ex;
        }
    }

    /**
     * Permet la mise à jour d'un organisme.
     *
     * @throws OptimisticLockException
     */
    public function update(Organisme $organisme): Organisme
    {
        $organismeInDb = $this->em
            ->getRepository(Organisme::class)
            ->findOneBy(['acronyme' => $organisme->getAcronyme()]);
        if (!$organismeInDb instanceof Organisme) {
            throw new ApiProblemNotFoundException();
        }

        $organismeInDb = $this->sync($organisme, $organismeInDb);
        $this->em->persist($organismeInDb);
        $this->em->flush();

        return $organisme;
    }

    public function sync(Organisme $organismeToUpdate, Organisme $organismeInDb)
    {
        $organismeInDb->setDenominationOrg($organismeToUpdate->getDenominationOrg());
        $organismeInDb->setSigle($organismeToUpdate->getSigle());
        $organismeInDb->setDescriptionOrg($organismeToUpdate->getDescriptionOrg());
        $organismeInDb->setCategorieInsee($organismeToUpdate->getCategorieInsee());
        $organismeInDb->setSiren($organismeToUpdate->getSiren());
        $organismeInDb->setComplement($organismeToUpdate->getComplement());
        $organismeInDb->setAdresse($organismeToUpdate->getAdresse());
        $organismeInDb->setCp($organismeToUpdate->getCp());
        $organismeInDb->setVille($organismeToUpdate->getVille());
        $organismeInDb->setPays($organismeToUpdate->getPays());
        $organismeInDb->setTypeArticleOrg($organismeToUpdate->getTypeArticleOrg());
        $organismeInDb->setUrl($organismeToUpdate->getUrl());
        $organismeInDb->setTel($organismeToUpdate->getTel());
        $organismeInDb->setTelecopie($organismeToUpdate->getTelecopie());
        $organismeInDb->setIdExterne($organismeToUpdate->getIdExterne());
        $organismeInDb->setIdEntite($organismeToUpdate->getIdEntite());

        return $organismeInDb;
    }

    public function getNomCourantAcheteurPublic(Agent $agent): string
    {
        $organisme = $agent->getOrganisme();

        $configurationOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
            ->findOneBy(['organisme' => $organisme->getAcronyme()]);

        $str = sprintf(
            '%s (%s - %s)',
            $organisme->getDenominationOrg(),
            $organisme->getCp(),
            $organisme->getVille()
        );
        if (!empty($configurationOrganisme) && $configurationOrganisme->getOrganisationCentralisee()) {
            $serviceId = $agent->getServiceId();

            if (!empty($serviceId)) {
                $service = $this->em->getRepository(Service::class)
                    ->findOneBy(['organisme' => $organisme->getAcronyme(), 'id' => $serviceId]);

                $str = sprintf(
                    '%s - %s (%s - %s)',
                    $organisme->getDenominationOrg(),
                    $service->getLibelle(),
                    $service->getCp(),
                    $service->getVille()
                );
            }
        }

        return $str;
    }
}
