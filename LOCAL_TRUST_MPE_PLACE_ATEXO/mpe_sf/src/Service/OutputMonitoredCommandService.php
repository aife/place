<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use Symfony\Component\Console\Output\ConsoleOutput;

class OutputMonitoredCommandService extends ConsoleOutput
{
    private string $commandOutput = '';

    public function write($messages, $newline = false, $options = 0)
    {
        $this->commandOutput .= $messages . ($newline ? \PHP_EOL : '');
        parent::write($messages, $options);
    }

    public function getCommandOutput(): string
    {
        return $this->commandOutput;
    }
}
