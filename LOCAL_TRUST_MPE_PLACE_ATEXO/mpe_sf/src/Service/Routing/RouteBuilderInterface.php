<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Routing;

interface RouteBuilderInterface
{
    /**
     * @param string $name
     * @param array $arguments
     * @return string
     */
    public function getRoute(string $name, array $arguments = []): string;
}
