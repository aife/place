<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Routing;

use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RouteBuilder implements RouteBuilderInterface
{
    /**
     * RouteBuilder constructor.
     */
    public function __construct(
        protected UrlGeneratorInterface $router,
    ) {
    }

    /**
     *
     * @throws RouteNotFoundException
     */
    public function getRoute(string $name, array $arguments = []): string
    {
        return $this->router->generate($name, $arguments);
    }
}
