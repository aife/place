<?php

namespace App\Service\Handler;

use App\Service\Agent\Guests;
use Exception;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Event\ValidateResponseEvent;
use App\Service\AtexoConfiguration;
use App\Service\Email\AtexoMailManager;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

/**
 * Mohamed BLAL.
 *
 * Class EmailAgentHandler
 */
class EmailAgentHandler extends EmailHandler
{
    public final const ACCUSE_RECEPTION_REPONSE_ENTREPRISE =
        'email/ar-reponse-agent.html.twig';
    public final const ENVOI_CODE_PIN_SGMAP_MPS_CONSULTATION =
        'email/envoi-code-pin-sgmap-mps-agent.html.twig';
    public final const PARTIAL_URL_TABLEAU_DE_BORD =
        '?page=Agent.TableauDeBord&AS=0&id=';
    private readonly LoggerInterface $logger;

    private AtexoMailManager $mailManager;

    public function __construct(
        AtexoConfiguration $stateModuleChecker,
        SwiftMessageFactory $messageFactory,
        MailSender $mailSender,
        ValidatorInterface $validator,
        Option $options,
        EntityManagerInterface $entityManager,
        AtexoCrypto $crypto,
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger,
        Environment $twig,
        private Guests $agentGuests
    ) {
        parent::__construct(
            $stateModuleChecker,
            $messageFactory,
            $mailSender,
            $validator,
            $options,
            $entityManager,
            $crypto,
            $parameterBag
        );
        $this->mailManager = new AtexoMailManager(
            $twig,
            $mailSender,
            $logger,
            'email/ar-reponse-agent.html.twig',
            [],
            ''
        );
        $this->logger = $logger;
    }

    public function setMailManager(AtexoMailManager $mailManager): void
    {
        $this->mailManager = $mailManager;
    }

    public function handle(ValidateResponseEvent $event)
    {
        $consultation = $event->getConsultation();
        $isPubliciteOpoceEnabled = $this->stateModuleChecker->hasConfigPlateforme('publiciteOpoce');
        $isAnnoncesMarchesEnabled = $this->stateModuleChecker->hasConfigPlateforme('annoncesMarches');
        $inscrit = $event->getCandidate();
        $response = $event->getResponse();
        $agentLocale = $this->parameterBag->get('AGENT_LOCALE');
        $agentsFavoris = [];
        try {
            $guests = $this->agentGuests->getAllGuestsByConsulation($consultation);
            $consultationFavoris = $this->entityManager->getRepository(ConsultationFavoris::class)
                ->findBy([
                    'consultation' => $consultation->getId(),
                ]);
            foreach ($consultationFavoris as $cf) {
                $agentsFavoris[] = $cf->getAgent()->getId();
            }
        } catch (Exception $exception) {
            $this->logger
                ->error(
                    'Problème de récupèration des agents sur la table "consultation_favoris". Exception : '.
                    $exception->__toString()
                );
        }
        $agents = $this->entityManager->getRepository(Agent::class)
            ->getAgentsWhoWantBeAlertedElectronicResponse($agentsFavoris);

        $listSendMail = [];
        foreach ($agents as $agent) {
            if (in_array($agent->getId(), $guests)) {
                $pfType = $this->parameterBag->get('PF_TYPE');
                $pfUrlAgent = '';
                if ('HM' == $pfType) {
                    try {
                        $pfUrlAgent = $this->parameterBag->get(
                            'PF_URL_AGENT_'.str_replace('-', '_', $consultation->getAcronymeOrg())
                        );
                    } catch (Exception) {
                        $pfUrlAgent = $this->parameterBag->get('PF_URL_AGENT');
                    }
                }
                $pfReference = $this->parameterBag->get('PF_URL_REFERENCE');

                $url = $this->getDefaultUrlAgent(
                    $consultation,
                    $agent,
                    $pfType,
                    $pfReference,
                    $pfUrlAgent
                );
                /** @var Agent $agent */
                $urlConsultation = $url.self::PARTIAL_URL_TABLEAU_DE_BORD.$consultation->getId();
                // Si l'email n'est pas valide, on ne souhaite pas envoyer un email sinon ça bloque le processus
                // aussi on gère les mail en doublons !
                if (
                    !$this->verificationEmailAgent($agent->getEmail()) ||
                    in_array($agent->getId(), $listSendMail)
                ) {
                    continue;
                }
                $pfName = $this->parameterBag->get('PF_LONG_NAME');
                if ($consultation->getPlateformeVirtuelle()) {
                    $options['from'] = $consultation->getPlateformeVirtuelle()->getNoReply();
                    $options['addressFromLabel'] = $consultation->getPlateformeVirtuelle()->getFromPfName();
                    $pfName = $consultation->getPlateformeVirtuelle()->getFromPfName();
                }

                $listSendMail[] = $agent->getId();
                $dataModelAgentEmail = [
                    'fromAgent' => true,
                    'isPubliciteOpoceEnabled' => $isPubliciteOpoceEnabled,
                    'isAnnoncesMarchesEnabled' => $isAnnoncesMarchesEnabled,
                    'inscrit' => $inscrit,
                    'consultation' => $consultation,
                    'typeProcedureOrganisme' => $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                        ->findOneBy([
                            'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                            'organisme' => $consultation->getOrganisme(),
                        ]),
                    'urlConsultation' => $urlConsultation,
                    'response' => $response,
                    'urlBandeauImage' => $this->getUrlImageBandeau(
                        $this->parameterBag->get('PF_URL_REFERENCE'),
                        $consultation->getPlateformeVirtuelle(),
                        $this->parameterBag->get('SOURCE_IMAGE_BANDEAU')
                    ),
                    'agent' => $agent,
                    'plateforme' => $pfName,
                    'isPdf' => false,
                    'listeFichiers' => [],
                ];

                $options = $this->mailManager->getOptions();
                $options['from'] = $this->options->addressFrom;

                $this->options->translator->setLocale($agentLocale);

                $options['subject'] = $this->options->translator
                        ->trans('RECEPTION_REPONSE_ELECRONIQUE', [], null, $agentLocale).
                    ' - '.$this->options->translator->trans('TEXT_REF', [], null, $agentLocale).
                    ' : '.$event->getConsultation()->getReferenceUtilisateur();
                $this->mailManager->setOptions($options);

                $this->mailManager->sendMail($agent->getEmail(), $dataModelAgentEmail);
            }
        }
    }

    /**
     * @param $event
     *
     * @return array
     */
    protected function getAllConcernedAgents($event)
    {
        $centralizedManagement = $this->stateModuleChecker
            ->hasConfigOrganisme($event->getConsultation()->getOrganisme()->getAcronyme(), 'OrganisationCentralisee');
        $invitedAgents = $this->entityManager->getRepository(Agent::class)->getInvitedAgents($event->getConsultation());
        $permanentsAgents = $this->entityManager->getRepository(Agent::class)
            ->getPermanentsAgent($event->getConsultation(), $centralizedManagement);
        $list = array_merge($invitedAgents, $permanentsAgents);

        return $list;
    }

    /**
     * Fonction centralisé pour vérifier l'email d'un agent dans cette classe.
     *
     * @param $email
     */
    public function verificationEmailAgent($email)
    {
        return $this->verificationEmail($email, 'agent');
    }
}
