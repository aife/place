<?php

namespace App\Service\Handler;

use App\Entity\CandidatureMps;
use App\Entity\EnveloppeFichier;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use App\Event\ValidateResponseEvent;
use App\Service\AtexoConfiguration;
use App\Service\AtexoFichierOrganisme;
use App\Service\Email\AtexoMailManager;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Exception\ServiceException;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Snappy\Pdf;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

/**
 * @author Mohamed BLAL
 * Class EmailCompanyHandler
 */
class EmailCompanyHandler extends EmailHandler
{
    public final const ACCUSE_RECEPTION_REPONSE_ENTREPRISE =
        'email/ar-reponse-entreprise.html.twig';

    private AtexoMailManager $mailManager;

    private readonly LoggerInterface $logger;

    public function __construct(
        AtexoConfiguration $stateModuleChecker,
        SwiftMessageFactory $messageFactory,
        MailSender $mailSender,
        ValidatorInterface $validator,
        Option $options,
        EntityManagerInterface $entityManager,
        AtexoCrypto $crypto,
        ParameterBagInterface $parameterBag,
        private readonly Pdf $knpSnappyPdfGenerator,
        LoggerInterface $logger,
        Environment $twig,
        /**
         * @var
         */
        private readonly AtexoFichierOrganisme $blobHandler
    ) {
        parent::__construct(
            $stateModuleChecker,
            $messageFactory,
            $mailSender,
            $validator,
            $options,
            $entityManager,
            $crypto,
            $parameterBag
        );
        $this->mailManager = new AtexoMailManager(
            $twig,
            $mailSender,
            $logger,
            'email/ar-reponse-entreprise.html.twig',
            [],
            ''
        );
        $this->logger = $logger;
    }

    /**
     * @return int
     */
    public function handle(ValidateResponseEvent $event)
    {
        $candidatureMps = [];
        $handled = false;
        $idBlobPjRecapMail = 0;
        $numeroSN = '';
        $listeDumeGroupement = null;

        if (
            (bool) $event->getConsultation()->getMarchePublicSimplifie() &&
            $this->stateModuleChecker->hasConfigOrganisme(
                'marchePublicSimplifie',
                $event->getConsultation()->getAcronymeOrg()
            ) &&
            !empty($event->getIdCandidatureMps())
        ) {
            $candidatureMps = $this->entityManager->getRepository(CandidatureMps::class)->getCandidatureMpsWithoutOffer(
                $event->getIdCandidatureMps(),
                $event->getConsultation(),
                $event->getResponse(),
                $event->getCandidate()
            );
        }

        $listeFichiers = $this->entityManager->getRepository(EnveloppeFichier::class)
            ->getListeFichiersDepot($event->getResponse(), $this->parameterBag->get('type_enveloppe'));

        $candidature = $this->entityManager->getRepository(TCandidature::class)->findByIdOffre(
            $event->getResponse()->getId()
        );

        if ($candidature) {
            $candidature = $candidature[0];

            $dumeNumero = $this->entityManager->getRepository(TDumeNumero::class)->findOneBy(
                ['idDumeContexte' => $candidature->getIdDumeContexte()]
            );

            if (
                null != $dumeNumero &&
                'dume' == $candidature->getTypeCandidature() &&
                'online' == $candidature->getTypeCandidatureDume()
            ) {
                $numeroSN = $dumeNumero->getNumeroDumeNational();
            }

            $configGroupement = $this->stateModuleChecker->hasConfigPlateforme('groupement');
            $idEntreprise = $event->getResponse()->getEntrepriseId();
            $idOffre = $event->getResponse()->getId();
            if ($configGroupement && $idEntreprise) {
                $listeDumeGroupement = $this->entityManager->getRepository(
                    TDumeNumero::class
                )->getDumeNumeroFromGroupement($idOffre, $idEntreprise);
            }
        }

        $pfName = $this->parameterBag->get('PF_LONG_NAME');
        $pfUrlReference = $this->parameterBag->get('PF_URL_REFERENCE');
        $sourceImageBandeau = $this->parameterBag->get('SOURCE_IMAGE_BANDEAU');

        if ($plateformeVirtuelle = $event->getResponse()->getPlateformeVirtuelle()) {
            $this->options->addressFrom = $plateformeVirtuelle->getNoReply();
            $this->options->addressFromLabel = $plateformeVirtuelle->getFromPfName();
            $pfName = $plateformeVirtuelle->getFromPfName();
        }

        $consultation = $event->getConsultation();

        $dataCompanyEmail = [
            'fromAgent' => false,
            'isPubliciteOpoceEnabled' => $this->stateModuleChecker->hasConfigPlateforme('publiciteOpoce'),
            'isAnnoncesMarchesEnabled' => $this->stateModuleChecker->hasConfigPlateforme('annoncesMarches'),
            'inscrit' => $event->getCandidate(),
            'consultation' => $consultation,
            'typeProcedureOrganisme' => $this->entityManager->getRepository(TypeProcedureOrganisme::class)
                ->findOneBy([
                             'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                             'organisme' => $consultation->getOrganisme(),
                         ]),
            'response' => $event->getResponse(),
            'listeFichiers' => $listeFichiers,
            'plateforme' => $pfName,
            'candidature' => $candidature,
            'numeroSN' => $numeroSN,
            'dumeGroupements' => $listeDumeGroupement,
            'urlBandeauImage' => $this->getUrlImageBandeauPlateformeVirtuelle(
                $plateformeVirtuelle,
                $pfUrlReference,
                $sourceImageBandeau
            ),
            'pfUrlReference' => $this->getDomainePlateforme(
                $plateformeVirtuelle,
                $pfUrlReference
            ),
        ];

        if (!empty($candidatureMps)) {
            $dataCompanyEmail['candidatureMps'] = $candidatureMps[0]->getFileSize();
        }

        $dateDepot = $event->getResponse()->getUntrusteddate();
        $htmlFileName = $event->getConsultation()->getReferenceUtilisateur() . '_' .
            str_replace(['-', ':', ' '], ['', '', ''], $dateDepot->format('Y-m-d H:i:s')) . '_' .
            $event->getResponse()->getId() . '.html';
        $htmlFileName = str_replace(' ', '', $htmlFileName);
        $pdfFileName = preg_replace('"\.html"', 'Tmp.pdf', $htmlFileName);
        $pdfFileNameSigne = preg_replace('"\.html"', '.pdf', $htmlFileName);

        $emailSignataire = $event->getResponse()->getMailSignataire();
        $emailInscrit = $event->getCandidate()->getEmail();

        $emails =
            strtolower($emailInscrit) ===
            strtolower($emailSignataire) ? [$emailInscrit] : [$emailSignataire, $emailInscrit];

        $objet = $this->options->translator->trans('ACCUSE_RECEPTION_REPONSE_ELECTRONIQUE', [], null, 'fr') . ' - ' .
            $this->options->translator->trans('TEXT_REF', [], null, 'fr') . ' : ' .
            $event->getConsultation()->getReferenceUtilisateur();

        foreach ($emails as $email) {
            if (!$handled) {
                $handled = true;
                $this->sendToCompany($event, $email, $objet, $pdfFileName, $pdfFileNameSigne, $dataCompanyEmail);
            }
        }

        return $idBlobPjRecapMail;
    }

    protected function sendToCompany(
        ValidateResponseEvent $event,
        string $email,
        string $objet,
        string $pdfFileName,
        string $pdfFileNameSigne,
        array $dataCompanyEmail
    ) {
        $mailValid = $this->verificationEmail($email);
        $dataCompanyEmail['isPdf'] = true;
        $options = $this->mailManager->getOptions();
        $options['from'] = $this->options->addressFrom;
        $options['fromLabel'] = $this->options->addressFromLabel;
        $options['subject'] = $objet;
        $this->mailManager->setOptions($options);
        $finalPdfFileName = '';

        if ($this->parameterBag->get('PATH_BIN_WKHTMLPDF')) {
            $dataCompanyEmail['isPdf'] = true;

            $this->knpSnappyPdfGenerator->generateFromHtml(
                $this->mailManager->generateHtml($dataCompanyEmail)->getHtml(),
                $this->options->commonTmp . $pdfFileName,
                ['encoding' => $this->options->encoding],
                true
            );

            if ($this->options->signaturePdfMail) {
                $this->setSignaturePdf($pdfFileName, $pdfFileNameSigne, $objet);
            } else {
                copy($this->options->commonTmp . $pdfFileName, $this->options->commonTmp . $pdfFileNameSigne);
            }
            $finalPdfFileName = $pdfFileNameSigne;
            $pdfFileNameSigne = $this->blobHandler->moveTmpFile($this->options->commonTmp . $pdfFileNameSigne);
            $idBlobPjRecapMail = $this->blobHandler->insertFile(
                $pdfFileNameSigne,
                $this->options->commonTmp . $pdfFileNameSigne,
                $event->getConsultation()->getAcronymeOrg()
            );
            $event->getResponse()->setIdPdfEchangeAccuse($idBlobPjRecapMail);
            $this->entityManager->persist($event->getResponse());
            $this->entityManager->flush();
        }

        $hasPdf = false;

        if (
            is_file($this->options->commonTmp . $finalPdfFileName) &&
            filesize($this->options->commonTmp . $finalPdfFileName) > 0
        ) {
            $hasPdf = true;
        }

        $dataCompanyEmail['isPdf'] = !$hasPdf;

        $isSigned = ('1' == $this->options->isSigned);
        if (true === $mailValid) {
            if ($hasPdf) {
                $this->mailManager
                    ->sendMail(
                        $email,
                        $dataCompanyEmail,
                        $isSigned,
                        $this->options->commonTmp . $finalPdfFileName
                    );
            } else {
                $this->mailManager
                    ->sendMail($email, $dataCompanyEmail, $isSigned);
            }
        }
        @unlink($this->options->commonTmp . $pdfFileName);
    }

    protected function setSignaturePdf(string $pdfFileName, string $pdfFileNameSigne, string $objet)
    {
        //Signature pdf mail
        $nombreTentatif = $this->parameterBag->get('nombre_tentatif_signature_pdf_mail');
        $pdfFileNameContent = '';

        do {
            try {
                --$nombreTentatif;
                $pdfFileNameContent = $this->crypto->signerPades(
                    $this->options->signaturePdfMailCertificat,
                    $this->options->signaturePdfMailPassword,
                    $this->options->commonTmp . $pdfFileName
                );
            } catch (ServiceException $e) {
                $nombreTentatif = null;
                if (!$nombreTentatif) {
                    $this->sendMailToSupport($e, $this->options->commonTmp . $pdfFileName, $objet);
                    $this->logger->error($this->options->commonTmp . $pdfFileName . ' $objet');
                }
            }
        } while ($nombreTentatif);

        if (!empty($pdfFileNameContent)) {
            file_put_contents($this->options->commonTmp . $pdfFileNameSigne, $pdfFileNameContent);
        } else {
            copy(
                $this->options->commonTmp . $pdfFileName,
                $this->options->commonTmp . $pdfFileNameSigne
            );
        }
    }

    /**
     * @param $e
     * @param $pathPfFileName
     * @param $objet
     */
    public function sendMailToSupport($e, $pathPfFileName, $objet)
    {
        $emailBody = "Erreur lors de la signature du pdf ci-joint \n code erreur = " .
            $e->errorCode .
            "\n message erreur = " .
            $e->message;

        $this->mailManager->sendSimpleEmail(
            $this->parameterBag->get('mail_support_atexo'),
            'Probleme Signature pdf mail objet : ' . $objet,
            $emailBody
        );
    }

    public function setMailManager($mailManager)
    {
        $this->mailManager = $mailManager;
    }
}
