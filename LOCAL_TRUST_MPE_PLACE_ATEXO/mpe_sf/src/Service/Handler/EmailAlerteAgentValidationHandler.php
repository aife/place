<?php

namespace App\Service\Handler;

use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Service\Agent\AgentInviterService;
use App\Service\AtexoConfiguration;
use App\Service\Email\AtexoMailManager;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

/**
 * Mohamed BLAL.
 *
 * Class EmailAgentHandler
 */
class EmailAlerteAgentValidationHandler extends EmailHandler
{
    public final const ENVOI_MAIL_ALERTE =
        'email/envoi-alerte-validation.html.twig';

    public final const PARTIAL_URL_TABLEAU_DE_BORD =
        '?page=Agent.TableauDeBord&AS=0&id=';

    private readonly LoggerInterface $logger;

    private AtexoMailManager $mailManager;

    private ?string $objet = null;

    public function __construct(
        AtexoConfiguration $stateModuleChecker,
        SwiftMessageFactory $messageFactory,
        MailSender $mailSender,
        ValidatorInterface $validator,
        Option $options,
        EntityManagerInterface $entityManager,
        AtexoCrypto $crypto,
        ParameterBagInterface $parameterBag,
        LoggerInterface $logger,
        Environment $twig,
        private readonly AgentInviterService $agentInviterService
    ) {
        parent::__construct(
            $stateModuleChecker,
            $messageFactory,
            $mailSender,
            $validator,
            $options,
            $entityManager,
            $crypto,
            $parameterBag
        );
        $this->mailManager = new AtexoMailManager(
            $twig,
            $mailSender,
            $logger,
            self::ENVOI_MAIL_ALERTE,
            [],
            ''
        );

        $this->logger = $logger;
    }

    public function setMailManager(AtexoMailManager $mailManager): void
    {
        $this->mailManager = $mailManager;
    }

    public function sendMailToAgentsAlertConsAttenteValidation(
        Consultation $consultation,
        $service,
        string $typeValidation,
        bool $optionValidation,
        string $local
    ) {
        $tabValidation = $this->setObjectAndValidation($consultation, $optionValidation, $typeValidation, $local);
        $dataModelAgentEmail = $this->setOptionMailerAndDataModel($consultation, $local);

        $listes = $this->filtreFavoris(
            $this->getListEmailAgent($consultation),
            $consultation
        );
        foreach ($listes as $email) {
            $agent = $email->getAgent();
            if ('1' === $agent->getAlerteValidationConsultation()) {
                $pfType = $this->parameterBag->get('PF_TYPE');
                $pfUrlAgent = '';
                if ('HM' == $pfType) {
                    try {
                        $pfUrlAgent = $this->parameterBag->get(
                            'PF_URL_AGENT_' . str_replace('-', '_', $consultation->getAcronymeOrg())
                        );
                    } catch (Exception) {
                        $pfUrlAgent = $this->parameterBag->get('PF_URL_AGENT');
                    }
                }
                $pfReference = $this->parameterBag->get('PF_URL_AGENT') != "/" ?
                    $this->parameterBag->get('PF_URL_AGENT') :
                    $this->parameterBag->get('PF_URL_REFERENCE');

                $url = $this->getDefaultUrlAgent(
                    $consultation,
                    $agent,
                    $pfType,
                    $pfReference,
                    $pfUrlAgent
                );
                /** @var Agent $agent */
                $urlConsultation = $url . self::PARTIAL_URL_TABLEAU_DE_BORD . $consultation->getId();

                $dataModelAgentEmail['agent'] = $agent;
                $dataModelAgentEmail['urlConsultation'] = $urlConsultation;

                $this->checkHabilitationAndSendMail(
                    $agent,
                    $tabValidation,
                    $service,
                    $typeValidation,
                    $dataModelAgentEmail
                );
            }
        }
    }

    public function getListEmailAgent(Consultation $consultation): array
    {
        return $this->agentInviterService->getListAgentInviter(
            $consultation->getAcronymeOrg(),
            $consultation->getServiceId()
        );
    }

    public function filtreFavoris(array $listeId, Consultation $consultation): array
    {
        $favoris = $this->entityManager->getRepository(ConsultationFavoris::class)->findBy([
            'agent' => $listeId,
            'consultation' => $consultation,
        ]);

        return $favoris;
    }

    /**
     * @param $pfType
     * @param $pfUrlAgent
     * @param $pfReference
     *
     * @return string
     */
    protected function getDefaultUrlAgent(
        Consultation $consultation,
        Agent $agent,
        $pfType,
        $pfReference,
        $pfUrlAgent = null
    ) {
        $urlAgent = '';
        $org = $consultation->getAcronymeOrg();
        if ($consultation->getPlateformeVirtuelle()) {
            $urlAgent = $consultation->getPlateformeVirtuelle()->getProtocole()
                . '://'
                . $consultation->getPlateformeVirtuelle()->getDomain()
                . '/';
            if ('HM' == $pfType) {
                $urlAgent .= $org;
            }
        } else {
            if ('HM' == $pfType) {
                $urlAgent = $pfUrlAgent;
            }
            if (empty($urlAgent)) {
                $urlAgent = $pfReference;
            }

            $urlAgent = ((!empty($agent->getOrganisme()->getPfUrl())) ? $agent->getOrganisme()->getPfUrl() : $urlAgent);
        }

        return $urlAgent;
    }

    /**
     * @return int[]
     */
    private function setObjectAndValidation(
        Consultation $consultation,
        bool $optionValidation,
        string $typeValidation,
        string $local
    ): array {
        $tabValidation = [
            'validation_simple' => 0,
            'validation_intermediaire' => 0,
            'validation_finale' => 0,
        ];

        $organisme = $consultation->getOrganisme();
        $denomination_org = $organisme->getDenominationOrg();
        if (
            $optionValidation
            || $typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_1')
            || $typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_2')
            || $typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')
        ) {
            if ($typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_1')) {
                $tabValidation['validation_simple'] = 1;
            } else {
                $tabValidation['validation_finale'] = 1;
            }
            $this->objet = $this->options->translator
                ->trans('OBJET_VALIDATION', ['organisme' => $denomination_org], null, $local);
        } else {
            $tabValidation['validation_intermediaire'] = 1;
            $this->objet = $this->options->translator
                ->trans('OBJET_APPROBATION', ['organisme' => $denomination_org], null, $local);
        }

        return $tabValidation;
    }

    /**
     * @param $local
     */
    private function setOptionMailerAndDataModel(Consultation $consultation, $local): array
    {
        $options = $this->mailManager->getOptions();
        $options['from'] = $this->parameterBag->get('PF_MAIL_FROM');
        $pfName = $this->parameterBag->get('PF_LONG_NAME');
        if (
            $consultation instanceof Consultation &&
            $consultation->getPlateformeVirtuelle()
        ) {
            $options['from'] = $consultation->getPlateformeVirtuelle()->getNoReply();
            $options['addressFromLabel'] = $consultation->getPlateformeVirtuelle()->getFromPfName();
            $pfName = $consultation->getPlateformeVirtuelle()->getFromPfName();
        }

        $dataModelAgentEmail = [
            'fromAgent' => true,
            'local' => $local,
            'consultation' => $consultation,
            'isPubliciteOpoceEnabled' => $this->stateModuleChecker->hasConfigPlateforme('publiciteOpoce'),
            'isAnnoncesMarchesEnabled' => $this->stateModuleChecker->hasConfigPlateforme('annoncesMarches'),
            'urlConsultation' => '',
            'urlBandeauImage' => $this->getUrlImageBandeau(
                $this->parameterBag->get('PF_URL_REFERENCE'),
                $consultation->getPlateformeVirtuelle(),
                $this->parameterBag->get('SOURCE_IMAGE_BANDEAU')
            ),
            'agent' => '',
            'plateforme' => $pfName,
        ];

        $options['subject'] = str_ireplace('}', '', str_ireplace('{', '', $this->objet));
        $options['from'] = $this->options->addressFrom;

        $this->mailManager->setOptions($options);

        return $dataModelAgentEmail;
    }

    private function checkHabilitationAndSendMail(
        Agent $agent,
        array $tabValidation,
        $service,
        string $typeValidation,
        array $dataModelAgentEmail
    ): void {
        $habilitation = $agent->getHabilitation();
        if (
            ('1' == $habilitation->getValidationSimple()
                && $tabValidation['validation_simple'] == $habilitation->getValidationSimple())
            || ('1' == $habilitation->getValidationIntermediaire()
                && $tabValidation['validation_intermediaire'] == $habilitation->getValidationIntermediaire())
            || ('1' == $habilitation->getValidationFinale()
                && $tabValidation['validation_finale'] == $habilitation->getValidationFinale())
        ) {
            if (
                $typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')
                || $typeValidation == $this->parameterBag->get('REGLE_VALIDATION_TYPE_5')
            ) {
                if ($agent->getServiceId() != $service) {
                    return;
                }
            }

            $this->mailManager->sendMail($agent->getEmail(), $dataModelAgentEmail);
        }
    }
}
