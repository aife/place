<?php

namespace App\Service\Handler;

use DateTimeImmutable;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;

/**
 * Class MpeRotatingFileHandler.
 */
class MpeRotatingFileHandler extends RotatingFileHandler
{
    protected $filepath;

    /**
     * MpeRotatingFileHandler constructor.
     *
     * @param string   $filename
     * @param int      $maxFiles
     * @param int      $level
     * @param bool     $bubble
     * @param int|null $filePermission
     * @param bool     $useLocking
     */
    public function __construct(
        $filename,
        $maxFiles = 0,
        $level = Logger::DEBUG,
        $bubble = true,
        $filePermission = null,
        $useLocking = false
    ) {
        $this->filename = $filename;
        $this->maxFiles = (int) $maxFiles;
        $this->nextRotation = new DateTimeImmutable('tomorrow');
        $this->filenameFormat = '{date}/{filename}';
        $this->dateFormat = 'Y/m/d';

        parent::__construct($this->getTimedFilename(), $maxFiles, $level, $bubble, $filePermission, $useLocking);

        $this->url = $this->filename;
    }
}
