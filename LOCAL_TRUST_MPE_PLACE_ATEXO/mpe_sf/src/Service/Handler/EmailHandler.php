<?php

namespace App\Service\Handler;

use Exception;
use App\Entity\Agent;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\Consultation;
use App\Service\AtexoConfiguration;
use App\Service\MailSender;
use App\Service\SwiftMessageFactory;
use Atexo\CryptoBundle\AtexoCrypto;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class EmailHandler
{
    /**
     * EmailHandler constructor.
     */
    public function __construct(
        protected AtexoConfiguration $stateModuleChecker,
        protected SwiftMessageFactory $messageFactory,
        protected MailSender $mailSender,
        protected ValidatorInterface $sfValidator,
        protected Option $options,
        protected EntityManagerInterface $entityManager,
        protected AtexoCrypto $crypto,
        protected ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @param $email 'email' à vérifier
     * @param string $type entreprise ou agent (afin de personnaliser le logger)
     *
     * @return bool Si email valide  true , sinon  false
     *
     * @throws Exception
     */
    protected function verificationEmail($email, $type = 'entreprise')
    {
        $emailConstraint = new Assert\Email(['mode' => Assert\Email::VALIDATION_MODE_STRICT]);
        $emailConstraint->message = 'Invalid email address';

        $errors = $this->sfValidator->validate(
            $email,
            $emailConstraint
        );

        $bool = true;
        if (0 !== count($errors)) {
            $bool = false;
            $logger = new Logger($type . ' email invalide');
            $logger->error('Le mail suivant est invalide : ' . $email);
        }

        return $bool;
    }

    /**
     * @param $baseUrl
     * @param $sourceImageBandeau
     *
     * @return string|string[]
     */
    protected function getUrlImageBandeau(
        $baseUrl,
        ?PlateformeVirtuelle $plateformeVirtuelle,
        $sourceImageBandeau
    ): string|array {
        $urlBandeau = $baseUrl . trim($sourceImageBandeau ?? '', '/');
        if ($plateformeVirtuelle) {
            $urlBandeau = str_replace(
                '/themes/images/',
                '/themes/' . $plateformeVirtuelle->getCodeDesign() . '/images/',
                $urlBandeau
            );
        }

        return $urlBandeau;
    }

    /**
     * @param $pfUrlReference
     *
     * @return string
     */
    protected function getDomainePlateforme(?PlateformeVirtuelle $plateformeVirtuelle, $pfUrlReference)
    {
        $domaine = $pfUrlReference;
        if ($plateformeVirtuelle) {
            $domaine = $plateformeVirtuelle->getProtocole()
                . '://' . $plateformeVirtuelle->getDomain()
                . '/';
        }

        return $domaine;
    }

    /**
     * @param $pfUrlReference
     * @param $sourceImageBandeau
     *
     * @return string
     */
    protected function getUrlImageBandeauPlateformeVirtuelle(
        ?PlateformeVirtuelle $plateformeVirtuelle,
        $pfUrlReference,
        $sourceImageBandeau
    ) {
        $baseUrl = $this->getDomainePlateforme(
            $plateformeVirtuelle,
            $pfUrlReference
        );
        $urlBandeau = $baseUrl . trim($sourceImageBandeau, '/');
        if ($plateformeVirtuelle) {
            $urlBandeau = str_replace(
                '/themes/images/',
                '/themes/' . $plateformeVirtuelle->getCodeDesign() . '/images/',
                $urlBandeau
            );
        }

        return $urlBandeau;
    }

    /**
     * @param $pfType
     * @param $pfUrlAgent
     * @param $pfReference
     *
     * @return string
     */
    protected function getDefaultUrlAgent(
        Consultation $consultation,
        Agent $agent,
        $pfType,
        $pfReference,
        $pfUrlAgent = null
    ) {
        $urlAgent = null;
        $org = $consultation->getAcronymeOrg();
        if ($consultation->getPlateformeVirtuelle()) {
            $urlAgent = $consultation->getPlateformeVirtuelle()->getProtocole()
                . '://'
                . $consultation->getPlateformeVirtuelle()->getDomain()
                . '/';
            if ('HM' == $pfType) {
                $urlAgent .= $org;
            }
        } else {
            if ('HM' == $pfType) {
                try {
                    $urlAgent = $pfUrlAgent;
                } catch (Exception) {
                    $urlAgent = '';
                }
            }
            if (empty($urlAgent)) {
                $urlAgent = $pfReference;
            }

            $urlAgent = ((!empty($agent->getOrganisme()->getPfUrl())) ? $agent->getOrganisme()->getPfUrl() : $urlAgent);
        }

        return $urlAgent;
    }
}
