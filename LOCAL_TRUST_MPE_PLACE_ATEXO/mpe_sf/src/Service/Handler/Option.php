<?php

namespace App\Service\Handler;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class Option
{
    /**
     * @var string
     */
    public $commonTmp;

    /**
     * @var string
     */
    public $addressFrom;

    public string $addressFromLabel = '';

    public string $encoding = 'UTF8';

    /**
     * @var int
     */
    public $isSigned;

    /**
     * @var string
     */
    public $signaturePdfMailCertificat;

    /**
     * @var string
     */
    public $signaturePdfMailPassword;

    /**
     * @var string
     */
    public $signaturePdfMail;

    /**
     * Option constructor.
     */
    public function __construct(public TranslatorInterface $translator, private readonly ParameterBagInterface $params)
    {
        $commonTmp = null;
        $this->commonTmp = $this->params->get('common_tmp_mail');
        if (!empty($commonTmp) && (!file_exists($commonTmp) || !is_dir($commonTmp))) {
            if (is_file($commonTmp)) {
                @unlink($commonTmp);
            }
            @mkdir($commonTmp, 0755, true);
        }
        $this->addressFrom = $this->params->get('PF_MAIL_FROM');
        $this->encoding = $this->params->get('HTTP_ENCODING');
        $this->isSigned = $this->params->get('SIGNATURE_MAIL');
        $this->addressFromLabel = $this->params->get('PF_SHORT_NAME') . ' - ' . $this->params->get('PF_LONG_NAME');
        $this->signaturePdfMailCertificat = $this->params->get('SIGNATURE_PDF_MAIL_CERTIFICAT');
        $this->signaturePdfMailPassword = $this->params->get('SIGNATURE_PDF_MAIL_PASSWORD');
        $this->signaturePdfMail = $this->params->get('SIGNATURE_PDF_MAIL');
    }
}
