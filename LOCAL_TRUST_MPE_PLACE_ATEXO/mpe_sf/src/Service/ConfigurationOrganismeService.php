<?php

namespace App\Service;

use Doctrine\ORM\OptimisticLockException;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Exception\ApiProblemAlreadyExistException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ConfigurationOrganismeService
{
    /**
     * OrganismeService constructor.
     */
    public function __construct(private readonly ContainerInterface $container, private readonly EntityManagerInterface $em, private readonly SerializerInterface $serializer)
    {
    }

    public function getConfigurationOrganismeByAcronyme($acronyme)
    {
        return $this->em->getRepository(ConfigurationOrganisme::class)->getByAcronyme($acronyme);
    }

    public function getConfigurationOrganismeByAcronymeAsArray($acronyme)
    {
        $entity = $this->getConfigurationOrganismeByAcronyme($acronyme);

        return $this->serializer->normalize($entity, null);
    }

    /**
     * @param $acronymeSource
     * @param $acronymeCible
     *
     * @return ConfigurationOrganisme
     *
     * @throws OptimisticLockException
     */
    public function copierConfigurationOrganisme($acronymeSource, $acronymeCible)
    {
        $configurationOrganisme = $this->getConfigurationOrganismeByAcronyme($acronymeSource);

        if (!$configurationOrganisme instanceof ConfigurationOrganisme) {
            $configurationOrganisme = new ConfigurationOrganisme();
        }

        $configurationOrganismeCible = $this->getConfigurationOrganismeByAcronyme($acronymeCible);

        if (!$configurationOrganismeCible instanceof ConfigurationOrganisme) {
            $configurationOrganismeCible = clone $configurationOrganisme;
        } else {
            $msg = sprintf(
                "L'organisme %s existe déjà.",
                $acronymeCible
            );

            throw new ApiProblemAlreadyExistException($msg);
        }

        $configurationOrganismeCible->setOrganisme($acronymeCible);
        $configurationOrganismeCible->setPresenceElu('1');

        $this->em->persist($configurationOrganismeCible);
        $this->em->flush();

        return $configurationOrganismeCible;
    }

    /**
     * Permet de savoir si un module est activé ou non pour un organisme
     */
    public function moduleIsEnabled(Organisme $organisme, string $module): bool
    {
        $confOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
                                        ->findOneBy(['organisme' => $organisme]);

        if ($confOrganisme) {
            $method = 'get' . $module;
            return $confOrganisme->$method();
        }

        return false;
    }

    /**
     * Permet de savoir si un module est désactivé ou non pour au moins un organisme
     */
    public function isModuleDisabledForOneOrganisme(string $module): bool
    {
        $confOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
                                        ->findOneBy([$module => false]);

        return !empty($confOrganisme);
    }

    /**
     * Permet de savoir si un module est activé ou non pour au moins un organisme
     */
    public function isModuleEnabledForOneOrganisme(string $module): bool
    {
        $confOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
                                        ->findOneBy([$module => true]);

        return !empty($confOrganisme);
    }
}
