<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices\Organisme;

use App\Attribute\ResultType;
use App\Service\AbstractService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebservicesOrgansimes extends AbstractService
{
    private const URI = '/referentiels/organismes';

    public function __construct(
        private readonly HttpClientInterface $apiClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($apiClient, $logger, $parameterBag);
    }

    private function getToken()
    {
        return $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . 'api/v2';
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchOrganismes(array $query = [], bool $withToken = true)
    {
        $url = $this->getMpeWsBaseUrl() . self::URI;

        $headers = ['Content-Type' => self::CONTENT_TYPE_JSON];
        if ($withToken) {
            $headers['Authorization'] = 'Bearer ' . $this->getToken();
        }
        $options = [
            'headers' => $headers,
            'query' => $query,
        ];

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }
}
