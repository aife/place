<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationComptePub;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\ContratTitulaire;
use App\Entity\FormePrix;
use App\Service\ClausesService;
use App\Service\Consultation\CommentService;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use Psr\Log\LoggerInterface;
use ReflectionException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class DuplicationService
{
    public function __construct(
        private readonly EntityManagerInterface $entity,
        private readonly LoggerInterface $logger,
        private readonly WebServiceFactory $webServiceFactory,
        private readonly ClausesService $clausesService,
        private readonly DuplicationConstraintService $duplicationConstraint,
        private readonly ParameterBagInterface $parameterBag,
        private readonly CommentService $commentService,
        private readonly TranslatorInterface $translator,
        private readonly FormePrixService $formePrixService
    ) {
    }

    public function duplicateObjectAndGetNewObject(
        int $id,
        string $endpoint,
        array $removeKeys = [],
        array $addKeys = [],
        array $query = [],
    ): string {
        $this->logger->info(
            sprintf(
                "Duplication consultation : get object to duplicate endpoint: %s, id: %s",
                $endpoint,
                $id
            )
        );

        $objectContent = $this->getWsObjectRemovedKeys(
            $id,
            $endpoint,
            ApiEndpointLibrary::METHOD_GET_ITEM,
            $removeKeys,
        );

        if ($addKeys) {
            $objectContent = array_merge($objectContent, $addKeys);
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : do duplication endpoint: %s, id: %s, content: %s, query: %s",
                $endpoint,
                $id,
                var_export($objectContent, true),
                var_export($query, true),
            )
        );

        return $this->webServiceFactory->getContent(
            ApiEndpointLibrary::METHOD_POST,
            $endpoint,
            $objectContent,
            $query
        );
    }

    /**
     * @throws ReflectionException
     * @throws JsonException
     */
    private function getWsObjectRemovedKeys(
        int $id,
        string $endpoint,
        string $methodName,
        array $removeKeys = []
    ): array {
        $removeKeys = array_merge(ApiEndpointLibrary::KEYS_TO_REMOVE, $removeKeys);

        $wsResult = $this->webServiceFactory->getContent($methodName, $endpoint, $id);
        $content = json_decode($wsResult, true, 512, JSON_THROW_ON_ERROR);

        $this->logger->info(
            sprintf(
                "Duplication consultation : getWsObjectRemovedKeys: %s, id: %s, content: %s",
                $endpoint,
                $id,
                $wsResult
            )
        );

        return array_diff_key($content, array_flip($removeKeys));
    }

    /**
     * @throws JsonException
     */
    public function getIriResourceFromWsResult(string $wsResult): ?string
    {
        $wsResult = json_decode($wsResult, true, 512, JSON_THROW_ON_ERROR);

        return $wsResult['@id'] ?? null;
    }

    public function createFormePrix(int $formePrixId): FormePrix
    {
        $formePrix = $this->entity->getRepository(FormePrix::class)->find($formePrixId);

        $newFormePrix = (new FormePrix())
            ->setFormePrix($formePrix->getFormePrix())
            ->setModalite($formePrix->getModalite())
            ->setIdMinMax($formePrix->getIdMinMax())
            ->setPfDateValeur($formePrix->getPfDateValeur())
            ->setPfEstimationHt($formePrix->getPfEstimationHt())
            ->setPfEstimationTtc($formePrix->getPfEstimationTtc())
            ->setPuDateValeur($formePrix->getPuDateValeur())
            ->setPuEstimationHt($formePrix->getPuEstimationHt())
            ->setPuEstimationTtc($formePrix->getPuEstimationTtc())
            ->setPuMax($formePrix->getPuMax())
            ->setPuMin($formePrix->getPuMin())
            ->setPuMaxTtc($formePrix->getPuMaxTtc())
            ->setPuMinTtc($formePrix->getPuMinTtc())
        ;

        $this->entity->persist($newFormePrix);
        $this->entity->flush();

        $this->formePrixService->createFormePrixValues($formePrix, $newFormePrix);

        return $newFormePrix;
    }

    public function duplicateComptePub(
        ConsultationComptePub $comptePub,
        DonneeComplementaire $newDonneComplementaire
    ): ConsultationComptePub {
        $newComptePub = (new ConsultationComptePub())
            ->setOrganisme($comptePub->getOrganisme())
            ->setIdComptePub($comptePub->getIdComptePub())
            ->setBoampLogin($comptePub->getBoampLogin())
            ->setBoampPassword($comptePub->getBoampPassword())
            ->setBoampMail($comptePub->getBoampMail())
            ->setBoampTarget($comptePub->getBoampTarget())
            ->setDenomination($comptePub->getDenomination())
            ->setPrm($comptePub->getPrm())
            ->setAdresse($comptePub->getAdresse())
            ->setCp($comptePub->getCp())
            ->setVille($comptePub->getVille())
            ->setUrl($comptePub->getUrl())
            ->setFactureDenomination($comptePub->getFactureDenomination())
            ->setFactureAdresse($comptePub->getFactureAdresse())
            ->setFactureCp($comptePub->getFactureCp())
            ->setFactureVille($comptePub->getFactureVille())
            ->setInstanceRecoursOrganisme($comptePub->getInstanceRecoursOrganisme())
            ->setInstanceRecoursAdresse($comptePub->getInstanceRecoursAdresse())
            ->setInstanceRecoursCp($comptePub->getInstanceRecoursCp())
            ->setInstanceRecoursVille($comptePub->getInstanceRecoursVille())
            ->setInstanceRecoursUrl($comptePub->getInstanceRecoursUrl())
            ->setEmail($comptePub->getEmail())
            ->setTelephone($comptePub->getTelephone())
            ->setDonneeComplementaire($newDonneComplementaire)
        ;

        $this->entity->persist($newComptePub);
        $this->entity->flush();

        return $newComptePub;
    }

    public function getKeysToRemove(Consultation $consultation): array
    {
        $additionalKeysToRemove = $this->duplicationConstraint->keysToRemove($consultation);

        return array_merge(
            ApiEndpointLibrary::KEYS_TO_REMOVE_CONSULTATION,
            $additionalKeysToRemove
        );
    }

    public function getKeysToAdd(
        Consultation $consultation,
        ?Agent $agent,
        ?string $donneComplementaireIdRessource
    ): array {
        $clausesSociales = [];
        if (!$consultation->isAlloti()) {
            $clausesSociales = $this->clausesService->getFormatedClausesForApiInjection($consultation);
        }

        $serviceIdRessource = $agent->getService()
            ? $this->duplicationConstraint->getFormatedIriFromItem($agent->getService())
            : null
        ;

        $numeroSad = !empty($consultation->getNumeroAc()) ? $consultation->getNumeroAc() : null;

        $addKeysConsultation = [
            ApiEndpointLibrary::KEY_DONNEE_COMPLEMENTAIRE => $donneComplementaireIdRessource,
            ApiEndpointLibrary::KEY_ID_CREATEUR => $agent?->getId(),
            ApiEndpointLibrary::KEY_DATE_MODIFICATION => new \DateTimeImmutable(),
            ApiEndpointLibrary::KEY_CLAUSES => $clausesSociales,
            ApiEndpointLibrary::KEY_SERVICE => $serviceIdRessource,
            ApiEndpointLibrary::KEY_NUMERO_SAD => $numeroSad,
        ];

        return array_merge(
            $addKeysConsultation,
            $this->duplicationConstraint->addReferenceUtilisateur($consultation),
            $this->duplicationConstraint->getKeysDume($consultation),
            $this->duplicationConstraint->getKeyCpv($consultation),
            $this->duplicationConstraint->addAdditionalKeys($consultation),
            $this->getKeysContratInfo($consultation),
        );
    }

    public function getKeysContratInfo(Consultation $consultation): array
    {
        $addKeysSad = [];

        if (!($idContrat = $consultation->getIdContrat())) {
            return $addKeysSad;
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : Début duplication Contrat Infos id: %s",
                $idContrat
            )
        );

        $contrat = $this->entity->getRepository(ContratTitulaire::class)->find($idContrat);
        if ($contrat) {
            if (!$this->parameterBag->get('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
                $contratsTitulaire = $this->entity->getRepository(ContratTitulaire::class)->findBy(
                    ['idContratMulti' => $idContrat]
                );

                $comment = sprintf("%s\n", $this->translator->trans('CONTRACTANT_S'));
                if (is_array($contratsTitulaire) && count($contratsTitulaire)) {
                    /** @var ContratTitulaire $contratTitulaire */
                    $commentMultiple = '';
                    foreach ($contratsTitulaire as $contratTitulaire) {
                        $commentMultiple .= $this->commentService->getCommentByContrat($contratTitulaire);
                    }
                    $comment .= $commentMultiple;
                } else {
                    $comment .= $this->commentService->getCommentByContrat($contrat);
                }

                $addKeysSad = [
                    ApiEndpointLibrary::KEY_ID_CONTRAT_TITULAIRE => $idContrat,
                    ApiEndpointLibrary::KEY_CHAMP_SUPPLEMENTAIRE_INVISIBLE => $comment,
                ];
            }
        }

        $this->logger->info(
            sprintf(
                "Duplication consultation : Fin duplication Contrat Infos id: %s",
                $idContrat
            )
        );

        return $addKeysSad;
    }
}
