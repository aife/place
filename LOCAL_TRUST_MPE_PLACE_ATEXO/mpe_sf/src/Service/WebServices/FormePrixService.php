<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use App\Entity\FormePrix;
use App\Entity\FormePrixPfHasRefVariation;
use App\Entity\FormePrixPuHasRefVariation;
use App\Repository\FormePrixHasRefTypePrixRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use Doctrine\ORM\EntityManagerInterface;

class FormePrixService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly FormePrixHasRefTypePrixRepository $formePrixHasRefTypePrixRepository,
        private readonly FormePrixPfHasRefVariationRepository $formePrixPfHasRefVariationRepository,
        private readonly FormePrixPuHasRefVariationRepository $formePrixPuHasRefVariationRepository
    ) {
    }

    public function createFormePrixValues(FormePrix $formePrix, FormePrix $newFormePrix): void
    {
        $idFormePrix = $formePrix->getIdFormePrix();
        $idNewFormePrix = $newFormePrix->getIdFormePrix();

        // t_forme_prix_pf_has_ref_variation : FormePrixPfHasRefVariation
        $this->duplicateFormePrixPfHasRefVariation($idFormePrix, $idNewFormePrix);

        // t_forme_prix_pu_has_ref_variation : FormePrixPuHasRefVariation
        $this->duplicateFormePrixPuHasRefVariation($idFormePrix, $idNewFormePrix);

        // t_forme_prix_has_ref_type_prix : FormePrixHasRefTypePrix
        $this->duplicateFormePrixHasRefTypePrix($idFormePrix, $idNewFormePrix);
    }

    public function duplicateFormePrixPfHasRefVariation(int $idFormePrix, int $idNewFormePrix): void
    {
        $formePrixPfHasRefVariation = $this->formePrixPfHasRefVariationRepository->findOneBy(
            ['idFormePrix' => $idFormePrix]
        );

        $this->duplicateFormePrixVariation($idNewFormePrix, $formePrixPfHasRefVariation);
    }

    public function duplicateFormePrixPuHasRefVariation(int $idFormePrix, int $idNewFormePrix): void
    {
        $formePrixPuHasRefVariation = $this->formePrixPuHasRefVariationRepository->findOneBy(
            ['idFormePrix' => $idFormePrix]
        );

        $this->duplicateFormePrixVariation($idNewFormePrix, $formePrixPuHasRefVariation);
    }

    public function duplicateFormePrixVariation(
        int $idNewFormePrix,
        FormePrixPuHasRefVariation|FormePrixPfHasRefVariation|null $formePrixVariation
    ): void {
        if (null === $formePrixVariation) {
            return;
        }

        $newFormePrixVariation = clone $formePrixVariation;
        $newFormePrixVariation->setIdFormePrix($idNewFormePrix);

        $this->entityManager->persist($newFormePrixVariation);
        $this->entityManager->flush();
    }

    public function duplicateFormePrixHasRefTypePrix(int $idFormePrix, int $idNewFormePrix): void
    {
        $formePrixHasRefTypePrixAll = $this->formePrixHasRefTypePrixRepository->findBy(
            ['idFormePrix' => $idFormePrix]
        );

        foreach ($formePrixHasRefTypePrixAll as $formePrixHasRefTypePrix) {
            $newFormePrixPuHasRefVariation = clone $formePrixHasRefTypePrix;
            $newFormePrixPuHasRefVariation->setIdFormePrix($idNewFormePrix);
            $this->entityManager->persist($newFormePrixPuHasRefVariation);
        }

        $this->entityManager->flush();
    }
}
