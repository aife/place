<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use App\Attribute\ResultType;
use App\Exception\ApiContentNotCompliantToConsultationValidationRulesException;
use App\Service\AbstractService;
use Exception;
use JsonException;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServiceFactory extends AbstractService
{
    final public const ERROR_MSG = "An error occurred";

    public function __construct(
        private readonly HttpClientInterface $apiClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager,
    ) {
        parent::__construct($apiClient, $logger, $parameterBag);
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws ClientExceptionInterface
     */
    #[ResultType(ResultType::JSON_DECODE)]
    protected function getCollection(string $uri, array $query = [])
    {
        $url = sprintf('%s%s', $this->getMpeWsBaseUrl(), $uri);

        return $this->request(
            Request::METHOD_GET,
            $url,
            $this->getOptions(query: $query)
        )->getContent();
    }

    protected function getItem(string $uri, int $objectId): string
    {
        $url = sprintf('%s%s/%s', $this->getMpeWsBaseUrl(), $uri, $objectId);

        return $this->request(Request::METHOD_GET, $url, $this->getOptions())->getContent();
    }

    protected function post(string $uri, array $body, array $query = []): ?string
    {
        $url = sprintf('%s%s', $this->getMpeWsBaseUrl(), $uri);

        $content = null;
        try {
            $options = $this->getOptions(query: $query, body: $body);
            $response = $this->request(Request::METHOD_POST, $url, $options);

            $content = $response->getContent();
        } catch (Exception $e) {
            $this->logger->error(
                sprintf(
                    "Duplication consultation : ERREUR Post WebService endpoint: %s, body: %s, query: %s",
                    $uri,
                    var_export($body, true),
                    var_export($query, true),
                )
            );

            $errorMessage = sprintf("Duplication consultation, message d'erreur: %s", $e->getMessage());
            $this->logger->error($errorMessage);

            if ($response && $response->getStatusCode() === Response::HTTP_UNPROCESSABLE_ENTITY) {
                throw new ApiContentNotCompliantToConsultationValidationRulesException(
                    str_replace(self::ERROR_MSG, '', $errorMessage)
                );
            }
        }

        return $content;
    }

    /**
     * @throws JsonException
     */
    private function getOptions(array $headers = [], array $query = [], array $body = []): array
    {
        return [
            'headers' => array_merge($this->getHeaders(), $headers),
            'query' => $query,
            'body' => $body ? json_encode($body, JSON_THROW_ON_ERROR) : []
        ];
    }

    private function getHeaders(): array
    {
        return [
            'Content-Type' => self::CONTENT_TYPE_JSON,
            'Authorization' => 'Bearer ' . $this->getToken(),
        ];
    }

    private function getToken(): string
    {
        return $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . ApiEndpointLibrary::ENPOINT_API_V2;
    }
}
