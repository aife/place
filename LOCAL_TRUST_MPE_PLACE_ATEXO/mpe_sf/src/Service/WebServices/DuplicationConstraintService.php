<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Entity\TypeProcedureDume;
use App\Repository\Procedure\ProcedureEquivalenceRepository;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Service\AtexoConfiguration;
use App\Service\LotService;
use App\Service\Procedure\GroupBuyers;
use App\Service\Procedure\ValeurEstimeeRule;
use Doctrine\ORM\EntityManagerInterface;

class DuplicationConstraintService
{
    final public const INDEX_KEY = '/index.php';

    public function __construct(
        private readonly EntityManagerInterface $entity,
        private readonly IriConverterInterface $iriConverter,
        private readonly ProcedureEquivalenceRepository $procedureEquivalenceRepository,
        private readonly LotService $lotService,
        private readonly AtexoConfiguration $atexoConfiguration,
        private readonly ValeurEstimeeRule $valeurEstimeeRule,
        private readonly GroupBuyers $groupementRule,
        private readonly TypeProcedureOrganismeRepository $procedureOrganismeRepository,
        private readonly ReferenceUtilisateurGenerator $referenceUtilisateurGenerator,
    ) {
    }

    public function keysToRemove(Consultation $consultation): array
    {
        $organisme = $consultation->getOrganisme();

        $additionalKeysToRemove = [];
        if (!($procedureEquivalence = $this->getProcedureEquivalenceByConsultation($consultation))) {
            return $additionalKeysToRemove;
        }

        if (
            str_starts_with($procedureEquivalence->getEnvOffreTypeMultiple(), '-')
            && str_starts_with($procedureEquivalence->getEnvOffreTypeUnique(), '-')
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_ALLOTI;
        }

        if (
            !$this->atexoConfiguration->isModuleEnabled('Publicite', $organisme)
            || (
                str_starts_with($procedureEquivalence->getAutoriserPubliciteOui(), '-')
                && str_starts_with($procedureEquivalence->getAutoriserPubliciteNon(), '-')
            )
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_PUB_OBLIGATOIRE;
        }

        if (!$this->atexoConfiguration->isModuleEnabled('InterfaceDume', $organisme)) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_DUME_DEMANDE;
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_TYPE_PROCEDURE_DUME;
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_DUME_SIMPLIFIE;
        }

        if (!$this->valeurEstimeeRule->isActive($organisme)) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_VALEUR_ESTIMEE;
        }

        $procedureOrganisme = $this->getProcedureOrganismeByConsultation($consultation);
        if (
            !$this->atexoConfiguration->isModuleEnabled('Publicite', $organisme)
            || !$procedureOrganisme?->getMapa()
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_PROCEDURE_OUVERTE;
        }

        if (str_starts_with($procedureEquivalence->getTypeProcedureDume(), '-')) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_TYPE_PROCEDURE_DUME;
        }

        if (
            str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
            && str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_DUME_SIMPLIFIE;
        }

        if (!str_starts_with($procedureEquivalence->getDumeDemande(), '+')) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_DUME_DEMANDE;
        }

        if (
            !$this->atexoConfiguration->isModuleEnabled('InterfaceModuleRsem', $organisme)
            || (
                str_starts_with($procedureEquivalence->getDonneesComplementaireOui(), '-')
                && str_starts_with($procedureEquivalence->getDonneesComplementaireNon(), '-')
            )
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_DC_OBLIGATOIRE;
        }

        if (
            !$this->atexoConfiguration->isModuleEnabled('CaseAttestationConsultation', $organisme)
        ) {
            $additionalKeysToRemove[] = ApiEndpointLibrary::KEY_ATTESTATION_CONSULTATION;
        }

        return $additionalKeysToRemove;
    }

    public function getKeysDume(Consultation $consultation): array
    {
        $keysToAdd = [];

        if (!($procedureEquivalence = $this->getProcedureEquivalenceByConsultation($consultation))) {
            return $keysToAdd;
        }

        if (str_starts_with($procedureEquivalence->getDumeDemande(), '+')) {
            $keysToAdd[ApiEndpointLibrary::KEY_DUME_DEMANDE] = (bool)$consultation->getDumeDemande();
        }

        if (!str_starts_with($procedureEquivalence->getTypeProcedureDume(), '-')) {
            // Type procedure dume
            $typeProcedureDumeObject = $this->entity->getRepository(TypeProcedureDume::class)
                ->find($consultation->getTypeProcedureDume())
            ;
            $typeProcedureDume = $typeProcedureDumeObject
                ? $this->getFormatedIriFromItem($typeProcedureDumeObject)
                : null
            ;

            $keysToAdd[ApiEndpointLibrary::KEY_TYPE_PROCEDURE_DUME] = $typeProcedureDume;
        }

        if (
            $this->atexoConfiguration->isModuleEnabled('InterfaceDume', $consultation->getOrganisme())
            && (
                !str_starts_with($procedureEquivalence->getTypeFormulaireDumeStandard(), '-')
                || !str_starts_with($procedureEquivalence->getTypeFormulaireDumeSimplifie(), '-')
            )
        ) {
            $keysToAdd[ApiEndpointLibrary::KEY_DUME_SIMPLIFIE] = (bool)$consultation->getTypeFormulaireDume();
        }

        return $keysToAdd;
    }

    private function getProcedureEquivalenceByConsultation(Consultation $consultation): ?ProcedureEquivalence
    {
        $organisme = $consultation->getOrganisme();
        $typeProcedureOrganisme = $this->entity->getRepository(TypeProcedureOrganisme::class)
            ->findOneBy([
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $organisme
            ])
        ;

        return $this->procedureEquivalenceRepository->findOneBy(
            [
                'idTypeProcedure' => $typeProcedureOrganisme?->getIdTypeProcedure(),
                'organisme' => $organisme
            ]
        );
    }

    private function getProcedureOrganismeByConsultation(Consultation $consultation): ?TypeProcedureOrganisme
    {
        return $this->procedureOrganismeRepository
            ->findOneBy([
                'idTypeProcedure' => $consultation->getIdTypeProcedureOrg(),
                'organisme' => $consultation->getOrganisme()
            ])
        ;
    }

    public function addReferenceUtilisateur(Consultation $consultation): array
    {
        if (
            $this->atexoConfiguration->isModuleEnabled('NumerotationRefCons', $consultation->getOrganisme())
        ) {
            return [];
        }

        $referenceUtilisateur = $this->referenceUtilisateurGenerator->createLabelReferenceUtilisateur($consultation);

        return [ApiEndpointLibrary::KEY_REFERENCE => $referenceUtilisateur];
    }

    public function getKeyCpv(Consultation $consultation): array
    {
        $keysToAdd = [];

        if (!($procedureEquivalence = $this->getProcedureEquivalenceByConsultation($consultation))) {
            return $keysToAdd;
        }

        if ('1' === $procedureEquivalence->getCodeCpvObligatoire()) {
            $keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL] = $consultation->getCodeCpv1();
            $separatedCodeCpv = $this->lotService->getExplodedCodeCpv(
                $consultation->getCodeCpv2(),
                LotService::SEPARATOR
            );
            if ($separatedCodeCpv) {
                $keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE1] = $separatedCodeCpv[0] ?? null;
                $keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE2] = $separatedCodeCpv[1] ?? null;
                $keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_SECONDAIRE3] = $separatedCodeCpv[2] ?? null;
            }
        }

        if (
            isset($keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL])
            && false === $keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL]
        ) {
            unset($keysToAdd[ApiEndpointLibrary::KEY_CODE_CPV_PRINCIPAL]);
        }

        return $keysToAdd;
    }

    public function addAdditionalKeys(Consultation $consultation): array
    {
        $keysToAdd = [];
        $organisme = $consultation->getOrganisme();

        if ($this->groupementRule->isActive($organisme)) {
            $keysToAdd[ApiEndpointLibrary::KEY_GROUPEMENT] = $consultation->isGroupement() ?? false;
        }

        $procedureOrganisme = $this->getProcedureOrganismeByConsultation($consultation);
        if (
            $this->atexoConfiguration->isModuleEnabled('Publicite', $organisme)
            && $procedureOrganisme?->getMapa()
        ) {
            $keysToAdd[ApiEndpointLibrary::KEY_PROCEDURE_OUVERTE] = $consultation->isProcedureOuverte() ?? false;
        }

        return $keysToAdd;
    }

    public function getFormatedIriFromItem(object $item): string
    {
        return str_replace(self::INDEX_KEY, '', $this->iriConverter->getIriFromItem($item));
    }
}
