<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use App\Service\AbstractService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Exception;

class WebServicesExec extends AbstractService
{
    final public const URI_GET_CONTRATS = '/webservices/api/v2/contrats/';

    final public const URI_GET_CONTACT_CONTRAT = '/webservices/api/v2/contrats/mpe/{{uuidExec}}/contacts/';

    final public const URI_ECHANGES_CHORUS = '/webservices/api/v2/contrats/{uuid}/echanges-chorus';

    final public const CLIENT_ID = 'exec-api';

    final public const NB_PAGE = 0;
    final public const NB_ITEMS = 30;

    final public const WS_APACH = 'mpe';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    protected function getToken(): ?string
    {
        return $this->getAuthenticationToken(
            self::OPEN_ID_EXEC,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD')
        )[AbstractService::ACCESS_TOKEN];
    }

    protected function getContrats(array $query = [], bool $apachFormat = false): array
    {
        $url = sprintf("%s%s", $this->parameterBag->get('URL_EXEC'), self::URI_GET_CONTRATS);
        $url = $apachFormat ? $url . self::WS_APACH : $url;

        $headers =
            [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ;

        $options = [
            'headers' => $headers,
            'query' => $query,
        ];

        $content = $this->request(Request::METHOD_GET, $url, $options)->getContent();

        return json_decode($content, true);
    }

    protected function getContratByUuid(string $uuidExec, array $query = []): array
    {
        $url = sprintf(
            "%s%s%s",
            $this->parameterBag->get('URL_EXEC'),
            self::URI_GET_CONTRATS,
            $uuidExec
        );

        $headers =
            [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ;

        $options = [
            'headers' => $headers,
            'query' => $query,
        ];

        $content = $this->request(Request::METHOD_GET, $url, $options)->getContent();

        return json_decode($content, true);
    }

    protected function getContactContratByUuid(string $uuidExec, array $query = []): array
    {
        $url = sprintf(
            "%s%s",
            $this->parameterBag->get('URL_EXEC'),
            str_replace('{{uuidExec}}', $uuidExec, self::URI_GET_CONTACT_CONTRAT)
        );

        $headers =
            [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ;

        $options = [
            'headers' => $headers,
            'query' => $query,
        ];

        $content = $this->request(Request::METHOD_GET, $url, $options)->getContent();

        return json_decode($content, true);
    }

    protected function updateContrat(array $body): array
    {
        $url = sprintf(
            "%s%s",
            $this->parameterBag->get('URL_EXEC'),
            str_replace('{uuid}', $body['uuidContrat'], self::URI_ECHANGES_CHORUS)
        );

        $headers =
            [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $this->getToken()
            ]
        ;

        $options = [
            'headers' => $headers,
            'body' => json_encode($body),
        ];

        $content = $this->request(Request::METHOD_PATCH, $url, $options)->getContent();

        return json_decode($content, true);
    }

    protected function createContrat(array $body): array
    {
        $url = $this->getExecBaseUrl() . '/webservices/api/v2/contrats/mpe';

        try {
            $this->logger->info(
                sprintf(
                    "Création du contrat Exec pour la consultation numéro '%s' avec le body suivant => '%s'",
                    $body['envoi']['contrat']['consultation']['id'],
                    json_encode($body)
                )
            );
            $response = $this->request(
                'POST',
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->getToken(),
                    ],
                    'body' => json_encode($body),
                ]
            );
            if ($response->getStatusCode() !== 200) {
                $this->logger->error(
                    sprintf(
                        "Erreur sur la création du contrat pour la consultation numéro '%s' : '%s'",
                        $body['envoi']['contrat']['consultation']['id'],
                        $response->getContent()
                    )
                );
            } else {
                return json_decode($response->getContent(), true);
            }
        } catch (Exception $e) {
            $this->logger->error(
                sprintf(
                    "Erreur sur la création du contrat pour la consultation numéro '%s' : '%s'",
                    $body['envoi']['contrat']['consultation']['id'],
                    $e->getMessage()
                )
            );
        }

        return [];
    }

    protected function deleteContrat(): void
    {
        $url = $this->getExecBaseUrl() . '/webservices/api/v2/contrats/mpe';

        try {
            $response = $this->request(
                'DELETE',
                $url,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $this->getToken(),
                    ]
                ]
            );
            if ($response->getStatusCode() !== 200) {
                $this->logger->error(
                    sprintf(
                        "Erreur sur la suppression du contrat '%s' : '%s'",
                        1,
                        $response->getContent()
                    )
                );
            }
        } catch (Exception $e) {
            $this->logger->error(
                sprintf(
                    "Erreur sur la suppression du contrat '%s' : '%s'",
                    1,
                    $e->getMessage()
                )
            );
        }
    }

    protected function getExecBaseUrl()
    {
        return $this->parameterBag->get('URL_EXEC');
    }
}
