<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

use App\Entity\Consultation;
use App\Entity\Consultation\NumerotationRefConsAuto;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Repository\ConsultationRepository;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ReferenceUtilisateurGenerator
{
    final public const INIAL_ID_CONS_AUTO = 1;

    public function __construct(
        private readonly AtexoConfiguration $configuration,
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $entity,
        private readonly ConsultationRepository $consultationRepository,
        private readonly Security $security,
    ) {
    }

    public function isModeCreationWithAutomatiqueReference(?Organisme $organisme): bool
    {
        // TODO : NUMEROTATION_AUTOMATIQUE_SUITE => 'continuation'?
        return
            $this->configuration->isModuleEnabled('NumerotationRefCons', $organisme)
            || $this->parameterBag->get('NUMEROTATION_AUTOMATIQUE_SUITE')
        ;
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function createLabelReferenceUtilisateur(Consultation $consultation): string
    {
        if ($this->isModeCreationWithAutomatiqueReference($consultation->getOrganisme())) {
            return $this->generateLabelReference();
        }

        $reference = '';
        if ($reference = $consultation->getReferenceUtilisateur()) {
            $reference = $reference . '_copie';
        }

        return $this->getUniqueReferenceUtilisateur($consultation->getReferenceUtilisateur(), $reference);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    private function getUniqueReferenceUtilisateur(string $previousReference, string $reference): string
    {
        $existingConsultationWithReference = $this->consultationRepository
            ->findBy(['referenceUtilisateur' => $reference])
        ;

        if (!$existingConsultationWithReference) {
            return $reference;
        }

        $nbConsultationWithReference = $this->consultationRepository
            ->getNbConsultationWithSimilarName($previousReference)
        ;

        $newReference = sprintf(
            "%s_%s",
            $reference,
            $nbConsultationWithReference + 1
        );

        return $this->getUniqueReferenceUtilisateur($reference, $newReference);
    }

    public function generateLabelReference(): string
    {
        $value = $this->parameterBag->get('CONSULTATION_NUMERATION_AUTOMATIQUE');
        $date = substr(date('Y'), 2, 4);
        $referenceName = $date . $value;

        $user = $this->security->getUser();
        $service = $user->getService();
        $organisme = $user->getOrganisme();

        $position = '0';
        if ('1' == $this->parameterBag->get('NUMEROTATION_AUTOMATIQUE_AJOUT_SERVICE_SIGLE')) {
            $increment = $this->getNewIncrementConsultation($organisme, $service?->getId());
            $sigle = $service ? $service->getSigle() : $organisme->getSigle();
            $position = '';
        } elseif ('1' == $this->parameterBag->get('NUMEROTATION_AUTOMATIQUE_AJOUT_SIGLE')) {
            $increment = $this->getNewIncrementConsultation($organisme);
            $sigle = $organisme->getSigle();
        } else {
            $increment = $this->getNewIncrementConsultation($organisme);
            $sigle = '';
        }

        if ($increment < '10') {
            $increment = sprintf("%s00%s", $position, $increment);
        } elseif ($increment < '100') {
            $increment = sprintf("%s0%s", $position, $increment);
        } elseif ($increment < '999') {
            $increment = $position . $increment;
        }

        return sprintf("%s%s%s", $sigle, $referenceName, $increment);
    }

    public function getNewIncrementConsultation(Organisme $organisme, ?int $serviceId = null): int
    {
        /** @var Consultation\NumerotationRefConsAuto $numerotationRefConsAuto */
        $numerotationRefConsAuto = $this->entity->getRepository(Consultation\NumerotationRefConsAuto::class)
            ->findOneBy(
                [
                    'annee' => date('Y'),
                    'organisme' => $organisme,
                    'serviceId' => $serviceId
                ]
            )
        ;

        if (null !== $numerotationRefConsAuto) {
            $numeroRefCons = $numerotationRefConsAuto->getIdConsAuto() + 1;

            $this->entity
                ->getRepository(NumerotationRefConsAuto::class)
                ->updateNumerotationRefConsAuto($numeroRefCons, $numerotationRefConsAuto, $serviceId)
            ;

            return $numeroRefCons;
        }

        $numerotationRefConsAuto = new Consultation\NumerotationRefConsAuto();
        $numerotationRefConsAuto->setAnnee(date('Y'));
        $numerotationRefConsAuto->setOrganisme($organisme->getAcronyme());
        $numerotationRefConsAuto->setIdConsAuto(self::INIAL_ID_CONS_AUTO);
        $numerotationRefConsAuto->setServiceId($serviceId);

        $this->entity->persist($numerotationRefConsAuto);
        $this->entity->flush();

        return $numerotationRefConsAuto->getIdConsAuto();
    }
}
