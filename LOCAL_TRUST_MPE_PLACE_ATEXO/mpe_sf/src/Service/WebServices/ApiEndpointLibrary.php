<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\WebServices;

class ApiEndpointLibrary
{
    final public const ENPOINT_API_V2 = 'api/v2';

    final public const URI_CONSULTATIONS = '/consultations';
    final public const URI_LOT = '/lots';
    final public const URI_DONNE_COMPLEMENTAIRES = '/donnee-complementaires';
    final public const URI_CRITERE_ATTRIBUTION = '/critere-attributions';
    final public const URI_SOUS_CRITERE_ATTRIBUTION = '/sous-critere-attributions';

    final public const METHOD_GET_ITEM = 'getItem';
    final public const METHOD_GET_COLLECTION = 'getCollection';
    final public const METHOD_POST = 'post';

    final public const KEYS_TO_REMOVE = ['@context', '@id', '@type'];
    final public const KEYS_TO_REMOVE_DONNEE_COMPLEMENTAIRE = [
        'idDonneeComplementaire',
        'criteresAttribution',
        'formePrix',
        'tranches',
    ];
    final public const KEYS_TO_REMOVE_CRITERE = ['donneeComplementaire', 'sousCritereAttributions', 'lot'];
    final public const KEYS_TO_REMOVE_SOUS_CRITERE = ['critereAttribution'];
    final public const KEYS_TO_REMOVE_LOT = ['consultation', 'clauses'];
    final public const KEYS_TO_REMOVE_CONSULTATION = [
        'id',
        'directionService',
        'reference',
        'dateLimiteRemisePlis',
        'datefinAffichage',
        'dateLimiteRemiseOffres',
        'idCreateur',
        'chiffrement',
        'signatureElectronique',
        'reponseElectronique',
        'urlConsultation',
        'dateModification',
        'clauses'
    ];

    final public const KEY_DUPLICATE = 'duplicate';

    final public const
        KEY_CONSULTATION = 'consultation',
        KEY_DONNEE_COMPLEMENTAIRE = 'donneeComplementaire',
        KEY_CRITERE_ATTRIBUTION = 'critereAttribution',
        KEY_LOT = 'lot',
        KEY_CLAUSES = 'clauses',
        KEY_SERVICE = 'directionService',
        KEY_TRADUCTION_DESCRIPTION = 'idTraductionDescription',
        KEY_TRADUCTION_DESCRIPTION_DETAIL = 'idTraductionDescriptionDetail',
        KEY_ID_CREATEUR = 'idCreateur',
        KEY_DATE_MODIFICATION = 'dateModification',
        KEY_DUME_DEMANDE = 'dume',
        KEY_PROCEDURE_OUVERTE = 'procedureOuverte',
        KEY_TYPE_PROCEDURE_DUME = 'typeProcedureDume',
        KEY_ID_CONTRAT_TITULAIRE = 'idContratTitulaire',
        KEY_CHAMP_SUPPLEMENTAIRE_INVISIBLE = 'commentaireInterne',
        KEY_DUME_SIMPLIFIE = 'dumeSimplifie',
        KEY_REFERENCE = 'reference',
        KEY_ALLOTI = 'alloti',
        KEY_CODE_CPV_PRINCIPAL = 'codeCpvPrincipal',
        KEY_CODE_CPV_SECONDAIRE1 = 'codeCpvSecondaire1',
        KEY_CODE_CPV_SECONDAIRE2 = 'codeCpvSecondaire2',
        KEY_CODE_CPV_SECONDAIRE3 = 'codeCpvSecondaire3',
        KEY_NUMERO_SAD = 'numeroSad',
        KEY_VALEUR_ESTIMEE = 'valeurEstimee',
        KEY_GROUPEMENT = 'groupement',
        KEY_PUB_OBLIGATOIRE = 'donneePubliciteObligatoire',
        KEY_DC_OBLIGATOIRE = 'donneeComplementaireObligatoire',
        KEY_ATTESTATION_CONSULTATION = 'attestationConsultation'
    ;
}
