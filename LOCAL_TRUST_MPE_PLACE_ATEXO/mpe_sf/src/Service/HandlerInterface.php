<?php

namespace App\Service;

use App\Event\EventInterface;

/**
 * @author Mohamed BLAL
 *
 * Interface HandlerInterface
 */
interface HandlerInterface
{
    public function handle(EventInterface $event);
}
