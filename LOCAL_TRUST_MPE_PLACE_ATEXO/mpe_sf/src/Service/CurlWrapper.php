<?php

namespace App\Service;

use CurlHandle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CurlWrapper
{
    private readonly CurlHandle $curlHandle;

    private ?string $lastError = null;

    private readonly array $postData;

    private readonly array $postFile;

    /**
     * CurlWrapper constructor.
     *
     * @param null $proxy
     */
    public function __construct(ContainerInterface $container)
    {
        $this->curlHandle = curl_init();
        $this->setProperties(CURLOPT_RETURNTRANSFER, 1);
        $this->setProperties(CURLOPT_FOLLOWLOCATION, 1);
        $this->setProperties(CURLOPT_MAXREDIRS, 5);

        $proxy = trim($container->getParameter('URL_PROXY'));

        if (!empty($proxy)) {
            $proxyPort = trim($container->getParameter('PORT_PROXY'));
            if (!empty($proxyPort)) {
                $proxy .= ':'.$proxyPort;
            }
            $this->setProperties(CURLOPT_HTTPPROXYTUNNEL, 1);
            $this->setProperties(CURLOPT_PROXY, $proxy);
        }
        $this->postFile = [];
        $this->postData = [];
        curl_setopt($this->curlHandle, CURLOPT_SSL_VERIFYPEER, true);
    }

    /**
     * @param $url
     * @param $timeout
     *
     * @return bool|mixed
     */
    public function get($url, $timeout = null)
    {
        $this->setProperties(CURLOPT_URL, $url);
        if ($this->postData || $this->postFile) {
            $this->curlSetPostData();
        }
        curl_setopt($this->curlHandle, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->curlHandle, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($this->curlHandle);

        $this->lastError = curl_error($this->curlHandle);
        if ($this->lastError) {
            $this->lastError = 'Erreur de connexion au serveur : '.$this->lastError;

            return false;
        }

        return $output;
    }

    /**
     * @param $url
     * @param $data
     * @param null $timeout
     */
    public function post($url, $data, $timeout = null): bool|string
    {
        $this->setProperties(CURLOPT_URL, $url);
        $this->setProperties(CURLOPT_POST, true);
        $this->setProperties(CURLOPT_POSTFIELDS, $data);

        curl_setopt($this->curlHandle, CURLOPT_TIMEOUT, $timeout);
        $output = curl_exec($this->curlHandle);

        $this->lastError = curl_error($this->curlHandle);
        if ($this->lastError) {
            $this->lastError = 'Erreur de connexion au serveur : '.$this->lastError;

            return false;
        }

        return $output;
    }

    private function setProperties($properties, $values)
    {
        curl_setopt($this->curlHandle, $properties, $values);
    }

    public function setAccept($format)
    {
        $curlHttpHeader = [];
        $curlHttpHeader[] = "Accept: $format";
        $this->setProperties(CURLOPT_HTTPHEADER, $curlHttpHeader);
    }

    public function dontVerifySSLCACert()
    {
        $this->setProperties(CURLOPT_SSL_VERIFYHOST, 0);
        $this->setProperties(CURLOPT_SSL_VERIFYPEER, 0);
    }

    public function __destruct()
    {
        curl_close($this->curlHandle);
    }

    public function getLastError()
    {
        return $this->lastError;
    }
}
