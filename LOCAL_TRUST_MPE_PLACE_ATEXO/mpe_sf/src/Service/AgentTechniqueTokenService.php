<?php

namespace App\Service;

use Exception;
use DateTime;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\WS\AgentTechniqueAssociation;
use App\Entity\WS\AgentTechniqueToken;
use App\Entity\WS\WebService;
use App\Repository\WS\AgentTechniqueAssociationRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class AgentTechniqueTokenService
{
    public const TOKEN_VALIDATION_TIME = 'now +24 hours';

    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function getAgentFromToken($token)
    {
        $agentTechniqueToken = $this->em->getRepository(AgentTechniqueToken::class)->findOneBy([
            'token' => $token,
        ]);
        if (empty($agentTechniqueToken)) {
            return false;
        }

        return $agentTechniqueToken->getAgent();
    }

    /**
     * return si l'agent technique est autorisé à faire cette action.
     *
     * @param $agentTechnique
     * @param $nodeFlux
     *
     * @return bool
     */
    public function isGrantedFromAgentWs($agentTechnique, $nodeFlux)
    {
        $granted = false;
        if ($agentTechnique instanceof Agent) {
            $service = $nodeFlux['service']['id'] ?? 0;
            $parentsService = $this->em->getRepository(Agent::class)
                ->getArParentsId($service, $nodeFlux['acronymeOrganisme'], true);
            $granted = $this->isAgentHasPermissionToAcces(
                $agentTechnique,
                $nodeFlux['acronymeOrganisme'],
                $parentsService
            );
        }

        return $granted;
    }

    /**
     * @param $org
     * @param $services
     *
     * @return bool
     */
    public function isAgentHasPermissionToAcces(Agent $agentTechnique, $org, $services)
    {
        /**
         * @var AgentTechniqueAssociationRepository $agentAssocRepository
         */
        $agentAssocRepository = $this->em->getRepository(AgentTechniqueAssociation::class);
        $valid = false;
        $params = ['idAgent' => $agentTechnique->getId()];
        $res = $agentAssocRepository->findBy($params);
        if (count($res)) {
            $res = $agentAssocRepository->getAssociationAgentForOrgAndServices($agentTechnique, $org, $services);
            if (is_countable($res) ? count($res) : 0) {
                $valid = true;
            }
        } else {
            $valid = true;
        }

        return $valid;
    }

    /**
     * @param $agentTechnique
     *
     * @return array
     *
     * @throws Exception
     */
    public function getAllOrganismesServicesAllowed($agentTechnique)
    {
        $orgs = [];
        if ($agentTechnique instanceof Agent) {
            $agentAssocRepository = $this->em->getRepository(AgentTechniqueAssociation::class);
            $params = ['idAgent' => $agentTechnique->getId()];
            $agentAssociations = $agentAssocRepository->findBy($params);
            foreach ($agentAssociations as $agentAssociation) {
                if ($agentAssociation instanceof AgentTechniqueAssociation) {
                    $org = $agentAssociation->getOrganisme()->getAcronyme();
                    $serviceChildren = [];
                    if ($agentAssociation->getServiceId()) {
                        $serviceChildren = $this->em->getRepository(AffiliationService::class)
                            ->getAllChildsId($agentAssociation->getServiceId(), $org, $serviceChildren);
                    } else {
                        $serviceChildren = [$agentAssociation->getServiceId()];
                    }
                    $orgs[$org] = $serviceChildren;
                }
            }
        } else {
            throw new Exception('token invalid');
        }

        return $orgs;
    }

    /**
     * Vérifie si Agent X peut utiliser un web service Y.
     *
     * @param Agent $agent
     * @param $nomWs
     *
     * @return bool
     */
    public function isTokenCanAccessWs(Agent $agent, $nomWs): bool
    {
        $webService = $this->em->getRepository(WebService::class)->findOneBy(['nomWs' => $nomWs]);
        if ($webService instanceof WebService) {
            $webservices = $agent->getWebservices();

            return $webservices->contains($webService);
        }

        return false;
    }

    public function generateTokenForAgentTechnique(Agent $agentTechnique): string
    {
        if (true === $agentTechnique->isTechnique()) {
            $token = uniqid() . date('Ymdhis');
            $agentTechniqueToken = new AgentTechniqueToken();
            $agentTechniqueToken->setToken($token);
            $agentTechniqueToken->setAgent($agentTechnique);
            $agentTechniqueToken->setDateExpiration(new DateTime(self::TOKEN_VALIDATION_TIME));
            $this->em->persist($agentTechniqueToken);
            $this->em->flush();

            return $token;
        }

        return '';
    }
}
