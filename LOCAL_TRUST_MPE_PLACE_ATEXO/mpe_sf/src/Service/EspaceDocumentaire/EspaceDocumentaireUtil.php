<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\EspaceDocumentaire;

use App\Entity\Avis;
use App\Entity\BloborganismeFile;
use App\Entity\Complement;
use App\Entity\Consultation;
use App\Entity\Consultation\AutrePieceConsultation;
use App\Entity\DCE;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\EnveloppeFichier;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\RG;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use App\Service\AtexoUtil;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use App\Utils\Zip;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class EspaceDocumentaireUtil
{
    public final const LISTES = 'listes';
    public final const POIDS = 'poids';
    public final const ENVELOPPE_CANDIDATURE = 'enveloppe_candidature';
    public final const ENVELOPPE_ANONYMAT = 'enveloppe_anonymat';
    public final const FICHIERS = 'fichiers';
    public final const ENVELOPPE_OFFRE = 'enveloppe_offre';
    public final const MESSAGE = 'message';
    public final const ID_BLOB = 'id_blob';
    public final const OPEN = 'open';
    public final const ALLOTI = 'alloti';
    public final const NOM = 'nom';
    public final const TYPE = 'type';
    public final const DUME_ENTREPRISE = 'dume_entreprise';
    public final const EXTENSION = 'extension';
    public final const CHECK_ECHANGE_DOCUMENTAIRE = 'checkEchangeDocumentaire';
    public final const LOT = 'LOT';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly MountManager $mountManager,
        private readonly Zip $zip,
        private readonly AtexoUtil $atexoUtil,
        private readonly TranslatorInterface $translator,
        private readonly Encryption $encryption
    ) {
    }

    /**
     * @return array
     */
    public function getAllBlockCounterFiles(Consultation $consultation, string $organisme)
    {
        return [
            'pieces_modele' => $this->piecesGenereesApartirModele($consultation),
            'pieces_libre' => $this->piecesLibresConsultation($consultation),
            'pieces_attributaire' => $this->piecesDesAttributaires($consultation, $organisme),
            'pieces_publicite' => $this->piecesPublicite($consultation, $organisme),
            'pieces_depot' => $this->piecesDesTousLesDepots($consultation, $organisme),
            'pieces_dce' => $this->piecesDCE($consultation, $organisme),
        ];
    }

    /**
     * @param  $organisme*
     * @return int
     */
    public function piecesDCE(Consultation $consultation, $organisme)
    {
        $counter = 0;
        $reglement = $this->em->getRepository(RG::class)
            ->getReglement($consultation, $organisme);

        $dce = $this->em->getRepository(DCE::class)
            ->getDce($consultation->getId(), $organisme);

        $complement = $this->em->getRepository(Complement::class)
            ->getLastComplement($consultation->getId(), $organisme);

        if ($reglement instanceof RG) {
            ++$counter;
        }

        if ($dce instanceof DCE) {
            $path = $this->mountManager->getAbsolutePath($dce->getDce(), $organisme, $this->em);
            $counter += $this->zip->getCountFilesZip($path);
        }

        if ((is_countable($complement) ? count($complement) : 0) > 0) {
            ++$counter;
        }

        return $counter;
    }

    /**
     * @return int
     */
    public function piecesPublicite(Consultation $consultation, string $organisme)
    {
        $listeAvis = $this->em->getRepository(Avis::class)
            ->getAvisFilesAvailable($consultation->getId(), $organisme);

        return (false !== $listeAvis && (is_countable($listeAvis) ? count($listeAvis) : 0) > 0) ? count($listeAvis) : 0;
    }

    public function piecesDesTousLesDepots(Consultation $consultation, string $organisme): int
    {
        $total = 0;

        $depots = $this->getDepots($consultation);

        foreach ($depots as $key => $depot) {
            $total += $this->getCounterFilesByIdOffre($consultation, $organisme, $depot['id']);
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getCounterFilesByIdOffre(Consultation $consultation, string $organisme, string $idOffre)
    {
        $result = $this->getPieces($consultation, $idOffre, $organisme);

        $enveloppeCandidature = (true == $result[self::LISTES][self::ENVELOPPE_CANDIDATURE][self::OPEN]) ?
            (is_countable($result[self::LISTES]
            [self::ENVELOPPE_CANDIDATURE]) ? count($result[self::LISTES][self::ENVELOPPE_CANDIDATURE]) : 0) - 1 : 0;
        $enveloppeOffre = $this->getCountEnveloppe($result[self::LISTES][self::ENVELOPPE_OFFRE] ?? []);
        $enveloppeAnonymat = $this->getCountEnveloppe($result[self::LISTES][self::ENVELOPPE_ANONYMAT] ?? []);
        $dumEntreprise = is_countable($result[self::LISTES]
        [self::DUME_ENTREPRISE]) ? count($result[self::LISTES][self::DUME_ENTREPRISE]) : 0;

        return $enveloppeCandidature + $enveloppeOffre + $enveloppeAnonymat + $dumEntreprise;
    }

    /**
     * @return int
     */
    public function piecesDesAttributaires(Consultation $consultation, string $organisme)
    {
        $total = 0;
        $data = [];

        $offres = $this->getOffres($consultation);

        foreach ($offres as $key => $offre) {
            $data = $this->getPiecesDesAttributaires($consultation, $organisme, $offre['id']);
        }

        if (!empty($data[self::LISTES])) {
            $enveloppeCandidature = (true == $data[self::LISTES][self::ENVELOPPE_CANDIDATURE][self::OPEN]) ?
                (is_countable($data[self::LISTES]
                [self::ENVELOPPE_CANDIDATURE]) ? count($data[self::LISTES][self::ENVELOPPE_CANDIDATURE]) : 0) - 1 : 0;
            $enveloppeOffre = $this->getCountEnveloppe($data[self::LISTES][self::ENVELOPPE_OFFRE] ?? []);
            $enveloppeAnonymat = $this->getCountEnveloppe($data[self::LISTES][self::ENVELOPPE_ANONYMAT] ?? []);
            $dumEntreprise = is_countable($data[self::LISTES]
            [self::DUME_ENTREPRISE]) ? count($data[self::LISTES][self::DUME_ENTREPRISE]) : 0;

            $total = $enveloppeCandidature + $enveloppeOffre + $enveloppeAnonymat + $dumEntreprise;
        }

        return $total;
    }

    public function getOffres(Consultation $consultation): array
    {
        $statutOffres = $this->parameterBag->get('STATUT_ENV_BROUILLON');
        $statusClosed = $this->parameterBag->get('STATUT_ENV_FERME');

        return $this->em->getRepository(Offre::class)
            ->getAttributaire($consultation->getId(), $statutOffres, $statusClosed);
    }

    public function getDepots(Consultation $consultation): array
    {
        $statusClosed = $this->parameterBag->get('STATUT_ENV_FERME');

        return $this->em->getRepository(Offre::class)
            ->getAllDepotFromConsultationId($consultation->getId(), $statusClosed);
    }

    /**
     * @return array
     */
    public function getPiecesDesAttributaires(Consultation $consultation, string $organisme, string $idOffre)
    {
        $result = [];
        $alloti = '0';

        if ($consultation->getAlloti()) {
            $alloti = '1';
        }
        $result['alloti'] = $alloti;

        $contexte = $this->em->getRepository(TCandidature::class)->getIdContexte(
            $idOffre,
            $organisme,
            $consultation->getId()
        );

        if (isset($contexte) && !empty($contexte[0])) {
            $idContexte = $contexte[0]['id'];

            $dumeEntreprise = $this->em->getRepository(TDumeNumero::class)->findOneBy(
                [
                    'idDumeContexte' => $idContexte,
                ]
            );

            /** PDF **/
            $result['listes']['dume_entreprise'][0]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.pdf';
            $result['listes']['dume_entreprise'][0]['poids'] = 0;
            $result['listes']['dume_entreprise'][0]['id_blob'] = null;
            if ($dumeEntreprise->getBlobId() !== null) {
                $result['listes']['dume_entreprise'][0]['poids'] = $this->mountManager
                    ->getFileSize($dumeEntreprise->getBlobId(), $organisme, $this->em);
                $result['listes']['dume_entreprise'][0]['id_blob'] = $this->encryption->cryptId(
                    $dumeEntreprise->getBlobId()
                );
            }
            $result['listes']['dume_entreprise'][0]['type'] = 'PDF';
            $result['listes']['dume_entreprise'][0]['extension'] = 'pdf';

            /** XML **/
            $result['listes']['dume_entreprise'][1]['nom'] = $dumeEntreprise->getNumeroDumeNational() . '.xml';
            $result['listes']['dume_entreprise'][1]['poids'] = 0;
            $result['listes']['dume_entreprise'][1]['id_blob'] = null;
            if ($dumeEntreprise->getBlobIdXml() !== null) {
                $result['listes']['dume_entreprise'][1]['poids'] = $this->mountManager
                    ->getFileSize($dumeEntreprise->getBlobIdXml(), $organisme, $this->em);
                $result['listes']['dume_entreprise'][1]['id_blob'] = $this->encryption->cryptId(
                    $dumeEntreprise->getBlobIdXml()
                );
            }

            $result['listes']['dume_entreprise'][1]['type'] = 'XML';
            $result['listes']['dume_entreprise'][1]['extension'] = 'XML';
        } else {
            $result['message'] = $this->translator->trans('AUCUN_PLIS');
        }

        if ('0' === $alloti) {
            $result = $this->getListeEnveloppeNonAlloti($idOffre, $result);
        } else {
            $result = $this->getListeEnveloppeAllotiAttributaire($idOffre, $result, $consultation);
        }

        return $result;
    }

    /**
     * @return int
     */
    public function piecesLibresConsultation(Consultation $consultation)
    {
        return count($consultation->getAutresPieceConsultation());
    }

    /**
     * @return int
     */
    public function piecesGenereesApartirModele(Consultation $consultation)
    {
        return count($consultation->getPiecesGenere());
    }

    /**
     * @return array
     */
    public function getListeEnveloppeNonAlloti(string $idOffre, array $result)
    {
        /**
         * Récupération des enveloppes candidatures.
         */
        $result = $this->getEnveloppeForTypeNonAlloti(
            $idOffre,
            self::ENVELOPPE_CANDIDATURE,
            1,
            $result
        );

        /**
         * Récupération des enveloppes offres.
         */
        $result = $this->getEnveloppeForTypeNonAlloti(
            $idOffre,
            self::ENVELOPPE_OFFRE,
            2,
            $result
        );

        /**
         * Récupération des enveloppes anonymats.
         */
        $result = $this->getEnveloppeForTypeNonAlloti(
            $idOffre,
            self::ENVELOPPE_ANONYMAT,
            3,
            $result
        );

        return $result;
    }

    /**
     * @param  $idOffre
     * @param  $atexoUtil
     *
     * @return array
     */
    public function getListeEnveloppeAlloti($idOffre, array $result, $consultation)
    {
        /**
         * Récupération des enveloppes candidatures (pas de lot sur Env. Candi).
         * C'est pour cela que l'on utilise la méthode NonAllotie.
         */
        $result = $this->getEnveloppeForTypeNonAlloti(
            $idOffre,
            self::ENVELOPPE_CANDIDATURE,
            1,
            $result
        );

        /**
         * Récupération des enveloppes offres.
         */
        $result = $this->getEnveloppeForTypeAlloti(
            $idOffre,
            self::ENVELOPPE_OFFRE,
            2,
            $result,
            $consultation
        );

        /**
         * Récupération des enveloppes anonymats.
         */
        $result = $this->getEnveloppeForTypeAlloti(
            $idOffre,
            self::ENVELOPPE_ANONYMAT,
            3,
            $result,
            $consultation
        );

        return $result;
    }

    /**
     * @param  $result
     * @param  $autrePieceConsultations
     *
     * @return array
     */
    public function getAutrePieceConsultations($result, $autrePieceConsultations, string $organisme)
    {
        /**
         * @var AutrePieceConsultation $autrePieceConsultation
         */
        foreach ($autrePieceConsultations as $key => $autrePieceConsultation) {
            $blob = $this->em->getRepository(BloborganismeFile::class)
                ->findOneBy(['id' => $autrePieceConsultation->getBlobId()]);

            if ($blob) {
                $result[self::FICHIERS][$key]['id'] = $this->encryption->cryptId($blob->getId());

                $result[self::FICHIERS][$key][self::NOM] = $blob->getName();
                $extension = $this->getFileExtension($blob->getName());
                $result[self::FICHIERS][$key][self::EXTENSION] = $extension;
                $result[self::FICHIERS][$key][self::POIDS] = $this->mountManager
                    ->getFileSize($blob->getId(), $organisme, $this->em);
                $result[self::FICHIERS][$key]['created_at'] =
                    $autrePieceConsultation->getCreatedAt()->format('d/m/Y - G:i');
                $result[self::FICHIERS][$key]['uploaded'] = true;
                $checkEchangeDocumentaire = $this->em
                    ->getRepository(EchangeDocBlob::class)
                    ->findBy(
                        [
                        'blobOrganisme' => $blob->getId(),
                        ]
                    );

                $result[self::FICHIERS][$key][self::CHECK_ECHANGE_DOCUMENTAIRE] = false;
                if (count($checkEchangeDocumentaire) > 0) {
                    $result[self::FICHIERS][$key][self::CHECK_ECHANGE_DOCUMENTAIRE] = true;
                }
            }
        }
        unset($result[self::MESSAGE]);

        return $result;
    }

    /**
     * @param  $fileName
     */
    public function getFileExtension($fileName): false|string
    {
        return substr(strrchr($fileName, '.'), 1);
    }

    public function getPieces(Consultation $consultation, $idOffre, $organisme)
    {
        $result = [];
        $result[self::TYPE] = 'PLI';
        $result['offre'] = $idOffre;
        $result[self::MESSAGE] = $this->translator->trans('NON_AUTORISE_RESSOURCE');

        $alloti = '0';

        $consultation = $this->em->getRepository(Consultation::class)->find($consultation->getId());
        if ($consultation->getAlloti()) {
            $alloti = '1';
        }
        $result[self::ALLOTI] = $alloti;

        $contexte = $this->em->getRepository(TCandidature::class)->getIdContexte(
            $idOffre,
            $organisme,
            $consultation->getId(),
            'dume',
            '99',
            'online',
            'reponse'
        );

        if (isset($contexte) && !empty($contexte[0])) {
            $idContexte = $contexte[0]['id'];

            $dumeEntreprise = $this->em->getRepository(TDumeNumero::class)->findOneBlob($idContexte);
            $result = $this->getDumeResult($organisme, $dumeEntreprise, $result);
        } else {
            $result[self::MESSAGE] = $this->translator->trans('AUCUN_PLIS');
        }

        if ('0' === $alloti) {
            $result = $this->getListeEnveloppeNonAlloti($idOffre, $result);
        } else {
            $result = $this->getListeEnveloppeAlloti($idOffre, $result, $consultation);
        }

        return $result;
    }

    public function getListeEnveloppeAllotiAttributaire($idOffre, array $result, $consultation)
    {
        /**
         * Récupération des enveloppes candidatures (pas de lot sur Env. Candi).
         * C'est pour cela que l'on utilise la méthode NonAllotie.
         */
        $result = $this->getEnveloppeForTypeNonAllotiAttributaire(
            $idOffre,
            self::ENVELOPPE_CANDIDATURE,
            1,
            $result
        );

        /**
         * Récupération des enveloppes offres.
         */
        $result = $this->getEnveloppeForTypeAllotiAttributaire(
            $idOffre,
            self::ENVELOPPE_OFFRE,
            2,
            $result,
            $consultation
        );

        /**
         * Récupération des enveloppes anonymats.
         */
        $result = $this->getEnveloppeForTypeAllotiAttributaire(
            $idOffre,
            self::ENVELOPPE_ANONYMAT,
            3,
            $result,
            $consultation
        );

        return $result;
    }

    protected function getEnveloppeForTypeNonAllotiAttributaire(
        $idOffre,
        $typeEnveloppe,
        $idTypeEnveloppe,
        array $result
    ) {
        $result[self::LISTES][$typeEnveloppe] = $this->em
            ->getRepository(EnveloppeFichier::class)
            ->getEnveloppeAttributaire($idOffre, $idTypeEnveloppe, $this->em);
        if (!empty($result[self::LISTES][$typeEnveloppe])) {
            foreach ($result[self::LISTES][$typeEnveloppe] as $key => $fichier) {
                $result[self::LISTES][$typeEnveloppe][$key][self::POIDS] =
                    $this->atexoUtil->getSizeName($fichier[self::POIDS]);
                $result[self::LISTES][$typeEnveloppe][$key][self::ID_BLOB] =
                    $this->encryption->cryptId($fichier[self::ID_BLOB]);
            }
            $result[self::LISTES][$typeEnveloppe][self::OPEN] = true;
            unset($result[self::MESSAGE]);
        }

        return $result;
    }

    protected function getEnveloppeForTypeAllotiAttributaire(
        $idOffre,
        $typeEnveloppe,
        $idTypeEnveloppe,
        array $result,
        Consultation $consultation
    ) {
        $listStatutEnv = [
            $this->parameterBag->get('STATUT_ENV_FERME'),
            $this->parameterBag->get('STATUT_ENV_REFUSEE'),
            $this->parameterBag->get('STATUT_ENV_SUPPRIMER'),
            $this->parameterBag->get('STATUT_ENV_CHIFFREMENT_EN_COURS'),
            $this->parameterBag->get('STATUT_ENV_DECHIFFREMENT_EN_COURS'),
            $this->parameterBag->get('STATUT_ENV_EN_ATTENTE_CHIFFREMENT'),
            $this->parameterBag->get('STATUT_ENV_FERMETURE_EN_COURS'),
            $this->parameterBag->get('STATUT_ENV_FERMETURE_EN_ATTENTE'),
            $this->parameterBag->get('STATUT_ENV_BROUILLON'),
        ];

        $enveloppeFichiers = $this->em
            ->getRepository(EnveloppeFichier::class)
            ->getEnveloppeAllotiAttributaire($idOffre, $idTypeEnveloppe, $listStatutEnv, $this->em);

        $numLot = null;
        $lots = [];
        $label = null;
        if (!empty($enveloppeFichiers)) {
            foreach ($enveloppeFichiers as $key => $fichier) {
                if ($fichier[self::ALLOTI] !== (int) $numLot) {
                    $numLot = $fichier[self::ALLOTI];
                    $lot = $this->em->getRepository(Lot::class)
                        ->findOneBy([
                            'organisme' => $consultation->getOrganisme(),
                            'consultation' => $consultation->getId(),
                            'lot' => $numLot,
                        ]);
                    $label = $this->translator->trans(self::LOT) . ' ' . $numLot . ' - ' . $lot->getDescriptionDetail();
                    $pli = [];

                    $pli['label'] = $label;
                    $pli['numLot'] = $fichier[self::ALLOTI];
                    $pli['labelLot'] = $this->translator->trans(self::LOT) . ' ' . $fichier[self::ALLOTI];
                }
                $tab = [];
                $tab[self::TYPE] = $fichier[self::TYPE];
                $tab[self::NOM] = $fichier[self::NOM];
                $tab[self::POIDS] = $this->atexoUtil->getSizeName($fichier[self::POIDS]);
                $tab[self::ID_BLOB] = $this->encryption->cryptId($fichier[self::ID_BLOB]);
                $pli[self::OPEN] = true;
                $pli[self::FICHIERS][] = $tab;
                $lots[$numLot] = $pli;
            }
            unset($result[self::MESSAGE]);
        }

        $result[self::LISTES][$typeEnveloppe] = $lots;

        return $result;
    }

    /**
     * @param  $enveloppeFichiers
     * @param  $numLot
     * @param  $lots
     * @param  $result
     * @param  $label
     * @param  $open
     *
     * @return mixed
     */
    private function getEnveloppes(
        Consultation $consultation,
        $enveloppeFichiers,
        $numLot,
        $lots,
        $result,
        $label,
        $open
    ) {
        foreach ($enveloppeFichiers as $key => $fichier) {
            if ($fichier[self::ALLOTI] !== (int) $numLot) {
                $numLot = $fichier[self::ALLOTI];
                $lot = $this->em->getRepository(Lot::class)
                    ->findOneBy(
                        [
                        'organisme' => $consultation->getOrganisme(),
                        'consultation' => $consultation->getId(),
                        'lot' => $numLot,
                        ]
                    );
                $label = $this->translator->trans(self::LOT) . ' ' . $numLot . ' - ' . $lot->getDescriptionDetail();
                $pli = [];
            }
            $tab = [];
            $extension = $this->getFileExtension($fichier[self::NOM]);
            $tab[self::TYPE] = $fichier[self::TYPE];
            $tab[self::NOM] = $fichier[self::NOM];
            $tab[self::EXTENSION] = $extension;
            $tab[self::POIDS] = $this->atexoUtil->getSizeName($fichier[self::POIDS]);
            $tab[self::ID_BLOB] = $this->encryption->cryptId($fichier[self::ID_BLOB]);
            $pli[self::FICHIERS][] = $tab;
            $pli['label'] = $label;
            $pli[self::OPEN] = $open;
            $pli['numLot'] = $fichier[self::ALLOTI];
            $pli['labelLot'] = $this->translator->trans(self::LOT) . ' ' . $fichier[self::ALLOTI];
            $lots[$numLot] = $pli;
        }
        unset($result[self::MESSAGE]);

        return $lots;
    }

    /**
     * @return int
     */
    public function getCountEnveloppe(array $data)
    {
        $counter = 0;
        if (!empty($data) && !empty($data[self::OPEN])) {
            $counter += count($data) - 1;
        } elseif (!empty($data) && !array_key_exists(self::OPEN, $data)) {
            $counter += $this->getCountEnveloppeWithKeyFilesExist($data);
        }

        return $counter;
    }

    /**
     * @return int
     */
    private function getCountEnveloppeWithKeyFilesExist(array $data)
    {
        $counter = 0;
        foreach ($data as $files) {
            if (!empty($files[self::FICHIERS]) && !empty($files[self::OPEN])) {
                $counter += is_countable($files[self::FICHIERS]) ? count($files[self::FICHIERS]) : 0;
            }
        }

        return $counter;
    }

    /**
     * @param  $idOffre
     * @param  $typeEnveloppe
     * @param  $idTypeEnveloppe
     *
     * @return array
     */
    private function getEnveloppeForTypeAlloti(
        $idOffre,
        $typeEnveloppe,
        $idTypeEnveloppe,
        array $result,
        Consultation $consultation
    ) {
        $statutEnveloppe = [];
        $wantedStatuses = [
            'STATUT_ENV_FERME', //1
            'STATUT_ENV_REFUSEE', //4
            'STATUT_ENV_SUPPRIMER', //6
            'STATUT_ENV_CHIFFREMENT_EN_COURS', //8
            'STATUT_ENV_DECHIFFREMENT_EN_COURS', //9
            'STATUT_ENV_EN_ATTENTE_CHIFFREMENT', //10
            'STATUT_ENV_FERMETURE_EN_COURS', //11
            'STATUT_ENV_FERMETURE_EN_ATTENTE', //12
        ];
        foreach ($wantedStatuses as $wantedStatus) {
            $statutEnveloppe[] = $this->parameterBag->get($wantedStatus);
        }

        $allStatus = [...$statutEnveloppe, ...[$this->parameterBag->get('STATUT_ENV_FERMETURE_EN_ATTENTE')]]; //99

        $enveloppeFichiers = $this->em
            ->getRepository(EnveloppeFichier::class)
            ->getEnveloppeFichiersByTypeEnveloppeAndOffreId($idOffre, $idTypeEnveloppe, $allStatus);

        $numLot = null;
        $lots = [];
        $label = null;
        $open = !empty($enveloppeFichiers);

        if (!$open) {
            //Cas enveloppe fermé
            $enveloppeFichiers = $this->em
                ->getRepository(EnveloppeFichier::class)
                ->getEnveloppeFichiersFermerByTypeEnveloppeAndOffreId($idOffre, $idTypeEnveloppe, $statutEnveloppe);
        }

        $lots = $this->getEnveloppes(
            $consultation,
            $enveloppeFichiers,
            $numLot,
            $lots,
            $result,
            $label,
            $open
        );

        $result[self::LISTES][$typeEnveloppe] = $lots;

        return $result;
    }

    /**
     * @param  $idOffre
     * @param  $typeEnveloppe
     * @param  $idTypeEnveloppe
     *
     * @return array
     */
    private function getEnveloppeForTypeNonAlloti(
        $idOffre,
        $typeEnveloppe,
        $idTypeEnveloppe,
        array $result
    ) {
        $statutEnveloppe = [];
        $wantedStatuses = [
            'STATUT_ENV_FERME', //1
            'STATUT_ENV_REFUSEE', //4
            'STATUT_ENV_SUPPRIMER', //6
            'STATUT_ENV_CHIFFREMENT_EN_COURS', //8
            'STATUT_ENV_DECHIFFREMENT_EN_COURS', //9
            'STATUT_ENV_EN_ATTENTE_CHIFFREMENT', //10
            'STATUT_ENV_FERMETURE_EN_COURS', //11
            'STATUT_ENV_FERMETURE_EN_ATTENTE', //12
        ];
        foreach ($wantedStatuses as $wantedStatus) {
            $statutEnveloppe[] = $this->parameterBag->get($wantedStatus);
        }

        //1,4,6,8,9,10,11,12,99
        $allStatus = [...$statutEnveloppe, ...[$this->parameterBag->get('STATUT_ENV_FERMETURE_EN_ATTENTE')]]; //99

        $result[self::LISTES][$typeEnveloppe] = $this->em
            ->getRepository(EnveloppeFichier::class)
            ->getEnveloppeFichiersByTypeEnveloppeAndOffreId($idOffre, $idTypeEnveloppe, $allStatus);

        //Cas enveloppe Ouvert
        if (!empty($result[self::LISTES][$typeEnveloppe])) {
            $result[self::LISTES][$typeEnveloppe][self::OPEN] = true;
            foreach ($result[self::LISTES][$typeEnveloppe] as $key => $fichier) {
                if (null !== $fichier[self::POIDS]) {
                    $result[self::LISTES][$typeEnveloppe][$key][self::POIDS] =
                        $this->atexoUtil->getSizeName($fichier[self::POIDS]);
                    $result[self::LISTES][$typeEnveloppe][$key][self::ID_BLOB] = $this->encryption->cryptId(
                        $fichier[self::ID_BLOB]
                    );
                }
            }
            unset($result[self::MESSAGE]);
        } else {
            //Cas enveloppe fermé
            $result[self::LISTES][$typeEnveloppe] = $this->em
                ->getRepository(EnveloppeFichier::class)
                ->getEnveloppeFichiersFermerByTypeEnveloppeAndOffreId($idOffre, $idTypeEnveloppe, $statutEnveloppe);

            if (!empty($result[self::LISTES][$typeEnveloppe])) {
                $result[self::LISTES][$typeEnveloppe] = [
                    self::MESSAGE => $this->translator->trans('ENVELOPPE_ACTUELLEMENT_FERMEE'),
                    self::OPEN => false,
                ];
            }
            unset($result[self::MESSAGE]);
        }

        if (empty($result[self::LISTES][$typeEnveloppe])) {
            unset($result[self::LISTES][$typeEnveloppe]);
        }

        return $result;
    }

    /**
     * @param $organisme
     * @param $dumeEntreprise
     * @param $result
     *
     * @return mixed
     */
    public function getDumeResult($organisme, $dumeEntreprise, $result)
    {
        if ($dumeEntreprise instanceof TDumeNumero) {
            $result[self::LISTES][self::DUME_ENTREPRISE][0][self::NOM] =
                $dumeEntreprise->getNumeroDumeNational() . '.pdf';

            $result[self::LISTES][self::DUME_ENTREPRISE][0][self::POIDS] = $this->mountManager
                ->getFileSize($dumeEntreprise->getBlobId(), $organisme, $this->em);

            $result[self::LISTES][self::DUME_ENTREPRISE][0][self::ID_BLOB] = $this->encryption->cryptId(
                $dumeEntreprise->getBlobId()
            );

            $result[self::LISTES][self::DUME_ENTREPRISE][0][self::TYPE] = 'PDF';
            $result[self::LISTES][self::DUME_ENTREPRISE][0][self::EXTENSION] = 'pdf';


            $result[self::LISTES][self::DUME_ENTREPRISE][1][self::NOM] =
                $dumeEntreprise->getNumeroDumeNational() . '.xml';

            $idBlobXml = 0;
            $poids = 0;
            if ($dumeEntreprise->getBlobIdXml() !== null) {
                $idBlobXml = $dumeEntreprise->getBlobIdXml();
                $poids = $this->mountManager
                    ->getFileSize($idBlobXml, $organisme, $this->em);
            }
            $result[self::LISTES][self::DUME_ENTREPRISE][1][self::POIDS] = $poids;

            $result[self::LISTES][self::DUME_ENTREPRISE][1][self::ID_BLOB] = $this->encryption->cryptId(
                $idBlobXml
            );
            $result[self::LISTES][self::DUME_ENTREPRISE][1][self::TYPE] = 'XML';
            $result[self::LISTES][self::DUME_ENTREPRISE][1][self::EXTENSION] = 'xml';
        } else {
            $result[self::MESSAGE] = $this->translator->trans('AUCUN_PLIS');
        }

        return $result;
    }
}
