<?php

namespace App\Service\EspaceDocumentaire;

use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Exception\ApiProblemForbiddenException;
use App\Service\AtexoConfiguration;
use App\Service\Authorization\AuthorizationAgent;
use App\Service\SearchAgent;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Class PerimetreAgent.
 */
class Authorization
{
    /**
     * Authorization constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly SearchAgent $searchAgent,
        private readonly SessionInterface $session,
        private readonly ContainerInterface $container,
        private readonly Security $security,
        private readonly AuthorizationAgent $authorizationAgent
    ) {
    }

    /**
     * @param $reference
     * Note : Paramètre qui permet de by pass la gestion des habiliations
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function isAuthorized($reference, bool $bypassHabilitation = false): bool
    {
        $auth = false;

        $agent = $this->security->getUser();
        $agent = ($agent instanceof Agent) ? $agent : null;

        if ($agent === null) {
            $contexteAuthentification = $this->session->get('contexte_authentification');

            if (!empty($contexteAuthentification) && isset($contexteAuthentification['id'])) {
                $agent = $this->em
                    ->getRepository(Agent::class)
                    ->findOneBy(['id' => $contexteAuthentification['id']]);
            }
        }

        if ($agent) {
            $habilitationAgent = $this->em
                ->getRepository(HabilitationAgent::class)
                ->findOneBy(['agent' => $agent->getId()]);
            $espaceDocumentaire = $this->container->get(AtexoConfiguration::class)
                ->hasConfigOrganisme($agent->getOrganisme()->getAcronyme(), 'espaceDocumentaire');

            if (
                (true === $espaceDocumentaire && true === $habilitationAgent->isEspaceDocumentaireConsultation())
                || true === $bypassHabilitation
            ) {
                $auth = $this->authorizationAgent->isInTheScope($reference);
            }
        }

        if (false === $auth) {
            throw new ApiProblemForbiddenException();
        }

        return true;
    }

    public function isAgentAuthorized($idAgent)
    {
        $auth = false;

        $agent = $this->security->getUser();
        $agent = ($agent instanceof Agent) ? $agent : null;

        if ($agent === null) {
            $contexteAuthentification = $this->session->get('contexte_authentification');

            if (!empty($contexteAuthentification) && isset($contexteAuthentification['id'])) {
                $agent = $this->em
                    ->getRepository(Agent::class)
                    ->findOneBy(['id' => $contexteAuthentification['id']]);
            }
        }

        if ($agent && $idAgent == $agent->getId()) {
            $habilitationAgent = $this->em
                ->getRepository(HabilitationAgent::class)
                ->findOneBy(['agent' => $agent->getId()]);
            $espaceDocumentaire = $this->container->get(AtexoConfiguration::class)
                ->hasConfigOrganisme($agent->getOrganisme()->getAcronyme(), 'espaceDocumentaire');

            if (true === $espaceDocumentaire && true === $habilitationAgent->isEspaceDocumentaireConsultation()) {
                $auth = true;
            }
        }

        if (false === $auth) {
            throw new ApiProblemForbiddenException();
        }

        return true;
    }
}
