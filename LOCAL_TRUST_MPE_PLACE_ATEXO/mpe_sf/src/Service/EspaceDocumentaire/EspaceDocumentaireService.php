<?php

namespace App\Service\EspaceDocumentaire;

use Exception;
use Symfony\Component\Translation\MessageCatalogueInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use UnhandledMatchError;
use Symfony\Bridge\Monolog\Logger;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Exception\ZipFileException;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use ZipArchive;

class EspaceDocumentaireService
{
    private const MSG_FAILER_ADD_FILE_TO_ARCHIVE = 'Failed to add file to archive';
    /**
     * EspaceDocumentaireService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param Encryption $encryption
     * @param MountManager $mountManager
     * @param ParameterBagInterface $parameterBag
     * @param ZipArchive $zipArchive
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager,
        private readonly ParameterBagInterface $parameterBag,
        private readonly ZipArchive $zipArchive,
        private readonly TranslatorInterface $translator,
    ) {
    }

    /**
     * @param $consultationId
     * @param $agentId
     * @param $json
     * @param $date
     *
     * @return array
     *
     * @throws Exception
     */
    public function genereteArchive($consultationId, $json, $date)
    {
        $information = [];
        $consultation = $this->entityManager->getRepository(Consultation::class)
            ->find($consultationId);

        if ($consultation instanceof Consultation) {
            $referenceUtilisateur = $consultation->getReferenceUtilisateur();
            $archiveName = hash('sha256', $referenceUtilisateur) . $date . '.zip';
            $archive = $this->parameterBag->get('COMMON_TMP') . '/' . $archiveName;
            $information['archiveName'] = $archiveName;
            $information['pathArchive'] = $archive;
            if (true === $this->zipArchive->open($archive, ZipArchive::CREATE)) {
                $listeFichiers = json_decode(base64_decode($json), null, 512, JSON_THROW_ON_ERROR);
                foreach ($listeFichiers as $key => $fichiers) {
                    $result = $this->extractionFichier($key, $fichiers);

                    $check = $this->checkConstructionArchive($result);
                    $information['resultatArchive'][$key] = $check;
                    if (false === $check) {
                        break;
                    }
                }
                if (!$this->zipArchive->close()) {
                    throw new ZipFileException('Failed to close archive');
                }
            }
        }

        return $information;
    }

    /**
     * @param $type
     * @param $fichiers
     * @return bool
     * @throws Exception
     */
    public function extractionFichier($type, $fichiers)
    {
        $result = [];

        try {
            $result[$type] = match ($type) {
                'PIECESDCE' => $this->extractionPieceDce($fichiers),
                'PLI' => $this->extractionPli($fichiers, 'PLI'),
                'ATTRIBUTAIRE' => $this->extractionPli($fichiers, 'ATTRIBUTAIRE'),
                'PUBLICITE' => $this->extractionPublicite($fichiers),
                'AUTRESPIECESCONSULTATION' => $this->extractionPiecesLibres($fichiers),
                'PIECESGENEREESCONSULTATION' => $this->extractionPiecesLibres($fichiers)
            };
        } catch (UnhandledMatchError) {
            // Si le type est différent, on ne fait rien.
        }

        return $this->checkValidArchive($result);
    }

    /**
     * @param $contenuDces
     *
     * @return array
     *
     * @throws Exception
     */
    public function extractionPieceDce($contenuDces)
    {
        $listStatut = [];
        foreach ($contenuDces as $key => $pieces) {
            if ('DUMEACHETEUR' === $key) {
                if (property_exists($pieces, 'XML')) {
                    $xml = $pieces->XML[0];
                    $listStatut['DUMEACHETEUR']['XML'] = $this->extractionDumeAgent($xml, 'XML');
                }
                if (property_exists($pieces, 'PDF')) {
                    $pdf = $pieces->PDF[0];
                    $listStatut['DUMEACHETEUR']['PDF'] = $this->extractionDumeAgent($pdf, 'PDF');
                }
            }

            if ('RC' === $key) {
                $path = 'PIECESDCE/RC/';
                $pieces = $pieces[0];
                $listStatut['RC'] = $this->extractionFiles($pieces, $path);
            }

            if ('DCE' === $key) {
                $path = 'PIECESDCE/DCE/';
                $listStatut['DCE'] = $this->extractionDCE($pieces, $path);
            }

            if ('AUTRESPIECES' === $key) {
                $path = 'PIECESDCE/AUTRESPIECES/';
                $pieces = $pieces[0];
                $listStatut['AUTRESPIECES'] = $this->extractionFiles($pieces, $path);
            }
        }

        return $this->checkValidArchive($listStatut);
    }

    /**
     * @param $hash
     * @param $extension
     *
     * @return bool
     *
     * @throws Exception
     */
    public function extractionDumeAgent($hash, $extension)
    {
        $addFile = false;
        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->find($this->encryption->decryptId($hash));

        if ($blobFile instanceof BloborganismeFile) {
            $fichier = $this->mountManager->getAbsolutePath(
                $blobFile->getId(),
                $blobFile->getOrganisme(),
                $this->entityManager
            );

            if (file_exists($fichier)) {
                $addFile = true;
                $file = 'Pièces du DCE/DUME Acheteur/' . $extension . '/' . $blobFile->getName();
                if (!$this->zipArchive->addFile($fichier, $file)) {
                    throw new ZipFileException(self::MSG_FAILER_ADD_FILE_TO_ARCHIVE);
                }
            }
        }

        return $addFile;
    }

    public function extractionDumeOE($hash, $pathEnveloppe)
    {
        $addFile = false;
        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->find($this->encryption->decryptId($hash));

        if ($blobFile instanceof BloborganismeFile) {
            $fichier = $this->mountManager->getAbsolutePath(
                $blobFile->getId(),
                $blobFile->getOrganisme(),
                $this->entityManager
            );

            if (file_exists($fichier)) {
                $addFile = true;
                $file = $pathEnveloppe . $blobFile->getName();
                $file = $this->renamePathFolder($file);
                if (!$this->zipArchive->addFile($fichier, $file)) {
                    $message = "Impossible d'ajouter le fichier =" . $file;
                    $this->logs($message);
                    throw new ZipFileException(self::MSG_FAILER_ADD_FILE_TO_ARCHIVE);
                }
            }
        }

        return $addFile;
    }

    /**
     * @param $piece
     * @param $path
     *
     * @return bool
     *
     * @throws Exception
     */
    public function extractionFiles($piece, $path)
    {
        $addFile = false;
        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->find($this->encryption->decryptId($piece));

        if ($blobFile instanceof BloborganismeFile) {
            $fichier = $this->mountManager->getAbsolutePath(
                $blobFile->getId(),
                $blobFile->getOrganisme(),
                $this->entityManager
            );

            if (file_exists($fichier)) {
                $file = $path . $blobFile->getName();
                $file = $this->renamePathFolder($file);
                if (!$this->zipArchive->addFile($fichier, $file)) {
                    $message = "Impossible d'ajouter le fichier =" . $file;
                    $this->logs($message);
                    throw new ZipFileException(self::MSG_FAILER_ADD_FILE_TO_ARCHIVE);
                }
                $addFile = true;
            } else {
                $message = 'Fichier manquant /  idblob = ' . $blobFile->getId();
                $this->logs($message);
            }
        }

        return $addFile;
    }

    public function getFileIntoDce($blobId, $pathIntoZip)
    {
        $blobFile = $this->entityManager
            ->getRepository(BloborganismeFile::class)
            ->find($this->encryption->decryptId($blobId));

        if ($blobFile instanceof BloborganismeFile) {
            $fichier = $this->mountManager->getAbsolutePath(
                $blobFile->getId(),
                $blobFile->getOrganisme(),
                $this->entityManager
            );

            if (file_exists($fichier)) {
                $dceZip = new ZipArchive();
                if (true === $dceZip->open($fichier)) {
                    $destination = $this->parameterBag->get('COMMON_TMP');
                    $fileExtract = $this->encryption->decryptId($pathIntoZip);
                    $elements = explode('/', (string) $fileExtract);
                    $fileName = end($elements);
                    $finalFilePath = $destination . $fileName;

                    if ($contentFile = $dceZip->getFromName($fileExtract)) {
                        file_put_contents($finalFilePath, $contentFile);
                    }
                    $dceZip->close();

                    return ['name' => $fileName, 'realPath' => $finalFilePath];
                }
            }
        }

        return false;
    }

    /**
     * @param $dce
     *
     * @return bool
     */
    public function extractionDCE($pieces, $path)
    {
        $addFile = true;
        $statut = [];
        foreach ($pieces as $idblob => $dce) {
            $blobFile = $this->entityManager
                ->getRepository(BloborganismeFile::class)
                ->find($this->encryption->decryptId($idblob));

            if ($blobFile instanceof BloborganismeFile) {
                $fichier = $this->mountManager->getAbsolutePath(
                    $blobFile->getId(),
                    $blobFile->getOrganisme(),
                    $this->entityManager
                );

                if (file_exists($fichier)) {
                    $dceZip = new ZipArchive();
                    if (true === $dceZip->open($fichier)) {
                        $check = false;
                        foreach ($dce as $file) {
                            $check = true;
                            $destination = $this->parameterBag->get('COMMON_TMP');
                            $fileExtract = $this->encryption->decryptId($file);
                            $dceZip->extractTo($destination, $fileExtract);
                            $file = str_ireplace('//', '/', $path . $fileExtract);
                            $fichierExtrait = $destination . $fileExtract;
                            $file = $this->renamePathFolder($file);
                            if (!$this->zipArchive->addFile($fichierExtrait, $file)) {
                                $check = false;
                                $message = "[DCE] - Impossible d'ajouter le fichier =" . $file;
                                $this->logs($message);
                                throw new ZipFileException('Failed to add file to archive');
                            }
                            $statut[] = $check;
                        }
                        $dceZip->close();
                    }

                    if (in_array(false, $statut)) {
                        $message = "[DCE] - Problème de construction de l'archive "
                            . $blobFile->getId()
                            . ' est manquant';
                        $this->logs($message);
                        $addFile = false;
                    }
                } else {
                    $message = '[DCE] - Fichier manquant /  idblob = ' . $blobFile->getId();
                    $this->logs($message);
                    $addFile = false;
                }
            }
        }

        return $addFile;
    }

    /**
     * @param $fichiers
     *
     * @return array
     */
    public function extractionPli($fichiers, $type)
    {
        $listStatut = [];
        $listStatut[] = $this->extractionEnveloppe($fichiers, $type);

        return $listStatut;
    }

    /**
     * @param $listes
     * @param $alloti
     *
     * @return array
     */
    public function extractionEnveloppe($listes, $type)
    {
        $result = [];
        foreach ($listes as $key => $plis) {
            $path = $type . '/' . strtoupper($key) . '/';
            foreach ($plis as $keyEnveloppes => $enveloppes) {
                $pathEnveloppe = $path . $keyEnveloppes . '/';

                if ('PDF' === $keyEnveloppes) {
                    $pdf = $enveloppes[0];
                    $result[] = $this->extractionDumeOE($pdf, $pathEnveloppe);
                }

                if ('XML' === $keyEnveloppes) {
                    $pdf = $enveloppes[0];
                    $result[] = $this->extractionDumeOE($pdf, $pathEnveloppe);
                }

                if ('ENVELOPPE_CANDIDATURE' === $keyEnveloppes) {
                    $result[] = $this->extractionPiecesEnveloppe($enveloppes, $pathEnveloppe);
                }

                if ('ENVELOPPE_OFFRE' === $keyEnveloppes) {
                    $result[] = $this->extractionPiecesEnveloppe($enveloppes, $pathEnveloppe);
                }

                if ('ENVELOPPE_ANONYMA' === $keyEnveloppes) {
                    $result[] = $this->extractionPiecesEnveloppe($enveloppes, $pathEnveloppe);
                }
            }
        }

        return $result;
    }

    /**
     * @param $enveloppes
     * @param $path
     * @param $alloti
     *
     * @return array
     *
     * @throws Exception
     */
    public function extractionPiecesEnveloppe($enveloppes, $path, $result = [])
    {
        $pathFichier = $path;
        foreach ($enveloppes as $key => $fichiers) {
            if (is_array($fichiers)) {
                $newPathFichier = $pathFichier . $key . '/';
                $this->extractionPiecesEnveloppe($fichiers, $newPathFichier, $result);
            } else {
                $result[] = $this->extractionFiles($fichiers, $pathFichier);
            }
        }

        return $result;
    }

    /**
     * @param $pieces
     *
     * @return array
     *
     * @throws Exception
     */
    public function extractionPublicite($pieces)
    {
        $listStatut = [];
        $path = 'PUBLICITE/';
        foreach ($pieces as $piece) {
            $listStatut[] = $this->extractionFiles($piece, $path);
        }

        return $listStatut;
    }

    public function extractionPiecesLibres($pieces)
    {
        $listStatut = [];
        $path = 'AUTRESPIECESCONSULTATION/';
        foreach ($pieces as $piece) {
            $listStatut[] = $this->extractionFiles($piece, $path);
        }

        return $listStatut;
    }

    /**
     * @param $tabs
     *
     * @return bool
     */
    public function checkConstructionArchive($tabs)
    {
        if (is_array($tabs)) {
            foreach ($tabs as $check) {
                return $this->checkConstructionArchive($check);
            }
        } else {
            $archiveKO = true;
            if (false === $tabs) {
                $archiveKO = false;
            }

            return $archiveKO;
        }
    }

    public function logs($message)
    {
        $logger = new Logger('log archive');
        $logger->error($message);
    }

    public function renamePathFolder($path)
    {
        $tableRemplacements = [
            'PIECESDCE' => 'Pièces du DCE',
            'AUTRESPIECESCONSULTATION' => 'Pièces libres',
            'AUTRESPIECES' => 'Autres pièces',
            'DUMEACHETEUR' => 'DUME Acheteur',
            'PLI' => 'Pièces de tous les dépôts',
            'DUMEOE' => 'DUME',
            'ENVELOPPE_ANONYMA' => 'Anonymat',
            'ENVELOPPE_CANDIDATURE' => 'Candidature',
            'ENVELOPPE_OFFRE' => 'Offre',
            'PUBLICITE' => 'Pièces de publicité',
            'ATTRIBUTAIRE' => "Pièces de l'attributaire",
            'RC' => 'RC',
        ];
        foreach ($tableRemplacements as $key => $changeFolder) {
            $path = str_replace($key, $changeFolder, $path);
        }

        return $path;
    }

    public function checkValidArchive($listStatut)
    {
        $valid = true;
        foreach ($listStatut as $ckecked) {
            if (false === $ckecked) {
                $valid = false;
            }
        }

        return $valid;
    }

    public function getPieceModeleTranslations(): array
    {
        /** @var MessageCatalogueInterface $catalogue */
        $catalogue = $this->translator->getCatalogue($this->translator->getLocale());
        $messages = $catalogue->all('messages');

        return $messages;
    }
}
