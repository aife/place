<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use DOMDocument;
use App\Attribute\ResultType;
use App\Entity\Agent;
use App\Exception\AgentTechniqueNotFoundException;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesRecensement extends AbstractService
{
    private ?string $idContext = '';
    private ?string $token = '';
    private ?string $refreshToken  = '';

    /**
     * @var string
     */
    public final const CLIENT_ID = 'recensement-api';

    public final const PLATEFORME = 'mpe';

    private const AGENT_TECHNIQUE_LOGIN = 'recensement';

    /**
     * @var string
     */
    public final const GRANT_TYPE = 'password';

    /**
     * WebServicesRecensement constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly EntityManagerInterface $entityManager,
        private readonly OrganismeService $organismeService,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    #[ResultType(ResultType::URL)]
    protected function getUrlRecensement(Agent $agent): string
    {
      $data = $this->getAuthenticationToken(
            self::OPEN_ID_RECENSEMENT,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD')
        );
        $this->token = $data[AbstractService::ACCESS_TOKEN];
        $this->refreshToken = $data['refresh_token'];

        $this->idContext = $this->getAgentContext(
            $this->getRecensementBaseUrl() . '/api-recensement/agents',
            $agent,
            $this->token,
            $this->refreshToken
        );

        if ('' === $this->idContext) {
            throw new NotFoundHttpException('Cette ressource n\'existe pas !');
        }

        return $this->getRecensementBaseUrl() .
            '/login/' . $this->token . '/' . $this->idContext;
    }

    /**
     * @return string
     */
    protected function getRecensementBaseUrl()
    {
        return $this->parameterBag->get('RECENSEMENT_BASE_URL');
    }

    protected function setDataXML(Agent $agent): false|string
    {
        $xml = new DOMDocument('1.0', 'ISO-8859-15');

        $xmlAgent = $xml->createElement('agent');
        $xmlAgentId = $xml->createElement('id', $agent->getId());
        $xmlAgentIdentifiant = $xml->createElement('identifiant', $agent->getLogin());
        $xmlAgentPlateforme = $xml->createElement('plateforme', $this->parameterBag->get('UID_PF_MPE'));
        $xmlAgentAcronymeOrganisme = $xml->createElement('acronymeOrganisme', $agent->getAcronymeOrganisme());
        $xmlAgentEmail = $xml->createElement('email', $agent->getEmail());
        $xmlAgentTelephone = $xml->createElement('telephone', $agent->getTelephone() ?? '');
        $xmlAgentFax = $xml->createElement('fax', $agent->getFax() ?? '');
        $xmlAgentNom = $xml->createElement('nom', $agent->getNom());
        $xmlAgentPrenom = $xml->createElement('prenom', $agent->getPrenom());
        $xmlAgentHabilitations = $xml->createElement('habilitations');

        $this->generateXMLNodeForRecensementHabilitation(
            $agent,
            $xml,
            $xmlAgentHabilitations
        );


        $xmlAgentSigleUrl = $xml->createElement('sigleUrl', '');
        $xmlAcheteurPublic = $xml->createElement(
            'nomCourantAcheteurPublic',
            $this->organismeService->getNomCourantAcheteurPublic($agent)
        );

        $xmlAgentUrlReconnexion = $xml->createElement('urlReconnexion', $this->getRecensementBaseUrl() .
            '/login/' . $this->token . '/' . $this->idContext);


        $api = $xml->createElement('api');
        $apiUrl = rtrim($this->parameterBag->get('PF_URL_REFERENCE'), "/");
        $apiUrl = $xml->createElement('url', $apiUrl);

        $agentTechnique = $this->entityManager->getRepository(Agent::class)
            ->findAgentTechniqueByLogin(self::AGENT_TECHNIQUE_LOGIN);
        if (null === $agentTechnique) {
            throw new AgentTechniqueNotFoundException(
                sprintf('Agent Technique with login %s not found.', self::AGENT_TECHNIQUE_LOGIN)
            );
        }
        $token = $this->tokenManager->create($agentTechnique);
        $apiToken = $xml->createElement('token', $token);
        $apiExecURL = $xml->createElement('execUrl', $this->parameterBag->get('URL_EXEC'));

        $api->appendChild($apiUrl);
        $api->appendChild($apiToken);
        $api->appendChild($apiExecURL);
        $xmlmpe = $xml->createElement('mpe');
        $xmlEnvoi = $xml->createElement('envoi');
        $xmlAgent->appendChild($xmlAgentId);
        $xmlAgent->appendChild($api);
        $xmlAgent->appendChild($xmlAgentIdentifiant);
        $xmlAgent->appendChild($xmlAgentPlateforme);
        $xmlAgent->appendChild($xmlAgentAcronymeOrganisme);
        $xmlAgent->appendChild($xmlAgentNom);
        $xmlAgent->appendChild($xmlAgentPrenom);
        $xmlAgent->appendChild($xmlAgentEmail);
        $xmlAgent->appendChild($xmlAcheteurPublic);
        $xmlAgent->appendChild($xmlAgentTelephone);
        $xmlAgent->appendChild($xmlAgentFax);
        $xmlAgent->appendChild($xmlAgentHabilitations);
        $xmlAgent->appendChild($xmlAgentSigleUrl);
        $xmlAgent->appendChild($xmlAgentUrlReconnexion);
        $xmlEnvoi->appendChild($xmlAgent);
        $xmlmpe->appendChild($xmlEnvoi);

        $xml->appendChild($xmlmpe);

        return $xml->saveXML();
    }

    /**
     * @param $xml
     * @param $xmlAgentHabilitations
     */
    private function generateXMLNodeForRecensementHabilitation(Agent $agent, $xml, $xmlAgentHabilitations): void
    {
        $habilitationsAgent = [
            'besoin_unitaire_consultation',
            'besoin_unitaire_creation_modification',
            'demande_achat_consultation',
            'demande_achat_creation_modification',
            'projet_achat_consultation',
            'projet_achat_creation_modification',
            'validation_opportunite',
            'validation_achat',
            'validation_budget',
            'valider_projet_achat',
            'strategie_achat_gestion',
            'recensement_programmation_administration'
        ];
        foreach ($habilitationsAgent as $habilitationAgent) {
            $function = $this->getGetterMethodName($habilitationAgent);
            if ($agent->getHabilitation()->getRecensementProgrammationStrategieAchat()->$function()) {
                $xmlAgentHabilitation = $xml->createElement('habilitation', $habilitationAgent);
                $xmlAgentHabilitations->appendChild($xmlAgentHabilitation);
            }
        }
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function pingForMonitoring()
    {
        $data = $this->getAuthenticationToken(
            self::OPEN_ID_RECENSEMENT,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD')
        );
        return $this->request(
            'POST',
            $this->getRecensementBaseUrl() . '',
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                    'Authorization' => 'Bearer ' . $data[AbstractService::ACCESS_TOKEN]
                ],
                'body' => '',
            ]
        )->getStatusCode();
    }

    private function convertToCamelCase(string $code): string
    {
        return ucfirst((new CamelCaseToSnakeCaseNameConverter())->denormalize($code));
    }

    /**
     * @param $code
     */
    private function getGetterMethodName($code): string
    {
        return 'is' . $this->convertToCamelCase($code);
    }
}
