<?php

namespace App\Service\Messagerie;

use App\Service\AtexoUtil;
use App\Service\WebServices\WebServicesExec;
use Symfony\Component\Security\Core\Security;
use Twig\Error\Error;
use Exception;
use App\Doctrine\Extension\ConsultationIdExtension;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\Decision\DecisionSelectionEntreprise;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\ContactContrat;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\Offre;
use App\Entity\PanierEntreprise;
use App\Entity\QuestionDCE;
use App\Entity\Telechargement;
use App\Exception\ClientWsException;
use App\Service\ClientWs\ClientWs;
use App\Service\WebservicesMpeConsultations;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

/**
 * Class MessagerieService.
 */
class MessagerieService
{
    public final const MESSAGE_EN_ATTENTE_ACQUITTEMENT = 'EN_ATTENTE_ACQUITTEMENT';
    public final const MESSAGE_ACQUITTE = 'ACQUITTE';
    public final const TYPE_PF_RECHERCHE = 'DESTINATAIRE';
    public final const URL_RETOUR = 'index.php?page=Agent.DetailConsultation&id=';

    /**
     * MessagerieService constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly Encryption $encryption,
        private readonly ContainerInterface $container,
        private readonly TranslatorInterface $translator,
        private readonly ClientWs $clientWs,
        private readonly Environment $templating,
        private readonly LoggerInterface $messecLogger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly WebservicesMpeConsultations $wsConsultations,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly WebServicesMessagerie $webServicesMessagerie,
        private readonly AtexoUtil $atexoUtil,
        private Security $security,
        private readonly WebServicesExec $webServicesExec,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @return string
     */
    public function getMessageError(ClientWsException $clientException)
    {
        $message = match ($clientException->getCode()) {
            Response::HTTP_BAD_REQUEST => $this->translator->trans('ERROR_MESSAGERIE_BAD_REQUEST'),
            Response::HTTP_BAD_GATEWAY => $this->translator->trans('ERROR_MESSAGERIE_BAD_GATEWAY'),
            Response::HTTP_UNAUTHORIZED => $this->translator->trans('ERROR_MESSAGERIE_UNAUTHORIZED'),
            Response::HTTP_FORBIDDEN => $this->translator->trans('ERROR_MESSAGERIE_FORBIDDEN'),
            Response::HTTP_GATEWAY_TIMEOUT => $this->translator->trans('ERROR_MESSAGERIE_GATEWAY_TIMEOUT'),
        };
        $message = $message . PHP_EOL . ' . Stack PHP' . $clientException->getTraceAsString();
        $this->logs($message);

        return $message;
    }

    /**
     * @param $message
     */
    public function logs($message)
    {
        $logger = $this->logger;
        $logger->error($message);
    }

    /**
     * @param $consultationId
     * @param $organisme
     *
     * @return array
     */
    public function getListRegistreQuestions($consultationId, $organisme)
    {
        $questions = [];
        $questionsDce = $this->entityManager->getRepository(QuestionDCE::class)->findBy([
            'consultation' => $consultationId,
            'organisme' => $organisme,
        ], [
            'nom' => 'ASC',
            'prenom' => 'ASC',
            'email' => 'ASC',
        ]);

        foreach ($questionsDce as $questionDce) {
            $questions[] = [
                'ordre' => 0,
                'type' => 'Registre des questions',
                'idContactDest' => $questionDce->getInscrit()?->getId(),
                'idEntrepriseDest' => $questionDce->getIdEntreprise(),
                'mailContactDestinataire' => $questionDce->getEmail(),
                'nomContactDest' => $questionDce->getNom() . ' ' . $questionDce->getPrenom(),
                'nomEntrepriseDest' => $questionDce->getEntreprise(),
            ];
        }

        return $questions;
    }

    /**
     * @param $consultationId
     * @param $organisme
     *
     * @return array
     */
    public function getListRegistreDepots($consultationId, $organisme)
    {
        $offres = [];
        $ListeOffres = $this->entityManager->getRepository(Offre::class)->getConsultationOffresByStatus(
            $consultationId,
            $organisme,
            $this->container->getParameter('STATUT_ENV_BROUILLON')
        );
        foreach ($ListeOffres as $offre) {
            $offres[] = [
                'ordre' => 0,
                'type' => 'Registre des dépôts',
                'idContactDest' => $offre->getInscritId(),
                'idEntrepriseDest' => $offre->getEntrepriseId(),
                'mailContactDestinataire' => $offre->getMailsignataire(),
                'nomContactDest' => $offre->getNomInscrit() . ' ' . $offre->getPrenomInscrit(),
                'nomEntrepriseDest' => $offre->getNomEntrepriseInscrit(),
            ];
        }

        return $offres;
    }

    public function getListRegistreRetraits($consultationId, $organisme)
    {
        $retraits = [];
        $listeRetraits = $this->entityManager->getRepository(Telechargement::class)->findBy([
            'consultation' => $consultationId,
            'organisme' => $organisme,
        ], [
            'nom' => 'ASC',
            'prenom' => 'ASC',
            'email' => 'ASC',
        ]);
        foreach ($listeRetraits as $retrait) {
            $retraits[] = [
                'ordre' => 0,
                'type' => 'Registre des retraits',
                'idContactDest' => $retrait->getIdInscrit(),
                'idEntrepriseDest' => $retrait->getIdEntreprise(),
                'mailContactDestinataire' => $retrait->getEmail(),
                'nomContactDest' => $retrait->getNom() . ' ' . $retrait->getPrenom(),
                'nomEntrepriseDest' => $retrait->getEntreprise(),
            ];
        }

        return $retraits;
    }

    /**
     * @param $consultationId
     *
     *
     * @throws Error
     */
    public function getCartoucheContent($consultationId): false|string
    {
        $consultation = $this
            ->entityManager
            ->getRepository(Consultation::class)
            ->find($consultationId);

        $pfUrl = $this->container->getParameter('PF_URL_MESSAGERIE');
        $urlDirect = null;
        if ($consultation->getTypeAcces() == $this->container->getParameter('TYPE_PROCEDURE_PUBLICITE')) {
            $urlDirect = $pfUrl . '?page=Entreprise.EntrepriseDetailConsultation&id='
                . $consultationId
                . '&orgAcronyme=' . $consultation->getAcronymeOrg();
        } elseif ($consultation->getTypeAcces() == $this->container->getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            $urlDirect = $pfUrl . '?page=Entreprise.EntrepriseDetailConsultation&id='
                . $consultationId
                . '&orgAcronyme=' . $consultation->getAcronymeOrg()
                . '&code=' . $consultation->getCodeProcedure();
        }

        $showDlro = false;
        if (
            $consultation->getDatefin()->format('Y-m-d H:i:s') >= date('Y-m-d H:i:s')
            && '1' == $consultation->getEtatValidation()
        ) {
            $showDlro = true;
        }

        return $this->templating->render(
            'messagerie/cartouche.html.twig',
            [
                'consultation' => $consultation,
                'url_direct' => $urlDirect,
                'url_img' => $this->container->getParameter('PF_URL_REFERENCE') . '/app/',
                'show_dlro' => $showDlro,
            ]
        );
    }

    /**
     * @param $organisme
     *
     * @return array
     */
    public function getListeDestinatairesIssusPhasePrecedente(Consultation $consultation)
    {
        $destinataires = [];
        $numLot = [];
        $lotsInitiale = [];
        $consultationInitiale = null;
        if ($consultation) {
            $lots = $consultation->getLots();
            if ($consultation->getReferenceConsultationInit()) {
                $consultationInitiale = $this->entityManager->getRepository(Consultation::class)->findOneBy([
                    'id' => $consultation->getReferenceConsultationInit(),
                    'organisme' => $consultation->getOrganismeConsultationInit(),
                ]);
                if ($consultationInitiale) {
                    $lotsInitiale = $consultationInitiale->getLots();
                }
            }
        }
        if ((count($lots) + (is_countable($lotsInitiale) ? count($lotsInitiale) : 0)) == 0) {
            $numLot[] = 0;
        } elseif (0 != count($lots) && 0 != (is_countable($lotsInitiale) ? count($lotsInitiale) : 0)) {
            foreach ($lots as $categorieLot) {
                $arrayNumLot[] = $categorieLot->getLot();
            }
            foreach ($lotsInitiale as $lotInitiale) {
                $arrayNumLotInitiale[] = $lotInitiale->getLot();
            }

            foreach ($arrayNumLot as $lot) {
                if (in_array($lot, $arrayNumLotInitiale)) {
                    $numLot[] = $lot;
                }
            }
        } elseif (0 != count($lots) && 0 == (is_countable($lotsInitiale) ? count($lotsInitiale) : 0)) {
            $numLot[] = 0;
        } elseif (0 == count($lots) && 0 != (is_countable($lotsInitiale) ? count($lotsInitiale) : 0)) {
            foreach ($lotsInitiale as $lotInitiale) {
                $numLot[] = $lotInitiale->getLot();
            }
        }

        if ($consultationInitiale) {
            $destinataires = $consultationInitiale ?
                $this->getListeDestinatairesIssusPhasePrecedenteDeConsultationInitiale(
                    $numLot,
                    $consultationInitiale
                ) : [];
        } elseif ($this->parameterBag->get('ACTIVE_EXEC_V2') && $consultation->getContratExecUuid()) {
            $destinataires = $this->getListeDestinatairesIssusPhasePrecedenteDeContratExec($consultation);
        } elseif ($consultation->getIdContrat()) {
            $destinataires = $this->getListeDestinatairesIssusPhasePrecedenteDeContrat($consultation);
        }

        return $destinataires;
    }

    public function getListeDestinatairesIssusPhasePrecedenteDeConsultationInitiale(
        $numLot,
        Consultation $consultationInitiale
    ) {
        $destinataires = [];
        foreach ($numLot as $lot) {
            $decisionSelectionEntreprises = $this->entityManager
                ->getRepository(DecisionSelectionEntreprise::class)
                ->findBy([
                    'consultationId' => $consultationInitiale->getId(),
                    'organisme' => $consultationInitiale->getOrganisme()->getAcronyme(),
                    'lot' => $lot,
                ]);

            foreach ($decisionSelectionEntreprises as $decisionSelectionEntreprise) {
                $offre = $this->entityManager->getRepository(Offre::class)->findOneBy([
                    'id' => $decisionSelectionEntreprise->getIdOffre(),
                    'organisme' => $decisionSelectionEntreprise->getOrganisme(),
                ]);


                if ($offre instanceof Offre) {
                    $destinataires[] = [
                        'ordre' => 0,
                        'type' => "Destinataires issus d'une phase précédente",
                        'idContactDest' => $offre->getInscritId(),
                        'idEntrepriseDest' => $offre->getEntrepriseId(),
                        'mailContactDestinataire' => $offre->getMailsignataire(),
                        'nomContactDest' => $offre->getNomInscrit() . ' ' . $offre->getPrenomInscrit(),
                        'nomEntrepriseDest' => $offre->getNomEntrepriseInscrit(),
                    ];
                }
            }
        }

        return $destinataires;
    }

    /**
     * @return array
     */
    public function getListeDestinatairesIssusPhasePrecedenteDeContrat(Consultation $consultation)
    {
        $destinataires = [];
        $contrat = $this->entityManager->getRepository(ContratTitulaire::class)
            ->find($consultation->getIdContrat());
        if ($contrat instanceof ContratTitulaire) {
            if ($contrat->getChapeau()) {
                $contratsAttributaires = $this->entityManager
                    ->getRepository(ContratTitulaire::class)
                    ->findBy([
                        'idContratMulti' => $contrat->getIdContratTitulaire(),
                    ]);
                foreach ($contratsAttributaires as $contratAttributaire) {
                    if ($contratAttributaire instanceof ContratTitulaire) {
                        $contact = $this->entityManager->getRepository(ContactContrat::class)
                            ->find($contratAttributaire->getIdContratTitulaire());
                        if ($contact instanceof ContactContrat) {
                            $entreprise = $this->entityManager->getRepository(Entreprise::class)
                                ->find($contact->getIdEntreprise());
                            $nomEntreprise = null;
                            if ($entreprise instanceof Entreprise) {
                                $nomEntreprise = $entreprise->getNom();
                            }
                            $destinataires[] = [
                                'ordre' => 0,
                                'type' => "Destinataires issus d'une phase précédente",
                                'idContactDest' => $contact->getIdInscrit(),
                                'idEntrepriseDest' => $contact->getIdEntreprise(),
                                'mailContactDestinataire' => $contact->getEmail(),
                                'nomContactDest' => $contact->getNom() . ' ' . $contact->getPrenom(),
                                'nomEntrepriseDest' => $nomEntreprise,
                            ];
                        }
                    }
                }
            }
        }

        return $destinataires;
    }

    /**
     * @return array
     */
    public function getListeDestinatairesIssusPhasePrecedenteDeContratExec(Consultation $consultation)
    {
        $destinataires = [];
        $contratUuid = $consultation->getContratExecUuid();
        $contactsExec = null;

        try {
            $contactsExec = $this->webServicesExec->getContent('getContactContratByUuid', $contratUuid);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }

        if (!empty($contactsExec)) {
            foreach ($contactsExec as $contact) {
                $destinataires[] = [
                    'ordre' => 0,
                    'type' => "Destinataires issus d'une phase précédente",
                    'idContactDest' => $contact['id'],
                    'idEntrepriseDest' => $contact['idEntreprise'],
                    'mailContactDestinataire' => $contact['email'],
                    'nomContactDest' => $contact['nom'] . ' ' . $contact['prenom'],
                    'nomEntrepriseDest' => $contact['raisonSociale'],
                ];
            }
        }

        return $destinataires;
    }

    /**
     * @param $idInscrit
     * @param $emailDestinataire
     *
     * @return array
     *
     * @throws Exception
     */
    public function getlistConsultationsPanierWithEchangeMessec2($idInscrit, $emailDestinataire)
    {
        $idsConsultation = [];
        try {
            $panierEntreprise = $this->entityManager->getRepository(PanierEntreprise::class)
                ->getRefAndIdConsMessV2FromPanier($idInscrit);
            if (!empty($panierEntreprise)) {
                $listeIds = '';
                $listeReferences = '';
                foreach ($panierEntreprise as $panier) {
                    $listeIds .= $panier['id'] . ';';
                    $listeReferences .= $panier['referenceUtilisateur'] . ';';
                }
                if (!empty($listeIds)) {
                    $countMessages = $this->webServicesMessagerie->getContent(
                        'countMessageStatusByIds',
                        $listeIds,
                        $listeReferences,
                        $emailDestinataire
                    );
                    $counts = json_decode($countMessages, null, 512, JSON_THROW_ON_ERROR);
                    foreach ($counts as $key => $count) {
                        if (0 != $count->nombreMessages) {
                            $idsConsultation[] = $count->idObjetMetier;
                        }
                    }
                }
            }
        } catch (Exception) {
        }

        return $idsConsultation;
    }

    public function getContactForConsultationContract(int $contratId): array
    {
        $result = $this->entityManager
            ->getRepository(Consultation::class)
            ->findContactForConsultationContract($contratId);

        /*
         * Récupèration des contrat multi Attributaire
         */
        if (empty($result)) {
            $result = $this->entityManager
                ->getRepository(Consultation::class)
                ->findContactForConsultationContractMultiAttributaire($contratId);
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getMetaDonnees(Consultation $consultation)
    {
        $atexoUtil = $this->atexoUtil;

        return [
            [
                'code' => 'ENTITE_PUBLIQUE',
                'cle' => $this->translator->trans('DEFINE_TEXT_ENTITE_PUBLIC'),
                'valeur' => $consultation->getEntitePublique(),
            ],
            [
                'code' => 'ENTITE_ACHAT',
                'cle' => $this->translator->trans('TEXT_ENTITES_ACHAT'),
                'valeur' => $consultation->getEntiteAchat(),
            ],
            [
                'code' => 'INTITULE',
                'cle' => $this->translator->trans('DEFINE_INTITULE_CONSULTATION'),
                'valeur' => $consultation->getIntitule(),
            ],
            [
                'code' => 'OBJET',
                'cle' => $this->translator->trans('DEFINE_OBJET_CONSULTATION'),
                'valeur' => $consultation->getObjet(),
            ],
            [
                'code' => 'REFERENCE',
                'cle' => $this->translator->trans('REFERENCE_DE_LA_CONSULTATION'),
                'valeur' => $consultation->getReferenceUtilisateur(),
            ],
            [
                'code' => 'TYPE_PROCEDURE',
                'cle' => $this->translator->trans('TEXT_TYPE_PROCEDURE'),
                'valeur' => $consultation->getLibelleTypeProcedure(),
            ],
            [
                'code' => 'DATE_MISE_EN_LIGNE',
                'cle' => $this->translator->trans('DEFINE_DATE_MISE_EN_LIGNE'),
                'valeur' => $atexoUtil->getFormattedDate($consultation->getDateMiseEnLigneCalcule()),
            ],
            [
                'code' => 'DATE_HEURE_LIMITE_REMISE_PLIS',
                'cle' => $this->translator->trans('DEFINE_DATE_HEURE_LIMITE_REMISE_PLIS'),
                'valeur' => $atexoUtil->getFormattedDate($consultation->getDatefin()),
            ],
        ];
    }

    public function initializeContentForSuiviToken(Agent $agent = null): array
    {
        $params = [
            'nomPfEmetteur'             => $this->parameterBag->get('MESSAGERIE_NOM_PF_EMETTEUR'),
            'typePlateformeRecherche'   => self::TYPE_PF_RECHERCHE,
            'idPlateformeRecherche'     => $this->parameterBag->get('UID_PF_MPE'),
        ];

        $wsResponse = $this->wsConsultations->getContent('searchConsultations', [
            'pagination' => false,
            ConsultationIdExtension::IDS_ONLY => 1
        ]);

        $objetsMetier = [];

        foreach ($wsResponse->{'hydra:member'} as $item) {
            $itemId = $item->{'0'}->{'id'};

            $urlSuivi = $this->urlGenerator->generate(
                'messagerie_suivi_message',
                ['consultationId' => $this->encryption->cryptId($itemId)],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            $objet = [
                'identifiant'   => $itemId,
                'urlRetour'     => self::URL_RETOUR . $itemId,
                'urlSuivi'      => $urlSuivi
            ];

            $partialConsultation = $this->entityManager->createQueryBuilder()
                ->select('c.objet, c.referenceUtilisateur as reference, a.login as creePar, a.id as idAgent')
                ->from(Consultation::class, 'c')
                ->leftJoin('c.agentCreateur', 'a')
                ->where('c.id =:id')
                ->setParameter('id', $itemId)
                ->getQuery()
                ->getSingleResult();

            $objet = array_merge($objet, $partialConsultation);

            if (!empty($agent)) {
                $objet['peutModifier'] = false;
                $objet['peutSupprimer'] = false;
                $objet['peutRelancer'] = false;

                $habilitations = $agent->getHabilitation();
                if (!empty($habilitations->getEnvoyerMessage())) {
                    $interneConsultations = $this->entityManager->getRepository(InterneConsultationSuiviSeul::class)
                        ->findByAgentAndConsultationId($itemId, $agent);
                    if (empty($interneConsultations)) {
                        $objet['peutModifier'] = true;
                        $objet['peutSupprimer'] = true;
                        $objet['peutRelancer'] = true;
                        $objet['url_modification_message'] = $this->urlGenerator->generate(
                            'messagerie_modification_message_brouillon_consultation',
                            [
                                'consultationId' => $itemId,
                                'codeLien' => 'codeLien',
                            ]
                        );
                    }
                }
            }
            $objetsMetier[] = $objet;
        }

        if (null !== $user = $this->security?->getUser()) {
            $params['agent']['id'] = $user->getId();
            $params['agent']['identifiant'] = $user->getLogin();
            $params['agent']['email'] = $user->getEmail();
            $params['agent']['nom'] = $user->getNom();
            $params['agent']['prenom'] = $user->getPrenom();
            $params['agent']['acronyme_organisme'] = $user?->getOrganisme()?->getAcronyme();
        }
        $params['objetsMetier'] = $objetsMetier;

        return $params;
    }
}
