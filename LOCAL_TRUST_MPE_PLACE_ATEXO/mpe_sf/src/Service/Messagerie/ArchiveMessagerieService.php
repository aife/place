<?php

namespace App\Service\Messagerie;

use App\Service\AtexoUtil;
use App\Service\ClientWs\ClientWs;
use App\Service\WebservicesMpeConsultations;
use App\Utils\Encryption;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Symfony\Component\Security\Core\Security;

class ArchiveMessagerieService extends MessagerieService
{
}
