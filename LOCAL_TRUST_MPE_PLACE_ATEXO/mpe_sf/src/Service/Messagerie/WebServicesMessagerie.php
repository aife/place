<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Messagerie;

use App\Exception\ClientWsException;
use App\Service\AbstractService;
use App\Service\AtexoUtil;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WebServicesMessagerie extends AbstractService
{
    private const CLIENT_ID = 'api';
    private const URL_IDENTITE = 'auth/realms/messec/protocol/openid-connect/token';
    private const WS_SUIVI_COUNT = 'rest/suivi/count';
    private const WS_SUIVI_INIT_TOKEN = 'rest/suivi/initToken';
    private const WS_REDACTION_INIT_TOKEN = 'rest/redaction/initToken';
    private const WS_SUIVI_MESSAGES = 'rest/suivi/messages';
    private const WS_SUIVI_DOWNLOADPIECEJOINTE = 'rest/suivi/downloadPieceJointe';
    private const WS_SUIVI_PREUVEECHANGES = 'rest/suivi/preuveEchanges';
    private const WS_COUNT_MESSAGE_STATUS_BY_IDS = 'rest/suivi/countMessageStatusByIds';
    private const MESSAGE_STATUT_ENVOYE = 'ENVOYE';
    public final const MESSAGE_EN_ATTENTE_ACQUITTEMENT = 'EN_ATTENTE_ACQUITTEMENT';
    public final const MESSAGE_ACQUITTE = 'ACQUITTE';
    private readonly string $uri;
    public static ?string $identiteToken = null;

    /**
     * @param ParameterBagInterface $parameterBag
     * @param HttpClientInterface $httpClient
     * @param LoggerInterface $messecLogger
     * @param AtexoUtil $atexoUtil
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $messecLogger,
        private readonly AtexoUtil $atexoUtil
    ) {
        parent::__construct($httpClient, $messecLogger, $parameterBag);
        $this->uri =  $atexoUtil->getPfUrlDomaine($parameterBag->get('URL_MESSAGERIE_SECURISEE'));
    }

    protected function getToken(): ?string
    {
        $token  = '';

        if ($this->parameterBag->get('MESSEC_AUTHENTIFICATION_VIA_KEYCLOAK')) {
            if (!empty(self::$identiteToken)) {
                return self::$identiteToken;
            }

            self::$identiteToken = $this->getAuthenticationToken(
                '/' . self::URL_IDENTITE,
                self::CLIENT_ID,
                $this->parameterBag->get('IDENTITE_USERNAME'),
                $this->parameterBag->get('IDENTITE_PASSWORD'),
            )[AbstractService::ACCESS_TOKEN];

            return self::$identiteToken;
        }

        return $token;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function initialisationSuiviToken(array $params): string
    {
        $params['idPlateformeRecherche'] = $this->parameterBag->get('UID_PF_MPE');
        return  $this->initialisationToken($params, $this->uri . self::WS_SUIVI_INIT_TOKEN);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function initialisationRedactionToken(array $params): string
    {
        return  $this->initialisationToken($params, $this->uri . self::WS_REDACTION_INIT_TOKEN);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getSuiviCount(string $token): string
    {
        $response = $this->request(
            Request::METHOD_GET,
            $this->uri . self::WS_SUIVI_COUNT . '?' . http_build_query(['token' => $token]),
            [
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getEchanges(string $token): string
    {
        $params  =  [
            'inclureReponses' => false,
        ];

        $response = $this->request(
            Request::METHOD_POST,
            $this->uri . self::WS_SUIVI_MESSAGES . '?' . http_build_query(['token' => $token]),
            [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => 'Bearer ' . $this->getToken(),
                ],
                'json' => $params,
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    protected function getPieceJointe(string $codeLien, int $pieceJointeId, string $contentType): string
    {
        $url = $this->uri . self::WS_SUIVI_DOWNLOADPIECEJOINTE .
            '?' . http_build_query(['codeLien' => $codeLien, 'pieceJointeId' => $pieceJointeId]);
        $response = $this->request(
            Request::METHOD_GET,
            $this->uri . self::WS_SUIVI_DOWNLOADPIECEJOINTE .
            '?' . http_build_query(['codeLien' => $codeLien, 'pieceJointeId' => $pieceJointeId]),
            [
                'headers' => ['Accept' => $contentType]
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getPdf(string $idEmail, string $token): string
    {
        $response = $this->request(
            Request::METHOD_GET,
            $this->uri . self::WS_SUIVI_PREUVEECHANGES . '/' . $idEmail .
            '?' . http_build_query(['token' => $token]),
            [
                'headers' => ['Accept' => 'application/pdf']
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function countMessageStatusByIds(
        string $listeIds,
        string $listeReferences,
        string $emailDestinataire
    ): string {
        $queryParams = http_build_query([
            'listeIds' => $listeIds,
            'listeReferences' => $listeReferences,
            'mailDestinataire' => urlencode($emailDestinataire)
            ]);
        $response = $this->request(
            Request::METHOD_GET,
            $this->uri . self::WS_COUNT_MESSAGE_STATUS_BY_IDS . $queryParams,
            [
                'headers' => ['Accept' => 'application/json']
            ]
        );

        if (null === $response) {
            return '';
        }

        return $response->getContent($this->throw);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function initialisationToken(array $params, string $url): string
    {
        try {
            $response = $this->request(
                Request::METHOD_POST,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->getToken(),
                    ],
                    'json' => $params,
                ]
            );

            if (null === $response) {
                return '';
            }

            return $response->getContent($this->throw);
        } catch (\Exception $e) {
            $this->messecLogger->error(
                "Erreur survenue lors de l'initialisation de token lié à la messagerie sécurisée : "
                . $e->getMessage()
                . $e->getTraceAsString()
            );
            return '';
        }
    }

    protected function initParamsWidgetMessec(string $parameter): array
    {
        $params = [];
        $params[$parameter] = true;

        if ($parameter === 'reponseAttendue') {
            $params['statutAcquitte'] = true;
            $params['statutDelivre'] = true;
        }

        return $params;
    }

    /**
     * @param string $url
     * @param array $params
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TimeoutExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function postGuzzleMessage(string $url, array $params = []): string
    {
        try {
            $response = $this->request(
                Request::METHOD_POST,
                $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                        'Authorization' => 'Bearer ' . $this->getToken(),
                    ],
                    'json' => $params,
                ]
            );

            if (null === $response) {
                return '';
            }

            return $response->getContent($this->throw);
        } catch (\Exception $e) {
            $this->messecLogger->error(
                "Erreur survenue lors de postGuzzleMessage de la messagerie sécurisée : "
                . $e->getMessage()
                . $e->getTraceAsString()
            );
            return '';
        }
    }
}
