<?php

namespace App\Service;

use Doctrine\DBAL\Exception as DBALException;
use App\Entity\ConfigurationPlateforme;
use App\Service\Messagerie\MessagerieService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AtexoSearchEntreprise extends AtexoSearch
{
    protected string $queryJoinCondition = '';
    protected ?string $querySelect = null;
    protected ?string $queryLeftJoinAlerte = null;
    protected ?string $queryLeftJoinCategorieLot = null;

    /**
     * AtexoSearch constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AtexoUtil $serviceUtile,
        MessagerieService $messagerieService,
        SessionInterface $session
    ) {
        parent::__construct($container, $em, $logger, $serviceUtile, $messagerieService, $session);
    }

    /**
     * @param $criteriaVo
     * @param bool $returnJustNumberOfElement
     * @param bool $returnReferences
     * @param bool $forAlertesEntreprises
     * @param bool $gestionOperations
     * @param null $referenceByReferentiel
     *
     * @return mixed
     *
     * @throws DBALException
     */
    public function getConsultationsByQuery(
        $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false,
        $gestionOperations = false,
        $referenceByReferentiel = null
    ) {
        $queryLeftJoinOrganisme = null;
        $this->queryParams = [];
        $this->isEnabled = $this
            ->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        $this->criteriaVo = $criteriaVo;

        if ($returnJustNumberOfElement || $returnReferences) {
            $this->getSqlCountEntreprise();
        } else {
            $this->getSqlInitListConsultation();
        }

        if (!$this->criteriaVo->getOrganismeTest()) {
            $queryLeftJoinOrganisme = ' INNER JOIN Organisme on Organisme.acronyme =consultation.organisme ';
        }

        $this->getSqlAlertEntreprise();

        $this->getSqlCategorieEntreprise();

        $queryFrom = <<<QUERY
FROM consultation
		$this->queryLeftJoinCategorieLot
		$this->queryLeftJoinAlerte
		$queryLeftJoinOrganisme
QUERY;

        //on parcourt l'objet CriteriaVo passe en parametre pour ajouter a la clause where
        $otherConditions = $this->otherConditions($gestionOperations, $referenceByReferentiel);

        $consultationExterneCondition = $this->addEntrepriseConsultationExterneCondition();
        $otherConditions .= $consultationExterneCondition;
        $stateCondition = $this->stateConditions();
        $otherConditions .= $stateCondition;

        $queryForPole = <<<QUERY
$this->querySelect
$queryFrom
WHERE
    1
	$this->queryJoinCondition
	$otherConditions
QUERY;

        $finalQuery = $this->getSqlTrie($queryForPole, $returnJustNumberOfElement, $returnReferences);

        $finalQuery = preg_replace("#\([ ]+OR[ ]+\(#", '( (', $finalQuery);
        if ($forAlertesEntreprises) {
            $finalQuery = str_replace('consultation.', 'consultation_alertes.', $finalQuery);
            $finalQuery = str_replace('FROM consultation', 'FROM consultation_alertes', $finalQuery);
        }

        $stmt = $this->em->getConnection()->prepare($finalQuery);
        $result = $stmt->executeQuery($this->queryParams);

        return $result->fetchAllAssociative();
    }

    private function getSqlCountEntreprise()
    {
        $this->querySelect = <<<QUERY
SELECT DISTINCT consultation.id
QUERY;
    }

    private function getSqlInitListConsultation()
    {
        $this->querySelect = <<<QUERY
SELECT
	DISTINCT CONCAT(consultation.id, consultation.organisme),
	consultation.id,
	consultation.organisme,
	consultation.datefin,
	consultation.reference_utilisateur,
	consultation.intitule,
	consultation.objet,
	datemiseenligne ,
	datevalidation,
	regle_mise_en_ligne ,
	consultation.categorie,
	date_format(consultation.dateDebut,'%d/%m/%Y') as datedebut,
	consultation.code_procedure as code_procedure

QUERY;
    }

    private function getSqlAlertEntreprise()
    {
        $this->queryLeftJoinAlerte = <<<QUERY
LEFT JOIN
	alerte_metier ON (alerte_metier.reference_cons = consultation.id AND alerte_metier.organisme = consultation.organisme )
QUERY;
        $this->queryLeftJoinAlerte = (0 != $this->criteriaVo->getIdAlerteConsultation() &&
            0 != $this->criteriaVo->getAlerteConsultationCloturee()) ? $this->queryLeftJoinAlerte : '';
    }

    private function getSqlCategorieEntreprise()
    {
        $this->queryLeftJoinCategorieLot = <<<QUERY
LEFT JOIN
		CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme)
QUERY;

        $this->queryLeftJoinCategorieLot = ($this->criteriaVo->getKeyWordAdvancedSearch() || $this->criteriaVo->getKeyWordRechercheRapide() || $this->criteriaVo->getIdCodeCpv2()) ? $this->queryLeftJoinCategorieLot : '';
    }
}
