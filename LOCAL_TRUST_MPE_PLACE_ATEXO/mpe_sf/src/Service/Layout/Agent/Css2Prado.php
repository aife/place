<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Layout\Agent;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;

class Css2Prado
{
    private readonly ?Request $request;

    /**
     * Css2Prado constructor.
     */
    public function __construct(
        private readonly Environment $twig,
        RequestStack $requestStack
    ) {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function render()
    {
        return $this->twig->render('menuAgent/css-javascript-layout-agent/css-agent.html.twig', [
            'design' => $this->request === null ? '' : $this->request->cookies->get('design', ''),
            'calledFromPrado' => true,
        ]);
    }
}
