<?php

namespace App\Service;

use App\Entity\Consultation;
use App\Entity\GeolocalisationN2;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class GeolocalisationService
{
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    public function getLieuxExecution(Consultation $consultation): array
    {
        $listeLieuxExecutions = explode(',', $consultation->getLieuExecution());

        return $this->em->getRepository(GeolocalisationN2::class)->findById($listeLieuxExecutions);
    }
}
