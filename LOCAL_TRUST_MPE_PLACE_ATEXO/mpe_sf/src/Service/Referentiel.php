<?php

namespace App\Service;

use App\Entity\ValeurReferentiel;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;

/**
 * Classe "Referentiel" de gestion des valeurs referentielles.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class Referentiel
{
    /**
     * Referentiel constructor.
     *
     * @param $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(private ContainerInterface $container, private EntityManagerInterface $em)
    {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getValeurReferentiel(array $criteres)
    {
        return $this->em->getRepository(ValeurReferentiel::class)->findOneBy($criteres);
    }

    /**
     * @return null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getIdValeurReferentiel(array $criteres)
    {
        $idRef = null;
        $valeurRef = $this->getValeurReferentiel($criteres);
        if (!empty($valeurRef)) {
            $idRef = $valeurRef->getId();
        }

        return (!empty($idRef)) ? $idRef : null;
    }

    /**
     * @return null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLibelleValeurReferentiel(array $criteres)
    {
        $libelle = null;
        $valeurRef = $this->getValeurReferentiel($criteres);
        if (!empty($valeurRef)) {
            $libelle = $valeurRef->getLibelleValeurReferentiel(); //TODO $valeurRef->getLibelleValeurReferentielTraduit()
        }

        return (!empty($libelle)) ? $libelle : null;
    }
}
