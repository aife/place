<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Service\Agent\StatisticsService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Stopwatch\Stopwatch;

class BiMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'BI';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 200 HTTP code';

    public function __construct(
        private StatisticsService $statistics,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('access');
            $codeResponse = $this->statistics->getContent('pingForMonitoring');
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');
            if (Response::HTTP_OK === $codeResponse) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'enabled' => true,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in BI service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
