<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ExecMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'EXEC';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 200 HTTP code';

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('access');
            $response = $this->client->request(
                'GET',
                $this->parameterBag->get('URL_EXEC') . '/exec-api/actuator/health'
            );

            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');
            if ('200' == $response->getStatusCode()) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in EXEC service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
