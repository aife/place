<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Repository\ConsultationRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class DatabaseMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'database';
    public const SERVICE_DESCRIPTION_STATUS = 'SELECT * FROM consultation LIMIT 10';

    public function __construct(
        private ConsultationRepository $consultationRepository,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('reading-database');
            $listConsultation = $this->consultationRepository->findBy([], [], 10, 0);
            $this->stopwatch->stop('reading-database');
            $eventReading = $this->stopwatch->getEvent('reading-database');

            return [self::SERVICE_NAME => [
                'status' => self::STATUS_OK,
                'access_time_millisecond' => sprintf('%d ms', $eventReading->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in Database service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return [self::SERVICE_NAME => [
                'status' => self::STATUS_KO,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
