<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Service\Messagerie\WebServicesMessagerie;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class MessecMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'Messec';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return no empty token';

    public function __construct(
        private WebServicesMessagerie $messec,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $params = [
                'idObjetMetier' => 1,
                'refObjetMetier' => 'Ref',
                'mailDestinataire' => 'test@email.com',
                'metaDonnees' => [],
            ];

            $this->stopwatch->start('access');
            $response = $this->messec->getContent('initialisationSuiviToken', $params);
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');

            if ('' !== $response) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in Messec service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
