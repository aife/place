<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class EnvolMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'ENVOL';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 415 HTTP code';

    public function __construct(
        private HttpClientInterface $client,
        private ParameterBagInterface $parameterBag,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $params = [
                'reference' => 'consultation-reference',
                'nomDossierVolumineux' => 'file-name',
                'serverLogin' => $this->parameterBag->get('LOGIN_TIERS_TUS'),
                'serverPassword' => $this->parameterBag->get('PWD_TIERS_TUS'),
                'serverURL' => $this->parameterBag->get('PF_URL_REFERENCE'),
                'statut' => '1',
            ];
            $this->stopwatch->start('access');
            $response = $this->client->request(
                'POST',
                $this->parameterBag->get('URL_API_TUS') . 'session/REE55G',
                ['body' => $params]
            )->getStatusCode();
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');
            if (Response::HTTP_UNSUPPORTED_MEDIA_TYPE === $response) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in MOL service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
