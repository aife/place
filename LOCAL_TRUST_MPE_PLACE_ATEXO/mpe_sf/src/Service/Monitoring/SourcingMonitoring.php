<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Service\WebServicesSourcing;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Stopwatch\Stopwatch;

class SourcingMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'Sourcing';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 200 or 400 HTTP code';

    public function __construct(
        private WebServicesSourcing $sourcing,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('access');
            $codeResponse = $this->sourcing->getContent('pingForMonitoring');
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');
            if (Response::HTTP_BAD_REQUEST === $codeResponse || Response::HTTP_OK === $codeResponse) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in Sourcing service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
