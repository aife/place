<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Utils\Filesystem\MountManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class FilesystemMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'fileSystem';
    public const SERVICE_DESCRIPTION_STATUS = 'Create and read file on NAS';
    public const SERVICE_FILE_TEST = '/tmp/MonitoringFileSystem.txt';

    public function __construct(
        private MountManager $mountManager,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('writing-file');
            $filesystem = $this->mountManager->getFilesystem('nas');
            $filesystem->write(self::SERVICE_FILE_TEST, 'Monitoring ...');
            $this->stopwatch->stop('writing-file');
            $eventWriting = $this->stopwatch->getEvent('writing-file');

            $this->stopwatch->start('reading-file');
            $content = $filesystem->read(self::SERVICE_FILE_TEST);
            $this->stopwatch->stop('reading-file');
            $eventReading = $this->stopwatch->getEvent('reading-file');
            $filesystem->delete(self::SERVICE_FILE_TEST);

            return [self::SERVICE_NAME => [
                'status' => self::STATUS_OK,
                'write_time_millisecond' => sprintf('%d ms', $eventWriting->getDuration()),
                'access_time_millisecond' => sprintf('%d ms', $eventReading->getDuration()),
                'description' => self::SERVICE_FILE_TEST
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in Filesystem service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return [self::SERVICE_NAME => [
                'status' => self::STATUS_KO,
                'write_time_millisecond' => 0,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_FILE_TEST
            ]];
        }
    }
}
