<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use Atexo\CryptoBundle\AtexoCrypto;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class CryptoMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'Crypto';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 201 HTTP code';

    public function __construct(
        private AtexoCrypto $crypt,
        private ParameterBagInterface $parameterBag,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $params['MpeURLPrivee'] = $this->parameterBag->get('MPE_WS_URL');
            $params['ServeurCryptoURLPublic'] = $this->parameterBag->get('URL_CRYPTO');
            $params['MpeFilePath'] = $this->parameterBag->get('BASE_ROOT_DIR')
                . $this->parameterBag->get('CRYPTO_ORGANISME')
                . $this->parameterBag->get('FILES_DIR');
            $params['MpeLogin'] = $this->parameterBag->get('MPE_WS_LOGIN_SRV_CRYPTO');
            $params['MpePassword'] = $this->parameterBag->get('MPE_WS_PASSWORD_SRV_CRYPTO');
            $params['Format'] = $this->parameterBag->get('FORMAT_RETOUR_CRYPTO');
            $params['EnLigne'] = $this->parameterBag->get('EN_LIGNE');

            $this->stopwatch->start('access');
            $response = $this->crypt->verifierDisponibilite($params);
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');
            $returnedObject = json_decode($response, false, 512, JSON_THROW_ON_ERROR);

            if (201 === $returnedObject->code) {
                return ['externalModule' => [
                    'name' => self::SERVICE_NAME,
                    'status' => self::STATUS_OK,
                    'enabled' => true,
                    'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                    'description' => self::SERVICE_DESCRIPTION_STATUS
                ]];
            }

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in Crypto service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
