<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use App\Service\AtexoDumeService;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class DumeMonitoring implements MonitoringInterface
{
    public const SERVICE_NAME = 'DUME';
    public const SERVICE_DESCRIPTION_STATUS = 'MUST return 200 HTTP code';

    public function __construct(
        private AtexoDumeService $dume,
        private ParameterBagInterface $parameterBag,
        private Stopwatch $stopwatch,
        private LoggerInterface $logger
    ) {
    }

    public function check(): array
    {
        try {
            $this->stopwatch->start('access');
            $clientDume = $this->dume->createConnexionLtDume();
            $response = $clientDume->authentificationService()->login(
                $this->parameterBag->get('LOGIN_DUME_API'),
                $this->parameterBag->get('PASSWORD_DUME_API'),
                $this->parameterBag->get('TYPE_DUME_OE')
            );
            $this->stopwatch->stop('access');
            $event = $this->stopwatch->getEvent('access');

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_OK,
                'enabled' => true,
                'access_time_millisecond' => sprintf('%d ms', $event->getDuration()),
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        } catch (\Exception $exception) {
            $this->logger->error('Monitoring error in DUME service', [
                'error' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]);

            return ['externalModule' => [
                'name' => self::SERVICE_NAME,
                'status' => self::STATUS_KO,
                'enabled' => true,
                'access_time_millisecond' => 0,
                'description' => self::SERVICE_DESCRIPTION_STATUS
            ]];
        }
    }
}
