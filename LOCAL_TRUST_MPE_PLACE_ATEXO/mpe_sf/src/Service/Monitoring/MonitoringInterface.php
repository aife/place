<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Monitoring;

use Symfony\Component\DependencyInjection\Attribute\Autoconfigure;

#[Autoconfigure(tags: [MonitoringInterface::class])]
interface MonitoringInterface
{
    public const STATUS_OK = 'OK';
    public const STATUS_KO = 'KO';
    public function check(): array;
}
