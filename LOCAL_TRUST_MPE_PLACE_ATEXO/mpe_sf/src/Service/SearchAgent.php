<?php

namespace App\Service;

use Doctrine\DBAL\Exception as DBALException;
use Exception;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Utils\ConsultationCriteriaVo;
use App\Utils\EntityPurchase;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class SearchAgent extends Search
{
    private $agent;
    private ?string $isEnabledOrganisme = null;
    private $habilitationAgent;
    protected EntityManagerInterface $em;
    protected ContainerInterface $container;
    protected ?bool $hasHabilitationInvitePermanent = null;
    protected $choix1;
    protected $choix2;
    protected $choix3;
    protected $choix4;
    protected $choix5;
    protected $isEnabled;

    /**
     * SearchAgent constructor.
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AtexoUtil $serviceUtile,
        protected AtexoConfiguration $configuration,
        protected Security $security,
        SessionInterface $session,
        private readonly EntityPurchase $entityPurchase
    ) {
        parent::__construct($container, $em, $logger, $serviceUtile, $session);

        $this->container = $container;
        $this->em = $em;
        $this->choix3 = $this->container->getParameter('REGLE_VALIDATION_TYPE_3');
        $this->choix4 = $this->container->getParameter('REGLE_VALIDATION_TYPE_4');
        $this->logger = $logger;
        $this->serviceUtile = $serviceUtile;
    }

    /**
     * @param bool $returnJustNumberOfElement
     * @param bool $returnReferences
     * @param bool $forAlertesEntreprises
     *
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function getConsultationsByQuerySf(
        ConsultationCriteriaVo $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false
    ): array|int {
        $QueryForValidationOnlyAgent = null;
        $QueryForAgentInvitedWithTheirHabilitation = null;
        $QueryForReadOnlyAgent = null;
        $queryParams = [];
        $queryServiceNon = '';
        $queryEtatAttenteValidation = '';
        $queryLeftJoinOrganisme = '';
        $queryJoinCondition = '';
        $queryJoinConditionValidation = '';
        $queryValidationIntermediaire = '';
        $queryValidationFinale = '';
        $queryValidationSimple = '';
        $listOfServicesAllowed = null;

        // Test des parametres obligatoires
        if (
            '' === $criteriaVo->getAcronymeOrganisme()
            || '' === $criteriaVo->getIdService()
        ) {
            throw new Exception('Les parametres AcronymeOrganisme et IdService sont obligatoires');
        }

        $this->agent = $this->security->getUser();
        $this->agent = ($this->agent instanceof Agent) ? $this->agent : null;

        if ($this->agent === null) {
            $contexteAuthentification = $this->container->get('session')->get('contexte_authentification');
            if (
                (is_countable($contexteAuthentification) ? count($contexteAuthentification) : 0) > 0
                && isset($contexteAuthentification['id'])
            ) {
                $this->agent = $this
                    ->em
                    ->getRepository(Agent::class)
                    ->findOneById($contexteAuthentification['id']);
            }
        }

        $this->habilitationAgent = $this
            ->em
            ->getRepository(HabilitationAgent::class)
            ->findOneByAgent($this->agent->getId());
        $this->isEnabledOrganisme = 'd';

        if ($returnJustNumberOfElement || $returnReferences) {
            $querySelect = $this->getSqlCountAgent();
        } else {
            $querySelect = $this->getSqlAgent();
        }

        if (!$criteriaVo->getCalledFromHelios() && '' !== $criteriaVo->getIdService()) {
            //On recupere la liste des entités qui ont le droit de voir la consultation organisation_centralisee
            $organisationCentralisee = $this->configuration
                ->hasConfigOrganisme($criteriaVo->getAcronymeOrganisme(), 'organisationCentralisee');

            if ($organisationCentralisee) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = $this->getInvitePermanentMonEntite(
                    $this->habilitationAgent->getInvitePermanentMonEntite(),
                    $this->agent->getServiceId()
                );
                $invitePermanentEntiteDependante = $this->habilitationAgent->getInvitePermanentEntiteDependante();
                $idServiceAgentConnected = $this->agent->getServiceId();
                $sousServicesIncluantLeService = $this->entityPurchase->retrieveAllChildrenServices(
                    $criteriaVo->getIdService(),
                    $criteriaVo->getAcronymeOrganisme(),
                    true
                );

                $listOfServicesAllowed = $this->getInvitePermanentEntiteDependante(
                    $listOfServicesAllowed,
                    $invitePermanentEntiteDependante,
                    $idServiceAgentConnected,
                    $sousServicesIncluantLeService
                );
                $invitePermanentTransverse = $this->habilitationAgent->getInvitePermanentTransverse();
                $idUser = $this->agent->getId();
                $listOfServicesAllowed = $this->getInvitePermanentTransverse(
                    $listOfServicesAllowed,
                    $invitePermanentTransverse,
                    $idUser
                );
                if ($this->hasHabilitationInvitePermanent) {
                    $listOfServicesAllowed = array_filter($listOfServicesAllowed, fn($value) => !empty($value));
                    $listOfServicesAllowed = '(' . str_replace(
                        ',,',
                        ',',
                        implode(',', $listOfServicesAllowed)
                    ) . ')';
                } else {
                    $listOfServicesAllowed = '( )';
                }

                $queryServiceNon = $this->hasHabilitationInvitePermanent ? 'consultation.service_id IN ' .
                    $listOfServicesAllowed . ' ' : '';

                $validationFinale = $this->habilitationAgent->getValidationFinale();
                $serviceValidationFinale = $this->getListServiceValidationFinaleModeCentralise(
                    $this->agent->getServiceId(),
                    $this->agent->getOrganisme()
                );
                $queryValidationFinale = $this->getSqlValidationFinal($validationFinale, $serviceValidationFinale);
            } else {
                if ('' !== $criteriaVo->getIdService()) {
                    $listOfServicesAllowed = $criteriaVo->getIdService() ?? 0;
                    $idService = $this->agent->getServiceId();
                    if ($this->habilitationAgent->getInvitePermanentMonEntite()) {
                        if (is_null($idService)) {
                            $queryServiceNon = " consultation.service_id is null and consultation.organisme = '"
                                . $this->agent->getOrganisme()->getAcronyme() . "' ";
                        } else {
                            $queryServiceNon = " consultation.service_id = '" . $idService . "' ";
                        }
                    }

                    $validationFinale = $this->habilitationAgent->getValidationFinale();
                    $queryValidationFinale = $this->getSqlValidationFinal(
                        $validationFinale,
                        $idService
                    );

                    $queryEtatAttenteValidation = "AND consultation.etat_en_attente_validation ='1'";
                }
            }

            if (null !== $listOfServicesAllowed) {
                if ($criteriaVo->getNoTemporaryCOnsultation()) {
                    $queryValidationIntermediaire = '';
                    $queryValidationFinale = '';
                    $queryValidationSimple = '';
                } else {
                    //Si et SSi une des deux options actives : cons a approuver ou cons a valider
                    if (
                        $criteriaVo->getConsultationAValiderSeulement() ||
                        $criteriaVo->getConsultationAApprouverSeulement()
                    ) {
                        // Si les consultations a retourner sont a valider ou a approuver seulement,
                        // la requete sur la liste des
                        //services autorise par defaut supprimee.
                        $queryServiceNon = '';
                        if (!$criteriaVo->getConsultationAApprouverSeulement()) {
                            // Si l'option 'consulation a approuver' n'est pas a true
                            // => ne pas remonter les cons a approuver
                            $queryValidationIntermediaire = '';
                        }
                        if (!$criteriaVo->getConsultationAValiderSeulement()) {
                            //Si l'option 'consulation a valider' n'est pas a true => ne pas remonter les cons a valider
                            $queryValidationSimple = '';
                            $queryValidationFinale = '';
                        }
                    }
                }
                $queryJoinCondition = ($queryServiceNon) ? ' AND ( ' . $queryServiceNon . ')' : ' AND 0 ';

                if ($this->hasHabilitationInvitePermanent || $this->habilitationAgent->getValidationFinale()) {
                    $queryValidationFinale = $queryValidationFinale ? ' OR ' . $queryValidationFinale : '';
                    $queryValidationIntermediaire = $queryValidationIntermediaire
                        ? ' OR ' . $queryValidationIntermediaire : '';
                    $queryValidationSimple = $queryValidationSimple ? ' OR ' . $queryValidationSimple : '';
                } else {
                    $queryValidationFinale = '';
                    $queryValidationIntermediaire = '';
                    $queryValidationSimple = '';
                }
                $queryJoinConditionValidation .= <<<QUERY
AND ( 0 $queryValidationFinale $queryValidationIntermediaire $queryValidationSimple )
$queryEtatAttenteValidation
QUERY;
            }
        }

        $queryLeftJoinCategorieLot = ($criteriaVo->getKeyWordAdvancedSearch() ||
            $criteriaVo->getKeyWordRechercheRapide() ||
            $criteriaVo->getIdCodeCpv2()) ? $this->getSqlLeftJoinCategorieLot() : '';

        $queryFrom = <<<QUERY
FROM consultation
		$queryLeftJoinCategorieLot
		$queryLeftJoinOrganisme
QUERY;

        //on parcourt l'objet CriteriaVo passe en parametre pour ajouter a la clause where
        $otherConditions = $this->otherConditionsAgent($criteriaVo, $queryParams);
        $stateCondition = $this->stateConditionsAgent($criteriaVo, $queryParams);
        $otherConditions .= $stateCondition;
        $idAgentConnecte = $criteriaVo->getConnectedAgentId();

        $queryParams['achatPublique'] = '0';
        $queryParams['organismeAgent'] = $criteriaVo->getAcronymeOrganisme();
        $conditionFromAcheteur = ' consultation.organisme =:organismeAgent '
            . 'AND consultation.consultation_achat_publique=:achatPublique ';

        $querySelectAvecHabilitation = (!$returnJustNumberOfElement && !$returnReferences)
            ? ', 1 as invitedWithHisHabilitation, 0 as invitedReadOnly, 0 as invitedValidationOnly'
            : '';
        $queryForPole = <<<QUERY
$querySelect $querySelectAvecHabilitation
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinCondition
	$otherConditions

QUERY;
        //requete 2 : la liste des consultations auxquelles il est invite avec ses habilitations
        if (!$criteriaVo->getConsultationAValiderSeulement() && !$criteriaVo->getConsultationAApprouverSeulement()) {
            $QueryForAgentInvitedWithTheirHabilitation = <<<QUERY
$querySelect  $querySelectAvecHabilitation
$queryFrom , InterneConsultation
WHERE
	 $conditionFromAcheteur
	 AND InterneConsultation.interne_id = '$idAgentConnecte'
	 AND InterneConsultation.consultation_id = consultation.id
	 AND InterneConsultation.organisme = consultation.organisme
	 $otherConditions

QUERY;

            $querySelectInvitedReadOnly = (!$returnJustNumberOfElement && !$returnReferences)
                ? ', 0 as invitedWithHisHabilitation, 1 as invitedReadOnly, 0 as invitedValidationOnly'
                : '';
            //requete 3 : la liste des consultations auxquelles l'agent est invite en lecture seule
            $QueryForReadOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedReadOnly
$queryFrom , InterneConsultationSuiviSeul
WHERE
	$conditionFromAcheteur
	AND InterneConsultationSuiviSeul.interne_id = '$idAgentConnecte'
	AND InterneConsultationSuiviSeul.consultation_id = consultation.id
	AND InterneConsultationSuiviSeul.organisme = consultation.organisme
	$otherConditions

QUERY;
            $querySelectInvitedValidationOnly = (!$returnJustNumberOfElement && !$returnReferences)
                ? ' , 0 as invitedWithHisHabilitation, 0 as invitedReadOnly, 1 as invitedValidationOnly'
                : '';
            $QueryForValidationOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedValidationOnly
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinConditionValidation
	$otherConditions

QUERY;
        }

        //Fusionner les 3 requetes
        $finalQueryPart1 = $queryForPole . ($QueryForValidationOnlyAgent
                ? ' UNION ' . $QueryForValidationOnlyAgent
                : '');
        $finalQueryPart2 = ($QueryForAgentInvitedWithTheirHabilitation ? $QueryForAgentInvitedWithTheirHabilitation .
                ' UNION ' : '') . $QueryForReadOnlyAgent;

        $finalQuery = ($finalQueryPart1) ? $finalQueryPart1 . (($finalQueryPart2)
                ? ' UNION ' . $finalQueryPart2 : ' ')
                : $finalQueryPart2;

        if (!$finalQuery) {
            if ($returnJustNumberOfElement) {
                return 0;
            } else {
                return [];
            }
        }
        if (!$returnJustNumberOfElement && !$returnReferences) {
            if ($criteriaVo->getSortByElement()) {
                $finalQuery .= ' ORDER BY ' . $criteriaVo->getSortByElement() . ' ' .
                    $criteriaVo->getSensOrderBy() . ' ' .
                    (('datefin' != strtolower($criteriaVo->getSortByElement())) ? ' , datefin DESC ' : ' ');
            } else {
                $finalQuery .= ' ORDER BY datefin DESC ';
            }
        }

        $finalQuery = preg_replace("#\([ ]+OR[ ]+\(#", '( (', $finalQuery);
        if ($forAlertesEntreprises) {
            $finalQuery = str_replace('consultation.', 'consultation_alertes.', $finalQuery);
            $finalQuery = str_replace('FROM consultation', 'FROM consultation_alertes', $finalQuery);
        }

        $stmt = $this->em->getConnection()->executeQuery($finalQuery, $queryParams);

        return $stmt->fetchAllAssociative();
    }

    /**
     * Methode qui prépare le comptage des consultations.
     *
     * @return string
     */
    public function getSqlCountAgent()
    {
        return <<<QUERY
SELECT
   DISTINCT consultation.id
QUERY;
    }

    /**
     * Méthode qui prépare les entête de la requête SQL.
     *
     * @return string
     */
    public function getSqlAgent()
    {
        return <<<QUERY
SELECT
   DISTINCT consultation.id,
   consultation.datefin,
   reference_utilisateur,
   nom_createur,
   prenom_createur,
   intitule,
   objet,
   id_type_avis,
   alloti
QUERY;
    }

    public function getSqlLeftJoinCategorieLot()
    {
        $queryLeftJoinCategorieLot = <<<QUERY
LEFT JOIN
		CategorieLot ON (CategorieLot.consultation_id = consultation.id
		AND CategorieLot.organisme = consultation.organisme)
QUERY;

        return $queryLeftJoinCategorieLot;
    }

    /**
     * @param $invitePermanentMonEntite
     * @param $idServiceAgentConnected
     *
     * @return array|null
     */
    public function getInvitePermanentMonEntite($invitePermanentMonEntite, $idServiceAgentConnected)
    {
        $listOfServicesAllowed = null;
        if ($invitePermanentMonEntite) {
            $this->hasHabilitationInvitePermanent = true;
            $listOfServicesAllowed = [$idServiceAgentConnected];
        }

        return $listOfServicesAllowed;
    }

    /**
     * @param $listOfServicesAllowed
     * @param $invitePermanentEntiteDependante
     * @param $idServiceAgentConnected
     * @param $sousServicesIncluantLeService
     *
     * @return array|null
     */
    public function getInvitePermanentEntiteDependante(
        $listOfServicesAllowed,
        $invitePermanentEntiteDependante,
        $idServiceAgentConnected,
        $sousServicesIncluantLeService
    ) {
        if ($invitePermanentEntiteDependante) {
            $this->hasHabilitationInvitePermanent = true;
            $sousServices = array_diff($sousServicesIncluantLeService, [$idServiceAgentConnected]);
            if (null === $listOfServicesAllowed) {
                $listOfServicesAllowed = [];
            }
            $listOfServicesAllowed = array_merge($sousServices, $listOfServicesAllowed);
            if (0 == count($listOfServicesAllowed)) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = null;
            }
        }

        return $listOfServicesAllowed;
    }

    /**
     * @param $listOfServicesAllowed
     * @param $invitePermanentTransverse
     * @param $idUser
     *
     * @return array|null
     */
    public function getInvitePermanentTransverse($listOfServicesAllowed, $invitePermanentTransverse, $idUser)
    {
        if ($invitePermanentTransverse) {
            $this->hasHabilitationInvitePermanent = true;
            if (null === $listOfServicesAllowed) {
                $listOfServicesAllowed = [];
            }
            $listOfServicesAllowed = array_merge(
                $this->em->getRepository(InvitePermanentTransverse::class)->getServicesIdByAgentId($idUser),
                $listOfServicesAllowed
            );
            if (0 == count($listOfServicesAllowed)) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = null;
            }
        }

        return $listOfServicesAllowed;
    }

    /*
     * @param $listOfServicesAllowed
     * @return string|null
     */
    public function getSqlValidationIntermediaire(
        $listOfServicesAllowed,
        $habilitationValidationIntermediaire,
        $operateur = 'IN'
    ) {
        $queryValidationIntermediaire = null;
        if ($habilitationValidationIntermediaire) {
            $queryValidationIntermediaire = <<<QUERY
((consultation.id_regle_validation='$this->choix4' OR consultation.id_regle_validation='$this->choix5' )
AND service_validation_intermediaire $operateur $listOfServicesAllowed
AND (date_validation_intermediaire IS NULL OR date_validation_intermediaire = '0000-00-00 00:00:00') )
QUERY;
        }

        return $queryValidationIntermediaire;
    }

    /**
     * @param $validationFinale
     * @param $serviceValidationFinale
     * @param string $operateur
     *
     * @return string|null
     */
    public function getSqlValidationFinal($validationFinale, $serviceValidationFinale, $operateur = 'IN')
    {
        $queryValidationFinale = null;
        if ($validationFinale) {
            $queryValidationFinale = <<<QUERY
(
	(
		(
			   consultation.id_regle_validation='$this->choix3'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation $operateur ('$serviceValidationFinale')
				AND (
						datevalidation IS NULL
						OR datevalidation = '0000-00-00 00:00:00'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation='$this->choix4'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation $operateur ('$serviceValidationFinale')
				AND (
					datevalidation IS NULL
					OR datevalidation = '0000-00-00 00:00:00'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != '0000-00-00 00:00:00'
				AND etat_approbation='1'
			)
	)
)
QUERY;
        }

        return $queryValidationFinale;
    }

    /**
     * @param $validationSimple
     * @param $listOfServicesAllowed
     * @param string $operateur
     *
     * @return string|null
     */
    public function getSqlValidationSimple($validationSimple, $listOfServicesAllowed, $operateur = 'IN')
    {
        $queryValidationSimple = null;
        if ($validationSimple) {
            $queryValidationSimple = <<<QUERY
(
	consultation.id_regle_validation='$this->choix1'
	AND service_validation $operateur $listOfServicesAllowed
	AND (datevalidation IS NULL OR datevalidation = '0000-00-00 00:00:00')
)
QUERY;
        }

        return $queryValidationSimple;
    }

    /**
     * @param $idServiceAgentConnected
     * @param $acronymeOrganisme
     *
     * @return array|null
     */
    public function getListServiceValidationFinaleModeCentralise($idServiceAgentConnected, $acronymeOrganisme)
    {
        //en mode centralisé : mon service et mes parents sinon en décentralisé mon service
        $allParent = $this->entityPurchase->getAllParents($idServiceAgentConnected, $acronymeOrganisme);
        $serviceValidationFinale = $idServiceAgentConnected . '';
        if (count($allParent) > 0) {
            foreach ($allParent as $parent) {
                $serviceValidationFinale .= ', ' . $parent['id'];
            }
        }

        return $serviceValidationFinale;
    }
}
