<?php


namespace App\Service;

use App\Entity\ConfigurationPlateforme;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ConfigurationPlatformService
 * @package App\Service
 */
class configurationPlatformService
{
    /**
     * Configuration constructor.
     * @param EntityManager $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @return ConfigurationPlateforme
     */
    public function getConfigurationPlateforme()
    {
        return $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
    }

    /**
     * Permet de savoir si un module est activé sur la PF
     */
    public function moduleIsEnabled(string $module, mixed $valueModule): bool
    {
        $confPlateforme = $this->em->getRepository(ConfigurationPlateforme::class)
            ->findOneBy([$module => $valueModule]);

        if ($confPlateforme) {
            $method = 'get' . $module;
            return $confPlateforme->$method();
        }

        return false;
    }

}
