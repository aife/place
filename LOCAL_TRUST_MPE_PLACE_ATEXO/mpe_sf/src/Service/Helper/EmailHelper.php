<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Helper;

use App\Entity\CategorieConsultation;
use App\Entity\GeolocalisationN2;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Entity\TypeProcedure;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class EmailHelper
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em
    ) {
    }

    public function getImageBandeau($design = null): ?string
    {
        $sourceImageBandeau = null;
        if (!empty($this->parameterBag->get('SOURCE_IMAGE_BANDEAU'))) {
            $pfUrl = $this->parameterBag->get('PF_URL_REFERENCE');
            $sourceImageBandeau = $pfUrl . trim($this->parameterBag->get('SOURCE_IMAGE_BANDEAU'), '/');
            $codeBandeauImage = html_entity_decode($this->parameterBag->get('CODE_BANDEAU_IMAGE'));
            $sourceImageBandeau = str_replace('SOURCE_IMAGE_BANDEAU', $sourceImageBandeau, $codeBandeauImage);
            if ($design) {
                $sourceImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/' . $design . '/images/',
                    $sourceImageBandeau
                );
            }
        }

        return $sourceImageBandeau;
    }


    public function getUrlImageBandeau($design = null): ?string
    {
        $sourceImageBandeau = null;
        if (!empty($this->parameterBag->get('SOURCE_IMAGE_BANDEAU'))) {
            $pfUrl = $this->parameterBag->get('PF_URL_REFERENCE');
            $sourceImageBandeau = $pfUrl . trim($this->parameterBag->get('SOURCE_IMAGE_BANDEAU'), '/');
            if ($design) {
                $sourceImageBandeau = str_replace(
                    '/themes/images/',
                    '/themes/' . $design . '/images/',
                    $sourceImageBandeau
                );
            }
        }

        return $sourceImageBandeau;
    }

    public function buildHtmlCriteriaForEmail(array $criteria): array
    {
        $emailCriteria = [];
        if (array_key_exists('orgDenomination', $criteria)) {
            $emailCriteria['organisme'] = $criteria['orgDenomination'];
        }

        if (array_key_exists('acronymeOrg', $criteria)) {
            $emailCriteria['organisme'] = $this->em->getRepository(Organisme::class)->findOneBy(
                ['acronyme' => $criteria['acronymeOrg']]
            )?->getDenominationOrg();
        }

        if (array_key_exists('service.id', $criteria)) {
            $emailCriteria['organisme'] = $this->em->getRepository(Service::class)->find($criteria['service.id'])
                ->getLibelle();
        }

        if (array_key_exists('typeProcedure', $criteria)) {
            $emailCriteria['typeProcedure'] = $this->em->getRepository(TypeProcedure::class)
                ->findOneByIdTypeProcedure($criteria['typeProcedure'])?->getLibelleTypeProcedure();
        }

        if (array_key_exists('categorie', $criteria)) {
            $emailCriteria['categorie'] = $this->em->getRepository(CategorieConsultation::class)
                ->findOneById($criteria['categorie'])->getLibelle();
        }

        if (array_key_exists('search_full', $criteria)) {
            $emailCriteria['keywords'] = implode(' ', $criteria['search_full']);
        }

        if (array_key_exists('lieuExecution', $criteria)) {
            $geoN2 = $this->em->getRepository(GeolocalisationN2::class)->findById($criteria['lieuExecution']);
            $labelGeoN2 = [];
            foreach ($geoN2 as $geo) {
                $labelGeoN2[] = $geo->getDenomination1();
            }

            $emailCriteria['lieuExecution'] = implode(',', $labelGeoN2);
        }

        if (array_key_exists('codesCPV', $criteria)) {
            $emailCriteria['codesCPV'] = implode(';', $criteria['codesCPV']);
        }

        return $emailCriteria;
    }
}
