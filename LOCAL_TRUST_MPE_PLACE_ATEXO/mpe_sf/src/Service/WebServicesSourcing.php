<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use DOMDocument;
use App\Attribute\ResultType;
use App\Entity\Agent;
use App\Security\ClientOpenIdConnect;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\HttpFoundation\Request;

class WebServicesSourcing extends AbstractService
{
    private string $idContext = '';
    private string $token = '';

    /**
     * @var string
     */
    final public const CLIENT_ID = 'mpe';

    final public const PLATEFORME = 'mpe';

    /**
     * @var string
     */
    final public const GRANT_TYPE = 'password';

    final public const TYPE_SEARCH_MARCHE = 'MARCHE';
    final public const TYPE_SEARCH_CONCESSION = 'CONCESSION';

    /**
     * WebServicesSourcing constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly CpvService $cpvService,
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        AgentService $agentService,
        private readonly OrganismeService $organismeService
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
        $this->agentService = $agentService;
    }

    #[ResultType(ResultType::URL)]
    protected function getUrlSourcing(Agent $agent): string
    {
        $this->token = $this->getToken();

        $this->idContext = $this->getAgentContext(
            $this->getSourcingBaseUrl() . '/api-agent/agents',
            $agent,
            $this->token
        );

        if ('' === $this->idContext) {
            throw new NotFoundHttpException('Cette ressource n\'existe pas !');
        }

        return $this->getSourcingBaseUrl()
            . '/agent/login/' . $this->token . '/' . $this->idContext;
    }

    private function getSourcingBaseUrl(): string
    {
        return $this->parameterBag->get('SOURCING_BASE_URL');
    }


    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchDecpContracts($criteriaVo): mixed
    {
        $url = $this->getSourcingBaseUrl() . '/api-agent/contracts/decp/search';
        $body = [
            'attributaires' => $criteriaVo->getNomAttributaire() ? [$criteriaVo->getNomAttributaire()] : null, // array
            'nomLieuxExecution' => $criteriaVo->getLieuExecution()
                ? $this->formatLieuxExecution(
                    $criteriaVo->getLieuExecution()
                ) : null,
            'codesCPV' => $criteriaVo->getCodeCpv() ? explode(',', (string)$criteriaVo->getCodeCpv()) : null, // array
            'dateNotificationBegin' => $criteriaVo->getDateNotificationStart() ?: null,
            'dateNotificationEnd' => $criteriaVo->getDateNotificationEnd() ?: null,
            'montantMax' => $criteriaVo->getMontantMax() ?: null,
            'montantMin' => $criteriaVo->getMontantMin() ?: null,
            'naturePrestation' => $criteriaVo->getCategorie() ?
                $this->cpvService->getCategorieLabel($criteriaVo->getCategorie()) : null,
            'objet' => $criteriaVo->getMotsCles() ?: null,
            'year' => $criteriaVo->getAnneeNotification(),
            'number' => empty($criteriaVo->getOffset()) ? 0 : $criteriaVo->getOffset(),
            'size' => empty($criteriaVo->getLimit()) ? 10 : $criteriaVo->getLimit(),
            'sort' => !empty($criteriaVo->getChampEtalabOrderBy()) ?
                [
                    'asc' => 'ASC' == $criteriaVo->getSensEtalabOrderBy(),
                    'field' => $criteriaVo->getChampEtalabOrderBy(),
                ]
                : null,
            'typeContrat' => $criteriaVo->getConcession() ? self::TYPE_SEARCH_CONCESSION : self::TYPE_SEARCH_MARCHE,
        ];
        if (!empty($criteriaVo->getSiretAcheteurs()) && !empty($criteriaVo->getSiretAcheteurs()[0])) {
            $body['siretAcheteur'] = $criteriaVo->getSiretAcheteurs();
        }

        $token = $this->getToken();
        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => json_encode($body, JSON_THROW_ON_ERROR),
        ];

        return $this->request('POST', $url, $options)->getContent();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function getDecpContract($numeroIdentificationUnique): mixed
    {
        $url = $this->getSourcingBaseUrl() . '/api-agent/contracts/decp/' . $numeroIdentificationUnique;
        $token = $this->getToken();
        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
        ];

        return $this->request('GET', $url, $options)->getContent();
    }

    /**
     * @param $criteriaVo
     *
     * @return string
     *
     * @throws HttpExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getDonneesEssentiellesEtalabFormatXml($criteriaVo)
    {
        $url = $this->getSourcingBaseUrl() . '/api-agent/contracts/decp/source-xml';
        $body = [
            'attributaires' => $criteriaVo->getNomAttributaire() ? [$criteriaVo->getNomAttributaire()] : null,
            'nomLieuxExecution' => $criteriaVo->getLieuExecution() ?
                $this->formatLieuxExecution($criteriaVo->getLieuExecution()) : null,
            'codesCPV' => $criteriaVo->getCodeCpv() ? explode(',', (string)$criteriaVo->getCodeCpv()) : null,
            'dateNotificationBegin' => $criteriaVo->getDateNotificationStart() ?: null,
            'dateNotificationEnd' => $criteriaVo->getDateNotificationEnd() ?: null,
            'montantMax' => $criteriaVo->getMontantMax() ?: null,
            'montantMin' => $criteriaVo->getMontantMin() ?: null,
            'naturePrestation' => $criteriaVo->getCategorie() ?
                $this->cpvService->getCategorieLabel($criteriaVo->getCategorie()) : null,
            'objet' => $criteriaVo->getMotsCles() ?: null,
            'year' => $criteriaVo->getAnneeNotification(),
            'number' => empty($criteriaVo->getOffset()) ? 0 : $criteriaVo->getOffset(),
            'size' => empty($criteriaVo->getLimit()) ? 10 : $criteriaVo->getLimit(),
            'sort' => !empty($criteriaVo->getChampEtalabOrderBy()) ?
                [
                    'asc' => 'ASC' == $criteriaVo->getSensEtalabOrderBy(),
                    'field' => $criteriaVo->getChampEtalabOrderBy(),
                ]
                : null,
            'typeContrat' => $criteriaVo->getConcession() ? self::TYPE_SEARCH_CONCESSION : self::TYPE_SEARCH_MARCHE,
        ];

        if (!empty($criteriaVo->getSiretAcheteurs()) && !empty($criteriaVo->getSiretAcheteurs()[0])) {
            $body['siretAcheteur'] = $criteriaVo->getSiretAcheteurs();
        }

        $token = $this->getToken();
        $options = [
            'headers' => [
                'accept: */*',
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'body' => json_encode($body, JSON_THROW_ON_ERROR),
        ];

        return $this->request('POST', $url, $options)->getContent();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function isCompanyRegistered(string $email)
    {
        $url = $this->getSourcingBaseUrl() . '/api-agent/agents/contact?email=' . $email;

        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
        ];

        return $this->request('GET', $url, $options)->getContent();
    }

    protected function setDataXML(Agent $agent): false|string
    {
        $xml = new DOMDocument('1.0', 'ISO-8859-15');

        $xml_agent = $xml->createElement('agent');
        $xml_agent_id = $xml->createElement('id', $agent->getId());
        $xml_agent_identifiant = $xml->createElement('identifiant', $agent->getLogin());
        $xml_agent_plateforme = $xml->createElement('plateforme', $this->parameterBag->get('UID_PF_MPE'));
        $xml_agent_acronymeOrganisme = $xml->createElement(
            'acronymeOrganisme',
            $agent->getAcronymeOrganisme()
        );
        $xml_agent_email = $xml->createElement('email', $agent->getEmail());
        $xml_agent_telephone = $xml->createElement('telephone', $agent->getTelephone() ?? '');
        $xml_agent_fax = $xml->createElement('fax', $agent->getFax() ?? '');
        $xml_agent_nomCourantAcheteurPublic = $xml->createElement(
            'nomCourantAcheteurPublic',
            $this->organismeService->getNomCourantAcheteurPublic($agent)
        );
        $xml_agent_nom = $xml->createElement('nom', $agent->getNom());
        $xml_agent_prenom = $xml->createElement('prenom', $agent->getPrenom());
        $api = $xml->createElement('api');
        $apiUrl = rtrim($this->parameterBag->get('PF_URL_REFERENCE'), "/");
        $apiUrl = $xml->createElement('url', $apiUrl);
        $apiEntrepriseRecipient = $xml->createElement(
            'apiEntrepriseRecipient',
            $this->parameterBag->get('API_ENTREPRISE_SIRET_CLIENT')
        );
        $apiEntrepriseToken = $xml->createElement(
            'apiEntrepriseToken',
            $this->parameterBag->get('API_ENTREPRISE_TOKEN')
        );

        $xml_mpe = $xml->createElement('mpe');
        $xml_mpe->setAttribute('xmlns', 'http://www.atexo.com/epm/xml');

        $xml_envoi = $xml->createElement('envoi');

        $xml_organisme = $xml->createElement('organisme');
        $xml_organisme_id = $xml->createElement('id', $agent->getOrganisme()?->getId() ?? '');
        $xml_organisme_acronyme = $xml->createElement(
            'acronyme',
            $agent->getOrganisme()?->getAcronyme() ?? ''
        );
        $xml_organisme_denominationOrganisme = $xml->createElement(
            'denominationOrganisme',
            $agent->getOrganisme()?->getDenominationOrg() ?? ''
        );
        $xml_organisme_siren = $xml->createElement(
            'siren',
            $agent->getOrganisme()?->getSiren() ?? ''
        );
        $xml_organisme_complement = $xml->createElement(
            'complement',
            $agent->getOrganisme()?->getComplement() ?? ''
        );

        $xml_organisme->appendChild($xml_organisme_id);
        $xml_organisme->appendChild($xml_organisme_acronyme);
        $xml_organisme->appendChild($xml_organisme_denominationOrganisme);
        $xml_organisme->appendChild($xml_organisme_siren);
        $xml_organisme->appendChild($xml_organisme_complement);

        $xml_service = $xml->createElement('service');
        $xml_service_id = $xml->createElement('id', $agent->getServiceId() ?? '');

        $xml_service_libelle = $xml->createElement(
            'libelle',
            $agent->getServiceId() ? $this->agentService->getLabelService($agent, false) : ''
        );
        $xml_service_siren = $xml->createElement(
            'siren',
            $agent->getServiceId() ? $this->agentService->getSirenService($agent) : ''
        );
        $xml_service_complement = $xml->createElement(
            'complement',
            $agent->getServiceId() ? $this->agentService->getComplementService($agent) : ''
        );

        $xml_service->appendChild($xml_service_id);
        $xml_service->appendChild($xml_service_libelle);
        $xml_service->appendChild($xml_service_siren);
        $xml_service->appendChild($xml_service_complement);

        $api->appendChild($apiUrl);
        $api->appendChild($apiEntrepriseRecipient);
        $api->appendChild($apiEntrepriseToken);
        $xml_agent->appendChild($xml_agent_id);
        $xml_agent->appendChild($xml_agent_identifiant);
        $xml_agent->appendChild($xml_agent_plateforme);
        $xml_agent->appendChild($api);
        $xml_agent->appendChild($xml_agent_acronymeOrganisme);
        $xml_agent->appendChild($xml_organisme);
        $xml_agent->appendChild($xml_agent_nom);
        $xml_agent->appendChild($xml_agent_prenom);
        $xml_agent->appendChild($xml_service);
        $xml_agent->appendChild($xml_agent_email);
        $xml_agent->appendChild($xml_agent_telephone);
        $xml_agent->appendChild($xml_agent_fax);
        $xml_agent->appendChild($xml_agent_nomCourantAcheteurPublic);

        $xml_envoi->appendChild($xml_agent);

        $xml_mpe->appendChild($xml_envoi);

        $xml->appendChild($xml_mpe);

        return $xml->saveXML();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function pingForMonitoring()
    {
        return $this->request(
            'POST',
            $this->getSourcingBaseUrl() . '/api-agent/agents',
            [
                'headers' => [
                    'Content-Type' => 'application/xml',
                    'Authorization' => 'Bearer ' . $this->getToken(),
                ],
                'body' => '',
            ]
        )->getStatusCode();
    }

    private function formatLieuxExecution(string $lieuExecution)
    {
        $lieuxExecution = preg_replace("/\([^)]+\)/", '', $lieuExecution);
        $lieuxExecution = str_replace([', ', '-'], [',', ' '], trim($lieuxExecution));
        $lieuxExecution = strtoupper($this->stripAccents($lieuxExecution));

        return explode(',', $lieuxExecution);
    }

    private function stripAccents($str)
    {
        return strtr(
            utf8_decode($str),
            utf8_decode(
                'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝßàáâãäåæçèéêëìíîïñòóôõöøùúûüýÿĀāĂăĄąĆćĈĉĊċČčĎďĐđĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħĨĩĪīĬĭĮįİıĲĳĴĵĶķĹĺĻļĽľĿŀŁłŃńŅņŇňŉŌōŎŏŐőŒœŔŕŖŗŘřŚśŜŝŞşŠšŢţŤťŦŧŨũŪūŬŭŮůŰűŲųŴŵŶŷŸŹźŻżŽžſƒƠơƯưǍǎǏǐǑǒǓǔǕǖǗǘǙǚǛǜǺǻǼǽǾǿ'
            ),
            'AAAAAAAECEEEEIIIIDNOOOOOOUUUUYsaaaaaaaeceeeeiiiinoooooouuuuyyAaAaAaCcCcCcCcDdDdEeEeEeEeEeGgGgGgGgHhHhIiIiIiIiIiIJijJjKkLlLlLlLlllNnNnNnnOoOoOoOEoeRrRrRrSsSsSsSsTtTtTtUuUuUuUuUuUuWwYyYZzZzZzsfOoUuAaIiOoUuUuUuUuUuAaAEaeOo'
        );
    }

    private function getToken(): ?string
    {
        return $this->getAuthenticationToken(
            self::OPEN_ID_SOURCING,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_NUKEMA_USERNAME'),
            $this->parameterBag->get('IDENTITE_NUKEMA_PASSWORD')
        )[AbstractService::ACCESS_TOKEN];
    }

    /**
     * @throws TransportExceptionInterface
     * @throws \JsonException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TimeoutExceptionInterface
     */
    protected function getTokenBaseDCE(Agent $agent): string
    {
        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
            ],
            'body' => json_encode([
                'domain' => $_SERVER['HTTP_HOST'],
                'login' => $agent->getLogin(),
                'firstName' => $agent->getNom(),
                'lastName' => $agent->getPrenom(),
                'email' => $agent->getEmail(),
                'organization' => $agent->getAcronymeOrganisme(),
            ], JSON_THROW_ON_ERROR),
        ];

        return $this->request(
            Request::METHOD_POST,
            sprintf(
                '%s/trade/mpe-access?client_id=%s&client_secret=%s',
                $this->parameterBag->get('DOMAINE_NUKEMA'),
                $this->parameterBag->get('UID_PF_MPE'),
                $this->parameterBag->get('SECRET_CLEF_NUKEMA')
            ),
            $options
        )->getContent();
    }

    /**
     * @throws \JsonException
     */
    protected function getBaseDceUrl(Agent $agent, array $param): string
    {
        $token = json_decode($this->getTokenBaseDCE($agent), true);

        return sprintf(
            '%s%s%s&%s',
            $this->parameterBag->get('DOMAINE_NUKEMA'),
            '/#/archived?access_token=',
            $token['access_token'],
            http_build_query($param)
        );
    }
}
