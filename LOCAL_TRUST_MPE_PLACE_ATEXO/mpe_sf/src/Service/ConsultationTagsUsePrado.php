<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Repository\ConsultationRepository;
use App\Repository\ConsultationTagsRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ConsultationTagsUsePrado extends AbstractService
{
    private const URI_CONSULTATION_TAGS = '/consultation-tags';
    public static $token = null;

    public function __construct(
        private HttpClientInterface $httpClient,
        private LoggerInterface $logger,
        private ParameterBagInterface $parameterBag,
        private Security $security,
        private JWTTokenManagerInterface $tokenManager,
        private ConsultationTagsRepository $consultationTagsRepository,
        private ConsultationRepository $consultationRepository
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    protected function createTag(array $data): bool
    {
        try {
            $url = $this->getMpeWsBaseUrl() . self::URI_CONSULTATION_TAGS;
            $options = [
                'headers'   => [
                    'Content-Type'  => self::CONTENT_TYPE_JSON,
                    'Authorization' => 'Bearer ' . $this->getToken(),
                ],
                'json'      => [
                    "tagCode"       => $data['tagCode'],
                    "consultation"  => '/api/v2/consultations/' . $data['consultationId']
                ]
            ];

            $this->request(Request::METHOD_POST, $url, $options)->getContent();
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error(sprintf(
                "L'ajout du typageJO2024 %s à la consultation id: %s à échoué avec le message d'erreur: %s",
                json_encode($data),
                $data['consultationId'],
                $exception->getMessage()
            ));
            return false;
        }

        return true;
    }

    protected function deleteTag(array $data): bool
    {
        try {
            $url = $this->getMpeWsBaseUrl() . self::URI_CONSULTATION_TAGS . '/' . $data['consultationTagId'];
            $options = [
                'headers'   => [
                    'Authorization' => 'Bearer ' . $this->getToken(),
                ]
            ];

            $this->request(Request::METHOD_DELETE, $url, $options)->getContent();
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error(sprintf(
                "La suppression du typageJO2024 %s à la consultation id: %s à échoué avec le message d'erreur: %s",
                json_encode($data),
                $data['consultationId'],
                $exception->getMessage()
            ));
            return false;
        }

        return true;
    }

    private function getToken(): string
    {
        return self::$token ?? $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . 'api/v2';
    }
}
