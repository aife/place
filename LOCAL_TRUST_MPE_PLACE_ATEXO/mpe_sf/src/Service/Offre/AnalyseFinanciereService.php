<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Offre;

use Exception;
use DateTime;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre\AnnexeFinanciere;
use App\Service\AtexoFichierOrganisme;
use App\Service\AtexoService;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\CurrentUser;
use App\Service\DocGen\AnalyseFinanciere;
use App\Service\EspaceDocumentaire\Authorization;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\Exception\ORMException;
use League\Flysystem\FileExistsException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\User\UserInterface;

class AnalyseFinanciereService
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBagInterface,
        private readonly EntityManagerInterface $em,
        private readonly Authorization $perimetreAgent,
        private readonly BlobOrganismeFileService $blobOrganismeFileService,
        private readonly AtexoFichierOrganisme $atexoFichierOrganisme,
        private readonly AtexoService $atexoService,
        private readonly SessionInterface $session,
        private readonly AnalyseFinanciere $docgenAnalyseFinanciere,
        private readonly CurrentUser $user
    ) {
    }

    /**
     * @return mixed|array
     */
    public function getAnnexeFinanciereReponse(Consultation $consultation)
    {
        $statutOffre = [
            $this->parameterBagInterface->get('STATUT_ENV_OUVERTE_EN_LIGNE'),
            $this->parameterBagInterface->get('STATUT_ENV_OUVERTE')
        ];

        $enveloppeFichierRepository = $this->em->getRepository(EnveloppeFichier::class);

        return $enveloppeFichierRepository->getEnveloppeAnnexeFinanciere($consultation, $statutOffre);
    }

    /**
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function getAnnexeFinanciereFile(int $idAgent, int $consultationId, int $blobId): BinaryFileResponse
    {
        $this->checkIfIsAuthorized($idAgent, $consultationId);

        $reponse = $this->blobOrganismeFileService->getFile($blobId, true);

        $fileResponse = new BinaryFileResponse($reponse['realPath']);
        $fileResponse->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $reponse['name']);

        return $fileResponse->deleteFileAfterSend(false);
    }

    /**
     *
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeAnnexeFinanciereManuel(int $idAgent, int $consultationId, int $annexeFinanciereId): void
    {
        $this->checkIfIsAuthorized($idAgent, $consultationId);

        $annexeFinanciere = $this->em->getRepository(AnnexeFinanciere::class)->find($annexeFinanciereId);

        if ($annexeFinanciere) {
            $this->em->remove($annexeFinanciere->getBlob());
            $this->em->remove($annexeFinanciere);
            $this->em->flush();
        }
    }

    /**
     *
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    private function checkIfIsAuthorized(int $idAgent, int $consultationId): void
    {
        $authAgent = $this->perimetreAgent->isAgentAuthorized($idAgent);
        $authConsultation = $this->perimetreAgent->isAuthorized($consultationId);

        if (!$authAgent || !$authConsultation) {
            throw new NotFoundHttpException('Impossible d\'accèder à cette resource !');
        }
    }

    /**
     * @param mixed $data
     *
     *
     * @throws FileExistsException
     * @throws Exception
     */
    public function add($data, Consultation $consultation): AnnexeFinanciere
    {
        $blob = $this->em->getRepository(BloborganismeFile::class)->find(
            $this->addFile($data['importFichier'])
        );

        $this->testAddDataExists($data, $blob, $consultation->isAlloti());

        $dateRemise = null;
        if ($data['dateRemiseDate'] instanceof DateTime && $data['dateRemiseHeure'] instanceof DateTime) {
            $dateRemise = new DateTime(
                $data['dateRemiseDate']->format('Y-m-d')
                . ' '
                . $data['dateRemiseHeure']->format('H:i')
            );
        }

        return
            (new AnnexeFinanciere())
            ->setAgent($this->getUserConnected())
            ->setBlob($blob)
            ->setConsultation($consultation)
            ->setDateRemise($dateRemise)
            ->setEntreprise($data['entreprise'])
            ->setNumeroLot($data['numeroLot'])
            ->setNumeroPli($data['numeroPli'])
        ;
    }

    public function analyzeAndNote(
        int $idAgent,
        Consultation $consultation,
        string $values,
        string $outputFileName,
        string $organisme
    ) {
        $files = [];
        foreach (explode('-', $values) as $idBlob) {
            $files[] = $this->getAnnexeFinanciereFile($idAgent, $consultation->getId(), $idBlob);
        }

        return $this->docgenAnalyseFinanciere->getPieceGeneree($consultation, $files, $outputFileName, $organisme);
    }

    /**
     *
     *
     * @throws FileExistsException
     */
    private function addFile(UploadedFile $file): int
    {
        $this->atexoFichierOrganisme->moveTmpFile(
            $file->getPathName()
        );

        return $this->atexoFichierOrganisme->insertFile(
            $file->getClientOriginalName(),
            $file->getPathName(),
            $this->getUserConnected()->getOrganisme()->getAcronyme()
        );
    }

    private function getUserConnected(): ?UserInterface
    {
        return $this->user->getCurrentUser() ?? null;
    }

    /**
     * @param mixed|array $data
     *
     * @throws FileExistsException
     */
    private function testAddDataExists($data, ?BloborganismeFile $bloborganismeFile, bool $isLot): void
    {
        if (
            !(
                (array_key_exists('entreprise', $data) && $data['entreprise'])
                && (array_key_exists('numeroLot', $data) || !$isLot)
                && array_key_exists('numeroPli', $data)
                && (array_key_exists('importFichier', $data) && $data['importFichier'])
                && array_key_exists('dateRemiseDate', $data)
                && array_key_exists('dateRemiseHeure', $data)
                && ($bloborganismeFile instanceof BloborganismeFile)
            )
        ) {
            throw new NotFoundHttpException('Les données sont incorrect!');
        }
    }
}
