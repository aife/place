<?php

namespace App\Service\Offre;

use App\Message\AskDumePublicationGroupementEntreprise;
use App\Message\DumePublication;
use DateTime;
use App\Entity\CandidatureMps;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use App\Entity\PanierEntreprise;
use App\Entity\TCandidature;
use App\Entity\TDumeNumero;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TMembreGroupementEntreprise;
use App\Entity\TNumeroReponse;
use App\Event\DepotValidationSynchronizeDocumentsEvent;
use App\Event\ResponseEvents;
use App\Listener\ResponseListener;
use App\Security\UidManagement;
use App\Service\AtexoChiffrementOffre;
use App\Service\AtexoConfiguration;
use App\Service\AtexoEnveloppeFichier;
use App\Service\AtexoGroupement;
use App\Service\AtexoHorodatageOffre;
use App\Service\Consultation\ConsultationDepotService;
use App\Service\Consultation\ConsultationSaveDepotService;
use App\Service\Consultation\ConsultationService;
use App\Service\Dume\DumeService;
use App\Service\InscritOperationsTracker;
use App\Service\PlateformeVirtuelle\Context;
use App\Service\Referentiel;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class ValidateDepotOffreService
{
    final public const ENVOI_COMPLET = 1;
    final public const LIBELLE_VALEUR_REFERENTIEL_39 = 'DESCRIPTION39';
    final public const LIBELLE_VALEUR_REFERENTIEL_32 = 'DESCRIPTION32';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ParameterBagInterface $parameter,
        private readonly Context $context,
        private readonly LoggerInterface $depotLogger,
        private readonly RequestStack $request,
        private readonly ConsultationService $consultationService,
        private readonly AtexoHorodatageOffre $horodatage,
        private readonly DumeService $dumeService,
        private readonly ResponseListener $responseListener,
        private readonly ConsultationSaveDepotService $consultationSaveDepotService,
        private readonly AtexoConfiguration $atexoConfig,
        private readonly InscritOperationsTracker $inscritOperationTracker,
        private readonly AtexoEnveloppeFichier $atexoEnveloppeFichier,
        private readonly AtexoChiffrementOffre $atexoChiffrementOffre,
        private readonly AtexoGroupement $atexoGroupement,
        private readonly ConsultationDepotService $consultationDepotService,
        private readonly UidManagement $uidManagement,
        private readonly Referentiel $atexoValeursReferentielles,
        private readonly MessageBusInterface $bus
    ) {
    }

    public function validate(Offre $offre)
    {
        $description = null;
        $errors = [];
        $consultation = $offre->getConsultation();
        $uidResponse = $this->uidManagement->generateUid($consultation->getId());

        if ($offre->getStatutOffres() == $this->parameter->get('STATUT_ENV_BROUILLON')) {
            $this->updateOffre($offre);
            $this->handleEnveloppe($offre, $uidResponse);
            if (!$offre->getEnveloppes()->isEmpty()) {
                $this->saveCandidatureMps($offre, $uidResponse);
                $this->horodatage->demanderHorodaterOffre($offre, $this->request->getCurrentRequest());

                if (!$offre->getNumeroReponse()) {
                    $tNumeroReponse = $this->em->getRepository(TNumeroReponse::class)
                        ->geTNumReponseFromConsultation(
                            $consultation->getId(),
                            $consultation->getOrganisme()
                        );
                    $nbReponses = $tNumeroReponse->getNumeroReponse() + 1;
                    $tNumeroReponse->setNumeroReponse($nbReponses);
                    $this->em->persist($tNumeroReponse);
                    $offre->setNumeroReponse($nbReponses);
                }

                $candidature = $this->closeCandidature($offre);
                $this->dumeCandidature($candidature);
                $this->groupementEntreprise($candidature, $offre, $uidResponse);
                $this->sendEmailAgentInscrit($offre);
                $this->syncDocEntrepriseInscrit($offre);
                $this->addConsultationPanier($offre);

                $description = $this->atexoValeursReferentielles->getLibelleValeurReferentiel(
                    [
                        'id' => $this->getIdValeurReferentielle(),
                        'idReferentiel' => $this->parameter->get('DESCRIPTION_HISTORIQUES_INSCRITS')
                    ]
                );
                $this->trackingOperationsInscrit(
                    $offre,
                    new DateTime(),
                    new DateTime(),
                    $description,
                    '',
                    null,
                    null,
                    false
                );
            }

            if (!$offre->getEnveloppes()->isEmpty()) {
                $this->changeExtensionFile($offre, $uidResponse);
                $this->atexoChiffrementOffre->demanderChiffrementOffre($offre);
            }

            if (empty($errors)) {
                $description = str_replace('[_nom_page_]', "Confirmation dépôt", $description);
                $linkDownload = sprintf(
                    "index.php?page=Agent.DownloadRecapReponse&idOffre=%d&orgAcronyme=%s",
                    $offre->getId(),
                    $consultation->getOrganisme()
                );

                $this->inscritOperationTracker->getInfosValeursReferentielles(self::LIBELLE_VALEUR_REFERENTIEL_39);
                $this->trackingOperationsInscrit(
                    $offre,
                    new DateTime(),
                    new DateTime(),
                    $description,
                    '',
                    $linkDownload
                );
            }
        } else {
            $errorMessage = sprintf(
                "[uid_response = %s]  erreur lors du depot de l'offre. 
                Erreur : %d le statuts de l'offre est invalide => %d",
                $uidResponse,
                $offre->getId(),
                $offre->getStatutOffres()
            );
            throw new Exception($errorMessage, Response::HTTP_CONFLICT);
        }
    }

    private function updateOffre(Offre $offre): Offre
    {
        $offre->setPlateformeVirtuelle($this->context->getCurrentPlateformeVirtuelle());
        $offre->setEnvoiComplet(self::ENVOI_COMPLET);
        $offre->setDateDepot(new DateTime());

        $inscrit = $offre->getInscrit();
        if (!empty($inscrit->getEmail())) {
            $offre->setMailSignataire($inscrit->getEmail());
        }

        if (!$offre->getEnveloppes()->isEmpty()) {
            $statutOffre = ($offre->getConsultation()->getChiffrementOffre() == '1')
                ? $this->parameter->get('STATUT_ENV_EN_ATTENTE_CHIFFREMENT')
                : $this->parameter->get('STATUT_ENV_FERMETURE_EN_ATTENTE');
            $offre->setStatutOffres($statutOffre);
        }

        return $offre;
    }

    private function handleEnveloppe(Offre $offre, string $uidResponse): void
    {
        $enveloppes = $offre->getEnveloppes();

        if ($enveloppes->isEmpty()) {
            $erreur = sprintf(
                "Une erreur est survenu lors du dépot de votre offre : 
                Il n'y a pas d'enveloppes pour l'offre => %d",
                $offre->getId()
            );
            $this->depotLogger->error($erreur);
            $this->errors[] = $erreur;
        } else {
            foreach ($enveloppes as $enveloppe) {
                if ($enveloppe instanceof Enveloppe) {
                    $countFile = $this->em
                        ->getRepository(EnveloppeFichier::class)
                        ->getNombreEnveloppeFichier($enveloppe->getIdEnveloppeElectro());

                    $info = sprintf(
                        "Nombre de fichiers dans l'enveloppe : Offre => %d | Enveloppe => %d | Nombre Fichiers => %d",
                        $offre->getId(),
                        $enveloppe->getIdEnveloppeElectro(),
                        $countFile
                    );

                    $this->depotLogger->info($info);
                    if ($countFile == 0) {
                        $info = sprintf(
                            "Suppression de l'enveloppe vide : Offre => %d | Enveloppe => %d",
                            $offre->getId(),
                            $enveloppe->getIdEnveloppeElectro(),
                        );
                        $this->depotLogger->info($info);
                        $this->em->remove($enveloppe);
                        $offre->removeEnveloppe($enveloppe);
                    }
                    // ELSE : gestion de la partie DossierVolumineux ??
                }
            }

            if ($this->atexoConfig->hasConfigPlateforme('interfaceModuleSub')) {
                $this->consultationService->createEnveloppeDossierFormulaire($offre);
            }

            $this->addEnveloppeFictive($offre, $uidResponse);
        }
    }

    private function addEnveloppeFictive(Offre $offre, string $uidResponse): void
    {
        $consultation = $offre->getConsultation();
        if (
            $consultation->getEnvCandidature() &&
            !$offre->hasEnveloppeCandidature($this->parameter->get('TYPE_ENV_CANDIDATURE'))
        ) {
            $this->consultationService->createEnveloppe(
                $uidResponse,
                $this->parameter->get('TYPE_ENV_CANDIDATURE'),
                0,
                $consultation->getAcronymeOrg(),
                true,
                true,
                $offre
            );
        }
        if (
            $consultation->getEnvOffre() &&
            !$offre->hasEnveloppeOffre($this->parameter->get('TYPE_ENV_OFFRE'))
        ) {
            $lots = [];
            if ($consultation->getAlloti()) {
                $listLotsCandidature = $this->em->getRepository(TListeLotsCandidature::class)
                    ->getListeLotByUserAndConsultationAndWithCandidature(
                        $offre->getInscrit(),
                        $consultation,
                        $this->parameter->get('STATUT_ENV_BROUILLON')
                    );

                foreach ($listLotsCandidature as $value) {
                    $lots[] = $value->getNumLot();
                }
            } else {
                $lots[] = '0';
            }
            foreach ($lots as $lot) {
                $this->consultationService->createEnveloppe(
                    $uidResponse,
                    $this->parameter->get('TYPE_ENV_OFFRE'),
                    $lot,
                    $consultation->getAcronymeOrg(),
                    true,
                    true,
                    $offre
                );
            }
        }
    }

    private function saveCandidatureMps(Offre $offre, string $uidResponse): void
    {
        $consultation = $offre->getConsultation();
        $candidatureMPS = $this->consultationDepotService->getCandidatureMps(
            $uidResponse,
            $consultation->getId(),
            $consultation,
            $offre->getInscrit()
        );

        if ($candidatureMPS instanceof CandidatureMps) {
            $candidatureMPS->setIdOffre($offre->getId());
        }
    }

    private function closeCandidature(Offre $offre): TCandidature
    {
        $consultation = $offre->getConsultation();
        $inscrit = $offre->getInscrit();
        if ($consultation->getAlloti()) {
            $listLotsCandidature = $this->em
                ->getRepository(TListeLotsCandidature::class)
                ->getListeLotByUserAndConsultationAndWithCandidature(
                    $inscrit,
                    $consultation,
                    $this->parameter->get('STATUT_ENV_BROUILLON')
                );

            foreach ($listLotsCandidature as $value) {
                $value->setStatus($this->parameter->get('STATUT_ENV_FERME'));
            }
        }

        $candidature = $this->em
            ->getRepository(TCandidature::class)
            ->findOneBy([
                'organisme' => $consultation->getAcronymeOrg(),
                'consultation' => $consultation->getId(),
                'idInscrit' => $inscrit->getId(),
                'idEntreprise' => $inscrit->getEntrepriseId(),
                'idEtablissement' => $inscrit->getIdEtablissement(),
                'status' => $this->parameter->get('STATUT_ENV_BROUILLON')
            ]);

        $candidature->setStatus($this->parameter->get('STATUT_ENV_FERME'));
        $candidature->setIdOffre($offre);

        return $candidature;
    }

    private function dumeCandidature(TCandidature $candidature): void
    {
        if ($candidature->getIdDumeContexte()) {
            $candidature->getIdDumeContexte()->setStatus(
                $this->parameter->get('STATUT_DUME_CONTEXTE_VALIDE')
            );
            $candidature->setDateDerniereValidationDume(new DateTime());
        }
    }

    private function groupementEntreprise(TCandidature $candidature, Offre $offre, string $uidResponse): void
    {
        $consultation = $offre->getConsultation();
        if ($this->atexoConfig->hasConfigPlateforme("groupement")) {
            $this->atexoGroupement->saveGroupementEntreprise(
                $this->request->getCurrentRequest(),
                $offre->getId(),
                $consultation,
                $uidResponse
            );
        }

        if (
            $candidature->getIdDumeContexte() &&
            $candidature->getTypeCandidature() == $this->parameter->get('TYPE_CANDIDATUE_DUME') &&
            $candidature->getTypeCandidatureDume() == $this->parameter->get('TYPE_CANDIDATUE_DUME_ONLINE')
        ) {
            $tGroupementEntreprise = $this->em
                ->getRepository(TGroupementEntreprise::class)
                ->findOneBy([
                    'candidature' => $candidature->getId()
                ]);
            $validationDumeEntreprise = true;
            if ($tGroupementEntreprise) {
                $mandataire = $this->em
                    ->getRepository(TMembreGroupementEntreprise::class)
                    ->findOneBy([
                        'idGroupementEntreprise' => $tGroupementEntreprise->getIdGroupementEntreprise(),
                        'idRoleJuridique' => $this->parameter->get('ID_ROLE_JURIDIQUE_MANDATAIRE')
                    ]);

                if ($mandataire instanceof TMembreGroupementEntreprise) {
                    $validationDumeEntreprise = false;
                    $numeroSnMandataire = $this->em
                        ->getRepository(TDumeNumero::class)
                        ->findOneBy([
                            'idDumeContexte' => $candidature->getIdDumeContexte()->getId()
                        ]);

                    $mandataire->setNumeroSn($numeroSnMandataire->getNumeroDumeNational());

                    $this->bus->dispatch(new AskDumePublicationGroupementEntreprise(
                        $tGroupementEntreprise->getIdGroupementEntreprise()
                    ));
                }
            }

            if ($validationDumeEntreprise) {
                $this->logger->info('Dispatch message DumePublication');
                $this->bus->dispatch(new DumePublication(
                    'entreprise',
                    false,
                    $candidature->getIdDumeContexte()->getContexteLtDumeId()
                ));
            }
        }
    }

    private function sendEmailAgentInscrit(Offre $offre): void
    {
        $idCandidatureMps = 0;


        $this->consultationSaveDepotService->addJobEmailAR(
            $offre->getConsultation()->getId(),
            $offre->getId(),
            $offre->getInscrit()->getId(),
            $idCandidatureMps
        );
    }

    private function syncDocEntrepriseInscrit(Offre $offre): void
    {
        if ($this->parameter->get('synchro_document_entreprise')) {
            $synchronizeDocumentsevent = new DepotValidationSynchronizeDocumentsEvent(
                $offre->getConsultation(),
                $offre->getEntrepriseId(),
                $offre->getIdEtablissement(),
                $offre->getId()
            );

            $eventDispatcher = new EventDispatcher();
            $eventDispatcher->addListener(
                ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS,
                [
                    $this->responseListener,
                    ResponseEvents::CALLBACK[ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS]
                ]
            );

            $eventDispatcher->dispatch(
                $synchronizeDocumentsevent,
                ResponseEvents::ON_DEPOTVALIDATION_SYNCHRONIZE_DOCUMENTS
            );
        }
    }

    private function addConsultationPanier(Offre $offre): void
    {
        if ($this->atexoConfig->hasConfigPlateforme('panierEntreprise')) {
            $panierRepository = $this->em
                ->getRepository(PanierEntreprise::class);
            $consultation = $offre->getConsultation();
            $inscritId = $offre->getInscrit()->getId();

            $panierEntreprise = $panierRepository
                ->getPanierEntreprise(
                    $consultation->getOrganisme(),
                    $consultation->getId(),
                    $offre->getEntrepriseId(),
                    $inscritId
                );

            if (!$panierEntreprise) {
                $panierRepository->addConsultationToPanierEntreprise(
                    $consultation->getOrganisme(),
                    $consultation->getId(),
                    $offre->getEntrepriseId(),
                    $inscritId
                );
            }
        }
    }

    private function trackingOperationsInscrit(
        Offre $offre,
        $dateStartAction,
        $dateEndAction,
        $description,
        $details,
        $linkDownload,
        $timestampDebutAction = null,
        $isFlush = true
    ): void {
        $this->inscritOperationTracker->tracerOperationsInscrit(
            $dateStartAction,
            $dateEndAction,
            $this->getIdValeurReferentielle(),
            $description,
            $offre->getConsultation(),
            $details,
            false,
            $linkDownload,
            1,
            null,
            $offre->getId(),
            $timestampDebutAction,
            $isFlush
        );
    }

    private function changeExtensionFile(Offre $offre, string $uidResponse): void
    {
        try {
            $extension = $this->parameter->get('EXTENSION_ATTENTE_CHIFFREMENT');
            if ($offre->getConsultation()->getChiffrementOffre() === '0') {
                $extension = $this->parameter->get('EXTENSION_NON_CHIFFRE');
            }

            $this->atexoEnveloppeFichier->changeExtensionFichiersOffre(
                $offre->getId(),
                $offre->getOrganisme(),
                $extension
            );
        } catch (Exception $e) {
            $erreur = "[uid_response = $uidResponse] "
                . "Une erreur est survenu lors de modification des extensions des fichiers de l'offre :  "
                . $offre->getId() . "  pour la consultation=> " . $offre->getConsultation()->getReference()
                . " Erreur => " . $e->getMessage() . "   " . $e->getTraceAsString();

            $this->depotLogger->error($erreur);
        }
    }

    public function getIdValeurReferentielle(): int
    {
        return $this->atexoValeursReferentielles->getIdValeurReferentiel(
            [
                'libelle2' => self::LIBELLE_VALEUR_REFERENTIEL_32,
                'idReferentiel' => $this->parameter->get('DESCRIPTION_HISTORIQUES_INSCRITS')
            ]
        );
    }
}
