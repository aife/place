<?php

namespace App\Service;

use Exception;
use DateTime;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Exception\ApiProblemAlreadyExistException;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Service\Entreprise\EntrepriseVerificationService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Classe contenant des fonctions utiles.
 *
 * @author     Oumar KONATE <oumar.konate@atexo.com>
 * @copyright  Atexo 2015
 *
 * @version    0
 *
 * @since      0
 */
class AtexoEtablissement
{
    /**
     * @var
     */
    private ?ContainerInterface $container = null;
    protected $config;
    protected $util;

    /**
     * Set container.
     *
     * @param $container : service container
     */
    public function setContainer(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Get container.
     *
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getUtil()
    {
        return $this->util;
    }

    /**
     * @param mixed $util
     */
    public function setUtil($util)
    {
        $this->util = $util;
    }

    /**
     * @return mixed
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @param mixed $em
     */
    public function setEm($em)
    {
        $this->em = $em;
    }

    /**
     * Constructeur de la classe.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version   0
     *
     * @since     0
     *
     * @copyright Atexo 2015
     */
    public function __construct(
        ContainerInterface $container,
        protected EntityManagerInterface $em,
        AtexoConfiguration $config,
        AtexoUtil $util,
        private readonly ApiGouvEntrepriseSynchro $apiGouvEntrepriseSynchro,
        private readonly ParameterBagInterface $parameterBag,
    ) {
        $this->setContainer($container);
        $this->setConfig($config);
        $this->setUtil($util);
    }

    /**
     * Permet de recuperer les informations de l'adresse etablissement
     * la régle respecter est la suivante :
     *   La ligne où cinq chiffres sont détectés au début est enregistrée comme :
     *   Les cinq chiffres : code postal
     *   La suite : ville
     *   Les autres lignes sont enregistrées dans les champs Adresse.
     *
     * @param     array $adresseInfo info adresse
     *
     * @return    array un tableau contenant les informations d'adresse sous format cp,ville,adresse
     *
     * @author    LEZ <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getInfoAdresseEtablissement($adresseInfo)
    {
        $infoAdresse = [];
        if (is_array($adresseInfo)) {
            $infoAdresse['cp'] = $this->getConfig()->toPfEncoding($adresseInfo['code_postal']);
            $infoAdresse['ville'] = $this->getConfig()->toPfEncoding($adresseInfo['libelle_commune']);
            $infoAdresse['adresse2'] = $this->getConfig()->toPfEncoding($adresseInfo['complement_adresse']);
            $infoAdresse['adresse'] = $this->getConfig()->toPfEncoding($adresseInfo['numero_voie']) . ' '
                . $this->getConfig()->toPfEncoding($adresseInfo['type_voie']) . ' '
                . $this->getConfig()->toPfEncoding($adresseInfo['libelle_voie']);
        }

        return $infoAdresse;
    }

    /**
     * Permet de remplir l'objet etablissement avec les donnees de l'API entreprise.
     *
     * @param array $infoEtablissement : données API entreprise
     *
     * @return Etablissement : objet contenant les informations
     */
    public function loadFromApiGouvEntreprise(array $infoEtablissement): Etablissement
    {
        $etablissement = new Etablissement();

        $etablissement->setEstSiege($infoEtablissement['siege_social'] ? '1' : '0');
        $etablissement->setSaisieManuelle('0');
        $etablissement->setSiret($infoEtablissement['siret']);
        $etablissement->setCodeEtablissement(substr($infoEtablissement['siret'], 9, 5));

        $infoAdresse = self::getInfoAdresseEtablissement($infoEtablissement['adresse']);
        if (is_array($infoAdresse)) {
            $etablissement->setAdresse($infoAdresse['adresse']);
            $etablissement->setAdresse2($infoAdresse['adresse2']);
            $etablissement->setCodePostal($infoAdresse['cp']);
            $etablissement->setVille($infoAdresse['ville']);
        }

        $status = '0';
        if (
            !empty($infoEtablissement['etat_administratif'])
            && EntrepriseVerificationService::ETAT_ADMINISTRATIF_ACTIF === $infoEtablissement['etat_administratif']
        ) {
            $status = '1';
        }
        $etablissement->setStatutActif($status);
        $etablissement->setEtatAdministratif($infoEtablissement['etat_administratif']);

        $activitePrincipale = $infoEtablissement['activite_principale'] ?? null;
        $codeApe = $this->getConfig()->toPfEncoding(
            str_replace('.', '', $activitePrincipale['code']) ?? ''
        );
        $libelleNaf = $this->getConfig()->toPfEncoding($activitePrincipale['libelle'] ?? '');

        $etablissement->setCodeape($codeApe);
        $etablissement->setLibelleApe($libelleNaf);
        $etablissement->setPays($this->getParameter('DENOMINATION1_GEON2_FRANCE'));

        return $etablissement;
    }

    /**
     * LEZ TODO.
     */
    public function enregistrerEtablissementApiGouvEntreprise($etablissementVo)
    {
        $loggerWs = null;
        if ($etablissementVo instanceof Etablissement) {
            try {
                $loggerWs = $this->get('mpe.logger');
                $loggerWs->info("La recuperation de l'etablissement dont le code est =>"
                    . $etablissementVo->getCodeEtablissement()
                    . ' et id entreprise =>' . $etablissementVo->getIdEntreprise());
                $repository = $this->em->getRepository(Etablissement::class);
                $etablissement = $repository->getEtablissementByCodeAndIdEntreprise(
                    $etablissementVo->getCodeEtablissement(),
                    $etablissementVo->getIdEntreprise()
                );

                if (!($etablissement instanceof Etablissement)) {
                    $loggerWs->info("L'Etablisssement siege n'existe pas deja en BD");
                    $etablissement = new Etablissement($loggerWs);
                } else {
                    $loggerWs->info("L'Etablisssement siege existe deja en BD");
                }
                $etablissement = self::fillInfo($etablissement, $etablissementVo);
                if ($etablissement instanceof Etablissement) {
                    $repository->updateEtablissement($etablissement);

                    return $etablissement;
                }

                return false;
            } catch (Exception $e) {
                echo "Erreur lors de l'enregistrement de l'etablissement : etablissementVo = "
                    . var_export($etablissementVo, true) . "\n\nErreur : "
                    . $e->getMessage() . " \n\nTrace : " . $e->getTraceAsString();
                $loggerWs->error("Erreur lors de l'enregistrement de l'etablissement : etablissementVo = "
                    . var_export($etablissementVo, true) . "\n\nErreur : "
                    . $e->getMessage() . " \n\nTrace : " . $e->getTraceAsString());
            }
        }
    }

    /**
     * Permet de recuperer un etablissement selon le siret.
     *
     * @param     $to
     * @param     $from
     *
     * @return    Etablissement
     *
     * @author    loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.9.0
     *
     * @copyright Atexo 2015
     */
    public function fillInfo($to, $from)
    {
        if ($to instanceof Etablissement && $from instanceof Etablissement) {
            $to->setCodeEtablissement($from->getCodeEtablissement());
            $to->setEstSiege($from->getEstSiege());
            $to->setAdresse($from->getAdresse());
            $to->setAdresse2($from->getAdresse2());
            $to->setCodePostal($from->getCodePostal());
            $to->setVille($from->getVille());
            $to->setPays($from->getPays());
            $to->setSaisieManuelle($from->getSaisieManuelle());
            $to->setInscritAnnuaireDefense($from->getInscritAnnuaireDefense());
            if ($from->getEntreprise()) {
                $to->setEntreprise($from->getEntreprise());
            }
            $to->setStatutActif($from->getStatutActif());
            $to->setIdEtablissement($from->getIdEtablissement());
        }

        return $to;
    }

    /**
     * Permet de synchroniser un etablissement identifie par son siret avec Api Gouv Entreprise.
     *
     * @param     string        $siret         : siret de l'etablissement sur 14 caracteres
     * @param     Etablissement $etablissement : objet etablissement
     *
     * @return    Etablissement
     *
     * @author    loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.9.0
     *
     * @copyright Atexo 2015
     */
    public function synchroEtablissementAvecApiGouvEntreprise($siret, $etablissement = null)
    {
        $loggerWs = $this->get('mpe.logger');
        $loggerWs->info("Debut de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");
        try {
            $idEtablissement = null;
            if ($etablissement instanceof Etablissement && !empty($etablissement->getIdEtablissement())) {
                $idEtablissement = $etablissement->getIdEtablissement();
                $loggerWs->info("L'etablissement dont le siret = $siret existe en base de donnees");
            } else {
                $loggerWs->info("L'etablissement dont le siret = $siret n'existe pas en base de donnees");
            }

            //Recuperation du siege de l'entreprise associe
            $loggerWs->info("Recuperation du siege de l'entreprise associe a l'etablissement dont le siret = $siret");
            $repositoryEntreprise = $this->em->getRepository(Entreprise::class);

            $entreprise = $repositoryEntreprise->getEntrepriseBySiren(substr($siret, 0, 9));
            $siege = null;
            if ($entreprise instanceof Entreprise) {
                $repositoryEtablissement = $this->em->getRepository(Etablissement::class);
                $siege = $repositoryEtablissement->getEtablissementSiegeByIdEntreprise($entreprise->getId());
                if ($siege instanceof Etablissement) {
                    $loggerWs->info('Etablissement siege trouve');
                }
                $loggerWs->info("La récupération de l'etablissement depuis Api Gouv Entreprise ");
                //Synchronisation de l'etablissement identifie par son siret
                $etablissement = self::recupererEtablissementApiGouvEntreprise($siret, $idEtablissement);
                if ($etablissement instanceof Etablissement) {
                    //$etablissement->setDateModification(date('Y-m-d H:i:s'));
                    $etablissement->setSaisieManuelle('0');
                    $etablissement->setIdEntreprise($entreprise->getId());
                    $etablissement->setEntreprise($entreprise);
                    $loggerWs->info("Debut de l'enregistrement de l'etablissement");
                    $repositoryEtablissement->updateEtablissement($etablissement);
                    $loggerWs->info("Fin de l'enregistrement de l'etablissement");
                    if (
                        $etablissement->getEstSiege() && $siege instanceof Etablissement
                        && $siege->getCodeEtablissement() != $etablissement->getCodeEtablissement()
                    ) {
                        $siege->setEstSiege('0');
                        $loggerWs->info("La modification de l'etablissement siege");
                        $repositoryEtablissement->updateEtablissement($siege);
                    }
                    $loggerWs->info("Fin de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");

                    return $etablissement;
                } else {
                    $message = "Une erreur est survenue lors de la synchronisation ";
                    $message .= "l'etablissement avec Api Gouv Entreprise : siret = $siret";
                    $loggerWs->error($message);
                }
            } else {
                $message = "L'entreprise rattache a l'etablissement ayant le siret ";
                $message .= "= $siret n'est pas enregistre en base de donnees";
                $loggerWs->error($message);
            }
        } catch (Exception $e) {
            $message = "Erreur lors de la synchronisation avec Api Gouv Entreprise  : siret = $siret \n\n";
            $message .= "Erreur: " . $e->getMessage() . "\n\nTrace: " . $e->getTraceAsString();
            $loggerWs->error($message);
        }
        $loggerWs->info("Fin de la synchronisation de l'etablissement dont le siret = $siret avec ApiGouvEntreprise");
    }

    /**
     * Permet de recuperer un etablissement selon le siret.
     *
     * @param int $siret le siret d'etablissement
     *
     * @return    mixed Atexo_Entreprise_EtablissementVo l'etablissement s'il existe sinon boolean false
     *
     * @author    loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererEtablissementApiGouvEntreprise($siret, $idObjet = null)
    {
        $token = $this->parameterBag->get('API_ENTREPRISE_TOKEN');
        $paramsWs = [
            'token' => $token,
            'api_gouv_url_entreprise' => $this->getContainer()->getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
            'api_gouv_url_etablissement' => $this->getContainer()->getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $this->apiGouvEntrepriseSynchro->setParameters($paramsWs);
        $resultat = $this->apiGouvEntrepriseSynchro->getEtablissementBySiret($siret);
        $loggerWs = $this->get('mpe.logger');
        if (is_array($resultat) && $this->apiGouvEntrepriseSynchro->isResponseSuccess($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'etablissement");
            $historiqueSynchro = $this->get(HistoriqueSynchro::class);
            $historiqueSynchro->enregistrerHistroique('ETABLISSEMENT', $siret, $jeton, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'etablissement");
            $etablissements = $resultat[1];
            if ($etablissements instanceof Etablissement) {
                return $etablissements;
            }
        }

        return false;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->getContainer()->getParameter($name);
    }

    /**
     * Gets a container service by its id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    protected function get($id)
    {
        return $this->getContainer()->get($id);
    }

    /**
     * @param  $arrayInfoEtablissement
     *
     * @return Etablissement
     */
    public function serializeEtablissement($arrayInfoEtablissement)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $etablissement = $serializer->denormalize($arrayInfoEtablissement, Etablissement::class);

            if (!empty($arrayInfoEtablissement['id'])) {
                $etablissement->setIdEtablissement($arrayInfoEtablissement['id']);
            }
            if (!empty($arrayInfoEtablissement['siege'])) {
                $etablissement->setEstSiege($arrayInfoEtablissement['siege']);
            }

            return $etablissement;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param  bool  $formUpdate
     *
     * @return array
     */
    public function validateInfo(array $arrayInfoEtablissement, $formUpdate = false)
    {
        $arrayErreur = [];
        $etablissement = $this->serializeEtablissement($arrayInfoEtablissement);

        $siret = $etablissement->getSiret();
        if (!empty($siret) && !$this->getUtil()->isValidSiret($siret)) {
            $arrayErreur[] = 'Siret non valide';
        }

        if ($formUpdate) {
            $this->etablissementExist($etablissement, $arrayErreur);
        } else {
            if (!empty($etablissement->getIdEntreprise())) {
                $entreprise = $this->em->getRepository(Entreprise::class)->find($etablissement->getIdEntreprise());
            }
            if (!$entreprise instanceof Entreprise) {
                $arrayErreur[] = "Etablissement n'est attaché à aucune entreprise";
                if (!empty($etablissement->getIdEntreprise())) {
                    $arrayErreur[] = "il n'y a aucune entreprise avec id " . $etablissement->getIdEntreprise();
                }
            } else {
                if (!empty($siret)) {
                    $etablissement = $this->em->getRepository(Etablissement::class)
                        ->findOneBy(
                            ['codeEtablissement' => substr($siret, 9, 5),
                                'idEntreprise' => $entreprise->getId(), ]
                        );
                    if ($etablissement) {
                        $arrayErreur[] =  'un etablissement avec le siret ' . $siret . ' existe déjà';
                    }
                }
            }
        }

        return $arrayErreur;
    }

    /**
     * @param Entreprise $enttreprise
     * @param $arrayErreur
     */
    public function etablissementExist(Etablissement $etablissement, &$arrayErreur)
    {
        $etablissementRepo = $this->em->getRepository(Etablissement::class);
        $id = $etablissement->getIdEtablissement();
        $idExterne = $etablissement->getIdExterne();

        if (empty($id) && empty($idExterne)) {
            $arrayErreur[] = 'id vide';
        } elseif (
            !empty($id)
            && !$etablissementRepo->find($id)
            && !empty($idExterne)
            && !$etablissementRepo->findByIdExterne($idExterne)
        ) {
            $arrayErreur[] = "Aucun etablissement avec l'id " . $id;
        }
    }

    /**
     * @param  array $node
     *
     * @return Etablissement
     */
    public function createEntity(array $arrayInfoEtablissement)
    {
        $etablissement = $this->serializeEtablissement($arrayInfoEtablissement);

        return $this->createOrUpdateEntity($etablissement, 'create');
    }

    /**
     * @param  array $node
     *
     * @return Etablissement
     */
    public function updateEntity(array $arrayInfoEtablissement)
    {
        $etablissement = $this->serializeEtablissement($arrayInfoEtablissement);

        return $this->createOrUpdateEntity($etablissement, 'update');
    }

    public function createOrUpdateEntity(Etablissement $node, $mode)
    {
        try {
            $etablissement = null;
            $entreprise = null;

            //Look for existing Entreprise
            if (!empty($node->getIdEntreprise())) {
                $entreprise = $this->em->getRepository(Entreprise::class)->find($node->getIdEntreprise());
            }
            if (!$entreprise instanceof Entreprise) {
                $message = "Etablissement n'est attaché à aucune entreprise";
                if (!empty($node->getIdEntreprise())) {
                    $message = "il n'y a aucune entreprise avec id " . $node->getIdEntreprise();
                }
                throw new ApiProblemInvalidArgumentException($message);
            }

            // Look for existing Etablissement
            if (!empty($node->getIdExterne())) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->findOneByIdExterne($node->getIdExterne());
            } elseif (!empty($node->getIdEtablissement())) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->find($node->getIdEtablissement());
            } elseif (!empty($entreprise->getSiren())) {
                $etablissement = $this->em->getRepository(Etablissement::class)
                    ->findOneBy(
                        ['codeEtablissement' => substr($node->getSiret(), 9, 5),
                        'idEntreprise' => $entreprise->getId(), ]
                    );
                if ($etablissement) {
                    throw new ApiProblemAlreadyExistException(
                        'un etablissement avec le siret ' . $node->getSiret() . ' existe déjà'
                    );
                }
            }

            if (!$etablissement instanceof Etablissement) {
                $etablissement = new Etablissement();
                $etablissement->setIdEtablissement($node->getIdEtablissement());
            }

            $codeEtablissement = null;
            if (!empty($node->getSiret())) {
                $codeEtablissement = substr($node->getSiret(), 9, 5);
            } elseif (!empty($entreprise->getSirenetranger())) {
                $codeEtablissement = $node->getCodeEtablissement();
            }

            $etablissement->setIdExterne($node->getIdExterne());
            $etablissement->setIdEtablissement($etablissement->getIdEtablissement());
            $etablissement->setIdEntreprise($node->getIdEntreprise());
            $etablissement->setSiret($node->getSiret());
            if (is_array($node->getAdresse())) {
                $etablissement->setCodePostal($node->getAdresse()['codePostal']);
                $etablissement->setVille($node->getAdresse()['ville']);
                $etablissement->setPays($node->getAdresse()['pays']);
                $etablissement->setAdresse($node->getAdresse()['rue']);
            } else {
                $etablissement->setCodePostal($node->getCodePostal() ?? '');
                $etablissement->setVille($node->getVille() ?? '');
                $etablissement->setPays($node->getPays() ?? '');
                $etablissement->setAdresse($node->getAdresse() ?? '');
            }
            $etablissement->setAdresse2($node->getAdresse2());
            if (null !== $codeEtablissement) {
                $etablissement->setCodeEtablissement($codeEtablissement);
            }
            $etablissement->setEstSiege($node->getEstSiege());
            $etablissement->setEntreprise($entreprise);
            $etablissement->setEtatAdministratif($node->getEtatAdministratif());

            // set DateCreation
            if ('create' === $mode) {
                $etablissement->setDateCreation(new DateTime());
            }

            $this->em->beginTransaction();
            $this->em->persist($etablissement);
            $this->em->flush();
            $this->em->commit();

            return $etablissement;
        } catch (ApiProblemException $e) {
            throw $e;
        } catch (Exception $e) {
            $this->em->rollback();
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    /**
     * @param  $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode(Etablissement $etablissement, Serializer $serializer, $mode)
    {
        $etablissementNormalize = $serializer->normalize($etablissement, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($etablissementNormalize, $mode);

        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', 'etablissement>', $content);
        }

        return $content;
    }
}
