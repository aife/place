<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\HistoriquePurge;
use Symfony\Component\Console\Style\SymfonyStyle;

class HistoriquePurgeService
{
    public function displayLogList(SymfonyStyle $io, array $headers, array $logInfoCommand): void
    {
        $body[] = implode(', ', $headers);

        foreach ($logInfoCommand as $subarray) {
            foreach ($subarray as $data) {
                $body[] = implode(', ', array_values($data));
            }
        }

        $io->listing($body);
    }

    public function notifyResultsInConsole(
        SymfonyStyle $io,
        array $logInfoCommand,
        int $nbFiles,
        bool $dryRun,
        string $textInit
    ): void {
        if (is_countable($logInfoCommand) && empty($logInfoCommand)) {
            $io->warning('Aucun fichier à supprimer');
        } elseif (
            isset($logInfoCommand[HistoriquePurge::STATE_DELETED])
            && count($logInfoCommand[HistoriquePurge::STATE_DELETED]) === $nbFiles
        ) {
            $io->success(sprintf('Tous les fichiers (%s) ont été supprimé avec succès', $nbFiles));
        } else {
            $errorMessages = [
                sprintf("La command n'a pas pu purger tous les $textInit séléctionnés (%s): ", $nbFiles)
            ];

            foreach ($logInfoCommand as $key => $value) {
                $messageByState = match ($key) {
                    HistoriquePurge::STATE_INITIAL => "sont restés à leur état initial",
                    HistoriquePurge::STATE_DELETED => "ont été supprimés avec succès",
                    HistoriquePurge::STATE_ERROR_FILE_NOT_FOUND =>
                    $dryRun
                        ? "ne peuvent pas être supprimé car le(s) fichier(s) n'existent pas sur le serveur"
                        : "n'ont pas pu être supprimé car le(s) fichier(s) n'existent pas sur le serveur",
                    HistoriquePurge::PURGE_STATE_ERROR =>
                    $dryRun
                        ? "ne peuvent pas être supprimé à cause d'une erreur"
                        : "n'ont pas pu être supprimé à cause d'une erreur",
                    HistoriquePurge::STATE_READY_TO_DELETE => "peuvent être supprimé",
                };

                $errorMessages[] = sprintf("- %s %s %s", count($value), $textInit, $messageByState);
            }

            if ($dryRun) {
                $io->warning($errorMessages);
            } else {
                $io->error($errorMessages);
            }
        }
    }
}
