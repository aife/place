<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use stdClass;
class Paginator
{
    public function getPagination(?stdClass $apiView, string $uri): array
    {
        $paginator = [];
        $pagePosition = strpos($uri, '&page=');
        if (false !== $pagePosition) {
            $uri = substr($uri, 0, $pagePosition);
        }
        $paginator['current'] =  str_contains((string) $apiView->{'@id'}, 'page=') ?
            str_replace('page=', '', strrchr($apiView->{'@id'}, 'page=')) : 1;
        $paginator['first'] = str_replace('page=', '', strrchr($apiView->{'hydra:first'}, 'page='));
        $paginator['last'] = $apiView->{'hydra:last'} ?
            str_replace('page=', '', strrchr($apiView->{'hydra:last'}, 'page=')) : 1;
        if ($apiView->{'hydra:next'}) {
            $paginator['next'] = $uri . '&' . strrchr($apiView->{'hydra:next'}, 'page=');
        }
        if ($apiView->{'hydra:previous'}) {
            $paginator['previous'] = $uri . '&' . strrchr($apiView->{'hydra:previous'}, 'page=');
        }
        $paginator['goto'] = $uri . '&' . 'page=';
        $paginator['itemsPerPage10'] = strpos($uri, '&itemsPerPage=')
            ? substr($uri, 0, strpos($uri, '&itemsPerPage=')) : $uri;
        $paginator['itemsPerPage20'] = strpos($uri, '&itemsPerPage=') ? $uri : $uri . '&' . 'itemsPerPage=20';
        $paginator['itemsPerPage20isActive'] = (bool)strpos($uri, '&itemsPerPage=');

        return $paginator;
    }
}
