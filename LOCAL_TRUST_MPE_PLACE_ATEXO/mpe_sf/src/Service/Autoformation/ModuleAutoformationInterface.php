<?php

namespace App\Service\Autoformation;

interface ModuleAutoformationInterface
{
    public const PAGE_AGENT = 'agent';
    public const PAGE_ENTREPRISE = 'entreprise';
    public const LOCALE_FR = 'fr';
    public const LOCALE_EN = 'en';
    public const RESPONSE = ['success' => true];
    public const MSG_PAGE_NOT_FOUND = 'La page n\'exixte pas';
    public const NAME_FR = 'nameFr';
    public const NAME_EN = 'nameEn';
    public const DESCRIPTION_FR = 'descriptionFr';
    public const DESCRIPTION_EN = 'descriptionEn';


    public function getBody(string $page, bool $showButtons): string;
    public function pageExists(string $page): bool;
}