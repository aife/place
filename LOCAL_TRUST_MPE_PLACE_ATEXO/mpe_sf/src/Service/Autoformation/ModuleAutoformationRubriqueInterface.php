<?php

namespace App\Service\Autoformation;

use App\Entity\Autoformation\Rubrique;

interface ModuleAutoformationRubriqueInterface
{
    public function create(array $data): array;
    public function update(Rubrique $rubrique, array  $data): array;
}