<?php

namespace App\Service\Autoformation;

use App\Entity\Autoformation\Rubrique;
use Symfony\Component\HttpFoundation\Request;

interface ModuleAutoformationModuleInterface
{
    public function create(Rubrique $rubrique, array $data): array;
}