<?php

namespace App\Service\Autoformation;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Exception;
use App\Entity\Autoformation\LangueModuleAutoformation;
use App\Entity\Autoformation\ModuleAutoformation;
use App\Entity\Autoformation\Rubrique;
use App\Entity\Langue;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class ModuleAutoformationModule implements ModuleAutoformationInterface, ModuleAutoformationModuleInterface
{
    private ?int $idModule = null;

    /**
     * @param Environment $twig
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private readonly Environment $twig, private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @param bool $showButtons
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getBody(string $page, $showButtons = true): string
    {
        if (!$this->pageExists($page)) {
            throw new NotFoundHttpException(self::MSG_PAGE_NOT_FOUND);
        }

        $context = [];

        if($this->idModule) {
            $context = $this->getContext($this->idModule);
        }

        $template = 'module_autoformation/module/_content.html.twig';

        return $this->twig->render($template, [
            'context' => $context,
            'page' => $page,
            'rubriques' => $this->getRubriquesToArray($page)
        ]);
    }

    public function pageExists(string $page): bool
    {
        return (self::PAGE_AGENT === $page) || (self::PAGE_ENTREPRISE === $page);
    }

    /**
     * @throws Exception
     */
    public function create(Rubrique $rubrique, array $data): array
    {
        $module = new ModuleAutoformation();
        $module = $this->setData($module, $rubrique, $data);

        $this->entityManager->persist($module);
        $this->entityManager->flush();

        return self::RESPONSE;
    }

    public function update(ModuleAutoformation $module, array $data): array
    {
        $idRubrique = $data['idRubrique'] ;

        $rubrique = $idRubrique ?
            $this->entityManager->getRepository(Rubrique::class)->find($idRubrique) : null;

        if (!$rubrique instanceof Rubrique) {
            throw new Exception('La rubrique avec l\'id ' . $idRubrique . ' n\'existe pas');
        }

        $this->setData($module, $rubrique, $data);

        $this->entityManager->flush();

        return self::RESPONSE;
    }

    public function setIdModule(int $idModule): void
    {
        $this->idModule = $idModule;
    }

    /**
     * @param ModuleAutoformation $module
     * @return ModuleAutoformation
     * @throws Exception
     */
    private function setData(ModuleAutoformation $module, Rubrique $rubrique, array $data): ModuleAutoformation
    {
        try {
            $module->setRubrique($rubrique);

            if ($module->getId()) {
                foreach ($module->getLangueModuleAutoformations() as $langueModule) {
                    $updateLangueModule = $this->setLangueModule($langueModule, $data[self::NAME_FR], $data[self::DESCRIPTION_FR]);
                    $module->removeLangueModuleAutoformation($langueModule);
                    $module->addLangueModuleAutoformation($updateLangueModule);
                }
            } else {
                $langueModule = $this->getLangueModule($data[self::NAME_FR], $data[self::DESCRIPTION_FR], self::LOCALE_FR);
                $module->addLangueModuleAutoformation($langueModule);

                if ($data['page'] === self::PAGE_ENTREPRISE) {
                    $langueModule = $this->getLangueModule($data[self::NAME_EN], $data[self::DESCRIPTION_EN], self::LOCALE_EN);
                    $module->addLangueModuleAutoformation($langueModule);
                }
            }

            if ($data['indexTypeFormat'] == 0) {
                $urlExterne = $data['urlExterne'];
                $module->setUrlExterne($urlExterne);
                $module->setPathVideo($urlExterne);
            }

            $module->setDuree($data['duration']);
            $module->setPosition($data['order']);


        }catch (Exception $exception) {
            throw $exception;
        }

        return $module;
    }

    private function getRubriquesToArray(string $page): array
    {
        $data = [];
        $rubriques =  $this->entityManager->getRepository(Rubrique::class)->findBy([
            'isAgent' => self::PAGE_AGENT === $page
        ]);

        foreach ($rubriques as $rubrique) {
            $data[] = [
                'id' => $rubrique->getId(),
                'name' => $this->getNameFrRubrique($rubrique)
            ];
        }

        return $data;
    }

    private function getNameFrRubrique(Rubrique $rubrique): string
    {
        $name = '';
        foreach ($rubrique->getLangueRubriques() as $langueRubrique) {
            if($langueRubrique->getLangue()->getLangue() === self::LOCALE_FR) {
                $name = $langueRubrique->getNom();
            }
        }

        return $name;
    }

    /**
     * @throws Exception
     */
    private function getLangueModule(string $name, string $description, string $locale): LangueModuleAutoformation
    {
        $langue = $this->entityManager->getRepository(Langue::class)->findOneBy(['langue' => $locale]);

        if (!$langue instanceof Langue) {
            throw new Exception('La langue ' . $locale . ' n\'existe pas');
        }

        $langueModule = new LangueModuleAutoformation();
        $langueModule->setNom($name);
        $langueModule->setLangue($langue);
        $langueModule->setDescription($description);

        return  $langueModule;
    }

    private function setLangueModule(
        LangueModuleAutoformation $langueModule,
        string $name,
        string $description
    ): LangueModuleAutoformation {
        $langueModule->setNom($name);
        $langueModule->setDescription($description);

        return  $langueModule;
    }

    /**
     * @throws Exception
     */
    private function getContext(int $id): array
    {
        /**
         * @var ModuleAutoformation $module
         */
        $module = $this->entityManager->getRepository(ModuleAutoformation::class)->find($id);
        $contexte = [];

        if (!$module instanceof ModuleAutoformation) {
            throw new Exception('Le module avec l\'id ' . $id . ' n\'existe pas');
        }

        foreach ($module->getLangueModuleAutoformations() as $langueModule) {
            $contexte[$langueModule->getLangue()->getLangue()] = [
                'name' => $langueModule->getNom(),
                'description' => $langueModule->getDescription()
            ];

        }

        $contexte['id'] = $id;
        $contexte['url'] = $module->getUrlExterne();
        $contexte['order'] = $module->getPosition();
        $contexte['duration'] = $module->getDuree();
        $contexte['video'] = $module->getPathVideo();
        $contexte['picture'] = $module->getPathPicture();
        $contexte['rubrique'] = $module->getRubrique()->getId();

        return $contexte;
    }
}
