<?php

namespace App\Service\Autoformation;

use Exception;
use App\Entity\Autoformation\LangueRubrique;
use App\Entity\Autoformation\Rubrique;
use App\Entity\Langue;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class ModuleAutoformationRubrique implements ModuleAutoformationInterface, ModuleAutoformationRubriqueInterface
{
    private ?int $idRubrique = null;

    public function __construct(private readonly Environment $twig, private readonly EntityManagerInterface $entityManager, private readonly LoggerInterface $logger)
    {
    }

    public function getBody(string $page, $showButtons = true): string
    {
        if (!$this->pageExists($page)) {
            throw new NotFoundHttpException('La page n\'exixte pas');
        }

        $context = [];

        if($this->idRubrique) {
           $context = $this->getContext($this->idRubrique);
        }

        return $this->twig->render('module_autoformation/rubrique/_content.html.twig', [
                'context' => $context,
                'page' => $page
            ]);
    }

    public function pageExists(string $page): bool
    {
        return (self::PAGE_AGENT === $page) || (self::PAGE_ENTREPRISE === $page);
    }

    public function create(array $data): array
    {
        try {
            $rubrique = new Rubrique();
            $rubrique->setIsAgent(true);

            $langueRubrique = $this->getLangueRubrique($data[self::NAME_FR], self::LOCALE_FR);

            $rubrique->addLangueRubrique($langueRubrique);

            if ($data['page'] === self::PAGE_ENTREPRISE) {
                $rubrique->setIsAgent(false);
                $langueRubrique = $this->getLangueRubrique($data[self::NAME_EN], self::LOCALE_EN);
                $rubrique->addLangueRubrique($langueRubrique);
            }
            $this->entityManager->persist($rubrique);
            $this->entityManager->flush();
        }catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw $exception;
        }

        return self::RESPONSE;
    }

    public function setIdRubrique(int $idRubrique): void
    {
        $this->idRubrique = $idRubrique;
    }

    /**
     * @throws Exception
     */
    public function update(Rubrique $rubrique, array $data): array
    {
        try {
            foreach ($rubrique->getLangueRubriques() as $langueRubrique) {
                if($langueRubrique->getLangue()->getLangue() === self::LOCALE_EN) {
                    $langueRubrique->setNom($data[self::NAME_EN]);
                }

                if($langueRubrique->getLangue()->getLangue() === self::LOCALE_FR) {
                    $langueRubrique->setNom($data[self::NAME_FR]);
                }
            }

            $this->entityManager->flush();
        }catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
            throw $exception;
        }

        return self::RESPONSE;
    }

    /**
     * @throws Exception
     */
    private function getContext(int $id): array
    {
        /**
         * @var Rubrique $rubrique
         */
        $rubrique = $this->entityManager->getRepository(Rubrique::class)->find($id);
        $contexte = [];

        if (!$rubrique instanceof Rubrique) {
            throw new Exception('La rubrique avec l\'id ' . $id . ' n\'existe pas');
        }

        foreach ($rubrique->getLangueRubriques() as $langueRubrique) {
            $contexte[$langueRubrique->getLangue()->getLangue()] = $langueRubrique->getNom();

        }

        $contexte['id'] = $id;

        return $contexte;
    }

    /**
     * @throws Exception
     */
    private function getLangueRubrique(string $name, string $locale): LangueRubrique
    {
        $langue = $this->entityManager->getRepository(Langue::class)->findOneBy(['langue' => $locale]);

        if (!$langue instanceof Langue) {
            throw new Exception('La langue ' . $locale . ' n\'existe pas');
        }

        $langueRubrique = new LangueRubrique();
        $langueRubrique->setNom($name);
        $langueRubrique->setLangue($langue);

        return  $langueRubrique;
    }
}