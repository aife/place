<?php

namespace App\Service\Autoformation;

use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use App\Entity\Autoformation\Rubrique;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;

class ModuleAutoformation implements ModuleAutoformationInterface
{
    public function __construct(private readonly Environment $twig, private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getBody(string $page, bool $showButtons = true): string
    {
        if (!$this->pageExists($page)) {
            throw new NotFoundHttpException(self::MSG_PAGE_NOT_FOUND);
        }

        return $this->twig->render('module_autoformation/_content.html.twig', [
            'rubriques' => $this->getRubriques($page),
            'locale' => self::LOCALE_FR,
            'show_btn' => $showButtons,
            'page' => $page
        ]);
    }

    public function pageExists(string $page): bool
    {
        return (self::PAGE_AGENT === $page) || (self::PAGE_ENTREPRISE === $page);
    }

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getModuleMenuList(): string
    {
        return $this->twig->render('module_autoformation/_list.html.twig', [
            'rubriques' => $this->getRubriques(self::PAGE_AGENT),
            'locale' => self::LOCALE_FR,
        ]);
    }

    /**
     * @return Rubrique[]
     */
    private function getRubriques(string $page): array
    {
        return $this->entityManager->getRepository(Rubrique::class)->findBy([
            'isAgent' => self::PAGE_AGENT === $page
        ]);
    }
}