<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\FlashBag;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageBagService implements MessageBagInterface
{
    public final const PARAM_SAVE_CONSULTATION = "save";
    public final const PARAM_WITHOUT_DCE = "withoutDce";
    public final const PARAM_ETAT_DATE_FIN = "etatDateFin";
    public final const PARAM_DATE_FIN_CONTRAT_INVALID = "dateFinContratInvalid";
    public final const PARAM_SIRET_INVALID = "siretInvalid";
    public final const PARAM_ECHEC_PUB = "EchecPub";
    public final const PARAM_ARCHIVE_EXPIRER = "ArchiveExpirer";
    public final const PARAM_NO_CHECKED_ENV = "noCheckedEnv";
    public final const PARAM_NO_AVIS_GENERATED = "noAvisGenerated";
    public final const PARAM_ETAT_CREATION_DATE_FIN = "etatDateCreationDateFin";
    public final const PARAM_ADD_DV = "addDv";

    public final const TEXT_SAVE_CONSULTATION = "TEXT_CONFIRMATION_ENREGISTREMENT_CONSULTATION";
    public final const TEXT_WITHOUT_DCE = "MSG_AVERTISSEMENT_AUCUN_DCE_CONSULTATION";
    public final const TEXT_ETAT_DATE_FIN = "MSG_AVERTISSEMENT_DATE_FIN";
    public final const TEXT_DATE_FIN_CONTRAT_INVALID = "DATE_FIN_CONTRAT_DEPASSEE";
    public final const TEXT_SIRET_INVALID = "MSG_CONTROLE_SIRET_ACHETEUR_TABLEAU_BORD_CONSULTATION";
    public final const TEXT_ECHEC_PUB = "MESSAGE_ECHEC_ENVOIE_DEPUIS_VALIDER_CONSULTATION";
    public final const TEXT_ARCHIVE_EXPIRER = "TELECHARGEMENT_EXPIRER";
    public final const TEXT_NO_CHECKED_ENV = "TEXT_CHOISIR_TYPE_ENVELOPPE";
    public final const TEXT_NO_AVIS_GENERATED = "TEXT_PROBLEME_GENERATION_AVIS_PDF";
    public final const TEXT_ETAT_CREATION_DATE_FIN = "ERREUR_DELAI_DLRO";

    public final const PARAM_VALUE_ONE = "1";
    public final const PARAM_VALUE_TRUE = "true";

    public final const MESSAGE_LIST = [
        self::PARAM_SAVE_CONSULTATION => self::TEXT_SAVE_CONSULTATION,
        self::PARAM_WITHOUT_DCE => self::TEXT_WITHOUT_DCE,
        self::PARAM_ETAT_DATE_FIN => self::TEXT_ETAT_DATE_FIN,
        self::PARAM_DATE_FIN_CONTRAT_INVALID => self::TEXT_DATE_FIN_CONTRAT_INVALID,
        self::PARAM_SIRET_INVALID => self::TEXT_SIRET_INVALID,
        self::PARAM_ECHEC_PUB => self::TEXT_ECHEC_PUB,
        self::PARAM_ARCHIVE_EXPIRER => self::TEXT_ARCHIVE_EXPIRER,
        self::PARAM_NO_CHECKED_ENV => self::TEXT_NO_CHECKED_ENV,
        self::PARAM_NO_AVIS_GENERATED => self::TEXT_NO_AVIS_GENERATED,
        self::PARAM_ETAT_CREATION_DATE_FIN => self::TEXT_ETAT_CREATION_DATE_FIN,
    ];

    public final const TYPE_FLASH_MESSAGE_SUCCESS = 'success';
    public final const TYPE_FLASH_MESSAGE_WARNING = 'warning';
    public final const TYPE_FLASH_MESSAGE_DANGER = 'danger';

    public final const LABEL_TYPE = 'type';
    public final const LABEL_MESSAGE = 'message';

    public final const STRING_DLRO_TO_REPLACE = '[__DELAI_DLRO__]';

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly ParameterBagInterface $parameterBag,
        private readonly SessionInterface $session
    ) {
    }

    public function displayFlashMessages(array $paramData): void
    {
        foreach ($this->getMessagesToDisplay($paramData) as $message) {
            $this->session->getFlashBag()->add($message[self::LABEL_TYPE], $message[self::LABEL_MESSAGE]);
        }
    }

    private function getMessagesToDisplay(array $paramData): array
    {
        $messages = [];
        foreach (self::MESSAGE_LIST as $keyMsg => $textMsg) {
            if (array_key_exists($keyMsg, $paramData)) {
                $valueRequest = $paramData[$keyMsg];
                $translatedMessage = $this->translator->trans($textMsg);

                if ($keyMsg === self::PARAM_SAVE_CONSULTATION) {
                    // Manage save=true
                    if (self::PARAM_VALUE_TRUE === $valueRequest) {
                        $messages[] = $this->getTransformedMessage(
                            self::TYPE_FLASH_MESSAGE_SUCCESS,
                            $translatedMessage
                        );
                    }
                } elseif ($keyMsg === self::PARAM_ETAT_CREATION_DATE_FIN) {
                    // Manage specific translation of etatDateCreationDateFin
                    $messages[] = $this->getTransformedMessage(
                        self::TYPE_FLASH_MESSAGE_WARNING,
                        str_replace(
                            self::STRING_DLRO_TO_REPLACE,
                            $this->parameterBag->get('DELAI_DLRO'),
                            $translatedMessage
                        )
                    );
                } elseif (
                    in_array($keyMsg, [self::PARAM_NO_CHECKED_ENV, self::PARAM_NO_AVIS_GENERATED])
                    && self::PARAM_VALUE_ONE !== $valueRequest
                    && !isset($paramData[self::PARAM_ADD_DV])
                ) {
                } else {
                    $messages[] = $this->getTransformedMessage(
                        self::TYPE_FLASH_MESSAGE_WARNING,
                        $translatedMessage
                    );
                }
            }
        }

        return $messages;
    }

    private function getTransformedMessage(string $type, string $message): array
    {
        return [
            self::LABEL_TYPE => $type,
            self::LABEL_MESSAGE => $message,
        ];
    }
}
