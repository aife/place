<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\FlashBag;

interface MessageBagInterface
{
    public function displayFlashMessages(array $paramData): void;
}
