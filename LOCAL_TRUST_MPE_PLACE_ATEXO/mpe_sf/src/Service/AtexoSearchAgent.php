<?php

namespace App\Service;

use App\Entity\InvitePermanentTransverse;
use App\Service\Messagerie\MessagerieService;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class AtexoSearchAgent extends AtexoSearch
{
    /*
     * protected $querySelect;
     * protected $queryJoinConditionValidation;
     * protected $querySelectAvecHabilitation;
     */
    protected EntityManagerInterface $em;
    protected ContainerInterface $container;
    protected ?bool $hasHabilitationInvitePermanent = null;
    protected $choix1;
    protected $choix2;
    protected $choix3;
    protected $choix4;
    protected $choix5;
    protected $isEnabled;

    /**
     * AtexoSearch constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        LoggerInterface $logger,
        AtexoUtil $serviceUtile,
        MessagerieService $messagerieService,
        SessionInterface $session
    ) {
        parent::__construct($container, $em, $logger, $serviceUtile, $messagerieService, $session);

        $this->container = $container;
        $this->em = $em;
        $this->choix3 = $this->container->getParameter('REGLE_VALIDATION_TYPE_3');
        $this->choix4 = $this->container->getParameter('REGLE_VALIDATION_TYPE_4');
    }

    /**
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function getConsultationsByQuery(
        Atexo_Consultation_CriteriaVo $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false
    ) {
        $QueryForValidationOnlyAgent = null;
        $QueryForAgentInvitedWithTheirHabilitation = null;
        $QueryForReadOnlyAgent = null;
        $queryParams = [];
        $queryServiceNon = '';
        $queryEtatAttenteValidation = '';
        $queryLeftJoinOrganisme = '';
        $queryJoinCondition = '';
        $queryJoinConditionValidation = '';
        $queryValidationIntermediaire = '';
        $queryValidationFinale = '';
        $queryValidationSimple = '';
        $listOfServicesAllowed = null;
        $serviceValidationFinale = null;

        // Test des parametres obligatoires
        if (
            '' === $criteriaVo->getAcronymeOrganisme()
            || '' === $criteriaVo->getIdService()
        ) {
            throw new Atexo_Consultation_Exception('Les parametres AcronymeOrganisme et IdService sont obligatoires');
        }
        if ($returnJustNumberOfElement || $returnReferences) {
            $querySelect = $this->getSqlCountAgent();
        } else {
            $querySelect = $this->getSqlAgent();
        }

        if (!$criteriaVo->getCalledFromHelios() && '' !== $criteriaVo->getIdService()) {
            $idUser = Atexo_CurrentUser::getId();
            //On recupere la liste des entités qui ont le droit de voir la consultation organisation_centralisee
            if (Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = $this->getInvitePermanentMonEntite(
                    Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite'),
                    Atexo_CurrentUser::getIdServiceAgentConnected()
                );
                $invitePermanentEntiteDependante = Atexo_CurrentUser::hasHabilitation(
                    'InvitePermanentEntiteDependante'
                );
                $idServiceAgentConnected = Atexo_CurrentUser::getIdServiceAgentConnected();
                $sousServicesIncluantLeService = Atexo_EntityPurchase::retrieveAllChildrenServices(
                    $criteriaVo->getIdService(),
                    $criteriaVo->getAcronymeOrganisme(),
                    true
                );
                $listOfServicesAllowed = $this->getInvitePermanentEntiteDependante(
                    $listOfServicesAllowed,
                    $invitePermanentEntiteDependante,
                    $idServiceAgentConnected,
                    $sousServicesIncluantLeService
                );
                $invitePermanentTransverse = Atexo_CurrentUser::hasHabilitation('InvitePermanentTransverse');
                $listOfServicesAllowed = $this->getInvitePermanentTransverse(
                    $listOfServicesAllowed,
                    $invitePermanentTransverse,
                    $idUser
                );

                if (is_array($listOfServicesAllowed) && in_array(null, $listOfServicesAllowed)) {
                    $listOfServicesAllowed = array_filter($listOfServicesAllowed, fn($element) => $element !== null);
                }

                $rawListOfServicesAllowed = $listOfServicesAllowed;
                if ($this->hasHabilitationInvitePermanent) {
                    $listOfServicesAllowed = '(' . str_replace(
                        ',,',
                        ',',
                        implode(',', $listOfServicesAllowed)
                    ) . ')';
                } else {
                    $listOfServicesAllowed = '( )';
                }

                if ($this->hasHabilitationInvitePermanent) {
                    if (
                        is_null(Atexo_CurrentUser::getUserSf()->getServiceId())
                        || (is_array($rawListOfServicesAllowed) && in_array(0, $rawListOfServicesAllowed))
                    ) {
                        $queryServiceNon = " (consultation.service_id is null "
                            . ' or consultation.service_id IN ' . $listOfServicesAllowed . ')'
                            . " and consultation.organisme = '"
                            . Atexo_CurrentUser::getUserSf()->getOrganisme()->getAcronyme() . "' ";
                    } else {
                        $queryServiceNon = ' consultation.service_id IN ' . $listOfServicesAllowed . ' ';
                    }
                }

                if (!$this->hasHabilitationInvitePermanent && !is_null(Atexo_CurrentUser::getCurrentServiceId())) {
                    $queryServiceNon = ' consultation.service_id = ' . Atexo_CurrentUser::getCurrentServiceId() . ' 
                    AND consultation.id_createur = ' . $idUser;
                }

                $validationFinale = Atexo_CurrentUser::hasHabilitation('ValidationFinale');
                $serviceValidationFinale = $this->getListServiceValidationFinaleModeCentralise(
                    Atexo_CurrentUser::getIdServiceAgentConnected(),
                    Atexo_CurrentUser::getOrganismAcronym()
                );
                $queryValidationFinale = $this->getSqlValidationFinal($validationFinale, $serviceValidationFinale);
            } else {
                if ('' !== $criteriaVo->getIdService()) {
                    $listOfServicesAllowed = $criteriaVo->getIdService();
                    $idService = Atexo_CurrentUser::getUserSf()->getServiceId();
                    if (Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) {
                        if (is_null($idService)) {
                            $queryServiceNon = " consultation.service_id is null and consultation.organisme = '"
                                . Atexo_CurrentUser::getUserSf()->getOrganisme()->getAcronyme() . "' ";
                        } else {
                            $queryServiceNon = " consultation.service_id = '" . $idService . "' ";
                        }
                    }

                    $validationFinale = Atexo_CurrentUser::hasHabilitation('ValidationFinale');
                    $queryValidationFinale = $this->getSqlValidationFinal(
                        $validationFinale,
                        $idService
                    );

                    $queryEtatAttenteValidation = "AND consultation.etat_en_attente_validation ='1'";
                }
            }

            if ('' !== $listOfServicesAllowed) {
                if ($criteriaVo->getNoTemporaryCOnsultation()) {
                    $queryValidationIntermediaire = '';
                    $queryValidationFinale = '';
                    $queryValidationSimple = '';
                } else {
                    //Si et SSi une des deux options actives : cons a approuver ou cons a valider
                    if (
                        $criteriaVo->getConsultationAValiderSeulement() ||
                        $criteriaVo->getConsultationAApprouverSeulement()
                    ) {
                        //Si les consultations a retourner sont a valider ou a approuver seulement,
                        //la requete sur la liste desservices autorise par defaut supprimee.
                        $queryServiceNon = '';
                        if (!$criteriaVo->getConsultationAApprouverSeulement()) {
                            //Si l'option 'consulation a approuver' n'est pas a true
                            // => ne pas remonter les cons a approuver
                            $queryValidationIntermediaire = '';
                        }
                        if (!$criteriaVo->getConsultationAValiderSeulement()) {
                            //Si l'option 'consulation a valider' n'est pas a true => ne pas remonter les cons a valider
                            $queryValidationSimple = '';
                            $queryValidationFinale = '';
                        }
                    }
                }

                if (!Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) {
                    $queryServiceNon = 'consultation.id_createur = ' . $idUser;
                }

                $queryJoinCondition = ($queryServiceNon) ? ' AND ( ' . $queryServiceNon . ')' : ' AND 0';
                if ($this->hasHabilitationInvitePermanent || Atexo_CurrentUser::hasHabilitation('ValidationFinale')) {
                    $queryValidationFinale = $queryValidationFinale ? ' OR ' . $queryValidationFinale : '';
                    $queryValidationIntermediaire = $queryValidationIntermediaire
                        ? ' OR ' . $queryValidationIntermediaire : '';
                    $queryValidationSimple = $queryValidationSimple ? ' OR ' . $queryValidationSimple : '';
                } else {
                    $queryValidationFinale = '';
                    $queryValidationIntermediaire = '';
                    $queryValidationSimple = '';
                }
                $queryJoinConditionValidation .= <<<QUERY
AND ( 0 $queryValidationFinale $queryValidationIntermediaire $queryValidationSimple )
$queryEtatAttenteValidation
QUERY;
            }
        }

        $queryLeftJoinCategorieLot = ($criteriaVo->getKeyWordAdvancedSearch() ||
            $criteriaVo->getKeyWordRechercheRapide() ||
            $criteriaVo->getIdCodeCpv2()) ? $this->getSqlLeftJoinCategorieLot() : '';

        $queryFrom = <<<QUERY
FROM consultation
		$queryLeftJoinCategorieLot
		$queryLeftJoinOrganisme
QUERY;

        //on parcourt l'objet CriteriaVo passe en parametre pour ajouter a la clause where
        $otherConditions = $this->otherConditionsAgent($criteriaVo, $queryParams);
        $stateCondition = $this->stateConditionsAgent($criteriaVo, $queryParams);
        $otherConditions .= $stateCondition;
        $idAgentConnecte = $criteriaVo->getConnectedAgentId();

        $queryParams['achatPublique'] = '0';
        $queryParams['organismeAgent'] = $criteriaVo->getAcronymeOrganisme();
        $conditionFromAcheteur = ' consultation.organisme =:organismeAgent 
        AND consultation.consultation_achat_publique=:achatPublique ';

        $querySelectAvecHabilitation = (!$returnJustNumberOfElement && !$returnReferences)
            ? ', 1 as invitedWithHisHabilitation, 0 as invitedReadOnly, 0 as invitedValidationOnly' : '';
        $queryForPole = <<<QUERY
$querySelect $querySelectAvecHabilitation
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinCondition
	$otherConditions

QUERY;
        //requete 2 : la liste des consultations auxquelles il est invite avec ses habilitations
        if (!$criteriaVo->getConsultationAValiderSeulement() && !$criteriaVo->getConsultationAApprouverSeulement()) {
            $QueryForAgentInvitedWithTheirHabilitation = <<<QUERY
$querySelect  $querySelectAvecHabilitation
$queryFrom , InterneConsultation
WHERE
	 $conditionFromAcheteur
	 AND InterneConsultation.interne_id = '$idAgentConnecte'
	 AND InterneConsultation.consultation_id = consultation.id
	 AND InterneConsultation.organisme = consultation.organisme
	 $otherConditions

QUERY;

            $querySelectInvitedReadOnly = (!$returnJustNumberOfElement && !$returnReferences)
                ? ', 0 as invitedWithHisHabilitation, 1 as invitedReadOnly, 0 as invitedValidationOnly' : '';
            //requete 3 : la liste des consultations auxquelles l'agent est invite en lecture seule
            $QueryForReadOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedReadOnly
$queryFrom , InterneConsultationSuiviSeul
WHERE
	$conditionFromAcheteur
	AND InterneConsultationSuiviSeul.interne_id = '$idAgentConnecte'
	AND InterneConsultationSuiviSeul.consultation_id = consultation.id
	AND InterneConsultationSuiviSeul.organisme = consultation.organisme
	$otherConditions

QUERY;
            $querySelectInvitedValidationOnly = (!$returnJustNumberOfElement && !$returnReferences)
                ? ' , 0 as invitedWithHisHabilitation, 0 as invitedReadOnly, 1 as invitedValidationOnly' : '';
            $QueryForValidationOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedValidationOnly
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinConditionValidation
	$otherConditions

QUERY;
        }

        //Fusionner les 3 requetes
        $finalQueryPart1 = $queryForPole . ($QueryForValidationOnlyAgent
                ? ' UNION ' . $QueryForValidationOnlyAgent : '');
        $finalQueryPart2 = ($QueryForAgentInvitedWithTheirHabilitation ? $QueryForAgentInvitedWithTheirHabilitation .
                ' UNION ' : '') . $QueryForReadOnlyAgent;

        $finalQuery = ($finalQueryPart1) ? $finalQueryPart1
            . (($finalQueryPart2) ? ' UNION ' . $finalQueryPart2 : ' ') : $finalQueryPart2;

        if (!$finalQuery) {
            if ($returnJustNumberOfElement) {
                return 0;
            } else {
                return [];
            }
        }
        if (!$returnJustNumberOfElement && !$returnReferences) {
            if ($criteriaVo->getSortByElement()) {
                $finalQuery .= ' ORDER BY ' . $criteriaVo->getSortByElement() . ' ' .
                    $criteriaVo->getSensOrderBy() . ' ' .
                    (('datefin' != strtolower($criteriaVo->getSortByElement())) ? ' , datefin DESC ' : ' ');
            } else {
                $finalQuery .= ' ORDER BY datefin DESC ';
            }
        }

        $finalQuery = preg_replace("#\([ ]+OR[ ]+\(#", '( (', $finalQuery);
        if ($forAlertesEntreprises) {
            $finalQuery = str_replace('consultation.', 'consultation_alertes.', $finalQuery);
            $finalQuery = str_replace('FROM consultation', 'FROM consultation_alertes', $finalQuery);
        }


        $stmt = $this->em->getConnection()->executeQuery($finalQuery, $queryParams);

        return $stmt->fetchAllAssociative();
    }

    /**
     * Methode qui prépare le comptage des consultations.
     *
     * @return string
     */
    public function getSqlCountAgent()
    {
        return <<<QUERY
SELECT
   DISTINCT consultation.id
QUERY;
    }

    /**
     * Méthode qui prépare les entête de la requête SQL.
     *
     * @return string
     */
    public function getSqlAgent()
    {
        return <<<QUERY
SELECT
   DISTINCT consultation.id,
   consultation.datefin,
   reference_utilisateur,
   nom_createur,
   prenom_createur,
   intitule,
   objet,
   id_type_avis,
   alloti
QUERY;
    }

    public function getSqlLeftJoinCategorieLot()
    {
        $queryLeftJoinCategorieLot = <<<QUERY
LEFT JOIN
		CategorieLot ON (CategorieLot.consultation_id = consultation.id 
		AND CategorieLot.organisme = consultation.organisme)
QUERY;

        return $queryLeftJoinCategorieLot;
    }

    /**
     * @param $invitePermanentMonEntite
     * @param $idServiceAgentConnected
     *
     * @return array|null
     */
    public function getInvitePermanentMonEntite($invitePermanentMonEntite, $idServiceAgentConnected)
    {
        $listOfServicesAllowed = null;
        if ($invitePermanentMonEntite) {
            $this->hasHabilitationInvitePermanent = true;
            $listOfServicesAllowed = [$idServiceAgentConnected];
        }

        return $listOfServicesAllowed;
    }

    /**
     * @param $listOfServicesAllowed
     * @param $invitePermanentEntiteDependante
     * @param $idServiceAgentConnected
     * @param $sousServicesIncluantLeService
     *
     * @return array|null
     */
    public function getInvitePermanentEntiteDependante(
        $listOfServicesAllowed,
        $invitePermanentEntiteDependante,
        $idServiceAgentConnected,
        $sousServicesIncluantLeService
    ) {
        if ($invitePermanentEntiteDependante) {
            $this->hasHabilitationInvitePermanent = true;
            $sousServices = array_diff($sousServicesIncluantLeService, [$idServiceAgentConnected]);
            if (null === $listOfServicesAllowed) {
                $listOfServicesAllowed = [];
            }
            $listOfServicesAllowed = array_merge($sousServices, $listOfServicesAllowed);
            if (0 == count($listOfServicesAllowed)) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = null;
            }
        }

        return $listOfServicesAllowed;
    }

    /**
     * @param $listOfServicesAllowed
     * @param $invitePermanentTransverse
     * @param $idUser
     *
     * @return array|null
     */
    public function getInvitePermanentTransverse($listOfServicesAllowed, $invitePermanentTransverse, $idUser)
    {
        if ($invitePermanentTransverse) {
            $this->hasHabilitationInvitePermanent = true;
            if (null === $listOfServicesAllowed) {
                $listOfServicesAllowed = [];
            }
            $listOfServicesAllowed = array_merge(
                $this->em->getRepository(InvitePermanentTransverse::class)->getServicesIdByAgentId($idUser),
                $listOfServicesAllowed
            );
            if (0 == count($listOfServicesAllowed)) {
                $this->hasHabilitationInvitePermanent = false;
                $listOfServicesAllowed = null;
            }
        }

        return $listOfServicesAllowed;
    }

    /*
     * @param $listOfServicesAllowed
     * @return string|null
     */
    public function getSqlValidationIntermediaire(
        $listOfServicesAllowed,
        $habilitationValidationIntermediaire,
        $operateur = 'IN'
    ) {
        $queryValidationIntermediaire = null;
        if ($habilitationValidationIntermediaire) {
            $queryValidationIntermediaire = <<<QUERY
((consultation.id_regle_validation='$this->choix4' OR consultation.id_regle_validation='$this->choix5' )
AND service_validation_intermediaire $operateur $listOfServicesAllowed
AND (date_validation_intermediaire IS NULL OR date_validation_intermediaire = '0000-00-00 00:00:00') )
QUERY;
        }

        return $queryValidationIntermediaire;
    }

    /**
     * @param $validationFinale
     * @param $serviceValidationFinale
     * @param string $operateur
     *
     * @return string|null
     */
    public function getSqlValidationFinal($validationFinale, $serviceValidationFinale, $operateur = 'IN')
    {
        $queryValidationFinale = null;
        if ($validationFinale) {
            $queryValidationFinale = <<<QUERY
(
	(
		(
			   consultation.id_regle_validation='$this->choix3'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation $operateur ('$serviceValidationFinale')
				AND (
						datevalidation IS NULL
						OR datevalidation = '0000-00-00 00:00:00'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation='$this->choix4'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation $operateur ('$serviceValidationFinale')
				AND (
					datevalidation IS NULL
					OR datevalidation = '0000-00-00 00:00:00'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != '0000-00-00 00:00:00'
				AND etat_approbation='1'
			)
	)
)
QUERY;
        }

        return $queryValidationFinale;
    }

    /**
     * @param $validationSimple
     * @param $listOfServicesAllowed
     * @param string $operateur
     *
     * @return string|null
     */
    public function getSqlValidationSimple($validationSimple, $listOfServicesAllowed, $operateur = 'IN')
    {
        $queryValidationSimple = null;
        if ($validationSimple) {
            $queryValidationSimple = <<<QUERY
(
	consultation.id_regle_validation='$this->choix1'
	AND service_validation $operateur $listOfServicesAllowed
	AND (datevalidation IS NULL OR datevalidation = '0000-00-00 00:00:00')
)
QUERY;
        }

        return $queryValidationSimple;
    }

    /**
     * @param $idServiceAgentConnected
     * @param $acronymeOrganisme
     *
     * @return array|null
     */
    public function getListServiceValidationFinaleModeCentralise($idServiceAgentConnected, $acronymeOrganisme)
    {
        //en mode centralisé : mon service et mes parents sinon en décentralisé mon service
        $allParent = Atexo_EntityPurchase::getAllParents($idServiceAgentConnected, $acronymeOrganisme);
        $serviceValidationFinale = $idServiceAgentConnected . '';
        if (count($allParent) > 0) {
            foreach ($allParent as $parent) {
                $serviceValidationFinale .= ', ' . $parent['id'];
            }
        }

        return $serviceValidationFinale;
    }
}
