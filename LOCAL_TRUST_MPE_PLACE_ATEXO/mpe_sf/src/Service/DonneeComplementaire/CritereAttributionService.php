<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DonneeComplementaire;

use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\SousCritereAttribution;

class CritereAttributionService
{
    public function isCriterePonderationValid(array $criteres, float $ponderation): bool
    {
        if (!$criteres) {
            return true;
        }

        return $this->isPonderationValid($criteres, $ponderation);
    }

    private function isPonderationValid(array $criteres, float $ponderation): bool
    {
        $totalPonderation = $ponderation;
        /** @var CritereAttribution|SousCritereAttribution $value */
        foreach ($criteres as $value) {
            $totalPonderation += $value->getPonderation();
        }

        return !($totalPonderation > 100);
    }
}
