<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DonneeComplementaire;

use App\Entity\Consultation;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Consultation\SousCritereAttribution;
use App\Entity\FormePrix;
use App\Entity\Lot;
use App\Entity\Tranche;
use App\Entity\ValeurReferentiel;
use App\Repository\FormePrixHasRefTypePrixRepository;
use App\Repository\FormePrixPfHasRefVariationRepository;
use App\Repository\FormePrixPuHasRefVariationRepository;
use App\Repository\FormePrixRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DonneeComplementaireService
{
    private const CRITERE_ATTRIBUTION_PONDERATION = 2;

    final public const ELECTRONIC_CATALOG_VALUE = [
        1 => 'ELECTRONIC_CATALOG_REQUIRED',
        2 => 'ELECTRONIC_CATALOG_AUTHORIZED',
        3 => 'ELECTRONIC_CATALOG_FORBIDDEN',
    ];

    final public const LIST_CRITERE_ATTRIBUTION = [
        1 => 'LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC',
        2 => 'LIBELLE_CRITERE_ATTRIBUTION_PONDERATION',
        3 => 'LIBELLE_CRITERE_ATTRIBUTION_PRIX_UNIQUE',
        4 => 'LIBELLE_CRITERE_ATTRIBUTION_DANS_DOC_CONSULTATION',
        5 => 'LIBELLE_CRITERE_ATTRIBUTION_COUT_UNIQUE',
    ];

    public function __construct(
        private readonly EntityManagerInterface $em,
        private FormePrixRepository $formePrixRepository,
        private ParameterBagInterface $parameterBag,
        private TranslatorInterface $translator,
        private FormePrixPfHasRefVariationRepository $prixForfaitRepository,
        private FormePrixPuHasRefVariationRepository $prixUnitaireRepository,
        private FormePrixHasRefTypePrixRepository $prixTypeRepository
    ) {
    }

    /**
     * Permet de récupérer l'intitulé du critère d'attribution
     *
     * @param int $id
     */
    public static function getTypeCriteriaAttribution(?int $index): string
    {
        $typeCriteriaAttribution = [
            1 => 'LIBELLE_CRITERE_ATTRIBUTION_ORDRE_PRIO_DESC',
            2 => 'LIBELLE_CRITERE_ATTRIBUTION_PONDERATION',
            3 => 'LIBELLE_CRITERE_ATTRIBUTION_PRIX_UNIQUE',
            4 => 'LIBELLE_CRITERE_ATTRIBUTION_DANS_DOC_CONSULTATION',
            5 => 'LIBELLE_CRITERE_ATTRIBUTION_COUT_UNIQUE',
        ];

        return $typeCriteriaAttribution[$index] ?? '';
    }

    /**
     * Permet d'enregistrer l'ensemble des données pour créer un critère d'atribution
     *
     * @param $data
     */
    public function saveCriteriaAttribution(string $data): void
    {
        $data = json_decode($data, true, 512, JSON_THROW_ON_ERROR);

        if (
            isset($data['criteria'])
            && is_array($data['criteria'])
            && count($data['criteria']) > 0
        ) {
            $idConsultation = $data['criteria'][0]['id_consultation'];
            $consultation = $this->em->getRepository(Consultation::class)->find($idConsultation);

            if (!$consultation) {
                throw new NotFoundHttpException(
                    'Consultation ' . $idConsultation . ' not found.',
                    null,
                    Response::HTTP_NOT_FOUND
                );
            }

            if (!$consultation->getDonneeComplementaire()) {
                $donneeComplementaire = new DonneeComplementaire();
                $donneeComplementaire->setProjetFinanceFondsUnionEuropeenne(0);
                $donneeComplementaire->setIdentificationProjet('');
            } else {
                $donneeComplementaire = $consultation->getDonneeComplementaire();
            }
            $donneeComplementaire->setIdCritereAttribution(self::CRITERE_ATTRIBUTION_PONDERATION);
            $this->em->persist($donneeComplementaire);

            $consultation->setDonneeComplementaire($donneeComplementaire);

            foreach ($data as $criterias) {
                $previousIdLot = [];
                foreach ($criterias as $criteria) {
                    $breakLoop = false;
                    $lot = null;
                    if (!empty($criteria['id_lot'])) {
                        $lot = $this->em->getRepository(Lot::class)->find($criteria['id_lot']);
                        if ($lot) {
                            $donneeComplementaire = $lot->getDonneeComplementaire() ?? $consultation->getDonneeComplementaire();
                            $donneeComplementaire->setIdCritereAttribution(self::CRITERE_ATTRIBUTION_PONDERATION);
                            if ($criteria['identical_bareme']) {
                                $donneeComplementaire->setCriteresIdentiques('1');
                                /*
                                *  Si le lot n'a pas de donnee complementaire,
                                *  on enregistrera en BDD les criteres d'un seul lot, d'où le 'breakLoop',
                                *  à contrario chaque lot aura ses enregistrements de critères
                                */
                                if (
                                    $lot->getDonneeComplementaire() === null
                                    && count($previousIdLot) > 0
                                    && end($previousIdLot) != $lot->getId()
                                ) {
                                    $breakLoop = true;
                                } else {
                                    $previousIdLot[] = $lot->getId();
                                }
                            } else {
                                $donneeComplementaire->setCriteresIdentiques('0');
                            }
                        }
                    }

                    if ($breakLoop) {
                        break;
                    }

                    $criteriaAttribution = new CritereAttribution();
                    $criteriaAttribution->setDonneeComplementaire($donneeComplementaire);
                    $criteriaAttribution->setEnonce($criteria['name']);
                    $criteriaAttribution->setPonderation($criteria['rate']);
                    if ($lot !== null) {
                        $criteriaAttribution->setLot($lot);
                    }
                    $this->em->persist($criteriaAttribution);

                    if (
                        $criteria['sub_criteria'] !== null
                        && (is_countable($criteria['sub_criteria']) ? count($criteria['sub_criteria']) : 0) > 0
                    ) {
                        foreach ($criteria['sub_criteria'] as $subCriteria) {
                            $subCriteriaAttribution = new SousCritereAttribution();
                            $subCriteriaAttribution->setEnonce($subCriteria['name']);
                            $subCriteriaAttribution->setPonderation($subCriteria['rate']);
                            $subCriteriaAttribution->setCritereAttribution($criteriaAttribution);
                            $this->em->persist($subCriteriaAttribution);
                        }
                    }
                }
            }
            $this->em->flush();
        } else {
            throw new NotFoundHttpException('No criteria found in the json', null, Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Récupère les informations pour l'affichage
     * des critères d'attribution dans la page d'analyse des offres
     *
     */
    public function getCriteriaAttributionInfos(Consultation $consultation, bool $detailledAttribution = false): array
    {
        $typeCriteriaAttribution = $this->getCritereReferentiel($consultation->getDonneeComplementaire());

        $labelCriteriaAttribution = ($consultation->getDonneeComplementaire())
            ? self::LIST_CRITERE_ATTRIBUTION[($consultation->getDonneeComplementaire())->getIdCritereAttribution()]
            : ''
        ;

        $lots = $this->em->getRepository(Lot::class)->findBy(['consultation' => $consultation]);

        $infoLotForFront = [];
        $listCriteriasAttribution = [];
        $identicalBareme = null;

        $criteriaAttributionRepo = $this->em->getRepository(CritereAttribution::class);

        if (empty($lots)) {
            $criteriasAttribution = $criteriaAttributionRepo->findBy(
                ['donneeComplementaire' => $consultation->getDonneeComplementaire()],
                ['id' => 'ASC']
            );
            if ($detailledAttribution) {
                $listCriteriasAttribution = $this->getCriteriaNode($criteriasAttribution);
            } else {
                $listCriteriasAttribution[] = ['criteria' => $criteriasAttribution];
            }
        } else {
            foreach ($lots as $lot) {
                $donneeComplementaire = $consultation->getDonneeComplementaire();
                $identicalBareme = (int) $donneeComplementaire?->getCriteresIdentiques();

                if(!$donneeComplementaire || "1" != $donneeComplementaire->getCriteresIdentiques()) {
                    $donneeComplementaire = $lot->getDonneeComplementaire();
                }
                $criteriasAttribution = $criteriaAttributionRepo->findBy(
                    ['donneeComplementaire' => $donneeComplementaire, 'lot' => $lot],
                    ['id' => 'ASC']
                );

                if (count($criteriasAttribution) == 0) {
                    $criteriasAttribution = $criteriaAttributionRepo->findBy(
                        ['donneeComplementaire' => $donneeComplementaire],
                        ['id' => 'ASC']
                    );
                }

                $listCriteriasAttribution[] = [
                    'lot' => $lot,
                    'criteria' => $this->getCriteriaNode($criteriasAttribution)
                ];
                $infoLotForFront[] = ['id' => $lot->getId(), 'name' => $lot->getDescription()];

            }
        }

        return [
            'typeCriteriaAttribution' => $typeCriteriaAttribution,
            'listCriteriaAttribution' => $listCriteriasAttribution,
            'infoLotFront' => $infoLotForFront,
            'identicalBareme' => $identicalBareme,
            'labelCriteriaAttribution' => $labelCriteriaAttribution,
        ];
    }

    public function getConsultationTranches(Consultation $consultation): ?array
    {
        $tranches = null;
        $listReferentielPrix = $this->getReferentielPrix();
        $prixTypesReferentiel = $this->getTypesPrix();
        $unitesPrix = $this->getUnitesPrix();
        if ($consultation->getDonneeComplementaire()) {
            $consultationTranches = $this->em->getRepository(Tranche::class)
                    ->findBy(
                        [
                            'idDonneeComplementaire' => $consultation->getDonneeComplementaire()->getIdDonneeComplementaire(),
                        ]
                    );
            foreach ($consultationTranches as $tranche) {
                $data['numero'] = $tranche->getCodeTranche();
                $data['typeTranche'] = $tranche->getNatureTranche() === $this->parameterBag->get('TRANCHE_FIXE')
                    ? 'Tranche ferme'
                    : 'Tranche optionnelle';
                $data['intitule'] = $tranche->getIntituleTranche();
                $trancheForme = $this->formePrixRepository->findOneBy(['idFormePrix' => $tranche->getIdFormePrix()]);
                $data['formePrix'] = $this->getFormPrixData(
                    $trancheForme,
                    $listReferentielPrix,
                    $prixTypesReferentiel,
                    $unitesPrix
                );
                $tranches[] = $data;
            }
        }

        return $tranches;
    }

    public function getLotTranches(?Lot $lot): ?array
    {
        $tranches = null;
        $listReferentielPrix = $this->getReferentielPrix();
        $prixTypesReferentiel = $this->getTypesPrix();
        $unitesPrix = $this->getUnitesPrix();
        if (!$lot instanceof Lot) {
            return null;
        }

        if ($donneeComplementaire = $lot->getDonneeComplementaire()) {
            $lotTranches = $this->em->getRepository(Tranche::class)
                ->findBy(['idDonneeComplementaire' => $donneeComplementaire->getIdDonneeComplementaire()]);
            foreach ($lotTranches as $tranche) {
                $data['numero'] = $tranche->getCodeTranche();
                $data['typeTranche'] = $tranche->getNatureTranche() === $this->parameterBag->get('TRANCHE_FIXE')
                    ? 'Tranche ferme'
                    : 'Tranche optionnelle';
                $data['intitule'] = $tranche->getIntituleTranche();
                $trancheForme = $this->formePrixRepository->findOneBy(['idFormePrix' => $tranche->getIdFormePrix()]);
                $data['formePrix'] = $this->getFormPrixData(
                    $trancheForme,
                    $listReferentielPrix,
                    $prixTypesReferentiel,
                    $unitesPrix
                );
                $tranches[] = $data;
            }
        }

        return $tranches;
    }

    public function getConsultationFormePrix(Consultation $consultation): ?array
    {
        $forme = null;
        $listReferentielPrix = $this->getReferentielPrix();
        $prixTypesReferentiel = $this->getTypesPrix();
        $unitesPrix = $this->getUnitesPrix();
        if ($consultation->getDonneeComplementaire()) {
            $consulttionForme = $this->formePrixRepository->findOneBy(['idFormePrix' => $consultation->getDonneeComplementaire()->getIdFormePrix()]);
            if (!$consulttionForme instanceof FormePrix) {
                return null;
            }
            $forme = $this->getFormPrixData(
                $consulttionForme,
                $listReferentielPrix,
                $prixTypesReferentiel,
                $unitesPrix
            );
        }

        return $forme ? array_values(array_filter($forme)) : $forme;
    }

    public function getLotFormePrix(?Lot $lot): ?array
    {
        $forme = null;
        $listReferentielPrix = $this->getReferentielPrix();
        $prixTypesReferentiel = $this->getTypesPrix();
        $unitesPrix = $this->getUnitesPrix();

        if (!$lot instanceof Lot) {
            return null;
        }

        if ($donneeComplementaire = $lot->getDonneeComplementaire()) {
            $lotForme = $this->formePrixRepository->findOneBy(['idFormePrix' => $donneeComplementaire->getIdFormePrix()]);
            if (!$lotForme instanceof FormePrix) {
                return null;
            }
            $forme = $this->getFormPrixData(
                $lotForme,
                $listReferentielPrix,
                $prixTypesReferentiel,
                $unitesPrix
            );
        }

        return $forme ? array_values(array_filter($forme)) : $forme;
    }

    private function getFormPrixData(
        ?FormePrix $formePrix,
        array $listReferentielPrix,
        array $prixTypesReferentiel,
        array $unitesPrix
    ): ?array {
        if (!$formePrix instanceof FormePrix) {
            return null;
        }

        $formes = [];
        if ($this->parameterBag->get('KEY_PRIX_UNITAIRE') !== $formePrix->getFormePrix()) {
            $prixForfait = $this->prixForfaitRepository->findOneBy(['idFormePrix' => $formePrix->getIdFormePrix()]);
            $data['formePrix'] = $this->translator->trans('DEFINE_PRIX_FORFAITE');
            $data['variation'] = $listReferentielPrix[$prixForfait->getIdVariation()];
            $formes[] = $data;
        }

        if ($this->parameterBag->get('KEY_PRIX_FORFAITAIRE') !== $formePrix->getFormePrix()) {
            $data['formePrix'] = $this->translator->trans('TEXT_PRIX_UNITAIRE');
            $data['modalite'] = $formePrix->getModalite() === $this->parameterBag->get('MODALITE_BON_COMMANDE')
                                ? $this->translator->trans('DEFINE_BONS_COMMANDE')
                                : $this->translator->trans('FCSP_A_QUANTITE_DEFINIE');
            $prixTypes = $this->prixTypeRepository->findBy(['idFormePrix' => $formePrix->getIdFormePrix()]);
            foreach ($prixTypes as $type) {
                $data['typePrix'][] = $prixTypesReferentiel[$type->getIdTypePrix()];
            }
            if ($formePrix->getPuMin() || $formePrix->getPuMax()) {
                $data['min'] = $formePrix->getPuMin();
                $data['max'] = $formePrix->getPuMax();
                $data['unite'] = $unitesPrix[$formePrix->getIdMinMax()];
            }
            $prixUnitaire = $this->prixUnitaireRepository->findOneBy(['idFormePrix' => $formePrix->getIdFormePrix()]);
            $data['variation'] = $listReferentielPrix[$prixUnitaire->getIdVariation()];
            $formes[] = $data;
        }

        return $formes;
    }

    private function getReferentielPrix(): array
    {
        $list = [];
        $referentielsPrix = $this->em->getRepository(ValeurReferentiel::class)
            ->findBy(['idReferentiel' => $this->parameterBag->get('REFERENTIEL_VARIATION_PRIX')]);
        foreach ($referentielsPrix as $referentiel) {
            $list[$referentiel->getId()] = $referentiel->getLibelleValeurReferentiel();
        }

        return $list;
    }

    private function getTypesPrix(): array
    {
        $list = [];
        $referentielsPrix = $this->em->getRepository(ValeurReferentiel::class)
            ->findBy(['idReferentiel' => $this->parameterBag->get('REFERENTIEL_TYPE_PRIX')]);
        foreach ($referentielsPrix as $referentiel) {
            $list[$referentiel->getId()] = $referentiel->getLibelleValeurReferentiel();
        }

        return $list;
    }

    private function getUnitesPrix(): array
    {
        $list = [];
        $referentielsPrix = $this->em->getRepository(ValeurReferentiel::class)
            ->findBy(['idReferentiel' => $this->parameterBag->get('REFERENTIEL_UNITE_PRIX')]);
        foreach ($referentielsPrix as $referentiel) {
            $list[$referentiel->getId()] = $referentiel->getLibelleValeurReferentiel();
        }

        return $list;
    }

    public function getLotCriteriaAttribution(Lot $lot): array
    {
        if ($lot->getConsultation()?->getDonneeComplementaire()?->getCriteresIdentiques()) {
            $donneeComplementaire = $lot->getConsultation()->getDonneeComplementaire();
            $lot = null;
        } else {
            $donneeComplementaire = $lot->getDonneeComplementaire();
        }

        return $this->getCriteriasByDonneeComplementaire($donneeComplementaire, $lot);
    }

    private function getCriteriasByDonneeComplementaire(
        ?DonneeComplementaire $donneeComplementaire,
        ?Lot $lot = null
    ): array {
        $typeCriteriaAttribution = $this->getCritereReferentiel($donneeComplementaire);

        $criteriaAttributionRepo = $this->em->getRepository(CritereAttribution::class);

        $criteriasAttribution = [];
        if ($lot) {
            $criteriasAttribution = $criteriaAttributionRepo->findBy(
                [
                    'donneeComplementaire' => $donneeComplementaire,
                    'lot' => $lot,
                ],
                ['id' => 'ASC']
            );
        }

        if (count($criteriasAttribution) == 0) {
            $criteriasAttribution = $criteriaAttributionRepo->findBy(
                ['donneeComplementaire' => $donneeComplementaire],
                ['id' => 'ASC']
            );
        }

        return [
            'typeCriteriaAttribution' => $typeCriteriaAttribution,
            'listCriteriaAttribution' => $this->getCriteriaNode($criteriasAttribution),
        ];
    }

    private function getCriteriaNode(array $criterias): array
    {
        if (!$criterias) {
            return [];
        }

        $criteriasNode = [];
        /** @var CritereAttribution $criteria */
        foreach ($criterias as $criteria) {
            $criteriasNode[] = [
                'enonce' => $criteria->getEnonce(),
                'ordre' => $criteria->getOrdre(),
                'ponderation' => $criteria->getPonderation(),
                'sousCritereAttribution' => $this->getSubCriteriaNode($criteria->getSousCriteres()),
            ];
        }

        return $criteriasNode;
    }

    private function getSubCriteriaNode(array $subCriterias): array
    {
        if (!$subCriterias) {
            return [];
        }

        $subCriteriasNode = [];
        /** @var SousCritereAttribution $criteria */
        foreach ($subCriterias as $subCriteria) {
            $subCriteriasNode[] = [
                'enonce' => $subCriteria->getEnonce(),
                'ponderation' => $subCriteria->getPonderation(),
            ];
        }

        return $subCriteriasNode;
    }

    private function getCritereReferentiel(?DonneeComplementaire $donneeComplementaire): array
    {
        if (!($idCritere = $donneeComplementaire?->getIdCritereAttribution())) {
            return [];
        }

        $idReferentielCriteres = $this->parameterBag->get('REFERENTIEL_CRITERE_ATTRIBUTION');

        $referentielCritere = $this->em->getRepository(ValeurReferentiel::class)
            ->findOneBy(
                [
                    'id' => $idCritere,
                    'idReferentiel' => $idReferentielCriteres,
                ]
            )
        ;

        if (!$referentielCritere) {
            return [];
        }

        return [
            'id' => $idCritere,
            'idReferentiel' => $idReferentielCriteres,
            'libelle' => $referentielCritere->getLibelleValeurReferentiel(),
        ];
    }
}
