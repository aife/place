<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\Consultation;
use App\Entity\HabilitationAgent;
use App\Repository\AffiliationServiceRepository;
use App\Repository\Consultation\InterneConsultationRepository;
use App\Repository\Consultation\InterneConsultationSuiviSeulRepository;
use App\Repository\HabilitationAgentRepository;
use App\Repository\InvitePermanentTransverseRepository;
use Psr\Log\LoggerInterface;

class Guests
{
    public function __construct(
        private LoggerInterface $logger,
        private InterneConsultationRepository $interneConsultationRepository,
        private InterneConsultationSuiviSeulRepository $consultationSuiviSeulRepository,
        private HabilitationAgentRepository $habilitationAgentRepository,
        private AffiliationServiceRepository $affiliationServiceRepository,
        private InvitePermanentTransverseRepository $invitePermanentTransverseRepository
    ) {
    }

    public function getAllGuestsByConsulation(Consultation $consultation): array
    {
        $idConsultation = $consultation->getId();
        $orgAcronym = $consultation->getOrganisme()->getAcronyme();
        $serviceId = $consultation->getServiceId();

        $agents = [];
        $agents = $this->getInterneConsultation($idConsultation, $orgAcronym, $agents);
        $agents = $this->getInterneConsultationSuiviSeul($idConsultation, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentMonEntite($serviceId, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentEntiteDependante($serviceId, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentTransverse($serviceId, $orgAcronym, $agents);

        return $agents;
    }

    public function getInterneConsultation(?int $idConsultation, string $orgAcronym, array $agents): array
    {
        try {
            $allGuests = $this->interneConsultationRepository
                ->getGuest($idConsultation, $orgAcronym);
            foreach ($allGuests as $g) {
                if (!in_array((int) $g['interne_id'], $agents)) {
                    $agents[] = (int) $g['interne_id'];
                }
            }
        } catch (\Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données sur la table "InterneConsultation". Exception : ' .
                    $exception->__toString());
        }

        return $agents;
    }

    public function getInterneConsultationSuiviSeul(?int $idConsultation, string $orgAcronym, array $agents): array
    {
        try {
            $allGuestsReadOnly = $this->consultationSuiviSeulRepository
                ->getGuest($idConsultation, $orgAcronym);
            foreach ($allGuestsReadOnly as $g) {
                if (!in_array((int) $g['interne_id'], $agents)) {
                    $agents[] = (int) $g['interne_id'];
                }
            }
        } catch (\Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données sur la table "InterneConsultationSuiviSeul". Exception : '
                    . $exception->__toString());
        }

        return $agents;
    }

    protected function getInvitePermanentMonEntite(?int $serviceId, string $orgAcronym, array $agents): array
    {
        $params = [];
        try {
            $params['invitePermanentMonEntite'] = '1';
            $allGuestsMonEntite = $this->habilitationAgentRepository
                ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

            /** @var HabilitationAgent $g */
            foreach ($allGuestsMonEntite as $g) {
                if (!in_array($g->getAgent()->getId(), $agents)) {
                    $agents[] = $g->getAgent()->getId();
                }
            }
        } catch (\Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->__toString());
        }

        return $agents;
    }

    protected function getInvitePermanentEntiteDependante(?int $serviceId, string $orgAcronym, array $agents): array
    {
        $params = [];
        try {
            $affiliationServices = $this->affiliationServiceRepository
                ->getParentByServiceIdAndOrganisme($serviceId, $orgAcronym);
            $tabPole = [];

            foreach ($affiliationServices as $affiliationService) {
                $tabPole[] = $affiliationService->getIdPole()->getId();
            }
            $params['invitePermanentEntiteDependante'] = '1';
            $allGuestsMonEntite = $this->habilitationAgentRepository
                ->retrievePermanentEntiteDependante($orgAcronym, $serviceId, $params, $tabPole);
            if (!is_null($allGuestsMonEntite)) {
                /** @var HabilitationAgent $g */
                foreach ($allGuestsMonEntite as $g) {
                    if (!in_array($g->getAgent()->getId(), $agents)) {
                        $agents[] = $g->getAgent()->getId();
                    }
                }
            }
        } catch (\Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->__toString());
        }

        return $agents;
    }

    /**
         * @param $serviceId
         * @param $orgAcronym
         * @param $agents
         *
         * @return array
         */
    protected function getInvitePermanentTransverse(?int $serviceId, string $orgAcronym, array $agents): array
    {
        try {
            $params = [];
            $params['invitePermanentTransverse'] = '1';
            $allGuestsTransverse = $this->habilitationAgentRepository
                ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

            /** @var HabilitationAgent $g */
            foreach ($allGuestsTransverse as $g) {
                $transverse = null;
                try {
                    $transverse = $this->invitePermanentTransverseRepository->findOneBy([
                        'agent' => $g->getAgent()->getId(),
                        'service' => $serviceId,
                        'acronyme' => $orgAcronym,
                    ]);
                } catch (\Exception $exception) {
                    $this->logger
                        ->error('Problème de récupèration des données. Exception : ' .
                            $exception->__toString());
                }

                if (is_null($transverse)) {
                    continue;
                }

                if (!in_array($g->getAgent()->getId(), $agents)) {
                    $agents[] = $g->getAgent()->getId();
                }
            }
        } catch (\Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->__toString());
        }

        return $agents;
    }
}
