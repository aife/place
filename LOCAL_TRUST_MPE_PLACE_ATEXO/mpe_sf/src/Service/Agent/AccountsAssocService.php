<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Entity\ComptesAgentsAssocies;
use App\Utils\EntityPurchase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AccountsAssocService
{
    public function __construct(private EntityManagerInterface $em, private EntityPurchase $entityPurchase, private ParameterBagInterface $parameter)
    {
    }

    public function listAccountsAssoc(Agent $agentConnected, int $typeAssoc): array
    {
        $accountsAssoc = null;
        $repoCompteAgentAssoc = $this->em->getRepository(ComptesAgentsAssocies::class);

        if (
            $typeAssoc == $this->parameter->get('TYPE_ASSOCIATION_COMPTE_PRINCIPALE')
            || $typeAssoc == $this->parameter->get('TYPE_ASSOCIATION_COMPTE_SECONDAIRE')
        ) {
            $accountsAssoc = $repoCompteAgentAssoc->findComptesAssocies($agentConnected);
        } elseif ($typeAssoc == $this->parameter->get('TYPE_ASSOCIATION_COMPTE_SECONDAIRE_PRINCIPALE')) {
            $accountsAssoc = $repoCompteAgentAssoc->getAllActiveComptesAssocies($agentConnected);
        }

        if (empty($accountsAssoc)) {
            return [];
        }

        $accountsAgents = [];
        $accountAgentConnected = [];
        foreach ($accountsAssoc as $accountAssoc) {
            $agents = $this->em->getRepository(Agent::class)->findListAgentFromAccountAssoc(
                [$accountAssoc->getComptePrincipal(), $accountAssoc->getCompteSecondaire()]
            );
            foreach ($agents as $agent) {
                if ($agentConnected->getId() == $agent->getId()) {
                    $accountAgentConnected[$agent->getId()] = [
                        'agent' => $agent,
                        'sigleLibelleEntity' => $this->entityPurchase->getSigleLibelleEntityById(
                            $agent->getServiceId(),
                            $agent->getOrganisme()->getAcronyme()
                        )
                    ];
                } else {
                    $accountsAgents[$agent->getId()] = [
                        'agent' => $agent,
                        'sigleLibelleEntity' => $this->entityPurchase->getSigleLibelleEntityById(
                            $agent->getServiceId(),
                            $agent->getOrganisme()->getAcronyme()
                        )
                    ];
                }
            }
        }

        return $accountsAgents + $accountAgentConnected;
    }
}
