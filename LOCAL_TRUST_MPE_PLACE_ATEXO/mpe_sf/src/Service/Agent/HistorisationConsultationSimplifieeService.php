<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Service\Crypto\CryptoService;
use App\Entity\Agent\DateFin;
use App\Entity\Agent\HistoriquesConsultation;
use Application\Service\Atexo\Atexo_Util;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class HistorisationConsultationSimplifieeService
{
    public function __construct(
        private CryptoService $atexoCrypto,
        private EntityManagerInterface $entityManager,
        private ParameterBagInterface $parameterBag,
        private AgentInviterService $agentInviterService
    ) {
    }

    public function saveHistory(bool $modification, Consultation $consultation, UserInterface $agent): void
    {
        $this->saveDatesHisto($modification, $consultation, $agent);
        $this->saveInvites($consultation->getId(), $agent);
    }

    private function saveDatesHisto(bool $modification, Consultation $consultation, UserInterface $agent): void
    {
        $date = new DateTime();
        if (
            (strcmp($date->format('Y-m-d H:i:s'), $consultation->getDatefinSad()->format('Y-m-d H:i:s')) != 0)
            || !$modification
        ) {
            $dateTmpFile = $this->parameterBag->get('COMMON_TMP') . "df" . session_id() . time() . "date.tmp";
            $atexoUtil = new Atexo_Util();
            $atexoUtil->write_file(
                $dateTmpFile,
                "Creation de la consultation -"
                . $consultation->getReferenceUtilisateur()
                . "-, date de cloture : "
                . $date->format('Y-m-d H:i:s')
            );
            if (is_file($dateTmpFile)) {
                $arrayTimeStampDateFin = $this->atexoCrypto->timeStampFile($dateTmpFile);
                if ($arrayTimeStampDateFin) {
                    $objetDateFin = $this->createDateFin($consultation, $agent, $arrayTimeStampDateFin, $modification);

                    $this->entityManager->persist($objetDateFin);
                    $this->entityManager->flush();
                }
            }
        }
    }

    private function saveInvites(int $consultationId, UserInterface $agent): void
    {
        $newHistorique = $this->createHistoriquesConsultation($consultationId, $agent);

        $this->entityManager->persist($newHistorique);
        $this->entityManager->flush();
    }

    public function createHistoriquesConsultation(int $consultationId, UserInterface $agent): HistoriquesConsultation
    {
        $listeInvitesIds = $this->agentInviterService->getListAgentInviter(
            $agent->getOrganisme(),
            $agent->getServiceId()
        );
        $listeInvites = $this->entityManager->getRepository(Agent::class)->findBy(['id' => $listeInvitesIds]);
        $detail1 = '';
        foreach ($listeInvites as $invite) {
            $detail1 .= $invite->getId() . '_1,';
        }
        $detail2 = '';
        $arrayTimeStamp = $this->atexoCrypto->timeStampData("1-" . $detail1 . "-" . $detail2);

        $newHistorique = new HistoriquesConsultation();

        $newHistorique->setOrganisme($agent->getOrganisme())
            ->setConsultationId($consultationId)
            ->setAgentId($agent->getId())
            ->setNomAgent($agent->getNom())
            ->setPrenomAgent($agent->getPrenom())
            ->setNomElement($this->parameterBag->get('ID_HISTORIQUE_LISTES_INVITES'))
            ->setValeur("1")
            ->setValeurDetail1($detail1)
            ->setValeurDetail2($detail2)
            ->setNumeroLot("0")
            ->setStatut($this->parameterBag->get('MODIFICATION_FILE'))
            ->setHorodatage($arrayTimeStamp['horodatage'] ?? '')
            ->setUntrusteddate($arrayTimeStamp['untrustedDate'] ?? '');

        return $newHistorique;
    }

    public function createDateFin(
        Consultation $consultation,
        Agent $agent,
        array $arrayTimeStampDateFin,
        bool $modification
    ): DateFin {
        $objetDateFin = new DateFin();

        $objetDateFin->setHorodatage($arrayTimeStampDateFin['horodatage'] ?? '')
            ->setUntrusteddate($arrayTimeStampDateFin['untrustedDate'] ?? '')
            ->setOrganisme($agent->getOrganisme())
            ->setAgentId($agent->getId())
            ->setDatefin($consultation->getDateFin()->format('Y-m-d h:i:s'))
            ->setDateFinLocale($consultation->getDateFin()->format('Y-m-d h:i:s'))
            ->setConsultationId($consultation->getId());

        if ($modification) {
            $objetDateFin->setStatut($this->parameterBag->get('MODIFICATION_FILE'));
        } else {
            $objetDateFin->setStatut($this->parameterBag->get('CREATION_FILE'));
        }

        return $objetDateFin;
    }
}
