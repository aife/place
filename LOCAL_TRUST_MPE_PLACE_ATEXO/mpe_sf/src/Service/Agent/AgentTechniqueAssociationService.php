<?php

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Entity\WS\AgentTechniqueAssociation;
use Doctrine\Persistence\ManagerRegistry;

class AgentTechniqueAssociationService
{
    public function __construct(private readonly ManagerRegistry $managerRegistry)
    {
    }

    public function getByAgent(Agent $agent)
    {
        $repository = $this->managerRegistry->getRepository(AgentTechniqueAssociation::class);
        $agentTechniqueAssociations = $repository->findBy(['idAgent' => $agent->getId()]);

        return $agentTechniqueAssociations;
    }
}
