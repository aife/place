<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\NumerotationRefConsAuto;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Service\AtexoConfiguration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ReferenceTypeService
{
    public function __construct(private EntityManagerInterface $entityManager, private AtexoConfiguration $atexoConfiguration, private ParameterBagInterface $parameterBag)
    {
    }

    /**
     * @throws Exception
     */
    public function setReferenceType(Consultation $consultation, Agent $agent, bool $byConsultation = false): Consultation
    {
        $organisme = $byConsultation ? $consultation->getOrganisme() : $agent->getOrganisme();
        $serviceId = $byConsultation ? $consultation->getService()?->getId() : $agent->getServiceId();
        if ($organisme instanceof Organisme) {
            if ($this->atexoConfiguration->isModuleEnabled('NumerotationRefCons', $organisme, 'get') &&
                empty($consultation->getReferenceUtilisateur())
            ) {
                $consultation->setReferenceUtilisateur($this->generateReferenceutilisateur($organisme, $serviceId));
            }
        }

        return $consultation;
    }

    /**
     * @param $serviceId
     */
    private function generateReferenceutilisateur(Organisme $organisme, $serviceId): string
    {
        $value = $this->parameterBag->get('CONSULTATION_NUMERATION_AUTOMATIQUE');
        $date = substr(date('Y'), 2, 4);
        $referenceName = $date.$value;
        //Numerotaion avec SIGLE et ID service ou organisme.

        if ('1' == $this->parameterBag->get('NUMEROTATION_AUTOMATIQUE_AJOUT_SERVICE_SIGLE')) {
            $sigle = $organisme->getSigle();
            $increment = $this->getNewIncrementConsultation($serviceId, $organisme);
            if ($serviceId) {
                $service = $this->entityManager->getRepository(Service::class)->find($serviceId);
                $sigle = $service ? $service->getSigle() : $organisme->getSigle() ;
            }
            $position = '';
        } elseif ('1' == $this->parameterBag->get('NUMEROTATION_AUTOMATIQUE_AJOUT_SIGLE')) {
            $increment = $this->getNewIncrementConsultation(null, $organisme);
            $sigle = $organisme->getSigle();
            $position = '0';
        } else {
            $increment = $this->getNewIncrementConsultation(null, $organisme);
            $sigle = '';
            $position = '0';
        }

        if ($increment < '10') {
            $increment = $position.'00'.$increment;
        } elseif ($increment < '100') {
            $increment = $position.'0'.$increment;
        } elseif ($increment < '999') {
            $increment = $position.$increment;
        }

        return $sigle.$referenceName.$increment;
    }

    /**
     * @param $serviceId
     * @param $organisme
     */
    private function getNewIncrementConsultation($serviceId, $organisme): int
    {
        $numerotationRefConsAuto = $this->entityManager->getRepository(NumerotationRefConsAuto::class)
            ->findOneBy(['annee' => date('Y'), 'serviceId' => $serviceId, 'organisme' => $organisme]);
        if ($numerotationRefConsAuto instanceof NumerotationRefConsAuto) {
            $numeroRefCons = $numerotationRefConsAuto->getIdConsAuto() + 1;
            $this->entityManager->getRepository(NumerotationRefConsAuto::class)
                ->updateNumerotationRefConsAuto($numeroRefCons, $numerotationRefConsAuto, $serviceId);
        } else {
            $numerotationRefConsAuto = new NumerotationRefConsAuto();
            $numerotationRefConsAuto->setAnnee(date('Y'));
            $numerotationRefConsAuto->setOrganisme($organisme->getAcronyme());
            $numerotationRefConsAuto->setIdConsAuto(1);
            if (null != $serviceId) {
                $numerotationRefConsAuto->setServiceId($serviceId);
            }
            $this->entityManager->persist($numerotationRefConsAuto);
            $this->entityManager->flush();
            $numeroRefCons = $numerotationRefConsAuto->getIdConsAuto();
        }

        return $numeroRefCons;
    }
}
