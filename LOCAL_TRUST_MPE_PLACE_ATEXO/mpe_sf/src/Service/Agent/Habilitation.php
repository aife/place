<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use ArrayObject;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationProfil;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use ReflectionClass;

class Habilitation
{
    public const EXCLUDED_METHODS = [
        'getId',
        'getLibelle',
        'getRecensementProgrammationStrategieAchat'
    ];
    public const PREFIX_GET = 'get';
    public const PREFIX_IS = 'is';
    public const PREFIX_SET = 'set';

    /**
     * Habilitation constructor.
     */
    public function __construct(
        private EntityManagerInterface $em,
        private LoggerInterface $logger
    ) {
    }

    public function checkHabilitation(Agent $agent, string $method): bool
    {
        $asHabilitation = false;

        $habilitation = $this->em
            ->getRepository(HabilitationAgent::class)
            ->findOneBy(['agent' => $agent]);

        if (
            $habilitation instanceof HabilitationAgent
            && method_exists($habilitation, $method)
            && $habilitation->$method()
        ) {
            $asHabilitation = true;
        }

        return $asHabilitation;
    }

    public function createHabilitationsAgent(?string $userName, ?string $profileId): HabilitationAgent
    {
        $habilitations = new HabilitationAgent();

        if ($habilitationProfil = $this->em->getRepository(HabilitationProfil::class)->find((int)$profileId)) {
            $class = new ReflectionClass(HabilitationProfil::class);
            foreach ($class->getMethods() as $method) {
                $methodName = $method->getName();
                if (
                    (
                        str_starts_with($methodName, self::PREFIX_GET)
                        || str_starts_with($methodName, self::PREFIX_IS)
                    )
                    && !in_array($methodName, self::EXCLUDED_METHODS)
                ) {
                    $setter = $this->getSetterMethod($methodName);
                    if (method_exists($habilitations, $setter)) {
                        $habilitations->$setter($habilitationProfil->$methodName());
                    }
                }
            }

            return $habilitations;
        }

        $this->logger->warning(
            sprintf("Habilitation profil not found for user %s and profileId=%s", $userName, $profileId)
        );

        return $habilitations;
    }

    /**
     * On retourne le nom de la méthode de setter en vérifiant si le getter commence par get ou is
     */
    private function getSetterMethod(string $methodName): ?string
    {
        $getterPrefix = str_starts_with($methodName, self::PREFIX_GET) ? self::PREFIX_GET : self::PREFIX_IS;

        $pos = strpos($methodName, $getterPrefix);
        if ($pos !== false) {
            return substr_replace($methodName, self::PREFIX_SET, $pos, strlen($getterPrefix));
        }

        return null;
    }

    public function createOrUpdateHabilitationAgentFromProfil(
        HabilitationProfil $profil,
        ?int $idAgent = null
    ): HabilitationAgent {
        $habilitation = null;
        if ($idAgent) {
            $habilitation = $this->em->getRepository(HabilitationAgent::class)->find($idAgent);
        }

        if (null === $idAgent || null === $habilitation) {
            $habilitation = new HabilitationAgent();
        }
        $arrayProfil = new ArrayObject((array) $profil);
        foreach ($arrayProfil as $key => $value) {
            $key = str_replace(HabilitationProfil::class, '', $key);
            if ('libelle' == $key || 'id' == $key) {
                break;
            }
            $fct = ucfirst(ltrim($key));
            $set = 'set' . $fct;
            if (method_exists($habilitation, $set)) {
                $habilitation->$set($value);
            }
        }

        return $habilitation;
    }
}
