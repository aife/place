<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Service\Publicite\EchangeConcentrateur;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class ValidationEcoSip
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EchangeConcentrateur $concentrateur,
        private readonly RequestStack $requestStack
    ) {
    }

    public function getConnectionParams(string $page, Agent $agent, string $typeValidation): array
    {
        $token = $this->concentrateur->getToken(typeValidation: $typeValidation);

        $this->concentrateur->addAgent($token, $agent);

        $org = $agent->getOrganisme()->getAcronyme();

        $uidPfMpe = $this->parameterBag->get('UID_PF_MPE');
        $lang = $this->requestStack->getMainRequest()->getLocale();

        return [
            'token' => $token,
            'org' => $org,
            'page' => $page,
            'uidPfMpe' => $uidPfMpe,
            'lang' => $lang,
            'isFullWidth' => true,
        ];
    }
}
