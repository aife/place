<?php

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Entity\Agent\NotificationAgent;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class AgentNotificationService
{
    public function __construct(private EntityManagerInterface $em)
    {
    }

    public function countUnread(Agent $agent): int
    {
        return $this->em->getRepository(NotificationAgent::class)->countUnread($agent);
    }

    public function listUnread(Agent $agent): array
    {
        return $this->em->getRepository(NotificationAgent::class)->listUnread($agent);
    }

    public function listRead(NotificationAgent $notification)
    {
        $notification->setNotificationLue(NotificationAgent::NOTIFICATION_READ)
            ->setDateLecture(new DateTime());
        $this->em->persist($notification);
        $this->em->flush();
    }

    public function disableNotification(NotificationAgent $notification)
    {
        $notification->setNotificationActif(NotificationAgent::NOTIFICATION_DISABLE);
        $this->em->persist($notification);
        $this->em->flush();
    }
}
