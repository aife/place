<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Doctrine\Extension\ConsultationIdExtension;
use App\Service\Consultation\ConsultationStatus;
use App\Service\Consultation\SearchAgentService;
use App\Service\CurrentUser;
use App\Service\WebservicesMpeConsultations;
use JsonException;
use ReflectionException;

class AccueilService
{
    /**
     * AccueilController constructor.
     */
    public function __construct(
        private SearchAgentService $searchAgentService,
        private WebservicesMpeConsultations $wsConsultations,
    ) {
    }

    public function checkHabilitationForMenu(CurrentUser $currentUser): bool
    {
        return $currentUser->checkHabilitation('gererMapaInferieurMontant')
            || $currentUser->checkHabilitation('gererMapaSuperieurMontant')
            || $currentUser->checkHabilitation('creerConsultation')
            || $currentUser->checkHabilitation('creerConsultationTransverse');
    }

    /**
     * @return array
     * @throws JsonException
     * @throws ReflectionException
     */
    public function getStatistics(): array
    {
        $data = [
            'groupBy' => 'statutCalcule',
            'pagination' => false,
            ConsultationIdExtension::IDS_ONLY => 1,
        ];
        $wsResponse = $this->wsConsultations->getContent('searchConsultations', $data);
        return $this->searchAgentService->getDataWidgetsStats($wsResponse, $data);
    }
}
