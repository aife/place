<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\HabilitationAgent;
use App\Entity\HabilitationProfil;
use App\Entity\HabilitationProfilHabilitation;
use App\Entity\HabilitationTypeProcedure;
use App\Entity\Agent;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ReferentielHabilitation;
use App\Enum\Habilitation\HabilitationSlug;
use App\Repository\AgentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\Attribute\Autoconfigure;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

#[Autoconfigure(public: true)]
class HabilitationTypeProcedureService
{
    public function __construct(
        private EntityManagerInterface $em,
        private ParameterBagInterface $parameterBag,
        private AgentRepository $agentRepository
    ) {
    }

    public function checkHabilitation(
        Agent $agent,
        string $slug,
        ?int $idTypeProcedure = null,
        ?string $organisme = null,
    ): bool {
        $hasHabilitation = false;

        $habilitations = $this->getHabilitationsBySlug(
            $agent,
            $slug,
            $idTypeProcedure
        );

        if (!empty($habilitations)) {
            $hasHabilitation = true;
        }

        return $hasHabilitation;
    }

    public function getHabilitation(
        Agent $agent
    ): array {
        return $this->em
            ->getRepository(HabilitationTypeProcedure::class)
            ->findBy(['agent' => $agent]);
    }

    public function getHabilitationsBySlug(
        Agent $agent,
        string $slug,
        ?int $typeProcedureId = null
    ): array {
        return $this->em
            ->getRepository(HabilitationTypeProcedure::class)
            ->findByAgentAndSlug(
                $agent,
                $slug,
                $typeProcedureId
            );
    }

    public function createHabilitation(
        Agent $agent,
        string $slug,
        ?int $typeProcedureId = null,
        bool $flush = false
    ): HabilitationTypeProcedure {
        $habilitation = $this->em->getRepository(ReferentielHabilitation::class)->findOneBy(['slug' => $slug]);

        $habilitationTypeProcedure = new HabilitationTypeProcedure();
        $habilitationTypeProcedure
            ->setAgent($agent)
            ->setHabilitation($habilitation);

        if (!empty($typeProcedureId)) {
            $habilitationTypeProcedure
                ->setTypeProcedure($typeProcedureId)
                ->setOrganism($agent->getOrganisme());
        }

        $this->em->persist($habilitationTypeProcedure);

        if ($flush) {
            $this->em->flush();
        }

        return $habilitationTypeProcedure;
    }

    public function deleteHabilitationsBySlug($agent, string $habilitationSlug, bool $flush = false)
    {
        $habilitations = $this->getHabilitationsBySlug($agent, $habilitationSlug);

        foreach ($habilitations as $habilitation) {
            $this->em->remove($habilitation);
        }

        if ($flush) {
            $this->em->flush();
        }
    }

    public function isHabilitationManaged(string $slug): bool
    {
        $habilitation = $this->em->getRepository(ReferentielHabilitation::class)->findOneBy(['slug' => $slug]);

        if (empty($habilitation)) {
            return false;
        }

        return $habilitation->isActive();
    }

    public function getCreerConsultationHabilitations(Agent $agent): array
    {
        $habilitationMapaInf = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT()
        );
        $habilitationMapaSup = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT()
        );
        $habilitationFormalisee = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES()
        );

        return array_merge($habilitationMapaInf, $habilitationMapaSup, $habilitationFormalisee);
    }

    public function getCreerSuiteConsultationHabilitations(Agent $agent): array
    {
        $habilitationMapaInf = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE()
        );
        $habilitationMapaSup = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE()
        );
        $habilitationFormalisee = $this->getHabilitationsBySlug(
            $agent,
            HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE()
        );

        return array_merge($habilitationMapaInf, $habilitationMapaSup, $habilitationFormalisee);
    }

    public function getCreerConsultationIdTypeProcedures(Agent $agent): array
    {
        $habilitations = $this->getCreerConsultationHabilitations($agent);

        return array_map(
            function (HabilitationTypeProcedure $habilitation) {
                return $habilitation->getTypeProcedure();
            },
            $habilitations
        );
    }

    public function getCreerSuiteConsultationIdTypeProcedures(Agent $agent)
    {
        $habilitations = $this->getCreerSuiteConsultationHabilitations($agent);

        return array_map(
            function (HabilitationTypeProcedure $habilitation) {
                return $habilitation->getTypeProcedure();
            },
            $habilitations
        );
    }


    public function getAllCreerConsultationIdTypeProcedures(Agent $agent): array
    {
        $habilitations = array_merge(
            $this->getCreerConsultationHabilitations($agent),
            $this->getCreerSuiteConsultationHabilitations($agent)
        );

        return array_map(
            function (HabilitationTypeProcedure $habilitation) {
                return $habilitation->getTypeProcedure();
            },
            $habilitations
        );
    }

    public function setTypeProcedureFromAgentServiceMetier(int $agentId): void
    {
        $agent = $this->em->getRepository(Agent::class)
            ->findOneBy(['id' => $agentId]);
        $habilitationProfil = $this->em->getRepository(HabilitationProfil::class)
            ->retrieveAgentProfile($agentId);

        if (!empty($habilitationProfil)) {
            $habilitationReferentiels = $this->em->getRepository(ReferentielHabilitation::class)->findAll();
            foreach ($habilitationReferentiels as $referentiel) {
                $habilitationAgent = $this->em->getRepository(HabilitationAgent::class)
                    ->findOneBy(['agent' => $agent]);
                $this->updateHabilitationAgent($habilitationAgent, $referentiel);
            }
        }
    }

    public function updateHabilitationAgent(
        HabilitationAgent $habilitationAgent,
        ReferentielHabilitation $referentiel
    ) {
        $referentielId = $referentiel->getId();
        $habilitation = $referentiel->getSlug();
        $isActive = false;
        $simpilfieMapa = 0;
        $methodName = str_replace('_', '', ucwords($habilitation, '_'));
        if (method_exists($habilitationAgent, 'get' . $methodName)) {
            $isActive = $habilitationAgent->{'get' . $methodName}();
        } elseif (method_exists($habilitationAgent, 'is' . $methodName)) {
            $isActive = $habilitationAgent->{'is' . $methodName}();
        }
        if (
            in_array(
                $habilitation,
                [
                    HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE->value,
                    HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE->value,
                    HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE->value
                ]
            )
        ) {
            $isActive = $habilitationAgent->getCreerSuiteConsultation();
        }

        if (HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES->value === $habilitation) {
            if ($habilitationAgent->getGererMapaSuperieurMontant()) {
                $isActive = true;
                $simpilfieMapa += 2;
            }

            if ($habilitationAgent->getGererMapaInferieurMontant()) {
                $isActive = true;
                $simpilfieMapa += 1;
            }
        }

        if ($isActive) {
            if (
                in_array(
                    $habilitation,
                    [
                        HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT->value,
                        HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE->value,
                    ]
                )
            ) {//MAPA > 90
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'mapa' => 1,
                            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_SUP_90'),
                            'organisme' => $habilitationAgent->getAgent()
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitationWithTypeProcedure(
                    $agentTypeProcedures,
                    $referentielId,
                    $habilitationAgent
                );
            } elseif (
                in_array(
                    $habilitation,
                    [
                        HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT->value,
                        HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE->value,
                    ]
                )
            ) {//MAPA < 90
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'mapa'            => '1',
                            'idMontantMapa'   => $this->parameterBag->get('MONTANT_MAPA_INF_90'),
                            'organisme'       => $habilitationAgent->getAgent()
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitationWithTypeProcedure(
                    $agentTypeProcedures,
                    $referentielId,
                    $habilitationAgent
                );
            } elseif (
                in_array(
                    $habilitation,
                    [
                    HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES->value,
                    HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE->value,
                    ]
                )
            ) {
                //procédures formalisées
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'activerMapa'   => '1',
                            'organisme'     => $habilitationAgent->getAgent()
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitationWithTypeProcedure(
                    $agentTypeProcedures,
                    $referentielId,
                    $habilitationAgent
                );
            } elseif (HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES->value === $habilitation) {
                $criteria = [
                    'procedureSimplifie' => '1',
                    'organisme' => $habilitationAgent->getAgent()
                        ?->getOrganisme()
                        ?->getAcronyme()
                ];
                if ($simpilfieMapa > 0 && $simpilfieMapa < 3) {
                    $criteria['idMontantMapa'] = $simpilfieMapa;
                }
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy($criteria);
                $this->saveHabilitationWithTypeProcedure(
                    $agentTypeProcedures,
                    $referentielId,
                    $habilitationAgent
                );
            } else {
                $habilitationProcedure = new HabilitationTypeProcedure();
                $habilitationProcedure->setHabilitation($referentiel)
                    ->setAgent($habilitationAgent->getAgent());
                $this->em->persist($habilitationProcedure);
            }
        }
    }

    protected function saveHabilitationWithTypeProcedure(
        array $agentTypeProcedures,
        int $referentielId,
        HabilitationAgent $habilitationAgent
    ) {
        $referentiel = $this->em->getRepository(ReferentielHabilitation::class)
            ->find($referentielId);
        foreach ($agentTypeProcedures as $agentTypeProcedure) {
            $habilitationProcedure = new HabilitationTypeProcedure();
            $habilitationProcedure->setHabilitation($referentiel)
                ->setTypeProcedure($agentTypeProcedure->getIdTypeProcedure())
                ->setOrganism($habilitationAgent->getAgent()?->getOrganisme())
                ->setAgent($habilitationAgent->getAgent());
            $this->em->persist($habilitationProcedure);
        }
        $this->em->flush();
    }

    public function createHabilitationsByProfile(int $agentId, int $profileId)
    {
        $agent = $this->em->getRepository(Agent::class)->find($agentId);

        $habilitationProfil = $this->em
            ->getRepository(HabilitationProfil::class)
            ->find($profileId);

        $habilitationProfilHabilitations = $this->em
            ->getRepository(HabilitationProfilHabilitation::class)
            ->findBy(['habilitationProfil' => $habilitationProfil]);

        foreach ($habilitationProfilHabilitations as $habilitationProfilHabilitation) {
            $habilitation = $habilitationProfilHabilitation->getHabilitation();
            $habilitationSlug = $habilitation->getSlug();
            if ($habilitationSlug === HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT()) {//MAPA > 90
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'mapa' => 1,
                            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_SUP_90'),
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitation(
                    $agentTypeProcedures,
                    $habilitationSlug,
                    $agent
                );
            } elseif ($habilitationSlug === HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT()) {//MAPA < 90
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'mapa' => '1',
                            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_INF_90'),
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitation(
                    $agentTypeProcedures,
                    $habilitationSlug,
                    $agent
                );
            } elseif ($habilitationSlug === HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES()) {
                //procédures formalisées
                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy(
                        [
                            'activerMapa' => '1',
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ]
                    );
                $this->saveHabilitation(
                    $agentTypeProcedures,
                    $habilitationSlug,
                    $agent
                );
            } elseif (HabilitationSlug::ADMINISTRER_PROCEDURES_SIMPLIFIEES() === $habilitationSlug) {
                $criteria = [
                    'procedureSimplifie' => '1',
                    'organisme' => $agent
                        ?->getOrganisme()
                        ?->getAcronyme(),
                ];

                $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                    ->findBy($criteria);
                $this->saveHabilitation(
                    $agentTypeProcedures,
                    $habilitationSlug,
                    $agent
                );
            } elseif (HabilitationSlug::CREER_SUITE_CONSULTATION() === $habilitationSlug) {
                $slugsCreerSuiteConsultation = HabilitationSlug::creerSuiteConsultationSection();

                foreach ($slugsCreerSuiteConsultation as $suiteSlug) {
                    $criteria = [];
                    if ($suiteSlug === HabilitationSlug::CREER_SUITE_CONSULTATION()) {
                        $habilitationTypeProcedure = new HabilitationTypeProcedure();
                        $habilitationTypeProcedure
                            ->setAgent($agent)
                            ->setHabilitation($habilitation);

                        $this->em->persist($habilitationTypeProcedure);
                        continue;
                    } elseif ($suiteSlug === HabilitationSlug::GERER_MAPA_SUPERIEUR_MONTANT_SUITE()) {
                        $criteria = [
                            'mapa' => 1,
                            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_SUP_90'),
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ];
                    } elseif ($suiteSlug === HabilitationSlug::GERER_MAPA_INFERIEUR_MONTANT_SUITE()) {
                        $criteria = [
                            'mapa' => '1',
                            'idMontantMapa' => $this->parameterBag->get('MONTANT_MAPA_INF_90'),
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ];
                    } elseif ($suiteSlug === HabilitationSlug::ADMINISTRER_PROCEDURES_FORMALISEES_SUITE()) {
                        $criteria = [
                            'activerMapa' => '1',
                            'organisme' => $agent
                                ?->getOrganisme()
                                ?->getAcronyme(),
                        ];
                    }

                    $agentTypeProcedures = $this->em->getRepository(TypeProcedureOrganisme::class)
                        ->findBy(
                            $criteria
                        );

                    $this->saveHabilitation(
                        $agentTypeProcedures,
                        $suiteSlug,
                        $agent
                    );
                }
            } else {
                $habilitationTypeProcedure = new HabilitationTypeProcedure();
                $habilitationTypeProcedure
                    ->setAgent($agent)
                    ->setHabilitation($habilitation);

                $this->em->persist($habilitationTypeProcedure);
            }
        }

        $this->em->flush();
    }

    public function checkHabilitationForPrado(int $idAgent, string $slug): bool
    {
        $agent = $this->agentRepository->find($idAgent);
        return false === empty($this->getHabilitationsBySlug(
            $agent,
            $slug
        ));
    }

    protected function saveHabilitation(
        array $typeProcedureOrganismes,
        string $slug,
        Agent $agent
    ) {
        $referentiel = $this->em->getRepository(ReferentielHabilitation::class)
            ->findOneBy(['slug' => $slug]);
        foreach ($typeProcedureOrganismes as $typeProcedureOrganisme) {
            $habilitationProcedure = new HabilitationTypeProcedure();
            $habilitationProcedure->setHabilitation($referentiel)
                ->setTypeProcedure($typeProcedureOrganisme->getIdTypeProcedure())
                ->setOrganism($agent?->getOrganisme())
                ->setAgent($agent);
            $this->em->persist($habilitationProcedure);
        }
    }
}
