<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use Exception;
use InvalidArgumentException;
use DateTime;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Service\AtexoFichierOrganisme;
use App\Utils\Filesystem\MountManager;
use Atexo\CryptoBundle\AtexoCrypto;
use AtexoCrypto\Dto\InfosHorodatage;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class DceService
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly EntityManagerInterface $em,
        private readonly MountManager $mountManager,
        private readonly AtexoCrypto $cryptoService,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws Exception
     */
    public function createDce(string $filePath, Consultation $consultation, Agent $agent): DCE
    {
        $fileSystem = $this->mountManager->getFilesystem('common_tmp');
        $fileSystemPath = str_replace($this->parameterBag->get('COMMON_TMP'), '/', $filePath);

        if (!$fileSystem->has($fileSystemPath) || !file_exists($filePath)) {
            throw new InvalidArgumentException('Fichier non trouvé : ' . $filePath);
        }

        $fileName = basename($filePath);
        $fileSize = $fileSystem->getSize($fileSystemPath);

        $fileHash = hash_file('sha256', $filePath);
        /** @var InfosHorodatage $horodatage */
        $horodatage = $this->cryptoService->demandeHorodatage($fileHash);

        $movedFilePath = $this->atexoBlobOrganisme->moveTmpFile($filePath);

        if (false === $movedFilePath) {
            $this->logger->error('Impossible de déplacer le fichier ' . $filePath);

            throw new Exception('Impossible de déplacer le fichier ' . $filePath);
        }

        $idBlob = $this->atexoBlobOrganisme->insertFile(
            $fileName,
            $fileSystemPath,
            $consultation->getOrganisme()->getAcronyme(),
            null,
            $this->parameterBag->get('EXTENSION_DCE')
        );

        $dce = new DCE();

        $dce->setAgentId($agent->getId());
        $dce->setConsultation($consultation);
        $dce->setDce($idBlob);

        $dce->setHorodatage($horodatage->getJetonHorodatageBase64());
        $dce->setUntrusteddate((new DateTime())->setTimestamp($horodatage->getHorodatage()));

        $dce->setOrganisme($consultation->getOrganisme());
        $dce->setStatus($this->parameterBag->get('AJOUT_FILE'));
        $dce->setNomDce($fileName);
        $dce->setNomFichier($fileName);
        $dce->setTailleDce($fileSize);

        $this->em->persist($dce);
        $this->em->flush();

        return $dce;
    }
}
