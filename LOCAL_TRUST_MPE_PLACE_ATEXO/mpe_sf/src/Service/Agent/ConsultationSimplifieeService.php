<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\Agent;
use App\Entity\AvisType;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Consultation;
use App\Entity\DCE;
use App\Entity\GeolocalisationN2;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\ProcedureEquivalence;
use App\Service\Consultation\ConsultationService;
use App\Service\Handler\EmailAlerteAgentValidationHandler;
use App\Service\Procedure\ProcedureEquivalenceAgentService;
use App\Utils\Utils;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ConsultationSimplifieeService
{
    public const PHASE_BROUILLON = 1;
    public const PHASE_VALIDATION = 2;
    public const PHASE_EN_LIGNE = 3;

    public function __construct(
        private ProcedureEquivalenceAgentService $equivalenceAgentService,
        private DceService $dceService,
        private EntityManagerInterface $entityManager,
        private ReferenceTypeService $referenceTypeService,
        private ParameterBagInterface $parameterBag,
        private SessionInterface $session,
        private EmailAlerteAgentValidationHandler $emailAgentHandler,
        private LoggerInterface $logger,
        private ConsultationService $consultationService
    ) {
    }

    /**
     * Permet de récupérer la liste des lieux d'exécution (session ou favoris de l'agent).
     *
     * @throws Exception
     */
    public function getSelectedGeoN2(Agent $agent): array
    {
        $lieux = $this->session->get('lieux') ?: $agent->getLieuExecution();
        return [
            'ids' => $lieux,
            'list' => $this->idsToGeoN2($lieux),
        ];
    }

    /**
     * @throws Exception
     */
    public function idsToGeoN2(?string $lieuxIds): array
    {
        return (new Utils())->transformeDataLieuxExecutionIntoArray(
            $this->entityManager
                ->getRepository(GeolocalisationN2::class)
                ->getLieuxExecution(explode(',', $lieuxIds))
        );
    }

    /**
     * @throws Exception
     */
    public function initConsultation(Agent $agent): Consultation
    {
        $consultation = new Consultation();

        // "Annonce de consultation"
        $avisType = $this->entityManager->getRepository(AvisType::class)->findOneBy(
            [
                'abbreviation' => 'AAPC',
            ]
        );
        $consultation->setAvisType($avisType);

        // Heure de la "date limite de remise des plis"
        /** @var ConfigurationOrganisme $configurationOrganime */
        $configurationOrganime = $this->entityManager
            ->getRepository(ConfigurationOrganisme::class)
            ->getByAcronyme($agent->getAcronymeOrganisme());
        $consultation->setDatefinHeure(
            new DateTime('0000-00-00 ' . $configurationOrganime->isHeureLimiteDeRemiseDePlisParDefaut())
        );

        return $consultation;
    }

    public function getTypeAccesFromTypeProcedure(int $idTypeProcedure, string $organisme): array
    {
        $procedureEquivalence = $this->entityManager->getRepository(ProcedureEquivalence::class)->findOneBy(
            [
                'idTypeProcedure' => $idTypeProcedure,
                'organisme' => $organisme,
            ]
        );

        $typeAcces = Consultation::TYPE_ACCES_PUBLIC;
        if ($this->equivalenceAgentService->getActivate($procedureEquivalence->getProcedureRestreinte())) {
            $typeAcces = Consultation::TYPE_ACCES_RESTREINT;
        }

        return [
            'type_acces' => $typeAcces,
            'procedure_public' => $procedureEquivalence->getProcedurePublicite(),
            'procedure_restreint' => $procedureEquivalence->getProcedureRestreinte()
        ];
    }

    public function getDce(Consultation $consultation): ?DCE
    {
        return $this->entityManager->getRepository(DCE::class)->findOneBy(
            ['consultationId' => $consultation->getId()],
            ['id' => 'DESC']
        );
    }

    public function getDceDownloadURL(Consultation $consultation): string
    {
        return $this->parameterBag->get('PF_URL')
            . 'index.php?page=Agent.DownloadDce&id='
            . base64_encode($consultation->getId());
    }

    /**
     * @throws Exception
     */
    public function initDefaultValues(Consultation $consultation, Agent $agent)
    {
        $consultation = $this->setConfirmationDefaultValues(
            $consultation,
            $agent
        );
        $this->entityManager->persist($consultation);
        $this->consultationService->setFavorisForInitialization($consultation, $agent);
        $this->entityManager->flush();
    }

    public function createDce(?string $filePath, Consultation $consultation, Agent $agent): bool
    {
        if (empty($filePath)) {
            return true;
        }
        try {
            $uploadedDceFilePath = $this->getDceFullFilePath($filePath);
            $this->addDce($uploadedDceFilePath, $consultation, $agent);
        } catch (Exception $ex) {
            $this->logger->error($ex);

            return false;
        }

        return true;
    }

    public function getUploadedDce(?string $filePath): ?DCE
    {
        if (empty($filePath) || !file_exists($this->getDceFullFilePath($filePath))) {
            return null;
        }
        $uploadedDce = new DCE();
        $uploadedDce->setNomDce(basename($filePath));
        $uploadedDce->setTailleDce(filesize($this->getDceFullFilePath($filePath)));

        return $uploadedDce;
    }

    public function sendMailAlert(Consultation $consultation, string $locale): Consultation
    {
        $optionValidation = true;
        $typeValidation = (string) $consultation->getIdRegleValidation();
        if (
            $typeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_1')
            || $typeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_2')
            || $typeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')
        ) {
            $service = $consultation->getServiceValidation();
        } else {
            $service = $consultation->getServiceValidationIntermediaire();
            $optionValidation = false;
        }

        $this->emailAgentHandler->sendMailToAgentsAlertConsAttenteValidation(
            $consultation,
            $service,
            $typeValidation,
            $optionValidation,
            $locale
        );

        return $consultation;
    }

    /**
     * Date limite de remise des plis (date de fin) à partir des 2 champs
     *
     * @throws Exception
     */
    public function setDateFinFromDateAndHour(FormInterface $form, Consultation $consultation): Consultation
    {
        return $consultation->setDatefin(
            new DateTime(
                $form->get('datefinDate')->getViewData()
                . ' '
                . $form->get('datefinHeure')->getViewData()
            )
        );
    }

    /**
     * @throws Exception
     */
    private function setConfirmationDefaultValues(Consultation $consultation, Agent $agent): Consultation
    {
        if (null === $consultation->getChampSuppInvisible()) {
            $consultation->setChampSuppInvisible('');
        }

        /*
         * @todo faire une comparaison avec le fichier :
         * mpe_sf/legacy/protected/Controls/IdentificationConsultation.php
         * Ligne :
         */
        $consultation->setNumeroPhase(self::PHASE_BROUILLON)
            ->setEtatEnAttenteValidation('0');

        //check on reference if it is automatic then set it
        $consultation = $this->referenceTypeService->setReferenceType($consultation, $agent);
        $consultation = $this->setInitValueForAgent($consultation, $agent);
        $consultation = $this->consultationService->setMessecForInitialization($consultation);
        $consultation = $this->consultationService->setPFVForInitialization($consultation);
        $consultation = $this->consultationService->setEmptyValuesForInitialization($consultation);

        /*
         * @todo : doublons entité / Doublons BDD !
         */
        $consultation->setProcedureType($consultation->getTypeProcedureOrganisme()->getProcedurePortailType());
        $consultation = $this->consultationService->setDatesForInitialization($consultation);

        /*
         * La consultation est en phase acces restreint
         */
        if ($consultation->getCodeProcedure() && Consultation::TYPE_ACCES_RESTREINT === $consultation->getTypeAcces()) {
            $consultation->setNumProcedure(Consultation::TYPE_ACCES_RESTREINT);
        } else {
            $consultation->setCodeProcedure(null);
        }

        /*
         * Id_affaire = Champs vide pour la consultation Simplifiée
         */

        $consultation->setEnvolActivation(false);

        return $this->setElementProcedureEquivalence($consultation);
    }

    /**
     * @throws Exception
     */
    private function addDce(string $uploadedDceFilePath, Consultation $consultation, Agent $agent): void
    {
        $this->dceService->createDce($uploadedDceFilePath, $consultation, $agent);
    }

    private function setElementProcedureEquivalence(Consultation $consultation): Consultation
    {
        $procedureEquivalence = $this->entityManager->getRepository(ProcedureEquivalence::class)->findOneBy(
            [
                'idTypeProcedure' => $consultation->getIdTypeProcedure(),
                'organisme' => $consultation->getOrganisme(),
            ]
        );

        $attributes = [
            'EnvCandidature',
            'EnvOffre',
            'EnvAnonymat',
        ];

        foreach ($attributes as $attribute) {
            $consultation = $this->equivalenceAgentService->setValueProcedure(
                $attribute,
                $consultation,
                $procedureEquivalence
            );
        }

        //set de l'acte d'engagement
        $consultation->setSignatureActeEngagement(
            $this->equivalenceAgentService->getActivate($procedureEquivalence->getSignaturePropre())
        );

        //Set signature offre
        /**
         * SignatureEnabled = Requise.
         */
        $signatureOffre = 0;
        if ($this->equivalenceAgentService->getActivate($procedureEquivalence->getSignatureEnabled())) {
            $signatureOffre = 1;
        }
        /*
         * SignatureAutoriser = Autorisé
         */
        if ($this->equivalenceAgentService->getActivate($procedureEquivalence->getSignatureAutoriser())) {
            $signatureOffre = 2;
        }
        $consultation->setSignatureOffre($signatureOffre);

        //Set Chiffre offre
        $consultation->setChiffrementOffre(
            $this->equivalenceAgentService->getActivate($procedureEquivalence->getCipherEnabled())
        );

        //set mode d'ouverture
        $consultation->setModeOuvertureReponse(
            $this->equivalenceAgentService->getActivate($procedureEquivalence->getModeOuvertureDossier())
        );

        /**
         * Gestion de la mise en ligne.
         */
        $tabMiseEnLigne = [
            1 => 'MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE',
            2 => 'MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE',
            3 => 'MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP',
            4 => 'MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP',
        ];

        $parameter = $tabMiseEnLigne[1];
        for ($i = 1; $i <= 4; ++$i) {
            $getter = 'getMiseEnLigne' . $i;
            $miseenligne = $this->equivalenceAgentService->getActivate($procedureEquivalence->$getter());
            if (0 != $miseenligne) {
                $parameter = $tabMiseEnLigne[$i];
            }
        }

        $validation = $this->parameterBag->get($parameter);
        $consultation->setRegleMiseEnLigne($validation);

        return $consultation;
    }

    private function getDceFullFilePath(string $filePath): string
    {
        return $this->parameterBag->get('tus_dce_upload_directory') . '/' . $filePath;
    }

    private function setInitValueForAgent(Consultation $consultation, Agent $agent): Consultation
    {
        /**
         * Liste des valeurs de la consultation par défaut.
         */
        $agentServiceId = !empty($agent->getService()) ? $agent->getService()->getId() : null;
        $consultation->setServiceId($agentServiceId)
            ->setService($agent->getService())
            ->setServiceAssocieId($agentServiceId)
            ->setOrganisme($agent->getOrganisme())
            ->setOrgDenomination($agent->getOrganisme()->getDenominationOrg())
            ->setAcronymeOrg($agent->getOrganisme()->getAcronyme())
            ->setIdCreateur($agent->getId())
            ->setNomCreateur($agent->getNom())
            ->setPrenomCreateur($agent->getPrenom())
            ->setIdRpa(null)
            ->setIdTypeProcedure($consultation->getTypeProcedureOrganisme()->getIdTypeProcedure());

        return $this->setIdServiceValidation($consultation, $agent);
    }

    private function setIdServiceValidation(Consultation $consultation, Agent $agent): Consultation
    {
        $idServiceValidation = $agent->getServiceId();
        $proceduresType = $consultation->getTypeProcedureOrganisme();
        $idTypeValidation = (string) $proceduresType->getIdTypeValidation();
        if ($proceduresType instanceof TypeProcedureOrganisme) {
            $consultation->setIdRegleValidation($proceduresType->getIdTypeValidation());
            if (
                $idTypeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_3')
                || $idTypeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_4')
            ) {
                $consultation->setServiceValidation($proceduresType->getServiceValidation());
                if ($idTypeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_4')) {
                    $consultation->setServiceValidationIntermediaire($idServiceValidation);
                }
            } else {
                $consultation->setServiceValidation($idServiceValidation);
                if ($idTypeValidation === $this->parameterBag->get('REGLE_VALIDATION_TYPE_5')) {
                    $consultation->setServiceValidationIntermediaire($idServiceValidation);
                }
            }
        }

        return $consultation;
    }
}
