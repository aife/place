<?php

namespace App\Service\Agent;

use Exception;
use DateTime;
use App\Entity\Agent\AgentServiceMetier;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AgentServiceMetierService
{
    /**
     * OrganismeService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(private ContainerInterface $container, private EntityManagerInterface $em)
    {
    }

    /**
     * @param $idAgent
     * @param $idServiceMetier
     * @param $idProfilService
     *
     * @return AgentServiceMetier
     *
     * @throws Exception
     */
    public function createForAdmin($idAgent, $idServiceMetier, $idProfilService)
    {
        try {
            return self::createAgentServiceMetier($idAgent, $idServiceMetier, $idProfilService);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $idAgent
     * @param $idServiceMetier
     * @param $idProfilService
     *
     * @return AgentServiceMetier
     *
     * @throws Exception
     */
    public function createAgentServiceMetier($idAgent, $idServiceMetier, $idProfilService)
    {
        try {
            $agentServiceMetier = new AgentServiceMetier();
            $agentServiceMetier->setIdAgent($idAgent);
            $agentServiceMetier->setIdServiceMetier($idServiceMetier);
            $agentServiceMetier->setIdProfilService($idProfilService);
            $dateTimeNow = new DateTime();
            $date = $dateTimeNow->format('Y-m-d H:i:s');
            $agentServiceMetier->setDateCreation($date);
            $agentServiceMetier->setDateModification($date);
            $this->em->persist($agentServiceMetier);
            $this->em->flush();

            return $agentServiceMetier;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $idProfilService
     *
     * @return AgentServiceMetier
     *
     * @throws Exception
     */
    public function modifyIdProfilAgentServiceMetier(AgentServiceMetier $agentServiceMetier, $idProfilService)
    {
        try {
            $agentServiceMetier->setIdProfilService($idProfilService);
            $dateTimeNow = new DateTime();
            $date = $dateTimeNow->format('Y-m-d H:i:s');
            $agentServiceMetier->setDateModification($date);
            $this->em->persist($agentServiceMetier);
            $this->em->flush();

            return $agentServiceMetier;
        } catch (Exception $e) {
            throw $e;
        }
    }
}
