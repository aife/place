<?php

namespace App\Service\Agent;

use App\Attribute\ResultType;
use DOMDocument;
use DOMAttr;
use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Service\AbstractService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class StatisticsService extends AbstractService
{
    private const URL_IDENTITE          = '/auth/realms/statistiques/protocol/openid-connect/token';
    private const STATS_BI_AGENT_URL    = '/stats-api/agents';
    private const REPORTS_URL           = '/stats-api/reports/';
    private const CLIENT_ID             = 'api';
    public const FREE_CODE              = 'FREE';
    public const PREMIUM_CODE           = 'PREMIUM';
    public const TIMEOUT                = 10;


    /**
     * StatisticsService constructor.
     */
    public function __construct(
        private ParameterBagInterface $parameterBag,
        private HttpClientInterface $httpClient,
        private Habilitation $habilitationService,
        private EntityManagerInterface $em,
        private SessionInterface $session,
        private LoggerInterface $logger
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    private function authentication(): void
    {
        $token = $this->getAuthenticationToken(
            self::URL_IDENTITE,
            self::CLIENT_ID,
            $this->parameterBag->get('IDENTITE_USERNAME'),
            $this->parameterBag->get('IDENTITE_PASSWORD'),
            self::TIMEOUT
        )[AbstractService::ACCESS_TOKEN];
        $this->setAgentAccessToken($token);
    }

    /**
     * Get list of reports
     *
     * @param Agent $agent
     *
     * @return array
     */
    protected function getReports(Agent $agent): array
    {
        if ($this->getAgentReports() !== null) {
            return $this->getAgentReports();
        }
        try {
            if (empty($this->getAgentAccessToken())) {
                $this->authentication();
            }
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }

        $this->setHttpClientThrow(false);
        $url = $this->getApiUrl() . self::REPORTS_URL;
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAgentAccessToken()
            ]
        ];
        $response = $this->request('GET', $url, $options);
        if (null !== $response && Response::HTTP_UNAUTHORIZED === $response->getStatusCode()) {
            $this->authentication();
            $options = ['headers' => [
                'Authorization' => 'Bearer ' . $this->getAgentAccessToken()
            ]];
            $response = $this->request('GET', $url, $options);
        }
        if ('' !== $this->getEndUserExceptionMessage('BI')) {
            return ['error' => $this->getEndUserExceptionMessage('BI')];
        }
        $content = $response->getContent(false);
        $reports = !empty($content) ? json_decode($content, true, 512, JSON_THROW_ON_ERROR) : [];
        $moduleEnabled = $this->isModuleEnabledByAgent($agent);

        foreach ($reports as $key => $report) {
            if (
                !$moduleEnabled
                && !empty($report['offer']['code'])
                && $report['offer']['code'] != self::FREE_CODE
            ) {
                unset($reports[$key]);
            }
        }
        $reports = array_values($reports);

        $this->setAgentReports($reports);

        return $reports;
    }

    /**
     * Permet de créer l'agent dans la BDD côté API BI
     *
     * @throws Exception
     */
    protected function createContext(Agent $agent): string
    {
        $this->authentication();
        $url = $this->getApiUrl() . self::STATS_BI_AGENT_URL;

        return $this->getAgentContext($url, $agent, $this->getAgentAccessToken());
    }

    /**
     * @throws Exception
     */
    protected function setDataXML(Agent $agent): string
    {
        try {
            $xml = new DOMDocument('1.0', 'UTF-8');
            $xml->xmlStandalone = true;

            $mpeRoot = $xml->createElement('mpe');
            $mpeAttribute = new DOMAttr('xmlns', 'http://www.atexo.com/epm/xml');
            $mpeRoot->setAttributeNode($mpeAttribute);

            $envoiNode = $xml->createElement('envoi');
            $agentNode = $xml->createElement('agent');

            $identifiantNodeChild = $xml->createElement('identifiant', $agent->getLogin());
            $agentNode->appendChild($identifiantNodeChild);
            $plateformeNodeChild = $xml->createElement('plateforme', $this->parameterBag->get('PF_SHORT_NAME'));
            $agentNode->appendChild($plateformeNodeChild);
            $acronymeOrganismeNodeChild = $xml->createElement('acronymeOrganisme', $agent->getAcronymeOrganisme());
            $agentNode->appendChild($acronymeOrganismeNodeChild);

            $organismeNodeChild = $xml->createElement('organisme');

            $idNodeChild = $xml->createElement('id', $agent->getOrganisme()?->getId() ?? '');
            $organismeNodeChild->appendChild($idNodeChild);
            $acronymeNodeChild = $xml->createElement('acronyme', $agent->getOrganisme()?->getAcronyme() ?? '');
            $organismeNodeChild->appendChild($acronymeNodeChild);
            $denominationOrganismeNodeChild =
                $xml->createElement('denominationOrganisme', $agent->getOrganisme()?->getDenominationOrg() ?? '');
            $organismeNodeChild->appendChild($denominationOrganismeNodeChild);

            $logoOrganisme =
                $this->parameterBag->get('BASE_ROOT_DIR')
                . $agent->getOrganisme()->getAcronyme()
                . $this->parameterBag->get('PATH_ORGANISME_IMAGE')
                . 'logo-organisme-grand.jpg';
            $logoNodeChild = $xml->createElement('logo', $logoOrganisme);

            $organismeNodeChild->appendChild($logoNodeChild);
            $agentNode->appendChild($organismeNodeChild);

            $nomNodeChild = $xml->createElement('nom', $agent->getNom());
            $agentNode->appendChild($nomNodeChild);
            $prenomNodeChild = $xml->createElement('prenom', $agent->getPrenom());
            $agentNode->appendChild($prenomNodeChild);
            $emailNodeChild = $xml->createElement('email', $agent->getEmail());
            $agentNode->appendChild($emailNodeChild);
            $hyperadminNodeChild = $xml->createElement(
                'hyperadmin',
                $this->habilitationService->checkHabilitation($agent, 'getHyperAdmin')
            );
            $agentNode->appendChild($hyperadminNodeChild);
            $envoiNode->appendChild($agentNode);
            $mpeRoot->appendChild($envoiNode);
            $xml->appendChild($mpeRoot);

            return $xml->saveXML();
        } catch (Exception $e) {
            throw new Exception('Erreur génération xml stats BI agent : ' . $e->getMessage());
        }
    }

    protected function getAgentAccessToken(): ?string
    {
        return $this->session->get('agentBIModuleAccessToken');
    }

    private function setAgentAccessToken(string $accessToken): void
    {
        $this->session->set('agentBIModuleAccessToken', $accessToken);
    }

    private function getAgentReports(): ?array
    {
        return $this->session->get('agentBIModuleReports');
    }

    private function setAgentReports(array $reports): void
    {
        $this->session->set('agentBIModuleReports', $reports);
    }

    protected function getApiUrl(): string
    {
        return $this->parameterBag->get('STATS_API_BASE_URL');
    }

    private function isModuleEnabledByAgent(Agent $agent): bool
    {
        $userOrganisme = $agent->getAcronymeOrganisme();

        $organismeRepository = $this->em->getRepository(ConfigurationOrganisme::class)
            ->findOneBy(['organisme' => $userOrganisme]);

        if (!empty($organismeRepository) && $organismeRepository->isModuleBiPremium()) {
            return true;
        }

        return false;
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function pingForMonitoring(): int
    {
        $this->authentication();
        $url = $this->getApiUrl() . self::REPORTS_URL;
        $options = [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->getAgentAccessToken()
            ]
        ];

        return $this->request('GET', $url, $options)->getStatusCode();
    }
}
