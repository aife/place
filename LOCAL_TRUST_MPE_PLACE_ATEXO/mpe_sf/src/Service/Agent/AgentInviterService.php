<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Agent;

use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Service\ContratService;
use Doctrine\ORM\EntityManagerInterface;

class AgentInviterService
{
    /**
     * AgentInviterService constructor.
     */
    public function __construct(private EntityManagerInterface $em, private ContratService $contratService, private Habilitation $habilitationService)
    {
    }

    public function getListAgentInviter(string $orgAcronym, ?int $serviceId, array $params = []): array
    {
        $listAgent = [];
        $listAgent = array_merge(
            $listAgent,
            $this->getPermanentGuestsMonEntitee($orgAcronym, $serviceId, $params),
            $this->getPermanentGuestsEntiteeDependante($orgAcronym, $serviceId, $params),
            $this->getPermanentGuestsEntiteeTransverse($orgAcronym, $serviceId, $params)
        );
        return $listAgent;
    }

    public function getPermanentGuestsMonEntitee(?string $orgAcronym, ?int $serviceId, array $params = []): array
    {
        $permanentGuestsMonEntitee = [];
        $params['invitePermanentMonEntite'] = '1';
        $allGuestsMonEntite = $this->em
            ->getRepository(HabilitationAgent::class)
            ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

        /** @var HabilitationAgent $g */
        foreach ($allGuestsMonEntite as $g) {
            $permanentGuestsMonEntitee[] = $g->getAgent()->getId();
        }

        return $permanentGuestsMonEntitee;
    }

    public function getPermanentGuestsEntiteeDependante(?string $orgAcronym, ?int $serviceId, array $params = []): array
    {
        $permanentGuestsEntiteeDependante = [];
        $affiliationServices = $this->em->getRepository(AffiliationService::class)
                ->getParentByServiceIdAndOrganisme($serviceId, $orgAcronym);
        $tabPole = [];

        foreach ($affiliationServices as $affiliationService) {
            $tabPole[] = $affiliationService->getServiceParentId()->getId();
        }

        $params['invitePermanentEntiteDependante'] = '1';
        $allGuestsMonEntite = $this->em->getRepository(HabilitationAgent::class)
                ->retrievePermanentEntiteDependante($orgAcronym, $serviceId, $params, $tabPole);

        /** @var HabilitationAgent $g */
        foreach ($allGuestsMonEntite as $g) {
            $agents[] = $g->getAgent()->getId();
        }

        return $permanentGuestsEntiteeDependante;
    }

    public function getPermanentGuestsEntiteeTransverse(?string $orgAcronym, ?int $serviceId, array $params = []): array
    {
        $permanentGuestsEntiteeTransverse = [];
        $params['invitePermanentTransverse'] = '1';
        $allGuestsTransverse = $this->em->getRepository(HabilitationAgent::class)
                ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

        /** @var HabilitationAgent $g */
        foreach ($allGuestsTransverse as $g) {
            $transverse = $this->em->getRepository(InvitePermanentTransverse::class)->findOneBy([
                        'agent' => $g->getAgent()->getId(),
                        'service' => $g->getAgent()->getServiceId(),
                        'acronyme' => $orgAcronym,
                    ]);

            if (is_null($transverse)) {
                continue;
            }

            $permanentGuestsEntiteeTransverse[] = $g->getAgent()->getId();
        }

        return $permanentGuestsEntiteeTransverse;
    }

    public function getEntitesWhereAgentIsInvited(Agent $agent): array
    {
        $listOfServicesAllowed = [];
        if (
            $this->habilitationService->checkHabilitation($agent, 'getInvitePermanentMonEntite')
            && !empty($agent->getService())
        ) {
            $listOfServicesAllowed[] = $agent->getService();
        }

        if (
            $this->habilitationService->checkHabilitation($agent, 'getInvitePermanentEntiteDependante')
            && !empty($agent->getService())
        ) {
            $listOfServicesAllowed = $this->contratService->getSubServices(
                $agent->getService()->getId()
            );
        }

        $servicesInvitePermanent = $this->em->getRepository(InvitePermanentTransverse::class)->findBy(['agent' => $agent]);
        foreach ($servicesInvitePermanent as $value) {
            if (!empty($value->getService())) {
                $listOfServicesAllowed[] = $value->getService();
            }
        }

        return $listOfServicesAllowed;
    }
}
