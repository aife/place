<?php

namespace App\Service;

use App\Message\CommandeMessenger;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Transport\Serialization\PhpSerializer;

class AtexoMigration
{
    /**
     * @throws Exception
     */
    public static function insertInMessengerMessage(
        QueryBuilder $queryBuilder,
        string $commande,
        array $params,
        ?string $availableAt = null,
    ): void {
        $message = new CommandeMessenger($commande, $params);
        $envelope = new Envelope($message);
        $serializer = new PhpSerializer();
        $serialized = $serializer->encode($envelope);

        if (null === $availableAt) {
            $availableAt = (new \DateTime('now'))->format('Y-m-d H:i:s');
        }

        $queryBuilder
            ->insert('messenger_messages')
            ->values([
                'body' => '?',
                'headers' => '?',
                'queue_name' => '?',
                'created_at' => '?',
                'available_at' => '?',
                'delivered_at' => '?',
            ])
            ->setParameter(0, $serialized['body'])
            ->setParameter(1, '[]')
            ->setParameter(2, 'default')
            ->setParameter(3, (new \DateTime('now'))->format('Y-m-d H:i:s'))
            ->setParameter(4, $availableAt)
            ->setParameter(5, null)
            ->execute();
    }
}
