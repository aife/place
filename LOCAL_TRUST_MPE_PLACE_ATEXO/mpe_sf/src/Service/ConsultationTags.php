<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Repository\ConsultationRepository;
use App\Repository\ConsultationTagsRepository;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConsultationTags
{
    // Tableau des tags existant (à mettre dans une table de référentiel dans le futur)
    public const TAGS = [
        ['code' => 'typageJo', 'label' => 'TYPAGE_JO_2024', 'logo' => 'app/img/logo-jo2024.png']
    ];

    public function __construct(
        private TranslatorInterface $translator,
        private ConsultationRepository $consultationRepository,
        private ConsultationTagsRepository $consultationTagsRepository
    ) {
    }

    public function getTagByConsultationId(int $consultationId, string $tagcode): ?int
    {
        $consultation = $this->consultationRepository->find($consultationId);
        $tag = $this->consultationTagsRepository->findOneBy(['consultation' => $consultation, 'tagCode' => $tagcode]);

        if (empty($tag)) {
            return null;
        }

        return $tag->getId();
    }

    public function getReferentielTags(): array
    {
        $tags = self::TAGS;

        foreach ($tags as $key => $tag) {
            $tags[$key]['label'] = $this->translator->trans($tag['label']);
        }

        return $tags;
    }
}
