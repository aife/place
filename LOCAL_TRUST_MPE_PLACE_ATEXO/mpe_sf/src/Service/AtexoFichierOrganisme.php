<?php

namespace App\Service;

use League\Flysystem\FileExistsException;
use Exception;
use App\Entity\BloborganismeFile;
use App\Entity\EnveloppeFichier;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Classe contenant les fonctions gestion fichier.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2016
 */
class AtexoFichierOrganisme extends AtexoFichier
{
    private $serverTmpName;

    private $hash;

    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        MountManager $mountManager,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct($container, $em, $mountManager);
    }

    /**
     * @todo This function serves no purpose and should be deleted once the new DCE File upload is merged.
     *
     * @param $tmpFilePath
     *
     * @return mixed
     */
    public function normalizeTmpPath($tmpFilePath)
    {
        $res = str_replace($this->parameterBag->get('COMMON_TMP'), '', $tmpFilePath);

        $upload_dir = ini_get('upload_tmp_dir');

        return str_replace($upload_dir, '', $res);
    }

    /**
     * @todo This function serves no purpose and should be deleted once the new DCE File upload is merged.
     *
     * @return bool
     * @throws FileExistsException
     */
    public function moveTmpFile($tmpFilePath, Request $request = null)
    {
        $filenameTmp = $this->normalizeTmpPath($tmpFilePath);
        if (!empty($request)) {
            $serverTmpName = sprintf('%s-%s', $request->server->get('SERVER_ADDR'), $filenameTmp);
        } else {
            $serverTmpName = $filenameTmp;
        }

        $mountManager = $this->getMountManager();
        if ($mountManager->has('common_tmp://' . $filenameTmp)) {
            $tmpFile = 'common_tmp://' . $filenameTmp;
        } else {
            $tmpFile = 'upload_tmp_dir://' . $filenameTmp;
        }

        if ($mountManager->copy($tmpFile, 'nas://tmp/' . $serverTmpName)) {
            $this->setServerTmpName($serverTmpName);

            return $serverTmpName;
        }

        return false;
    }

    /**
     * @param $organisme
     * @param $subfolder
     *
     * @return string
     *
     * @throws Exception
     */
    public function moveBlobFile(BloborganismeFile $blob, string $organisme = '', $subfolder = null, $extension = '')
    {
        $path = [];

        $mountManager = $this->getMountManager();

        $path[] = $mountManager->getRepertoire($organisme);

        if ($subfolder) {
            $path[] = $subfolder;
        }
        $dir = implode('/', $path);

        if (!$mountManager->has('nas://' . $dir)) {
            $mountManager->createDir('nas://' . $dir);
        }

        if (!$mountManager->has('nas://tmp/' . $this->getServerTmpName())) {
            throw new Exception('chemin du fichier incorrect ' . $this->getServerTmpName());
        }

        $path[] = $blob->getId() . '-0' . $extension;

        $blobFilePath = implode('/', $path);
        if (!$mountManager->has('nas://' . $blobFilePath)) {
            error_clear_last();
            if (!$mountManager->move('nas://tmp/' . $this->getServerTmpName(), 'nas://' . $blobFilePath)) {
                $details = ' Error : ' . print_r(error_get_last(), true);
                throw new Exception("une erreur s'est produite lors de 
                l'insertion du fichier au niveau du disque => " . $details);
            }
        }
        $this->setHash(
            $mountManager->sha1File('nas://' . $blobFilePath)
        );

        return $mountManager->uniformPath($dir);
    }

    /**
     * insertion du blobOrganismeFile et déplacement du fichier sur le nas.
     *
     * @param null $subfolder
     *
     *
     * @throws Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016*/
    public function insertFile(
        string $fileName,
        string $pathFile,
        string $organisme = '',
        $subfolder = null,
        $extension = '',
        ?BloborganismeFile $blobFile = null
    ) {
        try {
            $em = $this->getEm();

            if (null === $blobFile?->getId()) {
                $blobFile = new BloborganismeFile();
                $blobFile->setName($fileName);
                $blobFile->setOrganisme($organisme);
                $blobFile->setStatutSynchro(0);
                $blobFile->setDossier($subfolder);
                $em->persist($blobFile);
                $em->flush();
            }

            $blobFilePath = $this->moveBlobFile($blobFile, $organisme, $subfolder, $extension);
            $blobFile->setChemin($blobFilePath);
            $blobFile->setHash($this->getHash());
            $blobFile->setExtension($extension);
            $em->persist($blobFile);
            $em->flush();
            $id = $blobFile->getId();
            $em->clear(BloborganismeFile::class);

            return $id;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * delete file.
     *
     * @throws Exception
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     * @author Amal EL BEKKAOUI <amal@atexo.com>*/
    public function deleteBlobFile(int $idBlob, string $organisme)
    {
        $em = $this->getEm();

        $blobFichier = $em->getRepository(BloborganismeFile::class)
                ->getOrganismeBlobFichier($idBlob);

        if ($blobFichier !== null) {
            $this->getMountManager()->deleteFile($idBlob, $blobFichier->getOrganisme(), $em);
            $em->remove($blobFichier);
        }

        return true;
    }

    /**
     * @return string|null
     *
     * @throws Exception
     */
    public function getContentSignatureFichier(EnveloppeFichier $enveloppeFichier)
    {
        $content = null;
        if (
            null !== $enveloppeFichier->getIdFichierSignature()
            && $enveloppeFichier->getIdFichierSignature() != $enveloppeFichier->getIdFichier()
        ) {
            $doctrine = $this->getContainer()->get('doctrine');
            $jeton = $doctrine->getRepository(EnveloppeFichier::class)
                ->find($enveloppeFichier->getIdFichierSignature());
            if ($jeton) {
                $content = ($this->getMountManager())->getFileContent(
                    $jeton->getBlob()->getId(),
                    $enveloppeFichier->getOrganisme(),
                    $this->getEm()
                );
                $content = base64_encode($content);
            } else {
                throw new Exception('Jeton manquant');
            }
        }

        return $content;
    }

    /**
     * @return mixed
     */
    public function getServerTmpName()
    {
        return $this->serverTmpName;
    }

    /**
     * @param mixed $serverTmpName
     */
    public function setServerTmpName($serverTmpName)
    {
        $this->serverTmpName = $serverTmpName;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @param int    $idBlob
     * @param string $organisme
     * @param string $extension
     *
     * @return bool
     *
     * @throws Exception
     */
    public function changeExtensionBlob($idBlob, $organisme, $extension)
    {
        try {
            $em = $this->getEm();
            $blob = $em->getRepository(BloborganismeFile::class)
                ->getOrganismeBlobFichier($idBlob);
            if ($blob instanceof BloborganismeFile) {
                $oldExtension = $blob->getExtension();
                if ($oldExtension != $extension) {
                    try {
                        $mountManager = $this->getMountManager();
                        $pathBlob = $mountManager->getFilePath($idBlob, $organisme, $em);
                        $newPathBlob = str_replace($blob->getExtension(), $extension, $pathBlob);
                        $blob->setExtension($extension);
                        $em->persist($blob);
                        $em->flush($blob);

                        $path = [];
                        $path[] = $mountManager->getRepertoire($organisme);

                        $dir = implode('/', $path);

                        if (!$mountManager->has('nas://' . $dir)) {
                            $mountManager->createDir('nas://' . $dir);
                        }
                        error_clear_last();
                        if (!$mountManager->move('nas://' . $pathBlob, 'nas://' . $newPathBlob)) {
                            $details = ' Error : ' . print_r(error_get_last(), true);
                            throw new Exception("une erreur s'est produite lors de 
            l'insertion du fichier au niveau du disque => " . $details);
                        }

                        return true;
                    } catch (Exception $e) {
                        $blob->setExtension($oldExtension);
                        $em->persist($blob);
                        $em->flush($blob);
                        throw $e;
                    }
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $idBlob
     * @param $organisme
     */
    public function getFileSize($idBlob, $organisme): false|int
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $mountManager = $this->getMountManager();
        $pathBlob = $mountManager->getFilePath($idBlob, $organisme, $em);

        return $mountManager->getSize($pathBlob);
    }

    public function setPath(
        BloborganismeFile $blobFile,
        string $organisme = '',
        $subfolder = null,
        $extension = ''
    ) {
        try {
            $em = $this->getContainer()->get('doctrine')->getManager();
            $blobFilePath = $this->moveBlobFile($blobFile, $organisme, $subfolder, $extension);
            $blobFile->setChemin($blobFilePath);
            $em->flush($blobFile);
            $em->clear(BloborganismeFile::class);

            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }
    public function getBlobById($idBlob): ?BloborganismeFile
    {
        return $this->getEm()->getRepository(BloborganismeFile::class)->findOneBy(['id' => $idBlob]);
    }
}
