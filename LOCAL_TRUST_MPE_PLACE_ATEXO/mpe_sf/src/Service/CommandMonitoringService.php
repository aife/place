<?php

namespace App\Service;

use App\Repository\CommandMonitoringRepository;
use DateTime;
use App\Entity\CommandMonitoring;
use Doctrine\ORM\EntityManagerInterface;

class CommandMonitoringService
{
    public final const IN_PROGRESS = 'In progress';
    public final const SUCCESS = 'Success';
    public final const FAILED = 'Failed';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly OutputMonitoredCommandService $outputMonitoredCommand,
        private readonly CommandMonitoringRepository $monitoringRepository
    ) {
    }

    public function createCommandLog(string $commandName, array $commandParams = []): CommandMonitoring
    {
        $log = new CommandMonitoring();
        $log->setName($commandName);
        $log->setStatus(self::IN_PROGRESS);
        $log->setStartDate(new DateTime());

        $params = null;
        foreach ($commandParams as $key => $commandParam) {
            if ($key != 'command') {
                $params .= empty($params) ? '' : ' || ';
                $params .= "{$key} => {$commandParam}";
            }
        }

        $log->setParams($params);

        $this->em->persist($log);
        $this->em->flush();

        return $log;
    }

    public function setSuccess(CommandMonitoring $commandLog): void
    {
        $commandLog->setStatus(self::SUCCESS);
        $commandLog->setEndDate(new DateTime());
        $commandLog->setCommandOutput($this->outputMonitoredCommand->getCommandOutput());

        $this->em->flush();
    }

    public function setFailure(CommandMonitoring $commandLog, string $logs): void
    {
        $commandLog->setStatus(self::FAILED);
        $commandLog->setEndDate(new DateTime());
        $commandLog->setLogs($logs);
        $commandLog->setCommandOutput($this->outputMonitoredCommand->getCommandOutput());

        $this->em->flush();
    }

    public function getOutputMonitoredCommand(): OutputMonitoredCommandService
    {
        return $this->outputMonitoredCommand;
    }

    public function isCommandLaunched(string $name): bool
    {
        return (null !== $this->monitoringRepository->findOneBy(['name' => $name, 'status' => self::SUCCESS]));
    }
}
