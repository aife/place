<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Administrateur;
use App\Repository\AdministrateurRepository;
use App\Security\PasswordEncoder\CommonPasswordEncoder;

class AdministratorService
{
    public function __construct(
        private AdministrateurRepository $administrateurRepository,
        private CommonPasswordEncoder $commonPasswordEncoder
    ) {
    }

    public function convertPasswordToArgon(): int
    {
        $response = 0;
        $administrateurs = $this->administrateurRepository->getAllPasswordsNotArgon2Hashed();

        if (is_countable($administrateurs) && count($administrateurs) === 0) {
            return $response;
        }

        foreach ($administrateurs as $administrateur) {
            if (
                $this->isSha1($administrateur->getMdp() ?? '')
                || $this->isSha256($administrateur->getMdp() ?? '')
            ) {
                break;
            }
            $administrateur->setMdp(
                $this->commonPasswordEncoder->encodePassword(
                    Administrateur::class,
                    $administrateur->getMdp() ?? ''
                )
            );
            $this->administrateurRepository->persist($administrateur);
        }
        $this->administrateurRepository->flush();

        return $response;
    }

    private function isSha1(string $password): bool
    {
        return (bool) preg_match('/^[0-9a-f]{40}$/i', $password);
    }

    private function isSha256(string $password): bool
    {
        return (bool) preg_match('/^([a-f0-9]{64})$/', $password);
    }
}
