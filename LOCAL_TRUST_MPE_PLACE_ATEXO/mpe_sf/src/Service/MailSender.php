<?php

namespace App\Service;

use Swift_Mailer;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Swift_Message;
use Swift_Signers_SMimeSigner;
/**
 * Mohamed BLAL
 * Class MailSender.
 */
class MailSender
{
    /**
     * @var
     */
    private static string $signMailChaine;

    /**
     * @var
     */
    private static string $signMailChaineCert;

    private static string $signMailCert;

    private static string $signMailCle;

    private static string $baseRootDir;

    /**
     * MailSender constructor.
     *
     * @param $signMailChaine
     * @param $signMailChaineCert
     * @param $certChaine
     * @param $signMailCle
     */
    public function __construct(
        private readonly Swift_Mailer $sender,
        string $signMailChaine = '',
        string $signMailChaineCert = '',
        string $signMailCert = '',
        string $signMailCle = '',
        string $baseRootDir = ''
    ) {
        static::$signMailChaine = $signMailChaine;
        static::$signMailChaineCert = $signMailChaineCert;
        static::$signMailCert = $signMailCert;
        static::$signMailCle = $signMailCle;
        static::$baseRootDir = $baseRootDir;
    }

    /**
     * @param false $signed
     * @param bool  $backup
     *
     * @throws TransportExceptionInterface
     */
    public function send(Swift_Message $message, $signed = false, $backup = true)
    {
        if ($signed) {
            $message->attachSigner(self::signMail());
        }
        if ($backup) {
            $mailBackup = clone $message;
            static::backupMail($mailBackup);
        }

        $this->sender->send($message);
    }

    /**
     * Return a Swift_Signers_SMimeSigner object allows : la signature du mail.
     *
     * @return Swift_Signers_SMimeSigner
     *
     * @author Gregory CHEVRET <gregory@atexo.com>*/
    private static function signMail()
    {
        $smimeSigner = new Swift_Signers_SMimeSigner();
        $extraCerts = static::$signMailChaine ? static::$signMailChaineCert : null;
        $smimeSigner->setSignCertificate(self::$signMailCert, self::$signMailCle, PKCS7_DETACHED, $extraCerts);

        return $smimeSigner;
    }

    /**
     * Sauvegarde sur le disque le mail envoyé.
     *
     * @author Gregory CHEVRET <gregory@atexo.com>
     */
    public static function backupMail(Swift_Message $mail)
    {
        $saveMailDir = static::$baseRootDir.'/sendMail/'.date('Y/m/d');
        if (!is_dir($saveMailDir)) {
            mkdir($saveMailDir, 0755, true);
        }
        if (!is_dir($saveMailDir.DIRECTORY_SEPARATOR.'eml')) {
            mkdir($saveMailDir.DIRECTORY_SEPARATOR.'eml', 0755, true);
        }
        if (!is_dir($saveMailDir.DIRECTORY_SEPARATOR.'html')) {
            mkdir($saveMailDir.DIRECTORY_SEPARATOR.'html', 0755, true);
        }
        $mailTo = $mail->getTo();
        $to = implode('-', array_keys($mailTo));
        $filenameMail = 'mail_from_mpe_sf_'.$to.'_'.date('Ymd_His').'_'.uniqid();
        file_put_contents(
            $saveMailDir.DIRECTORY_SEPARATOR.'html'.DIRECTORY_SEPARATOR.$filenameMail.'.html',
            $mail->getBody()
        );
        file_put_contents(
            $saveMailDir.DIRECTORY_SEPARATOR.'eml'.DIRECTORY_SEPARATOR.$filenameMail.'.eml',
            $mail->toString()
        );
    }
}
