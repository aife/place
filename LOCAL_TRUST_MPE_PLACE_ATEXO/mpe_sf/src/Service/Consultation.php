<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

class Consultation
{
    public static function hasClauseSociale(object $object): bool
    {
        return ($object && method_exists($object,"hasThisClause")) && (
                $object->hasThisClause('ClauseSocialeConditionExecution')
                || $object->hasThisClause('ClauseSocialeInsertion') || $object->hasThisClause('ClauseSocialeAteliersProteges')
                || $object->hasThisClause('ClauseSocialeSiae') || $object->hasThisClause('ClauseSocialeEss')
                || $object->hasThisClause('MarcheInsertion')
                || $object->hasThisClause('ClauseSpecificationTechnique')
            );
    }

    public static function hasClauseSocialeAteliersProteges(object $object): bool
    {
        return ($object && method_exists($object, "hasThisClause")) && $object->hasThisClause('ClauseSocialeAteliersProteges');
    }

    public static function hasClauseEnvironnementales(object $objet): bool
    {
        return (($objet && method_exists($objet, "hasThisClause")) && ($objet->hasThisClause('ClauseEnvSpecsTechniques') || $objet->hasThisClause('ClauseEnvCondExecution') || $objet->hasThisClause('ClauseEnvCriteresSelect')));
    }
}