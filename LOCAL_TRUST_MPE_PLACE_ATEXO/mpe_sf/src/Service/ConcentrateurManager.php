<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\AdresseFacturationJal;
use App\Entity\Agent;
use App\Entity\TypeAvisPubProcedure;
use App\Repository\AdresseFacturationJalRepository;
use App\Repository\TypeAvisPubProcedureRepository;
use App\Service\Publicite\EchangeConcentrateur;
use Application\Propel\Mpe\CommonConsultation;
use App\Entity\Consultation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ConcentrateurManager
{
    public const CONCENTRATEUR_PUB_TNCP = 'TNCP';
    public const CONCENTRATEUR_PUB_MOL = 'MOL';
    public const CONCENTRATEUR_PUB_JAL_FR = 'JAL_FR';
    public const CONCENTRATEUR_PUB_JAL_LUX = 'JAL_LUX';
    public const CONCENTRATEUR_PUB_JOUE = 'JOUE';

    public function __construct(
        private EchangeConcentrateur $echangeConcentrateur,
        private WebservicesConcentrateur $webservicesConcentrateur,
        private AgentService $agentService,
        private ParameterBagInterface $parameterBag,
        private TranslatorInterface $translator,
        private EntityManagerInterface $entityManager,
        private ConfigurationOrganismeService $configurationOrganismeService,
        private AdresseFacturationJalRepository $facturationJalRepository,
        private Security $security
    ) {
    }

    public function addAgent(CommonConsultation | Consultation $consultation): string
    {
        $accessToken = $this->echangeConcentrateur->getToken($consultation->getId());
        if (empty($accessToken)) {
            throw new \Exception('No access token');
        }
        $body = [];
        if ($this->security->getUser() instanceof Agent) {
            $agent = $this->security->getUser();
            $body = $this->echangeConcentrateur->getDataEnvoi($agent);
        }

        return $this->webservicesConcentrateur->getContent('createToken', $accessToken, $body);
    }

    public function configAnnonces(
        CommonConsultation | Consultation $consultation,
        string $accessToken,
        array $data = []
    ): array|string {
        $body['idConsultation'] = $consultation->getId();
        $body['xml'] = base64_encode($this->echangeConcentrateur->generateXmlAnnonce($consultation->getId()));
        $body['idPlatform'] = $this->parameterBag->get('UID_PF_MPE');
        $body['dmls'] = $consultation->getDateMiseEnLigneSouhaitee();
        $body['jalList'] = $this->echangeConcentrateur->getJal($consultation);
        if (empty($data)) {
            $body['europeen'] = $this->isEuropeanPub($consultation);
            $body['blocList'] = $this->getConfigPub($consultation);
            $body['facturationList'] = $this->getFacturations($consultation);
        }
        $body = array_merge($body, $data);

        return $this->webservicesConcentrateur->getContent('configAnnonces', $accessToken, $body);
    }

    public function getPubTranslate(): false|string
    {
        return json_encode([
            'SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION' => base64_encode(
                $this->translator->trans('SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION')
            ),
            'SUPPORT_PUBLICATION' => base64_encode($this->translator->trans('SUPPORT_PUBLICATION')),
            'TYPE_AVIS_OFFRE' => base64_encode($this->translator->trans('TYPE_AVIS_OFFRE')),
            'DEFINE_ACTIONS' => base64_encode($this->translator->trans('DEFINE_ACTIONS')),
            'SELECTIONNEZ_TYPE_AVIS' => base64_encode($this->translator->trans('SELECTIONNEZ_TYPE_AVIS')),
        ], JSON_THROW_ON_ERROR);
    }

    public function isEuropeanPub(Consultation $consultation): bool
    {
        $typeAvisProcedure = $this->entityManager->getRepository(TypeAvisPubProcedure::class)->findOneBy([
            'organisme' => $consultation->getOrganisme(),
            'typeProcedure' => $consultation->getTypeProcedure()
        ]);

        if (null === $typeAvisProcedure) {
            return false;
        }

        return 1 === $typeAvisProcedure->getTypeAvis()?->getTypePub();
    }

    public function getConfigPub(Consultation $consultation): array
    {
        $configPub = [];

        if ($this->security->getUser() instanceof Agent) {
            $configOrganisme = $this->configurationOrganismeService
                ->getConfigurationOrganismeByAcronyme($this->security->getUser()->getOrganisme());

            $allConfigPub = [
                'isPubTncp'     => self::CONCENTRATEUR_PUB_TNCP,
                'isPubMol'      => self::CONCENTRATEUR_PUB_MOL,
                'isPubJalFr'    => self::CONCENTRATEUR_PUB_JAL_FR,
                'isPubJalLux'   => self::CONCENTRATEUR_PUB_JAL_LUX,
                'isPubJoue'     => self::CONCENTRATEUR_PUB_JOUE
            ];
            foreach ($allConfigPub as $key => $value) {
                if ($configOrganisme->$key()) {
                    $configPub[] = $value;
                }
            }
        }

        return $configPub;
    }

    public function getFacturations(Consultation $consultation): array
    {
        $criteria['organisme'] = $consultation->getAcronymeOrg();
        if (null !== $consultation->getService()) {
            $criteria['service'] = $consultation->getService();
        }

        $addresses = $this->facturationJalRepository->findBy($criteria);

        $addressList = [];

        foreach ($addresses as $address) {
            $addressList[] = [
                'address' => $address->getInformationFacturation(),
                'sip' => $address->getFacturationSip() ? 'true' : 'false',
                'email' => $address->getEmailAr()
            ];
        }

        return $addressList;
    }
}
