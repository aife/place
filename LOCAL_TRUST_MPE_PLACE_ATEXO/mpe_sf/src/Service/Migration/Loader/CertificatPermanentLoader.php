<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\CertificatPermanent;
use App\Repository\CertificatPermanentRepository;

class CertificatPermanentLoader extends Loader
{
    public function preLoad($resource)
    {
        /** @var CertificatPermanent $resource */
        $resource = parent::preLoad($resource);
        /** @var CertificatPermanentRepository $repository */
        $repository = $this->managerRegistry->getRepository(CertificatPermanent::class);
        $repository->setEm($this->managerRegistry->getManager('default'));
        $newId = $repository->getMaxId() + 1;
        $resource->setId($newId);

        return $resource;
    }
}
