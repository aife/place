<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\Publicite\GroupeMoniteur;
use App\Repository\Publicite\GroupeMoniteurRepository;

class GroupeMoniteurLoader extends Loader
{
    protected const RESET_PRIMARY_KEY = false;

    public function preLoad($resource)
    {
        /** @var GroupeMoniteur $resource */
        $resource = parent::preLoad($resource);
        /** @var GroupeMoniteurRepository $repository */
        $repository = $this->managerRegistry->getRepository(GroupeMoniteur::class);
        $repository->setEm($this->managerRegistry->getManager('default'));
        $newId = $repository->getMaxId() + 1;
        $resource->setId($newId);

        return $resource;
    }
}
