<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeContratEtTypeProcedure;
use App\Service\Migration\OrganismeMigrationService;
use App\Service\Migration\TypeProcedureOrganismeMigrationService;

class TypeContratEtTypeProcedureLoader extends Loader
{
    /**
     * @param TypeContratEtTypeProcedure $resource
     * @return TypeContratEtTypeProcedure
     */
    public function resetPrimaryKey($resource)
    {
        $resource->setIdTypeContratEtProcedure(null);

        return $resource;
    }

    public function preLoad($resource)
    {
        /** @var TypeContratEtTypeProcedure $resource */
        $resource = parent::preLoad($resource);
        $resource->setIdTypeProcedure($this->getTypeProcedureOrganisme($resource));

        return $resource;
    }

    private function getTypeProcedureOrganisme(TypeContratEtTypeProcedure $typeContratEtTypeProcedure)
    {
        $repository = $this->managerRegistry->getRepository(TypeProcedureOrganisme::class);
        $repository->setEm($this->managerRegistry->getManager('default'));

        $params = [
            'idTypeProcedure' => $typeContratEtTypeProcedure->getIdTypeProcedure()->getIdTypeProcedure(),
            'organisme' => $typeContratEtTypeProcedure->getOrganisme()
        ];

        return $repository->findOneBy($params);
    }
}
