<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\ProcedureEquivalence;
use App\Repository\Procedure\ProcedureEquivalenceRepository;

class ProcedureEquivalenceLoader extends Loader
{
    public function resetPrimaryKey($resource)
    {
        $resource->setIdTypeProcedure(null);

        return $resource;
    }

    /**
     * @param ProcedureEquivalence $resource
     * @return ProcedureEquivalence
     */
    public function preLoad($resource)
    {
        /** @var ProcedureEquivalence $resource */
        $resource = parent::preLoad($resource);
        /** @var ProcedureEquivalenceRepository $repository */
        $repository = $this->managerRegistry->getRepository(ProcedureEquivalence::class);
        $repository->setEm($this->managerRegistry->getManager('default'));
        $newId = $repository->getMaxId() + 1;
        $resource->setIdTypeProcedure($newId);

        return $resource;
    }
}
