<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Entity\TypeProcedure;
use App\Repository\Procedure\TypeProcedureOrganismeRepository;
use App\Repository\TypeProcedureRepository;

class TypeProcedureOrganismeLoader extends Loader
{
    public function resetPrimaryKey($resource)
    {
        $resource->setIdTypeProcedure(null);

        return $resource;
    }

    /**
     * @param TypeProcedureOrganisme $resource
     * @return TypeProcedureOrganisme
     */
    public function preLoad($resource)
    {
        /** @var TypeProcedureOrganisme $resource */
        $resource = parent::preLoad($resource);
        /** @var TypeProcedureOrganismeRepository $repository */
        $repository = $this->managerRegistry->getRepository(TypeProcedureOrganisme::class);
        $repository->setEm($this->managerRegistry->getManager('default'));
        $newId = $repository->getMaxId() + 1;
        $resource->setIdTypeProcedure($newId);
        $resource->setTypeContratEtProcedure(null);
        $resource->setProcedurePortailType($this->getProcedurePortailType($resource));

        return $resource;
    }

    private function getProcedurePortailType(TypeProcedureOrganisme $typeProcedureOrganisme)
    {
        $oldProcedurePortailType = $typeProcedureOrganisme->getProcedurePortailType();

        if (empty($oldProcedurePortailType)) {
            return null;
        }
        /** @var TypeProcedureRepository $repository */
        $repository = $this->managerRegistry->getRepository(TypeProcedure::class);
        $repository->setEm($this->managerRegistry->getManager('default'));

        return $repository->find($oldProcedurePortailType->getIdTypeProcedure());
    }
}
