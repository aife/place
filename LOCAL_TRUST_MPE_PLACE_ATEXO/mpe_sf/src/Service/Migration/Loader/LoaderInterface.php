<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

interface LoaderInterface
{
    public function resetPrimaryKey($resource);

    public function preLoad($resource);

    public function load($resource);

    public function postLoad($resource);
}
