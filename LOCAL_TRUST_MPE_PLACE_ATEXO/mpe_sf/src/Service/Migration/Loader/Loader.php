<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Service\Migration\MigrationService;
use App\Service\Migration\MigrationTableMappingService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

abstract class Loader implements LoaderInterface
{
    protected const RESET_PRIMARY_KEY = true;

    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected ContainerInterface $container,
        protected PropertyAccessorInterface $propertyAccessor,
        protected MigrationTableMappingService $tableMappingService
    ) {
        $this->entityManager = $this->managerRegistry->getManager('default');
        $this->entityManagerToMigrate = $this->managerRegistry->getManager('database_to_migrate');
    }

    public function resetPrimaryKey($resource)
    {
        $resource->setId(null);

        return $resource;
    }

    public function preLoad($resource)
    {
        $className = get_class($resource);

        if (static::RESET_PRIMARY_KEY) {
            $resource = $this->resetPrimaryKey($resource);
        }

        if (defined($className . '::FOREIGN_KEYS')) {
            foreach ($className::FOREIGN_KEYS as $attribut => $detail) {
                $class = $detail['class'];
                $property = $detail['property'];
                $isObject = $detail['isObject'];
                $migrationServiceClass = $detail['migrationService'];

                /** @var MigrationService $migrationService */
                $migrationService = $this->container->get($migrationServiceClass);
                $value = $this->propertyAccessor->getValue($resource, $attribut);

                if (empty($value)) {
                    $this->propertyAccessor->setValue($resource, $attribut, $value);
                    continue;
                }

                // Valeur sur attribut
                if ($isObject && !empty($value)) {
                    $value = $migrationService->getIdentifierValue($value);
                }

                $mainIdentifier = $this->tableMappingService->findMapping(
                    $migrationService::MIGRATION_TABLE_NAME,
                    $value
                );

                if (empty($mainIdentifier)) {
                    $repository = $this->entityManager->getRepository($class);
                    $repository->setEm($this->entityManager);

                    $nonMappedResource = $repository->findOneBy([$property => $value]);

                    if (empty($nonMappedResource)) {
                        $migrationService->silentMigrate($value);
                    } else {
                        $migrationService->mapResource($value, $value);
                    }

                    $mainIdentifier = $this->tableMappingService->findMapping(
                        $migrationService::MIGRATION_TABLE_NAME,
                        $value
                    );
                }

                $newValue = $mainIdentifier;

                if ($isObject) {
                    $entityRepository = $this->entityManager->getRepository($class);
                    $entityRepository->setEm($this->entityManager);
                    $params = [];


                    if (is_array($property)) {
                        $explodedIdentifier = explode(';', $mainIdentifier);

                        foreach ($property as $key => $itemProperty) {
                            $params[$itemProperty] = $explodedIdentifier[$key];
                        }
                    } else {
                        $params = [$property => $mainIdentifier];
                    }

                    $newValue = $entityRepository->findOneBy($params);
                }

                $this->propertyAccessor->setValue($resource, $attribut, $newValue);
            }
        }

        return $resource;
    }

    public function load($resource)
    {
        $resource = $this->preLoad($resource);
        $this->entityManager->persist($resource);
        $this->entityManager->flush();

        return $resource;
    }

    public function postLoad($resource)
    {
    }
}
