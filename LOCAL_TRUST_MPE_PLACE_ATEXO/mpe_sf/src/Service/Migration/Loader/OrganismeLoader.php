<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\ConfigurationOrganisme;
use App\Entity\Organisme;
use App\Entity\OrganismeServiceMetier;
use App\Repository\ConfigurationOrganismeRepository;
use App\Repository\OrganismeServiceMetierRepository;
use App\Service\Migration\OrganismeMigrationService;

class OrganismeLoader extends Loader
{
    /**
     * @param Organisme $resource
     * @return void
     */
    public function postLoad($resource)
    {
        $acronyme = $resource->getAcronyme();
        $oldAcronyme = $this->tableMappingService->findOldMapping(
            OrganismeMigrationService::MIGRATION_TABLE_NAME,
            $acronyme
        );

        $this->addOrganismeConfiguration($acronyme, $oldAcronyme);
        $this->addOrganismeServiceMetier($acronyme, $oldAcronyme);

        $this->managerRegistry->getManager('default')->flush();
    }

    private function addOrganismeConfiguration($acronyme, $oldAcronyme)
    {
        /** @var ConfigurationOrganismeRepository $configurationOrganismeRepository */
        $configurationOrganismeRepository = $this->entityManager->getRepository(
            ConfigurationOrganisme::class
        );
        $configurationOrganismeRepository->setEm($this->entityManager);
        $configurationOrganisme = $configurationOrganismeRepository->findOneBy(['organisme' => $acronyme]);

        if (!empty($configurationOrganisme)) {
            return;
        }

        /** @var ConfigurationOrganismeRepository $configurationOrganismeRepository */
        $configurationOrganismeRepositoryTm = $this->entityManagerToMigrate->getRepository(
            ConfigurationOrganisme::class
        );
        $configurationOrganismeRepositoryTm->setEm($this->entityManagerToMigrate);
        $configurationOrganisme = $configurationOrganismeRepositoryTm->findOneBy(['organisme' => $oldAcronyme]);

        if (empty($configurationOrganisme)) {
            return;
        }

        $configurationOrganisme->setOrganisme($acronyme);
        $this->managerRegistry->getManager('default')->persist($configurationOrganisme);
    }

    private function addOrganismeServiceMetier($acronyme, $oldAcronyme)
    {
        /** @var OrganismeServiceMetierRepository $organismeServiceMetierRepository */
        $organismeServiceMetierRepository = $this->entityManager->getRepository(
            OrganismeServiceMetier::class
        );
        $organismeServiceMetierRepository->setEm($this->entityManager);
        $organismeServiceMetier = $organismeServiceMetierRepository->findOneBy(['organisme' => $acronyme]);

        if (!empty($organismeServiceMetier)) {
            return;
        }

        /** @var OrganismeServiceMetierRepository $organismeServiceMetierRepository */
        $organismeServiceMetierRepositoryTm = $this->entityManagerToMigrate->getRepository(
            OrganismeServiceMetier::class
        );
        $organismeServiceMetierRepositoryTm->setEm($this->entityManagerToMigrate);

        $organismeServiceMetier = $organismeServiceMetierRepositoryTm->findOneBy(['organisme' => $oldAcronyme]);
        $organismeServiceMetier->setOrganisme($acronyme);

        $this->managerRegistry->getManager('default')->persist($organismeServiceMetier);
    }
}
