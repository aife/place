<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\Agent;
use App\Entity\Agent\AgentServiceMetier;
use App\Entity\SocleHabilitationAgent;
use App\Repository\Agent\AgentServiceMetierRepository;
use App\Service\Migration\AgentMigrationService;

class AgentLoader extends Loader
{
    private SocleHabilitationAgent|null $socleHabilitations = null;

    public function load($resource)
    {
        /** @var Agent $resource */
        $resource = $this->preLoad($resource);
        $this->managerRegistry->getManager('default')->persist($resource);
        $this->socleHabilitations = $resource->getSocleHabilitations();
        $resource->setSocleHabilitations(null);
        $this->managerRegistry->getManager('default')->flush();

        return $resource;
    }

    public function postLoad($resource)
    {
        $this->loadHabilitations($resource);
        $this->loadAgentServiceMetier($resource);

        $this->managerRegistry->getManager('default')->flush();
    }

    /**
     * @param Agent $resource
     * @return void
     */
    private function loadHabilitations($resource)
    {
        if (!empty($this->socleHabilitations)) {
            $this->socleHabilitations->setAgent($resource);
            $this->managerRegistry->getManager('default')->persist($this->socleHabilitations);
        }
    }

    /**
     * @param Agent $resource
     * @return void
     */
    private function loadAgentServiceMetier($resource)
    {
        $idAgent = $resource->getId();
        $oldIdAgent = $this->tableMappingService->findMapping(AgentMigrationService::MIGRATION_TABLE_NAME, $idAgent);

        /** @var AgentServiceMetierRepository */
        $agentServiceMetierRepository = $this->entityManager->getRepository(AgentServiceMetier::class);
        $agentServiceMetierRepository->setEm($this->entityManager);
        $agentServiceMetiers = $agentServiceMetierRepository->findBy(['idAgent' => $idAgent]);

        if (!empty($agentServiceMetiers)) {
            return;
        }

        /** @var AgentServiceMetierRepository $organismeServiceMetierRepository */
        $agentServiceMetierRepositoryTm = $this->entityManagerToMigrate->getRepository(AgentServiceMetier::class);
        $agentServiceMetierRepositoryTm->setEm($this->entityManagerToMigrate);

        $agentServiceMetiers = $agentServiceMetierRepositoryTm->findBy(['idAgent' => $oldIdAgent]);

        /** @var AgentServiceMetier $agentServiceMetier */
        foreach ($agentServiceMetiers as $agentServiceMetier) {
            $agentServiceMetier->setIdAgent($idAgent);
            $this->managerRegistry->getManager('default')->persist($agentServiceMetier);
        }
    }
}
