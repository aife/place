<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Loader;

use App\Entity\Publicite\CompteMoniteur;
use App\Repository\Publicite\CompteMoniteurRepository;

class CompteMoniteurLoader extends Loader
{
    protected const RESET_PRIMARY_KEY = false;

    public function preLoad($resource)
    {
        /** @var CompteMoniteur $resource */
        $resource = parent::preLoad($resource);
        /** @var CompteMoniteurRepository $repository */
        $repository = $this->managerRegistry->getRepository(CompteMoniteur::class);
        $repository->setEm($this->managerRegistry->getManager('default'));
        $newId = $repository->getMaxId() + 1;
        $resource->setId($newId);

        return $resource;
    }
}
