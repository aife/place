<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

use App\Service\Migration\Extractor\ExtractorInterface;
use App\Service\Migration\HealthChecker\HealthCheckerInterface;
use App\Service\Migration\Loader\LoaderInterface;
use App\Service\Migration\Transformer\TransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class OrganismeMigrationService extends MigrationService
{
    public const MIGRATION_TABLE_NAME = '__mapping_migration_organisme';
    public const ENTITY_IDENTIFIER = 'acronyme';

    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected EntityManagerInterface $em,
        protected ExtractorInterface $extractor,
        protected HealthCheckerInterface $healthChecker,
        protected TransformerInterface $transformer,
        protected LoaderInterface $loader,
        protected MigrationTableMappingService $tableMappingService,
        protected PropertyAccessorInterface $propertyAccessor,
        protected ContainerInterface $container
    ) {
        $this->entityManager = $this->managerRegistry->getManager('default');
    }

    public function addConfiguredMapping()
    {
        $jsonConfig = $this->container->getParameter('MIGRATION_ORGANISME_MAPPING');

        if (empty($jsonConfig)) {
            return;
        }

        $configuredMapping = json_decode($jsonConfig, true);

        foreach ($configuredMapping as $mapping) {
            $idSource = $mapping['idSource'];
            $idMain = $mapping['idMain'];

            $existingIdMain = $this->tableMappingService->findMapping(static::MIGRATION_TABLE_NAME, $idSource);

            if (!empty($existingIdMain)) {
                continue;
            }

            $this->tableMappingService->insert(static::MIGRATION_TABLE_NAME, $idMain, $idSource);
        }
    }
}
