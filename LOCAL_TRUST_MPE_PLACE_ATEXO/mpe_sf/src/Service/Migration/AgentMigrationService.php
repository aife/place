<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

use App\Entity\Agent;
use App\Repository\AgentRepository;

class AgentMigrationService extends MigrationService
{
    public const MIGRATION_TABLE_NAME = '__mapping_migration_agent';
    public const ENTITY_IDENTIFIER = 'id';

    public function addExistantMapping()
    {
        /** @var AgentRepository $agentRepository */
        $entityManager = $this->managerRegistry->getManager('default');
        $agentRepository = $entityManager->getRepository(Agent::class);
        $agentRepository->setEm($entityManager);
        $existantLogins = $agentRepository->findAllLogins();
        $entityManagerToMigrate = $this->managerRegistry->getManager('database_to_migrate');
        /** @var AgentRepository $agentRepository */
        $agentSourceRepository = $entityManagerToMigrate->getRepository(Agent::class);
        $agentSourceRepository->setEm($entityManagerToMigrate);
        $agentToMap = $agentSourceRepository->findBy(['login' => $existantLogins]);
        $agentRepository->setEm($entityManager);

        /** @var Agent $sourceAgent */
        foreach ($agentToMap as $sourceAgent) {
            $mapping = $this->tableMappingService->findMapping(static::MIGRATION_TABLE_NAME, $sourceAgent->getId());

            if (!empty($mapping)) {
                continue;
            }

            $agent = $agentRepository->findOneBy(['login' => $sourceAgent->getLogin()]);

            $this->tableMappingService->insert(static::MIGRATION_TABLE_NAME, $agent->getId(), $sourceAgent->getId());
        }
    }
}
