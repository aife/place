<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

use Doctrine\Persistence\ManagerRegistry;

class MigrationTableMappingService
{
    public function __construct(
        protected ManagerRegistry $managerRegistry,
    ) {
        $this->entityManager = $this->managerRegistry->getManager('default');
        $this->entityManagerToMigrate = $this->managerRegistry->getManager('database_to_migrate');
    }

    public function insert($tableName, $idMain, $idSource)
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare(
            'INSERT INTO `' . $tableName .
            '` (`id_main`, `id_source`) VALUE (:id_main, :id_source);'
        );
        $stmt->bindValue(':id_main', $idMain);
        $stmt->bindValue(':id_source', $idSource);
        $stmt->execute();
    }

    public function findMapping(string $tableName, $sourceIdentifier)
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare(
            'SELECT `id_main` FROM `' . $tableName .
            '` WHERE `id_source` = :id_source;'
        );
        $stmt->bindValue(':id_source', $sourceIdentifier);
        $stmt->executeStatement();

        return $stmt->fetchOne();
    }

    public function findOldMapping(string $tableName, $mainIdentifier)
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare(
            'SELECT `id_source` FROM `' . $tableName .
            '` WHERE `id_main` = :id_main;'
        );
        $stmt->bindValue(':id_main', $mainIdentifier);
        $stmt->executeStatement();

        return $stmt->fetchOne();
    }

    public function verify($tableName): bool
    {
        if (!$this->tableExist($tableName)) {
            $this->createTable($tableName);

            return false;
        }

        return true;
    }

    protected function tableExist($tableName): bool
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare(
            'SELECT * FROM `information_schema`.`tables` WHERE TABLE_NAME = "' .
            $tableName . '";'
        );
        $stmt->executeStatement();

        if ($stmt->rowCount() === 0) {
            return false;
        }

        return true;
    }

    protected function createTable($tableName): void
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare(
            'CREATE TABLE IF NOT EXISTS `' . $tableName . '` (
          `id_main` VARCHAR(50) NOT NULL,
          `id_source` VARCHAR(50) NOT NULL,
          PRIMARY KEY (`id_main`,`id_source`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'
        );
        $stmt->executeStatement();
    }

    public function findAllOldIdentifiers($tableName)
    {
        $connection = $this->entityManager->getConnection();
        $stmt = $connection->prepare('SELECT `id_source` FROM `' . $tableName . '`;');
        $stmt->executeStatement();

        return $stmt->fetchFirstColumn();
    }
}
