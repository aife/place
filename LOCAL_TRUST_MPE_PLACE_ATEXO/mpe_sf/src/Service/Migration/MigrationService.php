<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

use App\Service\Migration\Extractor\ExtractorInterface;
use App\Service\Migration\HealthChecker\HealthCheckerInterface;
use App\Service\Migration\Loader\LoaderInterface;
use App\Service\Migration\Transformer\TransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

/**
 * Classe de base pour la migration de données entre 2 plateformes
 * Etapes :
 * 1 - Création de classe xxxMigrationService et définition de constantes
 * 2 - Création des classes Extractor/HealthChecker/Transformer/Loader
 * 3 - Déclaration du xxxMigrationService créé dans services.yaml
 * 4 - Ajout d'une fonction findAllByIdentifierNotIn + setEm dans le repository dédié
 * 5 - Dans la classe entité remplir FOREIGN_KEYS avec les infos nécessaires
 * 5 - Ajouter notre service de migration dans MigrationPlateformeCommand mpe:migration:plateforme
 */
abstract class MigrationService implements MigrationServiceInterface
{
    public const MIGRATION_TABLE_NAME = '';
    public const ENTITY_IDENTIFIER = 'id';
    protected $sourceIdentifierValue = null;

    protected ObjectManager $entityManager;

    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected EntityManagerInterface $em,
        protected ExtractorInterface $extractor,
        protected HealthCheckerInterface $healthChecker,
        protected TransformerInterface $transformer,
        protected LoaderInterface $loader,
        protected MigrationTableMappingService $tableMappingService,
        protected PropertyAccessorInterface $propertyAccessor
    ) {
        $this->entityManager = $this->managerRegistry->getManager('default');
    }

    public function extract($identifier = null, $batchSize = 200, $identifiersToIgnore = [])
    {
        return $this->extractor->extract($identifier, static::MIGRATION_TABLE_NAME, $batchSize, $identifiersToIgnore);
    }

    public function healthCheck()
    {
        return $this->healthChecker->healthCheck();
    }

    public function transform($resource)
    {
        $this->sourceIdentifierValue = $this->getIdentifierValue($resource);

        return $this->transformer->transform($resource);
    }

    public function load($resource)
    {
        $resource = $this->loader->load($resource);
        $mainIdentifier = $this->getIdentifierValue($resource);
        $this->tableMappingService->insert(static::MIGRATION_TABLE_NAME, $mainIdentifier, $this->sourceIdentifierValue);
        $this->loader->postLoad($resource);

        return $resource;
    }

    public function migrate($resource)
    {
        $resource = $this->transform($resource);

        return $this->load($resource);
    }

    public function silentMigrate($identifier)
    {
        $entity = $this->extract($identifier);

        $entity = $this->transform($entity);

        $resource = $this->loader->load($entity);

        $mainIdentifier = $this->getIdentifierValue($resource);

        $this->tableMappingService->insert(static::MIGRATION_TABLE_NAME, $mainIdentifier, $this->sourceIdentifierValue);
    }

    public function mapResource($mainIdentifier, $sourceIdentifier)
    {
        $this->tableMappingService->insert(static::MIGRATION_TABLE_NAME, $mainIdentifier, $sourceIdentifier);
    }

    public function getIdentifierValue($resource)
    {
        if (is_array(static::ENTITY_IDENTIFIER)) {
            $identifier = [];
            foreach (static::ENTITY_IDENTIFIER as $partialIdentifier) {
                $identifier[] = $this->propertyAccessor->getValue($resource, $partialIdentifier);
            }

            return implode(';', $identifier);
        }

        return $this->propertyAccessor->getValue($resource, static::ENTITY_IDENTIFIER);
    }

    public function getResourceCount()
    {
        return $this->extractor->getResourceCount(static::MIGRATION_TABLE_NAME);
    }
}
