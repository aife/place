<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\HealthChecker;

class TypeProcedureOrganismeHealthChecker extends HealthChecker
{
    public function healthCheck()
    {
        $reports = [];

        $connection = $this->entityManagerToMigrate->getConnection();
        $sql = '
            SELECT tpo.id_type_procedure, tpo.organisme
            FROM Type_Procedure_Organisme tpo
            WHERE tpo.organisme = "";
        ';

        $stmt = $connection->prepare($sql);
        $results = $stmt->executeQuery()->fetchFirstColumn();

        foreach ($results as $identifier) {
            $reports['reports'][] = sprintf(
                'DONNEE INCORRECTE : Organisme non définit sur Type_Procedure_Organisme ID : %s',
                $identifier
            );
            $reports['identifiersToIgnore'][] = (string)$identifier;
        }

        return $reports;
    }
}
