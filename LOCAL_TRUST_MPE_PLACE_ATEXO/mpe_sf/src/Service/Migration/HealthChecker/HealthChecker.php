<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\HealthChecker;

use App\Doctrine\DatabaseToMigrate;
use App\Service\Migration\MigrationTableMappingService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

abstract class HealthChecker implements HealthCheckerInterface
{
    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected EntityManagerInterface $entityManager,
        protected DatabaseToMigrate $entityManagerToMigrate,
        protected ContainerInterface $container,
        protected PropertyAccessorInterface $propertyAccessor,
        protected MigrationTableMappingService $tableMappingService
    ) {
        $this->objectManager = $this->managerRegistry->getManager('default');
        $this->objectManagerToMigrate = $this->managerRegistry->getManager('database_to_migrate');
    }

    public function healthCheck()
    {
        return [];
    }
}
