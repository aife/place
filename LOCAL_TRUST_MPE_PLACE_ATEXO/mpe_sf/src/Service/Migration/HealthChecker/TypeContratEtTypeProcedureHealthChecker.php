<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\HealthChecker;

class TypeContratEtTypeProcedureHealthChecker extends HealthChecker
{
    public function healthCheck()
    {
        $reports = [];

        $connection = $this->entityManagerToMigrate->getConnection();
        $sql = '
            SELECT tctp.id_type_contrat_et_procedure
            FROM t_type_contrat_et_procedure tctp 
                LEFT JOIN Type_Procedure_Organisme tpo ON tctp.id_type_procedure = tpo.id_type_procedure
                                                              AND tctp.organisme = tpo.organisme
            WHERE tpo.id_type_procedure IS NULL;
        ';

        $stmt = $connection->prepare($sql);
        $results = $stmt->executeQuery()->fetchFirstColumn();

        foreach ($results as $identifier) {
            $reports['reports'][] = sprintf(
                'RELATION ORM INTROUVABLE : Entitée non disponible dans la base de données de destination ' .
                'pour l\'entité TypeContratEtTypeProcedure id_type_contrat_et_procedure : %s',
                $identifier
            );
            $reports['identifiersToIgnore'][] = (string)$identifier;
        }

        return $reports;
    }
}
