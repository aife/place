<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

use App\Service\Migration\Extractor\Extractor;
use App\Service\Migration\Extractor\ExtractorInterface;
use App\Service\Migration\HealthChecker\HealthCheckerInterface;
use App\Service\Migration\Loader\LoaderInterface;
use App\Service\Migration\Transformer\TransformerInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class CategorieINSEEMigrationService extends MigrationService
{
    public const MIGRATION_TABLE_NAME = '__mapping_migration_categorieINSEE';

    public function extract($identifier = null, $batchSize = 200, $identifiersToIgnore = [])
    {
        if (empty($identifier)) {
            return false;
        }

        return $this->extractor->extract($identifier);
    }
}
