<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

interface MigrationServiceInterface
{
    public function extract($identifier = null, $batchSize = 200, $identifiersToIgnore = []);

    public function healthCheck();

    public function transform($resource);

    public function load($resource);

    public function silentMigrate($identifier);
}
