<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Transformer;

use App\Entity\Agent;

class AgentTransformer extends Transformer
{
    public function transform($resource)
    {
        $suffix = $this->container->getParameter('MIGRATION_ORGANISME_SUFFIXE');
        /** @var Agent $resource */
        $resource->setLogin($resource->getLogin() . '_' .  $suffix);

        return $resource;
    }
}
