<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Transformer;

use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Transformer implements TransformerInterface
{
    public function __construct(
        protected ContainerInterface $container
    ) {
    }

    public function transform($resource)
    {
        return $resource;
    }
}
