<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Transformer;

use App\Entity\Organisme;

class OrganismeTransformer extends Transformer
{
    public function transform($resource)
    {
        $suffix = $this->container->getParameter('MIGRATION_ORGANISME_SUFFIXE');
        $jsonConfig = $this->container->getParameter('MIGRATION_ORGANISME_FORCED_ACRONYMES');

        $sourceAcronyme = $resource->getAcronyme();
        $acronyme = $sourceAcronyme . $suffix;

        if (!empty($jsonConfig)) {
            $forcedAcronymes = json_decode($jsonConfig, true);

            $sourceIds = array_column($forcedAcronymes, 'idSource');

            if (in_array($sourceAcronyme, $sourceIds)) {
                $key = array_search($sourceAcronyme, $sourceIds);
                $acronyme = $forcedAcronymes[$key]['idMain'];
            }
        }

        /** @var Organisme $resource */
        $resource->setAcronyme($acronyme);

        return $resource;
    }
}
