<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Extractor;

use App\Entity\Organisme;

class OrganismeExtractor extends Extractor implements ExtractorInterface
{
    public const ENTITY_CLASS = Organisme::class;

    public function extract($identifier = null, $migrationTableName = '', $batchSize = 200, $identifiersToIgnore = [])
    {
        $organismesToMigrateRepository = $this->entityManagerToMigrate->getRepository(Organisme::class);
        $organismesToMigrateRepository->setEm($this->entityManagerToMigrate);

        if (!empty($identifier)) {
            return $organismesToMigrateRepository->findByAcronymeOrganisme($identifier);
        }

        return parent::extract($identifier, $migrationTableName, $batchSize, $identifiersToIgnore);
    }
}
