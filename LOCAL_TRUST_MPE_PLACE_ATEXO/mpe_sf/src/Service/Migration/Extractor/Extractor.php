<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Extractor;

use App\Doctrine\DatabaseToMigrate;
use App\Service\Migration\MigrationTableMappingService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Extractor implements ExtractorInterface
{
    public const ENTITY_CLASS = '';

    public function __construct(
        protected ManagerRegistry $managerRegistry,
        protected ContainerInterface $container,
        protected MigrationTableMappingService $tableMappingService
    ) {
        $this->entityManager = $this->managerRegistry->getManager('default');
        $this->entityManagerToMigrate = $this->managerRegistry->getManager('database_to_migrate');
    }

    public function extract($identifier = null, $migrationTableName = '', $batchSize = 200, $identifiersToIgnore = [])
    {
        $resourceToMigrateRepository = $this->entityManagerToMigrate->getRepository(static::ENTITY_CLASS);
        $resourceToMigrateRepository->setEm($this->entityManagerToMigrate);
        $existantResourceIdentifiers = $this->getExistantIdentifiers($migrationTableName);
        $existantResourceIdentifiers = array_merge($existantResourceIdentifiers, $identifiersToIgnore);

        return $resourceToMigrateRepository->findAllByIdentifierNotIn($existantResourceIdentifiers, $batchSize);
    }

    public function getResourceCount($migrationTableName = '')
    {
        $resourceToMigrateRepository = $this->entityManagerToMigrate->getRepository(static::ENTITY_CLASS);
        $resourceToMigrateRepository->setEm($this->entityManagerToMigrate);
        $existantResourceIdentifiers = $this->getExistantIdentifiers($migrationTableName);

        return $resourceToMigrateRepository->countByIdentifierNotIn($existantResourceIdentifiers);
    }

    protected function getExistantIdentifiers($migrationTableName = '')
    {
        return $this->tableMappingService->findAllOldIdentifiers($migrationTableName);
    }
}
