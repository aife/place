<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Extractor;

use App\Entity\Agent;

class AgentExtractor extends Extractor implements ExtractorInterface
{
    public const ENTITY_CLASS = Agent::class;
}
