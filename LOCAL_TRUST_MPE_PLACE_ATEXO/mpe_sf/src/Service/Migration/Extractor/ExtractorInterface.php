<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Extractor;

interface ExtractorInterface
{
    public function extract($identifier = null, $migrationTableName = '', $batchSize = 200, $identifiersToIgnore = []);

    public function getResourceCount($migrationTableName = '');
}
