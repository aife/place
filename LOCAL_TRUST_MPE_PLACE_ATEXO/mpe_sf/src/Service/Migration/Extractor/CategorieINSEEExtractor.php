<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration\Extractor;

use App\Entity\CategorieINSEE;

class CategorieINSEEExtractor extends Extractor implements ExtractorInterface
{
    public const ENTITY_CLASS = CategorieINSEE::class;

    public function extract($identifier = null, $migrationTableName = '', $batchSize = 200, $identifiersToIgnore = [])
    {
        $categorieINSEEToMigrateRepository = $this->entityManagerToMigrate->getRepository(CategorieINSEE::class);
        $categorieINSEEToMigrateRepository->setEm($this->entityManagerToMigrate);

        return $categorieINSEEToMigrateRepository->find($identifier);
    }
}
