<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Migration;

class ServiceMigrationService extends MigrationService
{
    public const MIGRATION_TABLE_NAME = '__mapping_migration_service';
    public const ENTITY_IDENTIFIER = 'id';
}
