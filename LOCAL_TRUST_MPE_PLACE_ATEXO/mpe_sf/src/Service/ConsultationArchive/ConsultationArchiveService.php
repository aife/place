<?php

namespace App\Service\ConsultationArchive;

use Doctrine\DBAL\Exception as DBALException;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Consultation;
use App\Entity\ConsultationArchive\ConsultationArchive;
use App\Entity\ConsultationArchive\ConsultationArchiveAtlas;
use App\Entity\ConsultationArchive\ConsultationArchiveBloc;
use App\Entity\ConsultationArchive\ConsultationArchiveFichier;
use App\Utils\Filesystem\Adapter\Filesystem;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConsultationArchiveService
{
    protected ?\DateTime $newdate = null;

    protected $output;

    protected ?Filesystem $filesystem = null;

    protected string|\DateTime $formatDate = 'Ymd-h:i:s';

    /**
     * ConsultationArchiveService constructor.
     *
     * @param EntityManager|null $em
     *
     * @throws Exception
     */
    public function __construct(
        protected EntityManagerInterface $em,
        protected ContainerInterface $container,
        protected LoggerInterface $logger
    ) {
        $this->logger->info('Début');
    }

    /**
     * statistiques:
     * - nbre de renvoi à faire
     * - blocs manquants
     * - consultations flagguées comme archivées et non présentes.
     *
     * @return bool
     *
     * @throws Exception
     */
    public function populate(array $consultations = null)
    {
        $consultation = null;
        $consultationArchives = [];
        $consultationArchivesFichier = [];
        $poidsArchive = 0;
        /* @var Consultation $consultation */
        foreach ($consultations as $consultationArr) {
            $consultation = new Consultation();
            $consultation->setAcronymeOrg($consultationArr['acronymeOrg']);
            $consultation->setReference($consultationArr['reference']);
            $consultation->setDatedebut($consultationArr['datedebut']);
            $msg = sprintf(
                'Traitement de la consultation %s - %s ',
                $consultation->getId(),
                $consultation->getAcronymeOrg()
            );
            $this->logger->info($msg);

            /* @var Consultation $consultation */
            $criterias = [
                'consultationRef' => $consultation->getId(),
                'organisme' => $consultation->getAcronymeOrg(),
            ];
            $consultationsArchiveAtlas = $this->em->getRepository(ConsultationArchiveAtlas::class)
                ->findBy($criterias, ['numeroBloc' => 'ASC']);

            $consultationsArchiveAtlasFiltered = $this->filter($consultationsArchiveAtlas);

            try {
                /** @var ConsultationArchiveFichier $consultationArchiveFichier */
                $consultationArchiveFichier = $this->getCheminFichier($consultation);
                $consultationArchivesFichier[] = $consultationArchiveFichier;
                $cheminFichier = '';
                $poidsArchive = null;
                if (!empty($consultationArchiveFichier)) {
                    $cheminFichier = $consultationArchiveFichier->getCheminFichier();
                    $poidsArchive = $consultationArchiveFichier->getPoids();
                }

                if (empty($consultationsArchiveAtlasFiltered)
                    || false === $this->archiveComplete($consultationsArchiveAtlasFiltered)
                ) {
                    if (!empty($cheminFichier)) {
                        $msg = sprintf(
                            'Consultation existante en local! Elle va être renvoyée: %s - %s ',
                            $consultation->getId(),
                            $consultation->getAcronymeOrg()
                        );
                        $status = 'LOCAL_EXISTS';
                        if ($this->getSaeEnvoi($consultation)) {
                            $statusGlobalTransmission = $this->container
                                ->getParameter('ARCHIVE_TRANSMISSION_NON_DEMARREE');
                        } else {
                            $statusGlobalTransmission = $this->container
                                ->getParameter('ARCHIVE_TRANSMISSION_NON_CONCERNEE');
                        }

                        $dateArchivage = new DateTime($consultation->getDateArchivage());
                        $this->persistConsultationArchive(
                            $dateArchivage,
                            $consultation,
                            null,
                            $statusGlobalTransmission,
                            $cheminFichier,
                            $poidsArchive
                        );

                        $this->logger->info($msg);
                    } else {
                        $statusGlobalTransmission = $this->setEtatConsultation($consultation);
                        $dateArchivage = new DateTime($consultation->getDateArchivage());
                        $this->persistConsultationArchive(
                            $dateArchivage,
                            $consultation,
                            null,
                            $statusGlobalTransmission,
                            '',
                            null
                        );
                        $status = 'A_ARCHIVER';
                    }
                } else {
                    if ($this->getSaeEnvoi($consultation)) {
                        $statusGlobalTransmission = $this->container
                            ->getParameter('ARCHIVE_TRANSMISSION_FINALISEE');
                    } else {
                        $statusGlobalTransmission = $this->container
                            ->getParameter('ARCHIVE_TRANSMISSION_NON_CONCERNEE');
                    }
                    $consultationArchives[] = $this->saveConsultationArchive(
                        $consultationsArchiveAtlasFiltered,
                        $consultation,
                        $statusGlobalTransmission,
                        $cheminFichier
                    );
                    $msg = sprintf(
                        'Cette consultation est complète: %s - %s',
                        $consultation->getId(),
                        $consultation->getAcronymeOrg()
                    );
                    $status = 'COMPLETE';
                    $this->logger->info($msg);
                }
            } catch (Exception $e) {
                $msg = sprintf(
                    'Error Consultation (reference: %s, %s): ',
                    $consultation->getId(),
                    $e->getMessage()
                );
                $this->logger->error($msg);
                $statusGlobalTransmission = $this->setEtatConsultation($consultation);
                $status = 'A_ARCHIVER';
                $dateArchivage = new DateTime($consultation->getDateArchivage());
                $this->persistConsultationArchive(
                    $dateArchivage,
                    $consultation,
                    null,
                    $statusGlobalTransmission,
                    '',
                    null
                );
            }

            $this->stat($this->newdate, $consultation, $status, $statusGlobalTransmission, $poidsArchive);

            unset($consultationsArchiveAtlasFiltered);
            foreach ($consultationsArchiveAtlas as $consultationArchiveAtlas) {
                $this->em->detach($consultationArchiveAtlas);
            }
            $this->em->detach($consultation);
            if (isset($consultationArr)) {
                unset($consultationArr);
            }
        }

        try {
            $this->em->flush();
        } catch (Exception $e) {
            $msg = sprintf(
                'Error Consultation (reference: %s, %s): ',
                $consultation->getId(),
                $e->getMessage()
            );
            $this->logger->error($msg.' => '.$e->getMessage());
        }
        unset($consultations);
        foreach ($consultationArchivesFichier as $entity) {
            if (!empty($entity)) {
                $this->em->clear($entity);
            }
        }

        $this->detachEntities($consultationArchives);
        unset($consultations);
        unset($consultationArchives);
        unset($consultationArchivesFichier);

        return true;
    }

    /**
     * @return bool|mixed
     *
     * @throws DBALException
     */
    public function setEtatConsultation(Consultation $consultation)
    {
        if (empty($consultation)) {
            return false;
        }
        $msg = sprintf(
            'Consultation à archiver et renvoyer: %s - %s',
            $consultation->getId(),
            $consultation->getAcronymeOrg()
        );

        $this->logger->info($msg);
        $statusGlobalTransmission = $this->container
            ->getParameter('ARCHIVE_TRANSMISSION_NON_DEMARREE');

        if ($this->getSaeEnvoi($consultation)) {
            $sql = 'update consultation set id_etat_consultation = ? where reference = ? AND organisme = ?';
            $params = [
                $this->container->getParameter('STATUS_A_ARCHIVER'),
                $consultation->getId(),
                $consultation->getAcronymeOrg(),
            ];
            $conn = $this->em->getConnection();
            $conn->executeUpdate($sql, $params);
        } else {
            $statusGlobalTransmission = $this->container
                ->getParameter('ARCHIVE_TRANSMISSION_NON_CONCERNEE');
        }

        return $statusGlobalTransmission;
    }

    /**
     * @return bool
     */
    public function getSaeEnvoi(Consultation $consultation)
    {
        $configurationOrganisme = $this->em
            ->getRepository(ConfigurationOrganisme::class)
            ->findOneBy(['organisme' => $consultation->getAcronymeOrg()]);

        $res = false;
        if (!empty($configurationOrganisme)) {
            $res = (bool) $configurationOrganisme->getArchivageConsultationSaeExterneEnvoiArchive();
        }

        return $res;
    }

    /**
     * @param $datas
     */
    public function detachEntities($datas)
    {
        foreach ($datas as $consultationArchive) {
            foreach ($consultationArchive as $type) {
                foreach ($type as $entity) {
                    $this->em->clear($entity);
                }
            }
        }
        unset($datas);
    }

    /**
     * @param string $status
     * @param string $cheminFichier
     *
     * @return array
     */
    public function saveConsultationArchive(
        array $consultationsArchiveAtlas,
        Consultation $consultation,
        $status = 'FINALISE',
        $cheminFichier = ''
    ) {
        $res = [];
        $consultationsArchive = [];
        $consultationsArchiveBloc = [];
        $consultationArchiveTmp = null;
        $tmp = '';
        $nombreBloc = count($consultationsArchiveAtlas);
        $poids = 0;
        $count = 0;
        /* @var ConsultationArchiveAtlas $consultationArchiveAtlas */
        foreach ($consultationsArchiveAtlas as $consultationArchiveAtlas) {
            ++$count;
            try {
                $poids += $consultationArchiveAtlas->getTaille();
                $unique = $consultationArchiveAtlas->getConsultationRef().$consultationArchiveAtlas->getOrganisme();
                if ($tmp != $unique) {
                    $consultationArchive = $this->persistConsultationArchive(
                        $consultationArchiveAtlas->getDateEnvoi(),
                        $consultation,
                        $nombreBloc,
                        $status,
                        $cheminFichier,
                        $poids,
                        true
                    );

                    $tmp = $consultationArchive->getConsultationRef().$consultationArchive->getOrganisme();
                    $consultationsArchive[] = $consultationArchive;
                }
                if ($count === count($consultationsArchiveAtlas)) {
                    $consultationArchive->setPoidsArchivage($poids);
                    $this->em->persist($consultationArchive);
                }

                if ($this->container->getParameter('ARCHIVE_TRANSMISSION_NON_DEMARREE') != $status) {
                    try {
                        $consultationArchiveBloc = $this->persistConsultationArchiveBloc(
                            $consultationArchiveAtlas,
                            $consultationArchive
                        );
                    } catch (Exception $e) {
                        $msg = sprintf(
                            'Erreur sur consultationsArchiveAtlas %s : %s',
                            $consultationArchiveAtlas->getId(),
                            $e->getmessage()
                        );
                        $this->logger->error($msg);
                    }
                    $consultationsArchiveBloc[] = $consultationArchiveBloc;

                    unset($consultationArchiveBloc);
                }
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }
        $res['consultationsArchive'] = $consultationsArchive;
        $res['consultationsArchiveBloc'] = $consultationsArchiveBloc;

        return $res;
    }

    /**
     * @param $nombreBloc
     * @param string $status
     * @param string $cheminFichier
     * @param int    $poids
     * @param bool   $statusFragmentation
     *
     * @return ConsultationArchive
     */
    public function persistConsultationArchive(
        DateTime $dateArchivage,
        Consultation $consultation,
        $nombreBloc,
        $status = 'FINALISEE',
        $cheminFichier = '',
        $poids = 0,
        $statusFragmentation = false
    ) {
        $consultationArchive = new ConsultationArchive();
        $consultationArchive->setCheminFichier((string) $cheminFichier);
        $consultationArchive->setOrganisme($consultation->getAcronymeOrg());
        $consultationArchive->setStatusGlobalTransmission($status);
        $consultationArchive->setDateArchivage($dateArchivage);
        $consultationArchive->setNombreBloc($nombreBloc);
        $consultationArchive->setPoidsArchivage(0);
        $consultationArchive->setConsultationRef($consultation->getId());
        $consultationArchive->setPoidsArchivage($poids);
        $consultationArchive->setStatusFragmentation($statusFragmentation);
        $date = $consultation->getDatedebut();
        $anneeCreationConsultation = $date->format('Y');
        $consultationArchive->setAnneeCreationConsultation($anneeCreationConsultation);

        $this->em->persist($consultationArchive);

        return $consultationArchive;
    }

    /**
     * @return ConsultationArchiveBloc
     *
     * @throws Exception
     */
    public function persistConsultationArchiveBloc(
        ConsultationArchiveAtlas $consultationArchiveAtlas,
        ConsultationArchive $consultationArchive
    ) {
        $consultationArchiveBloc = new ConsultationArchiveBloc();
        $consultationArchiveBloc->setDocId($consultationArchiveAtlas->getDocId());
        $consultationArchiveBloc->setNumeroBloc($consultationArchiveAtlas->getNumeroBloc());
        $consultationArchiveBloc->setPoidsBloc($consultationArchiveAtlas->getTaille());
        $consultationArchiveBloc->setDateEnvoiDebut($consultationArchiveAtlas->getDateEnvoi());
        $consultationArchiveBloc->setDateEnvoiFin($consultationArchiveAtlas->getDateEnvoi());
        $consultationArchiveBloc->setStatusTransmission(true);
        $consultationArchiveBloc->setConsultationArchive($consultationArchive);
        $this->em->persist($consultationArchiveBloc);

        return $consultationArchiveBloc;
    }

    /**
     * @return array
     */
    public function getCountConsultationsArchivees()
    {
        return $this->em->getRepository(Consultation::class)->countConsultationsArchivees(
            $this->container->getParameter('STATUS_ARCHIVE_REALISEE')
        );
    }

    /**
     * @return array
     */
    public function getConsultationsArchivees($offset, $limit)
    {
        return $this->em->getRepository(Consultation::class)->findByEtatConsultation(
            $this->container->getParameter('STATUS_ARCHIVE_REALISEE'),
            $offset,
            $limit
        );
    }

    /**
     * enlève des doublons.
     *
     * @return array
     */
    public function filter(array $consultationsArchiveAtlas)
    {
        $count = 0;
        $out = [];

        foreach ($consultationsArchiveAtlas as $consultationArchiveAtlas) {
            if (count($consultationsArchiveAtlas) > 1) {
                if (true === $this->isValid($consultationArchiveAtlas->getDocId())) {
                    $out[] = $consultationArchiveAtlas;
                }
            } else {
                $out[] = $consultationArchiveAtlas;
            }
            ++$count;
        }

        return $out;
    }

    /**
     * @param $docId
     *
     * @return bool
     */
    public function isValid($docId)
    {
        preg_match('#([0-9]+)_([0-9]+)_([a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}+)$#', (string) $docId, $matches2);
        if (!empty($matches2)) {
            if (is_numeric($matches2[2])) {
                return true;
            }
        }

        return false;
    }

    public function statHead(DateTime $date)
    {
        $msg = sprintf(
            'stat-%s,Référence consultation,Organisme,Status Archive Atlas,Status Global Transmission,poids,',
            $date->format($this->formatDate)
        );
        $this->logger->info($msg);
    }

    /**
     * @param $status
     */
    public function stat(DateTime $date, Consultation $consultation, $status, $statusGlobalTransmission, $poidsArchive)
    {
        $msg = sprintf(
            'stat-%s,%s,%s,%s,%s,%s,',
            $date->format($this->formatDate),
            $consultation->getId(),
            $consultation->getAcronymeOrg(),
            $status,
            $statusGlobalTransmission,
            $poidsArchive
        );
        $this->logger->info($msg);
    }

    /**
     * @return ConsultationArchiveFichier
     */
    public function getCheminFichier(Consultation $consultation)
    {
        $criterias = [
            'consultationRef' => $consultation->getId(),
            'organisme' => $consultation->getAcronymeOrg(),
        ];
        /** @var ConsultationArchiveFichier $res */
        $res = $this->em->getRepository(ConsultationArchiveFichier::class)
            ->findOneBy($criterias);

        if (!empty($res)) {
            $this->em->clear($res);
        }

        return $res;
    }

    /**
     * @return bool
     *
     * @throws Exception
     */
    public function archiveComplete(array $consultationsArchiveAtlas)
    {
        $consultationArchiveAtlas = null;
        $nombreBloc = count($consultationsArchiveAtlas);
        $maxBlocSize = $this->getMaxBlocSize();
        $count = 0;

        /* @var ConsultationArchiveAtlas $consultationArchiveAtlas */
        foreach ($consultationsArchiveAtlas as $consultationArchiveAtlas) {
            ++$count;
            if ($count != $consultationArchiveAtlas->getNumeroBloc()) {
                $msg = sprintf('Les numeros de bloc ne sont pas contigus. Le bloc %s est manquant!', $count);
                throw new Exception($msg);
            }
            if ($count < $nombreBloc && $consultationArchiveAtlas->getTaille() < $maxBlocSize) {
                throw new Exception(sprintf('Bloc de taille inférieure à %s', $maxBlocSize));
            }
        }
        if (isset($consultationArchiveAtlas)) {
            if ($consultationArchiveAtlas->getNumeroBloc() === $nombreBloc) {
                if ($consultationArchiveAtlas->getTaille() === $maxBlocSize) {
                    $msg = sprintf('Le dernier Bloc ne devrait pas avoir une taille égale à %s', $maxBlocSize);
                    throw new Exception($msg);
                }
            }
        }

        return true;
    }

    /**
     * @return DateTime
     *
     * @throws Exception
     */
    public function frenchToDateTime(string $frenchDate)
    {
        $french_date_string = str_replace('/', '-', $frenchDate);
        try {
            $date = new DateTime($french_date_string);
            $date->format('Y-m-d H:i');

            return $date;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getMaxBlocSize(): float|int
    {
        $maxLength = (int) $this->container->getParameter('MAX_LENGHT_FILE_TO_SEND_ATLAS');

        return $maxLength * 1024 * 1024;
    }

    /**
     * @return mixed
     */
    public function getFilesystem()
    {
        return $this->filesystem;
    }

    /**
     * @param mixed $filesystem
     */
    public function setFilesystem(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    /**
     * @return mixed
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * @param mixed $output
     */
    public function setOutput($output)
    {
        $this->output = $output;
    }

    public function getNewdate(): DateTime
    {
        return $this->newdate;
    }

    public function setNewdate(DateTime $newdate)
    {
        $this->newdate = $newdate;
    }
}
