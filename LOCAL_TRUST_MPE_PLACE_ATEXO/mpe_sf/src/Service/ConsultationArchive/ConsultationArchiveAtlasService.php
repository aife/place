<?php

namespace App\Service\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchiveAtlas;
use Exception;

class ConsultationArchiveAtlasService extends ConsultationArchiveService
{
    /**
     * alimente la table consultation_archive_atlas
     * sera lancé une fois en production.
     */
    public function populate(array $consultations = null)
    {
        $this->logger->info('Début');
        $row = 0;
        $a = [];
        $pathArchive = $this->container->getParameter('FICHIERS_ARCHIVE_DIR');
        $pathCsvData = $pathArchive.'atlas_consolidation.csv';

        if (($handle = fopen($pathCsvData, 'r')) !== false) {
            $row = 0;
            while (($data = fgetcsv($handle, 1000, ',')) !== false) {
                ++$row;
                if (8 != count($data)) {
                    continue;
                }
                if ('CNT_HASH' === $data[0]) {
                    continue;
                }
                $docId = $data[6];

                try {
                    $consultationArchiveAtlas = $this->extractForArchiveAtlas($docId);
                    $dateEnvoi = $this->frenchToDateTime($data[7]);
                    $consultationArchiveAtlas->setDateEnvoi($dateEnvoi);
                    $consultationArchiveAtlas->setCompId($data[5]);
                    $consultationArchiveAtlas->setTaille((int) $data[2]);
                    $this->em->persist($consultationArchiveAtlas);
                    $a[] = $consultationArchiveAtlas;

                    if (0 == $row % 1000) {
                        $this->em->flush();
                        $this->output->writeln(sprintf('%s enregistrés !', $row));
                        foreach ($a as $e) {
                            $this->em->detach($e);
                        }
                        $this->em->clear();
                        if (0 == $row % 10000) {
                            $this->output->writeln(
                                'Memory usage after: '.(memory_get_usage() / 1024).' KB'.PHP_EOL
                            );
                        }
                        $a = [];
                    }
                } catch (Exception $e) {
                    $this->logger->error('fichier csv ligne : '.$row.' => '.$e->getMessage());
                    continue;
                }
            }
        }

        $this->output->writeln(sprintf('%s are stored !', $row));
        $this->em->flush();
        $this->logger->info('Fin');
    }

    /**
     * parse des docId a parser
     * 122369_1_b4n
     * 43a4n
     * 26302_d4t_27757_26302.
     *
     * @return ConsultationArchiveAtlas
     *
     * @throws Exception
     */
    public function extractForArchiveAtlas(string $docId)
    {
        $numeroBloc = null;
        $accronym = null;
        $consultationRef = null;

        // exemple : 26302_d4t_27757_26302
        preg_match('#(.*)_(.*)_(.*)_(.*)$#', $docId, $matches1);
        if (!empty($matches1)) {
            $numeroBloc = 1;
            $consultationRef = $matches1[1];
            $accronym = $matches1[2];
        }

        // exemple : 122369_1_b4n
        preg_match('#([0-9]+)_([0-9]+)_([a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}+)$#', $docId, $matches2);
        if (!empty($matches2)) {
            $consultationRef = $matches2[1];
            $numeroBloc = $matches2[2];
            $accronym = $matches2[3];
        }

        // exemple : 43a4n
        preg_match('#([0-9]+)([a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}+)$#', $docId, $matches3);
        if (!empty($matches3)) {
            $numeroBloc = 1;
            $consultationRef = $matches3[1];
            $accronym = $matches3[2];
        }

        if (empty($accronym)) {
            throw new Exception(sprintf("Ce DocId (%s) n'est pas valide !", $docId));
        }

        $consultationArchiveAtlas = new ConsultationArchiveAtlas();
        $consultationArchiveAtlas->setDocId($docId);
        $consultationArchiveAtlas->setNumeroBloc($numeroBloc);
        $consultationArchiveAtlas->setOrganisme($accronym);
        $consultationArchiveAtlas->setConsultationRef($consultationRef);

        return $consultationArchiveAtlas;
    }
}
