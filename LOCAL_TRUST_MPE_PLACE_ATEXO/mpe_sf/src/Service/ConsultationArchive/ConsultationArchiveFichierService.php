<?php

namespace App\Service\ConsultationArchive;

use Doctrine\ORM\OptimisticLockException;
use League\Flysystem\FileNotFoundException;
use App\Entity\ConsultationArchive\ConsultationArchiveFichier;
use Exception;

class ConsultationArchiveFichierService extends ConsultationArchiveService
{
    /**
     * @return bool
     *
     * @throws OptimisticLockException
     * @throws FileNotFoundException
     */
    public function populate(array $consultations = null)
    {
        $res = true;
        if (empty($this->filesystem)) {
            throw new \Exception('error filesystem');
        }
        $count = 0;
        $consultationsArchiveFichier = [];
        foreach ($this->filesystem->listContents('./', true) as $file) {
            ++$count;
            $path = $file['path'];
            if (str_ends_with($path, '.zip')) {
                $relativePathName = './'.$path;
                $s = urlencode($relativePathName);
                $s1 = str_replace('%2F', '/', $s);
                preg_match("#\.\/([a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}+|pmi-min-1)\/(.*)_([0-9]+).zip$#", $s1, $matches);

                if (!isset($matches[1])) {
                    $this->logger->error("Error avec l'accronym de ce chemin : ".$relativePathName);
                    $res = false;
                    continue;
                }

                if (!isset($matches[3])) {
                    $this->logger->error('Error avec la reference de ce chemin : '.$relativePathName);
                    $res = false;
                    continue;
                }

                $consultationArchiveFichier = new ConsultationArchiveFichier();
                $consultationArchiveFichier->setConsultationRef($matches[3]);
                $consultationArchiveFichier->setOrganisme($matches[1]);
                $consultationArchiveFichier->setCheminFichier($relativePathName);
                $consultationArchiveFichier->setPoids((int) $this->filesystem->getSize($path));
                $this->em->persist($consultationArchiveFichier);
                $this->output->writeln("$consultationArchiveFichier");
                $consultationsArchiveFichier[] = $consultationArchiveFichier;

                if (0 == $count % 1000) {
                    $this->em->flush();
                    $this->output->writeln(sprintf('%s enregistrés !', $count));
                    foreach ($consultationsArchiveFichier as $e) {
                        $this->em->detach($e);
                    }
                    $this->em->clear();
                    if (0 == $count % 1000) {
                        $this->output->writeln(
                            'Memory usage after: '.(memory_get_usage() / 1024).' KB'.PHP_EOL
                        );
                    }
                    $consultationsArchiveFichier = [];
                }
            }
        }
        $this->em->flush();
        $this->output->writeln(sprintf('%s are stored !', $count));
        $this->output->writeln(
            'Memory usage after: '.(memory_get_usage() / 1024).' KB'.PHP_EOL
        );
        $this->logger->info('Fin');

        return $res;
    }

    /**
     * alimente la table consultation_archive_atlas
     * sera lancé une fois en production.
     */
    public function populateWithFile(array $consultations = null)
    {
        $this->logger->info('Début');
        $row = 0;
        $a = [];
        $root = $this->container->getParameter('FICHIERS_ARCHIVE_DIR');
        $pathCsvData = $root.'fichier-archive.csv';

        if (($handle = fopen($pathCsvData, 'r')) !== false) {
            $row = 0;
            while (($data = fgetcsv($handle, 1000, "\t")) !== false) {
                $d = urlencode(trim($data[0]));
                $s = str_replace('%2F', '/', $d);
                $findme = './';
                $pos = strpos($s, $findme);
                $chemin = substr($s, $pos);
                $poids = substr($s, 0, $pos);

                ++$row;
                try {
                    preg_match(
                        '#^([0-9]{2,}+)+(.*)$#',
                        $poids,
                        $matches
                    );

                    $entity = $this->extractForArchiveFichier($chemin, $matches[1]);
                    $this->em->persist($entity);
                    $a[] = $entity;

                    if (0 == $row % 1000) {
                        $this->em->flush();
                        $this->output->writeln(sprintf('%s enregistrés !', $row));
                        foreach ($a as $e) {
                            $this->em->detach($e);
                        }
                        $this->em->clear();
                        if (0 == $row % 10000) {
                            $this->output->writeln(
                                'Memory usage after: '.(memory_get_usage() / 1024).' KB'.PHP_EOL
                            );
                        }
                        $a = [];
                    }
                } catch (Exception $e) {
                    $this->logger->error('fichier csv ligne : '.$row.' => '.$e->getMessage());
                    continue;
                }
            }
        }

        $this->output->writeln(sprintf('%s are stored !', $row));
        $this->em->flush();
        $this->logger->info('Fin');
    }

    /**
     * parse des chemins.
     *
     * @param string $docId
     *
     * @return ConsultationArchiveAtlas
     *
     * @throws Exception
     */
    public function extractForArchiveFichier(string $str, $poids)
    {
        $s = urlencode($str);
        $accronym = null;
        $consultationRef = null;
        $s1 = str_replace('%2F', '/', $s);
        preg_match(
            "#\.\/([a-zA-Z]{1}[0-9]{1}[a-zA-Z]{1}+|pmi-min-1)\/(.*)_([0-9]+).zip$#",
            $s1,
            $matches
        );
        if (!empty($matches)) {
            $accronym = $matches[1];
        }
        preg_match(
            '#(.*)_([0-9]+).zip$#',
            $s,
            $matches
        );
        if (!empty($matches)) {
            $consultationRef = $matches[2];
        }

        if (empty($accronym)) {
            throw new Exception(sprintf("Ce chemin (%s) n'est pas valide !", $str));
        }
        $entity = new ConsultationArchiveFichier();
        $entity->setConsultationRef($consultationRef);
        $entity->setOrganisme($accronym);
        $entity->setCheminFichier($s1);
        $entity->setPoids($poids);

        return $entity;
    }
}
