<?php

namespace App\Service\ConsultationArchive;

use App\Entity\ConsultationArchive\ConsultationArchive;
use App\Entity\ConsultationArchive\ConsultationArchiveBloc;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Flysystem\FileNotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ConsultationArchiveSplitService extends ConsultationArchiveService
{
    private $poidsMaximum;

    private $chunk;

    private $path;

    /**
     * ConsultationArchiveSplitService constructor.
     *
     * @param EntityManager $em
     *
     * @throws Exception
     */
    public function __construct(
        EntityManagerInterface $em,
        ContainerInterface $container,
        LoggerInterface $logger,
        private int $timeout = 0
    ) {
        parent::__construct($em, $container, $logger);
    }

    /**
     * @return array
     */
    public function findByStatusNonFragmente()
    {
        return $this->em->getRepository(ConsultationArchive::class)
            ->findByStatusNonFragmente();
    }

    /**
     * @param $str
     *
     * @return mixed
     */
    public function cleanPath($str)
    {
        return str_replace('+', ' ', urldecode($str));
    }

    /**
     * @throws FileNotFoundException
     */
    public function populate(array $datas = null): array|bool
    {
        $res = [];
        $poids = 0;
        foreach ($datas as $consultationArchive) {
            if (empty($consultationArchive->getCheminFichier())) {
                continue;
            }
            $chemin = $this->cleanPath($consultationArchive->getCheminFichier());
            if (!$this->filesystem->has($chemin)) {
                $info = sprintf("le fichier %s n'existe pas!", $consultationArchive->getCheminFichier());
                $this->logger->info($info);
                continue;
            }
            $poids += $this->filesystem->getSize($chemin);
            if ($poids > $this->poidsMaximum) {
                $info = sprintf('Le seuil de %s a été atteint', $this->poidsMaximum);
                $this->logger->info($info);
                break;
            }
            $res[] = $this->split($consultationArchive);
        }

        return $res;
    }

    /**
     * @return array
     */
    public function split(ConsultationArchive $consultationArchive)
    {
        $res = [];
        $filepath = $this->cleanPath($consultationArchive->getCheminFichier());
        $consultationsArchiveBloc = $this->getConsultationsArchiveBloc($consultationArchive);
        $absolutePath = $this->cleanPath($this->path.$filepath);
        try {
            if (!$this->filesystem->has($filepath)) {
                throw new FileNotFoundException($filepath);
            }
            $files = $this->filesystem->split($absolutePath, $this->getChunk(), $this->getTimeOut());

            foreach ($files as $file) {
                $poids = $file['size'];
                $numeroBloc = $this->extractNumeroBloc($file['path']) + 1;

                $id = $consultationArchive->getConsultationId();
                if ($consultationArchive->getConsultationRef()) {
                    $id = $consultationArchive->getConsultationRef();
                }

                $docId = $this->calculateDocId(
                    $id,
                    $numeroBloc,
                    $consultationArchive->getOrganisme()
                );

                if (isset($consultationsArchiveBloc[$docId])) {
                    $consultationArchiveBloc = $consultationsArchiveBloc[$docId];
                } else {
                    $consultationArchiveBloc = new ConsultationArchiveBloc();
                }

                $consultationArchiveBloc->setStatusTransmission(false);
                $consultationArchiveBloc->setDocId($docId);
                $consultationArchiveBloc->setCompId($id);
                $consultationArchiveBloc->setPoidsBloc($poids);
                $consultationArchiveBloc->setConsultationArchive($consultationArchive);
                $consultationArchiveBloc->setNumeroBloc($numeroBloc);
                $consultationArchiveBloc->setDateEnvoiDebut(new DateTime());
                $consultationArchiveBloc->setDateEnvoiFin(new DateTime());
                $consultationArchiveBloc->setCheminFichier($file['path']);
                $this->em->persist($consultationArchiveBloc);
                $res[] = $consultationArchiveBloc;
            }
            $numeroBloc = is_countable($files) ? count($files) : 0;
            $consultationArchive->setNombreBloc($numeroBloc);
            $consultationArchive->setStatusFragmentation(true);
            $this->em->flush();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            // suppression des fichiers fragmentés (ex: _archives_16IMT001M_2.zip-000006)
            // en cas d'erreur
            $this->filesystem->remove($filepath.'-*');
        }

        return $res;
    }

    /**
     * @return array
     */
    public function getConsultationsArchiveBloc(ConsultationArchive $consultationArchive)
    {
        $res = [];
        $criterias = ['consultationArchive' => $consultationArchive];
        $consultationsArchiveBloc = $this->em->getRepository(ConsultationArchiveBloc::class)
            ->findBy($criterias);
        /** @var ConsultationArchiveBloc $consultationArchiveBloc */
        foreach ($consultationsArchiveBloc as $consultationArchiveBloc) {
            $res[$consultationArchiveBloc->getDocId()] = $consultationArchiveBloc;
        }

        return $res;
    }

    /**
     * @param $str
     *
     * @return mixed
     *
     * @throws Exception
     */
    public function extractNumeroBloc($str)
    {
        preg_match('#^(.*)-([0-9]+)$#', (string) $str, $matches);
        if (isset($matches[2])) {
            return (int) $matches[2];
        } else {
            throw new Exception('Numéro de bloc non extractible pour le chemin : '.$str);
        }
    }

    /**
     * @param $consultation_id
     * @param $blocId
     * @param $organisme
     *
     * @return string
     */
    public function calculateDocId($consultation_id, $blocId, $organisme)
    {
        return sprintf(
            "%s_%s_%s",
            $consultation_id,
            $blocId,
            $organisme
        );
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPoidsMaximum()
    {
        return $this->poidsMaximum;
    }

    /**
     * @param mixed $poidsMaximum
     */
    public function setPoidsMaximum($poidsMaximum)
    {
        $this->poidsMaximum = $poidsMaximum;
    }

    /**
     * @return mixed
     *
     * @throws Exception
     */
    public function getChunk()
    {
        if (is_int($this->chunk) && $this->chunk > 0) {
            return $this->chunk;
        }
        throw new Exception("La variable 'Chunk' doit être un entier positif !");
    }

    /**
     * @param mixed $chunk
     */
    public function setChunk($chunk)
    {
        $this->chunk = $chunk;
    }

    public function getTimeOut()
    {
        return $this->timeout;
    }

    public function setTimeOut($timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }
}
