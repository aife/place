<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Organisme;

use App\Entity\Organisme;
use Doctrine\ORM\EntityManagerInterface;

class SearchOrganismeTools
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function mapFilters(array $data): array
    {
        if (isset($data['keyword'])) {
            $data['denominationOrg'] = $data['keyword'];
            unset($data['keyword']);
        }

        if (isset($data['entitePublique'])) {
            $data['id'] = $data['entitePublique']->id;
            unset($data['entitePublique']);
        }

        return $data;
    }

    public function getOrganismes($wsResponse): array
    {
        $idsList = [];
        foreach ($wsResponse->{'hydra:member'} as $item) {
            $idsList[] = $item->{'id'};
        }

        return $this->entityManager->getRepository(Organisme::class)
            ->getOrganismsForSearchPage($idsList);
    }
}
