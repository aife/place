<?php

namespace App\Service;

use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Repository\Referentiel\Consultation\ClausesN1Repository;
use App\Repository\Referentiel\Consultation\ClausesN2Repository;
use App\Repository\Referentiel\Consultation\ClausesN3Repository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;

class ClausesMigrationService
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ClausesN1Repository $clausesRefN1Repository,
        private readonly ClausesN2Repository $clausesRefN2Repository,
        private readonly ClausesN3Repository $clausesRefN3Repository,
        private readonly LoggerInterface $logger
    ) {
    }

    public function migrateClauses(Consultation|Lot|ContratTitulaire $object): void
    {
        $clausesReferences = $this->getClausesReferences();

        try {
            $this->migrateClausesSociales($object, $clausesReferences);
            $this->migrateClausesEnvironnementales($object, $clausesReferences);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    private function migrateClausesSociales(Consultation|Lot|ContratTitulaire $object, array $clausesReferences): void
    {
        //Migration de la partie considérations sociales

        if ($object->getClauseSociale() === '1') {
            $clauseN1 = new Consultation\ClausesN1();
            if ($object instanceof Consultation) {
                $clauseN1->setConsultation($object);
            } elseif ($object instanceof Lot) {
                $clauseN1->setLot($object);
            } else {
                $clauseN1->setContrat($object);
            }
            $clauseN1->setReferentielClauseN1($clausesReferences['n1'][1]);
            $this->entityManager->persist($clauseN1);

            //Partie considérations sociales : checkbox 1, 2 et 3
            $clausesN2WithSerializedArray = [
                1 => 'ClauseSocialeConditionExecution',
                2 => 'ClauseSpecificationTechnique',
                3 => 'ClauseSocialeInsertion'
            ];
            foreach ($clausesN2WithSerializedArray as $key => $value) {
                $method = "get{$value}";
                $serializedArray = $object->$method();
                if (!empty($serializedArray) && str_starts_with($serializedArray, 'a:')) { //Check si la valeur est bien un array sérialisé
                    $clauseN2 = new Consultation\ClausesN2();
                    $clauseN2->setClauseN1($clauseN1);
                    $clauseN2->setReferentielClauseN2($clausesReferences['n2'][$key]);
                    $this->entityManager->persist($clauseN2);

                    $arrayWithClausesN3 = unserialize($serializedArray);
                    foreach ($arrayWithClausesN3 as $idClauseN3) {
                        if ($idClauseN3 >= 1 && $idClauseN3 <= 9) {
                            $clauseN3 = new Consultation\ClausesN3();
                            $clauseN3->setClauseN2($clauseN2);
                            $clauseN3->setReferentielClauseN3($clausesReferences['n3'][$idClauseN3]);
                            $this->entityManager->persist($clauseN3);
                        }
                    }
                }
            }

            //Partie considérations sociales : checkbox 4
            $clausesN3ForClauseN2Number4 = [
                7 => 'ClauseSocialeAteliersProteges',
                8 => 'ClauseSocialeSiae',
                9 => 'ClauseSocialeEss'
            ];
            foreach ($clausesN3ForClauseN2Number4 as $key => $value) {
                $method = "get{$value}";
                if (!empty($object->$method())) {
                    if (empty($clauseN2Number4)) {
                        $clauseN2Number4 = new Consultation\ClausesN2();
                        $clauseN2Number4->setClauseN1($clauseN1);
                        $clauseN2Number4->setReferentielClauseN2($clausesReferences['n2'][4]);
                        $this->entityManager->persist($clauseN2Number4);
                    }

                    $clauseN3 = new Consultation\ClausesN3();
                    $clauseN3->setClauseN2($clauseN2Number4);
                    $clauseN3->setReferentielClauseN3($clausesReferences['n3'][$key]);
                    $this->entityManager->persist($clauseN3);
                }
            }

            //Partie considérations sociales : checkbox 5
            if (!empty($object->getMarcheInsertion())) {
                $clauseN2 = new Consultation\ClausesN2();
                $clauseN2->setClauseN1($clauseN1);
                $clauseN2->setReferentielClauseN2($clausesReferences['n2'][5]);
                $this->entityManager->persist($clauseN2);
            }
        }
    }

    private function migrateClausesEnvironnementales(
        Consultation|Lot|ContratTitulaire $object,
        array $clausesReferences
    ): void {
        //Migration de la partie considérations environnementales

        if ($object->getClauseEnvironnementale() === '1') {
            $clauseN1 = new Consultation\ClausesN1();
            if ($object instanceof Consultation) {
                $clauseN1->setConsultation($object);
            } elseif ($object instanceof Lot) {
                $clauseN1->setLot($object);
            } else {
                $clauseN1->setContrat($object);
            }
            $clauseN1->setReferentielClauseN1($clausesReferences['n1'][2]);
            $this->entityManager->persist($clauseN1);

            //Partie considérations environnementales : checkbox 1, 2 et 3
            $clausesN2ForClauseEnv = [
                6 => 'ClauseEnvSpecsTechniques',
                7 => 'ClauseEnvCondExecution',
                8 => 'ClauseEnvCriteresSelect'
            ];
            foreach ($clausesN2ForClauseEnv as $key => $value) {
                $method = "get{$value}";
                if (!empty($object->$method())) {
                    $clauseN2 = new Consultation\ClausesN2();
                    $clauseN2->setClauseN1($clauseN1);
                    $clauseN2->setReferentielClauseN2($clausesReferences['n2'][$key]);
                    $this->entityManager->persist($clauseN2);
                }
            }
        }
    }

    private function getClausesReferences(): array
    {
        $referenceClauseSociale = $this->clausesRefN1Repository->findOneBy(['slug' => ClausesN1::CLAUSES_SOCIALES]);
        $referenceClauseEnv = $this->clausesRefN1Repository->findOneBy(['slug' => ClausesN1::CLAUSES_ENVIRONNEMENTALES]);

        $referenceClauseN2Number1 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::CONDITION_EXECUTION]);
        $referenceClauseN2Number2 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::SPECIFICATION_TECHNIQUE]);
        $referenceClauseN2Number3 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::CRITERE_ATTRIBUTION_MARCHE]);
        $referenceClauseN2Number4 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::MARCHE_RESERVE]);
        $referenceClauseN2Number5 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::INSERTION]);
        $referenceClauseN2Number6 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::SPECIFICATIONS_TECHNIQUES]);
        $referenceClauseN2Number7 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::CONDITIONS_EXECUTIONS]);
        $referenceClauseN2Number8 = $this->clausesRefN2Repository->findOneBy(['slug' => ClausesN2::CRITERES_SELECTIONS]);

        $referenceClauseN3Number1 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::INSERTION_ACTIVITE_ECONOMIQUE]);
        $referenceClauseN3Number2 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::CLAUSE_SOCIALE_FORMATION_SCOLAIRE]);
        $referenceClauseN3Number3 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::LUTTE_CONTRE_DISCRIMINATIONS]);
        $referenceClauseN3Number4 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::COMMERCE_EQUITABLE]);
        $referenceClauseN3Number5 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::ACHATS_ETHIQUES_TRACABILITE_SOCIALE]);
        $referenceClauseN3Number6 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::AUTRE_CLAUSE_SOCIALE]);
        $referenceClauseN3Number7 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::CLAUSE_SOCIALE_RESERVE_ATELIER_PROTEGE]);
        $referenceClauseN3Number8 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::CLAUSE_SOCIALE_SIAE]);
        $referenceClauseN3Number9 = $this->clausesRefN3Repository->findOneBy(['slug' => ClausesN3::CLAUSE_SOCIALE_EESS]);

        return [
            'n1'    => [
                1 => $referenceClauseSociale,
                2 => $referenceClauseEnv
            ],
            'n2'    => [
                1 => $referenceClauseN2Number1,
                2 => $referenceClauseN2Number2,
                3 => $referenceClauseN2Number3,
                4 => $referenceClauseN2Number4,
                5 => $referenceClauseN2Number5,
                6 => $referenceClauseN2Number6,
                7 => $referenceClauseN2Number7,
                8 => $referenceClauseN2Number8
            ],
            'n3'    => [
                1 => $referenceClauseN3Number1,
                2 => $referenceClauseN3Number2,
                3 => $referenceClauseN3Number3,
                4 => $referenceClauseN3Number4,
                5 => $referenceClauseN3Number5,
                6 => $referenceClauseN3Number6,
                7 => $referenceClauseN3Number7,
                8 => $referenceClauseN3Number8,
                9 => $referenceClauseN3Number9
            ]
        ];
    }
}
