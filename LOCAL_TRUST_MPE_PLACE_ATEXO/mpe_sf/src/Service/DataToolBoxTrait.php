<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

trait DataToolBoxTrait
{
    private function getDisplayedDateTime(?\DateTime $datetime)
    {
        $emptyDate  = new \DateTime('0000-00-00 00:00:00', new \DateTimeZone(date_default_timezone_get()));

        if (empty($datetime) || $datetime == $emptyDate) {
            return '';
        }

        return $datetime->format('c');
    }
}
