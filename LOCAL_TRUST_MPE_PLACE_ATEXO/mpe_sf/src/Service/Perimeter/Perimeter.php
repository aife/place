<?php

namespace App\Service\Perimeter;

use Exception;
use App\Entity\AffiliationService;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\InterneConsultation;
use App\Entity\Consultation\InterneConsultationSuiviSeul;
use App\Entity\HabilitationAgent;
use App\Entity\InvitePermanentTransverse;
use App\Repository\AgentRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * Class Perimeter.
 */
class Perimeter
{
    /**
     * @param EntityManagerInterface $entityManager
     * @param LoggerInterface $logger
     * @param ContainerInterface $container
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly ContainerInterface $container,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @param $consultation
     *
     * @return array
     */
    public function getListEmailAgent($consultation)
    {
        $agentsFavoris = $this->getAgentConsultationFavoris($consultation);
        $agents = $this->getAllInvitedAgents($consultation);
        $agents = $this->entityManager->getRepository(Agent::class)
            ->getAgentsByAlerte($agents, AgentRepository::ALERT_RECEPTION_MESSAGE);
        $listSendMail = [];
        $listCheckMail = [];
        foreach ($agents as $agent) {
            if (
                !$this->verificationEmail($agent->getEmail()) ||
                in_array($agent->getId(), $listCheckMail) ||
                !in_array($agent->getId(), $agentsFavoris)
            ) {
                continue;
            }
            $listCheckMail[] = $agent->getId();
            $listSendMail[] = $agent->getEmail();
        }

        return $listSendMail;
    }

    /**
     * @param $consultation
     *
     * @return array
     */
    public function getAgentConsultationFavoris($consultation)
    {
        $agentsFavoris = [];
        $consultationFavoris = $this->entityManager->getRepository(ConsultationFavoris::class)
            ->findBy([
                'consultation' => $consultation->getId(),
            ]);
        foreach ($consultationFavoris as $cf) {
            $agentsFavoris[] = $cf->getAgent()->getId();
        }

        return $agentsFavoris;
    }

    /**
     * @return array
     */
    public function getAllInvitedAgents(Consultation $consultation)
    {
        $reference = $consultation->getId();
        $orgAcronym = $consultation->getOrganisme()->getAcronyme();
        $serviceId = $consultation->getServiceId();

        $agents = [];
        $agents = $this->getInterneConsultation($reference, $orgAcronym, $agents);
        $agents = $this->getInterneConsultationSuiviSeul($reference, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentMonEntite($serviceId, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentEntiteDependante($serviceId, $orgAcronym, $agents);
        $agents = $this->getInvitePermanentTransverse($serviceId, $orgAcronym, $agents);

        return $agents;
    }

    /**
     * @param $reference
     * @param $orgAcronym
     * @param $agents
     *
     * @return array
     */
    protected function getInterneConsultation($reference, $orgAcronym, $agents)
    {
        try {
            $allGuests = $this->entityManager->getRepository(InterneConsultation::class)
                ->getGuest($reference, $orgAcronym);
            foreach ($allGuests as $g) {
                if (!in_array((int) $g['interne_id'], $agents)) {
                    $agents[] = (int) $g['interne_id'];
                }
            }
        } catch (Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données sur la table "InterneConsultation". Exception : ' .
                    $exception->getMessage());
        }

        return $agents;
    }

    /**
     * @param $reference
     * @param $orgAcronym
     * @param $agents
     *
     * @return array
     */
    protected function getInterneConsultationSuiviSeul($reference, $orgAcronym, $agents)
    {
        try {
            $allGuestsReadOnly = $this->entityManager->getRepository(InterneConsultationSuiviSeul::class)
                ->getGuest($reference, $orgAcronym);
            foreach ($allGuestsReadOnly as $g) {
                if (!in_array((int) $g['interne_id'], $agents)) {
                    $agents[] = (int) $g['interne_id'];
                }
            }
        } catch (Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données sur la table "InterneConsultationSuiviSeul". Exception : '
                    . $exception->getMessage());
        }

        return $agents;
    }

    /**
     * @param $serviceId
     * @param $orgAcronym
     * @param $agents
     *
     * @return array
     */
    protected function getInvitePermanentMonEntite($serviceId, $orgAcronym, $agents)
    {
        $params = [];
        try {
            $params['invitePermanentMonEntite'] = '1';
            $allGuestsMonEntite = $this->entityManager->getRepository(HabilitationAgent::class)
                ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

            /** @var HabilitationAgent $g */
            foreach ($allGuestsMonEntite as $g) {
                if (!in_array($g->getAgent()->getId(), $agents)) {
                    $agents[] = $g->getAgent()->getId();
                }
            }
        } catch (Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->getMessage());
        }

        return $agents;
    }

    /**
     * @param $serviceId
     * @param $orgAcronym
     * @param $agents
     *
     * @return array
     */
    protected function getInvitePermanentEntiteDependante($serviceId, $orgAcronym, $agents)
    {
        $params = [];
        try {
            $tabPole = $this->getListServiceEntiteDependante($serviceId, $orgAcronym);
            $params['invitePermanentEntiteDependante'] = '1';
            $allGuestsMonEntite = $this->entityManager->getRepository(HabilitationAgent::class)
                ->retrievePermanentEntiteDependante($orgAcronym, $serviceId, $params, $tabPole);
            if (!is_null($allGuestsMonEntite)) {
                /** @var HabilitationAgent $g */
                foreach ($allGuestsMonEntite as $g) {
                    if (!in_array($g->getAgent()->getId(), $agents)) {
                        $agents[] = $g->getAgent()->getId();
                    }
                }
            }
        } catch (Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->getMessage());
        }

        return $agents;
    }

    private function getListServiceEntiteDependante($serviceId, $org, $pole = [])
    {
        if (null !== $serviceId) {
            $affiliationServices = $this->entityManager->getRepository(AffiliationService::class)
                ->getParentByServiceIdAndOrganisme($serviceId, $org);
            foreach ($affiliationServices as $affiliationService) {
                if ($affiliationService instanceof AffiliationService) {
                    $pole[] = $serviceId = $affiliationService->getServiceParentId()->getId();
                    $this->getListServiceEntiteDependante($serviceId, $org, $pole);
                }
            }
        }
        return $pole;
    }

    /**
     * @param $serviceId
     * @param $orgAcronym
     * @param $agents
     *
     * @return array
     */
    protected function getInvitePermanentTransverse($serviceId, $orgAcronym, $agents)
    {
        try {
            $params = [];
            $params['invitePermanentTransverse'] = '1';
            $allGuestsTransverse = $this->entityManager->getRepository(HabilitationAgent::class)
                ->retrievePermanentGuests($orgAcronym, $serviceId, $params);

            /** @var HabilitationAgent $g */
            foreach ($allGuestsTransverse as $g) {
                try {
                    $transverse = $this->entityManager->getRepository(InvitePermanentTransverse::class)->findOneBy([
                        'agent' => $g->getAgent()->getId(),
                        'service' => $serviceId,
                        'acronyme' => $orgAcronym,
                    ]);
                } catch (Exception $exception) {
                    $this->logger
                        ->error('Problème de récupèration des données. Exception : ' .
                            $exception->getMessage());
                }

                if (is_null($transverse)) {
                    continue;
                }

                if (!in_array($g->getAgent()->getId(), $agents)) {
                    $agents[] = $g->getAgent()->getId();
                }
            }
        } catch (Exception $exception) {
            $this->logger
                ->error('Problème de récupèration des données. Exception : ' .
                    $exception->__toString());
        }

        return $agents;
    }

    /**
     * @param $email
     * @param string $type
     *
     * @return bool
     *
     * @throws Exception
     */
    protected function verificationEmail($email, $type = 'entreprise')
    {
        $emailConstraint = new Assert\Email(['mode' => Assert\Email::VALIDATION_MODE_STRICT]);
        $emailConstraint->message = 'Invalid email address';

        $validator = Validation::createValidator();
        $violations = $validator->validate($email, [
            $emailConstraint,
        ]);

        $bool = true;
        if (0 !== count($violations)) {
            $bool = false;
            $this->logger->error('Le mail suivant est invalide : ' . $email);
        }

        return $bool;
    }
}
