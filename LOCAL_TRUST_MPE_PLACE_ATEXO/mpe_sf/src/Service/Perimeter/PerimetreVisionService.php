<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Perimeter;

use App\Doctrine\Extension\PerimetreVisionExtension;
use App\Entity\Agent;
use App\Filter\AgentPerimeterVisionSearchFilter;
use App\Repository\HabilitationTypeProcedureRepository;
use App\Repository\InvitePermanentTransverseRepository;
use App\Repository\ReferentielHabilitationRepository;
use App\Service\WebservicesMpeConsultations;
use App\Utils\EntityPurchase;
use Symfony\Component\HttpFoundation\RequestStack;

class PerimetreVisionService
{
    public function __construct(
        private RequestStack $requestStack,
        private WebservicesMpeConsultations $wsConsultations,
        private readonly EntityPurchase $entityPurchase,
        private readonly InvitePermanentTransverseRepository $invitePermanentTransverseRepository,
        private readonly ReferentielHabilitationRepository $referentielHabilitationRepository,
        private readonly HabilitationTypeProcedureRepository $habilitationTypeProcedureRepository
    ) {
    }

    public function isFromSubResourceClass(string $resourceClass): bool
    {
        $request = $this->requestStack->getCurrentRequest();

        if (
            $request
            && ($apiRessourceClass = $request->attributes->get('_api_resource_class'))
            && in_array($apiRessourceClass, PerimetreVisionExtension::NOT_APPLY_SUB_RESOURCE_CLASS)
        ) {
            return false;
        }

        $isFromSubResourceClass = in_array($resourceClass, PerimetreVisionExtension::SUB_RESOURCE_CLASS);

        if (
            $resourceClass === Agent::class
            && $request
            && $request->query->has(AgentPerimeterVisionSearchFilter::PROPERTY_NAME)
            && 'true' === $request->query->get(AgentPerimeterVisionSearchFilter::PROPERTY_NAME)
        ) {
            $isFromSubResourceClass = true;
        }

        return $isFromSubResourceClass;
    }

    public function getAgentsCreateurInPerimeterVision(): array
    {
        // On applique le filtre applyPerimeterVisionConsultations pour récupérer seulement les agents se trouvant
        // dans le périmétre de vision de l'agent connecté
        $wsAgents = $this->wsConsultations->getContent(
            'searchAgents',
            [
                WebservicesMpeConsultations::PARAM_LENGTH => WebservicesMpeConsultations::PARAM_LENGTH_VALUE,
                AgentPerimeterVisionSearchFilter::PROPERTY_NAME => 'true',
            ]
        );

        $agentsCreateurs = [];
        foreach ($wsAgents->{'hydra:member'} as $agent) {
            $agentsCreateurs[] = [
                'id' => $agent->id,
                'text' => $agent->libelleDetail
            ];
        }

        return $agentsCreateurs;
    }

    public function getServicesInPerimeterVision(Agent $agent): array
    {
        $listOfAllowedServices = [];

        $idServiceAgent = $agent->getServiceId() ?? 0;
        $habilitationAgent = $agent->getHabilitation();

        if ($habilitationAgent->getInvitePermanentMonEntite()) {
            $listOfAllowedServices[] = $idServiceAgent;
        }

        $serviceAndSubServices = $this->entityPurchase->retrieveAllChildrenServices(
            $idServiceAgent,
            $agent->getOrganisme(),
            true
        );

        if ($habilitationAgent->getInvitePermanentEntiteDependante()) {
            $listOfAllowedServices = $this->getServicesInvitePermanentEntiteDependante(
                $idServiceAgent,
                $listOfAllowedServices,
                $serviceAndSubServices
            );
        }

        if ($habilitationAgent->getInvitePermanentTransverse()) {
            $listOfAllowedServices = $this->getServicesInvitePermanentTransverse(
                $agent->getId(),
                $listOfAllowedServices
            );
        }

        sort($listOfAllowedServices);

        return array_values(array_unique($listOfAllowedServices));
    }

    private function getServicesInvitePermanentEntiteDependante(
        int $idServiceAgent,
        array $listOfAllowedServices,
        array $serviceAndSubServices,
    ): array {
        $sousServices = array_diff($serviceAndSubServices, [$idServiceAgent]);

        return array_merge($sousServices, $listOfAllowedServices);
    }

    private function getServicesInvitePermanentTransverse(
        int $idAgent,
        array $listOfAllowedServices,
    ): array {
        return array_merge(
            $this->invitePermanentTransverseRepository->getServicesIdByAgentId($idAgent),
            $listOfAllowedServices
        );
    }

    public function isPerimetreVisionHabilitationActiveBySlug(
        string $slug,
        Agent $agent
    ): bool {
        $habilitation = $this->referentielHabilitationRepository
            ->findOneBy([
                'slug'   => $slug,
                'active' => true
            ]);

        if (!is_null($habilitation)) {
            $typeProcedure = $this->habilitationTypeProcedureRepository
                ->findOneBy([
                    'agent'         => $agent,
                    'habilitation'  => $habilitation,
                ]);

            return !is_null($typeProcedure);
        }

        return false;
    }
}
