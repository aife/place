<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use App\Entity\AdmissibiliteEnveloppeLot;
use App\Entity\Agent;
use App\Entity\AvisType;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Etablissement;
use App\Entity\Inscrit;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\Organisme;
use App\Entity\TCandidature;
use App\Entity\TGroupementEntreprise;
use App\Entity\TListeLotsCandidature;
use App\Entity\TypeProcedure;
use App\Service\DonneeComplementaireService;
use App\Service\DonneeComplementaire\DonneeComplementaireService as SecondDonneeComplementaireService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MergeFields
{
    private const OFFRE_STATUS_MATCH = [
        '1' => 'DEFINE_FERMEE',
        '2' => 'DEFINE_OUVERTE_EN_LIGNE',
        '3' => 'DEFINE_OUVERTE_HORS_LIGNE',
        '4' => 'DEFINE_REFUSEE',
        '5' => 'DEFINE_OUVERTE',
        '6' => 'DEFINE_SUPPRIMEE',
        '7' => 'DEFINE_OUVERTE_A_DISTANCE',
        '8' => 'DEFINE_EN_COURS_CHIFFREMENT',
        '9' => 'DEFINE_EN_COURS_DECHIFFREMENT',
        '10' => 'DEFINE_EN_ATTENTE_CHIFFREMENT',
        '11' => 'DEFINE_EN_COURS_FERMETURE',
        '12' => 'DEFINE_EN_ATTENTE_FERMETURE',
        '99' => 'DEFINE_BROUILLON',
    ];

    /**
     * @var string
     */
    final public const ETABLISSEMENT = 'etablissement';

    /**
     * @var string
     */
    final public const GROUPEMENT = 'groupement';

    /**
     * ArgumentPieceGenereModele constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly DonneeComplementaireService $donneeComplementaireService,
        private readonly SecondDonneeComplementaireService $complementaireService,
        private readonly TranslatorInterface $translator,
    ) {
    }

    public function getConsultationDataForArchive(Consultation $consultation): array
    {
        $typeAnnonce = $this->em->getRepository(AvisType::class)
                            ->findOneBy(['id' => $consultation->getIdTypeAvis()]);
        $typeProcedure = $this->em->getRepository(TypeProcedure::class)
                              ->findOneBy(['idTypeProcedure' => $consultation->getIdTypeProcedure()]);

        $data =
            [
                'categoriePrincipale' => $consultation->getCategorieConsultation()->getLibelle(),
                'dateRemisePlis' => $consultation->getDatefin()->format('Y-m-d H:i:s'),
                'entiteAchat' => $consultation->getEntiteAchat(),
                'entitePublique' => $consultation->getEntitePublique(),
                'intituleConsultation' => $consultation->getIntitule(),
                'objetConsultation' => $consultation->getObjet(),
                'refConsultation' => $consultation->getReferenceUtilisateur(),
                'typeAnnonce' => $typeAnnonce->getIntituleAvis(),
                'typeProcedure' => $typeProcedure->getLibelleTypeProcedure(),
            ];

        if (!$consultation->isAlloti()) {
            $data['candidatures'] = $this->getConsultationCandidaturesData($consultation);
            $data['offres'] = $this->getConsultationOffresData($consultation);
        } else {
            $data['lots'] = $this->getDataLotForArchive($consultation);
        }

        return $data;
    }

    private function getDataLotForArchive(Consultation $consultation): array
    {
        $data = $dataLot = [];
        foreach ($consultation->getLots() as $lot) {
            $data = [
                "numero" => $lot->getLot(),
            ];

            $candidaturesLot = $this->em->getRepository(TListeLotsCandidature::class)
                                 ->findBy([
                                     'consultation' => $consultation,
                                     'numLot' => $lot->getLot(),
                                 ]);
            $dataCandidatures = $dataOffres = [];
            $indexCandidature = $indexOffre = 0;
            foreach ($candidaturesLot as $candidaLot) {
                /** @var TCandidature $candidature */
                $candidature = $candidaLot->getCandidature();
                $dataCandidatures[] = $this->getDataOfOffre($indexCandidature, $candidature->getIdOffre());

                if (!is_null($candidature->getIdOffre())) {
                    $dataOffres[] = $this->getDataOfOffre($indexOffre, $candidature->getIdOffre());
                }
            }
            $data['candidatures'] = $dataCandidatures;
            $data['offres'] = $dataOffres;
            $dataLot[] = $data;
        }

        return $dataLot;
    }

    private function getConsultationCandidaturesData(Consultation $consultation): array
    {
        $dataCandidatures = [];
        $index = 0;
        foreach ($consultation->getCandidatures() as $candidature) {
            $dataCandidatures[] = $this->getDataOfOffre($index, $candidature->getIdOffre());
        }

        return $dataCandidatures;
    }

    private function getDataOfOffre(int &$index, Offre $offre = null): array
    {
        $index++;
        if (!is_null($offre)) {
            $data = $this->getAdmissibiliteData($offre);
        }
        $data['total'] = $index;

        return $data;
    }

    private function getAgentName(Offre $offre): string
    {
        if ($offre->getDateHeureOuverture() != '0000-00-00 00:00:00') {
            $agent = $this->em
                ->getRepository(Agent::class)
                ->findOneBy(['id' => $offre->getAgentidOuverture()]);

            if (!is_null($agent)) {
                return $agent->getPrenom() . ' ' . $agent->getNom();
            }
        }

        return '';
    }

    private function getConsultationOffresData(Consultation $consultation): array
    {
        $dataOffres = [];
        $index = 0;
        /** @var Offre $offre */
        foreach ($consultation->getOffres() as $offre) {
            $dataOffres[] = $this->getDataOfOffre($index, $offre);
        }

        return $dataOffres;
    }

    private function getAdmissibiliteData(Offre $offre): array
    {
        $admissiblite = $this->em->getRepository(AdmissibiliteEnveloppeLot::class)
            ->findOneBy(['offreId' => $offre->getId()]);
        $entreprise = $this->em->getRepository(Entreprise::class)
            ->findOneBy(['id' => $offre->getEntrepriseId()]);

        return [
            'aTraiter' => $this->convertBoolToStringNumber($offre->getDateHeureOuverture() == '0000-00-00 00:00:00'),
            'admissible' => !is_null($admissiblite)
                ? $this->convertBoolToStringNumber($admissiblite->getAdmissibilite())
                : null,
            'agent' => $this->getAgentName($offre),
            'dateOuverture' => $offre->getDateHeureOuverture(),
            'denomination' => $entreprise?->getNom(),
            'horodatage' => $offre->getUntrusteddate()->format('d/m/Y H:i:s'),
            'nonAdmissible' => !is_null($admissiblite) ? !$admissiblite->getAdmissibilite() : null,
            'numero' => 'El' . $offre->getNumeroReponse(),
            'statut' => $this->translator->trans(self::OFFRE_STATUS_MATCH[$offre->getStatutOffres()]),
        ];
    }

    private function convertBoolToStringNumber(bool $myVar): string
    {
        return $myVar ? '1' : '0';
    }

    public function pieceGenereeModele(Consultation $consultation, ?Lot $lot, ?Entreprise $entreprise): array
    {
        $lots = empty($lot) ? [] : [
            'id' => $lot->getId(),
            'intitule' => $lot->getDescription(),
            'numero' => $lot->getLot(),
            'donneesComplementaires' => $this->getLotDonneesComplementaires($lot),
        ];

        $entreprises = empty($entreprise) ? [] : $this->getDataEntreprise($entreprise, $consultation, $lot);

        return $this->getArgument($consultation, $lots, $entreprises, $lot);
    }

    public function tableauComparatifAnalyseFinanciere(Consultation $consultation): array
    {
        $lots = $this->getDataLot($consultation);

        return $this->getArgument($consultation, $lots);
    }

    private function getDataLot(Consultation $consultation): array
    {
        $lots = [];
        foreach ($consultation->getLots() as $lot) {
            $lots[] = [
                'id' => $lot->getId(),
                'intitule' => $lot->getDescription(),
                'numero' => $lot->getLot(),
                'donneesComplementaires' => $this->getLotDonneesComplementaires($lot),
            ];
        }

        return $lots;
    }

    private function getArgument(Consultation $consultation, array $lots = [], array $entreprises = [], ?Lot $lot = null): array
    {
        $organismeAcronyme = $consultation->getOrganisme()->getAcronyme();
        $organismeRepo = $this->em->getRepository(Organisme::class)->findOneBy(['acronyme' => $organismeAcronyme]);

        $organismeAddress = $organismeRepo->getAdresse();
        if (!empty($organismeRepo->getAdresse2())) {
            $organismeAddress = $organismeRepo->getAdresse() . ' - ' . $organismeRepo->getAdresse2();
        }

        return
            [
                [
                    'key'   => 'consultation',
                    'value' => [
                        'organisme'         => [
                            'denomination'      => $consultation->getEntitePublique(),
                            'rue'               => $organismeAddress,
                            'ville'             => $organismeRepo->getVille(),
                            'codePostal'        => $organismeRepo->getCp(),
                            'pays'              => $organismeRepo->getPays(),
                            'email'             => $organismeRepo->getEmail(),
                            'telephone'         => $organismeRepo->getTel(),
                            'telecopie'         => $organismeRepo->getTelecopie(),
                        ],
                        'directionService'  => $consultation->getEntiteAchat(),
                        'objet'             => $consultation->getObjet(),
                        'reference'         => $consultation->getReferenceUtilisateur(),
                        'intitule'          => $consultation->getIntitule(),
                        'lot'               => $lots,
                        'dateLimiteRemisePlis' => $consultation->getDatefin()->format('d/m/Y H:i'),
                    ],
                ],
                [
                    'key'   => 'candidat',
                    'value' => $entreprises,
                ],
                [
                    'key'   => 'donneesComplementaires',
                    'value' => $this->getDataDonneesComplementaires($consultation, $lot),
                ],
            ];
    }

    /**
     * @return array|array[]
     */
    private function getDataEntreprise(Entreprise $entreprise, Consultation $consultation, ?Lot $lot): array
    {
        if (empty($lot)) {
            $data = $this->getEtablissementAndGroupement($consultation, $entreprise);
        } else {
            $data = $this->getEtablissementAndGroupementBylot($consultation, $entreprise, $lot);
        }

        $etablissement = $data[self::ETABLISSEMENT];

        return [
            [
                'contact'       => $this->getOffreContact($consultation, $entreprise),
                'entreprise'    => [
                    'rue'           => empty($etablissement->getAdresse2()) ? $entreprise->getAdresse() :
                        $entreprise->getAdresse() . '-' . $etablissement->getAdresse2(),
                    'codePostal'    => $entreprise->getCodepostal(),
                    'ville'         => $entreprise->getVille(),
                    'pays'          => $entreprise->getPays(),
                ],
                'etablissement' => [
                    'rue'           => empty($etablissement->getAdresse2()) ? $etablissement->getAdresse() :
                        $etablissement->getAdresse() . '-' . $etablissement->getAdresse2(),
                    'siret'         => $entreprise->getSiren() . $etablissement->getCodeEtablissement(),
                    'pays'          => $etablissement->getPays(),
                    'codePostal'    => $etablissement->getCodePostal(),
                    'ville'         => $etablissement->getVille(),
                ],
                'raisonSociale' => $entreprise->getNom(),
                'role'          => !empty($data[self::GROUPEMENT]) ? 'Mandataire' : 'Titulaire',
                'dateDepot'     => $this->getDateDepotOffre($consultation, $entreprise),
            ],
        ];
    }

    private function getEtablissementAndGroupement(Consultation $consultation, Entreprise $entreprise): array
    {
        $data = [
            self::ETABLISSEMENT => new Etablissement(),
            self::GROUPEMENT => new Offre(),
        ];

        foreach ($consultation->getOffres() as $offre) {
            if ($entreprise->getId() === $offre->getEntrepriseId()) {
                $data[self::ETABLISSEMENT] =
                    $this->em->getRepository(Etablissement::class)->find($offre->getIdEtablissement());
                $data[self::GROUPEMENT] =
                    $this->em->getRepository(TGroupementEntreprise::class)->findOneByIdOffre($offre->getId());

                break;
            }
        }

        return $data;
    }

    private function getEtablissementAndGroupementBylot(
        Consultation $consultation,
        Entreprise $entreprise,
        Lot $lot
    ): array {
        $data = [
            self::ETABLISSEMENT => new Etablissement(),
            self::GROUPEMENT => new Offre(),
        ];

        foreach ($consultation->getListLots() as $listLot) {
            if ($entreprise->getId() === $listLot->getIdEntreprise() && $lot->getLot() == $listLot->getNumlot()) {
                $data[self::ETABLISSEMENT] =
                    $this->em->getRepository(Etablissement::class)->find($listLot->getIdEtablissement());
                $data[self::GROUPEMENT] =
                    $this->em->getRepository(TGroupementEntreprise::class)
                        ->findByCandidature($listLot->getCandidature());
            }
        }

        return $data;
    }

    private function getOffreContact(Consultation $consultation, Entreprise $entreprise): array
    {
        $contact = [
            'prenom'    => '',
            'nom'       => '',
            'email'     => '',
            'telecopie' => '',
            'telephone' => '',
        ];

        foreach ($consultation->getOffres() as $offre) {
            if ($entreprise->getId() === $offre->getEntrepriseId()) {
                $contact = [
                    'prenom'    => $offre->getPrenomInscrit() ?? '',
                    'nom'       => $offre->getNomInscrit() ?? '',
                    'email'     => $offre->getEmailInscrit() ?? '',
                    'telecopie' => $offre->getFaxInscrit() ?? '',
                    'telephone' => $offre->getTelephoneInscrit() ?? '',
                ];
                break;
            }
        }

        return $contact;
    }

    private function getDateDepotOffre(Consultation $consultation, Entreprise $entreprise): DateTime|string|null
    {
        foreach ($consultation->getOffres() as $offre) {
            if ($entreprise->getId() === $offre->getEntrepriseId()) {
                return $offre->getUntrusteddate();
            }
        }

        return null;
    }

    private function getDataDonneesComplementaires(Consultation $consultation, ?Lot $lot = null): array
    {
        return [
            'dureeMarche'                   => $this->donneeComplementaireService->getDureeMarche($consultation->getDonneeComplementaire()),
            'delaiValiditeOffres'           => $consultation->getDonneeComplementaire()?->getDelaiValiditeOffres(),
            'nombreCandidats'               => $this->donneeComplementaireService->getNombreCandidatsAdmis($consultation->getDonneeComplementaire()),
            'ccag'                          => $this->donneeComplementaireService->getLabelCCAGReference($consultation->getDonneeComplementaire()),
            'varianteAutorisee'             => $consultation->getDonneeComplementaire()?->getVariantesAutorisees(),
            'varianteExigee'                => $consultation->getDonneeComplementaire()?->getVarianteExigee(),
            'attributionSansNegociation'    => $consultation->getDonneeComplementaire()?->getAttributionSansNegociation(),
            'reconduction'                  => [
                'nombre'                        => $consultation->getDonneeComplementaire()?->getNombreReconductions(),
                'modalite'                      => $consultation->getDonneeComplementaire()?->getModalitesReconduction(),
            ],
            'prestationsSupplementaires'    => [
                'enabled'                       => $consultation->getDonneeComplementaire()?->getVariantesTechniquesObligatoires(),
                'description'                   => $consultation->getDonneeComplementaire()?->getVariantesTechniquesDescription(),
            ],
            'criteres'                      => $this->complementaireService->getCriteriaAttributionInfos($consultation, true),
            'tranches'                      => $this->complementaireService->getConsultationTranches($consultation),
            'formePrix'                     => $this->complementaireService->getConsultationFormePrix($consultation),
        ];
    }

    private function getLotDonneesComplementaires(Lot $lot): array
    {
        $donneeComplementaire = $lot->getDonneeComplementaire();

        return [
            'dureeMarche'                   => $this->donneeComplementaireService->getDureeMarche($donneeComplementaire),
            'ccag'                          => $this->donneeComplementaireService->getLabelCCAGReference($donneeComplementaire),
            'varianteAutorisee'             => $donneeComplementaire?->getVariantesAutorisees(),
            'varianteExigee'                => $donneeComplementaire?->getVarianteExigee(),
            'prestationsSupplementaires'    => [
                'enabled'                       => $donneeComplementaire?->getVariantesTechniquesObligatoires(),
                'description'                   => $donneeComplementaire?->getVariantesTechniquesDescription(),
            ],
            'reconduction'                  => [
                'nombre'                        => $donneeComplementaire?->getNombreReconductions(),
                'modalite'                      => $donneeComplementaire?->getModalitesReconduction(),
            ],
            'criteres'                      => $this->complementaireService->getLotCriteriaAttribution($lot),
            'tranches'                      => $this->complementaireService->getLotTranches($lot),
            'formePrix'                     => $this->complementaireService->getLotFormePrix($lot),
        ];
    }
}
