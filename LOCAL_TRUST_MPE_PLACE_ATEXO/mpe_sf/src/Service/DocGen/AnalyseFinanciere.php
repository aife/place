<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use League\Flysystem\FileExistsException;
use Exception;
use App\Entity\Consultation;
use App\Entity\PieceGenereConsultation;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\Translation\TranslatorInterface;

class AnalyseFinanciere
{
    public final const TEMPLATE_NAME = 'annexe_financiere-tableau-comparaison.xlsx';

    public final const POSITION = 'position';

    public final const SHEET_NAME = 'sheetName';

    public final const COLUMN = 'column';

    public final const START_TARGET_POSITION = 'startTargetPosition';

    public final const ROW = 'row';

    public final const SYNTHESE = 'Synthèse';

    public function __construct(private readonly MergeFields $arguments, private readonly Template $template, private readonly TranslatorInterface $translator, private readonly Docgen $docGen, private readonly DocumentGenere $documentGenere)
    {
    }

    public function getPieceGeneree(
        Consultation $consultation,
        array $files,
        string $outputFileName,
        string $organisme
    ) {
        $pathTemplate = $this->template->getPathTemplate(self::TEMPLATE_NAME);
        $file = $this->template->templateExist($pathTemplate);

        if (!$file) {
            throw new FileNotFoundException($this->translator->trans('TEXT_FICHIER_NON_TROUVE'));
        }

        $pathTemplateGenere = $this->generate($consultation, $file, $files, $pathTemplate, $outputFileName);

        return $this->documentGenere->addPieceGeneree($consultation, $outputFileName, $pathTemplateGenere, $organisme);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function generate(
        Consultation $consultation,
        string $fileModele,
        array $files,
        string $pathTemplate,
        string $outputFileName
    ): string {
        $documentName = $this->template->withoutExtension(self::TEMPLATE_NAME);
        $arguments = $this->arguments->tableauComparatifAnalyseFinanciere($consultation);

        $configurations = [
            'keyValues'  => $arguments,
            'positions'  => $this->getPositions(),
            'staticData' => []
        ];

        $formFields = [
            'configuration' => new DataPart(
                json_encode($configurations, JSON_THROW_ON_ERROR),
                '',
                'application/json'
            ),
            'template' => DataPart::fromPath(
                $this->template->writeFile(
                    self::TEMPLATE_NAME,
                    $fileModele,
                    $documentName
                )
            ),
            'fichiers' => $this->getDataPartFiles($files)
        ];

        $formData = new FormDataPart($formFields);
        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/octet-stream';

        $content = $this->docGen->generate(
            $headers,
            $formData,
            '/docgen/api/v1/document-extractor/extract-xlsx'
        );

        $this->template->removeFile($pathTemplate);

        return $this->template->writeFile(
            $outputFileName,
            $content,
            $documentName
        );
    }

    /**
     * @throws FileExistsException
     * @throws \League\Flysystem\FileNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEditedDocument(PieceGenereConsultation $pieceGenee, string $organisme): bool
    {
        return $this->documentGenere->getEditedDocument($pieceGenee, $organisme, self::TEMPLATE_NAME);
    }

    private function getDataPartFiles(array $files): array
    {
        $data = [];
        foreach ($files as $file) {
            try {
                $currentFile = $file->getFile();
                $currentFileName = $currentFile->getFileName() . $this->template->getExtension(self::TEMPLATE_NAME);

                $data[] = DataPart::fromPath(
                    $this->template->writeFile(
                        $currentFileName,
                        file_get_contents($currentFile->getPathName()),
                        $currentFileName,
                    )
                );
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $data;
    }

    /**
     * @return array[]
     */
    private function getPositions(): array
    {
        return [
            [
                self::POSITION => [self::COLUMN => 'B', self::ROW => 8],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 9],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'C', self::ROW => 20],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 10],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'E', self::ROW => 20],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 11],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'C', self::ROW => 21],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 12],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'E', self::ROW => 21],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 13],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'C', self::ROW => 22],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 14],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'D', self::ROW => 22],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 15],
                self::SHEET_NAME => self::SYNTHESE
            ],
            [
                self::POSITION => [self::COLUMN => 'E', self::ROW => 22],
                self::START_TARGET_POSITION => [self::COLUMN => 'C', self::ROW => 16],
                self::SHEET_NAME => self::SYNTHESE
            ]
        ];
    }
}
