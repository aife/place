<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\DocGen;

use League\Flysystem\FileExistsException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Exception;
use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Enveloppe;
use App\Entity\Lot;
use App\Entity\Offre;
use App\Entity\PieceGenereConsultation;
use App\Service\Blob\BlobOrganismeFileService;
use App\Service\LotCandidature;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\Translation\TranslatorInterface;

class AnalyseOffre
{
    public final const TEMPLATE_NAME = 'template-analyse-des-offres.xlsx';

    public final const NAME = 'analyse-des-offres-';

    public final const ANALYSE_REPORT = '/docgen/api/v1/document-extractor/analyse-report?outputFileName=';

    public final const CONVERT_ANALYSE = '/docgen/api/v1/document-convertor/convert-analyse?outputFileName=';

    public final const CRITERE_ORDRE_PRIO_DESC = 1;

    public final const CRITERE_UNIQUE_DE_PRIX = 3;

    public final const CRITERE_UNIQUE_DE_COUT = 5;

    public final const DESCRIPTION = 'description';

    public final const MAXIMUMNOTE = 'maximumNote';

    public final const REALPATH = 'realPath';

    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly Docgen $docGen,
        private readonly Template $template,
        private readonly TranslatorInterface $translator,
        private readonly EntityManagerInterface $em,
        private readonly LotCandidature $lotCandidature,
        private readonly DocumentGenere $documentGenere,
        private readonly BlobOrganismeFileService $blobOrganismeFile,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws FileExistsException
     */
    public function getPieceGeneree(Consultation $consultation, string $organisme): PieceGenereConsultation
    {
        $fileOriginalName = $this->getFileOriginalName($consultation);
        $fileOriginalNameWithExtension = $fileOriginalName . $this->template->getExtension(self::TEMPLATE_NAME);

        $fileOutputName = self::NAME . uniqid();

        $pieceGenee =
            $this->documentGenere->getByIdConsultationAndFileName($fileOriginalNameWithExtension, $consultation);

        if ($pieceGenee instanceof PieceGenereConsultation) {
            return $pieceGenee;
        }

        $pathTemplate = $this->template->getPathTemplate(self::TEMPLATE_NAME);
        $file = $this->template->templateExist($pathTemplate);

        if (!$file) {
            throw new FileNotFoundException($this->translator->trans('TEXT_FICHIER_NON_TROUVE'));
        }

        $zipPath = $this->generate($fileOutputName, $file, $pathTemplate, $consultation);

        return $this->moveFile($consultation, $zipPath, $fileOutputName, $fileOriginalNameWithExtension, $organisme);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAnalyseInJson(Consultation $consultation, PieceGenereConsultation $pieceGeneree): bool|string
    {
        $fileName = $this->getFileOriginalName($consultation);

        $xlsxBlobFilePath = $this->getXlsxRealPath($pieceGeneree, $fileName);
        $jsonBlobFilePath = $this->getJsonRealPath($fileName);


        $formFields = [
            'configuration' => new DataPart(
                file_get_contents($jsonBlobFilePath),
                '',
                'application/json'
            ),
            'document' => DataPart::fromPath(
                $this->template->writeFile(
                    self::TEMPLATE_NAME,
                    file_get_contents($xlsxBlobFilePath),
                    $fileName
                )
            ),
        ];

        $formData = new FormDataPart($formFields);


        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/json';

        $jsonResponse = $this->docGen->generate(
            $headers,
            $formData,
            self::ANALYSE_REPORT . $fileName
        );


        return $jsonResponse;
    }

    public function getFileOriginalName(Consultation $consultation): string
    {
        return str_replace(chr(32), '', self::NAME . $consultation->getReferenceUtilisateur());
    }

    public function criteresAttributionExists(Consultation $consultation): bool
    {
        return $this->criteresAttributionConsultationExists($consultation) ||
            $this->criteresAttributionLotExists($consultation);
    }

    /**
     * @param $organisme
     * @param $urlCallback
     *
     * @return string|null
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getToken(PieceGenereConsultation $pieceGenereConsultation, Agent $agent, $organisme, $urlCallback)
    {
        return $this->docGen->getToken(
            $pieceGenereConsultation,
            $agent,
            $organisme,
            $this->template->getExtension(self::TEMPLATE_NAME),
            $urlCallback
        );
    }

    /**
     * @throws FileExistsException
     * @throws \League\Flysystem\FileNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEditedDocument(PieceGenereConsultation $pieceGeneree, string $organisme): bool
    {
        $this->logger->info('La modification du document ' . $pieceGeneree->getId() .
            ' a bien été effectuée au niveau du serveur GenDoc');
        return $this->documentGenere->getEditedDocument($pieceGeneree, $organisme, self::TEMPLATE_NAME);
    }

    public function getData(Consultation $consultation): array
    {
        $data = [
            'intitule' => $consultation->getIntitule(),
            'reference' => $consultation->getReferenceUtilisateur(),
        ];

        if (count($consultation->getLots()) > 0) {
            $data = $this->addDataForConsultationAlloti($consultation, $data);
        } else {
            $data = $this->addDataForConsultationNotAlloti($consultation, $data);
        }

        return $data;
    }

    /**
     * @return PieceGenereConsultation
     *
     * @throws FileExistsException
     */
    private function moveFile(
        Consultation $consultation,
        string $zipPath,
        string $zipName,
        string $fileOriginalName,
        string $organisme
    ) {
        $pathDirExtract = $this->template->extractTo($zipName, $zipPath);

        $jsonFileOutputName = $pathDirExtract . '/' . $zipName . '.json';
        $xlsxFileOutputName = $pathDirExtract . '/' . $zipName . $this->template->getExtension(self::TEMPLATE_NAME);

        if (
            !$this->template->templateExist($jsonFileOutputName) &&
            !$this->template->templateExist($xlsxFileOutputName)
        ) {
            throw new Exception('files ' . $jsonFileOutputName . 'and' . $xlsxFileOutputName . 'do not exist');
        }

        $fileNameJson = $this->template->withoutExtension($fileOriginalName) . '.json';
        $this->documentGenere->addBlobOrganismeFile($fileNameJson, $jsonFileOutputName, $organisme);

        return $this->documentGenere->addPieceGeneree(
            $consultation,
            $fileOriginalName,
            $xlsxFileOutputName,
            $organisme
        );
    }

    /**
     * @param $file
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function generate(
        string $fileOutputName,
        $file,
        string $pathTemplate,
        Consultation $consultation
    ): string {
        $documentName = $this->template->withoutExtension(self::TEMPLATE_NAME);

        $formFields = [
            'meta' => new DataPart(
                json_encode($this->getData($consultation), JSON_THROW_ON_ERROR),
                '',
                'application/json'
            ),
            'template' => DataPart::fromPath(
                $this->template->writeFile(
                    self::TEMPLATE_NAME,
                    $file,
                    $documentName
                )
            ),
        ];

        $formData = new FormDataPart($formFields);
        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/octet-stream';

        $content = $this->docGen->generate(
            $headers,
            $formData,
            self::CONVERT_ANALYSE . $fileOutputName
        );

        $this->template->removeFile($pathTemplate);

        $zipName = $fileOutputName . '.zip';

        return $this->template->writeFile(
            $zipName,
            $content,
            $documentName
        );
    }

    private function addDataForConsultationAlloti(Consultation $consultation, array $data): array
    {
        foreach ($consultation->getLots() as $lot) {
            $criteres = (count($this->getCriteresLot($consultation, $lot)) > 0) ?
                $this->getCriteresLot($consultation, $lot) : $this->getCriteresConsultation($consultation);

            $data['lots'][] = [
                'intitule' => $lot->getDescription(),
                'numero' => $lot->getLot(),
                'soumissionnaires' => $this->getDataSoumissionnaireWithLot($lot, $consultation),
                'criteres' => $criteres,
            ];
        }

        return $data;
    }

    private function getCriteresConsultation(Consultation $consultation): array
    {
        return $this->getCriteres($consultation, $consultation->getDonneeComplementaire()->getCriteresAttribution());
    }

    private function getCriteresLot(Consultation $consultation, Lot $lot): array
    {
        return !empty($lot->getDonneeComplementaire()) ?
            $this->getCriteres($consultation, $lot->getDonneeComplementaire()->getCriteresAttribution()) : [];
    }

    private function getCriteres(Consultation $consultation, array $data): array
    {
        $criteres = [];

        foreach ($data as $critere) {
            $criteres[] = [
                self::DESCRIPTION => $critere->getEnonce(),
                self::MAXIMUMNOTE => (int) $critere->getPonderation(),
                'sousCriteres' => $this->getSousCriteres($critere->getSousCriteres()),
            ];
        }

        if (empty($criteres) && $this->isExistSinglePriceCriterion($consultation)) {
            $criteres[] = [
                self::DESCRIPTION => 'Prix',
                self::MAXIMUMNOTE => 100
            ];
        } elseif (empty($criteres) && $this->isExistSingleCostCriterion($consultation)) {
            $criteres[] =  [
                self::DESCRIPTION => 'Coût',
                self::MAXIMUMNOTE => 100
            ];
        }

        return $criteres;
    }

    private function getSousCriteres(array $data): array
    {
        $sousCriteres = [];

        foreach ($data as $sousCritere) {
            $sousCriteres[] = [
                self::DESCRIPTION => $sousCritere->getEnonce(),
                self::MAXIMUMNOTE => (int) $sousCritere->getPonderation(),
            ];
        }

        return $sousCriteres;
    }

    /**
     * @return array
     */
    private function addDataForConsultationNotAlloti(Consultation $consultation, array $data)
    {
        $criteres = $consultation->getDonneeComplementaire()->getCriteresAttribution();
        $data['criteres'] = $this->getCriteres($consultation, $criteres);
        $data['soumissionnaires'] = $this->getDataSoumissionnaire($consultation);

        return $data;
    }

    /**
     * @return array
     */
    private function getDataSoumissionnaire(Consultation $consultation)
    {
        $data = [];

        /** @var Offre $offre */
        foreach ($consultation->getOffres() as $offre) {
            if (
                in_array($offre->getStatutOffres(), $this->getStatutEnveloppeOuvert())
                || in_array($offre->getStatutOffres(), $this->getStatutEnveloppeForOffre($offre))
            ) {
                $entreprise = $offre->getInscrit()->getEntreprise();
                $etablissement = $offre->getInscrit()->getEtablissement();

                $dataSoumissionnaire = [
                    'raisonSocial' => $entreprise->getNom(),
                    'siret' => $entreprise->getSiren() . $etablissement->getCodeEtablissement(),
                    'enveloppe' => ['numero' => $offre->getNumeroReponse()]
                ];

                if (!in_array($dataSoumissionnaire, $data)) {
                    $data[] = $dataSoumissionnaire;
                }
            }
        }

        return $data;
    }

    /**
     * @return array
     */
    private function getDataSoumissionnaireWithLot(Lot $lot, Consultation $consultation)
    {
        $soumissionnaires = [];

        $data = $this->lotCandidature->getRaisonSocialAndSiretForEntreprise(
            $lot->getLot(),
            $consultation->getId(),
            $this->getStatutEnveloppeOuvert()
        );

        foreach ($data as $soumissionnaire) {
            $soumissionnaires[] = $soumissionnaire;
        }

        return $soumissionnaires;
    }

    private function criteresAttributionConsultationExists(Consultation $consultation): bool
    {
        return  $this->isExistCriteriasetOutBelowWithTheirWeighting($consultation) ||
            $this->isExistSingleCostCriterion($consultation) ||
            $this->isExistSinglePriceCriterion($consultation) ||
            $this->isExistOrderDescPriorityCriterion($consultation)
            ;
    }

    private function isExistCriteriasetOutBelowWithTheirWeighting(Consultation $consultation): bool
    {
        return !empty($consultation->getDonneeComplementaire()) &&
            count($consultation->getDonneeComplementaire()->getCriteresAttribution()) > 0 ;
    }

    private function isExistSingleCostCriterion(Consultation $consultation): bool
    {
        return !empty($consultation->getDonneeComplementaire()) &&
            $consultation->getDonneeComplementaire()->getIdCritereAttribution() === self::CRITERE_UNIQUE_DE_COUT ;
    }

    private function isExistSinglePriceCriterion(Consultation $consultation): bool
    {
        return !empty($consultation->getDonneeComplementaire()) &&
            $consultation->getDonneeComplementaire()->getIdCritereAttribution() === self::CRITERE_UNIQUE_DE_PRIX ;
    }

    private function isExistOrderDescPriorityCriterion(Consultation $consultation): bool
    {
        return !empty($consultation->getDonneeComplementaire()) &&
            $consultation->getDonneeComplementaire()->getIdCritereAttribution() === self::CRITERE_ORDRE_PRIO_DESC ;
    }

    private function criteresAttributionLotExists(Consultation $consultation): bool
    {
        foreach ($consultation->getLots() as $lot) {
            if (count($this->getCriteresLot($consultation, $lot)) > 0) {
                return true;
            }
        }

        return false;
    }

    private function getJsonRealPath(string $fileName): string
    {
        $jsonFile = $fileName . '.json';
        $data = $this->blobOrganismeFile->getRealPathFileByName($jsonFile);

        if (empty($data)) {
            throw new NotFoundHttpException('File ' . $jsonFile . ' not found');
        }

        return $data[self::REALPATH];
    }

    private function getXlsxRealPath(
        PieceGenereConsultation $pieceGeneree,
        string $fileName
    ): string {
        $xlsxFileName = $fileName . $this->template->getExtension(self::TEMPLATE_NAME);

        $data = $this->blobOrganismeFile->getFile($pieceGeneree->getBlobId()->getId(), true);

        if (empty($data)) {
            throw new NotFoundHttpException('File ' . $xlsxFileName . ' not found');
        }

        return $data[self::REALPATH];
    }

    /**
     * @return int[]
     */
    private function getStatutEnveloppeForOffre(Offre $offre): array
    {
        $data = [];
        /**
         * @var Enveloppe $enveloppe
         */
        foreach ($offre->getEnveloppes() as $enveloppe) {
            $data[] = $enveloppe->getStatutEnveloppe();
        }

        return $data;
    }

    /**
     * Get array of Statut Ouvert en Ligne et Hors Ligne
     */
    private function getStatutEnveloppeOuvert(): array
    {
        return [
            $this->parameterBag->get('STATUT_ENV_OUVERTE_EN_LIGNE'),
            $this->parameterBag->get('STATUT_ENV_OUVERTE'),
            $this->parameterBag->get('STATUT_ENV_OUVERTE_HORS_LIGNE')
        ];
    }
}
