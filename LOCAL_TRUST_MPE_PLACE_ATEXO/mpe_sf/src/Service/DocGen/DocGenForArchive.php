<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use App\Entity\Consultation;
use App\Service\AtexoFichierOrganisme;
use App\Service\WebServicesRedac;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocGenForArchive extends AbstractDocGenService
{
    private const NUM_OF_ATTEMPTS = 2;
    private readonly Filesystem $filesystem;
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly TranslatorInterface $translator,
        private readonly Security $security,
        private readonly WebServicesRedac $redac,
        private readonly EntityManagerInterface $em,
        private readonly MergeFields $argument,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly Template $template,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager,
        private readonly Docgen $docgen,
        private readonly LoggerInterface $logger,
    ) {
        $this->filesystem = new Filesystem();

        parent::__construct(
            $this->parameterBag,
            $this->atexoBlobOrganisme,
            $this->em,
            $this->template,
            $this->translator,
            $this->encryption,
            $this->mountManager,
            $this->filesystem,
        );
    }

    public function generate(
        string $idConsultation,
        string $fileCode,
        string $extension = 'docx',
        bool $convertToPdf = true,
    ): string {
        $attempts = 1;

        do {
            $exception = null;
            try {
                if ($attempts > 1) {
                    $this->logger->info("Tentative de génération du docucment pour la " . $attempts . "ème fois.");
                }

                return $this->generateBody($idConsultation, $fileCode, $convertToPdf, $extension);
            } catch (\Exception $e) {
                $attempts++;
                $erreur = "Erreur lors de la génération du document de l'archive pour la " . $attempts . " ème fois.";
                $erreur .= PHP_EOL . 'Exception ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString();
                $this->logger->error($erreur);
                $exception = $e;
                sleep(5);
                continue;
            }
        } while ($attempts <= self::NUM_OF_ATTEMPTS);

        if ($exception instanceof \Exception) {
            $this->logger->error(
                "Error de génération du document via DocGen. Stack : " . $exception->getTraceAsString()
            );
        }
        return '';
    }

    public function generateBody(
        string $idConsultation,
        string $fileCode,
        bool $convertToPdf,
        string $extension,
    ): string {
        $consultation = $this->em->getRepository(Consultation::class)->findOneBy(['id' => $idConsultation]);
        $tmpTemplateName = 'Depouillement_consultation_' .  $consultation->getReferenceUtilisateur() . '.' . $extension;

        $file = $this->redac->getContent(
            'getTemplateDepouillement'
        );

        if (empty($file)) {
            throw new \Exception($this->translator->trans('ERROR_MESSAGE_DOCUMENT_MODEL_NOT_FOUND'));
        }


        return $this->prepareDumpFile($consultation, $tmpTemplateName, $file, $fileCode, $extension, $convertToPdf);
    }


    protected function prepareDumpFile(
        Consultation $consultation,
        string $tmpTemplateName,
        string $file,
        string $fileCode,
        string $extension,
        bool $convertToPdf = true
    ): string {
        $formData = $this->prepareFormData($consultation, $tmpTemplateName, $file, $fileCode);
        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/octet-stream';
        $url = $this->parameterBag->get('URL_API_DOC_GEN');

        if ($convertToPdf) {
            $url .= '?convertToPdf=true';
            $tmpTemplateName = str_replace($extension, 'pdf', $tmpTemplateName);
        }

        $content = $this->docgen->generate(
            $headers,
            $formData,
            $url
        );

        $pathTmpTemplate = $this->parameterBag->get('COMMON_TMP') . '/' . $tmpTemplateName;
        $this->filesystem->dumpFile($pathTmpTemplate, $content);

        $fileName = "Depouillement_consultation_" . $consultation->getReferenceUtilisateur() . '.pdf';

        $tmpFile = new UploadedFile(
            $pathTmpTemplate,
            $fileName
        );

        return $tmpFile->getPathname();
    }

    protected function prepareFormData(
        Consultation $consultation,
        string $tmpTemplateName,
        string $file,
        string $fileCode
    ): FormDataPart {
        $mergeFieldsValues = json_encode(
            $this->argument->getConsultationDataForArchive($consultation),
            JSON_THROW_ON_ERROR
        );

        $formFields = [
            'keyValues' => new DataPart(
                $mergeFieldsValues,
                '',
                'application/json'
            ),
            'template' => DataPart::fromPath($this->writeFile($tmpTemplateName, $file, $fileCode)),
        ];

        return new FormDataPart($formFields);
    }
}
