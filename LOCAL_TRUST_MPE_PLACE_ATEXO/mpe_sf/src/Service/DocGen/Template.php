<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\DocGen;

use Exception;
use ZipArchive;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Template
{
    private $tmpFolderMpe;

    private readonly Filesystem $filesystem;

    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
        $this->filesystem = new Filesystem();
        $this->tmpFolderMpe = $this->parameterBag->get('COMMON_TMP');
    }

    public function getPathTemplate(string $templateName): string
    {
        $urlGenDoc = $this->parameterBag->get('URL_GENDOC_TEMPLATE');

        return $urlGenDoc . $templateName;
    }

    public function withoutExtension(string $path): string
    {
        return substr($path, 0, strrpos($path, '.'));
    }

    public function getExtension(string $path): string
    {
        $infos = pathinfo($path);

        return '.' . $infos['extension'];
    }

    public function templateExist(string $pathTemplate): bool|string
    {
        $file = file_get_contents($pathTemplate);

        return (!$file) ? false : $file;
    }

    /**
     * @param $documentName
     * @param $content
     * @param $originalName
     */
    public function writeFile($documentName, $content, $originalName): string
    {
        $pathTmpTemplate = $this->tmpFolderMpe . '/' . $documentName;
        $this->filesystem->dumpFile($pathTmpTemplate, $content);
        $tmpFile = new UploadedFile($pathTmpTemplate, $originalName);

        return $tmpFile->getPathname();
    }

    public function removeFile(string $pathTemplate): void
    {
        $this->filesystem->remove($pathTemplate);
    }

    /**
     * @param $fileName
     * @param $path
     *
     * @throws Exception
     */
    public function extractTo($fileName, $path): string
    {
        $zip = new ZipArchive();

        $res = $zip->open($path);

        if (true === $res) {
            $zip->extractTo($this->tmpFolderMpe);
            $zip->close();
        } else {
            throw new Exception('Echec de l\'extraction du fichier' . $fileName);
        }

        return $this->tmpFolderMpe;
    }
}
