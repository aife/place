<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Lot;
use App\Entity\PieceGenereConsultation;
use App\Service\AtexoFichierOrganisme;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Contracts\Translation\TranslatorInterface;

class AbstractDocGenService
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly EntityManagerInterface $em,
        private readonly Template $template,
        private readonly TranslatorInterface $translator,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager,
        private readonly Filesystem $filesystem,
    ) {
    }

    protected function getFileExtension(string $fileName): false|string
    {
        return substr(strrchr($fileName, '.'), 1);
    }

    protected function moveAndInsertFile(
        UploadedFile $tmpFile,
        Consultation $consultation,
        string $documentName,
        string $organisme,
        Lot $lot = null,
        Entreprise $entreprise = null
    ): array {
        $tmpFilePath = $tmpFile->getPathname();
        $this->atexoBlobOrganisme->moveTmpFile($tmpFilePath);

        $idBlob = $this->atexoBlobOrganisme->insertFile(
            $tmpFile->getClientOriginalName(),
            $tmpFile->getPathName(),
            $organisme,
            null,
            $this->template->getExtension($documentName)
        );

        if ($idBlob) {
            $this->filesystem->remove($tmpFilePath);
            $blobOrganisme = $this->em->getRepository(BloborganismeFile::class)
                ->findOneBy(['id' => $idBlob]);

            $pieceGenereConsultation = new PieceGenereConsultation();

            $pieceGenereConsultation->setBlobId($blobOrganisme);
            $pieceGenereConsultation->setConsultation($consultation);
            $pieceGenereConsultation->setCreatedAt(new \DateTime('now'));
            $pieceGenereConsultation->setLot($lot);
            $pieceGenereConsultation->setEntreprise($entreprise);

            $this->em->persist($pieceGenereConsultation);
            $this->em->flush();

            $result['message'] = $this->translator->trans('SUCCES_UPLOAD_GENERATE_DOCUMENT');
            $result['statut'] = 200;
            $result['fichier'] = [];

            $result['fichier']['id'] = $this->encryption->cryptId($idBlob);
            $result['fichier']['nom'] = $blobOrganisme->getName();
            $extension = $this->getFileExtension($blobOrganisme->getName());
            $result['fichier']['extension'] = $extension;

            $result['fichier']['poids'] = $this->mountManager
                ->getFileSize($blobOrganisme->getId(), $organisme, $this->em);
            $result['fichier']['created_at'] =
                $pieceGenereConsultation->getCreatedAt()->format('d/m/Y - G:i');
            $result['fichier']['uploaded'] = true;
        } else {
            $result['message'] = $this->translator->trans('ERROR_UPLOAD_GENERATE_DOCUMENT');
        }

        return $result;
    }

    protected function escapeSpecialChars(string $fileName): string
    {
        $specialChars = ["'", '’', ' '];
        $replacement = ['', '', '_'];

        return str_replace($specialChars, $replacement, $fileName);
    }

    public function writeFile(
        string $documentName,
        string $content,
        string $originalName
    ): string {
        $pathTmpTemplate = $this->parameterBag->get('COMMON_TMP') . '/' . $documentName;
        $this->filesystem->dumpFile($pathTmpTemplate, $content);
        $tmpFile = new UploadedFile($pathTmpTemplate, $originalName);
        try {
            return $tmpFile->getPathname();
        } catch (\Exception) {
        }

        return '';
    }
}
