<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service\DocGen;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Exception;
use DateTime;
use App\Entity\Agent;
use App\Entity\PieceGenereConsultation;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Docgen
{
    public function __construct(private readonly ParameterBagInterface $parameterBag, private readonly HttpClientInterface $client, private readonly EntityManagerInterface $em, private readonly MountManager $mountManager, private readonly LoggerInterface $logger)
    {
    }

    /**
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generate(array $headers, FormDataPart $formData, string $uri): bool|string
    {
        $response = null;
        try {
            $response = $this->client->request(
                'POST',
                $this->parameterBag->get('GENDOC_BASE_URL') . $uri,
                [
                    'headers' => $headers,
                    'body' => $formData->bodyToString(),
                ]
            );

            return $response->getContent();
        } catch (Exception $e) {
            $this->logger->error($response?->getContent(false));
            throw $e;
        }
    }

    /**
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getFileUpdate(string $token): bool|string
    {
        $response = null;
        try {
            $response = $this->client->request(
                'GET',
                $this->parameterBag->get('GENDOC_BASE_URL') .
                '/docgen/api/v1/document-monitoring/status?token=' . $token
            );

            return $response->getContent();
        } catch (Exception $e) {
            $this->logger->error($response->getContent(false));
            throw $e;
        }
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getToken(
        PieceGenereConsultation $pieceGenereConsultation,
        Agent $agent,
        string $organisme,
        string $extension,
        ?string $urlCallBack
    ): ?string {
        if (!$this->tokenIsValid($pieceGenereConsultation)) {
            $pieceGenereConsultation =
                $this->setToken($pieceGenereConsultation, $agent, $organisme, $extension, $urlCallBack);
        }

        return $pieceGenereConsultation->getToken();
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setToken(
        PieceGenereConsultation $pieceGenereConsultation,
        Agent $agent,
        string $organisme,
        string $extension,
        string $callBackUrl = null
    ): PieceGenereConsultation {
        $response = null;
        try {
            $pathDocument =
                $this->mountManager->getAbsolutePath(
                    $pieceGenereConsultation->getBlobId()->getId(),
                    $organisme,
                    $this->em
                );

            $file = DataPart::fromPath($pathDocument, $pathDocument . $extension);

            $editorRequest = [
                'user' => [
                    'id' => $agent->getId(),
                    'name' => $agent->getLogin(),
                ],
                'callback' => $callBackUrl,
                'mode' => 'edit',
                'documentId' => $pieceGenereConsultation->getId(),
                'documentTitle' => $pieceGenereConsultation->getBlobId()->getName(),
                'plateformeId' => $this->parameterBag->get('PF_URL_REFERENCE'),
            ];

            $formFields = [
                'file' => $file,
                'editorRequest' => new DataPart(
                    json_encode($editorRequest, JSON_THROW_ON_ERROR),
                    '',
                    'application/json'
                ),
            ];

            $formData = new FormDataPart($formFields);
            $headers = $formData->getPreparedHeaders()->toArray();
            $headers[] = 'Accept: application/octet-stream';

            $response = $this->client->request(
                'POST',
                $this->parameterBag->get('GENDOC_BASE_URL') . '/docgen/api/v2/document-editor/request',
                [
                    'headers' => $headers,
                    'body' => $formData->bodyToString(),
                ]
            );

            $token = $response->getContent();
            $pieceGenereConsultation->setToken($token);
            $pieceGenereConsultation->setUpdatedAt(new DateTime('now'));


            $this->em->flush();

            return $pieceGenereConsultation;
        } catch (Exception $e) {
            $this->logger->error($response->getContent(false));
            throw $e;
        }
    }

    public function tokenIsValid(PieceGenereConsultation $pieceGenereConsultation): bool
    {
        if (empty($pieceGenereConsultation->getToken())) {
            return false;
        }
        $now = new DateTime('now');
        $interval = $now->diff($pieceGenereConsultation->getUpdatedAt());

        return $interval->format('%R%a') >= 0;
    }
}
