<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use DateTime;
use Exception;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\PieceGenereConsultation;
use App\Service\AtexoFichierOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DocumentGenere
{
    /**
     * DocumentGenere constructor.
     */
    public function __construct(
        private readonly Template $template,
        private readonly EntityManagerInterface $em,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly Docgen $docGen,
        private readonly LoggerInterface $logger
    ) {
    }

    /**
     * @throws FileExistsException
     * @throws FileNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getEditedDocument(
        PieceGenereConsultation $pieceGenereConsultation,
        string $organisme,
        string $templateName
    ): bool {
        $this->logger->info('La mise à jour du document ' . $pieceGenereConsultation->getId() .
            ' au niveau de MPE en cours');

        $content = $this->docGen->getFileUpdate($pieceGenereConsultation->getToken());

        $fileOutputName = $this->template->withoutExtension($templateName) . uniqid();

        $pieceGenereConsultation->getBlobId()->setExtension($this->template->getExtension($templateName));
        $path = $this->template->writeFile(
            $fileOutputName . $this->template->getExtension($templateName),
            $content,
            $fileOutputName
        );

        return $this->updatePathPieceGeneree($pieceGenereConsultation, $organisme, $path);
    }

    /**
     *
     * @throws FileExistsException
     * @throws FileNotFoundException
     */
    public function updatePathPieceGeneree(
        PieceGenereConsultation $piece,
        string $organisme,
        string $path
    ): bool {
        $this->atexoBlobOrganisme->getMountManager()->deleteFile(
            $piece->getBlobId()->getId(),
            $organisme,
            $this->em
        );

        $this->atexoBlobOrganisme->moveTmpFile($path);

        $isInsert = $this->atexoBlobOrganisme->setPath(
            $piece->getBlobId(),
            $organisme,
            null,
            $this->template->getExtension($path)
        );

        $this->template->removeFile($path);
        $this->logger->info('La mise à jour du document au niveau de MPE ' . $piece->getId() . ' est terminée');

        return  $isInsert;
    }

    public function addPieceGeneree(
        Consultation $consultation,
        string $fileName,
        string $path,
        string $organisme
    ): PieceGenereConsultation {
        $idBlob =  $this->addBlobOrganismeFile($fileName, $path, $organisme);
        $blobOrganisme = $this->em->getRepository(BloborganismeFile::class)
            ->findOneBy(['id' => $idBlob]);

        $pieceGeneree = new PieceGenereConsultation();

        $pieceGeneree->setBlobId($blobOrganisme);
        $pieceGeneree->setConsultation($consultation);
        $pieceGeneree->setCreatedAt(new DateTime('now'));

        $this->em->persist($pieceGeneree);
        $this->em->flush();

        return $pieceGeneree;
    }

    /**
     * @throws Exception
     */
    public function addBlobOrganismeFile(
        string $fileName,
        string $path,
        string $organisme
    ): int {
        $this->atexoBlobOrganisme->moveTmpFile($path);
        $id = $this->atexoBlobOrganisme->insertFile(
            $fileName,
            $path,
            $organisme,
            null,
            $this->template->getExtension($path)
        );

        $this->template->removeFile($path);

        return $id;
    }

    /**
     * @return null|PieceGenereConsultation
     */
    public function getByIdConsultationAndFileName(string $fileName, Consultation $consultation)
    {
        return $this->em->getRepository(PieceGenereConsultation::class)
            ->findPieceGenereeByIdConsultationAndFileName($fileName, $consultation->getId());
    }
}
