<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DocGen;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use App\Entity\Consultation;
use App\Entity\Entreprise;
use App\Entity\Lot;
use App\Entity\PieceGenereConsultation;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class OffreNonRetenue
{
    public final const DOCUMENT_NAME = 'Notification de rejet de candidature ou d’offre.docx';
    public final const IS_CONSULTATION_ALLOTI = '1';
    public final const STATUS_VALID = 'Terminé';
    public final const STATUS = 'status';
    public final const A_COMPLETER = 'à compléter';
    public final const KEY = 'key';
    public final const VALUE = 'value';
    public final const ATTRIBUTAIRE = 'attributaire';
    public final const SOUMISSIONNAIRE = 'soumissionnaire';
    public final const SOUMISSIONNAIRES = 'soumissionnaires';

    /**
     * OffreNonRetenue constructor.
     */
    public function __construct(
        private readonly AnalyseOffre $analyseOffre,
        private readonly DocumentGenere $documentGenere,
        private readonly Template $template,
        private readonly MergeFields $mergeFields
    ) {
    }

    /**
     * @return string|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getValues(
        string $documentName,
        Consultation $consultation,
        ?Lot $lot,
        ?Entreprise $entreprise
    ): string {
        $pieceGeneree =  $this->getPieceGenereeAnalyseOffre($consultation);
        $data = $this->getDefaultData($consultation, $lot, $entreprise);

        if (!$this->isDocumentNameValid($documentName)) {
            throw new FileException('Le nom du fichier n\' est pas valid');
        }

        if (
            empty($pieceGeneree)
            || empty($entreprise)
            || ($this->isConsultationAlloti($consultation) && empty($lot))
        ) {
            return json_encode($data, JSON_THROW_ON_ERROR);
        }
        $valuesAnalyseOffre = $this->getDataAnalyseOffre(
            $data,
            $consultation,
            $lot,
            $entreprise,
            $pieceGeneree
        );

        return json_encode($valuesAnalyseOffre, JSON_THROW_ON_ERROR);
    }

    private function getDataAnalyseOffreForConsultationNotAlloti(
        Entreprise $entreprise,
        array $values,
        array $data
    ): array {
        if ($values[self::STATUS] === self::STATUS_VALID) {
            $data = $this->setDataSoumissionnaire($data, $this->getDataSoumissionnaire(
                $entreprise,
                $values[self::SOUMISSIONNAIRES]
            ));
            $data = $this->setDataAttributaire($data, $values[self::ATTRIBUTAIRE]);
        }

        return $data;
    }

    private function getDataSoumissionnaire(Entreprise $entreprise, array $soumissionnaires): array
    {
        $data = [];
        foreach ($soumissionnaires as $soumissionnaire) {
            if ($soumissionnaire['raisonSocial'] === $entreprise->getNom()) {
                $data = $soumissionnaire;
                break;
            }
        }

        return $data;
    }

    public function getDataAnalyseOffreForConsultationAlloti(
        Lot $lot,
        Entreprise $entreprise,
        array $values,
        array $data
    ): array {
        foreach ($values['lots'] as $value) {
            if ($value['numero'] === $lot->getLot()) {
                if ($value[self::STATUS] == self::STATUS_VALID) {
                    $data = $this->setDataSoumissionnaire($data, $this->getDataSoumissionnaire(
                        $entreprise,
                        $value[self::SOUMISSIONNAIRES]
                    ));
                    $data = $this->setDataAttributaire($data, $value[self::ATTRIBUTAIRE]);

                    break;
                }
            }
        }

        return $data;
    }

    /**
     * @param array $soumissionnaires
     */
    public function setDataSoumissionnaire(array $data, ?array $soumissionnaires): array
    {
        $key = 3;

        unset($data[4]);

        if (array_key_exists($key, $data) && !empty($soumissionnaires)) {
            $data[$key] =  [
                self::KEY => self::SOUMISSIONNAIRE, self::VALUE => $soumissionnaires
            ];
        }

        return $data;
    }

    /**
     * @param array $attributaires
     */
    public function setDataAttributaire(array $data, ?array $attributaires): array
    {
        $key = 2;

        if (array_key_exists($key, $data) && !empty($attributaires)) {
            $data[$key] =  [
                self::KEY => self::ATTRIBUTAIRE, self::VALUE => $attributaires
            ];
        }

        return $data;
    }

    /**
     * @return string[]
     */
    public function addDefaultDataSoumissionnaire(): array
    {
        return [
            'classementString' => self::A_COMPLETER,
            'noteGlobale' => self::A_COMPLETER,
            'noteGlobaleAttributaire' => self::A_COMPLETER
        ];
    }

    /**
     * @return string[]
     */
    public function addDefaultDataAttributaire(): array
    {
        return [
            'raisonSocial' => self::A_COMPLETER
        ];
    }

    /**
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getDataAnalyseOffre(
        array $data,
        Consultation $consultation,
        ?Lot $lot,
        ?Entreprise $entreprise,
        ?PieceGenereConsultation $pieceGeneree
    ): array {
        $jsonResponse = $this->analyseOffre->getAnalyseInJson($consultation, $pieceGeneree);
        $arrayResponse = json_decode($jsonResponse, true, 512, JSON_THROW_ON_ERROR);

        if ($this->isConsultationAlloti($consultation)) {
            $data = $this->getDataAnalyseOffreForConsultationAlloti($lot, $entreprise, $arrayResponse, $data);
        } else {
            $data = $this->getDataAnalyseOffreForConsultationNotAlloti($entreprise, $arrayResponse, $data);
        }

        return $data;
    }

    public function isDocumentNameValid(string $documentName): bool
    {
        return $documentName === self::DOCUMENT_NAME;
    }

    private function isConsultationAlloti(Consultation $consultation): bool
    {
        return $consultation->getAlloti() === self::IS_CONSULTATION_ALLOTI;
    }

    private function getPieceGenereeAnalyseOffre(Consultation $consultation): ?PieceGenereConsultation
    {
        $fileName = $this->analyseOffre->getFileOriginalName($consultation);
        $xlsxFileName = $fileName . '.xlsx';

        $pieceGeneree =
            $this->documentGenere->getByIdConsultationAndFileName($xlsxFileName, $consultation);

        return $pieceGeneree;
    }

    /**
     * @return array|array[]
     */
    private function getDefaultData(Consultation $consultation, ?Lot $lot, ?Entreprise $entreprise): array
    {
        $data = $this->mergeFields->pieceGenereeModele($consultation, $lot, $entreprise);


        $data[] =  [
            self::KEY => self::ATTRIBUTAIRE, self::VALUE => $this->addDefaultDataAttributaire()
        ];

        $data[] =  [
            self::KEY => self::SOUMISSIONNAIRE, self::VALUE => $this->addDefaultDataSoumissionnaire()
        ];

        return $data;
    }
}
