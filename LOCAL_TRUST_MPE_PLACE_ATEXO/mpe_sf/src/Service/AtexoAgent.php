<?php

namespace App\Service;

use DateTime;
use Exception;
use ArrayObject;
use App\Entity\Agent;
use App\Entity\Agent\AgentServiceMetier;
use App\Entity\HabilitationAgent;
use App\Entity\HabilitationProfil;
use App\Entity\HistoriqueSuppressionAgent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Organisme;
use App\Entity\Service;
use App\Exception\ApiProblemDbException;
use App\Exception\ApiProblemException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\DataNotFoundException;
use App\Exception\OrganismeServiceNotFoundException;
use App\Model\ApiProblem;
use App\Security\PasswordEncoder\SodiumPepperEncoder;
use App\Service\Agent\AgentServiceMetierService;
use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class AtexoAgent
{
    private $doctrine;

    public function __construct(
        private ContainerInterface $container,
        private readonly SocleHabilitationAgentService $socleHabilitationAgentService,
        private readonly AgentServiceMetierService $agentServiceMetierService,
        /**
         * AtexoService constructor.
         *
         * @param $container
         */
        private readonly SodiumPepperEncoder $sodiumEncoder
    ) {
        $this->doctrine = $this->container->get('doctrine');
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getDoctrine()
    {
        return $this->doctrine;
    }

    /**
     * @param mixed $this ->doctrine
     */
    public function setDoctrine($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param bool $formUpdate
     *
     * @return array
     */
    public function validateInfo(array $arrayInfoAgent, $formUpdate = false)
    {
        $arrayErreur = [];
        $agent = $this->serializeAgent($arrayInfoAgent);
        if (empty($agent->getNom())) {
            $arrayErreur[] = 'Nom vide';
        }
        if (empty($agent->getPrenom())) {
            $arrayErreur[] = 'Prenom vide';
        }
        if ($formUpdate) {
            $this->agentExist($agent, $arrayErreur);
        } else {
            $this->validateLogin($agent, $arrayErreur);
        }

        $this->validateEmail($agent, $arrayErreur);
        $this->validateOrganisme($agent, $arrayErreur);
        $this->validateService($agent, $arrayErreur);
        $this->validateProfil($agent, $arrayErreur);

        return $arrayErreur;
    }

    /**
     * @param $arrayErreur
     */
    public function validateLogin(Agent $agent, &$arrayErreur)
    {
        $agentRepo = $this->doctrine->getRepository(Agent::class);
        if (empty($agent->getLogin())) {
            $arrayErreur[] = 'identifiant vide';
        } elseif ($agentRepo->isOtherAgentWithLoginExist($agent->getLogin(), $agent->getId())) {
            $arrayErreur[] = 'identifiant existe déjà';
        }
    }

    /**
     * @param $arrayErreur
     */
    public function agentExist(Agent $agent, &$arrayErreur)
    {
        $agentRepo = $this->doctrine->getRepository(Agent::class);
        $id = $agent->getId();
        $idExterne = $agent->getIdExterne();
        if (empty($id) && empty($idExterne)) {
            $arrayErreur[] = 'id vide';
        } elseif (
            !empty($id) &&
            !$agentRepo->find($id) &&
            !empty($idExterne) &&
            !$agentRepo->findByIdExterne($idExterne)
        ) {
            $arrayErreur[] = "Aucun agent avec l'id " . $id;
        }
    }

    /**
     * @param $arrayErreur
     */
    public function validateEmail(Agent $agent, &$arrayErreur)
    {
        if (
            empty($agent->getEmail())
            || !$this->container->get('atexo.atexo_util')->checkEmailFormat($agent->getEmail())
        ) {
            $arrayErreur[] = 'Email invalid';
        }
        // nous allons à l'avenir mettre une vérification supplémentaire par rapport au socle externe/interne
    }

    /**
     * @param $arrayErreur
     */
    public function validateService(Agent $agent, &$arrayErreur)
    {
        $serviceVo = $agent->getService();
        if ($serviceVo instanceof Service) {
            try {
                $this->doctrine->getRepository(Service::class)
                    ->findByIdOrOldIdAndOrganisme($serviceVo->getId(), $agent->getAcronymeOrganisme());
            } catch (OrganismeServiceNotFoundException) {
                $arrayErreur[] = "Aucun service avec l'id " . $serviceVo->getId() . " dans l'organisme " .
                    $agent->getAcronymeOrganisme();
            }
        }
    }

    /**
     * @param $arrayErreur
     */
    public function validateProfil(Agent $agent, &$arrayErreur)
    {
        if (empty($agent->getIdProfil())) {
            $arrayErreur[] = "Agent n'est rattaché à aucun profil";
        } else {
            $profil = $this->doctrine->getRepository(HabilitationProfil::class)->find($agent->getIdProfil());
            if (empty($profil)) {
                $arrayErreur[] = "Aucun profil avec l'id " . $agent->getIdProfil();
            }
        }
    }

    /**
     * @param $arrayErreur
     */
    public function validateOrganisme(Agent $agent, &$arrayErreur)
    {
        if (empty($agent->getAcronymeOrganisme())) {
            $arrayErreur[] = "Agent n'est rattaché à aucun organisme";
        } else {
            $organisme = $this->doctrine->getRepository(Organisme::class)
                ->findOneBy(['acronyme' => $agent->getAcronymeOrganisme()]);
            if (empty($organisme)) {
                $arrayErreur[] = "Aucun organisme avec l'acronyme " . $agent->getAcronymeOrganisme();
            }
        }
    }

    public function createOrUpdateEntity(Agent $node, $mode)
    {
        try {
            $em = $this->doctrine->getManager();
            $agent = null;
            if (!empty($node->getIdExterne())) {
                $agent = $this->doctrine->getRepository(Agent::class)->findOneByIdExterne($node->getIdExterne());
                if($agent instanceof Agent) {
                    $node->setId($agent->getId());
                }
            } elseif (null !== $node->getId()) {
                $agent = $this->doctrine->getRepository(Agent::class)->find($node->getId());
            }
            if (!$agent instanceof Agent) {
                if ('update' === $mode) {
                    throw new DataNotFoundException(404, ApiProblem::TYPE_NOT_FOUND);
                }
                $agent = new Agent();
            }

            /** @var Organisme $organisme * */
            $organisme = $this->doctrine->getRepository(Organisme::class)
                ->findOneBy(['acronyme' => $node->getAcronymeOrganisme()]);
            if ($organisme instanceof Organisme) {
                $agent->setOrganisme($organisme);
                $agent->setAcronymeOrganisme($organisme->getAcronyme());
                if ($node->getService() instanceof Service && !empty($node->getService()->getId())) {
                    $service = $this->doctrine->getRepository(Service::class)
                        ->findByIdOrOldIdAndOrganisme($node->getService()->getId(), $organisme->getAcronyme());
                    $agent->setService($service);
                    $agent->setServiceId($service->getId());
                } else {
                    $agent->setService(null);
                    $agent->setServiceId(null);
                }

                $actif = $node->getActif();
                $actif = ('1' === $actif || 'true' === $actif) ? 1 : 0;

                $agent->setActif($actif);
                $agent->setNom($node->getNom());
                $agent->setPrenom($node->getPrenom());
                $habilitation = null;
                $habilitationAgent = null;
                if ($agent->getId()) {
                    $habilitationAgent = $this->doctrine->getRepository(HabilitationAgent::class)
                        ->find($agent->getId());
                }
                if ($agent->getIdProfilSocleExterne() != $node->getIdProfil()
                    || null === $habilitationAgent
                ) {
                    $agent->setIdProfilSocleExterne($node->getIdProfil());
                    $idAgent = $node->getId();
                    if (null === $habilitationAgent) {
                        $idAgent = null;
                    }
                    $habilitation = $this->addOrModifyEnablingsOfTheAgents($node->getIdProfil(), $idAgent);
                    if ($agent->getId() && 0 == $habilitation->getInvitePermanentTransverse()) {
                        $this->cleanInvitePermanentTransverse($agent->getId());
                    }
                }
                $dateTime = new DateTime();
                $date = $dateTime->format('Y:m:d H:i:s');

                if ('create' == $mode) {
                    $agent->setDateCreation($date);
                }

                $agent->setLogin($node->getLogin());
                $agent->setDateModification($date);
                $agent->setEmail($node->getEmail());
                $agent->setIdExterne($node->getIdExterne());
                $agent->setTelephone($node->getTelephone());
                $agent->setFax($node->getFax());

                $em->beginTransaction();
                $em->persist($agent);
                $em->flush();
                if ($habilitation instanceof HabilitationAgent) {
                    $habilitation->setAgent($agent);
                    $em->persist($habilitation);
                    $em->flush();
                }
                self::addOrModifyAgentServiceMetier(
                    $agent->getId(),
                    $this->container->getParameter('SERVICE_METIER_MPE'),
                    $agent->getIdProfilSocleExterne()
                );
                $em->commit();

                return $agent;
            }
        } catch (ApiProblemException | DataNotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            $em->rollback();
            throw new ApiProblemDbException($e->getMessage());
        }
    }

    /**
     * affecte les habilitations à un agent selon le profile.
     *
     * @param int $idProfile l'id du profile de l'agent
     *
     * @return HabilitationAgent $hablitation habilitations des agents
     */
    public function addOrModifyEnablingsOfTheAgents($idProfil, $idAgent = null)
    {
        try {
            if ($idAgent) {
                $hablitation = $this->doctrine->getRepository(HabilitationAgent::class)->find($idAgent);
                if (!$hablitation) {
                    $hablitation = new HabilitationAgent();
                    $hablitation->setAgent($idAgent);
                }
            } else {
                $hablitation = new HabilitationAgent();
            }
            $profil = $this->doctrine->getRepository(HabilitationProfil::class)->find($idProfil);
            if ($profil instanceof HabilitationProfil) {
                $arrayobj = new ArrayObject((array) $profil);
                foreach ($arrayobj as $key => $value) {
                    $key = str_replace(HabilitationProfil::class, '', $key);
                    if ('libelle' == $key || 'id' == $key) {
                        break;
                    }
                    $fct = ucfirst(ltrim($key));
                    $set = 'set' . $fct;
                    if (method_exists($hablitation, $set)) {
                        $hablitation->$set($value);
                    }
                }
            }

            return $hablitation;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode(Agent $agent, Serializer $serializer, $mode)
    {
        $agentNormalize = $serializer->normalize($agent, $mode, ['groups' => ['webservice']]);
        $content = $serializer->serialize($agentNormalize, $mode);
        if ('xml' == $mode) {
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace("\n", '', $content);
            $content = str_replace('response>', 'agent>', $content);
        }

        return $content;
    }

    /**
     * @param array $node
     *
     * @return Agent
     */
    public function createEntity(array $arrayInfoAgent)
    {
        $agent = $this->serializeAgent($arrayInfoAgent);

        return $this->createOrUpdateEntity($agent, 'create');
    }

    /**
     * @param array $node
     *
     * @return Agent
     */
    public function updateEntity(array $arrayInfoAgent)
    {
        $agent = $this->serializeAgent($arrayInfoAgent);

        return $this->createOrUpdateEntity($agent, 'update');
    }

    /**
     * @param $arrayInfoAgent
     *
     * @return Agent
     */
    public function serializeAgent($arrayInfoAgent)
    {
        try {
            $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
            $encoders = [new XmlEncoder(), new JsonEncoder()];
            $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer($classMetadataFactory)];
            $serializer = new Serializer($normalizers, $encoders);
            $agent = $serializer->denormalize($arrayInfoAgent, Agent::class);

            if (array_key_exists('service', $arrayInfoAgent)) {
                $service = $serializer->denormalize($arrayInfoAgent['service'], Service::class);
                $agent->setService($service);
            }

            return $agent;
        } catch (Exception $e) {
            throw $e;
        }
    }

    private function cleanInvitePermanentTransverse($idAgent)
    {
        $em = $this->doctrine->getManager();
        $listInvitePermanentTransverse = $this->doctrine
            ->getRepository(InvitePermanentTransverse::class)
            ->findByAgent($idAgent);
        if ($listInvitePermanentTransverse) {
            foreach ($listInvitePermanentTransverse as $permanentTransverse) {
                $em->remove($permanentTransverse);
            }
            $em->flush();
        }
    }

    /**
     * @return Agent
     *
     * @throws Exception
     */
    public function createHyperAdmin(Organisme $organisme)
    {
        $em = $this->doctrine->getManager();
        $agent = new Agent();
        $agent->setLogin('hyperadmin');
        $agent->setPassword('b1ec4e52da9742d5098dfffe89a1f17647030d8f');
        $agent->setEmail('suivi.mpe@atexo.com');
        $agent->setNom('Admin');
        $agent->setPrenom('Hyper');
        $dateTime = new DateTime();
        $date = $dateTime->format('Y:m:d H:i:s');
        $agent->setOrganisme($organisme);
        $agent->setAcronymeOrganisme($organisme->getAcronyme());
        $agent->setDateModification($date);
        $agent->setDateCreation($date);
        $agent->setServiceId(0);

        $em->persist($agent);
        $em->flush();

        $habilitationAgent = $this->addOrModifyEnablingsOfTheAgents(1);
        $habilitationAgent->setHyperAdmin('1');
        $habilitationAgent->setAdministrerCompte('1');
        $habilitationAgent->setGestionAgentPole('1');
        $habilitationAgent->setGestionAgents('1');
        $habilitationAgent->setGestionBiCles('1');
        $habilitationAgent->setGestionHabilitations('1');
        $habilitationAgent->setGestionTypeValidation('1');
        $habilitationAgent->setDroitGestionServices('1');
        $habilitationAgent->setGestionCompteJal('1');
        $habilitationAgent->setGestionCompteBoamp('1');
        $habilitationAgent->setAdministrerProcedure('1');
        $habilitationAgent->setAdministrerProceduresFormalisees('1');
        $habilitationAgent->setGestionMapa('1');
        $habilitationAgent->setAgent($agent);
        $em->persist($habilitationAgent);

        $em->flush();

        return $agent;
    }

    /**
     * @return Agent
     *
     * @throws Exception
     */
    public function createAdmin(Organisme $organisme, $passwordAdministrateur)
    {
        $em = $this->doctrine->getManager();
        $acronyme = $organisme->getAcronyme();
        $agent = new Agent();
        $agent->setLogin('admin_' . $acronyme);
        $agent = $this->updateNewAgentPasswordByArgon2($agent, $passwordAdministrateur);
        $agent->setEmail('suivi.mpe@atexo.com');
        $agent->setNom(strtoupper($acronyme));
        $agent->setPrenom('Admin');
        $dateTime = new DateTime();
        $date = $dateTime->format('Y:m:d H:i:s');
        $agent->setOrganisme($organisme);
        $agent->setAcronymeOrganisme($organisme->getAcronyme());
        $agent->setDateModification($date);
        $agent->setDateCreation($date);

        $em->persist($agent);
        $em->flush();

        $habilitationAgent = $this->addOrModifyEnablingsOfTheAgents(1);
        $habilitationAgent->setAgent($agent);
        $em->persist($habilitationAgent);
        $em->flush();

        return $agent;
    }

    public function updateNewAgentPasswordByArgon2(Agent $agent, string $password): Agent
    {
        $password = htmlspecialchars_decode($password);
        $agent->setTypeHash(Agent::ARGON_TYPE);
        $agent->setPassword($this->sodiumEncoder->encodePassword($password, null));

        return $agent;
    }

    /**
     * @param $idAgent
     * @param $idServiceMetier
     * @param $idProfile
     *
     * @throws Exception
     */
    public function addOrModifyAgentServiceMetier($idAgent, $idServiceMetier, $idProfile)
    {
        try {
            $agentServiceMetier = $this->doctrine->getRepository(AgentServiceMetier::class)->findOneBy([
                'idAgent' => $idAgent,
                'idServiceMetier' => $idServiceMetier,
            ]);
            if (
                $agentServiceMetier instanceof AgentServiceMetier
                && $agentServiceMetier->getIdProfilService() != $idProfile
            ) {
                $this->agentServiceMetierService->modifyIdProfilAgentServiceMetier($agentServiceMetier, $idProfile);
            } elseif (!($agentServiceMetier instanceof AgentServiceMetier)) {
                $this->agentServiceMetierService->createAgentServiceMetier(
                    $idAgent,
                    $idServiceMetier,
                    $idProfile
                );
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * permet la gestion de la suppression d'un agent.
     * On renseigne la date deleted_at, on passe son service à null, on passe le nombre de tentative de mdp à 3
     * On historise cette suppression dans la table historisque_suppression_agent.
     *
     * @param bool $callByWS
     *
     * @return |null
     *
     * @throws Exception
     */
    public function deleteEntity(array $arrayInfoAgent, $callByWS = true)
    {
        $em = null;
        $node = $this->serializeAgent($arrayInfoAgent);
        try {
            $em = $this->doctrine->getManager();
            $agent = null;
            if (!empty($node->getIdExterne())) {
                $agent = $this->doctrine->getRepository(Agent::class)->findOneByIdExterne($node->getIdExterne());
            } elseif (null !== $node->getId()) {
                $agent = $this->doctrine->getRepository(Agent::class)->find($node->getId());
            }
            if (!$agent instanceof Agent) {
                throw new ApiProblemNotFoundException();
            }

            $historiqueSuppressionAgent = $this->doctrine->getRepository(HistoriqueSuppressionAgent::class)
                ->findOneBy(['idAgentSupprime' => $agent->getId()]);
            if ($historiqueSuppressionAgent instanceof HistoriqueSuppressionAgent) {
                throw new ApiProblemNotFoundException();
            }

            $dateTime = new DateTime();
            $date = $dateTime->format('Y:m:d H:i:s');

            $historiqueSuppressionAgent = new HistoriqueSuppressionAgent();
            $historiqueSuppressionAgent->setIdAgentSupprime($agent->getId());
            $historiqueSuppressionAgent->setDateSuppression($date);
            $em->beginTransaction();
            $em->persist($historiqueSuppressionAgent);
            $em->flush();

            $historiqueId = $historiqueSuppressionAgent->getId();
            if (true === $callByWS) {
                $agent->setLogin($historiqueId . '_' . $agent->getLogin());
                $agent->setEmail($historiqueId . '_' . $agent->getEmail());
            } else {
                // il faudra à l'avenir mettre un code particulier si on veut utiliser la suppression agent côté SF
                // en interne
            }
            $agent->setTentativesMdp(3);
            $agent->setServiceId(null);
            $agent->setDeletedAt(new DateTime());
            $agent->setDateModification($date);
            $agent->setActif('0');

            $em->flush();
            $em->commit();

            return $agent;
        } catch (ApiProblemException $e) {
            throw $e;
        } catch (Exception $e) {
            $em->rollback();
            throw new ApiProblemDbException($e->getMessage());
        }
    }
}
