<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Message\DumePublication;
use App\Message\GenerationArchiveDocumentaire;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class MessageBusService
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly LoggerInterface $logger
    ) {
    }

    public function publishDumePublication(string $type, bool $isPublish, ?string $contexteLtDumeId = null): void
    {
        $message = $isPublish ? 'publication ' : 'validation';
        $this->logger->info('Lancement du message DumePublication pour le type : ' . $type . 'en mode ' .
            $message . 'pour le contexteLtDumeId : ' . $contexteLtDumeId);
        $this->messageBus->dispatch(new DumePublication($type, $isPublish, $contexteLtDumeId));
    }

    public function publishGenerationArchiveDocumentaire(
        string $consultationId,
        string $agentId,
        string $json,
        ?string $type = null,
        ?string $supprimerApres = null
    ): void {
        $this->logger->info('Lancement du message GenerationArchiveDocumentaire pour la consultationId : ' .
        $consultationId, [$consultationId, $agentId, $json, $type, $supprimerApres]);
        $this->messageBus->dispatch(new GenerationArchiveDocumentaire(
            $consultationId,
            $agentId,
            $json,
            $type,
            $supprimerApres
        ));
    }
}
