<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

interface PradoPasswordEncoderInterface
{
    /**
     * Encodes the plain password.
     *
     * @param string $userClassName the user name class (Agent::class or Inscrit::class ...)
     */
    public function encodePassword(string $userClassName, string $plainPassword): string;

    /**
     * test if encoded password and password is same
     *
     * @param string $userClassName the user name class (Agent::class or Inscrit::class ...)
     */
    public function isPasswordValid(string $userClassName, string $encodedPassword, string $plainPassword): bool;

}
