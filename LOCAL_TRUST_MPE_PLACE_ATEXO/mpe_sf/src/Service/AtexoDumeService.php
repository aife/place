<?php

namespace App\Service;

use Doctrine\Persistence\ManagerRegistry;
use LogicException;
use ZipArchive;
use Exception;
use App\Entity\BloborganismeFile;
use App\Entity\TDumeContexte;
use App\Entity\TDumeNumero;
use App\Repository\BloborganismeFileRepository;
use App\Service\Handler\MpeRotatingFileHandler;
use AtexoDume\Client\Client as ClientDume;
use AtexoDume\Dto\Entreprise\Consultation;
use AtexoDume\Dto\Entreprise\Contact;
use AtexoDume\Dto\Entreprise\Metadata;
use AtexoDume\Dto\Lot;
use Doctrine\ORM\EntityManager;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AtexoDumeService pour la gestion de DUME.
 *
 * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
 */
class AtexoDumeService
{
    private readonly Logger|LoggerInterface $logger;

    /**
     * AtexoDumeService constructor.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function __construct(
        private ContainerInterface $container,
        LoggerInterface $logger,
        private TranslatorInterface $translator,
        private AtexoConfiguration $moduleStateChecker,
        private AtexoUtil $atexoUtil,
        private AtexoTelechargementConsultation $atexoTelechargementConsultation,
        private ManagerRegistry $registry
    ) {
        $this->container = $container;
        $this->logger = $logger;
    }

    /**
     * @return mixed
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param $container
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * Permet de créer la connexion vers lt_dume.
     *
     * @return ClientDume
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function createConnexionLtDume()
    {
        $client = new ClientDume(
            $this->container->getParameter('URL_PF_DUME_API'),
            $this->container->getParameter('LOGIN_DUME_API'),
            $this->container->getParameter('PASSWORD_DUME_API'),
            $this->logger,
            null
        );

        return $client;
    }

    /**
     * Permet de créer les metadata pour lt_dume.
     *
     * @param string $plateforme
     * @param int    $idConsultation
     * @param int    $idContextAcheteur
     * @param int    $idUser
     * @param string $siret
     * @param string $typeOE
     * @param bool   $first             = false
     *
     * @return Metadata
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function createMetadataLtDume(
        $plateforme,
        $idConsultation,
        $idContextAcheteur,
        $idUser,
        $siret,
        $typeOE,
        $first = false
    ) {
        $metadata = new Metadata();
        $metadata->setIdentifiantPlateforme($plateforme);
        $metadata->setIdConsultation($idConsultation);
        $metadata->setIdContextDumeAcheteur($idContextAcheteur);
        $metadata->setIdInscrit($idUser);
        $metadata->setSiret($siret);
        $metadata->setTypeOE($typeOE);
        $metadata->setFirst($first);

        return $metadata;
    }

    /**
     * Permet de créer les lots pour lt_dume.
     *
     * @param int    $numLot
     * @param string $intituleLot
     *
     * @return Lot
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function createLotDume($numLot, $intituleLot)
    {
        $lot = new Lot();
        $lot->setNumeroLot($numLot);
        $lot->setIntituleLot($intituleLot);

        return $lot;
    }

    /**
     * Permet de créer le contact pour lt_dume.
     *
     * @param $inscrit
     *
     * @return Contact
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function createContactDume($inscrit)
    {
        $contact = new Contact();
        $contact->setNom($inscrit->getPrenom().' '.$inscrit->getNom());
        $contact->setTelephone($inscrit->getTelephone());
        $contact->setEmail($inscrit->getEmail());

        return $contact;
    }

    /**
     * Permet de créer la consultation pour lt_dume.
     *
     * @param array    $lots
     * @param Metadata $metadata
     * @param Contact  $contact
     * @param bool     $alloti
     *
     * @return Consultation
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function createConsultationDume($lots, $metadata, $contact, $alloti)
    {
        $consultation = new Consultation();

        if (1 == $alloti) {
            $consultation->setLots($lots);
        }

        $consultation->setMetadata($metadata);
        $consultation->setContact($contact);

        return $consultation;
    }

    /**
     * @return array
     */
    public function getDume($consultation)
    {
        $infosDume = [];
        if ($this->moduleStateChecker->hasConfigPlateforme('interfaceDume')
            && $consultation
            && $consultation->getDumeDemande()
        ) {
            /** @var EntityManager $em */
            $em = $this->getDoctrine();
            $dumeContext = $em
                ->getRepository(TDumeContexte::class)
                ->getWaitingOrPublishDumeContext(
                    $consultation->getId(),
                    $consultation->getAcronymeOrg(),
                    $this->getParameter('TYPE_DUME_ACHETEUR'),
                    $this->getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'),
                    $this->getParameter('STATUT_DUME_CONTEXTE_PUBLIE')
                );

            $infosDume['dumeStandard'] = false;
            if ($dumeContext instanceof TDumeContexte && true === $dumeContext->isStandard()) {
                $infosDume['dumeStandard'] = true;
            }

            if (!empty($dumeContext)) {
                if ($dumeContext instanceof TDumeContexte) {
                    if ($dumeContext->getStatus() != $this->getParameter(
                        'STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'
                    )) {
                        $dumeNumeros = $em
                            ->getRepository(TDumeNumero::class)
                            ->getLastDumeNumeroByDumeContexte($dumeContext->getId());

                        if ($dumeNumeros instanceof TDumeNumero) {
                            if (!empty($dumeNumeros->getBlobIdXml())) {
                                $blodId = $dumeNumeros->getBlobIdXml();
                                $infosDume['format'] = 'xml';
                            } else {
                                $blodId = $dumeNumeros->getBlobId();
                                $infosDume['format'] = 'pdf';
                            }
                            $tailleDume = $this->atexoUtil
                                ->arrondirSizeFile((
                                    $this->atexoTelechargementConsultation
                                        ->getFileSize(
                                            $blodId,
                                            $consultation->getAcronymeOrg()
                                        )
                                    / 1024));

                            // Si la taille du dume est de 0 c'est que le fichier n'existe pas.
                            // alors on ne souhaite pas afficher le lien de téléchargement.
                            if (0 === (int) substr($tailleDume, 1)) {
                                $infosDume['dumePublie'] = '';
                            } else {
                                $infosDume['dumePublie'] = $this->translator->trans('DEFINE_DUME_ACHETEUR')
                                    .'- '.$tailleDume;
                            }

                            $infosDume['idNumero'] = $dumeNumeros->getId();
                            $infosDume['acronymeOrganisme'] = $dumeContext->getOrganisme();
                            $infosDume['idConsultation'] = $dumeContext->getConsultation()->getId();
                        }
                    }
                }
            }
        }

        return $infosDume;
    }

    /**
     * Shortcut to return the Doctrine Registry service.
     *
     * @throws LogicException If DoctrineBundle is not available
     * @final since version 3.4*/
    protected function getDoctrine()
    {
        if (!$this->container->has('doctrine')) {
            throw new LogicException('The DoctrineBundle is not registered in your application. 
                Try running "composer require symfony/orm-pack".');
        }

        return $this->registry;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     *
     * @final since version 3.4
     */
    protected function getParameter($name)
    {
        return $this->container->getParameter($name);
    }

    /**
     * Permet de retourner le zip DUME.
     *
     * @param $idNumero
     * @param $acronymeOrganisme
     * @param $format
     *
     * @return array()
     */
    public function downloadZipDUMEById($idNumero, $acronymeOrganisme, $format)
    {
        $filename = null;
        try {
            $result = [];
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $dumeNumeroAndBlob = $em
                ->getRepository(TDumeNumero::class)
                ->findOneBy(['id' => $idNumero]);
            if ($dumeNumeroAndBlob instanceof TDumeNumero) {
                $dir = $this->container->getParameter('BASE_ROOT_DIR').$acronymeOrganisme.'/files/';
                $dirArchive = $this->container->getParameter('COMMON_TMP');
                $blobId = null;
                if ('pdf' == $format) {
                    $filename = $dumeNumeroAndBlob->getBlobId().'-0';
                    $blobId = $dumeNumeroAndBlob->getBlobId();
                } elseif ('xml' == $format) {
                    $filename = $dumeNumeroAndBlob->getBlobIdXml().'-0';
                    $blobId = $dumeNumeroAndBlob->getBlobId();
                }

                if (!file_exists($dir.$filename) && $blobId) {
                    /** @var BloborganismeFileRepository $blobOrganismeFileRepository */
                    $blobOrganismeFileRepository = $em->getRepository(BloborganismeFile::class);
                    $organismeBlobFichierRepository = $blobOrganismeFileRepository->getOrganismeBlobFichier($blobId);
                    if ($organismeBlobFichierRepository instanceof BloborganismeFile) {
                        $chemin = null;
                        if (null !== $organismeBlobFichierRepository->getChemin()) {
                            $chemin = $organismeBlobFichierRepository->getChemin();
                            $dir = $this->container->getParameter('BASE_ROOT_DIR').
                                $organismeBlobFichierRepository->getChemin();
                        }
                        if (!file_exists($dir.$filename)) {
                            $mnts = $this->getParameter('DOCUMENT_ROOT_DIR_MULTIPLE');
                            if ('' !== $mnts) {
                                $arrayMnts = explode(';', (string) $mnts);
                                foreach ($arrayMnts as $mnt) {
                                    $dir = $mnt . $chemin;
                                    if (file_exists($dir . $filename)) {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                $archive = new ZipArchive();
                $archiveName = $dirArchive.'DUME_ACHETEUR_'.$filename.session_id().time();

                if (true === $archive->open($archiveName, ZipArchive::CREATE)) {
                    $archive->addFile($dir.$filename, $dumeNumeroAndBlob->getNumeroDumeNational().'.'.$format);
                    $downloadName = $this->translator->trans('DEFINE_DUME_ACHETEUR');
                }
                $archive->close();
                $result['archiveName'] = $archiveName;
                $result['downloadName'] = $downloadName;
                // prepare BinaryFileResponse
            }
        } catch (Exception $e) {
            $this->logger->info(' Erreur lors de telechargement du Zip DUME Acheteur '
                .$e->getMessage().' '.$e->getTraceAsString());
        }

        return $result;
    }
}
