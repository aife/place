<?php

/*
 * Class Authentication : classe de gestion de l'authentification
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Service;

use Exception;
use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Service\Entreprise\EntrepriseVerificationService;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\UserInterface;

class Authentication
{
    /**
     * @var string
     */
    private $agentProviderName;

    /**
     * @var string
     */
    private $entrepriseProviderName;

    /**
     * @var string
     */
    private $agentFirewallName;

    /**
     * @var string
     */
    private $entrepriseFirewallNname;

    /**
     * @return mixed
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * Authentication constructor.
     *
     * @param Logger $logger
     * @param string $agentProviderName
     * @param string $entrepriseProviderName
     * @param string $agentFirewallName
     * @param string $entrepriseFirewallNname
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(
        private readonly TokenStorageInterface $tokenStorage,
        private readonly SessionInterface $session,
        private LoggerInterface $logger,
        private readonly CurrentUser $currentUser,
        private readonly RequestStack $requestStack,
        private readonly EntrepriseVerificationService $entrepriseVerificationService,
        ParameterBagInterface $parameterBag = null
    ) {
        $this->agentProviderName = $parameterBag->get('agent_provider_name');
        $this->EntrepriseProviderName = $parameterBag->get('entreprise_provider_name');
        $this->agentFirewallName = $parameterBag->get('agent_firewall_name');
        $this->entrepriseFirewallNname = $parameterBag->get('entreprise_firewall_name');
    }

    /**
     * Permet d'authentifier l'utilisateur.
     *
     * @param  $user
     *
     * @return bool
     *
     * @throws Exception
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016*/
    public function authenticateUser($user)
    {
        if (empty($user)) {
            $log = 'The user does not exist.';
            $this->logger->error($log);
            throw new Exception($log);
        }

        $this->logger->info("Debut authentification de l'utilisateur (login = " . $user->getUsername() . ')');

        try {
            $firewallFkey = $this->getUserFirewallKey($user);

            if (null === $firewallFkey) {
                $log = 'Provider does not exist.';
                $this->logger->error($log);

                throw new Exception($log);
            }

            $this->logger->info(
                'Authentification en cours (user login = ' . $user->getUsername() . ') : ' .
                PHP_EOL . "[ProviderKey = $firewallFkey]" .
                PHP_EOL . '[User roles = ' . implode(',', $user->getRoles()) . ']'
            );

            $token = new UsernamePasswordToken($user, null, $firewallFkey, $user->getRoles());

            $this->tokenStorage->setToken($token);

            $this->session->set('_security_' . $firewallFkey, serialize($token));
            $this->session->save();

            $this->logger->info(
                'Utilisateur avec login = ' .
                $user->getUsername() . ' authentifie avec SUCCES !!!'
            );

            return true;
        } catch (Exception $e) {
            $messageErreur =
                "ERREUR lors de l'authentification de l'utilisateur (login = " . $user->getUsername() . ').' . PHP_EOL;
            $messageErreur .= 'Message erreur : ' . $e->getMessage() . PHP_EOL . 'Trace : ' . $e->getTraceAsString();
            $this->logger->error($messageErreur);
        } finally {
            //$this->logger->info("Fin authentification de l'utilisateur (login = " . $user->getUsername() .")");
        }
    }

    /**
     * Permet de deconnecter l'utilisateur.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function disconnectUser()
    {
        if ($this->currentUser->isConnected()) {
            $user = $this->currentUser->getCurrentUser();

            try {
                $this->tokenStorage->setToken(null);
                $this->requestStack->getCurrentRequest()->getSession()->invalidate();

                return true;
            } catch (Exception $e) {
                $messageErreur =
                    "ERREUR lors de la deconnexion de l'utilisateur (login = " . $user->getUsername() . ').' . PHP_EOL;
                $messageErreur .=
                    'Message erreur : ' . $e->getMessage() . PHP_EOL . 'Trace : ' . $e->getTraceAsString();
                $this->logger->error($messageErreur);
            } finally {
                //$this->logger->info("Fin deconnexion de l'utilisateur (login = " . $user->getUsername() . ")");
            }
        }
    }

    /**
     * Permet de recuperer le nom de la cle du firewall pour l'utilisateur.
     *
     * @param  $user
     *
     * @return null
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    private function getUserFirewallKey($user)
    {
        if (in_array('ROLE_AGENT', $user->getRoles())) {
            return $this->agentFirewallName;
        } elseif (in_array('ROLE_ENTREPRISE', $user->getRoles())) {
            return $this->entrepriseFirewallNname;
        }

        return null;
    }

    public function initSession(UserInterface $user, string $locale)
    {
        $contexte = [];
        $contexte['id'] = $user->getId();
        $contexte['nom'] = $user->getNom();
        $contexte['prenom'] = $user->getPrenom();
        $contexte['pwd'] = $user->getPassword();
        $contexte['username'] = $user->getLogin();
        $contexte['langue'] = $locale;
        $contexte['prado_connect'] = false;

        if ($user instanceof Administrateur) {
            $contexte['type_user'] = 'admin';
        } elseif ($user instanceof Agent) {
            $contexte['type_user'] = 'agent';
            $contexte['organisme'] = $user->getAcronymeOrganisme();
        } else {
            $contexte['type_user'] = 'entreprise';
            $contexte['email'] = $user->getEmail();
            $contexte['entreprise_id'] = $user->getEntreprise()->getId();
            $contexte['etablissement_id'] = $user->getEtablissement()->getIdEtablissement();
            $contexte['codeAcces'] ??= null;
        }

        $siretAlert = $this->entrepriseVerificationService->getSiretAlert();

        $this->session->set('contexte_authentification', $contexte);
        $this->session->set('token_contexte_authentification', $this->session->getId());
        $this->session->set('_locale', $locale);
        if (!empty($siretAlert)) {
            $this->session->getFlashBag()->add('alertSiret', $siretAlert);
        }
        $sfAttributes = $this->session->get('_sf2_attributes');
    }
}
