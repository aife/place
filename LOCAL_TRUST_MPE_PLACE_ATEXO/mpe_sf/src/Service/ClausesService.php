<?php

/*
 * Class AtexoUtil : classe contenant des methodes utiles
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Service;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Lot;
use App\Entity\Referentiel\Consultation\ClausesN1;
use App\Entity\Referentiel\Consultation\ClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN4;
use App\Repository\Consultation\ClausesN1Repository;
use App\Repository\Consultation\ClausesN2Repository;
use App\Repository\Consultation\ClausesN3Repository;
use App\Repository\Consultation\ClausesN4Repository;
use App\Repository\ConsultationRepository;
use App\Repository\ContratTitulaireRepository;
use App\Repository\LotRepository;
use Doctrine\ORM\OptimisticLockException;

class ClausesService
{
    public function __construct(
        private readonly ClausesN1Repository $clausesN1Repository,
        private readonly ClausesN2Repository $clausesN2Repository,
        private readonly ClausesN3Repository $clausesN3Repository,
        private readonly ClausesN4Repository $clausesN4Repository,
        private readonly IriConverterInterface $iriConverter,
        private readonly ConsultationRepository $consultationRepository,
        private LotRepository $lotRepository,
        private ContratTitulaireRepository $contratTitulaireRepository
    ) {
    }

    /**
     * @param array<array> $clauses
     * @throws OptimisticLockException
     */
    public function updateClauses(Consultation|Lot|ContratTitulaire $object, array $clauses): void
    {
        $this->deleteClausesRelatedTo($object, false);

        foreach ($clauses as $rawClauseN1) {
            /** @var ClausesN1 $referentielClauseN1 **/
            $referentielClauseN1 = $this->iriConverter->getItemFromIri($rawClauseN1['referentielClauseN1']);
            if (!$referentielClauseN1->isActif()) {
                continue;
            }

            $clauseN1 = new Consultation\ClausesN1();
            if ($object instanceof Consultation) {
                $clauseN1->setConsultation($object);
            } elseif ($object instanceof Lot) {
                $clauseN1->setLot($object);
            } else {
                $clauseN1->setContrat($object);
            }
            $clauseN1->setReferentielClauseN1($referentielClauseN1);
            $this->clausesN1Repository->add($clauseN1, false);

            foreach ($rawClauseN1['clausesN2'] as $rawClauseN2) {
                /** @var ClausesN2 $referentielClauseN2 **/
                $referentielClauseN2 = $this->iriConverter->getItemFromIri($rawClauseN2['referentielClauseN2']);
                if (!$referentielClauseN2->isActif()) {
                    continue;
                }

                $clauseN2 = new Consultation\ClausesN2();
                $clauseN2->setClauseN1($clauseN1);
                $clauseN2->setReferentielClauseN2($referentielClauseN2);
                $this->clausesN2Repository->add($clauseN2, false);

                if (!empty($rawClauseN2['clausesN3'])) {
                    foreach ($rawClauseN2['clausesN3'] as $rawClauseN3) {
                        /** @var ClausesN3 $referentielClauseN3 **/
                        $referentielClauseN3 = $this->iriConverter->getItemFromIri($rawClauseN3['referentielClauseN3']);
                        if (!$referentielClauseN3->isActif()) {
                            continue;
                        }

                        $clauseN3 = new Consultation\ClausesN3();
                        $clauseN3->setClauseN2($clauseN2);
                        $clauseN3->setReferentielClauseN3($referentielClauseN3);
                        $this->clausesN3Repository->add($clauseN3, false);

                        if (!empty($rawClauseN3['clausesN4'])) {
                            foreach ($rawClauseN3['clausesN4'] as $rawClauseN4) {

                                /** @var ClausesN4 $referentielClauseN4 * */
                                $referentielClauseN4 = $this->iriConverter->getItemFromIri(
                                    $rawClauseN4['referentielClauseN4']['id']
                                );
                                if (!$referentielClauseN4->getActif()) {
                                    continue;
                                }
                                $clauseN4 = new Consultation\ClausesN4();
                                $clauseN4->setClauseN3($clauseN3);
                                $clauseN4->setReferentielClausesN4($referentielClauseN4);
                                $clauseN4->setValeur($rawClauseN4['referentielClauseN4']['value']);
                                $this->clausesN4Repository->add($clauseN4, false);
                            }
                        }
                    }
                }
            }
        }
    }

    public function getIriFromItem(ClausesN1|ClausesN2|ClausesN3 $clause): string
    {
        return $this->iriConverter->getIriFromItem($clause);
    }

    public function deleteClausesRelatedTo(Consultation|Lot|ContratTitulaire $object, bool $flush = true): void
    {
        foreach ($object->getClausesN1() as $clauseN1) {
            $this->clausesN1Repository->remove($clauseN1, $flush);
        }
    }

    public function getClausesN1LabelByConsultationId(int $consultationId): array
    {
        if (!$consultation = $this->consultationRepository->find($consultationId)) {
            return [];
        }

        $clauseSocialeEnvironnementale = [];
        foreach ($consultation->getClausesN1() as $clauseN1) {
            $slugN1 = $clauseN1->getReferentielClauseN1()->getSlug();
            $clauseSocialeEnvironnementale[$slugN1] = [];

            foreach ($clauseN1->getClausesN2() as $clauseN2) {
                $clauseSocialeEnvironnementale[$slugN1][] = $clauseN2->getReferentielClauseN2()->getSlug();
            }
        }

        return $clauseSocialeEnvironnementale;
    }

    public function hasClauseSocialeByIdAndType(int $objectId, string $objectType): bool
    {
        if ($objectType == 'consultation') {
            $object = $this->consultationRepository->find($objectId);
        } elseif ($objectType == 'lot') {
            $object = $this->lotRepository->find($objectId);
        } else {
            $object = $this->contratTitulaireRepository->find($objectId);
        }
        if (empty($object)) {
            return false;
        }

        foreach ($object->getClausesN1() as $clauseN1) {
            if ($clauseN1->getReferentielClauseN1()->getSlug() == 'clausesSociales') {
                return true;
            }
        }

        return false;
    }

    public function hasClauseEnvironnementaleByIdAndType(int $objectId, string $objectType): bool
    {
        if ($objectType == 'consultation') {
            $object = $this->consultationRepository->find($objectId);
        } elseif ($objectType == 'lot') {
            $object = $this->lotRepository->find($objectId);
        } else {
            $object = $this->contratTitulaireRepository->find($objectId);
        }
        if (empty($object)) {
            return false;
        }

        foreach ($object->getClausesN1() as $clauseN1) {
            if ($clauseN1->getReferentielClauseN1()->getSlug() == 'clauseEnvironnementale') {
                return true;
            }
        }

        return false;
    }

    public function getFormatedClausesForApiInjection(Consultation|Lot $object): array
    {
        $clausesN1 = [];
        foreach ($object->getClausesN1() as $clauseN1) {
            $clausesN2 = [];
            foreach ($clauseN1->getClausesN2() as $clauseN2) {
                $clausesN3 = [];
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $clausesN3[] = [
                        'referentielClauseN3' => $this->getFormatedIriFromItem(
                            $clauseN3->getReferentielClauseN3()
                        )
                    ];
                }
                $clausesN2[] = [
                    'referentielClauseN2' => $this->getFormatedIriFromItem($clauseN2->getReferentielClauseN2()),
                    'clausesN3' => $clausesN3
                ];
            }

            $clausesN1[] = [
                'referentielClauseN1' => $this->getFormatedIriFromItem($clauseN1->getReferentielClauseN1()),
                'clausesN2' => $clausesN2
            ];
        }

        return $clausesN1;
    }

    public function getFormatedClausesForApiInjectionWithSlug(Consultation|Lot $object): array
    {
        $clausesN1 = [];
        foreach ($object->getClausesN1() as $clauseN1) {
            $clausesN2 = [];
            foreach ($clauseN1->getClausesN2() as $clauseN2) {
                $clausesN3 = [];
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $clausesN4 = [];
                    foreach ($clauseN3->getClausesN4() as $clauseN4) {
                        $clausesN4[] = [
                            'referentielClauseN4'   => $clauseN4->getReferentielClausesN4()->getSlug(),
                            'valeur'                => $clauseN4->getValeur()
                        ];
                    }
                    $clausesN3[] = [
                        'referentielClauseN3'   => $clauseN3->getReferentielClauseN3()->getSlug(),
                        'clausesN4'             => $clausesN4
                    ];
                }
                $clausesN2[] = [
                    'referentielClauseN2'   => $clauseN2->getReferentielClauseN2()->getSlug(),
                    'clausesN3'             => $clausesN3
                ];
            }
            $clausesN1[] = [
                'referentielClauseN1'   => $clauseN1->getReferentielClauseN1()->getSlug(),
                'clausesN2'             => $clausesN2
            ];
        }

        return $clausesN1;
    }

    private function getFormatedIriFromItem(ClausesN1|ClausesN2|ClausesN3 $clause): string
    {
        return str_replace('/index.php', '', $this->iriConverter->getIriFromItem($clause));
    }
}
