<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Organisme;
use App\Exception\DataNotFoundException;
use App\Utils\Utils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;

class AtexoConfiguration
{
    private array $configurationOrganisme = [];
    private ConfigurationPlateforme $configurationPlateforme;

    /**
     * AtexoConfiguration constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $em,
        private readonly CurrentUser $currentUser,
        private readonly Utils $utilsService,
    ) {
        $this->configurationOrganisme = [];
    }

    /**
     * Vérifie si le modue en qst est activé ou pas.
     *
     * @param     string $champ
     *
     * @return    bool
     *
     * @author    Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version   1
     *
     * @since     develop
     *
     * @copyright Atexo 2016
     */
    public function hasConfigPlateforme($champ)
    {
        $active = false;
        if (property_exists(new ConfigurationPlateforme(), $champ)) {
            $config = $this->em
                ->getRepository(ConfigurationPlateforme::class)
                ->findOneBy([$champ => 1]);
            if ($config instanceof ConfigurationPlateforme) {
                $active = true;
            }
        }

        return $active;
    }

    /**
     * Vérifie si le module en qst est activé ou pas.
     *
     * @param     string $acronyme
     * @param     string $champ
     *
     * @return    bool
     *
     * @author    Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version   1
     *
     * @since     develop
     *
     * @copyright Atexo 2016
     */
    public function hasConfigOrganisme($acronyme, $champ)
    {
        $active = false;

        if (property_exists(new ConfigurationOrganisme(), $champ)) {
            $config = $this->em
                ->getRepository(ConfigurationOrganisme::class)
                ->findOneBy([$champ => '1', 'organisme' => $acronyme]);
            if ($config instanceof ConfigurationOrganisme) {
                $active = true;
            }
        }

        return $active;
    }

    /**
     * Vérifie si le module est activé.
     */
    public function isModuleEnabled(string $moduleName, ?Organisme $organisme = null, string $prefixe = 'get'): bool
    {
        $right = false;
        $method = $prefixe . ucfirst($moduleName);

        // Si la colonne existe, charge le droit du module pour l'organisme
        $moduleOrganism = null;
        if (!empty($organisme)) {
            $confOrganism = $this->getConfigurationOrganisme($organisme);

            if (method_exists($confOrganism, $method)) {
                $moduleOrganism = (bool) $confOrganism->$method();
            }
        }
        // Si la colonne existe, charge le droit du module pour la plateforme
        $modulePlatform = null;
        $confPlatform = $this->getConfigurationPlateforme();
        if (method_exists($confPlatform, $method)) {
            $modulePlatform = (bool) $confPlatform->$method();
        }

        if (null === $moduleOrganism && null === $modulePlatform) {
            throw new DataNotFoundException(
                Response::HTTP_NOT_IMPLEMENTED,
                'Module ' . $moduleName . ' params not found'
            );
        }

        if (true === $moduleOrganism || (null === $moduleOrganism && true === $modulePlatform)) {
            $right = true;
        }

        return $right;
    }

    /**
     * Permet de recuperer la configuration plateforme.
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getConfigurationPlateforme(): ConfigurationPlateforme
    {
        if (!isset($this->configurationPlateforme)) {
            $this->configurationPlateforme = $this->em->getRepository(ConfigurationPlateforme::class)
                ->getConfigurationPlateforme();
        }
        return $this->configurationPlateforme;
    }

    /**
     * Permet de recuperer une configuration organisme par l'organisme.
     *
     * @param Organisme $organisme
     *
     * @return ConfigurationOrganisme
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getConfigurationOrganisme(Organisme $organisme): ConfigurationOrganisme
    {
        $organismeAcronyme = $organisme->getAcronyme();
        if (!isset($this->configurationOrganisme[$organismeAcronyme])) {
            $this->configurationOrganisme[$organismeAcronyme] =  $this->em->getRepository(ConfigurationOrganisme::class)
                ->findOneBy(
                    [
                    'organisme' => $organismeAcronyme,
                    ]
                );
        }
        return $this->configurationOrganisme[$organismeAcronyme];
    }

    /**
     * Retourne l'encoding de la plateforme.
     *
     * @param string|null $chaine Chaine à encoder
     *
     * @return string
     */
    public function toPfEncoding(?string $chaine): string
    {
        if ('utf-8' == strtolower($this->parameterBag->get('HTTP_ENCODING'))) {
            if ($this->isUTF8($chaine)) {
                return $chaine;
            } else {
                return utf8_encode($chaine);
            }
        } elseif ($this->isUTF8($chaine)) {
            return utf8_decode($chaine);
        } else {
            return $chaine;
        }
    }

    /**
     * Vérifie si le module est actif InterfaceChorusPmi est accessible pour le service et
     * si aucun service n'est affecté à l'Agent, on vérifie si le module est activé au niveau de l'Organisme
     *
     * @param Agent $agent Agent concerné par la vérification de l'activation du module InterfaceChorusPmi
     * @return bool
     */
    public function isChorusAccessActivate(Agent $agent): bool
    {
        $organisme = $agent->getOrganisme();
        $service = $agent->getService();

        if (!empty($service)) {
            return boolval(intval($service->getAccesChorus()));
        }

        return $this->isModuleEnabled('InterfaceChorusPmi', $organisme);
    }

    /**
     * Détecte si une chaine est en utf-8.
     *
     * @param string|null $str chaine a détecter
     * @return bool
     */
    public function isUTF8(?string $str): bool
    {
        if ($str === mb_convert_encoding(mb_convert_encoding($str, 'UTF-32', 'UTF-8'), 'UTF-8', 'UTF-32')) {
            return true;
        }

        return false;
    }

    public function hasConfigurationOrganisme(string $module): bool
    {
        $configurationOrganisme = $this->em->getRepository(ConfigurationOrganisme::class)
            ->findOneBy(['organisme' => $this->currentUser->getOrganisme()]);
        $getterMethod = $this->utilsService->getGetterMethodName($module, ConfigurationOrganisme::class);

        return $configurationOrganisme && ('1' === $configurationOrganisme->$getterMethod()
                || true === $configurationOrganisme->$getterMethod());
    }
}
