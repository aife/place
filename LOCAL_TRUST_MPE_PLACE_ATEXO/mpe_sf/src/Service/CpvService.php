<?php

/*
 * Class AtexoUtil : classe contenant des methodes utiles
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class CpvService
{
    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function isValidCpv($code, $categorie)
    {
        $categoryUpper = strtoupper($categorie);
        $prefix = intval(substr($code, 0, 2));

        $isTravaux = ($this->parameterBag->get('PRESTATION_TRAVAUX') === $categoryUpper && 45 === $prefix);
        $isFournitures = ($this->parameterBag->get('PRESTATION_FOURNITURES') === $categoryUpper && ($prefix < 45 || 48 === $prefix));
        $isServices = ($this->parameterBag->get('PRESTATION_SERVICES') === $categoryUpper && $prefix >= 49);

        if ($isTravaux || $isFournitures || $isServices) {
            return true;
        }

        return false;
    }

    public function getCategorieByCpv($code)
    {
        $prefix = intval(substr($code, 0, 2));
        $categorie = null;

        if (45 === $prefix) {
            $categorie = $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX');
        }
        if ($prefix < 45 || 48 === $prefix) {
            $categorie = $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES');
        }
        if ($prefix >= 49) {
            $categorie = $this->parameterBag->get('TYPE_PRESTATION_SERVICES');
        }

        return $categorie;
    }

    public function getCategorieLabel($idCategorie)
    {
        return match ($idCategorie) {
            $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX') => 'Travaux',
            $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES') => 'Fournitures',
            $this->parameterBag->get('TYPE_PRESTATION_SERVICES') => 'Services',
            default => null,
        };
    }
}
