<?php

namespace App\Service\ClientWs;

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;
use App\Exception\ClientWsException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;

/**
 * Class ClientWs.
 */
class ClientWs implements ClientWsInterface
{
    private readonly Client $client;

    /**
     * ClientWs constructor.
     */
    public function __construct(private readonly ContainerInterface $container, array $config = [])
    {
        $proxy = trim($container->getParameter('URL_PROXY'));
        if (!empty($proxy)) {
            $proxyPort = trim($container->getParameter('PORT_PROXY'));
            if (!empty($proxyPort)) {
                $proxy .= ':'.$proxyPort;
            }
            $config['proxy'] = $proxy;
        }
        $this->client = new Client($config);
    }

    /**
     * @return ResponseInterface
     */
    public function post(string $url = null, array $options = [])
    {
        try {
            return $this->client->post(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw new ClientWsException($clientException->getMessage(), $clientException->getRequest(), $clientException->getResponse());
        }
    }

    /**
     * @return ResponseInterface
     */
    public function get(string $url = null, array $options = [])
    {
        try {
            return $this->client->get(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return ResponseInterface
     */
    public function put(string $url = null, array $options = [])
    {
        try {
            return $this->client->put(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return ResponseInterface
     */
    public function patch(string $url = null, array $options = [])
    {
        try {
            return $this->client->patch(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return ResponseInterface
     */
    public function delete(string $url = null, array $options = [])
    {
        try {
            return $this->client->delete(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return ResponseInterface
     */
    public function head(string $url = null, array $options = [])
    {
        try {
            return $this->client->head(
                $url,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return mixed|ResponseInterface
     *
     * @throws GuzzleException
     */
    public function send(RequestInterface $request, array $options = [])
    {
        try {
            return $this->client->send(
                $request,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @param $method
     * @param $uri
     *
     * @return mixed|ResponseInterface
     *
     * @throws GuzzleException
     */
    public function request($method, $uri, array $options = [])
    {
        try {
            return $this->client->request(
                $method,
                $uri,
                $options
            );
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @param null $options
     *
     * @return array|mixed|null
     */
    public function getConfig($options = null)
    {
        try {
            return $this->client->getConfig($options);
        } catch (ClientException $clientException) {
            throw $this->getClientWsException($clientException);
        }
    }

    /**
     * @return ClientWsException
     */
    private function getClientWsException(ClientException $clientException)
    {
        return new ClientWsException(
            $clientException->getMessage(),
            $clientException->getRequest(),
            $clientException->getResponse(),
            $clientException,
            $clientException->getHandlerContext()
        );
    }
}
