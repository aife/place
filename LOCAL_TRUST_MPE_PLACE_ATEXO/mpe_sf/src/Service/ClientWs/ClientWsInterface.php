<?php

namespace App\Service\ClientWs;

use Psr\Http\Message\RequestInterface;

/**
 * Interface ClientWsInterface.
 */
interface ClientWsInterface
{
    public function post(string $url = null, array $options = []);

    public function get(string $url = null, array $options = []);

    public function put(string $url = null, array $options = []);

    public function patch(string $url = null, array $options = []);

    public function delete(string $url = null, array $options = []);

    public function head(string $url = null, array $options = []);

    public function send(RequestInterface $request, array $options = []);

    public function request($method, $uri, array $options = []);

    public function getConfig($option = null);
}
