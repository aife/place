<?php

namespace App\Service\DossierVolumineux;

use Ramsey\Uuid\UuidInterface;
use App\Entity\Agent;
use App\Entity\BlobFichier;
use App\Entity\ConfigurationOrganisme;
use App\Entity\Consultation;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Echange;
use App\Entity\EchangeDestinataire;
use App\Entity\Enveloppe;
use App\Entity\Inscrit;
use App\Service\AtexoConfiguration;
use App\Service\ClientWs\ClientWs;
use App\Service\CurrentUser;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Exception\ORMException;
use Exception;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class Gestion des DV.
 */
class DossierVolumineuxService
{
    private const TYPE_ENTREPRISE = 'entreprise';
    private const TYPE_AGENT = 'agent';

    private static array $prefix = [
        self::TYPE_ENTREPRISE => 'REP-',
        self::TYPE_AGENT => 'A-',
    ];

    /**
     * DossierVolumineuxService constructor.
     */
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly CurrentUser $currentUser,
        private readonly EntityManagerInterface $em,
        private readonly ValidatorInterface $validator,
        private readonly LoggerInterface $envolLogger,
        private readonly ClientWs $clientWs,
        private readonly AtexoConfiguration $atexoConfiguration,
        private readonly SessionInterface $session
    ) {
    }

    /**
     * Création de la référence du DV chez nous.
     *
     * @return string $uuidReference
     *
     * @throws \Exception
     */
    private function createUuidReference()
    {
        if ($this->currentUser->isAgent()) {
            $currentPrefix = self::$prefix[self::TYPE_AGENT];
        } else {
            $currentPrefix = self::$prefix[self::TYPE_ENTREPRISE];
        }

        // Concaténation du préfix + tron
        return $currentPrefix . Uuid::uuid4();
    }

    /**
     * Fonction local pour persister l'entité (factorisation).
     *
     * @return bool renvoie true si tout est OK
     *
     * @throws ORMException
     */
    private function persist(DossierVolumineux $dossierVolumineux)
    {
        try {
            $this->em->persist($dossierVolumineux);
            $this->em->flush();

            return true;
        } catch (ORMException $e) {
            $this->envolLogger->error('Cannot insert entity reference: ' . $dossierVolumineux->getUuidReference());
            throw $e;
        }
    }

    /**
     * Vérifie que le tableau reçu est valide pour une création de DV. crée une exception InvalidArgumentException.
     *
     * @param string $nom
     *
     * @return bool
     */
    public function checkIfValidToCreate($nom)
    {
        $constraint = new Assert\Collection([
            'nom' => new Assert\NotBlank(),
        ]);

        $errors = $this->validator->validate(['nom' => $nom], $constraint);

        if (!empty($errors[0])) {
            $errorMessage = '[ECHEC] le nom du dossier volumineux est vide, impossible de créer un DV';
            $this->envolLogger->error($errorMessage);
            throw new \InvalidArgumentException($errorMessage);
        }

        return true;
    }

    /**
     * création d'un dossier volumineux.
     *
     * @return string uuidtechnique
     *
     * @throws ORMException
     */
    public function createDossierVolumineux(): DossierVolumineux
    {
        $uuid = null;
        do {
            $uuid = $this->createUuidReference();
        } while ($this->uuidAlreadyExist($uuid));

        $dossierVolumineux = new DossierVolumineux();
        $dossierVolumineux->setUuidReference($uuid);
        $dossierVolumineux->setUuidTechnique($this->createUuidTechnique());
        $dossierVolumineux->setDateCreation();
        $dossierVolumineux->setActif(0);
        $dossierVolumineux->setStatut('init');

        if ($this->currentUser->isAgent()) {
            $agent = $this->em->getRepository(Agent::class)->find($this->currentUser->getIdAgent());
            $dossierVolumineux->setAgent($agent);
        } else {
            $inscrit = $this->em->getRepository(Inscrit::class)->find($this->currentUser->getIdInscrit());
            $dossierVolumineux->setInscrit($inscrit);
        }

        $this->persist($dossierVolumineux);

        return $dossierVolumineux;
    }

    /**
     * Création d'un uuid_technique (pour la demande de session auprès de tus)
     * dit "uuid_technique" dans notre application.
     *
     * @return UuidInterface
     *
     * @throws \Exception
     */
    private function createUuidTechnique()
    {
        return Uuid::uuid1();
    }

    private function uuidAlreadyExist($uuidreference)
    {
        $dossierVolumineux = $this->em
            ->getRepository(DossierVolumineux::class)
            ->findOneByUuidReference($uuidreference);

        if (empty($dossierVolumineux)) {
            return false;
        }
        $this->envolLogger->info('uuid: ' . $uuidreference . ' already exist.');

        return true;
    }

    /**
     * @param $params ['reference', 'nomDossierVolumineux', 'serverLogin', 'serverPassword', 'serverURL', 'statut']
     * @param $uuidTechnique
     *
     * @return mixed
     *
     * @throws \Exception
     */
    public function callTusSession($params, $uuidTechnique)
    {
        if (!$this->checkParameters($params) || (empty($uuidTechnique) && isset($uuidTechnique))) {
            $msg = '[ECHEC] Paramètre(s) manquant(s)';
            $this->envolLogger->error($msg);
            throw new InvalidArgumentException($msg);
        }
        $msg = '[ECHEC] Problème pour récupèrer la session. Exception : ';
        try {
            $url = $this->parameterBag->get('URL_API_TUS') . 'session/' . $uuidTechnique;
            $options = [
                'connect_timeout'   => $this->parameterBag->get('TUS_CONNECT_TIMEOUT'),
                'timeout'           => $this->parameterBag->get('TUS_TIMEOUT')
            ];
            $options['json'] = $params;
            $reponse = $this->clientWs->post($url, $options);
            if (method_exists($reponse, 'json')) {
                $callBack = $reponse->json();
            } else {
                $callBack = json_decode($reponse->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
            }
        } catch (Exception $exception) {
            $this->envolLogger->error($msg . $exception->getMessage());
            throw new Exception($msg . $exception->getMessage());
        }

        if (!isset($callBack['uuid'])) {
            $msg .= 'uuid non renvoyé par tus';
            $this->envolLogger->error($msg);
            throw new Exception($msg);
        }

        return $callBack;
    }

    public function checkParameters($params)
    {
        $constraint = new Assert\Collection([
            'reference' => new Assert\NotBlank(),
            'nomDossierVolumineux' => new Assert\NotBlank(),
            'serverLogin' => new Assert\NotBlank(),
            'serverPassword' => new Assert\NotBlank(),
            'serverURL' => new Assert\NotBlank(),
            'statut' => new Assert\NotBlank(),
        ]);
        $errors = $this->validator->validate($params, $constraint);

        if (!empty($errors[0])) {
            $errorMessage = '[ERROR] - les paramètres suivants sont vides ou invalides : ';
            $this->envolLogger->error($errorMessage . $errors->__toString());
            throw new \InvalidArgumentException("{$errorMessage} \n" . $errors->__toString());
        }

        return true;
    }

    /**
     * Récupération d'un dossier volumineux en fonction de son ID et de son propriétaire (idAgent / idInscrit).
     *
     * @param $id
     *
     * @return bool|object|null
     */
    public function getDossierVolumineux($id)
    {
        $dossierVolumineux = $this->em
            ->getRepository(DossierVolumineux::class)
            ->find($id);

        if (empty($dossierVolumineux) || !$this->isAuthorized($dossierVolumineux)) {
            return false;
        }

        return $dossierVolumineux;
    }

    /**
     * Récupération d'un dossier volumineux à partir de son uuid.
     *
     * @param $uuidReference
     *
     * @return object|null
     */
    public function getDossierVolumineuxByUuid($uuidReference)
    {
        $dossierVolumineux = $this->em
            ->getRepository(DossierVolumineux::class)
            ->findOneBy(['uuidReference' => $uuidReference]);

        if ($dossierVolumineux instanceof DossierVolumineux) {
            return $dossierVolumineux;
        } else {
            throw new \InvalidArgumentException('Dossier volumineux inconnu');
        }
    }

    /**
     * @return bool
     */
    public function checkServiceDossierVolumineux()
    {
        if (!$this->isEnable()) {
            throw new BadRequestHttpException('Cette page n\'existe pas !');
        }

        return true;
    }

    /**
     * Vérifie si l'utilisateur courant à le droit de manipuler le DV courant.
     *
     * @return bool
     */
    public function isAuthorized(DossierVolumineux $dossierVolumineux)
    {
        if (!empty($dossierVolumineux->getAgent())) {
            return $dossierVolumineux->getAgent()->getid() == $this->currentUser->getIdAgent();
        } elseif (!empty($dossierVolumineux->getInscrit())) {
            return $dossierVolumineux->getInscrit()->getid() == $this->currentUser->getIdInscrit();
        } else {
            return false;
        }
    }

    /**
     * Permettre d'activer un dossier volumineux si l'utilisateur est le propriétaire.
     *
     * @param $id
     *
     * @return bool
     *
     * @throws ORMException
     */
    public function enableDossierVolumineux($id)
    {
        $dossierVolumineux = $this->getDossierVolumineux($id);
        if (false === $dossierVolumineux) {
            return false;
        }

        $dossierVolumineux->setStatut(1);

        return $this->persist($dossierVolumineux);
    }

    /**
     * Permettre de désactiver un dossier volumineux si l'utilisateur est le propriétaire.
     *
     * @param $id
     *
     * @return bool
     *
     * @throws ORMException
     */
    public function disableDossierVolumineux($id)
    {
        $dossierVolumineux = $this->getDossierVolumineux($id);
        if (false === $dossierVolumineux) {
            return false;
        }

        $dossierVolumineux->setStatut(0);

        return $this->persist($dossierVolumineux);
    }

    /**
     * @return bool
     *
     * @throws Exception
     */
    public function isAuthorizedToDownload(
        DossierVolumineux $dossierVolumineux,
        EchangeDestinataire $echangeDestinataire = null
    ) {
        try {
            $this->checkServiceDossierVolumineux();
            if (!empty($echangeDestinataire)) {
                return $this->isDownloadableFromMail($dossierVolumineux, $echangeDestinataire);
            }

            return $this->isDownloadable($dossierVolumineux);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return bool
     */
    public function isDownloadableFromMail(
        DossierVolumineux $dossierVolumineux,
        EchangeDestinataire $echangeDestinataire
    ) {
        $echange = $this->em->getRepository(Echange::class)->find($echangeDestinataire->getIdEchange());
        $res = false;
        if (!empty($echange) && !empty($echange->getDossierVolumineux())) {
            $res = $echange->getDossierVolumineux()->getUuidReference() === $dossierVolumineux->getUuidReference();
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function isCurrentUserConnected()
    {
        return $this->currentUser->isConnected();
    }

    /**
     * @return bool
     */
    public function isDownloadable(DossierVolumineux $dossierVolumineux)
    {
        if ($this->isAttachedToOnlineConsultation($dossierVolumineux)) {
            return true;
        }
        if (false === $this->isCurrentUserConnected()) {
            return false;
        }

        if (
            $this->isAuthorized($dossierVolumineux)
            || $this->isAttachedToCurrentUserExchange($dossierVolumineux)
            || $this->isAgentAllowedToDownload($dossierVolumineux)
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isAttachedToOnlineConsultation(DossierVolumineux $dossierVolumineux)
    {
        $nbConsultation = $this->em->getRepository(Consultation::class)
            ->findOnlineConsultationWithDossierVolumineux($dossierVolumineux->getId());

        if ($nbConsultation > 0) {
            return true;
        }

        return false;
    }

    public function isAttachedToCurrentUserExchange(DossierVolumineux $dossierVolumineux)
    {
        $isAttached = $this->em->getRepository(Echange::class)
            ->isCurrentUserRecipientToDossierVolumineux($dossierVolumineux->getId(), $this->currentUser);

        if ($isAttached) {
            return true;
        }

        return false;
    }

    /**
     * Vérifie qu'un agent est habilité à télécharger un dossier volumineux
     * La variable de session 'allowedDossiersVolumineux' est sauvegardée dans ouvertureEtAnalyse et
     * DetailEchangeMailAgent.
     *
     * @return bool
     */
    public function isAgentAllowedToDownload(DossierVolumineux $dossierVolumineux)
    {
        if ($this->session->has('allowedDossiersVolumineux')) {
            $allowedDossiersVolumineux = $this->session->get('allowedDossiersVolumineux');
            if (is_array($allowedDossiersVolumineux)) {
                return in_array($dossierVolumineux->getId(), $allowedDossiersVolumineux);
            }
        }

        return false;
    }

    /**
     * @return object|null
     */
    public function getUser(CurrentUser $currentUser)
    {
        if ($currentUser->isEntreprise()) {
            return $inscrit = $this->em->getRepository(Inscrit::class)
                ->find($currentUser->getIdInscrit());
        } elseif (!empty($this->currentUser->getIdAgent())) {
            return $this->em->getRepository(Agent::class)
                ->find($this->currentUser->getIdAgent());
        }

        return null;
    }

    /**
     * @return array
     */
    public function getCriterias(CurrentUser $currentUser)
    {
        $user = $this->getUser($currentUser);
        $criterias = ['statut' => ['complet', 'incomplet']];
        if ($currentUser->getIdAgent()) {
            $criterias['agent'] = $user;
        } else {
            $criterias['inscrit'] = $user;
        }

        return $criterias;
    }

    /**
     * @return array
     */
    public function findAll(CurrentUser $currentUser)
    {
        $criterias = $this->getCriterias($currentUser);

        return $this->em->getRepository(DossierVolumineux::class)
            ->findBy($criterias);
    }

    /**
     * @return bool
     *
     * @throws ORMException
     */
    public function toggleStatut(DossierVolumineux $dossierVolumineux)
    {
        if ($this->isAuthorized($dossierVolumineux)) {
            $dossierVolumineux->setActif(!$dossierVolumineux->isActif());

            return $this->persist($dossierVolumineux);
        }

        return false;
    }

    /**
     * @return bool
     *
     * @throws ORMException
     */
    public function download(DossierVolumineux $dossierVolumineux)
    {
        if ($this->isAuthorized($dossierVolumineux)) {
            $dossierVolumineux->setActif(!$dossierVolumineux->isActif());

            return $this->persist($dossierVolumineux);
        }

        return false;
    }

    /**
     * Informe si le module Envol est activé
     * Agent => lié à son organisme
     * Entreprise ou non connecté => que le module soit actif pour au moins un organisme
     */
    public function isEnable(): bool
    {
        if (!empty($this->currentUser) && $this->currentUser->isAgent()) {
            $currentUserOrganisme = $this->currentUser->getAcronymeOrga();

            $organismeRepository = $this->em->getRepository(ConfigurationOrganisme::class)
                ->findOneBy(['organisme' => $currentUserOrganisme]);

            if (!empty($organismeRepository) && $organismeRepository->isModuleEnvol()) {
                return true;
            }
        } else {
            $organismeRepository = $this->em->getRepository(ConfigurationOrganisme::class)
                ->findBy(['moduleEnvol' => true]);

            if (!empty($organismeRepository) && count($organismeRepository) > 0) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permettre d'obtenir l'ensemble des DV utilisables pour l'utilisateur courant.
     *
     * @return array
     */
    public function getActiveDVforCurrentUser()
    {
        if ($this->currentUser->isAgent()) {
            $dossierVolumineux = $this->em
                ->getRepository(DossierVolumineux::class)
                ->findBy(['agent' => $this->currentUser->getIdAgent(), 'actif' => 1]);
        } else {
            $dossierVolumineux = $this->em
                ->getRepository(DossierVolumineux::class)
                ->findBy(['inscrit' => $this->currentUser->getIdInscrit(), 'actif' => 1]);
        }

        return $dossierVolumineux;
    }

    /**
     * @param $idDossierVolumineux
     * @param bool $flushMode
     *
     * @return bool
     *
     * @throws ORMException
     */
    public function setDossierVolumineuxFromEnveloppe(Enveloppe $enveloppe, $idDossierVolumineux, $flushMode = true)
    {
        $dossierVolumineux = null;
        try {
            $dossierVolumineux = $this->em->getRepository(DossierVolumineux::class)->find($idDossierVolumineux);
            $enveloppe->setDossierVolumineux($dossierVolumineux);
            $this->em->persist($enveloppe);
            if ($flushMode) {
                $this->em->flush();
            }

            $info = 'Ajout du dossier volumineux '
                . $dossierVolumineux->getUuidReference()
                . " à l'enveloppe n°"
                . $enveloppe->getIdEnveloppeElectro();
            $this->envolLogger->info($info);

            return true;
        } catch (ORMException $e) {
            $this->envolLogger->error('Cannot update Enveloppe entity n°'
                . $enveloppe->getIdEnveloppeElectro()
                . ' with DV reference: '
                . $dossierVolumineux->getUuidReference());
            throw $e;
        }
    }

    /**
     * @return object|null
     */
    public function getBlogFilePath(DossierVolumineux $dossierVolumineux)
    {
        if (!empty($dossierVolumineux->getBlobLogfile())) {
            $blobFichier = $this->em
                ->getRepository(BlobFichier::class)
                ->findOneBy(['id' => $dossierVolumineux->getBlobLogfile()]);
            if (!empty($blobFichier)) {
                return $blobFichier->getName();
            }
        }

        return null;
    }

    /**
     * @return bool
     */
    public function checkAccess(DossierVolumineux $dossierVolumineux)
    {
        if (false === $this->isEnable()) {
            return false;
        }

        return $this->isAuthorized($dossierVolumineux);
    }


    public function getDossierVolumineuxById($id)
    {
        $dossierVolumineux = $this->em
            ->getRepository(DossierVolumineux::class)
            ->find($id);

        if ($dossierVolumineux instanceof DossierVolumineux) {
            return $dossierVolumineux;
        } else {
            throw new \InvalidArgumentException('Dossier volumineux inconnu');
        }
    }
}
