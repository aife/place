<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use Socket\Raw\Factory;
use Xenolope\Quahog\Client;

final class AntivirusScanner
{
    public function __construct(private readonly string $clamavHost, private readonly int $clamavPort)
    {
    }

    public function scanFile(string $filePath): bool
    {
        $socket = (new Factory())->createClient("tcp://$this->clamavHost:$this->clamavPort", 5);
        $clamavClient = new Client($socket);

        return $clamavClient->scanStream(file_get_contents($filePath))->isFound();
    }
}
