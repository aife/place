<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Header;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;

class Header
{
    /**
     * @var string
     */
    private $linkRedirect = "/";

    private ?string $title = null;
    private ?string $altTitle = null;

    /**
     * Header constructor.
     * @param Environment $twig
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(private readonly Environment $twig, private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function getTemplate(string $productName): ?string
    {
        $this->title = $productName;
        $this->altTitle =  'ALT_HEADER';
        if ($productName === 'exec') {
            $this->linkRedirect = $this->parameterBag->get('URL_EXEC');
        }

        return $this->twig->render('ecrans/header.html.twig', [
            'link_redirect' => $this->linkRedirect,
            'title'  => $this->title,
            'altTitle'  => $this->altTitle
        ]);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getLinkRedirect()
    {
        return $this->linkRedirect;
    }

    public function setLinkRedirect(string $link): void
    {
        $this->linkRedirect = $link;
    }
}
