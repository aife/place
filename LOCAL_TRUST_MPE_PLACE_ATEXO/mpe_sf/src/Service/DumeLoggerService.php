<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use Symfony\Bridge\Monolog\Logger;
use App\Service\Handler\MpeRotatingFileHandler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DumeLoggerService.
 *
 * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
 */
class DumeLoggerService
{
    /**
     * AtexoDumeService constructor.
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     */
    public function __construct(private LoggerInterface $logger)
    {
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
        return $this;
    }

    public function addInfo($message)
    {
        $this->logger->info($message);
    }

    public function addError($message)
    {
        $this->logger->error($message);
    }
}
