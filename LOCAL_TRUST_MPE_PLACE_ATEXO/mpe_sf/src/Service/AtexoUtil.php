<?php

/*
 * Class AtexoUtil : classe contenant des methodes utiles
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @since ESR2016
 * @copyright Atexo 2016
 */

namespace App\Service;

use Exception;
use SimpleXMLElement;
use DomDocument;
use DateTime;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class AtexoUtil
{
    /**
     * AtexoCertificat constructor.
     *
     * @copyright Atexo 2016
     * AtexoUtil constructor.
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @since ESR2016
     */
    public function __construct(
        private ?ContainerInterface $container,
        private readonly ?SessionInterface $session,
        private readonly ?TranslatorInterface $translator,
        private readonly ?LoggerInterface $logger,
        private readonly ?RequestStack $requestStack,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Permet d'ecrire dans un fichier.
     *
     * @param $file_name
     * @param $data
     * @param string $mode
     *
     * @return int|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function write_file($file_name, $data, $mode = 'w')
    {
        $fp = fopen($file_name, $mode);
        if (!fwrite($fp, $data) && !empty($data) && !is_file($file_name)) {
            return null;
        }
        fclose($fp);

        return 1;
    }

    /**
     * Permet d'encoder en json les donnees a envoyer a l'applicatio UTAH.
     *
     * @param array $data
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function encodeToJson($data)
    {
        try {
            return json_encode($this->utf8Converter($data), JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $jsonLastError = match (json_last_error()) {
                JSON_ERROR_NONE => ' - Aucune erreur',
                JSON_ERROR_DEPTH => ' - Profondeur maximale atteinte',
                JSON_ERROR_STATE_MISMATCH => ' - Inadéquation des modes ou underflow',
                JSON_ERROR_CTRL_CHAR => ' - Erreur lors du contrôle des caractères',
                JSON_ERROR_SYNTAX => ' - Erreur de syntaxe ; JSON malformé',
                JSON_ERROR_UTF8 => ' - Caractères UTF-8 malformés, probablement une erreur d\'encodage',
                default => ' - Erreur inconnue',
            };
            $this->logger->error("json last error = $jsonLastError ".PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet d'encoder un tableau en utf8 de facon recursive.
     *
     * @param $array
     *
     * @return mixed
     */
    public function utf8Converter($array)
    {
        array_walk_recursive($array, function (&$item, $key) {
            if (!mb_detect_encoding($item, 'utf-8', true)) {
                $item = utf8_encode($item);
            }
        });

        return $array;
    }

    /**
     * Permet de remplacer les caractères spéciaux microsoft word par caractères normaux claviers respectifs.
     */
    public function replaceCharactersMsWordWithRegularCharacters($chaine)
    {
        $carac = ['«', '»', '', '', '', '"', '<', '>',
            '', '-', '', "'", '¨', '',  '', '',
            "\x80", "\x82", "\x84", "\x85", "\x8C", "\x91",
            "\x92", "\x93", "\x94", "\x96", "\x97", "\x98",
            "\x9C", '&#9679;', '&#8209;', '&#61607;', '&#61664;',
            '&#8805;',  "\u2026", "\u0085", "\u0152",
            "\u2019", "\u00E2\u0080\u0099", "\u2018",
            "\u0092", '&#x20KM;', "\u20AC", "\u0080",
            "\uFFFD",  "\u00E2\u0082\u00AC",  "\u00AB",
            "\u00BB", "\u201D", "\u201C", "\u2264", "\u0153",
            "\u009C", "\u0095", "\u25CF", '&#x25MP;',
            '&#x2019;', "\u0092", "\u2265", "\u2013",
            "\u0096", "\u2022", '¼', '½', '¾', '¡',
            '«', '»', '£', '¥', '¿', '¢', '°', '',
            '', '&#61485;', '?', '²', '§', 'œ',
            '¤', '€', '`', "\u0026", "\u0023", "&"];
        $value = ['"', '"', '...', 'OE', 'EUR', '"',
            '<', '>', 'oe', '-', "'", "'", '..',
            '-', 'Y', "'", "\xA4", "\x27", "\x22",
            '...', 'OE',   "\x27", "\x27", "\x22",
            "\x22", "\x2D", "\x2D", "\x7E", 'oe',
            "\x2D", "\x2D", ' -', ':', "\x3E \x3D",
            '...',  '...', 'OE', "'", "'", "'", "'",
            'euro(s)', 'euro(s)', 'euro(s)', 'euro(s)',
            'euro(s)', '"', '"', '"',  '"', '<=', 'oe',
            'oe', '-', '-', '-', "'",  "'", '>=', '-',
            '-', '-', '1/4', '1/2', '3/4', 'i', '<<',
            '>>', 'Livre sterling', 'Yen', '-', '-', 'o',
            "'", '-', '-', '-', '-', '-', '-', '-',
            '-', '-', '-', '-', '-',];

        return str_replace($carac, $value, $chaine);
    }

    /**
     * Permet de convertir des octets en kilo-octet
     * Formate le resultat en format francais (sous forme de "1 234,56").
     *
     * @param $octets
     *
     * @return float
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function convertAndFormatToKo($octets)
    {
        $ko = round($octets / 1024, 2);
        $formatFr = match ($this->session->get('_locale')) {
            'en' => number_format($ko),
            'fr' => number_format($ko, 2, ',', ' '),
            default => number_format($ko, 2, ',', ' '),
        };

        return $formatFr;
    }

    /**
     * retourne l'extension d'un fichier.
     *
     * @param string
     *
     * @return string
     *
     * @author    Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getExtension($fichier)
    {
        // coupe le nom du fichier
        $tableau = explode('.', (string) $fichier);
        // retourne les caractere apres le dernier .
        return $tableau[count($tableau) - 1];
    }

    /**
     * Permet d'encoder la chaine d'entrée au meme encodage que celui de la plateforme.
     *
     * @param $str: chaine d'entrée
     *
     * @return : la chaine encodée
     */
    public function toHttpEncoding($str)
    {
        $encodingPf = $this->getContainer()->getParameter('HTTP_ENCODING');
        $encodageStr = mb_detect_encoding($str);
        if ($encodingPf != $encodageStr) {
            return mb_convert_encoding($str, $encodingPf, $encodageStr);
        }

        return $str;
    }

    /**
     * @param $kiloOctets
     *
     * @return string
     */
    public function arrondirSizeFile($kiloOctets)
    {
        if ($kiloOctets < 1024) {
            return str_replace('.', ',', round($kiloOctets, 2)).' '.$this->translator->trans('TEXT_KILO_OCTET');
        } else {
            return str_replace('.', ',', round($kiloOctets / 1024, 2)).' '.$this->translator->trans('TEXT_MEGA_OCTET');
        }
    }

    /**
     * @param $octets
     *
     * @return string
     */
    public function getSizeName($octets)
    {
        $sizeName = [' octets', ' Ko', ' Mo', ' Go', ' To', ' Po'];
        $s = 0;
        $name = 0;

        while ($octets >= 1024) {
            $s = $octets;
            $octets = $octets / 1024;
            $name = $name + 1;
        }

        if ($s > 0 && $octets != intval($octets)) {
            if ($octets > 100) {
                return number_format($octets, 0) . $sizeName[$name];
            } elseif ($octets > 10) {
                return number_format($octets, 1) . $sizeName[$name];
            }

            return number_format($octets, 2) . $sizeName[$name];
        }

        return $octets . $sizeName[$name];
    }

    public function OterAccents($chaine)
    {
        $accents = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'à', 'á', 'â',
            'ã', 'ä', 'å', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'ò', 'ó',
            'ô', 'õ', 'ö', 'ø', 'È', 'É', 'Ê', 'Ë', 'è', 'é', 'ê',
            'ë', 'Ç', 'ç', 'Ì', 'Í', 'Î', 'Ï', 'ì', 'í', 'î', 'ï',
            'Ù', 'Ú', 'Û', 'Ü', 'ù', 'ú', 'û', 'ü', 'ÿ', 'Ñ', 'ñ',
            '°'];
        $carcRemplacement = ['A', 'A', 'A', 'A', 'A', 'A', 'a', 'a',
            'a', 'a', 'a', 'a', 'O', 'O', 'O', 'O', 'O', 'O', 'o',
            'o', 'o', 'o', 'o', 'o', 'E', 'E', 'E', 'E', 'e', 'e',
            'e', 'e', 'C', 'c', 'I', 'I', 'I', 'I', 'i', 'i', 'i',
            'i', 'U', 'U', 'U', 'U', 'u', 'u', 'u', 'u', 'y', 'N',
            'n', '_'];

        return str_replace($accents, $carcRemplacement, $chaine);
    }

    /**
     * Vérifie la validité d'un SIRET.
     *
     * Un numéro SIRET est composé de 14 chiffres, le neuvième est une clé de contrôle.
     *
     * @param $siret
     *
     * @return bool
     *
     * @todo Vérifier les cas particuliers tel indiqué sur Wikipédia
     *
     * @see https://fr.wikipedia.org/wiki/Système_d'identification_du_répertoire_des_entreprises
     *
     * @return bool
     */
    public function isValidSiret($siret)
    {
        return preg_match('/^[0-9]{14}$/', (string) $siret) and self::luhnVerify($siret);
    }

    /**
     * Vérifie la validité d'un SIREN.
     *
     * Un numéro SIREN est composé de 8 chiffres, le neuvième est une clé de contrôle.
     *
     * @param mixed $siren Le SIREN à vérifier
     *
     * @return bool
     *
     * @todo Vérifier les cas particuliers tel indiqué sur Wikipédia
     *
     * @see https://fr.wikipedia.org/wiki/Système_d'identification_du_répertoire_des_entreprises
     */
    public function isValidSiren($siren)
    {
        return preg_match('/^[0-9]{9}$/', (string) $siren) and self::luhnVerify($siren);
    }

    /**
     * @param $number
     * @param int $iterations
     *
     * @return bool
     */
    private function luhnVerify($number, $iterations = 1)
    {
        $result = substr($number, 0, -$iterations);

        return self::luhn($result, $iterations) == $number;
    }

    /**
     * Formule de Luhn pour la vérification de numéros par leur checksum.
     *
     * @param $number
     * @param int $iterations
     */
    private function luhn($number, $iterations = 1): array|string
    {
        while ($iterations-- >= 1) {
            $stack = 0;
            $number = str_split(strrev($number), 1);

            foreach ($number as $key => $value) {
                if (0 == $key % 2) {
                    $value = array_sum(str_split($value * 2, 1));
                }

                $stack += $value;
            }

            $stack %= 10;

            if (0 != $stack) {
                $stack -= 10;
            }

            $number = implode('', array_reverse($number)) . abs($stack);
        }

        return $number;
    }

    /**
     * retourne le chemin du service.
     *
     * @param string $service     le nom du service
     * @param array  $arrayParent tableau des parents du service
     * @param string $separateor  le séparateur
     *
     * @return string chemin du service
     */
    public function implodeArrayWithSeparator($service, $arrayParent, $separator)
    {
        if ($arrayParent) {
            foreach ($arrayParent as $oneElement) {
                $service = $oneElement['libelle'] . $separator . $service;
            }
        }

        return $service;
    }

    /**
     * @param $valeur
     */
    public function isEntierRelatif($valeur): false|int
    {
        return preg_match("/^([+\-]?)[0-9]+([\.,]?[0-9]+)?$/", (string) $valeur);
    }

    /**
     * @param $montant
     * @param int  $apresVirgule
     * @param bool $sepPoint
     *
     * @return mixed|string
     */
    public function getMontantArronditEspace($montant, $apresVirgule = 2, $sepPoint = false)
    {
        $montant = str_replace(' ', '', $montant);

        if (empty($montant)) {
            return '0,00';
        }

        if (!self::isEntierRelatif($montant)) {
            return $montant;
        }

        $montant = str_replace(',', '.', $montant);
        $montant = str_replace('.', ',', sprintf('%.' . $apresVirgule . 'f', $montant));

        [$mon, $vir] = explode(',', $montant);

        $i = 0;
        $mon = strrev($mon);
        $montant = '';

        while ($i < strlen($mon)) {
            $montant = $mon[$i] . $montant;

            if ((($i + 1) % 3) == 0) {
                $montant = ' ' . $montant;
            }

            ++$i;
        }

        if ($sepPoint) {
            return trim($montant . '.' . $vir);
        }

        return trim($montant . (0 != $apresVirgule ? ',' . $vir : ''));
    }

    /**
     * @param $montant
     * @param int  $apresVirgule
     * @param bool $sepPoint
     *
     * @return mixed|string
     */
    public function formatterMontant($montant, $apresVirgule = 2, $sepPoint = false)
    {
        if ('' != $montant && null != $montant) {
            $montant = $this->getMontantArronditEspace($montant, $apresVirgule, $sepPoint);
            $montant = str_replace(' ', '', $montant);
        }

        return $montant;
    }

    /**
     * permet de decoder les codes html d'une chaine de caractères.
     *
     * @param string $str
     *
     * @return string
     */
    public function atexoHtmlEntitiesDecode($str)
    {
        return htmlspecialchars_decode($str, ENT_NOQUOTES | ENT_QUOTES);
    }

    /**
     * permet de decoder les codes html d'une chaine de caractères.
     *
     * @param string $str
     *
     * @return string
     */
    public function supprimerCodeHtml($strNew)
    {
        $strOld = '';
        while ($strNew != $strOld) {
            $strOld = $strNew;
            $strNew = html_entity_decode(
                $strNew,
                ENT_NOQUOTES | ENT_QUOTES,
                $this->container->getParameter('HTTP_ENCODING')
            );
        }

        return $strNew;
    }

    /**
     * @param $str
     * @param int $max
     *
     * @return string
     */
    public function truncate($str, $max = 256)
    {
        return mb_strlen($str) > $max ? mb_substr($str, 0, $max - 3) . '...' : $str;
    }

    /**
     * Pvérifier si une chaine de caractères respect l'expression d'un mail.
     *
     * @param string $email le text à vérifier
     *
     * @return bool
     */
    public static function checkEmailFormat($email)
    {
        return filter_var(trim($email), FILTER_VALIDATE_EMAIL);
    }

    /** convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy
     *
     * @return date au format yyyy-mm-dd
     */
    public function frnDate2iso($_frnDate)
    {
        if (!$_frnDate) {
            return;
        }
        if (strstr($_frnDate, '/')) {
            [$d, $m, $y] = explode('/', (string) $_frnDate);

            return "$y-$m-$d";
        } else {
            return $_frnDate;
        }
    }

    /***
     * Permet de d'enlever les caracters non souhaitées dans une chaine
     * returne un array
     */
    public function removeSpecialCharacters($chaine, $carateres = false)
    {
        $arrayResult = [];
        if (!$carateres) {
            $carateres = ",;.&!?'\"()[]{}°+*/\\|:%";
        }
        if ($chaine) {
            $motCle = strtr($chaine, $carateres, '                      ');
            $arrayMotsCle = explode(' ', $motCle);
            foreach ($arrayMotsCle as $mot) {
                if ('' !== $mot) {
                    $arrayResult[] = $mot;
                }
            }
        }

        return $arrayResult;
    }

    public function getPfUrl()
    {
        $pfUrl = $this->container->getParameter('URL_LT_MPE_SF');

        if ($this->requestStack->getCurrentRequest() && $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost()) {
            $pfUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost().'/';
        }

        return $pfUrl;
    }

    public function getPfUrlDomaine($from)
    {
        if ($this->requestStack && !empty($this->requestStack->getCurrentRequest())) {
            $domaine = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
        } elseif (empty($_SERVER['HTTP_HOST'])) {
            $domaine = $this->parameterBag->get('PF_URL_REFERENCE');
        } else {
            $domaine = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
        }
        $from = rtrim($from, '/');
        $urlRef = rtrim($this->parameterBag->get('PF_URL_REFERENCE'), '/');
        $from = str_replace($urlRef, $domaine, $from);
        $from .= '/';

        return $from;
    }

    /**
     * generer xml depuis json.
     *
     * @param $json
     *
     * @return string
     */
    public function arrayToXml($array, $rootElement = null, $xml = null)
    {
        $xml = new SimpleXMLElement("<?xml version=\"1.0\" encoding='UTF-8'?>$rootElement");
        $this->convertArrayToXml($array, $xml);

        return $xml->asXML();
    }

    /**
     * valid xml via xsd.
     *
     * @param $content
     * @param $pathModelXsd
     *
     * @return bool
     */
    public function isXmlValide($content, $pathModelXsd)
    {
        $valide = false;
        try {
            $Dom = new DomDocument('1.0', 'UTF-8');
            $Dom->loadXML($content);
            $Dom->validateOnParse = true;
            libxml_use_internal_errors(true);
            if ($Dom->schemaValidate($pathModelXsd)) {
                $valide = true;
            }
        } catch (Exception) {
            $valide = false;
        }

        return $valide;
    }

    public function convertArrayToXml($template_info, &$xml_template_info)
    {
        foreach ($template_info as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_template_info->addChild("$key");
                    if (count($value) > 1 && is_array($value)) {
                        $jump = false;
                        $count = 1;
                        foreach ($value as $k => $v) {
                            if (is_array($v) && is_numeric($k)) {
                                if ($count++ > 1) {
                                    $subnode = $xml_template_info->addChild("$key");
                                }
                                $this->convertArrayToXml($v, $subnode);
                                $jump = true;
                            }
                        }
                        if ($jump) {
                            continue;
                        }
                        $this->convertArrayToXml($value, $subnode);
                    } else {
                        $this->convertArrayToXml($value, $subnode);
                    }
                } else {
                    $this->convertArrayToXml($value, $xml_template_info);
                }
            } else {
                $xml_template_info->addChild("$key", "$value");
            }
        }
    }

    /**
     * Transforme à partir d'un délimiter, un tableau simple en multidimensionnel
     * (voir les tests unitaires pour les cas pratiques.
     *
     * @param $array
     *
     * @return array
     */
    public function getArrayArbo($array, $delimiter = '##')
    {
        $return = [];

        foreach ($array as $value) {
            $elements = explode($delimiter, (string) $value);
            if (is_array($elements)) {
                $count = count($elements) - 1;
                $element = &$return[$elements[0]];
                for ($i = 1; $i < $count; ++$i) {
                    $element = &$element[$elements[$i]];
                }
                $element[] = $elements[$count];
            }
        }

        return $return;
    }

    /**
     * @param string $format
     *
     * @return string|null
     */
    public function getFormattedDate(DateTime $date = null, $format = 'd/m/Y H:i')
    {
        if (empty($date)) {
            return null;
        }
        // pour la gestion des dates "0000-00-00 00:00:00" au niveau de la BD
        if (preg_match('#^-0001#', $date->format('Y-m-d h:i:s'))) {
            return null;
        }

        return $date->format($format);
    }

    /***
     * Permet de d'enlever les caracters non souhaitées dans une chaine
     * returne un array
     */
    public function deleteSpecialCharacteresFromChaine($chaine, $carateres = false)
    {
        $arrayResult = [];
        if (!$carateres) {
            $carateres = ",;.&!?'\"()[]{}°+*/\\|:%";
        }
        if ($chaine) {
            $motCle = strtr($chaine, $carateres, '                      ');
            $arrayMotsCle = explode(' ', $motCle);
            foreach ($arrayMotsCle as $mot) {
                if ('' !== $mot) {
                    $arrayResult[] = $mot;
                }
            }
        }

        return $arrayResult;
    }

    public function addIntervalToDate(
        string $dateRef,
        int $nbrHeures = 0,
        int $nbrJours = 0,
        int $nbrMois = 0,
        int $nbrAnnees = 0,
        int $nbrMinute = 0,
        int $nbrSecondes = 0
    ): string {
        $dateArray = explode(' ', $dateRef);
        [$annee, $moi, $jour] = explode('-', $dateArray[0]);
        [$heure, $minute, $seconde] = explode(':', $dateArray[1]);

        $time = mktime(
            $heure + $nbrHeures,
            $minute + $nbrMinute,
            $seconde + $nbrSecondes,
            $moi + $nbrMois,
            $jour + $nbrJours,
            $annee + $nbrAnnees
        );

        return date('Y-m-d H:i:s', $time);
    }

    public function kebabCaseToCamelCase(string $value, bool $capitalizeFirstCharacter = false): string
    {
        $str = str_replace(' ', '', ucwords(str_replace('-', ' ', $value)));

        if (!$capitalizeFirstCharacter) {
            $str[0] = strtolower($str[0]);
        }

        return $str;
    }
}
