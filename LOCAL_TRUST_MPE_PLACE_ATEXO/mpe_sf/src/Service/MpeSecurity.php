<?php

namespace App\Service;

use App\Entity\Administrateur;
use App\Entity\Agent;
use App\Entity\ConfigurationPlateforme;
use App\Entity\Inscrit;
use App\Entity\SuiviAcces\SuiviAcces;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Security.
 */
class MpeSecurity
{
    /**
     * AtexoSecurity constructor.
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoEntreprise $atexoEntreprise,
        private readonly AtexoConfiguration $configuration,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function isUserActive(UserInterface $user): bool
    {
        /** @var ConfigurationPlateforme $configurationPlateforme */
        $configurationPlateforme = $this->em
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();

        if ($user->getTentativesMdp() >= $this->parameterBag->get('MAX_TENTATIVES_MDP')) {
            if ($user instanceof Inscrit && $configurationPlateforme->getAuthenticateInscritByLogin()) {
                return false;
            } elseif ($user instanceof Agent && $configurationPlateforme->getAuthenticateAgentByLogin()) {
                return false;
            } elseif ($user instanceof Administrateur) {
                return false;
            }
        }

        return !(
            (method_exists($user, 'getBloque') && '1' === $user->getBloque())
            || (method_exists($user, 'getActif') && '0' === $user->getActif())
            || (method_exists($user, 'isDeleted') && $user->isDeleted())
        );
    }

    public function incrementTentativesMdp(UserInterface $user)
    {
        $value = $user->getTentativesMdp();
        if ($value < $this->parameterBag->get('MAX_TENTATIVES_MDP')) {
            $user->setTentativesMdp(++$value);
            $this->em->flush();
        }
    }

    public function resetTentativesMdp(UserInterface $user)
    {
        if ($user->getTentativesMdp() > 0) {
            $user->setTentativesMdp(0);
            $this->em->flush();
        }
    }

    public function activerSynchronisationSgmap(UserInterface $user)
    {
        if (
            $user instanceof Inscrit
            && $this->parameterBag->get('ACTIVER_SYNCHRONISATION_SGMAP_LORS_D_AUTHENTIFICATION') === '1'
            && $this->configuration->hasConfigPlateforme("getSynchronisationSgmap")
        ) {
            $entreprise = $user->getEntreprise();
            $this->atexoEntreprise->synchroEntrepriseAvecApiGouvEntreprise($entreprise->getSiren(), $entreprise);
        }
    }

    public function isTechnicalAgent(UserInterface $user): bool
    {
        return $user instanceof Agent && $user->isTechnique() ;
    }

    public function updateConnectionTime(UserInterface $user): void
    {
        if ($user instanceof Agent) {
            $user->setDateConnexion(new DateTime());
            $suiviAcces = new SuiviAcces();
            $suiviAcces->setDateAcces(date('Y-m-d H:i:s'));
            if (null !== $user->getId()) {
                $suiviAcces->setIdAgent($user->getId());
            }
            $suiviAcces->setNom($user->getNom());
            $suiviAcces->setPrenom($user->getPrenom());
            $suiviAcces->setEmail($user->getEmail());
            $suiviAcces->setOrganisme($user->getAcronymeOrganisme());
            $suiviAcces->setServiceId($user->getServiceId());
            $this->em->persist($suiviAcces);
            $this->em->flush();
        }
    }
}
