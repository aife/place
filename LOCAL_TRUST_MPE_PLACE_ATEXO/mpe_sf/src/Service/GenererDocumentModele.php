<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Service;

use App\Service\DocGen\AbstractDocGenService;
use Exception;
use League\Flysystem\FileNotFoundException;
use App\Entity\Agent;
use App\Entity\BloborganismeFile;
use App\Entity\Consultation;
use App\Entity\DocumentTemplate;
use App\Entity\DocumentTemplateSurcharge;
use App\Entity\EchangeDocumentaire\EchangeDocBlob;
use App\Entity\Entreprise;
use App\Entity\Lot;
use App\Entity\PieceGenereConsultation;
use App\Exception\BlobNotFoundException;
use App\Service\DocGen\Docgen;
use App\Service\DocGen\DocumentGenere;
use App\Service\DocGen\MergeFields;
use App\Service\DocGen\OffreNonRetenue;
use App\Service\DocGen\Template;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\FileExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GenererDocumentModele extends AbstractDocGenService
{
    private $tmpFolderMpe;
    private readonly Filesystem $filesystem;

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly AtexoFichierOrganisme $atexoBlobOrganisme,
        private readonly TranslatorInterface $translator,
        private readonly Encryption $encryption,
        private readonly MountManager $mountManager,
        private readonly MergeFields $argument,
        private readonly HttpClientInterface $client,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Docgen $docgen,
        private readonly OffreNonRetenue $onr,
        private readonly Template $template,
        private readonly DocumentGenere $documentGenere,
        private readonly LoggerInterface $logger,
        private WebServicesRedac $redac,
        private Security $security
    ) {
        $this->filesystem = new Filesystem();
        $this->tmpFolderMpe = $this->parameterBag->get('COMMON_TMP');

        parent::__construct(
            $this->parameterBag,
            $this->atexoBlobOrganisme,
            $this->em,
            $this->template,
            $this->translator,
            $this->encryption,
            $this->mountManager,
            $this->filesystem,
        );
    }

    /**
     * @param string|null $idLot
     * @param string|null $idEntreprise
     *
     * @throws ClientExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generate(
        string $documentName,
        string $idConsultation,
        string $organisme,
        string $fileCode,
        string $idLot = null,
        string $idEntreprise = null,
        string $extension = 'docx'
    ): array {
        $result = [];
        $agent = $this->security->getUser();
        $organism = $agent?->getOrganisme()?->getAcronyme();
        $service = $agent?->getService()?->getId();
        $tmpTemplateName = uniqid()  . '-' .  $fileCode . '.' . $extension;
        $fileNameRoot = $documentName;
        $documentName .= '.' . $extension;

        $result['statut'] = 500;
        $result['message'] = $this->translator->trans('FILE_NOT_GENERATE_EXIST');
        $file = $this->redac->getContent(
            'getDocumentByCode',
            $fileCode,
            $this->parameterBag->get('UID_PF_MPE'),
            $organism,
            $service
        );
        if (empty($file)) {
            return [
                'statut' => Response::HTTP_NOT_FOUND,
                'message' => $this->translator->trans('ERROR_MESSAGE_DOCUMENT_MODEL_NOT_FOUND'),
            ];
        }

        $consultation = $this->em->getRepository(Consultation::class)->findOneBy(['id' => $idConsultation]);
        $lot = null;
        if (!empty($idLot)) {
            $lot = $this->em->getRepository(Lot::class)->findOneBy([
                'lot'           => $idLot,
                'consultation'  => $idConsultation
            ]);
        }

        $entreprise = !empty($idEntreprise) ?
            $this->em->getRepository(Entreprise::class)->find($idEntreprise) : null;

        $mergeFieldsValues =  $this->onr->isDocumentNameValid($documentName) ?
            $this->onr->getValues($documentName, $consultation, $lot, $entreprise) :
            json_encode($this->argument->pieceGenereeModele($consultation, $lot, $entreprise), JSON_THROW_ON_ERROR);

        $formFields = [
            'keyValues' => new DataPart(
                $mergeFieldsValues,
                '',
                'application/json'
            ),
            'template' => DataPart::fromPath($this->writeFile($tmpTemplateName, $file, $fileCode))
        ];
        $formData = new FormDataPart($formFields);
        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/octet-stream';

        $content = $this->docgen->generate(
            $headers,
            $formData,
            '/docgen/api/v1/document-generator/generate'
        );

        $pathTmpTemplate = $this->tmpFolderMpe . '/' . $tmpTemplateName;
        $this->filesystem->dumpFile($pathTmpTemplate, $content);

        $fileName = $this->getNameFile($lot, $entreprise, $fileNameRoot, $extension);

        $tmpFile = new UploadedFile(
            $pathTmpTemplate,
            $fileName
        );

        return array_merge(
            $result,
            $this->moveAndInsertFile($tmpFile, $consultation, $documentName, $organisme, $lot, $entreprise)
        );
    }

    /***
     * @param Lot|null $lot
     * @param Entreprise|null $entreprise
     * @param string $documentName
     * @return string
     */
    public function getNameFile(?Lot $lot, ?Entreprise $entreprise, string $documentName, string $extension): string
    {
        $name = !empty($lot) ? $documentName . '_lot_' . $lot->getLot() : $documentName;
        $name = !empty($entreprise) ? $name . '_' . $entreprise->getNom() : $name;

        return sprintf('%s.%s', $this->escapeSpecialChars($name), $extension);
    }

    /**
     * @param PieceGenereConsultation[] $documentConsultations
     */
    public function getDocumentConsultations(
        array $result,
        array $documentConsultations,
        Encryption $encryption,
        MountManager $mountManager,
        string $organisme
    ): array {
        /**
         * @var PieceGenereConsultation $documentConsultations
         */
        foreach ($documentConsultations as $key => $documentConsultation) {
            $blob = $this->em->getRepository(BloborganismeFile::class)
                ->findOneBy(['id' => $documentConsultation->getBlobId()]);
            if ($blob) {
                $result['fichiers'][$key]['id'] = $encryption->cryptId($blob->getId());

                $result['fichiers'][$key]['nom'] = $blob->getName();
                $extension = $this->getFileExtension($blob->getName());
                $result['fichiers'][$key]['extension'] = $extension;
                $result['fichiers'][$key]['poids'] = $mountManager
                    ->getFileSize($blob->getId(), $organisme, $this->em);
                $result['fichiers'][$key]['created_at'] =
                    $documentConsultation->getCreatedAt()->format('d/m/Y - G:i');
                $result['fichiers'][$key]['uploaded'] = true;
                $checkEchangeDocumentaire = $this->em->getRepository(EchangeDocBlob::class)
                    ->findBy([
                        'blobOrganisme' => $blob->getId(),
                    ]);

                $result['fichiers'][$key]['checkEchangeDocumentaire'] = false;
                if (count($checkEchangeDocumentaire) > 0) {
                    $result['fichiers'][$key]['checkEchangeDocumentaire'] = true;
                }
            }
        }
        unset($result['message']);

        return $result;
    }

    /**
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getToken(
        PieceGenereConsultation $documentGenere,
        Agent $agent,
        string $organisme,
        string $urlCallback
    ): ?string {
        if (!$this->docgen->tokenIsValid($documentGenere)) {
            return $this->docgen->getToken(
                $documentGenere,
                $agent,
                $organisme,
                $this->template->getExtension(
                    $this->getFullNameDocumentGenere($documentGenere, $organisme)
                ),
                $urlCallback
            );
        }

        return $documentGenere->getToken();
    }

    /**
     * @param $fileName
     */
    public function getFileExtension($fileName): false|string
    {
        return substr(strrchr($fileName, '.'), 1);
    }

    /**
     * @param $documentName
     * @param $content
     * @param $originalName
     */
    public function writeFile($documentName, $content, $originalName): string
    {
        $pathTmpTemplate = $this->tmpFolderMpe . '/' . $documentName;
        $this->filesystem->dumpFile($pathTmpTemplate, $content);
        $tmpFile = new UploadedFile($pathTmpTemplate, $originalName);
        try {
            return $tmpFile->getPathname();
        } catch (Exception) {
        }
        return '';
    }

    public function ksortModeles(array $data): array
    {
        $newData = [];

        if (!empty($data)) {
            $keys = array_keys($data);
            usort($keys, 'strnatcmp');

            foreach ($keys as $key) {
                $newData[$key] = $data[$key];
            }
        }

        return $newData;
    }

    public function getCustomTemplatePath(string $documentName, string $acronymeOrganisme): array
    {
        $docTemplatePath = $this->parameterBag->get('URL_GENDOC_TEMPLATE');

        /** @var DocumentTemplateSurcharge $documentTemplateSurchargeOrganisme */
        $documentTemplateSurchargeOrganisme = $this->em
            ->getRepository(DocumentTemplateSurcharge::class)
            ->getOverrideTemplate($documentName, DocumentTemplateSurcharge::PRIORITY_ORGANISME, $acronymeOrganisme)
        ;

        if ($documentTemplateSurchargeOrganisme) {
            // Surcharge document template organisme
            $docTemplatePath = $this->getCustomTemplatePathOrganisme($acronymeOrganisme);
            $documentName = $documentTemplateSurchargeOrganisme->getFileName() ?? $documentName;
        } else {
            // Surcharge document template client
            /** @var DocumentTemplateSurcharge $documentTemplateSurchargeClient */
            $documentTemplateSurchargeClient = $this->em
                ->getRepository(DocumentTemplateSurcharge::class)
                ->getOverrideTemplate($documentName, DocumentTemplateSurcharge::PRIORITY_CLIENT)
            ;

            if ($documentTemplateSurchargeClient) {
                $docTemplatePath = $this->getCustomTemplatePathClient();
                $documentName = $documentTemplateSurchargeClient->getFileName() ?? $documentName;
            }
        }

        return ['path' => $docTemplatePath, 'fileName' => $documentName];
    }

    /**
     * @throws ClientExceptionInterface
     * @throws FileExistsException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws FileNotFoundException
     */
    public function getEditedDocument(PieceGenereConsultation $documentGenere, string $organisme): bool
    {
        $this->logger->info('La modification du document ' . $documentGenere->getId() .
            ' a bien été effectuée au niveau du serveur GenDoc');

        return $this->documentGenere->getEditedDocument(
            $documentGenere,
            $organisme,
            $this->getFullNameDocumentGenere(
                $documentGenere,
                $organisme
            )
        );
    }

    private function getCustomTemplatePathClient(): string
    {
        return $this->parameterBag->get('BASE_ROOT_DIR') . DocumentTemplateSurcharge::PATH_CLIENT;
    }

    private function getCustomTemplatePathOrganisme(string $acronyme): string
    {
        return $this->parameterBag->get('BASE_ROOT_DIR') . DocumentTemplateSurcharge::PATH_ORGANISME . $acronyme . '/';
    }

    public function getDocumentsAfterOverrideByTemplateSurcharge(string $organismeAcronyme): array
    {
        // get initial Templates
        $documentTemplates = $this->em->getRepository(DocumentTemplate::class)->findAllTemplate();

        // get documents to override
        $documentSurcharges = $this->em
            ->getRepository(DocumentTemplateSurcharge::class)
            ->findSurchargeTemplate($organismeAcronyme)
        ;

        $documentsToOverride = [];
        /** @var DocumentTemplateSurcharge $documentSurcharge */
        foreach ($documentSurcharges as $documentSurcharge) {
            if (
                ($documentTemplate = $documentSurcharge->getDocumentTemplate())
                && !array_key_exists($documentTemplate->getDocument(), $documentsToOverride)
            ) {
                $documentsToOverride[$documentTemplate->getDocument()] = [
                    'nom' => $documentTemplate->getNom(),
                    "nomAfficher" => $documentSurcharge->getNomAfficher(),
                    "document" => $documentTemplate->getDocument()
                ];
            }
        }

        return array_merge($documentTemplates, $documentsToOverride);
    }

    public function getFullNameDocumentGenere(PieceGenereConsultation $documentGenere, string $organisme): string
    {
        $blob = $documentGenere->getBlobId();
        if (!$blob instanceof  BloborganismeFile) {
            $this->logger->error(
                sprintf('Le fichier blob du document %s n\'existe pas.', $documentGenere->getId())
            );

            throw new BlobNotFoundException(
                sprintf('Le fichier blob du document %s n\'existe pas.', $documentGenere->getId())
            );
        }
        $extension = $blob->getExtension() ?? $this->template->getExtension($blob->getName());
        $pathDocument =
            $this->mountManager->getAbsolutePath($blob->getId(), $organisme, $this->em);

        return $pathDocument . '' . $extension;
    }
    public function convertFileToPdf(Consultation $consultation, string $organisme, array $infos): array
    {
        $result = [];
        $result['statut'] = 500;
        $result['message'] = $this->translator->trans('FILE_NOT_GENERATE_EXIST');

        $file = file_get_contents($infos['realPath']);
        $fileName =  $infos['name'];

        $formFields = [
            'configuration' => new DataPart(
                $file,
                '',
                'application/json'
            ),
            'document' => DataPart::fromPath(
                $this->template->writeFile(
                    $fileName,
                    $file,
                    $this->template->withoutExtension($fileName)
                )
            ),
        ];

        $formData = new FormDataPart($formFields);

        $headers = $formData->getPreparedHeaders()->toArray();
        $headers[] = 'Accept: application/json';

        $content = $this->docgen->generate(
            $headers,
            $formData,
            '/docgen/api/v1/document-convertor/pdf/finalize'
        );

        $pathTmpTemplate = $this->tmpFolderMpe . '/' . uniqid() . '.pdf';
        $this->filesystem->dumpFile($pathTmpTemplate, $content);

        $documentName = $this->template->withoutExtension($fileName) . '.pdf';

        $tmpFile = new UploadedFile(
            $pathTmpTemplate,
            $documentName
        );

        return array_merge($result, $this->moveAndInsertFile($tmpFile, $consultation, $documentName, $organisme));
    }

    public function escapeSpecialChars(string $fileName): string
    {
        $specialChars = ["'", '’', ' '];
        $replacement = ['', '', '_'];

        return str_replace($specialChars, $replacement, $fileName);
    }
}
