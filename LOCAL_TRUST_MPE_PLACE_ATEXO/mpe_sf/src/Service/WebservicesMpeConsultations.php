<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Attribute\ResultType;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class WebservicesMpeConsultations extends AbstractService
{
    private const URI = '/consultations';
    private const URI_AGENT = '/agents';

    public const PARAM_LENGTH = 'itemsPerPage';
    public const PARAM_LENGTH_VALUE = 300; // La liste des agents est limité à 300

    public function __construct(
        private readonly HttpClientInterface $apiClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($apiClient, $logger, $parameterBag);
    }

    private function getToken()
    {
        return $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . 'api/v2';
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchConsultations(array $query = [], bool $withToken = true)
    {
        $url = $this->getMpeWsBaseUrl() . self::URI;

        $headers = ['Content-Type' => self::CONTENT_TYPE_JSON];
        if ($withToken) {
            $headers['Authorization'] = 'Bearer ' . $this->getToken();
        }
        $options = [
            'headers' => $headers,
            'query' => $query,
        ];

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchConsultationsById(int $consultationId, $status = true)
    {
        $url = $this->getMpeWsBaseUrl() . self::URI . '/' . $consultationId;

        $token = $this->getToken();

        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
        ];

        if ($status) {
            return $this->request(Request::METHOD_GET, $url, $options)->getStatusCode();
        }

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchConsultationsForEntreprise(array $query = [])
    {
        $url = $this->getMpeWsBaseUrl() . self::URI;

        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON
            ],
            'query' => $query,
        ];

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }

    #[ResultType(ResultType::JSON_DECODE)]
    protected function searchAgents(array $query = []): mixed
    {
        $url = $this->getMpeWsBaseUrl() . self::URI_AGENT;
        $token = $this->getToken();

        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_JSON,
                'Authorization' => 'Bearer ' . $token,
            ],
            'query' => $query,
        ];

        return $this->request(Request::METHOD_GET, $url, $options)->getContent();
    }
}
