<?php

namespace App\Service\Api;

use App\Service\WebServices\WebServicesExec;
use Exception;
use stdClass;
use App\Entity\Chorus\ChorusEchange;
use App\Serializer\AgentNormalizer;
use App\Serializer\EchangeChorusNormalizer;
use App\Serializer\ConsultationNormalizer;
use App\Serializer\ContactNormalizer;
use App\Serializer\ContratTitulaireNormalizer;
use App\Serializer\EchangeDocumentaire\BlobOrganismeEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\ConsultationEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationClientNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocApplicationNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocBlobEchangeNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocConsLotContratNormalizer;
use App\Serializer\EchangeDocumentaire\EchangeDocumentaireNormalizer;
use App\Serializer\EntrepriseNormalizer;
use App\Serializer\EtablissementNormalizer;
use App\Serializer\InscritNormalizer;
use App\Serializer\OrganismeNormalizer;
use App\Serializer\ServiceNormalizer;
use App\Serializer\TypeContratNormalizer;
use App\Service\ContratService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer as sfSerializer;

class Serializer
{
    public function __construct(
        private ContainerInterface $container,
        private EntityManagerInterface $em,
        private LoggerInterface $logger,
        private ContratService $contratService,
        private readonly ParameterBagInterface $parameterBag,
        private readonly WebServicesExec $webServicesExec,
    ) {
    }

    protected function getAtexoSerializer()
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [
            new DateTimeNormalizer(),
            new OrganismeNormalizer(new ObjectNormalizer($classMetadataFactory), $this->container, $this->em),
            new ContactNormalizer(new ObjectNormalizer($classMetadataFactory)),
            new ServiceNormalizer(new ObjectNormalizer($classMetadataFactory)),
            new ContratTitulaireNormalizer(
                new ObjectNormalizer($classMetadataFactory),
                new ConsultationNormalizer(new ObjectNormalizer($classMetadataFactory), $this->em),
                new TypeContratNormalizer(new ObjectNormalizer($classMetadataFactory)),
                $this->container,
                $this->em
            ),
            new EtablissementNormalizer(new ObjectNormalizer($classMetadataFactory)),
            new AgentNormalizer(
                new ObjectNormalizer($classMetadataFactory),
                new ServiceNormalizer(new ObjectNormalizer($classMetadataFactory))
            ),
            new EntrepriseNormalizer(
                new ObjectNormalizer($classMetadataFactory),
                new EtablissementNormalizer(new ObjectNormalizer($classMetadataFactory))
            ),
            new EchangeChorusNormalizer(new ObjectNormalizer($classMetadataFactory), $this->em, $this->logger),
            new InscritNormalizer(new ObjectNormalizer($classMetadataFactory)),
            new EchangeDocumentaireNormalizer(
                new ObjectNormalizer($classMetadataFactory),
                new ConsultationEchangeNormalizer(new ObjectNormalizer($classMetadataFactory), $this->em),
                new EchangeDocBlobEchangeNormalizer(
                    new ObjectNormalizer($classMetadataFactory),
                    new BlobOrganismeEchangeNormalizer(
                        new ObjectNormalizer($classMetadataFactory)
                    ),
                    $this->em
                ),
                new EchangeDocApplicationClientNormalizer(
                    new ObjectNormalizer($classMetadataFactory),
                    new EchangeDocApplicationNormalizer(new ObjectNormalizer($classMetadataFactory))
                ),
                new EchangeDocConsLotContratNormalizer(
                    $this->em,
                    $this->contratService,
                    $this->parameterBag,
                    $this->logger,
                    $this->webServicesExec,
                )
            ),
            new ObjectNormalizer($classMetadataFactory),
        ];

        return new sfSerializer($normalizers, $encoders);
    }

    public function constructResponseWS(
        $mode,
        $numeroPage,
        $nombreElementsParPage,
        $dateModification,
        $nodeClass,
        $module,
        $is_array = false,
        $orgServicesAllowed = [],
        $tabParams = [],
        $idEntityToSearch = null
    ) {
        $serializer = $this->getAtexoSerializer();
        $this->initHeadContentResponse($mode, $nodeClass, true, $is_array);
        $this->constructContentResponse(
            $mode,
            $serializer,
            $numeroPage,
            $nombreElementsParPage,
            $dateModification,
            $nodeClass,
            $module,
            $is_array,
            $orgServicesAllowed,
            $tabParams,
            $idEntityToSearch
        );
        $this->initFooterContentResponse($mode, $nodeClass, true, $is_array);
    }

    /**
     * @param $mode
     * @param $node
     * @param bool $streamMode
     * @param bool $is_array
     *
     * @return string
     */
    public function initHeadContentResponse($mode, $node, $streamMode = true, $is_array = false)
    {
        if ('xml' == $mode) {
            $content = '<?xml version="1.0"?>'.PHP_EOL;
            $content .= '<mpe xmlns="http://www.atexo.com/epm/xml" ';
            $content .= 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
            $urlSchema = 'http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd';
            $content .= 'xsi:schemaLocation="'.$urlSchema.'">'
                .PHP_EOL
            ;
            $content .= '<reponse statutReponse="OK">';
            if (!$is_array) {
                $content .= '<'.$node->headWs.'>'.PHP_EOL;
            } else {
                foreach ($node as $key => $value) {
                    if (!is_array($node[$key])) {
                        $content .= '<'.$key.'>'.$value.'</'.$key.'>'.PHP_EOL;
                    } else {
                        $content .= '<'.$key.'>'.PHP_EOL;
                    }
                }
            }
        } else {
            $content = '{"mpe":{"reponse":{"statutReponse":"OK",';
            if (!$is_array) {
                if (is_string($node)) {
                    $content .= '"'.$node.'":[';
                } else {
                    $content .= '"'.$node->headWs.'":[';
                }
            } else {
                $content .= '"data":{';
                $numItems = is_countable($node) ? count($node) : 0;
                $i = 0;
                foreach ($node as $key => $value) {
                    if (!is_array($node[$key])) {
                        $content .= '"'.$key.'":"'.$value.'"';
                        if (++$i !== $numItems) {
                            $content .= ',';
                        }
                    } else {
                        $content .= '"'.$key.'":';
                    }
                }
            }
        }

        if ($streamMode) {
            echo $content;
        }

        return $content;
    }

    /**
     * @param $mode
     * @param $serializer
     * @param $NumeroPage
     * @param $nombreElementsParPage
     * @param $dateModification
     */
    public function constructContentResponse(
        $mode,
        $serializer,
        $NumeroPage,
        $nombreElementsParPage,
        $dateModification,
        $nodeClass,
        $module,
        $is_array,
        $orgServicesAllowed = [],
        $tabParams = [],
        $idEntityToSearch = null
    ) {
        if (!$is_array) {
            $count = 1;

            $arrayWs = ($idEntityToSearch)
                ? $nodeClass->findByIdAndParamWs($idEntityToSearch, $module, $orgServicesAllowed, $tabParams)
                : $nodeClass->findAllByParamWs($nombreElementsParPage, $NumeroPage, $dateModification, $module, $orgServicesAllowed, $tabParams)
            ;

            foreach ($arrayWs as $node) {
                if (!empty($content = $this->getNode($node, $serializer, $mode, $nodeClass))) {
                    if (1 != $count && 'json' == $mode) {
                        echo ',';
                    }
                    echo $content.PHP_EOL;

                    if (0 === $count % 5 && 1 !== $count) {
                        ob_flush();
                        flush();
                    }
                    ++$count;
                }
            }
        } else {
            $count = 1;
            foreach ($nodeClass as $key => $value) {
                if (is_array($nodeClass[$key])) {
                    if (1 != $count && 'json' == $mode) {
                        echo ',';
                    }

                    echo $this->getArray($value, $serializer, $mode).PHP_EOL;

                    if (0 === $count % 5 && 1 !== $count) {
                        ob_flush();
                        flush();
                    }
                    ++$count;
                }
            }
        }
    }

    /**
     * @param $mode
     * @param $node
     * @param bool $streamMode
     * @param bool $is_array
     *
     * @return string
     */
    public function initFooterContentResponse($mode, $node, $streamMode = true, $is_array = false)
    {
        $content = '';
        if (!$is_array) {
            if ('xml' == $mode) {
                $content = '</'.$node->headWs.'>'.PHP_EOL;
                $content .= '</reponse>'.PHP_EOL;
                $content .= '</mpe>'.PHP_EOL;
            } else {
                $content = ']}}}';
            }
        } else {
            if ('xml' == $mode) {
                $content = '</reponse>'.PHP_EOL;
                $content .= '</mpe>'.PHP_EOL;
            } else {
                $content = '}}}}';
            }
            foreach ($node as $key => $value) {
                if (is_array($node[$key])) {
                    if ('xml' == $mode) {
                        $content = '</'.$key.'>'.PHP_EOL;
                        $content .= '</reponse>'.PHP_EOL;
                        $content .= '</mpe>'.PHP_EOL;
                    } else {
                        $content = '}}}}';
                    }
                }
            }
        }

        if ($streamMode) {
            echo $content;
        }

        return $content;
    }

    /**
     * @param $data
     * @param $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getNode($data, sfSerializer $serializer, $mode, $nodeClass)
    {
        try {
            $content = $serializer->serialize($data, $mode, ['groups' => ['webservice']]);
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace(PHP_EOL, '', $content);
            $content = str_replace('response>', $this->getNameNoeud($nodeClass->headWs).'>', $content);
            $content = html_entity_decode($content, ENT_QUOTES, 'UTF-8');

            return $content;
        } catch (Exception $e) {
            $complementInfo = null;
            if (is_callable([$data, 'getId'])) {
                $complementInfo = " avec l'id ".$data->getId();
            }

            $this->logger->error(
                "Problème lors de la serialization de l'objet ".$nodeClass->headWs.$complementInfo
                . ' ' . $e->getMessage() . ' ' . $e->getTraceAsString()
            );

            return '';
        }
    }

    /**
     * @param string $name
     * @return string
     */
    private function getNameNoeud(string $name): string
    {
        if(str_contains($name, '-') && is_array($exploded = explode('-', $name))){
            return substr($exploded[0], 0, -1).'-' .$exploded[1];
        }
        return substr($name, 0, -1);
    }

    /**
     * @param $data
     * @param $mode
     *
     * @return bool|float|int|mixed|string
     */
    public function getArray($data, sfSerializer $serializer, $mode)
    {
        try {
            $content = $serializer->serialize($data, $mode);
            $content = str_replace('<?xml version="1.0"?>', '', $content);
            $content = str_replace(PHP_EOL, '', $content);
            $content = str_replace('<response>', '', $content);
            $content = str_replace('</response>', '', $content);
            $content = html_entity_decode($content, ENT_QUOTES, 'UTF-8');

            return $content;
        } catch (Exception) {
            $this->logger->error('Problème lors de la serialization !');

            return '';
        }
    }

    public function simpleSerialize($object, $rootNode, $context, $mode = 'xml')
    {
        if (is_string($rootNode)) {
            $tmp = $rootNode;
            $rootNode = new stdClass();
            $rootNode->headWs = $tmp;
        }
        $serializer = $this->getAtexoSerializer();
        $content = $this->initHeadContentResponse($mode, $rootNode, false, false);
        $content .= $this->formatContent(
            $serializer->serialize(
                $object,
                $mode,
                [
                    'groups' => [$context],
                    'rootNode' => $rootNode,
                ]
            )
        );
        $content .= $this->initFooterContentResponse($mode, $rootNode, false, false);

        return $content;
    }

    private function formatContent($content)
    {
        $content = str_replace('<?xml version="1.0"?>', '', $content);
        $content = str_replace("\n", '', $content);
        $content = str_replace('<response>', '', $content);
        $content = str_replace('</response>', '', $content);

        return $content;
    }
}
