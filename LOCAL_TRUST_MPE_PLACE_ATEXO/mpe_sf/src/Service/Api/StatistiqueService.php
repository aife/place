<?php

namespace App\Service\Api;

use DateTime;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\DBAL\Exception as DBALException;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\Exception\ORMException;
use Doctrine\ORM\OptimisticLockException;
use DOMDocument;
use Exception;
use LibXMLError;
use App\Entity\Api\SupervisionInterface;
use App\Entity\Tiers;
use App\Exception\ApiProblemXsdException;
use App\Repository\Api\SupervisionInterfaceRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;

class StatistiqueService
{
    final const SUCCESS = 'OK';
    final const ECHEC = 'KO';
    final const INIT = 'INIT';
    final const CHORUS = 'Chorus';
    final const ATLAS = 'Atlas';
    final const ARCADE = 'Arcade';
    final const IXARM = 'IXARM';
    final const BOAMP = 'BOAMP';
    final const SGMAP = 'API_GOUV_ENTREPRISE';

    protected $xsd = null;

    public function __construct(protected EntityManagerInterface $em, protected ParameterBagInterface $parameterBag)
    {
    }

    public function get(Request $request)
    {
        $mode = $request->get('format');
        if ('json' != $mode) {
            $mode = 'xml';
        }

        $result = [];
        /** @var SupervisionInterfaceRepository $supervisionInterfaceRepository */
        $supervisionInterfaceRepository = $this->em->getRepository(SupervisionInterface::class);

        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::CHORUS
            );

        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }

        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::ATLAS
            );

        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }

        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::ARCADE
            );

        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }

        $tiers = $this->em->getRepository(Tiers::class)->findAll();

        /** @var Tiers $tier */
        foreach ($tiers as $tier) {
            $login = $tier->getLogin();

            $res = $supervisionInterfaceRepository
                ->getSupervisionInterfaceByDateByNomInterface(
                    new DateTime($request->query->get('createdDate') ?? 'now'),
                    $login
                );
            if (!empty($res)) {
                foreach ($res as $stat) {
                    $result[] = $stat;
                }
            }
        }

        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::IXARM
            );
        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }
        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::BOAMP
            );
        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }
        $res = $supervisionInterfaceRepository
            ->getSupervisionInterfaceByDateByService(
                new DateTime($request->query->get('createdDate') ?? 'now'),
                self::SGMAP
            );
        if (!empty($res)) {
            foreach ($res as $stat) {
                $result[] = $stat;
            }
        }

        return self::serialize($result, $mode);
    }

    /**
     * @param array $params
     *
     *
     * @throws AnnotationException
     * @throws DBALException
     * @throws NonUniqueResultException
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function create(Request $request, $params = []): bool|float|int|string
    {
        $mode = $request->get('format');
        if ('json' != $mode) {
            $mode = 'xml';
        }

        $success = self::SUCCESS == $this->returnValue($request, 'statut', $params);
        $init = self::INIT == $this->returnValue($request, 'statut', $params);

        /** @var SupervisionInterfaceRepository $repository */
        $repository = $this->em->getRepository(SupervisionInterface::class);

        /** @var SupervisionInterface $supervisionInterface */
        $supervisionInterface = $repository->getByDateAndInterface(
            $this->returnValue($request, 'interface', $params),
            $this->returnValue($request, 'service', $params),
            $this->returnValue($request, 'webserviceBatch', $params),
            new DateTime()
        );
        $poidsInc = 0;
        $poidsOkInc = 0;
        if (!empty($request->get('poids'))) {
            $poidsInc = (int) $request->get('poids');
        }
        if (true === $success) {
            if (!empty($request->get('poids'))) {
                $poidsOkInc = (int) $request->get('poids');
            }
        }

        if (empty($supervisionInterface)) {
            $supervisionInterface = new SupervisionInterface();
            $supervisionInterface->setCreatedDate(new DateTime());
            $supervisionInterface->setInterface($this->returnValue($request, 'interface', $params));
            $supervisionInterface->setService($this->returnValue($request, 'service', $params));
            $supervisionInterface->setWebserviceBatch($this->returnValue($request, 'webserviceBatch', $params));
            if (true === $init) {
                $supervisionInterface->setTotal(0);
            } else {
                $supervisionInterface->setTotal(1);
            }
            if (true === $success) {
                $supervisionInterface->setTotalOk(1);
            } else {
                $supervisionInterface->setTotalOk(0);
            }
            $supervisionInterface->setPoids($poidsInc);
            $supervisionInterface->setPoidsOk($poidsOkInc);
            $this->em->persist($supervisionInterface);
            $this->em->flush();
        } else {
            if (false === $init) {
                $repository->update($supervisionInterface, $poidsInc, $success);
                $supervisionInterface->setPoids($supervisionInterface->getPoids() + $poidsInc);
                $supervisionInterface->setPoidsOk($supervisionInterface->getPoidsOk() + $poidsOkInc);
                $supervisionInterface->setTotal($supervisionInterface->getTotal() + 1);
                $supervisionInterface->setTotalOk($supervisionInterface->getTotalOk() + 1);
            }
        }

        return self::serialize($supervisionInterface, $mode);
    }

    /**
     * @param array $params
     * @param $name
     *
     * @return mixed
     */
    private function returnValue(Request $request, $name, $params = [])
    {
        $value = $request->get($name);
        if (array_key_exists($name, $params)) {
            $value = $params[$name];
        }

        return $value;
    }

    /**
     * @param $datas
     * @param $mode
     *
     *
     * @throws AnnotationException
     */
    public function serialize($datas, $mode): bool|float|int|string
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoders = [new XmlEncoder([XmlEncoder::ROOT_NODE_NAME => 'mpe']), new JsonEncoder()];
        $normalizer = new PropertyNormalizer($classMetadataFactory);
        $normalizers = [new DateTimeNormalizer(), $normalizer];
        $serializer = new Serializer($normalizers, $encoders);
        $reponse = ['reponse' => ['statistiques' => ['statistique' => $datas]]];
        $data = $serializer->normalize($reponse, null, ['groups' => ['webservice']]);
        $data['@xmlns'] = 'http://www.atexo.com/epm/xml';

        return $serializer->encode($data, $mode);
    }

    /**
     * @param $content
     *
     * @throws AnnotationException
     */
    public function validate($content, Request $request)
    {
        $mode = $request->get('format');
        $valid = false;
        if ('json' === $mode) {
            $contentDecoded = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            if (isset($contentDecoded['reponse'])) {
                if (isset($contentDecoded['reponse']['statistiques'])) {
                    if (isset($contentDecoded['reponse']['statistiques']['statistique'])) {
                        $content = $this->serialize(
                            $contentDecoded['reponse']['statistiques']['statistique'],
                            'xml'
                        );
                        $valid = true;
                    }
                }
            }
        } else {
            $valid = true;
        }
        if (false === $valid) {
            throw new ApiProblemXsdException('le noeud `reponse` est manquant ! ');
        }

        libxml_use_internal_errors(true);

        $xmlValidationException = false;

        try {
            $dom = new DOMDocument();
            $dom->loadXML($content);
            @$dom->schemaValidate($this->getXsd());
        } catch (Exception $exception) {
            $xmlValidationException = $exception;
        } finally {
            $errors = \libxml_get_errors();
            if (!empty($errors)) {
                $errorsMsg = '';
                foreach ($errors as $error) {
                    /** @var LibXMLError $error */
                    if (isset($error->message)) {
                        $errorsMsg .= PHP_EOL . $error->message;
                    }
                }
                throw new ApiProblemXsdException($errorsMsg);
            }
            if ($xmlValidationException) {
                throw new ApiProblemXsdException($xmlValidationException);
            }
        }
    }

    /**
     * @return string
     */
    public function getXsd()
    {
        $res = $this->xsd;
        if (empty($this->xsd)) {
            $res = $this->parameterBag->get('kernel.project_dir') . '/public/app/xsd/mpe.xsd';
        }

        return $res;
    }

    public function setXsd($xsdPath)
    {
        if (!file_exists($xsdPath)) {
            throw new FileNotFoundException('XSD file not found : ' . $xsdPath);
        }

        $this->xsd = $xsdPath;

        return $this;
    }
}
