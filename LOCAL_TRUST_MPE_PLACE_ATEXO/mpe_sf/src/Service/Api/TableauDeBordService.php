<?php

namespace App\Service\Api;

use App\Service\AtexoConfiguration;
use Doctrine\Bundle\DoctrineBundle\Registry;
use App\Entity\Agent;
use App\Entity\ContratTitulaire;
use App\Exception\ApiProblemInvalidArgumentException;
use App\Exception\ApiProblemNotFoundException;
use App\Exception\ApiProblemUnauthorizedException;
use App\Service\AgentTechniqueTokenService;
use App\Service\CurlWrapper;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class TableauDeBordService
{
    private $url;
    private $urlContrats;
    private $urlPradoWs;

    public function __construct(
        private ContainerInterface $container,
        private EntityManagerInterface $em,
        private LoggerInterface $logger,
        private CurlWrapper $curl,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private AtexoConfiguration $atexoConfiguration,
        private ManagerRegistry $registry
    ) {
        $pfReference = $this->container->getParameter('PF_URL_AGENT') != "/" ?
            $this->container->getParameter('PF_URL_AGENT') :
            $this->container->getParameter('PF_URL_REFERENCE');

        $this->url = $pfReference.'?page=Agent.TableauDeBord&AS=0';
        $this->urlContrats = $pfReference .
                            '?page=Agent.ResultatRechercheContrat';
        $this->urlPradoWs = $this->container->getParameter('PF_URL_REFERENCE').'api.php/ws/';
    }

    public function getConsultationsEnCours(Request $request)
    {
        $consultationsEnCours = $this->callWs($request, 'consultationsencours');
        $consultationsEnCours->urlElaboration = $this->url.'&wsState=firstBisTab';
        $consultationsEnCours->urlAttenteValidation = $this->url.'&wsState=secondTab';
        $consultationsEnCours->urlConsultation = $this->url.'&wsState=thirdTab';
        $consultationsEnCours->urlOuvertureAnalyse = $this->url.'&wsState=fourthTab';
        $consultationsEnCours->urlDecision = $this->url.'&wsState=fifthTab';
        $response = $this->getEnveloppe(['consultationsEnCours' => $consultationsEnCours]);

        return $response;
    }

    private function callWs($request, $type)
    {
        $agent = $this->getAgent($request);
        $ticket = $request->get('ticket');
        $endpoint = $this->urlPradoWs.'tableaudebord?ticket='.$ticket;

        $data = [
            'login' => $agent->getLogin(),
            'password' => $agent->getPassword(),
            'type' => $type,
        ];
        $response = $this->curl->post($endpoint, $data);
        $result = json_decode($response);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new ApiProblemInvalidArgumentException('Problème lors de la récupération des informations');
        }

        return $result;
    }

    public function getQuestions(Request $request)
    {
        $questions = $this->callWs($request, 'questions');

        $questions->url = $this->url;
        $response = $this->getEnveloppe(['questions' => $questions]);

        return $response;
    }

    private function getAgent($request)
    {
        $agent = $this->em->getRepository(Agent::class)->findOneByLogin(
            $request->get('loginAgent')
        );

        if (!$agent) {
            throw new ApiProblemInvalidArgumentException('Agent inconnu');
        }
        $agentTechnique = $this->agentTechniqueTokenService->getAgentFromToken($request->get('ticket'));

        $nodeFlux = [];
        $nodeFlux['acronymeOrganisme'] =
            (!empty($agent->getOrganisme())) ? $agent->getOrganisme()->getAcronyme() : null;
        $nodeFlux['service']['id'] =
            (!empty($agent->getService()) && !empty($agent->getService()->getId())) ? $agent->getService()->getId() : 0;

        if (!$this->agentTechniqueTokenService->isGrantedFromAgentWs($agentTechnique, $nodeFlux)) {
            throw new ApiProblemUnauthorizedException();
        }

        return $agent;
    }

    public function getContrats(Request $request)
    {
        $arrayServices = [];
        $agent = $this->getAgent($request);

        if ($agent instanceof Agent) {
            $contratRepo = $this->em->getRepository(ContratTitulaire::class);
            $configOrgCentralise = $this->atexoConfiguration
                ->hasConfigPlateforme('OrganisationCentralisee');
            $idService = $agent->getServiceId();
            $organisme = $agent->getOrganisme()->getAcronyme();
            if ($configOrgCentralise) {
                $arrayServices = $this->registry->getRepository(ContratTitulaire::class)
                    ->getAllChildrenServices($idService, $organisme);
            } else {
                $arrayServices[] = $agent->getServiceId();
            }

            $statutNotifie = $this->container->getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
            $statutADeclarer = $this->container->getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR');
            $statutANotifier = $this->container->getParameter('STATUT_NOTIFICATION_CONTRAT');

            $statusContrat = [$statutNotifie, $statutADeclarer, $statutANotifier];
            $result = $contratRepo->getContratsTableauBord($organisme, $arrayServices, $statusContrat);

            $contrats = [];
            $contrats['contrats']['notifie'] = $result[$statutNotifie] ?? 0;
            $contrats['contrats']['aDeclarer'] = $result[$statutADeclarer] ?? 0;
            $contrats['contrats']['aNotifier'] = $result[$statutANotifier] ?? 0;
            $contrats['contrats']['urlNotifie'] = $this->urlContrats.'&wsState='.$statutNotifie;
            $contrats['contrats']['urlAdeclarer'] = $this->urlContrats.'&wsState='.$statutADeclarer;
            $contrats['contrats']['urlANotifier'] = $this->urlContrats.'&wsState='.$statutANotifier;

            return $this->getEnveloppe($contrats);
        } else {
            throw new ApiProblemNotFoundException();
        }
    }

    private function getEnveloppe($data)
    {
        $data['statutReponse'] = 'OK';

        return ['mpe' => ['reponse' => ['tableauBord' => $data]]];
    }
}
