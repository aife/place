<?php

namespace App\Service\Api;

use App\Entity\Organisme;
use App\Serializer\Denormalizer\OrganismeDenormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

class DeserializerService
{
    private Serializer $deserializer;

    public function __construct()
    {
        $encoders = [new JsonEncoder(), new XmlEncoder()];
        $normalizers = [new OrganismeDenormalizer()];
        $this->deserializer = new Serializer($normalizers, $encoders);

    }

    /**
     * Permet de générer un objet Organisme à partir d'un XML ou d'un JSON.
     *
     * @param $data
     * @param $format
     */
    public function deserializeOrganisme($data, $format)
    {
        return $this->deserializer->denormalize($data, Organisme::class, $format);
    }
}
