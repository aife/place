<?php

namespace App\Service;

use DateTime;
use Exception;
use App\Entity\Consultation;
use App\Entity\Inscrit;
use App\Entity\InscritOperationsTracker as InscritTracker;
use App\Entity\InscritOperationsTrackerDetails;
use App\Entity\Organisme;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Classe InscritOperationsTracker de gestion des traces des inscrits des entreprise sur la plateforme.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class InscritOperationsTracker
{
    public function __construct(private readonly ParameterBagInterface $parameterBag, private readonly EntityManagerInterface $em, private readonly RequestStack $request, private readonly Referentiel $atexoValeursReferentielles, private readonly TranslatorInterface $translator, private readonly Security $security)
    {
    }

    public function trackingOperations(
        string $detail,
        DateTime $dateDebAction,
        DateTime $dateFinAction,
        array $arrayDonnees = [],
        $flush = true
    ): void {
        $timestampDebutAction = null;
        try {
            $consultationId = null;
            $org = null;
            $idDescription = '';
            $afficher = '';
            $logCrypto = '';
            $description = '';
            $lienDownload = '';
            $dateDebClient = null;
            $idOffre = null;
            $date = new DateTime();
            $listeIps = $this->request->getCurrentRequest()->server->has('HTTP_X_FORWARDED_FOR')
                ? $this->request->getCurrentRequest()->server->get('HTTP_X_FORWARDED_FOR')
                : $this->request->getCurrentRequest()->server->get('REMOTE_ADDR');
            $ips = explode(',', $listeIps);
            $ip = trim($ips[0]);
            $action = $this->request->getCurrentRequest()->server->get('REQUEST_URI');
            $infosBrowser = $this->request->getCurrentRequest()->headers->get('User-Agent');
            if (is_array($arrayDonnees) && !empty($arrayDonnees)) {
                $consultationId = array_key_exists('ref', $arrayDonnees) ? $arrayDonnees['ref'] : null;
                $org = array_key_exists('org', $arrayDonnees) ? $arrayDonnees['org'] : null;
                $idDescription = array_key_exists('IdDescritpion', $arrayDonnees)
                    ? $arrayDonnees['IdDescritpion']
                    : null;
                $afficher = array_key_exists('afficher', $arrayDonnees) ? $arrayDonnees['afficher'] : 1;
                $logCrypto = array_key_exists('logCrypto', $arrayDonnees) ? $arrayDonnees['logCrypto'] : '';
                $description = array_key_exists('description', $arrayDonnees) ? $arrayDonnees['description'] : '';
                $lienDownload = array_key_exists('lienDownload', $arrayDonnees) ? $arrayDonnees['lienDownload'] : null;
                $dateDebClient = array_key_exists('dateDebutActionClient', $arrayDonnees)
                    ? $arrayDonnees['dateDebutActionClient']
                    : null;
                $idOffre = array_key_exists('idOffre', $arrayDonnees) ? $arrayDonnees['idOffre'] : null;
                $timestampDebutAction = array_key_exists('timestampDebutActionClient', $arrayDonnees)
                    ? $arrayDonnees['timestampDebutActionClient']
                    : null;
            }
            if ($this->security->getUser() instanceof Inscrit) {
                $inscritId = $this->security->getUser()->getId();
                $entrepriseId = $this->security->getUser()->getEntrepriseId();
                $operationInDb = '';
                $traceOprInscritObject = $this->retrieveTraceOperationsInscrit(
                    $inscritId,
                    $entrepriseId,
                    $ip,
                    $date,
                    $consultationId,
                    $org
                );
                $operations = PHP_EOL . $dateDebAction->format('Y-m-d H:i:s')
                    . ';' . $action . '; ' . $detail . ';' . $dateFinAction->format('Y-m-d H:i:s');
                if (!($traceOprInscritObject instanceof InscritTracker)) {
                    $traceOprInscritObject = new InscritTracker();
                    $traceOprInscritObject->setIdInscrit($inscritId);
                    $traceOprInscritObject->setIdEntreprise($entrepriseId);
                    $traceOprInscritObject->setAddrIp($ip);
                    $traceOprInscritObject->setDate($date);
                    $traceOprInscritObject->setAfficher($afficher);
                    if (!empty($consultationId) && !empty($org)) {
                        $traceOprInscritObject->setOrganisme($org);
                        $traceOprInscritObject->setConsultationId($consultationId);
                    }
                    $traceOprInscritObject->setOperations('@todo: Champ plus utiliser');
                }

                $detailsTrace = new InscritOperationsTrackerDetails();
                $detailsTrace->setDateDebutAction($dateDebAction);
                $detailsTrace->setNomAction($action);
                $detailsTrace->setDetails($detail);
                $detailsTrace->setdateFinAction($dateFinAction);
                $detailsTrace->setIdDescription($idDescription);
                $detailsTrace->setDescripton($description);
                $detailsTrace->setLogApplet($logCrypto);
                if (!empty($lienDownload)) {
                    $detailsTrace->setLienDownload($lienDownload);
                }
                $detailsTrace->setAfficher($afficher);
                $detailsTrace->setInfosBrowser($infosBrowser);
                if (!empty($dateDebClient)) {
                    $detailsTrace->setDateDebutActionClient($dateDebClient);
                }
                if (!empty($idOffre)) {
                    $detailsTrace->setIdOffre($idOffre);
                }
                if (!empty($timestampDebutAction)) {
                    $detailsTrace->setDebutActionMillisecond($timestampDebutAction);
                } else {
                    $detailsTrace->setDebutActionMillisecond(1000 * microtime(true));
                }
                $traceOprInscritObject->addDetailsTracesOperation($detailsTrace);
                if (true === $flush) {
                    $this->em->beginTransaction();
                }
                $this->em->persist($traceOprInscritObject);
                $this->em->persist($detailsTrace);
                if (true === $flush) {
                    $this->em->flush();
                    $this->em->commit();
                }
            }
        } catch (Exception $e) {
            if (true === $flush) {
                $this->em->rollBack();
            }
            throw $e;
        }
    }

    public function retrieveTraceOperationsInscrit(
        int $inscritId,
        int $entrepriseId,
        string $ip,
        DateTime $date,
        ?int $consultationId = null,
        ?Organisme $org = null
    ): ?InscritTracker {
        $criteres = ['idInscrit' => $inscritId, 'idEntreprise' => $entrepriseId, 'addrIp' => $ip, 'date' => $date];
        if ($consultationId) {
            $criteres['consultationId'] = $consultationId;
        }
        if ($org) {
            $criteres['organisme'] = $org;
        }
        $operationTraking = $this->em->getRepository(InscritTracker::class)->findOneBy($criteres);

        return $operationTraking ?: null;
    }

    public function getInfosValeursReferentielles(string $cleDescription): array
    {
        $idValeurReferentielle = $this->atexoValeursReferentielles->getIdValeurReferentiel(
            [
                'libelle2' => $cleDescription,
                'idReferentiel' => $this->parameterBag->get('DESCRIPTION_HISTORIQUES_INSCRITS')
            ]
        );
        $description = $this->atexoValeursReferentielles->getLibelleValeurReferentiel(
            [
                'id' => $idValeurReferentielle,
                'idReferentiel' => $this->parameterBag->get('DESCRIPTION_HISTORIQUES_INSCRITS')
            ]
        );

        return [$idValeurReferentielle, $description];
    }

    public function tracerOperationsInscrit(
        $dateDebAction,
        $dateFinAction,
        $idValeurReferentielle,
        $description,
        $consultation = null,
        $details = '',
        $logCrypto = false,
        $lienDownload = null,
        $afficher = 1,
        $dateDebutActionClient = null,
        $idOffre = null,
        $timestampDebutAction = null,
        $flush = true
    ): void {
        try {
            $refUtilisateurCons = ($consultation instanceof Consultation)
                ? $consultation->getReferenceUtilisateur()
                : null;
            $refCons = ($consultation instanceof Consultation) ? $consultation->getId() : null;
            $orgCons = ($consultation instanceof Consultation) ? $consultation->getOrganisme() : null;
            $details = ('ref utilisateur consultation : ' . $refUtilisateurCons . ((!empty($details)) ? $details : ''));
            $autresDetails = [
                'ref' => $refCons,
                'org' => $orgCons,
                'IdDescritpion' => $idValeurReferentielle,
                'afficher' => $afficher,
                'logCrypto' => (!empty($logCrypto)) ? $logCrypto : '',
                'description' => (!empty($description)) ? $description : '',
                'lienDownload' => $lienDownload,
                'dateDebutActionClient' => $dateDebutActionClient,
                'idOffre' => $idOffre,
                'timestampDebutActionClient' => $timestampDebutAction,
            ];
            $this->trackingOperations($details, $dateDebAction, $dateFinAction, $autresDetails, $flush);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de completer la description.
     */
    public function completeDescription(
        ?string &$description = '',
        ?string $fichiers = '',
        ?string $typeEnveloppe = null,
        ?string $typeFichier = null,
        ?string $fichierSignature = ''
    ): void {
        $messageTypesEnv = match ($typeEnveloppe) {
            $this->parameterBag->get('TYPE_ENV_CANDIDATURE') => $this->translator->trans('DEFINE_DE_CANDIDATURE'),
            $this->parameterBag->get('TYPE_ENV_OFFRE') => $this->translator->trans('DEFINE_D_OFFRE'),
            $this->parameterBag->get('TYPE_ENV_ANONYMAT') => $this->translator->trans('DEFINE_D_ANONYMAT'),
            $this->parameterBag->get('TYPE_ENV_OFFRE_TECHNIQUE') => $this->translator->trans('DEFINE_D_OFFRE_TECHNIQUE'),
            default => ''
        };
        $messageTypeFichier = match ($typeFichier) {
            'PRI' => $this->translator->trans('DEFINE_DE_LA_PIECE_LIBRE'),
            'ACE' => $this->translator->trans('DEFINE_DU_DC3_ACTE_ENGAGEMENT'),
            'SIG' => $this->translator->trans('DEFINE_DU_JETON_SIGNATURE'),
            default => ''
        };
        $description = str_replace('[_type_enveloppe_]', $messageTypesEnv, $description);
        $description = str_replace('[_liste_fichiers_tailles_]', $fichiers, $description);
        $description = str_replace('[_type_fichier_]', $messageTypeFichier, $description);
        if (!empty($fichierSignature)) {
            $description = str_replace('[_fichier_signature_taille_]', $fichierSignature, $description);
        }
    }

    /**
     * Permet de recuperer la cle de la description correspondant a une action.
     */
    public function getCleDescription(string $action, ?string $typeFichier): string
    {
        $cleDescription = '';
        switch ($action) {
            case 'ADD':
                if ('PRI' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION26';
                } elseif ('ACE' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION27';
                } elseif ('SIG' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION28';
                }
                break;
            case 'DELETE':
                if ('PRI' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION29';
                } elseif ('ACE' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION30';
                } elseif ('SIG' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION31';
                }
                break;
            case 'INTERRUPTION':
                $cleDescription = 'DESCRIPTION37';
                break;
            case 'UPLOAD_END':
                if ('SIG' == $typeFichier) {
                    $cleDescription = 'DESCRIPTION36';
                } else {
                    $cleDescription = 'DESCRIPTION35';
                }
                break;
            default:
        }

        return $cleDescription;
    }
}
