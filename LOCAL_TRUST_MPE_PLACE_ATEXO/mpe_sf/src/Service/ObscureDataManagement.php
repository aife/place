<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ObscureDataManagement d'obscurcissement des donnees de navigation.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class ObscureDataManagement
{
    /**
     * ObscureDataManagement constructor.
     *
     * @param $session
     */
    public function __construct(private readonly SessionInterface $session)
    {
    }

    /**
     * Permet de recuperer la valeur obscurcie de $data et la met en session.
     *
     * @param mixed  $data   donnee a obscurcir
     * @param string $prefix prefix des donnees obscurcies dans la session
     *
     * @return string donnee obscurcie
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getObscureData($data, string $prefix)
    {
        $hash = $this->obscureData($data);

        if (empty($this->getDataFromObscuredValue($hash, $prefix))) {
            $this->session->set($this->getPrefixObscureData($prefix).$hash, $data);
        }

        return $hash;
    }

    /**
     * Permet de recuperer la valeur en claire correspondant a la valeur obscurcie $obscuredValue.
     *
     * @param $obscuredValue donnees obscurcie
     * @param string $prefix prefix des donnees obscurcies dans la session
     *
     * @return mixed valeur en claire correspondant a la donnees obscurcie
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getDataFromObscuredValue($obscuredValue, $prefix)
    {
        if (!$prefix) {
            return null;
        }
        return $this->session->get($this->getPrefixObscureData($prefix).$obscuredValue) ?: null;
    }

    /**
     * Permet d'obscurcir la valeur $data.
     *
     * @param mixed $data valeur a obscurcir
     *
     * @return string valeur obscurcie
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    private function obscureData($data)
    {
        return sha1($data.uniqid($this->session->getId()));
    }

    /**
     * Retourne le prefix des donnees obscurcies dans la session.
     *
     * @param $prefix
     *
     * @return string
     *
     * @author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    private function getPrefixObscureData(string $prefix)
    {
        $consultationId = $this->session->get('refConsultation'.$prefix);

        return $this->session->get('token_contexte_authentification_'.$consultationId)."/$prefix/obscured_data_list/";
    }
}
