<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Chorus;

use App\Entity\Chorus\ChorusFicheModificative;
use App\Entity\Chorus\ChorusFicheModificativePj;
use App\Entity\ValeurReferentiel;
use App\Service\AtexoFichierOrganisme;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class FicheModificative
{
    private $parameterBag;
    private $translator;
    private $em;
    private $log;
    private $atexoFichierOrganisme;

    public function __construct(
        ParameterBagInterface $parameterBag,
        TranslatorInterface $translator,
        EntityManagerInterface $em,
        LoggerInterface $chorusLogger,
        AtexoFichierOrganisme $atexoFichierOrganisme
    ) {
        $this->parameterBag = $parameterBag;
        $this->translator = $translator;
        $this->em = $em;
        $this->log = $chorusLogger;
        $this->atexoFichierOrganisme = $atexoFichierOrganisme;
    }

    public function generatePdfFicheModificative(
        ChorusFicheModificative $ficheModificative,
        string $numEj,
        string $referenceLibre,
        string $montantContrat,
        $idContratTitulaire,
    ) {
        $htmlContent = self::generateHtmlFicheModificative(
            $ficheModificative,
            $numEj,
            $referenceLibre,
            $montantContrat
        );
        $arrayNumMarche = isset($numEj) ? explode('#', $numEj) : [];
        if (count($arrayNumMarche) > 1) {
            $lastNumeroMarcheDecision = str_replace('_', '', strstr($arrayNumMarche[count($arrayNumMarche) - 2], '_'));
        } else {
            $lastNumeroMarcheDecision = isset($numEj) ? $numEj : '';
        }
        $nomFichier = "Fiche_modificative_" . date('Ymd') .
            "_" . date('Hi') . "_" .  $lastNumeroMarcheDecision . ".pdf";

        $attempts = 0;
        do {
            try {
                $idBlobFicheModificative = self::genererPdfFromHtml(
                    $htmlContent,
                    $nomFichier,
                    $idContratTitulaire,
                    $ficheModificative->getOrganisme()
                );
                $fileActeModifOnBlob = $this->parameterBag->get('BASE_ROOT_DIR') .
                    $ficheModificative->getOrganisme() .
                    $this->parameterBag->get("FILES_DIR") .
                    $idBlobFicheModificative . "-0";
                if (
                    empty($idBlobFicheModificative) ||
                    !is_file($fileActeModifOnBlob) ||
                    file_get_contents($fileActeModifOnBlob) === false ||
                    empty(file_get_contents($fileActeModifOnBlob))
                ) {
                    $attempts++;
                    continue;
                }
            } catch (Exception $e) {
                $attempts++;
                continue;
            }
            break;
        } while ($attempts < $this->parameterBag->get('CHORUS_NUM_OF_ATTEMPTS_GEN_ACTE_MODIFICATIF'));

        if (!empty($idBlobFicheModificative)) {
            $this->log->info(
                " MAJ Fiche Modificative, ajout IdBlob 
            ( = $idBlobFicheModificative) du fichier pdf genere \n"
            );
            try {
                $ficheModificative->setIdBlobFicheModificative($idBlobFicheModificative);
            } catch (Exception $e) {
                $msgErreur = "Erreur lors du MAJ de la Fiche Modificative. Erreur \n"
                    . $e->getMessage() . "\n" . "Fiche Modificative Objet : "
                    . print_r($ficheModificative, true);
                $this->log->error($msgErreur);
            }
        } else {
            $this->log->info(
                "La fiche modificative n'a pas ete mise a jour: " .
                "car la generation a echoue. Pour plus de details voir le fichier " .
                "'BASE_ROOT_DIR/logs/interfacePlaceChorus/aaaa/mm/jj/interfacePlaceChorus.log'"
            );
        }
    }

    //@todo note: done
    public function generateHtmlFicheModificative(
        $ficheModificative,
        string $numEj,
        string $referenceLibre,
        string $montantContrat
    ) {
        try {
            $this->log->info(" debut generation du pdf de la Fiche Modificative \n");
            $this->log->info(" MAJ Fiche Modificative \n");
            $modelhtmlFicheMofificative = $this->parameterBag->get("PATH_MODELE_CHORUS_FICH_MODIFICATIF");
            $content = file_get_contents($modelhtmlFicheMofificative);
            $content = str_replace("NUMERO_CONTRAT", $referenceLibre, $content);

            $arrayNumMarche = isset($numEj) ? explode('#', $numEj) : [];
            if (count($arrayNumMarche) > 1) {
                $lastNumeroMarcheDecision = str_replace(
                    '_',
                    '',
                    strstr($arrayNumMarche[count($arrayNumMarche) - 2], '_')
                );
            } else {
                $lastNumeroMarcheDecision = isset($numEj) ? $numEj : '';
            }

            $content = str_replace("NUMERO_EJ", $lastNumeroMarcheDecision, $content);
            $content = str_replace("MONTANT_MARCHE", $montantContrat, $content);

            $idTypeModification = $ficheModificative->getTypeModification();
            $refTypeFournisseur = $this->parameterBag->get("REFERENTIEL_TYPE_MODIFICATION");
            $referentielTypeModification = $this->em->getRepository(ValeurReferentiel::class)->findOneBy(
                [
                    'id' => $idTypeModification,
                    'idReferentiel' => $refTypeFournisseur
                ]
            );
            if ($referentielTypeModification) {
                $libelleTypeModification = $referentielTypeModification->getLibelleValeurReferentielFr();
                $content = str_replace("TYPE_MODIFICATION", $libelleTypeModification, $content);
            }
            $content = str_replace(
                "DATE_PREVISIONNELLE_NOTIFICATION",
                !empty($ficheModificative->getDatePrevueNotification()) ?
                    $ficheModificative->getDatePrevueNotification()->format('Y-m-d H:i:s') : '',
                $content
            );
            $content = str_replace(
                "DATE_FIN_MARCHE",
                !empty($ficheModificative->getDateFinMarche()) ?
                    $ficheModificative->getDateFinMarche()->format('Y-m-d H:i:s') : '',
                $content
            );
            if ($ficheModificative->getDateFinMarcheModifie()) {
                $content = str_replace("BOOL_DATE_FIN_MODIFIE", $this->translator->trans('DEFINE_OUI'), $content);
                $content = str_replace(
                    "DATE_FIN_MODIFIE",
                    !empty($ficheModificative->getDateFinMarcheModifie()) ?
                        $ficheModificative->getDateFinMarcheModifie()
                            ->format('Y-m-d H:i:s') : '',
                    $content
                );
            } else {
                $content = str_replace("BOOL_DATE_FIN_MODIFIE", $this->translator->trans('TEXT_NON'), $content);
                $content = str_replace("DATE_FIN_MODIFIE", '-', $content);
            }
            $content = str_replace("MONTANT_ACTE", $ficheModificative->getMontantActe(), $content);
            if ($ficheModificative->getTauxTva()) {
                $content = str_replace("TVA_MODIFIE", $ficheModificative->getTauxTva(), $content);
            } else {
                $content = str_replace("TVA_MODIFIE", '-', $content);
            }
            $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
            $content = str_replace("NOMBRE_FOURNISSEURS", $nbrFournisseurs, $content);
            $listeFournisseurs = '';
            if ($nbrFournisseurs) {
                $listeFournisseurs = self::getCodeListeFournisseurs($ficheModificative);
            }
            $content = str_replace("CODE_LISTE_NOUVEAUX_FOURNISSEURS", $listeFournisseurs, $content);
            $visaAccfValue = $ficheModificative->getVisaAccf();
            switch ($visaAccfValue) {
                case '0':
                    $visaAccfText = $this->translator->trans('NON_RENSEIGNE');
                    break;
                case '1':
                    $visaAccfText = $this->translator->trans('DEFINE_OUI');
                    break;
                case '2':
                    $visaAccfText = $this->translator->trans('TEXT_NON');
                    break;
            }
            $content = str_replace("VISA_AACF", $visaAccfText, $content);

            $visaPrefetValue = $ficheModificative->getVisaPrefet();
            switch ($visaPrefetValue) {
                case '0':
                    $visaPrefetText = $this->translator->trans('NON_RENSEIGNE');
                    break;
                case '1':
                    $visaPrefetText = $this->translator->trans('DEFINE_OUI');
                    break;
                case '2':
                    $visaPrefetText = $this->translator->trans('TEXT_NON');
                    break;
            }
            $content = str_replace("VISA_PREFET", $visaPrefetText, $content);
            $content = str_replace("VALUE_REMARQUE", $ficheModificative->getRemarque(), $content);

            $pjs = $this->em->getRepository(ChorusFicheModificativePj::class)
                ->findBy([
                    'idFicheModificative' => $ficheModificative->getIdFicheModificative(),
                ]);
            if ($pjs) {
                foreach ($pjs as $pj) {
                    $arrayFiles[] = $pj->getNomFichier();
                }

                $listeNameFile = '- ' . implode(',<br/>- ', $arrayFiles);
                $content = str_replace("LISTE_PJ", $listeNameFile, $content);
            }

            return $content;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation de l'html Fiche Modificative \n" . $e->getMessage() . "\n";
            $this->log->error($msgErreur);
        }
    }


    public function genererPdfFromHtml(
        $htmlContent,
        $nomFichier,
        string $idContratTitulaire,
        string $organisme //$contrat['organisme']
    ) {
        try {
            $this->log->info(" generation du pdf de la Fiche Modificative a partir du contenu HTML \n");
            $idContrat = $idContratTitulaire;
            $idGen = $idContrat . session_id();
            $nomFichierPdf =  $nomFichier . '.pdf';
            $tmpHtmlFile = $this->parameterBag->get('COMMON_TMP') . DIRECTORY_SEPARATOR . $idGen . '.html';
            $tmpPdfFile  = $this->parameterBag->get('COMMON_TMP') . DIRECTORY_SEPARATOR . $idGen . '.pdf';

            $fp = fopen($tmpHtmlFile, 'w');
            fwrite($fp, $htmlContent);
            fclose($fp);

            $cmd = $this->parameterBag->get('PATH_BIN_WKHTMLPDF') .
                " " . $tmpHtmlFile . " " .
                $tmpPdfFile . "  > /tmp/logPrintFile 2>&1 ";
            system($cmd);

            if (is_file($tmpPdfFile)) {
                $this->atexoFichierOrganisme->moveTmpFile($tmpPdfFile);
                $idBlobFicheModificative = $this->atexoFichierOrganisme->insertFile(
                    $nomFichierPdf,
                    $tmpPdfFile,
                    $organisme
                );
            } else {
                $idBlobFicheModificative = null;
                $contenuLogGenerationPdf = file_get_contents('/tmp/logPrintFile');
                $this->log->error(
                    "Erreur: la generation de la fiche modificative a echoue." .
                    PHP_EOL . "Details erreur : $contenuLogGenerationPdf " .
                    PHP_EOL . "Commande executee = '$cmd'"
                );
            }

            return $idBlobFicheModificative;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation du pdf 
            de la Fiche Modificative \n" . $e->getMessage() . "\n";
            $this->log->error($msgErreur);
        }
    }

    public function getCodeListeFournisseurs($ficheModificative)
    {
        try {
            $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
            $listeFournisseurs = '';
            $localiteFournisseur = explode("#", $ficheModificative->getLocalitesFournisseurs());
            $sirenFournisseur = explode("#", $ficheModificative->getSirenFournisseur());
            $siretFournisseur = explode("#", $ficheModificative->getSiretFournisseur());
            $nomFournisseur = explode("#", $ficheModificative->getNomFournisseur());
            $typeFournisseur = explode("#", $ficheModificative->getTypeFournisseur());
            $refTypeFournisseur = $this->parameterBag->get("REFERENTIEL_TYPE_FOURNISSEUR");
            for ($c = 0; $c < $nbrFournisseurs; $c++) {
                $numEse = $c + 1;
                $entrepriseFranceiase = $localiteFournisseur[$c] ?
                    $this->translator->trans("DEFINE_OUI") :
                    $this->translator->trans("DEFINE_NON");
                $referentielTypeFournisseur = $this->em->getRepository(ValeurReferentiel::class)->findOneBy(
                    [
                        'id' => $typeFournisseur[$c],
                        'idReferentiel' => $refTypeFournisseur
                    ]
                );

                if ($referentielTypeFournisseur) {
                    $libelleTypeFournisseur = $referentielTypeFournisseur->getLibelleValeurReferentielFr();
                }

                $listeFournisseurs .= '
										<tr>
										<td>
											<strong>'
                    . $this->translator->trans("TEXT_L_ENTREPRISE") . ' '
                    . $numEse . ' ' .
                    $this->translator->trans("TEXT_EST_ELLE_ETABLIE_FRANCE") .
                    '</strong>
										</td>
										  <td>' . $entrepriseFranceiase . '</td>
										</tr>
										<tr>
											<td>
                                                <strong>'
                    . $this->translator->trans("NOM_ENTREPRISE") .
                    '<br />' . $this->translator->trans("TEXT_ENTREPRISE") .
                    ' ' . $numEse .
                    '</strong>
											</td>
											<td>' . $nomFournisseur[$c] . '</td>
										</tr>';
                if ($localiteFournisseur[$c]) {
                    $listeFournisseurs .= '<tr>
											<td>
												<strong>'
                        . $this->translator->trans("TEXT_SIRET_L_ENTREPRISE") .
                        '<br />' . $this->translator->trans("TEXT_ENTREPRISE") .
                        ' ' . $numEse .
                        '</strong>
											</td>
										  	<td>' . $sirenFournisseur[$c] . ' ' . $siretFournisseur[$c] . '</td>
										</tr>';
                }
                $listeFournisseurs .= '<tr>
											<td>
												<strong>'
                    . $this->translator->trans("DEFINE_TYPE_FOURNISSEUR") .
                    '<br />' . $this->translator->trans("TEXT_ENTREPRISE") .
                    ' ' . $numEse .
                    '</strong>
											</td>
											<td>' . $libelleTypeFournisseur . '</td>
										</tr>';
            }

            return $listeFournisseurs;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation de la liste de 
            fournisseur pour l'acte modificatif  Fiche Modificative \n" .
                $e->getMessage() . "\n";
            $this->log->error($msgErreur);
        }
    }
}
