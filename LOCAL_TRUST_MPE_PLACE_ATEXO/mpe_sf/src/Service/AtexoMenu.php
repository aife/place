<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\ComptesAgentsAssocies;
use App\Entity\ConfigurationOrganisme;
use App\Entity\ConfigurationPlateforme;
use App\Service\Agent\Habilitation;
use Application\Service\Atexo\Atexo_Util;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class AtexoMenu.
 */
class AtexoMenu
{
    /**
     * AtexoAlertes constructor.
     *
     * @param $container
     */
    public function __construct(
        private ContainerInterface $container,
        private readonly EntityManagerInterface $doctrine,
        private readonly SessionInterface $session,
        private readonly CurrentUser $currentUser,
        private readonly RequestStack $request,
        private readonly TranslatorInterface $translator,
        private readonly Habilitation $habilitation
    ) {
    }

    /**
     * @return mixed
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * @return boolean
     */
    public function isPanier()
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isPanier = false;

        if (
            $configurationPlateforme->getPanierEntreprise()
            && $this->currentUser->isEntreprise() && $this->currentUser->isConnected()
        ) {
            $isPanier = true;
        }

        return $isPanier;
    }

    /**
     * @return boolean
     */
    public function isBourseCoTraitance()
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isBourseCoTraitance = false;

        if ($configurationPlateforme->getBourseALaSousTraitance() && $this->currentUser->isEntreprise()) {
            $isBourseCoTraitance = true;
        }

        return $isBourseCoTraitance;
    }

    /**
     * @return boolean
     */
    public function isEntiteAchat()
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isEntiteAchat = false;

        if (
            $configurationPlateforme->getAnnuaireEntitesAchatVisibleParEntreprise()
            && !$configurationPlateforme->getAnnoncesMarches()
        ) {
            $isEntiteAchat = true;
        }

        return $isEntiteAchat;
    }

    /**
     * @return boolean
     */
    public function isDocumentsReference()
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isDocumentsReference = false;

        if ($configurationPlateforme->getDocumentsReference()) {
            $isDocumentsReference = true;
        }

        return $isDocumentsReference;
    }

    /**
     * @return boolean
     */
    public function isAnnoncesMarches()
    {
        /** @var ConfigurationPlateforme $configurationPlateforme */
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isAnnoncesMarches = false;

        if ($configurationPlateforme->getAnnoncesMarches()) {
            $isAnnoncesMarches = true;
        }

        return $isAnnoncesMarches;
    }

    /**
     * @return boolean
     */
    public function isLogo()
    {
        return false; //demande de Porduit de desactiver le logo orga https://atexo.atlassian.net/browse/MPE-15846
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isLogo = false;

        if (
            $configurationPlateforme->getAfficherImageOrganisme()
            && ($_COOKIE['selectedorg'] || $this->request->getCurrentRequest()->get('orgAcronyme'))
        ) {
            $isLogo = true;
        }

        return $isLogo;
    }

    public function isConnect()
    {
        $isConnect = false;

        if ($this->currentUser->isEntreprise() && $this->currentUser->isConnected()) {
            $isConnect = true;
        }

        return $isConnect;
    }

    public function isAutoFormation()
    {
        $configurationPlateforme = $this->getConfigurationPlateforme();
        $isAutoFormation = false;
        if (
            !$configurationPlateforme->getAnnoncesMarches() &&
            'TEXT_AUTOFORMATION' != $this->translator->trans('TEXT_AUTOFORMATION')
        ) {
            $isAutoFormation = true;
        }

        return $isAutoFormation;
    }

    public function isRechercheAvancee()
    {
        $isRechercheAvancee = false;
        $configurationPlateforme = $this->getConfigurationPlateforme();

        if (
            $configurationPlateforme->getArticle133GenerationPf() &&
            !$configurationPlateforme->getAnnoncesMarches() &&
            0 == $this->container->getParameter('RECHERCHE_DONNEES_ESSENTIELLES_EXTERNE')
        ) {
            $isRechercheAvancee = true;
        }

        return $isRechercheAvancee;
    }

    public function isRechercheAvanceeExterne()
    {
        $isRechercheAvanceeExterne = false;
        $configurationPlateforme = $this->getConfigurationPlateforme();

        if (
            $configurationPlateforme->getArticle133GenerationPf() &&
            !$configurationPlateforme->getAnnoncesMarches() &&
            1 == $this->container->getParameter('RECHERCHE_DONNEES_ESSENTIELLES_EXTERNE')
        ) {
            $isRechercheAvanceeExterne = true;
        }

        return $isRechercheAvanceeExterne;
    }

    public function isUpload()
    {
        $isUpload = false;
        $configurationPlateforme = $this->getConfigurationPlateforme();

        if (
            $configurationPlateforme->getArticle133UploadFichier() &&
            !$configurationPlateforme->getAnnoncesMarches()
        ) {
            $isUpload = true;
        }

        return $isUpload;
    }

    /**
     * @return mixed
     */
    private function getConfigurationPlateforme()
    {
        return $this->doctrine
            ->getRepository(ConfigurationPlateforme::class)
            ->getConfigurationPlateforme();
    }

    private function getConfigurationOrganisme(): ?ConfigurationOrganisme
    {
        return $this->doctrine
            ->getRepository(ConfigurationOrganisme::class)
            ->getByAcronyme($this->currentUser->getAcronymeOrga());
    }

    public function isConnectionVisible()
    {
        $isConnectionVisible = false;
        $configurationPlateforme = $this->getConfigurationPlateforme();
        if (
            !$configurationPlateforme->getSocleExterneEntreprise() &&
            !$configurationPlateforme->getSocleExternePpp()
        ) {
            $isConnectionVisible = true;
        }

        return $isConnectionVisible;
    }

    public function isDossierVolumineux()
    {
        $isDossierVolumineux = false;

        $configurationOrganisme = $this->doctrine->getRepository(ConfigurationOrganisme::class)
            ->findBy(['moduleEnvol' => true]);

        if (count($configurationOrganisme) > 0 && $this->currentUser->isEntreprise()) {
            $isDossierVolumineux = true;
        }

        return $isDossierVolumineux;
    }

    /**
     * @return bool
     */
    public function isAgentConnect()
    {
        $isConnect = false;

        if ($this->currentUser->isAgent() && $this->currentUser->isConnected()) {
            $isConnect = true;
        }

        return $isConnect;
    }

    /**
     * @return bool
     */
    public function getPartagerConsultation()
    {
        $res = false;
        $partagerConsultation = $this->getConfigurationPlateforme();
        if ('0' != $partagerConsultation->getPartagerConsultation()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function isPanelLogo()
    {
        $res = ['logo' => 'true'];
        $panelLogo = $this->container->getParameter('URL_LOGO');
        if ('URL_LOGO' != $panelLogo) {
            $res['logo'] = $panelLogo;
            $res['alt'] = $this->container->getParameter('ALT_URL_LOGO');
            $res['title'] = $this->container->getParameter('TITLE_URL_LOGO');
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function isPanelLogoBoamp()
    {
        $res = false;
        $logo = $this->container->getParameter('AFFICHER_BANDEAU_LOGO_BOAMP_AGENT');
        if ('0' != $logo) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function getAfficherImageOrganisme()
    {
        $res = false;
        $organisme = $this->getConfigurationPlateforme();
        if ('0' != $organisme->getAfficherImageOrganisme()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return string
     */
    public function getAcroOrganisme()
    {
        return $this->currentUser->getAcronymeOrga();
    }

    /**
     * @return string
     */
    public function getLang()
    {
        $extension = '.js';
        $lang = $this->request->getCurrentRequest()->getLocale();
        if (null != $lang) {
            $extension = '.' . $lang . '.js';
        }

        return $extension;
    }

    /**
     * @return bool
     */
    public function getSocleExternePpp()
    {
        $res = false;
        $socle = $this->getConfigurationPlateforme();
        if ('0' != $socle->getSocleExternePpp()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function getSocleInterne()
    {
        $res = false;
        $socle = $this->getConfigurationPlateforme();
        if ('0' != $socle->getSocleInterne()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function getMultiLinguismeAgent()
    {
        $res = false;
        $socle = $this->getConfigurationPlateforme();
        if ('0' != $socle->getMultiLinguismeAgent()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function isPortailVisible()
    {
        $url = 'false';
        if ($this->getSocleInterne() && $this->container->getParameter('URL_ACCUEIL_SPIP')) {
            $url = $this->container->getParameter('URL_ACCUEIL_SPIP_AGENT');
        } elseif ($this->getSocleExternePpp()) {
            $url = $this->container->getParameter('URL_SOCLE_EXTERNE');
        }

        return $url;
    }

    /**
     * @return string
     */
    public function getLinkAccueilAgent()
    {
        if (!$this->isAgentConnect()) {
            $link = $this->container->getParameter('PF_URL') . 'index.php?page=Agent.AgentHome';
        } else {
            if ($this->getSocleInterne()) {
                $link = $this->container->getParameter('PF_URL_AGENT') .
                    'index.php?page=Agent.AccueilAgentAuthentifieSocleinterne';
            } else {
                $link = $this->container->getParameter('PF_URL_AGENT') .
                    'agent';
            }
        }

        return $link;
    }

    /**
     * @return bool
     */
    public function isNotificationsAgent()
    {
        $res = false;
        if ($this->isAgentConnect() && $this->getConfigurationPlateforme()->getNotificationsAgent()) {
            $res = true;
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function afficherFooterAgent()
    {
        $res = 'false';
        if (0 != $this->container->getParameter('AFFICHAGE_FOOTER_MPE_VERSION_AGENT')) {
            $res = substr(
                $this->container->getParameter('MPE_VERSION'),
                0,
                $this->container->getParameter('NOMBRE_CARACTERE_NUMERO_COURT_VERSION')
            );
        }

        return $res;
    }

    /**
     * @return string
     */
    public function selectFlag($langue)
    {
        $flag = 'on';
        if ('atx' === $this->session->get('lang') || $this->session->get('lang') != $langue) {
            $flag = 'off';
        }

        return $flag;
    }

    /**
     * Permet de récupérer le logo organisme brut (en binaire)
     */
    public function getUrlLogoOrganisme(string $orga, bool $grand = false)
    {
        $filePath = $this->container->getParameter('BASE_ROOT_DIR')
            . '/' . $orga;
        $filePath .= $this->container->getParameter('PATH_ORGANISME_IMAGE')
            . '/logo-organisme-';
        $filePath .= (($grand) ? 'grand' : 'petit') . '.jpg';
        $url = $this->container->getParameter('PF_URL')
            . "index.php?page=" . Atexo_Util::getTypeUserCalledForPradoPages() . ".LogoOrganisme&org="
           ;
        $url .= $orga . (($grand) ? '&grand' : '&petit') . '=true';

        if (is_file($filePath)) {
            return $url;
        }

        return false;
    }

    public function hasStatisticsMenu(): bool
    {
        return
            $this->isAgentConnect()
            && $this->getConfigurationOrganisme()->isModuleBI()
            && $this->currentUser->checkHabilitation('gererStatistiquesMetier')
            ;
    }

    public function isAuthentificationAgentMultiOrganismes(): bool
    {
        return $this->getConfigurationPlateforme()->getAuthentificationAgentMultiOrganismes();
    }

    public function getComptesAgentsAssocies(): array
    {
        if (($agent = $this->currentUser->getCurrentUser()) instanceof Agent) {
            return $this->doctrine
                ->getRepository(ComptesAgentsAssocies::class)
                ->findComptesAssocies($agent);
        } else {
            return [];
        }
    }

    public function getCompteAgentAssocieIsPrimaryAndActive(): bool
    {
        if (($agent = $this->currentUser->getCurrentUser()) instanceof Agent) {
            $accounts =  $this->doctrine
                ->getRepository(ComptesAgentsAssocies::class)
                ->getAllActiveComptesAssocies($agent);

            if ((is_countable($accounts) ? count($accounts) : 0) > 0) {
                return true;
            }
        }

        return false;
    }

    public function isAccessSpaserGranted(): bool
    {
        return $this->getConfigurationOrganisme()->isAccesModuleSpaser();
    }
}
