<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\InvitePermanentTransverse;
use App\Entity\Service;
use Doctrine\ORM\EntityManagerInterface;

class ServiceService
{
    public function __construct(
        private readonly EntityManagerInterface $em
    ) {
    }

    public function getServiceArborescence(Agent $agent): array
    {
        $services = $this->em->getRepository(Service::class)->getServiceArborescence($agent);
        $organismeRootInviteTransverse = $this->em->getRepository(InvitePermanentTransverse::class)
            ->findOneBy(['acronyme' => $agent->getOrganisme(), 'agent' => $agent, 'service' => null]);
        $preparedServices = [];
        $organismeRoot = [
            'id' => 0,
            'libelle' => $agent->getOrganisme()->getDenominationOrg(),
            'inviteTransverseId' => $organismeRootInviteTransverse
                ? $organismeRootInviteTransverse->getId() ?? 0 : null,
            'parent_id' => null,
            'children' => []
        ];

        foreach ($services as $service) {
            $id = $service['id'];
            $libelle = $service['sigle'] . ' - ' . $service['libelle'];
            $parentId = $service['parent_id'];
            $inviteTransverseId = $service['invite_permanent_transverse_id'];
            $preparedServices[] = [
                'id' => $id,
                'libelle' => $libelle,
                'inviteTransverseId' => $inviteTransverseId,
                'parent_id' => $parentId,
                'children' => []
            ];
        }

        $serviceTree = $this->buildServiceTree($preparedServices);
        $organismeRoot['children'] = $serviceTree;

        return [$organismeRoot];
    }

    private function buildServiceTree(array &$services, $parentId = null)
    {
        $tree = [];

        foreach ($services as &$service) {
            if ($service['parent_id'] == $parentId) {
                $children = $this->buildServiceTree($services, $service['id']);
                if ($children) {
                    $service['children'] = $children;
                }
                $tree[] = $service;
                unset($service);
            }
        }

        return $tree;
    }
}
