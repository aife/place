<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DataTransformer;

use DateTime;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SearchCriteriaTransformer
{
    private const DATE_FORMAT = 'd/m/Y';
    public const SPECIAL_WORDS_FILTER = " and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf 
                                ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ";
    public const SPECIAL_CHARS_FILTER = ['«', '»', ',', ';', '.', '&', '!', '?', "'", '"', '(', ')', '[', ']', '{', '}', '°', '+', '*', '/', '\\', '|', ':', '%'];


    public function __construct(private readonly ParameterBagInterface $parameterBag)
    {
    }

    public function transform(Atexo_Consultation_CriteriaVo $criteriaVo): array
    {
        $criteria = [];
        if ($criteriaVo->getReferenceConsultation()) {
            $criteria['referenceUtilisateur'] = $criteriaVo->getReferenceConsultation();
        }

        if ($criteriaVo->getIdTypeAvis()) {
            $criteria['idTypeAvis'] = $criteriaVo->getIdTypeAvis();
        }

        if ($criteriaVo->getDateFinStart()) {
            $finStart  = DateTime::createFromFormat(self::DATE_FORMAT, $criteriaVo->getDateFinStart());
            if ($finStart instanceof \DateTimeInterface) {
                $criteria['datefin']['after'] = $finStart->format('Y-m-d');
            }
        }

        if ($criteriaVo->getDateFinEnd()) {
            $finEnd = DateTime::createFromFormat(self::DATE_FORMAT, $criteriaVo->getDateFinEnd());
            if ($finEnd instanceof \DateTimeInterface) {
                $criteria['datefin']['before'] = $finEnd->format('Y-m-d');
            }
        }

        if (trim($criteriaVo->getKeyWordAdvancedSearch())) {
            $words = explode(' ', trim($criteriaVo->getKeyWordAdvancedSearch()));
            foreach ($words as $word) {
                $word = str_replace(self::SPECIAL_CHARS_FILTER, '', $word);
                if (false === strstr(self::SPECIAL_WORDS_FILTER, $word)) {
                    $criteria['search_full'][] = $word;
                }
            }
        }

        if ($criteriaVo->getEnLigneDepuis()) {
            $enLigneDepuis = DateTime::createFromFormat(self::DATE_FORMAT, $criteriaVo->getEnLigneDepuis());
            if ($enLigneDepuis instanceof \DateTimeInterface) {
                $criteria['dateMiseEnLigneCalcule']['after'] = $enLigneDepuis->format('Y-m-d');
            }
        }

        if ($criteriaVo->getEnLigneJusquau()) {
            $enLigneJusquau = DateTime::createFromFormat(self::DATE_FORMAT, $criteriaVo->getEnLigneJusquau());
            if ($enLigneJusquau instanceof \DateTimeInterface) {
                $criteria['dateMiseEnLigneCalcule']['before'] = $enLigneJusquau->format('Y-m-d');
            }
        }

        if ($criteriaVo->getCategorieConsultation()) {
            $criteria['categorie'][] = $criteriaVo->getCategorieConsultation();
        }

        if ($criteriaVo->getIdTypeProcedure()) {
            $criteria['typeProcedure'] = $criteriaVo->getIdTypeProcedure();
        }

        if ($criteriaVo->getEtatConsultation()) {
            $criteria['statutCalcule'] = $criteriaVo->getEtatConsultation();
        }

        if (0 !== $criteriaVo->getClauseSociale()) {
            $criteria['clauseSociale'] = $criteriaVo->getClauseSociale();
        }

        if (0 !== $criteriaVo->getAtelierProtege()) {
            $criteria['clauseSocialeAteliersProteges'] = ($criteriaVo->getSiae() == $this->parameterBag
                ->get('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0');
        }

        if (0 !== $criteriaVo->getEss()) {
            $criteria['clauseSocialeEss'] = ($criteriaVo->getEss() == $this->parameterBag
                ->get('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0');
        }

        if (0 !== $criteriaVo->getSiae()) {
            $criteria['clauseSocialeSiae'] = ($criteriaVo->getSiae() == $this->parameterBag
                ->get('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0');
        }

        if (0 !== $criteriaVo->getSocialeCommerceEquitable()) {
            $criteria['socialeCommerceEquitable'] = $criteriaVo->getSocialeCommerceEquitable();
        }

        if (0 !== $criteriaVo->getSocialeInsertionActiviterEconomique()) {
            $criteria['socialeInsertionActiviterEconomique'] = $criteriaVo->getSocialeInsertionActiviterEconomique();
        }

        if (0 !== $criteriaVo->getClauseEnv()) {
            $criteria['clauseEnvironnementale'] = $criteriaVo->getClauseEnv();
        }

        if ($criteriaVo->getIdCodeCpv2()) {
            $codesCPV = array_filter(explode('#', (string) $criteriaVo->getIdCodeCpv2()));
            foreach ($codesCPV as $cpv) {
                if (10 == strlen($cpv)) {
                    $cpv = substr($cpv, 0, -2);
                }
                $criteria['codesCPV'][] = $cpv;
            }
        }

        if ($criteriaVo->getLieuxexecution()) {
            $lieux = array_filter(explode(',', (string) $criteriaVo->getLieuxexecution()));
            if (120 > count($lieux)) {
                $criteria['lieuExecution'] = $lieux;
            }
        }

        if (trim($criteriaVo->getOrgDenomination())) {
            $criteria['orgDenomination'] = trim($criteriaVo->getOrgDenomination());
        }

        if (trim($criteriaVo->getAcronymeOrganisme())) {
            $criteria['acronymeOrg'] = trim($criteriaVo->getAcronymeOrganisme());
        }

        if (trim($criteriaVo->getIdService())) {
            $criteria['service.id'] = trim($criteriaVo->getIdService());
        }

        return $criteria;
    }
}
