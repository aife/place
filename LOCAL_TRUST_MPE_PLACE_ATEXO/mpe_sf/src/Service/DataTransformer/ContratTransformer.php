<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DataTransformer;

use App\Entity\ContratTitulaire;
use App\Entity\GeolocalisationN2;
use App\Entity\InvitationConsultationTransverse;
use App\Entity\TrancheArticle133;
use App\Entity\ValeurReferentiel;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ContratTransformer
{
    public final const
        CODE_PRINCIPAL = 'codePrincipal',
        CODE_SECONDAIRE = 'codeSecondaire1',
        VALUE = 'value',
        ENTITE_ELIGIBLE = 'entiteEligible',
        ID = 'id',
        ACRONYM = 'acronyme',
        DEMONINATION = 'denomination'
    ;

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly ParameterBagInterface $parameterBag
    ) {
    }

    public function getLieuExecutions(ContratTitulaire $contratTitulaire): string
    {
        $lieuxExecutionContrat = '';

        if (!empty($contratTitulaire->getLieuExecution())) {
            $lieuExecutions = explode(',', $contratTitulaire->getLieuExecution());
            if (is_countable($lieuExecutions)) {
                $lieux = $this->entityManager
                    ->getRepository(GeolocalisationN2::class)
                    ->getDenominationLieuxExecution($lieuExecutions)
                ;
                if (is_countable($lieux)) {
                    foreach ($lieux as $lieu) {
                        $lieuxExecutionContrat .= $lieu . ', ';
                    }
                    $lieuxExecutionContrat =  substr($lieuxExecutionContrat, 0, -2);
                }
            }
        }

        return $lieuxExecutionContrat;
    }

    public function getTrancheBudgetaire(?int $idTrancheBudgetaire): ?string
    {
        if (null === $idTrancheBudgetaire) {
            return null;
        }

        /** @var TrancheArticle133 $trancheBudgetaire */
        $trancheBudgetaire = $this->entityManager
            ->getRepository(TrancheArticle133::class)
            ->find($idTrancheBudgetaire)
        ;

        return $trancheBudgetaire?->getLibelleTrancheBudgetaire();
    }

    public function getCcag(?int $ccagApplicable): ?string
    {
        if (null === $ccagApplicable) {
            return null;
        }

        $ccag = $this->entityManager
            ->getRepository(ValeurReferentiel::class)
            ->findOneBy(
                [
                    'id' => $ccagApplicable,
                    'idReferentiel' => $this->parameterBag->get('REFERENTIEL_CCAG_REFERENCE'),
                ]
            );

        return $ccag?->getLibelleValeurReferentiel();
    }

    public function getCpv(ContratTitulaire $contratTitulaire): array
    {
        $cpv = [];

        if (!empty($contratTitulaire->getCodeCpv1())) {
            $cpv[self::CODE_PRINCIPAL] = $contratTitulaire->getCodeCpv1();
            if (!empty($contratTitulaire->getCodeCpv2())) {
                $cpv[self::CODE_SECONDAIRE] = $contratTitulaire->getCodeCpv2();
            }
        }

        return $cpv;
    }

    public function getContratTransverse(ContratTitulaire $contratTitulaire): array
    {
        $contratTransverse = [];
        $entiteEligible = $this->entityManager
            ->getRepository(InvitationConsultationTransverse::class)
            ->findIfContratExists($contratTitulaire->getIdContratTitulaire())
        ;

        if (!$entiteEligible) {
            return [self::VALUE => 'false'];
        }

        $contratTransverse[self::VALUE] = 'true';

        foreach ($entiteEligible as $key => $value) {
            $contratTransverse[self::ENTITE_ELIGIBLE][$key][self::ID] = $value['id'];
            $contratTransverse[self::ENTITE_ELIGIBLE][$key][self::ACRONYM] = $value['acronyme'];
            $contratTransverse[self::ENTITE_ELIGIBLE][$key][self::DEMONINATION] = $value['denominationOrg'];
        }

        return $contratTransverse;
    }

    public function getCategorieByLabel(string $categorie): string|int
    {
        return match ($categorie) {
            'FOURNITURES' => $this->parameterBag->get('TYPE_PRESTATION_FOURNITURES'),
            'TRAVAUX' => $this->parameterBag->get('TYPE_PRESTATION_TRAVAUX'),
            'SERVICES' => $this->parameterBag->get('TYPE_PRESTATION_SERVICES'),
            default => 0,
        };
    }

    public function getStatutIdByLabel(string $categorie): string|int
    {
        return match ($categorie) {
            'STATUT_DECISION_CONTRAT' => $this->parameterBag->get('STATUT_DECISION_CONTRAT'),
            'STATUT_DONNEES_CONTRAT_A_SAISIR' => $this->parameterBag->get('STATUT_DONNEES_CONTRAT_A_SAISIR'),
            'STATUT_NUMEROTATION_AUTONOME' => $this->parameterBag->get('STATUT_NUMEROTATION_AUTONOME'),
            'STATUT_NOTIFICATION_CONTRAT' => $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT'),
            'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE' =>
            $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'),
            default => 0,
        };
    }

    public function getArrayLieuExecutionById(?string $lieuExecutionIds): array
    {
        $lieuxExecution = [];

        $departements = array_filter(explode(',', (string) $lieuExecutionIds));

        if (empty($departements)) {
            return $lieuxExecution;
        }

        $geolocalisationsN2 = $this->entityManager
            ->getRepository(GeolocalisationN2::class)
            ->findBy(['id' => $departements])
        ;

        /** @var GeolocalisationN2 $value */
        foreach ($geolocalisationsN2 as $value) {
            $lieuxExecution[] = (int)$value->getDenomination2();
        }

        return $lieuxExecution;
    }

    public function getStatutLabelById(string $statusId): string
    {
        return match ($statusId) {
            $this->parameterBag->get('STATUT_DECISION_CONTRAT') => 'STATUT_DECISION_CONTRAT',
            $this->parameterBag->get('STATUT_DONNEES_CONTRAT_A_SAISIR') => 'STATUT_DONNEES_CONTRAT_A_SAISIR',
            $this->parameterBag->get('STATUT_NUMEROTATION_AUTONOME') => 'STATUT_NUMEROTATION_AUTONOME',
            $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT') => 'STATUT_NOTIFICATION_CONTRAT',
            $this->parameterBag->get('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
            => 'STATUT_NOTIFICATION_CONTRAT_EFFECTUEE',
            default => 0,
        };
    }
}
