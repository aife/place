<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\DataTransformer;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Lot;
use Symfony\Contracts\Translation\TranslatorInterface;

class ClausesTransformer
{
    public final const
        IRI = 'iri',
        LABEL = 'label',
        SLUG = 'slug',
        VALUE = 'value',
        ACTIVE = 'active',
        TYPE = 'type',
        CLAUSES_N2 = 'clausesN2',
        CLAUSES_N3 = 'clausesN3',
        CLAUSES_N4 = 'clausesN4'
    ;

    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly IriConverterInterface $iriConverter
    ) {
    }

    public function getNestedClauses(Consultation|Lot|ContratTitulaire $object): array
    {
        $clauses = [];

        foreach ($object->getClausesN1() as $itemClauseN1) {
            $referenctielN1 = $itemClauseN1->getReferentielClauseN1();
            $nodeClauseN1 = [];
            $nodeClauseN1[self::SLUG] = $referenctielN1->getSlug();
            $nodeClauseN1[self::LABEL] = $this->translator->trans($referenctielN1->getLabel());
            $nodeClauseN1[self::IRI] = $this->iriConverter->getIriFromItem($referenctielN1);
            $nodeClauseN1[self::ACTIVE] = $referenctielN1->isActif();

            // Noeud ClauseN2
            foreach ($itemClauseN1->getClausesN2() as $itemClauseN2) {
                $referenctielN2 = $itemClauseN2->getReferentielClauseN2();
                $nodeClauseN2 = [];
                $nodeClauseN2[self::SLUG] = $referenctielN2->getSlug();
                $nodeClauseN2[self::ACTIVE] = $referenctielN2->isActif();
                $nodeClauseN2[self::LABEL] = $this->translator->trans($referenctielN2->getLabel());
                $nodeClauseN2[self::IRI] = $this->iriConverter->getIriFromItem($referenctielN2);

                // Noeud ClauseN3
                foreach ($itemClauseN2->getClausesN3() as $itemClauseN3) {
                    $referenctielN3 = $itemClauseN3->getReferentielClauseN3();
                    $nodeClauseN3 = [];
                    $nodeClauseN3[self::SLUG] = $referenctielN3->getSlug();
                    $nodeClauseN3[self::ACTIVE] = $referenctielN3->isActif();
                    $nodeClauseN3[self::LABEL] = $this->translator->trans($referenctielN3->getLabel());
                    $nodeClauseN3[self::IRI] = $this->iriConverter->getIriFromItem($referenctielN3);

                    // Noeud ClauseN4
                    foreach ($itemClauseN3->getClausesN4() as $itemClauseN4) {
                        $referenctielN4 = $itemClauseN4->getReferentielClausesN4();
                        $nodeClauseN4 = [];
                        $nodeClauseN4[self::VALUE] = $itemClauseN4->getValeur();
                        $nodeClauseN4[self::SLUG] = $referenctielN4->getSlug();
                        $nodeClauseN4[self::ACTIVE] = $referenctielN4->getActif();
                        $nodeClauseN4[self::TYPE] = $referenctielN4->getTypeChamp();
                        $nodeClauseN4[self::LABEL] = $referenctielN4->getLabel();
                        $nodeClauseN4[self::IRI] = $this->iriConverter->getIriFromItem($referenctielN4);

                        $nodeClauseN3[self::CLAUSES_N4][] = $nodeClauseN4;
                    }
                    $nodeClauseN2[self::CLAUSES_N3][] = $nodeClauseN3;
                }
                $nodeClauseN1[self::CLAUSES_N2][] = $nodeClauseN2;
            }

            if (!in_array($nodeClauseN1, $clauses)) {
                $clauses[] = $nodeClauseN1;
            }
        }

        return $clauses;
    }
}
