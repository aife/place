<?php

namespace App\Service;

use DateTime;
use AtexoCrypto\Exception\ServiceException;
use Exception;
use App\Entity\Enveloppe;
use App\Entity\EnveloppeFichier;
use App\Entity\Offre;
use AtexoCrypto\Dto\InfosHorodatage;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class AtexoHorodatageOffre de gestion d'horodatage des offres.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since ESR2016
 *
 * @copyright Atexo 2016
 */
class AtexoHorodatageOffre
{
    /**
     * AtexoHorodatageOffre constructor.
     *
     * @param $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct(private ContainerInterface $container, LoggerInterface $depotLogger)
    {
        $this->logger = $depotLogger;
    }

    /**
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @param mixed $container
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    /**
     * Permet de horodater l'offre.
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function demanderHorodaterOffre(Offre $offre, $request)
    {
        $hash256 = null;
        $fullPathXmlOffre = null;
        $untrusteddateOffre = null;
        $horodatageSucces = null;
        $infoOffre = '';
        try {
            $idConsultation = $offre->getConsultationId();
            $infoOffre = 'idConsultation = '.$idConsultation.' , idOffre = '.$offre->getId();
            $infoOffre .= ' , organisme = '.$offre->getOrganisme();
            $info = "Debut demande d'horodatge de l'offre: ".$infoOffre;
            $info .= "  statut de l'offre  : ".$offre->getStatutOffres();
            $this->logger->info($info);
            $arrayStatutAuthorise = [
                $this->getContainer()->getParameter('STATUT_ENV_EN_ATTENTE_CHIFFREMENT'),
                $this->getContainer()->getParameter('STATUT_ENV_FERMETURE_EN_ATTENTE'),
            ];
            if (in_array($offre->getStatutOffres(), $arrayStatutAuthorise)) {
                $infosHorodatage = null;
                try {
                    $pathXmlOffre = $this->generateXmlOffre($offre);
                    $atexoBlobOrganisme = $this->container->get(AtexoFichierOrganisme::class);
                    $atexoBlobOrganisme->moveTmpFile($pathXmlOffre, $request);
                    $idBlobXmlHash256 = $atexoBlobOrganisme->insertFile(
                        'xmlOffre_'.$offre->getId(),
                        $pathXmlOffre,
                        $offre->getOrganisme()
                    );
                    $offre->setIdBlobHorodatageHash($idBlobXmlHash256);
                    $fullPathXmlOffre = $this->container->getParameter('COMMON_TMP').$pathXmlOffre;
                    $hash256 = hash_file('sha256', $fullPathXmlOffre);
                    $cryptoService = $this->container->get('atexo_crypto.crypto');
                    $infosHorodatage = $cryptoService->demandeHorodatage($hash256);
                    $untrusteddateOffre = '';
                    if ($infosHorodatage instanceof InfosHorodatage) {
                        $info = "Demande d'horodatage OK.".PHP_EOL;
                        $info .= "Infos demande d'horodatage : ".print_r($infosHorodatage, true);
                        $this->logger->info($info);
                        $untrusteddateOffre = new DateTime(
                            date('Y-m-d H:i:s', $infosHorodatage->getHorodatage())
                        );
                        $offre->setHorodatage($infosHorodatage->getJetonHorodatageBase64());
                        $horodatageSucces = true;
                    }
                    @unlink($fullPathXmlOffre);
                } catch (ServiceException $e) {
                    $erreur = '['.$infoOffre.']'.PHP_EOL.'[code_erreur = '.$e->errorCode.'] '.PHP_EOL
                        .' [etatObjet = '.$e->etatObjet.'] '.PHP_EOL.' [message_erreur = '.$e->message.'] '
                        .PHP_EOL." [xmlHash256 : $hash256 ]".PHP_EOL." [pathXmlOffre : $fullPathXmlOffre ]";
                    $this->logger->error("Erreur lors de la demande d'horodatage de l'offre : $erreur");
                } catch (Exception $e) {
                    $erreur = '['.$infoOffre.']'.PHP_EOL.'[Erreur = '.$e->getMessage().'] '.PHP_EOL;
                    $erreur .= ' [Trace = '.$e->getTraceAsString().']';
                    $this->logger->error("Erreur lors de la demande d'horodatatge de l'offre : $erreur ");
                }
                if (!$horodatageSucces || (empty($untrusteddateOffre) || (
                    $untrusteddateOffre instanceof DateTime
                    && '-0001-11-30 00:00:00' == $untrusteddateOffre->format('Y-m-d H:i:s'))
                    )
                ) {
                    $offre->setUntrusteddate($offre->getDateDepot());
                } else {
                    $offre->setUntrusteddate($untrusteddateOffre);
                }
                $em = $this->getContainer()->get('doctrine')->getManager();
                $em->persist($offre);
                $em->flush();
                $this->logger->info('Fin demande de horodatage offre : '.$infoOffre);
            } else {
                $info = "Demande d'horodatage offre: ".$infoOffre;
                $info .= ' a un statut invalide : ( '.$offre->getStatutOffres().') ';
                $info .= ' les offres autorisées sont les offres en statut en attente de chiffrement ou attente de ';
                $info .= 'fermeture)';

                $this->logger->info($info);
            }
        } catch (Exception $e) {
            $erreur = '['.$infoOffre.']'.PHP_EOL.'[Erreur = '.$e->getMessage().'] '.PHP_EOL;
            $erreur .= ' [Trace = '.$e->getTraceAsString().']';
            $this->logger->error("Erreur lors de la demande d'horodatatge de l'offre : $erreur ");
        }

        return $horodatageSucces;
    }

    /**
     * Gets a container configuration parameter by its name.
     *
     * @param string $name The parameter name
     *
     * @return mixed
     */
    protected function getParameter($name)
    {
        return $this->getContainer()->getParameter($name);
    }

    /**
     * @param $offre
     *
     * @return string
     *
     * @throws Exception
     */
    public function generateXmlOffre($offre)
    {
        $filename = null;
        $fileTmp = '';
        try {
            if ($offre instanceof Offre) {
                $this->logger->info('Debut generation xml offre : '.$offre->getId());
                $fileTmp = $this->container->getParameter('COMMON_TMP');
                $filename = 'xmlOffre_'.$offre->getId().time().uniqid();
                $fp = fopen($fileTmp.$filename, 'a');
                $xmlEntete = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<horodatage>
   <idOffre>{$offre->getId()}</idOffre>
   <fichiers>
XML;
                fwrite($fp, $xmlEntete);
                $enveloppes = $offre->getEnveloppes();
                $xmlFichiers = '';
                foreach ($enveloppes as $enveloppe) {
                    /* @var Enveloppe $enveloppe */
                    $fichiers = $enveloppe->getFichierEnveloppes();
                    foreach ($fichiers as $fichier) {
                        /* @var EnveloppeFichier $fichier */
                        $info = 'generation xml offre : '.$offre->getId();
                        $info .= ' enveloppe '.$enveloppe->getIdEnveloppeElectro();
                        $info .= ' fichier '.$fichier->getIdFichier();
                        $this->logger->info($info);
                        $xmlFichiers = <<<XMl

        <fichier>
            <hash>{$fichier->getHash256()}</hash>
             <nom>{$fichier->getNomFichier()}</nom>
         </fichier>
XMl;
                        fwrite($fp, $xmlFichiers);
                    }
                }
                $xmlFooter = <<<STRING

    </fichiers>
</horodatage>
STRING;
                fwrite($fp, $xmlFooter);
                fclose($fp);
                $this->logger->info('Fin generation xml offre : '.$offre->getId());
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $filename;
    }
}
