<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service;

use App\Entity\Agent;
use App\Security\FormAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LoginFromPrado
{
    protected const SECURITY_PROVIDER = 'agent_provider';

    public function __construct(private readonly GuardAuthenticatorHandler $authenticatorHandler, private readonly FormAuthenticator $formAuthenticator, private readonly EntityManagerInterface $em, private readonly Security $security)
    {
    }

    public function loginByUsername(string $username): Agent
    {
        if ($this->security->getUser() instanceof Agent) {
            return $this->security->getUser();
        }

        $request = Request::createFromGlobals();
        $repo = $this->em->getRepository(Agent::class);
        $agent = $repo->findOneBy(['login' => $username]);

        $this->authenticatorHandler->authenticateUserAndHandleSuccess(
            $agent,
            $request,
            $this->formAuthenticator,
            self::SECURITY_PROVIDER
        );

        return $this->security->getUser();
    }
}
