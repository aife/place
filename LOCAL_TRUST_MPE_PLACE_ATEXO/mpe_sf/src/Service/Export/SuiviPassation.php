<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Export;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\ContratTitulaire;
use App\Entity\Entreprise;
use App\Entity\GeolocalisationN2;
use App\Entity\Offre;
use App\Entity\OffrePapier;
use App\Service\AtexoConfiguration;
use App\Utils\EntityPurchase;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SuiviPassation
{
    public function __construct(
        private readonly ParameterBagInterface $parameterBag,
        private readonly EntityManagerInterface $entityManager,
        private readonly AtexoConfiguration $atexoConfiguration,
        private readonly TranslatorInterface $translator,
        private readonly EntityPurchase $entityPurchase,
    ) {
    }


    public function getEnteteExportMarchesConclus($fileCsv)
    {
        $csvContent = 'nom_entreprise|numero_marche|date_notification|montant_marche|date_mise_en_ligne_calcule'.
            '|reference_utilisateur|libelle_type_procedure (TypeProcedure)|libelle (CategorieConsultation)|resume'.
            '|lieu_execution|code_cpv_1|alloti|donnée calculée|lot|description|cpvLot|libelle (unite)'.
            '|libelle (Mandataire)|calculé à partir de service_id|calculé à partir de service_associe_id'.
            '|libelle (nature acte juridique)|forme_groupement|libelle (mode execution contrat)|montant_estime'.
            '|min_bon_commande|max_bon_commande|duree_execution_marche_hors_reconduction|nombre_reconduction'.
            '|duree_total_marche|ccag_applicable|numero_deliberation_financiere|date_deliberation_financiere'.
            '|numero_deliberation_autorisant_signature_marche|date_deliberation_autorisant_signature_marche'.
            '|date_notification_previsionnelle|date_reception_projet_DCE_Service_Validateur'.
            '|date_formulations_premieres_observations|date_retour_projet_DCE_finalise'.
            '|date_validation_projet_DCE_par_service_validateur|date_validation_projet_DCE_vue_par'.
            '|date_reception_projet_AAPC_par_Service_Validateur|date_formulations_premieres_observations_AAPC'.
            '|date_retour_projet_AAPC_finalise|date_validation_projet_AAPC_par_Service_Validateur'.
            '|date_validation_projet_AAPC_par_Service_Validateur_vu_par|date_envoi_publicite'.
            '|date_envoi_invitations_remettre_offre|date_limite_remise_offres|datefin'.
            '|Délai de remise des offres (donnée calculée)|delai_validite_offres'.
            '|Date de péremption des offres (donnée calculée)|date_reunion_ouverture_candidatures'.
            '|date_reunion_ouverture_offres|date_reception_analyse_offre|date_formulation_observation_projet_rapport'.
            '|date_retour_projet_rapport_finalise|date_validation_projet_rapport|projet_rapport_vu_par'.
            '|date_reunion_attribution|date_decision|decision|date_envoi_courrier_condidat_non_retenu'.
            '|date_signature_marche_pa|date_reception_controle_legalite|date_formulation_observation_dossier'.
            '|date_retour_dossier_finalise|date_transmission_prefecture|dossier_vu_par'.
            '|date_validation_rapport_information|id_consultation|id_passation_consultation'.
            '|id_passation_marche_a_venir|id_decision_enveloppe';
        if ($this->atexoConfiguration->hasConfigPlateforme('consultationClause')) {
            $csvContent .= '|clause_sociale|clause_environnementale';
        }
        $csvContent .= "\r\n";

        $csvContent .= "Nom du Titulaire du marché|N° de marché|Date de notification|Montant du marché".
            "|Date de mise en ligne|Référence|Type de procédure|Catégorie principale|Objet de la consultation".
            "|Lieu d'exécution |Code principal du CPV de la consultation|Allotissement|Nombre de lots|N° du lot".
            "|Intitulé du lot|Code principal du CPV du lot|Unité|Mandataire|Service de rattachement|Service associé".
            "|Nature de l'acte juridique|Forme du groupement|Mode d'exécution du contrat|montant estimé du marché".
            "|Montant Mini HT (MBC)|Montant Maxi HT (MBC)|Durée d'exécution prévisionnelle du marché hors reconduction".
            "|Nombre de reconduction|Durée totale du marché|CCAG applicable|N° de délibération financière|".
            "Date de la délibération financière|N° de délibération autorisant la signature du marché".
            "|Date de la délibération autorisant la signature du marché|Date de notification prévisionnelle".
            "|Date de réception du projet de DCE par le Service Valideur".
            "|Date des formulations des premières observations sur le projet de DCE par le Service Validateur ".
            "|Date de retour du projet de DCE finalisé |Date de validation du projet de DCE par le Service Validateur ".
            "|Vu par :|Date de réception du projet d'AAPC par le Service Validateur ".
            "|Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur ".
            "|Date de retour du projet d'AAPC finalisé |Date de validation du projet d'AAPC par le Service Validateur ".
            "|Vu par :|Date d'envoi de la publicité|Date d'envoi des invitations à remettre une offre".
            "|Date limite de remise des offres|Date limite de remise des plis".
            "|Délai de remise des offres (en jours francs)|Délai de validité des offres|Date de péremption des offres|".
            "Réunion d'ouverture des candidatures |Réunion d'ouverture des offres".
            "|Réception du projet de rapport d'analyse des offres par le Service Validateur ".
            "|Formulations des premières observations sur le projet de rapport par le Service Validateur ".
            "|Retour du projet de rapport finalisé |Validation du projet de rapport finalisé |Vu par :".
            "|Date de la réunion d'attribution|Date de la décision|Nature de la décision".
            "|Envoi des courriers aux candidats non retenus |Signature du marché par le Pouvoir Adjudicateur ".
            "|Réception du dossier 'contrôle de légalité' parle Service Validateur :".
            "|Formulations des premières observations sur le dossier par le Service Validateur".
            "|Retour du dossier finalisé|Transmission en Préfecture|Vu par :".
            "|Si MAPA, Validation du rapport d'information destiné à la CP|identifiant de la consultation".
            "|identifiant de la passation|identifiant marche|identifiant de la décision";
        if ($this->atexoConfiguration->hasConfigPlateforme('consultationClause')) {
            $csvContent .= '|clause sociale|clause environnementale';
        }
        $csvContent .= "\r\n";

        $this->writeCsv($fileCsv, $csvContent);
    }

    public function generateExportMarchesConclus($organisme, $fileCsv)
    {
        $separateur = "|";
        $resDecisions = $this->entityManager->getRepository(ContratTitulaire::class)->getContratsWithConsLotNotChapeau($organisme);
        while ($decision = $resDecisions->fetchAssociative()) {
            $lineCsvMarcheConclus = "";
            //Nom du Titulaire du marché
            $lineCsvMarcheConclus .= $this->getNomEntreprise(
                $decision['typeDepotReponse'],
                $decision['idOffre'],
                $organisme
            );
            $lineCsvMarcheConclus .= $separateur;
            //N° de marché
            $lineCsvMarcheConclus .= $decision['numeroContrat'];
            $lineCsvMarcheConclus .= $separateur;
            //Date de notification
            $decision['dateNotification'] = new \DateTime($decision['dateNotification']);
            $date = $decision['dateNotification'] ? $decision['dateNotification']->format("Y/m/d H:i:s") : "";

            $lineCsvMarcheConclus .= $date;
            $lineCsvMarcheConclus .= $separateur;
            //Montant du marché
            $lineCsvMarcheConclus .= $decision['montantContrat'];
            $lineCsvMarcheConclus .= $separateur;
            $infosPassation = $this->getSuiviPassationInformation($organisme, $decision['consultationId'], $decision['lot']);
            foreach ($infosPassation as $info) {
                $csvContent = $lineCsvMarcheConclus.implode($separateur, $info)."\n";
                $this->writeCsv($fileCsv, $csvContent, 'a');
            }
        }
    }

    public function writeCsv($fileCsv, $ligne, $mode = 'w')
    {
        $fp = fopen($fileCsv, $mode);
        fwrite($fp, $ligne);
        fclose($fp);
    }

    public function getNomEntreprise($typeDepot, $id, $organisme)
    {
        $nomEntreprise = "";
        if ($typeDepot == $this->parameterBag->get('DEPOT_ELECTRONIQUE')) {
            $offre = $this->entityManager->getRepository(Offre::class)->findOneBy(
                [
                    "id" => $id,
                    "organisme" => $organisme,
                ]
            );
            if ($offre instanceof Offre) {
                $entreprise = $this->entityManager->getRepository(Entreprise::class)->find($offre->getEntrepriseId());
                if ($entreprise) {
                    $nomEntreprise = $entreprise->getNom();
                }
            }
        } else {
            $offre = $this->entityManager->getRepository(OffrePapier::class)->findOneBy(
                [
                    "id" => $id,
                    "organisme" => $organisme,
                ]
            );
            if ($offre instanceof OffrePapier) {
                $nomEntreprise = $offre->getNomEntreprise();
            }
        }

        return $nomEntreprise;
    }

    public function getSuiviPassationInformation(
        $organisme,
        $id = null,
        $lot = null,
        $idDecision = null
    ) {
        $res = $this->entityManager->getRepository(Consultation::class)->getDonneesPassation($organisme, $id, $lot);
        foreach ($res as $r) {
            $ligne = $this->formatDonneesPassation($r, $organisme, $idDecision);
            yield $ligne;
        }
    }

    public function formatDonneesPassation(
        array $arrayData,
        $organisme,
        $idDecision = null
    ) {
        $data = [];
        //Date de mise en ligne *
        $dateMiseEnligne = '';
        $data [] = $this->formatDateTimeToFr($arrayData['date_mise_en_ligne_calcule']);
        //Référence *
        $data [] = $arrayData['reference_utilisateur'];

        //Type de procédure *
        $libelleTypeProcedure = str_replace(
            ['&#8804;', '&#8805;'],
            [$this->translator->trans('MAPA_INF_OU_EGALE'), $this->translator->trans('MAPA_SUPP_OU_EGALE')],
            $arrayData['procedure_org']
        );
        $data [] = $libelleTypeProcedure;

        //Catégorie principale *
        $data [] = $arrayData['libelle_categorie'];

        //Objet de la consultation
        $data [] = str_replace("\r", '', str_replace("\n", '', ($arrayData['objet'] ?: ' ')));

        //Lieu d'exécution
        $lieuxExecutions = ' ';
        $listeLieuxExecution = explode(',', $arrayData['lieu_execution']);
        if (is_array($listeLieuxExecution)) {
            $lieux = $this->entityManager
                ->getRepository(GeolocalisationN2::class)
                ->getDenominationLieuxExecution($listeLieuxExecution);
            if (!empty($lieux)) {
                $lieuxExecutions = implode("#", $lieux);
            }
        }
        $data [] = $lieuxExecutions;

        //Code principal du CPV de la consultation
        $data [] = ($arrayData['code_cpv_1'] ?: ' ');

        //Allotissement
        $data [] =
            ('1' == $arrayData['alloti']) ? $this->translator->trans('DEFINE_OUI') : $this->translator->trans('TEXT_NON');

        //Nombre de lots
        $nombreLot = $arrayData['nombre_lot'];
        $data [] = $nombreLot;

        //N° du lot
        $data [] = $arrayData['numLot'];

        //Intitulé du lot
        $data [] =
            ($arrayData['description'] ?
                str_replace(["\r", "\n"], ['', ''], $arrayData['description']) : ' ');

        //Code principal du CPV du lot
        $data [] = ($arrayData['cpvLot'] ?: ' ');

        //Unité
        $data [] = ($arrayData['unite_fcsp']) ? str_replace(["\r", "\n"], ['', ''], $arrayData['unite_fcsp']) : ' ';
        //Mandataire
        $data [] = ($arrayData['libelle_mandataire']) ? str_replace(["\r", "\n"], ['', ''], $arrayData['libelle_mandataire']) : ' ';;

        //Service de rattachement
        $libelleService = $this->entityPurchase->getEntityPathById($arrayData['service_id'], $organisme, true, false);
        $libelleService = $libelleService ?: ' ';
        $data [] = $libelleService;

        //Service associé
        $libelleService = $this->entityPurchase->getEntityPathById($arrayData['service_associe_id'], $organisme, true, false);
        $libelleService = $libelleService ?: ' ';
        $data [] = $libelleService;

        //Nature de l'acte juridique
        $data [] = ($arrayData['libelle_nature_acte_juridique']) ? str_replace(["\r", "\n"], ['', ''], $arrayData['libelle_nature_acte_juridique']) : ' ';

        //Forme du groupement
        if (null !== $arrayData['forme_groupement']) {
            if ('1' == $arrayData['forme_groupement']) {
                $data [] = $this->translator->trans('FCSP_CONJOINT');
            } elseif ('2' == $arrayData['forme_groupement']) {
                $data [] = $this->translator->trans('FCSP_SOLIDAIRE');
            } else {
                $data [] = $this->translator->trans('FCSP_AU_CHOIX');
            }
        }

        //Mode d'exécution du contrat
        $data [] = ($arrayData['libelle_mode_execution_contrat']) ? str_replace(["\r", "\n"], ['', ''], $arrayData['libelle_mode_execution_contrat']) : ' ';

        //Montant estimé
        $data [] = ($arrayData['montant_estime'] ?: ' ');

        //Montant Mini HT (MBC)|Montant Maxi HT (MBC)
        if ('2' == $arrayData['mode_execution_contrat']) {
            $data [] = $arrayData['min_bon_commande'];

            $data [] = $arrayData['max_bon_commande'];
        } else {
            $data [] = ' ';

            $data [] = ' ';
        }
        //Durée d'exécution prévisionnelle du marché hors reconduction
        $data [] = ($arrayData['duree_execution_marche_hors_reconduction'] ?: '0');

        //Nombre de reconduction
        $data [] = ($arrayData['nombre_reconduction'] ?: '0');

        //Durée totale du marché
        $data [] = ($arrayData['duree_total_marche'] ?: '0');

        //CCAG applicable
        $data [] = ($arrayData['libelle_ccag_applicable']) ? str_replace(["\r", "\n"], ['', ''], $arrayData['libelle_ccag_applicable']) : ' ';

        //N° de délibération financière
        $data [] = ($arrayData['numero_deliberation_financiere'] ?: ' ');

        //Date de la délibération financière
        $data [] = $this->formatDateTimeToFr($arrayData['date_deliberation_financiere']);

        //N° de délibération autorisant la signature du marché
        $data [] = ($arrayData['numero_deliberation_autorisant_signature_marche'] ?: '0');

        //Date de la délibération autorisant la signature du marché
        $data [] = $this->formatDateToFr($arrayData['date_deliberation_autorisant_signature_marche']);

        //Date de notification prévisionnelle
        $data [] = $this->formatDateToFr($arrayData['date_notification_previsionnelle']);

        //Date de réception du projet de DCE par le Service Valideur
        $data [] = $this->formatDateToFr($arrayData['date_reception_projet_DCE_Service_Validateur']);

        //Date des formulations des premières observations sur le projet de DCE par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_formulations_premieres_observations']);

        //Date de retour du projet de DCE finalisé
        $data [] = $this->formatDateToFr($arrayData['date_retour_projet_DCE_finalise']);

        //Date de validation du projet de DCE par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_validation_projet_DCE_par_service_validateur']);

        //Vu par :
        $data [] = $this->getPrenomNomAgent($arrayData['date_validation_projet_DCE_vue_par']);

        //Date de réception du projet d'AAPC par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_reception_projet_AAPC_par_Service_Validateur']);

        //Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_formulations_premieres_observations_AAPC']);

        //Date de retour du projet d'AAPC finalisé
        $data [] = $this->formatDateToFr($arrayData['date_retour_projet_AAPC_finalise']);

        //Date de validation du projet d'AAPC par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_validation_projet_AAPC_par_Service_Validateur']);

        //Vu par :
        $data [] = $this->getPrenomNomAgent($arrayData['date_validation_projet_AAPC_par_Service_Validateur_vu_par']);

        //Date d'envoi de la publicité
        $data [] = $this->formatDateToFr($arrayData['date_envoi_publicite']);

        //Date d'envoi des invitations à remettre une offre
        $data [] = $this->formatDateToFr($arrayData['date_envoi_invitations_remettre_offre']);

        //Date limite de remise des offres
        $data [] = $this->formatDateToFr($arrayData['date_limite_remise_offres']);

        //Date limite de remise des plis
        $data [] = $this->formatDateTimeToFr($arrayData['datefin']);

        //Délai de remise des offres (en jours francs)
        if ($arrayData['date_limite_remise_offres'] && $arrayData['date_envoi_invitations_remettre_offre']) {
            $delaiRemiseOffre = ceil(
                    $this->diffJours(
                        $arrayData['date_limite_remise_offres'],
                        $arrayData['date_envoi_invitations_remettre_offre']
                    )
                ) - 1;
        } elseif ($arrayData['date_limite_remise_offres'] && $arrayData['date_envoi_publicite']) {
            $delaiRemiseOffre = ceil(
                    $this->diffJours($arrayData['date_limite_remise_offres'], $arrayData['date_envoi_publicite'])
                ) - 1;
        } else {
            $delaiRemiseOffre = '';
        }
        $data [] = $delaiRemiseOffre;

        //Délai de validité des offres
        $data [] = ($arrayData['delai_validite_offres'] ?: '0');

        //Date de péremption des offres
        $date = $arrayData['date_limite_remise_offres'];
        $nombreJours = $arrayData['delai_validite_offres'] ?: '0';
        if ($date) {
            [$j, $m, $y] = explode('/', $date);
            $timeStamp = mktime(0, 0, 0, $m, $j, $y) + ($nombreJours * 24 * 60 * 60);
            $resultat = date('d/m/Y', $timeStamp);
        } else {
            $resultat = ' ';
        }
        $data [] = $resultat;

        //Réunion d'ouverture des candidatures
        $data [] = $this->formatDateToFr($arrayData['date_reunion_ouverture_candidatures']);

        //Réunion d'ouverture des offres
        $data [] = $this->formatDateToFr($arrayData['date_reunion_ouverture_offres']);

        //Réception du projet de rapport d'analyse des offres par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_reception_analyse_offre']);

        //Formulations des premières observations sur le projet de rapport par le Service Validateur
        $data [] = $this->formatDateToFr($arrayData['date_formulation_observation_projet_rapport']);

        //Retour du projet de rapport finalisé
        $data [] = $this->formatDateToFr($arrayData['date_retour_projet_rapport_finalise']);

        //Validation du projet de rapport finalisé
        $data [] = $this->formatDateToFr($arrayData['date_validation_projet_rapport']);

        //Vu par :
        $data [] = $this->getPrenomNomAgent($arrayData['projet_rapport_vu_par']);

        //Date de la réunion d'attribution
        $data [] = $this->formatDateToFr($arrayData['date_reunion_attribution']);

        //Date de la décision
        $lot = $arrayData['numLot'] ?: '0';
        $data [] = $this->formatDateToFr($arrayData['date_decision']);

        //Nature de la décision
        $data [] = $arrayData['libelle_decision'] ?: ' ';

        //Envoi des courriers aux candidats non retenus
        $data [] = $this->formatDateToFr($arrayData['date_envoi_courrier_condidat_non_retenu']);

        //Signature du marché par le Pouvoir Adjudicateur
        $data [] = $this->formatDateToFr($arrayData['date_signature_marche_pa']);

        //Réception du dossier 'contrôle de légalité' parle Service Validateur :
        $data [] = $this->formatDateToFr($arrayData['date_reception_controle_legalite']);

        //"Formulations des premières observations sur le dossier par le Service Validateur "
        $data [] = $this->formatDateToFr($arrayData['date_formulation_observation_dossier']);

        //Retour du dossier finalisé
        $data [] = $this->formatDateToFr($arrayData['date_retour_dossier_finalise']);

        //Transmission en Préfecture
        $data [] = $this->formatDateToFr($arrayData['date_transmission_prefecture']);

        //Vu par :
        $data [] = $this->getPrenomNomAgent($arrayData['dossier_vu_par']);

        //"Si MAPA, Validation du rapport d'information destiné à la CP"
        if ('1' == $arrayData['mapa']) {
            $data [] = $this->formatDateToFr($arrayData['date_validation_rapport_information']);
        } else {
            $data [] = '';
        }
        $data [] = $arrayData['id'];
        $data [] = $arrayData['idPassation'];
        $data [] = $arrayData['idMarche'];
        if ($idDecision) {
            $data [] = $idDecision;
        }
        if ($this->atexoConfiguration->hasConfigPlateforme('consultationClause')) {
            if ('0' == $arrayData['alloti']) {
                if (2 == $arrayData['clause_sociale']) {
                    $data [] = $this->translator->trans('TEXT_NON');
                } elseif (1 == $arrayData['clause_sociale']) {
                    $data [] = $this->translator->trans('TEXT_OUI');
                } else {
                    $data [] = $this->translator->trans('NON_RENSEIGNE');
                }

                if (2 == $arrayData['clause_environnementale']) {
                    $data [] = $this->translator->trans('TEXT_NON');
                } elseif (1 == $arrayData['clause_environnementale']) {
                    $data [] = $this->translator->trans('TEXT_OUI');
                } else {
                    $data [] = $this->translator->trans('NON_RENSEIGNE');
                }
            } else {
                if (2 == $arrayData['clause_sociale_lot']) {
                    $data [] = $this->translator->trans('TEXT_NON');
                } elseif (1 == $arrayData['clause_sociale_lot']) {
                    $data [] = $this->translator->trans('TEXT_OUI');
                } else {
                    $data [] = $this->translator->trans('NON_RENSEIGNE');
                }
                if (1 == $arrayData['clause_environnementale_lot']) {
                    $data [] = $this->translator->trans('TEXT_OUI');
                } elseif (2 == $arrayData['clause_environnementale_lot']) {
                    $data [] = $this->translator->trans('TEXT_NON');
                } else {
                    $data [] = $this->translator->trans('NON_RENSEIGNE');
                }
            }
        }

        return $data;
    }

    public function formatDateToFr($date)
    {
        $return = '';
        if (
            !empty($date)
            &&
            $date != "0000-00-00 00:00:00"
        ) {
            $return = date("Y-m-d", strtotime($date));
        }

        return $return;
    }

    public function formatDateTimeToFr($date)
    {
        $return = '';
        if (10 == strlen($date)) {
            $return = $this->formatDateToFr($date);
        } elseif (
            !empty($date)
            &&
            $date != "0000-00-00 00:00:00"
        ) {
            $date = new \DateTime($date);
            $return = $date->format("Y/m/d H:i:s");
        }

        return $return;
    }

    public function diffJours($date1, $date2)
    {
        $start_date = new \DateTime();
        $start_date->setTimestamp(strtotime($date1));
        $end_date = new \DateTime();
        $end_date->setTimestamp(strtotime($date2));

        $interval = $start_date->diff($end_date);

        return $interval->format('%a');
    }

    public function getPrenomNomAgent($id)
    {
        $nomAgent = ' ';
        if (!empty($id)) {
            $agent = $this->entityManager->getRepository(Agent::class)->find($id);
            if ($agent instanceof Agent) {
                $nomAgent = $agent->getPrenom().' '.$agent->getNom();
            }
        }

        return $nomAgent;
    }

    public function getEnteteExportSuiviPassation($fileCsv)
    {
        $csvContent =
            'date_mise_en_ligne_calcule|reference_utilisateur|libelle_type_procedure (TypeProcedure)'.
            '|libelle (CategorieConsultation)|resume|lieu_execution|code_cpv_1|alloti|donnée calculée|lot'.
            '|description|cpvLot|libelle (unite)|libelle (Mandataire)|calculé à partir de service_id'.
            '|calculé à partir de service_associe_id|libelle (nature acte juridique)|forme_groupement'.
            '|libelle (mode execution contrat)|montant_estime|min_bon_commande|max_bon_commande'.
            '|duree_execution_marche_hors_reconduction|nombre_reconduction|duree_total_marche'.
            '|ccag_applicable|numero_deliberation_financiere|date_deliberation_financiere'.
            '|numero_deliberation_autorisant_signature_marche|date_deliberation_autorisant_signature_marche'.
            '|date_notification_previsionnelle|date_reception_projet_DCE_Service_Validateur'.
            '|date_formulations_premieres_observations|date_retour_projet_DCE_finalise'.
            '|date_validation_projet_DCE_par_service_validateur|date_validation_projet_DCE_vue_par'.
            '|date_reception_projet_AAPC_par_Service_Validateur|date_formulations_premieres_observations_AAPC'.
            '|date_retour_projet_AAPC_finalise|date_validation_projet_AAPC_par_Service_Validateur'.
            '|date_validation_projet_AAPC_par_Service_Validateur_vu_par|date_envoi_publicite'.
            '|date_envoi_invitations_remettre_offre|date_limite_remise_offres|datefin'.
            '|Délai de remise des offres (donnée calculée)|delai_validite_offres'.
            '|Date de péremption des offres (donnée calculée)|date_reunion_ouverture_candidatures'.
            '|date_reunion_ouverture_offres|date_reception_analyse_offre'.
            '|date_formulation_observation_projet_rapport|date_retour_projet_rapport_finalise'.
            '|date_validation_projet_rapport|projet_rapport_vu_par|date_reunion_attribution|date_decision|decision'.
            '|date_envoi_courrier_condidat_non_retenu|date_signature_marche_pa|date_reception_controle_legalite'.
            '|date_formulation_observation_dossier|date_retour_dossier_finalise|date_transmission_prefecture'.
            '|dossier_vu_par|date_validation_rapport_information|id_consultation|id_passation_consultation'.
            '|id_passation_marche_a_venir';
        if ($this->atexoConfiguration->hasConfigPlateforme('consultationClause')) {
            $csvContent .= '|clause_sociale|clause_environnementale';
        }
        $csvContent .= "\r\n";
        $csvContent .=
            "Date de mise en ligne|Référence|Type de procédure|Catégorie principale".
            "|Objet de la consultation|Lieu d'exécution |Code principal du CPV de la consultation|Allotissement".
            "|Nombre de lots|N° du lot|Intitulé du lot|Code principal du CPV du lot|Unité|Mandataire".
            "|Service de rattachement|Service associé|Nature de l'acte juridique|Forme du groupement".
            "|Mode d'exécution du contrat|montant estimé du marché|Montant Mini HT (MBC)|Montant Maxi HT (MBC)".
            "|Durée d'exécution prévisionnelle du marché hors reconduction|Nombre de reconduction".
            "|Durée totale du marché|CCAG applicable|N° de délibération financière".
            "|Date de la délibération financière|N° de délibération autorisant la signature du marché".
            "|Date de la délibération autorisant la signature du marché|Date de notification prévisionnelle".
            "|Date de réception du projet de DCE par le Service Valideur".
            "|Date des formulations des premières observations sur le projet de DCE par le Service Validateur ".
            "|Date de retour du projet de DCE finalisé ".
            "|Date de validation du projet de DCE par le Service Validateur |Vu par :".
            "|Date de réception du projet d'AAPC par le Service Validateur ".
            "|Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur ".
            "|Date de retour du projet d'AAPC finalisé ".
            "|Date de validation du projet d'AAPC par le Service Validateur |Vu par :".
            "|Date d'envoi de la publicité|Date d'envoi des invitations à remettre une offre".
            "|Date limite de remise des offres|Date limite de remise des plis".
            "|Délai de remise des offres (en jours francs)|Délai de validité des offres".
            "|Date de péremption des offres|Réunion d'ouverture des candidatures |Réunion d'ouverture des offres".
            "|Réception du projet de rapport d'analyse des offres par le Service Validateur ".
            "|Formulations des premières observations sur le projet de rapport par le Service Validateur ".
            "|Retour du projet de rapport finalisé |Validation du projet de rapport finalisé |Vu par :".
            "|Date de la réunion d'attribution |Date de la décision|Nature de la décision".
            "|Envoi des courriers aux candidats non retenus |Signature du marché par le Pouvoir Adjudicateur ".
            "|Réception du dossier 'contrôle de légalité' parle Service Validateur :".
            "|Formulations des premières observations sur le dossier par le Service Validateur".
            "|Retour du dossier finalisé|Transmission en Préfecture|Vu par :".
            "|Si MAPA, Validation du rapport d'information destiné à la CP|identifiant de la consultation".
            "|identifiant de la passation|ide du marche à venir";
        if ($this->atexoConfiguration->hasConfigPlateforme('consultationClause')) {
            $csvContent .= '|clause sociale|clause environnementale';
        }
        $csvContent .= "\r\n";

        $this->writeCsv($fileCsv, $csvContent);
    }

    public function generateExportPassation($organisme, $fileCsv)
    {
        $separateur = "|";
        $infosPassation = $this->getSuiviPassationInformation($organisme);
        foreach ($infosPassation as $info) {
            if (!empty($info)) {
                $csvContent = implode($separateur, $info)."\n";
            }
            $this->writeCsv($fileCsv, $csvContent, 'a');
        }
    }
}
