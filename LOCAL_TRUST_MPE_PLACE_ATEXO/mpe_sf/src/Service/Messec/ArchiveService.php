<?php

namespace App\Service\Messec;

use League\Flysystem\FilesystemInterface;
use Doctrine\DBAL\Exception as DBALException;
use Exception;
use DateTime;
use App\Entity\Consultation;
use App\Model\Messec\Email;
use App\Serializer\Messec\EmailDenormalizer;
use App\Serializer\Messec\MessageDenormalizer;
use App\Serializer\Messec\PieceJointeDenormalizer;
use App\Service\Messagerie\ArchiveMessagerieService;
use App\Service\Messagerie\MessagerieService;
use App\Service\Messagerie\WebServicesMessagerie;
use App\Utils\Encryption;
use App\Utils\Filesystem\MountManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\Translation\TranslatorInterface;

class ArchiveService
{
    private readonly FilesystemInterface $fileManager;

    /**
     * EspaceDocumentaireService constructor.
     *
     * @param EntityManager       $entityManager
     * @param Encryption          $encryption
     * @param TranslatorInterface $translator
     */
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly ContainerInterface $container,
        private readonly ArchiveMessagerieService $messagerieService,
        private readonly LoggerInterface $archivageLogger,
        private readonly WebServicesMessagerie $webServicesMessagerie,
        MountManager $mountManager,
    ) {
        $this->fileManager = $mountManager->getFilesystem('common_tmp');
    }

    /**
     *
     * @throws Atexo_Consultation_Exception
     * @throws DBALException
     */
    public function getMessecEchanges(int $consultationId): array|bool
    {
        // Aucune vérification des droit ici car cela doit être fait dans un controller ou côté Prado
        try {
            $token = $this->getFormatedToken($consultationId);
            $echanges = $this->webServicesMessagerie->getContent('getEchanges', $token);
            $formatedEchanges = false;
            if (!empty($echanges)) {
                $formatedEchanges = $this->formatEchangesForDIC(
                    $this->getEchangesFromMessecResponse($echanges),
                    $token
                );
            }

            return $formatedEchanges;
        } catch (Exception $exception) {
            $this->loggerWriter($exception->getMessage());

            return false;
        }
    }

    /**
     * @param $consultationId
     *
     * @return array
     */
    private function getParamForToken($consultationId)
    {
        $params = [];
        $consultation = $this->em->getRepository(Consultation::class)->find($consultationId);
        if ($consultation instanceof Consultation) {
            $params = [
                'idObjetMetier' => $consultationId,
                'refObjetMetier' => $consultation->getReferenceUtilisateur(),
                'nomPfEmetteur' => $this->container->getParameter('MESSAGERIE_NOM_PF_EMETTEUR'),
                'typePlateformeRecherche' => 'DESTINATAIRE',
            ];
        }

        return $params;
    }

    /**
     * @param $emails
     * @param $token
     *
     *
     * @throws Exception
     */
    public function formatEchangesForDIC($emails, $token): array|bool
    {
        try {
            $echanges = [];
            if ($emails instanceof Email) {
                $echanges[] = $this->getMessages($emails, $token);
            } else {
                /**
                 * @var Email $email
                 */
                foreach ($emails as $email) {
                    $echanges[] = $this->getMessages($email, $token);
                }
            }
            return $echanges;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @throws Exception
     */
    protected function getMessages(Email $email, $token): array
    {
        $tempEchange = [];
        $tempEchange['ID'] = $email->getId();
        $tempEchange['ID_ECHANGE'] = $email->getMessage()->getId();
        $tempEchange['MAIL_DESTINATAIRE'] = $email->getEmail();
        $tempEchange['OBJET'] = $email->getMessage()->getObjetText();
        $tempEchange['CORPS'] = $email->getMessage()->getContenuText();

        if (!empty($email->getDateDemandeEnvoiAsDate())) {
            $dateMessage = new DateTime();
            $dateMessage->setTimestamp($email->getDateDemandeEnvoiAsDate());
            $tempEchange['DATE_MESSAGE'] = $dateMessage->format('Y-m-d H:i:s');
        }
        //Variables non utilisées dans le DIC et non définies explicitement dans MESSEC
        $tempEchange['UID'] = 'tempmessec' . uniqid();
        $tempEchange['AR'] = null;
        $tempEchange['DATE_AR'] = null;
        $tempEchange['TYPE_AR'] = null;
        $tempEchange['EXPEDITEUR'] = null;
        $tempEchange['FORMAT'] = null;
        $tempEchange['STATUS'] = null;
        $echangeDirectory = $tempEchange['UID'] . '/Echange_' . $email->getId() . '/';
        $pdfFileName = 'echanges_' . $tempEchange['ID'] . '.pdf';
        $saved = $this->getAndSavePdf($tempEchange['ID'], $token, $echangeDirectory, $pdfFileName);
        if ($saved) {
            $tempEchange['PDF_ECHANGE'] = $echangeDirectory . $pdfFileName;
        } else {
            throw new Exception('problème dans la récupération du PDF ayant pour idEmail ' . $email->getId());
        }

        $tempEchange['PJ_MESSAGE_1'] = [];

        foreach ($email->getMessage()->getPiecesJointes() as $pjAgent) {
            $saved = $this->getAndSavePieceJointe(
                $email->getCodeLien(),
                $pjAgent->getId(),
                $pjAgent->getContentType(),
                $echangeDirectory . 'pj_message_1/',
                $pjAgent->getNom()
            );
            if ($saved) {
                $tempEchange['PJ_MESSAGE_1'][] = $echangeDirectory . 'pj_message_1/' . $pjAgent->getNom();
            } else {
                throw new Exception(
                    'Problème dans la récupération de la PJ ayant pour id: '
                    . $pjAgent->getId()
                    . ' et comme codeLien: '
                    . $email->getCodeLien()
                );
            }
        }

        $tempEchange['PJ_MESSAGE_2'] = [];
        if (!empty($email->getReponse())) {
            foreach ($email->getReponse()->getMessage()->getPiecesJointes() as $pjEntreprise) {
                $saved = $this->getAndSavePieceJointe(
                    $email->getReponse()->getCodeLien(),
                    $pjEntreprise->getId(),
                    $pjEntreprise->getContentType(),
                    $echangeDirectory . 'pj_message_2/',
                    $pjEntreprise->getNom()
                );
                if ($saved) {
                    $path = $echangeDirectory . 'pj_message_2/' . $pjEntreprise->getNom();
                    $tempEchange['PJ_MESSAGE_2'][] = $path;
                } else {
                    throw new Exception(
                        'Problème dans la récupération de la PJ ayant pour id: '
                        . $pjEntreprise->getId()
                        . ' et comme codeLien: '
                        . $email->getCodeLien()
                    );
                }
            }
        }
        return $tempEchange;
    }


    /**
     * permet l'écriture d'un log dédié à la messagerie sécurisée.
     *
     * @param $message
     */
    public function loggerWriter($message)
    {
        $this->archivageLogger->error($message);
    }

    /**
     * @param $idEmail envoyé par la MESSEC
     * @param $token envoyé par la MESSEC
     * @param $dirName répertoire cible
     * @param $fileName nom du fichier cible
     *
     * @return bool true si le fichier a été sauvegardé sans problème, sinon false
     *
     * @throws Exception
     */
    public function getAndSavePdf($idEmail, $token, $dirName, $fileName)
    {
        try {
            $pdfData = $this->webServicesMessagerie->getContent('getPdf', $idEmail, $token);

            return $this->saveMessecFile($dirName, $fileName, $pdfData);
        } catch (Exception) {
            throw new Exception("Problème lors de la sauvegarde du PDF ayant pour idEmail($idEmail)");
        }
    }

    /**
     * @param $codeLien envoyé par la MESSEC
     * @param $pieceJointeId envoyé par la MESSEC
     * @param $contentType envoyé par la MESSEC
     * @param $dirName répertoire cible
     * @param $fileName nom du fichier cible
     *
     * @return bool true si le fichier a été sauvegardé sans problème, sinon false
     *
     * @throws Exception
     */
    public function getAndSavePieceJointe($codeLien, $pieceJointeId, $contentType, $dirName, $fileName)
    {
        try {
            $pieceJointeData = $this->webServicesMessagerie->getContent(
                'getPieceJointe',
                $codeLien,
                $pieceJointeId,
                $contentType
            );

            return $this->saveMessecFile($dirName, $fileName, $pieceJointeData);
        } catch (Exception) {
            throw new Exception(
                "Problème lors de la sauvegarde de la pieceJointeId($pieceJointeId) ayant pour codeLien($codeLien)"
            );
        }
    }

    /**
     * @param $dirName répertoire cible
     * @param $fileName nom du fichier cible
     * @param $data données à sauvegarder
     *
     * @return bool true si le fichier a été sauvegardé sans problème, sinon false
     *
     * @throws Exception
     */
    public function saveMessecFile($dirName, $fileName, $data)
    {
        try {
            $directoryOK = true;
            $fileOK = true;

            if (!$this->fileManager->has($dirName)) {
                $directoryOK = $this->fileManager->createDir($dirName);
            }

            $u = new Atexo_Util();
            if (!$this->fileManager->has($fileName)) {
                $fileName = $u->clean($u->OterAccents($fileName));
                $fileName = preg_replace('/([^.a-z0-9]+)/i', '_', $fileName);
                $fileOK = $this->fileManager->write(
                    $dirName . $fileName,
                    $data
                );
            }

            return $fileOK && $directoryOK;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $json
     */
    public function getResponseContent($json): false|string
    {
        $temp = json_decode($json, null, 512, JSON_THROW_ON_ERROR);
        $finalContent = $temp->content;

        return json_encode($finalContent, JSON_THROW_ON_ERROR);
    }

    /**
     * @return Serializer
     */
    private function getMessecSerializer()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [
            new EmailDenormalizer(
                new ObjectNormalizer(),
                new MessageDenormalizer(
                    new ObjectNormalizer(),
                    new PieceJointeDenormalizer(new ObjectNormalizer())
                )
            ),
        ];

        return new Serializer($normalizers, $encoders);
    }

    /**
     * Désérialisation de la réponse de MESSEC vers une liste d'objet Email.
     *
     * @param $echanges
     */
    public function getEchangesFromMessecResponse($echanges): array|object
    {
        try {
            $serializer = $this->getMessecSerializer();

            return $serializer->deserialize($this->getResponseContent($echanges), Email::class, 'json');
        } catch (Exception) {
            throw new Exception("Problème lors de la désérialisation de l'objet MESSEC");
        }
    }

    /**
     * Permet de recupérer le token formaté que l'on reçoit de MESSEC.
     *
     * @param $consultationId
     *
     * @return string
     */
    public function getFormatedToken($consultationId)
    {
        try {
            $token = $this->webServicesMessagerie->getContent(
                'initialisationSuiviToken',
                $this->getParamForToken($consultationId)
            );

            if (null === $token) {
                throw new Exception('Problème lors de la récupération ou du formatage du token suivi');
            }
            return $token;
        } catch (Exception) {
            throw new Exception('Problème lors de la récupération ou du formatage du token suivi');
        }
    }
}
