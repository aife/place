<?php

namespace App\Service;

use App\Entity\Agent;
use App\Entity\Consultation;
use App\Entity\Consultation\ConsultationComptePub;
use App\Entity\Consultation\CritereAttribution;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\Consultation\SousCritereAttribution;
use App\Entity\GeolocalisationN2;
use App\Entity\Lot;
use App\Entity\ValeurReferentiel;
use App\Utils\Publicite\Utils;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class DonneeComplementaireService.
 */
class DonneeComplementaireService
{
    /**
     * DonneeComplementaireService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(
        private readonly ContainerInterface $container,
        private readonly EntityManagerInterface $em,
        private readonly AtexoConfiguration $configService,
        private readonly TranslatorInterface $translator,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Utils $utils
    ) {
    }

    /**
     * @param DonneeComplementaire | null $donneeCompl
     *
     * @return array
     */
    public function getDureeMarche(DonneeComplementaire $donneeCompl = null)
    {
        $duree = [
            'dureeValue'        => null,
            'dureeType'         => null,
            'descriptionLibre'  => null
        ];
        if ($donneeCompl instanceof DonneeComplementaire) {
            $dureeDelaiDescription = $this->em->getRepository(ValeurReferentiel::class)
                ->findOneBy([
                    'idReferentiel' => $this->container->getParameter('REFERENTIEL_DUREE_DELAI'),
                    'id' => $donneeCompl->getIdDureeDelaiDescription(),
                ]);
            if ($dureeDelaiDescription instanceof ValeurReferentiel) {
                switch ($dureeDelaiDescription->getLibelle2()) {
                    case 'DUREE_MARCHEE':
                        $duree['dureeValue'] = $donneeCompl->getDureeMarche();
                        $refUniteDureeMarche = $this->em->getRepository(ValeurReferentiel::class)
                            ->findOneBy([
                                'idReferentiel' => $this->container->getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'),
                                'id' => $donneeCompl->getIdChoixMoisJour(),
                            ]);
                        if ($refUniteDureeMarche instanceof ValeurReferentiel) {
                            $duree['dureeType'] = match ($refUniteDureeMarche->getLibelle2()) {
                                'dureeMois' => 'MONTH',
                                'dureeJours' => 'DAY',
                            };
                        }
                        break;
                    case 'DESCRIPTION_LIBRE':
                        $duree['descriptionLibre'] = $donneeCompl->getDureeDescription();
                        break;
                    default:
                }
            }
        }

        return $duree;
    }

    /**
     * @param DonneeComplementaire | null $donneeCompl
     * @param DonneeComplementaire | null $donneeComplCons
     *
     * @return array
     */
    public function getCriteresAttribution($donneeCompl, $donneeComplCons = null)
    {
        $critere = [];
        if ($donneeCompl instanceof DonneeComplementaire) {
            if (
                $donneeComplCons instanceof DonneeComplementaire
                &&
                '0' !== $donneeComplCons->getCriteresIdentiques()
            ) {
                // cas des critères identique à tous les lots
                $donneeCompl = $donneeComplCons;
            }
            if (0 != $donneeCompl->getIdCritereAttribution()) {
                $oneRefCritere = $this->em->getRepository(ValeurReferentiel::class)
                    ->findOneBy([
                        'idReferentiel' => $this->container->getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'),
                        'id' => $donneeCompl->getIdCritereAttribution(),
                    ]);
                if ($oneRefCritere instanceof ValeurReferentiel) {
                    switch ($oneRefCritere->getLibelle2()) {
                        case 'layerDefinitionCriteres_1':
                            $critere['sansPonderation'] = [];
                            $this->getCriteresDetailles($donneeCompl, $oneRefCritere, $critere['sansPonderation']);
                            break;
                        case 'layerDefinitionCriteres_2':
                            $critere['avecPonderation'] = [];
                            $this->getCriteresDetailles($donneeCompl, $oneRefCritere, $critere['avecPonderation']);
                            break;
                        case 'layerDefinitionVide':
                            $critere['prix'] = true;
                            break;
                        case 'layerDefinitionVide_2':
                            $critere['procurementDoc'] = true;
                            break;
                        case 'layerDefinitionVide_3':
                            $critere['cout'] = true;
                            break;
                        default:
                    }
                }
            }
        }

        return $critere;
    }

    public function getCriteresDetailles(
        DonneeComplementaire $donneeCompl,
        ValeurReferentiel $oneRefCritere,
        array &$critereQ
    ) {
        $criteresAttr = $donneeCompl->getCriteresAttribution();
        if (is_array($criteresAttr) && count($criteresAttr)) {
            $i = 1;
            $index = 0;
            foreach ($criteresAttr as $unCritere) {
                if ($unCritere instanceof CritereAttribution) {
                    $critereQ[$index]['name'] = "C$i " . $unCritere->getEnonce();
                    if (
                        'layerDefinitionCriteres_2' == $oneRefCritere->getLibelle2()
                        &&
                        round($unCritere->getPonderation())
                    ) {
                        $critereQ[$index]['poid'] = $unCritere->getPonderation() . '%';
                    }
                    $sousCriteres = $unCritere->getSousCriteres();
                    if (is_array($sousCriteres) && count($sousCriteres)) {
                        $j = 1;
                        foreach ($sousCriteres as $sousCritere) {
                            if ($sousCritere instanceof SousCritereAttribution) {
                                $critereQ[++$index]['name'] = "C$i.$j " . $sousCritere->getEnonce();
                                if (
                                    'layerDefinitionCriteres_2' == $oneRefCritere->getLibelle2()
                                    &&
                                    round($sousCritere->getPonderation())
                                ) {
                                    $critereQ[$index]['poid'] = $sousCritere->getPonderation() . '%';
                                }
                                ++$j;
                            }
                        }
                    }
                    ++$i;
                    ++$index;
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getPubConsultation(?int $id, ?Agent $agentCreateur)
    {
        $infoComptePub = [];
        $comptePub = $this->em->getRepository(ConsultationComptePub::class)
            ->findOneBy(['idDonneesComplementaires' => $id]);
        if ($comptePub instanceof ConsultationComptePub) {
            $pfUrl = $this->container->get('atexo.atexo_util')
                ->getPfUrlDomaine($this->container->getParameter('PF_URL_REFERENCE'));
            $infoComptePub['organismeMarche'] = [
                'nomOrganisme' => $comptePub->getDenomination(),
                'nom' => $comptePub->getPrm(),
                'adresse' => $comptePub->getAdresse(),
                'cp' => $comptePub->getCp(),
                'email' => $comptePub->getEmail(),
                'telephone' => $this->utils->formatTelephone($comptePub->getTelephone()),
                'ville' => $comptePub->getVille(),
                'url_general' => $comptePub->getUrl(),
                'urlAcheteur' => $pfUrl,
            ];

            if (! is_null($agentCreateur) && '' === $comptePub->getEmail()) {
                $infoComptePub["organismeMarche"]['email'] = $agentCreateur->getEmail();
            }

            if (! is_null($agentCreateur) && '' === $comptePub->getTelephone()) {
                $infoComptePub["organismeMarche"]['telephone'] = $agentCreateur->getNumTel();
            }

            $infoComptePub ['organismeProcedure'] = [
                'nomOrganisme' => $comptePub->getInstanceRecoursOrganisme(),
                'adresse' => $comptePub->getInstanceRecoursAdresse(),
                'cp' => $comptePub->getInstanceRecoursCp(),
                'ville' => $comptePub->getInstanceRecoursVille(),
                'url_general' => $comptePub->getInstanceRecoursUrl(),
            ];

            $infoComptePub['organismeFacture'] = [
                'nomOrganisme' => $comptePub->getFactureDenomination(),
                'adresse' => $comptePub->getFactureAdresse(),
                'cp' => $comptePub->getFactureCp(),
                'email' => $comptePub->getEmail(),
                'telephone' => $this->utils->formatTelephone($comptePub->getTelephone()),
                'ville' => $comptePub->getFactureVille(),
                'url_general' => $comptePub->getUrl(),
            ];

            if (! is_null($agentCreateur) && '' === $comptePub->getEmail()) {
                $infoComptePub["organismeFacture"]['email'] = $agentCreateur->getEmail();
            }

            if (! is_null($agentCreateur) && '' === $comptePub->getTelephone()) {
                $infoComptePub["organismeFacture"]['telephone'] = $agentCreateur->getNumTel();
            }
        }

        return $infoComptePub;
    }

    /**
     * @return string
     */
    public function getAddInfo(Consultation $cons, DonneeComplementaire $donneeCompl = null)
    {
        $infoComp = '';
        $msg = $this->translator->trans(
            'DEFINE_MODALITES_ESSENTIELLES_DE_FINANCEMENT_ET_DE_PAIEMENT_REFERENCE_AUX_TEXTES_QUI_LES_REGLEMENTENT'
        );
        if ($donneeCompl instanceof DonneeComplementaire) {
            if ($donneeCompl->getCautionnementRegimeFinancier()) {
                $infoComp .= $this->translator->trans('DEFINE_CAUTIONNEMENT_ET_GARANTIES_EXIGEES_CAS_ECHEANT')
                    . " : \n" . $donneeCompl->getCautionnementRegimeFinancier();
            }
            if ($donneeCompl->getModalitesFinancementRegimeFinancier()) {
                $infoComp .= "\n" . $msg . " : \n" . $donneeCompl->getModalitesFinancementRegimeFinancier();
            }
        }
        if ($this->configService->hasConfigPlateforme('consultationClause')) {
            $infoComp .= "\n" . $this->translator->trans('CRITERES_SOCIAUX_ENVIRONNEMENTAUX') . "\n";
            $clauseSoc = $this->translator->trans('DEFINE_NON');
            if ($cons->hasThisClause('clauseSociale')) {
                $clauseSoc = $this->translator->trans('DEFINE_OUI');
            }
            $infoComp .= $this->translator->trans('CONSULTATION_INTEGRE_CRITERES_SOCIAUX') . $clauseSoc . "\n";
            $clauseEnv = $this->translator->trans('DEFINE_NON');
            if ($cons->hasThisClause('clauseEnvironnementale')) {
                $clauseEnv = $this->translator->trans('DEFINE_OUI');
            }
            $infoComp .= $this->translator->trans('CONSULTATION_INTEGRE_CRITERES_ENVIRONNEMENTAUX') . $clauseEnv;
        }

        return $infoComp;
    }

    public function hasVariante(Consultation|Lot $objet): bool
    {
        $variante = false;

        if ($objet instanceof Consultation) {
            $organisme = $objet->getAcronymeOrg();
        } else {
            /** @var Lot $objet */
            $organisme = $objet->getOrganisme();
        }
        if (
            $this->configService->hasConfigPlateforme('consultationVariantesAutorisees')
            && $objet->getVariantes()
        ) {
            $variante = true;
        }
        $donnesCompl = $objet->getDonneeComplementaire();
        if (
            (
                $this->configService->hasConfigOrganisme('donneesRedac', $organisme)
                || $this->configService->hasConfigPlateforme('publicite')
            )
            && $donnesCompl instanceof DonneeComplementaire
            && (
                $donnesCompl->getVarianteExigee()
                || $donnesCompl->getVariantesAutorisees()
                || $donnesCompl->getVariantesTechniquesObligatoires()
            )
        ) {
            $variante = true;
        }

        return $variante;
    }

    /**
     * @param DonneeComplementaire | null $donneeCompl
     *
     * @return string
     */
    public function getFormeJuridique($donneeCompl)
    {
        $forme = $this->translator->trans('FORME_JURIDIQUE_GROUPRMENT');
        $valRef = $this->em->getRepository(ValeurReferentiel::class)
            ->findOneBy([
                'idReferentiel' => $this->parameterBag->get('REFERENTIEL_FORME_GROUPEMENT'),
                'id' => $donneeCompl->getIdGroupementAttributaire(),
            ]);
        if ($valRef instanceof ValeurReferentiel) {
            $forme .= $valRef->getLibelleValeurReferentiel();
        }

        return $forme;
    }

    /**
     * @return string
     */
    public function getUrlConsultation(Consultation $consultation)
    {
        $pfUrl = $this->container->get('atexo.atexo_util')
            ->getPfUrlDomaine($this->parameterBag->get('PF_URL_REFERENCE'));

        return $pfUrl .
            '/index.php?page=Entreprise.EntrepriseDetailsConsultation&id=' .
            $consultation->getId() . '&orgAcronyme=' . $consultation->getAcronymeOrg();
    }

    /**
     * @return array
     */
    public function getArrayCodesNuts(Consultation $consultation)
    {
        $nuts = [];
        if (!empty($consultation->getLieuExecution())) {
            $lieux = explode(',', trim($consultation->getLieuExecution(), ','));
            $nuts = $this->em->getRepository(GeolocalisationN2::class)->getCodeNutsLieuxExecution($lieux);
        } elseif (!empty($consultation->getCodesNuts())) {
            $nuts = $consultation->getArrayNuts();
        }

        return $nuts;
    }

    public function getNombreCandidatsAdmis(?DonneeComplementaire $donneeComplementaire): array
    {
        $data = [
            'nombreFixe'    => null,
            'nombreMin'     => null,
            'nombreMax'     => null
        ];
        if ($donneeComplementaire?->getIdNbCandidatsAdmis() == $this->parameterBag->get('ID_NB_CANDIDATS_ADMIS_FIXE')) {
            $data['nombreFixe'] = $donneeComplementaire->getNombreCandidatsFixe();
        } elseif ($donneeComplementaire?->getIdNbCandidatsAdmis() == $this->parameterBag->get('ID_NB_CANDIDATS_ADMIS_FOURCHETTE')) {
            $data['nombreMin'] = $donneeComplementaire->getNombreCandidatsMin();
            $data['nombreMax'] = $donneeComplementaire->getNombreCandidatsMax();
        }

        return $data;
    }

    public function getLabelCCAGReference(?DonneeComplementaire $donneeComplementaire): string
    {
        $valeurReference = $this->em->getRepository(ValeurReferentiel::class)
            ->findOneBy([
                'idReferentiel' => $this->parameterBag->get('REFERENTIEL_CCAG_REFERENCE'),
                'id' => $donneeComplementaire?->getIdCcagReference(),
            ]);

        if ($valeurReference instanceof ValeurReferentiel) {
            return $valeurReference->getLibelleValeurReferentiel();
        }

        return '';
    }

    public function deleteCritereAttribution(?DonneeComplementaire $donneeComplementaire): void
    {
        if (empty($donneeComplementaire)) {
            return;
        }

        $criteresAttribution = $donneeComplementaire->getCriteresAttribution();
        foreach ($criteresAttribution as $value) {
            foreach ($value->getSousCriteresCollection() as $sousCritere) {
                $this->em->remove($sousCritere);
            }
            $this->em->remove($value);
        }
    }
}
