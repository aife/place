<?php

namespace App\Service\Contrat;

use App\Entity\ConsLotContrat;
use Doctrine\ORM\EntityManagerInterface;

class LotContrat
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    /**
     * @return mixed
     */
    public function getByIdConsultation(int $idConsultation): array
    {
        return $this->entityManager->getRepository(ConsLotContrat::class)->getListContratLotByIdConsultation($idConsultation);
    }
}