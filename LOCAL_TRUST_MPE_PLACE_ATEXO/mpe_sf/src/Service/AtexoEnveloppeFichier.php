<?php

namespace App\Service;

use Exception;
use App\Entity\EnveloppeFichier;
use App\Utils\Filesystem\MountManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Classe contenant les fonctions gestion de l'enveloppe fichier.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2016
 */
class AtexoEnveloppeFichier extends AtexoFichierOrganisme
{
    public function __construct(
        ContainerInterface $container,
        EntityManagerInterface $em,
        MountManager $mountManager,
        private ParameterBagInterface $parameterBag
    ) {
        parent::__construct($container, $em, $mountManager, $parameterBag);
    }

    /**
     * inset enveloppe fichier.
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function insertEnveloppeFichier(EnveloppeFichier &$enveloppeFichier, string $fileName = '', string $pathFile = '')
    {
        try {
            $idBlob = $this->insertFile($fileName, $pathFile, $enveloppeFichier->getOrganisme());
            $enveloppeFichier->setIdBlob($idBlob);
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * delete enveloppe fichier.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1
     *
     * @since   develop
     *
     * @copyright Atexo 2016
     */
    public function deleteEnveloppeFichier(int $idFichier)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $fichier = $doctrine->getRepository(EnveloppeFichier::class)->find($idFichier);
            if ($fichier instanceof EnveloppeFichier) {
                $em = $doctrine->getManager();
                $this->deleteBlobFile($fichier->getBlob()->getId(), $fichier->getOrganisme());
                $em->remove($fichier);
                $em->flush();
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * changer l'extention des fichiers.
     *
     * @return void
     *
     * @throws Exception
     */
    public function changeExtensionFichiersOffre(int $idOffre, string $organisme, string $newExtention)
    {
        try {
            $doctrine = $this->getContainer()->get('doctrine');
            $fichiers = $doctrine->getRepository(EnveloppeFichier::class)->getIdBlobFiles($idOffre, $organisme);
            $atexoBlobOrganisme = $this->getContainer()->get(AtexoFichierOrganisme::class);
            if (is_array($fichiers)) {
                foreach ($fichiers as $fichier) {
                    $atexoBlobOrganisme->changeExtensionBlob($fichier, $organisme, $newExtention);
                }
            }
        } catch (Exception $e) {
            throw $e;
        }
    }
}
