<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\ConsultationCertificats;

use App\Entity\Consultation;
use App\Repository\CertificatChiffrementRepository;

class ConsultationCertificatsService
{
    public function __construct(
        protected readonly CertificatChiffrementRepository $certificatChiffrementRepository,
    ) {
    }
    public function getList(Consultation $consultation): array
    {
        $listCertificatesType = $this->certificatChiffrementRepository->findCertificats(
            $consultation->getId(),
            $consultation->getOrganisme()
        );

        $data = [];
        foreach ($listCertificatesType as $key => $lct) {
            $data[$key]['id'] = $lct->getId();
            $data[$key]['indexCertificat'] = $lct->getIndexCertificat();
            $data[$key]['souPli'] = $lct->getSousPli();
        }

        return $data;
    }
}
