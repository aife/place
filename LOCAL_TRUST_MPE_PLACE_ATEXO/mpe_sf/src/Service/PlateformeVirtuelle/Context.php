<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\PlateformeVirtuelle;

use Doctrine\Persistence\ObjectRepository;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Repository\Configuration\PlateformeVirtuelleRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class Context
{
    private readonly ObjectRepository $plateformeVirtuelleRepo;

    private array $plateformesVirtuelles = [];

    /**
     * GraphicChart constructor.
     */
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly EntityManagerInterface $em,
        private readonly LoggerInterface $logger
    ) {
        $this->plateformeVirtuelleRepo = $this->em->getRepository(PlateformeVirtuelle::class);
    }

    /**
     * Retourne le domaine de la requête en cours.
     */
    public function getCurrentDomain(): string
    {
        if (is_null($this->requestStack->getCurrentRequest())) {
            $this->requestStack->push(Request::createFromGlobals());
        }

        return $this->requestStack->getCurrentRequest()->getHost();
    }

    /**
     * Retourne la PlateformeVirtuelle correspondant au domaine de la requête en cours, s'il en existe une.
     */
    public function getCurrentPlateformeVirtuelle(): ?PlateformeVirtuelle
    {
        $domain = $this->getCurrentDomain();
        if (!array_key_exists($domain, $this->plateformesVirtuelles)) {
            $this->plateformesVirtuelles[$domain] =
                $this->plateformeVirtuelleRepo->findOneBy(['domain' => $domain]);
        }

        return $this->plateformesVirtuelles[$domain];
    }

    /**
     * Retourne l'ID de la PlateformeVirtuelle correspondant au domaine de la requête en cours, s'il en existe une.
     */
    public function getCurrentPlateformeVirtuelleId(): ?int
    {
        $plateformeVirtuelle = $this->getCurrentPlateformeVirtuelle();

        return (is_null($plateformeVirtuelle)) ? null : $plateformeVirtuelle->getId();
    }
}
