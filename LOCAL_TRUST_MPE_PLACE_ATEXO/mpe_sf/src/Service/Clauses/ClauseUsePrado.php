<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Clauses;

use App\Service\AbstractService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ClauseUsePrado extends AbstractService
{
    private const URI_CONSULTATION = '/consultations/';
    private const URI_LOT = '/lots/';
    private const URI_CONTRAT = '/contrat-titulaires/';
    public static $token = null;

    public function __construct(
        private readonly HttpClientInterface $httpClient,
        private readonly LoggerInterface $logger,
        private readonly ParameterBagInterface $parameterBag,
        private readonly Security $security,
        private readonly JWTTokenManagerInterface $tokenManager
    ) {
        parent::__construct($httpClient, $logger, $parameterBag);
    }

    protected function sendConsultation(array $data): bool
    {
        try {
            $idConsultation = $data['id_consultation'];
            $url = $this->getMpeWsBaseUrl() . self::URI_CONSULTATION . $idConsultation;
            $this->send($url, $data);
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error(sprintf(
                "L'ajout des clauses %s à la consultation id: %s à échoué avec le message d'erreur: %s",
                json_encode($data['values']),
                $idConsultation,
                $exception->getMessage()
            ));
            return false;
        }

        return true;
    }

    protected function sendLot(array $data): bool
    {
        try {
            $idLot = $data['id_lot'];
            $url = $this->getMpeWsBaseUrl() . self::URI_LOT . $data['id_lot'];
            $this->send($url, $data);
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error(sprintf(
                "L'ajout des clauses %s au lot id: %s à échoué avec le message d'erreur: %s",
                json_encode($data['values']),
                $idLot,
                $exception->getMessage()
            ));
            return false;
        }

        return true;
    }

    protected function sendContrat(array $data): bool
    {
        try {
            $idContrat = $data['id_contrat'];
            $url = $this->getMpeWsBaseUrl() . self::URI_CONTRAT . $idContrat;
            $this->send($url, $data);
        } catch (ClientExceptionInterface $exception) {
            $this->logger->error(sprintf(
                "L'ajout des clauses %s au contrat id: %s à échoué avec le message d'erreur: %s",
                json_encode($data['values']),
                $idContrat,
                $exception->getMessage()
            ));
            return false;
        }

        return true;
    }

    private function send(string $url, array $data): void
    {
        $options = [
            'headers' => [
                'Content-Type' => self::CONTENT_TYPE_PATCH,
                'Authorization' => 'Bearer ' . $this->getToken(),
            ],
            'json' => ['clauses' =>  $data['values'] ?? []]
        ];

        $this->request(Request::METHOD_PATCH, $url, $options)->getContent();
    }

    private function getToken(): string
    {
        return self::$token ?? $this->tokenManager->create($this->security->getUser());
    }

    private function getMpeWsBaseUrl(): string
    {
        return $this->parameterBag->get('PF_URL_REFERENCE') . 'api/v2';
    }
}
