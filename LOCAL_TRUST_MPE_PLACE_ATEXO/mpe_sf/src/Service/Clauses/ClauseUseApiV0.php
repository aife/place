<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Service\Clauses;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Referentiel\Consultation\ClausesN2 as ReferentielClausesN2;
use App\Entity\Referentiel\Consultation\ClausesN3 as ReferentielClausesN3;
use App\Entity\Referentiel\Consultation\ClausesN1 as ReferentielClausesN1;
use App\Entity\Consultation\ClausesN1;
use App\Entity\Consultation\ClausesN2;
use App\Entity\Consultation\ClausesN3;
use App\Repository\Consultation\ClausesN1Repository as ClausesRepository;
use App\Repository\ConsultationRepository;
use App\Repository\Referentiel\Consultation\ClausesN1Repository;
use App\Repository\Referentiel\Consultation\ClausesN2Repository;
use App\Repository\Referentiel\Consultation\ClausesN3Repository;
use App\Service\AgentTechniqueTokenService;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;

class ClauseUseApiV0
{
    public function __construct(
        private ClausesN1Repository $clausesN1Repository,
        private ClausesN2Repository $clausesN2Repository,
        private ClausesN3Repository $clausesN3Repository,
        private ClausesRepository $clausesRepository,
        private IriConverterInterface $iriConverter,
        private JWTTokenManagerInterface $tokenManager,
        private AgentTechniqueTokenService $agentTechniqueTokenService,
        private ClauseUsePrado $clauseUsePrado,
        private LoggerInterface $logger,
        private ConsultationRepository $consultationRepository
    ) {
    }

    public function saveClausesConsultation(SimpleXMLElement $xmlElements, int $idConsultation, string $ticket): mixed
    {
        ClauseUsePrado::$token = $this->getToken($ticket);

        $values = $this->transform($xmlElements);
        $values = array_filter($values, static function ($element) {
            return !empty($element);
        });
        if (empty($values)) {
            return false;
        }
        $data = [
            'values' => $values,
            'id_consultation' => $idConsultation
        ];

        return $this->clauseUsePrado->getContent('sendConsultation', $data);
    }

    public function addClausesConsultationInXMLElement(SimpleXMLElement $xmlElements, int $idConsultation): string
    {
        $consultation = $this->consultationRepository->find($idConsultation);
        if ($consultation->isAlloti()) {
            foreach ($xmlElements->reponse->consultation->lots->lot as $lotXml) {
                $clausesN1 = $consultation->getLots()->filter(function ($lot) use ($lotXml) {
                    return $lot->getLot() === (int) $lotXml->numero;
                })->first()->getClausesN1();
                foreach ($clausesN1 as $clauseN1) {
                    $this->addClauseN1XmlElements($lotXml, $clauseN1);
                }
            }
        } else {
            $clausesConsultation = $this->clausesRepository->findByIdConsultation($idConsultation);
            if (is_countable($clausesConsultation) && count($clausesConsultation) > 0) {
                foreach ($clausesConsultation as $clauseN1) {
                    $this->addClauseN1XmlElements($xmlElements->reponse->consultation, $clauseN1);
                }
            }
        }

        return $xmlElements->asXML();
    }

    private function addClauseN1XmlElements(SimpleXMLElement $xmlElements, ClausesN1 $clausesN1): SimpleXMLElement
    {
        $slug = ($clausesN1->getReferentielClauseN1()->getSlug() === ReferentielClausesN1::CLAUSES_ENVIRONNEMENTALES)
            ? 'clausesEnvironnementales' : $clausesN1->getReferentielClauseN1()->getSlug();

        $method = ($clausesN1->getReferentielClauseN1()->getSlug() === ReferentielClausesN1::CLAUSES_ENVIRONNEMENTALES)
            ? 'addClauseEnvN2XmlElements' : 'addClauseN2XmlElements';
        $clauseN1XmlElements = $xmlElements->addChild($slug);
        foreach ($clausesN1->getClausesN2() as $clausesN2) {
            $this->$method($clauseN1XmlElements, $clausesN2);
        }

        return  $xmlElements;
    }

    private function addClauseN2XmlElements(SimpleXMLElement $xmlElements, ClausesN2 $clausesN2): SimpleXMLElement
    {
        $clauseN2XmlElements = $xmlElements->addChild($clausesN2->getReferentielClauseN2()->getSlug());
        foreach ($clausesN2->getClausesN3() as $clausesN3) {
            $this->addClauseN3XmlElements($clauseN2XmlElements, $clausesN3);
        }

        return $xmlElements;
    }
    private function addClauseEnvN2XmlElements(SimpleXMLElement $xmlElements, ClausesN2 $clausesN2): void
    {
        $xmlElements->addChild($clausesN2->getReferentielClauseN2()->getSlug(), "true");
    }

    private function addClauseN3XmlElements(SimpleXMLElement $xmlElements, ClausesN3 $clausesN3): SimpleXMLElement
    {
        return $xmlElements->addChild("critere", $clausesN3->getReferentielClauseN3()->getSlug());
    }

    private function transform(SimpleXMLElement $xmlElements): array
    {
        return [$this->addClausesSociales($xmlElements), $this->addClausesEnvironnementales($xmlElements)];
    }

    private function addClausesSociales(SimpleXMLElement $xmlElements): array
    {
        $data = [];

        if ($xmlElements->envoi?->consultation?->clausesSociales) {
            $data = $this->addClauseN1(
                $xmlElements->envoi->consultation->clausesSociales,
                ClausesN1::CLAUSE_SOCIALES
            );
        }

        return $data;
    }

    private function addClausesEnvironnementales(SimpleXMLElement $xmlElements): array
    {
        $data = [];

        if ($xmlElements->envoi?->consultation?->clausesEnvironnementales) {
            $data = $this->addClauseN1(
                $xmlElements->envoi->consultation->clausesEnvironnementales,
                ClausesN1::CLAUSES_ENVIRONNEMENTALES
            );
        }

        return $data;
    }

    private function addClauseN1(SimpleXMLElement $clauseN1, string $slug): array
    {
        return [
            'referentielClauseN1' => $this->getIriFromItem($this->clausesN1Repository->findOneBy(compact('slug'))),
            'clausesN2' => $this->addClauseN2($clauseN1)
        ];
    }

    private function getToken(string $ticket): string
    {
        $user = $this->getUser($ticket);

        return is_bool($user) ? '' : $this->tokenManager->create($user);
    }

    private function getUser(string $ticket): mixed
    {
        return $this->agentTechniqueTokenService->getAgentFromToken($ticket);
    }

    private function addClauseN2(SimpleXMLElement $xmlClauseN1): array
    {
        $data = [];
        foreach ($xmlClauseN1 as $clauseN2) {
            foreach ($clauseN2 as $slugClauseN2 => $clauseN3) {
                $referentielClauseN2 =  $this->clausesN2Repository->findOneBy(['slug' => $slugClauseN2]);
                if ($referentielClauseN2) {
                    $data[] = [
                        'referentielClauseN2' => $this->getIriFromItem($referentielClauseN2),
                         'clausesN3' => $this->addClauseN3($clauseN2->{$slugClauseN2})
                    ];
                }
            }
        }
        return $data;
    }

    private function addClauseN3(SimpleXMLElement $xmlClauseN2): array
    {
        $data = [];
        foreach ($this->xmlToArray($xmlClauseN2->critere) as $slug) {
            $referentielClauseN3 =  $this->clausesN3Repository->findOneBy(compact('slug'));
            if ($referentielClauseN3) {
                $data[] = ['referentielClauseN3' => $this->getIriFromItem($referentielClauseN3)];
            }
        }
        return $data;
    }

    private function xmlToArray(SimpleXMLElement $xml): array
    {
        return json_decode(json_encode((array) $xml), true);
    }

    private function getIriFromItem(ReferentielClausesN1|ReferentielClausesN2|ReferentielClausesN3 $clause): string
    {
        return str_replace('api.php/', '', $this->iriConverter->getIriFromItem($clause));
    }
}
