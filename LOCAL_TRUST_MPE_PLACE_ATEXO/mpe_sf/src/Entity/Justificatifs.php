<?php

namespace App\Entity;

use DateTimeInterface;
use App\Repository\JustificatifsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Justificatifs")
 * @ORM\Entity(repositoryClass=JustificatifsRepository::class)
 */
class Justificatifs
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $intituleJustificatif;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idEntreprise;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private ?string $taille;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $justificatif;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private ?string $statut;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $idDocument;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $nom_fr;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_en;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_es;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_su;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_du;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_cz;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_ar;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $nom_it;

    /**
     * @ORM\Column(type="date")
     */
    private ?DateTimeInterface $dateFinValidite;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private ?string $visibleParAgents;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIntituleJustificatif(): ?string
    {
        return $this->intituleJustificatif;
    }

    public function setIntituleJustificatif(string $intituleJustificatif): self
    {
        $this->intituleJustificatif = $intituleJustificatif;

        return $this;
    }

    public function getIdEntreprise(): int
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise(?int $idEntreprise): self
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    public function getTaille(): ?string
    {
        return $this->taille;
    }

    public function setTaille(string $taille): self
    {
        $this->taille = $taille;

        return $this;
    }

    public function getJustificatif(): ?int
    {
        return $this->justificatif;
    }

    public function setJustificatif(int $justificatif): self
    {
        $this->justificatif = $justificatif;

        return $this;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getIdDocument(): ?int
    {
        return $this->idDocument;
    }

    public function setIdDocument(?int $idDocument): self
    {
        $this->idDocument = $idDocument;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNomFr(): ?string
    {
        return $this->nom_fr;
    }

    public function setNomFr(string $nom_fr): self
    {
        $this->nom_fr = $nom_fr;

        return $this;
    }

    public function getNomEn(): ?string
    {
        return $this->nom_en;
    }

    public function setNomEn(?string $nom_en): self
    {
        $this->nom_en = $nom_en;

        return $this;
    }

    public function getNomEs(): ?string
    {
        return $this->nom_es;
    }

    public function setNomEs(?string $nom_es): self
    {
        $this->nom_es = $nom_es;

        return $this;
    }

    public function getNomSu(): ?string
    {
        return $this->nom_su;
    }

    public function setNomSu(?string $nom_su): self
    {
        $this->nom_su = $nom_su;

        return $this;
    }

    public function getNomDu(): ?string
    {
        return $this->nom_du;
    }

    public function setNomDu(?string $nom_du): self
    {
        $this->nom_du = $nom_du;

        return $this;
    }

    public function getNomCz(): ?string
    {
        return $this->nom_cz;
    }

    public function setNomCz(?string $nom_cz): self
    {
        $this->nom_cz = $nom_cz;

        return $this;
    }

    public function getNomAr(): ?string
    {
        return $this->nom_ar;
    }

    public function setNomAr(?string $nom_ar): self
    {
        $this->nom_ar = $nom_ar;

        return $this;
    }

    public function getNomIt(): ?string
    {
        return $this->nom_it;
    }

    public function setNomIt(?string $nom_it): self
    {
        $this->nom_it = $nom_it;

        return $this;
    }

    public function getDateFinValidite(): ?DateTimeInterface
    {
        return $this->dateFinValidite;
    }

    public function setDateFinValidite(DateTimeInterface $dateFinValidite): self
    {
        $this->dateFinValidite = $dateFinValidite;

        return $this;
    }

    public function getVisibleParAgents(): ?string
    {
        return $this->visibleParAgents;
    }

    public function setVisibleParAgents(?string $visibleParAgents): self
    {
        $this->visibleParAgents = $visibleParAgents;

        return $this;
    }
}
