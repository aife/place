<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TNumeroReponse.
 *
 * @ORM\Table(name="t_numero_reponse" )
 * @ORM\Entity(repositoryClass="App\Repository\TNumeroReponseRepository")
 */
class TNumeroReponse
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="numero_reponse", type="integer")
     */
    private ?int $numeroReponse = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     */
    private ?int $consultationId = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        return $this->id = $id;
    }

    /**
     * @return int
     */
    public function getNumeroReponse()
    {
        return $this->numeroReponse;
    }

    /**
     * @param int $numeroReponse
     */
    public function setNumeroReponse($numeroReponse)
    {
        $this->numeroReponse = $numeroReponse;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set consultationId.
     *
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * Get consultationId.
     *
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }
}
