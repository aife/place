<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TMembreGroupementEntreprise.
 *
 * @ORM\Table(name="t_membre_groupement_entreprise", indexes={@ORM\Index(name="fk_Membre_Groupement_Entreprise_Role_Juridique", columns={"id_role_juridique"}), @ORM\Index(name="fk_Membre_Groupement_Entreprise_Groupement", columns={"id_groupement_entreprise"}), @ORM\Index(name="fk_Membre_Groupement_Entreprise_Perent", columns={"id_membre_parent"})})
 * @ORM\Entity(repositoryClass="App\Repository\TMembreGroupementEntrepriseRepository")
 */
class TMembreGroupementEntreprise
{
    /**
     *
     * @ORM\Column(name="id_membre_groupement_entreprise", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idMembreGroupementEntreprise;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_etablissement", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private ?int $idEtablissement = null;

    /**
     * @ORM\Column(name="id_groupement_entreprise", type="integer", precision=0, scale=0, nullable=true, unique=false)
     */
    private ?int $idGroupementEntreprise = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TMembreGroupementEntreprise", inversedBy="sousMembres")
     * @ORM\JoinColumn(name="id_membre_parent", referencedColumnName="id_membre_groupement_entreprise", nullable=true)
     */
    private ?\App\Entity\TMembreGroupementEntreprise $idMembreParent = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TRoleJuridique")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_role_juridique", referencedColumnName="id_role_juridique", nullable=true)
     * })
     */
    private ?TRoleJuridique $idRoleJuridique = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TGroupementEntreprise", inversedBy="membresGroupement")
     * @ORM\JoinColumn(name="id_groupement_entreprise", referencedColumnName="id_groupement_entreprise")
     */
    private ?TGroupementEntreprise $groupement = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TMembreGroupementEntreprise", mappedBy="idMembreParent",cascade={"persist"})
     * @ORM\JoinColumn(name="id_membre_groupement_entreprise", referencedColumnName="id_membre_parent", nullable=true)
     */
    private Collection $sousMembres;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise")
     * @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id", nullable=true)
     */
    private ?Entreprise $entreprise = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Etablissement")
     * @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement", nullable=true)
     */
    private ?Etablissement $etablissement = null;

    /**
     * @ORM\Column(name="numeroSN", type="string", nullable=true)
     */
    private ?string $numeroSN = null;

    /**
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private ?string $email = null;

    /**
     * TGroupementEntreprise constructor.
     */
    public function __construct()
    {
        $this->sousMembres = new ArrayCollection();
    }

    /**
     * @return TGroupementEntreprise
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    public function setGroupement(TGroupementEntreprise $groupement)
    {
        $this->groupement = $groupement;
    }

    /**
     * Get idMembreGroupementEntreprise.
     *
     * @return int
     */
    public function getIdMembreGroupementEntreprise()
    {
        return $this->idMembreGroupementEntreprise;
    }

    /**
     * Set idEntreprise.
     *
     * @param int $idEntreprise
     *
     * @return TMembreGroupementEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * Get idEntreprise.
     *
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Set idEtablissement.
     *
     * @param int $idEtablissement
     *
     * @return TMembreGroupementEntreprise
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Set idGroupementEntreprise.
     *
     * @param int $idGroupementEntreprise
     *
     * @return TMembreGroupementEntreprise
     */
    public function setIdGroupementEntreprise($idGroupementEntreprise)
    {
        $this->idGroupementEntreprise = $idGroupementEntreprise;

        return $this;
    }

    /**
     * Get idGroupementEntreprise.
     *
     * @return int
     */
    public function getIdGroupementEntreprise()
    {
        return $this->idGroupementEntreprise;
    }

    /**
     * Set idMembreParent.
     *
     *
     * @return TMembreGroupementEntreprise
     */
    public function setIdMembreParent(TMembreGroupementEntreprise $idMembreParent = null)
    {
        $this->idMembreParent = $idMembreParent;

        return $this;
    }

    /**
     * Get idMembreParent.
     *
     * @return \App\Entity\TMembreGroupementEntreprise
     */
    public function getIdMembreParent()
    {
        return $this->idMembreParent;
    }

    /**
     * Set idRoleJuridique.
     *
     *
     * @return TMembreGroupementEntreprise
     */
    public function setIdRoleJuridique(TRoleJuridique $idRoleJuridique = null)
    {
        $this->idRoleJuridique = $idRoleJuridique;

        return $this;
    }

    /**
     * Get idRoleJuridique.
     *
     * @return TRoleJuridique
     */
    public function getIdRoleJuridique()
    {
        return $this->idRoleJuridique;
    }

    /**
     * ajoute un membre.
     *
     * @return $this
     */
    public function addSousMembres(TMembreGroupementEntreprise $sousMembres)
    {
        $this->sousMembres[$sousMembres->getIdEntreprise()] = $sousMembres;

        return $this;
    }

    /**
     * supprime un membre.
     *
     * @return $this
     */
    public function removeSousMembres(TMembreGroupementEntreprise $sousMembres)
    {
        $this->sousMembres->removeElement($sousMembres);

        return $this;
    }

    /**
     * trouver un membre.
     *
     * @param int $idEntreprise
     *
     * @return bool
     */
    public function hasMembre($idEntreprise)
    {
        return $this->sousMembres->containsKey($idEntreprise);
    }

    /**
     * supprime un membre by key.
     *
     * @param int $idEntreprise
     *
     * @return $this
     */
    public function removeSousMembresByKey($idEntreprise)
    {
        $this->sousMembres->remove($idEntreprise);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSousMembres()
    {
        return $this->sousMembres;
    }

    /**
     * @param ArrayCollection $sousMembres
     */
    public function setSousMembres($sousMembres)
    {
        $this->sousMembres = $sousMembres;
    }

    /**
     * trouver un membre.
     *
     * @param int $idEntreprise
     *
     * @return TMembreGroupementEntreprise
     */
    public function getSousMembreByKey($idEntreprise)
    {
        return $this->sousMembres[$idEntreprise];
    }

    /**
     * @return Entreprise
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setEntreprise(Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return Etablissement
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    public function setEtablissement(Etablissement $etablissement = null)
    {
        $this->etablissement = $etablissement;
    }

    /**
     * retoune info etablissement.
     *
     * @return string
     */
    public function getAdresse()
    {
        $etab = $this->getEtablissement();

        return ($etab instanceof Etablissement) ? $etab->getCodePostal().' '.$etab->getVille() : '';
    }

    /**
     * @return string
     */
    public function getNumeroSN()
    {
        return $this->numeroSN;
    }

    /**
     * @param string $numeroSN
     *
     * @return $this
     */
    public function setNumeroSN($numeroSN)
    {
        $this->numeroSN = $numeroSN;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }
}
