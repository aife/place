<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * InscritOperationsTrackerDetails.
 *
 * @ORM\Table(name="T_trace_operations_inscrit_details", indexes={@ORM\Index(name="Fk_Trace", columns={"id_trace"})})
 * @ORM\Entity
 */
class InscritOperationsTrackerDetails
{
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\InscritOperationsTracker", inversedBy="detailsTracesOperations", fetch="EAGER")
     * @ORM\JoinColumn(name="id_trace", referencedColumnName="id_trace")
     */
    private ?InscritOperationsTracker $traceOperation = null;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="id_trace", type="integer", nullable=false)
     */
    private ?int $idTrace = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_debut_action", type="datetime", nullable=true)*/
    private $dateDebutAction;

    /**
     * @ORM\Column(name="nom_action", type="string", length=100, nullable=true)
     */
    private ?string $nomAction = null;

    /**
     * @ORM\Column(name="details", type="string", length=50, nullable=true)
     */
    private ?string $details = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_fin_action", type="datetime", nullable=true)*/
    private $dateFinAction;

    /**
     * @ORM\Column(name="id_description", type="integer", nullable=true)
     */
    private $idDescription = null;

    /**
     * @ORM\Column(name="afficher", type="string", nullable=false)
     */
    private string $afficher = '0';

    /**
     * @ORM\Column(name="descripton", type="text", length=65535, nullable=false)
     */
    private ?string $descripton = null;

    /**
     * @ORM\Column(name="log_applet", type="text", length=65535, nullable=false)
     */
    private ?string $logApplet = null;

    /**
     * @ORM\Column(name="lien_download", type="string", length=250, nullable=true)
     */
    private ?string $lienDownload = null;

    /**
     * @ORM\Column(name="infos_browser", type="string", nullable=true)
     */
    private ?string $infosBrowser = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_debut_action_client", type="datetime", nullable=true)*/
    private $dateDebutActionClient;

    /**
     * @ORM\Column(name="id_offre", type="integer", nullable=false)
     */
    private ?int $idOffre = null;

    /**
     * @var float
     *
     * @ORM\Column(name="debut_action_millisecond", type="bigint", length=20, nullable=true)
     */
    private $debutActionMillisecond;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idTrace.
     *
     * @param int $idTrace
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setIdTrace($idTrace)
    {
        $this->idTrace = $idTrace;

        return $this;
    }

    /**
     * Get idTrace.
     *
     * @return int
     */
    public function getIdTrace()
    {
        return $this->idTrace;
    }

    /**
     * Set dateDebutAction.
     *
     * @param DateTime $dateDebutAction
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setDateDebutAction($dateDebutAction)
    {
        $this->dateDebutAction = $dateDebutAction;

        return $this;
    }

    /**
     * Get dateDebutAction.
     *
     * @return DateTime
     */
    public function getDateDebutAction()
    {
        return $this->dateDebutAction;
    }

    /**
     * Set nomAction.
     *
     * @param string $nomAction
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setNomAction($nomAction)
    {
        $this->nomAction = $nomAction;

        return $this;
    }

    /**
     * Get nomAction.
     *
     * @return string
     */
    public function getNomAction()
    {
        return $this->nomAction;
    }

    /**
     * Set details.
     *
     * @param string $details
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details.
     *
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set dateFinAction.
     *
     * @param DateTime $dateFinAction
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setDateFinAction($dateFinAction)
    {
        $this->dateFinAction = $dateFinAction;

        return $this;
    }

    /**
     * Get dateFinAction.
     *
     * @return DateTime
     */
    public function getDateFinAction()
    {
        return $this->dateFinAction;
    }

    /**
     * Set idDescription.
     *
     * @param int $idDescription
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setIdDescription($idDescription)
    {
        $this->idDescription = $idDescription;

        return $this;
    }

    /**
     * Get idDescription.
     *
     * @return int
     */
    public function getIdDescription()
    {
        return $this->idDescription;
    }

    /**
     * Set afficher.
     *
     * @param string $afficher
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setAfficher($afficher)
    {
        $this->afficher = $afficher;

        return $this;
    }

    /**
     * Get afficher.
     *
     * @return string
     */
    public function getAfficher()
    {
        return $this->afficher;
    }

    /**
     * Set descripton.
     *
     * @param string $descripton
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setDescripton($descripton)
    {
        $this->descripton = $descripton;

        return $this;
    }

    /**
     * Get descripton.
     *
     * @return string
     */
    public function getDescripton()
    {
        return $this->descripton;
    }

    /**
     * Set logApplet.
     *
     * @param string $logApplet
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setLogApplet($logApplet)
    {
        $this->logApplet = $logApplet;

        return $this;
    }

    /**
     * Get logApplet.
     *
     * @return string
     */
    public function getLogApplet()
    {
        return $this->logApplet;
    }

    /**
     * Set lienDownload.
     *
     * @param string $lienDownload
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setLienDownload($lienDownload)
    {
        $this->lienDownload = $lienDownload;

        return $this;
    }

    /**
     * Get lienDownload.
     *
     * @return string
     */
    public function getLienDownload()
    {
        return $this->lienDownload;
    }

    /**
     * Set infosBrowser.
     *
     * @param string $infosBrowser
     *
     * @return TTraceOperationsInscritDetails
     */
    public function setInfosBrowser($infosBrowser)
    {
        $this->infosBrowser = $infosBrowser;

        return $this;
    }

    /**
     * Get infosBrowser.
     *
     * @return string
     */
    public function getInfosBrowser()
    {
        return $this->infosBrowser;
    }

    public function getTraceOperation(): InscritOperationsTracker
    {
        return $this->traceOperation;
    }

    /**
     * @param $traceOperation
     */
    public function setTraceOperation(InscritOperationsTracker $traceOperation)
    {
        $this->traceOperation = $traceOperation;
    }

    /**
     * @return DateTime
     */
    public function getDateDebutActionClient()
    {
        return $this->dateDebutActionClient;
    }

    /**
     * @param DateTime $dateDebutActionClient
     */
    public function setDateDebutActionClient($dateDebutActionClient)
    {
        $this->dateDebutActionClient = $dateDebutActionClient;
    }

    /**
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param int $idOffre
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;
    }

    public function getDebutActionMillisecond(): float
    {
        return $this->debutActionMillisecond;
    }

    public function setDebutActionMillisecond(float $debutActionMillisecond)
    {
        $this->debutActionMillisecond = $debutActionMillisecond;
    }
}
