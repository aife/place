<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * ChampsFusionRedac
 *
 * @ORM\Table(name="champs_fusion_redac")
 * @ORM\Entity
 */
#[ApiResource(
    collectionOperations: [
        "get",
    ],
    itemOperations: [
        "get",
    ],
    denormalizationContext: [
        'groups' => ['champsFusionRedacWrite'],
        AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true
    ],
    normalizationContext: [
        AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true
    ],
)]
#[ApiFilter(
    BooleanFilter::class,
    properties: [
        'actif' => 'exact'
    ]
)]
class ChampsFusionRedac
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="libelle", type="string", length=100, nullable=false)
     */
    #[Groups('champsFusionRedacWrite')]
    private string $libelle;

    /**
     * @ORM\Column(name="champ_fusion", type="string", length=255, nullable=false)
     */
    #[Groups('champsFusionRedacWrite')]
    private string $champFusion;

    /**
     * @ORM\Column(name="type_fusion", type="string", length=255, nullable=false)
     */
    #[Groups('champsFusionRedacWrite')]
    private string $typeFusion;

    /**
     * @ORM\Column(name="actif", type="boolean", nullable=false, options={"default"="1"})
     */
    #[Groups('champsFusionRedacWrite')]
    private bool $actif = true;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private \DateTimeInterface $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $updatedAt;

    public function getId(): int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getChampFusion(): ?string
    {
        return $this->champFusion;
    }

    public function setChampFusion(string $champFusion): self
    {
        $this->champFusion = $champFusion;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getTypeFusion(): string
    {
        return $this->typeFusion;
    }

    public function setTypeFusion(string $typeFusion): self
    {
        $this->typeFusion = $typeFusion;

        return $this;
    }
}
