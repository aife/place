<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\ProcedureEquivalenceDumeOutput;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProcedureEquivalenceDume
 *
 * @ORM\Table(name="t_procedure_equivalence_dume")
 * @ORM\Entity(repositoryClass="App\Repository\Procedure\ProcedureEquivalenceDumeRepository")
 */
#[ApiResource(
    shortName: 'parameters-procedures-dume',
    itemOperations: ['get'],
    collectionOperations: ['get'],
    output: ProcedureEquivalenceDumeOutput::class
)]
#[ApiFilter(
    SearchFilter::class,
    properties: ['idTypeProcedure' => 'exact']
)]
class ProcedureEquivalenceDume
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_procedure", type="integer", nullable=false)
     */
    private $idTypeProcedure = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=50, nullable=false)
     */
    private $organisme = '';

    /**
     * @var int
     *
     * @ORM\Column(name="id_type_procedure_dume", type="integer", nullable=false)
     */
    private $idTypeProcedureDume = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="afficher", type="string", length=0, nullable=false)
     */
    private $afficher = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="figer", type="string", length=0, nullable=false)
     */
    private $figer = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="selectionner", type="string", length=0, nullable=false)
     */
    private $selectionner = '0';

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedure(): int|string
    {
        return $this->idTypeProcedure;
    }

    /**
     * @param int $idTypeProcedure
     */
    public function setIdTypeProcedure(int|string $idTypeProcedure): self
    {
        $this->idTypeProcedure = $idTypeProcedure;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedureDume(): int|string
    {
        return $this->idTypeProcedureDume;
    }

    /**
     * @param int $idTypeProcedureDume
     */
    public function setIdTypeProcedureDume(int|string $idTypeProcedureDume): self
    {
        $this->idTypeProcedureDume = $idTypeProcedureDume;
        return $this;
    }

    /**
     * @return string
     */
    public function getAfficher(): string
    {
        return $this->afficher;
    }

    /**
     * @param string $afficher
     */
    public function setAfficher(string $afficher): self
    {
        $this->afficher = $afficher;
        return $this;
    }

    /**
     * @return string
     */
    public function getFiger(): string
    {
        return $this->figer;
    }

    /**
     * @param string $figer
     */
    public function setFiger(string $figer): self
    {
        $this->figer = $figer;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelectionner(): string
    {
        return $this->selectionner;
    }

    /**
     * @param string $selectionner
     */
    public function setSelectionner(string $selectionner): self
    {
        $this->selectionner = $selectionner;
        return $this;
    }
}
