<?php

namespace App\Entity;

use Stringable;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\CategorieConsultationOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CategorieConsultation.
 *
 * @ORM\Table(name="CategorieConsultation")
 * @ORM\Entity
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    shortName: 'NaturePrestation',
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: CategorieConsultationOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idExterne' => 'exact',
    ]
)]
class CategorieConsultation implements Stringable
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="categorieConsultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="categorie")
     */
    private Collection $consultations;

    /**
     *
     * @ORM\Column(name="id", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private string $id = '';

    /**
     * @ORM\Column(name="libelle", type="string", length=100, nullable=false)
     */
    private string $libelle = '';

    /**
     * @ORM\Column(name="libelle_fr", type="string", length=100, nullable=true)
     */
    private ?string $libelleFr = '';

    /**
     * @ORM\Column(name="libelle_en", type="string", length=100, nullable=true)
     */
    private ?string $libelleEn = '';

    /**
     * @ORM\Column(name="libelle_es", type="string", length=100, nullable=true)
     */
    private ?string $libelleEs = '';

    /**
     * @ORM\Column(name="libelle_su", type="string", length=100, nullable=true)
     */
    private ?string $libelleSu = '';

    /**
     * @ORM\Column(name="libelle_du", type="string", length=100, nullable=true)
     */
    private ?string $libelleDu = '';

    /**
     * @ORM\Column(name="libelle_cz", type="string", length=100, nullable=true)
     */
    private ?string $libelleCz = '';

    /**
     * @ORM\Column(name="libelle_ar", type="string", length=100, nullable=true)
     */
    private ?string $libelleAr = null;

    /**
     * @ORM\Column(name="id_categorie_ANM", type="string", length=11, nullable=false)
     */
    private string $idCategorieAnm = '0';

    /**
     * @ORM\Column(name="libelle_it", type="string", length=100, nullable=true)
     */
    private ?string $libelleIt = null;

    /**
     * @ORM\Column(name="code", type="string", length=10, nullable=true)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="id_externe", type="string", length=10, nullable=false)
     */
    private string $idExterne;

    public function __toString(): string
    {
        return $this->libelle;
    }

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return CategorieConsultation
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set libelleFr.
     *
     * @param string $libelleFr
     *
     * @return CategorieConsultation
     */
    public function setLibelleFr($libelleFr)
    {
        $this->libelleFr = $libelleFr;

        return $this;
    }

    /**
     * Get libelleFr.
     *
     * @return string
     */
    public function getLibelleFr()
    {
        return $this->libelleFr;
    }

    /**
     * Set libelleEn.
     *
     * @param string $libelleEn
     *
     * @return CategorieConsultation
     */
    public function setLibelleEn($libelleEn)
    {
        $this->libelleEn = $libelleEn;

        return $this;
    }

    /**
     * Get libelleEn.
     *
     * @return string
     */
    public function getLibelleEn()
    {
        return $this->libelleEn;
    }

    /**
     * Set libelleEs.
     *
     * @param string $libelleEs
     *
     * @return CategorieConsultation
     */
    public function setLibelleEs($libelleEs)
    {
        $this->libelleEs = $libelleEs;

        return $this;
    }

    /**
     * Get libelleEs.
     *
     * @return string
     */
    public function getLibelleEs()
    {
        return $this->libelleEs;
    }

    /**
     * Set libelleSu.
     *
     * @param string $libelleSu
     *
     * @return CategorieConsultation
     */
    public function setLibelleSu($libelleSu)
    {
        $this->libelleSu = $libelleSu;

        return $this;
    }

    /**
     * Get libelleSu.
     *
     * @return string
     */
    public function getLibelleSu()
    {
        return $this->libelleSu;
    }

    /**
     * Set libelleDu.
     *
     * @param string $libelleDu
     *
     * @return CategorieConsultation
     */
    public function setLibelleDu($libelleDu)
    {
        $this->libelleDu = $libelleDu;

        return $this;
    }

    /**
     * Get libelleDu.
     *
     * @return string
     */
    public function getLibelleDu()
    {
        return $this->libelleDu;
    }

    /**
     * Set libelleCz.
     *
     * @param string $libelleCz
     *
     * @return CategorieConsultation
     */
    public function setLibelleCz($libelleCz)
    {
        $this->libelleCz = $libelleCz;

        return $this;
    }

    /**
     * Get libelleCz.
     *
     * @return string
     */
    public function getLibelleCz()
    {
        return $this->libelleCz;
    }

    /**
     * Set libelleAr.
     *
     * @param string $libelleAr
     *
     * @return CategorieConsultation
     */
    public function setLibelleAr($libelleAr)
    {
        $this->libelleAr = $libelleAr;

        return $this;
    }

    /**
     * Get libelleAr.
     *
     * @return string
     */
    public function getLibelleAr()
    {
        return $this->libelleAr;
    }

    /**
     * Set idCategorieAnm.
     *
     * @param string $idCategorieAnm
     *
     * @return CategorieConsultation
     */
    public function setIdCategorieAnm($idCategorieAnm)
    {
        $this->idCategorieAnm = $idCategorieAnm;

        return $this;
    }

    /**
     * Get idCategorieAnm.
     *
     * @return string
     */
    public function getIdCategorieAnm()
    {
        return $this->idCategorieAnm;
    }

    /**
     * Set libelleIt.
     *
     * @param string $libelleIt
     *
     * @return CategorieConsultation
     */
    public function setLibelleIt($libelleIt)
    {
        $this->libelleIt = $libelleIt;

        return $this;
    }

    /**
     * Get libelleIt.
     *
     * @return string
     */
    public function getLibelleIt()
    {
        return $this->libelleIt;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return CategorieConsultation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return $this
     */
    public function addConsultation(Consultation $consultation)
    {
        $this->consultations[] = $consultation;
        $consultation->setCategorieConsultation($this);

        return $this;
    }

    public function removeConsultation(Consultation $consultation)
    {
        $this->consultations->removeElement($consultation);
    }

    /**
     * @return ArrayCollection
     */
    public function getConsultations()
    {
        return $this->consultations;
    }

    public function getIdExterne(): string
    {
        return $this->idExterne;
    }

    public function setIdExterne(string $idExterne): CategorieConsultation
    {
        $this->idExterne = $idExterne;

        return $this;
    }
}
