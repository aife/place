<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use App\Repository\ReferentielSousTypeParapheurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="referentiel_sous_type_parapheur")
 * @ORM\Entity(repositoryClass=ReferentielSousTypeParapheurRepository::class)
 */
class ReferentielSousTypeParapheur
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private string $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $label;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }
}
