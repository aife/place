<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationOrganisme.
 *
 * @ORM\Table(name="configuration_organisme")
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationOrganismeRepository")
 */
class ConfigurationOrganisme
{
    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="encheres", type="string", nullable=false)
     */
    private string $encheres = '0';

    /**
     * @ORM\Column(name="consultation_pj_autres_pieces_telechargeables", type="string", nullable=false)
     */
    private string $consultationPjAutresPiecesTelechargeables = '1';

    /**
     * @ORM\Column(name="no_activex", type="string", nullable=false)
     */
    private string $noActivex = '0';

    /**
     * @ORM\Column(name="gestion_mapa", type="string", nullable=false)
     */
    private string $gestionMapa = '1';

    /**
     * @ORM\Column(name="article_133_upload_fichier", type="string", nullable=false)
     */
    private string $article133UploadFichier = '1';

    /**
     * @ORM\Column(name="centrale_publication", type="string", nullable=false)
     */
    private string $centralePublication = '0';

    /**
     * @ORM\Column(name="organisation_centralisee", type="string", nullable=false)
     */
    private string $organisationCentralisee = '1';

    /**
     * @ORM\Column(name="presence_elu", type="string", nullable=false)
     */
    private string $presenceElu = '0';

    /**
     * @ORM\Column(name="traduire_consultation", type="string", nullable=false)
     */
    private string $traduireConsultation = '0';

    /**
     * @ORM\Column(name="suivi_passation", type="string", nullable=false)
     */
    private string $suiviPassation = '0';

    /**
     * @ORM\Column(name="numerotation_ref_cons", type="string", nullable=false)
     */
    private string $numerotationRefCons = '0';

    /**
     * @ORM\Column(name="pmi_lien_portail_defense_agent", type="string", nullable=false)
     */
    private string $pmiLienPortailDefenseAgent = '0';

    /**
     * @ORM\Column(name="interface_archive_arcade_pmi", type="string", nullable=false)
     */
    private string $interfaceArchiveArcadePmi = '0';

    /**
     * @ORM\Column(name="desarchivage_consultation", type="string", nullable=false)
     */
    private string $desarchivageConsultation = '0';

    /**
     * @ORM\Column(name="alimentation_automatique_liste_invites", type="string", nullable=false)
     */
    private string $alimentationAutomatiqueListeInvites = '0';

    /**
     * @ORM\Column(name="interface_chorus_pmi", type="string", nullable=false)
     */
    private string $interfaceChorusPmi = '0';

    /**
     * @ORM\Column(name="archivage_consultation_sur_pf", type="string", nullable=false)
     */
    private string $archivageConsultationSurPf = '0';

    /**
     * @ORM\Column(name="autoriser_modification_apres_phase_consultation", type="string", nullable=false)
     */
    private string $autoriserModificationApresPhaseConsultation = '1';

    /**
     * @ORM\Column(name="importer_enveloppe", type="string", nullable=false)
     */
    private string $importerEnveloppe = '1';

    /**
     * @ORM\Column(name="export_marches_notifies", type="string", nullable=false)
     */
    private string $exportMarchesNotifies = '0';

    /**
     * @ORM\Column(name="acces_agents_cfe_bd_fournisseur", type="string", nullable=false)
     */
    private string $accesAgentsCfeBdFournisseur = '0';

    /**
     * @ORM\Column(name="acces_agents_cfe_ouverture_analyse", type="string", nullable=false)
     */
    private string $accesAgentsCfeOuvertureAnalyse = '1';

    /**
     * @ORM\Column(name="utiliser_parametrage_encheres", type="string", nullable=false)
     */
    private string $utiliserParametrageEncheres = '0';

    /**
     * @ORM\Column(name="verifier_compte_boamp", type="string", nullable=false)
     */
    private string $verifierCompteBoamp = '0';

    /**
     * @ORM\Column(name="gestion_mandataire", type="string", nullable=false)
     */
    private string $gestionMandataire = '0';

    /**
     * @ORM\Column(name="four_eyes", type="string", nullable=false)
     */
    private string $fourEyes = '0';

    /**
     * @ORM\Column(name="interface_module_rsem", type="string", nullable=false)
     */
    private string $interfaceModuleRsem = '0';

    /**
     * @ORM\Column(name="archivage_consultation_sae_externe_envoi_archive", type="string", nullable=false)
     */
    private string $archivageConsultationSaeExterneEnvoiArchive = '0';

    /**
     * @ORM\Column(name="archivage_consultation_sae_externe_telechargement_archive", type="string", nullable=false)
     */
    private string $archivageConsultationSaeExterneTelechargementArchive = '0';

    /**
     * @ORM\Column(name="agent_verification_certificat_peppol", type="string", nullable=false)
     */
    private string $agentVerificationCertificatPeppol = '0';

    /**
     * @ORM\Column(name="fuseau_horaire", type="string", nullable=false)
     */
    private string $fuseauHoraire = '0';

    /**
     * @ORM\Column(name="fiche_weka", type="string", nullable=false)
     */
    private string $ficheWeka = '0';

    /**
     * @ORM\Column(name="mise_disposition_pieces_marche", type="string", nullable=false)
     */
    private string $miseDispositionPiecesMarche = '0';

    /**
     * @ORM\Column(name="base_dce", type="string", nullable=false)
     */
    private string $baseDce = '0';

    /**
     * @ORM\Column(name="avis_membres_commision", type="string", nullable=false)
     */
    private string $avisMembresCommision = '0';

    /**
     * @ORM\Column(name="Donnees_Redac", type="string", nullable=false)
     */
    private string $donneesRedac = '0';

    /**
     * @ORM\Column(name="Personnaliser_Affichage_Theme_Et_Illustration", type="string", nullable=false)
     */
    private string $personnaliserAffichageThemeEtIllustration = '0';

    /**
     * @ORM\Column(name="type_contrat", type="string", nullable=false)
     */
    private string $typeContrat = '1';

    /**
     * @ORM\Column(name="entite_adjudicatrice", type="string", nullable=false)
     */
    private string $entiteAdjudicatrice = '0';

    /**
     * @ORM\Column(name="gestion_operations", type="string", nullable=false)
     */
    private string $gestionOperations = '0';

    /**
     * @ORM\Column(name="calendrier_de_la_consultation", type="string", nullable=false)
     */
    private string $calendrierDeLaConsultation = '0';

    /**
     * @ORM\Column(name="donnees_complementaires", type="string", nullable=false)
     */
    private string $donneesComplementaires = '0';

    /**
     * @ORM\Column(name="espace_collaboratif", type="string", nullable=false)
     */
    private string $espaceCollaboratif = '0';

    /**
     * @ORM\Column(name="historique_navigation_inscrits", type="string", nullable=false)
     */
    private string $historiqueNavigationInscrits = '1';

    /**
     * @ORM\Column(name="Identification_contrat", type="string", nullable=false)
     */
    private string $identificationContrat = '0';

    /**
     * @ORM\Column(name="extraction_accords_cadres", type="string", nullable=false)
     */
    private string $extractionAccordsCadres = '0';

    /**
     * @ORM\Column(name="extraction_siret_acheteur", type="string", nullable=false)
     */
    private string $extractionSiretAcheteur = '0';

    /**
     * @ORM\Column(name="marche_public_simplifie", type="string", nullable=false)
     */
    private string $marchePublicSimplifie = '0';

    /**
     * @ORM\Column(name="recherches_favorites_agent", type="string", nullable=false)
     */
    private string $recherchesFavoritesAgent = '0mm';

    /**
     * @ORM\Column(name="profil_rma", type="string", nullable=false)
     */
    private string $profilRma = '0';

    /**
     * @ORM\Column(name="filtre_contrat_ac_sad", type="string", nullable=false)
     */
    private string $filtreContratAcSad = '0';

    /**
     * @ORM\Column(name="affichage_nom_service_pere", type="string", nullable=false)
     */
    private string $affichageNomServicePere = '0';

    /**
     * @ORM\Column(name="module_exec", type="string", nullable=false)
     */
    private string $moduleExec = '0';

    /**
     * @ORM\Column(name="mode_applet", type="string", nullable=false)
     */
    private string $modeApplet = '0';

    /**
     * @ORM\Column(name="marche_defense", type="string", nullable=false)
     */
    private string $marcheDefense = '0';

    /**
     * @ORM\Column(name="num_donnees_essentielles_manuel", type="string", nullable=false)
     */
    private string $numDonneesEssentiellesManuel = '0';

    /**
     ** @ORM\Column(name="numero_projet_achat", type="string", nullable=false)
     */
    private string $numeroProjetAchat = '0';

    /**
     * @var bool
     * @ORM\Column(name="espace_documentaire", type="boolean", nullable=false)
     */
    private $espaceDocumentaire = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="echanges_documents", type="boolean", nullable=false)
     */
    private $echangesDocuments = false;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $heureLimiteDeRemiseDePlisParDefaut;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $saisieManuelleIdExterne = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="module_sourcing", type="boolean", nullable=false)
     */
    private $moduleSourcing = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $moduleRecensementProgrammation = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $cmsActif = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $moduleEnvol = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $moduleBiPremium = false;

    /**
     * @var boolean
     * @ORM\Column(name="analyse_offres", nullable=false)
     */
    private $analyseOffres = true;

    /**
     * @var boolean
     * @ORM\Column(name="module_BI", nullable=false)
     */
    private $moduleBI = true;

    /**
     * @var bool
     * @ORM\Column(nullable=false)
     */
    private $cao = false;


    /**
     * @var bool
     * @ORM\Column(nullable=false)
     */
    private bool $dceRestreint = false;

    /**
     * @var bool
     * @ORM\Column(nullable=false)
     */
    private bool $activerMonAssistantMarchesPublics = false;

    /**
     * @var bool
     * @ORM\Column(nullable=false)
     */
    private bool $gestionContratDansExec = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $consultationSimplifiee = true;

    /**
     * @var bool
     * @ORM\Column(name="typage_jo2024", nullable=false)
     */
    private bool $typageJo2024 = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $moduleTncp = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $accesModuleSpaser = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pubTncp = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pubMol = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pubJalFr = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pubJalLux = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $pubJoue = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $moduleEcoSip = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $moduleMpePub = true;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $moduleAdministrationDocument = true;

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): ConfigurationOrganisme
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Set encheres.
     *
     * @param string $encheres
     *
     * @return ConfigurationOrganisme
     */
    public function setEncheres($encheres)
    {
        $this->encheres = $encheres;

        return $this;
    }

    /**
     * Get encheres.
     *
     * @return string
     */
    public function getEncheres()
    {
        return $this->encheres;
    }

    /**
     * Set consultationPjAutresPiecesTelechargeables.
     *
     * @param string $consultationPjAutresPiecesTelechargeables
     *
     * @return ConfigurationOrganisme
     */
    public function setConsultationPjAutresPiecesTelechargeables($consultationPjAutresPiecesTelechargeables)
    {
        $this->consultationPjAutresPiecesTelechargeables = $consultationPjAutresPiecesTelechargeables;

        return $this;
    }

    /**
     * Get consultationPjAutresPiecesTelechargeables.
     *
     * @return string
     */
    public function getConsultationPjAutresPiecesTelechargeables()
    {
        return $this->consultationPjAutresPiecesTelechargeables;
    }

    /**
     * Set noActivex.
     *
     * @param string $noActivex
     *
     * @return ConfigurationOrganisme
     */
    public function setNoActivex($noActivex)
    {
        $this->noActivex = $noActivex;

        return $this;
    }

    /**
     * Get noActivex.
     *
     * @return string
     */
    public function getNoActivex()
    {
        return $this->noActivex;
    }

    /**
     * Set gestionMapa.
     *
     * @param string $gestionMapa
     *
     * @return ConfigurationOrganisme
     */
    public function setGestionMapa($gestionMapa)
    {
        $this->gestionMapa = $gestionMapa;

        return $this;
    }

    /**
     * Get gestionMapa.
     *
     * @return string
     */
    public function getGestionMapa()
    {
        return $this->gestionMapa;
    }

    /**
     * Set article133UploadFichier.
     *
     * @param string $article133UploadFichier
     *
     * @return ConfigurationOrganisme
     */
    public function setArticle133UploadFichier($article133UploadFichier)
    {
        $this->article133UploadFichier = $article133UploadFichier;

        return $this;
    }

    /**
     * Get article133UploadFichier.
     *
     * @return string
     */
    public function getArticle133UploadFichier()
    {
        return $this->article133UploadFichier;
    }

    /**
     * Set centralePublication.
     *
     * @param string $centralePublication
     *
     * @return ConfigurationOrganisme
     */
    public function setCentralePublication($centralePublication)
    {
        $this->centralePublication = $centralePublication;

        return $this;
    }

    /**
     * Get centralePublication.
     *
     * @return string
     */
    public function getCentralePublication()
    {
        return $this->centralePublication;
    }

    /**
     * Set organisationCentralisee.
     *
     * @param string $organisationCentralisee
     *
     * @return ConfigurationOrganisme
     */
    public function setOrganisationCentralisee($organisationCentralisee)
    {
        $this->organisationCentralisee = $organisationCentralisee;

        return $this;
    }

    /**
     * Get organisationCentralisee.
     *
     * @return string
     */
    public function getOrganisationCentralisee()
    {
        return $this->organisationCentralisee;
    }

    /**
     * Set presenceElu.
     *
     * @param string $presenceElu
     *
     * @return ConfigurationOrganisme
     */
    public function setPresenceElu($presenceElu)
    {
        $this->presenceElu = $presenceElu;

        return $this;
    }

    /**
     * Get presenceElu.
     *
     * @return string
     */
    public function getPresenceElu()
    {
        return $this->presenceElu;
    }

    /**
     * Set traduireConsultation.
     *
     * @param string $traduireConsultation
     *
     * @return ConfigurationOrganisme
     */
    public function setTraduireConsultation($traduireConsultation)
    {
        $this->traduireConsultation = $traduireConsultation;

        return $this;
    }

    /**
     * Get traduireConsultation.
     *
     * @return string
     */
    public function getTraduireConsultation()
    {
        return $this->traduireConsultation;
    }

    /**
     * Set suiviPassation.
     *
     * @param string $suiviPassation
     *
     * @return ConfigurationOrganisme
     */
    public function setSuiviPassation($suiviPassation)
    {
        $this->suiviPassation = $suiviPassation;

        return $this;
    }

    /**
     * Get suiviPassation.
     *
     * @return string
     */
    public function getSuiviPassation()
    {
        return $this->suiviPassation;
    }

    /**
     * Set numerotationRefCons.
     *
     * @param string $numerotationRefCons
     *
     * @return ConfigurationOrganisme
     */
    public function setNumerotationRefCons($numerotationRefCons)
    {
        $this->numerotationRefCons = $numerotationRefCons;

        return $this;
    }

    /**
     * Get numerotationRefCons.
     *
     * @return string
     */
    public function getNumerotationRefCons()
    {
        return $this->numerotationRefCons;
    }

    /**
     * Set pmiLienPortailDefenseAgent.
     *
     * @param string $pmiLienPortailDefenseAgent
     *
     * @return ConfigurationOrganisme
     */
    public function setPmiLienPortailDefenseAgent($pmiLienPortailDefenseAgent)
    {
        $this->pmiLienPortailDefenseAgent = $pmiLienPortailDefenseAgent;

        return $this;
    }

    /**
     * Get pmiLienPortailDefenseAgent.
     *
     * @return string
     */
    public function getPmiLienPortailDefenseAgent()
    {
        return $this->pmiLienPortailDefenseAgent;
    }

    /**
     * Set interfaceArchiveArcadePmi.
     *
     * @param string $interfaceArchiveArcadePmi
     *
     * @return ConfigurationOrganisme
     */
    public function setInterfaceArchiveArcadePmi($interfaceArchiveArcadePmi)
    {
        $this->interfaceArchiveArcadePmi = $interfaceArchiveArcadePmi;

        return $this;
    }

    /**
     * Get interfaceArchiveArcadePmi.
     *
     * @return string
     */
    public function getInterfaceArchiveArcadePmi()
    {
        return $this->interfaceArchiveArcadePmi;
    }

    /**
     * Set desarchivageConsultation.
     *
     * @param string $desarchivageConsultation
     *
     * @return ConfigurationOrganisme
     */
    public function setDesarchivageConsultation($desarchivageConsultation)
    {
        $this->desarchivageConsultation = $desarchivageConsultation;

        return $this;
    }

    /**
     * Get desarchivageConsultation.
     *
     * @return string
     */
    public function getDesarchivageConsultation()
    {
        return $this->desarchivageConsultation;
    }

    /**
     * Set alimentationAutomatiqueListeInvites.
     *
     * @param string $alimentationAutomatiqueListeInvites
     *
     * @return ConfigurationOrganisme
     */
    public function setAlimentationAutomatiqueListeInvites($alimentationAutomatiqueListeInvites)
    {
        $this->alimentationAutomatiqueListeInvites = $alimentationAutomatiqueListeInvites;

        return $this;
    }

    /**
     * Get alimentationAutomatiqueListeInvites.
     *
     * @return string
     */
    public function getAlimentationAutomatiqueListeInvites()
    {
        return $this->alimentationAutomatiqueListeInvites;
    }

    /**
     * Set interfaceChorusPmi.
     *
     * @param string $interfaceChorusPmi
     *
     * @return ConfigurationOrganisme
     */
    public function setInterfaceChorusPmi($interfaceChorusPmi)
    {
        $this->interfaceChorusPmi = $interfaceChorusPmi;

        return $this;
    }

    /**
     * Get interfaceChorusPmi.
     *
     * @return string
     */
    public function getInterfaceChorusPmi()
    {
        return $this->interfaceChorusPmi;
    }

    /**
     * Set archivageConsultationSurPf.
     *
     * @param string $archivageConsultationSurPf
     *
     * @return ConfigurationOrganisme
     */
    public function setArchivageConsultationSurPf($archivageConsultationSurPf)
    {
        $this->archivageConsultationSurPf = $archivageConsultationSurPf;

        return $this;
    }

    /**
     * Get archivageConsultationSurPf.
     *
     * @return string
     */
    public function getArchivageConsultationSurPf()
    {
        return $this->archivageConsultationSurPf;
    }

    /**
     * Set autoriserModificationApresPhaseConsultation.
     *
     * @param string $autoriserModificationApresPhaseConsultation
     *
     * @return ConfigurationOrganisme
     */
    public function setAutoriserModificationApresPhaseConsultation($autoriserModificationApresPhaseConsultation)
    {
        $this->autoriserModificationApresPhaseConsultation = $autoriserModificationApresPhaseConsultation;

        return $this;
    }

    /**
     * Get autoriserModificationApresPhaseConsultation.
     *
     * @return string
     */
    public function getAutoriserModificationApresPhaseConsultation()
    {
        return $this->autoriserModificationApresPhaseConsultation;
    }

    /**
     * Set importerEnveloppe.
     *
     * @param string $importerEnveloppe
     *
     * @return ConfigurationOrganisme
     */
    public function setImporterEnveloppe($importerEnveloppe)
    {
        $this->importerEnveloppe = $importerEnveloppe;

        return $this;
    }

    /**
     * Get importerEnveloppe.
     *
     * @return string
     */
    public function getImporterEnveloppe()
    {
        return $this->importerEnveloppe;
    }

    /**
     * Set exportMarchesNotifies.
     *
     * @param string $exportMarchesNotifies
     *
     * @return ConfigurationOrganisme
     */
    public function setExportMarchesNotifies($exportMarchesNotifies)
    {
        $this->exportMarchesNotifies = $exportMarchesNotifies;

        return $this;
    }

    /**
     * Get exportMarchesNotifies.
     *
     * @return string
     */
    public function getExportMarchesNotifies()
    {
        return $this->exportMarchesNotifies;
    }

    /**
     * Set accesAgentsCfeBdFournisseur.
     *
     * @param string $accesAgentsCfeBdFournisseur
     *
     * @return ConfigurationOrganisme
     */
    public function setAccesAgentsCfeBdFournisseur($accesAgentsCfeBdFournisseur)
    {
        $this->accesAgentsCfeBdFournisseur = $accesAgentsCfeBdFournisseur;

        return $this;
    }

    /**
     * Get accesAgentsCfeBdFournisseur.
     *
     * @return string
     */
    public function getAccesAgentsCfeBdFournisseur()
    {
        return $this->accesAgentsCfeBdFournisseur;
    }

    /**
     * Set accesAgentsCfeOuvertureAnalyse.
     *
     * @param string $accesAgentsCfeOuvertureAnalyse
     *
     * @return ConfigurationOrganisme
     */
    public function setAccesAgentsCfeOuvertureAnalyse($accesAgentsCfeOuvertureAnalyse)
    {
        $this->accesAgentsCfeOuvertureAnalyse = $accesAgentsCfeOuvertureAnalyse;

        return $this;
    }

    /**
     * Get accesAgentsCfeOuvertureAnalyse.
     *
     * @return string
     */
    public function getAccesAgentsCfeOuvertureAnalyse()
    {
        return $this->accesAgentsCfeOuvertureAnalyse;
    }

    /**
     * Set utiliserParametrageEncheres.
     *
     * @param string $utiliserParametrageEncheres
     *
     * @return ConfigurationOrganisme
     */
    public function setUtiliserParametrageEncheres($utiliserParametrageEncheres)
    {
        $this->utiliserParametrageEncheres = $utiliserParametrageEncheres;

        return $this;
    }

    /**
     * Get utiliserParametrageEncheres.
     *
     * @return string
     */
    public function getUtiliserParametrageEncheres()
    {
        return $this->utiliserParametrageEncheres;
    }

    /**
     * Set verifierCompteBoamp.
     *
     * @param string $verifierCompteBoamp
     *
     * @return ConfigurationOrganisme
     */
    public function setVerifierCompteBoamp($verifierCompteBoamp)
    {
        $this->verifierCompteBoamp = $verifierCompteBoamp;

        return $this;
    }

    /**
     * Get verifierCompteBoamp.
     *
     * @return string
     */
    public function getVerifierCompteBoamp()
    {
        return $this->verifierCompteBoamp;
    }

    /**
     * Set gestionMandataire.
     *
     * @param string $gestionMandataire
     *
     * @return ConfigurationOrganisme
     */
    public function setGestionMandataire($gestionMandataire)
    {
        $this->gestionMandataire = $gestionMandataire;

        return $this;
    }

    /**
     * Get gestionMandataire.
     *
     * @return string
     */
    public function getGestionMandataire()
    {
        return $this->gestionMandataire;
    }

    /**
     * Set fourEyes.
     *
     * @param string $fourEyes
     *
     * @return ConfigurationOrganisme
     */
    public function setFourEyes($fourEyes)
    {
        $this->fourEyes = $fourEyes;

        return $this;
    }

    /**
     * Get fourEyes.
     *
     * @return string
     */
    public function getFourEyes()
    {
        return $this->fourEyes;
    }

    /**
     * Set interfaceModuleRsem.
     *
     * @param string $interfaceModuleRsem
     *
     * @return ConfigurationOrganisme
     */
    public function setInterfaceModuleRsem($interfaceModuleRsem)
    {
        $this->interfaceModuleRsem = $interfaceModuleRsem;

        return $this;
    }

    /**
     * Get interfaceModuleRsem.
     *
     * @return string
     */
    public function getInterfaceModuleRsem()
    {
        return $this->interfaceModuleRsem;
    }

    /**
     * Set archivageConsultationSaeExterneEnvoiArchive.
     *
     * @param string $archivageConsultationSaeExterneEnvoiArchive
     *
     * @return ConfigurationOrganisme
     */
    public function setArchivageConsultationSaeExterneEnvoiArchive($archivageConsultationSaeExterneEnvoiArchive)
    {
        $this->archivageConsultationSaeExterneEnvoiArchive = $archivageConsultationSaeExterneEnvoiArchive;

        return $this;
    }

    /**
     * Get archivageConsultationSaeExterneEnvoiArchive.
     *
     * @return string
     */
    public function getArchivageConsultationSaeExterneEnvoiArchive()
    {
        return $this->archivageConsultationSaeExterneEnvoiArchive;
    }

    /**
     * Set archivageConsultationSaeExterneTelechargementArchive.
     *
     * @param string $archivageConsultationSaeExterneTelechargementArchive
     *
     * @return ConfigurationOrganisme
     */
    public function setArchivageConsultationSaeExterneTelechargementArchive(
        $archivageConsultationSaeExterneTelechargementArchive
    ) {
        $this->archivageConsultationSaeExterneTelechargementArchive =
            $archivageConsultationSaeExterneTelechargementArchive;

        return $this;
    }

    /**
     * Get archivageConsultationSaeExterneTelechargementArchive.
     *
     * @return string
     */
    public function getArchivageConsultationSaeExterneTelechargementArchive()
    {
        return $this->archivageConsultationSaeExterneTelechargementArchive;
    }

    /**
     * Set agentVerificationCertificatPeppol.
     *
     * @param string $agentVerificationCertificatPeppol
     *
     * @return ConfigurationOrganisme
     */
    public function setAgentVerificationCertificatPeppol($agentVerificationCertificatPeppol)
    {
        $this->agentVerificationCertificatPeppol = $agentVerificationCertificatPeppol;

        return $this;
    }

    /**
     * Get agentVerificationCertificatPeppol.
     *
     * @return string
     */
    public function getAgentVerificationCertificatPeppol()
    {
        return $this->agentVerificationCertificatPeppol;
    }

    /**
     * Set fuseauHoraire.
     *
     * @param string $fuseauHoraire
     *
     * @return ConfigurationOrganisme
     */
    public function setFuseauHoraire($fuseauHoraire)
    {
        $this->fuseauHoraire = $fuseauHoraire;

        return $this;
    }

    /**
     * Get fuseauHoraire.
     *
     * @return string
     */
    public function getFuseauHoraire()
    {
        return $this->fuseauHoraire;
    }

    /**
     * Set ficheWeka.
     *
     * @param string $ficheWeka
     *
     * @return ConfigurationOrganisme
     */
    public function setFicheWeka($ficheWeka)
    {
        $this->ficheWeka = $ficheWeka;

        return $this;
    }

    /**
     * Get ficheWeka.
     *
     * @return string
     */
    public function getFicheWeka()
    {
        return $this->ficheWeka;
    }

    /**
     * Set miseDispositionPiecesMarche.
     *
     * @param string $miseDispositionPiecesMarche
     *
     * @return ConfigurationOrganisme
     */
    public function setMiseDispositionPiecesMarche($miseDispositionPiecesMarche)
    {
        $this->miseDispositionPiecesMarche = $miseDispositionPiecesMarche;

        return $this;
    }

    /**
     * Get miseDispositionPiecesMarche.
     *
     * @return string
     */
    public function getMiseDispositionPiecesMarche()
    {
        return $this->miseDispositionPiecesMarche;
    }

    /**
     * Set baseDce.
     *
     * @param string $baseDce
     *
     * @return ConfigurationOrganisme
     */
    public function setBaseDce($baseDce)
    {
        $this->baseDce = $baseDce;

        return $this;
    }

    /**
     * Get baseDce.
     *
     * @return string
     */
    public function getBaseDce()
    {
        return $this->baseDce;
    }

    /**
     * Set avisMembresCommision.
     *
     * @param string $avisMembresCommision
     *
     * @return ConfigurationOrganisme
     */
    public function setAvisMembresCommision($avisMembresCommision)
    {
        $this->avisMembresCommision = $avisMembresCommision;

        return $this;
    }

    /**
     * Get avisMembresCommision.
     *
     * @return string
     */
    public function getAvisMembresCommision()
    {
        return $this->avisMembresCommision;
    }

    /**
     * Set donneesRedac.
     *
     * @param string $donneesRedac
     *
     * @return ConfigurationOrganisme
     */
    public function setDonneesRedac($donneesRedac)
    {
        $this->donneesRedac = $donneesRedac;

        return $this;
    }

    /**
     * Get donneesRedac.
     *
     * @return string
     */
    public function getDonneesRedac()
    {
        return $this->donneesRedac;
    }

    /**
     * Set personnaliserAffichageThemeEtIllustration.
     *
     * @param string $personnaliserAffichageThemeEtIllustration
     *
     * @return ConfigurationOrganisme
     */
    public function setPersonnaliserAffichageThemeEtIllustration($personnaliserAffichageThemeEtIllustration)
    {
        $this->personnaliserAffichageThemeEtIllustration = $personnaliserAffichageThemeEtIllustration;

        return $this;
    }

    /**
     * Get personnaliserAffichageThemeEtIllustration.
     *
     * @return string
     */
    public function getPersonnaliserAffichageThemeEtIllustration()
    {
        return $this->personnaliserAffichageThemeEtIllustration;
    }

    /**
     * Set typeContrat.
     *
     * @param string $typeContrat
     *
     * @return ConfigurationOrganisme
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;

        return $this;
    }

    /**
     * Get typeContrat.
     *
     * @return string
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * Set entiteAdjudicatrice.
     *
     * @param string $entiteAdjudicatrice
     *
     * @return ConfigurationOrganisme
     */
    public function setEntiteAdjudicatrice($entiteAdjudicatrice)
    {
        $this->entiteAdjudicatrice = $entiteAdjudicatrice;

        return $this;
    }

    /**
     * Get entiteAdjudicatrice.
     *
     * @return string
     */
    public function getEntiteAdjudicatrice()
    {
        return $this->entiteAdjudicatrice;
    }

    /**
     * Set gestionOperations.
     *
     * @param string $gestionOperations
     *
     * @return ConfigurationOrganisme
     */
    public function setGestionOperations($gestionOperations)
    {
        $this->gestionOperations = $gestionOperations;

        return $this;
    }

    /**
     * Get gestionOperations.
     *
     * @return string
     */
    public function getGestionOperations()
    {
        return $this->gestionOperations;
    }

    /**
     * Set calendrierDeLaConsultation.
     *
     * @param string $calendrierDeLaConsultation
     *
     * @return ConfigurationOrganisme
     */
    public function setCalendrierDeLaConsultation($calendrierDeLaConsultation)
    {
        $this->calendrierDeLaConsultation = $calendrierDeLaConsultation;

        return $this;
    }

    /**
     * Get calendrierDeLaConsultation.
     *
     * @return string
     */
    public function getCalendrierDeLaConsultation()
    {
        return $this->calendrierDeLaConsultation;
    }

    /**
     * Set donneesComplementaires.
     *
     * @param string $donneesComplementaires
     *
     * @return ConfigurationOrganisme
     */
    public function setDonneesComplementaires($donneesComplementaires)
    {
        $this->donneesComplementaires = $donneesComplementaires;

        return $this;
    }

    /**
     * Get donneesComplementaires.
     *
     * @return string
     */
    public function getDonneesComplementaires()
    {
        return $this->donneesComplementaires;
    }

    /**
     * Set espaceCollaboratif.
     *
     * @param string $espaceCollaboratif
     *
     * @return ConfigurationOrganisme
     */
    public function setEspaceCollaboratif($espaceCollaboratif)
    {
        $this->espaceCollaboratif = $espaceCollaboratif;

        return $this;
    }

    /**
     * Get espaceCollaboratif.
     *
     * @return string
     */
    public function getEspaceCollaboratif()
    {
        return $this->espaceCollaboratif;
    }

    /**
     * Set historiqueNavigationInscrits.
     *
     * @param string $historiqueNavigationInscrits
     *
     * @return ConfigurationOrganisme
     */
    public function setHistoriqueNavigationInscrits($historiqueNavigationInscrits)
    {
        $this->historiqueNavigationInscrits = $historiqueNavigationInscrits;

        return $this;
    }

    /**
     * Get historiqueNavigationInscrits.
     *
     * @return string
     */
    public function getHistoriqueNavigationInscrits()
    {
        return $this->historiqueNavigationInscrits;
    }

    /**
     * Set identificationContrat.
     *
     * @param string $identificationContrat
     *
     * @return ConfigurationOrganisme
     */
    public function setIdentificationContrat($identificationContrat)
    {
        $this->identificationContrat = $identificationContrat;

        return $this;
    }

    /**
     * Get identificationContrat.
     *
     * @return string
     */
    public function getIdentificationContrat()
    {
        return $this->identificationContrat;
    }

    /**
     * Set extractionAccordsCadres.
     *
     * @param string $extractionAccordsCadres
     *
     * @return ConfigurationOrganisme
     */
    public function setExtractionAccordsCadres($extractionAccordsCadres)
    {
        $this->extractionAccordsCadres = $extractionAccordsCadres;

        return $this;
    }

    /**
     * Get extractionAccordsCadres.
     *
     * @return string
     */
    public function getExtractionAccordsCadres()
    {
        return $this->extractionAccordsCadres;
    }

    /**
     * Set extractionSiretAcheteur.
     *
     * @param string $extractionSiretAcheteur
     *
     * @return ConfigurationOrganisme
     */
    public function setExtractionSiretAcheteur($extractionSiretAcheteur)
    {
        $this->extractionSiretAcheteur = $extractionSiretAcheteur;

        return $this;
    }

    /**
     * Get extractionSiretAcheteur.
     *
     * @return string
     */
    public function getExtractionSiretAcheteur()
    {
        return $this->extractionSiretAcheteur;
    }

    /**
     * Set marchePublicSimplifie.
     *
     * @param string $marchePublicSimplifie
     *
     * @return ConfigurationOrganisme
     */
    public function setMarchePublicSimplifie($marchePublicSimplifie)
    {
        $this->marchePublicSimplifie = $marchePublicSimplifie;

        return $this;
    }

    /**
     * Get marchePublicSimplifie.
     *
     * @return string
     */
    public function getMarchePublicSimplifie()
    {
        return $this->marchePublicSimplifie;
    }

    /**
     * Set recherchesFavoritesAgent.
     *
     * @param string $recherchesFavoritesAgent
     *
     * @return ConfigurationOrganisme
     */
    public function setRecherchesFavoritesAgent($recherchesFavoritesAgent)
    {
        $this->recherchesFavoritesAgent = $recherchesFavoritesAgent;

        return $this;
    }

    /**
     * Get recherchesFavoritesAgent.
     *
     * @return string
     */
    public function getRecherchesFavoritesAgent()
    {
        return $this->recherchesFavoritesAgent;
    }

    /**
     * Set profilRma.
     *
     * @param string $profilRma
     *
     * @return ConfigurationOrganisme
     */
    public function setProfilRma($profilRma)
    {
        $this->profilRma = $profilRma;

        return $this;
    }

    /**
     * Get profilRma.
     *
     * @return string
     */
    public function getProfilRma()
    {
        return $this->profilRma;
    }

    /**
     * Set filtreContratAcSad.
     *
     * @param string $filtreContratAcSad
     *
     * @return ConfigurationOrganisme
     */
    public function setFiltreContratAcSad($filtreContratAcSad)
    {
        $this->filtreContratAcSad = $filtreContratAcSad;

        return $this;
    }

    /**
     * Get filtreContratAcSad.
     *
     * @return string
     */
    public function getFiltreContratAcSad()
    {
        return $this->filtreContratAcSad;
    }

    /**
     * Set affichageNomServicePere.
     *
     * @param string $affichageNomServicePere
     *
     * @return ConfigurationOrganisme
     */
    public function setAffichageNomServicePere($affichageNomServicePere)
    {
        $this->affichageNomServicePere = $affichageNomServicePere;

        return $this;
    }

    /**
     * Get affichageNomServicePere.
     *
     * @return string
     */
    public function getAffichageNomServicePere()
    {
        return $this->affichageNomServicePere;
    }

    /**
     * @return string
     */
    public function getModuleExec()
    {
        return $this->moduleExec;
    }

    /**
     * @param string $moduleExec
     */
    public function setModuleExec($moduleExec)
    {
        $this->moduleExec = $moduleExec;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroProjetAchat()
    {
        return $this->numeroProjetAchat;
    }

    /**
     * @param string $numeroProjetAchat
     */
    public function setNumeroProjetAchat($numeroProjetAchat)
    {
        $this->numeroProjetAchat = $numeroProjetAchat;

        return $this;
    }

    /**
     * @return string
     */
    public function getModeApplet()
    {
        return $this->modeApplet;
    }

    /**
     * @param string $modeApplet
     */
    public function setModeApplet($modeApplet)
    {
        $this->modeApplet = $modeApplet;
    }

    /**
     * @return string
     */
    public function getMarcheDefense()
    {
        return $this->marcheDefense;
    }

    /**
     * @param string $marcheDefense
     */
    public function setMarcheDefense($marcheDefense)
    {
        $this->marcheDefense = $marcheDefense;
    }

    /**
     * @return string
     */
    public function getNumDonneesEssentiellesManuel()
    {
        return $this->numDonneesEssentiellesManuel;
    }

    /**
     * @param string $numDonneesEssentiellesManuel
     */
    public function setNumDonneesEssentiellesManuel($numDonneesEssentiellesManuel)
    {
        $this->numDonneesEssentiellesManuel = $numDonneesEssentiellesManuel;
    }

    /**
     * @return bool
     */
    public function isEspaceDocumentaire()
    {
        return $this->espaceDocumentaire;
    }

    /**
     * @return ConfigurationOrganisme
     */
    public function setEspaceDocumentaire(bool $espaceDocumentaire)
    {
        $this->espaceDocumentaire = $espaceDocumentaire;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEchangesDocuments()
    {
        return $this->echangesDocuments;
    }

    /**
     * @param bool $echangesDocuments
     *
     * @return ConfigurationOrganisme
     */
    public function setEchangesDocuments($echangesDocuments)
    {
        $this->echangesDocuments = $echangesDocuments;

        return $this;
    }

    /**
     * @return string
     */
    public function isHeureLimiteDeRemiseDePlisParDefaut(): ?string
    {
        return $this->heureLimiteDeRemiseDePlisParDefaut;
    }

    /**
     * @param  $heureLimiteDeRemiseDePlisParDefaut
     */
    public function setHeureLimiteDeRemiseDePlisParDefaut($heureLimiteDeRemiseDePlisParDefaut): ConfigurationOrganisme
    {
        $this->heureLimiteDeRemiseDePlisParDefaut = $heureLimiteDeRemiseDePlisParDefaut;

        return $this;
    }

    public function getSaisieManuelleIdExterne(): bool
    {
        return $this->saisieManuelleIdExterne;
    }

    /**
     * @param bool $saisieManuelleIdExterne
     */
    public function setSaisieManuelleIdExterne($saisieManuelleIdExterne): ConfigurationOrganisme
    {
        $this->saisieManuelleIdExterne = $saisieManuelleIdExterne;

        return $this;
    }

    public function getModuleSourcing(): bool
    {
        return $this->moduleSourcing;
    }

    /**
     * @param bool $moduleSourcing
     */
    public function setModuleSourcing($moduleSourcing): ConfigurationOrganisme
    {
        $this->moduleSourcing = $moduleSourcing;

        return $this;
    }

    public function isModuleRecensementProgrammation(): bool
    {
        return $this->moduleRecensementProgrammation;
    }

    public function setModuleRecensementProgrammation(bool $moduleRecensementProgrammation): ConfigurationOrganisme
    {
        $this->moduleRecensementProgrammation = $moduleRecensementProgrammation;
        return $this;
    }

    public function isModuleEnvol(): bool
    {
        return $this->moduleEnvol;
    }

    public function setModuleEnvol(bool $moduleEnvol): self
    {
        $this->moduleEnvol = $moduleEnvol;
        return $this;
    }

    public function isModuleBiPremium(): bool
    {
        return $this->moduleBiPremium;
    }

    public function setModuleBiPremium(bool $moduleBiPremium): ConfigurationOrganisme
    {
        $this->moduleBiPremium = $moduleBiPremium;

        return $this;
    }

    public function isAnalyseOffres(): bool
    {
        return $this->analyseOffres;
    }

    public function setAnalyseOffres(bool $analyseOffres): self
    {
        $this->analyseOffres = $analyseOffres;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCao(): bool
    {
        return $this->cao;
    }

    /**
     * @param bool $cao
     */
    public function setCao(bool $cao): self
    {
        $this->cao = $cao;
        return $this;
    }

    /**
     * @return bool
     */
    public function getCmsActif(): bool
    {
        return $this->cmsActif;
    }

    /**
     * @param bool $cmsActif
     * @return ConfigurationOrganisme
     */
    public function setCmsActif(bool $cmsActif): self
    {
        $this->cmsActif = $cmsActif;
        return $this;
    }



    /**
     * @return bool
     */
    public function isModuleBI(): bool
    {
        return $this->moduleBI;
    }

    /**
     * @param bool $moduleBI
     */
    public function setModuleBI(bool $moduleBI): self
    {
        $this->moduleBI = $moduleBI;

        return $this;
    }

    /**
     * @return bool
     */
    public function isDceRestreint(): bool
    {
        return $this->dceRestreint;
    }

    /**
     * @param bool $dceRestreint
     */
    public function setDceRestreint(bool $dceRestreint): self
    {
        $this->dceRestreint = $dceRestreint;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActiverMonAssistantMarchesPublics(): bool
    {
        return $this->activerMonAssistantMarchesPublics;
    }

    /**
     * @param bool $activerMonAssistantMarchesPublics
     */
    public function setActiverMonAssistantMarchesPublics(bool $activerMonAssistantMarchesPublics): self
    {
        $this->activerMonAssistantMarchesPublics = $activerMonAssistantMarchesPublics;

        return $this;
    }

    /**
     * @return bool
     */
    public function isGestionContratDansExec(): bool
    {
        return $this->gestionContratDansExec;
    }

    /**
     * @param bool $gestionContratDansExec
     */
    public function setGestionContratDansExec(bool $gestionContratDansExec): void
    {
        $this->gestionContratDansExec = $gestionContratDansExec;
    }

    public function isConsultationSimplifiee(): bool
    {
        return $this->consultationSimplifiee;
    }

    public function setConsultationSimplifiee(bool $consultationSimplifiee): self
    {
        $this->consultationSimplifiee = $consultationSimplifiee;

        return $this;
    }
    public function isTypageJo2024(): bool
    {
        return $this->typageJo2024;
    }

    public function setTypageJo2024(bool $typageJo2024): self
    {
        $this->typageJo2024 = $typageJo2024;

        return $this;
    }

    public function isModuleTncp(): bool
    {
        return $this->moduleTncp;
    }

    public function setModuleTncp(bool $moduleTncp): self
    {
        $this->moduleTncp = $moduleTncp;

        return $this;
    }
    public function isAccesModuleSpaser(): bool
    {
        return $this->accesModuleSpaser;
    }

    public function setAccesModuleSpaser(bool $accesModuleSpaser): self
    {
        $this->accesModuleSpaser = $accesModuleSpaser;

        return $this;
    }

    public function isPubTncp(): bool
    {
        return $this->pubTncp;
    }

    public function setPubTncp(bool $pubTncp): void
    {
        $this->pubTncp = $pubTncp;
    }

    public function isPubMol(): bool
    {
        return $this->pubMol;
    }

    public function setPubMol(bool $pubMol): void
    {
        $this->pubMol = $pubMol;
    }

    public function isPubJalFr(): bool
    {
        return $this->pubJalFr;
    }

    public function setPubJalFr(bool $pubJalFr): void
    {
        $this->pubJalFr = $pubJalFr;
    }

    public function isPubJalLux(): bool
    {
        return $this->pubJalLux;
    }

    public function setPubJalLux(bool $pubJalLux): void
    {
        $this->pubJalLux = $pubJalLux;
    }

    public function isPubJoue(): bool
    {
        return $this->pubJoue;
    }

    public function setPubJoue(bool $pubJoue): void
    {
        $this->pubJoue = $pubJoue;
    }

    public function isModuleEcoSip(): bool
    {
        return $this->moduleEcoSip;
    }

    public function setModuleEcoSip(bool $moduleEcoSip): void
    {
        $this->moduleEcoSip = $moduleEcoSip;
    }

    public function isModuleMpePub(): bool
    {
        return $this->moduleMpePub;
    }

    public function setModuleMpePub(bool $moduleMpePub): self
    {
        $this->moduleMpePub = $moduleMpePub;

        return $this;
    }

    public function isModuleAdministrationDocument(): bool
    {
        return $this->moduleAdministrationDocument;
    }

    public function setModuleAdministrationDocument(bool $moduleAdministrationDocument): self
    {
        $this->moduleAdministrationDocument = $moduleAdministrationDocument;

        return $this;
    }
}
