<?php

namespace App\Entity\Contrat;

use Exception;
use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_numerotation_automatique")
 * @ORM\Entity(repositoryClass="App\Repository\Contrat\NumerotationAutomatiqueRepository")
 */
class NumerotationAutomatique
{
    private EntityManager $em;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private ?string $annee = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $contratMulti = null;

    /**
     * @ORM\Column(type="string", length=20, name="contrat_SA_dynamique", nullable=true)
     */
    private ?string $contratSADynamique = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $contratTitulaire = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    public function setServiceId(int $serviceId = null)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    public function setAnnee(string $annee = null)
    {
        $this->annee = $annee;
    }

    /**
     * @return string
     */
    public function getContratMulti()
    {
        return $this->contratMulti;
    }

    public function setContratMulti(string $contratMulti = null)
    {
        $this->contratMulti = $contratMulti;
    }

    /**
     * @return string
     */
    public function getContratSADynamique()
    {
        return $this->contratSADynamique;
    }

    public function setContratSADynamique(string $contratSADynamique = null)
    {
        $this->contratSADynamique = $contratSADynamique;
    }

    /**
     * @return string
     */
    public function getContratTitulaire()
    {
        return $this->contratTitulaire;
    }

    public function setContratTitulaire(string $contratTitulaire = null)
    {
        $this->contratTitulaire = $contratTitulaire;
    }

    /**
     * Cette fonction permet de copier le NumerotationAutomatique lui attribuant un nouveau service et un nouvel organisme de destination passés en paramètres.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|RPA false si le clone n'a pas réussi, sinon le nouvel objet NumerotationAutomatique créé
     *
     * @throws Exception
     */
    public function copier(int $idServiceDestination, string $organismeDestination)
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $numerotationAutomatique = clone $this;
            $numerotationAutomatique->setId($this->getMaxId() + 1);
            $numerotationAutomatique->setServiceId($idServiceDestination);
            $numerotationAutomatique->setOrganisme($organismeDestination);
            $this->em->persist($numerotationAutomatique);
            $this->em->flush();

            return $numerotationAutomatique;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(NumerotationAutomatique::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }
}
