<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * InvitationConsultationTransverse.
 *
 * @ORM\Table(name="InvitationConsultationTransverse")
 * @ORM\Entity(repositoryClass="App\Repository\InvitationConsultationTransverseRepository")
 */
class InvitationConsultationTransverse
{
    /**
     *
     * @ORM\Column(name="id",                   type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme_emetteur", type="string",length=30, nullable=false)
     */
    private ?string $organismeEmetteur = null;

    /**
     * @ORM\Column(name="reference", type="string",length=255, nullable=false)
     */
    private ?string $reference = null;

    /**
     * @ORM\Column(name="organisme_invite", type="string",length=30, nullable=false)
     */
    private ?string $organismeInvite = null;

    /**
     * @ORM\Column(name="lot", type="integer",length=11, nullable=false)
     */
    private ?int $lot = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_decision", type="date",nullable=true)*/
    private $dateDecision;

    /**
     * @var ContratTitulaire
     * @ORM\Column(name="id_contrat_titulaire",             type="integer", nullable=true, length=11)
     * @ORM\ManyToOne(targetEntity="App\Entity\ContratTitulaire", inversedBy="transverse", cascade={"persist"})
     */
    private $idContratTitulaire;

    /**
     * @ORM\Column(name="consultation_id", type="integer",length=11, nullable=true)
     */
    private ?int $consultationId = null;

    #[Groups('webservice')]
    public function getId() : int
    {
        return $this->id;
    }

    #[Groups('webservice')]
    public function getOrganismeEmetteur() : string
    {
        return $this->organismeEmetteur;
    }

    public function setOrganismeEmetteur(string $organismeEmetteur)
    {
        $this->organismeEmetteur = $organismeEmetteur;

        return $this;
    }

    public function getReference(): string
    {
        return $this->reference;
    }

    public function setReference(string $reference)
    {
        $this->reference = $reference;

        return $this;
    }

    public function getOrganismeInvite(): string
    {
        return $this->organismeInvite;
    }

    public function setOrganismeInvite(string $organismeInvite)
    {
        $this->organismeInvite = $organismeInvite;

        return $this;
    }

    /**
     * @return int
     */
    public function getLot()
    {
        return $this->lot;
    }

    public function setLot(int $lot)
    {
        $this->lot = $lot;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateDecision()
    {
        return $this->dateDecision;
    }

    public function setDateDecision(DateTime $dateDecision = null)
    {
        $this->dateDecision = $dateDecision;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    /**
     * @param $idContratTitulaire
     */
    public function setIdContratTitulaire($idContratTitulaire = null)
    {
        $this->idContratTitulaire = $idContratTitulaire;

        return $this;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    public function setConsultationId(int $consultationId = null)
    {
        $this->consultationId = $consultationId;

        return $this;
    }
}
