<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ModificationContrat.
 *
 * @ORM\Table(name="modification_contrat")
 * @ORM\Entity(repositoryClass="App\Repository\ModificationContratRepository")
 */
class ModificationContrat
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")*/
    #[Groups('webservice')]
    private int $id;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\ContratTitulaire", inversedBy="modificationContrats",
     *     fetch="EAGER")
     * @ORM\JoinColumn(name="id_contrat_titulaire", referencedColumnName="id_contrat_titulaire")*/
    #[Assert\NotBlank(message: "'idContratTitulaire' ne peut être null")]
    #[Groups('webservice')]
    private $idContratTitulaire;

    /**
     * @ORM\Column(name="num_ordre", type="integer")*/
    #[Assert\NotBlank(message: "'numOrdre' ne peut être null")]
    #[Groups('webservice')]
    private ?int $numOrdre = null;

    /**
     * @var datetime
     *
     * @ORM\Column(name="date_creation", type="datetime")*/
    #[Groups('webservice')]
    private $dateCreation;

    /**
     * @var datetime
     *
     * @ORM\Column(name="date_modification", type="datetime")*/
    #[Groups('webservice')]
    private $dateModification;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent", inversedBy="modificationContrats",
     *     fetch="EAGER")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")*/
    #[Groups('webservice')]
    private $idAgent;

    /**
     * @ORM\Column(name="objet_modification", type="text", length=65535, nullable=false)*/
    #[Assert\NotBlank(message: "'objetModification' ne peut être null")]
    #[Groups('webservice')]
    private ?string $objetModification = null;

    /**
     * @var datetime
     * @ORM\Column(name="date_signature", type="datetime")*/
    #[Assert\NotBlank(message: "'dateSignature' ne peut être null")]
    #[Groups('webservice')]
    private $dateSignature;

    /**
     *
     * @ORM\Column(name="montant", type="string")*/
    #[Groups('webservice')]
    private ?string $montant = null;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="App\Entity\Etablissement", inversedBy="modificationContrats",
     *     fetch="EAGER")
     * @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")*/
    #[Groups('webservice')]
    private $idEtablissement;

    /**
     *
     * @ORM\Column(name="duree_marche", type="integer")*/
    #[Groups('webservice')]
    private ?int $dureeMarche = null;

    /**
     *
     * @ORM\Column(name="statut_publication_sn", type="integer", nullable=false)*/
    #[Groups('webservice')]
    private int $statutPublicationSn = 0;


    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_publication_sn", type="datetime", nullable=true)*/
    private $datePublicationSN =  null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification_sn", type="datetime", nullable=true)*/
    private $dateModificationSN = null;

    /**
     * @var string
     *
     * @ORM\Column(name="erreur_sn", type="text", length=65535, nullable=true)
     */
    private $erreurSN;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numOrdre.
     *
     * @param int $numOrdre
     *
     * @return ModificationContrat
     */
    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;

        return $this;
    }

    /**
     * Get numOrdre.
     *
     * @return int
     */
    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    /**
     * Set dateCreation.
     *
     * @param DateTime $dateCreation
     *
     * @return ModificationContrat
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param DateTime $dateModification
     *
     * @return ModificationContrat
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set objetModification.
     *
     * @param string $objetModification
     *
     * @return ModificationContrat
     */
    public function setObjetModification($objetModification)
    {
        $this->objetModification = $objetModification;

        return $this;
    }

    /**
     * Get objetModification.
     *
     * @return string
     */
    public function getObjetModification()
    {
        return $this->objetModification;
    }

    /**
     * Set dateSignature.
     *
     * @param DateTime $dateSignature
     *
     * @return ModificationContrat
     */
    public function setDateSignature($dateSignature)
    {
        $this->dateSignature = $dateSignature;

        return $this;
    }

    /**
     * Get dateSignature.
     *
     * @return DateTime
     */
    public function getDateSignature()
    {
        return $this->dateSignature;
    }

    /**
     * Set montant.
     *
     * @param string $montant
     *
     * @return ModificationContrat
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    /**
     * Get montant.
     *
     * @return string
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * Set dureeMarche.
     *
     * @param int $dureeMarche
     *
     * @return ModificationContrat
     */
    public function setDureeMarche($dureeMarche)
    {
        $this->dureeMarche = $dureeMarche;

        return $this;
    }

    /**
     * Get dureeMarche.
     *
     * @return int
     */
    public function getDureeMarche()
    {
        return $this->dureeMarche;
    }

    /**
     * Set idContratTitulaire.
     *
     *
     * @return ModificationContrat
     */
    public function setIdContratTitulaire(ContratTitulaire $idContratTitulaire = null)
    {
        $this->idContratTitulaire = $idContratTitulaire;

        return $this;
    }

    /**
     * Get idContratTitulaire.
     *
     * @return ContratTitulaire
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    /**
     * Set idAgent.
     *
     *
     * @return ModificationContrat
     */
    public function setIdAgent(Agent $idAgent = null)
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    /**
     * Get idAgent.
     *
     * @return Agent
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * Set idEtablissement.
     *
     *
     * @return ModificationContrat
     */
    public function setIdEtablissement(Etablissement $idEtablissement = null)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return Etablissement
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    public function getStatutPublicationSn(): int
    {
        return $this->statutPublicationSn;
    }

    public function setStatutPublicationSn(int $statutPublicationSn)
    {
        $this->statutPublicationSn = $statutPublicationSn;
    }

    /**
     * @return DateTime
     */
    public function getDatePublicationSN(): ?DateTime
    {
        return $this->datePublicationSN;
    }

    /**
     * @param DateTime $datePublicationSN
     */
    public function setDatePublicationSN($datePublicationSN = null): self
    {
        $this->datePublicationSN = $datePublicationSN;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateModificationSN(): ?DateTime
    {
        return $this->dateModificationSN;
    }

    /**
     * @param DateTime $dateModificationSN
     */
    public function setDateModificationSN($dateModificationSN = null): self
    {
        $this->dateModificationSN = $dateModificationSN;
        return $this;
    }

    /**
     * @return string
     */
    public function getErreurSN(): ?string
    {
        return $this->erreurSN;
    }

    /**
     * @param string $erreurSN
     */
    public function setErreurSN(string $erreurSN): self
    {
        $this->erreurSN = $erreurSN;
        return $this;
    }

}
