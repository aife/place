<?php

namespace App\Entity;

use DateTime;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApiPlatformCustom\AttestationCoffreFort;
use App\Controller\ApiPlatformCustom\AttestationFiscales;
use App\Dto\Input\EntrepriseInput;
use App\Dto\Output\EntrepriseOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Entreprise.
 *
 * @ORM\Table(name="Entreprise", indexes={@ORM\Index(name="region", columns={"region"}),
 *     @ORM\Index(name="province", columns={"province"}),
 *     @ORM\Index(name="domaines_activites", columns={"domaines_activites"}),
 * @ORM\Index(name="qualification", columns={"qualification"})})
 * @ORM\Entity(repositoryClass="App\Repository\EntrepriseRepository")
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: [
        'get','put','patch',
        'get_attestation_coffre_fort' => [
            'method' => 'GET',
            'path' => '/attestations/coffre-fort/{id}',
            'controller' => AttestationCoffreFort::class,
            'openapi_context' => [
                'responses' => [
                    '200' => [
                        'description' => 'Binary File Response'
                    ],
                    '404' => [
                        'description' => 'Resource not found'
                    ],
                ]
            ],
            'shortName' => 'AAAAAAAAAAAAa',
        ],
        'get_download' => [
            'method' => 'GET',
            'path' => '/attestations/fiscal/{id}',
            'controller' => AttestationFiscales::class,
            'openapi_context' => [
                'responses' => [
                    '200' => [
                        'description' => 'Binary File Response'
                    ],
                    '404' => [
                        'description' => 'Resource not found'
                    ],
                ]
            ],
        ]
    ],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input:EntrepriseInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: EntrepriseOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'id' => 'exact',
        'siren' => 'exact',
        'codeAPE' => 'exact',
        'raisonSociale' => 'partial',
    ]
)]
class Entreprise
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\TBourseCotraitance", mappedBy="entreprise")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_entreprise")
     */
    private Collection $bourses;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Inscrit", mappedBy="entreprise")
     * @ORM\JoinColumn(name="id", referencedColumnName="entreprise_id")
     */
    private Collection $inscrits;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Etablissement", mappedBy="entreprise")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_entreprise")
     */
    private Collection $etablissements;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\EntrepriseDocument", mappedBy="entreprise")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_entreprise")
     */
    private Collection $documents;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Responsableengagement",
     *     mappedBy="entreprise",cascade={"persist"}) )
     * @ORM\JoinColumn(name="id", referencedColumnName="entreprise_id")
     */
    private Collection $responsableengagements;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     **/
    #[Groups('webservice')]
    private ?int $id = null;

    /**
     * @ORM\Column(name="admin_id", type="integer", nullable=false)
     */
    private string|int $adminId = '0';

    /**
     * @ORM\Column(name="siren", type="string", length=20, nullable=true)
     */
    private ?string $siren = null;

    /**
     * @ORM\Column(name="repmetiers", type="string", length=1, nullable=false)
     */
    private string $repmetiers = '';

    /**
     * @ORM\Column(name="nom", type="text", length=65535, nullable=false)
     */
    private ?string $nom = null;

    /**
     *
     * @ORM\Column(name="adresse", type="string", length=80, nullable=false)*/
    #[Groups('webservice')]
    private string $adresse = '';

    /**
     *
     * @ORM\Column(name="codepostal", type="string", length=5, nullable=false)*/
    #[Groups('webservice')]
    private string $codepostal = '';

    /**
     * @ORM\Column(name="villeadresse", type="string", length=50, nullable=false)
     */
    private string $villeadresse = '';

    /**
     * @ORM\Column(name="paysadresse", type="string", length=50, nullable=false)
     */
    private string $paysadresse = '';

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="taille", type="integer", nullable=true)
     */
    private ?int $taille = null;

    /**
     * @ORM\Column(name="formejuridique", type="string", length=255, nullable=false)
     */
    private string $formejuridique = '';

    /**
     * @ORM\Column(name="villeenregistrement", type="string", length=50, nullable=true)
     */
    private ?string $villeenregistrement = null;

    /**
     * @ORM\Column(name="motifNonIndNum", type="integer", nullable=true)
     */
    private ?int $motifnonindnum = null;

    /**
     * @ORM\Column(name="ordreProfOuAgrement", type="string", length=30, nullable=true)
     */
    private ?string $ordreprofouagrement = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateConstSociete", type="date", nullable=true)*/
    private $dateconstsociete;

    /**
     * @ORM\Column(name="nomOrgInscription", type="string", length=30, nullable=true)
     */
    private ?string $nomorginscription = null;

    /**
     * @ORM\Column(name="adrOrgInscription", type="string", length=80, nullable=true)
     */
    private ?string $adrorginscription = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateConstAssoc", type="date", nullable=true)*/
    private $dateconstassoc;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateConstAssocEtrangere", type="date", nullable=true)*/
    private $dateconstassocetrangere;

    /**
     * @ORM\Column(name="nomPersonnePublique", type="string", length=30, nullable=true)
     */
    private ?string $nompersonnepublique = null;

    /**
     * @ORM\Column(name="nationalite", type="string", length=2, nullable=true)
     */
    private ?string $nationalite = null;

    /**
     * @ORM\Column(name="redressement", type="integer", nullable=true)
     */
    private ?int $redressement = null;

    /**
     * @ORM\Column(name="paysenregistrement", type="string", length=50, nullable=true)
     */
    private ?string $paysenregistrement = null;

    /**
     * @ORM\Column(name="sirenEtranger", type="string", length=20, nullable=true)
     */
    private ?string $sirenetranger = null;

    /**
     * @ORM\Column(name="numAssoEtrangere", type="string", length=20, nullable=true)
     */
    private ?string $numassoetrangere = null;

    /**
     * @ORM\Column(name="debutExerciceGlob1", type="string", length=15, nullable=true)
     */
    private ?string $debutexerciceglob1 = '';

    /**
     * @ORM\Column(name="finExerciceGlob1", type="string", length=15, nullable=true)
     */
    private ?string $finexerciceglob1 = '';

    /**
     * @ORM\Column(name="debutExerciceGlob2", type="string", length=15, nullable=true)
     */
    private ?string $debutexerciceglob2 = '';

    /**
     * @ORM\Column(name="finExerciceGlob2", type="string", length=15, nullable=true)
     */
    private ?string $finexerciceglob2 = '';

    /**
     * @ORM\Column(name="debutExerciceGlob3", type="string", length=15, nullable=true)
     */
    private ?string $debutexerciceglob3 = '';

    /**
     * @ORM\Column(name="finExerciceGlob3", type="string", length=15, nullable=true)
     */
    private ?string $finexerciceglob3 = '';

    /**
     * @ORM\Column(name="ventesGlob1", type="string", length=10, nullable=true)
     */
    private ?string $ventesglob1 = '';

    /**
     * @ORM\Column(name="ventesGlob2", type="string", length=10, nullable=true)
     */
    private ?string $ventesglob2 = '';

    /**
     * @ORM\Column(name="ventesGlob3", type="string", length=10, nullable=true)
     */
    private ?string $ventesglob3 = '';

    /**
     * @ORM\Column(name="biensGlob1", type="string", length=10, nullable=true)
     */
    private ?string $biensglob1 = '';

    /**
     * @ORM\Column(name="biensGlob2", type="string", length=10, nullable=true)
     */
    private ?string $biensglob2 = '';

    /**
     * @ORM\Column(name="biensGlob3", type="string", length=10, nullable=true)
     */
    private ?string $biensglob3 = '';

    /**
     * @ORM\Column(name="servicesGlob1", type="string", length=10, nullable=true)
     */
    private ?string $servicesglob1 = '';

    /**
     * @ORM\Column(name="servicesGlob2", type="string", length=10, nullable=true)
     */
    private ?string $servicesglob2 = '';

    /**
     * @ORM\Column(name="servicesGlob3", type="string", length=10, nullable=true)
     */
    private ?string $servicesglob3 = '';

    /**
     * @ORM\Column(name="totalGlob1", type="string", length=10, nullable=true)
     */
    private ?string $totalglob1 = '';

    /**
     * @ORM\Column(name="totalGlob2", type="string", length=10, nullable=true)
     */
    private ?string $totalglob2 = '';

    /**
     * @ORM\Column(name="totalGlob3", type="string", length=10, nullable=true)
     */
    private ?string $totalglob3 = '';

    /**
     * @ORM\Column(name="codeape", type="string", length=20, nullable=false)
     */
    private string $codeape = '';

    /**
     * @ORM\Column(name="libelle_ape", type="string", length=255, nullable=true)
     */
    private ?string $libelleApe = null;

    /**
     * @ORM\Column(name="origine_compte", type="string", length=255, nullable=true)
     */
    private ?string $origineCompte = null;

    /**
     * @ORM\Column(name="telephone", type="string", length=50, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(name="fax", type="string", length=50, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @ORM\Column(name="site_internet", type="string", length=50, nullable=false)
     */
    private string $siteInternet = '';

    /**
     * @ORM\Column(name="description_activite", type="text", length=65535, nullable=true)
     */
    private ?string $descriptionActivite = null;

    /**
     * @ORM\Column(name="activite_domaine_defense", type="text", length=65535, nullable=true)
     */
    private ?string $activiteDomaineDefense = null;

    /**
     * @ORM\Column(name="annee_cloture_exercice1", type="string", length=15, nullable=false)
     */
    private string $anneeClotureExercice1 = '';

    /**
     * @ORM\Column(name="annee_cloture_exercice2", type="string", length=15, nullable=false)
     */
    private string $anneeClotureExercice2 = '';

    /**
     * @ORM\Column(name="annee_cloture_exercice3", type="string", length=15, nullable=false)
     */
    private string $anneeClotureExercice3 = '';

    /**
     * @ORM\Column(name="effectif_moyen1", type="integer", nullable=false)
     */
    private string|int $effectifMoyen1 = '0';

    /**
     * @ORM\Column(name="effectif_moyen2", type="integer", nullable=false)
     */
    private string|int $effectifMoyen2 = '0';

    /**
     * @ORM\Column(name="effectif_moyen3", type="integer", nullable=false)
     */
    private string|int $effectifMoyen3 = '0';

    /**
     * @ORM\Column(name="effectif_encadrement1", type="integer", nullable=false)
     */
    private string|int $effectifEncadrement1 = '0';

    /**
     * @ORM\Column(name="effectif_encadrement2", type="integer", nullable=false)
     */
    private string|int $effectifEncadrement2 = '0';

    /**
     * @ORM\Column(name="effectif_encadrement3", type="integer", nullable=false)
     */
    private string|int $effectifEncadrement3 = '0';

    /**
     * @ORM\Column(name="pme1", type="string", nullable=true)
     */
    private ?string $pme1 = null;

    /**
     * @ORM\Column(name="pme2", type="string", nullable=true)
     */
    private ?string $pme2 = null;

    /**
     * @ORM\Column(name="pme3", type="string", nullable=true)
     */
    private ?string $pme3 = null;

    /**
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=true)
     */
    private ?string $adresse2 = null;

    /**
     * @ORM\Column(name="nicSiege", type="string", length=5, nullable=true)
     */
    private ?string $nicSiege = null;

    /**
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)
     */
    private ?string $acronymePays = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=false)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_modification", type="string", length=20, nullable=false)
     */
    private ?string $dateModification = null;

    /**
     * @ORM\Column(name="id_initial", type="integer", nullable=false)
     */
    private string|int $idInitial = '0';

    /**
     * @ORM\Column(name="region", type="string", length=250, nullable=true)
     */
    private ?string $region = null;

    /**
     * @ORM\Column(name="province", type="string", length=250, nullable=true)
     */
    private ?string $province = null;

    /**
     * @ORM\Column(name="telephone2", type="string", length=250, nullable=true)
     */
    private ?string $telephone2 = null;

    /**
     * @ORM\Column(name="telephone3", type="string", length=250, nullable=true)
     */
    private ?string $telephone3 = null;

    /**
     * @ORM\Column(name="cnss", type="string", length=250, nullable=true)
     */
    private ?string $cnss = null;

    /**
     * @ORM\Column(name="rc_num", type="string", length=250, nullable=true)
     */
    private ?string $rcNum = null;

    /**
     * @ORM\Column(name="rc_ville", type="string", length=250, nullable=true)
     */
    private ?string $rcVille = null;

    /**
     * @ORM\Column(name="domaines_activites", type="string", length=255, nullable=true)
     */
    private ?string $domainesActivites = null;

    /**
     * @ORM\Column(name="num_tax", type="string", length=250, nullable=true)
     */
    private ?string $numTax = null;

    /**
     * @ORM\Column(name="documents_commerciaux", type="integer", nullable=true)
     */
    private ?int $documentsCommerciaux = null;

    /**
     * @ORM\Column(name="intitule_documents_commerciaux", type="string", length=255, nullable=true)
     */
    private ?string $intituleDocumentsCommerciaux = null;

    /**
     * @ORM\Column(name="taille_documents_commerciaux", type="string", length=50, nullable=true)
     */
    private ?string $tailleDocumentsCommerciaux = null;

    /**
     * @ORM\Column(name="qualification", type="string", length=255, nullable=true)
     */
    private ?string $qualification = null;

    /**
     * @ORM\Column(name="agrement", type="string", length=255, nullable=true)
     */
    private ?string $agrement = null;

    /**
     * @ORM\Column(name="moyens_technique", type="text", nullable=true)
     */
    private ?string $moyensTechnique = null;

    /**
     * @ORM\Column(name="moyens_humains", type="text", nullable=true)
     */
    private ?string $moyensHumains = null;

    /**
     * @ORM\Column(name="compte_actif", type="integer", nullable=false)
     */
    private string|int $compteActif = '1';

    /**
     * @ORM\Column(name="capital_social", type="string", length=50, nullable=false)
     */
    private ?string $capitalSocial = null;

    /**
     * @ORM\Column(name="ifu", type="string", length=200, nullable=false)
     */
    private string $ifu = '';

    /**
     * @ORM\Column(name="id_agent_createur", type="integer", nullable=false)
     */
    private int $idAgentCreateur = 0;

    /**
     * @ORM\Column(name="nom_agent", type="string", length=200, nullable=false)
     */
    private string $nomAgent = '';

    /**
     * @ORM\Column(name="prenom_agent", type="string", length=200, nullable=false)
     */
    private string $prenomAgent = '';

    /**
     * @ORM\Column(name="adresses_electroniques", type="string", length=255, nullable=true)
     */
    private ?string $adressesElectroniques = null;

    /**
     * @ORM\Column(name="visible_bourse", type="string", length=1, nullable=false)
     */
    private string $visibleBourse = '0';

    /**
     * @ORM\Column(name="type_collaboration", type="string", length=255, nullable=true)
     */
    private ?string $typeCollaboration = null;

    /**
     * @ORM\Column(name="entreprise_EA", type="string", nullable=false)
     */
    private string $entrepriseEa = '0';

    /**
     * @ORM\Column(name="entreprise_SIAE", type="string", nullable=false)
     */
    private string $entrepriseSiae = '0';

    /**
     * @ORM\Column(name="saisie_manuelle", type="string", nullable=true)
     */
    private ?string $saisieManuelle = '1';

    /**
     * @ORM\Column(name="created_from_decision", type="string", nullable=false)
     */
    private string $createdFromDecision = '0';

    /**
     * @ORM\Column(name="id_code_effectif", type="integer", nullable=true)
     */
    private ?int $idCodeEffectif = null;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie_entreprise", type="string", length=255, nullable=true)
     */
    private $categorieEntreprise;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="update_date_api_gouv", type="string", nullable=true)
     */
    private $updateDateApiGouv;

    private $siretSiegeSocial;
    private $formejuridiqueCode;
    private $nombreEtablissementsActifs;
    private $nomCommercial;
    private $procedureCollective;
    private $raisonSociale;
    private $exercices;
    private $mandataires;
    private $etablissementSiege = null;

    /**
     *
     * @ORM\Column(name="id_externe", type="string", nullable=false)*/
    #[Groups('webservice')]
    private string|int $idExterne = '0';

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1,  nullable=true,
     *     options={"default":null, "comment":"etat administratif : A => active, C => Cessée"})
     */
    private $etatAdministratif;

    /**
     * @ORM\Column(type="string", length=10, nullable=true,
     *     options={"default":null, "comment":"timestamp de la date de cessation"})
     */
    private ?string $dateCessation = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\PieceGenereConsultation", mappedBy="entreprise")
     */
    private ?PieceGenereConsultation $pieceGeneree = null;

    /**
     * @return string
     */
    public function getEtatAdministratif()
    {
        return $this->etatAdministratif;
    }

    /**
     * @param $etatAdministratif
     *
     * @return $this
     */
    public function setEtatAdministratif($etatAdministratif)
    {
        $this->etatAdministratif = $etatAdministratif;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateCessation()
    {
        return $this->dateCessation;
    }

    /**
     * @param string $dateCessation
     *
     * @return $this
     */
    public function setDateCessation($dateCessation)
    {
        $this->dateCessation = $dateCessation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormejuridiqueCode()
    {
        return $this->formejuridiqueCode;
    }

    /**
     * @param mixed $formejuridiqueCode
     */
    public function setFormejuridiqueCode($formejuridiqueCode)
    {
        $this->formejuridiqueCode = $formejuridiqueCode;
    }

    /**
     * @return mixed
     */
    public function getNombreEtablissementsActifs()
    {
        return $this->nombreEtablissementsActifs;
    }

    /**
     * @param mixed $nombreEtablissementsActifs
     */
    public function setNombreEtablissementsActifs($nombreEtablissementsActifs)
    {
        $this->nombreEtablissementsActifs = $nombreEtablissementsActifs;
    }

    /**
     * @return mixed
     */
    public function getNomCommercial()
    {
        return $this->nomCommercial;
    }

    /**
     * @param mixed $nomCommercial
     */
    public function setNomCommercial($nomCommercial)
    {
        $this->nomCommercial = $nomCommercial;
    }

    /**
     * @return mixed
     */
    public function getProcedureCollective()
    {
        return $this->procedureCollective;
    }

    /**
     * @param mixed $procedureCollective
     */
    public function setProcedureCollective($procedureCollective)
    {
        $this->procedureCollective = $procedureCollective;
    }

    /**
     * @return mixed
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * @param mixed $raisonSociale
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;
    }

    /**
     * @return mixed
     */
    public function getExercices()
    {
        return $this->exercices;
    }

    /**
     * @param mixed $exercices
     */
    public function setExercices($exercices)
    {
        $this->exercices = $exercices;
    }

    /**
     * @return mixed
     */
    public function getMandataires()
    {
        return $this->mandataires;
    }

    /**
     * @param mixed $mandataires
     */
    public function setMandataires($mandataires)
    {
        $this->mandataires = $mandataires;
    }

    /**
     * Set id.
     *
     * @return int
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set adminId.
     *
     * @param int $adminId
     *
     * @return Entreprise
     */
    public function setAdminId($adminId)
    {
        $this->adminId = $adminId;

        return $this;
    }

    /**
     * Get adminId.
     *
     * @return int
     */
    public function getAdminId()
    {
        return $this->adminId;
    }

    /**
     * Set siren.
     *
     * @param string $siren
     *
     * @return Entreprise
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set repmetiers.
     *
     * @param string $repmetiers
     *
     * @return Entreprise
     */
    public function setRepmetiers($repmetiers)
    {
        $this->repmetiers = $repmetiers;

        return $this;
    }

    /**
     * Get repmetiers.
     *
     * @return string
     */
    public function getRepmetiers()
    {
        return $this->repmetiers;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Entreprise
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Entreprise
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set codepostal.
     *
     * @param string $codepostal
     *
     * @return Entreprise
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * Get codepostal.
     *
     * @return string
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * Set villeadresse.
     *
     * @param string $villeadresse
     *
     * @return Entreprise
     */
    public function setVilleadresse($villeadresse)
    {
        $this->villeadresse = $villeadresse;

        return $this;
    }

    /**
     * Get villeadresse.
     *
     * @return string
     */
    public function getVilleadresse()
    {
        return $this->villeadresse;
    }

    /**
     * Set paysadresse.
     *
     * @param string $paysadresse
     *
     * @return Entreprise
     */
    public function setPaysadresse($paysadresse)
    {
        $this->paysadresse = $paysadresse;

        return $this;
    }

    /**
     * Get paysadresse.
     *
     * @return string
     */
    public function getPaysadresse()
    {
        return $this->paysadresse;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Entreprise
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set taille.
     *
     * @param int $taille
     *
     * @return Entreprise
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;

        return $this;
    }

    /**
     * Get taille.
     *
     * @return int
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * Set formejuridique.
     *
     * @param string $formejuridique
     *
     * @return Entreprise
     */
    public function setFormejuridique($formejuridique)
    {
        $this->formejuridique = $formejuridique;

        return $this;
    }

    /**
     * Get formejuridique.
     *
     * @return string
     */
    public function getFormejuridique()
    {
        return $this->formejuridique;
    }

    /**
     * Set villeenregistrement.
     *
     * @param string $villeenregistrement
     *
     * @return Entreprise
     */
    public function setVilleenregistrement($villeenregistrement)
    {
        $this->villeenregistrement = $villeenregistrement;

        return $this;
    }

    /**
     * Get villeenregistrement.
     *
     * @return string
     */
    public function getVilleenregistrement()
    {
        return $this->villeenregistrement;
    }

    /**
     * Set motifnonindnum.
     *
     * @param int $motifnonindnum
     *
     * @return Entreprise
     */
    public function setMotifnonindnum($motifnonindnum)
    {
        $this->motifnonindnum = $motifnonindnum;

        return $this;
    }

    /**
     * Get motifnonindnum.
     *
     * @return int
     */
    public function getMotifnonindnum()
    {
        return $this->motifnonindnum;
    }

    /**
     * Set ordreprofouagrement.
     *
     * @param string $ordreprofouagrement
     *
     * @return Entreprise
     */
    public function setOrdreprofouagrement($ordreprofouagrement)
    {
        $this->ordreprofouagrement = $ordreprofouagrement;

        return $this;
    }

    /**
     * Get ordreprofouagrement.
     *
     * @return string
     */
    public function getOrdreprofouagrement()
    {
        return $this->ordreprofouagrement;
    }

    /**
     * Set dateconstsociete.
     *
     * @param DateTime $dateconstsociete
     *
     * @return Entreprise
     */
    public function setDateconstsociete($dateconstsociete)
    {
        $this->dateconstsociete = $dateconstsociete;

        return $this;
    }

    /**
     * Get dateconstsociete.
     *
     * @return DateTime
     */
    public function getDateconstsociete()
    {
        return $this->dateconstsociete;
    }

    /**
     * Set nomorginscription.
     *
     * @param string $nomorginscription
     *
     * @return Entreprise
     */
    public function setNomorginscription($nomorginscription)
    {
        $this->nomorginscription = $nomorginscription;

        return $this;
    }

    /**
     * Get nomorginscription.
     *
     * @return string
     */
    public function getNomorginscription()
    {
        return $this->nomorginscription;
    }

    /**
     * Set adrorginscription.
     *
     * @param string $adrorginscription
     *
     * @return Entreprise
     */
    public function setAdrorginscription($adrorginscription)
    {
        $this->adrorginscription = $adrorginscription;

        return $this;
    }

    /**
     * Get adrorginscription.
     *
     * @return string
     */
    public function getAdrorginscription()
    {
        return $this->adrorginscription;
    }

    /**
     * Set dateconstassoc.
     *
     * @param DateTime $dateconstassoc
     *
     * @return Entreprise
     */
    public function setDateconstassoc($dateconstassoc)
    {
        $this->dateconstassoc = $dateconstassoc;

        return $this;
    }

    /**
     * Get dateconstassoc.
     *
     * @return DateTime
     */
    public function getDateconstassoc()
    {
        return $this->dateconstassoc;
    }

    /**
     * Set dateconstassocetrangere.
     *
     * @param DateTime $dateconstassocetrangere
     *
     * @return Entreprise
     */
    public function setDateconstassocetrangere($dateconstassocetrangere)
    {
        $this->dateconstassocetrangere = $dateconstassocetrangere;

        return $this;
    }

    /**
     * Get dateconstassocetrangere.
     *
     * @return DateTime
     */
    public function getDateconstassocetrangere()
    {
        return $this->dateconstassocetrangere;
    }

    /**
     * Set nompersonnepublique.
     *
     * @param string $nompersonnepublique
     *
     * @return Entreprise
     */
    public function setNompersonnepublique($nompersonnepublique)
    {
        $this->nompersonnepublique = $nompersonnepublique;

        return $this;
    }

    /**
     * Get nompersonnepublique.
     *
     * @return string
     */
    public function getNompersonnepublique()
    {
        return $this->nompersonnepublique;
    }

    /**
     * Set nationalite.
     *
     * @param string $nationalite
     *
     * @return Entreprise
     */
    public function setNationalite($nationalite)
    {
        $this->nationalite = $nationalite;

        return $this;
    }

    /**
     * Get nationalite.
     *
     * @return string
     */
    public function getNationalite()
    {
        return $this->nationalite;
    }

    /**
     * Set redressement.
     *
     * @param int $redressement
     *
     * @return Entreprise
     */
    public function setRedressement($redressement)
    {
        $this->redressement = $redressement;

        return $this;
    }

    /**
     * Get redressement.
     *
     * @return int
     */
    public function getRedressement()
    {
        return $this->redressement;
    }

    /**
     * Set paysenregistrement.
     *
     * @param string $paysenregistrement
     *
     * @return Entreprise
     */
    public function setPaysenregistrement($paysenregistrement)
    {
        $this->paysenregistrement = $paysenregistrement;

        return $this;
    }

    /**
     * Get paysenregistrement.
     *
     * @return string
     */
    public function getPaysenregistrement()
    {
        return $this->paysenregistrement;
    }

    /**
     * Set sirenetranger.
     *
     * @param string $sirenetranger
     *
     * @return Entreprise
     */
    public function setSirenetranger($sirenetranger)
    {
        $this->sirenetranger = $sirenetranger;

        return $this;
    }

    /**
     * Get sirenetranger.
     *
     * @return string
     */
    public function getSirenetranger()
    {
        return $this->sirenetranger;
    }

    /**
     * Set numassoetrangere.
     *
     * @param string $numassoetrangere
     *
     * @return Entreprise
     */
    public function setNumassoetrangere($numassoetrangere)
    {
        $this->numassoetrangere = $numassoetrangere;

        return $this;
    }

    /**
     * Get numassoetrangere.
     *
     * @return string
     */
    public function getNumassoetrangere()
    {
        return $this->numassoetrangere;
    }

    /**
     * Set debutexerciceglob1.
     *
     * @param string $debutexerciceglob1
     *
     * @return Entreprise
     */
    public function setDebutexerciceglob1($debutexerciceglob1)
    {
        $this->debutexerciceglob1 = $debutexerciceglob1;

        return $this;
    }

    /**
     * Get debutexerciceglob1.
     *
     * @return string
     */
    public function getDebutexerciceglob1()
    {
        return $this->debutexerciceglob1;
    }

    /**
     * Set finexerciceglob1.
     *
     * @param string $finexerciceglob1
     *
     * @return Entreprise
     */
    public function setFinexerciceglob1($finexerciceglob1)
    {
        $this->finexerciceglob1 = $finexerciceglob1;

        return $this;
    }

    /**
     * Get finexerciceglob1.
     *
     * @return string
     */
    public function getFinexerciceglob1()
    {
        return $this->finexerciceglob1;
    }

    /**
     * Set debutexerciceglob2.
     *
     * @param string $debutexerciceglob2
     *
     * @return Entreprise
     */
    public function setDebutexerciceglob2($debutexerciceglob2)
    {
        $this->debutexerciceglob2 = $debutexerciceglob2;

        return $this;
    }

    /**
     * Get debutexerciceglob2.
     *
     * @return string
     */
    public function getDebutexerciceglob2()
    {
        return $this->debutexerciceglob2;
    }

    /**
     * Set finexerciceglob2.
     *
     * @param string $finexerciceglob2
     *
     * @return Entreprise
     */
    public function setFinexerciceglob2($finexerciceglob2)
    {
        $this->finexerciceglob2 = $finexerciceglob2;

        return $this;
    }

    /**
     * Get finexerciceglob2.
     *
     * @return string
     */
    public function getFinexerciceglob2()
    {
        return $this->finexerciceglob2;
    }

    /**
     * Set debutexerciceglob3.
     *
     * @param string $debutexerciceglob3
     *
     * @return Entreprise
     */
    public function setDebutexerciceglob3($debutexerciceglob3)
    {
        $this->debutexerciceglob3 = $debutexerciceglob3;

        return $this;
    }

    /**
     * Get debutexerciceglob3.
     *
     * @return string
     */
    public function getDebutexerciceglob3()
    {
        return $this->debutexerciceglob3;
    }

    /**
     * Set finexerciceglob3.
     *
     * @param string $finexerciceglob3
     *
     * @return Entreprise
     */
    public function setFinexerciceglob3($finexerciceglob3)
    {
        $this->finexerciceglob3 = $finexerciceglob3;

        return $this;
    }

    /**
     * Get finexerciceglob3.
     *
     * @return string
     */
    public function getFinexerciceglob3()
    {
        return $this->finexerciceglob3;
    }

    /**
     * Set ventesglob1.
     *
     * @param string $ventesglob1
     *
     * @return Entreprise
     */
    public function setVentesglob1($ventesglob1)
    {
        $this->ventesglob1 = $ventesglob1;

        return $this;
    }

    /**
     * Get ventesglob1.
     *
     * @return string
     */
    public function getVentesglob1()
    {
        return $this->ventesglob1;
    }

    /**
     * Set ventesglob2.
     *
     * @param string $ventesglob2
     *
     * @return Entreprise
     */
    public function setVentesglob2($ventesglob2)
    {
        $this->ventesglob2 = $ventesglob2;

        return $this;
    }

    /**
     * Get ventesglob2.
     *
     * @return string
     */
    public function getVentesglob2()
    {
        return $this->ventesglob2;
    }

    /**
     * Set ventesglob3.
     *
     * @param string $ventesglob3
     *
     * @return Entreprise
     */
    public function setVentesglob3($ventesglob3)
    {
        $this->ventesglob3 = $ventesglob3;

        return $this;
    }

    /**
     * Get ventesglob3.
     *
     * @return string
     */
    public function getVentesglob3()
    {
        return $this->ventesglob3;
    }

    /**
     * Set biensglob1.
     *
     * @param string $biensglob1
     *
     * @return Entreprise
     */
    public function setBiensglob1($biensglob1)
    {
        $this->biensglob1 = $biensglob1;

        return $this;
    }

    /**
     * Get biensglob1.
     *
     * @return string
     */
    public function getBiensglob1()
    {
        return $this->biensglob1;
    }

    /**
     * Set biensglob2.
     *
     * @param string $biensglob2
     *
     * @return Entreprise
     */
    public function setBiensglob2($biensglob2)
    {
        $this->biensglob2 = $biensglob2;

        return $this;
    }

    /**
     * Get biensglob2.
     *
     * @return string
     */
    public function getBiensglob2()
    {
        return $this->biensglob2;
    }

    /**
     * Set biensglob3.
     *
     * @param string $biensglob3
     *
     * @return Entreprise
     */
    public function setBiensglob3($biensglob3)
    {
        $this->biensglob3 = $biensglob3;

        return $this;
    }

    /**
     * Get biensglob3.
     *
     * @return string
     */
    public function getBiensglob3()
    {
        return $this->biensglob3;
    }

    /**
     * Set servicesglob1.
     *
     * @param string $servicesglob1
     *
     * @return Entreprise
     */
    public function setServicesglob1($servicesglob1)
    {
        $this->servicesglob1 = $servicesglob1;

        return $this;
    }

    /**
     * Get servicesglob1.
     *
     * @return string
     */
    public function getServicesglob1()
    {
        return $this->servicesglob1;
    }

    /**
     * Set servicesglob2.
     *
     * @param string $servicesglob2
     *
     * @return Entreprise
     */
    public function setServicesglob2($servicesglob2)
    {
        $this->servicesglob2 = $servicesglob2;

        return $this;
    }

    /**
     * Get servicesglob2.
     *
     * @return string
     */
    public function getServicesglob2()
    {
        return $this->servicesglob2;
    }

    /**
     * Set servicesglob3.
     *
     * @param string $servicesglob3
     *
     * @return Entreprise
     */
    public function setServicesglob3($servicesglob3)
    {
        $this->servicesglob3 = $servicesglob3;

        return $this;
    }

    /**
     * Get servicesglob3.
     *
     * @return string
     */
    public function getServicesglob3()
    {
        return $this->servicesglob3;
    }

    /**
     * Set totalglob1.
     *
     * @param string $totalglob1
     *
     * @return Entreprise
     */
    public function setTotalglob1($totalglob1)
    {
        $this->totalglob1 = $totalglob1;

        return $this;
    }

    /**
     * Get totalglob1.
     *
     * @return string
     */
    public function getTotalglob1()
    {
        return $this->totalglob1;
    }

    /**
     * Set totalglob2.
     *
     * @param string $totalglob2
     *
     * @return Entreprise
     */
    public function setTotalglob2($totalglob2)
    {
        $this->totalglob2 = $totalglob2;

        return $this;
    }

    /**
     * Get totalglob2.
     *
     * @return string
     */
    public function getTotalglob2()
    {
        return $this->totalglob2;
    }

    /**
     * Set totalglob3.
     *
     * @param string $totalglob3
     *
     * @return Entreprise
     */
    public function setTotalglob3($totalglob3)
    {
        $this->totalglob3 = $totalglob3;

        return $this;
    }

    /**
     * Get totalglob3.
     *
     * @return string
     */
    public function getTotalglob3()
    {
        return $this->totalglob3;
    }

    /**
     * Set codeape.
     *
     * @param string $codeape
     *
     * @return Entreprise
     */
    public function setCodeape($codeape)
    {
        $this->codeape = $codeape;

        return $this;
    }

    /**
     * Get codeape.
     *
     * @return string
     */
    public function getCodeape()
    {
        return $this->codeape;
    }

    /**
     * Set libelleApe.
     *
     * @param string $libelleApe
     *
     * @return Entreprise
     */
    public function setLibelleApe($libelleApe)
    {
        $this->libelleApe = $libelleApe;

        return $this;
    }

    /**
     * Get libelleApe.
     *
     * @return string
     */
    public function getLibelleApe()
    {
        return $this->libelleApe;
    }

    /**
     * Set origineCompte.
     *
     * @param string $origineCompte
     *
     * @return Entreprise
     */
    public function setOrigineCompte($origineCompte)
    {
        $this->origineCompte = $origineCompte;

        return $this;
    }

    /**
     * Get origineCompte.
     *
     * @return string
     */
    public function getOrigineCompte()
    {
        return $this->origineCompte;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Entreprise
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Entreprise
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set siteInternet.
     *
     * @param string $siteInternet
     *
     * @return Entreprise
     */
    public function setSiteInternet($siteInternet)
    {
        $this->siteInternet = $siteInternet;

        return $this;
    }

    /**
     * Get siteInternet.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getSiteInternet()
    {
        return $this->siteInternet;
    }

    /**
     * Set descriptionActivite.
     *
     * @param string $descriptionActivite
     *
     * @return Entreprise
     */
    public function setDescriptionActivite($descriptionActivite)
    {
        $this->descriptionActivite = $descriptionActivite;

        return $this;
    }

    /**
     * Get descriptionActivite.
     *
     * @return string
     */
    public function getDescriptionActivite()
    {
        return $this->descriptionActivite;
    }

    /**
     * Set activiteDomaineDefense.
     *
     * @param string $activiteDomaineDefense
     *
     * @return Entreprise
     */
    public function setActiviteDomaineDefense($activiteDomaineDefense)
    {
        $this->activiteDomaineDefense = $activiteDomaineDefense;

        return $this;
    }

    /**
     * Get activiteDomaineDefense.
     *
     * @return string
     */
    public function getActiviteDomaineDefense()
    {
        return $this->activiteDomaineDefense;
    }

    /**
     * Set anneeClotureExercice1.
     *
     * @param string $anneeClotureExercice1
     *
     * @return Entreprise
     */
    public function setAnneeClotureExercice1($anneeClotureExercice1)
    {
        $this->anneeClotureExercice1 = $anneeClotureExercice1;

        return $this;
    }

    /**
     * Get anneeClotureExercice1.
     *
     * @return string
     */
    public function getAnneeClotureExercice1()
    {
        return $this->anneeClotureExercice1;
    }

    /**
     * Set anneeClotureExercice2.
     *
     * @param string $anneeClotureExercice2
     *
     * @return Entreprise
     */
    public function setAnneeClotureExercice2($anneeClotureExercice2)
    {
        $this->anneeClotureExercice2 = $anneeClotureExercice2;

        return $this;
    }

    /**
     * Get anneeClotureExercice2.
     *
     * @return string
     */
    public function getAnneeClotureExercice2()
    {
        return $this->anneeClotureExercice2;
    }

    /**
     * Set anneeClotureExercice3.
     *
     * @param string $anneeClotureExercice3
     *
     * @return Entreprise
     */
    public function setAnneeClotureExercice3($anneeClotureExercice3)
    {
        $this->anneeClotureExercice3 = $anneeClotureExercice3;

        return $this;
    }

    /**
     * Get anneeClotureExercice3.
     *
     * @return string
     */
    public function getAnneeClotureExercice3()
    {
        return $this->anneeClotureExercice3;
    }

    /**
     * Set effectifMoyen1.
     *
     * @param int $effectifMoyen1
     *
     * @return Entreprise
     */
    public function setEffectifMoyen1($effectifMoyen1)
    {
        $this->effectifMoyen1 = $effectifMoyen1;

        return $this;
    }

    /**
     * Get effectifMoyen1.
     *
     * @return int
     */
    public function getEffectifMoyen1()
    {
        return $this->effectifMoyen1;
    }

    /**
     * Set effectifMoyen2.
     *
     * @param int $effectifMoyen2
     *
     * @return Entreprise
     */
    public function setEffectifMoyen2($effectifMoyen2)
    {
        $this->effectifMoyen2 = $effectifMoyen2;

        return $this;
    }

    /**
     * Get effectifMoyen2.
     *
     * @return int
     */
    public function getEffectifMoyen2()
    {
        return $this->effectifMoyen2;
    }

    /**
     * Set effectifMoyen3.
     *
     * @param int $effectifMoyen3
     *
     * @return Entreprise
     */
    public function setEffectifMoyen3($effectifMoyen3)
    {
        $this->effectifMoyen3 = $effectifMoyen3;

        return $this;
    }

    /**
     * Get effectifMoyen3.
     *
     * @return int
     */
    public function getEffectifMoyen3()
    {
        return $this->effectifMoyen3;
    }

    /**
     * Set effectifMoyen3.
     *
     * @param int $effectifMoyen3
     *
     * @return Entreprise
     */
    public function setEffectif($effectifMoyen3)
    {
        $this->effectifMoyen3 = $effectifMoyen3;

        return $this;
    }

    /**
     * Get effectifMoyen3.
     *
     * @return int*/
    #[Groups('webservice')]
    public function getEffectif()
    {
        return $this->effectifMoyen3;
    }

    /**
     * Set effectifEncadrement1.
     *
     * @param int $effectifEncadrement1
     *
     * @return Entreprise
     */
    public function setEffectifEncadrement1($effectifEncadrement1)
    {
        $this->effectifEncadrement1 = $effectifEncadrement1;

        return $this;
    }

    /**
     * Get effectifEncadrement1.
     *
     * @return int
     */
    public function getEffectifEncadrement1()
    {
        return $this->effectifEncadrement1;
    }

    /**
     * Set effectifEncadrement2.
     *
     * @param int $effectifEncadrement2
     *
     * @return Entreprise
     */
    public function setEffectifEncadrement2($effectifEncadrement2)
    {
        $this->effectifEncadrement2 = $effectifEncadrement2;

        return $this;
    }

    /**
     * Get effectifEncadrement2.
     *
     * @return int
     */
    public function getEffectifEncadrement2()
    {
        return $this->effectifEncadrement2;
    }

    /**
     * Set effectifEncadrement3.
     *
     * @param int $effectifEncadrement3
     *
     * @return Entreprise
     */
    public function setEffectifEncadrement3($effectifEncadrement3)
    {
        $this->effectifEncadrement3 = $effectifEncadrement3;

        return $this;
    }

    /**
     * Get effectifEncadrement3.
     *
     * @return int
     */
    public function getEffectifEncadrement3()
    {
        return $this->effectifEncadrement3;
    }

    /**
     * Set pme1.
     *
     * @param string $pme1
     *
     * @return Entreprise
     */
    public function setPme1($pme1)
    {
        $this->pme1 = $pme1;

        return $this;
    }

    /**
     * Get pme1.
     *
     * @return string
     */
    public function getPme1()
    {
        return $this->pme1;
    }

    /**
     * Set pme2.
     *
     * @param string $pme2
     *
     * @return Entreprise
     */
    public function setPme2($pme2)
    {
        $this->pme2 = $pme2;

        return $this;
    }

    /**
     * Get pme2.
     *
     * @return string
     */
    public function getPme2()
    {
        return $this->pme2;
    }

    /**
     * Set pme3.
     *
     * @param string $pme3
     *
     * @return Entreprise
     */
    public function setPme3($pme3)
    {
        $this->pme3 = $pme3;

        return $this;
    }

    /**
     * Get pme3.
     *
     * @return string
     */
    public function getPme3()
    {
        return $this->pme3;
    }

    /**
     * Set adresse2.
     *
     * @param string $adresse2
     *
     * @return Entreprise
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2.
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set nicSiege.
     *
     * @param string $nicSiege
     *
     * @return Entreprise
     */
    public function setNicSiege($nicSiege)
    {
        $this->nicSiege = $nicSiege;

        return $this;
    }

    /**
     * Get nicSiege.
     *
     * @return string
     */
    public function getNicSiege()
    {
        return $this->nicSiege;
    }

    /**
     * Set acronymePays.
     *
     * @param string $acronymePays
     *
     * @return Entreprise
     */
    public function setAcronymePays($acronymePays)
    {
        $this->acronymePays = $acronymePays;

        return $this;
    }

    /**
     * Get acronymePays.
     *
     * @return string
     */
    public function getAcronymePays()
    {
        return $this->acronymePays;
    }

    /**
     * Set dateCreation.
     *
     * @param string $dateCreation
     *
     * @return Entreprise
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     *
     * @return Entreprise
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set idInitial.
     *
     * @param int $idInitial
     *
     * @return Entreprise
     */
    public function setIdInitial($idInitial)
    {
        $this->idInitial = $idInitial;

        return $this;
    }

    /**
     * Get idInitial.
     *
     * @return int
     */
    public function getIdInitial()
    {
        return $this->idInitial;
    }

    /**
     * Set region.
     *
     * @param string $region
     *
     * @return Entreprise
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set province.
     *
     * @param string $province
     *
     * @return Entreprise
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province.
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set telephone2.
     *
     * @param string $telephone2
     *
     * @return Entreprise
     */
    public function setTelephone2($telephone2)
    {
        $this->telephone2 = $telephone2;

        return $this;
    }

    /**
     * Get telephone2.
     *
     * @return string
     */
    public function getTelephone2()
    {
        return $this->telephone2;
    }

    /**
     * Set telephone3.
     *
     * @param string $telephone3
     *
     * @return Entreprise
     */
    public function setTelephone3($telephone3)
    {
        $this->telephone3 = $telephone3;

        return $this;
    }

    /**
     * Get telephone3.
     *
     * @return string
     */
    public function getTelephone3()
    {
        return $this->telephone3;
    }

    /**
     * Set cnss.
     *
     * @param string $cnss
     *
     * @return Entreprise
     */
    public function setCnss($cnss)
    {
        $this->cnss = $cnss;

        return $this;
    }

    /**
     * Get cnss.
     *
     * @return string
     */
    public function getCnss()
    {
        return $this->cnss;
    }

    /**
     * Set rcNum.
     *
     * @param string $rcNum
     *
     * @return Entreprise
     */
    public function setRcNum($rcNum)
    {
        $this->rcNum = $rcNum;

        return $this;
    }

    /**
     * Get rcNum.
     *
     * @return string
     */
    public function getRcNum()
    {
        return $this->rcNum;
    }

    /**
     * Set rcVille.
     *
     * @param string $rcVille
     *
     * @return Entreprise
     */
    public function setRcVille($rcVille)
    {
        $this->rcVille = $rcVille;

        return $this;
    }

    /**
     * Get rcVille.
     *
     * @return string
     */
    public function getRcVille()
    {
        return $this->rcVille;
    }

    /**
     * Set domainesActivites.
     *
     * @param string $domainesActivites
     *
     * @return Entreprise
     */
    public function setDomainesActivites($domainesActivites)
    {
        $this->domainesActivites = $domainesActivites;

        return $this;
    }

    /**
     * Get domainesActivites.
     *
     * @return string
     */
    public function getDomainesActivites()
    {
        return $this->domainesActivites;
    }

    /**
     * Set numTax.
     *
     * @param string $numTax
     *
     * @return Entreprise
     */
    public function setNumTax($numTax)
    {
        $this->numTax = $numTax;

        return $this;
    }

    /**
     * Get numTax.
     *
     * @return string
     */
    public function getNumTax()
    {
        return $this->numTax;
    }

    /**
     * Set documentsCommerciaux.
     *
     * @param int $documentsCommerciaux
     *
     * @return Entreprise
     */
    public function setDocumentsCommerciaux($documentsCommerciaux)
    {
        $this->documentsCommerciaux = $documentsCommerciaux;

        return $this;
    }

    /**
     * Get documentsCommerciaux.
     *
     * @return int
     */
    public function getDocumentsCommerciaux()
    {
        return $this->documentsCommerciaux;
    }

    /**
     * Set intituleDocumentsCommerciaux.
     *
     * @param string $intituleDocumentsCommerciaux
     *
     * @return Entreprise
     */
    public function setIntituleDocumentsCommerciaux($intituleDocumentsCommerciaux)
    {
        $this->intituleDocumentsCommerciaux = $intituleDocumentsCommerciaux;

        return $this;
    }

    /**
     * Get intituleDocumentsCommerciaux.
     *
     * @return string
     */
    public function getIntituleDocumentsCommerciaux()
    {
        return $this->intituleDocumentsCommerciaux;
    }

    /**
     * Set tailleDocumentsCommerciaux.
     *
     * @param string $tailleDocumentsCommerciaux
     *
     * @return Entreprise
     */
    public function setTailleDocumentsCommerciaux($tailleDocumentsCommerciaux)
    {
        $this->tailleDocumentsCommerciaux = $tailleDocumentsCommerciaux;

        return $this;
    }

    /**
     * Get tailleDocumentsCommerciaux.
     *
     * @return string
     */
    public function getTailleDocumentsCommerciaux()
    {
        return $this->tailleDocumentsCommerciaux;
    }

    /**
     * Set qualification.
     *
     * @param string $qualification
     *
     * @return Entreprise
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification.
     *
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Set agrement.
     *
     * @param string $agrement
     *
     * @return Entreprise
     */
    public function setAgrement($agrement)
    {
        $this->agrement = $agrement;

        return $this;
    }

    /**
     * Get agrement.
     *
     * @return string
     */
    public function getAgrement()
    {
        return $this->agrement;
    }

    /**
     * Set moyensTechnique.
     *
     * @param string $moyensTechnique
     *
     * @return Entreprise
     */
    public function setMoyensTechnique($moyensTechnique)
    {
        $this->moyensTechnique = $moyensTechnique;

        return $this;
    }

    /**
     * Get moyensTechnique.
     *
     * @return string
     */
    public function getMoyensTechnique()
    {
        return $this->moyensTechnique;
    }

    /**
     * Set moyensHumains.
     *
     * @param string $moyensHumains
     *
     * @return Entreprise
     */
    public function setMoyensHumains($moyensHumains)
    {
        $this->moyensHumains = $moyensHumains;

        return $this;
    }

    /**
     * Get moyensHumains.
     *
     * @return string
     */
    public function getMoyensHumains()
    {
        return $this->moyensHumains;
    }

    /**
     * Set compteActif.
     *
     * @param int $compteActif
     *
     * @return Entreprise
     */
    public function setCompteActif($compteActif)
    {
        $this->compteActif = $compteActif;

        return $this;
    }

    /**
     * Get compteActif.
     *
     * @return int
     */
    public function getCompteActif()
    {
        return $this->compteActif;
    }

    /**
     * Set capitalSocial.
     *
     * @param string $capitalSocial
     *
     * @return Entreprise
     */
    public function setCapitalSocial($capitalSocial)
    {
        $this->capitalSocial = $capitalSocial;

        return $this;
    }

    /**
     * Get capitalSocial.
     *
     * @return string
     */
    public function getCapitalSocial()
    {
        return $this->capitalSocial;
    }

    /**
     * Set ifu.
     *
     * @param string $ifu
     *
     * @return Entreprise
     */
    public function setIfu($ifu)
    {
        $this->ifu = $ifu;

        return $this;
    }

    /**
     * Get ifu.
     *
     * @return string
     */
    public function getIfu()
    {
        return $this->ifu;
    }

    /**
     * Set idAgentCreateur.
     *
     * @param int $idAgentCreateur
     *
     * @return Entreprise
     */
    public function setIdAgentCreateur($idAgentCreateur)
    {
        $this->idAgentCreateur = $idAgentCreateur;

        return $this;
    }

    /**
     * Get idAgentCreateur.
     *
     * @return int
     */
    public function getIdAgentCreateur()
    {
        return $this->idAgentCreateur;
    }

    /**
     * Set nomAgent.
     *
     * @param string $nomAgent
     *
     * @return Entreprise
     */
    public function setNomAgent($nomAgent)
    {
        $this->nomAgent = $nomAgent;

        return $this;
    }

    /**
     * Get nomAgent.
     *
     * @return string
     */
    public function getNomAgent()
    {
        return $this->nomAgent;
    }

    /**
     * Set prenomAgent.
     *
     * @param string $prenomAgent
     *
     * @return Entreprise
     */
    public function setPrenomAgent($prenomAgent)
    {
        $this->prenomAgent = $prenomAgent;

        return $this;
    }

    /**
     * Get prenomAgent.
     *
     * @return string
     */
    public function getPrenomAgent()
    {
        return $this->prenomAgent;
    }

    /**
     * Set adressesElectroniques.
     *
     * @param string $adressesElectroniques
     *
     * @return Entreprise
     */
    public function setAdressesElectroniques($adressesElectroniques)
    {
        $this->adressesElectroniques = $adressesElectroniques;

        return $this;
    }

    /**
     * Get adressesElectroniques.
     *
     * @return string
     */
    public function getAdressesElectroniques()
    {
        return $this->adressesElectroniques;
    }

    /**
     * Set visibleBourse.
     *
     * @param string $visibleBourse
     *
     * @return Entreprise
     */
    public function setVisibleBourse($visibleBourse)
    {
        $this->visibleBourse = $visibleBourse;

        return $this;
    }

    /**
     * Get visibleBourse.
     *
     * @return string
     */
    public function getVisibleBourse()
    {
        return $this->visibleBourse;
    }

    /**
     * Set typeCollaboration.
     *
     * @param string $typeCollaboration
     *
     * @return Entreprise
     */
    public function setTypeCollaboration($typeCollaboration)
    {
        $this->typeCollaboration = $typeCollaboration;

        return $this;
    }

    /**
     * Get typeCollaboration.
     *
     * @return string
     */
    public function getTypeCollaboration()
    {
        return $this->typeCollaboration;
    }

    /**
     * Set entrepriseEa.
     *
     * @param string $entrepriseEa
     *
     * @return Entreprise
     */
    public function setEntrepriseEa($entrepriseEa)
    {
        $this->entrepriseEa = $entrepriseEa;

        return $this;
    }

    /**
     * Get entrepriseEa.
     *
     * @return string
     */
    public function getEntrepriseEa()
    {
        return $this->entrepriseEa;
    }

    /**
     * Set entrepriseSiae.
     *
     * @param string $entrepriseSiae
     *
     * @return Entreprise
     */
    public function setEntrepriseSiae($entrepriseSiae)
    {
        $this->entrepriseSiae = $entrepriseSiae;

        return $this;
    }

    /**
     * Get entrepriseSiae.
     *
     * @return string
     */
    public function getEntrepriseSiae()
    {
        return $this->entrepriseSiae;
    }

    /**
     * Set saisieManuelle.
     *
     * @param string $saisieManuelle
     *
     * @return Entreprise
     */
    public function setSaisieManuelle($saisieManuelle)
    {
        $this->saisieManuelle = $saisieManuelle;

        return $this;
    }

    /**
     * Get saisieManuelle.
     *
     * @return string
     */
    public function getSaisieManuelle()
    {
        return $this->saisieManuelle;
    }

    /**
     * Set createdFromDecision.
     *
     * @param string $createdFromDecision
     *
     * @return Entreprise
     */
    public function setCreatedFromDecision($createdFromDecision)
    {
        $this->createdFromDecision = $createdFromDecision;

        return $this;
    }

    /**
     * Get createdFromDecision.
     *
     * @return string
     */
    public function getCreatedFromDecision()
    {
        return $this->createdFromDecision;
    }

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
        $this->inscrits = new ArrayCollection();
        $this->etablissements = new ArrayCollection();
        $this->documents = new ArrayCollection();
        $this->responsableengagements = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addInscrit(Inscrit $inscrit)
    {
        $this->inscrits[] = $inscrit;
        $inscrit->setEntreprise($this);

        return $this;
    }

    public function removeInscrit(Inscrit $inscrit)
    {
        $this->inscrits->removeElement($inscrit);
    }

    /**
     * @return ArrayCollection
     */
    public function getInscrits()
    {
        return $this->inscrits;
    }

    /**
     * @return $this
     */
    public function addEtablissement(Etablissement $etablissement)
    {
        $this->etablissements[] = $etablissement;
        $etablissement->setEntreprise($this);

        return $this;
    }

    public function removeEtablissement(Etablissement $etablissement)
    {
        $this->etablissements->removeElement($etablissement);
    }

    /**
     * @return ArrayCollection
     */
    public function getEtablissements()
    {
        return $this->etablissements;
    }

    /**
     * @return $this
     */
    public function addDocument(EntrepriseDocument $document)
    {
        $this->documents[] = $document;
        $document->setEntreprise($this);

        return $this;
    }

    public function removeDocument(EntrepriseDocument $document)
    {
        $this->documents->removeElement($document);
    }

    /**
     * @return ArrayCollection
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * Set idCodeEffectif.
     *
     * @param int $idCodeEffectif
     *
     * @return Entreprise
     */
    public function setIdCodeEffectif($idCodeEffectif)
    {
        $this->idCodeEffectif = $idCodeEffectif;

        return $this;
    }

    /**
     * Get idCodeEffectif.
     *
     * @return int
     */
    public function getIdCodeEffectif()
    {
        return $this->idCodeEffectif;
    }

    /**
     * Get categorie_entreprise.
     *
     * @return string
     */
    public function getCategorieEntreprise()
    {
        return $this->categorieEntreprise;
    }

    /**
     * Set categorie_entreprise.
     *
     * @param string $categorie_entreprise
     *
     * @return Entreprise
     */
    public function setCategorieEntreprise($categorieEntreprise)
    {
        $this->categorieEntreprise = $categorieEntreprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSiretSiegeSocial()
    {
        return $this->siretSiegeSocial;
    }

    /**
     * @param mixed $siretSiegeSocial
     */
    public function setSiretSiegeSocial($siretSiegeSocial)
    {
        $this->siretSiegeSocial = $siretSiegeSocial;
    }

    /**
     * @return null
     */
    public function getEtablissementSiege()
    {
        return $this->etablissementSiege;
    }

    /**
     * @param null $etablissementSiege
     */
    public function setEtablissementSiege($etablissementSiege)
    {
        $this->etablissementSiege = $etablissementSiege;
    }

    /**
     * @return ArrayCollection
     */
    public function getResponsableengagements()
    {
        return $this->responsableengagements;
    }

    /**
     * @param ArrayCollection $responsableengagements
     */
    public function setResponsableengagements($responsableengagements)
    {
        $this->responsableengagements = $responsableengagements;
    }

    /**
     * @param Etablissement $etablissement
     *
     * @return $this
     */
    public function addResponsableengagement(Responsableengagement $responsableengagement)
    {
        $this->responsableengagements[] = $responsableengagement;
        $responsableengagement->setEntreprise($this);

        return $this;
    }

    /**
     * @param Etablissement $etablissement
     */
    public function removeResponsableengagement(Responsableengagement $responsableengagement)
    {
        $this->responsableengagements->removeElement($responsableengagement);
    }

    /**
     * @ORM\PrePersist
     */
    public function createDate()
    {
        $this->dateCreation = date('Y-m-d H:i:s');
        $this->dateModification = date('Y-m-d H:i:s');
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->dateModification = date('Y-m-d H:i:s');
    }

    /**
     * @return $this
     */
    public function addBourse(TBourseCotraitance $bourse)
    {
        $this->bourses[] = $bourse;
        $bourse->setEntreprise($this);

        return $this;
    }

    public function removeBourse(TBourseCotraitance $bourse)
    {
        $this->bourses->removeElement($bourse);
    }

    /**
     * @return ArrayCollection
     */
    public function getBourses()
    {
        return $this->bourses;
    }

    /**
     * Set villeadresse.
     *
     * @param string $villeadresse
     *
     * @return Entreprise*/
    #[Groups('webservice')]
    public function setVille($villeadresse)
    {
        $this->villeadresse = $villeadresse;
        return $this;
    }

    /**
     * Get villeadresse.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getVille()
    {
        return $this->villeadresse;
    }

    /**
     * Set paysadresse.
     *
     * @param string $paysadresse
     *
     * @return Entreprise*/
    #[Groups('webservice')]
    public function setPays($paysadresse)
    {
        $this->paysadresse = $paysadresse;
    }

    /**
     * Get paysadresse.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->paysadresse;
    }

    public function getIdExterne(): int
    {
        return $this->idExterne;
    }

    public function setIdExterne(int $idExterne): Entreprise
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    public function getPieceGeneree(): ?PieceGenereConsultation
    {
        return $this->pieceGeneree;
    }

    public function setPieceGeneree(?PieceGenereConsultation $pieceGenereConsultation): self
    {
        $this->pieceGeneree = $pieceGenereConsultation;

        return $this;
    }

    public function getUpdateDateApiGouv()
    {
        return $this->updateDateApiGouv;
    }

    public function setUpdateDateApiGouv($updateDateApiGouv): self
    {
        $this->updateDateApiGouv = $updateDateApiGouv;

        return $this;
    }
}
