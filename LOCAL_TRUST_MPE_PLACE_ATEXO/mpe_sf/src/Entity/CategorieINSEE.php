<?php

namespace App\Entity;

use App\Repository\CategorieINSEERepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="CategorieINSEE")
 * @ORM\Entity(repositoryClass=CategorieINSEERepository::class)
 */
class CategorieINSEE
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string", length=20)
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleFr;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleEn;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleEs;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleSu;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleDu;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleCz;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleAr;

    /**
     * @ORM\Column(type="string", length=100, options={"default" : ""})
     */
    private $libelleIt;

    /**
     * @ORM\Column(type="string", length=10, nullable=true,
     *     options={"comment": "Code de la catégorie INSEE correspondant"})
     */
    private $code;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getLibelleFr(): ?string
    {
        return $this->libelleFr;
    }

    public function setLibelleFr(string $libelleFr): self
    {
        $this->libelleFr = $libelleFr;

        return $this;
    }

    public function getLibelleEn(): ?string
    {
        return $this->libelleEn;
    }

    public function setLibelleEn(string $libelleEn): self
    {
        $this->libelleEn = $libelleEn;

        return $this;
    }

    public function getLibelleEs(): ?string
    {
        return $this->libelleEs;
    }

    public function setLibelleEs(string $libelleEs): self
    {
        $this->libelleEs = $libelleEs;

        return $this;
    }

    public function getLibelleSu(): ?string
    {
        return $this->libelleSu;
    }

    public function setLibelleSu(string $libelleSu): self
    {
        $this->libelleSu = $libelleSu;

        return $this;
    }

    public function getLibelleDu(): ?string
    {
        return $this->libelleDu;
    }

    public function setLibelleDu(string $libelleDu): self
    {
        $this->libelleDu = $libelleDu;

        return $this;
    }

    public function getLibelleCz(): ?string
    {
        return $this->libelleCz;
    }

    public function setLibelleCz(string $libelleCz): self
    {
        $this->libelleCz = $libelleCz;

        return $this;
    }

    public function getLibelleAr(): ?string
    {
        return $this->libelleAr;
    }

    public function setLibelleAr(string $libelleAr): self
    {
        $this->libelleAr = $libelleAr;

        return $this;
    }

    public function getLibelleIt(): ?string
    {
        return $this->libelleIt;
    }

    public function setLibelleIt(string $libelleIt): self
    {
        $this->libelleIt = $libelleIt;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }
}
