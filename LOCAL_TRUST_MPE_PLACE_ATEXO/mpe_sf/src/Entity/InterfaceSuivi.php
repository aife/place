<?php

namespace App\Entity;

use DateTimeInterface;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\InterfaceSuiviRepository;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @ORM\Entity(repositoryClass=InterfaceSuiviRepository::class)
 */

#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    order: ['id' => 'DESC']
)]
#[ApiFilter(SearchFilter::class, properties: [
    'uuid' => SearchFilter::STRATEGY_EXACT,
    'flux' => SearchFilter::STRATEGY_EXACT,
    'consultation' => SearchFilter::STRATEGY_EXACT,
    'idObjetDestination' => SearchFilter::STRATEGY_EXACT,
    'action' => SearchFilter::STRATEGY_EXACT,
    'statut' => SearchFilter::STRATEGY_EXACT,
    'etape' => SearchFilter::STRATEGY_EXACT,
    'annonce' => SearchFilter::STRATEGY_EXACT,
])]
#[ApiFilter(DateFilter::class, properties: ['dateCreation'])]
#[ApiFilter(OrderFilter::class, properties: [
    'uuid',
    'flux',
    'action',
    'dateCreation',
    'consultation.id',
    'idObjetDestination',
    'statut',
    'message'
])]

class InterfaceSuivi
{
    public const ACTION_PUBLICATION_CONSULTATION = 'PUBLICATION_CONSULTATION',
    ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI = 'MODIFICATION_DATE_LIMITE_REMISE_PLI',
    ACTION_MODIFICATION_DCE = 'MODIFICATION_DCE',
    ACTION_RECUPERATION_NOTIFICATION = 'RECUPERATION_NOTIFICATION',
    ACTION_INTERGRATION_CONSULTATIONS_TNCP = 'INTERGRATION_CONSULTATIONS_TNCP',
    ACTION_PUBLICATION_ANNONCE = 'PUBLICATION_ANNONCE';

    public const STATUS_EN_ATTENTE = 1,
    STATUS_EN_COURS = 2,
    STATUS_FINI = 3,
    STATUS_ERREUR = 4;

    public const STATUS_EN_ATTENTE_TEXT = 'EN_ATTENTE',
    STATUS_EN_COURS_TEXT = 'EN_COURS',
    STATUS_FINI_TEXT = 'FINI',
    STATUS_ERREUR_TEXT = 'ERREUR';

    public const TNCP_INTERFACE = 'TNCP_SORTANTE',
    TNCP_INTERFACE_INPUT = 'TNCP_ENTRANTE',
    ANNONCE_TNCP_INTERFACE = 'ANNONCE_TNCP_SORTANT';

    public const ETAPE_AJOUT_CONSULTATION = 'AJOUT_CONSULTATION',
    ETAPE_AJOUT_LOT = 'AJOUT_LOT',
    ETAPE_AJOUT_DCE = 'AJOUT_DCE',
    ETAPE_VALIDATION_CONSULTATION = 'VALIDATION_CONSULTATION',
    ETAPE_ABONNEMENT_CONSULTATION = 'ABONNEMENT_CONSULTATION',
    ETAPE_RECUPERATION_CONSULTATIONS_TNCP = 'RECUPERATION_CONSULTATIONS_TNCP',
    ETAPE_ANNONCE_SYNCHRO_CONSULTATION = 'ANNONCE_SYNCHRO_CONSULTATION';

    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="uuid")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private UuidInterface $uuid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private string $flux;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private string $action;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    #[ApiProperty(
        openapiContext: [
            'type' => 'integer',
            'enum' => [
                self::STATUS_EN_ATTENTE_TEXT . ' = ' . self::STATUS_EN_ATTENTE,
                self::STATUS_EN_COURS_TEXT . ' = ' . self::STATUS_EN_COURS,
                self::STATUS_FINI_TEXT . ' = ' . self::STATUS_FINI,
                self::STATUS_ERREUR_TEXT . ' = ' . self::STATUS_ERREUR,
            ],
            'example' => 1
        ]
    )]
    private int $statut;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $message;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class)
     * @ORM\JoinColumn(name="id_consultation", referencedColumnName="id")
     */
    private ?Consultation $consultation;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $idObjetDestination;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $dateCreation;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $dateModification;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $messageDetails;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private ?string $etape;

    /**
     * @ORM\ManyToOne(targetEntity=AnnonceBoamp::class, inversedBy="interfaceSuivis")
     * @ORM\JoinColumn(name="annonce_id", referencedColumnName="id_boamp")
     */
    private $annonce;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUuid(): UuidInterface
    {
        return $this->uuid;
    }

    public function setUuid(UuidInterface $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getFlux(): string
    {
        return $this->flux;
    }

    public function setFlux(string $flux): self
    {
        $this->flux = $flux;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getStatut(): int
    {
        return $this->statut;
    }

    public function setStatut(int $statut): self
    {
        $this->statut = $statut;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getIdObjetDestination(): ?string
    {
        return $this->idObjetDestination;
    }

    public function setIdObjetDestination(?string $idObjetDestination): self
    {
        $this->idObjetDestination = $idObjetDestination;

        return $this;
    }

    public function getDateCreation(): DateTimeInterface
    {
        return $this->dateCreation;
    }

    public function setDateCreation(DateTimeInterface $dateCreation): self
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    public function getDateModification(): DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getMessageDetails(): ?string
    {
        return $this->messageDetails;
    }

    public function setMessageDetails(?string $messageDetails): void
    {
        $this->messageDetails = $messageDetails;
    }

    public function getEtape(): ?string
    {
        return $this->etape;
    }

    public function setEtape(?string $etape): self
    {
        $this->etape = $etape;

        return $this;
    }

    public function __clone()
    {
        if ($this->id) {
            $this->id = null;
        }
    }

    public function getAnnonce(): ?AnnonceBoamp
    {
        return $this->annonce;
    }

    public function setAnnonce(?AnnonceBoamp $annonce): self
    {
        $this->annonce = $annonce;

        return $this;
    }
}
