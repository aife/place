<?php

namespace App\Entity\Publicite;

use App\Entity\Organisme;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="CompteMoniteur")
 * @ORM\Entity(repositoryClass="App\Repository\Publicite\CompteMoniteurRepository")
 */
class CompteMoniteur
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
    ];

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme;

    /**
     * @ORM\Column(type="string", name="moniteur_login", length=50)
     */
    private string $moniteurLogin;

    /**
     * @ORM\Column(type="string", name="moniteur_password", length=100)
     */
    private string $moniteurPassword;

    /**
     * @ORM\Column(type="string", name="moniteur_mail", length=100)
     */
    private string $moniteurMail;

    /**
     * @ORM\Column(type="string", name="moniteur_target", length=1)
     */
    private string $moniteurTarget = '0';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): CompteMoniteur
    {
        $this->id = $id;

        return $this;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): CompteMoniteur
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getMoniteurLogin(): string
    {
        return $this->moniteurLogin;
    }

    public function setMoniteurLogin(string $moniteurLogin): CompteMoniteur
    {
        $this->moniteurLogin = $moniteurLogin;

        return $this;
    }

    public function getMoniteurPassword(): string
    {
        return $this->moniteurPassword;
    }

    public function setMoniteurPassword(string $moniteurPassword): CompteMoniteur
    {
        $this->moniteurPassword = $moniteurPassword;

        return $this;
    }

    public function getMoniteurMail(): string
    {
        return $this->moniteurMail;
    }

    public function setMoniteurMail(string $moniteurMail): CompteMoniteur
    {
        $this->moniteurMail = $moniteurMail;

        return $this;
    }

    public function getMoniteurTarget(): string
    {
        return $this->moniteurTarget;
    }

    public function setMoniteurTarget(string $moniteurTarget): CompteMoniteur
    {
        $this->moniteurTarget = $moniteurTarget;

        return $this;
    }
}
