<?php

namespace App\Entity\Publicite;

use Exception;
use App\Entity\Agent\AcheteurPublic;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Table(name="renseignements_boamp", uniqueConstraints={
 *          @UniqueConstraint(name="renseignements_boamp_id",columns={"id"}),
 *          @UniqueConstraint(name="renseignements_boamp_organismeOrg",columns={"acronymeOrg"})
 *      })
 * @ORM\Entity(repositoryClass="App\Repository\Publicite\RenseignementsBoampRepository")
 */
class RenseignementsBoamp
{
    private EntityManager $em;

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=30, name="acronymeOrg")
     */
    private ?string $acronymeOrg = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idType = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $idCompte = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $correspondant = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $adresse = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $cp = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $poste = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $mail = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $url = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $organeChargeProcedure = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getAcronymeOrg(): string
    {
        return $this->acronymeOrg;
    }

    public function setAcronymeOrg(string $acronymeOrg)
    {
        $this->acronymeOrg = $acronymeOrg;
    }

    public function getIdType(): int
    {
        return $this->idType;
    }

    public function setIdType(int $idType)
    {
        $this->idType = $idType;
    }

    public function getIdCompte(): int
    {
        return $this->idCompte;
    }

    public function setIdCompte(int $idCompte)
    {
        $this->idCompte = $idCompte;
    }

    /**
     * @return string
     */
    public function getCorrespondant()
    {
        return $this->correspondant;
    }

    public function setCorrespondant(string $correspondant = null)
    {
        $this->correspondant = $correspondant;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme = null)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse = null)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    public function setCp(string $cp = null)
    {
        $this->cp = $cp;
    }

    /**
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    public function setVille(string $ville = null)
    {
        $this->ville = $ville;
    }

    /**
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    public function setPays(string $pays = null)
    {
        $this->pays = $pays;
    }

    /**
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone = null)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getPoste()
    {
        return $this->poste;
    }

    public function setPoste(string $poste = null)
    {
        $this->poste = $poste;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    public function setFax(string $fax = null)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    public function setMail(string $mail = null)
    {
        $this->mail = $mail;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl(string $url = null)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getOrganeChargeProcedure()
    {
        return $this->organeChargeProcedure;
    }

    public function setOrganeChargeProcedure(string $organeChargeProcedure = null)
    {
        $this->organeChargeProcedure = $organeChargeProcedure;
    }

    /**
     * Cette fonction permet de copier l'objet lui attribuant un nouvel acheteur public de destination passé en paramètre.
     *
     * @param AcheteurPublic $acheteurPublic de destination
     *
     * @return bool|AcheteurPublic false si le clone n'a pas réussi, sinon le nouvel objet AcheteurPublic créé
     *
     * @throws Exception
     */
    public function copier(AcheteurPublic $acheteurPublic): bool|AcheteurPublic
    {
        if ($acheteurPublic instanceof AcheteurPublic) {
            $renseignementsBoamp = clone $this;
            $renseignementsBoamp->setId($this->getMaxId() + 1);
            $renseignementsBoamp->setIdCompte($acheteurPublic->getId());
            $renseignementsBoamp->setAcronymeOrg($acheteurPublic->getOrganisme());
            $this->em->persist($renseignementsBoamp);
            $this->em->flush();

            return $renseignementsBoamp;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(RenseignementsBoamp::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }
}
