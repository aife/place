<?php

namespace App\Entity\Publicite;

use App\Entity\Organisme;
use App\Entity\Service;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Groupe_Moniteur")
 * @ORM\Entity(repositoryClass="App\Repository\Publicite\GroupeMoniteurRepository")
 */
class GroupeMoniteur
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
        'serviceId' => [
            'class' => Service::class,
            'isObject' => false,
            'property' => 'id',
            'migrationService' => 'atexo.service.migration.service',
        ],
    ];

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(type="integer", name="old_service_id")
     */
    private ?int $oldServiceId = null;

    /**
     * @ORM\Column(type="string", name="Identifiant", length=50)
     */
    private string $identifiant;

    /**
     * @ORM\Column(type="string", name="Mdp", length=50)
     */
    private string $mdp;

    /**
     * @ORM\Column(type="string", name="Num_Abonnement", length=50)
     */
    private ?string $numAbonnement = null;

    /**
     * @ORM\Column(type="string", name="Num_Abonne", length=50)
     */
    private ?string $numAbonne = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): GroupeMoniteur
    {
        $this->id = $id;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): GroupeMoniteur
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }

    public function setServiceId(?int $serviceId): GroupeMoniteur
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }

    public function setOldServiceId(?int $oldServiceId): GroupeMoniteur
    {
        $this->oldServiceId = $oldServiceId;

        return $this;
    }

    public function getIdentifiant(): ?string
    {
        return $this->identifiant;
    }

    public function setIdentifiant(?string $identifiant): GroupeMoniteur
    {
        $this->identifiant = $identifiant;

        return $this;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(?string $mdp): GroupeMoniteur
    {
        $this->mdp = $mdp;

        return $this;
    }

    public function getNumAbonnement(): ?string
    {
        return $this->numAbonnement;
    }

    public function setNumAbonnement(?string $numAbonnement): GroupeMoniteur
    {
        $this->numAbonnement = $numAbonnement;

        return $this;
    }

    public function getNumAbonne(): ?string
    {
        return $this->numAbonne;
    }

    public function setNumAbonne(?string $numAbonne): GroupeMoniteur
    {
        $this->numAbonne = $numAbonne;

        return $this;
    }
}
