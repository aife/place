<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TRoleJuridique.
 *
 * @ORM\Table(name="t_role_juridique")
 * @ORM\Entity
 */
class TRoleJuridique
{
    /**
     *
     * @ORM\Column(name="id_role_juridique", type="integer", precision=0, scale=0, nullable=false, unique=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idRoleJuridique;

    /**
     * @ORM\Column(name="libelle_role_juridique", type="string", length=255, precision=0, scale=0, nullable=true, unique=false)
     */
    private ?string $libelleRoleJuridique = null;

    /**
     * Get idRoleJuridique.
     *
     * @return int
     */
    public function getIdRoleJuridique()
    {
        return $this->idRoleJuridique;
    }

    /**
     * Set libelleRoleJuridique.
     *
     * @param string $libelleRoleJuridique
     *
     * @return TRoleJuridique
     */
    public function setLibelleRoleJuridique($libelleRoleJuridique)
    {
        $this->libelleRoleJuridique = $libelleRoleJuridique;

        return $this;
    }

    /**
     * Get libelleRoleJuridique.
     *
     * @return string
     */
    public function getLibelleRoleJuridique()
    {
        return $this->libelleRoleJuridique;
    }
}
