<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\FormePrixInput;
use App\Dto\Output\FormePrixOutput;
use App\Repository\FormePrixRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_forme_prix" )
 * @ORM\Entity(repositoryClass=FormePrixRepository::class)
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['output' => FormePrixOutput::class],
        'post' => ['input' => FormePrixInput::class, 'output' => FormePrixOutput::class],
    ],
    itemOperations: ['get' => ['output' => FormePrixOutput::class]],
)]
class FormePrix
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(name="id_forme_prix", type="integer")
     */
    private ?int $idFormePrix;

    /**
     * @ORM\Column(name="forme_prix", type="string", length=2)
     */
    private ?string $formePrix;

    /**
     * @ORM\Column(name="pf_estimation_ht", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $pfEstimationHt = null;

    /**
     * @ORM\Column(name="pf_estimation_ttc", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $pfEstimationTtc = null;

    /**
     * @ORM\Column(name="pf_date_valeur", type="date", nullable=true)
     */
    private ?\DateTimeInterface $pfDateValeur = null;

    /**
     * @ORM\Column(name="id_min_max", type="integer", nullable=true)
     */
    private ?int $idMinMax = null;

    /**
     * @ORM\Column(name="modalite", type="string", length=50, nullable=true)
     */
    private ?string $modalite = null;

    /**
     * @ORM\Column(name="pu_min", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puMin = null;

    /**
     * @ORM\Column(name="pu_max", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puMax = null;

    /**
     * @ORM\Column(name="pu_min_ttc", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puMinTtc = null;

    /**
     * @ORM\Column(name="pu_max_ttc", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puMaxTtc = null;

    /**
     * @ORM\Column(name="pu_estimation_ht", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puEstimationHt = null;

    /**
     * @ORM\Column(name="pu_estimation_ttc", type="decimal", precision=30, scale=2, nullable=true)
     */
    private ?string $puEstimationTtc = null;

    /**
     * @ORM\Column(name="pu_date_valeur", type="date", nullable=true)
     */
    private ?\DateTimeInterface $puDateValeur;

    public function getIdFormePrix(): ?int
    {
        return $this->idFormePrix;
    }

    public function getFormePrix(): ?string
    {
        return $this->formePrix;
    }

    public function setFormePrix(string $formePrix): self
    {
        $this->formePrix = $formePrix;

        return $this;
    }

    public function getPfEstimationHt(): ?string
    {
        return $this->pfEstimationHt;
    }

    public function setPfEstimationHt(?string $pfEstimationHt): self
    {
        $this->pfEstimationHt = $pfEstimationHt;

        return $this;
    }

    public function getPfDateValeur(): ?\DateTimeInterface
    {
        return $this->pfDateValeur;
    }

    public function setPfDateValeur(?\DateTimeInterface $pfDateValeur): self
    {
        $this->pfDateValeur = $pfDateValeur;

        return $this;
    }

    public function getIdMinMax(): ?int
    {
        return $this->idMinMax;
    }

    public function setIdMinMax(?int $idMinMax): self
    {
        $this->idMinMax = $idMinMax;

        return $this;
    }

    public function getModalite(): ?string
    {
        return $this->modalite;
    }

    public function setModalite(?string $modalite): self
    {
        $this->modalite = $modalite;

        return $this;
    }

    public function getPuDateValeur(): ?\DateTimeInterface
    {
        return $this->puDateValeur;
    }

    public function setPuDateValeur(?\DateTimeInterface $puDateValeur): self
    {
        $this->puDateValeur = $puDateValeur;

        return $this;
    }

    public function getPfEstimationTtc(): ?string
    {
        return $this->pfEstimationTtc;
    }

    public function setPfEstimationTtc(?string $pfEstimationTtc): self
    {
        $this->pfEstimationTtc = $pfEstimationTtc;

        return $this;
    }

    public function getPuMin(): ?string
    {
        return $this->puMin;
    }

    public function setPuMin(?string $puMin): self
    {
        $this->puMin = $puMin;

        return $this;
    }

    public function getPuMax(): ?string
    {
        return $this->puMax;
    }

    public function setPuMax(?string $puMax): self
    {
        $this->puMax = $puMax;

        return $this;
    }

    public function getPuMinTtc(): ?string
    {
        return $this->puMinTtc;
    }

    public function setPuMinTtc(?string $puMinTtc): self
    {
        $this->puMinTtc = $puMinTtc;

        return $this;
    }

    public function getPuMaxTtc(): ?string
    {
        return $this->puMaxTtc;
    }

    public function setPuMaxTtc(?string $puMaxTtc): self
    {
        $this->puMaxTtc = $puMaxTtc;

        return $this;
    }

    public function getPuEstimationHt(): ?string
    {
        return $this->puEstimationHt;
    }

    public function setPuEstimationHt(?string $puEstimationHt): self
    {
        $this->puEstimationHt = $puEstimationHt;

        return $this;
    }

    public function getPuEstimationTtc(): ?string
    {
        return $this->puEstimationTtc;
    }

    public function setPuEstimationTtc(?string $puEstimationTtc): self
    {
        $this->puEstimationTtc = $puEstimationTtc;

        return $this;
    }
}
