<?php

namespace App\Entity;

use App\Repository\TraductionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="T_Traduction")
 * @ORM\Entity(repositoryClass=TraductionRepository::class)
 */
class Traduction
{
    /**
     * @ORM\Column(name="id_libelle", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idLibelle;

    /**
     * @ORM\Column(type="string", length=2, nullable=false)
     */
    private string $langue;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $libelle;

    public function getIdLibelle(): ?int
    {
        return $this->idLibelle;
    }

    public function getLangue(): string
    {
        return $this->langue;
    }

    public function setLangue(string $langue): self
    {
        $this->langue = $langue;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }
}
