<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\EncherePmi;

/**
 * Enchereentreprisepmi
 *
 * @ORM\Table(name="EnchereEntreprisePmi", indexes={@ORM\Index(name="FK_9A77EC7D2868ECFD", columns={"idEnchere"})})
 * @ORM\Entity(repositoryClass="App\Repository\EnchereEntreprisePmiRepository")
 */
class EnchereEntreprisePmi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private string $organisme = '';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=256, nullable=false)
     */
    private string $nom = '';

    /**
     * @var int|null
     *
     * @ORM\Column(name="numeroAnonyme", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $numeroAnonyme = null;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=256, nullable=false)
     */
    private string $email = '';

    /**
     * @var string
     *
     * @ORM\Column(name="mdp", type="string", length=256, nullable=false)
     */
    private string $mdp = '';

    /**
     * @var float|null
     *
     * @ORM\Column(name="noteTechnique", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $noteTechnique = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idEntreprise", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $identreprise = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="datePing", type="datetime", nullable=false, options={"default"="0000-00-00 00:00:00"})
     */
    private $dateping = '0000-00-00 00:00:00';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nomAgentConnecte", type="string", length=256, nullable=true)
     */
    private ?string $nomAgentConnecte = null;

    /**
     * @var Encherepmi|null
     *
     * @ORM\ManyToOne(targetEntity=EncherePmi::class)
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEnchere", referencedColumnName="id")
     * })
     */
    private ?Encherepmi $idEnchere = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id_enchere", type="integer", nullable=true)
     */
    private $oldIdEnchere;


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getIdEnchere()
    {
        return $this->idEnchere;
    }

    public function setIdEnchere($idEnchere): self
    {
        $this->idEnchere = $idEnchere;

        return $this;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNumeroAnonyme(): ?int
    {
        return $this->numeroAnonyme;
    }

    public function setNumeroAnonyme(?int $numeroAnonyme): self
    {
        $this->numeroAnonyme = $numeroAnonyme;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMdp(): string
    {
        return $this->mdp;
    }

    public function setMdp(string $mdp): self
    {
        $this->mdp = $mdp;

        return $this;
    }

    public function getNoteTechnique(): ?float
    {
        return $this->noteTechnique;
    }

    public function setNoteTechnique(?float $noteTechnique): self
    {
        $this->noteTechnique = $noteTechnique;

        return $this;
    }

    public function getIdEntreprise(): ?int
    {
        return $this->identreprise;
    }

    public function setIdEntreprise(?int $idEntreprise): self
    {
        $this->identreprise = $idEntreprise;

        return $this;
    }

    public function getDatePing(): DateTime
    {
        return $this->dateping;
    }

    public function setDatePing(DateTime $datePing): self
    {
        $this->dateping = $datePing;

        return $this;
    }

    public function getNomAgentConnecte(): ?string
    {
        return $this->nomAgentConnecte;
    }

    public function setNomAgentConnecte(?string $nomAgentConnecte): self
    {
        $this->nomAgentConnecte = $nomAgentConnecte;

        return $this;
    }
}
