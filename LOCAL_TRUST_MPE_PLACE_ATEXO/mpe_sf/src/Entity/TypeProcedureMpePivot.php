<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProcedureMpePivot.
 *
 * @ORM\Table(name="type_procedure_mpe_pivot")
 * @ORM\Entity
 */
class TypeProcedureMpePivot
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="id_type_procedure_mpe", type="integer", nullable=false)
     */
    private ?int $idTypeProcedureMpe = null;

    /**
     * @ORM\Column(name="id_type_procedure_pivot", type="integer", nullable=false)
     */
    private ?int $idTypeProcedurePivot = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedureMpe()
    {
        return $this->idTypeProcedureMpe;
    }

    public function setIdTypeProcedureMpe(int $idTypeProcedureMpe)
    {
        $this->idTypeProcedureMpe = $idTypeProcedureMpe;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedurePivot()
    {
        return $this->idTypeProcedurePivot;
    }

    public function setIdTypeProcedurePivot(int $idTypeProcedurePivot)
    {
        $this->idTypeProcedurePivot = $idTypeProcedurePivot;
    }
}
