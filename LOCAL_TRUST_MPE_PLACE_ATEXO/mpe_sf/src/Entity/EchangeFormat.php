<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class EchangeFormat.
 *
 * @ORM\Table(name="EchangeFormat")
 * @ORM\Entity()
 */
class EchangeFormat
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="libelle", type="string", length=100, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @ORM\Column(name="libelle_fr", type="string", length=100, nullable=false)
     */
    private ?string $libelleFr = null;

    /**
     * @ORM\Column(name="libelle_en", type="string", length=100, nullable=false)
     */
    private ?string $libelleEn = null;

    /**
     * @ORM\Column(name="libelle_es", type="string", length=100, nullable=false)
     */
    private ?string $libelleEs = null;

    /**
     * @ORM\Column(name="libelle_su", type="string", length=100, nullable=false)
     */
    private ?string $libelleSu = null;

    /**
     * @ORM\Column(name="libelle_du", type="string", length=100, nullable=false)
     */
    private ?string $libelleDu = null;

    /**
     * @ORM\Column(name="libelle_cz", type="string", length=100, nullable=false)
     */
    private ?string $libelleCz = null;

    /**
     * @ORM\Column(name="libelle_ar", type="string", length=100, nullable=false)
     */
    private ?string $libelleAr = null;

    /**
     * @ORM\Column(name="libelle_it", type="string", length=100, nullable=false)
     */
    private ?string $libelleIt = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getLibelleFr()
    {
        return $this->libelleFr;
    }

    /**
     * @param string $libelleFr
     */
    public function setLibelleFr($libelleFr)
    {
        $this->libelleFr = $libelleFr;
    }

    /**
     * @return string
     */
    public function getLibelleEn()
    {
        return $this->libelleEn;
    }

    /**
     * @param string $libelleEn
     */
    public function setLibelleEn($libelleEn)
    {
        $this->libelleEn = $libelleEn;
    }

    /**
     * @return string
     */
    public function getLibelleEs()
    {
        return $this->libelleEs;
    }

    /**
     * @param string $libelleEs
     */
    public function setLibelleEs($libelleEs)
    {
        $this->libelleEs = $libelleEs;
    }

    /**
     * @return string
     */
    public function getLibelleSu()
    {
        return $this->libelleSu;
    }

    /**
     * @param string $libelleSu
     */
    public function setLibelleSu($libelleSu)
    {
        $this->libelleSu = $libelleSu;
    }

    /**
     * @return string
     */
    public function getLibelleDu()
    {
        return $this->libelleDu;
    }

    /**
     * @param string $libelleDu
     */
    public function setLibelleDu($libelleDu)
    {
        $this->libelleDu = $libelleDu;
    }

    /**
     * @return string
     */
    public function getLibelleCz()
    {
        return $this->libelleCz;
    }

    /**
     * @param string $libelleCz
     */
    public function setLibelleCz($libelleCz)
    {
        $this->libelleCz = $libelleCz;
    }

    /**
     * @return string
     */
    public function getLibelleAr()
    {
        return $this->libelleAr;
    }

    /**
     * @param string $libelleAr
     */
    public function setLibelleAr($libelleAr)
    {
        $this->libelleAr = $libelleAr;
    }

    /**
     * @return string
     */
    public function getLibelleIt()
    {
        return $this->libelleIt;
    }

    /**
     * @param string $libelleIt
     */
    public function setLibelleIt($libelleIt)
    {
        $this->libelleIt = $libelleIt;
    }
}
