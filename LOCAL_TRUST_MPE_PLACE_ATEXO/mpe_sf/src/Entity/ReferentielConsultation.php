<?php

namespace App\Entity;

use App\Repository\ReferentielConsultationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReferentielConsultationRepository::class)
 * @ORM\Table(name="Referentiel_Consultation")
 */
class ReferentielConsultation
{
    final public const REFERENTIEL_CONSULTATION_ONLY = 0;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $organisme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity=LtReferentiel::class)
     * @ORM\JoinColumn(name="id_Lt_Referentiel", referencedColumnName="id")
     */
    private $ltReferentiel;

    /**
     * @ORM\Column(type="integer")
     */
    private $lot = 0;

    /**
     * @ORM\Column(type="text")
     */
    private $valeurPrincipaleLtReferentiel;

    /**
     * @ORM\Column(type="text")
     */
    private $valeurSecondaireLtReferentiel = '';

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class)
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getReference(): ?int
    {
        return $this->reference;
    }

    public function setReference(?int $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getLtReferentiel(): ?LtReferentiel
    {
        return $this->ltReferentiel;
    }

    public function setLtReferentiel(?LtReferentiel $ltReferentiel): self
    {
        $this->ltReferentiel = $ltReferentiel;

        return $this;
    }

    public function getLot(): ?int
    {
        return $this->lot;
    }

    public function setLot(int $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getValeurPrincipaleLtReferentiel(): ?string
    {
        return $this->valeurPrincipaleLtReferentiel;
    }

    public function setValeurPrincipaleLtReferentiel(string $valeurPrincipaleLtReferentiel): self
    {
        $this->valeurPrincipaleLtReferentiel = $valeurPrincipaleLtReferentiel;

        return $this;
    }

    public function getValeurSecondaireLtReferentiel(): ?string
    {
        return $this->valeurSecondaireLtReferentiel;
    }

    public function setValeurSecondaireLtReferentiel(string $valeurSecondaireLtReferentiel): self
    {
        $this->valeurSecondaireLtReferentiel = $valeurSecondaireLtReferentiel;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }
}
