<?php

namespace App\Entity;

use Stringable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AvisType.
 *
 * @ORM\Table(name="TypeAvis")
 * @ORM\Entity
 */
class AvisType implements Stringable
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="avisType")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_type_avis")
     */
    private Collection $consultations;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="intitule_avis", type="string", length=100, nullable=false)
     */
    private string $intituleAvis = '';

    /**
     * @ORM\Column(name="intitule_avis_fr", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisFr = null;

    /**
     * @ORM\Column(name="intitule_avis_en", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisEn = null;

    /**
     * @ORM\Column(name="intitule_avis_es", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisEs = null;

    /**
     * @ORM\Column(name="intitule_avis_su", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisSu = null;

    /**
     * @ORM\Column(name="intitule_avis_du", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisDu = null;

    /**
     * @ORM\Column(name="intitule_avis_cz", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisCz = null;

    /**
     * @ORM\Column(name="intitule_avis_ar", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisAr = null;

    /**
     * @ORM\Column(name="abbreviation", type="string", length=50, nullable=false)
     */
    private string $abbreviation = '';

    /**
     * @ORM\Column(name="id_type_avis_ANM", type="integer", nullable=false)
     */
    private string|int $idTypeAvisAnm = '0';

    /**
     * @ORM\Column(name="intitule_avis_it", type="string", length=100, nullable=true)
     */
    private ?string $intituleAvisIt = null;

    public function __toString(): string
    {
        return $this->intituleAvis;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set intituleAvis.
     *
     * @param string $intituleAvis
     *
     * @return TypeAvis
     */
    public function setIntituleAvis($intituleAvis)
    {
        $this->intituleAvis = $intituleAvis;

        return $this;
    }

    /**
     * Get intituleAvis.
     *
     * @return string
     */
    public function getIntituleAvis()
    {
        return $this->intituleAvis;
    }

    /**
     * Set intituleAvisFr.
     *
     * @param string $intituleAvisFr
     *
     * @return TypeAvis
     */
    public function setIntituleAvisFr($intituleAvisFr)
    {
        $this->intituleAvisFr = $intituleAvisFr;

        return $this;
    }

    /**
     * Get intituleAvisFr.
     *
     * @return string
     */
    public function getIntituleAvisFr()
    {
        return $this->intituleAvisFr;
    }

    /**
     * Set intituleAvisEn.
     *
     * @param string $intituleAvisEn
     *
     * @return TypeAvis
     */
    public function setIntituleAvisEn($intituleAvisEn)
    {
        $this->intituleAvisEn = $intituleAvisEn;

        return $this;
    }

    /**
     * Get intituleAvisEn.
     *
     * @return string
     */
    public function getIntituleAvisEn()
    {
        return $this->intituleAvisEn;
    }

    /**
     * Set intituleAvisEs.
     *
     * @param string $intituleAvisEs
     *
     * @return TypeAvis
     */
    public function setIntituleAvisEs($intituleAvisEs)
    {
        $this->intituleAvisEs = $intituleAvisEs;

        return $this;
    }

    /**
     * Get intituleAvisEs.
     *
     * @return string
     */
    public function getIntituleAvisEs()
    {
        return $this->intituleAvisEs;
    }

    /**
     * Set intituleAvisSu.
     *
     * @param string $intituleAvisSu
     *
     * @return TypeAvis
     */
    public function setIntituleAvisSu($intituleAvisSu)
    {
        $this->intituleAvisSu = $intituleAvisSu;

        return $this;
    }

    /**
     * Get intituleAvisSu.
     *
     * @return string
     */
    public function getIntituleAvisSu()
    {
        return $this->intituleAvisSu;
    }

    /**
     * Set intituleAvisDu.
     *
     * @param string $intituleAvisDu
     *
     * @return TypeAvis
     */
    public function setIntituleAvisDu($intituleAvisDu)
    {
        $this->intituleAvisDu = $intituleAvisDu;

        return $this;
    }

    /**
     * Get intituleAvisDu.
     *
     * @return string
     */
    public function getIntituleAvisDu()
    {
        return $this->intituleAvisDu;
    }

    /**
     * Set intituleAvisCz.
     *
     * @param string $intituleAvisCz
     *
     * @return TypeAvis
     */
    public function setIntituleAvisCz($intituleAvisCz)
    {
        $this->intituleAvisCz = $intituleAvisCz;

        return $this;
    }

    /**
     * Get intituleAvisCz.
     *
     * @return string
     */
    public function getIntituleAvisCz()
    {
        return $this->intituleAvisCz;
    }

    /**
     * Set intituleAvisAr.
     *
     * @param string $intituleAvisAr
     *
     * @return TypeAvis
     */
    public function setIntituleAvisAr($intituleAvisAr)
    {
        $this->intituleAvisAr = $intituleAvisAr;

        return $this;
    }

    /**
     * Get intituleAvisAr.
     *
     * @return string
     */
    public function getIntituleAvisAr()
    {
        return $this->intituleAvisAr;
    }

    /**
     * Set abbreviation.
     *
     * @param string $abbreviation
     *
     * @return TypeAvis
     */
    public function setAbbreviation($abbreviation)
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * Get abbreviation.
     *
     * @return string
     */
    public function getAbbreviation()
    {
        return $this->abbreviation;
    }

    /**
     * Set idTypeAvisAnm.
     *
     * @param int $idTypeAvisAnm
     *
     * @return TypeAvis
     */
    public function setIdTypeAvisAnm($idTypeAvisAnm)
    {
        $this->idTypeAvisAnm = $idTypeAvisAnm;

        return $this;
    }

    /**
     * Get idTypeAvisAnm.
     *
     * @return int
     */
    public function getIdTypeAvisAnm()
    {
        return $this->idTypeAvisAnm;
    }

    /**
     * Set intituleAvisIt.
     *
     * @param string $intituleAvisIt
     *
     * @return TypeAvis
     */
    public function setIntituleAvisIt($intituleAvisIt)
    {
        $this->intituleAvisIt = $intituleAvisIt;

        return $this;
    }

    /**
     * Get intituleAvisIt.
     *
     * @return string
     */
    public function getIntituleAvisIt()
    {
        return $this->intituleAvisIt;
    }

    /**
     * @return $this
     */
    public function addConsultation(Consultation $consultation)
    {
        $this->consultations[] = $consultation;
        $consultation->setAvisType($this);

        return $this;
    }

    public function removeConsultation(Consultation $consultation)
    {
        $this->consultations->removeElement($consultation);
    }

    /**
     * @return ArrayCollection
     */
    public function getConsultations()
    {
        return $this->consultations;
    }
}
