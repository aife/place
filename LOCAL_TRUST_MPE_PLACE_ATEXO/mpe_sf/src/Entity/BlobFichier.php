<?php

namespace App\Entity;

use Stringable;
use DateTime;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Controller\ApiPlatformCustom\DownloadBlobFile;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * BlobFichier.
 *
 * @ORM\Table(name="blob_file")
 * @ORM\Entity(repositoryClass="App\Repository\BlobFichierRepository")
 */
#[ApiResource(
    collectionOperations: [],
    itemOperations: [
        'get_download' => [
            'method' => 'GET',
            'path' => '/attestations-medias/{id}/download',
            'controller' => DownloadBlobFile::class,
            'openapi_context' => [
                'responses' => [
                    '200' => [
                        'description' => 'Binary File Response'
                    ],
                    '404' => [
                        'description' => 'Resource not found'
                    ],
                ]
            ]
        ]
    ],
    shortName: 'AttestationsMedias',
    normalizationContext: ['groups' => ['read'], AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
)]
class BlobFichier implements Stringable
{
    /**
     *
     * @ORM\Column(name="id", unique=true, type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private ?int $oldId = null;

    /**
     * @ORM\Column(name="name", type="text", length=65535, nullable=false)
     */
    private ?string $name = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="deletion_datetime", type="datetime", nullable=true)*/
    private $deletionDatetime;

    /**
     * @ORM\Column(name="statut_synchro", type="integer")
     */
    private ?int $statutSynchro = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux", mappedBy="blobDescripteur")
     */
    private Collection $dossierVolumineuxDescripteur;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux", mappedBy="blobLogfile")
     */
    private Collection $dossierVolumineuxLogfile;

    /**
     * BlobFichier constructor.
     */
    public function __construct()
    {
        $this->dossierVolumineuxDescripteur = new ArrayCollection();
        $this->dossierVolumineuxLogfile = new ArrayCollection();
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * @ORM\Column(name="hash", type="string", length=255)
     */
    private ?string $hash = null;

    /**
     * @ORM\Column(name="dossier", type="string", length=255, nullable=true )
     */
    private ?string $dossier = null;

    /**
     * @ORM\Column(name="chemin", type="text", nullable=true)
     */
    private ?string $chemin = null;

    /**
     * @ORM\Column(name="extension", type="text", nullable=false)
     */
    private string $extension = '';

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(?string $chemin = null): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return BlobFichier
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return BlobFichier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set deletionDatetime.
     *
     * @param DateTime $deletionDatetime
     *
     * @return BlobFichier
     */
    public function setDeletionDatetime($deletionDatetime)
    {
        $this->deletionDatetime = $deletionDatetime;

        return $this;
    }

    /**
     * Get deletionDatetime.
     *
     * @return DateTime
     */
    public function getDeletionDatetime()
    {
        return $this->deletionDatetime;
    }

    public function getStatutSynchro(): int
    {
        return $this->statutSynchro;
    }

    public function setStatutSynchro(int $statutSynchro)
    {
        $this->statutSynchro = $statutSynchro;

        return $this;
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function setHash(string $hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return string
     */
    public function getDossier()
    {
        return $this->dossier;
    }

    /**
     * @param string $dossier
     */
    public function setDossier($dossier = null)
    {
        $this->dossier = $dossier;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension = '')
    {
        $this->extension = $extension;
    }

    /**
     * @return ArrayCollection
     */
    public function getDossiersVolumineuxDescripteur()
    {
        return $this->dossierVolumineuxDescripteur;
    }

    public function addDossierVolumineuxDescripteur(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineuxDescripteur->add($dossierVolumineux);
        $dossierVolumineux->setBlobDescripteur($this);
    }

    /**
     * @return ArrayCollection
     */
    public function getDossiersVolumineuxLogfile()
    {
        return $this->dossierVolumineuxLogfile;
    }

    public function addDossierVolumineuxLogfile(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineuxLogfile->add($dossierVolumineux);
        $dossierVolumineux->setBlobLogfile($this);
    }

    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    public function setOldId(int $oldId): BlobFichier
    {
        $this->oldId = $oldId;

        return $this;
    }
}
