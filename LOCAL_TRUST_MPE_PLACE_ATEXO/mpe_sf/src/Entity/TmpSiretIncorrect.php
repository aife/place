<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cette classe doit être supprimée une fois que tous les mails sont envoyés
 * TmpSiretIncorrect.
 *
 * @ORM\Table(name="tmp_siret_incorrect")
 * @ORM\Entity(repositoryClass="App\Repository\TmpSiretIncorrectRepository")
 */
class TmpSiretIncorrect
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="siret", type="string", length=255)
     */
    private ?string $siret = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_send_message", type="boolean")
     */
    private $isSendMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="send_date", type="datetime", nullable=true)
     */
    private $sendDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $createDate;

    /**
     * @ORM\Column(name="id_inscrit", type="integer")
     */
    private ?int $idInscrit = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return TmpSiretIncorrect
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set siret.
     *
     * @param string $siret
     *
     * @return TmpSiretIncorrect
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret.
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set isSendMessage.
     *
     * @param bool $isSendMessage
     *
     * @return TmpSiretIncorrect
     */
    public function setIsSendMessage($isSendMessage)
    {
        $this->isSendMessage = $isSendMessage;

        return $this;
    }

    /**
     * Get isSendMessage.
     *
     * @return bool
     */
    public function getIsSendMessage()
    {
        return $this->isSendMessage;
    }

    /**
     * Set sendDate.
     *
     * @param \DateTime $sendDate
     *
     * @return TmpSiretIncorrect
     */
    public function setSendDate($sendDate)
    {
        $this->sendDate = $sendDate;

        return $this;
    }

    /**
     * Get sendDate.
     *
     * @return \DateTime
     */
    public function getSendDate()
    {
        return $this->sendDate;
    }

    /**
     * Set createDate.
     *
     * @param \DateTime $createDate
     *
     * @return TmpSiretIncorrect
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate.
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set idInscrit.
     *
     * @return TmpSiretIncorrect
     */
    public function setIdInscrit(int $idInscrit)
    {
        $this->idInscrit = $idInscrit;

        return $this;
    }

    /**
     * Get idInscrit.
     *
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }
}

