<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\ReferentielOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Referentiel.
 *
 * @ORM\Table(name="Referentiel", indexes={@ORM\Index(name="id_referentiel", columns={"id_referentiel"})})
 * @ORM\Entity
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ReferentielOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'id' => 'exact',
        'libelleReferentiel' => 'partial',
    ]
)]
class Referentiel
{
    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ValeurReferentiel", mappedBy="referentiel")
     * @ORM\JoinColumn(name="id_referentiel", referencedColumnName="id_referentiel")
     */
    private Collection $valeursReferentielles;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ValeurReferentielOrg", mappedBy="referentiel")
     * @ORM\JoinColumn(name="id_referentiel", referencedColumnName="id_referentiel")
     */
    private Collection $valeursReferentiellesOrg;

    /**
     *
     * @ORM\Column(name="id_referentiel", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idReferentiel;

    /**
     * @ORM\Column(name="libelle_referentiel", type="string", length=200, nullable=false)
     */
    private string $libelleReferentiel = '';

    /**
     * Referentiel constructor.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function __construct()
    {
        $this->valeursReferentielles = new ArrayCollection();

        $this->valeursReferentiellesOrg = new ArrayCollection();
    }

    /**
     * Get idReferentiel.
     *
     * @return int
     */
    public function getIdReferentiel()
    {
        return $this->idReferentiel;
    }

    /**
     * Set libelleReferentiel.
     *
     * @param string $libelleReferentiel
     *
     * @return Referentiel
     */
    public function setLibelleReferentiel($libelleReferentiel)
    {
        $this->libelleReferentiel = $libelleReferentiel;

        return $this;
    }

    /**
     * Get libelleReferentiel.
     *
     * @return string
     */
    public function getLibelleReferentiel()
    {
        return $this->libelleReferentiel;
    }

    /**
     * @return $this
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function addValeurReferentielle(ValeurReferentiel $valeurReferentielle)
    {
        $this->valeursReferentielles[] = $valeurReferentielle;
        $valeurReferentielle->setReferentiel($this);

        return $this;
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function removeValeurReferentielle(ValeurReferentiel $valeurReferentielle)
    {
        $this->valeursReferentielles->removeElement($valeurReferentielle);
    }

    /**
     * @return ArrayCollection
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getValeursReferentielles()
    {
        return $this->valeursReferentielles;
    }

    /**
     * @return $this
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function addValeurReferentielleOrg(ValeurReferentielOrg $valeurReferentielleOrg)
    {
        $this->valeursReferentiellesOrg[] = $valeurReferentielleOrg;
        $valeurReferentielleOrg->setReferentiel($this);

        return $this;
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function removeValeurReferentielleOrg(ValeurReferentielOrg $valeurReferentielleOrg)
    {
        $this->valeursReferentiellesOrg->removeElement($valeurReferentielleOrg);
    }

    /**
     * @return ArrayCollection
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016
     */
    public function getValeursReferentiellesOrg()
    {
        return $this->valeursReferentiellesOrg;
    }
}
