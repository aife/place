<?php

namespace App\Entity;

use App\Repository\TypeDecisionRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeDecisionRepository::class)
 * @ORM\Table(name="TypeDecision")
 */
class TypeDecision
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $idTypeDecision;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $CodeTypeDecision;

    public function getIdTypeDecision(): ?int
    {
        return $this->idTypeDecision;
    }

    public function getCodeTypeDecision(): ?string
    {
        return $this->CodeTypeDecision;
    }

    public function setCodeTypeDecision(string $CodeTypeDecision): self
    {
        $this->CodeTypeDecision = $CodeTypeDecision;

        return $this;
    }
}
