<?php

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Input\EtablissementInput;
use App\Dto\Output\EtablissementOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Etablissement.
 *
 * @ORM\Table(name="t_etablissement", indexes={@ORM\Index(name="id_entreprise_idx", columns={"id_entreprise"})})
 * @ORM\Entity(repositoryClass="App\Repository\EtablissementRepository")
 * @ORM\HasLifecycleCallbacks()
 */
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: ['get','put','patch'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input:EtablissementInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: EtablissementOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idEtablissement' => 'exact',
        'codeEtablissement' => 'exact',
    ]
)]
class Etablissement
{
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Inscrit", mappedBy="etablissement")
     * @ORM\JoinColumn(name="id_etablissement", referencedColumnName="id_etablissement")
     */
    private Collection $inscrits;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entreprise",
     *     inversedBy="etablissements", fetch="EAGER")
     * @ORM\JoinColumn(name="id_entreprise", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id_etablissement", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idEtablissement;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="code_etablissement", type="string", length=5, nullable=false)
     */
    private ?string $codeEtablissement = null;

    /**
     * @ORM\Column(name="est_siege", type="string", nullable=false)
     */
    private string $estSiege = '0';

    /**
     * @ORM\Column(name="adresse", type="string", length=80, nullable=false)
     */
    private $adresse = null;

    /**
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=true)
     */
    private ?string $adresse2 = null;

    /**
     * @ORM\Column(name="code_postal", type="string", length=20, nullable=false)
     */
    private ?string $codePostal = null;

    /**
     * @ORM\Column(name="ville", type="string", length=50, nullable=false)
     */
    private ?string $ville = null;

    /**
     * @ORM\Column(name="pays", type="string", length=50, nullable=false)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(name="saisie_manuelle", type="string", nullable=false)
     */
    private string $saisieManuelle = '0';

    /**
     * @ORM\Column(name="id_initial", type="integer", nullable=false)
     */
    private int $idInitial = 0;

    /**
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private DateTimeInterface|DateTime $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=false)*/
    private $dateModification;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_suppression", type="datetime", nullable=true)*/
    private $dateSuppression;

    /**
     * @ORM\Column(name="statut_actif", type="string", nullable=false)
     */
    private string $statutActif = '1';

    /**
     * @ORM\Column(name="inscrit_annuaire_defense", type="string", nullable=false)
     */
    private string $inscritAnnuaireDefense = '0';

    /**
     * @ORM\Column(name="`long`", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $long = null;

    /**
     * @ORM\Column(name="lat", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $lat = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="maj_long_lat", type="datetime", nullable=true)*/
    private $majLongLat;

    /**
     * @ORM\Column(name="tva_intracommunautaire", length=20, type="string")
     */
    private ?string $tvaIntracommunautaire = null;

    /**
     *
     * @ORM\Column(name="id_externe", type="string", nullable=false)*/
    #[Groups('webservice')]
    private string|int $idExterne = '0';

    private $codeApe;
    private $siret;
    private $libelleApe;

    /**
     * @ORM\Column(type="string", length=1,  nullable=true,
     *     options={"default":null, "comment":"etat administratif : A => active, C => Cessée"})
     */
    private $etatAdministratif;

    /**
     * @ORM\Column(type="datetime", length=10, nullable=true,
     *     options={"default":null, "comment":"timestamp de la date de fermeture"})
     */
    private $dateFermeture;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\ModificationContrat", mappedBy="idEtablissement")
     */
    private Collection $modificationContrats;

    public function __construct()
    {
        $this->dateCreation = new DateTime();
        $this->modificationContrats = new ArrayCollection();
        $this->inscrits = new ArrayCollection();
    }

    /**
     * @param ModificationContrat $modificationContrat
     *
     * @return $this
     */
    public function addModificationContrat($modificationContrat)
    {
        $this->modificationContrats[] = $modificationContrat;

        return $this;
    }

    /**
     * @param ModificationContrat $modificationContrat
     */
    public function removeModificationContrat($modificationContrat)
    {
        $this->modificationContrats->removeElement($modificationContrat);
    }

    /**
     * @return ArrayCollection
     */
    public function getModificationContrats()
    {
        return $this->modificationContrats;
    }

    /**
     * @return mixed
     */
    public function getEtatAdministratif()
    {
        return $this->etatAdministratif;
    }

    /**
     * @param $etatAdministratif
     *
     * @return $this
     */
    public function setEtatAdministratif($etatAdministratif)
    {
        $this->etatAdministratif = $etatAdministratif;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateFermeture()
    {
        return $this->dateFermeture;
    }

    /**
     * @param $dateFermeture
     *
     * @return $this
     */
    public function setDateFermeture($dateFermeture)
    {
        $this->dateFermeture = $dateFermeture;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * Set idEntreprise.
     *
     * @param int $idEntreprise
     *
     * @return Etablissement
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * Get idEntreprise.
     *
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Set codeEtablissement.
     *
     * @param string $codeEtablissement
     *
     * @return Etablissement
     */
    public function setCodeEtablissement($codeEtablissement)
    {
        $this->codeEtablissement = $codeEtablissement;

        return $this;
    }

    /**
     * Get codeEtablissement.
     *
     * @return string
     */
    public function getCodeEtablissement()
    {
        return $this->codeEtablissement;
    }

    /**
     * Set estSiege.
     *
     * @param string $estSiege
     *
     * @return Etablissement
     */
    public function setEstSiege($estSiege)
    {
        $this->estSiege = $estSiege;

        return $this;
    }

    /**
     * Get estSiege.
     *
     * @return string
     */
    public function getEstSiege()
    {
        return $this->estSiege;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Etablissement
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresse2.
     *
     * @param string $adresse2
     *
     * @return Etablissement
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2.
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set codePostal.
     *
     * @param string $codePostal
     *
     * @return Etablissement
     */
    public function setCodePostal($codePostal)
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    /**
     * Get codePostal.
     *
     * @return string
     */
    public function getCodePostal()
    {
        return $this->codePostal;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return Etablissement
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Etablissement
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set saisieManuelle.
     *
     * @param string $saisieManuelle
     *
     * @return Etablissement
     */
    public function setSaisieManuelle($saisieManuelle)
    {
        $this->saisieManuelle = $saisieManuelle;

        return $this;
    }

    /**
     * Get saisieManuelle.
     *
     * @return string
     */
    public function getSaisieManuelle()
    {
        return $this->saisieManuelle;
    }

    /**
     * Set idInitial.
     *
     * @param int $idInitial
     *
     * @return Etablissement
     */
    public function setIdInitial($idInitial)
    {
        $this->idInitial = $idInitial;

        return $this;
    }

    /**
     * Get idInitial.
     *
     * @return int
     */
    public function getIdInitial()
    {
        return $this->idInitial;
    }

    /**
     * Set dateCreation.
     *
     * @return Etablissement
     */
    public function setDateCreation(DateTime $dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     */
    public function getDateCreation(): DateTime
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param DateTime $dateModification
     *
     * @return Etablissement
     */
    public function setDateModification(DateTime $dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return DateTime
     */
    public function getDateModification(): ?DateTime
    {
        return $this->dateModification;
    }

    /**
     * Set dateSuppression.
     *
     * @param DateTime $dateSuppression
     *
     * @return Etablissement
     */
    public function setDateSuppression($dateSuppression)
    {
        $this->dateSuppression = $dateSuppression;

        return $this;
    }

    /**
     * Get dateSuppression.
     *
     * @return DateTime
     */
    public function getDateSuppression()
    {
        return $this->dateSuppression;
    }

    /**
     * Set statutActif.
     *
     * @param string $statutActif
     *
     * @return Etablissement
     */
    public function setStatutActif($statutActif)
    {
        $this->statutActif = $statutActif;

        return $this;
    }

    /**
     * Get statutActif.
     *
     * @return string
     */
    public function getStatutActif()
    {
        return $this->statutActif;
    }

    /**
     * Set inscritAnnuaireDefense.
     *
     * @param string $inscritAnnuaireDefense
     *
     * @return Etablissement
     */
    public function setInscritAnnuaireDefense($inscritAnnuaireDefense)
    {
        $this->inscritAnnuaireDefense = $inscritAnnuaireDefense;

        return $this;
    }

    /**
     * Get inscritAnnuaireDefense.
     *
     * @return string
     */
    public function getInscritAnnuaireDefense()
    {
        return $this->inscritAnnuaireDefense;
    }

    /**
     * Set long.
     *
     * @param float $long
     *
     * @return Etablissement
     */
    public function setLong($long)
    {
        $this->long = $long;

        return $this;
    }

    /**
     * Get longs.
     *
     * @return float
     */
    public function getLong()
    {
        return $this->long;
    }

    /**
     * Set lat.
     *
     * @param float $lat
     *
     * @return Etablissement
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat.
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set majLongLat.
     *
     * @param DateTime $majLongLat
     *
     * @return Etablissement
     */
    public function setMajLongLat($majLongLat)
    {
        $this->majLongLat = $majLongLat;

        return $this;
    }

    /**
     * Get majLongLat.
     *
     * @return DateTime
     */
    public function getMajLongLat()
    {
        return $this->majLongLat;
    }

    /**
     * @return string
     */
    public function getTvaIntracommunautaire()
    {
        return $this->tvaIntracommunautaire;
    }

    /**
     * @param string $tvaIntracommunautaire
     *
     * @return Etablissement
     */
    public function setTvaIntracommunautaire($tvaIntracommunautaire)
    {
        $this->tvaIntracommunautaire = $tvaIntracommunautaire;

        return $this;
    }

    public function setEntreprise(Entreprise $entreprise)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @return mixed
     */
    public function getCodeApe()
    {
        return $this->codeApe;
    }

    /**
     * @param mixed $codeApe
     */
    public function setCodeApe($codeApe)
    {
        $this->codeApe = $codeApe;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param mixed $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return mixed
     */
    public function getLibelleApe()
    {
        return $this->libelleApe;
    }

    /**
     * @param mixed $libelleApe
     */
    public function setLibelleApe($libelleApe)
    {
        $this->libelleApe = $libelleApe;
    }

    /**
     * @ORM\PrePersist
     */
    public function createDate()
    {
        $this->dateCreation = new DateTime();
        $this->dateModification = new DateTime();
    }

    /**
     * @ORM\PreUpdate
     */
    public function updateDate()
    {
        $this->dateModification = new DateTime();
    }

    /**
     * @return $this
     */
    public function addInscrit(Inscrit $inscrit)
    {
        $this->inscrits[] = $inscrit;

        return $this;
    }

    /**
     * @param Inscrit $offre
     */
    public function removeInscrit(Inscrit $inscrit)
    {
        $this->inscrits->removeElement($inscrit);
    }

    /**
     * @return ArrayCollection
     */
    public function getInscrits()
    {
        return $this->inscrits;
    }

    public function getIdExterne(): int
    {
        return $this->idExterne;
    }

    public function setIdExterne(int $idExterne): Etablissement
    {
        $this->idExterne = $idExterne;

        return $this;
    }
}
