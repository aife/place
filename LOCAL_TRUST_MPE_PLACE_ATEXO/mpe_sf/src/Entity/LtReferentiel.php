<?php

namespace App\Entity;

use App\Repository\LtReferentielRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LtReferentielRepository::class)
 * @ORM\Table(name="Lt_Referentiel")
 */
class LtReferentiel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private string $codeLibelle;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $entreprise = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $consultation = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $lot = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $agent = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $obligatoire = false;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private string $typeSearch = '0';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $pages = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $pathConfig = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $modeAffichage = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $modeModification = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private ?string $modeRecherche = null;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $type = '0';

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $organismes = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $libelleInfoBulle = null;

    /**
     * @ORM\Column(type="string", length=200, nullable=true)
     */
    private ?string $logo = null;

    /**
     * @ORM\Column(name="defaultValue", type="string", length=255, nullable=true)
     */
    private ?string $defaultValue = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $dependanceAllotissement = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $dataType = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCodeLibelle(): ?string
    {
        return $this->codeLibelle;
    }

    public function setCodeLibelle(string $codeLibelle): self
    {
        $this->codeLibelle = $codeLibelle;

        return $this;
    }

    public function getEntreprise(): ?bool
    {
        return $this->entreprise;
    }

    public function setEntreprise(bool $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    public function getConsultation(): ?bool
    {
        return $this->consultation;
    }

    public function setConsultation(bool $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getLot(): ?bool
    {
        return $this->lot;
    }

    public function setLot(bool $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getAgent(): ?bool
    {
        return $this->agent;
    }

    public function setAgent(bool $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getObligatoire(): ?bool
    {
        return $this->obligatoire;
    }

    public function setObligatoire(bool $obligatoire): self
    {
        $this->obligatoire = $obligatoire;

        return $this;
    }

    public function getTypeSearch(): ?string
    {
        return $this->typeSearch;
    }

    public function setTypeSearch(string $typeSearch): self
    {
        $this->typeSearch = $typeSearch;

        return $this;
    }

    public function getPages(): ?string
    {
        return $this->pages;
    }

    public function setPages(?string $pages): self
    {
        $this->pages = $pages;

        return $this;
    }

    public function getPathConfig(): ?string
    {
        return $this->pathConfig;
    }

    public function setPathConfig(?string $pathConfig): self
    {
        $this->pathConfig = $pathConfig;

        return $this;
    }

    public function getModeAffichage(): ?string
    {
        return $this->modeAffichage;
    }

    public function setModeAffichage(?string $modeAffichage): self
    {
        $this->modeAffichage = $modeAffichage;

        return $this;
    }

    public function getModeModification(): ?string
    {
        return $this->modeModification;
    }

    public function setModeModification(?string $modeModification): self
    {
        $this->modeModification = $modeModification;

        return $this;
    }

    public function getModeRecherche(): ?string
    {
        return $this->modeRecherche;
    }

    public function setModeRecherche(?string $modeRecherche): self
    {
        $this->modeRecherche = $modeRecherche;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getOrganismes(): ?string
    {
        return $this->organismes;
    }

    public function setOrganismes(?string $organismes): self
    {
        $this->organismes = $organismes;

        return $this;
    }

    public function getLibelleInfoBulle(): ?string
    {
        return $this->libelleInfoBulle;
    }

    public function setLibelleInfoBulle(?string $libelleInfoBulle): self
    {
        $this->libelleInfoBulle = $libelleInfoBulle;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    public function setDefaultValue(?string $defaultValue): self
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    public function getDependanceAllotissement(): ?bool
    {
        return $this->dependanceAllotissement;
    }

    public function setDependanceAllotissement(bool $dependanceAllotissement): self
    {
        $this->dependanceAllotissement = $dependanceAllotissement;

        return $this;
    }

    public function getDataType(): ?string
    {
        return $this->dataType;
    }

    public function setDataType(?string $dataType): self
    {
        $this->dataType = $dataType;

        return $this;
    }
}
