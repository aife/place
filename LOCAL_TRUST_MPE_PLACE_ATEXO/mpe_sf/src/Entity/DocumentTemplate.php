<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DocumentTemplateRepository")
 */
class DocumentTemplate
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $document = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $nom = null;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private ?string $nomAfficher = null;

    /**
     * @ORM\OneToMany(targetEntity=DocumentTemplateSurcharge::class, mappedBy="documentTemplate")
     */
    private array|Collection|ArrayCollection $documentTemplateSurcharges;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private $isCustom;

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->documentTemplateSurcharges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocument(): ?string
    {
        return $this->document;
    }

    public function setDocument(string $document): self
    {
        $this->document = $document;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNomAfficher(): ?string
    {
        return $this->nomAfficher;
    }

    public function setNomAfficher(string $nomAfficher): self
    {
        $this->nomAfficher = $nomAfficher;

        return $this;
    }

    /**
     * @return Collection|DocumentTemplateSurcharge[]
     */
    public function getDocumentTemplateSurcharges(): Collection
    {
        return $this->documentTemplateSurcharges;
    }

    public function addDocumentTemplateSurcharge(DocumentTemplateSurcharge $documentTemplateSurcharge): self
    {
        if (!$this->documentTemplateSurcharges->contains($documentTemplateSurcharge)) {
            $this->documentTemplateSurcharges[] = $documentTemplateSurcharge;
            $documentTemplateSurcharge->setDocumentTemplate($this);
        }

        return $this;
    }

    public function removeDocumentTemplateSurcharge(DocumentTemplateSurcharge $documentTemplateSurcharge): self
    {
        if (
            $this->documentTemplateSurcharges->removeElement($documentTemplateSurcharge)
            && $documentTemplateSurcharge->getDocumentTemplate() === $this
        ) {
            $documentTemplateSurcharge->setDocumentTemplate(null);
        }

        return $this;
    }

    public function isCustom(): bool
    {
        return $this->isCustom;
    }

    public function setIsCustom(bool $isCustom): self
    {
        $this->isCustom = $isCustom;

        return $this;
    }
}
