<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Stringable;
use DateTimeInterface;
use DateTime;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\OrganismeInput;
use App\Dto\Output\OrganismeOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Organisme.
 *
 * @ORM\Table(name="Organisme",
 *     indexes={@ORM\Index(name="acronyme",
 *     columns={"acronyme"}),
 *     @ORM\Index(name="categorie_insee", columns={"categorie_insee"})})
 * @ORM\Entity(repositoryClass="App\Repository\OrganismeRepository")
 */
#[ApiResource(
    collectionOperations: ['get','post'],
    itemOperations: ['get','put','patch'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input: OrganismeInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: OrganismeOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'id' => 'exact',
        'denominationOrg' => 'partial'
    ]
)]
class Organisme implements Stringable
{
    public const FOREIGN_KEYS = [
        'categorieInsee' => [
            'class' => CategorieINSEE::class,
            'property' => 'id',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.categorieINSEE'
        ],
    ];

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation",
     *     mappedBy="organisme",cascade={"persist"})
     *
     */
    private Collection $consultations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Configuration\PlateformeVirtuelleOrganisme", mappedBy="organisme")
     * @ORM\JoinColumn(name="id", referencedColumnName="organisme_id")
     */
    private Collection $plateformeVirtuelleOrganisme;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\TelechargementAnonyme",
     *     mappedBy="organisme",cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $telechargementAnonymes;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\EchangeDestinataire", mappedBy="organisme",cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $echangeDestinataires;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Telechargement",
     *     mappedBy="organisme",cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $telechargements;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Agent", mappedBy="organisme")
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $agents;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Administrateur", mappedBy="organisme")
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $administrateurs;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\WS\AgentTechniqueAssociation", mappedBy="organisme")
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $agentTechAssociations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Service", mappedBy="organisme")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private Collection $services;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\TDumeContexte",
     *     mappedBy="organisme", cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $dumeContextes;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\TListeLotsCandidature",
     *     mappedBy="organisme", cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $listLots;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\TCandidature",
     *     mappedBy="organisme", cascade={"persist"})
     * @ORM\JoinColumn(name="acronyme", referencedColumnName="organisme")
     */
    private Collection $candidatures;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\AffiliationService",
     *     mappedBy="organisme", cascade={"persist"})
     */
    private Collection $affiliations;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Echange", mappedBy="organisme", cascade={"persist"})
     */
    private Collection $echanges;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionDCE", mappedBy="organisme",
     *     cascade={"persist"})
     */
    private Collection $questions;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\EchangePieceJointe", mappedBy="organisme",
     *     cascade={"persist"})
     */
    private Collection $echangePieceJointes;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\RelationEchange", mappedBy="organisme",
     *     cascade={"persist"})
     */
    private Collection $relationEchanges;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\EchangeDocumentaire\EchangeDocApplicationClientOrganisme", mappedBy="organisme")
     */
    private Collection $echangeDocApplicationClientsOrganismes;

    /**
     * @ORM\Column(name="id", type="integer")
     */
    #[Groups('webservice')]
    private ?int $id = null;

    /**
     * @ORM\Column(name="acronyme", type="string", length=30, nullable=false)
     * @ORM\Id
     */
    #[Groups('webservice')]
    private string $acronyme = '';

    /**
     * @ORM\Column(name="type_article_org", type="integer", nullable=false)
     */
    private string|int $typeArticleOrg = '0';

    /**
     * @ORM\Column(name="denomination_org", type="text", length=65535, nullable=true)
     */
    private ?string $denominationOrg = null;

    /**
     *
     * @ORM\Column(name="categorie_insee", type="string", length=20, nullable=true)*/
    #[Groups('webservice')]
    private ?string $categorieInsee = null;

    /**
     * @ORM\Column(name="description_org", type="text", length=16777215, nullable=true)
     */
    private ?string $descriptionOrg = null;

    /**
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     */
    #[Assert\NotBlank]
    private string $adresse = '';

    /**
     * @ORM\Column(name="cp", type="string", length=5, nullable=false)
     */
    #[Assert\NotBlank]
    private string $cp = '';

    /**
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     */
    #[Assert\NotBlank]
    private string $ville = '';

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private string $email = '';

    /**
     * @ORM\Column(name="url", type="string", length=100, nullable=false)
     */
    private string $url = '';

    /**
     * @ORM\Column(name="id_attrib_file", type="string", length=11, nullable=true)
     */
    private ?string $idAttribFile = null;

    /**
     * @ORM\Column(name="attrib_file", type="string", length=150, nullable=false)
     */
    private string $attribFile = '';

    /**
     * @ORM\Column(name="date_creation", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|DateTime $dateCreation = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="active", type="string", length=1, nullable=false)
     */
    private string $active = '1';

    /**
     * @ORM\Column(name="id_client_ANM", type="string", length=32, nullable=false)
     */
    private string $idClientAnm = '0';

    /**
     * @ORM\Column(name="status", type="string", nullable=false)
     */
    private string $status = '0';

    /**
     * @ORM\Column(name="signataire_cao", type="text", length=65535, nullable=true)
     */
    private ?string $signataireCao = null;

    /**
     * @ORM\Column(name="sigle", type="string", length=100, nullable=false)*/
    #[Groups('webservice')]
    private string $sigle = '';

    /**
     * @ORM\Column(name="adresse2", type="string", length=100, nullable=false)
     */
    private string $adresse2 = '';

    /**
     * @ORM\Column(name="tel", type="string", length=50, nullable=false)
     */
    private string $tel = '';

    /**
     * @ORM\Column(name="telecopie", type="string", length=50, nullable=false)
     */
    private string $telecopie = '';

    /**
     * @ORM\Column(name="pays", type="string", length=150, nullable=true)
     */
    private ?string $pays = null;

    /**
     * @ORM\Column(name="affichage_entite", type="string", length=1, nullable=false)
     */
    private string $affichageEntite = '';

    /**
     * @ORM\Column(name="id_initial", type="integer", nullable=false)
     */
    private string|int $idInitial = '0';

    /**
     * @ORM\Column(name="denomination_org_ar", type="string", length=100, nullable=false)
     */
    private string $denominationOrgAr = '';

    /**
     * @ORM\Column(name="description_org_ar", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgAr = '';

    /**
     * @ORM\Column(name="adresse_ar", type="string", length=100, nullable=false)
     */
    private string $adresseAr = '';

    /**
     * @ORM\Column(name="ville_ar", type="string", length=100, nullable=false)
     */
    private string $villeAr = '';

    /**
     * @ORM\Column(name="adresse2_ar", type="string", length=100, nullable=false)
     */
    private string $adresse2Ar = '';

    /**
     * @ORM\Column(name="pays_ar", type="string", length=150, nullable=false)
     */
    private string $paysAr = '';

    /**
     * @ORM\Column(name="denomination_org_fr", type="string", length=100, nullable=false)
     */
    private string $denominationOrgFr = '';

    /**
     * @ORM\Column(name="description_org_fr", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgFr = '';

    /**
     * @ORM\Column(name="adresse_fr", type="string", length=100, nullable=false)
     */
    private string $adresseFr = '';

    /**
     * @ORM\Column(name="ville_fr", type="string", length=100, nullable=false)
     */
    private string $villeFr = '';

    /**
     * @ORM\Column(name="adresse2_fr", type="string", length=100, nullable=false)
     */
    private string $adresse2Fr = '';

    /**
     * @ORM\Column(name="pays_fr", type="string", length=150, nullable=false)
     */
    private string $paysFr = '';

    /**
     * @ORM\Column(name="denomination_org_es", type="string", length=100, nullable=false)
     */
    private string $denominationOrgEs = '';

    /**
     * @ORM\Column(name="description_org_es", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgEs = '';

    /**
     * @ORM\Column(name="adresse_es", type="string", length=100, nullable=false)
     */
    private string $adresseEs = '';

    /**
     * @ORM\Column(name="ville_es", type="string", length=100, nullable=false)
     */
    private string $villeEs = '';

    /**
     * @ORM\Column(name="adresse2_es", type="string", length=100, nullable=false)
     */
    private string $adresse2Es = '';

    /**
     * @ORM\Column(name="pays_es", type="string", length=150, nullable=false)
     */
    private string $paysEs = '';

    /**
     * @ORM\Column(name="denomination_org_en", type="string", length=100, nullable=false)
     */
    private string $denominationOrgEn = '';

    /**
     * @ORM\Column(name="description_org_en", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgEn = '';

    /**
     * @ORM\Column(name="adresse_en", type="string", length=100, nullable=false)
     */
    private string $adresseEn = '';

    /**
     * @ORM\Column(name="ville_en", type="string", length=100, nullable=false)
     */
    private string $villeEn = '';

    /**
     * @ORM\Column(name="adresse2_en", type="string", length=100, nullable=false)
     */
    private string $adresse2En = '';

    /**
     * @ORM\Column(name="pays_en", type="string", length=150, nullable=false)
     */
    private string $paysEn = '';

    /**
     * @ORM\Column(name="denomination_org_su", type="string", length=100, nullable=false)
     */
    private string $denominationOrgSu = '';

    /**
     * @ORM\Column(name="description_org_su", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgSu = '';

    /**
     * @ORM\Column(name="adresse_su", type="string", length=100, nullable=false)
     */
    private string $adresseSu = '';

    /**
     * @ORM\Column(name="ville_su", type="string", length=100, nullable=false)
     */
    private string $villeSu = '';

    /**
     * @ORM\Column(name="adresse2_su", type="string", length=100, nullable=false)
     */
    private string $adresse2Su = '';

    /**
     * @ORM\Column(name="pays_su", type="string", length=150, nullable=false)
     */
    private string $paysSu = '';

    /**
     * @ORM\Column(name="denomination_org_du", type="string", length=100, nullable=false)
     */
    private string $denominationOrgDu = '';

    /**
     * @ORM\Column(name="description_org_du", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgDu = '';

    /**
     * @ORM\Column(name="adresse_du", type="string", length=100, nullable=false)
     */
    private string $adresseDu = '';

    /**
     * @ORM\Column(name="ville_du", type="string", length=100, nullable=false)
     */
    private string $villeDu = '';

    /**
     * @ORM\Column(name="adresse2_du", type="string", length=100, nullable=false)
     */
    private string $adresse2Du = '';

    /**
     * @ORM\Column(name="pays_du", type="string", length=150, nullable=false)
     */
    private string $paysDu = '';

    /**
     * @ORM\Column(name="denomination_org_cz", type="string", length=100, nullable=false)
     */
    private string $denominationOrgCz = '';

    /**
     * @ORM\Column(name="description_org_cz", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgCz = '';

    /**
     * @ORM\Column(name="adresse_cz", type="string", length=100, nullable=false)
     */
    private string $adresseCz = '';

    /**
     * @ORM\Column(name="ville_cz", type="string", length=100, nullable=false)
     */
    private string $villeCz = '';

    /**
     * @ORM\Column(name="adresse2_cz", type="string", length=100, nullable=false)
     */
    private string $adresse2Cz = '';

    /**
     * @ORM\Column(name="pays_cz", type="string", length=150, nullable=false)
     */
    private string $paysCz = '';

    /**
     * @ORM\Column(name="denomination_org_it", type="string", length=100, nullable=false)
     */
    private string $denominationOrgIt = '';

    /**
     * @ORM\Column(name="description_org_it", type="text", length=16777215, nullable=false)
     */
    private string $descriptionOrgIt = '';

    /**
     * @ORM\Column(name="adresse_it", type="string", length=100, nullable=false)
     */
    private string $adresseIt = '';

    /**
     * @ORM\Column(name="ville_it", type="string", length=100, nullable=false)
     */
    private string $villeIt = '';

    /**
     * @ORM\Column(name="adresse2_it", type="string", length=100, nullable=false)
     */
    private string $adresse2It = '';

    /**
     * @ORM\Column(name="pays_it", type="string", length=150, nullable=false)
     */
    private string $paysIt = '';

    /**
     *
     * @ORM\Column(name="siren", type="string", length=9, nullable=false)*/
    #[Groups('webservice')]
    private string $siren = '';

    /**
     * @ORM\Column(name="complement", type="string", length=5, nullable=false)
     */
    #[Assert\NotBlank]
    private string $complement = '';

    /**
     * @var int
     *
     * @ORM\Column(name="moniteur_provenance", type="integer", nullable=true)
     */
    private $moniteurProvenance = '90';

    /**
     * @ORM\Column(name="code_acces_logiciel", type="string", length=30, nullable=true)
     */
    private ?string $codeAccesLogiciel = 'Y8YG-69WD-4421-4G28';

    /**
     * @ORM\Column(name="decalage_horaire", type="string", length=5, nullable=true)
     */
    private ?string $decalageHoraire = null;

    /**
     * @ORM\Column(name="lieu_residence", type="string", length=255, nullable=true)
     */
    private ?string $lieuResidence = null;

    /**
     * @ORM\Column(name="activation_fuseau_horaire", type="string", nullable=false)
     */
    private string $activationFuseauHoraire = '0';

    /**
     * @ORM\Column(name="alerte", type="string", nullable=false)
     */
    private string $alerte = '0';

    /**
     * @ORM\Column(name="ordre", type="integer", nullable=false)
     */
    private string|int $ordre = '0';

    /**
     * @ORM\Column(name="URL_INTERFACE_ANM", type="string", length=100, nullable=true)
     */
    private ?string $urlInterfaceAnm = null;

    /**
     * @ORM\Column(name="pf_url", type="string", length=255, nullable=true)
     */
    private ?string $pfUrl = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="tag_purge", type="boolean")
     */
    private $tagPurge;

    /**
     * @ORM\Column(name="id_externe", type="string", nullable=false)*/
    #[Groups('webservice')]
    private string|int $idExterne = '0';

    /**
     *
     * @ORM\Column(name="id_entite", type="integer", nullable=true)*/
    #[Groups('webservice')]
    private ?int $idEntite = null;

    /**
     * @ORM\OneToMany(targetEntity=DocumentTemplateSurcharge::class, mappedBy="organisme")
     */
    private Collection $documentTemplateSurcharges;

    /**
     *
     * @ORM\Column(name="sous_type_organisme", type="integer", options={
     *     "default": 2,
     *     "comment":"1: Etat et ses établissements publics - Autres que ceux ayant un caractère industriel
     *     et commercial 2 :Collectivités territoriales / EPL / EPS"
     *     })*/
    private int $sousTypeOrganisme = 2;

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->consultations = new ArrayCollection();
        $this->agents = new ArrayCollection();
        $this->administrateurs = new ArrayCollection();
        $this->services = new ArrayCollection();
        $this->echangeDestinataires = new ArrayCollection();
        $this->echanges = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->echangePieceJointes = new ArrayCollection();
        $this->relationEchanges = new ArrayCollection();
        $this->dateCreation = new DateTime();
        $this->echangeDocApplicationClientsOrganismes = new ArrayCollection();
        $this->documentTemplateSurcharges = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * Set acronyme.
     *
     * @param string $acronyme
     *
     * @return Organisme
     */
    public function setAcronyme($acronyme)
    {
        $this->acronyme = $acronyme;

        return $this;
    }

    /**
     * Get acronyme.
     *
     * @return string
     */
    public function getAcronyme()
    {
        return $this->acronyme;
    }

    /**
     * Set typeArticleOrg.
     *
     * @param int $typeArticleOrg
     *
     * @return Organisme
     */
    public function setTypeArticleOrg($typeArticleOrg)
    {
        $this->typeArticleOrg = $typeArticleOrg;

        return $this;
    }

    /**
     * Get typeArticleOrg.
     *
     * @return int
     */
    public function getTypeArticleOrg()
    {
        return $this->typeArticleOrg;
    }

    /**
     * Set denominationOrg.
     *
     * @param string $denominationOrg
     *
     * @return Organisme
     */
    public function setDenominationOrg($denominationOrg)
    {
        $this->denominationOrg = $denominationOrg;

        return $this;
    }

    /**
     * Get denominationOrg.
     *
     * @return string
     */
    public function getDenominationOrg()
    {
        return $this->denominationOrg;
    }

    /**
     * Set categorieInsee.
     *
     * @param string $categorieInsee
     *
     * @return Organisme
     */
    public function setCategorieInsee($categorieInsee)
    {
        $this->categorieInsee = $categorieInsee;

        return $this;
    }

    /**
     * Get categorieInsee.
     *
     * @return string
     */
    public function getCategorieInsee()
    {
        return $this->categorieInsee;
    }

    /**
     * Set descriptionOrg.
     *
     * @param string $descriptionOrg
     *
     * @return Organisme
     */
    public function setDescriptionOrg($descriptionOrg)
    {
        $this->descriptionOrg = $descriptionOrg;

        return $this;
    }

    /**
     * Get descriptionOrg.
     *
     * @return string
     */
    public function getDescriptionOrg()
    {
        return $this->descriptionOrg;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Organisme
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set cp.
     *
     * @param string $cp
     *
     * @return Organisme
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return Organisme
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return Organisme
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Organisme
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set idAttribFile.
     *
     * @param string $idAttribFile
     *
     * @return Organisme
     */
    public function setIdAttribFile($idAttribFile)
    {
        $this->idAttribFile = $idAttribFile;

        return $this;
    }

    /**
     * Get idAttribFile.
     *
     * @return string
     */
    public function getIdAttribFile()
    {
        return $this->idAttribFile;
    }

    /**
     * Set attribFile.
     *
     * @param string $attribFile
     *
     * @return Organisme
     */
    public function setAttribFile($attribFile)
    {
        $this->attribFile = $attribFile;

        return $this;
    }

    /**
     * Get attribFile.
     *
     * @return string
     */
    public function getAttribFile()
    {
        return $this->attribFile;
    }

    /**
     * Set dateCreation.
     *
     * @param DateTime $dateCreation
     *
     * @return Organisme
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set active.
     *
     * @param string $active
     *
     * @return Organisme
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return string
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set idClientAnm.
     *
     * @param string $idClientAnm
     *
     * @return Organisme
     */
    public function setIdClientAnm($idClientAnm)
    {
        $this->idClientAnm = $idClientAnm;

        return $this;
    }

    /**
     * Get idClientAnm.
     *
     * @return string
     */
    public function getIdClientAnm()
    {
        return $this->idClientAnm;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Organisme
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set signataireCao.
     *
     * @param string $signataireCao
     *
     * @return Organisme
     */
    public function setSignataireCao($signataireCao)
    {
        $this->signataireCao = $signataireCao;

        return $this;
    }

    /**
     * Get signataireCao.
     *
     * @return string
     */
    public function getSignataireCao()
    {
        return $this->signataireCao;
    }

    /**
     * Set sigle.
     *
     * @param string $sigle
     *
     * @return Organisme
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * Get sigle.
     *
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * Set adresse2.
     *
     * @param string $adresse2
     *
     * @return Organisme
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * Get adresse2.
     *
     * @return string
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * Set tel.
     *
     * @param string $tel
     *
     * @return Organisme
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel.
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set telecopie.
     *
     * @param string $telecopie
     *
     * @return Organisme
     */
    public function setTelecopie($telecopie)
    {
        $this->telecopie = $telecopie;

        return $this;
    }

    /**
     * Get telecopie.
     *
     * @return string
     */
    public function getTelecopie()
    {
        return $this->telecopie;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Organisme
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set affichageEntite.
     *
     * @param string $affichageEntite
     *
     * @return Organisme
     */
    public function setAffichageEntite($affichageEntite)
    {
        $this->affichageEntite = $affichageEntite;

        return $this;
    }

    /**
     * Get affichageEntite.
     *
     * @return string
     */
    public function getAffichageEntite()
    {
        return $this->affichageEntite;
    }

    /**
     * Set idInitial.
     *
     * @param int $idInitial
     *
     * @return Organisme
     */
    public function setIdInitial($idInitial)
    {
        $this->idInitial = $idInitial;

        return $this;
    }

    /**
     * Get idInitial.
     *
     * @return int
     */
    public function getIdInitial()
    {
        return $this->idInitial;
    }

    /**
     * Set denominationOrgAr.
     *
     * @param string $denominationOrgAr
     *
     * @return Organisme
     */
    public function setDenominationOrgAr($denominationOrgAr)
    {
        $this->denominationOrgAr = $denominationOrgAr;

        return $this;
    }

    /**
     * Get denominationOrgAr.
     *
     * @return string
     */
    public function getDenominationOrgAr()
    {
        return $this->denominationOrgAr;
    }

    /**
     * Set descriptionOrgAr.
     *
     * @param string $descriptionOrgAr
     *
     * @return Organisme
     */
    public function setDescriptionOrgAr($descriptionOrgAr)
    {
        $this->descriptionOrgAr = $descriptionOrgAr;

        return $this;
    }

    /**
     * Get descriptionOrgAr.
     *
     * @return string
     */
    public function getDescriptionOrgAr()
    {
        return $this->descriptionOrgAr;
    }

    /**
     * Set adresseAr.
     *
     * @param string $adresseAr
     *
     * @return Organisme
     */
    public function setAdresseAr($adresseAr)
    {
        $this->adresseAr = $adresseAr;

        return $this;
    }

    /**
     * Get adresseAr.
     *
     * @return string
     */
    public function getAdresseAr()
    {
        return $this->adresseAr;
    }

    /**
     * Set villeAr.
     *
     * @param string $villeAr
     *
     * @return Organisme
     */
    public function setVilleAr($villeAr)
    {
        $this->villeAr = $villeAr;

        return $this;
    }

    /**
     * Get villeAr.
     *
     * @return string
     */
    public function getVilleAr()
    {
        return $this->villeAr;
    }

    /**
     * Set adresse2Ar.
     *
     * @param string $adresse2Ar
     *
     * @return Organisme
     */
    public function setAdresse2Ar($adresse2Ar)
    {
        $this->adresse2Ar = $adresse2Ar;

        return $this;
    }

    /**
     * Get adresse2Ar.
     *
     * @return string
     */
    public function getAdresse2Ar()
    {
        return $this->adresse2Ar;
    }

    /**
     * Set paysAr.
     *
     * @param string $paysAr
     *
     * @return Organisme
     */
    public function setPaysAr($paysAr)
    {
        $this->paysAr = $paysAr;

        return $this;
    }

    /**
     * Get paysAr.
     *
     * @return string
     */
    public function getPaysAr()
    {
        return $this->paysAr;
    }

    /**
     * Set denominationOrgFr.
     *
     * @param string $denominationOrgFr
     *
     * @return Organisme
     */
    public function setDenominationOrgFr($denominationOrgFr)
    {
        $this->denominationOrgFr = $denominationOrgFr;

        return $this;
    }

    /**
     * Get denominationOrgFr.
     *
     * @return string
     */
    public function getDenominationOrgFr()
    {
        return $this->denominationOrgFr;
    }

    /**
     * Set descriptionOrgFr.
     *
     * @param string $descriptionOrgFr
     *
     * @return Organisme
     */
    public function setDescriptionOrgFr($descriptionOrgFr)
    {
        $this->descriptionOrgFr = $descriptionOrgFr;

        return $this;
    }

    /**
     * Get descriptionOrgFr.
     *
     * @return string
     */
    public function getDescriptionOrgFr()
    {
        return $this->descriptionOrgFr;
    }

    /**
     * Set adresseFr.
     *
     * @param string $adresseFr
     *
     * @return Organisme
     */
    public function setAdresseFr($adresseFr)
    {
        $this->adresseFr = $adresseFr;

        return $this;
    }

    /**
     * Get adresseFr.
     *
     * @return string
     */
    public function getAdresseFr()
    {
        return $this->adresseFr;
    }

    /**
     * Set villeFr.
     *
     * @param string $villeFr
     *
     * @return Organisme
     */
    public function setVilleFr($villeFr)
    {
        $this->villeFr = $villeFr;

        return $this;
    }

    /**
     * Get villeFr.
     *
     * @return string
     */
    public function getVilleFr()
    {
        return $this->villeFr;
    }

    /**
     * Set adresse2Fr.
     *
     * @param string $adresse2Fr
     *
     * @return Organisme
     */
    public function setAdresse2Fr($adresse2Fr)
    {
        $this->adresse2Fr = $adresse2Fr;

        return $this;
    }

    /**
     * Get adresse2Fr.
     *
     * @return string
     */
    public function getAdresse2Fr()
    {
        return $this->adresse2Fr;
    }

    /**
     * Set paysFr.
     *
     * @param string $paysFr
     *
     * @return Organisme
     */
    public function setPaysFr($paysFr)
    {
        $this->paysFr = $paysFr;

        return $this;
    }

    /**
     * Get paysFr.
     *
     * @return string
     */
    public function getPaysFr()
    {
        return $this->paysFr;
    }

    /**
     * Set denominationOrgEs.
     *
     * @param string $denominationOrgEs
     *
     * @return Organisme
     */
    public function setDenominationOrgEs($denominationOrgEs)
    {
        $this->denominationOrgEs = $denominationOrgEs;

        return $this;
    }

    /**
     * Get denominationOrgEs.
     *
     * @return string
     */
    public function getDenominationOrgEs()
    {
        return $this->denominationOrgEs;
    }

    /**
     * Set descriptionOrgEs.
     *
     * @param string $descriptionOrgEs
     *
     * @return Organisme
     */
    public function setDescriptionOrgEs($descriptionOrgEs)
    {
        $this->descriptionOrgEs = $descriptionOrgEs;

        return $this;
    }

    /**
     * Get descriptionOrgEs.
     *
     * @return string
     */
    public function getDescriptionOrgEs()
    {
        return $this->descriptionOrgEs;
    }

    /**
     * Set adresseEs.
     *
     * @param string $adresseEs
     *
     * @return Organisme
     */
    public function setAdresseEs($adresseEs)
    {
        $this->adresseEs = $adresseEs;

        return $this;
    }

    /**
     * Get adresseEs.
     *
     * @return string
     */
    public function getAdresseEs()
    {
        return $this->adresseEs;
    }

    /**
     * Set villeEs.
     *
     * @param string $villeEs
     *
     * @return Organisme
     */
    public function setVilleEs($villeEs)
    {
        $this->villeEs = $villeEs;

        return $this;
    }

    /**
     * Get villeEs.
     *
     * @return string
     */
    public function getVilleEs()
    {
        return $this->villeEs;
    }

    /**
     * Set adresse2Es.
     *
     * @param string $adresse2Es
     *
     * @return Organisme
     */
    public function setAdresse2Es($adresse2Es)
    {
        $this->adresse2Es = $adresse2Es;

        return $this;
    }

    /**
     * Get adresse2Es.
     *
     * @return string
     */
    public function getAdresse2Es()
    {
        return $this->adresse2Es;
    }

    /**
     * Set paysEs.
     *
     * @param string $paysEs
     *
     * @return Organisme
     */
    public function setPaysEs($paysEs)
    {
        $this->paysEs = $paysEs;

        return $this;
    }

    /**
     * Get paysEs.
     *
     * @return string
     */
    public function getPaysEs()
    {
        return $this->paysEs;
    }

    /**
     * Set denominationOrgEn.
     *
     * @param string $denominationOrgEn
     *
     * @return Organisme
     */
    public function setDenominationOrgEn($denominationOrgEn)
    {
        $this->denominationOrgEn = $denominationOrgEn;

        return $this;
    }

    /**
     * Get denominationOrgEn.
     *
     * @return string
     */
    public function getDenominationOrgEn()
    {
        return $this->denominationOrgEn;
    }

    /**
     * Set descriptionOrgEn.
     *
     * @param string $descriptionOrgEn
     *
     * @return Organisme
     */
    public function setDescriptionOrgEn($descriptionOrgEn)
    {
        $this->descriptionOrgEn = $descriptionOrgEn;

        return $this;
    }

    /**
     * Get descriptionOrgEn.
     *
     * @return string
     */
    public function getDescriptionOrgEn()
    {
        return $this->descriptionOrgEn;
    }

    /**
     * Set adresseEn.
     *
     * @param string $adresseEn
     *
     * @return Organisme
     */
    public function setAdresseEn($adresseEn)
    {
        $this->adresseEn = $adresseEn;

        return $this;
    }

    /**
     * Get adresseEn.
     *
     * @return string
     */
    public function getAdresseEn()
    {
        return $this->adresseEn;
    }

    /**
     * Set villeEn.
     *
     * @param string $villeEn
     *
     * @return Organisme
     */
    public function setVilleEn($villeEn)
    {
        $this->villeEn = $villeEn;

        return $this;
    }

    /**
     * Get villeEn.
     *
     * @return string
     */
    public function getVilleEn()
    {
        return $this->villeEn;
    }

    /**
     * Set adresse2En.
     *
     * @param string $adresse2En
     *
     * @return Organisme
     */
    public function setAdresse2En($adresse2En)
    {
        $this->adresse2En = $adresse2En;

        return $this;
    }

    /**
     * Get adresse2En.
     *
     * @return string
     */
    public function getAdresse2En()
    {
        return $this->adresse2En;
    }

    /**
     * Set paysEn.
     *
     * @param string $paysEn
     *
     * @return Organisme
     */
    public function setPaysEn($paysEn)
    {
        $this->paysEn = $paysEn;

        return $this;
    }

    /**
     * Get paysEn.
     *
     * @return string
     */
    public function getPaysEn()
    {
        return $this->paysEn;
    }

    /**
     * Set denominationOrgSu.
     *
     * @param string $denominationOrgSu
     *
     * @return Organisme
     */
    public function setDenominationOrgSu($denominationOrgSu)
    {
        $this->denominationOrgSu = $denominationOrgSu;

        return $this;
    }

    /**
     * Get denominationOrgSu.
     *
     * @return string
     */
    public function getDenominationOrgSu()
    {
        return $this->denominationOrgSu;
    }

    /**
     * Set descriptionOrgSu.
     *
     * @param string $descriptionOrgSu
     *
     * @return Organisme
     */
    public function setDescriptionOrgSu($descriptionOrgSu)
    {
        $this->descriptionOrgSu = $descriptionOrgSu;

        return $this;
    }

    /**
     * Get descriptionOrgSu.
     *
     * @return string
     */
    public function getDescriptionOrgSu()
    {
        return $this->descriptionOrgSu;
    }

    /**
     * Set adresseSu.
     *
     * @param string $adresseSu
     *
     * @return Organisme
     */
    public function setAdresseSu($adresseSu)
    {
        $this->adresseSu = $adresseSu;

        return $this;
    }

    /**
     * Get adresseSu.
     *
     * @return string
     */
    public function getAdresseSu()
    {
        return $this->adresseSu;
    }

    /**
     * Set villeSu.
     *
     * @param string $villeSu
     *
     * @return Organisme
     */
    public function setVilleSu($villeSu)
    {
        $this->villeSu = $villeSu;

        return $this;
    }

    /**
     * Get villeSu.
     *
     * @return string
     */
    public function getVilleSu()
    {
        return $this->villeSu;
    }

    /**
     * Set adresse2Su.
     *
     * @param string $adresse2Su
     *
     * @return Organisme
     */
    public function setAdresse2Su($adresse2Su)
    {
        $this->adresse2Su = $adresse2Su;

        return $this;
    }

    /**
     * Get adresse2Su.
     *
     * @return string
     */
    public function getAdresse2Su()
    {
        return $this->adresse2Su;
    }

    /**
     * Set paysSu.
     *
     * @param string $paysSu
     *
     * @return Organisme
     */
    public function setPaysSu($paysSu)
    {
        $this->paysSu = $paysSu;

        return $this;
    }

    /**
     * Get paysSu.
     *
     * @return string
     */
    public function getPaysSu()
    {
        return $this->paysSu;
    }

    /**
     * Set denominationOrgDu.
     *
     * @param string $denominationOrgDu
     *
     * @return Organisme
     */
    public function setDenominationOrgDu($denominationOrgDu)
    {
        $this->denominationOrgDu = $denominationOrgDu;

        return $this;
    }

    /**
     * Get denominationOrgDu.
     *
     * @return string
     */
    public function getDenominationOrgDu()
    {
        return $this->denominationOrgDu;
    }

    /**
     * Set descriptionOrgDu.
     *
     * @param string $descriptionOrgDu
     *
     * @return Organisme
     */
    public function setDescriptionOrgDu($descriptionOrgDu)
    {
        $this->descriptionOrgDu = $descriptionOrgDu;

        return $this;
    }

    /**
     * Get descriptionOrgDu.
     *
     * @return string
     */
    public function getDescriptionOrgDu()
    {
        return $this->descriptionOrgDu;
    }

    /**
     * Set adresseDu.
     *
     * @param string $adresseDu
     *
     * @return Organisme
     */
    public function setAdresseDu($adresseDu)
    {
        $this->adresseDu = $adresseDu;

        return $this;
    }

    /**
     * Get adresseDu.
     *
     * @return string
     */
    public function getAdresseDu()
    {
        return $this->adresseDu;
    }

    /**
     * Set villeDu.
     *
     * @param string $villeDu
     *
     * @return Organisme
     */
    public function setVilleDu($villeDu)
    {
        $this->villeDu = $villeDu;

        return $this;
    }

    /**
     * Get villeDu.
     *
     * @return string
     */
    public function getVilleDu()
    {
        return $this->villeDu;
    }

    /**
     * Set adresse2Du.
     *
     * @param string $adresse2Du
     *
     * @return Organisme
     */
    public function setAdresse2Du($adresse2Du)
    {
        $this->adresse2Du = $adresse2Du;

        return $this;
    }

    /**
     * Get adresse2Du.
     *
     * @return string
     */
    public function getAdresse2Du()
    {
        return $this->adresse2Du;
    }

    /**
     * Set paysDu.
     *
     * @param string $paysDu
     *
     * @return Organisme
     */
    public function setPaysDu($paysDu)
    {
        $this->paysDu = $paysDu;

        return $this;
    }

    /**
     * Get paysDu.
     *
     * @return string
     */
    public function getPaysDu()
    {
        return $this->paysDu;
    }

    /**
     * Set denominationOrgCz.
     *
     * @param string $denominationOrgCz
     *
     * @return Organisme
     */
    public function setDenominationOrgCz($denominationOrgCz)
    {
        $this->denominationOrgCz = $denominationOrgCz;

        return $this;
    }

    /**
     * Get denominationOrgCz.
     *
     * @return string
     */
    public function getDenominationOrgCz()
    {
        return $this->denominationOrgCz;
    }

    /**
     * Set descriptionOrgCz.
     *
     * @param string $descriptionOrgCz
     *
     * @return Organisme
     */
    public function setDescriptionOrgCz($descriptionOrgCz)
    {
        $this->descriptionOrgCz = $descriptionOrgCz;

        return $this;
    }

    /**
     * Get descriptionOrgCz.
     *
     * @return string
     */
    public function getDescriptionOrgCz()
    {
        return $this->descriptionOrgCz;
    }

    /**
     * Set adresseCz.
     *
     * @param string $adresseCz
     *
     * @return Organisme
     */
    public function setAdresseCz($adresseCz)
    {
        $this->adresseCz = $adresseCz;

        return $this;
    }

    /**
     * Get adresseCz.
     *
     * @return string
     */
    public function getAdresseCz()
    {
        return $this->adresseCz;
    }

    /**
     * Set villeCz.
     *
     * @param string $villeCz
     *
     * @return Organisme
     */
    public function setVilleCz($villeCz)
    {
        $this->villeCz = $villeCz;

        return $this;
    }

    /**
     * Get villeCz.
     *
     * @return string
     */
    public function getVilleCz()
    {
        return $this->villeCz;
    }

    /**
     * Set adresse2Cz.
     *
     * @param string $adresse2Cz
     *
     * @return Organisme
     */
    public function setAdresse2Cz($adresse2Cz)
    {
        $this->adresse2Cz = $adresse2Cz;

        return $this;
    }

    /**
     * Get adresse2Cz.
     *
     * @return string
     */
    public function getAdresse2Cz()
    {
        return $this->adresse2Cz;
    }

    /**
     * Set paysCz.
     *
     * @param string $paysCz
     *
     * @return Organisme
     */
    public function setPaysCz($paysCz)
    {
        $this->paysCz = $paysCz;

        return $this;
    }

    /**
     * Get paysCz.
     *
     * @return string
     */
    public function getPaysCz()
    {
        return $this->paysCz;
    }

    /**
     * Set denominationOrgIt.
     *
     * @param string $denominationOrgIt
     *
     * @return Organisme
     */
    public function setDenominationOrgIt($denominationOrgIt)
    {
        $this->denominationOrgIt = $denominationOrgIt;

        return $this;
    }

    /**
     * Get denominationOrgIt.
     *
     * @return string
     */
    public function getDenominationOrgIt()
    {
        return $this->denominationOrgIt;
    }

    /**
     * Set descriptionOrgIt.
     *
     * @param string $descriptionOrgIt
     *
     * @return Organisme
     */
    public function setDescriptionOrgIt($descriptionOrgIt)
    {
        $this->descriptionOrgIt = $descriptionOrgIt;

        return $this;
    }

    /**
     * Get descriptionOrgIt.
     *
     * @return string
     */
    public function getDescriptionOrgIt()
    {
        return $this->descriptionOrgIt;
    }

    /**
     * Set adresseIt.
     *
     * @param string $adresseIt
     *
     * @return Organisme
     */
    public function setAdresseIt($adresseIt)
    {
        $this->adresseIt = $adresseIt;

        return $this;
    }

    /**
     * Get adresseIt.
     *
     * @return string
     */
    public function getAdresseIt()
    {
        return $this->adresseIt;
    }

    /**
     * Set villeIt.
     *
     * @param string $villeIt
     *
     * @return Organisme
     */
    public function setVilleIt($villeIt)
    {
        $this->villeIt = $villeIt;

        return $this;
    }

    /**
     * Get villeIt.
     *
     * @return string
     */
    public function getVilleIt()
    {
        return $this->villeIt;
    }

    /**
     * Set adresse2It.
     *
     * @param string $adresse2It
     *
     * @return Organisme
     */
    public function setAdresse2It($adresse2It)
    {
        $this->adresse2It = $adresse2It;

        return $this;
    }

    /**
     * Get adresse2It.
     *
     * @return string
     */
    public function getAdresse2It()
    {
        return $this->adresse2It;
    }

    /**
     * Set paysIt.
     *
     * @param string $paysIt
     *
     * @return Organisme
     */
    public function setPaysIt($paysIt)
    {
        $this->paysIt = $paysIt;

        return $this;
    }

    /**
     * Get paysIt.
     *
     * @return string
     */
    public function getPaysIt()
    {
        return $this->paysIt;
    }

    /**
     * Set siren.
     *
     * @param string $siren
     *
     * @return Organisme
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren.
     *
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set complement.
     *
     * @param string $complement
     *
     * @return Organisme
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * Get complement.
     *
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * Set moniteurProvenance.
     *
     * @param int $moniteurProvenance
     *
     * @return Organisme
     */
    public function setMoniteurProvenance($moniteurProvenance)
    {
        $this->moniteurProvenance = $moniteurProvenance;

        return $this;
    }

    /**
     * Get moniteurProvenance.
     *
     * @return int
     */
    public function getMoniteurProvenance()
    {
        return $this->moniteurProvenance;
    }

    /**
     * Set codeAccesLogiciel.
     *
     * @param string $codeAccesLogiciel
     *
     * @return Organisme
     */
    public function setCodeAccesLogiciel($codeAccesLogiciel)
    {
        $this->codeAccesLogiciel = $codeAccesLogiciel;

        return $this;
    }

    /**
     * Get codeAccesLogiciel.
     *
     * @return string
     */
    public function getCodeAccesLogiciel()
    {
        return $this->codeAccesLogiciel;
    }

    /**
     * Set decalageHoraire.
     *
     * @param string $decalageHoraire
     *
     * @return Organisme
     */
    public function setDecalageHoraire($decalageHoraire)
    {
        $this->decalageHoraire = $decalageHoraire;

        return $this;
    }

    /**
     * Get decalageHoraire.
     *
     * @return string
     */
    public function getDecalageHoraire()
    {
        return $this->decalageHoraire;
    }

    /**
     * Set lieuResidence.
     *
     * @param string $lieuResidence
     *
     * @return Organisme
     */
    public function setLieuResidence($lieuResidence)
    {
        $this->lieuResidence = $lieuResidence;

        return $this;
    }

    /**
     * Get lieuResidence.
     *
     * @return string
     */
    public function getLieuResidence()
    {
        return $this->lieuResidence;
    }

    /**
     * Set activationFuseauHoraire.
     *
     * @param string $activationFuseauHoraire
     *
     * @return Organisme
     */
    public function setActivationFuseauHoraire($activationFuseauHoraire)
    {
        $this->activationFuseauHoraire = $activationFuseauHoraire;

        return $this;
    }

    /**
     * Get activationFuseauHoraire.
     *
     * @return string
     */
    public function getActivationFuseauHoraire()
    {
        return $this->activationFuseauHoraire;
    }

    /**
     * Set alerte.
     *
     * @param string $alerte
     *
     * @return Organisme
     */
    public function setAlerte($alerte)
    {
        $this->alerte = $alerte;

        return $this;
    }

    /**
     * Get alerte.
     *
     * @return string
     */
    public function getAlerte()
    {
        return $this->alerte;
    }

    /**
     * Set ordre.
     *
     * @param int $ordre
     *
     * @return Organisme
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;

        return $this;
    }

    /**
     * Get ordre.
     *
     * @return int
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * Set urlInterfaceAnm.
     *
     * @param string $urlInterfaceAnm
     *
     * @return Organisme
     */
    public function setUrlInterfaceAnm($urlInterfaceAnm)
    {
        $this->urlInterfaceAnm = $urlInterfaceAnm;

        return $this;
    }

    /**
     * Get urlInterfaceAnm.
     *
     * @return string
     */
    public function getUrlInterfaceAnm()
    {
        return $this->urlInterfaceAnm;
    }

    /**
     * @return $this
     */
    public function addConsultation(Consultation $consultation)
    {
        $this->consultations[] = $consultation;
        $consultation->setOrganisme($this);

        return $this;
    }

    public function removeConsultation(Consultation $consultation)
    {
        $this->consultations->removeElement($consultation);
    }

    /**
     * @return ArrayCollection
     */
    public function getConsultations()
    {
        return $this->consultations;
    }

    /**
     * @return $this
     */
    public function addAgent(Agent $agent)
    {
        $this->agents[] = $agent;
        $agent->setOrganisme($this);

        return $this;
    }

    public function removeAgent(Agent $agent)
    {
        $this->agents->removeElement($agent);
    }

    /**
     * @return ArrayCollection
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * @return $this
     */
    public function addAdministrateur(Administrateur $administrateur)
    {
        $this->administrateurs[] = $administrateur;
        $administrateur->setOrganisme($this);

        return $this;
    }

    public function removeAdministrateur(Administrateur $administrateur)
    {
        $this->administrateurs->removeElement($administrateur);
    }

    /**
     * @return ArrayCollection
     */
    public function getAdministrateurs()
    {
        return $this->administrateurs;
    }

    /**
     * @return $this
     */
    public function addService(Service $service)
    {
        $this->services[] = $service;
        $service->setOrganisme($this);

        return $this;
    }

    public function removeService(Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * @return ArrayCollection
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @return $this
     */
    public function addDumeContextes(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes[] = $dumeContexte;
        $dumeContexte->setOrganisme($this);

        return $this;
    }

    public function removeDumeContextes(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes->removeElement($dumeContexte);
    }

    /**
     * @return ArrayCollection
     */
    public function getDumeContextes()
    {
        return $this->dumeContextes;
    }

    /***
     * @return string
     */
    public function __toString(): string
    {
        return $this->acronyme;
    }

    /**
     * @return ArrayCollection
     */
    public function getCandidatures()
    {
        return $this->candidatures;
    }

    /**
     * @param ArrayCollection $candidatures
     */
    public function setCandidatures($candidatures)
    {
        $this->candidatures = $candidatures;
    }

    /**
     * @return $this
     */
    public function addListeLots(TListeLotsCandidature $listLot)
    {
        $this->listLots[] = $listLot;
        $listLot->setOrganisme($this);

        return $this;
    }

    public function removeListeLots(TListeLotsCandidature $listLot)
    {
        $this->listLots->removeElement($listLot);
    }

    /**
     * @return ArrayCollection
     */
    public function getListeLots()
    {
        return $this->listLots;
    }

    /**
     * @param TelechargementAnonyme $telechargementAnonymes
     *
     * @return $this
     */
    public function addTelechargementAnonyme(TelechargementAnonyme $telechargementAnonyme)
    {
        $this->telechargementAnonymes[] = $telechargementAnonyme;
        $telechargementAnonyme->setOrganisme($this);

        return $this;
    }

    /**
     * @param TelechargementAnonyme $telechargementAnonymes
     */
    public function removeTelechargementAnonyme(TelechargementAnonyme $telechargementAnonyme)
    {
        $this->telechargementAnonymes->removeElement($telechargementAnonyme);
    }

    /**
     * @return ArrayCollection
     */
    public function getTelechargementAnonymes()
    {
        return $this->telechargementAnonymes;
    }

    /**
     * @param Telechargement $telechargements
     *
     * @return $this
     */
    public function addTelechargement(Telechargement $telechargement)
    {
        $this->telechargements[] = $telechargement;
        $telechargement->setOrganisme($this);

        return $this;
    }

    /**
     * @param Telechargement $telechargements
     */
    public function removeTelechargement(Telechargement $telechargement)
    {
        $this->telechargements->removeElement($telechargement);
    }

    /**
     * @return ArrayCollection
     */
    public function getTelechargements()
    {
        return $this->telechargements;
    }

    /**
     * @return ArrayCollection
     */
    public function getAffiliations()
    {
        return $this->affiliations;
    }

    /**
     * @return $this
     */
    public function addAffiliation(AffiliationService $affiliation)
    {
        $this->affiliations[] = $affiliation;
        $affiliation->setOrganisme($this);

        return $this;
    }

    public function removeAffiliation(AffiliationService $affiliation)
    {
        $this->affiliations->removeElement($affiliation);
    }

    /**
     * @return string
     */
    public function getPfUrl()
    {
        return $this->pfUrl;
    }

    /**
     * @param string $pfUrl
     */
    public function setPfUrl($pfUrl)
    {
        $this->pfUrl = $pfUrl;
    }

    /**
     * @return ArrayCollection
     */
    public function getEchangeDestinataires()
    {
        return $this->echangeDestinataires;
    }

    /**
     * @return $this
     */
    public function addEchangeDestinataire(EchangeDestinataire $echangeDestinataire)
    {
        $this->echangeDestinataires[] = $echangeDestinataire;
        $echangeDestinataire->setOrganisme($this);

        return $this;
    }

    public function removeEchangeDestinataire(EchangeDestinataire $echangeDestinataire)
    {
        $this->echangeDestinataires->removeElement($echangeDestinataire);
    }

    /**
     * @return ArrayCollection
     */
    public function getListLots()
    {
        return $this->listLots;
    }

    /**
     * @return $this
     */
    public function addListLots(TListeLotsCandidature $lot)
    {
        $this->listLots[] = $lot;
        $lot->setOrganisme($this);

        return $this;
    }

    public function removeListLots(TListeLotsCandidature $lot)
    {
        $this->listLots->removeElement($lot);
    }

    /**
     * @return ArrayCollection
     */
    public function getEchanges()
    {
        return $this->echanges;
    }

    /**
     * @return $this
     */
    public function addEchange(Echange $echange)
    {
        $this->echanges[] = $echange;
        $echange->setOrganisme($this);

        return $this;
    }

    public function removeEchange(Echange $echange)
    {
        $this->echanges->removeElement($echange);
    }

    /**
     * @return ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return $this
     */
    public function addQuestion(QuestionDCE $question)
    {
        $this->questions[] = $question;
        $question->setOrganisme($this);

        return $this;
    }

    public function removeQuestion(QuestionDCE $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * @return ArrayCollection
     */
    public function getEchangePieceJointes()
    {
        return $this->echangePieceJointes;
    }

    /**
     * @return $this
     */
    public function addEchangePieceJointe(EchangePieceJointe $echangePieceJointe)
    {
        $this->echangePieceJointes[] = $echangePieceJointe;
        $echangePieceJointe->setOrganisme($this);

        return $this;
    }

    public function removeEchangePieceJointe(EchangePieceJointe $echangePieceJointe)
    {
        $this->questions->removeElement($echangePieceJointe);
    }

    /**
     * @return ArrayCollection
     */
    public function getRelationEchange()
    {
        return $this->relationEchanges;
    }

    /**
     * @return $this
     */
    public function addRelationEchanges(RelationEchange $relationEchange)
    {
        $this->relationEchanges[] = $relationEchange;
        $relationEchange->setOrganisme($this);

        return $this;
    }

    public function removeRelationEchanges(RelationEchange $relationEchange)
    {
        $this->relationEchanges->removeElement($relationEchange);
    }

    /**
     * @return bool
     */
    public function isTagPurge()
    {
        return $this->tagPurge;
    }

    /**
     * @param bool $tagPurge
     */
    public function setTagPurge($tagPurge = 0)
    {
        $this->tagPurge = $tagPurge;

        return $this;
    }

    public function getIdExterne(): int
    {
        return $this->idExterne;
    }

    /**
     * @return $this
     */
    public function setIdExterne(int $idExterne)
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getAgentTechAssociations()
    {
        return $this->agentTechAssociations;
    }

    public function setAgentTechAssociations(Collection $agentTechAssociations)
    {
        $this->agentTechAssociations = $agentTechAssociations;
    }

    public function getEchangeDocApplicationClientsOrganisme(): Collection
    {
        return $this->echangeDocApplicationClientsOrganismes;
    }

    /**
     * @param $echangeDocApplicationClientOrganismes
     *
     * @return $this
     */
    public function addEchangeDocApplicationClientOrganismes($echangeDocApplicationClientOrganismes)
    {
        $this->echangeDocApplicationClientsOrganismes[] = $echangeDocApplicationClientOrganismes;

        return $this;
    }

    /**
     * @param $echangeDocApplicationClientOrganismes
     */
    public function removeEchangeDocApplicationClientOrganismes($echangeDocApplicationClientOrganismes)
    {
        $this->echangeDocApplicationClientsOrganismes->removeElement($echangeDocApplicationClientOrganismes);
    }

    /**
     * @return int
     */
    public function getIdEntite(): ?int
    {
        return $this->idEntite;
    }

    /**
     * @param int $idEntite
     */
    public function setIdEntite($idEntite): Organisme
    {
        $this->idEntite = $idEntite;

        return $this;
    }

    public function getPlateformeVirtuelleOrganisme(): Collection
    {
        return $this->plateformeVirtuelleOrganisme;
    }

    public function setPlateformeVirtuelleOrganisme(Collection $plateformeVirtuelleOrganisme): Organisme
    {
        $this->plateformeVirtuelleOrganisme = $plateformeVirtuelleOrganisme;

        return $this;
    }

    /**
     * @return Collection|DocumentTemplateSurcharge[]
     */
    public function getDocumentTemplateSurcharges(): Collection
    {
        return $this->documentTemplateSurcharges;
    }

    public function addDocumentTemplateSurcharge(DocumentTemplateSurcharge $documentTemplateSurcharge): self
    {
        if (!$this->documentTemplateSurcharges->contains($documentTemplateSurcharge)) {
            $this->documentTemplateSurcharges[] = $documentTemplateSurcharge;
            $documentTemplateSurcharge->setOrganisme($this);
        }

        return $this;
    }

    public function removeDocumentTemplateSurcharge(DocumentTemplateSurcharge $documentTemplateSurcharge): self
    {
        if ($this->documentTemplateSurcharges->removeElement($documentTemplateSurcharge)) {
            if ($documentTemplateSurcharge->getOrganisme() === $this) {
                $documentTemplateSurcharge->setOrganisme(null);
            }
        }

        return $this;
    }

    public function getSousTypeOrganisme(): int
    {
        return $this->sousTypeOrganisme;
    }

    public function setSousTypeOrganisme(int $sousTypeOrganisme): self
    {
        $this->sousTypeOrganisme = $sousTypeOrganisme;

        return $this;
    }
}
