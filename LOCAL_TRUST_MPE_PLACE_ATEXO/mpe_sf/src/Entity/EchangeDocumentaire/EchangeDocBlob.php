<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * EchangeDocBlob.
 *
 * @ORM\Table(name="echange_doc_blob",
 *     indexes={
 *     @Index(name="IDX_echange_doc_id", columns={"echange_doc_id"}),
 *     @Index(name="IDX_blob_organisme_id", columns={"blob_organisme_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocBlobRepository")
 */
class EchangeDocBlob
{
    public const FICHIER_PRINCIPAL = 1;
    public const FICHIER_JETON = 2;
    public const FICHIER_ANNEXE = 3;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="EchangeDoc", inversedBy="echangeDocBlobs")
     * @ORM\JoinColumn(name="echange_doc_id", referencedColumnName="id")
     */
    private $echangeDoc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="blob_organisme_id", referencedColumnName="id")
     */
    private $blobOrganisme;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EchangeDocumentaire\EchangeDocTypePieceActes", inversedBy="echangeDocBlobs")
     * @ORM\JoinColumn(name="echange_type_piece_actes_id", referencedColumnName="id")
     */
    private ?EchangeDocTypePieceActes $echangeTypeActes = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EchangeDocumentaire\EchangeDocTypePieceStandard", inversedBy="echangeDocBlobs")
     * @ORM\JoinColumn(name="echange_type_piece_standard_id", referencedColumnName="id")
     */
    private ?EchangeDocTypePieceStandard $echangeTypeStandard = null;

    /**
     * @var string
     *
     * @ORM\Column(name="chemin", type="text", nullable=true)
     */
    private $chemin;

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="checksum", type="text", nullable=true)
     */
    private $checksum;

    /**
     * @ORM\Column(name="categorie_piece", type="integer")
     */
    private ?int $categoriePiece = null;

    /**
     * Echange Doc Blob principal
     *
     * @ORM\ManyToOne(targetEntity="EchangeDocBlob", inversedBy="annexes")
     * @ORM\JoinColumn(name="doc_blob_principal_id", referencedColumnName="id", nullable=true)
     */
    private ?\App\Entity\EchangeDocumentaire\EchangeDocBlob $docPrincipal = null;

    /**
     * Echange doc Bloc annexes
     * @ORM\OneToMany(targetEntity="EchangeDocBlob", mappedBy="docPrincipal")
     */
    private array|Collection|ArrayCollection $annexes;

    public function __construct()
    {
        $this->annexes = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEchangeDoc()
    {
        return $this->echangeDoc;
    }

    public function setEchangeDoc(EchangeDoc $echangeDoc): self
    {
        $this->echangeDoc = $echangeDoc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBlobOrganisme()
    {
        return $this->blobOrganisme;
    }

    /**
     * @param mixed $blobOrganisme
     *
     * @return EchangeDoc
     */
    public function setBlobOrganisme($blobOrganisme)
    {
        $this->blobOrganisme = $blobOrganisme;

        return $this;
    }

    /**
     * @return string
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * @param $chemin
     */
    public function setChemin($chemin): EchangeDocBlob
    {
        $this->chemin = $chemin;

        return $this;
    }

    /**
     * @return string
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @param $poids
     */
    public function setPoids($poids): EchangeDocBlob
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * @return string
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * @param $checksum
     */
    public function setChecksum($checksum): EchangeDocBlob
    {
        $this->checksum = $checksum;

        return $this;
    }

    /**
     * Set echangeTypeActes.
     */
    public function setEchangeTypeActes(EchangeDocTypePieceActes $echangeTypeActes = null): self
    {
        $this->echangeTypeActes = $echangeTypeActes;

        return $this;
    }

    /**
     * Get echangeTypeActes.
     */
    public function getEchangeTypeActes(): ?EchangeDocTypePieceActes
    {
        return $this->echangeTypeActes;
    }

    /**
     * Set echangeTypeStandard.
     */
    public function setEchangeTypeStandard(EchangeDocTypePieceStandard $echangeTypeStandard = null): self
    {
        $this->echangeTypeStandard = $echangeTypeStandard;

        return $this;
    }

    /**
     * Get echangeTypeStandard.
     */
    public function getEchangeTypeStandard(): ?EchangeDocTypePieceStandard
    {
        return $this->echangeTypeStandard;
    }

    public function getCategoriePiece(): ?int
    {
        return $this->categoriePiece;
    }

    /**
     * @param mixed $categoriePiece
     */
    public function setCategoriePiece(int $categoriePiece): EchangeDocBlob
    {
        $this->categoriePiece = $categoriePiece;

        return $this;
    }

    public function getDocPrincipal(): ?EchangeDocBlob
    {
        return $this->docPrincipal;
    }

    public function setDocPrincipal(?EchangeDocBlob $docPrincipal): EchangeDocBlob
    {
        $this->docPrincipal = $docPrincipal;

        return $this;
    }

    public function getAnnexes(): Collection
    {
        return $this->annexes;
    }

    /**
     * @param $annexe
     */
    public function addAnnexe($annexe): EchangeDocBlob
    {
        $this->annexes[] = $annexe;

        return $this;
    }

    /**
     * @param $annexe
     */
    public function removeAnnexe($annexe): EchangeDocBlob
    {
        $this->annexes->removeElement($annexe);

        return $this;
    }
}
