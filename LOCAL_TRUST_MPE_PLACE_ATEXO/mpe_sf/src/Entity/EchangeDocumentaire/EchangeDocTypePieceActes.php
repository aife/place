<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EchangeDocTypePieceActes.
 *
 * @ORM\Table(name="echange_doc_type_piece_actes")
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocTypePieceActesRepository")
 */
class EchangeDocTypePieceActes
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private ?string $typeNature = null;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private ?string $codeClassification = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EchangeDocumentaire\EchangeDocBlob", mappedBy="echangeTypeActes")
     */
    private array|Collection|ArrayCollection $echangeDocBlobs;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->echangeDocBlobs = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): EchangeDocTypePieceActes
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): EchangeDocTypePieceActes
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getTypeNature(): string
    {
        return $this->typeNature;
    }

    public function setTypeNature(string $typeNature): EchangeDocTypePieceStandard
    {
        $this->typeNature = $typeNature;

        return $this;
    }

    public function getCodeClassification(): string
    {
        return $this->codeClassification;
    }

    public function setCodeClassification(string $codeClassification): EchangeDocTypePieceStandard
    {
        $this->codeClassification = $codeClassification;

        return $this;
    }

    /**
     * Add echangeDocBlob.
     *
     *
     */
    public function addEchangeDocBlob(
        EchangeDocBlob $echangeDocBlob
    ): self {
        $this->echangeDocBlobs[] = $echangeDocBlob;

        return $this;
    }

    /**
     * Remove echangeDocBlob.
     *
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeEchangeDocBlob(
        EchangeDocBlob $echangeDocBlob
    ): bool {
        return $this->echangeDocBlobs->removeElement($echangeDocBlob);
    }

    /**
     * Get echangeDocBlobs.
     */
    public function getEchangeDocBlobs(): Collection
    {
        return $this->echangeDocBlobs;
    }
}
