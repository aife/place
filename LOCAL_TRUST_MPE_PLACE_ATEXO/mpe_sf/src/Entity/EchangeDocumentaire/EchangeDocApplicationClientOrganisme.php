<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * EchangeDocApplicationClientOrganisme.
 *
 * @ORM\Table(name="echange_doc_application_client_organisme",
 *     indexes={
 *     @Index(name="IDX_echange_doc_application_client_id",columns={"echange_doc_application_client_id"}),
 *     @Index(name="organisme_acronyme_edaco_fk",columns={"organisme_acronyme"})
 *     })
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocApplicationClientOrganismeRepository")
 */
class EchangeDocApplicationClientOrganisme
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="EchangeDocApplicationClient", inversedBy="echangeDocApplicationClientOrganismes")
     * @ORM\JoinColumn(name="echange_doc_application_client_id", referencedColumnName="id")
     */
    private $echangeDocApplicationClient;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="echangeDocApplicationClientsOrganismes")
     * @ORM\JoinColumn(name="organisme_acronyme", referencedColumnName="acronyme")
     */
    private $organisme;

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEchangeDocApplicationClient()
    {
        return $this->echangeDocApplicationClient;
    }

    /**
     * @param mixed $echangeDocApplicationClient
     *
     * @return EchangeDocApplicationClientOrganisme
     */
    public function setEchangeDocApplicationClient($echangeDocApplicationClient)
    {
        $this->echangeDocApplicationClient = $echangeDocApplicationClient;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     *
     * @return EchangeDocApplicationClientOrganisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }
}
