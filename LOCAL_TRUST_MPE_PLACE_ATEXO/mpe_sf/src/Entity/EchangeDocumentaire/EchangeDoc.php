<?php

namespace App\Entity\EchangeDocumentaire;

use App\Entity\ReferentielSousTypeParapheur;
use App\Traits\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * EchangeDoc.
 *
 * @ORM\Table(name="echange_doc",indexes={
 *     @Index(name="IDX_echange_doc_application_client_id", columns={"echange_doc_application_client_id"}),
 *     @Index(name="IDX_consultation_id", columns={"consultation_id"}),
 *     @Index(name="IDX_agent_id", columns={"agent_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EchangeDoc
{
    use TimestampableTrait;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="EchangeDocApplicationClient", inversedBy="echangeDocs")
     * @ORM\JoinColumn(name="echange_doc_application_client_id", referencedColumnName="id")
     */
    private $echangeDocApplicationClient;

    /**
     * @ORM\OneToMany(targetEntity="EchangeDocBlob", mappedBy="echangeDoc")
     */
    private Collection $echangeDocBlobs;

    /**
     * @ORM\Column(name="objet", type="string", length=100, nullable=false)
     */
    private ?string $objet = null;

    /**
     * @ORM\Column(name="description", type="string", length=500, nullable=false)
     */
    private ?string $description = null;

    /**
     * @ORM\Column(name="statut", type="string", length=50, nullable=false)
     */
    private ?string $statut = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private $consultation;

    /**
     * @ORM\OneToMany(targetEntity="EchangeDocHistorique", mappedBy="echangeDoc")
     */
    private Collection|ArrayCollection $echangeDocHistoriques;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_signature", type="boolean", options={"default":false})
     */
    private $cheminementSignature;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_ged", type="boolean", options={"default":false})
     */
    private $cheminementGed;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_sae", type="boolean", options={"default":false})
     */
    private $cheminementSae;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_tdt", type="boolean", options={"default":false})
     */
    private $cheminementTdt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContratTitulaire", inversedBy="echangeDoc")
     * @ORM\JoinColumn(name="id_contrat_titulaire", referencedColumnName="id_contrat_titulaire")
     */
    private $contratTitulaire;

    /**
     * @var int
     */
    private $montantContrat;

    /**
     * @var string
     */
    private $raisonSocialeTitulaire;

    /**
     * @ORM\ManyToOne(targetEntity=ReferentielSousTypeParapheur::class)
     * @ORM\JoinColumn(name="referentiel_sous_type_parapheur_id", referencedColumnName="id")
     */
    private ?ReferentielSousTypeParapheur $referentielSousTypeParapheur = null;

    /**
     * @ORM\Column(name="uuid_externe_exec", type="string", length=50, nullable=true)
     */
    private ?string $uuidExterneExec = null;

    private ?string $numeroContrat = null;

    public function __construct()
    {
        $this->echangeDocBlobs = new ArrayCollection();
        $this->echangeDocHistoriques = new ArrayCollection();
        $this->montantContrat = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEchangeDocApplicationClient(): EchangeDocApplicationClient
    {
        return $this->echangeDocApplicationClient;
    }

    /**
     * @param mixed $echangeDocApplicationClient
     *
     * @return EchangeDoc
     */
    public function setEchangeDocApplicationClient($echangeDocApplicationClient)
    {
        $this->echangeDocApplicationClient = $echangeDocApplicationClient;

        return $this;
    }

    public function getObjet(): string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): EchangeDoc
    {
        $this->objet = $objet;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): EchangeDoc
    {
        $this->description = $description;

        return $this;
    }

    public function getEchangeDocBlobs(): Collection
    {
        return $this->echangeDocBlobs;
    }

    public function addEchangeDocsBlob(EchangeDocBlob $echangeDocBlob): self
    {
        if (!$this->echangeDocBlobs->contains($echangeDocBlob)) {
            $this->echangeDocBlobs[] = $echangeDocBlob;
            $echangeDocBlob->setEchangeDoc($this);
        }

        return $this;
    }

    public function getStatut(): string
    {
        return $this->statut;
    }

    public function setStatut(string $statut): EchangeDoc
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @param mixed $consultation
     *
     * @return EchangeDoc
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     *
     * @return EchangeDoc
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;

        return $this;
    }

    public function getEchangeDocHistoriques(): Collection
    {
        return $this->echangeDocHistoriques;
    }

    /**
     * @param $echangeDocHistorique
     *
     * @return $this
     */
    public function addEchangeDocHistorique($echangeDocHistorique)
    {
        $this->echangeDocHistoriques[] = $echangeDocHistorique;

        return $this;
    }

    /**
     * @param $echangeDocHistorique
     */
    public function removeEchangeDocHistorique($echangeDocHistorique)
    {
        $this->echangeDocHistoriques->removeElement($echangeDocHistorique);
    }

    /**
     * @return bool
     */
    public function isCheminementSignature(): ?bool
    {
        return $this->cheminementSignature;
    }

    public function setCheminementSignature(bool $cheminementSignature): EchangeDoc
    {
        $this->cheminementSignature = $cheminementSignature;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCheminementGed(): ?bool
    {
        return $this->cheminementGed;
    }

    public function setCheminementGed(bool $cheminementGed): EchangeDoc
    {
        $this->cheminementGed = $cheminementGed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCheminementSae(): ?bool
    {
        return $this->cheminementSae;
    }

    public function setCheminementSae(bool $cheminementSae): EchangeDoc
    {
        $this->cheminementSae = $cheminementSae;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCheminementTdt(): ?bool
    {
        return $this->cheminementTdt;
    }

    public function setCheminementTdt(bool $cheminementTdt): EchangeDoc
    {
        $this->cheminementTdt = $cheminementTdt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContratTitulaire()
    {
        return $this->contratTitulaire;
    }

    /**
     * @param $contratTitulaire
     */
    public function setContratTitulaire($contratTitulaire): EchangeDoc
    {
        $this->contratTitulaire = $contratTitulaire;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMontantContrat(): ?float
    {
        return $this->montantContrat;
    }

    /**
     * @param $montantContrat
     */
    public function setMontantContrat($montantContrat): EchangeDoc
    {
        $this->montantContrat = $montantContrat;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRaisonSocialeTitulaire(): ?string
    {
        return $this->raisonSocialeTitulaire;
    }

    /**
     * @param $raisonSocialeTitulaire
     */
    public function setRaisonSocialeTitulaire($raisonSocialeTitulaire): EchangeDoc
    {
        $this->raisonSocialeTitulaire = $raisonSocialeTitulaire;

        return $this;
    }

    public function getReferentielSousTypeParapheur(): ?ReferentielSousTypeParapheur
    {
        return $this->referentielSousTypeParapheur;
    }

    public function setReferentielSousTypeParapheur(?ReferentielSousTypeParapheur $referentielSousTypeParapheur): self
    {
        $this->referentielSousTypeParapheur = $referentielSousTypeParapheur;

        return $this;
    }

    public function getUuidExterneExec(): ?string
    {
        return $this->uuidExterneExec;
    }

    public function setUuidExterneExec(?string $uuidExterneExec): self
    {
        $this->uuidExterneExec = $uuidExterneExec;

        return $this;
    }

    public function getNumeroContrat(): ?string
    {
        return $this->numeroContrat;
    }

    public function setNumeroContrat(?string $numeroContrat): self
    {
        $this->numeroContrat = $numeroContrat;

        return $this;
    }
}
