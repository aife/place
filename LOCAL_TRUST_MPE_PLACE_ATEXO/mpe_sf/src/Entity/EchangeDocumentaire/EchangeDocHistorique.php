<?php

namespace App\Entity\EchangeDocumentaire;

use DateTime;
use App\Entity\Agent;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * EchangeDoc.
 *
 * @ORM\Table(name="echange_doc_historique",indexes={
 *     @Index(name="IDX_agent_id", columns={"agent_id"}),
 *     @Index(name="IDX_echange_doc_id", columns={"echange_doc_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocHistoriqueRepository")
 */
class EchangeDocHistorique
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EchangeDocumentaire\EchangeDoc", inversedBy="echangeDocHistoriques")
     * @ORM\JoinColumn(name="echange_doc_id", referencedColumnName="id")
     */
    private $echangeDoc;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="agent_id", referencedColumnName="id")
     */
    private $agent;

    /**
     * @var DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)*/
    private $createdAt;

    /**
     * @ORM\Column(name="statut", type="string", length=50, nullable=true)
     */
    private ?string $statut = null;

    /**
     * @ORM\Column(name="message_fonctionnel", type="string", length=500, nullable=true)
     */
    private ?string $messageFonctionnel = null;

    /**
     * @ORM\Column(name="message_technique", type="string", length=1000, nullable=true)
     */
    private ?string $messageTechnique = null;

    public function __construct()
    {
        $this->createdAt = new DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getEchangeDoc()
    {
        return $this->echangeDoc;
    }

    /**
     * @param mixed $echangeDoc
     */
    public function setEchangeDoc($echangeDoc)
    {
        $this->echangeDoc = $echangeDoc;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessageFonctionnel()
    {
        return $this->messageFonctionnel;
    }

    /**
     * @param string| null $messageFnctionnel
     */
    public function setMessageFonctionnel(?string $messageFonctionnel)
    {
        $this->messageFonctionnel = $messageFonctionnel;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessageTechnique()
    {
        return $this->messageTechnique;
    }

    public function setMessageTechnique(?string $messageTechnique)
    {
        $this->messageTechnique = $messageTechnique;

        return $this;
    }

    /**
     * @return string | NULL
     */
    public function getStatut()
    {
        return $this->statut;
    }

    public function setStatut(string $statut)
    {
        $this->statut = $statut;

        return $this;
    }
}
