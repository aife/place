<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EchangeDocApplication.
 *
 * @ORM\Table(name="echange_doc_application")
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocApplicationRepository")
 */
class EchangeDocApplication
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="flux_actes", type="boolean", options={"default":false})
     */
    private $fluxActes;
    /**
     * @var bool
     *
     * @ORM\Column(name="primo_signature", type="boolean", options={"default":false})
     */
    private $primoSignature;

    /**
     * @ORM\OneToMany(targetEntity="EchangeDocApplicationClient", mappedBy="echangeDocApplication")
     */
    private array|Collection|ArrayCollection $echangeDocApplicationClients;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private bool $envoiDic = false;

    public function __construct()
    {
        $this->echangeDocApplicationClients = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): EchangeDocApplication
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): EchangeDocApplication
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getEchangeDocApplicationClients(): ArrayCollection
    {
        return $this->echangeDocApplicationClients;
    }

    /**
     * @param $echangeDocApplicationClients
     *
     * @return $this
     */
    public function addEchangeDocApplicationClients($echangeDocApplicationClients)
    {
        $this->echangeDocApplicationClients[] = $echangeDocApplicationClients;

        return $this;
    }

    /**
     * @param $echangeDocApplicationClients
     */
    public function removeEchangeDocApplicationClients($echangeDocApplicationClients)
    {
        $this->echangeDocApplicationClients->removeElement($echangeDocApplicationClients);
    }

    public function isFluxActes(): bool
    {
        return $this->fluxActes;
    }

    public function setFluxActes(bool $fluxActes): EchangeDocApplication
    {
        $this->fluxActes = $fluxActes;

        return $this;
    }

    public function isPrimoSignature(): bool
    {
        return $this->primoSignature;
    }

    public function setPrimoSignature(bool $primoSignature): EchangeDocApplication
    {
        $this->primoSignature = $primoSignature;

        return $this;
    }

    public function isEnvoiDic(): bool
    {
        return $this->envoiDic;
    }

    public function setEnvoiDic(bool $envoiDic): self
    {
        $this->envoiDic = $envoiDic;

        return $this;
    }
}
