<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * EchangeDocApplicationClient.
 *
 * @ORM\Table(name="echange_doc_application_client",indexes={@Index(name="IDX_application_id", columns={"echange_doc_application_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocApplicationClientRepository")
 */
class EchangeDocApplicationClient
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="EchangeDocApplication", inversedBy="echangeDocApplicationClients")
     * @ORM\JoinColumn(name="echange_doc_application_id", referencedColumnName="id")
     */
    private $echangeDocApplication;

    /**
     * @ORM\Column(name="code", type="string", length=255, nullable=false)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @var string
     *
     * @ORM\Column(name="actif", type="boolean", options={"default":true})
     */
    private $actif;

    /**
     * @ORM\OneToMany(targetEntity="EchangeDocApplicationClientOrganisme", mappedBy="echangeDocApplicationClient")
     */
    private Collection $echangeDocApplicationClientOrganismes;

    /**
     * @ORM\OneToMany(targetEntity="EchangeDoc", mappedBy="echangeDocApplicationClient")
     */
    private array|Collection|ArrayCollection $echangeDocs;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_signature", type="boolean", options={"default":false})
     */
    private $cheminementSignature;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_ged", type="boolean", options={"default":false})
     */
    private $cheminementGed;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_sae", type="boolean", options={"default":false})
     */
    private $cheminementSae;

    /**
     * @var bool
     *
     * @ORM\Column(name="cheminement_tdt", type="boolean", options={"default":false})
     */
    private $cheminementTdt;

    /**
     * @ORM\Column(name="classification_1", type="string", length=255, nullable=true)
     */
    private ?string $classification1 = null;

    /**
     * @ORM\Column(name="classification_2", type="string", length=255, nullable=true)
     */
    private ?string $classification2 = null;

    /**
     * @ORM\Column(name="classification_3", type="string", length=255, nullable=true)
     */
    private ?string $classification3 = null;

    /**
     * @ORM\Column(name="classification_4", type="string", length=255, nullable=true)
     */
    private ?string $classification4 = null;

    /**
     * @ORM\Column(name="classification_5", type="string", length=255, nullable=true)
     */
    private ?string $classification5 = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="multi_docs_principaux", type="boolean", options={"default":false})
     */
    private $multiDocsPrincipaux = false;

    /**
     * @ORM\Column(name="envoi_auto_archivage", type="boolean", options={"default":false})
     */
    private bool $envoiAutoArchivage = false;


    public function __construct()
    {
        $this->echangeDocApplicationClientOrganismes = new ArrayCollection();
        $this->echangeDocs = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getEchangeDocApplication(): ?EchangeDocApplication
    {
        return $this->echangeDocApplication;
    }

    /**
     * @param mixed $echangeDocApplication
     *
     * @return EchangeDocApplicationClient
     */
    public function setEchangeDocApplication($echangeDocApplication)
    {
        $this->echangeDocApplication = $echangeDocApplication;

        return $this;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): EchangeDocApplicationClient
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): EchangeDocApplicationClient
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getActif(): string
    {
        return $this->actif;
    }

    public function setActif(string $actif): EchangeDocApplicationClient
    {
        $this->actif = $actif;

        return $this;
    }

    public function getEchangeDocApplicationClientOrganismes(): ArrayCollection
    {
        return $this->echangeDocApplicationClientOrganismes;
    }

    public function getEchangeDocs(): Collection
    {
        return $this->echangeDocs;
    }

    /**
     * @param $echangeDocApplicationClientOrganismes
     *
     * @return $this
     */
    public function addEchangeDocApplicationClientOrganismes($echangeDocApplicationClientOrganismes)
    {
        $this->echangeDocApplicationClientOrganismes[] = $echangeDocApplicationClientOrganismes;

        return $this;
    }

    /**
     * @param $echangeDocApplicationClientOrganismes
     */
    public function removeEchangeDocApplicationClientOrganismes($echangeDocApplicationClientOrganismes)
    {
        $this->echangeDocApplicationClientOrganismes->removeElement($echangeDocApplicationClientOrganismes);
    }

    /**
     * @param $echangeDocs
     *
     * @return $this
     */
    public function addEchangeDocs($echangeDocs)
    {
        $this->echangeDocs[] = $echangeDocs;

        return $this;
    }

    /**
     * @param $echangeDocs
     */
    public function removeEchangeDocs($echangeDocs)
    {
        $this->echangeDocs->removeElement($echangeDocs);
    }

    public function isCheminementSignature(): bool
    {
        return $this->cheminementSignature;
    }

    public function setCheminementSignature(bool $cheminementSignature): EchangeDocApplicationClient
    {
        $this->cheminementSignature = $cheminementSignature;

        return $this;
    }

    public function isCheminementGed(): bool
    {
        return $this->cheminementGed;
    }

    public function setCheminementGed(bool $cheminementGed): EchangeDocApplicationClient
    {
        $this->cheminementGed = $cheminementGed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCheminementSae(): ?bool
    {
        return $this->cheminementSae;
    }

    public function setCheminementSae(bool $cheminementSae): EchangeDocApplicationClient
    {
        $this->cheminementSae = $cheminementSae;

        return $this;
    }

    public function isCheminementTdt(): bool
    {
        return $this->cheminementTdt;
    }

    public function setCheminementTdt(bool $cheminementTdt): EchangeDocApplicationClient
    {
        $this->cheminementTdt = $cheminementTdt;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassification1(): ?string
    {
        return $this->classification1;
    }

    public function setClassification1(string $classification1): EchangeDocApplicationClient
    {
        $this->classification1 = $classification1;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassification2(): ?string
    {
        return $this->classification2;
    }

    public function setClassification2(string $classification2): EchangeDocApplicationClient
    {
        $this->classification2 = $classification2;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassification3(): ?string
    {
        return $this->classification3;
    }

    public function setClassification3(string $classification3): EchangeDocApplicationClient
    {
        $this->classification3 = $classification3;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassification4(): ?string
    {
        return $this->classification4;
    }

    public function setClassification4(string $classification4): EchangeDocApplicationClient
    {
        $this->classification4 = $classification4;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassification5(): ?string
    {
        return $this->classification5;
    }

    public function setClassification5(string $classification5): EchangeDocApplicationClient
    {
        $this->classification5 = $classification5;

        return $this;
    }

    public function isMultiDocsPrincipaux(): bool
    {
        return $this->multiDocsPrincipaux;
    }

    public function setMultiDocsPrincipaux(bool $multiDocsPrincipaux): self
    {
        $this->multiDocsPrincipaux = $multiDocsPrincipaux;

        return $this;
    }

    public function isEnvoiAutoArchivage(): bool
    {
        return $this->envoiAutoArchivage;
    }

    public function setEnvoiAutoArchivage(bool $envoiAutoArchivage): self
    {
        $this->envoiAutoArchivage = $envoiAutoArchivage;

        return $this;
    }
}
