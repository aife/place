<?php

namespace App\Entity\EchangeDocumentaire;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * EchangeDocTypePieceStandard.
 *
 * @ORM\Table(name="echange_doc_type_piece_standard")
 * @ORM\Entity(repositoryClass="App\Repository\EchangeDocumentaire\EchangeDocTypePieceStandardRepository")
 */
class EchangeDocTypePieceStandard
{
    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=false)
     */
    private ?string $code = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EchangeDocumentaire\EchangeDocBlob", mappedBy="echangeTypeStandard")
     */
    private array|Collection|ArrayCollection $echangeDocBlobs;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->echangeDocBlobs = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): EchangeDocTypePieceStandard
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): EchangeDocTypePieceStandard
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Add echangeDocBlob.
     *
     *
     */
    public function addEchangeDocBlob(
        EchangeDocBlob $echangeDocBlob
    ): self {
        $this->echangeDocBlobs[] = $echangeDocBlob;

        return $this;
    }

    /**
     * Remove echangeDocBlob.
     *
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeEchangeDocBlob(
        EchangeDocBlob $echangeDocBlob
    ): bool {
        return $this->echangeDocBlobs->removeElement($echangeDocBlob);
    }

    /**
     * Get echangeDocBlobs.
     */
    public function getEchangeDocBlobs(): Collection
    {
        return $this->echangeDocBlobs;
    }
}
