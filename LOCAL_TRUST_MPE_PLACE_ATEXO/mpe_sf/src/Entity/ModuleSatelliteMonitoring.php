<?php

namespace App\Entity;

use App\Repository\ModuleSatelliteMonitoringRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ModuleSatelliteMonitoringRepository::class)
 */
class ModuleSatelliteMonitoring
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $service;

    /**
     * @ORM\Column(type="json")
     */
    private $jsonResponse = [];

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getService(): ?string
    {
        return $this->service;
    }

    public function setService(string $service): self
    {
        $this->service = $service;

        return $this;
    }

    public function getJsonResponse(): ?array
    {
        return $this->jsonResponse;
    }

    public function setJsonResponse(array $jsonResponse): self
    {
        $this->jsonResponse = $jsonResponse;

        return $this;
    }
}
