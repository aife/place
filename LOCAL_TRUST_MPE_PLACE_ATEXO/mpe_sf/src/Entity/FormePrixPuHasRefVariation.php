<?php

namespace App\Entity;

use App\Repository\FormePrixPuHasRefVariationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_forme_prix_pu_has_ref_variation" )
 * @ORM\Entity(repositoryClass=FormePrixPuHasRefVariationRepository::class)
 */
class FormePrixPuHasRefVariation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_forme_prix", type="integer")
     */
    private ?int $idFormePrix;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="id_variation", type="integer", nullable=false)
     */
    private ?int $idVariation;

    public function getIdFormePrix(): ?int
    {
        return $this->idFormePrix;
    }

    public function setIdFormePrix(?int $idFormePrix): self
    {
        $this->idFormePrix = $idFormePrix;

        return $this;
    }

    public function getIdVariation(): ?int
    {
        return $this->idVariation;
    }

    public function setIdVariation(?int $idVariation): self
    {
        $this->idVariation = $idVariation;

        return $this;
    }
}
