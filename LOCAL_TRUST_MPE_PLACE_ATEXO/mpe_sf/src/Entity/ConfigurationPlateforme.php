<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ConfigurationPlateforme.
 *
 * @ORM\Table(name="configuration_plateforme")
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationPlateformeRepository")
 */
class ConfigurationPlateforme
{
    /**
     *
     * @ORM\Column(name="id_auto", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idAuto = null;

    /**
     * @ORM\Column(name="code_cpv", type="string", nullable=false)
     */
    private string $codeCpv = '0';

    /**
     * @ORM\Column(name="multi_linguisme_entreprise", type="string", nullable=false)
     */
    private string $multiLinguismeEntreprise = '0';

    /**
     * @ORM\Column(name="gestion_fournisseurs_docs_mes_sous_services", type="string", nullable=false)
     */
    private string $gestionFournisseursDocsMesSousServices = '1';

    /**
     * @ORM\Column(name="authenticate_inscrit_by_cert", type="string", nullable=false)
     */
    private string $authenticateInscritByCert = '0';

    /**
     * @ORM\Column(name="authenticate_inscrit_by_login", type="string", nullable=false)
     */
    private string $authenticateInscritByLogin = '1';

    /**
     * @ORM\Column(name="base_qualifiee_entreprise_insee", type="string", nullable=false)
     */
    private string $baseQualifieeEntrepriseInsee = '0';

    /**
     * @ORM\Column(name="gestion_boamp_mes_sous_services", type="string", nullable=false)
     */
    private string $gestionBoampMesSousServices = '1';

    /**
     * @ORM\Column(name="gestion_bi_cle_mes_sous_services", type="string", nullable=false)
     */
    private string $gestionBiCleMesSousServices = '1';

    /**
     * @ORM\Column(name="nom_entreprise_toujours_visible", type="string", nullable=false)
     */
    private string $nomEntrepriseToujoursVisible = '0';

    /**
     * @ORM\Column(name="gestion_jal_mes_sous_services", type="string", nullable=false)
     */
    private string $gestionJalMesSousServices = '1';

    /**
     * @ORM\Column(name="choix_langue_affichage_consultation", type="string", nullable=false)
     */
    private string $choixLangueAffichageConsultation = '0';

    /**
     * @ORM\Column(name="compte_entreprise_donnees_complementaires", type="string", nullable=false)
     */
    private string $compteEntrepriseDonneesComplementaires = '0';

    /**
     * @ORM\Column(name="annuaire_entites_achat_visible_par_entreprise", type="string", nullable=false)
     */
    private string $annuaireEntitesAchatVisibleParEntreprise = '1';

    /**
     * @ORM\Column(name="affichage_recherche_avancee_agent_ac_sad_transversaux", type="string", nullable=false)
     */
    private string $affichageRechercheAvanceeAgentAcSadTransversaux = '0';

    /**
     * @ORM\Column(name="encheres_entreprise", type="string", nullable=false)
     */
    private string $encheresEntreprise = '0';

    /**
     * @ORM\Column(name="socle_interne", type="string", nullable=false)
     */
    private string $socleInterne = '0';

    /**
     * @ORM\Column(name="module_certificat", type="string", nullable=false)
     */
    private string $moduleCertificat = '0';

    /**
     * @ORM\Column(name="socle_externe_agent", type="string", nullable=false)
     */
    private string $socleExterneAgent = '0';

    /**
     * @ORM\Column(name="afficher_image_organisme", type="string", nullable=false)
     */
    private string $afficherImageOrganisme = '0';

    /**
     * @ORM\Column(name="socle_externe_entreprise", type="string", nullable=false)
     */
    private string $socleExterneEntreprise = '0';

    /**
     * @ORM\Column(name="portail_defense_entreprise", type="string", nullable=false)
     */
    private string $portailDefenseEntreprise = '0';

    /**
     * @ORM\Column(name="compte_entreprise_province", type="string", nullable=false)
     */
    private string $compteEntrepriseProvince = '0';

    /**
     * @ORM\Column(name="compte_entreprise_telephone3", type="string", nullable=false)
     */
    private string $compteEntrepriseTelephone3 = '0';

    /**
     * @ORM\Column(name="compte_entreprise_tax_prof", type="string", nullable=false)
     */
    private string $compteEntrepriseTaxProf = '0';

    /**
     * @ORM\Column(name="compte_entreprise_rcville", type="string", nullable=false)
     */
    private string $compteEntrepriseRcville = '0';

    /**
     * @ORM\Column(name="compte_entreprise_declaration_honneur", type="string", nullable=false)
     */
    private string $compteEntrepriseDeclarationHonneur = '0';

    /**
     * @ORM\Column(name="compte_entreprise_qualification", type="string", nullable=false)
     */
    private string $compteEntrepriseQualification = '0';

    /**
     * @ORM\Column(name="compte_entreprise_moyens_techniques", type="string", nullable=false)
     */
    private string $compteEntrepriseMoyensTechniques = '0';

    /**
     * @ORM\Column(name="compte_entreprise_prestations_realisees", type="string", nullable=false)
     */
    private string $compteEntreprisePrestationsRealisees = '0';

    /**
     * @ORM\Column(name="compte_entreprise_chiffre_affaire_production_biens_services", type="string", nullable=false)
     */
    private string $compteEntrepriseChiffreAffaireProductionBiensServices = '1';

    /**
     * @ORM\Column(name="enveloppe_offre_technique", type="string", nullable=false)
     */
    private string $enveloppeOffreTechnique = '0';

    /**
     * @ORM\Column(name="compte_inscrit_choix_profil", type="string", nullable=false)
     */
    private string $compteInscritChoixProfil = '0';

    /**
     * @ORM\Column(name="procedure_adaptee", type="string", nullable=false)
     */
    private string $procedureAdaptee = '1';

    /**
     * @ORM\Column(name="compte_entreprise_siren", type="string", nullable=false)
     */
    private string $compteEntrepriseSiren = '1';

    /**
     * @ORM\Column(name="compte_entreprise_activation_inscription_par_agent", type="string", nullable=false)
     */
    private string $compteEntrepriseActivationInscriptionParAgent = '0';

    /**
     * @ORM\Column(name="menu_entreprise_consultations_en_cours", type="string", nullable=false)
     */
    private string $menuEntrepriseConsultationsEnCours = '0';

    /**
     * @ORM\Column(name="compte_entreprise_capital_social", type="string", nullable=false)
     */
    private string $compteEntrepriseCapitalSocial = '0';

    /**
     * @ORM\Column(name="mail_activation_compte_inscrit_entreprise", type="string", nullable=false)
     */
    private string $mailActivationCompteInscritEntreprise = '0';

    /**
     * @ORM\Column(name="decision_date_notification", type="string", nullable=false)
     */
    private string $decisionDateNotification = '1';

    /**
     * @ORM\Column(name="decision_pmi_pme", type="string", nullable=false)
     */
    private string $decisionPmiPme = '1';

    /**
     * @ORM\Column(name="decision_nature_prestations", type="string", nullable=false)
     */
    private string $decisionNaturePrestations = '1';

    /**
     * @ORM\Column(name="decision_objet_marche", type="string", nullable=false)
     */
    private string $decisionObjetMarche = '1';

    /**
     * @ORM\Column(name="decision_note", type="string", nullable=false)
     */
    private string $decisionNote = '0';

    /**
     * @ORM\Column(name="decision_fiche_recensement", type="string", nullable=false)
     */
    private string $decisionFicheRecensement = '1';

    /**
     * @ORM\Column(name="registre_papier_mail_obligatoire", type="string", nullable=false)
     */
    private string $registrePapierMailObligatoire = '0';

    /**
     * @ORM\Column(name="menu_entreprise_indicateurs_cles", type="string", nullable=false)
     */
    private string $menuEntrepriseIndicateursCles = '0';

    /**
     * @ORM\Column(name="ajout_rpa_champ_email", type="string", nullable=false)
     */
    private string $ajoutRpaChampEmail = '0';

    /**
     * @ORM\Column(name="ajout_rpa_champ_telephone", type="string", nullable=false)
     */
    private string $ajoutRpaChampTelephone = '0';

    /**
     * @ORM\Column(name="ajout_rpa_champ_fax", type="string", nullable=false)
     */
    private string $ajoutRpaChampFax = '0';

    /**
     * @ORM\Column(name="entreprise_poser_question_sans_pj", type="string", nullable=false)
     */
    private string $entreprisePoserQuestionSansPj = '1';

    /**
     * @ORM\Column(name="url_demarche_agent", type="string", nullable=false)
     */
    private string $urlDemarcheAgent = '0';

    /**
     * @ORM\Column(name="url_demarche_entreprise", type="string", nullable=false)
     */
    private string $urlDemarcheEntreprise = '0';

    /**
     * @ORM\Column(name="siret_detail_entite_achat", type="string", nullable=false)
     */
    private string $siretDetailEntiteAchat = '1';

    /**
     * @ORM\Column(name="presence_elu", type="string", nullable=false)
     */
    private string $presenceElu = '1';

    /**
     * @ORM\Column(name="gerer_mon_service", type="string", nullable=false)
     */
    private string $gererMonService = '0';

    /**
     * @ORM\Column(name="depouillement_enveloppe_depend_RAT_enveloppe_precedente", type="string", nullable=false)
     */
    private string $depouillementEnveloppeDependRatEnveloppePrecedente = '0';

    /**
     * @ORM\Column(name="consultation_adresse_retrais_dossiers", type="string", nullable=false)
     */
    private string $consultationAdresseRetraisDossiers = '0';

    /**
     * @ORM\Column(name="consultation_adresse_depot_offres", type="string", nullable=false)
     */
    private string $consultationAdresseDepotOffres = '0';

    /**
     * @ORM\Column(name="consultation_caution_provisoire", type="string", nullable=false)
     */
    private string $consultationCautionProvisoire = '0';

    /**
     * @ORM\Column(name="consultation_lieu_ouverture_plis", type="string", nullable=false)
     */
    private string $consultationLieuOuverturePlis = '0';

    /**
     * @ORM\Column(name="consultation_qualification", type="string", nullable=false)
     */
    private string $consultationQualification = '0';

    /**
     * @ORM\Column(name="consultation_agrement", type="string", nullable=false)
     */
    private string $consultationAgrement = '0';

    /**
     * @ORM\Column(name="consultation_echantillons_demandes", type="string", nullable=false)
     */
    private string $consultationEchantillonsDemandes = '0';

    /**
     * @ORM\Column(name="consultation_reunion", type="string", nullable=false)
     */
    private string $consultationReunion = '0';

    /**
     * @ORM\Column(name="consultation_visite_des_lieux", type="string", nullable=false)
     */
    private string $consultationVisiteDesLieux = '0';

    /**
     * @ORM\Column(name="consultation_prix_acquisition", type="string", nullable=false)
     */
    private string $consultationPrixAcquisition = '0';

    /**
     * @ORM\Column(name="resultat_analyse_avant_decision", type="string", nullable=false)
     */
    private string $resultatAnalyseAvantDecision = '0';

    /**
     * @ORM\Column(name="creation_inscrit_par_ates", type="string", nullable=false)
     */
    private string $creationInscritParAtes = '0';

    /**
     * @ORM\Column(name="consultation_variantes_autorisees", type="string", nullable=false)
     */
    private string $consultationVariantesAutorisees = '0';

    /**
     * @ORM\Column(name="recherche_avancee_par_type_org", type="string", nullable=false)
     */
    private string $rechercheAvanceeParTypeOrg = '0';

    /**
     * @ORM\Column(name="menu_agent_societes_exclues", type="string", nullable=false)
     */
    private string $menuAgentSocietesExclues = '0';

    /**
     * @ORM\Column(name="recherche_avancee_par_domaines_activite", type="string", nullable=false)
     */
    private string $rechercheAvanceeParDomainesActivite = '0';

    /**
     * @ORM\Column(name="recherche_avancee_par_qualification", type="string", nullable=false)
     */
    private string $rechercheAvanceeParQualification = '0';

    /**
     * @ORM\Column(name="recherche_avancee_par_agrement", type="string", nullable=false)
     */
    private string $rechercheAvanceeParAgrement = '0';

    /**
     * @ORM\Column(name="contact_administratif_dans_detail_consultation_cote_entreprise", type="string", nullable=false)
     */
    private string $contactAdministratifDansDetailConsultationCoteEntreprise = '0';

    /**
     * @ORM\Column(name="consultation_pieces_dossiers", type="string", nullable=false)
     */
    private string $consultationPiecesDossiers = '0';

    /**
     * @ORM\Column(name="gerer_adresses_service", type="string", nullable=false)
     */
    private string $gererAdressesService = '0';

    /**
     * @ORM\Column(name="traduire_annonces", type="string", nullable=false)
     */
    private string $traduireAnnonces = '0';

    /**
     * @ORM\Column(name="afficher_bloc_actions_dans_details_annonces", type="string", nullable=false)
     */
    private string $afficherBlocActionsDansDetailsAnnonces = '0';

    /**
     * @ORM\Column(name="autoriser_une_seule_reponse_principale_par_entreprise", type="string", nullable=false)
     */
    private string $autoriserUneSeuleReponsePrincipaleParEntreprise = '0';

    /**
     * @ORM\Column(name="generation_avis", type="string", nullable=false)
     */
    private string $generationAvis = '0';

    /**
     * @ORM\Column(name="passation_appliquer_donnees_ensemble_lots", type="string", nullable=false)
     */
    private string $passationAppliquerDonneesEnsembleLots = '0';

    /**
     * @ORM\Column(name="autre_annonce_extrait_pv", type="string", nullable=false)
     */
    private string $autreAnnonceExtraitPv = '0';

    /**
     * @ORM\Column(name="autre_annonce_rapport_achevement", type="string", nullable=false)
     */
    private string $autreAnnonceRapportAchevement = '0';

    /**
     * @ORM\Column(name="ajout_fichier_joint_autre_annonce", type="string", nullable=false)
     */
    private string $ajoutFichierJointAutreAnnonce = '0';

    /**
     * @ORM\Column(name="consultation_mode_passation", type="string", nullable=false)
     */
    private string $consultationModePassation = '0';

    /**
     * @ORM\Column(name="compte_entreprise_identifiant_unique", type="string", nullable=false)
     */
    private string $compteEntrepriseIdentifiantUnique = '0';

    /**
     * @ORM\Column(name="gerer_certificats_agent", type="string", nullable=false)
     */
    private string $gererCertificatsAgent = '0';

    /**
     * @ORM\Column(name="autre_annonce_programme_previsionnel", type="string", nullable=false)
     */
    private string $autreAnnonceProgrammePrevisionnel = '0';

    /**
     * @ORM\Column(name="annuler_consultation", type="string", nullable=false)
     */
    private string $annulerConsultation = '0';

    /**
     * @ORM\Column(name="cfe_entreprise_accessible_par_agent", type="string", nullable=false)
     */
    private string $cfeEntrepriseAccessibleParAgent = '1';

    /**
     * @ORM\Column(name="compte_entreprise_code_nace_referentiel", type="string", nullable=false)
     */
    private string $compteEntrepriseCodeNaceReferentiel = '0';

    /**
     * @ORM\Column(name="code_nut_lt_referentiel", type="string", nullable=false)
     */
    private string $codeNutLtReferentiel = '0';

    /**
     * @ORM\Column(name="lieux_execution", type="string", nullable=false)
     */
    private string $lieuxExecution = '1';

    /**
     * @ORM\Column(name="compte_entreprise_domaine_activite_lt_referentiel", type="string", nullable=false)
     */
    private string $compteEntrepriseDomaineActiviteLtReferentiel = '0';

    /**
     * @ORM\Column(name="consultation_domaines_activites_lt_referentiel", type="string", nullable=false)
     */
    private string $consultationDomainesActivitesLtReferentiel = '0';

    /**
     * @ORM\Column(name="compte_entreprise_agrement_lt_referentiel", type="string", nullable=false)
     */
    private string $compteEntrepriseAgrementLtReferentiel = '0';

    /**
     * @ORM\Column(name="compte_entreprise_qualification_lt_referentiel", type="string", nullable=false)
     */
    private string $compteEntrepriseQualificationLtReferentiel = '0';

    /**
     * @ORM\Column(name="reponse_pas_a_pas", type="string", nullable=false)
     */
    private string $reponsePasAPas = '0';

    /**
     * @ORM\Column(name="agent_controle_format_mot_de_passe", type="string", nullable=false)
     */
    private string $agentControleFormatMotDePasse = '0';

    /**
     * @ORM\Column(name="entreprise_validation_email_inscription", type="string", nullable=false)
     */
    private string $entrepriseValidationEmailInscription = '0';

    /**
     * @ORM\Column(name="telecharger_dce_avec_authentification", type="string", nullable=false)
     */
    private string $telechargerDceAvecAuthentification = '0';

    /**
     * @ORM\Column(name="authentification_basic", type="string", nullable=false)
     */
    private string $authentificationBasic = '0';

    /**
     * @ORM\Column(name="reglement_consultation", type="string", nullable=false)
     */
    private string $reglementConsultation = '1';

    /**
     * @ORM\Column(name="annonces_marches", type="string", nullable=false)
     */
    private string $annoncesMarches = '0';

    /**
     * @ORM\Column(name="cfe_date_fin_validite_obligatoire", type="string", nullable=false)
     */
    private string $cfeDateFinValiditeObligatoire = '1';

    /**
     * @ORM\Column(name="associer_documents_cfe_consultation", type="string", nullable=false)
     */
    private string $associerDocumentsCfeConsultation = '0';

    /**
     * @ORM\Column(name="compte_entreprise_region", type="string", nullable=false)
     */
    private string $compteEntrepriseRegion = '0';

    /**
     * @ORM\Column(name="compte_entreprise_telephone2", type="string", nullable=false)
     */
    private string $compteEntrepriseTelephone2 = '0';

    /**
     * @ORM\Column(name="compte_entreprise_cnss", type="string", nullable=false)
     */
    private string $compteEntrepriseCnss = '0';

    /**
     * @ORM\Column(name="compte_entreprise_rcnum", type="string", nullable=false)
     */
    private string $compteEntrepriseRcnum = '0';

    /**
     * @ORM\Column(name="compte_entreprise_domaine_activite", type="string", nullable=false)
     */
    private string $compteEntrepriseDomaineActivite = '0';

    /**
     * @ORM\Column(name="compte_inscrit_code_nic", type="string", nullable=false)
     */
    private string $compteInscritCodeNic = '1';

    /**
     * @ORM\Column(name="compte_entreprise_code_ape", type="string", nullable=false)
     */
    private string $compteEntrepriseCodeApe = '1';

    /**
     * @ORM\Column(name="compte_entreprise_documents_commerciaux", type="string", nullable=false)
     */
    private string $compteEntrepriseDocumentsCommerciaux = '0';

    /**
     * @ORM\Column(name="compte_entreprise_agrement", type="string", nullable=false)
     */
    private string $compteEntrepriseAgrement = '0';

    /**
     * @ORM\Column(name="compte_entreprise_moyens_humains", type="string", nullable=false)
     */
    private string $compteEntrepriseMoyensHumains = '0';

    /**
     * @ORM\Column(name="compte_entreprise_activite_domaine_defense", type="string", nullable=false)
     */
    private string $compteEntrepriseActiviteDomaineDefense = '1';

    /**
     * @ORM\Column(name="compte_entreprise_donnees_financieres", type="string", nullable=false)
     */
    private string $compteEntrepriseDonneesFinancieres = '0';

    /**
     * @ORM\Column(name="enveloppe_anonymat", type="string", nullable=false)
     */
    private string $enveloppeAnonymat = '1';

    /**
     * @ORM\Column(name="publicite_format_xml", type="string", nullable=false)
     */
    private string $publiciteFormatXml = '1';

    /**
     * @ORM\Column(name="article_133_generation_pf", type="string", nullable=false)
     */
    private string $article133GenerationPf = '1';

    /**
     * @ORM\Column(name="entreprise_repondre_consultation_apres_cloture", type="string", nullable=false)
     */
    private string $entrepriseRepondreConsultationApresCloture = '1';

    /**
     * @ORM\Column(name="telechargement_outil_verif_horodatage", type="string", nullable=false)
     */
    private string $telechargementOutilVerifHorodatage = '0';

    /**
     * @ORM\Column(name="affichage_code_cpv", type="string", nullable=false)
     */
    private string $affichageCodeCpv = '1';

    /**
     * @ORM\Column(name="consultation_domaines_activites", type="string", nullable=false)
     */
    private string $consultationDomainesActivites = '0';

    /**
     * @ORM\Column(name="statistiques_mesure_demat", type="string", nullable=false)
     */
    private string $statistiquesMesureDemat = '1';

    /**
     * @ORM\Column(name="publication_procure", type="string", nullable=false)
     */
    private string $publicationProcure = '0';

    /**
     * @ORM\Column(name="menu_entreprise_toutes_les_consultations", type="string", nullable=false)
     */
    private string $menuEntrepriseToutesLesConsultations = '1';

    /**
     * @ORM\Column(name="compte_entreprise_cp_obligatoire", type="string", nullable=false)
     */
    private string $compteEntrepriseCpObligatoire = '1';

    /**
     * @ORM\Column(name="annuler_depot", type="string", nullable=false)
     */
    private string $annulerDepot = '0';

    /**
     * @ORM\Column(name="traduire_entite_achat_arabe", type="string", nullable=false)
     */
    private string $traduireEntiteAchatArabe = '0';

    /**
     * @ORM\Column(name="traduire_organisme_arabe", type="string", nullable=true)
     */
    private ?string $traduireOrganismeArabe = '0';

    /**
     * @ORM\Column(name="decision_cp", type="string", nullable=false)
     */
    private string $decisionCp = '1';

    /**
     * @ORM\Column(name="decision_tranche_budgetaire", type="string", nullable=false)
     */
    private string $decisionTrancheBudgetaire = '1';

    /**
     * @ORM\Column(name="decision_classement", type="string", nullable=false)
     */
    private string $decisionClassement = '0';

    /**
     * @ORM\Column(name="decision_afficher_detail_candidat_par_defaut", type="string", nullable=false)
     */
    private string $decisionAfficherDetailCandidatParDefaut = '0';

    /**
     * @ORM\Column(name="article_133_upload_fichier", type="string", nullable=false)
     */
    private string $article133UploadFichier = '0';

    /**
     * @ORM\Column(name="multi_linguisme_agent", type="string", nullable=false)
     */
    private string $multiLinguismeAgent = '0';

    /**
     * @ORM\Column(name="compte_entreprise_ifu", type="string", nullable=false)
     */
    private string $compteEntrepriseIfu = '0';

    /**
     * @ORM\Column(name="gestion_organisme_par_agent", type="string", nullable=false)
     */
    private string $gestionOrganismeParAgent = '0';

    /**
     * @ORM\Column(name="utiliser_lucene", type="string", nullable=false)
     */
    private string $utiliserLucene = '1';

    /**
     * @ORM\Column(name="utiliser_page_html_lieux_execution", type="string", nullable=false)
     */
    private string $utiliserPageHtmlLieuxExecution = '1';

    /**
     * @ORM\Column(name="prado_validateur_format_date", type="string", nullable=false)
     */
    private string $pradoValidateurFormatDate = '1';

    /**
     * @ORM\Column(name="prado_validateur_format_email", type="string", nullable=false)
     */
    private string $pradoValidateurFormatEmail = '1';

    /**
     * @ORM\Column(name="socle_externe_ppp", type="string", nullable=false)
     */
    private string $socleExternePpp = '0';

    /**
     * @ORM\Column(name="validation_format_champs_stricte", type="string", nullable=false)
     */
    private string $validationFormatChampsStricte = '0';

    /**
     * @ORM\Column(name="poser_question_necessite_authentification", type="string", nullable=false)
     */
    private string $poserQuestionNecessiteAuthentification = '0';

    /**
     * @ORM\Column(name="autoriser_modif_profil_inscrit_ates", type="string", nullable=true)
     */
    private ?string $autoriserModifProfilInscritAtes = '1';

    /**
     * @ORM\Column(name="unicite_reference_consultation", type="string", nullable=false)
     */
    private string $uniciteReferenceConsultation = '0';

    /**
     * @ORM\Column(name="registre_papier_rcnum_rcville_obligatoires", type="string", nullable=false)
     */
    private string $registrePapierRcnumRcvilleObligatoires = '0';

    /**
     * @ORM\Column(name="registre_papier_adresse_cp_ville_obligatoires", type="string", nullable=false)
     */
    private string $registrePapierAdresseCpVilleObligatoires = '0';

    /**
     * @ORM\Column(name="telecharger_dce_sans_identification", type="string", nullable=false)
     */
    private string $telechargerDceSansIdentification = '0';

    /**
     * @ORM\Column(name="gestion_entreprise_par_agent", type="string", nullable=false)
     */
    private string $gestionEntrepriseParAgent = '0';

    /**
     * @ORM\Column(name="autoriser_caracteres_speciaux_dans_reference", type="string", nullable=false)
     */
    private string $autoriserCaracteresSpeciauxDansReference = '0';

    /**
     * @ORM\Column(name="inscription_libre_entreprise", type="string", nullable=false)
     */
    private string $inscriptionLibreEntreprise = '1';

    /**
     * @ORM\Column(name="afficher_code_service", type="string", nullable=false)
     */
    private string $afficherCodeService = '0';

    /**
     * @ORM\Column(name="authenticate_agent_by_login", type="string", nullable=false)
     */
    private string $authenticateAgentByLogin = '1';

    /**
     * @ORM\Column(name="authenticate_agent_by_cert", type="string", nullable=false)
     */
    private string $authenticateAgentByCert = '0';

    /**
     * @ORM\Column(name="generer_acte_dengagement", type="string", nullable=false)
     */
    private string $genererActeDengagement = '0';

    /**
     * @ORM\Column(name="entreprise_controle_format_mot_de_passe", type="string", nullable=false)
     */
    private string $entrepriseControleFormatMotDePasse = '0';

    /**
     * @ORM\Column(name="autre_annonce_information", type="string", nullable=false)
     */
    private string $autreAnnonceInformation = '1';

    /**
     * @ORM\Column(name="creer_autre_annonce", type="string", nullable=false)
     */
    private string $creerAutreAnnonce = '1';

    /**
     * @ORM\Column(name="consultation_clause", type="string", nullable=false)
     */
    private string $consultationClause = '0';

    /**
     * @ORM\Column(name="panier_entreprise", type="string", nullable=false)
     */
    private string $panierEntreprise = '0';

    /**
     * @ORM\Column(name="parametrage_publicite_par_type_procedure", type="string", nullable=false)
     */
    private string $parametragePubliciteParTypeProcedure = '0';

    /**
     * @ORM\Column(name="export_decision", type="string", nullable=false)
     */
    private string $exportDecision = '0';

    /**
     * @ORM\Column(name="regle_mise_en_ligne_par_entite_coordinatrice", type="string", nullable=false)
     */
    private string $regleMiseEnLigneParEntiteCoordinatrice = '0';

    /**
     * @ORM\Column(name="gestion_newsletter", type="string", nullable=false)
     */
    private string $gestionNewsletter = '0';

    /**
     * @ORM\Column(name="publicite_opoce", type="string", nullable=false)
     */
    private string $publiciteOpoce = '0';

    /**
     * @ORM\Column(name="gestion_modeles_formulaire", type="string", nullable=false)
     */
    private string $gestionModelesFormulaire = '0';

    /**
     * @ORM\Column(name="gestion_adresses_facturation_JAL", type="string", nullable=false)
     */
    private string $gestionAdressesFacturationJal = '0';

    /**
     * @ORM\Column(name="publicite_marches_en_ligne", type="string", nullable=false)
     */
    private string $publiciteMarchesEnLigne = '1';

    /**
     * @ORM\Column(name="lieu_ouverture_plis_obligatoire", type="string", nullable=false)
     */
    private string $lieuOuverturePlisObligatoire = '0';

    /**
     * @ORM\Column(name="dossier_additif", type="string", nullable=false)
     */
    private string $dossierAdditif = '0';

    /**
     * @ORM\Column(name="type_marche", type="string", nullable=false)
     */
    private string $typeMarche = '0';

    /**
     * @ORM\Column(name="type_prestation", type="string", nullable=false)
     */
    private string $typePrestation = '0';

    /**
     * @ORM\Column(name="afficher_tjr_bloc_caracteristique_reponse", type="string", nullable=false)
     */
    private string $afficherTjrBlocCaracteristiqueReponse = '0';

    /**
     * @ORM\Column(name="alerte_metier", type="string", nullable=false)
     */
    private string $alerteMetier = '0';

    /**
     * @ORM\Column(name="bourse_a_la_sous_traitance", type="string", nullable=false)
     */
    private string $bourseALaSousTraitance = '0';

    /**
     * @ORM\Column(name="partager_consultation", type="string", nullable=false)
     */
    private string $partagerConsultation = '0';

    /**
     * @ORM\Column(name="annuaire_acheteurs_publics", type="string", nullable=false)
     */
    private string $annuaireAcheteursPublics = '0';

    /**
     * @ORM\Column(name="entreprise_actions_groupees", type="string", nullable=false)
     */
    private string $entrepriseActionsGroupees = '0';

    /**
     * @ORM\Column(name="publier_guides", type="string", nullable=false)
     */
    private string $publierGuides = '0';

    /**
     * @ORM\Column(name="recherche_auto_completion", type="string", nullable=false)
     */
    private string $rechercheAutoCompletion = '0';

    /**
     * @ORM\Column(name="statut_compte_entreprise", type="string", nullable=false)
     */
    private string $statutCompteEntreprise = '0';

    /**
     * @ORM\Column(name="gestion_organismes", type="string", nullable=false)
     */
    private string $gestionOrganismes = '0';

    /**
     * @ORM\Column(name="accueil_entreprise_personnalise", type="string", nullable=false)
     */
    private string $accueilEntreprisePersonnalise = '0';

    /**
     * @ORM\Column(name="interface_module_sub", type="string", nullable=false)
     */
    private string $interfaceModuleSub = '0';

    /**
     * @ORM\Column(name="authentification_agent_multi_organismes", type="string", nullable=false)
     */
    private string $authentificationAgentMultiOrganismes = '1';

    /**
     * @ORM\Column(name="lieux_execution_carte", type="string", nullable=false)
     */
    private string $lieuxExecutionCarte = '1';

    /**
     * @ORM\Column(name="surcharge_referentiels", type="string", nullable=false)
     */
    private string $surchargeReferentiels = '0';

    /**
     * @ORM\Column(name="Mode_Restriction_RGS", type="string", nullable=false)
     */
    private string $modeRestrictionRgs = '0';

    /**
     * @ORM\Column(name="autre_annonce_decision_resiliation", type="string", nullable=false)
     */
    private string $autreAnnonceDecisionResiliation = '0';

    /**
     * @ORM\Column(name="autre_annonce_synthese_rapport_audit", type="string", nullable=false)
     */
    private string $autreAnnonceSyntheseRapportAudit = '0';

    /**
     * @ORM\Column(name="fiche_weka", type="string", nullable=false)
     */
    private string $ficheWeka = '0';

    /**
     * @ORM\Column(name="generation_automatique_mdp_agent", type="string", nullable=false)
     */
    private string $generationAutomatiqueMdpAgent = '0';

    /**
     * @ORM\Column(name="generation_automatique_mdp_inscrit", type="string", nullable=false)
     */
    private string $generationAutomatiqueMdpInscrit = '0';

    /**
     * @ORM\Column(name="liste_ac_rgs", type="string", nullable=false)
     */
    private string $listeAcRgs = '0';

    /**
     * @ORM\Column(name="liste_cons_org", type="string", nullable=false)
     */
    private string $listeConsOrg = '0';

    /**
     * @ORM\Column(name="marche_public_simplifie_entreprise", type="string", nullable=false)
     */
    private string $marchePublicSimplifieEntreprise = '0';

    /**
     * @ORM\Column(name="archive_par_lot", type="string", nullable=false)
     */
    private string $archiveParLot = '0';

    /**
     * @ORM\Column(name="documents_reference", type="string", nullable=false)
     */
    private string $documentsReference = '0';

    /**
     * @ORM\Column(name="recherches_favorites", type="string", nullable=false)
     */
    private string $recherchesFavorites = '0';

    /**
     * @ORM\Column(name="synchronisation_SGMAP", type="string", nullable=false)
     */
    private string $synchronisationSgmap = '0';

    /**
     * @ORM\Column(name="donnees_candidat", type="string", nullable=false)
     */
    private ?string $donneesCandidat = null;

    /**
     * @ORM\Column(name="autoriser_creation_entreprise_etrangere", type="string", nullable=false)
     */
    private string $autoriserCreationEntrepriseEtrangere = '1';

    /**
     * @ORM\Column(name="bourse_cotraitance", type="string", nullable=false)
     */
    private string $bourseCotraitance = '0';

    /**
     * @ORM\Column(name="ac_sad_transversaux", type="string", nullable=false)
     */
    private string $acSadTransversaux = '0';

    /**
     * @ORM\Column(name="web_service_par_silo", type="string", nullable=false)
     */
    private string $webServiceParSilo = '0';

    /**
     * @ORM\Column(name="groupement", type="string", nullable=false)
     */
    private string $groupement = '0';

    /**
     * @ORM\Column(name="notifications_agent", type="string", nullable=false)
     */
    private string $notificationsAgent = '1';

    /**
     * @ORM\Column(name="publicite", type="string", nullable=false)
     */
    private string $publicite = '0';

    /**
     * @ORM\Column(name="interface_dume", type="string", nullable=false)
     */
    private string $interfaceDume = '0';

    /**
     * @ORM\Column(name="plateforme_editeur", type="string", nullable=false)
     */
    private string $plateformeEditeur = '1';

    /**
     * @ORM\Column(name="donnees_essentielles_suivi_sn", type="string", nullable=false)
     */
    private string $donneesEssentiellesSuiviSn = '0';

    /**
     * @ORM\Column(name="token_api_sgmap_api", type="string", nullable=true)
     */
    private ?string $tokenApiSgmapApi = null;

    /**
     * @ORM\Column(name="entreprise_duree_vie_mot_de_passe", type="integer", nullable=false)
     */
    private int $entrepriseDureeVieMotDePasse = 0;

    /**
     * @ORM\Column(name="entreprise_mots_de_passe_historises", type="integer", nullable=false)
     */
    private int $entrepriseMotsDePasseHistorises = 0;

    /**
     * @var bool
     * @ORM\Column(name="messagerie_v2", type="boolean", nullable=false)
     */
    private $messagerieV2 = false;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"default":"1000"})
     */
    private $controleTailleDepot = 1000;

    /**
     * @var bool
     * @ORM\Column(name="menu_agent_complet", type="boolean", nullable=false)
     */
    private $menuAgentComplet = true;

    /**
     * @ORM\Column(name="case_attestation_consultation", type="string", nullable=false)
     */
    private string $caseAttestationConsultation = '0';

    /**
     * @ORM\Column(name="masquer_elements_mps", type="string", nullable=false)
     */
    private string $masquerElementsMps = '1';

    /**
     * @ORM\Column(name="masquer_atexo_sign", type="string", nullable=false)
     */
    private string $masquerAtexoSign = '0';

    /**
     * @ORM\Column(name="afficher_valeur_estimee", type="string", nullable=false)*/
    #[Assert\Length(min: 1, max: 1)]
    private string $afficherValeurEstimee = '0';

    /**
     * @ORM\Column(name="unicite_mail_agent", type="integer", nullable=false)
     */
    private int $uniciteMailAgent = 1;

    /**
     * @ORM\Column(name="authenticate_agent_openid_microsoft", type="string", nullable=false)
     */
    private string $authenticateAgentOpenidMicrosoft = '0';

    /**
     * @ORM\Column(name="authenticate_inscrit_openid_microsoft", type="string", nullable=false)
     */
    private string $authenticateInscritOpenidMicrosoft = '0';

    /**
     * @ORM\Column(name="authenticate_agent_openid_keycloak", type="string", nullable=false)
     */
    private string $authenticateAgentOpenidKeycloak = '0';

    /**
     * @ORM\Column(name="authenticate_inscrit_openid_keycloak", type="string", nullable=false)
     */
    private string $authenticateInscritOpenidKeycloak = '0';

    /**
     * @ORM\Column(name="authenticate_agent_saml", type="string", nullable=false)
     */
    private string $authenticateAgentSaml = '0';

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $donnerAvisDepotEntreprise = false;

    /**
     * @ORM\Column(name="recueil_consentement_rgpd", type="boolean", nullable=false)
     */
    private bool $recueilConsentementRgpd = false;

    /**
     * @ORM\Column(name="saisie_part_france_ue_depot", type="boolean", nullable=false)
     */
    private bool $saisiePartFranceUeDepot = false;

    /**
     * @var bool
     * @ORM\Column(name="modules_autoformation", type="boolean", nullable=false)
     */
    private $modulesAutoformation = false;

    /**
     * @var bool
     * @ORM\Column(name="afficher_rattachement_service", type="boolean", nullable=false)
     */
    private $afficherRattachementService = false;

    /**
     * @ORM\Column(name="publication_format_libre", type="boolean", nullable=false)
     */
    private bool $publicationFormatLibre = true;

    /**
     * @ORM\Column(name="conf_publicite_francaise", type="boolean", nullable=false)
     */
    private bool $confPubliciteFrancaise = true;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $authenticateAgentByInternalKeycloak = true;

    /**
     * Get idAuto.
     *
     * @return int
     */
    public function getIdAuto()
    {
        return $this->idAuto;
    }

    /**
     * Set codeCpv.
     *
     * @param string $codeCpv
     *
     * @return ConfigurationPlateforme
     */
    public function setCodeCpv($codeCpv)
    {
        $this->codeCpv = $codeCpv;

        return $this;
    }

    /**
     * Get codeCpv.
     *
     * @return string
     */
    public function getCodeCpv()
    {
        return $this->codeCpv;
    }

    /**
     * Set multiLinguismeEntreprise.
     *
     * @param string $multiLinguismeEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setMultiLinguismeEntreprise($multiLinguismeEntreprise)
    {
        $this->multiLinguismeEntreprise = $multiLinguismeEntreprise;

        return $this;
    }

    /**
     * Get multiLinguismeEntreprise.
     *
     * @return string
     */
    public function getMultiLinguismeEntreprise()
    {
        return $this->multiLinguismeEntreprise;
    }

    /**
     * Set gestionFournisseursDocsMesSousServices.
     *
     * @param string $gestionFournisseursDocsMesSousServices
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionFournisseursDocsMesSousServices($gestionFournisseursDocsMesSousServices)
    {
        $this->gestionFournisseursDocsMesSousServices = $gestionFournisseursDocsMesSousServices;

        return $this;
    }

    /**
     * Get gestionFournisseursDocsMesSousServices.
     *
     * @return string
     */
    public function getGestionFournisseursDocsMesSousServices()
    {
        return $this->gestionFournisseursDocsMesSousServices;
    }

    /**
     * Set authenticateInscritByCert.
     *
     * @param string $authenticateInscritByCert
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthenticateInscritByCert($authenticateInscritByCert)
    {
        $this->authenticateInscritByCert = $authenticateInscritByCert;

        return $this;
    }

    /**
     * Get authenticateInscritByCert.
     *
     * @return string
     */
    public function getAuthenticateInscritByCert()
    {
        return $this->authenticateInscritByCert;
    }

    /**
     * Set authenticateInscritByLogin.
     *
     * @param string $authenticateInscritByLogin
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthenticateInscritByLogin($authenticateInscritByLogin)
    {
        $this->authenticateInscritByLogin = $authenticateInscritByLogin;

        return $this;
    }

    /**
     * Get authenticateInscritByLogin.
     *
     * @return string
     */
    public function getAuthenticateInscritByLogin()
    {
        return $this->authenticateInscritByLogin;
    }

    /**
     * Set baseQualifieeEntrepriseInsee.
     *
     * @param string $baseQualifieeEntrepriseInsee
     *
     * @return ConfigurationPlateforme
     */
    public function setBaseQualifieeEntrepriseInsee($baseQualifieeEntrepriseInsee)
    {
        $this->baseQualifieeEntrepriseInsee = $baseQualifieeEntrepriseInsee;

        return $this;
    }

    /**
     * Get baseQualifieeEntrepriseInsee.
     *
     * @return string
     */
    public function getBaseQualifieeEntrepriseInsee()
    {
        return $this->baseQualifieeEntrepriseInsee;
    }

    /**
     * Set gestionBoampMesSousServices.
     *
     * @param string $gestionBoampMesSousServices
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionBoampMesSousServices($gestionBoampMesSousServices)
    {
        $this->gestionBoampMesSousServices = $gestionBoampMesSousServices;

        return $this;
    }

    /**
     * Get gestionBoampMesSousServices.
     *
     * @return string
     */
    public function getGestionBoampMesSousServices()
    {
        return $this->gestionBoampMesSousServices;
    }

    /**
     * Set gestionBiCleMesSousServices.
     *
     * @param string $gestionBiCleMesSousServices
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionBiCleMesSousServices($gestionBiCleMesSousServices)
    {
        $this->gestionBiCleMesSousServices = $gestionBiCleMesSousServices;

        return $this;
    }

    /**
     * Get gestionBiCleMesSousServices.
     *
     * @return string
     */
    public function getGestionBiCleMesSousServices()
    {
        return $this->gestionBiCleMesSousServices;
    }

    /**
     * Set nomEntrepriseToujoursVisible.
     *
     * @param string $nomEntrepriseToujoursVisible
     *
     * @return ConfigurationPlateforme
     */
    public function setNomEntrepriseToujoursVisible($nomEntrepriseToujoursVisible)
    {
        $this->nomEntrepriseToujoursVisible = $nomEntrepriseToujoursVisible;

        return $this;
    }

    /**
     * Get nomEntrepriseToujoursVisible.
     *
     * @return string
     */
    public function getNomEntrepriseToujoursVisible()
    {
        return $this->nomEntrepriseToujoursVisible;
    }

    /**
     * Set gestionJalMesSousServices.
     *
     * @param string $gestionJalMesSousServices
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionJalMesSousServices($gestionJalMesSousServices)
    {
        $this->gestionJalMesSousServices = $gestionJalMesSousServices;

        return $this;
    }

    /**
     * Get gestionJalMesSousServices.
     *
     * @return string
     */
    public function getGestionJalMesSousServices()
    {
        return $this->gestionJalMesSousServices;
    }

    /**
     * Set choixLangueAffichageConsultation.
     *
     * @param string $choixLangueAffichageConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setChoixLangueAffichageConsultation($choixLangueAffichageConsultation)
    {
        $this->choixLangueAffichageConsultation = $choixLangueAffichageConsultation;

        return $this;
    }

    /**
     * Get choixLangueAffichageConsultation.
     *
     * @return string
     */
    public function getChoixLangueAffichageConsultation()
    {
        return $this->choixLangueAffichageConsultation;
    }

    /**
     * Set compteEntrepriseDonneesComplementaires.
     *
     * @param string $compteEntrepriseDonneesComplementaires
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDonneesComplementaires($compteEntrepriseDonneesComplementaires)
    {
        $this->compteEntrepriseDonneesComplementaires = $compteEntrepriseDonneesComplementaires;

        return $this;
    }

    /**
     * Get compteEntrepriseDonneesComplementaires.
     *
     * @return string
     */
    public function getCompteEntrepriseDonneesComplementaires()
    {
        return $this->compteEntrepriseDonneesComplementaires;
    }

    /**
     * Set annuaireEntitesAchatVisibleParEntreprise.
     *
     * @param string $annuaireEntitesAchatVisibleParEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setAnnuaireEntitesAchatVisibleParEntreprise($annuaireEntitesAchatVisibleParEntreprise)
    {
        $this->annuaireEntitesAchatVisibleParEntreprise = $annuaireEntitesAchatVisibleParEntreprise;

        return $this;
    }

    /**
     * Get annuaireEntitesAchatVisibleParEntreprise.
     *
     * @return string
     */
    public function getAnnuaireEntitesAchatVisibleParEntreprise()
    {
        return $this->annuaireEntitesAchatVisibleParEntreprise;
    }

    /**
     * Set affichageRechercheAvanceeAgentAcSadTransversaux.
     *
     * @param string $affichageRechercheAvanceeAgentAcSadTransversaux
     *
     * @return ConfigurationPlateforme
     */
    public function setAffichageRechercheAvanceeAgentAcSadTransversaux($affichageRechercheAvanceeAgentAcSadTransversaux)
    {
        $this->affichageRechercheAvanceeAgentAcSadTransversaux = $affichageRechercheAvanceeAgentAcSadTransversaux;

        return $this;
    }

    /**
     * Get affichageRechercheAvanceeAgentAcSadTransversaux.
     *
     * @return string
     */
    public function getAffichageRechercheAvanceeAgentAcSadTransversaux()
    {
        return $this->affichageRechercheAvanceeAgentAcSadTransversaux;
    }

    /**
     * Set encheresEntreprise.
     *
     * @param string $encheresEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setEncheresEntreprise($encheresEntreprise)
    {
        $this->encheresEntreprise = $encheresEntreprise;

        return $this;
    }

    /**
     * Get encheresEntreprise.
     *
     * @return string
     */
    public function getEncheresEntreprise()
    {
        return $this->encheresEntreprise;
    }

    /**
     * Set socleInterne.
     *
     * @param string $socleInterne
     *
     * @return ConfigurationPlateforme
     */
    public function setSocleInterne($socleInterne)
    {
        $this->socleInterne = $socleInterne;

        return $this;
    }

    /**
     * Get socleInterne.
     *
     * @return string
     */
    public function getSocleInterne()
    {
        return $this->socleInterne;
    }

    /**
     * Set moduleCertificat.
     *
     * @param string $moduleCertificat
     *
     * @return ConfigurationPlateforme
     */
    public function setModuleCertificat($moduleCertificat)
    {
        $this->moduleCertificat = $moduleCertificat;

        return $this;
    }

    /**
     * Get moduleCertificat.
     *
     * @return string
     */
    public function getModuleCertificat()
    {
        return $this->moduleCertificat;
    }

    /**
     * Set socleExterneAgent.
     *
     * @param string $socleExterneAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setSocleExterneAgent($socleExterneAgent)
    {
        $this->socleExterneAgent = $socleExterneAgent;

        return $this;
    }

    /**
     * Get socleExterneAgent.
     *
     * @return string
     */
    public function getSocleExterneAgent()
    {
        return $this->socleExterneAgent;
    }

    /**
     * Set afficherImageOrganisme.
     *
     * @param string $afficherImageOrganisme
     *
     * @return ConfigurationPlateforme
     */
    public function setAfficherImageOrganisme($afficherImageOrganisme)
    {
        $this->afficherImageOrganisme = $afficherImageOrganisme;

        return $this;
    }

    /**
     * Get afficherImageOrganisme.
     *
     * @return string
     */
    public function getAfficherImageOrganisme()
    {
        return $this->afficherImageOrganisme;
    }

    /**
     * Set socleExterneEntreprise.
     *
     * @param string $socleExterneEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setSocleExterneEntreprise($socleExterneEntreprise)
    {
        $this->socleExterneEntreprise = $socleExterneEntreprise;

        return $this;
    }

    /**
     * Get socleExterneEntreprise.
     *
     * @return string
     */
    public function getSocleExterneEntreprise()
    {
        return $this->socleExterneEntreprise;
    }

    /**
     * Set portailDefenseEntreprise.
     *
     * @param string $portailDefenseEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setPortailDefenseEntreprise($portailDefenseEntreprise)
    {
        $this->portailDefenseEntreprise = $portailDefenseEntreprise;

        return $this;
    }

    /**
     * Get portailDefenseEntreprise.
     *
     * @return string
     */
    public function getPortailDefenseEntreprise()
    {
        return $this->portailDefenseEntreprise;
    }

    /**
     * Set compteEntrepriseProvince.
     *
     * @param string $compteEntrepriseProvince
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseProvince($compteEntrepriseProvince)
    {
        $this->compteEntrepriseProvince = $compteEntrepriseProvince;

        return $this;
    }

    /**
     * Get compteEntrepriseProvince.
     *
     * @return string
     */
    public function getCompteEntrepriseProvince()
    {
        return $this->compteEntrepriseProvince;
    }

    /**
     * Set compteEntrepriseTelephone3.
     *
     * @param string $compteEntrepriseTelephone3
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseTelephone3($compteEntrepriseTelephone3)
    {
        $this->compteEntrepriseTelephone3 = $compteEntrepriseTelephone3;

        return $this;
    }

    /**
     * Get compteEntrepriseTelephone3.
     *
     * @return string
     */
    public function getCompteEntrepriseTelephone3()
    {
        return $this->compteEntrepriseTelephone3;
    }

    /**
     * Set compteEntrepriseTaxProf.
     *
     * @param string $compteEntrepriseTaxProf
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseTaxProf($compteEntrepriseTaxProf)
    {
        $this->compteEntrepriseTaxProf = $compteEntrepriseTaxProf;

        return $this;
    }

    /**
     * Get compteEntrepriseTaxProf.
     *
     * @return string
     */
    public function getCompteEntrepriseTaxProf()
    {
        return $this->compteEntrepriseTaxProf;
    }

    /**
     * Set compteEntrepriseRcville.
     *
     * @param string $compteEntrepriseRcville
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseRcville($compteEntrepriseRcville)
    {
        $this->compteEntrepriseRcville = $compteEntrepriseRcville;

        return $this;
    }

    /**
     * Get compteEntrepriseRcville.
     *
     * @return string
     */
    public function getCompteEntrepriseRcville()
    {
        return $this->compteEntrepriseRcville;
    }

    /**
     * Set compteEntrepriseDeclarationHonneur.
     *
     * @param string $compteEntrepriseDeclarationHonneur
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDeclarationHonneur($compteEntrepriseDeclarationHonneur)
    {
        $this->compteEntrepriseDeclarationHonneur = $compteEntrepriseDeclarationHonneur;

        return $this;
    }

    /**
     * Get compteEntrepriseDeclarationHonneur.
     *
     * @return string
     */
    public function getCompteEntrepriseDeclarationHonneur()
    {
        return $this->compteEntrepriseDeclarationHonneur;
    }

    /**
     * Set compteEntrepriseQualification.
     *
     * @param string $compteEntrepriseQualification
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseQualification($compteEntrepriseQualification)
    {
        $this->compteEntrepriseQualification = $compteEntrepriseQualification;

        return $this;
    }

    /**
     * Get compteEntrepriseQualification.
     *
     * @return string
     */
    public function getCompteEntrepriseQualification()
    {
        return $this->compteEntrepriseQualification;
    }

    /**
     * Set compteEntrepriseMoyensTechniques.
     *
     * @param string $compteEntrepriseMoyensTechniques
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseMoyensTechniques($compteEntrepriseMoyensTechniques)
    {
        $this->compteEntrepriseMoyensTechniques = $compteEntrepriseMoyensTechniques;

        return $this;
    }

    /**
     * Get compteEntrepriseMoyensTechniques.
     *
     * @return string
     */
    public function getCompteEntrepriseMoyensTechniques()
    {
        return $this->compteEntrepriseMoyensTechniques;
    }

    /**
     * Set compteEntreprisePrestationsRealisees.
     *
     * @param string $compteEntreprisePrestationsRealisees
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntreprisePrestationsRealisees($compteEntreprisePrestationsRealisees)
    {
        $this->compteEntreprisePrestationsRealisees = $compteEntreprisePrestationsRealisees;

        return $this;
    }

    /**
     * Get compteEntreprisePrestationsRealisees.
     *
     * @return string
     */
    public function getCompteEntreprisePrestationsRealisees()
    {
        return $this->compteEntreprisePrestationsRealisees;
    }

    /**
     * Set compteEntrepriseChiffreAffaireProductionBiensServices.
     *
     * @param string $compteEntrepriseChiffreAffaireProductionBiensServices
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseChiffreAffaireProductionBiensServices(
        $compteEntrepriseChiffreAffaireProductionBiensServices
    ) {
        $this->compteEntrepriseChiffreAffaireProductionBiensServices =
            $compteEntrepriseChiffreAffaireProductionBiensServices;

        return $this;
    }

    /**
     * Get compteEntrepriseChiffreAffaireProductionBiensServices.
     *
     * @return string
     */
    public function getCompteEntrepriseChiffreAffaireProductionBiensServices()
    {
        return $this->compteEntrepriseChiffreAffaireProductionBiensServices;
    }

    /**
     * Set enveloppeOffreTechnique.
     *
     * @param string $enveloppeOffreTechnique
     *
     * @return ConfigurationPlateforme
     */
    public function setEnveloppeOffreTechnique($enveloppeOffreTechnique)
    {
        $this->enveloppeOffreTechnique = $enveloppeOffreTechnique;

        return $this;
    }

    /**
     * Get enveloppeOffreTechnique.
     *
     * @return string
     */
    public function getEnveloppeOffreTechnique()
    {
        return $this->enveloppeOffreTechnique;
    }

    /**
     * Set compteInscritChoixProfil.
     *
     * @param string $compteInscritChoixProfil
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteInscritChoixProfil($compteInscritChoixProfil)
    {
        $this->compteInscritChoixProfil = $compteInscritChoixProfil;

        return $this;
    }

    /**
     * Get compteInscritChoixProfil.
     *
     * @return string
     */
    public function getCompteInscritChoixProfil()
    {
        return $this->compteInscritChoixProfil;
    }

    /**
     * Set procedureAdaptee.
     *
     * @param string $procedureAdaptee
     *
     * @return ConfigurationPlateforme
     */
    public function setProcedureAdaptee($procedureAdaptee)
    {
        $this->procedureAdaptee = $procedureAdaptee;

        return $this;
    }

    /**
     * Get procedureAdaptee.
     *
     * @return string
     */
    public function getProcedureAdaptee()
    {
        return $this->procedureAdaptee;
    }

    /**
     * Set compteEntrepriseSiren.
     *
     * @param string $compteEntrepriseSiren
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseSiren($compteEntrepriseSiren)
    {
        $this->compteEntrepriseSiren = $compteEntrepriseSiren;

        return $this;
    }

    /**
     * Get compteEntrepriseSiren.
     *
     * @return string
     */
    public function getCompteEntrepriseSiren()
    {
        return $this->compteEntrepriseSiren;
    }

    /**
     * Set compteEntrepriseActivationInscriptionParAgent.
     *
     * @param string $compteEntrepriseActivationInscriptionParAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseActivationInscriptionParAgent($compteEntrepriseActivationInscriptionParAgent)
    {
        $this->compteEntrepriseActivationInscriptionParAgent = $compteEntrepriseActivationInscriptionParAgent;

        return $this;
    }

    /**
     * Get compteEntrepriseActivationInscriptionParAgent.
     *
     * @return string
     */
    public function getCompteEntrepriseActivationInscriptionParAgent()
    {
        return $this->compteEntrepriseActivationInscriptionParAgent;
    }

    /**
     * Set menuEntrepriseConsultationsEnCours.
     *
     * @param string $menuEntrepriseConsultationsEnCours
     *
     * @return ConfigurationPlateforme
     */
    public function setMenuEntrepriseConsultationsEnCours($menuEntrepriseConsultationsEnCours)
    {
        $this->menuEntrepriseConsultationsEnCours = $menuEntrepriseConsultationsEnCours;

        return $this;
    }

    /**
     * Get menuEntrepriseConsultationsEnCours.
     *
     * @return string
     */
    public function getMenuEntrepriseConsultationsEnCours()
    {
        return $this->menuEntrepriseConsultationsEnCours;
    }

    /**
     * Set compteEntrepriseCapitalSocial.
     *
     * @param string $compteEntrepriseCapitalSocial
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseCapitalSocial($compteEntrepriseCapitalSocial)
    {
        $this->compteEntrepriseCapitalSocial = $compteEntrepriseCapitalSocial;

        return $this;
    }

    /**
     * Get compteEntrepriseCapitalSocial.
     *
     * @return string
     */
    public function getCompteEntrepriseCapitalSocial()
    {
        return $this->compteEntrepriseCapitalSocial;
    }

    /**
     * Set mailActivationCompteInscritEntreprise.
     *
     * @param string $mailActivationCompteInscritEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setMailActivationCompteInscritEntreprise($mailActivationCompteInscritEntreprise)
    {
        $this->mailActivationCompteInscritEntreprise = $mailActivationCompteInscritEntreprise;

        return $this;
    }

    /**
     * Get mailActivationCompteInscritEntreprise.
     *
     * @return string
     */
    public function getMailActivationCompteInscritEntreprise()
    {
        return $this->mailActivationCompteInscritEntreprise;
    }

    /**
     * Set decisionDateNotification.
     *
     * @param string $decisionDateNotification
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionDateNotification($decisionDateNotification)
    {
        $this->decisionDateNotification = $decisionDateNotification;

        return $this;
    }

    /**
     * Get decisionDateNotification.
     *
     * @return string
     */
    public function getDecisionDateNotification()
    {
        return $this->decisionDateNotification;
    }

    /**
     * Set decisionPmiPme.
     *
     * @param string $decisionPmiPme
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionPmiPme($decisionPmiPme)
    {
        $this->decisionPmiPme = $decisionPmiPme;

        return $this;
    }

    /**
     * Get decisionPmiPme.
     *
     * @return string
     */
    public function getDecisionPmiPme()
    {
        return $this->decisionPmiPme;
    }

    /**
     * Set decisionNaturePrestations.
     *
     * @param string $decisionNaturePrestations
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionNaturePrestations($decisionNaturePrestations)
    {
        $this->decisionNaturePrestations = $decisionNaturePrestations;

        return $this;
    }

    /**
     * Get decisionNaturePrestations.
     *
     * @return string
     */
    public function getDecisionNaturePrestations()
    {
        return $this->decisionNaturePrestations;
    }

    /**
     * Set decisionObjetMarche.
     *
     * @param string $decisionObjetMarche
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionObjetMarche($decisionObjetMarche)
    {
        $this->decisionObjetMarche = $decisionObjetMarche;

        return $this;
    }

    /**
     * Get decisionObjetMarche.
     *
     * @return string
     */
    public function getDecisionObjetMarche()
    {
        return $this->decisionObjetMarche;
    }

    /**
     * Set decisionNote.
     *
     * @param string $decisionNote
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionNote($decisionNote)
    {
        $this->decisionNote = $decisionNote;

        return $this;
    }

    /**
     * Get decisionNote.
     *
     * @return string
     */
    public function getDecisionNote()
    {
        return $this->decisionNote;
    }

    /**
     * Set decisionFicheRecensement.
     *
     * @param string $decisionFicheRecensement
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionFicheRecensement($decisionFicheRecensement)
    {
        $this->decisionFicheRecensement = $decisionFicheRecensement;

        return $this;
    }

    /**
     * Get decisionFicheRecensement.
     *
     * @return string
     */
    public function getDecisionFicheRecensement()
    {
        return $this->decisionFicheRecensement;
    }

    /**
     * Set registrePapierMailObligatoire.
     *
     * @param string $registrePapierMailObligatoire
     *
     * @return ConfigurationPlateforme
     */
    public function setRegistrePapierMailObligatoire($registrePapierMailObligatoire)
    {
        $this->registrePapierMailObligatoire = $registrePapierMailObligatoire;

        return $this;
    }

    /**
     * Get registrePapierMailObligatoire.
     *
     * @return string
     */
    public function getRegistrePapierMailObligatoire()
    {
        return $this->registrePapierMailObligatoire;
    }

    /**
     * Set menuEntrepriseIndicateursCles.
     *
     * @param string $menuEntrepriseIndicateursCles
     *
     * @return ConfigurationPlateforme
     */
    public function setMenuEntrepriseIndicateursCles($menuEntrepriseIndicateursCles)
    {
        $this->menuEntrepriseIndicateursCles = $menuEntrepriseIndicateursCles;

        return $this;
    }

    /**
     * Get menuEntrepriseIndicateursCles.
     *
     * @return string
     */
    public function getMenuEntrepriseIndicateursCles()
    {
        return $this->menuEntrepriseIndicateursCles;
    }

    /**
     * Set ajoutRpaChampEmail.
     *
     * @param string $ajoutRpaChampEmail
     *
     * @return ConfigurationPlateforme
     */
    public function setAjoutRpaChampEmail($ajoutRpaChampEmail)
    {
        $this->ajoutRpaChampEmail = $ajoutRpaChampEmail;

        return $this;
    }

    /**
     * Get ajoutRpaChampEmail.
     *
     * @return string
     */
    public function getAjoutRpaChampEmail()
    {
        return $this->ajoutRpaChampEmail;
    }

    /**
     * Set ajoutRpaChampTelephone.
     *
     * @param string $ajoutRpaChampTelephone
     *
     * @return ConfigurationPlateforme
     */
    public function setAjoutRpaChampTelephone($ajoutRpaChampTelephone)
    {
        $this->ajoutRpaChampTelephone = $ajoutRpaChampTelephone;

        return $this;
    }

    /**
     * Get ajoutRpaChampTelephone.
     *
     * @return string
     */
    public function getAjoutRpaChampTelephone()
    {
        return $this->ajoutRpaChampTelephone;
    }

    /**
     * Set ajoutRpaChampFax.
     *
     * @param string $ajoutRpaChampFax
     *
     * @return ConfigurationPlateforme
     */
    public function setAjoutRpaChampFax($ajoutRpaChampFax)
    {
        $this->ajoutRpaChampFax = $ajoutRpaChampFax;

        return $this;
    }

    /**
     * Get ajoutRpaChampFax.
     *
     * @return string
     */
    public function getAjoutRpaChampFax()
    {
        return $this->ajoutRpaChampFax;
    }

    /**
     * Set entreprisePoserQuestionSansPj.
     *
     * @param string $entreprisePoserQuestionSansPj
     *
     * @return ConfigurationPlateforme
     */
    public function setEntreprisePoserQuestionSansPj($entreprisePoserQuestionSansPj)
    {
        $this->entreprisePoserQuestionSansPj = $entreprisePoserQuestionSansPj;

        return $this;
    }

    /**
     * Get entreprisePoserQuestionSansPj.
     *
     * @return string
     */
    public function getEntreprisePoserQuestionSansPj()
    {
        return $this->entreprisePoserQuestionSansPj;
    }

    /**
     * Set urlDemarcheAgent.
     *
     * @param string $urlDemarcheAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setUrlDemarcheAgent($urlDemarcheAgent)
    {
        $this->urlDemarcheAgent = $urlDemarcheAgent;

        return $this;
    }

    /**
     * Get urlDemarcheAgent.
     *
     * @return string
     */
    public function getUrlDemarcheAgent()
    {
        return $this->urlDemarcheAgent;
    }

    /**
     * Set urlDemarcheEntreprise.
     *
     * @param string $urlDemarcheEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setUrlDemarcheEntreprise($urlDemarcheEntreprise)
    {
        $this->urlDemarcheEntreprise = $urlDemarcheEntreprise;

        return $this;
    }

    /**
     * Get urlDemarcheEntreprise.
     *
     * @return string
     */
    public function getUrlDemarcheEntreprise()
    {
        return $this->urlDemarcheEntreprise;
    }

    /**
     * Set siretDetailEntiteAchat.
     *
     * @param string $siretDetailEntiteAchat
     *
     * @return ConfigurationPlateforme
     */
    public function setSiretDetailEntiteAchat($siretDetailEntiteAchat)
    {
        $this->siretDetailEntiteAchat = $siretDetailEntiteAchat;

        return $this;
    }

    /**
     * Get siretDetailEntiteAchat.
     *
     * @return string
     */
    public function getSiretDetailEntiteAchat()
    {
        return $this->siretDetailEntiteAchat;
    }

    /**
     * Set presenceElu.
     *
     * @param string $presenceElu
     *
     * @return ConfigurationPlateforme
     */
    public function setPresenceElu($presenceElu)
    {
        $this->presenceElu = $presenceElu;

        return $this;
    }

    /**
     * Get presenceElu.
     *
     * @return string
     */
    public function getPresenceElu()
    {
        return $this->presenceElu;
    }

    /**
     * Set gererMonService.
     *
     * @param string $gererMonService
     *
     * @return ConfigurationPlateforme
     */
    public function setGererMonService($gererMonService)
    {
        $this->gererMonService = $gererMonService;

        return $this;
    }

    /**
     * Get gererMonService.
     *
     * @return string
     */
    public function getGererMonService()
    {
        return $this->gererMonService;
    }

    /**
     * Set depouillementEnveloppeDependRatEnveloppePrecedente.
     *
     * @param string $depouillementEnveloppeDependRatEnveloppePrecedente
     *
     * @return ConfigurationPlateforme
     */
    public function setDepouillementEnveloppeDependRatEnveloppePrecedente(
        $depouillementEnveloppeDependRatEnveloppePrecedente
    ) {
        $this->depouillementEnveloppeDependRatEnveloppePrecedente = $depouillementEnveloppeDependRatEnveloppePrecedente;

        return $this;
    }

    /**
     * Get depouillementEnveloppeDependRatEnveloppePrecedente.
     *
     * @return string
     */
    public function getDepouillementEnveloppeDependRatEnveloppePrecedente()
    {
        return $this->depouillementEnveloppeDependRatEnveloppePrecedente;
    }

    /**
     * Set consultationAdresseRetraisDossiers.
     *
     * @param string $consultationAdresseRetraisDossiers
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationAdresseRetraisDossiers($consultationAdresseRetraisDossiers)
    {
        $this->consultationAdresseRetraisDossiers = $consultationAdresseRetraisDossiers;

        return $this;
    }

    /**
     * Get consultationAdresseRetraisDossiers.
     *
     * @return string
     */
    public function getConsultationAdresseRetraisDossiers()
    {
        return $this->consultationAdresseRetraisDossiers;
    }

    /**
     * Set consultationAdresseDepotOffres.
     *
     * @param string $consultationAdresseDepotOffres
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationAdresseDepotOffres($consultationAdresseDepotOffres)
    {
        $this->consultationAdresseDepotOffres = $consultationAdresseDepotOffres;

        return $this;
    }

    /**
     * Get consultationAdresseDepotOffres.
     *
     * @return string
     */
    public function getConsultationAdresseDepotOffres()
    {
        return $this->consultationAdresseDepotOffres;
    }

    /**
     * Set consultationCautionProvisoire.
     *
     * @param string $consultationCautionProvisoire
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationCautionProvisoire($consultationCautionProvisoire)
    {
        $this->consultationCautionProvisoire = $consultationCautionProvisoire;

        return $this;
    }

    /**
     * Get consultationCautionProvisoire.
     *
     * @return string
     */
    public function getConsultationCautionProvisoire()
    {
        return $this->consultationCautionProvisoire;
    }

    /**
     * Set consultationLieuOuverturePlis.
     *
     * @param string $consultationLieuOuverturePlis
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationLieuOuverturePlis($consultationLieuOuverturePlis)
    {
        $this->consultationLieuOuverturePlis = $consultationLieuOuverturePlis;

        return $this;
    }

    /**
     * Get consultationLieuOuverturePlis.
     *
     * @return string
     */
    public function getConsultationLieuOuverturePlis()
    {
        return $this->consultationLieuOuverturePlis;
    }

    /**
     * Set consultationQualification.
     *
     * @param string $consultationQualification
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationQualification($consultationQualification)
    {
        $this->consultationQualification = $consultationQualification;

        return $this;
    }

    /**
     * Get consultationQualification.
     *
     * @return string
     */
    public function getConsultationQualification()
    {
        return $this->consultationQualification;
    }

    /**
     * Set consultationAgrement.
     *
     * @param string $consultationAgrement
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationAgrement($consultationAgrement)
    {
        $this->consultationAgrement = $consultationAgrement;

        return $this;
    }

    /**
     * Get consultationAgrement.
     *
     * @return string
     */
    public function getConsultationAgrement()
    {
        return $this->consultationAgrement;
    }

    /**
     * Set consultationEchantillonsDemandes.
     *
     * @param string $consultationEchantillonsDemandes
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationEchantillonsDemandes($consultationEchantillonsDemandes)
    {
        $this->consultationEchantillonsDemandes = $consultationEchantillonsDemandes;

        return $this;
    }

    /**
     * Get consultationEchantillonsDemandes.
     *
     * @return string
     */
    public function getConsultationEchantillonsDemandes()
    {
        return $this->consultationEchantillonsDemandes;
    }

    /**
     * Set consultationReunion.
     *
     * @param string $consultationReunion
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationReunion($consultationReunion)
    {
        $this->consultationReunion = $consultationReunion;

        return $this;
    }

    /**
     * Get consultationReunion.
     *
     * @return string
     */
    public function getConsultationReunion()
    {
        return $this->consultationReunion;
    }

    /**
     * Set consultationVisiteDesLieux.
     *
     * @param string $consultationVisiteDesLieux
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationVisiteDesLieux($consultationVisiteDesLieux)
    {
        $this->consultationVisiteDesLieux = $consultationVisiteDesLieux;

        return $this;
    }

    /**
     * Get consultationVisiteDesLieux.
     *
     * @return string
     */
    public function getConsultationVisiteDesLieux()
    {
        return $this->consultationVisiteDesLieux;
    }

    /**
     * Set consultationPrixAcquisition.
     *
     * @param string $consultationPrixAcquisition
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationPrixAcquisition($consultationPrixAcquisition)
    {
        $this->consultationPrixAcquisition = $consultationPrixAcquisition;

        return $this;
    }

    /**
     * Get consultationPrixAcquisition.
     *
     * @return string
     */
    public function getConsultationPrixAcquisition()
    {
        return $this->consultationPrixAcquisition;
    }

    /**
     * Set resultatAnalyseAvantDecision.
     *
     * @param string $resultatAnalyseAvantDecision
     *
     * @return ConfigurationPlateforme
     */
    public function setResultatAnalyseAvantDecision($resultatAnalyseAvantDecision)
    {
        $this->resultatAnalyseAvantDecision = $resultatAnalyseAvantDecision;

        return $this;
    }

    /**
     * Get resultatAnalyseAvantDecision.
     *
     * @return string
     */
    public function getResultatAnalyseAvantDecision()
    {
        return $this->resultatAnalyseAvantDecision;
    }

    /**
     * Set creationInscritParAtes.
     *
     * @param string $creationInscritParAtes
     *
     * @return ConfigurationPlateforme
     */
    public function setCreationInscritParAtes($creationInscritParAtes)
    {
        $this->creationInscritParAtes = $creationInscritParAtes;

        return $this;
    }

    /**
     * Get creationInscritParAtes.
     *
     * @return string
     */
    public function getCreationInscritParAtes()
    {
        return $this->creationInscritParAtes;
    }

    /**
     * Set consultationVariantesAutorisees.
     *
     * @param string $consultationVariantesAutorisees
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationVariantesAutorisees($consultationVariantesAutorisees)
    {
        $this->consultationVariantesAutorisees = $consultationVariantesAutorisees;

        return $this;
    }

    /**
     * Get consultationVariantesAutorisees.
     *
     * @return string
     */
    public function getConsultationVariantesAutorisees()
    {
        return $this->consultationVariantesAutorisees;
    }

    /**
     * Set rechercheAvanceeParTypeOrg.
     *
     * @param string $rechercheAvanceeParTypeOrg
     *
     * @return ConfigurationPlateforme
     */
    public function setRechercheAvanceeParTypeOrg($rechercheAvanceeParTypeOrg)
    {
        $this->rechercheAvanceeParTypeOrg = $rechercheAvanceeParTypeOrg;

        return $this;
    }

    /**
     * Get rechercheAvanceeParTypeOrg.
     *
     * @return string
     */
    public function getRechercheAvanceeParTypeOrg()
    {
        return $this->rechercheAvanceeParTypeOrg;
    }

    /**
     * Set menuAgentSocietesExclues.
     *
     * @param string $menuAgentSocietesExclues
     *
     * @return ConfigurationPlateforme
     */
    public function setMenuAgentSocietesExclues($menuAgentSocietesExclues)
    {
        $this->menuAgentSocietesExclues = $menuAgentSocietesExclues;

        return $this;
    }

    /**
     * Get menuAgentSocietesExclues.
     *
     * @return string
     */
    public function getMenuAgentSocietesExclues()
    {
        return $this->menuAgentSocietesExclues;
    }

    /**
     * Set rechercheAvanceeParDomainesActivite.
     *
     * @param string $rechercheAvanceeParDomainesActivite
     *
     * @return ConfigurationPlateforme
     */
    public function setRechercheAvanceeParDomainesActivite($rechercheAvanceeParDomainesActivite)
    {
        $this->rechercheAvanceeParDomainesActivite = $rechercheAvanceeParDomainesActivite;

        return $this;
    }

    /**
     * Get rechercheAvanceeParDomainesActivite.
     *
     * @return string
     */
    public function getRechercheAvanceeParDomainesActivite()
    {
        return $this->rechercheAvanceeParDomainesActivite;
    }

    /**
     * Set rechercheAvanceeParQualification.
     *
     * @param string $rechercheAvanceeParQualification
     *
     * @return ConfigurationPlateforme
     */
    public function setRechercheAvanceeParQualification($rechercheAvanceeParQualification)
    {
        $this->rechercheAvanceeParQualification = $rechercheAvanceeParQualification;

        return $this;
    }

    /**
     * Get rechercheAvanceeParQualification.
     *
     * @return string
     */
    public function getRechercheAvanceeParQualification()
    {
        return $this->rechercheAvanceeParQualification;
    }

    /**
     * Set rechercheAvanceeParAgrement.
     *
     * @param string $rechercheAvanceeParAgrement
     *
     * @return ConfigurationPlateforme
     */
    public function setRechercheAvanceeParAgrement($rechercheAvanceeParAgrement)
    {
        $this->rechercheAvanceeParAgrement = $rechercheAvanceeParAgrement;

        return $this;
    }

    /**
     * Get rechercheAvanceeParAgrement.
     *
     * @return string
     */
    public function getRechercheAvanceeParAgrement()
    {
        return $this->rechercheAvanceeParAgrement;
    }

    /**
     * Set contactAdministratifDansDetailConsultationCoteEntreprise.
     *
     * @param string $contactAdministratifDansDetailConsultationCoteEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setContactAdministratifDansDetailConsultationCoteEntreprise(
        $contactAdministratifDansDetailConsultationCoteEntreprise
    ) {
        $this->contactAdministratifDansDetailConsultationCoteEntreprise
            = $contactAdministratifDansDetailConsultationCoteEntreprise;

        return $this;
    }

    /**
     * Get contactAdministratifDansDetailConsultationCoteEntreprise.
     *
     * @return string
     */
    public function getContactAdministratifDansDetailConsultationCoteEntreprise()
    {
        return $this->contactAdministratifDansDetailConsultationCoteEntreprise;
    }

    /**
     * Set consultationPiecesDossiers.
     *
     * @param string $consultationPiecesDossiers
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationPiecesDossiers($consultationPiecesDossiers)
    {
        $this->consultationPiecesDossiers = $consultationPiecesDossiers;

        return $this;
    }

    /**
     * Get consultationPiecesDossiers.
     *
     * @return string
     */
    public function getConsultationPiecesDossiers()
    {
        return $this->consultationPiecesDossiers;
    }

    /**
     * Set gererAdressesService.
     *
     * @param string $gererAdressesService
     *
     * @return ConfigurationPlateforme
     */
    public function setGererAdressesService($gererAdressesService)
    {
        $this->gererAdressesService = $gererAdressesService;

        return $this;
    }

    /**
     * Get gererAdressesService.
     *
     * @return string
     */
    public function getGererAdressesService()
    {
        return $this->gererAdressesService;
    }

    /**
     * Set traduireAnnonces.
     *
     * @param string $traduireAnnonces
     *
     * @return ConfigurationPlateforme
     */
    public function setTraduireAnnonces($traduireAnnonces)
    {
        $this->traduireAnnonces = $traduireAnnonces;

        return $this;
    }

    /**
     * Get traduireAnnonces.
     *
     * @return string
     */
    public function getTraduireAnnonces()
    {
        return $this->traduireAnnonces;
    }

    /**
     * Set afficherBlocActionsDansDetailsAnnonces.
     *
     * @param string $afficherBlocActionsDansDetailsAnnonces
     *
     * @return ConfigurationPlateforme
     */
    public function setAfficherBlocActionsDansDetailsAnnonces($afficherBlocActionsDansDetailsAnnonces)
    {
        $this->afficherBlocActionsDansDetailsAnnonces = $afficherBlocActionsDansDetailsAnnonces;

        return $this;
    }

    /**
     * Get afficherBlocActionsDansDetailsAnnonces.
     *
     * @return string
     */
    public function getAfficherBlocActionsDansDetailsAnnonces()
    {
        return $this->afficherBlocActionsDansDetailsAnnonces;
    }

    /**
     * Set autoriserUneSeuleReponsePrincipaleParEntreprise.
     *
     * @param string $autoriserUneSeuleReponsePrincipaleParEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setAutoriserUneSeuleReponsePrincipaleParEntreprise($autoriserUneSeuleReponsePrincipaleParEntreprise)
    {
        $this->autoriserUneSeuleReponsePrincipaleParEntreprise = $autoriserUneSeuleReponsePrincipaleParEntreprise;

        return $this;
    }

    /**
     * Get autoriserUneSeuleReponsePrincipaleParEntreprise.
     *
     * @return string
     */
    public function getAutoriserUneSeuleReponsePrincipaleParEntreprise()
    {
        return $this->autoriserUneSeuleReponsePrincipaleParEntreprise;
    }

    /**
     * Set generationAvis.
     *
     * @param string $generationAvis
     *
     * @return ConfigurationPlateforme
     */
    public function setGenerationAvis($generationAvis)
    {
        $this->generationAvis = $generationAvis;

        return $this;
    }

    /**
     * Get generationAvis.
     *
     * @return string
     */
    public function getGenerationAvis()
    {
        return $this->generationAvis;
    }

    /**
     * Set passationAppliquerDonneesEnsembleLots.
     *
     * @param string $passationAppliquerDonneesEnsembleLots
     *
     * @return ConfigurationPlateforme
     */
    public function setPassationAppliquerDonneesEnsembleLots($passationAppliquerDonneesEnsembleLots)
    {
        $this->passationAppliquerDonneesEnsembleLots = $passationAppliquerDonneesEnsembleLots;

        return $this;
    }

    /**
     * Get passationAppliquerDonneesEnsembleLots.
     *
     * @return string
     */
    public function getPassationAppliquerDonneesEnsembleLots()
    {
        return $this->passationAppliquerDonneesEnsembleLots;
    }

    /**
     * Set autreAnnonceExtraitPv.
     *
     * @param string $autreAnnonceExtraitPv
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceExtraitPv($autreAnnonceExtraitPv)
    {
        $this->autreAnnonceExtraitPv = $autreAnnonceExtraitPv;

        return $this;
    }

    /**
     * Get autreAnnonceExtraitPv.
     *
     * @return string
     */
    public function getAutreAnnonceExtraitPv()
    {
        return $this->autreAnnonceExtraitPv;
    }

    /**
     * Set autreAnnonceRapportAchevement.
     *
     * @param string $autreAnnonceRapportAchevement
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceRapportAchevement($autreAnnonceRapportAchevement)
    {
        $this->autreAnnonceRapportAchevement = $autreAnnonceRapportAchevement;

        return $this;
    }

    /**
     * Get autreAnnonceRapportAchevement.
     *
     * @return string
     */
    public function getAutreAnnonceRapportAchevement()
    {
        return $this->autreAnnonceRapportAchevement;
    }

    /**
     * Set ajoutFichierJointAutreAnnonce.
     *
     * @param string $ajoutFichierJointAutreAnnonce
     *
     * @return ConfigurationPlateforme
     */
    public function setAjoutFichierJointAutreAnnonce($ajoutFichierJointAutreAnnonce)
    {
        $this->ajoutFichierJointAutreAnnonce = $ajoutFichierJointAutreAnnonce;

        return $this;
    }

    /**
     * Get ajoutFichierJointAutreAnnonce.
     *
     * @return string
     */
    public function getAjoutFichierJointAutreAnnonce()
    {
        return $this->ajoutFichierJointAutreAnnonce;
    }

    /**
     * Set consultationModePassation.
     *
     * @param string $consultationModePassation
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationModePassation($consultationModePassation)
    {
        $this->consultationModePassation = $consultationModePassation;

        return $this;
    }

    /**
     * Get consultationModePassation.
     *
     * @return string
     */
    public function getConsultationModePassation()
    {
        return $this->consultationModePassation;
    }

    /**
     * Set compteEntrepriseIdentifiantUnique.
     *
     * @param string $compteEntrepriseIdentifiantUnique
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseIdentifiantUnique($compteEntrepriseIdentifiantUnique)
    {
        $this->compteEntrepriseIdentifiantUnique = $compteEntrepriseIdentifiantUnique;

        return $this;
    }

    /**
     * Get compteEntrepriseIdentifiantUnique.
     *
     * @return string
     */
    public function getCompteEntrepriseIdentifiantUnique()
    {
        return $this->compteEntrepriseIdentifiantUnique;
    }

    /**
     * Set gererCertificatsAgent.
     *
     * @param string $gererCertificatsAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setGererCertificatsAgent($gererCertificatsAgent)
    {
        $this->gererCertificatsAgent = $gererCertificatsAgent;

        return $this;
    }

    /**
     * Get gererCertificatsAgent.
     *
     * @return string
     */
    public function getGererCertificatsAgent()
    {
        return $this->gererCertificatsAgent;
    }

    /**
     * Set autreAnnonceProgrammePrevisionnel.
     *
     * @param string $autreAnnonceProgrammePrevisionnel
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceProgrammePrevisionnel($autreAnnonceProgrammePrevisionnel)
    {
        $this->autreAnnonceProgrammePrevisionnel = $autreAnnonceProgrammePrevisionnel;

        return $this;
    }

    /**
     * Get autreAnnonceProgrammePrevisionnel.
     *
     * @return string
     */
    public function getAutreAnnonceProgrammePrevisionnel()
    {
        return $this->autreAnnonceProgrammePrevisionnel;
    }

    /**
     * Set annulerConsultation.
     *
     * @param string $annulerConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setAnnulerConsultation($annulerConsultation)
    {
        $this->annulerConsultation = $annulerConsultation;

        return $this;
    }

    /**
     * Get annulerConsultation.
     *
     * @return string
     */
    public function getAnnulerConsultation()
    {
        return $this->annulerConsultation;
    }

    /**
     * Set cfeEntrepriseAccessibleParAgent.
     *
     * @param string $cfeEntrepriseAccessibleParAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setCfeEntrepriseAccessibleParAgent($cfeEntrepriseAccessibleParAgent)
    {
        $this->cfeEntrepriseAccessibleParAgent = $cfeEntrepriseAccessibleParAgent;

        return $this;
    }

    /**
     * Get cfeEntrepriseAccessibleParAgent.
     *
     * @return string
     */
    public function getCfeEntrepriseAccessibleParAgent()
    {
        return $this->cfeEntrepriseAccessibleParAgent;
    }

    /**
     * Set compteEntrepriseCodeNaceReferentiel.
     *
     * @param string $compteEntrepriseCodeNaceReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseCodeNaceReferentiel($compteEntrepriseCodeNaceReferentiel)
    {
        $this->compteEntrepriseCodeNaceReferentiel = $compteEntrepriseCodeNaceReferentiel;

        return $this;
    }

    /**
     * Get compteEntrepriseCodeNaceReferentiel.
     *
     * @return string
     */
    public function getCompteEntrepriseCodeNaceReferentiel()
    {
        return $this->compteEntrepriseCodeNaceReferentiel;
    }

    /**
     * Set codeNutLtReferentiel.
     *
     * @param string $codeNutLtReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setCodeNutLtReferentiel($codeNutLtReferentiel)
    {
        $this->codeNutLtReferentiel = $codeNutLtReferentiel;

        return $this;
    }

    /**
     * Get codeNutLtReferentiel.
     *
     * @return string
     */
    public function getCodeNutLtReferentiel()
    {
        return $this->codeNutLtReferentiel;
    }

    /**
     * Set lieuxExecution.
     *
     * @param string $lieuxExecution
     *
     * @return ConfigurationPlateforme
     */
    public function setLieuxExecution($lieuxExecution)
    {
        $this->lieuxExecution = $lieuxExecution;

        return $this;
    }

    /**
     * Get lieuxExecution.
     *
     * @return string
     */
    public function getLieuxExecution()
    {
        return $this->lieuxExecution;
    }

    /**
     * Set compteEntrepriseDomaineActiviteLtReferentiel.
     *
     * @param string $compteEntrepriseDomaineActiviteLtReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDomaineActiviteLtReferentiel($compteEntrepriseDomaineActiviteLtReferentiel)
    {
        $this->compteEntrepriseDomaineActiviteLtReferentiel = $compteEntrepriseDomaineActiviteLtReferentiel;

        return $this;
    }

    /**
     * Get compteEntrepriseDomaineActiviteLtReferentiel.
     *
     * @return string
     */
    public function getCompteEntrepriseDomaineActiviteLtReferentiel()
    {
        return $this->compteEntrepriseDomaineActiviteLtReferentiel;
    }

    /**
     * Set consultationDomainesActivitesLtReferentiel.
     *
     * @param string $consultationDomainesActivitesLtReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationDomainesActivitesLtReferentiel($consultationDomainesActivitesLtReferentiel)
    {
        $this->consultationDomainesActivitesLtReferentiel = $consultationDomainesActivitesLtReferentiel;

        return $this;
    }

    /**
     * Get consultationDomainesActivitesLtReferentiel.
     *
     * @return string
     */
    public function getConsultationDomainesActivitesLtReferentiel()
    {
        return $this->consultationDomainesActivitesLtReferentiel;
    }

    /**
     * Set compteEntrepriseAgrementLtReferentiel.
     *
     * @param string $compteEntrepriseAgrementLtReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseAgrementLtReferentiel($compteEntrepriseAgrementLtReferentiel)
    {
        $this->compteEntrepriseAgrementLtReferentiel = $compteEntrepriseAgrementLtReferentiel;

        return $this;
    }

    /**
     * Get compteEntrepriseAgrementLtReferentiel.
     *
     * @return string
     */
    public function getCompteEntrepriseAgrementLtReferentiel()
    {
        return $this->compteEntrepriseAgrementLtReferentiel;
    }

    /**
     * Set compteEntrepriseQualificationLtReferentiel.
     *
     * @param string $compteEntrepriseQualificationLtReferentiel
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseQualificationLtReferentiel($compteEntrepriseQualificationLtReferentiel)
    {
        $this->compteEntrepriseQualificationLtReferentiel = $compteEntrepriseQualificationLtReferentiel;

        return $this;
    }

    /**
     * Get compteEntrepriseQualificationLtReferentiel.
     *
     * @return string
     */
    public function getCompteEntrepriseQualificationLtReferentiel()
    {
        return $this->compteEntrepriseQualificationLtReferentiel;
    }

    /**
     * Set reponsePasAPas.
     *
     * @param string $reponsePasAPas
     *
     * @return ConfigurationPlateforme
     */
    public function setReponsePasAPas($reponsePasAPas)
    {
        $this->reponsePasAPas = $reponsePasAPas;

        return $this;
    }

    /**
     * Get reponsePasAPas.
     *
     * @return string
     */
    public function getReponsePasAPas()
    {
        return $this->reponsePasAPas;
    }

    /**
     * Set agentControleFormatMotDePasse.
     *
     * @param string $agentControleFormatMotDePasse
     *
     * @return ConfigurationPlateforme
     */
    public function setAgentControleFormatMotDePasse($agentControleFormatMotDePasse)
    {
        $this->agentControleFormatMotDePasse = $agentControleFormatMotDePasse;

        return $this;
    }

    /**
     * Get agentControleFormatMotDePasse.
     *
     * @return string
     */
    public function getAgentControleFormatMotDePasse()
    {
        return $this->agentControleFormatMotDePasse;
    }

    /**
     * Set entrepriseValidationEmailInscription.
     *
     * @param string $entrepriseValidationEmailInscription
     *
     * @return ConfigurationPlateforme
     */
    public function setEntrepriseValidationEmailInscription($entrepriseValidationEmailInscription)
    {
        $this->entrepriseValidationEmailInscription = $entrepriseValidationEmailInscription;

        return $this;
    }

    /**
     * Get entrepriseValidationEmailInscription.
     *
     * @return string
     */
    public function getEntrepriseValidationEmailInscription()
    {
        return $this->entrepriseValidationEmailInscription;
    }

    /**
     * Set telechargerDceAvecAuthentification.
     *
     * @param string $telechargerDceAvecAuthentification
     *
     * @return ConfigurationPlateforme
     */
    public function setTelechargerDceAvecAuthentification($telechargerDceAvecAuthentification)
    {
        $this->telechargerDceAvecAuthentification = $telechargerDceAvecAuthentification;

        return $this;
    }

    /**
     * Get telechargerDceAvecAuthentification.
     *
     * @return string
     */
    public function getTelechargerDceAvecAuthentification()
    {
        return $this->telechargerDceAvecAuthentification;
    }

    /**
     * Set authentificationBasic.
     *
     * @param string $authentificationBasic
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthentificationBasic($authentificationBasic)
    {
        $this->authentificationBasic = $authentificationBasic;

        return $this;
    }

    /**
     * Get authentificationBasic.
     *
     * @return string
     */
    public function getAuthentificationBasic()
    {
        return $this->authentificationBasic;
    }

    /**
     * Set reglementConsultation.
     *
     * @param string $reglementConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setReglementConsultation($reglementConsultation)
    {
        $this->reglementConsultation = $reglementConsultation;

        return $this;
    }

    /**
     * Get reglementConsultation.
     *
     * @return string
     */
    public function getReglementConsultation()
    {
        return $this->reglementConsultation;
    }

    /**
     * Set annoncesMarches.
     *
     * @param string $annoncesMarches
     *
     * @return ConfigurationPlateforme
     */
    public function setAnnoncesMarches($annoncesMarches)
    {
        $this->annoncesMarches = $annoncesMarches;

        return $this;
    }

    /**
     * Get annoncesMarches.
     *
     * @return string
     */
    public function getAnnoncesMarches()
    {
        return $this->annoncesMarches;
    }

    /**
     * Set cfeDateFinValiditeObligatoire.
     *
     * @param string $cfeDateFinValiditeObligatoire
     *
     * @return ConfigurationPlateforme
     */
    public function setCfeDateFinValiditeObligatoire($cfeDateFinValiditeObligatoire)
    {
        $this->cfeDateFinValiditeObligatoire = $cfeDateFinValiditeObligatoire;

        return $this;
    }

    /**
     * Get cfeDateFinValiditeObligatoire.
     *
     * @return string
     */
    public function getCfeDateFinValiditeObligatoire()
    {
        return $this->cfeDateFinValiditeObligatoire;
    }

    /**
     * Set associerDocumentsCfeConsultation.
     *
     * @param string $associerDocumentsCfeConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setAssocierDocumentsCfeConsultation($associerDocumentsCfeConsultation)
    {
        $this->associerDocumentsCfeConsultation = $associerDocumentsCfeConsultation;

        return $this;
    }

    /**
     * Get associerDocumentsCfeConsultation.
     *
     * @return string
     */
    public function getAssocierDocumentsCfeConsultation()
    {
        return $this->associerDocumentsCfeConsultation;
    }

    /**
     * Set compteEntrepriseRegion.
     *
     * @param string $compteEntrepriseRegion
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseRegion($compteEntrepriseRegion)
    {
        $this->compteEntrepriseRegion = $compteEntrepriseRegion;

        return $this;
    }

    /**
     * Get compteEntrepriseRegion.
     *
     * @return string
     */
    public function getCompteEntrepriseRegion()
    {
        return $this->compteEntrepriseRegion;
    }

    /**
     * Set compteEntrepriseTelephone2.
     *
     * @param string $compteEntrepriseTelephone2
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseTelephone2($compteEntrepriseTelephone2)
    {
        $this->compteEntrepriseTelephone2 = $compteEntrepriseTelephone2;

        return $this;
    }

    /**
     * Get compteEntrepriseTelephone2.
     *
     * @return string
     */
    public function getCompteEntrepriseTelephone2()
    {
        return $this->compteEntrepriseTelephone2;
    }

    /**
     * Set compteEntrepriseCnss.
     *
     * @param string $compteEntrepriseCnss
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseCnss($compteEntrepriseCnss)
    {
        $this->compteEntrepriseCnss = $compteEntrepriseCnss;

        return $this;
    }

    /**
     * Get compteEntrepriseCnss.
     *
     * @return string
     */
    public function getCompteEntrepriseCnss()
    {
        return $this->compteEntrepriseCnss;
    }

    /**
     * Set compteEntrepriseRcnum.
     *
     * @param string $compteEntrepriseRcnum
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseRcnum($compteEntrepriseRcnum)
    {
        $this->compteEntrepriseRcnum = $compteEntrepriseRcnum;

        return $this;
    }

    /**
     * Get compteEntrepriseRcnum.
     *
     * @return string
     */
    public function getCompteEntrepriseRcnum()
    {
        return $this->compteEntrepriseRcnum;
    }

    /**
     * Set compteEntrepriseDomaineActivite.
     *
     * @param string $compteEntrepriseDomaineActivite
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDomaineActivite($compteEntrepriseDomaineActivite)
    {
        $this->compteEntrepriseDomaineActivite = $compteEntrepriseDomaineActivite;

        return $this;
    }

    /**
     * Get compteEntrepriseDomaineActivite.
     *
     * @return string
     */
    public function getCompteEntrepriseDomaineActivite()
    {
        return $this->compteEntrepriseDomaineActivite;
    }

    /**
     * Set compteInscritCodeNic.
     *
     * @param string $compteInscritCodeNic
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteInscritCodeNic($compteInscritCodeNic)
    {
        $this->compteInscritCodeNic = $compteInscritCodeNic;

        return $this;
    }

    /**
     * Get compteInscritCodeNic.
     *
     * @return string
     */
    public function getCompteInscritCodeNic()
    {
        return $this->compteInscritCodeNic;
    }

    /**
     * Set compteEntrepriseCodeApe.
     *
     * @param string $compteEntrepriseCodeApe
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseCodeApe($compteEntrepriseCodeApe)
    {
        $this->compteEntrepriseCodeApe = $compteEntrepriseCodeApe;

        return $this;
    }

    /**
     * Get compteEntrepriseCodeApe.
     *
     * @return string
     */
    public function getCompteEntrepriseCodeApe()
    {
        return $this->compteEntrepriseCodeApe;
    }

    /**
     * Set compteEntrepriseDocumentsCommerciaux.
     *
     * @param string $compteEntrepriseDocumentsCommerciaux
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDocumentsCommerciaux($compteEntrepriseDocumentsCommerciaux)
    {
        $this->compteEntrepriseDocumentsCommerciaux = $compteEntrepriseDocumentsCommerciaux;

        return $this;
    }

    /**
     * Get compteEntrepriseDocumentsCommerciaux.
     *
     * @return string
     */
    public function getCompteEntrepriseDocumentsCommerciaux()
    {
        return $this->compteEntrepriseDocumentsCommerciaux;
    }

    /**
     * Set compteEntrepriseAgrement.
     *
     * @param string $compteEntrepriseAgrement
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseAgrement($compteEntrepriseAgrement)
    {
        $this->compteEntrepriseAgrement = $compteEntrepriseAgrement;

        return $this;
    }

    /**
     * Get compteEntrepriseAgrement.
     *
     * @return string
     */
    public function getCompteEntrepriseAgrement()
    {
        return $this->compteEntrepriseAgrement;
    }

    /**
     * Set compteEntrepriseMoyensHumains.
     *
     * @param string $compteEntrepriseMoyensHumains
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseMoyensHumains($compteEntrepriseMoyensHumains)
    {
        $this->compteEntrepriseMoyensHumains = $compteEntrepriseMoyensHumains;

        return $this;
    }

    /**
     * Get compteEntrepriseMoyensHumains.
     *
     * @return string
     */
    public function getCompteEntrepriseMoyensHumains()
    {
        return $this->compteEntrepriseMoyensHumains;
    }

    /**
     * Set compteEntrepriseActiviteDomaineDefense.
     *
     * @param string $compteEntrepriseActiviteDomaineDefense
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseActiviteDomaineDefense($compteEntrepriseActiviteDomaineDefense)
    {
        $this->compteEntrepriseActiviteDomaineDefense = $compteEntrepriseActiviteDomaineDefense;

        return $this;
    }

    /**
     * Get compteEntrepriseActiviteDomaineDefense.
     *
     * @return string
     */
    public function getCompteEntrepriseActiviteDomaineDefense()
    {
        return $this->compteEntrepriseActiviteDomaineDefense;
    }

    /**
     * Set compteEntrepriseDonneesFinancieres.
     *
     * @param string $compteEntrepriseDonneesFinancieres
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseDonneesFinancieres($compteEntrepriseDonneesFinancieres)
    {
        $this->compteEntrepriseDonneesFinancieres = $compteEntrepriseDonneesFinancieres;

        return $this;
    }

    /**
     * Get compteEntrepriseDonneesFinancieres.
     *
     * @return string
     */
    public function getCompteEntrepriseDonneesFinancieres()
    {
        return $this->compteEntrepriseDonneesFinancieres;
    }

    /**
     * Set enveloppeAnonymat.
     *
     * @param string $enveloppeAnonymat
     *
     * @return ConfigurationPlateforme
     */
    public function setEnveloppeAnonymat($enveloppeAnonymat)
    {
        $this->enveloppeAnonymat = $enveloppeAnonymat;

        return $this;
    }

    /**
     * Get enveloppeAnonymat.
     *
     * @return string
     */
    public function getEnveloppeAnonymat()
    {
        return $this->enveloppeAnonymat;
    }

    /**
     * Set publiciteFormatXml.
     *
     * @param string $publiciteFormatXml
     *
     * @return ConfigurationPlateforme
     */
    public function setPubliciteFormatXml($publiciteFormatXml)
    {
        $this->publiciteFormatXml = $publiciteFormatXml;

        return $this;
    }

    /**
     * Get publiciteFormatXml.
     *
     * @return string
     */
    public function getPubliciteFormatXml()
    {
        return $this->publiciteFormatXml;
    }

    /**
     * Set article133GenerationPf.
     *
     * @param string $article133GenerationPf
     *
     * @return ConfigurationPlateforme
     */
    public function setArticle133GenerationPf($article133GenerationPf)
    {
        $this->article133GenerationPf = $article133GenerationPf;

        return $this;
    }

    /**
     * Get article133GenerationPf.
     *
     * @return string
     */
    public function getArticle133GenerationPf()
    {
        return $this->article133GenerationPf;
    }

    /**
     * Set entrepriseRepondreConsultationApresCloture.
     *
     * @param string $entrepriseRepondreConsultationApresCloture
     *
     * @return ConfigurationPlateforme
     */
    public function setEntrepriseRepondreConsultationApresCloture($entrepriseRepondreConsultationApresCloture)
    {
        $this->entrepriseRepondreConsultationApresCloture = $entrepriseRepondreConsultationApresCloture;

        return $this;
    }

    /**
     * Get entrepriseRepondreConsultationApresCloture.
     *
     * @return string
     */
    public function getEntrepriseRepondreConsultationApresCloture()
    {
        return $this->entrepriseRepondreConsultationApresCloture;
    }

    /**
     * Set telechargementOutilVerifHorodatage.
     *
     * @param string $telechargementOutilVerifHorodatage
     *
     * @return ConfigurationPlateforme
     */
    public function setTelechargementOutilVerifHorodatage($telechargementOutilVerifHorodatage)
    {
        $this->telechargementOutilVerifHorodatage = $telechargementOutilVerifHorodatage;

        return $this;
    }

    /**
     * Get telechargementOutilVerifHorodatage.
     *
     * @return string
     */
    public function getTelechargementOutilVerifHorodatage()
    {
        return $this->telechargementOutilVerifHorodatage;
    }

    /**
     * Set affichageCodeCpv.
     *
     * @param string $affichageCodeCpv
     *
     * @return ConfigurationPlateforme
     */
    public function setAffichageCodeCpv($affichageCodeCpv)
    {
        $this->affichageCodeCpv = $affichageCodeCpv;

        return $this;
    }

    /**
     * Get affichageCodeCpv.
     *
     * @return string
     */
    public function getAffichageCodeCpv()
    {
        return $this->affichageCodeCpv;
    }

    /**
     * Set consultationDomainesActivites.
     *
     * @param string $consultationDomainesActivites
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationDomainesActivites($consultationDomainesActivites)
    {
        $this->consultationDomainesActivites = $consultationDomainesActivites;

        return $this;
    }

    /**
     * Get consultationDomainesActivites.
     *
     * @return string
     */
    public function getConsultationDomainesActivites()
    {
        return $this->consultationDomainesActivites;
    }

    /**
     * Set statistiquesMesureDemat.
     *
     * @param string $statistiquesMesureDemat
     *
     * @return ConfigurationPlateforme
     */
    public function setStatistiquesMesureDemat($statistiquesMesureDemat)
    {
        $this->statistiquesMesureDemat = $statistiquesMesureDemat;

        return $this;
    }

    /**
     * Get statistiquesMesureDemat.
     *
     * @return string
     */
    public function getStatistiquesMesureDemat()
    {
        return $this->statistiquesMesureDemat;
    }

    /**
     * Set publicationProcure.
     *
     * @param string $publicationProcure
     *
     * @return ConfigurationPlateforme
     */
    public function setPublicationProcure($publicationProcure)
    {
        $this->publicationProcure = $publicationProcure;

        return $this;
    }

    /**
     * Get publicationProcure.
     *
     * @return string
     */
    public function getPublicationProcure()
    {
        return $this->publicationProcure;
    }

    /**
     * Set menuEntrepriseToutesLesConsultations.
     *
     * @param string $menuEntrepriseToutesLesConsultations
     *
     * @return ConfigurationPlateforme
     */
    public function setMenuEntrepriseToutesLesConsultations($menuEntrepriseToutesLesConsultations)
    {
        $this->menuEntrepriseToutesLesConsultations = $menuEntrepriseToutesLesConsultations;

        return $this;
    }

    /**
     * Get menuEntrepriseToutesLesConsultations.
     *
     * @return string
     */
    public function getMenuEntrepriseToutesLesConsultations()
    {
        return $this->menuEntrepriseToutesLesConsultations;
    }

    /**
     * Set compteEntrepriseCpObligatoire.
     *
     * @param string $compteEntrepriseCpObligatoire
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseCpObligatoire($compteEntrepriseCpObligatoire)
    {
        $this->compteEntrepriseCpObligatoire = $compteEntrepriseCpObligatoire;

        return $this;
    }

    /**
     * Get compteEntrepriseCpObligatoire.
     *
     * @return string
     */
    public function getCompteEntrepriseCpObligatoire()
    {
        return $this->compteEntrepriseCpObligatoire;
    }

    /**
     * Set annulerDepot.
     *
     * @param string $annulerDepot
     *
     * @return ConfigurationPlateforme
     */
    public function setAnnulerDepot($annulerDepot)
    {
        $this->annulerDepot = $annulerDepot;

        return $this;
    }

    /**
     * Get annulerDepot.
     *
     * @return string
     */
    public function getAnnulerDepot()
    {
        return $this->annulerDepot;
    }

    /**
     * Set traduireEntiteAchatArabe.
     *
     * @param string $traduireEntiteAchatArabe
     *
     * @return ConfigurationPlateforme
     */
    public function setTraduireEntiteAchatArabe($traduireEntiteAchatArabe)
    {
        $this->traduireEntiteAchatArabe = $traduireEntiteAchatArabe;

        return $this;
    }

    /**
     * Get traduireEntiteAchatArabe.
     *
     * @return string
     */
    public function getTraduireEntiteAchatArabe()
    {
        return $this->traduireEntiteAchatArabe;
    }

    /**
     * Set traduireOrganismeArabe.
     *
     * @param string $traduireOrganismeArabe
     *
     * @return ConfigurationPlateforme
     */
    public function setTraduireOrganismeArabe($traduireOrganismeArabe)
    {
        $this->traduireOrganismeArabe = $traduireOrganismeArabe;

        return $this;
    }

    /**
     * Get traduireOrganismeArabe.
     *
     * @return string
     */
    public function getTraduireOrganismeArabe()
    {
        return $this->traduireOrganismeArabe;
    }

    /**
     * Set decisionCp.
     *
     * @param string $decisionCp
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionCp($decisionCp)
    {
        $this->decisionCp = $decisionCp;

        return $this;
    }

    /**
     * Get decisionCp.
     *
     * @return string
     */
    public function getDecisionCp()
    {
        return $this->decisionCp;
    }

    /**
     * Set decisionTrancheBudgetaire.
     *
     * @param string $decisionTrancheBudgetaire
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionTrancheBudgetaire($decisionTrancheBudgetaire)
    {
        $this->decisionTrancheBudgetaire = $decisionTrancheBudgetaire;

        return $this;
    }

    /**
     * Get decisionTrancheBudgetaire.
     *
     * @return string
     */
    public function getDecisionTrancheBudgetaire()
    {
        return $this->decisionTrancheBudgetaire;
    }

    /**
     * Set decisionClassement.
     *
     * @param string $decisionClassement
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionClassement($decisionClassement)
    {
        $this->decisionClassement = $decisionClassement;

        return $this;
    }

    /**
     * Get decisionClassement.
     *
     * @return string
     */
    public function getDecisionClassement()
    {
        return $this->decisionClassement;
    }

    /**
     * Set decisionAfficherDetailCandidatParDefaut.
     *
     * @param string $decisionAfficherDetailCandidatParDefaut
     *
     * @return ConfigurationPlateforme
     */
    public function setDecisionAfficherDetailCandidatParDefaut($decisionAfficherDetailCandidatParDefaut)
    {
        $this->decisionAfficherDetailCandidatParDefaut = $decisionAfficherDetailCandidatParDefaut;

        return $this;
    }

    /**
     * Get decisionAfficherDetailCandidatParDefaut.
     *
     * @return string
     */
    public function getDecisionAfficherDetailCandidatParDefaut()
    {
        return $this->decisionAfficherDetailCandidatParDefaut;
    }

    /**
     * Set article133UploadFichier.
     *
     * @param string $article133UploadFichier
     *
     * @return ConfigurationPlateforme
     */
    public function setArticle133UploadFichier($article133UploadFichier)
    {
        $this->article133UploadFichier = $article133UploadFichier;

        return $this;
    }

    /**
     * Get article133UploadFichier.
     *
     * @return string
     */
    public function getArticle133UploadFichier()
    {
        return $this->article133UploadFichier;
    }

    /**
     * Set multiLinguismeAgent.
     *
     * @param string $multiLinguismeAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setMultiLinguismeAgent($multiLinguismeAgent)
    {
        $this->multiLinguismeAgent = $multiLinguismeAgent;

        return $this;
    }

    /**
     * Get multiLinguismeAgent.
     *
     * @return string
     */
    public function getMultiLinguismeAgent()
    {
        return $this->multiLinguismeAgent;
    }

    /**
     * Set compteEntrepriseIfu.
     *
     * @param string $compteEntrepriseIfu
     *
     * @return ConfigurationPlateforme
     */
    public function setCompteEntrepriseIfu($compteEntrepriseIfu)
    {
        $this->compteEntrepriseIfu = $compteEntrepriseIfu;

        return $this;
    }

    /**
     * Get compteEntrepriseIfu.
     *
     * @return string
     */
    public function getCompteEntrepriseIfu()
    {
        return $this->compteEntrepriseIfu;
    }

    /**
     * Set gestionOrganismeParAgent.
     *
     * @param string $gestionOrganismeParAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionOrganismeParAgent($gestionOrganismeParAgent)
    {
        $this->gestionOrganismeParAgent = $gestionOrganismeParAgent;

        return $this;
    }

    /**
     * Get gestionOrganismeParAgent.
     *
     * @return string
     */
    public function getGestionOrganismeParAgent()
    {
        return $this->gestionOrganismeParAgent;
    }

    /**
     * Set utiliserLucene.
     *
     * @param string $utiliserLucene
     *
     * @return ConfigurationPlateforme
     */
    public function setUtiliserLucene($utiliserLucene)
    {
        $this->utiliserLucene = $utiliserLucene;

        return $this;
    }

    /**
     * Get utiliserLucene.
     *
     * @return string
     */
    public function getUtiliserLucene()
    {
        return $this->utiliserLucene;
    }

    /**
     * Set utiliserPageHtmlLieuxExecution.
     *
     * @param string $utiliserPageHtmlLieuxExecution
     *
     * @return ConfigurationPlateforme
     */
    public function setUtiliserPageHtmlLieuxExecution($utiliserPageHtmlLieuxExecution)
    {
        $this->utiliserPageHtmlLieuxExecution = $utiliserPageHtmlLieuxExecution;

        return $this;
    }

    /**
     * Get utiliserPageHtmlLieuxExecution.
     *
     * @return string
     */
    public function getUtiliserPageHtmlLieuxExecution()
    {
        return $this->utiliserPageHtmlLieuxExecution;
    }

    /**
     * Set pradoValidateurFormatDate.
     *
     * @param string $pradoValidateurFormatDate
     *
     * @return ConfigurationPlateforme
     */
    public function setPradoValidateurFormatDate($pradoValidateurFormatDate)
    {
        $this->pradoValidateurFormatDate = $pradoValidateurFormatDate;

        return $this;
    }

    /**
     * Get pradoValidateurFormatDate.
     *
     * @return string
     */
    public function getPradoValidateurFormatDate()
    {
        return $this->pradoValidateurFormatDate;
    }

    /**
     * Set pradoValidateurFormatEmail.
     *
     * @param string $pradoValidateurFormatEmail
     *
     * @return ConfigurationPlateforme
     */
    public function setPradoValidateurFormatEmail($pradoValidateurFormatEmail)
    {
        $this->pradoValidateurFormatEmail = $pradoValidateurFormatEmail;

        return $this;
    }

    /**
     * Get pradoValidateurFormatEmail.
     *
     * @return string
     */
    public function getPradoValidateurFormatEmail()
    {
        return $this->pradoValidateurFormatEmail;
    }

    /**
     * Set socleExternePpp.
     *
     * @param string $socleExternePpp
     *
     * @return ConfigurationPlateforme
     */
    public function setSocleExternePpp($socleExternePpp)
    {
        $this->socleExternePpp = $socleExternePpp;

        return $this;
    }

    /**
     * Get socleExternePpp.
     *
     * @return string
     */
    public function getSocleExternePpp()
    {
        return $this->socleExternePpp;
    }

    /**
     * Set validationFormatChampsStricte.
     *
     * @param string $validationFormatChampsStricte
     *
     * @return ConfigurationPlateforme
     */
    public function setValidationFormatChampsStricte($validationFormatChampsStricte)
    {
        $this->validationFormatChampsStricte = $validationFormatChampsStricte;

        return $this;
    }

    /**
     * Get validationFormatChampsStricte.
     *
     * @return string
     */
    public function getValidationFormatChampsStricte()
    {
        return $this->validationFormatChampsStricte;
    }

    /**
     * Set poserQuestionNecessiteAuthentification.
     *
     * @param string $poserQuestionNecessiteAuthentification
     *
     * @return ConfigurationPlateforme
     */
    public function setPoserQuestionNecessiteAuthentification($poserQuestionNecessiteAuthentification)
    {
        $this->poserQuestionNecessiteAuthentification = $poserQuestionNecessiteAuthentification;

        return $this;
    }

    /**
     * Get poserQuestionNecessiteAuthentification.
     *
     * @return string
     */
    public function getPoserQuestionNecessiteAuthentification()
    {
        return $this->poserQuestionNecessiteAuthentification;
    }

    /**
     * Set autoriserModifProfilInscritAtes.
     *
     * @param string $autoriserModifProfilInscritAtes
     *
     * @return ConfigurationPlateforme
     */
    public function setAutoriserModifProfilInscritAtes($autoriserModifProfilInscritAtes)
    {
        $this->autoriserModifProfilInscritAtes = $autoriserModifProfilInscritAtes;

        return $this;
    }

    /**
     * Get autoriserModifProfilInscritAtes.
     *
     * @return string
     */
    public function getAutoriserModifProfilInscritAtes()
    {
        return $this->autoriserModifProfilInscritAtes;
    }

    /**
     * Set uniciteReferenceConsultation.
     *
     * @param string $uniciteReferenceConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setUniciteReferenceConsultation($uniciteReferenceConsultation)
    {
        $this->uniciteReferenceConsultation = $uniciteReferenceConsultation;

        return $this;
    }

    /**
     * Get uniciteReferenceConsultation.
     *
     * @return string
     */
    public function getUniciteReferenceConsultation()
    {
        return $this->uniciteReferenceConsultation;
    }

    /**
     * Set registrePapierRcnumRcvilleObligatoires.
     *
     * @param string $registrePapierRcnumRcvilleObligatoires
     *
     * @return ConfigurationPlateforme
     */
    public function setRegistrePapierRcnumRcvilleObligatoires($registrePapierRcnumRcvilleObligatoires)
    {
        $this->registrePapierRcnumRcvilleObligatoires = $registrePapierRcnumRcvilleObligatoires;

        return $this;
    }

    /**
     * Get registrePapierRcnumRcvilleObligatoires.
     *
     * @return string
     */
    public function getRegistrePapierRcnumRcvilleObligatoires()
    {
        return $this->registrePapierRcnumRcvilleObligatoires;
    }

    /**
     * Set registrePapierAdresseCpVilleObligatoires.
     *
     * @param string $registrePapierAdresseCpVilleObligatoires
     *
     * @return ConfigurationPlateforme
     */
    public function setRegistrePapierAdresseCpVilleObligatoires($registrePapierAdresseCpVilleObligatoires)
    {
        $this->registrePapierAdresseCpVilleObligatoires = $registrePapierAdresseCpVilleObligatoires;

        return $this;
    }

    /**
     * Get registrePapierAdresseCpVilleObligatoires.
     *
     * @return string
     */
    public function getRegistrePapierAdresseCpVilleObligatoires()
    {
        return $this->registrePapierAdresseCpVilleObligatoires;
    }

    /**
     * Set telechargerDceSansIdentification.
     *
     * @param string $telechargerDceSansIdentification
     *
     * @return ConfigurationPlateforme
     */
    public function setTelechargerDceSansIdentification($telechargerDceSansIdentification)
    {
        $this->telechargerDceSansIdentification = $telechargerDceSansIdentification;

        return $this;
    }

    /**
     * Get telechargerDceSansIdentification.
     *
     * @return string
     */
    public function getTelechargerDceSansIdentification()
    {
        return $this->telechargerDceSansIdentification;
    }

    /**
     * Set gestionEntrepriseParAgent.
     *
     * @param string $gestionEntrepriseParAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionEntrepriseParAgent($gestionEntrepriseParAgent)
    {
        $this->gestionEntrepriseParAgent = $gestionEntrepriseParAgent;

        return $this;
    }

    /**
     * Get gestionEntrepriseParAgent.
     *
     * @return string
     */
    public function getGestionEntrepriseParAgent()
    {
        return $this->gestionEntrepriseParAgent;
    }

    /**
     * Set autoriserCaracteresSpeciauxDansReference.
     *
     * @param string $autoriserCaracteresSpeciauxDansReference
     *
     * @return ConfigurationPlateforme
     */
    public function setAutoriserCaracteresSpeciauxDansReference($autoriserCaracteresSpeciauxDansReference)
    {
        $this->autoriserCaracteresSpeciauxDansReference = $autoriserCaracteresSpeciauxDansReference;

        return $this;
    }

    /**
     * Get autoriserCaracteresSpeciauxDansReference.
     *
     * @return string
     */
    public function getAutoriserCaracteresSpeciauxDansReference()
    {
        return $this->autoriserCaracteresSpeciauxDansReference;
    }

    /**
     * Set inscriptionLibreEntreprise.
     *
     * @param string $inscriptionLibreEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setInscriptionLibreEntreprise($inscriptionLibreEntreprise)
    {
        $this->inscriptionLibreEntreprise = $inscriptionLibreEntreprise;

        return $this;
    }

    /**
     * Get inscriptionLibreEntreprise.
     *
     * @return string
     */
    public function getInscriptionLibreEntreprise()
    {
        return $this->inscriptionLibreEntreprise;
    }

    /**
     * Set afficherCodeService.
     *
     * @param string $afficherCodeService
     *
     * @return ConfigurationPlateforme
     */
    public function setAfficherCodeService($afficherCodeService)
    {
        $this->afficherCodeService = $afficherCodeService;

        return $this;
    }

    /**
     * Get afficherCodeService.
     *
     * @return string
     */
    public function getAfficherCodeService()
    {
        return $this->afficherCodeService;
    }

    /**
     * Set authenticateAgentByLogin.
     *
     * @param string $authenticateAgentByLogin
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthenticateAgentByLogin($authenticateAgentByLogin)
    {
        $this->authenticateAgentByLogin = $authenticateAgentByLogin;

        return $this;
    }

    /**
     * Get authenticateAgentByLogin.
     *
     * @return string
     */
    public function getAuthenticateAgentByLogin()
    {
        return $this->authenticateAgentByLogin;
    }

    /**
     * Set authenticateAgentByCert.
     *
     * @param string $authenticateAgentByCert
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthenticateAgentByCert($authenticateAgentByCert)
    {
        $this->authenticateAgentByCert = $authenticateAgentByCert;

        return $this;
    }

    /**
     * Get authenticateAgentByCert.
     *
     * @return string
     */
    public function getAuthenticateAgentByCert()
    {
        return $this->authenticateAgentByCert;
    }

    /**
     * Set genererActeDengagement.
     *
     * @param string $genererActeDengagement
     *
     * @return ConfigurationPlateforme
     */
    public function setGenererActeDengagement($genererActeDengagement)
    {
        $this->genererActeDengagement = $genererActeDengagement;

        return $this;
    }

    /**
     * Get genererActeDengagement.
     *
     * @return string
     */
    public function getGenererActeDengagement()
    {
        return $this->genererActeDengagement;
    }

    /**
     * Set entrepriseControleFormatMotDePasse.
     *
     * @param string $entrepriseControleFormatMotDePasse
     *
     * @return ConfigurationPlateforme
     */
    public function setEntrepriseControleFormatMotDePasse($entrepriseControleFormatMotDePasse)
    {
        $this->entrepriseControleFormatMotDePasse = $entrepriseControleFormatMotDePasse;

        return $this;
    }

    /**
     * Get entrepriseControleFormatMotDePasse.
     *
     * @return string
     */
    public function getEntrepriseControleFormatMotDePasse()
    {
        return $this->entrepriseControleFormatMotDePasse;
    }

    /**
     * Set autreAnnonceInformation.
     *
     * @param string $autreAnnonceInformation
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceInformation($autreAnnonceInformation)
    {
        $this->autreAnnonceInformation = $autreAnnonceInformation;

        return $this;
    }

    /**
     * Get autreAnnonceInformation.
     *
     * @return string
     */
    public function getAutreAnnonceInformation()
    {
        return $this->autreAnnonceInformation;
    }

    /**
     * Set creerAutreAnnonce.
     *
     * @param string $creerAutreAnnonce
     *
     * @return ConfigurationPlateforme
     */
    public function setCreerAutreAnnonce($creerAutreAnnonce)
    {
        $this->creerAutreAnnonce = $creerAutreAnnonce;

        return $this;
    }

    /**
     * Get creerAutreAnnonce.
     *
     * @return string
     */
    public function getCreerAutreAnnonce()
    {
        return $this->creerAutreAnnonce;
    }

    /**
     * Set consultationClause.
     *
     * @param string $consultationClause
     *
     * @return ConfigurationPlateforme
     */
    public function setConsultationClause($consultationClause)
    {
        $this->consultationClause = $consultationClause;

        return $this;
    }

    /**
     * Get consultationClause.
     *
     * @return string
     */
    public function getConsultationClause()
    {
        return $this->consultationClause;
    }

    /**
     * Set panierEntreprise.
     *
     * @param string $panierEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setPanierEntreprise($panierEntreprise)
    {
        $this->panierEntreprise = $panierEntreprise;

        return $this;
    }

    /**
     * Get panierEntreprise.
     *
     * @return string
     */
    public function getPanierEntreprise()
    {
        return $this->panierEntreprise;
    }

    /**
     * Set parametragePubliciteParTypeProcedure.
     *
     * @param string $parametragePubliciteParTypeProcedure
     *
     * @return ConfigurationPlateforme
     */
    public function setParametragePubliciteParTypeProcedure($parametragePubliciteParTypeProcedure)
    {
        $this->parametragePubliciteParTypeProcedure = $parametragePubliciteParTypeProcedure;

        return $this;
    }

    /**
     * Get parametragePubliciteParTypeProcedure.
     *
     * @return string
     */
    public function getParametragePubliciteParTypeProcedure()
    {
        return $this->parametragePubliciteParTypeProcedure;
    }

    /**
     * Set exportDecision.
     *
     * @param string $exportDecision
     *
     * @return ConfigurationPlateforme
     */
    public function setExportDecision($exportDecision)
    {
        $this->exportDecision = $exportDecision;

        return $this;
    }

    /**
     * Get exportDecision.
     *
     * @return string
     */
    public function getExportDecision()
    {
        return $this->exportDecision;
    }

    /**
     * Set regleMiseEnLigneParEntiteCoordinatrice.
     *
     * @param string $regleMiseEnLigneParEntiteCoordinatrice
     *
     * @return ConfigurationPlateforme
     */
    public function setRegleMiseEnLigneParEntiteCoordinatrice($regleMiseEnLigneParEntiteCoordinatrice)
    {
        $this->regleMiseEnLigneParEntiteCoordinatrice = $regleMiseEnLigneParEntiteCoordinatrice;

        return $this;
    }

    /**
     * Get regleMiseEnLigneParEntiteCoordinatrice.
     *
     * @return string
     */
    public function getRegleMiseEnLigneParEntiteCoordinatrice()
    {
        return $this->regleMiseEnLigneParEntiteCoordinatrice;
    }

    /**
     * Set gestionNewsletter.
     *
     * @param string $gestionNewsletter
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionNewsletter($gestionNewsletter)
    {
        $this->gestionNewsletter = $gestionNewsletter;

        return $this;
    }

    /**
     * Get gestionNewsletter.
     *
     * @return string
     */
    public function getGestionNewsletter()
    {
        return $this->gestionNewsletter;
    }

    /**
     * Set publiciteOpoce.
     *
     * @param string $publiciteOpoce
     *
     * @return ConfigurationPlateforme
     */
    public function setPubliciteOpoce($publiciteOpoce)
    {
        $this->publiciteOpoce = $publiciteOpoce;

        return $this;
    }

    /**
     * Get publiciteOpoce.
     *
     * @return string
     */
    public function getPubliciteOpoce()
    {
        return $this->publiciteOpoce;
    }

    /**
     * Set gestionModelesFormulaire.
     *
     * @param string $gestionModelesFormulaire
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionModelesFormulaire($gestionModelesFormulaire)
    {
        $this->gestionModelesFormulaire = $gestionModelesFormulaire;

        return $this;
    }

    /**
     * Get gestionModelesFormulaire.
     *
     * @return string
     */
    public function getGestionModelesFormulaire()
    {
        return $this->gestionModelesFormulaire;
    }

    /**
     * Set gestionAdressesFacturationJal.
     *
     * @param string $gestionAdressesFacturationJal
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionAdressesFacturationJal($gestionAdressesFacturationJal)
    {
        $this->gestionAdressesFacturationJal = $gestionAdressesFacturationJal;

        return $this;
    }

    /**
     * Get gestionAdressesFacturationJal.
     *
     * @return string
     */
    public function getGestionAdressesFacturationJal()
    {
        return $this->gestionAdressesFacturationJal;
    }

    /**
     * Set publiciteMarchesEnLigne.
     *
     * @param string $publiciteMarchesEnLigne
     *
     * @return ConfigurationPlateforme
     */
    public function setPubliciteMarchesEnLigne($publiciteMarchesEnLigne)
    {
        $this->publiciteMarchesEnLigne = $publiciteMarchesEnLigne;

        return $this;
    }

    /**
     * Get publiciteMarchesEnLigne.
     *
     * @return string
     */
    public function getPubliciteMarchesEnLigne()
    {
        return $this->publiciteMarchesEnLigne;
    }

    /**
     * Set lieuOuverturePlisObligatoire.
     *
     * @param string $lieuOuverturePlisObligatoire
     *
     * @return ConfigurationPlateforme
     */
    public function setLieuOuverturePlisObligatoire($lieuOuverturePlisObligatoire)
    {
        $this->lieuOuverturePlisObligatoire = $lieuOuverturePlisObligatoire;

        return $this;
    }

    /**
     * Get lieuOuverturePlisObligatoire.
     *
     * @return string
     */
    public function getLieuOuverturePlisObligatoire()
    {
        return $this->lieuOuverturePlisObligatoire;
    }

    /**
     * Set dossierAdditif.
     *
     * @param string $dossierAdditif
     *
     * @return ConfigurationPlateforme
     */
    public function setDossierAdditif($dossierAdditif)
    {
        $this->dossierAdditif = $dossierAdditif;

        return $this;
    }

    /**
     * Get dossierAdditif.
     *
     * @return string
     */
    public function getDossierAdditif()
    {
        return $this->dossierAdditif;
    }

    /**
     * Set typeMarche.
     *
     * @param string $typeMarche
     *
     * @return ConfigurationPlateforme
     */
    public function setTypeMarche($typeMarche)
    {
        $this->typeMarche = $typeMarche;

        return $this;
    }

    /**
     * Get typeMarche.
     *
     * @return string
     */
    public function getTypeMarche()
    {
        return $this->typeMarche;
    }

    /**
     * Set typePrestation.
     *
     * @param string $typePrestation
     *
     * @return ConfigurationPlateforme
     */
    public function setTypePrestation($typePrestation)
    {
        $this->typePrestation = $typePrestation;

        return $this;
    }

    /**
     * Get typePrestation.
     *
     * @return string
     */
    public function getTypePrestation()
    {
        return $this->typePrestation;
    }

    /**
     * Set afficherTjrBlocCaracteristiqueReponse.
     *
     * @param string $afficherTjrBlocCaracteristiqueReponse
     *
     * @return ConfigurationPlateforme
     */
    public function setAfficherTjrBlocCaracteristiqueReponse($afficherTjrBlocCaracteristiqueReponse)
    {
        $this->afficherTjrBlocCaracteristiqueReponse = $afficherTjrBlocCaracteristiqueReponse;

        return $this;
    }

    /**
     * Get afficherTjrBlocCaracteristiqueReponse.
     *
     * @return string
     */
    public function getAfficherTjrBlocCaracteristiqueReponse()
    {
        return $this->afficherTjrBlocCaracteristiqueReponse;
    }

    /**
     * Set alerteMetier.
     *
     * @param string $alerteMetier
     *
     * @return ConfigurationPlateforme
     */
    public function setAlerteMetier($alerteMetier)
    {
        $this->alerteMetier = $alerteMetier;

        return $this;
    }

    /**
     * Get alerteMetier.
     *
     * @return string
     */
    public function getAlerteMetier()
    {
        return $this->alerteMetier;
    }

    /**
     * Set bourseALaSousTraitance.
     *
     * @param string $bourseALaSousTraitance
     *
     * @return ConfigurationPlateforme
     */
    public function setBourseALaSousTraitance($bourseALaSousTraitance)
    {
        $this->bourseALaSousTraitance = $bourseALaSousTraitance;

        return $this;
    }

    /**
     * Get bourseALaSousTraitance.
     *
     * @return string
     */
    public function getBourseALaSousTraitance()
    {
        return $this->bourseALaSousTraitance;
    }

    /**
     * Set partagerConsultation.
     *
     * @param string $partagerConsultation
     *
     * @return ConfigurationPlateforme
     */
    public function setPartagerConsultation($partagerConsultation)
    {
        $this->partagerConsultation = $partagerConsultation;

        return $this;
    }

    /**
     * Get partagerConsultation.
     *
     * @return string
     */
    public function getPartagerConsultation()
    {
        return $this->partagerConsultation;
    }

    /**
     * Set annuaireAcheteursPublics.
     *
     * @param string $annuaireAcheteursPublics
     *
     * @return ConfigurationPlateforme
     */
    public function setAnnuaireAcheteursPublics($annuaireAcheteursPublics)
    {
        $this->annuaireAcheteursPublics = $annuaireAcheteursPublics;

        return $this;
    }

    /**
     * Get annuaireAcheteursPublics.
     *
     * @return string
     */
    public function getAnnuaireAcheteursPublics()
    {
        return $this->annuaireAcheteursPublics;
    }

    /**
     * Set entrepriseActionsGroupees.
     *
     * @param string $entrepriseActionsGroupees
     *
     * @return ConfigurationPlateforme
     */
    public function setEntrepriseActionsGroupees($entrepriseActionsGroupees)
    {
        $this->entrepriseActionsGroupees = $entrepriseActionsGroupees;

        return $this;
    }

    /**
     * Get entrepriseActionsGroupees.
     *
     * @return string
     */
    public function getEntrepriseActionsGroupees()
    {
        return $this->entrepriseActionsGroupees;
    }

    /**
     * Set publierGuides.
     *
     * @param string $publierGuides
     *
     * @return ConfigurationPlateforme
     */
    public function setPublierGuides($publierGuides)
    {
        $this->publierGuides = $publierGuides;

        return $this;
    }

    /**
     * Get publierGuides.
     *
     * @return string
     */
    public function getPublierGuides()
    {
        return $this->publierGuides;
    }

    /**
     * Set rechercheAutoCompletion.
     *
     * @param string $rechercheAutoCompletion
     *
     * @return ConfigurationPlateforme
     */
    public function setRechercheAutoCompletion($rechercheAutoCompletion)
    {
        $this->rechercheAutoCompletion = $rechercheAutoCompletion;

        return $this;
    }

    /**
     * Get rechercheAutoCompletion.
     *
     * @return string
     */
    public function getRechercheAutoCompletion()
    {
        return $this->rechercheAutoCompletion;
    }

    /**
     * Set statutCompteEntreprise.
     *
     * @param string $statutCompteEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setStatutCompteEntreprise($statutCompteEntreprise)
    {
        $this->statutCompteEntreprise = $statutCompteEntreprise;

        return $this;
    }

    /**
     * Get statutCompteEntreprise.
     *
     * @return string
     */
    public function getStatutCompteEntreprise()
    {
        return $this->statutCompteEntreprise;
    }

    /**
     * Set gestionOrganismes.
     *
     * @param string $gestionOrganismes
     *
     * @return ConfigurationPlateforme
     */
    public function setGestionOrganismes($gestionOrganismes)
    {
        $this->gestionOrganismes = $gestionOrganismes;

        return $this;
    }

    /**
     * Get gestionOrganismes.
     *
     * @return string
     */
    public function getGestionOrganismes()
    {
        return $this->gestionOrganismes;
    }

    /**
     * Set accueilEntreprisePersonnalise.
     *
     * @param string $accueilEntreprisePersonnalise
     *
     * @return ConfigurationPlateforme
     */
    public function setAccueilEntreprisePersonnalise($accueilEntreprisePersonnalise)
    {
        $this->accueilEntreprisePersonnalise = $accueilEntreprisePersonnalise;

        return $this;
    }

    /**
     * Get accueilEntreprisePersonnalise.
     *
     * @return string
     */
    public function getAccueilEntreprisePersonnalise()
    {
        return $this->accueilEntreprisePersonnalise;
    }

    /**
     * Set interfaceModuleSub.
     *
     * @param string $interfaceModuleSub
     *
     * @return ConfigurationPlateforme
     */
    public function setInterfaceModuleSub($interfaceModuleSub)
    {
        $this->interfaceModuleSub = $interfaceModuleSub;

        return $this;
    }

    /**
     * Get interfaceModuleSub.
     *
     * @return string
     */
    public function getInterfaceModuleSub()
    {
        return $this->interfaceModuleSub;
    }

    /**
     * Set authentificationAgentMultiOrganismes.
     *
     * @param string $authentificationAgentMultiOrganismes
     *
     * @return ConfigurationPlateforme
     */
    public function setAuthentificationAgentMultiOrganismes($authentificationAgentMultiOrganismes)
    {
        $this->authentificationAgentMultiOrganismes = $authentificationAgentMultiOrganismes;

        return $this;
    }

    /**
     * Get authentificationAgentMultiOrganismes.
     *
     * @return string
     */
    public function getAuthentificationAgentMultiOrganismes()
    {
        return $this->authentificationAgentMultiOrganismes;
    }

    /**
     * Set lieuxExecutionCarte.
     *
     * @param string $lieuxExecutionCarte
     *
     * @return ConfigurationPlateforme
     */
    public function setLieuxExecutionCarte($lieuxExecutionCarte)
    {
        $this->lieuxExecutionCarte = $lieuxExecutionCarte;

        return $this;
    }

    /**
     * Get lieuxExecutionCarte.
     *
     * @return string
     */
    public function getLieuxExecutionCarte()
    {
        return $this->lieuxExecutionCarte;
    }

    /**
     * Set surchargeReferentiels.
     *
     * @param string $surchargeReferentiels
     *
     * @return ConfigurationPlateforme
     */
    public function setSurchargeReferentiels($surchargeReferentiels)
    {
        $this->surchargeReferentiels = $surchargeReferentiels;

        return $this;
    }

    /**
     * Get surchargeReferentiels.
     *
     * @return string
     */
    public function getSurchargeReferentiels()
    {
        return $this->surchargeReferentiels;
    }

    /**
     * Set modeRestrictionRgs.
     *
     * @param string $modeRestrictionRgs
     *
     * @return ConfigurationPlateforme
     */
    public function setModeRestrictionRgs($modeRestrictionRgs)
    {
        $this->modeRestrictionRgs = $modeRestrictionRgs;

        return $this;
    }

    /**
     * Get modeRestrictionRgs.
     *
     * @return string
     */
    public function getModeRestrictionRgs()
    {
        return $this->modeRestrictionRgs;
    }

    /**
     * Set autreAnnonceDecisionResiliation.
     *
     * @param string $autreAnnonceDecisionResiliation
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceDecisionResiliation($autreAnnonceDecisionResiliation)
    {
        $this->autreAnnonceDecisionResiliation = $autreAnnonceDecisionResiliation;

        return $this;
    }

    /**
     * Get autreAnnonceDecisionResiliation.
     *
     * @return string
     */
    public function getAutreAnnonceDecisionResiliation()
    {
        return $this->autreAnnonceDecisionResiliation;
    }

    /**
     * Set autreAnnonceSyntheseRapportAudit.
     *
     * @param string $autreAnnonceSyntheseRapportAudit
     *
     * @return ConfigurationPlateforme
     */
    public function setAutreAnnonceSyntheseRapportAudit($autreAnnonceSyntheseRapportAudit)
    {
        $this->autreAnnonceSyntheseRapportAudit = $autreAnnonceSyntheseRapportAudit;

        return $this;
    }

    /**
     * Get autreAnnonceSyntheseRapportAudit.
     *
     * @return string
     */
    public function getAutreAnnonceSyntheseRapportAudit()
    {
        return $this->autreAnnonceSyntheseRapportAudit;
    }

    /**
     * Set ficheWeka.
     *
     * @param string $ficheWeka
     *
     * @return ConfigurationPlateforme
     */
    public function setFicheWeka($ficheWeka)
    {
        $this->ficheWeka = $ficheWeka;

        return $this;
    }

    /**
     * Get ficheWeka.
     *
     * @return string
     */
    public function getFicheWeka()
    {
        return $this->ficheWeka;
    }

    /**
     * Set generationAutomatiqueMdpAgent.
     *
     * @param string $generationAutomatiqueMdpAgent
     *
     * @return ConfigurationPlateforme
     */
    public function setGenerationAutomatiqueMdpAgent($generationAutomatiqueMdpAgent)
    {
        $this->generationAutomatiqueMdpAgent = $generationAutomatiqueMdpAgent;

        return $this;
    }

    /**
     * Get generationAutomatiqueMdpAgent.
     *
     * @return string
     */
    public function getGenerationAutomatiqueMdpAgent()
    {
        return $this->generationAutomatiqueMdpAgent;
    }

    /**
     * Set generationAutomatiqueMdpInscrit.
     *
     * @param string $generationAutomatiqueMdpInscrit
     *
     * @return ConfigurationPlateforme
     */
    public function setGenerationAutomatiqueMdpInscrit($generationAutomatiqueMdpInscrit)
    {
        $this->generationAutomatiqueMdpInscrit = $generationAutomatiqueMdpInscrit;

        return $this;
    }

    /**
     * Get generationAutomatiqueMdpInscrit.
     *
     * @return string
     */
    public function getGenerationAutomatiqueMdpInscrit()
    {
        return $this->generationAutomatiqueMdpInscrit;
    }

    /**
     * Set listeAcRgs.
     *
     * @param string $listeAcRgs
     *
     * @return ConfigurationPlateforme
     */
    public function setListeAcRgs($listeAcRgs)
    {
        $this->listeAcRgs = $listeAcRgs;

        return $this;
    }

    /**
     * Get listeAcRgs.
     *
     * @return string
     */
    public function getListeAcRgs()
    {
        return $this->listeAcRgs;
    }

    /**
     * Set listeConsOrg.
     *
     * @param string $listeConsOrg
     *
     * @return ConfigurationPlateforme
     */
    public function setListeConsOrg($listeConsOrg)
    {
        $this->listeConsOrg = $listeConsOrg;

        return $this;
    }

    /**
     * Get listeConsOrg.
     *
     * @return string
     */
    public function getListeConsOrg()
    {
        return $this->listeConsOrg;
    }

    /**
     * Set marchePublicSimplifieEntreprise.
     *
     * @param string $marchePublicSimplifieEntreprise
     *
     * @return ConfigurationPlateforme
     */
    public function setMarchePublicSimplifieEntreprise($marchePublicSimplifieEntreprise)
    {
        $this->marchePublicSimplifieEntreprise = $marchePublicSimplifieEntreprise;

        return $this;
    }

    /**
     * Get marchePublicSimplifieEntreprise.
     *
     * @return string
     */
    public function getMarchePublicSimplifieEntreprise()
    {
        return $this->marchePublicSimplifieEntreprise;
    }

    /**
     * Set archiveParLot.
     *
     * @param string $archiveParLot
     *
     * @return ConfigurationPlateforme
     */
    public function setArchiveParLot($archiveParLot)
    {
        $this->archiveParLot = $archiveParLot;

        return $this;
    }

    /**
     * Get archiveParLot.
     *
     * @return string
     */
    public function getArchiveParLot()
    {
        return $this->archiveParLot;
    }

    /**
     * Set documentsReference.
     *
     * @param string $documentsReference
     *
     * @return ConfigurationPlateforme
     */
    public function setDocumentsReference($documentsReference)
    {
        $this->documentsReference = $documentsReference;

        return $this;
    }

    /**
     * Get documentsReference.
     *
     * @return string
     */
    public function getDocumentsReference()
    {
        return $this->documentsReference;
    }

    /**
     * Set recherchesFavorites.
     *
     * @param string $recherchesFavorites
     *
     * @return ConfigurationPlateforme
     */
    public function setRecherchesFavorites($recherchesFavorites)
    {
        $this->recherchesFavorites = $recherchesFavorites;

        return $this;
    }

    /**
     * Get recherchesFavorites.
     *
     * @return string
     */
    public function getRecherchesFavorites()
    {
        return $this->recherchesFavorites;
    }

    /**
     * Set synchronisationSgmap.
     *
     * @param string $synchronisationSgmap
     *
     * @return ConfigurationPlateforme
     */
    public function setSynchronisationSgmap($synchronisationSgmap)
    {
        $this->synchronisationSgmap = $synchronisationSgmap;

        return $this;
    }

    /**
     * Get synchronisationSgmap.
     *
     * @return string
     */
    public function getSynchronisationSgmap()
    {
        return $this->synchronisationSgmap;
    }

    /**
     * Set donneesCandidat.
     *
     * @param string $donneesCandidat
     *
     * @return ConfigurationPlateforme
     */
    public function setDonneesCandidat($donneesCandidat)
    {
        $this->donneesCandidat = $donneesCandidat;

        return $this;
    }

    /**
     * Get donneesCandidat.
     *
     * @return string
     */
    public function getDonneesCandidat()
    {
        return $this->donneesCandidat;
    }

    /**
     * Set autoriserCreationEntrepriseEtrangere.
     *
     * @param string $autoriserCreationEntrepriseEtrangere
     *
     * @return ConfigurationPlateforme
     */
    public function setAutoriserCreationEntrepriseEtrangere($autoriserCreationEntrepriseEtrangere)
    {
        $this->autoriserCreationEntrepriseEtrangere = $autoriserCreationEntrepriseEtrangere;

        return $this;
    }

    /**
     * Get autoriserCreationEntrepriseEtrangere.
     *
     * @return string
     */
    public function getAutoriserCreationEntrepriseEtrangere()
    {
        return $this->autoriserCreationEntrepriseEtrangere;
    }

    /**
     * Set bourseCotraitance.
     *
     * @param string $bourseCotraitance
     *
     * @return ConfigurationPlateforme
     */
    public function setBourseCotraitance($bourseCotraitance)
    {
        $this->bourseCotraitance = $bourseCotraitance;

        return $this;
    }

    /**
     * Get bourseCotraitance.
     *
     * @return string
     */
    public function getBourseCotraitance()
    {
        return $this->bourseCotraitance;
    }

    /**
     * Set acSadTransversaux.
     *
     * @param string $acSadTransversaux
     *
     * @return ConfigurationPlateforme
     */
    public function setAcSadTransversaux($acSadTransversaux)
    {
        $this->acSadTransversaux = $acSadTransversaux;

        return $this;
    }

    /**
     * Get acSadTransversaux.
     *
     * @return string
     */
    public function getAcSadTransversaux()
    {
        return $this->acSadTransversaux;
    }

    /**
     * Set webServiceParSilo.
     *
     * @param string $webServiceParSilo
     *
     * @return ConfigurationPlateforme
     */
    public function setWebServiceParSilo($webServiceParSilo)
    {
        $this->webServiceParSilo = $webServiceParSilo;

        return $this;
    }

    /**
     * Get webServiceParSilo.
     *
     * @return string
     */
    public function getWebServiceParSilo()
    {
        return $this->webServiceParSilo;
    }

    /**
     * Set groupement.
     *
     * @param string $groupement
     *
     * @return ConfigurationPlateforme
     */
    public function setGroupement($groupement)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Get groupement.
     *
     * @return string
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * @return string
     */
    public function getNotificationsAgent()
    {
        return $this->notificationsAgent;
    }

    /**
     * @param string $notificationsAgent
     */
    public function setNotificationsAgent($notificationsAgent)
    {
        $this->notificationsAgent = $notificationsAgent;
    }

    /**
     * @return string
     */
    public function getPublicite()
    {
        return $this->publicite;
    }

    /**
     * @param string $publicite
     */
    public function setPublicite($publicite)
    {
        $this->publicite = $publicite;
    }

    public function getInterfaceDume(): string
    {
        return $this->interfaceDume;
    }

    public function setInterfaceDume(string $interfaceDume)
    {
        $this->interfaceDume = $interfaceDume;
    }

    /**
     * @return string
     */
    public function getTokenApiSgmapApi()
    {
        return $this->tokenApiSgmapApi;
    }

    public function setTokenApiSgmapApi(string $tokenApiSgmapApi)
    {
        $this->tokenApiSgmapApi = $tokenApiSgmapApi;
    }

    /**
     * @return string
     */
    public function getCaseAttestationConsultation()
    {
        return $this->caseAttestationConsultation;
    }

    /**
     * @param string $caseAttestationConsultation
     */
    public function setCaseAttestationConsultation($caseAttestationConsultation)
    {
        $this->caseAttestationConsultation = $caseAttestationConsultation;
    }

    /**
     * @return string
     */
    public function getPlateformeEditeur()
    {
        return $this->plateformeEditeur;
    }

    /**
     * @param string $plateformeEditeur
     */
    public function setPlateformeEditeur($plateformeEditeur)
    {
        $this->plateformeEditeur = $plateformeEditeur;
    }

    /**
     * @return string
     */
    public function getDonneesEssentiellesSuiviSn()
    {
        return $this->donneesEssentiellesSuiviSn;
    }

    /**
     * @param string $donneesEssentiellesSuiviSn
     */
    public function setDonneesEssentiellesSuiviSn($donneesEssentiellesSuiviSn)
    {
        $this->donneesEssentiellesSuiviSn = $donneesEssentiellesSuiviSn;
    }


    /**
     * @return string
     */
    public function getMasquerElementsMps()
    {
        return $this->masquerElementsMps;
    }

    public function setMasquerElementsMps(string $masquerElementsMps)
    {
        $this->masquerElementsMps = $masquerElementsMps;

        return $this;
    }

    /**
     * @return string
     */
    public function getMasquerAtexoSign()
    {
        return $this->masquerAtexoSign;
    }

    /**
     * @param string $masquerAtexoSign
     *
     * @return ConfigurationPlateforme
     */
    public function setMasquerAtexoSign($masquerAtexoSign)
    {
        $this->masquerAtexoSign = $masquerAtexoSign;

        return $this;
    }

    public function isMessagerieV2(): bool
    {
        return $this->messagerieV2;
    }

    public function setMessagerieV2(bool $messagerieV2)
    {
        $this->messagerieV2 = $messagerieV2;
    }

    public function getEntrepriseDureeVieMotDePasse(): int
    {
        return $this->entrepriseDureeVieMotDePasse;
    }

    public function setEntrepriseDureeVieMotDePasse(int $entrepriseDureeVieMotDePasse)
    {
        $this->entrepriseDureeVieMotDePasse = $entrepriseDureeVieMotDePasse;
    }

    public function getEntrepriseMotsDePasseHistorises(): int
    {
        return $this->entrepriseMotsDePasseHistorises;
    }

    public function setEntrepriseMotsDePasseHistorises(int $entrepriseMotsDePasseHistorises)
    {
        $this->entrepriseMotsDePasseHistorises = $entrepriseMotsDePasseHistorises;
    }

    public function isMenuAgentComplet(): bool
    {
        return $this->menuAgentComplet;
    }

    public function setMenuAgentComplet(bool $menuAgentComplet): self
    {
        $this->menuAgentComplet = $menuAgentComplet;

        return $this;
    }

    public function getUniciteMailAgent(): int
    {
        return $this->uniciteMailAgent;
    }

    public function setUniciteMailAgent(int $uniciteMailAgent): void
    {
        $this->uniciteMailAgent = $uniciteMailAgent;
    }

    public function getAuthenticateAgentOpenidMicrosoft(): string
    {
        return $this->authenticateAgentOpenidMicrosoft;
    }

    public function setAuthenticateAgentOpenidMicrosoft(string $authenticateAgentOpenidMicrosoft): self
    {
        $this->authenticateAgentOpenidMicrosoft = $authenticateAgentOpenidMicrosoft;
        return $this;
    }

    public function getAuthenticateInscritOpenidMicrosoft(): string
    {
        return $this->authenticateInscritOpenidMicrosoft;
    }

    public function setAuthenticateInscritOpenidMicrosoft(string $authenticateInscritOpenidMicrosoft): self
    {
        $this->authenticateInscritOpenidMicrosoft = $authenticateInscritOpenidMicrosoft;
        return $this;
    }

    public function getAuthenticateAgentOpenidKeycloak(): string
    {
        return $this->authenticateAgentOpenidKeycloak;
    }

    public function setAuthenticateAgentOpenidKeycloak(string $authenticateAgentOpenidKeycloak): self
    {
        $this->authenticateAgentOpenidKeycloak = $authenticateAgentOpenidKeycloak;
        return $this;
    }

    public function getAuthenticateInscritOpenidKeycloak(): string
    {
        return $this->authenticateInscritOpenidKeycloak;
    }

    public function setAuthenticateInscritOpenidKeycloak(string $authenticateInscritOpenidKeycloak): self
    {
        $this->authenticateInscritOpenidKeycloak = $authenticateInscritOpenidKeycloak;
        return $this;
    }

    /**
     * @param $controleTailleDepot
     * @return $this
     */
    public function setControleTailleDepot($controleTailleDepot): ConfigurationPlateforme
    {
        $this->controleTailleDepot = $controleTailleDepot;
        return $this;
    }

    /**
     * @return int
     */
    public function getControleTailleDepot(): ?int
    {
        return $this->controleTailleDepot;
    }

    /**
     * @return string
     */
    public function getAuthenticateAgentSaml(): string
    {
        return $this->authenticateAgentSaml;
    }

    /**
     * @param string $authenticateAgentSaml
     * @return ConfigurationPlateforme
     */
    public function setAuthenticateAgentSaml(string $authenticateAgentSaml): ConfigurationPlateforme
    {
        $this->authenticateAgentSaml = $authenticateAgentSaml;
        return $this;
    }

    public function isDonnerAvisDepotEntreprise(): bool
    {
        return $this->donnerAvisDepotEntreprise;
    }

    public function setDonnerAvisDepotEntreprise(bool $donnerAvisDepotEntreprise): self
    {
        $this->donnerAvisDepotEntreprise = $donnerAvisDepotEntreprise;

        return $this;
    }

    public function isRecueilConsentementRgpd(): bool
    {
        return $this->recueilConsentementRgpd;
    }

    public function setRecueilConsentementRgpd(bool $recueilConsentementRgpd): self
    {
        $this->recueilConsentementRgpd = $recueilConsentementRgpd;

        return $this;
    }

    public function isSaisiePartFranceUeDepot(): bool
    {
        return $this->saisiePartFranceUeDepot;
    }

    public function setSaisiePartFranceUeDepot(bool $saisiePartFranceUeDepot): self
    {
        $this->saisiePartFranceUeDepot = $saisiePartFranceUeDepot;

        return $this;
    }

    /**
     * @return bool
     */
    public function getModulesAutoformation(): bool
    {
        return $this->modulesAutoformation;
    }

    /**
     * @param bool $modulesAutoformation
     */
    public function setModulesAutoformation(bool $modulesAutoformation): self
    {
        $this->modulesAutoformation = $modulesAutoformation;

        return $this;
    }

    public function getAfficherValeurEstimee(): string
    {
        return $this->afficherValeurEstimee;
    }

    public function setAfficherValeurEstimee(string $afficherValeurEstimee): self
    {
        $this->afficherValeurEstimee = $afficherValeurEstimee;

        return $this;
    }

    public function isAfficherRattachementService(): bool
    {
        return $this->afficherRattachementService;
    }

    public function setAfficherRattachementService(bool $afficherRattachementService): self
    {
        $this->afficherRattachementService = $afficherRattachementService;

        return $this;
    }

    public function isPublicationFormatLibre(): bool
    {
        return $this->publicationFormatLibre;
    }

    public function setPublicationFormatLibre(bool $publicationFormatLibre): self
    {
        $this->publicationFormatLibre = $publicationFormatLibre;

        return $this;
    }

    public function isConfPubliciteFrancaise(): bool
    {
        return $this->confPubliciteFrancaise;
    }

    public function setConfPubliciteFrancaise(bool $confPubliciteFrancaise): self
    {
        $this->confPubliciteFrancaise = $confPubliciteFrancaise;

        return $this;
    }

    public function isAuthenticateAgentByInternalKeycloak(): bool
    {
        return $this->authenticateAgentByInternalKeycloak;
    }

    public function setAuthenticateAgentByInternalKeycloak(bool $authenticateAgentByInternalKeycloak): self
    {
        $this->authenticateAgentByInternalKeycloak = $authenticateAgentByInternalKeycloak;

        return $this;
    }
}
