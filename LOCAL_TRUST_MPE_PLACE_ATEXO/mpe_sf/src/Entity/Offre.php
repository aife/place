<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use DateTime;
use DateTimeInterface;
use DateTimeZone;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\OffreInput;
use App\Dto\Output\OffreOutput;
use App\Traits\Entity\PlateformeVirtuelleLinkTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Offre.
 *
 * @ORM\Table(name="Offres",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="untrustedserial", columns={"untrustedserial"})},
 *     indexes={@ORM\Index(name="inscrit_id", columns={"inscrit_id"}),
 *     @ORM\Index(name="consultation_id", columns={"consultation_id"}),
 *     @ORM\Index(name="entreprise_id", columns={"entreprise_id"}),
 *     @ORM\Index(name="Offres_consultation", columns={"organisme", "consultation_id"}),
 *     @ORM\Index(name="candidature_id_externe_idx", columns={"candidature_id_externe"})})
 * @ORM\Entity(repositoryClass="App\Repository\OffreRepository")
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'post',
    ],
    itemOperations: [
        'get',
        'validate_depot' => [
            'method' => 'PATCH',
            'input' => false,
            'path' => '/offres/{id}/validate',
            'openapi_context' => [
                'summary'     => 'Validate the depot of an Offre.',
                'description' => "A depot is related to an Offre. To validate it, we update Offre resource.",
                'requestBody' => [
                    'content' => [
                        'application/merge-patch+json' => [
                            'schema' => []
                        ]
                    ]
                ]
            ]
        ]
    ],
    input: OffreInput::class,
    output: OffreOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'consultation' => 'exact',
    ]
)]
class Offre
{
    use PlateformeVirtuelleLinkTrait;

    public const STATUT_OFFRE_OUVERT = '2';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation",
     *     inversedBy="offres", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Inscrit",
     *     inversedBy="offres", fetch="EAGER",cascade={"persist"})
     * @ORM\JoinColumn(name="inscrit_id", referencedColumnName="id")
     */
    private ?Inscrit $inscrit = null;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Enveloppe",
     *     mappedBy="offre",cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="id", referencedColumnName="offre_id")
     */
    private Collection $enveloppes;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private $organisme = '';

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     */
    private $consultationId = '0';

    /**
     * @ORM\Column(name="entreprise_id", type="integer", nullable=false)
     */
    private $entrepriseId = '0';

    /**
     * @ORM\Column(name="id_etablissement", type="integer", nullable=false)
     */
    private $idEtablissement = null;

    /**
     * @ORM\Column(name="inscrit_id", type="integer", nullable=false)
     */
    private $inscritId = '0';

    /**
     * @ORM\Column(name="signatureenvxml", type="blob", nullable=false)
     */
    private $signatureenvxml = null;

    /**
     * @ORM\Column(name="horodatage", type="blob", nullable=false)
     */
    private $horodatage = null;

    /**
     * @var string
     *
     * @ORM\Column(name="mailsignataire", type="string", length=80, nullable=false)
     */
    private $mailSignataire = '';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="untrusteddate", type="datetime", options={"default":"0000-00-00 00:00:00"})*/
    private $untrusteddate;

    /**
     * @ORM\Column(name="untrustedserial", type="string", length=40, nullable=false)
     */
    private string $untrustedserial = '';

    /**
     * @ORM\Column(name="envoi_complet", type="string", length=1, nullable=false)
     */
    private string $envoiComplet = '';

    /**
     * @ORM\Column(name="date_depot_differe", type="string", nullable=false)
     */
    private $dateDepotDiffere = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="horodatage_envoi_differe", type="blob", nullable=false)
     */
    private $horodatageEnvoiDiffere = null;

    /**
     * @ORM\Column(name="signatureenvxml_envoi_differe", type="blob", nullable=false)
     */
    private $signatureenvxmlEnvoiDiffere = null;

    /**
     * @ORM\Column(name="external_serial", type="string", length=8, nullable=true)
     */
    private $externalSerial = null;

    /**
     * @ORM\Column(name="internal_serial", type="string", length=8, nullable=true)
     */
    private $internalSerial = null;

    /**
     * @ORM\Column(name="uid_offre", type="string", length=40, nullable=false)
     */
    private string $uidOffre = '';

    /**
     * @ORM\Column(name="offre_selectionnee", type="integer", nullable=false)
     */
    private $offreSelectionnee = '0';

    /**
     * @ORM\Column(name="Observation", type="text", length=65535, nullable=true)
     */
    private ?string $observation = null;

    /**
     * @ORM\Column(name="xml_string", type="text", nullable=false)
     */
    private ?string $xmlString = null;

    /**
     * @ORM\Column(name="nom_entreprise_inscrit", type="string", length=30, nullable=true)
     */
    private ?string $nomEntrepriseInscrit = null;

    /**
     * @ORM\Column(name="nom_inscrit", type="string", length=80, nullable=true)
     */
    private ?string $nomInscrit = null;

    /**
     * @ORM\Column(name="prenom_inscrit", type="string", length=80, nullable=true)
     */
    private ?string $prenomInscrit = null;

    /**
     * @ORM\Column(name="adresse_inscrit", type="string", length=100, nullable=true)
     */
    private ?string $adresseInscrit = null;

    /**
     * @ORM\Column(name="adresse2_inscrit", type="string", length=100, nullable=true)
     */
    private ?string $adresse2Inscrit = null;

    /**
     * @ORM\Column(name="telephone_inscrit", type="string", length=20, nullable=true)
     */
    private ?string $telephoneInscrit = null;

    /**
     * @ORM\Column(name="fax_inscrit", type="string", length=30, nullable=true)
     */
    private ?string $faxInscrit = null;

    /**
     * @ORM\Column(name="code_postal_inscrit", type="string", length=5, nullable=true)
     */
    private ?string $codePostalInscrit = null;

    /**
     * @ORM\Column(name="ville_inscrit", type="string", length=50, nullable=true)
     */
    private ?string $villeInscrit = null;

    /**
     * @ORM\Column(name="pays_inscrit", type="string", length=50, nullable=true)
     */
    private ?string $paysInscrit = null;

    /**
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)
     */
    private ?string $acronymePays = null;

    /**
     * @ORM\Column(name="siret_entreprise", type="string", length=14, nullable=true)
     */
    private ?string $siretEntreprise = null;

    /**
     * @ORM\Column(name="identifiant_national", type="string", length=20, nullable=true)
     */
    private ?string $identifiantNational = null;

    /**
     * @ORM\Column(name="email_inscrit", type="string", length=100, nullable=true)
     */
    private ?string $emailInscrit = null;

    /**
     * @ORM\Column(name="siret_inscrit", type="string", length=14, nullable=true)
     */
    private ?string $siretInscrit = null;

    /**
     * @ORM\Column(name="nom_entreprise", type="string", length=30, nullable=true)
     */
    private ?string $nomEntreprise = null;

    /**
     * @ORM\Column(name="horodatage_annulation", type="blob", nullable=true)
     */
    private ?string $horodatageAnnulation = null;

    /**
     * @ORM\Column(name="date_annulation", type="string", length=20, nullable=true)
     */
    private ?string $dateAnnulation = null;

    /**
     * @ORM\Column(name="signature_annulation", type="text", length=65535, nullable=true)
     */
    private ?string $signatureAnnulation = null;

    /**
     * @ORM\Column(name="depot_annule", type="string", nullable=true)
     */
    private ?string $depotAnnule = '0';

    /**
     * @ORM\Column(name="string_annulation", type="text", length=65535, nullable=true)
     */
    private ?string $stringAnnulation = null;

    /**
     * @ORM\Column(name="verification_certificat_annulation", type="string", length=5, nullable=true)
     */
    private ?string $verificationCertificatAnnulation = null;

    /**
     * @ORM\Column(name="offre_variante", type="string", nullable=true)
     */
    private ?string $offreVariante = null;

    /**
     * @ORM\Column(name="reponse_pas_a_pas", type="string", nullable=false)
     */
    private string $reponsePasAPas = '0';

    /**
     * @ORM\Column(name="numero_reponse", type="integer", nullable=false)
     */
    private ?int $numeroReponse = null;

    /**
     * @ORM\Column(name="statut_offres", type="integer", nullable=true)
     */
    private ?int $statutOffres = null;

    /**
     * @ORM\Column(name="date_heure_ouverture", type="string", length=20, nullable=false)
     */
    private string $dateHeureOuverture = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="agentid_ouverture", type="integer", nullable=true)
     */
    private ?int $agentidOuverture = null;

    /**
     * @ORM\Column(name="agentid_ouverture2", type="integer", nullable=true)
     */
    private ?int $agentidOuverture2 = null;

    /**
     * @ORM\Column(name="date_heure_ouverture_agent2", type="string", length=20, nullable=false)
     */
    private string $dateHeureOuvertureAgent2 = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="cryptage_reponse", type="string", length=1, nullable=false)
     */
    private string $cryptageReponse = '1';

    /**
     * @ORM\Column(name="nom_agent_ouverture", type="string", length=100, nullable=true)
     */
    private ?string $nomAgentOuverture = null;

    /**
     * @ORM\Column(name="agent_telechargement_offre", type="integer", nullable=true)
     */
    private ?int $agentTelechargementOffre = null;

    /**
     * @ORM\Column(name="date_telechargement_offre", type="string", length=20, nullable=true)
     */
    private ?string $dateTelechargementOffre = null;

    /**
     * @ORM\Column(name="repertoire_telechargement_offre", type="string", length=100, nullable=true)
     */
    private ?string $repertoireTelechargementOffre = null;

    /**
     * @ORM\Column(name="candidature_id_externe", type="integer", nullable=true)
     */
    private ?int $candidatureIdExterne = null;

    /**
     * @ORM\Column(name="etat_chiffrement", type="integer", nullable=false)
     */
    private int $etatChiffrement = 1;

    /**
     * @ORM\Column(name="erreur_chiffrement", type="text", length=100, nullable=true)
     */
    private ?string $erreurChiffrement = null;

    /**
     * @ORM\Column(name="horodatage_hash_fichiers", type="text", nullable=true)
     */
    private $horodatageHashFichiers;

    /**
     * @ORM\Column(name="id_pdf_echange_accuse", type="integer", nullable=true)
     */
    private $idPdfEchangeAccuse;

    /**
     * @var DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)*/
    private $createdAt;
    /**
     * @ORM\Column(name="uid_response", type="string", length=255, nullable=true)
     */
    private ?string $uidResponse = '';

    /**
     * @var DateTimeInterface
     *
     * @ORM\Column(name="date_depot", type="datetime", nullable=true)*/
    private $dateDepot;

    /**
     * @ORM\Column(name="id_blob_horodatage_hash", type="integer", nullable=true)
     */
    private ?int $idBlobHorodatageHash = null;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\BloborganismeFile",
     *     fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="id_blob_xml_reponse", referencedColumnName="id", nullable=true)
     */
    private $idBlobXmlReponse;

    /**
     * @var integer
     *
     * @ORM\Column(name="taux_production_france", type="integer", nullable=true)
     */
    private $tauxProductionFrance;

    /**
     * @var integer
     *
     * @ORM\Column(name="taux_production_europe", type="integer", nullable=true)
     */
    private $tauxProductionEurope;

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->enveloppes = new ArrayCollection();
        if (empty($this->untrusteddate)) {
            $this->untrusteddate = new DateTime('0000-00-00 00:00:00', new DateTimeZone('Europe/Paris'));
        }
    }

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return Offre
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return Offre
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set consultationId.
     *
     * @param int $consultationId
     *
     * @return Offre
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    /**
     * Get consultationId.
     *
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * Set entrepriseId.
     *
     * @param int $entrepriseId
     *
     * @return Offre
     */
    public function setEntrepriseId($entrepriseId)
    {
        $this->entrepriseId = $entrepriseId;

        return $this;
    }

    /**
     * Get entrepriseId.
     *
     * @return int
     */
    public function getEntrepriseId()
    {
        return $this->entrepriseId;
    }

    /**
     * Set idEtablissement.
     *
     * @param int $idEtablissement
     *
     * @return Offre
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;

        return $this;
    }

    /**
     * Get idEtablissement.
     *
     * @return int
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * Set inscritId.
     *
     * @param int $inscritId
     *
     * @return Offre
     */
    public function setInscritId($inscritId)
    {
        $this->inscritId = $inscritId;

        return $this;
    }

    /**
     * Get inscritId.
     *
     * @return int
     */
    public function getInscritId()
    {
        return $this->inscritId;
    }

    /**
     * Set signatureenvxml.
     *
     * @param string $signatureenvxml
     *
     * @return Offre
     */
    public function setSignatureenvxml($signatureenvxml)
    {
        $this->signatureenvxml = $signatureenvxml;

        return $this;
    }

    /**
     * Get signatureenvxml.
     *
     * @return string
     */
    public function getSignatureenvxml()
    {
        return $this->signatureenvxml;
    }

    /**
     * Set horodatage.
     *
     * @param string $horodatage
     *
     * @return Offre
     */
    public function setHorodatage($horodatage)
    {
        $this->horodatage = $horodatage;

        return $this;
    }

    /**
     * Get horodatage.
     *
     * @return string
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * Set mailsignataire.
     *
     * @param string $mailSignataire
     *
     * @return Offre
     */
    public function setMailSignataire($mailsignataire)
    {
        $this->mailSignataire = $mailsignataire;

        return $this;
    }

    /**
     * Get mailSignataire.
     *
     * @return string
     */
    public function getMailSignataire()
    {
        return $this->mailSignataire;
    }

    /**
     * Set untrusteddate.
     *
     * @param DateTime $untrusteddate
     *
     * @return Offre
     */
    public function setUntrusteddate($untrusteddate)
    {
        $this->untrusteddate = $untrusteddate;

        return $this;
    }

    /**
     * Get untrusteddate.
     *
     * @return DateTime
     */
    public function getUntrusteddate()
    {
        return $this->untrusteddate;
    }

    /**
     * Set untrustedserial.
     *
     * @param string $untrustedserial
     *
     * @return Offre
     */
    public function setUntrustedserial($untrustedserial)
    {
        $this->untrustedserial = $untrustedserial;

        return $this;
    }

    /**
     * Get untrustedserial.
     *
     * @return string
     */
    public function getUntrustedserial()
    {
        return $this->untrustedserial;
    }

    /**
     * Set envoiComplet.
     *
     * @param string $envoiComplet
     *
     * @return Offre
     */
    public function setEnvoiComplet($envoiComplet)
    {
        $this->envoiComplet = $envoiComplet;

        return $this;
    }

    /**
     * Get envoiComplet.
     *
     * @return string
     */
    public function getEnvoiComplet()
    {
        return $this->envoiComplet;
    }

    /**
     * Set dateDepotDiffere.
     *
     * @param string $dateDepotDiffere
     *
     * @return Offre
     */
    public function setDateDepotDiffere($dateDepotDiffere)
    {
        $this->dateDepotDiffere = $dateDepotDiffere;

        return $this;
    }

    /**
     * Get dateDepotDiffere.
     *
     * @return string
     */
    public function getDateDepotDiffere()
    {
        return $this->dateDepotDiffere;
    }

    /**
     * Set horodatageEnvoiDiffere.
     *
     * @param string $horodatageEnvoiDiffere
     *
     * @return Offre
     */
    public function setHorodatageEnvoiDiffere($horodatageEnvoiDiffere)
    {
        $this->horodatageEnvoiDiffere = $horodatageEnvoiDiffere;

        return $this;
    }

    /**
     * Get horodatageEnvoiDiffere.
     *
     * @return string
     */
    public function getHorodatageEnvoiDiffere()
    {
        return $this->horodatageEnvoiDiffere;
    }

    /**
     * Set signatureenvxmlEnvoiDiffere.
     *
     * @param string $signatureenvxmlEnvoiDiffere
     *
     * @return Offre
     */
    public function setSignatureenvxmlEnvoiDiffere($signatureenvxmlEnvoiDiffere)
    {
        $this->signatureenvxmlEnvoiDiffere = $signatureenvxmlEnvoiDiffere;

        return $this;
    }

    /**
     * Get signatureenvxmlEnvoiDiffere.
     *
     * @return string
     */
    public function getSignatureenvxmlEnvoiDiffere()
    {
        return $this->signatureenvxmlEnvoiDiffere;
    }

    /**
     * Set externalSerial.
     *
     * @param string $externalSerial
     *
     * @return Offre
     */
    public function setExternalSerial($externalSerial)
    {
        $this->externalSerial = $externalSerial;

        return $this;
    }

    /**
     * Get externalSerial.
     *
     * @return string
     */
    public function getExternalSerial()
    {
        return $this->externalSerial;
    }

    /**
     * Set internalSerial.
     *
     * @param string $internalSerial
     *
     * @return Offre
     */
    public function setInternalSerial($internalSerial)
    {
        $this->internalSerial = $internalSerial;

        return $this;
    }

    /**
     * Get internalSerial.
     *
     * @return string
     */
    public function getInternalSerial()
    {
        return $this->internalSerial;
    }

    /**
     * Set uidOffre.
     *
     * @param string $uidOffre
     *
     * @return Offre
     */
    public function setUidOffre($uidOffre)
    {
        $this->uidOffre = $uidOffre;

        return $this;
    }

    /**
     * Get uidOffre.
     *
     * @return string
     */
    public function getUidOffre()
    {
        return $this->uidOffre;
    }

    /**
     * Set offreSelectionnee.
     *
     * @param int $offreSelectionnee
     *
     * @return Offre
     */
    public function setOffreSelectionnee($offreSelectionnee)
    {
        $this->offreSelectionnee = $offreSelectionnee;

        return $this;
    }

    /**
     * Get offreSelectionnee.
     *
     * @return int
     */
    public function getOffreSelectionnee()
    {
        return $this->offreSelectionnee;
    }

    /**
     * Set observation.
     *
     * @param string $observation
     *
     * @return Offre
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * Get observation.
     *
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * Set xmlString.
     *
     * @param string $xmlString
     *
     * @return Offre
     */
    public function setXmlString($xmlString)
    {
        $this->xmlString = $xmlString;

        return $this;
    }

    /**
     * Get xmlString.
     *
     * @return string
     */
    public function getXmlString()
    {
        return $this->xmlString;
    }

    /**
     * Set nomEntrepriseInscrit.
     *
     * @param string $nomEntrepriseInscrit
     *
     * @return Offre
     */
    public function setNomEntrepriseInscrit($nomEntrepriseInscrit)
    {
        $this->nomEntrepriseInscrit = $nomEntrepriseInscrit;

        return $this;
    }

    /**
     * Get nomEntrepriseInscrit.
     *
     * @return string
     */
    public function getNomEntrepriseInscrit()
    {
        return $this->nomEntrepriseInscrit;
    }

    /**
     * Set nomInscrit.
     *
     * @param string $nomInscrit
     *
     * @return Offre
     */
    public function setNomInscrit($nomInscrit)
    {
        $this->nomInscrit = $nomInscrit;

        return $this;
    }

    /**
     * Get nomInscrit.
     *
     * @return string
     */
    public function getNomInscrit()
    {
        return $this->nomInscrit;
    }

    /**
     * Set prenomInscrit.
     *
     * @param string $prenomInscrit
     *
     * @return Offre
     */
    public function setPrenomInscrit($prenomInscrit)
    {
        $this->prenomInscrit = $prenomInscrit;

        return $this;
    }

    /**
     * Get prenomInscrit.
     *
     * @return string
     */
    public function getPrenomInscrit()
    {
        return $this->prenomInscrit;
    }

    /**
     * Set adresseInscrit.
     *
     * @param string $adresseInscrit
     *
     * @return Offre
     */
    public function setAdresseInscrit($adresseInscrit)
    {
        $this->adresseInscrit = $adresseInscrit;

        return $this;
    }

    /**
     * Get adresseInscrit.
     *
     * @return string
     */
    public function getAdresseInscrit()
    {
        return $this->adresseInscrit;
    }

    /**
     * Set adresse2Inscrit.
     *
     * @param string $adresse2Inscrit
     *
     * @return Offre
     */
    public function setAdresse2Inscrit($adresse2Inscrit)
    {
        $this->adresse2Inscrit = $adresse2Inscrit;

        return $this;
    }

    /**
     * Get adresse2Inscrit.
     *
     * @return string
     */
    public function getAdresse2Inscrit()
    {
        return $this->adresse2Inscrit;
    }

    /**
     * Set telephoneInscrit.
     *
     * @param string $telephoneInscrit
     *
     * @return Offre
     */
    public function setTelephoneInscrit($telephoneInscrit)
    {
        $this->telephoneInscrit = $telephoneInscrit;

        return $this;
    }

    /**
     * Get telephoneInscrit.
     *
     * @return string
     */
    public function getTelephoneInscrit()
    {
        return $this->telephoneInscrit;
    }

    /**
     * Set faxInscrit.
     *
     * @param string $faxInscrit
     *
     * @return Offre
     */
    public function setFaxInscrit($faxInscrit)
    {
        $this->faxInscrit = $faxInscrit;

        return $this;
    }

    /**
     * Get faxInscrit.
     *
     * @return string
     */
    public function getFaxInscrit()
    {
        return $this->faxInscrit;
    }

    /**
     * Set codePostalInscrit.
     *
     * @param string $codePostalInscrit
     *
     * @return Offre
     */
    public function setCodePostalInscrit($codePostalInscrit)
    {
        $this->codePostalInscrit = $codePostalInscrit;

        return $this;
    }

    /**
     * Get codePostalInscrit.
     *
     * @return string
     */
    public function getCodePostalInscrit()
    {
        return $this->codePostalInscrit;
    }

    /**
     * Set villeInscrit.
     *
     * @param string $villeInscrit
     *
     * @return Offre
     */
    public function setVilleInscrit($villeInscrit)
    {
        $this->villeInscrit = $villeInscrit;

        return $this;
    }

    /**
     * Get villeInscrit.
     *
     * @return string
     */
    public function getVilleInscrit()
    {
        return $this->villeInscrit;
    }

    /**
     * Set paysInscrit.
     *
     * @param string $paysInscrit
     *
     * @return Offre
     */
    public function setPaysInscrit($paysInscrit)
    {
        $this->paysInscrit = $paysInscrit;

        return $this;
    }

    /**
     * Get paysInscrit.
     *
     * @return string
     */
    public function getPaysInscrit()
    {
        return $this->paysInscrit;
    }

    /**
     * Set acronymePays.
     *
     * @param string $acronymePays
     *
     * @return Offre
     */
    public function setAcronymePays($acronymePays)
    {
        $this->acronymePays = $acronymePays;

        return $this;
    }

    /**
     * Get acronymePays.
     *
     * @return string
     */
    public function getAcronymePays()
    {
        return $this->acronymePays;
    }

    /**
     * Set siretEntreprise.
     *
     * @param string $siretEntreprise
     *
     * @return Offre
     */
    public function setSiretEntreprise($siretEntreprise)
    {
        $this->siretEntreprise = $siretEntreprise;

        return $this;
    }

    /**
     * Get siretEntreprise.
     *
     * @return string
     */
    public function getSiretEntreprise()
    {
        return $this->siretEntreprise;
    }

    /**
     * Set identifiantNational.
     *
     * @param string $identifiantNational
     *
     * @return Offre
     */
    public function setIdentifiantNational($identifiantNational)
    {
        $this->identifiantNational = $identifiantNational;

        return $this;
    }

    /**
     * Get identifiantNational.
     *
     * @return string
     */
    public function getIdentifiantNational()
    {
        return $this->identifiantNational;
    }

    /**
     * Set emailInscrit.
     *
     * @param string $emailInscrit
     *
     * @return Offre
     */
    public function setEmailInscrit($emailInscrit)
    {
        $this->emailInscrit = $emailInscrit;

        return $this;
    }

    /**
     * Get emailInscrit.
     *
     * @return string
     */
    public function getEmailInscrit()
    {
        return $this->emailInscrit;
    }

    /**
     * Set siretInscrit.
     *
     * @param string $siretInscrit
     *
     * @return Offre
     */
    public function setSiretInscrit($siretInscrit)
    {
        $this->siretInscrit = $siretInscrit;

        return $this;
    }

    /**
     * Get siretInscrit.
     *
     * @return string
     */
    public function getSiretInscrit()
    {
        return $this->siretInscrit;
    }

    /**
     * Set nomEntreprise.
     *
     * @param string $nomEntreprise
     *
     * @return Offre
     */
    public function setNomEntreprise($nomEntreprise)
    {
        $this->nomEntreprise = $nomEntreprise;

        return $this;
    }

    /**
     * Get nomEntreprise.
     *
     * @return string
     */
    public function getNomEntreprise()
    {
        return $this->nomEntreprise;
    }

    /**
     * Set horodatageAnnulation.
     *
     * @param string $horodatageAnnulation
     *
     * @return Offre
     */
    public function setHorodatageAnnulation($horodatageAnnulation)
    {
        $this->horodatageAnnulation = $horodatageAnnulation;

        return $this;
    }

    /**
     * Get horodatageAnnulation.
     *
     * @return string
     */
    public function getHorodatageAnnulation()
    {
        return $this->horodatageAnnulation;
    }

    /**
     * Set dateAnnulation.
     *
     * @param string $dateAnnulation
     *
     * @return Offre
     */
    public function setDateAnnulation($dateAnnulation)
    {
        $this->dateAnnulation = $dateAnnulation;

        return $this;
    }

    /**
     * Get dateAnnulation.
     *
     * @return string
     */
    public function getDateAnnulation()
    {
        return $this->dateAnnulation;
    }

    /**
     * Set signatureAnnulation.
     *
     * @param string $signatureAnnulation
     *
     * @return Offre
     */
    public function setSignatureAnnulation($signatureAnnulation)
    {
        $this->signatureAnnulation = $signatureAnnulation;

        return $this;
    }

    /**
     * Get signatureAnnulation.
     *
     * @return string
     */
    public function getSignatureAnnulation()
    {
        return $this->signatureAnnulation;
    }

    /**
     * Set depotAnnule.
     *
     * @param string $depotAnnule
     *
     * @return Offre
     */
    public function setDepotAnnule($depotAnnule)
    {
        $this->depotAnnule = $depotAnnule;

        return $this;
    }

    /**
     * Get depotAnnule.
     *
     * @return string
     */
    public function getDepotAnnule()
    {
        return $this->depotAnnule;
    }

    /**
     * Set stringAnnulation.
     *
     * @param string $stringAnnulation
     *
     * @return Offre
     */
    public function setStringAnnulation($stringAnnulation)
    {
        $this->stringAnnulation = $stringAnnulation;

        return $this;
    }

    /**
     * Get stringAnnulation.
     *
     * @return string
     */
    public function getStringAnnulation()
    {
        return $this->stringAnnulation;
    }

    /**
     * Set verificationCertificatAnnulation.
     *
     * @param string $verificationCertificatAnnulation
     *
     * @return Offre
     */
    public function setVerificationCertificatAnnulation($verificationCertificatAnnulation)
    {
        $this->verificationCertificatAnnulation = $verificationCertificatAnnulation;

        return $this;
    }

    /**
     * Get verificationCertificatAnnulation.
     *
     * @return string
     */
    public function getVerificationCertificatAnnulation()
    {
        return $this->verificationCertificatAnnulation;
    }

    /**
     * Set offreVariante.
     *
     * @param string $offreVariante
     *
     * @return Offre
     */
    public function setOffreVariante($offreVariante)
    {
        $this->offreVariante = $offreVariante;

        return $this;
    }

    /**
     * Get offreVariante.
     *
     * @return string
     */
    public function getOffreVariante()
    {
        return $this->offreVariante;
    }

    /**
     * Set reponsePasAPas.
     *
     * @param string $reponsePasAPas
     *
     * @return Offre
     */
    public function setReponsePasAPas($reponsePasAPas)
    {
        $this->reponsePasAPas = $reponsePasAPas;

        return $this;
    }

    /**
     * Get reponsePasAPas.
     *
     * @return string
     */
    public function getReponsePasAPas()
    {
        return $this->reponsePasAPas;
    }

    /**
     * Set numeroReponse.
     *
     * @param int $numeroReponse
     *
     * @return Offre
     */
    public function setNumeroReponse($numeroReponse)
    {
        $this->numeroReponse = $numeroReponse;

        return $this;
    }

    /**
     * Get numeroReponse.
     *
     * @return int
     */
    public function getNumeroReponse()
    {
        return $this->numeroReponse;
    }

    /**
     * Set statutOffres.
     *
     * @param int $statutOffres
     *
     * @return Offre
     */
    public function setStatutOffres($statutOffres)
    {
        $this->statutOffres = $statutOffres;
        $enveloppes = $this->getEnveloppes();
        foreach ($enveloppes as $enveloppe) {
            if ($enveloppe instanceof Enveloppe) {
                $enveloppe->setStatutEnveloppe($statutOffres);
            }
        }

        return $this;
    }

    /**
     * Get statutOffres.
     *
     * @return int
     */
    public function getStatutOffres()
    {
        return $this->statutOffres;
    }

    /**
     * Set dateHeureOuverture.
     *
     * @param string $dateHeureOuverture
     *
     * @return Offre
     */
    public function setDateHeureOuverture($dateHeureOuverture)
    {
        $this->dateHeureOuverture = $dateHeureOuverture;

        return $this;
    }

    /**
     * Get dateHeureOuverture.
     *
     * @return string
     */
    public function getDateHeureOuverture()
    {
        return $this->dateHeureOuverture;
    }

    /**
     * Set agentidOuverture.
     *
     * @param int $agentidOuverture
     *
     * @return Offre
     */
    public function setAgentidOuverture($agentidOuverture)
    {
        $this->agentidOuverture = $agentidOuverture;

        return $this;
    }

    /**
     * Get agentidOuverture.
     *
     * @return int
     */
    public function getAgentidOuverture()
    {
        return $this->agentidOuverture;
    }

    /**
     * Set agentidOuverture2.
     *
     * @param int $agentidOuverture2
     *
     * @return Offre
     */
    public function setAgentidOuverture2($agentidOuverture2)
    {
        $this->agentidOuverture2 = $agentidOuverture2;

        return $this;
    }

    /**
     * Get agentidOuverture2.
     *
     * @return int
     */
    public function getAgentidOuverture2()
    {
        return $this->agentidOuverture2;
    }

    /**
     * Set dateHeureOuvertureAgent2.
     *
     * @param string $dateHeureOuvertureAgent2
     *
     * @return Offre
     */
    public function setDateHeureOuvertureAgent2($dateHeureOuvertureAgent2)
    {
        $this->dateHeureOuvertureAgent2 = $dateHeureOuvertureAgent2;

        return $this;
    }

    /**
     * Get dateHeureOuvertureAgent2.
     *
     * @return string
     */
    public function getDateHeureOuvertureAgent2()
    {
        return $this->dateHeureOuvertureAgent2;
    }

    /**
     * Set cryptageReponse.
     *
     * @param string $cryptageReponse
     *
     * @return Offre
     */
    public function setCryptageReponse($cryptageReponse)
    {
        $this->cryptageReponse = $cryptageReponse;

        return $this;
    }

    /**
     * Get cryptageReponse.
     *
     * @return string
     */
    public function getCryptageReponse()
    {
        return $this->cryptageReponse;
    }

    /**
     * Set nomAgentOuverture.
     *
     * @param string $nomAgentOuverture
     *
     * @return Offre
     */
    public function setNomAgentOuverture($nomAgentOuverture)
    {
        $this->nomAgentOuverture = $nomAgentOuverture;

        return $this;
    }

    /**
     * Get nomAgentOuverture.
     *
     * @return string
     */
    public function getNomAgentOuverture()
    {
        return $this->nomAgentOuverture;
    }

    /**
     * Set agentTelechargementOffre.
     *
     * @param int $agentTelechargementOffre
     *
     * @return Offre
     */
    public function setAgentTelechargementOffre($agentTelechargementOffre)
    {
        $this->agentTelechargementOffre = $agentTelechargementOffre;

        return $this;
    }

    /**
     * Get agentTelechargementOffre.
     *
     * @return int
     */
    public function getAgentTelechargementOffre()
    {
        return $this->agentTelechargementOffre;
    }

    /**
     * Set dateTelechargementOffre.
     *
     * @param string $dateTelechargementOffre
     *
     * @return Offre
     */
    public function setDateTelechargementOffre($dateTelechargementOffre)
    {
        $this->dateTelechargementOffre = $dateTelechargementOffre;

        return $this;
    }

    /**
     * Get dateTelechargementOffre.
     *
     * @return string
     */
    public function getDateTelechargementOffre()
    {
        return $this->dateTelechargementOffre;
    }

    /**
     * Set repertoireTelechargementOffre.
     *
     * @param string $repertoireTelechargementOffre
     *
     * @return Offre
     */
    public function setRepertoireTelechargementOffre($repertoireTelechargementOffre)
    {
        $this->repertoireTelechargementOffre = $repertoireTelechargementOffre;

        return $this;
    }

    /**
     * Get repertoireTelechargementOffre.
     *
     * @return string
     */
    public function getRepertoireTelechargementOffre()
    {
        return $this->repertoireTelechargementOffre;
    }

    /**
     * Set candidatureIdExterne.
     *
     * @param int $candidatureIdExterne
     *
     * @return Offre
     */
    public function setCandidatureIdExterne($candidatureIdExterne)
    {
        $this->candidatureIdExterne = $candidatureIdExterne;

        return $this;
    }

    /**
     * Get candidatureIdExterne.
     *
     * @return int
     */
    public function getCandidatureIdExterne()
    {
        return $this->candidatureIdExterne;
    }

    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;
        $this->organisme = $consultation->getAcronymeOrg();
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @return $this
     */
    public function addEnveloppe(Enveloppe $enveloppe)
    {
        $this->enveloppes[] = $enveloppe;
        $enveloppe->setOffre($this);

        return $this;
    }

    public function removeEnveloppe(Enveloppe $enveloppe)
    {
        $this->enveloppes->removeElement($enveloppe);
    }

    /**
     * @return ArrayCollection
     */
    public function getEnveloppes()
    {
        return $this->enveloppes;
    }

    public function getErreurChiffrement(): string
    {
        return $this->erreurChiffrement;
    }

    public function setErreurChiffrement(string $erreurChiffrement): void
    {
        $this->erreurChiffrement = $erreurChiffrement;
    }

    /**
     * @return int
     */
    public function getEtatChiffrement()
    {
        return $this->etatChiffrement;
    }

    /**
     * @return mixed
     */
    public function getHorodatageHashFichiers()
    {
        return $this->horodatageHashFichiers;
    }

    /**
     * @param mixed $horodatageHashFichiers
     */
    public function setHorodatageHashFichiers($horodatageHashFichiers)
    {
        $this->horodatageHashFichiers = $horodatageHashFichiers;
    }

    /**
     * @param int $etatChiffrement
     * @return Offre
     */
    public function setEtatChiffrement($etatChiffrement)
    {
        $this->etatChiffrement = $etatChiffrement;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdPdfEchangeAccuse()
    {
        return $this->idPdfEchangeAccuse;
    }

    /**
     * @param mixed $idPdfEchangeAccuse
     */
    public function setIdPdfEchangeAccuse($idPdfEchangeAccuse)
    {
        $this->idPdfEchangeAccuse = $idPdfEchangeAccuse;
    }

    public function setInscrit(Inscrit $inscrit)
    {
        $this->inscrit = $inscrit;
        //$this->inscritId = $inscrit->getId();
        $this->setNomInscrit($inscrit->getNom());
        $this->setPrenomInscrit($inscrit->getPrenom());
        $this->setAdresseInscrit($inscrit->getAdresse());
        $this->setAdresse2Inscrit($inscrit->getAdresse2());
        $this->setTelephoneInscrit($inscrit->getTelephone());
        $this->setFaxInscrit($inscrit->getFax());
        $this->setCodePostalInscrit($inscrit->getCodepostal());
        $this->setVilleInscrit($inscrit->getVille());
        $this->setPaysInscrit($inscrit->getPays());
        $this->setEmailInscrit($inscrit->getEmail());
        $this->setMailSignataire($inscrit->getEmail());

        $this->entrepriseId = $inscrit->getEntrepriseId();
        $this->idEtablissement = $inscrit->getIdEtablissement();

        $entreprise = $inscrit->getEntreprise();
        if ($entreprise instanceof Entreprise) {
            $this->setAcronymePays($entreprise->getAcronymePays());
            $this->setNomEntrepriseInscrit($entreprise->getNom());
            $etablissement = $inscrit->getEtablissement();
            $siretInscrit = ($etablissement instanceof Etablissement) ? $entreprise->getSiren() . $inscrit
                    ->getEtablissement()
                    ->getCodeEtablissement() : $entreprise->getSiren() . $entreprise->getNicSiege();
            $this->setSiretInscrit($siretInscrit);
            $this->setSiretEntreprise($entreprise->getSiren() . $entreprise->getNicSiege());
            $this->setIdentifiantNational($entreprise->getSirenEtranger());
        }
    }

    /**
     * @return Inscrit
     */
    public function getInscrit()
    {
        return $this->inscrit;
    }

    /**
     * Vérifie si l'enveloppe candidature a été renseigné par l'entreprise
     * lors de sa réponse.
     *
     * @return bool
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     */
    public function hasEnveloppeCandidature($typeEnvCandValue)
    {
        if ($this->consultation->getEnvCandidature()) {
            foreach ($this->enveloppes as $enveloppe) {
                if ($typeEnvCandValue == $enveloppe->getTypeEnv()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getUidResponse()
    {
        return $this->uidResponse;
    }

    /**
     * @param string $uidResponse
     * @return Offre
     */
    public function setUidResponse($uidResponse)
    {
        $this->uidResponse = $uidResponse;

        return $this;
    }

    public function getDateDepot(): ?DateTimeInterface
    {
        return $this->dateDepot;
    }

    /**
     * @param string $dateDepot
     */
    public function setDateDepot($dateDepot)
    {
        $this->dateDepot = $dateDepot;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getIdBlobHorodatageHash()
    {
        return $this->idBlobHorodatageHash;
    }

    /**
     * @param int $idBlobHorodatageHash
     */
    public function setIdBlobHorodatageHash($idBlobHorodatageHash)
    {
        $this->idBlobHorodatageHash = $idBlobHorodatageHash;
    }

    /**
     * @return int
     */
    public function getIdBlobXmlReponse()
    {
        return $this->idBlobXmlReponse;
    }

    /**
     * @param int $idBlobXmlReponse
     */
    public function setIdBlobXmlReponse($idBlobXmlReponse)
    {
        $this->idBlobXmlReponse = $idBlobXmlReponse;
    }

    /**
     * Vérifie si une enveloppe offre a été renseigné par l'entreprise
     * lors de sa réponse.
     *
     * @return bool
     *
     * @author Salwa DRISSI <salwa.drissi@atexo.com>
     */
    public function hasEnveloppeOffre($typeEnvOffreValue)
    {
        if ($this->consultation->getEnvOffre()) {
            foreach ($this->enveloppes as $enveloppe) {
                if ($typeEnvOffreValue == $enveloppe->getTypeEnv()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return int|null
     */
    public function getTauxProductionFrance(): ?int
    {
        return $this->tauxProductionFrance;
    }

    /**
     * @param int|null $tauxProductionFrance
     * @return $this
     */
    public function setTauxProductionFrance(?int $tauxProductionFrance): self
    {
        $this->tauxProductionFrance = $tauxProductionFrance;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTauxProductionEurope(): ?int
    {
        return $this->tauxProductionEurope;
    }

    /**
     * @param int|null $tauxProductionEurope
     * @return $this
     */
    public function setTauxProductionEurope(?int $tauxProductionEurope): self
    {
        $this->tauxProductionEurope = $tauxProductionEurope;
        return $this;
    }
}
