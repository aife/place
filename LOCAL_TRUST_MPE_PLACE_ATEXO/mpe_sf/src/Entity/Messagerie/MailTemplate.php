<?php

namespace App\Entity\Messagerie;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * MailTemplate.
 *
 * @ORM\Table(name="mail_template")
 * @ORM\Entity(repositoryClass="App\Repository\Messagerie\MailTemplateRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MailTemplate
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")*/
    #[Groups('webservice')]
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Messagerie\MailType")
     * @ORM\JoinColumn(nullable=false)*/
    #[Groups('webservice')]
    private ?MailType $mailType = null;

    /**
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private ?string $code = null;

    /**
     *
     * @ORM\Column(name="objet", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private ?string $objet = null;

    /**
     *
     * @ORM\Column(name="corps", type="text", nullable=false)*/
    #[Groups('webservice')]
    private ?string $corps = null;

    /**
     *
     * @ORM\Column(name="ordre_affichage", type="integer", nullable=true)*/
    #[Groups('webservice')]
    private ?int $ordreAffichage = null;

    /**
     *
     * @ORM\Column(name="envoi_modalite", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private string $envoiModalite = 'AVEC_AR';

    /**
     * @var bool
     *
     * @ORM\Column(name="envoi_modalite_figee", type="boolean", nullable=false)*/
    #[Groups('webservice')]
    private $envoiModaliteFigee = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="reponse_attendue", type="boolean", nullable=false)*/
    #[Groups('webservice')]
    private $reponseAttendue = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="reponse_attendue_figee", type="boolean", nullable=false)*/
    #[Groups('webservice')]
    private $reponseAttendueFigee = false;

    /**
     * @ORM\Column(type="boolean")
     */
    #[Groups('webservice')]
    private $reponseAgentAttendue;

    public function getId(): int
    {
        return $this->id;
    }

    public function setMailType(MailType $mailType): MailTemplate
    {
        $this->mailType = $mailType;

        return $this;
    }

    public function getMailType(): MailType
    {
        return $this->mailType;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;
    }

    public function getCorps(): string
    {
        return $this->corps;
    }

    public function setCorps(string $corps)
    {
        $this->corps = $corps;
    }

    public function getOrdreAffichage(): int
    {
        return $this->ordreAffichage;
    }

    public function setOrdreAffichage(int $ordreAffichage)
    {
        $this->ordreAffichage = $ordreAffichage;
    }

    public function getEnvoiModalite(): string
    {
        return $this->envoiModalite;
    }

    public function setEnvoiModalite(string $envoiModalite)
    {
        $this->envoiModalite = $envoiModalite;
    }

    public function isEnvoiModaliteFigee(): bool
    {
        return $this->envoiModaliteFigee;
    }

    public function setEnvoiModaliteFigee(bool $envoiModaliteFigee)
    {
        $this->envoiModaliteFigee = $envoiModaliteFigee;
    }

    public function isReponseAttendue(): bool
    {
        return $this->reponseAttendue;
    }

    public function setReponseAttendue(bool $reponseAttendue): MailTemplate
    {
        $this->reponseAttendue = $reponseAttendue;

        return $this;
    }

    public function isReponseAttendueFigee(): bool
    {
        return $this->reponseAttendueFigee;
    }

    public function setReponseAttendueFigee(bool $reponseAttendueFigee): MailTemplate
    {
        $this->reponseAttendueFigee = $reponseAttendueFigee;

        return $this;
    }

    public function getObjet(): string
    {
        return $this->objet;
    }

    public function setObjet(string $objet)
    {
        $this->objet = $objet;
    }

    public function getReponseAgentAttendue(): ?bool
    {
        return $this->reponseAgentAttendue;
    }

    public function setReponseAgentAttendue(bool $reponseAgentAttendue): self
    {
        $this->reponseAgentAttendue = $reponseAgentAttendue;

        return $this;
    }
}
