<?php

namespace App\Entity\Messagerie;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * MailTypeGroup.
 *
 * @ORM\Table(name="mail_type_group")
 * @ORM\Entity(repositoryClass="App\Repository\Messagerie\MailTypeGroupRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MailTypeGroup
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private ?string $label = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }
}
