<?php

namespace App\Entity\Messagerie;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * MailType.
 *
 * @ORM\Table(name="mail_type")
 * @ORM\Entity(repositoryClass="App\Repository\Messagerie\MailTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class MailType
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private ?string $label = null;

    /**
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=false)*/
    #[Groups('webservice')]
    private ?string $code = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Messagerie\MailTypeGroup")
     * @ORM\JoinColumn(nullable=false)*/
    #[Groups('webservice')]
    private ?MailTypeGroup $mailTypeGroup = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setMailTypeGroup(MailTypeGroup $mailTypeGroup): MailType
    {
        $this->mailTypeGroup = $mailTypeGroup;

        return $this;
    }

    public function getMailTypeGroup(): MailTypeGroup
    {
        return $this->mailTypeGroup;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code)
    {
        $this->code = $code;
    }
}
