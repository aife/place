<?php

namespace App\Entity;

use App\Repository\TechniqueAchatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TechniqueAchatRepository::class)
 */
class TechniqueAchat
{
    /**
     * @ORM\Column(name="id_technique_achat", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private int $id;

    /**
     * @ORM\Column(name="libelle", type="string", length=100)
     */
    private string $libelle;

    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private string $code;

    /**
     * @ORM\OneToMany(targetEntity=TypeContrat::class, mappedBy="techniqueAchat")
     */
    private Collection $typeContrats;

    public function __construct()
    {
        $this->typeContrats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Collection|TypeContrat[]
     */
    public function getTypeContrats(): Collection
    {
        return $this->typeContrats;
    }

    public function addTypeContrat(TypeContrat $typeContrat): self
    {
        if (!$this->typeContrats->contains($typeContrat)) {
            $this->typeContrats[] = $typeContrat;
            $typeContrat->setTechniqueAchat($this);
        }

        return $this;
    }

    public function removeTypeContrat(TypeContrat $typeContrat): self
    {
        if ($this->typeContrats->removeElement($typeContrat)) {
            // set the owning side to null (unless already changed)
            if ($typeContrat->getTechniqueAchat() === $this) {
                $typeContrat->setTechniqueAchat(null);
            }
        }

        return $this;
    }
}
