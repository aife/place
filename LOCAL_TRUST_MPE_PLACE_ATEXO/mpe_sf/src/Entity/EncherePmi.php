<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Encherepmi
 *
 * @ORM\Table(name="EncherePmi", uniqueConstraints={@ORM\UniqueConstraint(name="referenceUtilisateur", columns={"referenceUtilisateur"})}, indexes={@ORM\Index(name="refConsultation", columns={"refConsultation"}), @ORM\Index(name="idEntiteeAssociee", columns={"old_service_id"}), @ORM\Index(name="FK_B6D793AFED5CA9E6", columns={"service_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\EncherePmiRepository")
 */
class EncherePmi
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private string $organisme;

    /**
     * @var int|null
     *
     * @ORM\Column(name="refConsultation", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $refConsultation = null;

    /**
     * @ORM\Column(name="referenceUtilisateur", type="string", length=45, nullable=true)
     */
    private ?string $referenceUtilisateur = null;

    /**
     * @ORM\Column(name="auteur", type="string", length=256, nullable=true)
     */
    private ?string $auteur = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="idLot", type="integer", nullable=true, options={"unsigned"=true})
     */
    private ?int $idLot = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="objet", type="text", length=0, nullable=true)
     */
    private ?string $objet = null;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="dateDebut", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $datedebut = '0000-00-00 00:00:00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateFin", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $datefin = '0000-00-00 00:00:00';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateSuspension", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $datesuspension = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="delaiProlongation", type="integer", nullable=true)
     */
    private ?int $delaiProlongation = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="commentaire", type="text", length=0, nullable=true)
     */
    private ?string $commentaire = null;

    /**
     * @ORM\Column(name="meilleureEnchereObligatoire", type="string", options={"default"="0"}, nullable=false)
     */
    private string $meilleureenchereobligatoire = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="typeBaremeNETC", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $typebaremenetc = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="typeBaremeEnchereGlobale", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $typebaremeenchereglobale = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="meilleurNoteHaute", type="string", length=0, options={"default"="0"}, nullable=false)
     */
    private string $meilleurnotehaute = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="nbrCandidatsVisible", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $nbrcandidatsvisible = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="listeCandidatsVisible", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $listecandidatsvisible = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="rangVisible", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $rangvisible = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="meilleureOffreVisible", type="string", length=0, nullable=false, options={"default"="1"})
     */
    private string $meilleureoffrevisible = '1';

    /**
     * @var float|null
     *
     * @ORM\Column(name="montantReserve", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $montantReserve = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="noteMaxBaremeRelatif", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $noteMaxBaremeRelatif = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="coeffA", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $coeffA = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="coeffB", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $coeffB = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="coeffC", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $coeffC = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mail", type="text", length=0, nullable=true)
     */
    private ?string $mail = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="consultation_id", type="integer", nullable=true)
     */
    private ?int $consultationId = null;

    /**
     * @var Service|null
     *
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     * })
     */
    private ?Service $service = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private $oldId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_service_id", type="integer", nullable=true)
     */
    private $oldServiceId;


    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getRefConsultation(): ?int
    {
        return $this->refConsultation;
    }

    public function setRefConsultation(?int $refConsultation): self
    {
        $this->refConsultation = $refConsultation;

        return $this;
    }

    public function getReferenceUtilisateur(): ?string
    {
        return $this->referenceUtilisateur;
    }

    public function setReferenceUtilisateur(?string $referenceUtilisateur): self
    {
        $this->referenceUtilisateur = $referenceUtilisateur;

        return $this;
    }

    public function getAuteur(): ?string
    {
        return $this->auteur;
    }

    public function setAuteur(?string $auteur): self
    {
        $this->auteur = $auteur;

        return $this;
    }

    public function getIdLot(): ?int
    {
        return $this->idLot;
    }

    public function setIdLot(?int $idLot): self
    {
        $this->idLot = $idLot;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(?string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * @return datetime|null
     */
    public function getDateDebut(): ?DateTime
    {
        return $this->datedebut;
    }

    public function setDateDebut(?Datetime $dateDebut): self
    {
        $this->datedebut = $dateDebut;

        return $this;
    }

    /**
     * @return datetime|null
     */
    public function getDateFin(): ?DateTime
    {
        return $this->datefin;
    }

    public function setDateFin(?Datetime $dateFin): self
    {
        $this->datefin = $dateFin;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDateSuspension(): ?DateTime
    {
        return $this->datesuspension;
    }

    public function setDateSuspension(?Datetime $dateSuspension): self
    {
        $this->datesuspension = $dateSuspension;

        return $this;
    }

    public function getDelaiProlongation(): ?int
    {
        return $this->delaiProlongation;
    }

    public function setDelaiProlongation(?int $delaiProlongation): self
    {
        $this->delaiProlongation = $delaiProlongation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    /**
     * @param string|null $commentaire
     */
    public function setCommentaire(string $commentaire = null): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getMeilleureEnchereObligatoire(): string
    {
        return $this->meilleureenchereobligatoire;
    }

    public function setMeilleureEnchereObligatoire(string $meilleureEnchereObligatoire): self
    {
        $this->meilleureenchereobligatoire = $meilleureEnchereObligatoire;

        return $this;
    }

    public function getTypeBaremeNETC(): string
    {
        return $this->typebaremenetc;
    }

    public function setTypeBaremeNETC(string $typeBaremeNETC): self
    {
        $this->typebaremenetc = $typeBaremeNETC;

        return $this;
    }

    public function getTypeBaremeEnchereGlobale(): string
    {
        return $this->typebaremeenchereglobale;
    }

    public function setTypeBaremeEnchereGlobale(string $typeBaremeEnchereGlobale): self
    {
        $this->typebaremeenchereglobale = $typeBaremeEnchereGlobale;

        return $this;
    }

    public function getMeilleurNoteHaute(): string
    {
        return $this->meilleurnotehaute;
    }

    public function setMeilleurNoteHaute(string $meilleurNoteHaute): self
    {
        $this->meilleurnotehaute = $meilleurNoteHaute;

        return $this;
    }

    public function getNbrCandidatsVisible(): string
    {
        return $this->nbrcandidatsvisible;
    }

    public function setNbrCandidatsVisible(string $nbrCandidatsVisible): self
    {
        $this->nbrcandidatsvisible = $nbrCandidatsVisible;

        return $this;
    }

    public function getListeCandidatsVisible(): string
    {
        return $this->listecandidatsvisible;
    }

    public function setListeCandidatsVisible(string $listeCandidatsVisible): self
    {
        $this->listecandidatsvisible = $listeCandidatsVisible;

        return $this;
    }

    public function getRangVisible(): string
    {
        return $this->rangvisible;
    }

    public function setRangVisible(string $rangVisible): self
    {
        $this->rangvisible = $rangVisible;

        return $this;
    }

    public function getMeilleureOffreVisible(): string
    {
        return $this->meilleureoffrevisible;
    }

    public function setMeilleureOffreVisible(string $meilleureOffreVisible): self
    {
        $this->meilleureoffrevisible = $meilleureOffreVisible;

        return $this;
    }

    public function getMontantReserve(): ?float
    {
        return $this->montantReserve;
    }

    public function setMontantReserve(?float $montantReserve): self
    {
        $this->montantReserve = $montantReserve;

        return $this;
    }

    public function getNoteMaxBaremeRelatif(): ?float
    {
        return $this->noteMaxBaremeRelatif;
    }

    public function setNoteMaxBaremeRelatif(?float $noteMaxBaremeRelatif): self
    {
        $this->noteMaxBaremeRelatif = $noteMaxBaremeRelatif;

        return $this;
    }

    public function getCoeffA(): ?float
    {
        return $this->coeffA;
    }

    public function setCoeffA(?float $coeffA): self
    {
        $this->coeffA = $coeffA;

        return $this;
    }

    public function getCoeffB(): ?float
    {
        return $this->coeffB;
    }

    public function setCoeffB(?float $coeffB): self
    {
        $this->coeffB = $coeffB;

        return $this;
    }

    public function getCoeffC(): ?float
    {
        return $this->coeffC;
    }

    public function setCoeffC(?float $coeffC): self
    {
        $this->coeffC = $coeffC;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(?string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getConsultationId(): ?int
    {
        return $this->consultationId;
    }

    public function setConsultationId(?int $consultationId): self
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    public function setService(?Service $service): self
    {
        $this->service = $service;

        return $this;
    }
}
