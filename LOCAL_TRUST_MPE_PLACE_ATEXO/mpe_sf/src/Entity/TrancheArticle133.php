<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\TrancheArticle133Output;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Repository\TrancheArticle133Repository;

/**
 * TrancheArticle133
 *
 * @ORM\Table(name="Tranche_Article_133" )
 * @ORM\Entity(repositoryClass="App\Repository\TrancheArticle133Repository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: TrancheArticle133Output::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'id' => 'exact',
        'acronymeOrg' => 'exact',
        'LibelleTrancheBudgetaire' => 'partial',
    ]
)]
class TrancheArticle133
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=30)
     */
    private $acronymeOrg;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=10)
     */
    private $millesime;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=250)
     */
    private $LibelleTrancheBudgetaire;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=40)
     */
    private $borneInf;

    /**
     * @var string
     *
     * @ORM\Column(type="text", length=40)
     */
    private $borneSup;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAcronymeOrg(): string
    {
        return $this->acronymeOrg;
    }

    /**
     * @param string $acronymeOrg
     * @return $this
     */
    public function setAcronymeOrg(string $acronymeOrg): self
    {
        $this->acronymeOrg = $acronymeOrg;
        return $this;
    }

    /**
     * @return string
     */
    public function getMillesime(): string
    {
        return $this->millesime;
    }

    /**
     * @param string $millesime
     * @return $this
     */
    public function setMillesime(string $millesime): self
    {
        $this->millesime = $millesime;
        return $this;
    }

    /**
     * @return string
     */
    public function getLibelleTrancheBudgetaire(): string
    {
        return $this->LibelleTrancheBudgetaire;
    }

    /**
     * @param string $LibelleTrancheBudgetaire
     * @return $this
     */
    public function setLibelleTrancheBudgetaire(string $LibelleTrancheBudgetaire): self
    {
        $this->LibelleTrancheBudgetaire = $LibelleTrancheBudgetaire;
        return $this;
    }

    /**
     * @return string
     */
    public function getBorneInf(): string
    {
        return $this->borneInf;
    }

    /**
     * @param string $borneInf
     * @return $this
     */
    public function setBorneInf(string $borneInf): self
    {
        $this->borneInf = $borneInf;
        return $this;
    }

    /**
     * @return string
     */
    public function getBorneSup(): string
    {
        return $this->borneSup;
    }

    /**
     * @param string $borneSup
     * @return $this
     */
    public function setBorneSup(string $borneSup): self
    {
        $this->borneSup = $borneSup;
        return $this;
    }

}