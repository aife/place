<?php

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PieceGenereConsultationRepository")
 */
class PieceGenereConsultation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BloborganismeFile", cascade={"persist", "remove" })
     * @ORM\JoinColumn(name="blob_id", referencedColumnName="id")
     */
    private ?BloborganismeFile $blobId = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="piecesGenere")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?DateTimeInterface $updatedAt = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Lot", inversedBy="pieceGeneree")
     * @ORM\JoinColumn(name="lot_id", referencedColumnName="id")
     */
    private ?Lot $lot = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Entreprise", inversedBy="pieceGeneree")
     * @ORM\JoinColumn(name="entreprise_id", referencedColumnName="id")
     */
    private ?Entreprise $entreprise = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $token = null;

    public function __construct()
    {
        $this->setUpdatedAt(new DateTime('now'));
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlobId(): ?BloborganismeFile
    {
        return $this->blobId;
    }

    public function setBlobId(?BloborganismeFile $blobId): self
    {
        $this->blobId = $blobId;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(?string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getLot(): ?Lot
    {
        return $this->lot;
    }

    public function setLot(?Lot $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getEntreprise(): ?Entreprise
    {
        return $this->entreprise;
    }

    public function setEntreprise(?Entreprise $entreprise): self
    {
        $this->entreprise = $entreprise;

        return $this;
    }
}
