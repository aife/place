<?php

namespace App\Entity\Partenaire;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Partenaire.
 *
 * @ORM\Table(name="Partenaire")
 * @ORM\Entity(repositoryClass="App\Repository\Partenaire\PartenaireRepository")
 */
class Partenaire
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue()
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="initials", type="string", length=30, nullable=false)
     */
    private ?string $initials = null;

    /**
     * @ORM\Column(name="desc_partenaire", type="text", nullable=false)
     */
    private ?string $descPartenaire = null;

    /**
     * @ORM\Column(name="desc_partenaire_fr", type="text", nullable=true)
     */
    private ?string $descPartenaireFr = null;

    /**
     * @ORM\Column(name="desc_partenaire_en", type="text", nullable=true)
     */
    private ?string $descPartenaireEn = null;

    /**
     * @ORM\Column(name="desc_partenaire_es", type="text", nullable=true)
     */
    private ?string $descPartenaireEs = null;

    /**
     * @ORM\Column(name="desc_partenaire_it", type="text", nullable=true)
     */
    private ?string $descPartenaireIt = null;

    /**
     * @ORM\Column(name="desc_partenaire_ar", type="text", nullable=true)
     */
    private ?string $descPartenaireAr = null;

    /**
     * @ORM\Column(name="desc_partenaire_su", type="text", nullable=true)
     */
    private ?string $descPartenaireSu = null;

    /**
     * @ORM\Column(name="lien_img", type="text", nullable=false)
     */
    private ?string $lienImg = null;

    /**
     * @ORM\Column(name="lien_externe", type="text", nullable=false)
     */
    private ?string $lienExterne = null;

    /**
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private ?string $title = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getInitials(): ?string
    {
        return $this->initials;
    }

    public function setInitials(string $initials): self
    {
        $this->initials = $initials;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLienExterne(): ?string
    {
        return $this->lienExterne;
    }

    public function setLienExterne(string $lienExterne): self
    {
        $this->lienExterne = $lienExterne;

        return $this;
    }

    public function getLienImg(): ?string
    {
        return $this->lienImg;
    }

    public function setLienImg(string $lienImg): self
    {
        $this->lienImg = $lienImg;

        return $this;
    }

    public function getDescPartenaireSu(): ?string
    {
        return $this->descPartenaireSu;
    }

    public function setDescPartenaireSu(string $descPartenaireSu): self
    {
        $this->descPartenaireSu = $descPartenaireSu;

        return $this;
    }

    public function getDescPartenaireAr(): ?string
    {
        return $this->descPartenaireAr;
    }

    public function setDescPartenaireAr(string $descPartenaireAr): self
    {
        $this->descPartenaireAr = $descPartenaireAr;

        return $this;
    }

    public function getDescPartenaireIt(): ?string
    {
        return $this->descPartenaireIt;
    }

    public function setDescPartenaireIt(string $descPartenaireIt): self
    {
        $this->descPartenaireIt = $descPartenaireIt;

        return $this;
    }

    public function getDescPartenaireEs(): ?string
    {
        return $this->descPartenaireEs;
    }

    public function setDescPartenaireEs(string $descPartenaireEs): self
    {
        $this->descPartenaireEs = $descPartenaireEs;

        return $this;
    }

    public function getDescPartenaireEn(): ?string
    {
        return $this->descPartenaireEn;
    }

    public function setDescPartenaireEn(string $descPartenaireEn): self
    {
        $this->descPartenaireEn = $descPartenaireEn;

        return $this;
    }

    public function getDescPartenaireFr(): ?string
    {
        return $this->descPartenaireFr;
    }

    public function setDescPartenaireFr(string $descPartenaireFr): self
    {
        $this->descPartenaireFr = $descPartenaireFr;

        return $this;
    }

    public function getDescPartenaire(): ?string
    {
        return $this->descPartenaire;
    }

    public function setDescPartenaire(string $descPartenaire): self
    {
        $this->descPartenaire = $descPartenaire;

        return $this;
    }
}
