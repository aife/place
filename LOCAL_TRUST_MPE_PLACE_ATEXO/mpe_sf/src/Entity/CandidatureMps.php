<?php

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * CandidatureMps.
 *
 * @ORM\Table(name="t_candidature_mps")
 * @ORM\Entity(repositoryClass="App\Repository\CandidatureMpsRepository")
 */
class CandidatureMps
{
    /**
     *
     * @ORM\Column(name="id_candidature", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idCandidature;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private ?int $idEntreprise = null;

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=false)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer", nullable=false)
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_offre", type="integer", nullable=true)
     */
    private ?int $idOffre = null;

    /**
     * @ORM\Column(name="id_blob", type="integer", nullable=false)
     */
    private ?int $idBlob = null;

    /**
     * @ORM\Column(name="horodatage", type="blob", nullable=false)
     */
    private ?string $horodatage = null;

    /**
     * @ORM\Column(name="untrusted_date", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|DateTime $untrustedDate = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="untrusted_serial", type="string", length=40, nullable=false)
     */
    private ?string $untrustedSerial = null;

    /**
     * @var int
     *
     * @ORM\Column(name="taille_fichier", type="integer", nullable=false)
     */
    private $tailleFichier;

    /**
     * @ORM\Column(name="liste_lots", type="text", nullable=true)
     */
    private ?string $listeLots = null;

    /**
     * CandidatureMps constructor.
     */
    public function __construct()
    {
        $this->untrustedDate = new DateTime();
    }

    /**
     * Get idCandidature.
     *
     * @return int
     */
    public function getIdCandidature()
    {
        return $this->idCandidature;
    }

    /**
     * Set idEntreprise.
     *
     * @param int $idEntreprise
     *
     * @return CandidatureMps
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;

        return $this;
    }

    /**
     * Get idEntreprise.
     *
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Set idInscrit.
     *
     * @param int $idInscrit
     *
     * @return CandidatureMps
     */
    public function setIdInscrit($idInscrit)
    {
        $this->idInscrit = $idInscrit;

        return $this;
    }

    /**
     * Get idInscrit.
     *
     * @return int
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return CandidatureMps
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set idOffre.
     *
     * @param int $idOffre
     *
     * @return CandidatureMps
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;

        return $this;
    }

    /**
     * Get idOffre.
     *
     * @return int
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * Set idBlob.
     *
     * @param int $idBlob
     *
     * @return CandidatureMps
     */
    public function setIdBlob($idBlob)
    {
        $this->idBlob = $idBlob;

        return $this;
    }

    /**
     * Get idBlob.
     *
     * @return int
     */
    public function getIdBlob()
    {
        return $this->idBlob;
    }

    /**
     * Set horodatage.
     *
     * @param string $horodatage
     *
     * @return CandidatureMps
     */
    public function setHorodatage($horodatage)
    {
        $this->horodatage = $horodatage;

        return $this;
    }

    /**
     * Get horodatage.
     *
     * @return string
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * Set untrustedDate.
     *
     * @param DateTime $untrustedDate
     *
     * @return CandidatureMps
     */
    public function setUntrustedDate($untrustedDate)
    {
        $this->untrustedDate = $untrustedDate;

        return $this;
    }

    /**
     * Get untrustedDate.
     *
     * @return DateTime
     */
    public function getUntrustedDate()
    {
        return $this->untrustedDate;
    }

    /**
     * Set untrustedSerial.
     *
     * @param string $untrustedSerial
     *
     * @return CandidatureMps
     */
    public function setUntrustedSerial($untrustedSerial)
    {
        $this->untrustedSerial = $untrustedSerial;

        return $this;
    }

    /**
     * Get untrustedSerial.
     *
     * @return string
     */
    public function getUntrustedSerial()
    {
        return $this->untrustedSerial;
    }

    /**
     * Set tailleFichier.
     *
     * @param int $tailleFichier
     *
     * @return CandidatureMps
     */
    public function setTailleFichier($tailleFichier)
    {
        $this->tailleFichier = $tailleFichier;

        return $this;
    }

    /**
     * Get tailleFichier.
     *
     * @return int
     */
    public function getTailleFichier()
    {
        return $this->tailleFichier;
    }

    /**
     * @return string
     */
    public function getListeLots()
    {
        return $this->listeLots;
    }

    /**
     * @param string $listeLots
     */
    public function setListeLots($listeLots)
    {
        $this->listeLots = $listeLots;
    }

    public function getFileSize(string $unity = 'Mo')
    {
        switch (strtolower($unity)) {
            case 'mo':
                $this->tailleFichier /= 1024;
                $this->aroundFileSize($this->tailleFichier);
                break;
        }

        return $this->tailleFichier;
    }

    protected function aroundFileSize($kiloOctets)
    {
        if ($kiloOctets < 1024) {
            $this->tailleFichier = str_replace('.', ',', round($kiloOctets, 2)).' '.'KO';
        } else {
            $this->tailleFichier = str_replace('.', ',', round($kiloOctets / 1024, 2)).' '.'MO';
        }
    }
}
