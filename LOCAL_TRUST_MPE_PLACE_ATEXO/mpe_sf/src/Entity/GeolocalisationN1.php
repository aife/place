<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * GeolocalisationN1.
 *
 * @ORM\Table(name="GeolocalisationN1", indexes={@ORM\Index(name="id_geolocalisationN0", columns={"id_geolocalisationN0"})})
 * @ORM\Entity(repositoryClass="App\Repository\GeolocalisationN1Repository")
 */
class GeolocalisationN1
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GeolocalisationN0", inversedBy="geolocalisationN1s")
     * @ORM\JoinColumn(name="id_geolocalisationN0", referencedColumnName="id")
     */
    private ?GeolocalisationN0 $geolocalisationN0 = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\GeolocalisationN2", mappedBy="geolocalisationN1")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_geolocalisationN1")
     */
    private Collection $geolocalisationN2s;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="id_geolocalisationN0", type="integer", nullable=false)
     */
    private string|int $idGeolocalisationN0 = '0';

    /**
     * @ORM\Column(name="denomination1", type="string", length=100, nullable=false)
     */
    private ?string $denomination1 = null;

    /**
     * @ORM\Column(name="denomination2", type="string", length=100, nullable=false)
     */
    private ?string $denomination2 = null;

    /**
     * @ORM\Column(name="denomination1_ar", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Ar = null;

    /**
     * @ORM\Column(name="denomination2_ar", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Ar = null;

    /**
     * @ORM\Column(name="denomination1_fr", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Fr = null;

    /**
     * @ORM\Column(name="denomination2_fr", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Fr = null;

    /**
     * @ORM\Column(name="denomination1_en", type="string", length=100, nullable=false)
     */
    private ?string $denomination1En = null;

    /**
     * @ORM\Column(name="denomination2_en", type="string", length=100, nullable=false)
     */
    private ?string $denomination2En = null;

    /**
     * @ORM\Column(name="denomination1_es", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Es = null;

    /**
     * @ORM\Column(name="denomination2_es", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Es = null;

    /**
     * @ORM\Column(name="denomination1_su", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Su = null;

    /**
     * @ORM\Column(name="denomination2_su", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Su = null;

    /**
     * @ORM\Column(name="denomination1_du", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Du = null;

    /**
     * @ORM\Column(name="denomination2_du", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Du = null;

    /**
     * @ORM\Column(name="denomination1_cz", type="string", length=100, nullable=false)
     */
    private ?string $denomination1Cz = null;

    /**
     * @ORM\Column(name="denomination2_cz", type="string", length=100, nullable=false)
     */
    private ?string $denomination2Cz = null;

    /**
     * @ORM\Column(name="denomination1_it", type="string", length=100, nullable=false)
     */
    private string $denomination1It = '';

    /**
     * @ORM\Column(name="denomination2_it", type="string", length=100, nullable=false)
     */
    private string $denomination2It = '';

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGeolocalisationN0.
     *
     * @param int $idGeolocalisationN0
     *
     * @return GeolocalisationN1
     */
    public function setIdGeolocalisationN0($idGeolocalisationN0)
    {
        $this->idGeolocalisationN0 = $idGeolocalisationN0;

        return $this;
    }

    /**
     * Get idGeolocalisationN0.
     *
     * @return int
     */
    public function getIdGeolocalisationN0()
    {
        return $this->idGeolocalisationN0;
    }

    /**
     * Set denomination1.
     *
     * @param string $denomination1
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1($denomination1)
    {
        $this->denomination1 = $denomination1;

        return $this;
    }

    /**
     * Get denomination1.
     *
     * @return string
     */
    public function getDenomination1()
    {
        return $this->denomination1;
    }

    /**
     * Set denomination2.
     *
     * @param string $denomination2
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2($denomination2)
    {
        $this->denomination2 = $denomination2;

        return $this;
    }

    /**
     * Get denomination2.
     *
     * @return string
     */
    public function getDenomination2()
    {
        return $this->denomination2;
    }

    /**
     * Set denomination1Ar.
     *
     * @param string $denomination1Ar
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Ar($denomination1Ar)
    {
        $this->denomination1Ar = $denomination1Ar;

        return $this;
    }

    /**
     * Get denomination1Ar.
     *
     * @return string
     */
    public function getDenomination1Ar()
    {
        return $this->denomination1Ar;
    }

    /**
     * Set denomination2Ar.
     *
     * @param string $denomination2Ar
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Ar($denomination2Ar)
    {
        $this->denomination2Ar = $denomination2Ar;

        return $this;
    }

    /**
     * Get denomination2Ar.
     *
     * @return string
     */
    public function getDenomination2Ar()
    {
        return $this->denomination2Ar;
    }

    /**
     * Set denomination1Fr.
     *
     * @param string $denomination1Fr
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Fr($denomination1Fr)
    {
        $this->denomination1Fr = $denomination1Fr;

        return $this;
    }

    /**
     * Get denomination1Fr.
     *
     * @return string
     */
    public function getDenomination1Fr()
    {
        return $this->denomination1Fr;
    }

    /**
     * Set denomination2Fr.
     *
     * @param string $denomination2Fr
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Fr($denomination2Fr)
    {
        $this->denomination2Fr = $denomination2Fr;

        return $this;
    }

    /**
     * Get denomination2Fr.
     *
     * @return string
     */
    public function getDenomination2Fr()
    {
        return $this->denomination2Fr;
    }

    /**
     * Set denomination1En.
     *
     * @param string $denomination1En
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1En($denomination1En)
    {
        $this->denomination1En = $denomination1En;

        return $this;
    }

    /**
     * Get denomination1En.
     *
     * @return string
     */
    public function getDenomination1En()
    {
        return $this->denomination1En;
    }

    /**
     * Set denomination2En.
     *
     * @param string $denomination2En
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2En($denomination2En)
    {
        $this->denomination2En = $denomination2En;

        return $this;
    }

    /**
     * Get denomination2En.
     *
     * @return string
     */
    public function getDenomination2En()
    {
        return $this->denomination2En;
    }

    /**
     * Set denomination1Es.
     *
     * @param string $denomination1Es
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Es($denomination1Es)
    {
        $this->denomination1Es = $denomination1Es;

        return $this;
    }

    /**
     * Get denomination1Es.
     *
     * @return string
     */
    public function getDenomination1Es()
    {
        return $this->denomination1Es;
    }

    /**
     * Set denomination2Es.
     *
     * @param string $denomination2Es
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Es($denomination2Es)
    {
        $this->denomination2Es = $denomination2Es;

        return $this;
    }

    /**
     * Get denomination2Es.
     *
     * @return string
     */
    public function getDenomination2Es()
    {
        return $this->denomination2Es;
    }

    /**
     * Set denomination1Su.
     *
     * @param string $denomination1Su
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Su($denomination1Su)
    {
        $this->denomination1Su = $denomination1Su;

        return $this;
    }

    /**
     * Get denomination1Su.
     *
     * @return string
     */
    public function getDenomination1Su()
    {
        return $this->denomination1Su;
    }

    /**
     * Set denomination2Su.
     *
     * @param string $denomination2Su
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Su($denomination2Su)
    {
        $this->denomination2Su = $denomination2Su;

        return $this;
    }

    /**
     * Get denomination2Su.
     *
     * @return string
     */
    public function getDenomination2Su()
    {
        return $this->denomination2Su;
    }

    /**
     * Set denomination1Du.
     *
     * @param string $denomination1Du
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Du($denomination1Du)
    {
        $this->denomination1Du = $denomination1Du;

        return $this;
    }

    /**
     * Get denomination1Du.
     *
     * @return string
     */
    public function getDenomination1Du()
    {
        return $this->denomination1Du;
    }

    /**
     * Set denomination2Du.
     *
     * @param string $denomination2Du
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Du($denomination2Du)
    {
        $this->denomination2Du = $denomination2Du;

        return $this;
    }

    /**
     * Get denomination2Du.
     *
     * @return string
     */
    public function getDenomination2Du()
    {
        return $this->denomination2Du;
    }

    /**
     * Set denomination1Cz.
     *
     * @param string $denomination1Cz
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1Cz($denomination1Cz)
    {
        $this->denomination1Cz = $denomination1Cz;

        return $this;
    }

    /**
     * Get denomination1Cz.
     *
     * @return string
     */
    public function getDenomination1Cz()
    {
        return $this->denomination1Cz;
    }

    /**
     * Set denomination2Cz.
     *
     * @param string $denomination2Cz
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2Cz($denomination2Cz)
    {
        $this->denomination2Cz = $denomination2Cz;

        return $this;
    }

    /**
     * Get denomination2Cz.
     *
     * @return string
     */
    public function getDenomination2Cz()
    {
        return $this->denomination2Cz;
    }

    /**
     * Set denomination1It.
     *
     * @param string $denomination1It
     *
     * @return GeolocalisationN1
     */
    public function setDenomination1It($denomination1It)
    {
        $this->denomination1It = $denomination1It;

        return $this;
    }

    /**
     * Get denomination1It.
     *
     * @return string
     */
    public function getDenomination1It()
    {
        return $this->denomination1It;
    }

    /**
     * Set denomination2It.
     *
     * @param string $denomination2It
     *
     * @return GeolocalisationN1
     */
    public function setDenomination2It($denomination2It)
    {
        $this->denomination2It = $denomination2It;

        return $this;
    }

    /**
     * Get denomination2It.
     *
     * @return string
     */
    public function getDenomination2It()
    {
        return $this->denomination2It;
    }

    /**
     * @return GeolocalisationN0
     */
    public function getGeolocalisationNo()
    {
        return $this->geolocalisationN0;
    }

    public function setGeolocalisationNo(GeolocalisationN0 $geolocalisationNo)
    {
        $this->geolocalisationN0 = $geolocalisationNo;
    }

    /**
     * Entreprise constructor.
     */
    public function __construct()
    {
        $this->geolocalisationN2s = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addGeolocalisationN2(GeolocalisationN2 $geolocalisationN2)
    {
        $this->geolocalisationN2s[] = $geolocalisationN2;
        $geolocalisationN2->setGeolocalisationN1($this);

        return $this;
    }

    public function removeGeolocalisationN2(GeolocalisationN2 $geolocalisationN2)
    {
        $this->geolocalisationN2s->removeElement($geolocalisationN2);
    }

    /**
     * @return ArrayCollection
     */
    public function getGeolocalisationN2s()
    {
        return $this->geolocalisationN2s;
    }
}
