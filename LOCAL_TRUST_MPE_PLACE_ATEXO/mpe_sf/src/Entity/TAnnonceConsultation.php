<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Table(name="t_annonce_consultation")
 * @ORM\Entity(repositoryClass="App\Repository\TAnnonceConsultationRepository")
 */
class TAnnonceConsultation
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_dossier_sub", type="integer")
     */
    private ?int $idDossierSub = null;

    /**
     * @ORM\Column(name="id_dispositif", type="integer")
     */
    private ?int $idDispositif = null;

    /**
     * @ORM\Column(name="id_compte_boamp", type="integer")
     */
    private ?int $idCompteBoamp = null;

    /**
     * @ORM\Column(name="statut", type="string")
     */
    private ?string $statut = null;

    /**
     * @ORM\Column(name="id_type_annonce", type="integer", nullable=true)
     */
    private ?int $idTypeAnnonce = null;

    /**
     * @ORM\Column(name="id_agent", type="integer")
     */
    private ?int $idAgent = null;

    /**
     * @ORM\Column(name="prenom_nom_agent", type="string")
     */
    private ?string $prenomNomAgent = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")
     */
    private $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime")
     */
    private $dateModification;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_statut", type="datetime")
     */
    private $dateStatut;

    /**
     * @ORM\Column(name="motif_statut", type="string", nullable=true)
     */
    private ?string $motifStatut = null;

    /**
     * @ORM\Column(name="id_dossier_parent", type="integer", nullable=true)
     */
    private ?int $idDossierParent = null;

    /**
     * @ORM\Column(name="id_dispositif_parent", type="integer", nullable=true)
     */
    private ?int $idDispositifParent = null;

    /**
     * @ORM\Column(name="publicite_simplifie", type="string")
     */
    private string $publiciteSimplifie = '0';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getIdDossierSub()
    {
        return $this->idDossierSub;
    }

    /**
     * @param int $idDossierSub
     */
    public function setIdDossierSub($idDossierSub)
    {
        $this->idDossierSub = $idDossierSub;
    }

    /**
     * @return int
     */
    public function getIdDispositif()
    {
        return $this->idDispositif;
    }

    /**
     * @param int $idDispositif
     */
    public function setIdDispositif($idDispositif)
    {
        $this->idDispositif = $idDispositif;
    }

    /**
     * @return int
     */
    public function getIdCompteBoamp()
    {
        return $this->idCompteBoamp;
    }

    /**
     * @param int $idCompteBoamp
     */
    public function setIdCompteBoamp($idCompteBoamp)
    {
        $this->idCompteBoamp = $idCompteBoamp;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return int
     */
    public function getIdTypeAnnonce()
    {
        return $this->idTypeAnnonce;
    }

    /**
     * @param int $idTypeAnnonce
     */
    public function setIdTypeAnnonce($idTypeAnnonce)
    {
        $this->idTypeAnnonce = $idTypeAnnonce;
    }

    /**
     * @return int
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * @param int $idAgent
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * @return string
     */
    public function getPrenomNomAgent()
    {
        return $this->prenomNomAgent;
    }

    /**
     * @param string $prenomNomAgent
     */
    public function setPrenomNomAgent($prenomNomAgent)
    {
        $this->prenomNomAgent = $prenomNomAgent;
    }

    /**
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param DateTime $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return DateTime
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * @param DateTime $dateModification
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;
    }

    /**
     * @return DateTime
     */
    public function getDateStatut()
    {
        return $this->dateStatut;
    }

    /**
     * @param DateTime $dateStatut
     */
    public function setDateStatut($dateStatut)
    {
        $this->dateStatut = $dateStatut;
    }

    /**
     * @return string
     */
    public function getMotifStatut()
    {
        return $this->motifStatut;
    }

    /**
     * @param string $motifStatut
     */
    public function setMotifStatut($motifStatut)
    {
        $this->motifStatut = $motifStatut;
    }

    /**
     * @return int
     */
    public function getIdDossierParent()
    {
        return $this->idDossierParent;
    }

    /**
     * @param int $idDossierParent
     */
    public function setIdDossierParent($idDossierParent)
    {
        $this->idDossierParent = $idDossierParent;
    }

    /**
     * @return int
     */
    public function getIdDispositifParent()
    {
        return $this->idDispositifParent;
    }

    /**
     * @param int $idDispositifParent
     */
    public function setIdDispositifParent($idDispositifParent)
    {
        $this->idDispositifParent = $idDispositifParent;
    }

    /**
     * @return string
     */
    public function getPubliciteSimplifie()
    {
        return $this->publiciteSimplifie;
    }

    /**
     * @param string $publiciteSimplifie
     */
    public function setPubliciteSimplifie($publiciteSimplifie)
    {
        $this->publiciteSimplifie = $publiciteSimplifie;
    }
}
