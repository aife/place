<?php

namespace App\Entity;

use App\Repository\DonneesConsultationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_donnees_consultation")
 * @ORM\Entity(repositoryClass=DonneesConsultationRepository::class)
 */
class DonneesConsultation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $idDonneesConsultation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $referenceConsultation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $idContratTitulaire;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private ?string $organisme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $oldServiceId;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $idTypeProcedure;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedure;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $nbreOffresRecues;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $nbreOffresDematerialisees;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private ?string $signatureOffre;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $serviceId;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $uuidExterneExec;

    public function getId(): ?int
    {
        return $this->idDonneesConsultation;
    }

    public function getReferenceConsultation(): ?int
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation(?int $referenceConsultation): self
    {
        $this->referenceConsultation = $referenceConsultation;

        return $this;
    }

    public function getIdContratTitulaire(): ?int
    {
        return $this->idContratTitulaire;
    }

    public function setIdContratTitulaire(int $idContratTitulaire): self
    {
        $this->idContratTitulaire = $idContratTitulaire;

        return $this;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(?string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }

    public function setOldServiceId(?int $oldServiceId): self
    {
        $this->oldServiceId = $oldServiceId;

        return $this;
    }

    public function getIdTypeProcedure(): ?int
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure(?int $idTypeProcedure): self
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    public function getLibelleTypeProcedure(): ?string
    {
        return $this->libelleTypeProcedure;
    }

    public function setLibelleTypeProcedure(?string $libelleTypeProcedure): self
    {
        $this->libelleTypeProcedure = $libelleTypeProcedure;

        return $this;
    }

    public function getNbreOffresRecues(): ?int
    {
        return $this->nbreOffresRecues;
    }

    public function setNbreOffresRecues(?int $nbreOffresRecues): self
    {
        $this->nbreOffresRecues = $nbreOffresRecues;

        return $this;
    }

    public function getNbreOffresDematerialisees(): ?int
    {
        return $this->nbreOffresDematerialisees;
    }

    public function setNbreOffresDematerialisees(?int $nbreOffresDematerialisees): self
    {
        $this->nbreOffresDematerialisees = $nbreOffresDematerialisees;

        return $this;
    }

    public function getSignatureOffre(): ?string
    {
        return $this->signatureOffre;
    }

    public function setSignatureOffre(?string $signatureOffre): self
    {
        $this->signatureOffre = $signatureOffre;

        return $this;
    }

    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }

    public function setServiceId(?int $serviceId): self
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    public function getUuidExterneExec(): ?string
    {
        return $this->uuidExterneExec;
    }

    public function setUuidExterneExec(?string $uuidExterneExec): self
    {
        $this->uuidExterneExec = $uuidExterneExec;

        return $this;
    }
}
