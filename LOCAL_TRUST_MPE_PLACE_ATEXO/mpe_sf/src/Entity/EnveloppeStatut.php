<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EnveloppeStatut.
 *
 * @ORM\Table(name="StatutEnveloppe")
 * @ORM\Entity
 */
class EnveloppeStatut
{
    /**
     *
     * @ORM\Column(name="id_statut", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private string|int $idStatut = '0';

    /**
     * @ORM\Column(name="description", type="string", length=100, nullable=false)
     */
    private string $description = '';

    /**
     * Get idStatut.
     *
     * @return int
     */
    public function getIdStatut()
    {
        return $this->idStatut;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return EnveloppeStatut
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
}
