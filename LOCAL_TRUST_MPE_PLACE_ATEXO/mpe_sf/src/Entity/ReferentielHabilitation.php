<?php

namespace App\Entity;

use App\Repository\ReferentielHabilitationRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ReferentielHabilitationRepository::class)]
/**
 * @ORM\Entity(repositoryClass=ReferentielHabilitationRepository::class)
 */
class ReferentielHabilitation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $slug = null;

    #[ORM\Column(type: 'boolean')]
    /**
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): ReferentielHabilitation
    {
        $this->active = $active;

        return $this;
    }
}
