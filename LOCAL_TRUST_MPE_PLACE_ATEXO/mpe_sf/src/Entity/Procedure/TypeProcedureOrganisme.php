<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity\Procedure;

use App\Entity\Organisme;
use App\Entity\Service;
use Stringable;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\TypeProcedureOrganismeOutput;
use App\Entity\TypeProcedure;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TypeProcedureOrganisme.
 *
 * @ORM\Table(name="Type_Procedure_Organisme")
 * @ORM\Entity(repositoryClass="App\Repository\Procedure\TypeProcedureOrganismeRepository")
 */
#[ApiResource(
    collectionOperations: [
        'get',
        'get_for_suite' => [
            'method' => 'GET',
            'path' => '/procedures-suite',
        ],
        'get_for_consultation_creation' => [
            'method' => 'GET',
            'path' => '/procedures-consultation-creation',
        ],
    ],
    itemOperations: ['get'],
    shortName: 'Procedure',
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: TypeProcedureOrganismeOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'idExterne' => 'exact',
        'typeContratEtProcedure.idTypeContrat' => 'exact',
        'procedureSimplifie' => 'exact',
    ]
)]
class TypeProcedureOrganisme implements Stringable
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => false,
            'migrationService' => 'atexo.service.migration.organisme',
        ],
        'serviceValidation' => [
            'class' => Service::class,
            'isObject' => false,
            'property' => 'id',
            'migrationService' => 'atexo.service.migration.service',
        ],
    ];

    /**
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $idTypeProcedure = null;

    /**
     *
     * @ORM\Column(type="string", length=30, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $organisme = null;

    private $organismes = [];

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeProcedure", inversedBy="typeProcedureOrganismes", fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_procedure_portail", referencedColumnName="id_type_procedure"),
     */
    private ?TypeProcedure $procedurePortailType = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private string $libelleTypeProcedure = '';

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private string $abbreviation = '';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $typeBoamp = '0';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $idTypeProcedurePortail = '0';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $categorieProcedure = '0';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $delaiAlerte = '0';

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $idTypeValidation = '2';

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $serviceValidation = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $mapa = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $activerMapa = '0';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureFr = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureEn = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureEs = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureSu = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureDu = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureCz = '';

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureAr = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private string|int $idMontantMapa = '0';

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private ?string $codeRecensement = null;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $depouillablePhaseConsultation = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $consultationTransverse = '0';

    /**
     * @ORM\Column(type="string", length=50, nullable=false)
     */
    private string $tagBoamp = '';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $ao = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $mn = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $dc = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $autre = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $sad = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $accordCadre = '0';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $pn = '0';

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private ?string $tagNameMesureAvancement = null;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private ?string $abreviationInterface = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private ?string $libelleTypeProcedureIt = null;

    /**
     * @ORM\Column(type="text", length=65535, nullable=false)
     */
    private ?string $publiciteTypesFormXml = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private ?string $tagNameChorus = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private ?string $equivalentOpoce = null;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    private ?string $equivalentBoamp = null;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ordreAffichage = '0';

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private ?string $idExterne = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="procedure_simplifie", type="boolean")
     */
    private $procedureSimplifie = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TypeContratEtTypeProcedure", mappedBy="idTypeProcedure", fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_procedure", referencedColumnName="id_type_procedure"),
     */
    private ?Collection $typeContratEtProcedure;

    public function __construct()
    {
        $this->typeContratEtProcedure = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->libelleTypeProcedure;
    }

    public function concatTypeProcedureOrganisme(): string
    {
        return $this->idTypeProcedure . '_' . $this->organisme;
    }

    public function getIdTypeProcedure(): int
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure(?int $idTypeProcedure): TypeProcedureOrganisme
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): TypeProcedureOrganisme
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getProcedurePortailType(): ?TypeProcedure
    {
        return $this->procedurePortailType;
    }

    public function setProcedurePortailType(?TypeProcedure $procedurePortailType): TypeProcedureOrganisme
    {
        $this->procedurePortailType = $procedurePortailType;

        return $this;
    }

    public function getLibelleTypeProcedure(): string
    {
        return $this->libelleTypeProcedure;
    }

    public function setLibelleTypeProcedure(string $libelleTypeProcedure): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedure = $libelleTypeProcedure;

        return $this;
    }

    public function getAbbreviation(): string
    {
        return $this->abbreviation;
    }

    public function setAbbreviation(string $abbreviation): TypeProcedureOrganisme
    {
        $this->abbreviation = $abbreviation;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeBoamp()
    {
        return $this->typeBoamp;
    }

    /**
     * @param int $typeBoamp
     *
     * @return TypeProcedureOrganisme
     */
    public function setTypeBoamp($typeBoamp)
    {
        $this->typeBoamp = $typeBoamp;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeProcedurePortail()
    {
        return $this->idTypeProcedurePortail;
    }

    /**
     * @param int $idTypeProcedurePortail
     *
     * @return TypeProcedureOrganisme
     */
    public function setIdTypeProcedurePortail($idTypeProcedurePortail)
    {
        $this->idTypeProcedurePortail = $idTypeProcedurePortail;

        return $this;
    }

    /**
     * @return int
     */
    public function getCategorieProcedure()
    {
        return $this->categorieProcedure;
    }

    /**
     * @param int $categorieProcedure
     *
     * @return TypeProcedureOrganisme
     */
    public function setCategorieProcedure($categorieProcedure)
    {
        $this->categorieProcedure = $categorieProcedure;

        return $this;
    }

    /**
     * @return int
     */
    public function getDelaiAlerte()
    {
        return $this->delaiAlerte;
    }

    /**
     * @param int $delaiAlerte
     *
     * @return TypeProcedureOrganisme
     */
    public function setDelaiAlerte($delaiAlerte)
    {
        $this->delaiAlerte = $delaiAlerte;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdTypeValidation()
    {
        return $this->idTypeValidation;
    }

    /**
     * @param int $idTypeValidation
     *
     * @return TypeProcedureOrganisme
     */
    public function setIdTypeValidation($idTypeValidation)
    {
        $this->idTypeValidation = $idTypeValidation;

        return $this;
    }

    /**
     * @return int
     */
    public function getServiceValidation()
    {
        return $this->serviceValidation;
    }

    /**
     * @param int $serviceValidation
     *
     * @return TypeProcedureOrganisme
     */
    public function setServiceValidation($serviceValidation)
    {
        $this->serviceValidation = $serviceValidation;

        return $this;
    }

    public function getMapa(): string
    {
        return $this->mapa;
    }

    public function setMapa(string $mapa): TypeProcedureOrganisme
    {
        $this->mapa = $mapa;

        return $this;
    }

    public function getActiverMapa(): string
    {
        return $this->activerMapa;
    }

    public function setActiverMapa(string $activerMapa): TypeProcedureOrganisme
    {
        $this->activerMapa = $activerMapa;

        return $this;
    }

    public function getLibelleTypeProcedureFr(): string
    {
        return $this->libelleTypeProcedureFr;
    }

    public function setLibelleTypeProcedureFr(string $libelleTypeProcedureFr): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureFr = $libelleTypeProcedureFr;

        return $this;
    }

    public function getLibelleTypeProcedureEn(): string
    {
        return $this->libelleTypeProcedureEn;
    }

    public function setLibelleTypeProcedureEn(string $libelleTypeProcedureEn): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureEn = $libelleTypeProcedureEn;

        return $this;
    }

    public function getLibelleTypeProcedureEs(): string
    {
        return $this->libelleTypeProcedureEs;
    }

    public function setLibelleTypeProcedureEs(string $libelleTypeProcedureEs): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureEs = $libelleTypeProcedureEs;

        return $this;
    }

    public function getLibelleTypeProcedureSu(): string
    {
        return $this->libelleTypeProcedureSu;
    }

    public function setLibelleTypeProcedureSu(string $libelleTypeProcedureSu): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureSu = $libelleTypeProcedureSu;

        return $this;
    }

    public function getLibelleTypeProcedureDu(): string
    {
        return $this->libelleTypeProcedureDu;
    }

    public function setLibelleTypeProcedureDu(string $libelleTypeProcedureDu): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureDu = $libelleTypeProcedureDu;

        return $this;
    }

    public function getLibelleTypeProcedureCz(): string
    {
        return $this->libelleTypeProcedureCz;
    }

    public function setLibelleTypeProcedureCz(string $libelleTypeProcedureCz): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureCz = $libelleTypeProcedureCz;

        return $this;
    }

    public function getLibelleTypeProcedureAr(): string
    {
        return $this->libelleTypeProcedureAr;
    }

    public function setLibelleTypeProcedureAr(string $libelleTypeProcedureAr): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureAr = $libelleTypeProcedureAr;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdMontantMapa()
    {
        return $this->idMontantMapa;
    }

    /**
     * @param int $idMontantMapa
     *
     * @return TypeProcedureOrganisme
     */
    public function setIdMontantMapa($idMontantMapa)
    {
        $this->idMontantMapa = $idMontantMapa;

        return $this;
    }

    public function getCodeRecensement(): string
    {
        return $this->codeRecensement;
    }

    public function setCodeRecensement(string $codeRecensement): TypeProcedureOrganisme
    {
        $this->codeRecensement = $codeRecensement;

        return $this;
    }

    public function getDepouillablePhaseConsultation(): string
    {
        return $this->depouillablePhaseConsultation;
    }

    public function setDepouillablePhaseConsultation(string $depouillablePhaseConsultation): TypeProcedureOrganisme
    {
        $this->depouillablePhaseConsultation = $depouillablePhaseConsultation;

        return $this;
    }

    public function getConsultationTransverse(): string
    {
        return $this->consultationTransverse;
    }

    public function setConsultationTransverse(string $consultationTransverse): TypeProcedureOrganisme
    {
        $this->consultationTransverse = $consultationTransverse;

        return $this;
    }

    public function getTagBoamp(): string
    {
        return $this->tagBoamp;
    }

    public function setTagBoamp(string $tagBoamp): TypeProcedureOrganisme
    {
        $this->tagBoamp = $tagBoamp;

        return $this;
    }

    public function getAo(): string
    {
        return $this->ao;
    }

    public function setAo(string $ao): TypeProcedureOrganisme
    {
        $this->ao = $ao;

        return $this;
    }

    public function getMn(): string
    {
        return $this->mn;
    }

    public function setMn(string $mn): TypeProcedureOrganisme
    {
        $this->mn = $mn;

        return $this;
    }

    public function getDc(): string
    {
        return $this->dc;
    }

    public function setDc(string $dc): TypeProcedureOrganisme
    {
        $this->dc = $dc;

        return $this;
    }

    public function getAutre(): string
    {
        return $this->autre;
    }

    public function setAutre(string $autre): TypeProcedureOrganisme
    {
        $this->autre = $autre;

        return $this;
    }

    public function getSad(): string
    {
        return $this->sad;
    }

    public function setSad(string $sad): TypeProcedureOrganisme
    {
        $this->sad = $sad;

        return $this;
    }

    public function getAccordCadre(): string
    {
        return $this->accordCadre;
    }

    public function setAccordCadre(string $accordCadre): TypeProcedureOrganisme
    {
        $this->accordCadre = $accordCadre;

        return $this;
    }

    public function getPn(): string
    {
        return $this->pn;
    }

    public function setPn(string $pn): TypeProcedureOrganisme
    {
        $this->pn = $pn;

        return $this;
    }

    public function getTagNameMesureAvancement(): string
    {
        return $this->tagNameMesureAvancement;
    }

    public function setTagNameMesureAvancement(string $tagNameMesureAvancement): TypeProcedureOrganisme
    {
        $this->tagNameMesureAvancement = $tagNameMesureAvancement;

        return $this;
    }

    public function getAbreviationInterface(): ?string
    {
        return $this->abreviationInterface;
    }

    public function setAbreviationInterface(string $abreviationInterface): TypeProcedureOrganisme
    {
        $this->abreviationInterface = $abreviationInterface;

        return $this;
    }

    public function getLibelleTypeProcedureIt(): string
    {
        return $this->libelleTypeProcedureIt;
    }

    public function setLibelleTypeProcedureIt(string $libelleTypeProcedureIt): TypeProcedureOrganisme
    {
        $this->libelleTypeProcedureIt = $libelleTypeProcedureIt;

        return $this;
    }

    public function getPubliciteTypesFormXml(): string
    {
        return $this->publiciteTypesFormXml;
    }

    public function setPubliciteTypesFormXml(string $publiciteTypesFormXml): TypeProcedureOrganisme
    {
        $this->publiciteTypesFormXml = $publiciteTypesFormXml;

        return $this;
    }

    public function getTagNameChorus(): string
    {
        return $this->tagNameChorus;
    }

    public function setTagNameChorus(string $tagNameChorus): TypeProcedureOrganisme
    {
        $this->tagNameChorus = $tagNameChorus;

        return $this;
    }

    public function getEquivalentOpoce(): string
    {
        return $this->equivalentOpoce;
    }

    public function setEquivalentOpoce(string $equivalentOpoce): TypeProcedureOrganisme
    {
        $this->equivalentOpoce = $equivalentOpoce;

        return $this;
    }

    public function getEquivalentBoamp(): string
    {
        return $this->equivalentBoamp;
    }

    public function setEquivalentBoamp(string $equivalentBoamp): TypeProcedureOrganisme
    {
        $this->equivalentBoamp = $equivalentBoamp;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrdreAffichage()
    {
        return $this->ordreAffichage;
    }

    /**
     * @param int $ordreAffichage
     *
     * @return TypeProcedureOrganisme
     */
    public function setOrdreAffichage($ordreAffichage)
    {
        $this->ordreAffichage = $ordreAffichage;

        return $this;
    }

    public function getIdExterne(): ?string
    {
        return $this->idExterne;
    }

    public function setIdExterne(string $idExterne): TypeProcedureOrganisme
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    public function getProcedureSimplifie(): bool
    {
        return $this->procedureSimplifie;
    }

    public function setProcedureSimplifie(bool $procedureSimplifie): TypeProcedureOrganisme
    {
        $this->procedureSimplifie = $procedureSimplifie;

        return $this;
    }

    public function getOrganismes(): array
    {
        return $this->organismes;
    }

    public function setOrganismes(array $organismes): TypeProcedureOrganisme
    {
        $this->organismes = $organismes;

        return $this;
    }

    public function getTypeContratEtProcedure(): ?Collection
    {
        return $this->typeContratEtProcedure;
    }

    public function setTypeContratEtProcedure(?Collection $typeContratEtProcedure): self
    {
        $this->typeContratEtProcedure = $typeContratEtProcedure;

        return $this;
    }
}
