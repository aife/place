<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Tiers.
 *
 * @ORM\Table(name="Tiers")
 * @ORM\Entity(repositoryClass="App\Repository\TiersRepository")
 */
class Tiers
{
    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private $organisme;

    /**
     *
     * @ORM\Column(name="id_tiers", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private string|int $idTiers;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", nullable=false)
     */
    private $login;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", nullable=false)
     */
    private $denomination;

    /**
     * @var string
     *
     * @ORM\Column(name="fonctionnalite", type="string", nullable=false)
     */
    private $fonctionnalite;

    /**
     * @return int
     */
    public function getIdTiers()
    {
        return $this->idTiers;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param $login
     *
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param $password
     *
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * @param $denomination
     *
     * @return $this
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * @return string
     */
    public function getFonctionnalite()
    {
        return $this->fonctionnalite;
    }

    /**
     * @param $fonctionnalite
     *
     * @return $this
     */
    public function setFonctionnalite($fonctionnalite)
    {
        $this->fonctionnalite = $fonctionnalite;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param $organisme
     *
     * @return $this
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }
}
