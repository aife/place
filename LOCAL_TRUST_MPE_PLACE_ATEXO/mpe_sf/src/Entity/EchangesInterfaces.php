<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="echanges_interfaces")
 * @ORM\Entity(repositoryClass="App\Repository\EchangesInterfacesRepository")
 */
class EchangesInterfaces
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
}
