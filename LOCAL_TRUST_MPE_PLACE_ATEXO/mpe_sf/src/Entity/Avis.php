<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="AVIS")
 * @ORM\Entity(repositoryClass="App\Repository\AvisRepository")
 */
class Avis
{
    /**
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="avis", type="integer")
     */
    private ?int $avis = null;

    /**
     * @ORM\Column(name="intitule_avis", type="integer")
     */
    private ?int $intituleAvis = null;

    /**
     * @ORM\Column(name="nom_avis", type="string")
     */
    private ?string $nomAvis = null;

    /**
     * @ORM\Column(name="statut", type="string")
     */
    private ?string $statut = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string")
     */
    private ?string $nomFichier = null;

    /**
     * @var blob
     *
     * @ORM\Column(name="horodatage", type="blob")
     */
    private $horodatage;

    /**
     * @var datetime
     *
     * @ORM\Column(name="untrusteddate", type="datetime")
     */
    private $untrusteddate;

    /**
     * @ORM\Column(name="agent_id", type="integer")
     */
    private ?int $agentId = null;

    /**
     * @ORM\Column(name="avis_telechargeable", type="integer")
     */
    private ?int $avisTelechargeable = null;

    /**
     * @ORM\Column(name="url", type="string")
     */
    private ?string $url = null;

    /**
     * @ORM\Column(name="type", type="string")
     */
    private ?string $type = null;

    /**
     * @ORM\Column(name="date_creation", type="string")
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_pub", type="string")
     */
    private ?string $datePub = null;

    /**
     * @ORM\Column(name="type_doc_genere", type="integer")
     */
    private ?int $typeDocGenere = null;

    /**
     * @ORM\Column(name="langue", type="string")
     */
    private ?string $langue = null;

    /**
     * @ORM\Column(name="type_avis_pub", type="integer")
     */
    private ?int $typeAvisPub = null;

    /**
     * @ORM\Column(name="chemin", type="text", nullable=true)
     */
    private ?string $chemin = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return int
     */
    public function getConsultationId()
    {
        return $this->consultationId;
    }

    /**
     * @param int $consultationId
     */
    public function setConsultationId($consultationId)
    {
        $this->consultationId = $consultationId;
    }

    /**
     * @return int
     */
    public function getAvis()
    {
        return $this->avis;
    }

    /**
     * @param int $avis
     */
    public function setAvis($avis)
    {
        $this->avis = $avis;
    }

    /**
     * @return int
     */
    public function getIntituleAvis()
    {
        return $this->intituleAvis;
    }

    /**
     * @param int $intituleAvis
     */
    public function setIntituleAvis($intituleAvis)
    {
        $this->intituleAvis = $intituleAvis;
    }

    /**
     * @return string
     */
    public function getNomAvis()
    {
        return $this->nomAvis;
    }

    /**
     * @param string $nomAvis
     */
    public function setNomAvis($nomAvis)
    {
        $this->nomAvis = $nomAvis;
    }

    /**
     * @return string
     */
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * @param string $statut
     */
    public function setStatut($statut)
    {
        $this->statut = $statut;
    }

    /**
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param string $nomFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    /**
     * @return blob
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * @param blob $horodatage
     */
    public function setHorodatage($horodatage)
    {
        $this->horodatage = $horodatage;
    }

    /**
     * @return date
     */
    public function getUntrusteddate()
    {
        return $this->untrusteddate;
    }

    /**
     * @param date $untrusteddate
     */
    public function setUntrusteddate($untrusteddate)
    {
        $this->untrusteddate = $untrusteddate;
    }

    /**
     * @return int
     */
    public function getAgentId()
    {
        return $this->agentId;
    }

    /**
     * @param int $agentId
     */
    public function setAgentId($agentId)
    {
        $this->agentId = $agentId;
    }

    /**
     * @return int
     */
    public function getAvisTelechargeable()
    {
        return $this->avisTelechargeable;
    }

    /**
     * @param int $avisTelechargeable
     */
    public function setAvisTelechargeable($avisTelechargeable)
    {
        $this->avisTelechargeable = $avisTelechargeable;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    /**
     * @return string
     */
    public function getDatePub()
    {
        return $this->datePub;
    }

    /**
     * @param string $datePub
     */
    public function setDatePub($datePub)
    {
        $this->datePub = $datePub;
    }

    /**
     * @return int
     */
    public function getTypeDocGenere()
    {
        return $this->typeDocGenere;
    }

    /**
     * @param int $typeDocGenere
     */
    public function setTypeDocGenere($typeDocGenere)
    {
        $this->typeDocGenere = $typeDocGenere;
    }

    /**
     * @return string
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * @param string $langue
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;
    }

    /**
     * @return int
     */
    public function getTypeAvisPub()
    {
        return $this->typeAvisPub;
    }

    /**
     * @param int $typeAvisPub
     */
    public function setTypeAvisPub($typeAvisPub)
    {
        $this->typeAvisPub = $typeAvisPub;
    }

    /**
     * @return string
     */
    public function getChemin()
    {
        return $this->chemin;
    }

    /**
     * @param string $chemin
     */
    public function setChemin($chemin)
    {
        $this->chemin = $chemin;
    }
}
