<?php

namespace App\Entity;

use App\Repository\TypeAvisPubRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeAvisPubRepository::class)
 * @ORM\Table(name="Type_Avis_Pub")
 */
class TypeAvisPub
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="integer")
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $resourceFormulaire;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ressourceDocPresse;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $natureAvis;

    /**
     * @ORM\Column(type="integer")
     */
    private $idDispositif;

    /**
     * @ORM\Column(type="integer")
     */
    private $typePub;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getRegion(): ?int
    {
        return $this->region;
    }

    public function setRegion(int $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getResourceFormulaire(): ?string
    {
        return $this->resourceFormulaire;
    }

    public function setResourceFormulaire(string $resourceFormulaire): self
    {
        $this->resourceFormulaire = $resourceFormulaire;

        return $this;
    }

    public function getRessourceDocPresse(): ?string
    {
        return $this->ressourceDocPresse;
    }

    public function setRessourceDocPresse(?string $ressourceDocPresse): self
    {
        $this->ressourceDocPresse = $ressourceDocPresse;

        return $this;
    }

    public function getNatureAvis(): ?int
    {
        return $this->natureAvis;
    }

    public function setNatureAvis(?int $natureAvis): self
    {
        $this->natureAvis = $natureAvis;

        return $this;
    }

    public function getIdDispositif(): ?int
    {
        return $this->idDispositif;
    }

    public function setIdDispositif(int $idDispositif): self
    {
        $this->idDispositif = $idDispositif;

        return $this;
    }

    public function getTypePub(): ?int
    {
        return $this->typePub;
    }

    public function setTypePub(int $typePub): self
    {
        $this->typePub = $typePub;

        return $this;
    }
}
