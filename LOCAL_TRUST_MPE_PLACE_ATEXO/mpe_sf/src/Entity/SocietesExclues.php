<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocietesExclues
 *
 * @ORM\Table(name="Societes_Exclues")
 * @ORM\Entity(repositoryClass="App\Repository\SocietesExcluesRepository")
 */
class SocietesExclues
{
    /**
     * @var int
     *
     * @ORM\Column(name="id_societes_exclues", type="integer", nullable=false, options={"comment"="Clé primaire"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $idSocietesExclues;

    /**
     * @var string|null
     *
     * @ORM\Column(name="organisme_acronyme", type="string", length=30, nullable=true)
     */
    private ?string $organismeAcronyme = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="old_service_id", type="integer", nullable=true)
     */
    private ?int $oldServiceId = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_agent", type="integer", nullable=true)
     */
    private ?int $idAgent = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nom_document", type="string", length=256, nullable=true)
     */
    private ?string $nomDocument = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id_blob", type="integer", nullable=true)
     */
    private ?int $idBlob = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="taille_document", type="string", length=80, nullable=true)
     */
    private ?string $tailleDocument = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identifiant_entreprise", type="string", length=20, nullable=true)
     */
    private ?string $identifiantEntreprise = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale", type="string", length=256, nullable=true)
     */
    private ?string $raisonSociale = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motif", type="string", length=256, nullable=true)
     */
    private ?string $motif = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale_fr", type="string", length=256, nullable=true)
     */
    private ?string $raisonSocialeFr = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motif_fr", type="string", length=256, nullable=true)
     */
    private ?string $motifFr = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale_ar", type="string", length=256, nullable=true)
     */
    private ?string $raisonSocialeAr = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motif_ar", type="string", length=256, nullable=true)
     */
    private ?string $motifAr = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_exclusion", type="string", length=0, nullable=true)
     */
    private ?string $typeExclusion = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="date_debut_exclusion", type="string", length=20, nullable=true)
     */
    private ?string $dateDebutExclusion = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="date_fin_exclusion", type="string", length=20, nullable=true)
     */
    private ?string $dateFinExclusion = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type_portee", type="string", length=0, nullable=true)
     */
    private ?string $typePortee = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="raison_sociale_it", type="string", length=256, nullable=true)
     */
    private ?string $raisonSocialeIt = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="motif_it", type="string", length=256, nullable=true)
     */
    private ?string $motifIt = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="service_id", type="bigint", nullable=true, options={"unsigned"=true})
     */
    private ?int $serviceId = null;

    public function getIdSocietesExclues(): ?int
    {
        return $this->idSocietesExclues;
    }

    public function getOrganismeAcronyme(): ?string
    {
        return $this->organismeAcronyme;
    }

    public function setOrganismeAcronyme(?string $organismeAcronyme): self
    {
        $this->organismeAcronyme = $organismeAcronyme;

        return $this;
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }

    public function setOldServiceId(?int $oldServiceId): self
    {
        $this->oldServiceId = $oldServiceId;

        return $this;
    }

    public function getIdAgent(): ?int
    {
        return $this->idAgent;
    }

    public function setIdAgent(?int $idAgent): self
    {
        $this->idAgent = $idAgent;

        return $this;
    }

    public function getNomDocument(): ?string
    {
        return $this->nomDocument;
    }

    public function setNomDocument(?string $nomDocument): self
    {
        $this->nomDocument = $nomDocument;

        return $this;
    }

    public function getIdBlob(): ?int
    {
        return $this->idBlob;
    }

    public function setIdBlob(?int $idBlob): self
    {
        $this->idBlob = $idBlob;

        return $this;
    }

    public function getTailleDocument(): ?string
    {
        return $this->tailleDocument;
    }

    public function setTailleDocument(?string $tailleDocument): self
    {
        $this->tailleDocument = $tailleDocument;

        return $this;
    }

    public function getIdentifiantEntreprise(): ?string
    {
        return $this->identifiantEntreprise;
    }

    public function setIdentifiantEntreprise(?string $identifiantEntreprise): self
    {
        $this->identifiantEntreprise = $identifiantEntreprise;

        return $this;
    }

    public function getRaisonSociale(): ?string
    {
        return $this->raisonSociale;
    }

    public function setRaisonSociale(?string $raisonSociale): self
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    public function getMotif(): ?string
    {
        return $this->motif;
    }

    public function setMotif(?string $motif): self
    {
        $this->motif = $motif;

        return $this;
    }

    public function getRaisonSocialeFr(): ?string
    {
        return $this->raisonSocialeFr;
    }

    public function setRaisonSocialeFr(?string $raisonSocialeFr): self
    {
        $this->raisonSocialeFr = $raisonSocialeFr;

        return $this;
    }

    public function getMotifFr(): ?string
    {
        return $this->motifFr;
    }

    public function setMotifFr(?string $motifFr): self
    {
        $this->motifFr = $motifFr;

        return $this;
    }

    public function getRaisonSocialeAr(): ?string
    {
        return $this->raisonSocialeAr;
    }

    public function setRaisonSocialeAr(?string $raisonSocialeAr): self
    {
        $this->raisonSocialeAr = $raisonSocialeAr;

        return $this;
    }

    public function getMotifAr(): ?string
    {
        return $this->motifAr;
    }

    public function setMotifAr(?string $motifAr): self
    {
        $this->motifAr = $motifAr;

        return $this;
    }

    public function getTypeExclusion(): ?string
    {
        return $this->typeExclusion;
    }

    public function setTypeExclusion(?string $typeExclusion): self
    {
        $this->typeExclusion = $typeExclusion;

        return $this;
    }

    public function getDateDebutExclusion(): ?string
    {
        return $this->dateDebutExclusion;
    }

    public function setDateDebutExclusion(?string $dateDebutExclusion): self
    {
        $this->dateDebutExclusion = $dateDebutExclusion;

        return $this;
    }

    public function getDateFinExclusion(): ?string
    {
        return $this->dateFinExclusion;
    }

    public function setDateFinExclusion(?string $dateFinExclusion): self
    {
        $this->dateFinExclusion = $dateFinExclusion;

        return $this;
    }

    public function getTypePortee(): ?string
    {
        return $this->typePortee;
    }

    public function setTypePortee(?string $typePortee): self
    {
        $this->typePortee = $typePortee;

        return $this;
    }

    public function getRaisonSocialeIt(): ?string
    {
        return $this->raisonSocialeIt;
    }

    public function setRaisonSocialeIt(?string $raisonSocialeIt): self
    {
        $this->raisonSocialeIt = $raisonSocialeIt;

        return $this;
    }

    public function getMotifIt(): ?string
    {
        return $this->motifIt;
    }

    public function setMotifIt(?string $motifIt): self
    {
        $this->motifIt = $motifIt;

        return $this;
    }

    public function getServiceId(): ?string
    {
        return $this->serviceId;
    }

    public function setServiceId(?string $serviceId): self
    {
        $this->serviceId = $serviceId;

        return $this;
    }


}
