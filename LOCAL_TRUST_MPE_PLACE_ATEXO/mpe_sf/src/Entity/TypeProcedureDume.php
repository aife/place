<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\TypeProcedureDumeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeProcedureDumeRepository::class)
 * @ORM\Table(name="t_type_procedure_dume")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    routePrefix: '/referentiels'
)]
class TypeProcedureDume
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $codeDume;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getActive(): ?string
    {
        return $this->active;
    }

    public function setActive(string $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCodeDume(): ?string
    {
        return $this->codeDume;
    }

    public function setCodeDume(string $codeDume): self
    {
        $this->codeDume = $codeDume;

        return $this;
    }
}
