<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * DonneesAnnuellesConcessionTarif.
 *
 * @ORM\Table(name="donnees_annuelles_concession_tarif")
 * @ORM\Entity
 */
class DonneesAnnuellesConcessionTarif
{
    /**
     *
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DonneesAnnuellesConcession", inversedBy="donneesAnnuellesConcessionTarifs")
     * @ORM\JoinColumn(name="id_donnees_annuelle", referencedColumnName="id")
     */
    private ?DonneesAnnuellesConcession $donneesAnnuellesConcession = null;

    /**
     *
     * @ORM\Column(name="intitule_tarif", type="string", length=255 ,nullable=true)
     * @Serializer\XmlElement(cdata=false)
     */
    private ?string $intituleTarif = null;

    /**
     * @var decimal
     *
     * @ORM\Column(name="montant", type="decimal", nullable=true)
     *
     * @Serializer\XmlElement(cdata=false)
     */
    private $montant;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DonneesAnnuellesConcession
     */
    public function getDonneesAnnuellesConcession()
    {
        return $this->donneesAnnuellesConcession;
    }

    /**
     * @param DonneesAnnuellesConcession $donneesAnnuellesConcession
     */
    public function setDonneesAnnuellesConcession($donneesAnnuellesConcession)
    {
        $this->donneesAnnuellesConcession = $donneesAnnuellesConcession;
    }

    /**
     * @return string
     */
    public function getIntituleTarif()
    {
        return $this->intituleTarif;
    }

    /**
     * @param string $intituleTarif
     */
    public function setIntituleTarif($intituleTarif)
    {
        $this->intituleTarif = $intituleTarif;
    }

    /**
     * @return decimal
     */
    public function getMontant()
    {
        return $this->montant;
    }

    /**
     * @param decimal $montant
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;
    }
}
