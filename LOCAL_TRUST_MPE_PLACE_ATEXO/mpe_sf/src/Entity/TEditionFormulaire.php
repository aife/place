<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TEditionFormulaire.
 *
 * @ORM\Table(name="t_edition_formulaire", indexes={@ORM\Index(name="t_dossier_form_t_edition_form_id_dossier_form_fk", columns={"id_dossier_formulaire"})})
 * @ORM\Entity
 */
class TEditionFormulaire
{
    /**
     * @return int
     */
    public function getIdEditionFormulaire()
    {
        return $this->idEditionFormulaire;
    }

    /**
     * @param int $idEditionFormulaire
     */
    public function setIdEditionFormulaire($idEditionFormulaire)
    {
        $this->idEditionFormulaire = $idEditionFormulaire;
    }

    /**
     *
     * @ORM\Column(name="id_edition_formulaire", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $idEditionFormulaire = null;

    /**
     * @ORM\Column(name="cle_externe_edition", type="integer", nullable=false)
     */
    private ?int $cleExterneEdition = null;

    /**
     * @ORM\Column(name="libelle", type="string", length=255, nullable=false)
     */
    private ?string $libelle = null;

    /**
     * @ORM\Column(name="extension", type="string", length=3, nullable=false)
     */
    private ?string $extension = null;

    /**
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private ?string $path = null;

    /**
     * @ORM\Column(name="hash", type="string", length=40, nullable=true)
     */
    private ?string $hash = null;

    /**
     * @ORM\Column(name="signature", type="text", length=65535, nullable=true)
     */
    private ?string $signature = null;

    /**
     * @ORM\Column(name="date_generation", type="string", length=255, nullable=true)
     */
    private ?string $dateGeneration = null;

    /**
     * @ORM\Column(name="date_depot", type="string", length=255, nullable=true)
     */
    private ?string $dateDepot = null;

    /**
     * @ORM\Column(name="statut_generation", type="integer", nullable=false)
     */
    private ?int $statutGeneration = null;

    /**
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private ?int $type = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string", length=255, nullable=false)
     */
    private ?string $nomFichier = null;

    /**
     * @return int
     */
    public function getCleExterneEdition()
    {
        return $this->cleExterneEdition;
    }

    /**
     * @param int $cleExterneEdition
     */
    public function setCleExterneEdition($cleExterneEdition)
    {
        $this->cleExterneEdition = $cleExterneEdition;
    }

    /**
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param string $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return string
     */
    public function getDateGeneration()
    {
        return $this->dateGeneration;
    }

    /**
     * @param string $dateGeneration
     */
    public function setDateGeneration($dateGeneration)
    {
        $this->dateGeneration = $dateGeneration;
    }

    /**
     * @return string
     */
    public function getDateDepot()
    {
        return $this->dateDepot;
    }

    /**
     * @param string $dateDepot
     */
    public function setDateDepot($dateDepot)
    {
        $this->dateDepot = $dateDepot;
    }

    /**
     * @return int
     */
    public function getStatutGeneration()
    {
        return $this->statutGeneration;
    }

    /**
     * @param int $statutGeneration
     */
    public function setStatutGeneration($statutGeneration)
    {
        $this->statutGeneration = $statutGeneration;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param string $nomFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    /**
     * @return TDossierFormulaire
     */
    public function getTDossierFormulaire()
    {
        return $this->tDossierFormulaire;
    }

    /**
     * @param TDossierFormulaire $tDossierFormulaire
     */
    public function setTDossierFormulaire($tDossierFormulaire)
    {
        $this->tDossierFormulaire = $tDossierFormulaire;
    }

    /**
     * @return int
     */
    public function getIdDossierFormulaire()
    {
        return $this->idDossierFormulaire;
    }

    /**
     * @param int $idDossierFormulaire
     */
    public function setIdDossierFormulaire($idDossierFormulaire)
    {
        $this->idDossierFormulaire = $idDossierFormulaire;
    }

    /**
     * @var TDossierFormulaire
     *
     * @ORM\ManyToOne(targetEntity="TDossierFormulaire", inversedBy="tEditionFormulaires")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_dossier_formulaire", referencedColumnName="id_dossier_formulaire")
     * })*/
    private $tDossierFormulaire;

    /**
     * @ORM\Column(name="id_dossier_formulaire", type="integer", nullable=true)
     */
    private ?int $idDossierFormulaire = null;
}
