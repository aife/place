<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="Admissibilite_Enveloppe_Lot", indexes={@ORM\Index(name="offre_id",
 *     columns={"offre_id", "organisme", "sous_pli"})})
 * @ORM\Entity(repositoryClass="App\Repository\AdmissibiliteEnveloppeLotRepository")
 */
class AdmissibiliteEnveloppeLot
{
    /**
     * @ORM\Column(name="id_Offre", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $offreId;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme;

    /**
     * @ORM\Column(name="sous_pli", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $sousPli;

    /**
     * @ORM\Column(name="admissibilite", type="integer", nullable=false)
     */
    private int $admissibilite;

    /**
     * @ORM\Column(name="type_enveloppe ", type="integer", nullable=false)
     */
    private int $typeEnveloppe;

    /**
     * @ORM\Column(name="commentaire", type="string")
     */
    private ?string $commentaire;

    public function getOffreId(): int
    {
        return $this->offreId;
    }

    public function setOffreId(int $offreId): void
    {
        $this->offreId = $offreId;
    }

    public function getOrganisme(): Organisme
    {
        return $this->organisme;
    }

    public function setOrganisme(Organisme $organisme): void
    {
        $this->organisme = $organisme;
    }

    public function getSousPli(): int
    {
        return $this->sousPli;
    }

    public function setSousPli(int $sousPli): void
    {
        $this->sousPli = $sousPli;
    }

    public function getAdmissibilite(): int
    {
        return $this->admissibilite;
    }

    public function setAdmissibilite(int $admissibilite): void
    {
        $this->admissibilite = $admissibilite;
    }

    public function getTypeEnveloppe(): int
    {
        return $this->typeEnveloppe;
    }

    public function setTypeEnveloppe(int $typeEnveloppe): void
    {
        $this->typeEnveloppe = $typeEnveloppe;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): void
    {
        $this->commentaire = $commentaire;
    }
}
