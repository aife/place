<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class SsoTiers.
 *
 * @ORM\Table(name="Sso_Tiers")
 * @ORM\Entity(repositoryClass="App\Repository\SsoTiersRepository")
 */
class SsoTiers
{
    /**
     * @var string
     *
     * @ORM\Column(name="id_sso_tiers", type="string", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idSsoTiers;

    /**
     * @var int
     *
     * @ORM\Column(name="id_tiers", type="integer", nullable=false)
     */
    private $idTiers;

    /**
     * @var string
     *
     * @ORM\Column(name="id_fonctionnalite", type="string", nullable=false)
     */
    private $idFonctionnalite;

    /**
     * @var string
     *
     * @ORM\Column(name="date_connexion", type="string", nullable=false)
     */
    private $dateConnexion;

    /**
     * @var string
     *
     * @ORM\Column(name="date_last_request", type="string", nullable=false)
     */
    private $dateLastRequest;

    /**
     * @return string
     */
    public function getIdSsoTiers()
    {
        return $this->idSsoTiers;
    }

    /**
     * @param $idSsoTiers
     *
     * @return $this
     */
    public function setIdSsoTiers($idSsoTiers)
    {
        $this->idSsoTiers = $idSsoTiers;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdTiers()
    {
        return $this->idTiers;
    }

    /**
     * @param $idTiers
     *
     * @return $this
     */
    public function setIdTiers($idTiers)
    {
        $this->idTiers = $idTiers;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdFonctionnalite()
    {
        return $this->idFonctionnalite;
    }

    /**
     * @param $idFonctionnalite
     *
     * @return $this
     */
    public function setIdFonctionnalite($idFonctionnalite)
    {
        $this->idFonctionnalite = $idFonctionnalite;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateConnexion()
    {
        return $this->dateConnexion;
    }

    /**
     * @param $dateConnexion
     *
     * @return $this
     */
    public function setDateConnexion($dateConnexion)
    {
        $this->dateConnexion = $dateConnexion;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateLastRequest()
    {
        return $this->dateLastRequest;
    }

    /**
     * @param $dateLastRequest
     *
     * @return $this
     */
    public function setDateLastRequest($dateLastRequest)
    {
        $this->dateLastRequest = $dateLastRequest;

        return $this;
    }
}
