<?php

namespace App\Entity;

use DateTime;
use App\Traits\Entity\PlateformeVirtuelleLinkTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Telechargement.
 *
 * @ORM\Table(name="Telechargement", indexes={@ORM\Index(name="organisme", columns={"organisme"}), @ORM\Index(name="consultation_id", columns={"consultation_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\TelechargementRepository")
 */
class Telechargement
{
    use PlateformeVirtuelleLinkTrait;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="telechargements")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="telechargements")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\Column(name="datetelechargement", type="string", length=20, nullable=false)
     */
    private string|DateTime $dateTelechargement = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="tirage_plan", type="integer", nullable=false)
     */
    private string|int $tiragePlan = '0';

    /**
     * @ORM\Column(name="id_inscrit", type="integer", nullable=true)
     */
    private ?int $idInscrit = null;

    /**
     * @ORM\Column(name="id_entreprise", type="integer", nullable=false)
     */
    private string|int $idEntreprise = '0';

    /**
     * @ORM\Column(name="support", type="integer")
     */
    private ?int $support = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)*/
    private $nom;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)*/
    private $adresse;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)*/
    private $email;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="entreprise", type="string", length=100, nullable=false)*/
    private $entreprise;

    /**
     * @ORM\Column(name="codepostal", type="string", length=5, nullable=false)
     */
    private string|DateTime $codepostal = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=false)*/
    private $ville;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=true)*/
    private $pays;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="siret", type="string", length=14, nullable=false)*/
    private $siret;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="fax", type="string", length=30, nullable=false)*/
    private $fax;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="lots", type="string", length=255, nullable=true)*/
    private $lots = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="sirenEtranger", type="string", length=20, nullable=true)*/
    private $sirenEtranger = '0';

    /**
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=false)
     */
    private string|DateTime $adresse2 = '0';

    /**
     * @var DateTime
     *
     * @ORM\Column(name="prenom", type="string", length=11, nullable=true)*/
    private $prenom;

    /**
     * @ORM\Column(name="noms_fichiers_dce", type="text", length=65535, nullable=true)
     */
    private ?string $nomsFichiersDce = null;

    /**
     * @ORM\Column(name="Observation", type="text", length=65535, nullable=true)
     */
    private ?string $observation = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)*/
    private $acronymePays;

    /**
     * @ORM\Column(name="poids_telechargement", type="integer", nullable=false)
     */
    private string|int $poidsTelechargement = '0';

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return DateTime
     */
    public function getDateTelechargement()
    {
        return $this->dateTelechargement;
    }

    /**
     * @param DateTime $dateTelechargement
     */
    public function setDateTelechargement($dateTelechargement)
    {
        $this->dateTelechargement = $dateTelechargement;
    }

    /**
     * @return int
     */
    public function getTiragePlan()
    {
        return $this->tiragePlan;
    }

    /**
     * @param int $tiragePlan
     */
    public function setTiragePlan($tiragePlan)
    {
        $this->tiragePlan = $tiragePlan;
    }

    /**
     * @return int|null
     */
    public function getIdInscrit(): ?int
    {
        return $this->idInscrit;
    }

    /**
     * @param int|null $idInscrit
     */
    public function setIdInscrit(?int $idInscrit)
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return int
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * @param int $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    /**
     * @return int
     */
    public function getSupport()
    {
        return $this->support;
    }

    /**
     * @param int $support
     */
    public function setSupport($support)
    {
        $this->support = $support;
    }

    /**
     * @return DateTime
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param DateTime $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return DateTime
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param DateTime $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return DateTime
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param DateTime $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return DateTime
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param DateTime $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return DateTime
     */
    public function getCodepostal()
    {
        return $this->codepostal;
    }

    /**
     * @param DateTime $codepostal
     */
    public function setCodepostal($codepostal)
    {
        $this->codepostal = $codepostal;
    }

    /**
     * @return DateTime
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param DateTime $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return DateTime
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * @param DateTime $pays
     */
    public function setPays($pays)
    {
        $this->pays = $pays;
    }

    /**
     * @return DateTime
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * @param DateTime $siret
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;
    }

    /**
     * @return DateTime
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param DateTime $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return DateTime
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @param DateTime $lots
     */
    public function setLots($lots)
    {
        $this->lots = $lots;
    }

    /**
     * @return DateTime
     */
    public function getSirenEtranger()
    {
        return $this->sirenEtranger;
    }

    /**
     * @param DateTime $sirenEtranger
     */
    public function setSirenEtranger($sirenEtranger)
    {
        $this->sirenEtranger = $sirenEtranger;
    }

    /**
     * @return DateTime
     */
    public function getAdresse2()
    {
        return $this->adresse2;
    }

    /**
     * @param DateTime $adresse2
     */
    public function setAdresse2($adresse2)
    {
        $this->adresse2 = $adresse2;
    }

    /**
     * @return DateTime
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param DateTime $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getNomsFichiersDce()
    {
        return $this->nomsFichiersDce;
    }

    /**
     * @param string $nomsFichiersDce
     */
    public function setNomsFichiersDce($nomsFichiersDce)
    {
        $this->nomsFichiersDce = $nomsFichiersDce;
    }

    /**
     * @return string
     */
    public function getObservation()
    {
        return $this->observation;
    }

    /**
     * @param string $observation
     */
    public function setObservation($observation)
    {
        $this->observation = $observation;
    }

    /**
     * @return DateTime
     */
    public function getAcronymePays()
    {
        return $this->acronymePays;
    }

    /**
     * @param DateTime $acronymePays
     */
    public function setAcronymePays($acronymePays)
    {
        $this->acronymePays = $acronymePays;
    }

    /**
     * @return int
     */
    public function getPoidsTelechargement()
    {
        return $this->poidsTelechargement;
    }

    /**
     * @param int $poidsTelechargement
     */
    public function setPoidsTelechargement($poidsTelechargement)
    {
        $this->poidsTelechargement = $poidsTelechargement;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param Organisme $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return Consultation
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * @return Telechargement
     */
    public function setConsultation(Consultation $consultation)
    {
        $this->consultation = $consultation;

        return $this;
    }
}
