<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="FormXmlDestinataireOpoce")
 * @ORM\Entity(repositoryClass="App\Repository\FormXmlDestinataireOpoceRepository")
 */
class FormXmlDestinataireOpoce
{
    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="id_destinataire_opoce", type="integer")
     */
    private ?int $idDestinataireOpoce = null;

    /**
     * @ORM\Column(name="xml", type="text")
     */
    private ?string $xml = null;

    /**
     * @var string
     *
     * @ORM\Column(name="code_retour", type="string", nullable=true)
     */
    private $codeRetour = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="message_retour", type="string", nullable=true)
     */
    private $messageRetour = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="id_joue", type="string", nullable=true)
     */
    private $idJoue = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="date_pub_joue", type="string", nullable=true)
     */
    private $datePubJoue = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="lien_publication", type="string", nullable=true)
     */
    private $lienPublication = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="no_doc_ext", type="string", nullable=true)
     */
    private $noDocExt = 0;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param string $organisme
     *
     * @return $this
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdDestinataireOpoce()
    {
        return $this->idDestinataireOpoce;
    }

    /**
     * @param int $idDestinataireOpoce
     *
     * @return $this
     */
    public function setIdDestinataireOpoce($idDestinataireOpoce)
    {
        $this->idDestinataireOpoce = $idDestinataireOpoce;

        return $this;
    }

    /**
     * @return string
     */
    public function getXml()
    {
        return $this->xml;
    }

    /**
     * @param string $xml
     *
     * @return $this
     */
    public function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeRetour()
    {
        return $this->codeRetour;
    }

    /**
     * @param string $codeRetour
     *
     * @return $this
     */
    public function setCodeRetour($codeRetour)
    {
        $this->codeRetour = $codeRetour;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessageRetour()
    {
        return $this->messageRetour;
    }

    /**
     * @param string $messageRetour
     *
     * @return $this
     */
    public function setMessageRetour($messageRetour)
    {
        $this->messageRetour = $messageRetour;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdJoue()
    {
        return $this->idJoue;
    }

    /**
     * @param string $idJoue
     *
     * @return $this
     */
    public function setIdJoue($idJoue)
    {
        $this->idJoue = $idJoue;

        return $this;
    }

    /**
     * @return string
     */
    public function getDatePubJoue()
    {
        return $this->datePubJoue;
    }

    /**
     * @param string $datePubJoue
     *
     * @return $this
     */
    public function setDatePubJoue($datePubJoue)
    {
        $this->datePubJoue = $datePubJoue;

        return $this;
    }

    /**
     * @return string
     */
    public function getLienPublication()
    {
        return $this->lienPublication;
    }

    /**
     * @param string $lienPublication
     *
     * @return $this
     */
    public function setLienPublication($lienPublication)
    {
        $this->lienPublication = $lienPublication;

        return $this;
    }

    /**
     * @return string
     */
    public function getNoDocExt()
    {
        return $this->noDocExt;
    }

    /**
     * @param string $noDocExt
     *
     * @return $this
     */
    public function setNoDocExt($noDocExt)
    {
        $this->noDocExt = $noDocExt;

        return $this;
    }
}
