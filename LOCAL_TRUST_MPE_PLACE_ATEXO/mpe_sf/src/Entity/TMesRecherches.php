<?php

namespace App\Entity;

use DateTime;
use App\Traits\Entity\PlateformeVirtuelleLinkTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * TMesRecherches.
 *
 * @ORM\Table(name="T_MesRecherches")
 * @ORM\Entity(repositoryClass="App\Repository\TMesRecherchesRepository")
 */
class TMesRecherches
{
    use PlateformeVirtuelleLinkTrait;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="id_createur", type="integer")
     */
    private ?int $idCreateur = null;

    /**
     * @ORM\Column(name="type_createur", type="string", length=20)
     */
    private ?string $typeCreateur = null;

    /**
     * @ORM\Column(name="denomination", type="string", length=200)
     */
    private ?string $denomination = null;

    /**
     * @ORM\Column(name="periodicite", type="string", length=1)
     */
    private ?string $periodicite = null;

    /**
     * @ORM\Column(name="xmlCriteria", type="text")
     */
    private ?string $xmlCriteria = null;

    /**
     * @ORM\Column(name="categorie", type="string", length=30)
     */
    private ?string $categorie = null;

    /**
     * @ORM\Column(name="id_initial", type="integer")
     */
    private ?int $idInitial = null;

    /**
     * @ORM\Column(name="format", type="string", length=3)
     */
    private ?string $format = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime")*/
    private $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime")*/
    private $dateModification;

    /**
     * @ORM\Column(name="recherche", type="string", columnDefinition="ENUM('0', '1')")
     */
    private ?string $recherche = null;

    /**
     * @ORM\Column(name="alerte", type="string", columnDefinition="ENUM('0', '1')")
     */
    private ?string $alerte = null;

    /**
     * @ORM\Column(name="type_avis", type="integer", length=2)
     */
    private ?int $typeAvis = null;

    /**
     * @ORM\Column(name="plateforme_virtuelle_id", type="integer")
     */
    private ?int $platformeVirtuelleId = null;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $criteria = null;

    /**
     * @ORM\Column(type="string", nullable=true , length=32)
     */
    private ?string $hashedCriteria = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getIdCreateur(): int
    {
        return $this->idCreateur;
    }

    public function setIdCreateur(int $idCreateur)
    {
        $this->idCreateur = $idCreateur;
    }

    public function getTypeCreateur(): string
    {
        return $this->typeCreateur;
    }

    public function setTypeCreateur(string $typeCreateur)
    {
        $this->typeCreateur = $typeCreateur;
    }

    public function getDenomination(): string
    {
        return $this->denomination;
    }

    public function setDenomination(string $denomination)
    {
        $this->denomination = $denomination;
    }

    public function getPeriodicite(): string
    {
        return $this->periodicite;
    }

    public function setPeriodicite(string $periodicite)
    {
        $this->periodicite = $periodicite;
    }

    public function getXmlCriteria(): string
    {
        return $this->xmlCriteria;
    }

    public function setXmlCriteria(string $xmlCriteria)
    {
        $this->xmlCriteria = $xmlCriteria;
    }

    public function getCategorie(): string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie)
    {
        $this->categorie = $categorie;
    }

    public function getIdInitial(): int
    {
        return $this->idInitial;
    }

    public function setIdInitial(int $idInitial)
    {
        $this->idInitial = $idInitial;
    }

    public function getFormat(): string
    {
        return $this->format;
    }

    public function setFormat(string $format)
    {
        $this->format = $format;
    }

    public function getDateCreation(): DateTime
    {
        return $this->dateCreation;
    }

    public function setDateCreation(DateTime $dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }

    public function getDateModification(): DateTime
    {
        return $this->dateModification;
    }

    public function setDateModification(DateTime $dateModification)
    {
        $this->dateModification = $dateModification;
    }

    public function getRecherche(): string
    {
        return $this->recherche;
    }

    public function setRecherche(string $recherche)
    {
        $this->recherche = $recherche;
    }

    public function getAlerte(): string
    {
        return $this->alerte;
    }

    public function setAlerte(string $alerte)
    {
        $this->alerte = $alerte;
    }

    public function getTypeAvis(): int
    {
        return $this->typeAvis;
    }

    public function setTypeAvis(int $typeAvis)
    {
        $this->typeAvis = $typeAvis;
    }

    public function getPlatformeVirtuelleId(): ?int
    {
        return $this->platformeVirtuelleId;
    }

    public function setPlatformeVirtuelleId(int $platformeVirtuelleId): self
    {
        $this->platformeVirtuelleId = $platformeVirtuelleId;

        return $this;
    }

    public function setCriteria(array $criteria): self
    {
        $this->criteria = $criteria;

        return $this;
    }

    public function getCriteria(): array
    {
        return $this->criteria;
    }

    public function setHashedCriteria(string $hashedCriteria): self
    {
        $this->hashedCriteria = $hashedCriteria;

        return $this;
    }

    public function getHashedCriteria(): string
    {
        return $this->hashedCriteria;
    }
}
