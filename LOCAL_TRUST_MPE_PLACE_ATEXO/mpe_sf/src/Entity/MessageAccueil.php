<?php

namespace App\Entity;

use App\Repository\MessageAccueilRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="t_message_accueil" )
 * @ORM\Entity(repositoryClass=MessageAccueilRepository::class)
 */
class MessageAccueil
{
    public const TYPE_MESSAGE_INFO    = 'info';
    public const TYPE_MESSAGE_WARNING = 'avertissement';
    public const TYPE_MESSAGE_ERROR   = 'erreur';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=256)
     */
    private ?string $typeMessage = null;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private ?string $destinataire = null;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $authentifier = null;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private ?string $config = null;

    /**
     * @ORM\Column(type="text")
     */
    private ?string $contenu = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeMessage(): ?string
    {
        return $this->typeMessage;
    }

    public function setTypeMessage(string $typeMessage): self
    {
        $this->typeMessage = $typeMessage;

        return $this;
    }

    public function getDestinataire(): ?string
    {
        return $this->destinataire;
    }

    public function setDestinataire(string $destinataire): self
    {
        $this->destinataire = $destinataire;

        return $this;
    }

    public function getAuthentifier(): ?int
    {
        return $this->authentifier;
    }

    public function setAuthentifier(?int $authentifier): self
    {
        $this->authentifier = $authentifier;

        return $this;
    }

    public function getConfig(): ?string
    {
        return $this->config;
    }

    public function setConfig(?string $config): self
    {
        $this->config = $config;

        return $this;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function __toString(): string
    {
        return $this->typeMessage;
    }
}
