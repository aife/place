<?php

namespace App\Entity;

use Exception;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Input\ServiceInput;
use App\Dto\Output\ServiceOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Service.
 *
 * @ORM\Table(name="Service", indexes={@ORM\Index(name="organisme", columns={"organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\ServiceRepository")
 */
#[ApiResource(
    collectionOperations: ['get', 'post'],
    itemOperations: ['get', 'put', 'patch'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input: ServiceInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ServiceOutput::class,
    routePrefix: '/referentiels'
)]
#[ApiFilter(SearchFilter::class, properties: ['idExterne' => 'exact', 'organisme' => 'exact', 'oldId' => 'exact'])]
class Service
{
    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => true,
            'migrationService' => 'atexo.service.migration.organisme'
        ],
    ];

    private EntityManager $em;

    private int $depth = 0;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="service")
     * @ORM\JoinColumn(name="id", referencedColumnName="serviceId")
     */
    private Collection $consultations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\WS\AgentTechniqueAssociation", mappedBy="service")
     * @ORM\JoinColumn(name="id", referencedColumnName="service_id", nullable=true),
     */
    private Collection $agentTechAssotiations;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="services")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="Agent", mappedBy="service")
     * @ORM\JoinColumn(name="id", referencedColumnName="service_id")
     */
    private Collection $agents;

    /**
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")*/
    #[Groups('webservice')]
    private ?int $id;

    /**
     *
     * @ORM\Column(name="old_id", type="integer", nullable=true)
     */
    private ?int $oldId;

    /**
     * @var string
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\OneToMany(targetEntity="App\Entity\Organisme", mappedBy="services", fetch="EAGER")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private string $acronymeOrg;

    /**
     * @ORM\Column(name="type_service", type="string", nullable=false)
     */
    private string $typeService = '2';

    /**
     *
     * @ORM\Column(name="libelle", type="text", length=65535, nullable=false)*/
    #[Groups('webservice')]
    private ?string $libelle = null;

    /**
     *
     * @ORM\Column(name="sigle", type="text", length=65535, nullable=false)*/
    #[Groups('webservice')]
    private ?string $sigle = null;

    /**
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)
     */
    private string $adresse = '';

    /**
     * @ORM\Column(name="adresse_suite", type="string", length=100, nullable=false)
     */
    private string $adresseSuite = '';

    /**
     * @ORM\Column(name="cp", type="string", length=5, nullable=false)
     */
    private string $cp = '';

    /**
     * @ORM\Column(name="ville", type="string", length=100, nullable=false)
     */
    private string $ville = '';

    /**
     * @ORM\Column(name="telephone", type="string", length=100, nullable=false)
     */
    private string $telephone = '';

    /**
     * @ORM\Column(name="fax", type="string", length=100, nullable=false)
     */
    private string $fax = '';

    /**
     * @ORM\Column(name="mail", type="string", length=100, nullable=false)
     */
    private string $mail = '';

    /**
     * @ORM\Column(name="pays", type="string", length=150, nullable=true)
     */
    private ?string $pays = null;

    /**
     *
     * @ORM\Column(name="id_externe", type="string", length=50, nullable=false)*/
    #[Groups('webservice')]
    private ?string $idExterne = null;

    /**
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=true)
     */
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_modification", type="string", length=20, nullable=true)
     */
    private ?string $dateModification = null;

    /**
     *
     * @ORM\Column(name="siren", type="string", length=9, nullable=true)*/
    #[Groups('webservice')]
    private ?string $siren = null;

    /**
     *
     * @ORM\Column(name="complement", type="string", length=5, nullable=true)*/
    #[Groups('webservice')]
    private ?string $complement = null;

    /**
     * @ORM\Column(name="libelle_ar", type="text", length=65535, nullable=false)
     */
    private string $libelleAr = '';

    /**
     * @ORM\Column(name="adresse_ar", type="string", length=100, nullable=false)
     */
    private string $adresseAr = '';

    /**
     * @ORM\Column(name="adresse_suite_ar", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteAr = '';

    /**
     * @ORM\Column(name="ville_ar", type="string", length=100, nullable=false)
     */
    private string $villeAr = '';

    /**
     * @ORM\Column(name="pays_ar", type="string", length=150, nullable=false)
     */
    private string $paysAr = 'NULL';

    /**
     * @ORM\Column(name="libelle_fr", type="text", length=65535, nullable=false)
     */
    private string $libelleFr = '';

    /**
     * @ORM\Column(name="adresse_fr", type="string", length=100, nullable=false)
     */
    private string $adresseFr = '';

    /**
     * @ORM\Column(name="adresse_suite_fr", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteFr = '';

    /**
     * @ORM\Column(name="ville_fr", type="string", length=100, nullable=false)
     */
    private string $villeFr = '';

    /**
     * @ORM\Column(name="pays_fr", type="string", length=150, nullable=false)
     */
    private string $paysFr = 'NULL';

    /**
     * @ORM\Column(name="libelle_es", type="text", length=65535, nullable=false)
     */
    private string $libelleEs = '';

    /**
     * @ORM\Column(name="adresse_es", type="string", length=100, nullable=false)
     */
    private string $adresseEs = '';

    /**
     * @ORM\Column(name="adresse_suite_es", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteEs = '';

    /**
     * @ORM\Column(name="ville_es", type="string", length=100, nullable=false)
     */
    private string $villeEs = '';

    /**
     * @ORM\Column(name="pays_es", type="string", length=150, nullable=false)
     */
    private string $paysEs = 'NULL';

    /**
     * @ORM\Column(name="libelle_en", type="text", length=65535, nullable=false)
     */
    private string $libelleEn = '';

    /**
     * @ORM\Column(name="adresse_en", type="string", length=100, nullable=false)
     */
    private string $adresseEn = '';

    /**
     * @ORM\Column(name="adresse_suite_en", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteEn = '';

    /**
     * @ORM\Column(name="ville_en", type="string", length=100, nullable=false)
     */
    private string $villeEn = '';

    /**
     * @ORM\Column(name="pays_en", type="string", length=150, nullable=false)
     */
    private string $paysEn = 'NULL';

    /**
     * @ORM\Column(name="libelle_su", type="text", length=65535, nullable=false)
     */
    private string $libelleSu = '';

    /**
     * @ORM\Column(name="adresse_su", type="string", length=100, nullable=false)
     */
    private string $adresseSu = '';

    /**
     * @ORM\Column(name="adresse_suite_su", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteSu = '';

    /**
     * @ORM\Column(name="ville_su", type="string", length=100, nullable=false)
     */
    private string $villeSu = '';

    /**
     * @ORM\Column(name="pays_su", type="string", length=150, nullable=false)
     */
    private string $paysSu = 'NULL';

    /**
     * @ORM\Column(name="libelle_du", type="text", length=65535, nullable=false)
     */
    private string $libelleDu = '';

    /**
     * @ORM\Column(name="adresse_du", type="string", length=100, nullable=false)
     */
    private string $adresseDu = '';

    /**
     * @ORM\Column(name="adresse_suite_du", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteDu = '';

    /**
     * @ORM\Column(name="ville_du", type="string", length=100, nullable=false)
     */
    private string $villeDu = '';

    /**
     * @ORM\Column(name="pays_du", type="string", length=150, nullable=false)
     */
    private string $paysDu = 'NULL';

    /**
     * @ORM\Column(name="libelle_cz", type="text", length=65535, nullable=false)
     */
    private string $libelleCz = '';

    /**
     * @ORM\Column(name="adresse_cz", type="string", length=100, nullable=false)
     */
    private string $adresseCz = '';

    /**
     * @ORM\Column(name="adresse_suite_cz", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteCz = '';

    /**
     * @ORM\Column(name="ville_cz", type="string", length=100, nullable=false)
     */
    private string $villeCz = '';

    /**
     * @ORM\Column(name="pays_cz", type="string", length=150, nullable=false)
     */
    private string $paysCz = '';

    /**
     * @ORM\Column(name="libelle_it", type="text", length=65535, nullable=false)
     */
    private string $libelleIt = '';

    /**
     * @ORM\Column(name="adresse_it", type="string", length=100, nullable=false)
     */
    private string $adresseIt = '';

    /**
     * @ORM\Column(name="adresse_suite_it", type="string", length=100, nullable=false)
     */
    private string $adresseSuiteIt = '';

    /**
     * @ORM\Column(name="ville_it", type="string", length=100, nullable=false)
     */
    private string $villeIt = '';

    /**
     * @ORM\Column(name="pays_it", type="string", length=150, nullable=false)
     */
    private string $paysIt = 'NULL';

    /**
     * @ORM\Column(name="chemin_complet", type="string", length=255, nullable=false)
     */
    private string $cheminComplet = '';

    /**
     * @ORM\Column(name="chemin_complet_fr", type="string", length=255, nullable=false)
     */
    private string $cheminCompletFr = '';

    /**
     * @ORM\Column(name="chemin_complet_en", type="string", length=255, nullable=false)
     */
    private string $cheminCompletEn = '';

    /**
     * @ORM\Column(name="chemin_complet_es", type="string", length=255, nullable=false)
     */
    private string $cheminCompletEs = '';

    /**
     * @ORM\Column(name="chemin_complet_su", type="string", length=255, nullable=false)
     */
    private string $cheminCompletSu = '';

    /**
     * @ORM\Column(name="chemin_complet_du", type="string", length=255, nullable=false)
     */
    private string $cheminCompletDu = '';

    /**
     * @ORM\Column(name="chemin_complet_cz", type="string", length=255, nullable=false)
     */
    private string $cheminCompletCz = '';

    /**
     * @ORM\Column(name="chemin_complet_ar", type="string", length=255, nullable=false)
     */
    private string $cheminCompletAr = '';

    /**
     * @ORM\Column(name="chemin_complet_it", type="string", length=255, nullable=false)
     */
    private string $cheminCompletIt = '';

    /**
     * @ORM\Column(name="nom_service_archiveur", type="string", length=100, nullable=true)
     */
    private ?string $nomServiceArchiveur = null;

    /**
     * @ORM\Column(name="identifiant_service_archiveur", type="string", length=100, nullable=true)
     */
    private ?string $identifiantServiceArchiveur = null;

    /**
     * @ORM\Column(name="affichage_service", type="string", nullable=false)
     */
    private string $affichageService = '1';

    /**
     * @ORM\Column(name="activation_fuseau_horaire", type="string", nullable=false)
     */
    private string $activationFuseauHoraire = '0';

    /**
     * @ORM\Column(name="decalage_horaire", type="string", length=5, nullable=true)
     */
    private ?string $decalageHoraire = null;

    /**
     * @ORM\Column(name="lieu_residence", type="string", length=255, nullable=true)
     */
    private ?string $lieuResidence = null;

    /**
     * @ORM\Column(name="alerte", type="string", nullable=false)
     */
    private string $alerte = '0';

    /**
     * @ORM\Column(name="acces_chorus", type="string", nullable=false)
     */
    private string $accesChorus = '0';

    /**
     *
     * @ORM\Column(name="forme_juridique", type="string", nullable=false)*/
    #[Groups('webservice')]
    private ?string $formeJuridique = null;

    /**
     *
     * @ORM\Column(name="forme_juridique_code", type="string", nullable=false)*/
    #[Groups('webservice')]
    private ?string $formeJuridiqueCode = null;

    /**
     * @ORM\Column(name="synchronisation_exec", type="string", nullable=false)
     */
    private string $synchronisationExec = '1';

    private $idParent;

    private $oldIdParent = null;

    private $idExterneParent;

    /**
     *
     * @ORM\Column(name="id_entite", type="integer", nullable=true)*/
    #[Groups('webservice')]
    private ?int $idEntite = null;

    /**
     * Set id.
     *
     * @param ?int $id
     *
     * @return Service
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $oldId
     * @return Service
     */
    public function setOldId(?int $oldId): Service
    {
        $this->oldId = $oldId;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOldId(): ?int
    {
        return $this->oldId;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param $value
     */
    public function setDepth($value)
    {
        $this->depth = $value;
    }

    /**
     * @return string
     */
    public function toString()
    {
        return 'Id=' . $this->getId() . ' ; Libellé=' . $this->getLibelle() . ' ; Profondeur=' . $this->getDepth();
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return Service
     */
    public function setAcronymeOrg($organisme)
    {
        $this->acronymeOrg = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getAcronymeOrg()
    {
        return $this->acronymeOrg;
    }

    /**
     * Set typeService.
     *
     * @param string $typeService
     *
     * @return Service
     */
    public function setTypeService($typeService)
    {
        $this->typeService = $typeService;

        return $this;
    }

    /**
     * Get typeService.
     *
     * @return string
     */
    public function getTypeService()
    {
        return $this->typeService;
    }

    /**
     * Set libelle.
     *
     * @param string $libelle
     *
     * @return Service
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle.
     *
     * @return string
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set sigle.
     *
     * @param string $sigle
     *
     * @return Service
     */
    public function setSigle($sigle)
    {
        $this->sigle = $sigle;

        return $this;
    }

    /**
     * Get sigle.
     *
     * @return string
     */
    public function getSigle()
    {
        return $this->sigle;
    }

    /**
     * Set adresse.
     *
     * @param string $adresse
     *
     * @return Service
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set adresseSuite.
     *
     * @param string $adresseSuite
     *
     * @return Service
     */
    public function setAdresseSuite($adresseSuite)
    {
        $this->adresseSuite = $adresseSuite;

        return $this;
    }

    /**
     * Get adresseSuite.
     *
     * @return string
     */
    public function getAdresseSuite()
    {
        return $this->adresseSuite;
    }

    /**
     * Set cp.
     *
     * @param string $cp
     *
     * @return Service
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp.
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set ville.
     *
     * @param string $ville
     *
     * @return Service
     */
    public function setVille($ville)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Service
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set fax.
     *
     * @param string $fax
     *
     * @return Service
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax.
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set mail.
     *
     * @param string $mail
     *
     * @return Service
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail.
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set pays.
     *
     * @param string $pays
     *
     * @return Service
     */
    public function setPays($pays)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays.
     *
     * @return string
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set idExterne.
     *
     * @param string $idExterne
     *
     * @return Service
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    /**
     * Get idExterne.
     *
     * @return string
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * Set dateCreation.
     *
     * @param string $dateCreation
     *
     * @return Service
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     *
     * @return Service
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set siren.
     *
     * @param string $siren
     *
     * @return Service
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren.
     *
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set complement.
     *
     * @param string $complement
     *
     * @return Service
     */
    public function setComplement($complement)
    {
        $this->complement = $complement;

        return $this;
    }

    /**
     * Get complement.
     *
     * @return string
     */
    public function getComplement()
    {
        return $this->complement;
    }

    /**
     * Set libelleAr.
     *
     * @param string $libelleAr
     *
     * @return Service
     */
    public function setLibelleAr($libelleAr)
    {
        $this->libelleAr = $libelleAr;

        return $this;
    }

    /**
     * Get libelleAr.
     *
     * @return string
     */
    public function getLibelleAr()
    {
        return $this->libelleAr;
    }

    /**
     * Set adresseAr.
     *
     * @param string $adresseAr
     *
     * @return Service
     */
    public function setAdresseAr($adresseAr)
    {
        $this->adresseAr = $adresseAr;

        return $this;
    }

    /**
     * Get adresseAr.
     *
     * @return string
     */
    public function getAdresseAr()
    {
        return $this->adresseAr;
    }

    /**
     * Set adresseSuiteAr.
     *
     * @param string $adresseSuiteAr
     *
     * @return Service
     */
    public function setAdresseSuiteAr($adresseSuiteAr)
    {
        $this->adresseSuiteAr = $adresseSuiteAr;

        return $this;
    }

    /**
     * Get adresseSuiteAr.
     *
     * @return string
     */
    public function getAdresseSuiteAr()
    {
        return $this->adresseSuiteAr;
    }

    /**
     * Set villeAr.
     *
     * @param string $villeAr
     *
     * @return Service
     */
    public function setVilleAr($villeAr)
    {
        $this->villeAr = $villeAr;

        return $this;
    }

    /**
     * Get villeAr.
     *
     * @return string
     */
    public function getVilleAr()
    {
        return $this->villeAr;
    }

    /**
     * Set paysAr.
     *
     * @param string $paysAr
     *
     * @return Service
     */
    public function setPaysAr($paysAr)
    {
        $this->paysAr = $paysAr;

        return $this;
    }

    /**
     * Get paysAr.
     *
     * @return string
     */
    public function getPaysAr()
    {
        return $this->paysAr;
    }

    /**
     * Set libelleFr.
     *
     * @param string $libelleFr
     *
     * @return Service
     */
    public function setLibelleFr($libelleFr)
    {
        $this->libelleFr = $libelleFr;

        return $this;
    }

    /**
     * Get libelleFr.
     *
     * @return string
     */
    public function getLibelleFr()
    {
        return $this->libelleFr;
    }

    /**
     * Set adresseFr.
     *
     * @param string $adresseFr
     *
     * @return Service
     */
    public function setAdresseFr($adresseFr)
    {
        $this->adresseFr = $adresseFr;

        return $this;
    }

    /**
     * Get adresseFr.
     *
     * @return string
     */
    public function getAdresseFr()
    {
        return $this->adresseFr;
    }

    /**
     * Set adresseSuiteFr.
     *
     * @param string $adresseSuiteFr
     *
     * @return Service
     */
    public function setAdresseSuiteFr($adresseSuiteFr)
    {
        $this->adresseSuiteFr = $adresseSuiteFr;

        return $this;
    }

    /**
     * Get adresseSuiteFr.
     *
     * @return string
     */
    public function getAdresseSuiteFr()
    {
        return $this->adresseSuiteFr;
    }

    /**
     * Set villeFr.
     *
     * @param string $villeFr
     *
     * @return Service
     */
    public function setVilleFr($villeFr)
    {
        $this->villeFr = $villeFr;

        return $this;
    }

    /**
     * Get villeFr.
     *
     * @return string
     */
    public function getVilleFr()
    {
        return $this->villeFr;
    }

    /**
     * Set paysFr.
     *
     * @param string $paysFr
     *
     * @return Service
     */
    public function setPaysFr($paysFr)
    {
        $this->paysFr = $paysFr;

        return $this;
    }

    /**
     * Get paysFr.
     *
     * @return string
     */
    public function getPaysFr()
    {
        return $this->paysFr;
    }

    /**
     * Set libelleEs.
     *
     * @param string $libelleEs
     *
     * @return Service
     */
    public function setLibelleEs($libelleEs)
    {
        $this->libelleEs = $libelleEs;

        return $this;
    }

    /**
     * Get libelleEs.
     *
     * @return string
     */
    public function getLibelleEs()
    {
        return $this->libelleEs;
    }

    /**
     * Set adresseEs.
     *
     * @param string $adresseEs
     *
     * @return Service
     */
    public function setAdresseEs($adresseEs)
    {
        $this->adresseEs = $adresseEs;

        return $this;
    }

    /**
     * Get adresseEs.
     *
     * @return string
     */
    public function getAdresseEs()
    {
        return $this->adresseEs;
    }

    /**
     * Set adresseSuiteEs.
     *
     * @param string $adresseSuiteEs
     *
     * @return Service
     */
    public function setAdresseSuiteEs($adresseSuiteEs)
    {
        $this->adresseSuiteEs = $adresseSuiteEs;

        return $this;
    }

    /**
     * Get adresseSuiteEs.
     *
     * @return string
     */
    public function getAdresseSuiteEs()
    {
        return $this->adresseSuiteEs;
    }

    /**
     * Set villeEs.
     *
     * @param string $villeEs
     *
     * @return Service
     */
    public function setVilleEs($villeEs)
    {
        $this->villeEs = $villeEs;

        return $this;
    }

    /**
     * Get villeEs.
     *
     * @return string
     */
    public function getVilleEs()
    {
        return $this->villeEs;
    }

    /**
     * Set paysEs.
     *
     * @param string $paysEs
     *
     * @return Service
     */
    public function setPaysEs($paysEs)
    {
        $this->paysEs = $paysEs;

        return $this;
    }

    /**
     * Get paysEs.
     *
     * @return string
     */
    public function getPaysEs()
    {
        return $this->paysEs;
    }

    /**
     * Set libelleEn.
     *
     * @param string $libelleEn
     *
     * @return Service
     */
    public function setLibelleEn($libelleEn)
    {
        $this->libelleEn = $libelleEn;

        return $this;
    }

    /**
     * Get libelleEn.
     *
     * @return string
     */
    public function getLibelleEn()
    {
        return $this->libelleEn;
    }

    /**
     * Set adresseEn.
     *
     * @param string $adresseEn
     *
     * @return Service
     */
    public function setAdresseEn($adresseEn)
    {
        $this->adresseEn = $adresseEn;

        return $this;
    }

    /**
     * Get adresseEn.
     *
     * @return string
     */
    public function getAdresseEn()
    {
        return $this->adresseEn;
    }

    /**
     * Set adresseSuiteEn.
     *
     * @param string $adresseSuiteEn
     *
     * @return Service
     */
    public function setAdresseSuiteEn($adresseSuiteEn)
    {
        $this->adresseSuiteEn = $adresseSuiteEn;

        return $this;
    }

    /**
     * Get adresseSuiteEn.
     *
     * @return string
     */
    public function getAdresseSuiteEn()
    {
        return $this->adresseSuiteEn;
    }

    /**
     * Set villeEn.
     *
     * @param string $villeEn
     *
     * @return Service
     */
    public function setVilleEn($villeEn)
    {
        $this->villeEn = $villeEn;

        return $this;
    }

    /**
     * Get villeEn.
     *
     * @return string
     */
    public function getVilleEn()
    {
        return $this->villeEn;
    }

    /**
     * Set paysEn.
     *
     * @param string $paysEn
     *
     * @return Service
     */
    public function setPaysEn($paysEn)
    {
        $this->paysEn = $paysEn;

        return $this;
    }

    /**
     * Get paysEn.
     *
     * @return string
     */
    public function getPaysEn()
    {
        return $this->paysEn;
    }

    /**
     * Set libelleSu.
     *
     * @param string $libelleSu
     *
     * @return Service
     */
    public function setLibelleSu($libelleSu)
    {
        $this->libelleSu = $libelleSu;

        return $this;
    }

    /**
     * Get libelleSu.
     *
     * @return string
     */
    public function getLibelleSu()
    {
        return $this->libelleSu;
    }

    /**
     * Set adresseSu.
     *
     * @param string $adresseSu
     *
     * @return Service
     */
    public function setAdresseSu($adresseSu)
    {
        $this->adresseSu = $adresseSu;

        return $this;
    }

    /**
     * Get adresseSu.
     *
     * @return string
     */
    public function getAdresseSu()
    {
        return $this->adresseSu;
    }

    /**
     * Set adresseSuiteSu.
     *
     * @param string $adresseSuiteSu
     *
     * @return Service
     */
    public function setAdresseSuiteSu($adresseSuiteSu)
    {
        $this->adresseSuiteSu = $adresseSuiteSu;

        return $this;
    }

    /**
     * Get adresseSuiteSu.
     *
     * @return string
     */
    public function getAdresseSuiteSu()
    {
        return $this->adresseSuiteSu;
    }

    /**
     * Set villeSu.
     *
     * @param string $villeSu
     *
     * @return Service
     */
    public function setVilleSu($villeSu)
    {
        $this->villeSu = $villeSu;

        return $this;
    }

    /**
     * Get villeSu.
     *
     * @return string
     */
    public function getVilleSu()
    {
        return $this->villeSu;
    }

    /**
     * Set paysSu.
     *
     * @param string $paysSu
     *
     * @return Service
     */
    public function setPaysSu($paysSu)
    {
        $this->paysSu = $paysSu;

        return $this;
    }

    /**
     * Get paysSu.
     *
     * @return string
     */
    public function getPaysSu()
    {
        return $this->paysSu;
    }

    /**
     * Set libelleDu.
     *
     * @param string $libelleDu
     *
     * @return Service
     */
    public function setLibelleDu($libelleDu)
    {
        $this->libelleDu = $libelleDu;

        return $this;
    }

    /**
     * Get libelleDu.
     *
     * @return string
     */
    public function getLibelleDu()
    {
        return $this->libelleDu;
    }

    /**
     * Set adresseDu.
     *
     * @param string $adresseDu
     *
     * @return Service
     */
    public function setAdresseDu($adresseDu)
    {
        $this->adresseDu = $adresseDu;

        return $this;
    }

    /**
     * Get adresseDu.
     *
     * @return string
     */
    public function getAdresseDu()
    {
        return $this->adresseDu;
    }

    /**
     * Set adresseSuiteDu.
     *
     * @param string $adresseSuiteDu
     *
     * @return Service
     */
    public function setAdresseSuiteDu($adresseSuiteDu)
    {
        $this->adresseSuiteDu = $adresseSuiteDu;

        return $this;
    }

    /**
     * Get adresseSuiteDu.
     *
     * @return string
     */
    public function getAdresseSuiteDu()
    {
        return $this->adresseSuiteDu;
    }

    /**
     * Set villeDu.
     *
     * @param string $villeDu
     *
     * @return Service
     */
    public function setVilleDu($villeDu)
    {
        $this->villeDu = $villeDu;

        return $this;
    }

    /**
     * Get villeDu.
     *
     * @return string
     */
    public function getVilleDu()
    {
        return $this->villeDu;
    }

    /**
     * Set paysDu.
     *
     * @param string $paysDu
     *
     * @return Service
     */
    public function setPaysDu($paysDu)
    {
        $this->paysDu = $paysDu;

        return $this;
    }

    /**
     * Get paysDu.
     *
     * @return string
     */
    public function getPaysDu()
    {
        return $this->paysDu;
    }

    /**
     * Set libelleCz.
     *
     * @param string $libelleCz
     *
     * @return Service
     */
    public function setLibelleCz($libelleCz)
    {
        $this->libelleCz = $libelleCz;

        return $this;
    }

    /**
     * Get libelleCz.
     *
     * @return string
     */
    public function getLibelleCz()
    {
        return $this->libelleCz;
    }

    /**
     * Set adresseCz.
     *
     * @param string $adresseCz
     *
     * @return Service
     */
    public function setAdresseCz($adresseCz)
    {
        $this->adresseCz = $adresseCz;

        return $this;
    }

    /**
     * Get adresseCz.
     *
     * @return string
     */
    public function getAdresseCz()
    {
        return $this->adresseCz;
    }

    /**
     * Set adresseSuiteCz.
     *
     * @param string $adresseSuiteCz
     *
     * @return Service
     */
    public function setAdresseSuiteCz($adresseSuiteCz)
    {
        $this->adresseSuiteCz = $adresseSuiteCz;

        return $this;
    }

    /**
     * Get adresseSuiteCz.
     *
     * @return string
     */
    public function getAdresseSuiteCz()
    {
        return $this->adresseSuiteCz;
    }

    /**
     * Set villeCz.
     *
     * @param string $villeCz
     *
     * @return Service
     */
    public function setVilleCz($villeCz)
    {
        $this->villeCz = $villeCz;

        return $this;
    }

    /**
     * Get villeCz.
     *
     * @return string
     */
    public function getVilleCz()
    {
        return $this->villeCz;
    }

    /**
     * Set paysCz.
     *
     * @param string $paysCz
     *
     * @return Service
     */
    public function setPaysCz($paysCz)
    {
        $this->paysCz = $paysCz;

        return $this;
    }

    /**
     * Get paysCz.
     *
     * @return string
     */
    public function getPaysCz()
    {
        return $this->paysCz;
    }

    /**
     * Set libelleIt.
     *
     * @param string $libelleIt
     *
     * @return Service
     */
    public function setLibelleIt($libelleIt)
    {
        $this->libelleIt = $libelleIt;

        return $this;
    }

    /**
     * Get libelleIt.
     *
     * @return string
     */
    public function getLibelleIt()
    {
        return $this->libelleIt;
    }

    /**
     * Set adresseIt.
     *
     * @param string $adresseIt
     *
     * @return Service
     */
    public function setAdresseIt($adresseIt)
    {
        $this->adresseIt = $adresseIt;

        return $this;
    }

    /**
     * Get adresseIt.
     *
     * @return string
     */
    public function getAdresseIt()
    {
        return $this->adresseIt;
    }

    /**
     * Set adresseSuiteIt.
     *
     * @param string $adresseSuiteIt
     *
     * @return Service
     */
    public function setAdresseSuiteIt($adresseSuiteIt)
    {
        $this->adresseSuiteIt = $adresseSuiteIt;

        return $this;
    }

    /**
     * Get adresseSuiteIt.
     *
     * @return string
     */
    public function getAdresseSuiteIt()
    {
        return $this->adresseSuiteIt;
    }

    /**
     * Set villeIt.
     *
     * @param string $villeIt
     *
     * @return Service
     */
    public function setVilleIt($villeIt)
    {
        $this->villeIt = $villeIt;

        return $this;
    }

    /**
     * Get villeIt.
     *
     * @return string
     */
    public function getVilleIt()
    {
        return $this->villeIt;
    }

    /**
     * Set paysIt.
     *
     * @param string $paysIt
     *
     * @return Service
     */
    public function setPaysIt($paysIt)
    {
        $this->paysIt = $paysIt;

        return $this;
    }

    /**
     * Get paysIt.
     *
     * @return string
     */
    public function getPaysIt()
    {
        return $this->paysIt;
    }

    /**
     * Set cheminComplet.
     *
     * @param string $cheminComplet
     *
     * @return Service
     */
    public function setCheminComplet($cheminComplet)
    {
        $this->cheminComplet = $cheminComplet;

        return $this;
    }

    /**
     * Get cheminComplet.
     *
     * @return string
     */
    public function getCheminComplet()
    {
        return $this->cheminComplet;
    }

    /**
     * Set cheminCompletFr.
     *
     * @param string $cheminCompletFr
     *
     * @return Service
     */
    public function setCheminCompletFr($cheminCompletFr)
    {
        $this->cheminCompletFr = $cheminCompletFr;

        return $this;
    }

    /**
     * Get cheminCompletFr.
     *
     * @return string
     */
    public function getCheminCompletFr()
    {
        return $this->cheminCompletFr;
    }

    /**
     * Set cheminCompletEn.
     *
     * @param string $cheminCompletEn
     *
     * @return Service
     */
    public function setCheminCompletEn($cheminCompletEn)
    {
        $this->cheminCompletEn = $cheminCompletEn;

        return $this;
    }

    /**
     * Get cheminCompletEn.
     *
     * @return string
     */
    public function getCheminCompletEn()
    {
        return $this->cheminCompletEn;
    }

    /**
     * Set cheminCompletEs.
     *
     * @param string $cheminCompletEs
     *
     * @return Service
     */
    public function setCheminCompletEs($cheminCompletEs)
    {
        $this->cheminCompletEs = $cheminCompletEs;

        return $this;
    }

    /**
     * Get cheminCompletEs.
     *
     * @return string
     */
    public function getCheminCompletEs()
    {
        return $this->cheminCompletEs;
    }

    /**
     * Set cheminCompletSu.
     *
     * @param string $cheminCompletSu
     *
     * @return Service
     */
    public function setCheminCompletSu($cheminCompletSu)
    {
        $this->cheminCompletSu = $cheminCompletSu;

        return $this;
    }

    /**
     * Get cheminCompletSu.
     *
     * @return string
     */
    public function getCheminCompletSu()
    {
        return $this->cheminCompletSu;
    }

    /**
     * Set cheminCompletDu.
     *
     * @param string $cheminCompletDu
     *
     * @return Service
     */
    public function setCheminCompletDu($cheminCompletDu)
    {
        $this->cheminCompletDu = $cheminCompletDu;

        return $this;
    }

    /**
     * Get cheminCompletDu.
     *
     * @return string
     */
    public function getCheminCompletDu()
    {
        return $this->cheminCompletDu;
    }

    /**
     * Set cheminCompletCz.
     *
     * @param string $cheminCompletCz
     *
     * @return Service
     */
    public function setCheminCompletCz($cheminCompletCz)
    {
        $this->cheminCompletCz = $cheminCompletCz;

        return $this;
    }

    /**
     * Get cheminCompletCz.
     *
     * @return string
     */
    public function getCheminCompletCz()
    {
        return $this->cheminCompletCz;
    }

    /**
     * Set cheminCompletAr.
     *
     * @param string $cheminCompletAr
     *
     * @return Service
     */
    public function setCheminCompletAr($cheminCompletAr)
    {
        $this->cheminCompletAr = $cheminCompletAr;

        return $this;
    }

    /**
     * Get cheminCompletAr.
     *
     * @return string
     */
    public function getCheminCompletAr()
    {
        return $this->cheminCompletAr;
    }

    /**
     * Set cheminCompletIt.
     *
     * @param string $cheminCompletIt
     *
     * @return Service
     */
    public function setCheminCompletIt($cheminCompletIt)
    {
        $this->cheminCompletIt = $cheminCompletIt;

        return $this;
    }

    /**
     * Get cheminCompletIt.
     *
     * @return string
     */
    public function getCheminCompletIt()
    {
        return $this->cheminCompletIt;
    }

    /**
     * Set nomServiceArchiveur.
     *
     * @param string $nomServiceArchiveur
     *
     * @return Service
     */
    public function setNomServiceArchiveur($nomServiceArchiveur)
    {
        $this->nomServiceArchiveur = $nomServiceArchiveur;

        return $this;
    }

    /**
     * Get nomServiceArchiveur.
     *
     * @return string
     */
    public function getNomServiceArchiveur()
    {
        return $this->nomServiceArchiveur;
    }

    /**
     * Set identifiantServiceArchiveur.
     *
     * @param string $identifiantServiceArchiveur
     *
     * @return Service
     */
    public function setIdentifiantServiceArchiveur($identifiantServiceArchiveur)
    {
        $this->identifiantServiceArchiveur = $identifiantServiceArchiveur;

        return $this;
    }

    /**
     * Get identifiantServiceArchiveur.
     *
     * @return string
     */
    public function getIdentifiantServiceArchiveur()
    {
        return $this->identifiantServiceArchiveur;
    }

    /**
     * Set affichageService.
     *
     * @param string $affichageService
     *
     * @return Service
     */
    public function setAffichageService($affichageService)
    {
        $this->affichageService = $affichageService;

        return $this;
    }

    /**
     * Get affichageService.
     *
     * @return string
     */
    public function getAffichageService()
    {
        return $this->affichageService;
    }

    /**
     * Set activationFuseauHoraire.
     *
     * @param string $activationFuseauHoraire
     *
     * @return Service
     */
    public function setActivationFuseauHoraire($activationFuseauHoraire)
    {
        $this->activationFuseauHoraire = $activationFuseauHoraire;

        return $this;
    }

    /**
     * Get activationFuseauHoraire.
     *
     * @return string
     */
    public function getActivationFuseauHoraire()
    {
        return $this->activationFuseauHoraire;
    }

    /**
     * Set decalageHoraire.
     *
     * @param string $decalageHoraire
     *
     * @return Service
     */
    public function setDecalageHoraire($decalageHoraire)
    {
        $this->decalageHoraire = $decalageHoraire;

        return $this;
    }

    /**
     * Get decalageHoraire.
     *
     * @return string
     */
    public function getDecalageHoraire()
    {
        return $this->decalageHoraire;
    }

    /**
     * Set lieuResidence.
     *
     * @param string $lieuResidence
     *
     * @return Service
     */
    public function setLieuResidence($lieuResidence)
    {
        $this->lieuResidence = $lieuResidence;

        return $this;
    }

    /**
     * Get lieuResidence.
     *
     * @return string
     */
    public function getLieuResidence()
    {
        return $this->lieuResidence;
    }

    /**
     * Set alerte.
     *
     * @param string $alerte
     *
     * @return Service
     */
    public function setAlerte($alerte)
    {
        $this->alerte = $alerte;

        return $this;
    }

    /**
     * Get alerte.
     *
     * @return string
     */
    public function getAlerte()
    {
        return $this->alerte;
    }

    /**
     * Set accesChorus.
     *
     * @param string $accesChorus
     *
     * @return Service
     */
    public function setAccesChorus($accesChorus)
    {
        $this->accesChorus = $accesChorus;

        return $this;
    }

    /**
     * Get accesChorus.
     *
     * @return string
     */
    public function getAccesChorus()
    {
        return $this->accesChorus;
    }

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->consultations = new ArrayCollection();
    }

    /**
     * @return $this
     */
    public function addConsultation(Consultation $consultation)
    {
        $this->consultations[] = $consultation;
        $consultation->setService($this);

        return $this;
    }

    public function removeConsultation(Consultation $consultation)
    {
        $this->consultations->removeElement($consultation);
    }

    /**
     * @return ArrayCollection
     */
    public function getConsultations()
    {
        return $this->consultations;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme(Organisme $organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormeJuridique()
    {
        return $this->formeJuridique;
    }

    /**
     * @param string $formeJuridique
     */
    public function setFormeJuridique($formeJuridique)
    {
        $this->formeJuridique = $formeJuridique;
    }

    /**
     * @return string
     */
    public function getFormeJuridiqueCode()
    {
        return $this->formeJuridiqueCode;
    }

    /**
     * @param string $formeJuridiqueCode
     */
    public function setFormeJuridiqueCode($formeJuridiqueCode)
    {
        $this->formeJuridiqueCode = $formeJuridiqueCode;
    }

    /**
     * @return ArrayCollection
     */
    public function getAgents()
    {
        return $this->agents;
    }

    /**
     * @param ArrayCollection $agents
     */
    public function setAgents($agents)
    {
        $this->agents = $agents;
    }

    /**
     * @return mixed
     */
    public function getIdParent()
    {
        return $this->idParent;
    }

    /**
     * @param mixed $idParent
     */
    public function setIdParent($idParent)
    {
        $this->idParent = $idParent;
    }

    /**
     * @return mixed
     */
    public function getIdExterneParent()
    {
        return $this->idExterneParent;
    }

    /**
     * @param mixed $idExterneParent
     */
    public function setIdExterneParent($idExterneParent)
    {
        $this->idExterneParent = $idExterneParent;
    }

    /**
     * @return string
     */
    public function getSynchronisationExec()
    {
        return $this->synchronisationExec;
    }

    /**
     * @param string $synchronisationExec
     */
    public function setSynchronisationExec($synchronisationExec)
    {
        $this->synchronisationExec = $synchronisationExec;

        return $this;
    }

    /**
     * Cette fonction permet de cloner le service en lui attribuant
     * un nouvel identifiant et l'organisme de destination passé en paramètre.
     *
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|Service false si le clone n'a pas réussi, sinon le nouvel objet Service créé
     *
     * @throws Exception
     */
    public function copier(string $organismeDestination): bool|\App\Entity\Service
    {
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)
            ->findByAcronymeOrganisme($organismeDestination);

        if ($organismeDestinationObject instanceof Organisme) {
            $nouveauService = clone $this;
            $nouveauService->setId($this->getMaxId() + 1);
            $nouveauService->setOrganisme($organismeDestinationObject);
            $this->em->persist($nouveauService);
            $this->em->flush();

            return $nouveauService;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(Service::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }

    /**
     * @return ArrayCollection
     */
    public function getAgentTechAssotiations()
    {
        return $this->agentTechAssotiations;
    }

    /**
     * @param ArrayCollection $agentTechAssotiations
     */
    public function setAgentTechAssotiations(ArrayCollection $agentTechAssotiation)
    {
        $this->agentTechAssotiations = $agentTechAssotiation;

        return $this;
    }

    /**
     * @return int
     */
    public function getIdEntite(): ?int
    {
        return $this->idEntite;
    }

    /**
     * @param int $idEntite
     */
    public function setIdEntite($idEntite): Service
    {
        $this->idEntite = $idEntite;

        return $this;
    }

    /**
     * @return null
     */
    public function getOldIdParent()
    {
        return $this->oldIdParent;
    }

    /**
     * @param null $oldIdParent
     * @return Service
     */
    public function setOldIdParent($oldIdParent)
    {
        $this->oldIdParent = $oldIdParent;

        return $this;
    }
}
