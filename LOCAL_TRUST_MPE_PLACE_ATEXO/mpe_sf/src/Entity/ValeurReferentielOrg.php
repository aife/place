<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeurReferentielOrg.
 *
 * @ORM\Table(name="ValeurReferentielOrg")
 * @ORM\Entity
 */
class ValeurReferentielOrg
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Referentiel", inversedBy="valeursReferentiellesOrg")
     * @ORM\JoinColumn(name="id_referentiel", referencedColumnName="id_referentiel")
     */
    private ?Referentiel $referentiel = null;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $id = null;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="id_referentiel", type="integer", nullable=false)
     */
    private string|int $idReferentiel = '0';

    /**
     * @ORM\Column(name="libelle_valeur_referentiel", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentiel = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_fr", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielFr = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_en", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielEn = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_es", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielEs = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_su", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielSu = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_du", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielDu = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_cz", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielCz = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_ar", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielAr = null;

    /**
     * @ORM\Column(name="libelle_valeur_referentiel_it", type="text", length=65535, nullable=false)
     */
    private ?string $libelleValeurReferentielIt = null;

    /**
     * Set id.
     *
     * @param int $id
     *
     * @return Valeurreferentielorg
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return Valeurreferentielorg
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set idReferentiel.
     *
     * @param int $idReferentiel
     *
     * @return Valeurreferentielorg
     */
    public function setIdReferentiel($idReferentiel)
    {
        $this->idReferentiel = $idReferentiel;

        return $this;
    }

    /**
     * Get idReferentiel.
     *
     * @return int
     */
    public function getIdReferentiel()
    {
        return $this->idReferentiel;
    }

    /**
     * Set libelleValeurReferentiel.
     *
     * @param string $libelleValeurReferentiel
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentiel($libelleValeurReferentiel)
    {
        $this->libelleValeurReferentiel = $libelleValeurReferentiel;

        return $this;
    }

    /**
     * Get libelleValeurReferentiel.
     *
     * @return string
     */
    public function getLibelleValeurReferentiel()
    {
        return $this->libelleValeurReferentiel;
    }

    /**
     * Set libelleValeurReferentielFr.
     *
     * @param string $libelleValeurReferentielFr
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielFr($libelleValeurReferentielFr)
    {
        $this->libelleValeurReferentielFr = $libelleValeurReferentielFr;

        return $this;
    }

    /**
     * Get libelleValeurReferentielFr.
     *
     * @return string
     */
    public function getLibelleValeurReferentielFr()
    {
        return $this->libelleValeurReferentielFr;
    }

    /**
     * Set libelleValeurReferentielEn.
     *
     * @param string $libelleValeurReferentielEn
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielEn($libelleValeurReferentielEn)
    {
        $this->libelleValeurReferentielEn = $libelleValeurReferentielEn;

        return $this;
    }

    /**
     * Get libelleValeurReferentielEn.
     *
     * @return string
     */
    public function getLibelleValeurReferentielEn()
    {
        return $this->libelleValeurReferentielEn;
    }

    /**
     * Set libelleValeurReferentielEs.
     *
     * @param string $libelleValeurReferentielEs
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielEs($libelleValeurReferentielEs)
    {
        $this->libelleValeurReferentielEs = $libelleValeurReferentielEs;

        return $this;
    }

    /**
     * Get libelleValeurReferentielEs.
     *
     * @return string
     */
    public function getLibelleValeurReferentielEs()
    {
        return $this->libelleValeurReferentielEs;
    }

    /**
     * Set libelleValeurReferentielSu.
     *
     * @param string $libelleValeurReferentielSu
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielSu($libelleValeurReferentielSu)
    {
        $this->libelleValeurReferentielSu = $libelleValeurReferentielSu;

        return $this;
    }

    /**
     * Get libelleValeurReferentielSu.
     *
     * @return string
     */
    public function getLibelleValeurReferentielSu()
    {
        return $this->libelleValeurReferentielSu;
    }

    /**
     * Set libelleValeurReferentielDu.
     *
     * @param string $libelleValeurReferentielDu
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielDu($libelleValeurReferentielDu)
    {
        $this->libelleValeurReferentielDu = $libelleValeurReferentielDu;

        return $this;
    }

    /**
     * Get libelleValeurReferentielDu.
     *
     * @return string
     */
    public function getLibelleValeurReferentielDu()
    {
        return $this->libelleValeurReferentielDu;
    }

    /**
     * Set libelleValeurReferentielCz.
     *
     * @param string $libelleValeurReferentielCz
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielCz($libelleValeurReferentielCz)
    {
        $this->libelleValeurReferentielCz = $libelleValeurReferentielCz;

        return $this;
    }

    /**
     * Get libelleValeurReferentielCz.
     *
     * @return string
     */
    public function getLibelleValeurReferentielCz()
    {
        return $this->libelleValeurReferentielCz;
    }

    /**
     * Set libelleValeurReferentielAr.
     *
     * @param string $libelleValeurReferentielAr
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielAr($libelleValeurReferentielAr)
    {
        $this->libelleValeurReferentielAr = $libelleValeurReferentielAr;

        return $this;
    }

    /**
     * Get libelleValeurReferentielAr.
     *
     * @return string
     */
    public function getLibelleValeurReferentielAr()
    {
        return $this->libelleValeurReferentielAr;
    }

    /**
     * Set libelleValeurReferentielIt.
     *
     * @param string $libelleValeurReferentielIt
     *
     * @return Valeurreferentielorg
     */
    public function setLibelleValeurReferentielIt($libelleValeurReferentielIt)
    {
        $this->libelleValeurReferentielIt = $libelleValeurReferentielIt;

        return $this;
    }

    /**
     * Get libelleValeurReferentielIt.
     *
     * @return string
     */
    public function getLibelleValeurReferentielIt()
    {
        return $this->libelleValeurReferentielIt;
    }

    /**
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     * @copyright Atexo 2016
     */
    public function setReferentiel(Referentiel $referentiel)
    {
        $this->referentiel = $referentiel;
    }

    /**
     * @return Referentiel
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since ESR2016
     *
     * @copyright Atexo 2016*/
    public function getReferentiel()
    {
        return $this->referentiel;
    }
}
