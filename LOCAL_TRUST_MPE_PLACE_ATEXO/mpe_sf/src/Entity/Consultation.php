<?php

namespace App\Entity;

use App\Controller\ApiPlatformCustom\ConsultationAttenteValidation;
use App\Controller\ApiPlatformCustom\ConsultationDonneeComplementaire;
use App\Controller\ApiPlatformCustom\ConsultationDuplicateParent;
use App\Controller\ApiPlatformCustom\ConsultationReferentielController;
use App\Controller\ApiPlatformCustom\CreateAutrePiece;
use App\Controller\ApiPlatformCustom\CreateConsultationReferentielController;
use App\Controller\ApiPlatformCustom\CreateOrUpdateDceDocument;
use App\Controller\ApiPlatformCustom\CreateRcDocument;
use App\Controller\ApiPlatformCustom\UpdateConsultationReferentielController;
use DateTimeInterface;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Controller\ApiPlatformCustom\ConsultationDce;
use App\Dto\Input\CreateConsultationInput;
use App\Dto\Input\UpdateConsultationInput;
use App\Dto\Output\ConsultationOutput;
use App\Entity\Consultation\ClausesN1;
use App\Filter\ClauseEnvironnementaleFilter;
use App\Filter\ClauseSocialeFilter;
use App\Filter\CodesCpvFilter;
use App\Filter\CommaSeparatedValuesSearchFilter;
use App\Filter\ConsultationStatutSearchFilter;
use App\Filter\FullTextSearchFilter;
use App\Entity\Configuration\PlateformeVirtuelle;
use App\Entity\Consultation\AutrePieceConsultation;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\Consultation\DonneeComplementaire;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\Offre\AnnexeFinanciere;
use App\Entity\Procedure\TypeProcedureOrganisme;
use App\Filter\GroupBySearchFilter;
use App\Filter\SocialCommerceEquitableFilter;
use App\Filter\SocialeAteliersProtegesFilter;
use App\Filter\SocialeEssFilter;
use App\Filter\SocialeSiaeFilter;
use App\Filter\SocialInsertionFilter;
use App\Model\TypePrestation;
use App\Repository\BloborganismeFileRepository;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Controller\ApiPlatformCustom\ConsultationCertificats;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\UuidInterface;

/**
 * Consultation.
 *
 * @ORM\Table(name="consultation", indexes={@ORM\Index(name="organisme", columns={"organisme"}),
 *     @ORM\Index(name="id", columns={"id"}), @ORM\Index(name="Idx_Consultation_Categorie",
 *     columns={"categorie"}), @ORM\Index(name="Idx_Consultation_CAP", columns={"consultation_achat_publique"}),
 *     @ORM\Index(name="Idx_Consultation_Datevalid", columns={"datevalidation"}),
 *     @ORM\Index(name="Idx_Consultation_EA", columns={"etat_approbation"}),
 *     @ORM\Index(name="Idx_Consultation_DVI", columns={"date_validation_intermediaire"}),
 *     @ORM\Index(name="Idx_Consultation_RU", columns={"reference_utilisateur"}),
 *     @ORM\Index(name="Idx_Consultation_Datefin", columns={"datefin"}),
 *     @ORM\Index(name="Idx_Consultation_ITP", columns={"id_type_procedure"}),
 *     @ORM\Index(name="Idx_Consultation_ITA", columns={"id_type_avis"}),
 *     @ORM\Index(name="Idx_Consultation_TML", columns={"type_mise_en_ligne"}),
 *     @ORM\Index(name="Idx_Consultation_DMLC", columns={"date_mise_en_ligne_calcule"}),
 *     @ORM\Index(name="Idx_Consultation_Datemiseenligne", columns={"datemiseenligne"}),
 *     @ORM\Index(name="Idx_Consultation_Code_CPV", columns={"code_cpv_1", "code_cpv_2"}),
 *     @ORM\Index(name="idx_consultation_alerte", columns={"alerte"}),
 *     @ORM\Index(name="index_url_migration", columns={"url_consultation_externe"}),
 *     @ORM\Index(name="index_denomination_adapte", columns={"denomination_adapte"}),
 *     @ORM\Index(name="Operations_fk", columns={"idOperation"}),
 *     @ORM\Index(name="DATE_FIN_UNIX", columns={"DATE_FIN_UNIX"})})
 * @ORM\Entity(repositoryClass="App\Repository\ConsultationRepository")
 */
#[ApiResource(
    collectionOperations: [
        "get",
        "create" => [
            "method" => "POST",
            "input" => CreateConsultationInput::class,
            "security_post_denormalize" =>
                "is_granted('CONSULTATION_CREATE', object) or is_granted('CONSULTATION_SUITE_CREATE', object) ",
            "openapi_context" => [
                "description" => '## Ce WebService nécessite l\'habilitation "creer_consultation".',
            ],
        ],
    ],
    itemOperations: [
        "get",
        "update" => [
            "method" => "PUT",
            "input" => UpdateConsultationInput::class,
            "security" => "is_granted('CONSULTATION_UPDATE', object)",
            "openapi_context" => [
                "description" =>
                    "## Ce WebService nécessite l'habilitation 'modifier_consultation_avant_validation'.",
            ],
        ],
        "patch" => [
            "method" => "PATCH",
            "input" => UpdateConsultationInput::class,
            "security" => "is_granted('CONSULTATION_UPDATE', object)",
            "openapi_context" => [
                "description" =>
                    "## Ce WebService nécessite l'habilitation 'modifier_consultation_avant_validation'.",
            ],
        ],
        "get_dce" => [
            "method" => "GET",
            "path" => "/consultations/{id}/dce",
            'controller' => ConsultationDce::class,
            'openapi_context' => [
                'summary' => 'Retrieves Consultation files',
                'description' => 'Retrieves Consultation files',
                'responses' => [
                    '200' => [
                        'description' => 'Retrieves Consultation files',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'array',
                                    'properties' => [
                                        'id' => ['type' => 'string'],
                                        'name' => ['type' => 'string'],
                                        'type' => [
                                            'type' => 'string',
                                            'enum' => [
                                                BloborganismeFileRepository::DCE_BLOB_TYPE,
                                                BloborganismeFileRepository::DUME_BLOB_TYPE,
                                                BloborganismeFileRepository::RC_BLOB_TYPE,
                                                BloborganismeFileRepository::COMPLEMENT_BLOB_TYPE,
                                            ],
                                        ],
                                        'dateModification' => ['type' => 'string'],
                                        'hash' => ['type' => 'string'],
                                        'taille' => ['type' => 'integer'],
                                    ],
                                ],
                                'example' => [
                                    [
                                        "id" => "/api/v2/media-objects/49855",
                                        "name" => "DCE.zip",
                                        "type" => "AUTRE_PIECE",
                                        "dateModification" => "2022-11-29T10:20:04+01:00",
                                        "hash" => "518137d403e82bec3f1d17168a0c1f9834f23a3f",
                                        "taille" => 181606,
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
        "create_dce_document" => [
            "method" => "POST",
            "path" => "/consultations/{id}/documents/dce",
            "controller" => CreateOrUpdateDceDocument::class,
            "validate" => false,
            "security" =>
                "is_granted('CONSULTATION_CREATE', object) or is_granted('ONLINE_CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_SUITE_CREATE', object)",
            "openapi_context" => [
                "summary" => "Creates a new DCE document for the consultation",
                "description" => '## Ce WebService nécessite l\'habilitation "creer_consultation".',
                "requestBody" => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file_path' => ['type' => 'string']
                                ]
                            ],
                            'example' => [
                                'file_path' => '868fcc62-ded5-465b-af59-22673233b31f/dce.zip'
                            ]
                        ]
                    ],
                ],
            ],
        ],
        "create_rc_document" => [
            "method" => "POST",
            "path" => "/consultations/{id}/documents/rc",
            "controller" => CreateRcDocument::class,
            "validate" => false,
            "security" =>
                "is_granted('CONSULTATION_CREATE', object) or is_granted('ONLINE_CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_SUITE_CREATE', object)",
            "openapi_context" => [
                "summary" => "Creates a new RC document for the consultation",
                "description" => '## Ce WebService nécessite l\'habilitation "creer_consultation".',
                "requestBody" => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file_path' => ['type' => 'string']
                                ]
                            ],
                            'example' => [
                                'file_path' => '868fcc62-ded5-465b-af59-22673233b31f/rc-file.pdf'
                            ]
                        ]
                    ],
                ],
            ],
        ],
        "create_autre_piece" => [
            "method" => "POST",
            "path" => "/consultations/{id}/documents/autre-piece",
            "controller" => CreateAutrePiece::class,
            "validate" => false,
            "security" =>
                "is_granted('CONSULTATION_CREATE', object) or is_granted('ONLINE_CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_UPDATE', object) or is_granted('CONSULTATION_SUITE_CREATE', object)",
            "openapi_context" => [
                "summary" => "Creates a new other document for the consultation",
                "description" => '## Ce WebService nécessite l\'habilitation "creer_consultation".',
                "requestBody" => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'file_path' => ['type' => 'string']
                                ]
                            ],
                            'example' => [
                                'file_path' => '868fcc62-ded5-465b-af59-22673233b31f/autrePieces-file.pdf'
                            ]
                        ]
                    ],
                ],
            ],
        ],
        "get_donnee" => [
            "method" => "GET",
            "path" => "/consultations/{id}/donnee-complementaire",
            'controller' => ConsultationDonneeComplementaire::class,
            "openapi_context" => [
                'description' => 'Retrieves donnee complementaire',
                'responses' => [
                    '200' => [
                        'description' => 'Retrieves Consultation files',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                ],
                            ],
                        ],

                    ],
                ],
            ],
        ],
        "get_justification_non_Allotissement" => [
            "method" => "GET",
            "path" => "/consultations/{id}/justification-non-allotissement",
            'controller' => ConsultationCertificats::class,
            "openapi_context" => [
                'description' => 'Retrieves liste des certificats ',
                'responses' => [
                    '200' => [
                        'description' => 'Liste des certificats de chiffrement associés à la consultation.',
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    'type' => 'object',
                                ],
                            ],
                        ],

                    ],
                ],
            ],
        ],
        "set_attente_validation" => [
            "method" => "PUT",
            "path" => "/consultations/{id}/attente-validation",
            "controller" => ConsultationAttenteValidation::class,
            'defaults' => ['_api_receive' => false],
            "openapi_context" => [
                "summary" => "Set attente validation status for the consultation",
                "description" => "Set attente validation status for the consultation",
                "requestBody" => [
                    'required'  => false,
                    'content' => []
                ],
                'responses' => [
                    '200' => [
                    ],
                    '204' => [
                        'description' => 'No content',
                    ],
                ]
            ]
        ],
        "get_referentiels" => [
            "method" => "GET",
            "path" => "/consultations/{id}/referentiels",
            "controller" => ConsultationReferentielController::class,
            "openapi_context" => [
                "summary" => "Retrieves a consultation referentiels",
                "description" => "Retrieves a consultation referentiels",
                'responses' => [
                    '200' => [
                    ],
                    '204' => [
                        'description' => 'No content',
                    ],
                ]
            ]
        ],
        "post_referentiels" => [
            "method" => "POST",
            "path" => "/consultations/{id}/referentiels",
            "controller" => CreateConsultationReferentielController::class,
            "openapi_context" => [
                "summary" => "Create new consultation referentiels",
                "description" => '## Create new consultation referentiels',
                "requestBody" => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'DEFINE_CODE_CPV' => ['type' => 'object']
                                ]
                            ],
                            'example' => [
                                'DEFINE_CODE_CPV' => [
                                    'valeurPrincipale' => 'ER45566',
                                    'valeurSecondaire' => 'ER9830'
                                ],
                                'AUTRES_INFORMATIONS' => [
                                    'valeurPrincipale' => 'Autres informations'
                                ],
                                'CONDITION_DE_PARTICIPATION' => [
                                    'valeurPrincipale' => 'Conditions de participation'
                                ],
                                'AUTRE_POUVOIR_ADJUDICATEUR' => [
                                    'valeurPrincipale' => 'Autre pouvoir adjudicateur'
                                ],
                                'DEFINE_INFORMATIONS_COMPLEMENTAIRES' => [
                                    'valeurPrincipale' => 'Informations complémentaires'
                                ],
                                'FORMULE_DE_SIGNATURE' => [
                                    'valeurPrincipale' => 'Formule de signature'
                                ],
                                'MODALITES_VISITE_LIEUX_REUNION_INFORMATION'
                                => [
                                    'valeurPrincipale' => 'Modalités de visite, '
                                        . 'lieux et dates de réunion d\'information'
                                ],
                            ]
                        ]
                    ],
                ],
            ]
        ],
        "patch_referentiels" => [
            "method" => "PUT",
            "path" => "/consultations/{id}/referentiels",
            "controller" => UpdateConsultationReferentielController::class,
            "openapi_context" => [
                "summary" => "Update a consultation referentiels",
                "description" => '## Update a consultation referentiels',
                "requestBody" => [
                    'content' => [
                        'application/json' => [
                            'schema' => [
                                'type' => 'object',
                                'properties' => [
                                    'DEFINE_CODE_CPV' => ['type' => 'object']
                                ]
                            ],
                            'example' => [
                                'DEFINE_CODE_CPV' => [
                                    'valeurPrincipale' => 'ER45566',
                                    'valeurSecondaire' => 'ER9830'
                                ],
                                'AUTRES_INFORMATIONS' => [
                                    'valeurPrincipale' => 'Autres informations'
                                ],
                                'CONDITION_DE_PARTICIPATION' => [
                                    'valeurPrincipale' => 'Conditions de participation'
                                ],
                                'AUTRE_POUVOIR_ADJUDICATEUR' => [
                                    'valeurPrincipale' => 'Autre pouvoir adjudicateur'
                                ],
                                'DEFINE_INFORMATIONS_COMPLEMENTAIRES' => [
                                    'valeurPrincipale' => 'Informations complémentaires'
                                ],
                                'FORMULE_DE_SIGNATURE' => [
                                    'valeurPrincipale' => 'Formule de signature'
                                ],
                                'MODALITES_VISITE_LIEUX_REUNION_INFORMATION'
                                => [
                                    'valeurPrincipale' => 'Modalités de visite, '
                                        . 'lieux et dates de réunion d\'information'
                                ],
                            ]
                        ]
                    ],
                ],
            ]
        ],
        "duplicate_conultation_parent" => [
            "method" => "PUT",
            "path" => "/consultations/{id}/duplicate-parent",
            "controller" => ConsultationDuplicateParent::class,
            'defaults' => ['_api_receive' => false],
            "openapi_context" => [
                "summary" => "Duplicate consultation father for the consultation",
                "description" => "Duplicate consultation father for the consultation",
                "requestBody" => [
                    'required'  => false,
                    'content' => []
                ],
                'responses' => [
                    '200' => [
                    ],
                    '204' => [
                        'description' => 'No content',
                    ],
                ]
            ]
        ]
    ],
    attributes: ["security_message" => "Habilitation manquante pour effectuer cette action"],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    order: ['datefin' => 'DESC'],
    output: ConsultationOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'referenceUtilisateur' => 'partial',
        'intitule' => 'partial',
        'objet' => 'partial',
        'acronymeOrg' => 'exact',
        'categorie' => 'exact',
        'typeProcedure' => 'exact',
        'idTypeProcedureOrg' => 'exact',
        'id' => 'exact',
        'idTypeAvis' => 'exact',
        'orgDenomination' => 'partial',
        'idCreateur' => 'exact',
        'service.id' => 'exact',
        'referenceConsultationParent.id' => 'exact',
        'reference' => 'exact'
    ]
)]
#[ApiFilter(
    FullTextSearchFilter::class,
    properties: [
        'search_full' => [
            'referenceUtilisateur' => 'ipartial',
            'intitule' => 'ipartial',
            'objet' => 'ipartial',
        ],
    ]
)]
#[ApiFilter(
    CommaSeparatedValuesSearchFilter::class,
    properties: [
        'lieuExecution' => 'partial',
    ]
)]
#[ApiFilter(
    DateFilter::class,
    properties: [
        'dateMiseEnLigneCalcule',
        'datefin',
    ]
)]
#[ApiFilter(
    ConsultationStatutSearchFilter::class
)]
#[ApiFilter(
    CodesCpvFilter::class
)]
#[ApiFilter(
    GroupBySearchFilter::class
)]
#[ApiFilter(ClauseSocialeFilter::class)]
#[ApiFilter(ClauseEnvironnementaleFilter::class)]
#[ApiFilter(SocialCommerceEquitableFilter::class)]
#[ApiFilter(SocialInsertionFilter::class)]
#[ApiFilter(SocialeAteliersProtegesFilter::class)]
#[ApiFilter(SocialeEssFilter::class)]
#[ApiFilter(SocialeSiaeFilter::class)]
class Consultation
{
    public const CLAUSE_ACTIVE = 1;

    public const TYPE_ACCES_PUBLIC = '1';
    public const TYPE_ACCES_RESTREINT = '2';

    public const TYPE_AVIS_CONSULTATION = '3';

    /* Statut  Consultation */
    public const STATUT_CONSULTATION = '2';
    public const STATUT_OA = '3';
    public const STATUT_DECISION = '4';
    public const STATUT_A_ARCHIVER = '5';
    public const STATUT_ARCHIVE_REALISEE = '6';

    public const REFERENTIELS = 'referentiels';
    public const LOTS = 'lots';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TBourseCotraitance",
     *     mappedBy="consultation", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $bourses;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offre",
     *     mappedBy="consultation", cascade={"persist"}, fetch = "EXTRA_LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $offres;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TelechargementAnonyme",
     *     mappedBy="consultation", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $telechargementAnonymes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Telechargement",
     *     mappedBy="consultation", cascade={"persist"}, fetch = "EXTRA_LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $telechargements;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OffrePapier",
     *     mappedBy="consultation", cascade={"persist"}, fetch = "EXTRA_LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $offresPapiers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TDumeContexte",
     *     mappedBy="consultation", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $dumeContextes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TCandidature",
     *     mappedBy="consultation", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $candidatures;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TListeLotsCandidature",
     *     mappedBy="consultation", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $listLots;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="consultations", fetch="EAGER")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", inversedBy="consultations", fetch="EAGER")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private ?Service $service = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Lot", mappedBy="consultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $lots;


    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Retrait\RetraitPapier",
     *     mappedBy="consultation", cascade={"persist"}, fetch = "EXTRA_LAZY")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $retraitPapiers;

    private ?TypeProcedureOrganisme $typeProcedureOrganisme = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeProcedure",
     *     inversedBy="consultations")
     * @ORM\JoinColumn(name="id_type_procedure", referencedColumnName="id_type_procedure")
     */
    private ?TypeProcedure $typeProcedure = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\AvisType",
     *     inversedBy="consultations", fetch="EAGER")
     * @ORM\JoinColumn(name="id_type_avis", referencedColumnName="id")
     */
    private ?AvisType $avisType = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\CategorieConsultation",
     *     inversedBy="consultations", fetch="EAGER")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     */
    private ?CategorieConsultation $categorieConsultation = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\CertificatChiffrement", mappedBy="consultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $certificatChiffrements;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Consultation\DonneeComplementaire", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private ?DonneeComplementaire $donneeComplementaire = null;

    /**
     * @ORM\Column(name="reference", type="integer")
     */
    private ?int $reference = null;

    /**
     * @var string
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    #[Groups('read')]
    private $acronymeOrg = '';

    /**
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    #[Groups('read')]
    private ?int $id = null;

    /**
     * @ORM\Column(name="reference_utilisateur", type="string", length=255, nullable=false)
     */
    private string $referenceUtilisateur = '';

    /**
     * @ORM\Column(name="categorie", type="string", length=30, nullable=true)
     */
    private ?string $categorie = '0';

    /**
     * @ORM\Column(name="titre", type="text", nullable=true)
     */
    #[Groups('read')]
    private ?string $titre = null;

    /**
     * @ORM\Column(name="resume", type="text", nullable=true)
     */
    private ?string $resume = null;

    /**
     * @ORM\Column(name="datedebut", type="date", nullable=false)
     */
    private string|DateTimeInterface|\DateTime $datedebut = '0000-00-00';

    /**
     * @ORM\Column(name="datefin", type="datetime", nullable=false)
     */
    #[Groups('read')]
    private string|DateTimeInterface|\DateTime $datefin = '0000-00-00 00:00:00';

    private ?\DateTime $datefinDate = null;

    private ?\DateTime $datefinHeure = null;

    /**
     * @ORM\Column(name="datevalidation", type="string", length=20, nullable=false)
     */
    private string|\DateTime $datevalidation = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="code_procedure", type="string", length=15, nullable=true)
     */
    private ?string $codeProcedure = null;

    /**
     * @ORM\Column(name="reponse_electronique", type="string", length=1, nullable=false)
     */
    private string $reponseElectronique = '1';

    /**
     * @ORM\Column(name="num_procedure", type="integer", nullable=false)
     */
    private string|int $numProcedure = '0';

    /**
     * @ORM\Column(name="id_type_procedure", type="integer", nullable=true)
     */
    private ?int $idTypeProcedure = null;

    /**
     * @ORM\Column(name="id_type_avis", type="integer", nullable=false)
     */
    private string|int $idTypeAvis = '0';

    /**
     * @ORM\Column(name="lieu_execution", type="text", length=65535, nullable=false)
     */
    private ?string $lieuExecution = null;

    /**
     * @ORM\Column(name="type_mise_en_ligne", type="integer", nullable=false)
     */
    private string|int $typeMiseEnLigne = '1';

    /**
     * @ORM\Column(name="datemiseenligne", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|\DateTime $datemiseenligne = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="is_tiers_avis", type="string", length=1, nullable=false)
     */
    private string $isTiersAvis = '0';

    /**
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private string $url = '';

    /**
     * @ORM\Column(name="datefin_sad", type="datetime", nullable=false)
     */
    private string|DateTimeInterface|\DateTime $datefinSad = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="is_sys_acq_dyn", type="integer", nullable=false)
     */
    private string|int $isSysAcqDyn = '0';

    /**
     * @ORM\Column(name="reference_consultation_init", type="string", length=250, nullable=false)
     */
    private string|int|null $referenceConsultationInit = '';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation", mappedBy="referenceConsultationParent")
     */
    private Collection $referenceConsultationsChildren;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="referenceConsultationsChildren",
     *     fetch="EAGER")
     * @ORM\JoinColumn(name="reference_consultation_init", referencedColumnName="id")
     */
    private ?Consultation $referenceConsultationParent = null;

    /**
     * @ORM\Column(name="signature_offre", type="string", length=1, nullable=true)
     */
    private ?string $signatureOffre = null;

    /**
     * @ORM\Column(name="id_type_validation", type="integer", nullable=false)
     */
    private string|int $idTypeValidation = '2';

    /**
     * @ORM\Column(name="etat_approbation", type="string", nullable=false)
     */
    private string $etatApprobation = '0';

    /**
     * @ORM\Column(name="etat_validation", type="string", nullable=false)
     */
    private string $etatValidation = '0';

    /**
     * @ORM\Column(name="champ_supp_invisible", type="text", length=65535, nullable=false)
     */
    private ?string $champSuppInvisible = null;

    /**
     * @ORM\Column(name="code_cpv_1", type="string", length=8, nullable=true)
     */
    private ?string $codeCpv1 = null;

    /**
     * @ORM\Column(name="code_cpv_2", type="string", length=255, nullable=true)
     */
    private ?string $codeCpv2 = null;

    /**
     * @ORM\Column(name="publication_europe", type="string", nullable=true)
     */
    private ?string $publicationEurope = '0';

    /**
     * @ORM\Column(name="etat_publication", type="integer", nullable=false)
     */
    private string|int $etatPublication = '0';

    /**
     * @ORM\Column(name="poursuivre_affichage", type="integer", nullable=false)
     */
    private string|int $poursuivreAffichage = '0';

    /**
     * @ORM\Column(name="poursuivre_affichage_unite", type="string", nullable=false)
     */
    private string $poursuivreAffichageUnite = 'DAY';

    /**
     * @ORM\Column(name="nbr_telechargement_dce", type="integer", nullable=true)
     */
    private ?int $nbrTelechargementDce = null;

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="service_associe_id", type="integer", nullable=true)
     */
    private ?int $serviceAssocieId = null;

    /**
     * @ORM\Column(name="detail_consultation", type="text", length=65535, nullable=false)
     */
    private ?string $detailConsultation = null;

    /**
     * @ORM\Column(name="date_fin_affichage", type="string", length=20, nullable=false)
     */
    private string $dateFinAffichage = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="depouillable_phase_consultation", type="string", nullable=false)
     */
    private string $depouillablePhaseConsultation = '0';

    /**
     * @ORM\Column(name="consultation_transverse", type="string", nullable=false)
     */
    private string $consultationTransverse = '0';

    /**
     * @ORM\Column(name="consultation_achat_publique", type="string", nullable=false)
     */
    private string $consultationAchatPublique = '0';

    /**
     * @ORM\Column(name="url_consultation_achat_publique", type="text", length=65535, nullable=true)
     */
    private ?string $urlConsultationAchatPublique = null;

    /**
     * @ORM\Column(name="partial_dce_download", type="string", length=1, nullable=false)
     */
    private string $partialDceDownload = '0';

    /**
     * @ORM\Column(name="tirage_plan", type="integer", nullable=false)
     */
    private string|int $tiragePlan = '0';

    /**
     * @ORM\Column(name="tireur_plan", type="integer", nullable=false)
     */
    private string|int $tireurPlan = '0';

    /**
     * @var ?DateTime
     *
     * @ORM\Column(name="date_mise_en_ligne_calcule", type="datetime", nullable=true)
     */
    #[Groups('read')]
    private $dateMiseEnLigneCalcule;

    /**
     * @ORM\Column(name="accessibilite_en", type="string", length=1, nullable=false)
     */
    private string $accessibiliteEn = '0';

    /**
     * @ORM\Column(name="accessibilite_es", type="string", length=1, nullable=false)
     */
    private string $accessibiliteEs = '0';

    /**
     * @ORM\Column(name="nbr_reponse", type="integer", nullable=true)
     */
    private ?int $nbrReponse = null;

    /**
     * @ORM\Column(name="id_type_procedure_org", type="integer", nullable=false)
     */
    private int $idTypeProcedureOrg = 0;

    /**
     * @ORM\Column(name="tirage_descriptif", type="text", length=16777215, nullable=false)
     */
    private ?string $tirageDescriptif = null;

    /**
     * @ORM\Column(name="organisme_consultation_init", type="string", length=255, nullable=false)
     */
    private string $organismeConsultationInit = '';

    /**
     * @ORM\Column(name="date_validation_intermediaire", type="string", length=20, nullable=true)
     */
    private ?string $dateValidationIntermediaire = null;

    /**
     * @ORM\Column(name="accessibilite_fr", type="string", nullable=false)
     */
    private string $accessibiliteFr = '0';

    /**
     * @ORM\Column(name="id_tr_accessibilite", type="integer", nullable=true)
     */
    private ?int $idTrAccessibilite = null;

    /**
     * @ORM\Column(name="accessibilite_cz", type="string", nullable=false)
     */
    private string $accessibiliteCz = '0';

    /**
     * @ORM\Column(name="accessibilite_du", type="string", nullable=false)
     */
    private string $accessibiliteDu = '0';

    /**
     * @ORM\Column(name="accessibilite_su", type="string", nullable=false)
     */
    private string $accessibiliteSu = '0';

    /**
     * @ORM\Column(name="accessibilite_ar", type="string", nullable=false)
     */
    private string $accessibiliteAr = '0';

    /**
     * @ORM\Column(name="alloti", type="string", nullable=false)
     */
    private string $alloti = '0';

    /**
     * @ORM\Column(name="numero_phase", type="integer", nullable=false)
     */
    private string|int $numeroPhase = '0';

    /**
     * @ORM\Column(name="consultation_externe", type="string", nullable=false)
     */
    private string $consultationExterne = '0';

    /**
     * @ORM\Column(name="url_consultation_externe", type="text", length=65535, nullable=true)
     */
    private ?string $urlConsultationExterne = null;

    /**
     * @ORM\Column(name="org_denomination", type="string", length=250, nullable=true)
     */
    private ?string $orgDenomination = null;

    /**
     * @ORM\Column(name="domaines_activites", type="string", length=250, nullable=true)
     */
    private ?string $domainesActivites = '';

    /**
     * @ORM\Column(name="id_affaire", type="integer", nullable=true)
     */
    private ?int $idAffaire = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers", type="text", nullable=true)
     */
    private ?string $adresseRetraisDossiers = null;

    /**
     * @ORM\Column(name="caution_provisoire", type="string", length=255, nullable=true)
     */
    private ?string $cautionProvisoire = null;

    /**
     * @ORM\Column(name="adresse_depot_offres", type="text", nullable=true)
     */
    private ?string $adresseDepotOffres = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis", type="text", nullable=true)
     */
    private ?string $lieuOuverturePlis = null;

    /**
     * @ORM\Column(name="prix_aquisition_plans", type="string", length=255, nullable=true)
     */
    private ?string $prixAquisitionPlans = null;

    /**
     * @ORM\Column(name="qualification", type="string", length=255, nullable=true)
     */
    private ?string $qualification = null;

    /**
     * @ORM\Column(name="agrements", type="string", length=255, nullable=true)
     */
    private ?string $agrements = null;

    /**
     * @ORM\Column(name="add_echantillion", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillion = null;

    /**
     * @ORM\Column(name="date_limite_echantillion", type="string", length=50, nullable=true)
     */
    private ?string $dateLimiteEchantillion = null;

    /**
     * @ORM\Column(name="add_reunion", type="string", length=255, nullable=true)
     */
    private ?string $addReunion = null;

    /**
     * @ORM\Column(name="date_reunion", type="string", length=50, nullable=true)
     */
    private ?string $dateReunion = null;

    /**
     * @ORM\Column(name="variantes", type="string", length=1, nullable=true)
     */
    private ?string $variantes = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_ar", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresAr = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_ar", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisAr = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_ar", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersAr = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdmin = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_fr", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminFr = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_en", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminEn = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_es", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminEs = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_su", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminSu = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_du", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminDu = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_cz", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminCz = null;

    /**
     * @ORM\Column(name="pieces_dossier_admin_ar", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdminAr = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTech = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_fr", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechFr = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_en", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechEn = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_es", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechEs = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_su", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechSu = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_du", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechDu = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_cz", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechCz = null;

    /**
     * @ORM\Column(name="pieces_dossier_tech_ar", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierTechAr = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditif = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_fr", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifFr = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_en", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifEn = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_es", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifEs = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_su", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifSu = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_du", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifDu = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_cz", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifCz = null;

    /**
     * @ORM\Column(name="pieces_dossier_additif_ar", type="text", length=65535, nullable=true)
     */
    private ?string $piecesDossierAdditifAr = null;

    /**
     * @ORM\Column(name="id_rpa", type="integer", nullable=true)
     */
    private ?int $idRpa = null;

    /**
     * @ORM\Column(name="detail_consultation_fr", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationFr = null;

    /**
     * @ORM\Column(name="detail_consultation_en", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationEn = null;

    /**
     * @ORM\Column(name="detail_consultation_es", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationEs = null;

    /**
     * @ORM\Column(name="detail_consultation_su", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationSu = null;

    /**
     * @ORM\Column(name="detail_consultation_du", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationDu = null;

    /**
     * @ORM\Column(name="detail_consultation_cz", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationCz = null;

    /**
     * @ORM\Column(name="detail_consultation_ar", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationAr = null;

    /**
     * @ORM\Column(name="echantillon", type="string", nullable=false)
     */
    private ?string $echantillon = null;

    /**
     * @ORM\Column(name="reunion", type="string", nullable=false)
     */
    private ?string $reunion = null;

    /**
     * @ORM\Column(name="visites_lieux", type="string", nullable=false)
     */
    private ?string $visitesLieux = null;

    /**
     * @ORM\Column(name="variante_calcule", type="string", nullable=false)
     */
    private string $varianteCalcule = '0';

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_fr", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersFr = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_en", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersEn = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_es", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersEs = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_su", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersSu = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_du", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersDu = null;

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_cz", type="string", length=255, nullable=true)
     */
    private ?string $adresseRetraisDossiersCz = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_fr", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresFr = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_en", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresEn = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_es", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresEs = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_su", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresSu = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_du", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresDu = null;

    /**
     * @ORM\Column(name="adresse_depot_offres_cz", type="string", length=255, nullable=true)
     */
    private ?string $adresseDepotOffresCz = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_fr", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisFr = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_en", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisEn = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_es", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisEs = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_su", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisSu = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_du", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisDu = null;

    /**
     * @ORM\Column(name="lieu_ouverture_plis_cz", type="string", length=255, nullable=true)
     */
    private ?string $lieuOuverturePlisCz = null;

    /**
     * @ORM\Column(name="add_echantillion_fr", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionFr = null;

    /**
     * @ORM\Column(name="add_echantillion_en", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionEn = null;

    /**
     * @ORM\Column(name="add_echantillion_es", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionEs = null;

    /**
     * @ORM\Column(name="add_echantillion_su", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionSu = null;

    /**
     * @ORM\Column(name="add_echantillion_du", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionDu = null;

    /**
     * @ORM\Column(name="add_echantillion_cz", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionCz = null;

    /**
     * @ORM\Column(name="add_echantillion_ar", type="string", length=255, nullable=true)
     */
    private ?string $addEchantillionAr = null;

    /**
     * @ORM\Column(name="add_reunion_fr", type="string", length=255, nullable=true)
     */
    private ?string $addReunionFr = null;

    /**
     * @ORM\Column(name="add_reunion_en", type="string", length=255, nullable=true)
     */
    private ?string $addReunionEn = null;

    /**
     * @ORM\Column(name="add_reunion_es", type="string", length=255, nullable=true)
     */
    private ?string $addReunionEs = null;

    /**
     * @ORM\Column(name="add_reunion_su", type="string", length=255, nullable=true)
     */
    private ?string $addReunionSu = null;

    /**
     * @ORM\Column(name="add_reunion_du", type="string", length=255, nullable=true)
     */
    private ?string $addReunionDu = null;

    /**
     * @ORM\Column(name="add_reunion_cz", type="string", length=255, nullable=true)
     */
    private ?string $addReunionCz = null;

    /**
     * @ORM\Column(name="add_reunion_ar", type="string", length=255, nullable=true)
     */
    private ?string $addReunionAr = null;

    /**
     * @ORM\Column(name="mode_passation", type="string", length=1, nullable=true)
     */
    private ?string $modePassation = null;

    /**
     * @ORM\Column(name="consultation_annulee", type="string", nullable=false)
     */
    private string $consultationAnnulee = '0';

    /**
     * @ORM\Column(name="accessibilite_it", type="string", nullable=false)
     */
    private string $accessibiliteIt = '0';

    /**
     * @ORM\Column(name="adresse_depot_offres_it", type="string", length=255, nullable=false)
     */
    private string $adresseDepotOffresIt = '';

    /**
     * @ORM\Column(name="lieu_ouverture_plis_it", type="string", length=255, nullable=false)
     */
    private string $lieuOuverturePlisIt = '';

    /**
     * @ORM\Column(name="adresse_retrais_dossiers_it", type="string", length=255, nullable=false)
     */
    private string $adresseRetraisDossiersIt = '';

    /**
     * @ORM\Column(name="pieces_dossier_admin_it", type="string", length=255, nullable=false)
     */
    private string $piecesDossierAdminIt = '';

    /**
     * @ORM\Column(name="pieces_dossier_tech_it", type="string", length=255, nullable=false)
     */
    private string $piecesDossierTechIt = '';

    /**
     * @ORM\Column(name="pieces_dossier_additif_it", type="string", length=255, nullable=false)
     */
    private string $piecesDossierAdditifIt = '';

    /**
     * @ORM\Column(name="detail_consultation_it", type="text", length=65535, nullable=true)
     */
    private ?string $detailConsultationIt = null;

    /**
     * @ORM\Column(name="add_echantillion_it", type="string", length=250, nullable=false)
     */
    private string $addEchantillionIt = '';

    /**
     * @ORM\Column(name="add_reunion_it", type="string", length=250, nullable=false)
     */
    private string $addReunionIt = '';

    /**
     * @ORM\Column(name="codes_nuts", type="text", length=65535, nullable=true)
     */
    private ?string $codesNuts = null;

    /**
     * @ORM\Column(name="intitule", type="text", nullable=false)
     */
    private string $intitule = '';

    /**
     * @ORM\Column(name="id_tr_intitule", type="integer", nullable=true)
     */
    private ?int $idTrIntitule = null;

    /**
     * @ORM\Column(name="objet", type="text", nullable=false)
     */
    #[Groups('read')]
    private string $objet = '';

    /**
     * @ORM\Column(name="id_tr_objet", type="integer", nullable=true)
     */
    private ?int $idTrObjet = null;

    /**
     * @ORM\Column(name="type_acces", type="string", length=1, nullable=false)
     */
    private ?string $typeAcces = null;

    /**
     * @ORM\Column(name="autoriser_reponse_electronique", type="string", length=1, nullable=false)
     */
    private string $autoriserReponseElectronique = '1';

    /**
     * @ORM\Column(name="regle_mise_en_ligne", type="integer", nullable=false)
     */
    private string|int $regleMiseEnLigne = '1';

    /**
     * @ORM\Column(name="id_regle_validation", type="integer", nullable=false)
     */
    private string|int $idRegleValidation = '2';

    /**
     * @ORM\Column(name="intitule_fr", type="text", nullable=true)
     */
    private ?string $intituleFr = null;

    /**
     * @ORM\Column(name="intitule_en", type="text", nullable=true)
     */
    private ?string $intituleEn = null;

    /**
     * @ORM\Column(name="intitule_es", type="text", nullable=true)
     */
    private ?string $intituleEs = null;

    /**
     * @ORM\Column(name="intitule_su", type="text", nullable=true)
     */
    private ?string $intituleSu = null;

    /**
     * @ORM\Column(name="intitule_du", type="text", nullable=true)
     */
    private ?string $intituleDu = null;

    /**
     * @ORM\Column(name="intitule_cz", type="text", nullable=true)
     */
    private ?string $intituleCz = null;

    /**
     * @ORM\Column(name="intitule_ar", type="text", nullable=true)
     */
    private ?string $intituleAr = null;

    /**
     * @ORM\Column(name="intitule_it", type="text", nullable=true)
     */
    private ?string $intituleIt = null;

    /**
     * @ORM\Column(name="objet_fr", type="text", nullable=true)
     */
    private ?string $objetFr = null;

    /**
     * @ORM\Column(name="objet_en", type="text", nullable=true)
     */
    private ?string $objetEn = null;

    /**
     * @ORM\Column(name="objet_es", type="text", nullable=true)
     */
    private ?string $objetEs = null;

    /**
     * @ORM\Column(name="objet_su", type="text", nullable=true)
     */
    private ?string $objetSu = null;

    /**
     * @ORM\Column(name="objet_du", type="text", nullable=true)
     */
    private ?string $objetDu = null;

    /**
     * @ORM\Column(name="objet_cz", type="text", nullable=true)
     */
    private ?string $objetCz = null;

    /**
     * @ORM\Column(name="objet_ar", type="text", nullable=true)
     */
    private ?string $objetAr = null;

    /**
     * @ORM\Column(name="objet_it", type="text", nullable=true)
     */
    private ?string $objetIt = null;

    /**
     * @ORM\Column(name="date_decision", type="string", length=10, nullable=false)
     */
    private string $dateDecision = '';

    /**
     * @ORM\Column(name="clause_sociale", type="string", nullable=false)
     */
    private string $clauseSociale = '0';

    /**
     * @ORM\Column(name="clause_environnementale", type="string", nullable=false)
     */
    private string $clauseEnvironnementale = '0';

    /**
     * @ORM\Column(name="reponse_obligatoire", type="string", length=1, nullable=false)
     */
    private string $reponseObligatoire = '0';

    /**
     * @ORM\Column(name="Compte_Boamp_Associe", type="integer", nullable=true)
     */
    private ?int $compteBoampAssocie = null;

    /**
     * @ORM\Column(name="autoriser_publicite", type="integer", nullable=false)
     */
    private string|int $autoriserPublicite = '1';

    /**
     * @ORM\Column(name="type_envoi", type="string", length=1, nullable=false)
     */
    private string $typeEnvoi = '';

    /**
     * @ORM\Column(name="chiffrement_offre", type="string", length=1, nullable=false)
     */
    private string $chiffrementOffre = '';

    /**
     * @ORM\Column(name="env_candidature", type="integer", nullable=false)
     */
    private string|int $envCandidature = '0';

    /**
     * @ORM\Column(name="env_offre", type="integer", nullable=false)
     */
    private string|int $envOffre = '0';

    /**
     * @ORM\Column(name="env_anonymat", type="integer", nullable=false)
     */
    private string|int $envAnonymat = '0';

    /**
     * @ORM\Column(name="id_etat_consultation", type="integer", nullable=false)
     */
    private string|int $idEtatConsultation = '0';

    /**
     * @ORM\Column(name="reference_connecteur", type="string", length=255, nullable=false)
     */
    private string $referenceConnecteur = '';

    /**
     * @ORM\Column(name="cons_statut", type="string", length=1, nullable=false)
     */
    private string $consStatut = '0';

    /**
     * @ORM\Column(name="id_approbateur", type="integer", nullable=false)
     */
    private string|int $idApprobateur = '0';

    /**
     * @ORM\Column(name="id_valideur", type="integer", nullable=false)
     */
    private string|int $idValideur = '0';

    /**
     * @ORM\Column(name="service_validation", type="integer", nullable=true)
     */
    private ?int $serviceValidation = null;

    /**
     * @ORM\Column(name="id_createur", type="integer", nullable=false)
     */
    #[Groups('read')]
    private int $idCreateur = 0;

    /**
     * Champ id_createur qui fait référence à l'Agent, utilisation de ce champ mappé dans APIP Extension
     *
     * @ORM\ManyToOne(targetEntity=Agent::class, inversedBy="consultations")
     * @ORM\JoinColumn(name="id_createur")
     *
     */
    private ?Agent $agentCreateur = null;

    /**
     * Champ agent_technique_createur qui fait référence à l'Agent, utilisation de ce champ mappé dans APIP Extension
     *
     * @ORM\ManyToOne(targetEntity=Agent::class, inversedBy="referenceConsultationAgentTechniqueCreateur")
     * @ORM\JoinColumn(name="agent_technique_createur")
     *
     */
    private ?Agent $agentTechniqueCreateur = null;

    /**
     * @ORM\Column(name="nom_createur", type="string", length=100, nullable=true)
     */
    private ?string $nomCreateur = null;

    /**
     * @ORM\Column(name="prenom_createur", type="string", length=100, nullable=true)
     */
    private ?string $prenomCreateur = null;

    /**
     * @ORM\Column(name="signature_acte_engagement", type="string", nullable=false)
     */
    private string $signatureActeEngagement = '0';

    /**
     * @ORM\Column(name="archiveMetaDescription", type="text", nullable=true)
     */
    private ?string $archiveMetaDescription = null;

    /**
     * @ORM\Column(name="archiveMetaMotsClef", type="text", nullable=true)
     */
    private ?string $archiveMetaMotsClef = null;

    /**
     * @ORM\Column(name="archiveIdBlobZip", type="integer", nullable=true)
     */
    private ?int $archiveIdBlobZip = null;

    /**
     * @ORM\Column(name="decision_partielle", type="string", nullable=false)
     */
    private string $decisionPartielle = '0';

    /**
     * @ORM\Column(name="type_decision_a_renseigner", type="string", nullable=false)
     */
    private string $typeDecisionARenseigner = '1';

    /**
     * @ORM\Column(name="type_decision_attribution_marche", type="string", nullable=false)
     */
    private string $typeDecisionAttributionMarche = '1';

    /**
     * @ORM\Column(name="type_decision_declaration_sans_suite", type="string", nullable=false)
     */
    private string $typeDecisionDeclarationSansSuite = '1';

    /**
     * @ORM\Column(name="type_decision_declaration_infructueux", type="string", nullable=false)
     */
    private string $typeDecisionDeclarationInfructueux = '1';

    /**
     * @ORM\Column(name="type_decision_selection_entreprise", type="string", nullable=false)
     */
    private string $typeDecisionSelectionEntreprise = '1';

    /**
     * @ORM\Column(name="type_decision_attribution_accord_cadre", type="string", nullable=false)
     */
    private string $typeDecisionAttributionAccordCadre = '1';

    /**
     * @ORM\Column(name="type_decision_admission_sad", type="string", nullable=false)
     */
    private string $typeDecisionAdmissionSad = '1';

    /**
     * @ORM\Column(name="type_decision_autre", type="string", nullable=false)
     */
    private string $typeDecisionAutre = '1';

    /**
     * @ORM\Column(name="id_archiveur", type="integer", nullable=true)
     */
    private ?int $idArchiveur = null;

    /**
     * @ORM\Column(name="prenom_nom_agent_telechargement_plis", type="string", length=255, nullable=true)
     */
    private ?string $prenomNomAgentTelechargementPlis = null;

    /**
     * @ORM\Column(name="id_agent_telechargement_plis", type="integer", nullable=false)
     */
    private string|int $idAgentTelechargementPlis = '0';

    /**
     * @ORM\Column(name="path_telechargement_plis", type="string", length=255, nullable=true)
     */
    private ?string $pathTelechargementPlis = null;

    /**
     * @ORM\Column(name="date_telechargement_plis", type="string", length=20, nullable=true)
     */
    private ?string $dateTelechargementPlis = null;

    /**
     * @ORM\Column(name="service_validation_intermediaire", type="integer", nullable=true)
     */
    private ?int $serviceValidationIntermediaire = null;

    /**
     * @ORM\Column(name="env_offre_technique", type="integer", nullable=false)
     */
    private string|int $envOffreTechnique = '0';

    /**
     * @ORM\Column(name="ref_org_partenaire", type="string", length=250, nullable=false)
     */
    private string $refOrgPartenaire = '';

    /**
     * @ORM\Column(name="date_archivage", type="string", length=20, nullable=true)
     */
    private ?string $dateArchivage = null;

    /**
     * @ORM\Column(name="date_decision_annulation", type="string", length=20, nullable=true)
     */
    private ?string $dateDecisionAnnulation = null;

    /**
     * @ORM\Column(name="commentaire_annulation", type="text", length=65535, nullable=true)
     */
    private ?string $commentaireAnnulation = null;

    /**
     * @ORM\Column(name="date_mise_en_ligne_souhaitee", type="string", length=20, nullable=true)
     */
    private ?string $dateMiseEnLigneSouhaitee = null;

    /**
     * @ORM\Column(name="etat_en_attente_validation", type="string", nullable=false)
     */
    private string $etatEnAttenteValidation = '1';

    /**
     * @ORM\Column(name="dossier_additif", type="string", nullable=false)
     */
    private string $dossierAdditif = '0';

    /**
     * @var ?TypeContrat
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeContrat")
     * @ORM\JoinColumn(name="type_marche", referencedColumnName="id_type_contrat", nullable=false)
     */
    private ?TypeContrat $typeMarche = null;

    /**
     * @ORM\Column(name="type_prestation", type="integer", nullable=false)
     */
    private string|int $typePrestation = '1';

    /**
     * @ORM\Column(name="date_modification", type="string", length=20, nullable=true)
     */
    private ?string $dateModification = null;

    /**
     * @ORM\Column(name="delai_partiel", type="string", nullable=false)
     */
    private string $delaiPartiel = '0';

    /**
     * @ORM\Column(name="dateFinLocale", type="string", length=20, nullable=true)
     */
    private ?string $dateFinLocale = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="lieuResidence", type="string", length=255, nullable=true)
     */
    private ?string $lieuResidence = null;

    /**
     * @ORM\Column(name="alerte", type="string", nullable=false)
     */
    private string $alerte = '0';

    /**
     * @ORM\Column(name="doublon", type="string", nullable=false)
     */
    private string $doublon = '0';

    /**
     * @ORM\Column(name="denomination_adapte", type="string", length=250, nullable=true)
     */
    private ?string $denominationAdapte = null;

    /**
     * @ORM\Column(name="url_consultation_avis_pub", type="text", length=65535, nullable=false)
     */
    private ?string $urlConsultationAvisPub = null;

    /**
     * @ORM\Column(name="doublon_de", type="string", length=250, nullable=true)
     */
    private ?string $doublonDe = null;

    /**
     * @ORM\Column(name="entite_adjudicatrice", type="string", nullable=true)
     */
    private ?string $entiteAdjudicatrice = null;

    /**
     * @ORM\Column(name="code_operation", type="string", length=255, nullable=true)
     */
    private ?string $codeOperation = null;

    /**
     * @ORM\Column(name="clause_sociale_condition_execution", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeConditionExecution = '0';

    /**
     * @ORM\Column(name="clause_sociale_insertion", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeInsertion = '0';

    /**
     * @ORM\Column(name="clause_sociale_ateliers_proteges", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeAteliersProteges = '0';

    /**
     * @ORM\Column(name="clause_sociale_siae", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeSiae = '0';

    /**
     * @ORM\Column(name="clause_sociale_ess", type="string", length=255, nullable=true)
     */
    private ?string $clauseSocialeEss = '0';

    /**
     * @ORM\Column(name="clause_env_specs_techniques", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvSpecsTechniques = '0';

    /**
     * @ORM\Column(name="clause_env_cond_execution", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvCondExecution = '0';

    /**
     * @ORM\Column(name="clause_env_criteres_select", type="string", length=255, nullable=true)
     */
    private ?string $clauseEnvCriteresSelect = '0';

    /**
     * @ORM\Column(name="id_donnee_complementaire", type="integer", nullable=true)
     */
    private ?int $idDonneeComplementaire = null;

    /**
     * @ORM\Column(name="donnee_complementaire_obligatoire", type="string", nullable=false)
     */
    private string $donneeComplementaireObligatoire = '0';

    /**
     * @ORM\Column(name="mode_ouverture_reponse", type="string", nullable=false)
     */
    private string $modeOuvertureReponse = '0';

    /**
     * @ORM\Column(name="id_fichier_annulation", type="integer", nullable=true)
     */
    private ?int $idFichierAnnulation = null;

    /**
     * @ORM\Column(name="idOperation", type="integer", nullable=true)
     */
    private ?int $idOperation = null;

    /**
     * @ORM\Column(name="marche_public_simplifie", type="string", nullable=false)
     */
    private string $marchePublicSimplifie = '0';

    /**
     * @ORM\Column(name="infos_blocs_atlas", type="string", length=10, nullable=false)
     */
    private string $infosBlocsAtlas = '0##0';

    /**
     * @ORM\Column(name="DATE_FIN_UNIX", type="string", length=20, nullable=true)
     */
    private ?string $dateFinUnix = '0';

    /**
     * @ORM\Column(name="numero_AC", type="string", length=255, nullable=true)
     */
    private ?string $numeroAc = null;

    /**
     * @ORM\Column(name="id_contrat", type="integer", nullable=true)
     */
    private ?int $idContrat = null;

    /**
     * @ORM\Column(name="pin_api_sgmap_mps", type="string", length=20, nullable=true)
     */
    private ?string $pinApiSgmapMps = null;

    private $certificatChiffrementsNonDouble;

    /**
     * @ORM\Column(name="dume_demande", type="string", length=20, nullable=false)
     */
    private int|string $dumeDemande = 0;

    /**
     * @ORM\Column(name="type_procedure_dume", type="integer", nullable=false)
     */
    private int $typeProcedureDume = 0;

    /**
     * @ORM\Column(name="type_formulaire_dume", type="integer", nullable=false)
     */
    private int $typeFormulaireDume = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="marche_insertion", type="boolean", nullable=false)
     */
    private $marcheInsertion = false;

    /**
     * @ORM\Column(name="clause_specification_technique", type="string", length=255, nullable=true)
     */
    private ?string $clauseSpecificationTechnique = '0';

    /**
     * @ORM\Column(name="source_externe", type="string", length=255, nullable=true)
     */
    private ?string $sourceExterne = null;

    /**
     * @var string
     *
     * @ORM\Column(name="id_source_externe", type="integer", length=11, nullable=true)
     */
    private $idSourceExterne;

    /**
     * @ORM\Column(name="attestation_consultation", type="string", length=2, nullable=false)
     */
    private string $attestationConsultation = '0';

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux" , inversedBy="consultations")
     * @ORM\JoinColumn(name="id_dossier_volumineux", referencedColumnName="id")
     */
    private ?DossierVolumineux $dossierVolumineux = null;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ConsultationFavoris", mappedBy="consultation")
     */
    private Collection $consultationFavoris;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\QuestionDCE",
     *     mappedBy="consultation", cascade={"persist"}, fetch = "EXTRA_LAZY")
     */
    private Collection $questions;

    /**
     * @ORM\Column(name="code_externe", type="string", length=255, nullable=true)
     */
    private ?string $codeExterne = null;

    /**
     * @ORM\Column(name="version_messagerie", type="integer")
     */
    private ?int $versionMessagerie = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Configuration\PlateformeVirtuelle", fetch="EAGER")
     * @ORM\JoinColumn(name="plateforme_virtuelle_id", referencedColumnName="id")
     */
    private ?PlateformeVirtuelle $plateformeVirtuelle = null;

    /**
     * @ORM\Column(type="boolean", options={"default":"0"})
     */
    private $isEnvoiPubliciteValidation;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    private $cmsActif = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\AutrePieceConsultation", mappedBy="consultationId")
     */
    private Collection $autresPieceConsultation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PieceGenereConsultation", mappedBy="consultation")
     */
    private Collection $piecesGenere;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $annexeFinanciere;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offre\AnnexeFinanciere", mappedBy="consultation")
     * @ORM\JoinColumn(referencedColumnName="id_consultation")
     */
    private Collection $annexesFinanciereManuel;

    /**
     * @var bool
     *
     * @ORM\Column(name="envol_activation", type="boolean", options={"default":"0"})
     */
    private $envolActivation;

    /**
     * @ORM\Column(name="groupement", type="boolean", nullable=true)
     */
    private ?bool $groupement = null;

    /**
     * @ORM\Column(name="autre_technique_achat", type="string", length=255, nullable=true)
     */
    private ?string $autreTechniqueAchat = null;

    /**
     * @ORM\Column(name="procedure_ouverte", type="boolean", nullable=true)
     */
    private ?bool $procedureOuverte = null;

    /**
     * Cette propriété non mappé est utilisé dans le cadre du ConsultationItemDataProvider pour gérer le statutCalule
     */
    private ?string $calculatedStatus = null;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private ?string $codeDceRestreint = null;


    /**
     * @var int
     * @ORM\Column(type="integer", options={"default":"1000"})
     */
    private $controleTailleDepot = 1000;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $numeroProjetAchat;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ClausesN1", mappedBy="consultation", orphanRemoval=true)
     */
    private Collection $clausesN1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ConsultationTags", mappedBy="consultation", orphanRemoval=true)
     */
    private Collection $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DCE", mappedBy="consultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $dce;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RG", mappedBy="consultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $rc;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Complement", mappedBy="consultation")
     * @ORM\JoinColumn(name="id", referencedColumnName="consultation_id")
     */
    private Collection $autresPieces;

    /**
     * @ORM\Column(name="donnee_publicite_obligatoire", type="string", nullable=true)
     */
    private ?string $donneePubliciteObligatoire = '0';

    /**
     * @ORM\ManyToOne(targetEntity=ReferentielAchat::class)
     */
    private $referentielAchat;

    /**
     * @ORM\Column(type="uuid", unique=true)
     */
    private ?UuidInterface $uuid;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $besoinRecurrent = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $contratExecUuid = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $oldServiceId;

    /**
     * Consultation constructor.
     */
    public function __construct()
    {
        $this->offres = new ArrayCollection();
        $this->lots = new ArrayCollection();
        $this->offresPapiers = new ArrayCollection();
        $this->certificatChiffrements = new ArrayCollection();
        $this->consultationFavoris = new ArrayCollection();
        $this->questions = new ArrayCollection();
        $this->autresPieceConsultation = new ArrayCollection();
        $this->piecesGenere = new ArrayCollection();
        $this->annexesFinanciereManuel = new ArrayCollection();
        $this->bourses = new ArrayCollection();
        $this->telechargementAnonymes = new ArrayCollection();
        $this->telechargements = new ArrayCollection();
        $this->dumeContextes = new ArrayCollection();
        $this->candidatures = new ArrayCollection();
        $this->listLots = new ArrayCollection();
        $this->referenceConsultationsChildren = new ArrayCollection();
        $this->clausesN1 = new ArrayCollection();
        $this->tags = new ArrayCollection();
        $this->dce = new ArrayCollection();
        $this->rc = new ArrayCollection();
        $this->autresPieces = new ArrayCollection();
        $this->retraitPapiers = new ArrayCollection();
        $this->uuid = Uuid::uuid4();
    }

    /**
     * Set reference.
     *
     * @param int $reference
     *
     * @return Consultation
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference.
     *
     * @return int
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set acronyme org.
     *
     * @param string $organisme
     *
     * @return Consultation
     */
    public function setAcronymeOrg($acronymeOrg)
    {
        $this->acronymeOrg = $acronymeOrg;

        return $this;
    }

    /**
     * Get acronyme org.
     *
     * @return string
     */
    public function getAcronymeOrg()
    {
        return $this->acronymeOrg;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set referenceUtilisateur.
     *
     * @param string $referenceUtilisateur
     *
     * @return Consultation
     */
    public function setReferenceUtilisateur($referenceUtilisateur)
    {
        $this->referenceUtilisateur = $referenceUtilisateur;

        return $this;
    }

    /**
     * Get referenceUtilisateur.
     *
     * @return string
     */
    public function getReferenceUtilisateur()
    {
        return $this->referenceUtilisateur;
    }

    /**
     * Set categorie.
     *
     * @param string $categorie
     *
     * @return Consultation
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie.
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Set titre.
     *
     * @param string $titre
     *
     * @return Consultation
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre.
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set resume.
     *
     * @param string $resume
     *
     * @return Consultation
     */
    public function setResume($resume)
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * Get resume.
     *
     * @return string
     */
    public function getResume()
    {
        return $this->resume;
    }

    /**
     * Set datedebut.
     *
     * @param DateTime $datedebut
     *
     * @return Consultation
     */
    public function setDatedebut($datedebut)
    {
        $this->datedebut = $datedebut;

        return $this;
    }

    /**
     * Get datedebut.
     *
     * @return DateTime
     */
    public function getDatedebut()
    {
        return $this->datedebut;
    }

    public function setDatefin(string|DateTimeInterface $datefin): self
    {
        $this->datefin = $datefin;

        return $this;
    }

    /**
     * Get datefin.
     *
     * @return DateTime
     */
    public function getDatefin()
    {
        return $this->datefin;
    }

    /**
     * Set datevalidation.
     *
     * @param string $datevalidation
     *
     * @return Consultation
     */
    public function setDatevalidation($datevalidation)
    {
        $this->datevalidation = $datevalidation;

        return $this;
    }

    /**
     * Get datevalidation.
     *
     * @return string
     */
    public function getDatevalidation()
    {
        return $this->datevalidation;
    }

    /**
     * Set codeProcedure.
     *
     * @param string $codeProcedure
     *
     * @return Consultation
     */
    public function setCodeProcedure($codeProcedure)
    {
        $this->codeProcedure = $codeProcedure;

        return $this;
    }

    /**
     * Get codeProcedure.
     *
     * @return string
     */
    public function getCodeProcedure()
    {
        return $this->codeProcedure;
    }

    /**
     * Set reponseElectronique.
     *
     * @param string $reponseElectronique
     *
     * @return Consultation
     */
    public function setReponseElectronique($reponseElectronique)
    {
        $this->reponseElectronique = $reponseElectronique;

        return $this;
    }

    /**
     * Get reponseElectronique.
     *
     * @return string
     */
    public function getReponseElectronique()
    {
        return $this->reponseElectronique;
    }

    /**
     * Set numProcedure.
     *
     * @param int $numProcedure
     *
     * @return Consultation
     */
    public function setNumProcedure($numProcedure)
    {
        $this->numProcedure = $numProcedure;

        return $this;
    }

    /**
     * Get numProcedure.
     *
     * @return int
     */
    public function getNumProcedure()
    {
        return $this->numProcedure;
    }

    /**
     * Set idTypeProcedure.
     *
     * @param int $idTypeProcedure
     *
     * @return Consultation
     */
    public function setIdTypeProcedure(int $idTypeProcedure): self
    {
        $this->idTypeProcedure = $idTypeProcedure;

        return $this;
    }

    /**
     * Get idTypeProcedure.
     *
     * @return int
     */
    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    /**
     * Set idTypeAvis.
     *
     * @param int $idTypeAvis
     *
     * @return Consultation
     */
    public function setIdTypeAvis($idTypeAvis)
    {
        $this->idTypeAvis = $idTypeAvis;

        return $this;
    }

    /**
     * Get idTypeAvis.
     *
     * @return int
     */
    public function getIdTypeAvis()
    {
        return $this->idTypeAvis;
    }

    /**
     * Set lieuxExecution.
     *
     * @param string $lieuExecution
     * @return Consultation
     */
    public function setLieuExecution($lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;

        return $this;
    }

    /**
     * Get lieuxExecution.
     *
     * @return string
     */
    public function getLieuExecution()
    {
        return $this->lieuExecution;
    }

    /**
     * Set typeMiseEnLigne.
     *
     * @param int $typeMiseEnLigne
     *
     * @return Consultation
     */
    public function setTypeMiseEnLigne($typeMiseEnLigne)
    {
        $this->typeMiseEnLigne = $typeMiseEnLigne;

        return $this;
    }

    /**
     * Get typeMiseEnLigne.
     *
     * @return int
     */
    public function getTypeMiseEnLigne()
    {
        return $this->typeMiseEnLigne;
    }

    /**
     * Set datemiseenligne.
     *
     * @param DateTime $datemiseenligne
     *
     * @return Consultation
     */
    public function setDatemiseenligne($datemiseenligne)
    {
        $this->datemiseenligne = $datemiseenligne;

        return $this;
    }

    /**
     * Get datemiseenligne.
     *
     * @return DateTime
     */
    public function getDatemiseenligne()
    {
        return $this->datemiseenligne;
    }

    /**
     * Set isTiersAvis.
     *
     * @param string $isTiersAvis
     *
     * @return Consultation
     */
    public function setIsTiersAvis($isTiersAvis)
    {
        $this->isTiersAvis = $isTiersAvis;

        return $this;
    }

    /**
     * Get isTiersAvis.
     *
     * @return string
     */
    public function getIsTiersAvis()
    {
        return $this->isTiersAvis;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Consultation
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set datefinSad.
     *
     * @param DateTime $datefinSad
     *
     * @return Consultation
     */
    public function setDatefinSad($datefinSad)
    {
        $this->datefinSad = $datefinSad;

        return $this;
    }

    /**
     * Get datefinSad.
     *
     * @return DateTime
     */
    public function getDatefinSad()
    {
        return $this->datefinSad;
    }

    /**
     * Set isSysAcqDyn.
     *
     * @param int $isSysAcqDyn
     *
     * @return Consultation
     */
    public function setIsSysAcqDyn($isSysAcqDyn)
    {
        $this->isSysAcqDyn = $isSysAcqDyn;

        return $this;
    }

    /**
     * Get isSysAcqDyn.
     *
     * @return int
     */
    public function getIsSysAcqDyn()
    {
        return $this->isSysAcqDyn;
    }

    public function setReferenceConsultationInit(?int $referenceConsultationInit): self
    {
        $this->referenceConsultationInit = $referenceConsultationInit;

        return $this;
    }

    public function getReferenceConsultationInit(): string|int|null
    {
        return $this->referenceConsultationInit;
    }

    /**
     * Set signatureOffre.
     *
     * @param string $signatureOffre
     *
     * @return Consultation
     */
    public function setSignatureOffre($signatureOffre)
    {
        $this->signatureOffre = $signatureOffre;

        return $this;
    }

    /**
     * Get signatureOffre.
     *
     * @return string
     */
    public function getSignatureOffre()
    {
        return $this->signatureOffre;
    }

    /**
     * Set idTypeValidation.
     *
     * @param int $idTypeValidation
     *
     * @return Consultation
     */
    public function setIdTypeValidation($idTypeValidation)
    {
        $this->idTypeValidation = $idTypeValidation;

        return $this;
    }

    /**
     * Get idTypeValidation.
     *
     * @return int
     */
    public function getIdTypeValidation()
    {
        return $this->idTypeValidation;
    }

    /**
     * Set etatApprobation.
     *
     * @param string $etatApprobation
     *
     * @return Consultation
     */
    public function setEtatApprobation($etatApprobation)
    {
        $this->etatApprobation = $etatApprobation;

        return $this;
    }

    /**
     * Get etatApprobation.
     *
     * @return string
     */
    public function getEtatApprobation()
    {
        return $this->etatApprobation;
    }

    /**
     * Set etatValidation.
     *
     * @param string $etatValidation
     *
     * @return Consultation
     */
    public function setEtatValidation($etatValidation)
    {
        $this->etatValidation = $etatValidation;

        return $this;
    }

    /**
     * Get etatValidation.
     *
     * @return string
     */
    public function getEtatValidation()
    {
        return $this->etatValidation;
    }

    public function setChampSuppInvisible(?string $champSuppInvisible): self
    {
        $this->champSuppInvisible = $champSuppInvisible;

        return $this;
    }

    public function getChampSuppInvisible(): ?string
    {
        return $this->champSuppInvisible;
    }

    /**
     * Set codeCpv1.
     *
     * @param string $codeCpv1
     *
     * @return Consultation
     */
    public function setCodeCpv1($codeCpv1)
    {
        $this->codeCpv1 = $codeCpv1;

        return $this;
    }

    /**
     * Get codeCpv1.
     *
     * @return string
     */
    public function getCodeCpv1()
    {
        return $this->codeCpv1;
    }

    /**
     * Set codeCpv2.
     *
     * @param string $codeCpv2
     *
     * @return Consultation
     */
    public function setCodeCpv2($codeCpv2)
    {
        $this->codeCpv2 = $codeCpv2;

        return $this;
    }

    /**
     * Get codeCpv2.
     *
     * @return string
     */
    public function getCodeCpv2()
    {
        return $this->codeCpv2;
    }

    /**
     * Set publicationEurope.
     *
     * @param string $publicationEurope
     *
     * @return Consultation
     */
    public function setPublicationEurope($publicationEurope)
    {
        $this->publicationEurope = $publicationEurope;

        return $this;
    }

    /**
     * Get publicationEurope.
     *
     * @return string
     */
    public function getPublicationEurope()
    {
        return $this->publicationEurope;
    }

    /**
     * Set etatPublication.
     *
     * @param int $etatPublication
     *
     * @return Consultation
     */
    public function setEtatPublication($etatPublication)
    {
        $this->etatPublication = $etatPublication;

        return $this;
    }

    /**
     * Get etatPublication.
     *
     * @return int
     */
    public function getEtatPublication()
    {
        return $this->etatPublication;
    }

    /**
     * Set poursuivreAffichage.
     *
     * @param int $poursuivreAffichage
     *
     * @return Consultation
     */
    public function setPoursuivreAffichage($poursuivreAffichage)
    {
        $this->poursuivreAffichage = $poursuivreAffichage;

        return $this;
    }

    /**
     * Get poursuivreAffichage.
     *
     * @return int
     */
    public function getPoursuivreAffichage()
    {
        return $this->poursuivreAffichage;
    }

    /**
     * Set poursuivreAffichageUnite.
     *
     * @param string $poursuivreAffichageUnite
     *
     * @return Consultation
     */
    public function setPoursuivreAffichageUnite($poursuivreAffichageUnite)
    {
        $this->poursuivreAffichageUnite = $poursuivreAffichageUnite;

        return $this;
    }

    /**
     * Get poursuivreAffichageUnite.
     *
     * @return string
     */
    public function getPoursuivreAffichageUnite()
    {
        return $this->poursuivreAffichageUnite;
    }

    /**
     * Set nbrTelechargementDce.
     *
     * @param int $nbrTelechargementDce
     *
     * @return Consultation
     */
    public function setNbrTelechargementDce($nbrTelechargementDce)
    {
        $this->nbrTelechargementDce = $nbrTelechargementDce;

        return $this;
    }

    /**
     * Get nbrTelechargementDce.
     *
     * @return int
     */
    public function getNbrTelechargementDce()
    {
        return $this->nbrTelechargementDce;
    }

    /**
     * Set serviceId.
     *
     * @param int $serviceId
     *
     * @return Consultation
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * Get serviceId.
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Set serviceAssocieId.
     *
     * @param int $serviceAssocieId
     *
     * @return Consultation
     */
    public function setServiceAssocieId($serviceAssocieId)
    {
        $this->serviceAssocieId = $serviceAssocieId;

        return $this;
    }

    /**
     * Get serviceAssocieId.
     *
     * @return int
     */
    public function getServiceAssocieId()
    {
        return $this->serviceAssocieId;
    }

    /**
     * Set detailConsultation.
     *
     * @param string $detailConsultation
     *
     * @return Consultation
     */
    public function setDetailConsultation($detailConsultation)
    {
        $this->detailConsultation = $detailConsultation;

        return $this;
    }

    /**
     * Get detailConsultation.
     *
     * @return string
     */
    public function getDetailConsultation()
    {
        return $this->detailConsultation;
    }

    /**
     * Set dateFinAffichage.
     *
     * @param string $dateFinAffichage
     *
     * @return Consultation
     */
    public function setDateFinAffichage($dateFinAffichage)
    {
        $this->dateFinAffichage = $dateFinAffichage;

        return $this;
    }

    /**
     * Get dateFinAffichage.
     *
     * @return string
     */
    public function getDateFinAffichage()
    {
        return $this->dateFinAffichage;
    }

    /**
     * Set depouillablePhaseConsultation.
     *
     * @param string $depouillablePhaseConsultation
     *
     * @return Consultation
     */
    public function setDepouillablePhaseConsultation($depouillablePhaseConsultation)
    {
        $this->depouillablePhaseConsultation = $depouillablePhaseConsultation;

        return $this;
    }

    /**
     * Get depouillablePhaseConsultation.
     *
     * @return string
     */
    public function getDepouillablePhaseConsultation()
    {
        return $this->depouillablePhaseConsultation;
    }

    /**
     * Set consultationTransverse.
     *
     * @param string $consultationTransverse
     *
     * @return Consultation
     */
    public function setConsultationTransverse($consultationTransverse)
    {
        $this->consultationTransverse = $consultationTransverse;

        return $this;
    }

    /**
     * Get consultationTransverse.
     *
     * @return string
     */
    public function getConsultationTransverse()
    {
        return $this->consultationTransverse;
    }

    /**
     * Set consultationAchatPublique.
     *
     * @param string $consultationAchatPublique
     *
     * @return Consultation
     */
    public function setConsultationAchatPublique($consultationAchatPublique)
    {
        $this->consultationAchatPublique = $consultationAchatPublique;

        return $this;
    }

    /**
     * Get consultationAchatPublique.
     *
     * @return string
     */
    public function getConsultationAchatPublique()
    {
        return $this->consultationAchatPublique;
    }

    /**
     * Set urlConsultationAchatPublique.
     *
     * @param string $urlConsultationAchatPublique
     *
     * @return Consultation
     */
    public function setUrlConsultationAchatPublique($urlConsultationAchatPublique)
    {
        $this->urlConsultationAchatPublique = $urlConsultationAchatPublique;

        return $this;
    }

    /**
     * Get urlConsultationAchatPublique.
     *
     * @return string
     */
    public function getUrlConsultationAchatPublique()
    {
        return $this->urlConsultationAchatPublique;
    }

    /**
     * Set partialDceDownload.
     *
     * @param string $partialDceDownload
     *
     * @return Consultation
     */
    public function setPartialDceDownload($partialDceDownload)
    {
        $this->partialDceDownload = $partialDceDownload;

        return $this;
    }

    /**
     * Get partialDceDownload.
     *
     * @return string
     */
    public function getPartialDceDownload()
    {
        return $this->partialDceDownload;
    }

    /**
     * Set tiragePlan.
     *
     * @param int $tiragePlan
     *
     * @return Consultation
     */
    public function setTiragePlan($tiragePlan)
    {
        $this->tiragePlan = $tiragePlan;

        return $this;
    }

    /**
     * Get tiragePlan.
     *
     * @return int
     */
    public function getTiragePlan()
    {
        return $this->tiragePlan;
    }

    /**
     * Set tireurPlan.
     *
     * @param int $tireurPlan
     *
     * @return Consultation
     */
    public function setTireurPlan($tireurPlan)
    {
        $this->tireurPlan = $tireurPlan;

        return $this;
    }

    /**
     * Get tireurPlan.
     *
     * @return int
     */
    public function getTireurPlan()
    {
        return $this->tireurPlan;
    }

    /**
     * Set dateMiseEnLigneCalcule.
     *
     * @param ?DateTime $dateMiseEnLigneCalcule
     *
     * @return Consultation
     */
    public function setDateMiseEnLigneCalcule($dateMiseEnLigneCalcule)
    {
        $this->dateMiseEnLigneCalcule = $dateMiseEnLigneCalcule;

        return $this;
    }

    /**
     * Get dateMiseEnLigneCalcule.
     *
     * @return ?DateTime
     */
    public function getDateMiseEnLigneCalcule()
    {
        return $this->dateMiseEnLigneCalcule;
    }

    /**
     * Set accessibiliteEn.
     *
     * @param string $accessibiliteEn
     *
     * @return Consultation
     */
    public function setAccessibiliteEn($accessibiliteEn)
    {
        $this->accessibiliteEn = $accessibiliteEn;

        return $this;
    }

    /**
     * Get accessibiliteEn.
     *
     * @return string
     */
    public function getAccessibiliteEn()
    {
        return $this->accessibiliteEn;
    }

    /**
     * Set accessibiliteEs.
     *
     * @param string $accessibiliteEs
     *
     * @return Consultation
     */
    public function setAccessibiliteEs($accessibiliteEs)
    {
        $this->accessibiliteEs = $accessibiliteEs;

        return $this;
    }

    /**
     * Get accessibiliteEs.
     *
     * @return string
     */
    public function getAccessibiliteEs()
    {
        return $this->accessibiliteEs;
    }

    /**
     * Set nbrReponse.
     *
     * @param int $nbrReponse
     *
     * @return Consultation
     */
    public function setNbrReponse($nbrReponse)
    {
        $this->nbrReponse = $nbrReponse;

        return $this;
    }

    /**
     * Get nbrReponse.
     *
     * @return int
     */
    public function getNbrReponse()
    {
        return $this->nbrReponse;
    }

    /**
     * Set idTypeProcedureOrg.
     *
     * @param int $idTypeProcedureOrg
     *
     * @return Consultation
     */
    public function setIdTypeProcedureOrg($idTypeProcedureOrg)
    {
        $this->idTypeProcedureOrg = $idTypeProcedureOrg;

        return $this;
    }

    /**
     * Get idTypeProcedureOrg.
     *
     * @return int
     */
    public function getIdTypeProcedureOrg()
    {
        return $this->idTypeProcedureOrg;
    }

    /**
     * Set tirageDescriptif.
     *
     * @param string $tirageDescriptif
     *
     * @return Consultation
     */
    public function setTirageDescriptif($tirageDescriptif)
    {
        $this->tirageDescriptif = $tirageDescriptif;

        return $this;
    }

    /**
     * Get tirageDescriptif.
     *
     * @return string
     */
    public function getTirageDescriptif()
    {
        return $this->tirageDescriptif;
    }

    /**
     * Set organismeConsultationInit.
     *
     * @param string $organismeConsultationInit
     *
     * @return Consultation
     */
    public function setOrganismeConsultationInit($organismeConsultationInit)
    {
        $this->organismeConsultationInit = $organismeConsultationInit;

        return $this;
    }

    /**
     * Get organismeConsultationInit.
     *
     * @return string
     */
    public function getOrganismeConsultationInit()
    {
        return $this->organismeConsultationInit;
    }

    /**
     * Set dateValidationIntermediaire.
     *
     * @param string $dateValidationIntermediaire
     *
     * @return Consultation
     */
    public function setDateValidationIntermediaire($dateValidationIntermediaire)
    {
        $this->dateValidationIntermediaire = $dateValidationIntermediaire;

        return $this;
    }

    /**
     * Get dateValidationIntermediaire.
     *
     * @return string
     */
    public function getDateValidationIntermediaire()
    {
        return $this->dateValidationIntermediaire;
    }

    /**
     * Set accessibiliteFr.
     *
     * @param string $accessibiliteFr
     *
     * @return Consultation
     */
    public function setAccessibiliteFr($accessibiliteFr)
    {
        $this->accessibiliteFr = $accessibiliteFr;

        return $this;
    }

    /**
     * Get accessibiliteFr.
     *
     * @return string
     */
    public function getAccessibiliteFr()
    {
        return $this->accessibiliteFr;
    }

    /**
     * Set idTrAccessibilite.
     *
     * @param int $idTrAccessibilite
     *
     * @return Consultation
     */
    public function setIdTrAccessibilite($idTrAccessibilite)
    {
        $this->idTrAccessibilite = $idTrAccessibilite;

        return $this;
    }

    /**
     * Get idTrAccessibilite.
     *
     * @return int
     */
    public function getIdTrAccessibilite()
    {
        return $this->idTrAccessibilite;
    }

    /**
     * Set accessibiliteCz.
     *
     * @param string $accessibiliteCz
     *
     * @return Consultation
     */
    public function setAccessibiliteCz($accessibiliteCz)
    {
        $this->accessibiliteCz = $accessibiliteCz;

        return $this;
    }

    /**
     * Get accessibiliteCz.
     *
     * @return string
     */
    public function getAccessibiliteCz()
    {
        return $this->accessibiliteCz;
    }

    /**
     * Set accessibiliteDu.
     *
     * @param string $accessibiliteDu
     *
     * @return Consultation
     */
    public function setAccessibiliteDu($accessibiliteDu)
    {
        $this->accessibiliteDu = $accessibiliteDu;

        return $this;
    }

    /**
     * Get accessibiliteDu.
     *
     * @return string
     */
    public function getAccessibiliteDu()
    {
        return $this->accessibiliteDu;
    }

    /**
     * Set accessibiliteSu.
     *
     * @param string $accessibiliteSu
     *
     * @return Consultation
     */
    public function setAccessibiliteSu($accessibiliteSu)
    {
        $this->accessibiliteSu = $accessibiliteSu;

        return $this;
    }

    /**
     * Get accessibiliteSu.
     *
     * @return string
     */
    public function getAccessibiliteSu()
    {
        return $this->accessibiliteSu;
    }

    /**
     * Set accessibiliteAr.
     *
     * @param string $accessibiliteAr
     *
     * @return Consultation
     */
    public function setAccessibiliteAr($accessibiliteAr)
    {
        $this->accessibiliteAr = $accessibiliteAr;

        return $this;
    }

    /**
     * Get accessibiliteAr.
     *
     * @return string
     */
    public function getAccessibiliteAr()
    {
        return $this->accessibiliteAr;
    }

    /**
     * Set alloti.
     *
     * @param string $alloti
     *
     * @return Consultation
     */
    public function setAlloti($alloti)
    {
        $this->alloti = $alloti;

        return $this;
    }

    /**
     * Get alloti.
     *
     * @return string
     */
    public function getAlloti()
    {
        return $this->alloti;
    }

    /**
     * Set numeroPhase.
     *
     * @param int $numeroPhase
     *
     * @return Consultation
     */
    public function setNumeroPhase($numeroPhase)
    {
        $this->numeroPhase = $numeroPhase;

        return $this;
    }

    /**
     * Get numeroPhase.
     *
     * @return int
     */
    public function getNumeroPhase()
    {
        return $this->numeroPhase;
    }

    /**
     * Set consultationExterne.
     *
     * @param string $consultationExterne
     *
     * @return Consultation
     */
    public function setConsultationExterne($consultationExterne)
    {
        $this->consultationExterne = $consultationExterne;

        return $this;
    }

    /**
     * Get consultationExterne.
     *
     * @return string
     */
    public function getConsultationExterne()
    {
        return $this->consultationExterne;
    }

    /**
     * Set urlConsultationExterne.
     *
     * @param string $urlConsultationExterne
     *
     * @return Consultation
     */
    public function setUrlConsultationExterne($urlConsultationExterne)
    {
        $this->urlConsultationExterne = $urlConsultationExterne;

        return $this;
    }

    /**
     * Get urlConsultationExterne.
     *
     * @return string
     */
    public function getUrlConsultationExterne()
    {
        return $this->urlConsultationExterne;
    }

    /**
     * Set orgDenomination.
     *
     * @param string $orgDenomination
     *
     * @return Consultation
     */
    public function setOrgDenomination($orgDenomination)
    {
        $this->orgDenomination = $orgDenomination;

        return $this;
    }

    /**
     * Get orgDenomination.
     *
     * @return string
     */
    public function getOrgDenomination()
    {
        return $this->orgDenomination;
    }

    /**
     * Set domainesActivites.
     *
     * @param string $domainesActivites
     *
     * @return Consultation
     */
    public function setDomainesActivites($domainesActivites)
    {
        $this->domainesActivites = $domainesActivites;

        return $this;
    }

    /**
     * Get domainesActivites.
     *
     * @return string
     */
    public function getDomainesActivites()
    {
        return $this->domainesActivites;
    }

    /**
     * Set idAffaire.
     *
     * @param int $idAffaire
     *
     * @return Consultation
     */
    public function setIdAffaire($idAffaire)
    {
        $this->idAffaire = $idAffaire;

        return $this;
    }

    /**
     * Get idAffaire.
     *
     * @return int
     */
    public function getIdAffaire()
    {
        return $this->idAffaire;
    }

    /**
     * Set adresseRetraisDossiers.
     *
     * @param string $adresseRetraisDossiers
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiers($adresseRetraisDossiers)
    {
        $this->adresseRetraisDossiers = $adresseRetraisDossiers;

        return $this;
    }

    /**
     * Get adresseRetraisDossiers.
     *
     * @return string
     */
    public function getAdresseRetraisDossiers()
    {
        return $this->adresseRetraisDossiers;
    }

    /**
     * Set cautionProvisoire.
     *
     * @param string $cautionProvisoire
     *
     * @return Consultation
     */
    public function setCautionProvisoire($cautionProvisoire)
    {
        $this->cautionProvisoire = $cautionProvisoire;

        return $this;
    }

    /**
     * Get cautionProvisoire.
     *
     * @return string
     */
    public function getCautionProvisoire()
    {
        return $this->cautionProvisoire;
    }

    /**
     * Set adresseDepotOffres.
     *
     * @param string $adresseDepotOffres
     *
     * @return Consultation
     */
    public function setAdresseDepotOffres($adresseDepotOffres)
    {
        $this->adresseDepotOffres = $adresseDepotOffres;

        return $this;
    }

    /**
     * Get adresseDepotOffres.
     *
     * @return string
     */
    public function getAdresseDepotOffres()
    {
        return $this->adresseDepotOffres;
    }

    /**
     * Set lieuOuverturePlis.
     *
     * @param string $lieuOuverturePlis
     *
     * @return Consultation
     */
    public function setLieuOuverturePlis($lieuOuverturePlis)
    {
        $this->lieuOuverturePlis = $lieuOuverturePlis;

        return $this;
    }

    /**
     * Get lieuOuverturePlis.
     *
     * @return string
     */
    public function getLieuOuverturePlis()
    {
        return $this->lieuOuverturePlis;
    }

    /**
     * Set prixAquisitionPlans.
     *
     * @param string $prixAquisitionPlans
     *
     * @return Consultation
     */
    public function setPrixAquisitionPlans($prixAquisitionPlans)
    {
        $this->prixAquisitionPlans = $prixAquisitionPlans;

        return $this;
    }

    /**
     * Get prixAquisitionPlans.
     *
     * @return string
     */
    public function getPrixAquisitionPlans()
    {
        return $this->prixAquisitionPlans;
    }

    /**
     * Set qualification.
     *
     * @param string $qualification
     *
     * @return Consultation
     */
    public function setQualification($qualification)
    {
        $this->qualification = $qualification;

        return $this;
    }

    /**
     * Get qualification.
     *
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * Set agrements.
     *
     * @param string $agrements
     *
     * @return Consultation
     */
    public function setAgrements($agrements)
    {
        $this->agrements = $agrements;

        return $this;
    }

    /**
     * Get agrements.
     *
     * @return string
     */
    public function getAgrements()
    {
        return $this->agrements;
    }

    /**
     * Set addEchantillion.
     *
     * @param string $addEchantillion
     *
     * @return Consultation
     */
    public function setAddEchantillion($addEchantillion)
    {
        $this->addEchantillion = $addEchantillion;

        return $this;
    }

    /**
     * Get addEchantillion.
     *
     * @return string
     */
    public function getAddEchantillion()
    {
        return $this->addEchantillion;
    }

    /**
     * Set dateLimiteEchantillion.
     *
     * @param string $dateLimiteEchantillion
     *
     * @return Consultation
     */
    public function setDateLimiteEchantillion($dateLimiteEchantillion)
    {
        $this->dateLimiteEchantillion = $dateLimiteEchantillion;

        return $this;
    }

    /**
     * Get dateLimiteEchantillion.
     *
     * @return string
     */
    public function getDateLimiteEchantillion()
    {
        return $this->dateLimiteEchantillion;
    }

    /**
     * Set addReunion.
     *
     * @param string $addReunion
     *
     * @return Consultation
     */
    public function setAddReunion($addReunion)
    {
        $this->addReunion = $addReunion;

        return $this;
    }

    /**
     * Get addReunion.
     *
     * @return string
     */
    public function getAddReunion()
    {
        return $this->addReunion;
    }

    /**
     * Set dateReunion.
     *
     * @param string $dateReunion
     *
     * @return Consultation
     */
    public function setDateReunion($dateReunion)
    {
        $this->dateReunion = $dateReunion;

        return $this;
    }

    /**
     * Get dateReunion.
     *
     * @return string
     */
    public function getDateReunion()
    {
        return $this->dateReunion;
    }

    /**
     * Set variantes.
     *
     * @param string $variantes
     *
     * @return Consultation
     */
    public function setVariantes($variantes)
    {
        $this->variantes = $variantes;

        return $this;
    }

    /**
     * Get variantes.
     *
     * @return string
     */
    public function getVariantes()
    {
        return $this->variantes;
    }

    /**
     * Set adresseDepotOffresAr.
     *
     * @param string $adresseDepotOffresAr
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresAr($adresseDepotOffresAr)
    {
        $this->adresseDepotOffresAr = $adresseDepotOffresAr;

        return $this;
    }

    /**
     * Get adresseDepotOffresAr.
     *
     * @return string
     */
    public function getAdresseDepotOffresAr()
    {
        return $this->adresseDepotOffresAr;
    }

    /**
     * Set lieuOuverturePlisAr.
     *
     * @param string $lieuOuverturePlisAr
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisAr($lieuOuverturePlisAr)
    {
        $this->lieuOuverturePlisAr = $lieuOuverturePlisAr;

        return $this;
    }

    /**
     * Get lieuOuverturePlisAr.
     *
     * @return string
     */
    public function getLieuOuverturePlisAr()
    {
        return $this->lieuOuverturePlisAr;
    }

    /**
     * Set adresseRetraisDossiersAr.
     *
     * @param string $adresseRetraisDossiersAr
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersAr($adresseRetraisDossiersAr)
    {
        $this->adresseRetraisDossiersAr = $adresseRetraisDossiersAr;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersAr.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersAr()
    {
        return $this->adresseRetraisDossiersAr;
    }

    /**
     * Set piecesDossierAdmin.
     *
     * @param string $piecesDossierAdmin
     *
     * @return Consultation
     */
    public function setPiecesDossierAdmin($piecesDossierAdmin)
    {
        $this->piecesDossierAdmin = $piecesDossierAdmin;

        return $this;
    }

    /**
     * Get piecesDossierAdmin.
     *
     * @return string
     */
    public function getPiecesDossierAdmin()
    {
        return $this->piecesDossierAdmin;
    }

    /**
     * Set piecesDossierAdminFr.
     *
     * @param string $piecesDossierAdminFr
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminFr($piecesDossierAdminFr)
    {
        $this->piecesDossierAdminFr = $piecesDossierAdminFr;

        return $this;
    }

    /**
     * Get piecesDossierAdminFr.
     *
     * @return string
     */
    public function getPiecesDossierAdminFr()
    {
        return $this->piecesDossierAdminFr;
    }

    /**
     * Set piecesDossierAdminEn.
     *
     * @param string $piecesDossierAdminEn
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminEn($piecesDossierAdminEn)
    {
        $this->piecesDossierAdminEn = $piecesDossierAdminEn;

        return $this;
    }

    /**
     * Get piecesDossierAdminEn.
     *
     * @return string
     */
    public function getPiecesDossierAdminEn()
    {
        return $this->piecesDossierAdminEn;
    }

    /**
     * Set piecesDossierAdminEs.
     *
     * @param string $piecesDossierAdminEs
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminEs($piecesDossierAdminEs)
    {
        $this->piecesDossierAdminEs = $piecesDossierAdminEs;

        return $this;
    }

    /**
     * Get piecesDossierAdminEs.
     *
     * @return string
     */
    public function getPiecesDossierAdminEs()
    {
        return $this->piecesDossierAdminEs;
    }

    /**
     * Set piecesDossierAdminSu.
     *
     * @param string $piecesDossierAdminSu
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminSu($piecesDossierAdminSu)
    {
        $this->piecesDossierAdminSu = $piecesDossierAdminSu;

        return $this;
    }

    /**
     * Get piecesDossierAdminSu.
     *
     * @return string
     */
    public function getPiecesDossierAdminSu()
    {
        return $this->piecesDossierAdminSu;
    }

    /**
     * Set piecesDossierAdminDu.
     *
     * @param string $piecesDossierAdminDu
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminDu($piecesDossierAdminDu)
    {
        $this->piecesDossierAdminDu = $piecesDossierAdminDu;

        return $this;
    }

    /**
     * Get piecesDossierAdminDu.
     *
     * @return string
     */
    public function getPiecesDossierAdminDu()
    {
        return $this->piecesDossierAdminDu;
    }

    /**
     * Set piecesDossierAdminCz.
     *
     * @param string $piecesDossierAdminCz
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminCz($piecesDossierAdminCz)
    {
        $this->piecesDossierAdminCz = $piecesDossierAdminCz;

        return $this;
    }

    /**
     * Get piecesDossierAdminCz.
     *
     * @return string
     */
    public function getPiecesDossierAdminCz()
    {
        return $this->piecesDossierAdminCz;
    }

    /**
     * Set piecesDossierAdminAr.
     *
     * @param string $piecesDossierAdminAr
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminAr($piecesDossierAdminAr)
    {
        $this->piecesDossierAdminAr = $piecesDossierAdminAr;

        return $this;
    }

    /**
     * Get piecesDossierAdminAr.
     *
     * @return string
     */
    public function getPiecesDossierAdminAr()
    {
        return $this->piecesDossierAdminAr;
    }

    /**
     * Set piecesDossierTech.
     *
     * @param string $piecesDossierTech
     *
     * @return Consultation
     */
    public function setPiecesDossierTech($piecesDossierTech)
    {
        $this->piecesDossierTech = $piecesDossierTech;

        return $this;
    }

    /**
     * Get piecesDossierTech.
     *
     * @return string
     */
    public function getPiecesDossierTech()
    {
        return $this->piecesDossierTech;
    }

    /**
     * Set piecesDossierTechFr.
     *
     * @param string $piecesDossierTechFr
     *
     * @return Consultation
     */
    public function setPiecesDossierTechFr($piecesDossierTechFr)
    {
        $this->piecesDossierTechFr = $piecesDossierTechFr;

        return $this;
    }

    /**
     * Get piecesDossierTechFr.
     *
     * @return string
     */
    public function getPiecesDossierTechFr()
    {
        return $this->piecesDossierTechFr;
    }

    /**
     * Set piecesDossierTechEn.
     *
     * @param string $piecesDossierTechEn
     *
     * @return Consultation
     */
    public function setPiecesDossierTechEn($piecesDossierTechEn)
    {
        $this->piecesDossierTechEn = $piecesDossierTechEn;

        return $this;
    }

    /**
     * Get piecesDossierTechEn.
     *
     * @return string
     */
    public function getPiecesDossierTechEn()
    {
        return $this->piecesDossierTechEn;
    }

    /**
     * Set piecesDossierTechEs.
     *
     * @param string $piecesDossierTechEs
     *
     * @return Consultation
     */
    public function setPiecesDossierTechEs($piecesDossierTechEs)
    {
        $this->piecesDossierTechEs = $piecesDossierTechEs;

        return $this;
    }

    /**
     * Get piecesDossierTechEs.
     *
     * @return string
     */
    public function getPiecesDossierTechEs()
    {
        return $this->piecesDossierTechEs;
    }

    /**
     * Set piecesDossierTechSu.
     *
     * @param string $piecesDossierTechSu
     *
     * @return Consultation
     */
    public function setPiecesDossierTechSu($piecesDossierTechSu)
    {
        $this->piecesDossierTechSu = $piecesDossierTechSu;

        return $this;
    }

    /**
     * Get piecesDossierTechSu.
     *
     * @return string
     */
    public function getPiecesDossierTechSu()
    {
        return $this->piecesDossierTechSu;
    }

    /**
     * Set piecesDossierTechDu.
     *
     * @param string $piecesDossierTechDu
     *
     * @return Consultation
     */
    public function setPiecesDossierTechDu($piecesDossierTechDu)
    {
        $this->piecesDossierTechDu = $piecesDossierTechDu;

        return $this;
    }

    /**
     * Get piecesDossierTechDu.
     *
     * @return string
     */
    public function getPiecesDossierTechDu()
    {
        return $this->piecesDossierTechDu;
    }

    /**
     * Set piecesDossierTechCz.
     *
     * @param string $piecesDossierTechCz
     *
     * @return Consultation
     */
    public function setPiecesDossierTechCz($piecesDossierTechCz)
    {
        $this->piecesDossierTechCz = $piecesDossierTechCz;

        return $this;
    }

    /**
     * Get piecesDossierTechCz.
     *
     * @return string
     */
    public function getPiecesDossierTechCz()
    {
        return $this->piecesDossierTechCz;
    }

    /**
     * Set piecesDossierTechAr.
     *
     * @param string $piecesDossierTechAr
     *
     * @return Consultation
     */
    public function setPiecesDossierTechAr($piecesDossierTechAr)
    {
        $this->piecesDossierTechAr = $piecesDossierTechAr;

        return $this;
    }

    /**
     * Get piecesDossierTechAr.
     *
     * @return string
     */
    public function getPiecesDossierTechAr()
    {
        return $this->piecesDossierTechAr;
    }

    /**
     * Set piecesDossierAdditif.
     *
     * @param string $piecesDossierAdditif
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditif($piecesDossierAdditif)
    {
        $this->piecesDossierAdditif = $piecesDossierAdditif;

        return $this;
    }

    /**
     * Get piecesDossierAdditif.
     *
     * @return string
     */
    public function getPiecesDossierAdditif()
    {
        return $this->piecesDossierAdditif;
    }

    /**
     * Set piecesDossierAdditifFr.
     *
     * @param string $piecesDossierAdditifFr
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifFr($piecesDossierAdditifFr)
    {
        $this->piecesDossierAdditifFr = $piecesDossierAdditifFr;

        return $this;
    }

    /**
     * Get piecesDossierAdditifFr.
     *
     * @return string
     */
    public function getPiecesDossierAdditifFr()
    {
        return $this->piecesDossierAdditifFr;
    }

    /**
     * Set piecesDossierAdditifEn.
     *
     * @param string $piecesDossierAdditifEn
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifEn($piecesDossierAdditifEn)
    {
        $this->piecesDossierAdditifEn = $piecesDossierAdditifEn;

        return $this;
    }

    /**
     * Get piecesDossierAdditifEn.
     *
     * @return string
     */
    public function getPiecesDossierAdditifEn()
    {
        return $this->piecesDossierAdditifEn;
    }

    /**
     * Set piecesDossierAdditifEs.
     *
     * @param string $piecesDossierAdditifEs
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifEs($piecesDossierAdditifEs)
    {
        $this->piecesDossierAdditifEs = $piecesDossierAdditifEs;

        return $this;
    }

    /**
     * Get piecesDossierAdditifEs.
     *
     * @return string
     */
    public function getPiecesDossierAdditifEs()
    {
        return $this->piecesDossierAdditifEs;
    }

    /**
     * Set piecesDossierAdditifSu.
     *
     * @param string $piecesDossierAdditifSu
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifSu($piecesDossierAdditifSu)
    {
        $this->piecesDossierAdditifSu = $piecesDossierAdditifSu;

        return $this;
    }

    /**
     * Get piecesDossierAdditifSu.
     *
     * @return string
     */
    public function getPiecesDossierAdditifSu()
    {
        return $this->piecesDossierAdditifSu;
    }

    /**
     * Set piecesDossierAdditifDu.
     *
     * @param string $piecesDossierAdditifDu
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifDu($piecesDossierAdditifDu)
    {
        $this->piecesDossierAdditifDu = $piecesDossierAdditifDu;

        return $this;
    }

    /**
     * Get piecesDossierAdditifDu.
     *
     * @return string
     */
    public function getPiecesDossierAdditifDu()
    {
        return $this->piecesDossierAdditifDu;
    }

    /**
     * Set piecesDossierAdditifCz.
     *
     * @param string $piecesDossierAdditifCz
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifCz($piecesDossierAdditifCz)
    {
        $this->piecesDossierAdditifCz = $piecesDossierAdditifCz;

        return $this;
    }

    /**
     * Get piecesDossierAdditifCz.
     *
     * @return string
     */
    public function getPiecesDossierAdditifCz()
    {
        return $this->piecesDossierAdditifCz;
    }

    /**
     * Set piecesDossierAdditifAr.
     *
     * @param string $piecesDossierAdditifAr
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifAr($piecesDossierAdditifAr)
    {
        $this->piecesDossierAdditifAr = $piecesDossierAdditifAr;

        return $this;
    }

    /**
     * Get piecesDossierAdditifAr.
     *
     * @return string
     */
    public function getPiecesDossierAdditifAr()
    {
        return $this->piecesDossierAdditifAr;
    }

    /**
     * Set idRpa.
     *
     * @param int $idRpa
     *
     * @return Consultation
     */
    public function setIdRpa($idRpa)
    {
        $this->idRpa = $idRpa;

        return $this;
    }

    /**
     * Get idRpa.
     *
     * @return int
     */
    public function getIdRpa()
    {
        return $this->idRpa;
    }

    /**
     * Set detailConsultationFr.
     *
     * @param string $detailConsultationFr
     *
     * @return Consultation
     */
    public function setDetailConsultationFr($detailConsultationFr)
    {
        $this->detailConsultationFr = $detailConsultationFr;

        return $this;
    }

    /**
     * Get detailConsultationFr.
     *
     * @return string
     */
    public function getDetailConsultationFr()
    {
        return $this->detailConsultationFr;
    }

    /**
     * Set detailConsultationEn.
     *
     * @param string $detailConsultationEn
     *
     * @return Consultation
     */
    public function setDetailConsultationEn($detailConsultationEn)
    {
        $this->detailConsultationEn = $detailConsultationEn;

        return $this;
    }

    /**
     * Get detailConsultationEn.
     *
     * @return string
     */
    public function getDetailConsultationEn()
    {
        return $this->detailConsultationEn;
    }

    /**
     * Set detailConsultationEs.
     *
     * @param string $detailConsultationEs
     *
     * @return Consultation
     */
    public function setDetailConsultationEs($detailConsultationEs)
    {
        $this->detailConsultationEs = $detailConsultationEs;

        return $this;
    }

    /**
     * Get detailConsultationEs.
     *
     * @return string
     */
    public function getDetailConsultationEs()
    {
        return $this->detailConsultationEs;
    }

    /**
     * Set detailConsultationSu.
     *
     * @param string $detailConsultationSu
     *
     * @return Consultation
     */
    public function setDetailConsultationSu($detailConsultationSu)
    {
        $this->detailConsultationSu = $detailConsultationSu;

        return $this;
    }

    /**
     * Get detailConsultationSu.
     *
     * @return string
     */
    public function getDetailConsultationSu()
    {
        return $this->detailConsultationSu;
    }

    /**
     * Set detailConsultationDu.
     *
     * @param string $detailConsultationDu
     *
     * @return Consultation
     */
    public function setDetailConsultationDu($detailConsultationDu)
    {
        $this->detailConsultationDu = $detailConsultationDu;

        return $this;
    }

    /**
     * Get detailConsultationDu.
     *
     * @return string
     */
    public function getDetailConsultationDu()
    {
        return $this->detailConsultationDu;
    }

    /**
     * Set detailConsultationCz.
     *
     * @param string $detailConsultationCz
     *
     * @return Consultation
     */
    public function setDetailConsultationCz($detailConsultationCz)
    {
        $this->detailConsultationCz = $detailConsultationCz;

        return $this;
    }

    /**
     * Get detailConsultationCz.
     *
     * @return string
     */
    public function getDetailConsultationCz()
    {
        return $this->detailConsultationCz;
    }

    /**
     * Set detailConsultationAr.
     *
     * @param string $detailConsultationAr
     *
     * @return Consultation
     */
    public function setDetailConsultationAr($detailConsultationAr)
    {
        $this->detailConsultationAr = $detailConsultationAr;

        return $this;
    }

    /**
     * Get detailConsultationAr.
     *
     * @return string
     */
    public function getDetailConsultationAr()
    {
        return $this->detailConsultationAr;
    }

    /**
     * Set echantillon.
     *
     * @param string $echantillon
     *
     * @return Consultation
     */
    public function setEchantillon($echantillon)
    {
        $this->echantillon = $echantillon;

        return $this;
    }

    /**
     * Get echantillon.
     *
     * @return string
     */
    public function getEchantillon()
    {
        return $this->echantillon;
    }

    /**
     * Set reunion.
     *
     * @param string $reunion
     *
     * @return Consultation
     */
    public function setReunion($reunion)
    {
        $this->reunion = $reunion;

        return $this;
    }

    /**
     * Get reunion.
     *
     * @return string
     */
    public function getReunion()
    {
        return $this->reunion;
    }

    /**
     * Set visitesLieux.
     *
     * @param string $visitesLieux
     *
     * @return Consultation
     */
    public function setVisitesLieux($visitesLieux)
    {
        $this->visitesLieux = $visitesLieux;

        return $this;
    }

    /**
     * Get visitesLieux.
     *
     * @return string
     */
    public function getVisitesLieux()
    {
        return $this->visitesLieux;
    }

    /**
     * Set varianteCalcule.
     *
     * @param string $varianteCalcule
     *
     * @return Consultation
     */
    public function setVarianteCalcule($varianteCalcule)
    {
        $this->varianteCalcule = $varianteCalcule;

        return $this;
    }

    /**
     * Get varianteCalcule.
     *
     * @return string
     */
    public function getVarianteCalcule()
    {
        return $this->varianteCalcule;
    }

    /**
     * Set adresseRetraisDossiersFr.
     *
     * @param string $adresseRetraisDossiersFr
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersFr($adresseRetraisDossiersFr)
    {
        $this->adresseRetraisDossiersFr = $adresseRetraisDossiersFr;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersFr.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersFr()
    {
        return $this->adresseRetraisDossiersFr;
    }

    /**
     * Set adresseRetraisDossiersEn.
     *
     * @param string $adresseRetraisDossiersEn
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersEn($adresseRetraisDossiersEn)
    {
        $this->adresseRetraisDossiersEn = $adresseRetraisDossiersEn;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersEn.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersEn()
    {
        return $this->adresseRetraisDossiersEn;
    }

    /**
     * Set adresseRetraisDossiersEs.
     *
     * @param string $adresseRetraisDossiersEs
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersEs($adresseRetraisDossiersEs)
    {
        $this->adresseRetraisDossiersEs = $adresseRetraisDossiersEs;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersEs.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersEs()
    {
        return $this->adresseRetraisDossiersEs;
    }

    /**
     * Set adresseRetraisDossiersSu.
     *
     * @param string $adresseRetraisDossiersSu
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersSu($adresseRetraisDossiersSu)
    {
        $this->adresseRetraisDossiersSu = $adresseRetraisDossiersSu;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersSu.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersSu()
    {
        return $this->adresseRetraisDossiersSu;
    }

    /**
     * Set adresseRetraisDossiersDu.
     *
     * @param string $adresseRetraisDossiersDu
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersDu($adresseRetraisDossiersDu)
    {
        $this->adresseRetraisDossiersDu = $adresseRetraisDossiersDu;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersDu.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersDu()
    {
        return $this->adresseRetraisDossiersDu;
    }

    /**
     * Set adresseRetraisDossiersCz.
     *
     * @param string $adresseRetraisDossiersCz
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersCz($adresseRetraisDossiersCz)
    {
        $this->adresseRetraisDossiersCz = $adresseRetraisDossiersCz;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersCz.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersCz()
    {
        return $this->adresseRetraisDossiersCz;
    }

    /**
     * Set adresseDepotOffresFr.
     *
     * @param string $adresseDepotOffresFr
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresFr($adresseDepotOffresFr)
    {
        $this->adresseDepotOffresFr = $adresseDepotOffresFr;

        return $this;
    }

    /**
     * Get adresseDepotOffresFr.
     *
     * @return string
     */
    public function getAdresseDepotOffresFr()
    {
        return $this->adresseDepotOffresFr;
    }

    /**
     * Set adresseDepotOffresEn.
     *
     * @param string $adresseDepotOffresEn
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresEn($adresseDepotOffresEn)
    {
        $this->adresseDepotOffresEn = $adresseDepotOffresEn;

        return $this;
    }

    /**
     * Get adresseDepotOffresEn.
     *
     * @return string
     */
    public function getAdresseDepotOffresEn()
    {
        return $this->adresseDepotOffresEn;
    }

    /**
     * Set adresseDepotOffresEs.
     *
     * @param string $adresseDepotOffresEs
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresEs($adresseDepotOffresEs)
    {
        $this->adresseDepotOffresEs = $adresseDepotOffresEs;

        return $this;
    }

    /**
     * Get adresseDepotOffresEs.
     *
     * @return string
     */
    public function getAdresseDepotOffresEs()
    {
        return $this->adresseDepotOffresEs;
    }

    /**
     * Set adresseDepotOffresSu.
     *
     * @param string $adresseDepotOffresSu
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresSu($adresseDepotOffresSu)
    {
        $this->adresseDepotOffresSu = $adresseDepotOffresSu;

        return $this;
    }

    /**
     * Get adresseDepotOffresSu.
     *
     * @return string
     */
    public function getAdresseDepotOffresSu()
    {
        return $this->adresseDepotOffresSu;
    }

    /**
     * Set adresseDepotOffresDu.
     *
     * @param string $adresseDepotOffresDu
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresDu($adresseDepotOffresDu)
    {
        $this->adresseDepotOffresDu = $adresseDepotOffresDu;

        return $this;
    }

    /**
     * Get adresseDepotOffresDu.
     *
     * @return string
     */
    public function getAdresseDepotOffresDu()
    {
        return $this->adresseDepotOffresDu;
    }

    /**
     * Set adresseDepotOffresCz.
     *
     * @param string $adresseDepotOffresCz
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresCz($adresseDepotOffresCz)
    {
        $this->adresseDepotOffresCz = $adresseDepotOffresCz;

        return $this;
    }

    /**
     * Get adresseDepotOffresCz.
     *
     * @return string
     */
    public function getAdresseDepotOffresCz()
    {
        return $this->adresseDepotOffresCz;
    }

    /**
     * Set lieuOuverturePlisFr.
     *
     * @param string $lieuOuverturePlisFr
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisFr($lieuOuverturePlisFr)
    {
        $this->lieuOuverturePlisFr = $lieuOuverturePlisFr;

        return $this;
    }

    /**
     * Get lieuOuverturePlisFr.
     *
     * @return string
     */
    public function getLieuOuverturePlisFr()
    {
        return $this->lieuOuverturePlisFr;
    }

    /**
     * Set lieuOuverturePlisEn.
     *
     * @param string $lieuOuverturePlisEn
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisEn($lieuOuverturePlisEn)
    {
        $this->lieuOuverturePlisEn = $lieuOuverturePlisEn;

        return $this;
    }

    /**
     * Get lieuOuverturePlisEn.
     *
     * @return string
     */
    public function getLieuOuverturePlisEn()
    {
        return $this->lieuOuverturePlisEn;
    }

    /**
     * Set lieuOuverturePlisEs.
     *
     * @param string $lieuOuverturePlisEs
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisEs($lieuOuverturePlisEs)
    {
        $this->lieuOuverturePlisEs = $lieuOuverturePlisEs;

        return $this;
    }

    /**
     * Get lieuOuverturePlisEs.
     *
     * @return string
     */
    public function getLieuOuverturePlisEs()
    {
        return $this->lieuOuverturePlisEs;
    }

    /**
     * Set lieuOuverturePlisSu.
     *
     * @param string $lieuOuverturePlisSu
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisSu($lieuOuverturePlisSu)
    {
        $this->lieuOuverturePlisSu = $lieuOuverturePlisSu;

        return $this;
    }

    /**
     * Get lieuOuverturePlisSu.
     *
     * @return string
     */
    public function getLieuOuverturePlisSu()
    {
        return $this->lieuOuverturePlisSu;
    }

    /**
     * Set lieuOuverturePlisDu.
     *
     * @param string $lieuOuverturePlisDu
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisDu($lieuOuverturePlisDu)
    {
        $this->lieuOuverturePlisDu = $lieuOuverturePlisDu;

        return $this;
    }

    /**
     * Get lieuOuverturePlisDu.
     *
     * @return string
     */
    public function getLieuOuverturePlisDu()
    {
        return $this->lieuOuverturePlisDu;
    }

    /**
     * Set lieuOuverturePlisCz.
     *
     * @param string $lieuOuverturePlisCz
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisCz($lieuOuverturePlisCz)
    {
        $this->lieuOuverturePlisCz = $lieuOuverturePlisCz;

        return $this;
    }

    /**
     * Get lieuOuverturePlisCz.
     *
     * @return string
     */
    public function getLieuOuverturePlisCz()
    {
        return $this->lieuOuverturePlisCz;
    }

    /**
     * Set addEchantillionFr.
     *
     * @param string $addEchantillionFr
     *
     * @return Consultation
     */
    public function setAddEchantillionFr($addEchantillionFr)
    {
        $this->addEchantillionFr = $addEchantillionFr;

        return $this;
    }

    /**
     * Get addEchantillionFr.
     *
     * @return string
     */
    public function getAddEchantillionFr()
    {
        return $this->addEchantillionFr;
    }

    /**
     * Set addEchantillionEn.
     *
     * @param string $addEchantillionEn
     *
     * @return Consultation
     */
    public function setAddEchantillionEn($addEchantillionEn)
    {
        $this->addEchantillionEn = $addEchantillionEn;

        return $this;
    }

    /**
     * Get addEchantillionEn.
     *
     * @return string
     */
    public function getAddEchantillionEn()
    {
        return $this->addEchantillionEn;
    }

    /**
     * Set addEchantillionEs.
     *
     * @param string $addEchantillionEs
     *
     * @return Consultation
     */
    public function setAddEchantillionEs($addEchantillionEs)
    {
        $this->addEchantillionEs = $addEchantillionEs;

        return $this;
    }

    /**
     * Get addEchantillionEs.
     *
     * @return string
     */
    public function getAddEchantillionEs()
    {
        return $this->addEchantillionEs;
    }

    /**
     * Set addEchantillionSu.
     *
     * @param string $addEchantillionSu
     *
     * @return Consultation
     */
    public function setAddEchantillionSu($addEchantillionSu)
    {
        $this->addEchantillionSu = $addEchantillionSu;

        return $this;
    }

    /**
     * Get addEchantillionSu.
     *
     * @return string
     */
    public function getAddEchantillionSu()
    {
        return $this->addEchantillionSu;
    }

    /**
     * Set addEchantillionDu.
     *
     * @param string $addEchantillionDu
     *
     * @return Consultation
     */
    public function setAddEchantillionDu($addEchantillionDu)
    {
        $this->addEchantillionDu = $addEchantillionDu;

        return $this;
    }

    /**
     * Get addEchantillionDu.
     *
     * @return string
     */
    public function getAddEchantillionDu()
    {
        return $this->addEchantillionDu;
    }

    /**
     * Set addEchantillionCz.
     *
     * @param string $addEchantillionCz
     *
     * @return Consultation
     */
    public function setAddEchantillionCz($addEchantillionCz)
    {
        $this->addEchantillionCz = $addEchantillionCz;

        return $this;
    }

    /**
     * Get addEchantillionCz.
     *
     * @return string
     */
    public function getAddEchantillionCz()
    {
        return $this->addEchantillionCz;
    }

    /**
     * Set addEchantillionAr.
     *
     * @param string $addEchantillionAr
     *
     * @return Consultation
     */
    public function setAddEchantillionAr($addEchantillionAr)
    {
        $this->addEchantillionAr = $addEchantillionAr;

        return $this;
    }

    /**
     * Get addEchantillionAr.
     *
     * @return string
     */
    public function getAddEchantillionAr()
    {
        return $this->addEchantillionAr;
    }

    /**
     * Set addReunionFr.
     *
     * @param string $addReunionFr
     *
     * @return Consultation
     */
    public function setAddReunionFr($addReunionFr)
    {
        $this->addReunionFr = $addReunionFr;

        return $this;
    }

    /**
     * Get addReunionFr.
     *
     * @return string
     */
    public function getAddReunionFr()
    {
        return $this->addReunionFr;
    }

    /**
     * Set addReunionEn.
     *
     * @param string $addReunionEn
     *
     * @return Consultation
     */
    public function setAddReunionEn($addReunionEn)
    {
        $this->addReunionEn = $addReunionEn;

        return $this;
    }

    /**
     * Get addReunionEn.
     *
     * @return string
     */
    public function getAddReunionEn()
    {
        return $this->addReunionEn;
    }

    /**
     * Set addReunionEs.
     *
     * @param string $addReunionEs
     *
     * @return Consultation
     */
    public function setAddReunionEs($addReunionEs)
    {
        $this->addReunionEs = $addReunionEs;

        return $this;
    }

    /**
     * Get addReunionEs.
     *
     * @return string
     */
    public function getAddReunionEs()
    {
        return $this->addReunionEs;
    }

    /**
     * Set addReunionSu.
     *
     * @param string $addReunionSu
     *
     * @return Consultation
     */
    public function setAddReunionSu($addReunionSu)
    {
        $this->addReunionSu = $addReunionSu;

        return $this;
    }

    /**
     * Get addReunionSu.
     *
     * @return string
     */
    public function getAddReunionSu()
    {
        return $this->addReunionSu;
    }

    /**
     * Set addReunionDu.
     *
     * @param string $addReunionDu
     *
     * @return Consultation
     */
    public function setAddReunionDu($addReunionDu)
    {
        $this->addReunionDu = $addReunionDu;

        return $this;
    }

    /**
     * Get addReunionDu.
     *
     * @return string
     */
    public function getAddReunionDu()
    {
        return $this->addReunionDu;
    }

    /**
     * Set addReunionCz.
     *
     * @param string $addReunionCz
     *
     * @return Consultation
     */
    public function setAddReunionCz($addReunionCz)
    {
        $this->addReunionCz = $addReunionCz;

        return $this;
    }

    /**
     * Get addReunionCz.
     *
     * @return string
     */
    public function getAddReunionCz()
    {
        return $this->addReunionCz;
    }

    /**
     * Set addReunionAr.
     *
     * @param string $addReunionAr
     *
     * @return Consultation
     */
    public function setAddReunionAr($addReunionAr)
    {
        $this->addReunionAr = $addReunionAr;

        return $this;
    }

    /**
     * Get addReunionAr.
     *
     * @return string
     */
    public function getAddReunionAr()
    {
        return $this->addReunionAr;
    }

    /**
     * Set modePassation.
     *
     * @param string $modePassation
     *
     * @return Consultation
     */
    public function setModePassation($modePassation)
    {
        $this->modePassation = $modePassation;

        return $this;
    }

    /**
     * Get modePassation.
     *
     * @return string
     */
    public function getModePassation()
    {
        return $this->modePassation;
    }

    /**
     * Set consultationAnnulee.
     *
     * @param string $consultationAnnulee
     *
     * @return Consultation
     */
    public function setConsultationAnnulee($consultationAnnulee)
    {
        $this->consultationAnnulee = $consultationAnnulee;

        return $this;
    }

    /**
     * Get consultationAnnulee.
     *
     * @return string
     */
    public function getConsultationAnnulee()
    {
        return $this->consultationAnnulee;
    }

    /**
     * Set accessibiliteIt.
     *
     * @param string $accessibiliteIt
     *
     * @return Consultation
     */
    public function setAccessibiliteIt($accessibiliteIt)
    {
        $this->accessibiliteIt = $accessibiliteIt;

        return $this;
    }

    /**
     * Get accessibiliteIt.
     *
     * @return string
     */
    public function getAccessibiliteIt()
    {
        return $this->accessibiliteIt;
    }

    /**
     * Set adresseDepotOffresIt.
     *
     * @param string $adresseDepotOffresIt
     *
     * @return Consultation
     */
    public function setAdresseDepotOffresIt($adresseDepotOffresIt)
    {
        $this->adresseDepotOffresIt = $adresseDepotOffresIt;

        return $this;
    }

    /**
     * Get adresseDepotOffresIt.
     *
     * @return string
     */
    public function getAdresseDepotOffresIt()
    {
        return $this->adresseDepotOffresIt;
    }

    /**
     * Set lieuOuverturePlisIt.
     *
     * @param string $lieuOuverturePlisIt
     *
     * @return Consultation
     */
    public function setLieuOuverturePlisIt($lieuOuverturePlisIt)
    {
        $this->lieuOuverturePlisIt = $lieuOuverturePlisIt;

        return $this;
    }

    /**
     * Get lieuOuverturePlisIt.
     *
     * @return string
     */
    public function getLieuOuverturePlisIt()
    {
        return $this->lieuOuverturePlisIt;
    }

    /**
     * Set adresseRetraisDossiersIt.
     *
     * @param string $adresseRetraisDossiersIt
     *
     * @return Consultation
     */
    public function setAdresseRetraisDossiersIt($adresseRetraisDossiersIt)
    {
        $this->adresseRetraisDossiersIt = $adresseRetraisDossiersIt;

        return $this;
    }

    /**
     * Get adresseRetraisDossiersIt.
     *
     * @return string
     */
    public function getAdresseRetraisDossiersIt()
    {
        return $this->adresseRetraisDossiersIt;
    }

    /**
     * Set piecesDossierAdminIt.
     *
     * @param string $piecesDossierAdminIt
     *
     * @return Consultation
     */
    public function setPiecesDossierAdminIt($piecesDossierAdminIt)
    {
        $this->piecesDossierAdminIt = $piecesDossierAdminIt;

        return $this;
    }

    /**
     * Get piecesDossierAdminIt.
     *
     * @return string
     */
    public function getPiecesDossierAdminIt()
    {
        return $this->piecesDossierAdminIt;
    }

    /**
     * Set piecesDossierTechIt.
     *
     * @param string $piecesDossierTechIt
     *
     * @return Consultation
     */
    public function setPiecesDossierTechIt($piecesDossierTechIt)
    {
        $this->piecesDossierTechIt = $piecesDossierTechIt;

        return $this;
    }

    /**
     * Get piecesDossierTechIt.
     *
     * @return string
     */
    public function getPiecesDossierTechIt()
    {
        return $this->piecesDossierTechIt;
    }

    /**
     * Set piecesDossierAdditifIt.
     *
     * @param string $piecesDossierAdditifIt
     *
     * @return Consultation
     */
    public function setPiecesDossierAdditifIt($piecesDossierAdditifIt)
    {
        $this->piecesDossierAdditifIt = $piecesDossierAdditifIt;

        return $this;
    }

    /**
     * Get piecesDossierAdditifIt.
     *
     * @return string
     */
    public function getPiecesDossierAdditifIt()
    {
        return $this->piecesDossierAdditifIt;
    }

    /**
     * Set detailConsultationIt.
     *
     * @param string $detailConsultationIt
     *
     * @return Consultation
     */
    public function setDetailConsultationIt($detailConsultationIt)
    {
        $this->detailConsultationIt = $detailConsultationIt;

        return $this;
    }

    /**
     * Get detailConsultationIt.
     *
     * @return string
     */
    public function getDetailConsultationIt()
    {
        return $this->detailConsultationIt;
    }

    /**
     * Set addEchantillionIt.
     *
     * @param string $addEchantillionIt
     *
     * @return Consultation
     */
    public function setAddEchantillionIt($addEchantillionIt)
    {
        $this->addEchantillionIt = $addEchantillionIt;

        return $this;
    }

    /**
     * Get addEchantillionIt.
     *
     * @return string
     */
    public function getAddEchantillionIt()
    {
        return $this->addEchantillionIt;
    }

    /**
     * Set addReunionIt.
     *
     * @param string $addReunionIt
     *
     * @return Consultation
     */
    public function setAddReunionIt($addReunionIt)
    {
        $this->addReunionIt = $addReunionIt;

        return $this;
    }

    /**
     * Get addReunionIt.
     *
     * @return string
     */
    public function getAddReunionIt()
    {
        return $this->addReunionIt;
    }

    /**
     * Set codesNuts.
     *
     * @param string $codesNuts
     *
     * @return Consultation
     */
    public function setCodesNuts($codesNuts)
    {
        $this->codesNuts = $codesNuts;

        return $this;
    }

    /**
     * Get codesNuts.
     *
     * @return string
     */
    public function getCodesNuts()
    {
        return $this->codesNuts;
    }

    /**
     * Set intitule.
     *
     * @param string $intitule
     *
     * @return Consultation
     */
    public function setIntitule($intitule)
    {
        $this->intitule = $intitule;

        return $this;
    }

    /**
     * Get intitule.
     *
     * @return string
     */
    public function getIntitule()
    {
        return $this->intitule;
    }

    /**
     * Set idTrIntitule.
     *
     * @param int $idTrIntitule
     *
     * @return Consultation
     */
    public function setIdTrIntitule($idTrIntitule)
    {
        $this->idTrIntitule = $idTrIntitule;

        return $this;
    }

    /**
     * Get idTrIntitule.
     *
     * @return int
     */
    public function getIdTrIntitule()
    {
        return $this->idTrIntitule;
    }

    /**
     * Set objet.
     *
     * @param string $objet
     *
     * @return Consultation
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;

        return $this;
    }

    /**
     * Get objet.
     *
     * @return string
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Set idTrObjet.
     *
     * @param int $idTrObjet
     *
     * @return Consultation
     */
    public function setIdTrObjet($idTrObjet)
    {
        $this->idTrObjet = $idTrObjet;

        return $this;
    }

    /**
     * Get idTrObjet.
     *
     * @return int
     */
    public function getIdTrObjet()
    {
        return $this->idTrObjet;
    }

    /**
     * Set typeAcces.
     *
     * @param string $typeAcces
     *
     * @return Consultation
     */
    public function setTypeAcces($typeAcces)
    {
        $this->typeAcces = $typeAcces;

        return $this;
    }

    /**
     * Get typeAcces.
     *
     * @return string
     */
    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    /**
     * Set autoriserReponseElectronique.
     *
     * @param string $autoriserReponseElectronique
     *
     * @return Consultation
     */
    public function setAutoriserReponseElectronique($autoriserReponseElectronique)
    {
        $this->autoriserReponseElectronique = $autoriserReponseElectronique;

        return $this;
    }

    /**
     * Get autoriserReponseElectronique.
     *
     * @return string
     */
    public function getAutoriserReponseElectronique()
    {
        return $this->autoriserReponseElectronique;
    }

    /**
     * Set regleMiseEnLigne.
     *
     * @param int $regleMiseEnLigne
     *
     * @return Consultation
     */
    public function setRegleMiseEnLigne($regleMiseEnLigne)
    {
        $this->regleMiseEnLigne = $regleMiseEnLigne;

        return $this;
    }

    /**
     * Get regleMiseEnLigne.
     *
     * @return int
     */
    public function getRegleMiseEnLigne()
    {
        return $this->regleMiseEnLigne;
    }

    /**
     * Set idRegleValidation.
     *
     * @param int $idRegleValidation
     *
     * @return Consultation
     */
    public function setIdRegleValidation($idRegleValidation)
    {
        $this->idRegleValidation = $idRegleValidation;

        return $this;
    }

    /**
     * Get idRegleValidation.
     *
     * @return int
     */
    public function getIdRegleValidation()
    {
        return $this->idRegleValidation;
    }

    /**
     * Set intituleFr.
     *
     * @param string $intituleFr
     *
     * @return Consultation
     */
    public function setIntituleFr($intituleFr)
    {
        $this->intituleFr = $intituleFr;

        return $this;
    }

    /**
     * Get intituleFr.
     *
     * @return string
     */
    public function getIntituleFr()
    {
        return $this->intituleFr;
    }

    /**
     * Set intituleEn.
     *
     * @param string $intituleEn
     *
     * @return Consultation
     */
    public function setIntituleEn($intituleEn)
    {
        $this->intituleEn = $intituleEn;

        return $this;
    }

    /**
     * Get intituleEn.
     *
     * @return string
     */
    public function getIntituleEn()
    {
        return $this->intituleEn;
    }

    /**
     * Set intituleEs.
     *
     * @param string $intituleEs
     *
     * @return Consultation
     */
    public function setIntituleEs($intituleEs)
    {
        $this->intituleEs = $intituleEs;

        return $this;
    }

    /**
     * Get intituleEs.
     *
     * @return string
     */
    public function getIntituleEs()
    {
        return $this->intituleEs;
    }

    /**
     * Set intituleSu.
     *
     * @param string $intituleSu
     *
     * @return Consultation
     */
    public function setIntituleSu($intituleSu)
    {
        $this->intituleSu = $intituleSu;

        return $this;
    }

    /**
     * Get intituleSu.
     *
     * @return string
     */
    public function getIntituleSu()
    {
        return $this->intituleSu;
    }

    /**
     * Set intituleDu.
     *
     * @param string $intituleDu
     *
     * @return Consultation
     */
    public function setIntituleDu($intituleDu)
    {
        $this->intituleDu = $intituleDu;

        return $this;
    }

    /**
     * Get intituleDu.
     *
     * @return string
     */
    public function getIntituleDu()
    {
        return $this->intituleDu;
    }

    /**
     * Set intituleCz.
     *
     * @param string $intituleCz
     *
     * @return Consultation
     */
    public function setIntituleCz($intituleCz)
    {
        $this->intituleCz = $intituleCz;

        return $this;
    }

    /**
     * Get intituleCz.
     *
     * @return string
     */
    public function getIntituleCz()
    {
        return $this->intituleCz;
    }

    /**
     * Set intituleAr.
     *
     * @param string $intituleAr
     *
     * @return Consultation
     */
    public function setIntituleAr($intituleAr)
    {
        $this->intituleAr = $intituleAr;

        return $this;
    }

    /**
     * Get intituleAr.
     *
     * @return string
     */
    public function getIntituleAr()
    {
        return $this->intituleAr;
    }

    /**
     * Set intituleIt.
     *
     * @param string $intituleIt
     *
     * @return Consultation
     */
    public function setIntituleIt($intituleIt)
    {
        $this->intituleIt = $intituleIt;

        return $this;
    }

    /**
     * Get intituleIt.
     *
     * @return string
     */
    public function getIntituleIt()
    {
        return $this->intituleIt;
    }

    /**
     * Set objetFr.
     *
     * @param string $objetFr
     *
     * @return Consultation
     */
    public function setObjetFr($objetFr)
    {
        $this->objetFr = $objetFr;

        return $this;
    }

    /**
     * Get objetFr.
     *
     * @return string
     */
    public function getObjetFr()
    {
        return $this->objetFr;
    }

    /**
     * Set objetEn.
     *
     * @param string $objetEn
     *
     * @return Consultation
     */
    public function setObjetEn($objetEn)
    {
        $this->objetEn = $objetEn;

        return $this;
    }

    /**
     * Get objetEn.
     *
     * @return string
     */
    public function getObjetEn()
    {
        return $this->objetEn;
    }

    /**
     * Set objetEs.
     *
     * @param string $objetEs
     *
     * @return Consultation
     */
    public function setObjetEs($objetEs)
    {
        $this->objetEs = $objetEs;

        return $this;
    }

    /**
     * Get objetEs.
     *
     * @return string
     */
    public function getObjetEs()
    {
        return $this->objetEs;
    }

    /**
     * Set objetSu.
     *
     * @param string $objetSu
     *
     * @return Consultation
     */
    public function setObjetSu($objetSu)
    {
        $this->objetSu = $objetSu;

        return $this;
    }

    /**
     * Get objetSu.
     *
     * @return string
     */
    public function getObjetSu()
    {
        return $this->objetSu;
    }

    /**
     * Set objetDu.
     *
     * @param string $objetDu
     *
     * @return Consultation
     */
    public function setObjetDu($objetDu)
    {
        $this->objetDu = $objetDu;

        return $this;
    }

    /**
     * Get objetDu.
     *
     * @return string
     */
    public function getObjetDu()
    {
        return $this->objetDu;
    }

    /**
     * Set objetCz.
     *
     * @param string $objetCz
     *
     * @return Consultation
     */
    public function setObjetCz($objetCz)
    {
        $this->objetCz = $objetCz;

        return $this;
    }

    /**
     * Get objetCz.
     *
     * @return string
     */
    public function getObjetCz()
    {
        return $this->objetCz;
    }

    /**
     * Set objetAr.
     *
     * @param string $objetAr
     *
     * @return Consultation
     */
    public function setObjetAr($objetAr)
    {
        $this->objetAr = $objetAr;

        return $this;
    }

    /**
     * Get objetAr.
     *
     * @return string
     */
    public function getObjetAr()
    {
        return $this->objetAr;
    }

    /**
     * Set objetIt.
     *
     * @param string $objetIt
     *
     * @return Consultation
     */
    public function setObjetIt($objetIt)
    {
        $this->objetIt = $objetIt;

        return $this;
    }

    /**
     * Get objetIt.
     *
     * @return string
     */
    public function getObjetIt()
    {
        return $this->objetIt;
    }

    /**
     * Set dateDecision.
     *
     * @param string $dateDecision
     *
     * @return Consultation
     */
    public function setDateDecision($dateDecision)
    {
        $this->dateDecision = $dateDecision;

        return $this;
    }

    /**
     * Get dateDecision.
     *
     * @return string
     */
    public function getDateDecision()
    {
        return $this->dateDecision;
    }

    /**
     * Set clauseSociale.
     *
     * @param string $clauseSociale
     *
     * @return Consultation
     */
    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;

        return $this;
    }

    /**
     * Get clauseSociale.
     *
     * @return string
     */
    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    /**
     * Set clauseEnvironnementale.
     *
     * @param string $clauseEnvironnementale
     *
     * @return Consultation
     */
    public function setClauseEnvironnementale($clauseEnvironnementale)
    {
        $this->clauseEnvironnementale = $clauseEnvironnementale;

        return $this;
    }

    /**
     * Get clauseEnvironnementale.
     *
     * @return string
     */
    public function getClauseEnvironnementale()
    {
        return $this->clauseEnvironnementale;
    }

    /**
     * Set reponseObligatoire.
     *
     * @param string $reponseObligatoire
     *
     * @return Consultation
     */
    public function setReponseObligatoire($reponseObligatoire)
    {
        $this->reponseObligatoire = $reponseObligatoire;

        return $this;
    }

    /**
     * Get reponseObligatoire.
     *
     * @return string
     */
    public function getReponseObligatoire()
    {
        return $this->reponseObligatoire;
    }

    /**
     * Set compteBoampAssocie.
     *
     * @param int $compteBoampAssocie
     *
     * @return Consultation
     */
    public function setCompteBoampAssocie($compteBoampAssocie)
    {
        $this->compteBoampAssocie = $compteBoampAssocie;

        return $this;
    }

    /**
     * Get compteBoampAssocie.
     *
     * @return int
     */
    public function getCompteBoampAssocie()
    {
        return $this->compteBoampAssocie;
    }

    /**
     * Set autoriserPublicite.
     *
     * @param int $autoriserPublicite
     *
     * @return Consultation
     */
    public function setAutoriserPublicite($autoriserPublicite)
    {
        $this->autoriserPublicite = $autoriserPublicite;

        return $this;
    }

    /**
     * Get autoriserPublicite.
     *
     * @return int
     */
    public function getAutoriserPublicite()
    {
        return $this->autoriserPublicite;
    }

    /**
     * Set typeEnvoi.
     *
     * @param string $typeEnvoi
     *
     * @return Consultation
     */
    public function setTypeEnvoi($typeEnvoi)
    {
        $this->typeEnvoi = $typeEnvoi;

        return $this;
    }

    /**
     * Get typeEnvoi.
     *
     * @return string
     */
    public function getTypeEnvoi()
    {
        return $this->typeEnvoi;
    }

    /**
     * Set chiffrementOffre.
     *
     * @param string $chiffrementOffre
     *
     * @return Consultation
     */
    public function setChiffrementOffre($chiffrementOffre)
    {
        $this->chiffrementOffre = $chiffrementOffre;

        return $this;
    }

    /**
     * Get chiffrementOffre.
     *
     * @return string
     */
    public function getChiffrementOffre()
    {
        return $this->chiffrementOffre;
    }

    /**
     * Set envCandidature.
     *
     * @param int $envCandidature
     *
     * @return Consultation
     */
    public function setEnvCandidature($envCandidature)
    {
        $this->envCandidature = $envCandidature;

        return $this;
    }

    /**
     * Get envCandidature.
     *
     * @return int
     */
    public function getEnvCandidature()
    {
        return $this->envCandidature;
    }

    /**
     * Set envOffre.
     *
     * @param int $envOffre
     *
     * @return Consultation
     */
    public function setEnvOffre($envOffre)
    {
        $this->envOffre = $envOffre;

        return $this;
    }

    /**
     * Get envOffre.
     *
     * @return int
     */
    public function getEnvOffre()
    {
        return $this->envOffre;
    }

    /**
     * Set envAnonymat.
     *
     * @param int $envAnonymat
     *
     * @return Consultation
     */
    public function setEnvAnonymat($envAnonymat)
    {
        $this->envAnonymat = $envAnonymat;

        return $this;
    }

    /**
     * Get envAnonymat.
     *
     * @return int
     */
    public function getEnvAnonymat()
    {
        return $this->envAnonymat;
    }

    /**
     * Set idEtatConsultation.
     *
     * @param int $idEtatConsultation
     *
     * @return Consultation
     */
    public function setIdEtatConsultation($idEtatConsultation)
    {
        $this->idEtatConsultation = $idEtatConsultation;

        return $this;
    }

    /**
     * Get idEtatConsultation.
     *
     * @return int
     */
    public function getIdEtatConsultation()
    {
        return $this->idEtatConsultation;
    }

    /**
     * Set referenceConnecteur.
     *
     * @param string $referenceConnecteur
     *
     * @return Consultation
     */
    public function setReferenceConnecteur($referenceConnecteur)
    {
        $this->referenceConnecteur = $referenceConnecteur;

        return $this;
    }

    /**
     * Get referenceConnecteur.
     *
     * @return string
     */
    public function getReferenceConnecteur()
    {
        return $this->referenceConnecteur;
    }

    /**
     * Set consStatut.
     *
     * @param string $consStatut
     *
     * @return Consultation
     */
    public function setConsStatut($consStatut)
    {
        $this->consStatut = $consStatut;

        return $this;
    }

    /**
     * Get consStatut.
     *
     * @return string
     */
    public function getConsStatut()
    {
        return $this->consStatut;
    }

    /**
     * Set idApprobateur.
     *
     * @param int $idApprobateur
     *
     * @return Consultation
     */
    public function setIdApprobateur($idApprobateur)
    {
        $this->idApprobateur = $idApprobateur;

        return $this;
    }

    /**
     * Get idApprobateur.
     *
     * @return int
     */
    public function getIdApprobateur()
    {
        return $this->idApprobateur;
    }

    /**
     * Set idValideur.
     *
     * @param int $idValideur
     *
     * @return Consultation
     */
    public function setIdValideur($idValideur)
    {
        $this->idValideur = $idValideur;

        return $this;
    }

    /**
     * Get idValideur.
     *
     * @return int
     */
    public function getIdValideur()
    {
        return $this->idValideur;
    }

    public function setServiceValidation(?int $serviceValidation): self
    {
        $this->serviceValidation = $serviceValidation;

        return $this;
    }

    /**
     * Get serviceValidation.
     *
     * @return int
     */
    public function getServiceValidation()
    {
        return $this->serviceValidation;
    }

    /**
     * Set idCreateur.
     *
     * @param int $idCreateur
     *
     * @return Consultation
     */
    public function setIdCreateur($idCreateur)
    {
        $this->idCreateur = $idCreateur;

        return $this;
    }

    /**
     * Get idCreateur.
     *
     * @return int
     */
    public function getIdCreateur()
    {
        return $this->idCreateur;
    }

    /**
     * Set nomCreateur.
     *
     * @param string $nomCreateur
     *
     * @return Consultation
     */
    public function setNomCreateur($nomCreateur)
    {
        $this->nomCreateur = $nomCreateur;

        return $this;
    }

    /**
     * Get nomCreateur.
     *
     * @return string
     */
    public function getNomCreateur()
    {
        return $this->nomCreateur;
    }

    /**
     * Set prenomCreateur.
     *
     * @param string $prenomCreateur
     *
     * @return Consultation
     */
    public function setPrenomCreateur($prenomCreateur)
    {
        $this->prenomCreateur = $prenomCreateur;

        return $this;
    }

    /**
     * Get prenomCreateur.
     *
     * @return string
     */
    public function getPrenomCreateur()
    {
        return $this->prenomCreateur;
    }

    /**
     * Set signatureActeEngagement.
     *
     * @param string $signatureActeEngagement
     *
     * @return Consultation
     */
    public function setSignatureActeEngagement($signatureActeEngagement)
    {
        $this->signatureActeEngagement = $signatureActeEngagement;

        return $this;
    }

    /**
     * Get signatureActeEngagement.
     *
     * @return string
     */
    public function getSignatureActeEngagement()
    {
        return $this->signatureActeEngagement;
    }

    /**
     * Set archivemetadescription.
     *
     * @param string $archiveMetaDescription
     * @return Consultation
     */
    public function setArchiveMetaDescription($archiveMetaDescription)
    {
        $this->archiveMetaDescription = $archiveMetaDescription;

        return $this;
    }

    /**
     * Get archivemetadescription.
     *
     * @return string
     */
    public function getArchiveMetaDescription()
    {
        return $this->archiveMetaDescription;
    }

    /**
     * Set archivemetamotsclef.
     *
     * @param string $archiveMetaMotsClef
     * @return Consultation
     */
    public function setArchiveMetaMotsClef($archiveMetaMotsClef)
    {
        $this->archiveMetaMotsClef = $archiveMetaMotsClef;

        return $this;
    }

    /**
     * Get archivemetamotsclef.
     *
     * @return string
     */
    public function getArchiveMetaMotsClef()
    {
        return $this->archiveMetaMotsClef;
    }

    /**
     * Set archiveidblobzip.
     *
     * @param int $archiveIdBlobZip
     * @return Consultation
     */
    public function setArchiveIdBlobZip($archiveIdBlobZip)
    {
        $this->archiveIdBlobZip = $archiveIdBlobZip;

        return $this;
    }

    /**
     * Get archiveidblobzip.
     *
     * @return int
     */
    public function getArchiveIdBlobZip()
    {
        return $this->archiveIdBlobZip;
    }

    /**
     * Set decisionPartielle.
     *
     * @param string $decisionPartielle
     *
     * @return Consultation
     */
    public function setDecisionPartielle($decisionPartielle)
    {
        $this->decisionPartielle = $decisionPartielle;

        return $this;
    }

    /**
     * Get decisionPartielle.
     *
     * @return string
     */
    public function getDecisionPartielle()
    {
        return $this->decisionPartielle;
    }

    /**
     * Set typeDecisionARenseigner.
     *
     * @param string $typeDecisionARenseigner
     *
     * @return Consultation
     */
    public function setTypeDecisionARenseigner($typeDecisionARenseigner)
    {
        $this->typeDecisionARenseigner = $typeDecisionARenseigner;

        return $this;
    }

    /**
     * Get typeDecisionARenseigner.
     *
     * @return string
     */
    public function getTypeDecisionARenseigner()
    {
        return $this->typeDecisionARenseigner;
    }

    /**
     * Set typeDecisionAttributionMarche.
     *
     * @param string $typeDecisionAttributionMarche
     *
     * @return Consultation
     */
    public function setTypeDecisionAttributionMarche($typeDecisionAttributionMarche)
    {
        $this->typeDecisionAttributionMarche = $typeDecisionAttributionMarche;

        return $this;
    }

    /**
     * Get typeDecisionAttributionMarche.
     *
     * @return string
     */
    public function getTypeDecisionAttributionMarche()
    {
        return $this->typeDecisionAttributionMarche;
    }

    /**
     * Set typeDecisionDeclarationSansSuite.
     *
     * @param string $typeDecisionDeclarationSansSuite
     *
     * @return Consultation
     */
    public function setTypeDecisionDeclarationSansSuite($typeDecisionDeclarationSansSuite)
    {
        $this->typeDecisionDeclarationSansSuite = $typeDecisionDeclarationSansSuite;

        return $this;
    }

    /**
     * Get typeDecisionDeclarationSansSuite.
     *
     * @return string
     */
    public function getTypeDecisionDeclarationSansSuite()
    {
        return $this->typeDecisionDeclarationSansSuite;
    }

    /**
     * Set typeDecisionDeclarationInfructueux.
     *
     * @param string $typeDecisionDeclarationInfructueux
     *
     * @return Consultation
     */
    public function setTypeDecisionDeclarationInfructueux($typeDecisionDeclarationInfructueux)
    {
        $this->typeDecisionDeclarationInfructueux = $typeDecisionDeclarationInfructueux;

        return $this;
    }

    /**
     * Get typeDecisionDeclarationInfructueux.
     *
     * @return string
     */
    public function getTypeDecisionDeclarationInfructueux()
    {
        return $this->typeDecisionDeclarationInfructueux;
    }

    /**
     * Set typeDecisionSelectionEntreprise.
     *
     * @param string $typeDecisionSelectionEntreprise
     *
     * @return Consultation
     */
    public function setTypeDecisionSelectionEntreprise($typeDecisionSelectionEntreprise)
    {
        $this->typeDecisionSelectionEntreprise = $typeDecisionSelectionEntreprise;

        return $this;
    }

    /**
     * Get typeDecisionSelectionEntreprise.
     *
     * @return string
     */
    public function getTypeDecisionSelectionEntreprise()
    {
        return $this->typeDecisionSelectionEntreprise;
    }

    /**
     * Set typeDecisionAttributionAccordCadre.
     *
     * @param string $typeDecisionAttributionAccordCadre
     *
     * @return Consultation
     */
    public function setTypeDecisionAttributionAccordCadre($typeDecisionAttributionAccordCadre)
    {
        $this->typeDecisionAttributionAccordCadre = $typeDecisionAttributionAccordCadre;

        return $this;
    }

    /**
     * Get typeDecisionAttributionAccordCadre.
     *
     * @return string
     */
    public function getTypeDecisionAttributionAccordCadre()
    {
        return $this->typeDecisionAttributionAccordCadre;
    }

    /**
     * Set typeDecisionAdmissionSad.
     *
     * @param string $typeDecisionAdmissionSad
     *
     * @return Consultation
     */
    public function setTypeDecisionAdmissionSad($typeDecisionAdmissionSad)
    {
        $this->typeDecisionAdmissionSad = $typeDecisionAdmissionSad;

        return $this;
    }

    /**
     * Get typeDecisionAdmissionSad.
     *
     * @return string
     */
    public function getTypeDecisionAdmissionSad()
    {
        return $this->typeDecisionAdmissionSad;
    }

    /**
     * Set typeDecisionAutre.
     *
     * @param string $typeDecisionAutre
     *
     * @return Consultation
     */
    public function setTypeDecisionAutre($typeDecisionAutre)
    {
        $this->typeDecisionAutre = $typeDecisionAutre;

        return $this;
    }

    /**
     * Get typeDecisionAutre.
     *
     * @return string
     */
    public function getTypeDecisionAutre()
    {
        return $this->typeDecisionAutre;
    }

    /**
     * Set idArchiveur.
     *
     * @param int $idArchiveur
     *
     * @return Consultation
     */
    public function setIdArchiveur($idArchiveur)
    {
        $this->idArchiveur = $idArchiveur;

        return $this;
    }

    /**
     * Get idArchiveur.
     *
     * @return int
     */
    public function getIdArchiveur()
    {
        return $this->idArchiveur;
    }

    /**
     * Set prenomNomAgentTelechargementPlis.
     *
     * @param string $prenomNomAgentTelechargementPlis
     *
     * @return Consultation
     */
    public function setPrenomNomAgentTelechargementPlis($prenomNomAgentTelechargementPlis)
    {
        $this->prenomNomAgentTelechargementPlis = $prenomNomAgentTelechargementPlis;

        return $this;
    }

    /**
     * Get prenomNomAgentTelechargementPlis.
     *
     * @return string
     */
    public function getPrenomNomAgentTelechargementPlis()
    {
        return $this->prenomNomAgentTelechargementPlis;
    }

    /**
     * Set idAgentTelechargementPlis.
     *
     * @param int $idAgentTelechargementPlis
     *
     * @return Consultation
     */
    public function setIdAgentTelechargementPlis($idAgentTelechargementPlis)
    {
        $this->idAgentTelechargementPlis = $idAgentTelechargementPlis;

        return $this;
    }

    /**
     * Get idAgentTelechargementPlis.
     *
     * @return int
     */
    public function getIdAgentTelechargementPlis()
    {
        return $this->idAgentTelechargementPlis;
    }

    /**
     * Set pathTelechargementPlis.
     *
     * @param string $pathTelechargementPlis
     *
     * @return Consultation
     */
    public function setPathTelechargementPlis($pathTelechargementPlis)
    {
        $this->pathTelechargementPlis = $pathTelechargementPlis;

        return $this;
    }

    /**
     * Get pathTelechargementPlis.
     *
     * @return string
     */
    public function getPathTelechargementPlis()
    {
        return $this->pathTelechargementPlis;
    }

    /**
     * Set dateTelechargementPlis.
     *
     * @param string $dateTelechargementPlis
     *
     * @return Consultation
     */
    public function setDateTelechargementPlis($dateTelechargementPlis)
    {
        $this->dateTelechargementPlis = $dateTelechargementPlis;

        return $this;
    }

    /**
     * Get dateTelechargementPlis.
     *
     * @return string
     */
    public function getDateTelechargementPlis()
    {
        return $this->dateTelechargementPlis;
    }

    /**
     * Set serviceValidationIntermediaire.
     *
     * @param int $serviceValidationIntermediaire
     *
     * @return Consultation
     */
    public function setServiceValidationIntermediaire($serviceValidationIntermediaire)
    {
        $this->serviceValidationIntermediaire = $serviceValidationIntermediaire;

        return $this;
    }

    /**
     * Get serviceValidationIntermediaire.
     *
     * @return int
     */
    public function getServiceValidationIntermediaire()
    {
        return $this->serviceValidationIntermediaire;
    }

    /**
     * Set envOffreTechnique.
     *
     * @param int $envOffreTechnique
     *
     * @return Consultation
     */
    public function setEnvOffreTechnique($envOffreTechnique)
    {
        $this->envOffreTechnique = 0;

        return $this;
    }

    /**
     * Get envOffreTechnique.
     *
     * @return int
     */
    public function getEnvOffreTechnique()
    {
        return 0;
    }

    /**
     * Set refOrgPartenaire.
     *
     * @param string $refOrgPartenaire
     *
     * @return Consultation
     */
    public function setRefOrgPartenaire($refOrgPartenaire)
    {
        $this->refOrgPartenaire = $refOrgPartenaire;

        return $this;
    }

    /**
     * Get refOrgPartenaire.
     *
     * @return string
     */
    public function getRefOrgPartenaire()
    {
        return $this->refOrgPartenaire;
    }

    /**
     * Set dateArchivage.
     *
     * @param string $dateArchivage
     *
     * @return Consultation
     */
    public function setDateArchivage($dateArchivage)
    {
        $this->dateArchivage = $dateArchivage;

        return $this;
    }

    /**
     * Get dateArchivage.
     *
     * @return string
     */
    public function getDateArchivage()
    {
        return $this->dateArchivage;
    }

    /**
     * Set dateDecisionAnnulation.
     *
     * @param string $dateDecisionAnnulation
     *
     * @return Consultation
     */
    public function setDateDecisionAnnulation($dateDecisionAnnulation)
    {
        $this->dateDecisionAnnulation = $dateDecisionAnnulation;

        return $this;
    }

    /**
     * Get dateDecisionAnnulation.
     *
     * @return string
     */
    public function getDateDecisionAnnulation()
    {
        return $this->dateDecisionAnnulation;
    }

    /**
     * Set commentaireAnnulation.
     *
     * @param string $commentaireAnnulation
     *
     * @return Consultation
     */
    public function setCommentaireAnnulation($commentaireAnnulation)
    {
        $this->commentaireAnnulation = $commentaireAnnulation;

        return $this;
    }

    /**
     * Get commentaireAnnulation.
     *
     * @return string
     */
    public function getCommentaireAnnulation()
    {
        return $this->commentaireAnnulation;
    }

    /**
     * Set dateMiseEnLigneSouhaitee.
     *
     * @param string $dateMiseEnLigneSouhaitee
     *
     * @return Consultation
     */
    public function setDateMiseEnLigneSouhaitee($dateMiseEnLigneSouhaitee)
    {
        $this->dateMiseEnLigneSouhaitee = $dateMiseEnLigneSouhaitee;

        return $this;
    }

    /**
     * Get dateMiseEnLigneSouhaitee.
     *
     * @return string
     */
    public function getDateMiseEnLigneSouhaitee()
    {
        return $this->dateMiseEnLigneSouhaitee;
    }

    /**
     * Set etatEnAttenteValidation.
     *
     * @param string $etatEnAttenteValidation
     *
     * @return Consultation
     */
    public function setEtatEnAttenteValidation($etatEnAttenteValidation)
    {
        $this->etatEnAttenteValidation = $etatEnAttenteValidation;

        return $this;
    }

    /**
     * Get etatEnAttenteValidation.
     *
     * @return string
     */
    public function getEtatEnAttenteValidation()
    {
        return $this->etatEnAttenteValidation;
    }

    /**
     * Set dossierAdditif.
     *
     * @param string $dossierAdditif
     *
     * @return Consultation
     */
    public function setDossierAdditif($dossierAdditif)
    {
        $this->dossierAdditif = $dossierAdditif;

        return $this;
    }

    /**
     * Get dossierAdditif.
     *
     * @return string
     */
    public function getDossierAdditif()
    {
        return $this->dossierAdditif;
    }

    /**
     * @return ?TypeContrat
     */
    public function getTypeMarche()
    {
        return $this->typeMarche;
    }

    /**
     * @param ?TypeContrat $typeMarche
     */
    public function setTypeMarche(?TypeContrat $typeMarche): self
    {
        $this->typeMarche = $typeMarche;

        return $this;
    }

    /**
     * Set typePrestation.
     *
     * @param int $typePrestation
     *
     * @return Consultation
     */
    public function setTypePrestation($typePrestation)
    {
        $this->typePrestation = $typePrestation;

        return $this;
    }

    /**
     * Get typePrestation.
     *
     * @return int
     */
    public function getTypePrestation()
    {
        return $this->typePrestation;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     *
     * @return Consultation
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set delaiPartiel.
     *
     * @param string $delaiPartiel
     *
     * @return Consultation
     */
    public function setDelaiPartiel($delaiPartiel)
    {
        $this->delaiPartiel = $delaiPartiel;

        return $this;
    }

    /**
     * Get delaiPartiel.
     *
     * @return string
     */
    public function getDelaiPartiel()
    {
        return $this->delaiPartiel;
    }

    /**
     * Set datefinlocale.
     *
     * @param string $dateFinLocale
     * @return Consultation
     */
    public function setDateFinLocale($dateFinLocale)
    {
        $this->dateFinLocale = $dateFinLocale;

        return $this;
    }

    /**
     * Get datefinlocale.
     *
     * @return string
     */
    public function getDateFinLocale()
    {
        return $this->dateFinLocale;
    }

    /**
     * Set lieuresidence.
     *
     * @param string $lieuResidence
     * @return Consultation
     */
    public function setLieuResidence($lieuResidence)
    {
        $this->lieuResidence = $lieuResidence;

        return $this;
    }

    /**
     * Get lieuresidence.
     *
     * @return string
     */
    public function getLieuResidence()
    {
        return $this->lieuResidence;
    }

    /**
     * Set alerte.
     *
     * @param string $alerte
     *
     * @return Consultation
     */
    public function setAlerte($alerte)
    {
        $this->alerte = $alerte;

        return $this;
    }

    /**
     * Get alerte.
     *
     * @return string
     */
    public function getAlerte()
    {
        return $this->alerte;
    }

    /**
     * Set doublon.
     *
     * @param string $doublon
     *
     * @return Consultation
     */
    public function setDoublon($doublon)
    {
        $this->doublon = $doublon;

        return $this;
    }

    /**
     * Get doublon.
     *
     * @return string
     */
    public function getDoublon()
    {
        return $this->doublon;
    }

    /**
     * Set denominationAdapte.
     *
     * @param string $denominationAdapte
     *
     * @return Consultation
     */
    public function setDenominationAdapte($denominationAdapte)
    {
        $this->denominationAdapte = $denominationAdapte;

        return $this;
    }

    /**
     * Get denominationAdapte.
     *
     * @return string
     */
    public function getDenominationAdapte()
    {
        return $this->denominationAdapte;
    }

    /**
     * Set urlConsultationAvisPub.
     *
     * @param string $urlConsultationAvisPub
     *
     * @return Consultation
     */
    public function setUrlConsultationAvisPub($urlConsultationAvisPub)
    {
        $this->urlConsultationAvisPub = $urlConsultationAvisPub;

        return $this;
    }

    /**
     * Get urlConsultationAvisPub.
     *
     * @return string
     */
    public function getUrlConsultationAvisPub()
    {
        return $this->urlConsultationAvisPub;
    }

    /**
     * Set doublonDe.
     *
     * @param string $doublonDe
     *
     * @return Consultation
     */
    public function setDoublonDe($doublonDe)
    {
        $this->doublonDe = $doublonDe;

        return $this;
    }

    /**
     * Get doublonDe.
     *
     * @return string
     */
    public function getDoublonDe()
    {
        return $this->doublonDe;
    }

    /**
     * Set entiteAdjudicatrice.
     *
     * @param string $entiteAdjudicatrice
     *
     * @return Consultation
     */
    public function setEntiteAdjudicatrice($entiteAdjudicatrice)
    {
        $this->entiteAdjudicatrice = $entiteAdjudicatrice;

        return $this;
    }

    /**
     * Get entiteAdjudicatrice.
     *
     * @return string
     */
    public function getEntiteAdjudicatrice()
    {
        return $this->entiteAdjudicatrice;
    }

    /**
     * Set codeOperation.
     *
     * @param string $codeOperation
     *
     * @return Consultation
     */
    public function setCodeOperation($codeOperation)
    {
        $this->codeOperation = $codeOperation;

        return $this;
    }

    /**
     * Get codeOperation.
     *
     * @return string
     */
    public function getCodeOperation()
    {
        return $this->codeOperation;
    }

    /**
     * Set clauseSocialeConditionExecution.
     *
     * @param string $clauseSocialeConditionExecution
     *
     * @return Consultation
     */
    public function setClauseSocialeConditionExecution($clauseSocialeConditionExecution)
    {
        $this->clauseSocialeConditionExecution = $clauseSocialeConditionExecution;

        return $this;
    }

    /**
     * Get clauseSocialeConditionExecution.
     *
     * @return string
     */
    public function getClauseSocialeConditionExecution()
    {
        return $this->clauseSocialeConditionExecution;
    }

    /**
     * Set clauseSocialeInsertion.
     *
     * @param string $clauseSocialeInsertion
     *
     * @return Consultation
     */
    public function setClauseSocialeInsertion($clauseSocialeInsertion)
    {
        $this->clauseSocialeInsertion = $clauseSocialeInsertion;

        return $this;
    }

    /**
     * Get clauseSocialeInsertion.
     *
     * @return string
     */
    public function getClauseSocialeInsertion()
    {
        return $this->clauseSocialeInsertion;
    }

    /**
     * Set clauseSocialeAteliersProteges.
     *
     * @param string $clauseSocialeAteliersProteges
     *
     * @return Consultation
     */
    public function setClauseSocialeAteliersProteges($clauseSocialeAteliersProteges)
    {
        $this->clauseSocialeAteliersProteges = $clauseSocialeAteliersProteges;

        return $this;
    }

    /**
     * Get clauseSocialeAteliersProteges.
     *
     * @return string
     */
    public function getClauseSocialeAteliersProteges()
    {
        return $this->clauseSocialeAteliersProteges;
    }

    /**
     * Set clauseSocialeSiae.
     *
     * @param string $clauseSocialeSiae
     *
     * @return Consultation
     */
    public function setClauseSocialeSiae($clauseSocialeSiae)
    {
        $this->clauseSocialeSiae = $clauseSocialeSiae;

        return $this;
    }

    /**
     * Get clauseSocialeSiae.
     *
     * @return string
     */
    public function getClauseSocialeSiae()
    {
        return $this->clauseSocialeSiae;
    }

    /**
     * Set clauseSocialeEss.
     *
     * @param string $clauseSocialeEss
     *
     * @return Consultation
     */
    public function setClauseSocialeEss($clauseSocialeEss)
    {
        $this->clauseSocialeEss = $clauseSocialeEss;

        return $this;
    }

    /**
     * Get clauseSocialeEss.
     *
     * @return string
     */
    public function getClauseSocialeEss()
    {
        return $this->clauseSocialeEss;
    }

    /**
     * Set clauseEnvSpecsTechniques.
     *
     * @param string $clauseEnvSpecsTechniques
     *
     * @return Consultation
     */
    public function setClauseEnvSpecsTechniques($clauseEnvSpecsTechniques)
    {
        $this->clauseEnvSpecsTechniques = $clauseEnvSpecsTechniques;

        return $this;
    }

    /**
     * Get clauseEnvSpecsTechniques.
     *
     * @return string
     */
    public function getClauseEnvSpecsTechniques()
    {
        return $this->clauseEnvSpecsTechniques;
    }

    /**
     * Set clauseEnvCondExecution.
     *
     * @param string $clauseEnvCondExecution
     *
     * @return Consultation
     */
    public function setClauseEnvCondExecution($clauseEnvCondExecution)
    {
        $this->clauseEnvCondExecution = $clauseEnvCondExecution;

        return $this;
    }

    /**
     * Get clauseEnvCondExecution.
     *
     * @return string
     */
    public function getClauseEnvCondExecution()
    {
        return $this->clauseEnvCondExecution;
    }

    /**
     * Set clauseEnvCriteresSelect.
     *
     * @param string $clauseEnvCriteresSelect
     *
     * @return Consultation
     */
    public function setClauseEnvCriteresSelect($clauseEnvCriteresSelect)
    {
        $this->clauseEnvCriteresSelect = $clauseEnvCriteresSelect;

        return $this;
    }

    /**
     * Get clauseEnvCriteresSelect.
     *
     * @return string
     */
    public function getClauseEnvCriteresSelect()
    {
        return $this->clauseEnvCriteresSelect;
    }

    /**
     * Set idDonneeComplementaire.
     *
     * @param int $idDonneeComplementaire
     *
     * @return Consultation
     */
    public function setIdDonneeComplementaire($idDonneeComplementaire)
    {
        $this->idDonneeComplementaire = $idDonneeComplementaire;

        return $this;
    }

    /**
     * Get idDonneeComplementaire.
     *
     * @return int
     */
    public function getIdDonneeComplementaire()
    {
        return $this->idDonneeComplementaire;
    }

    /**
     * Set donneeComplementaireObligatoire.
     *
     * @param string $donneeComplementaireObligatoire
     *
     * @return Consultation
     */
    public function setDonneeComplementaireObligatoire($donneeComplementaireObligatoire)
    {
        $this->donneeComplementaireObligatoire = $donneeComplementaireObligatoire;

        return $this;
    }

    /**
     * Get donneeComplementaireObligatoire.
     *
     * @return string
     */
    public function getDonneeComplementaireObligatoire()
    {
        return $this->donneeComplementaireObligatoire;
    }

    /**
     * Set modeOuvertureReponse.
     *
     * @param string $modeOuvertureReponse
     *
     * @return Consultation
     */
    public function setModeOuvertureReponse($modeOuvertureReponse)
    {
        $this->modeOuvertureReponse = $modeOuvertureReponse;

        return $this;
    }

    /**
     * Get modeOuvertureReponse.
     *
     * @return string
     */
    public function getModeOuvertureReponse()
    {
        return $this->modeOuvertureReponse;
    }

    /**
     * Set idFichierAnnulation.
     *
     * @param int $idFichierAnnulation
     *
     * @return Consultation
     */
    public function setIdFichierAnnulation($idFichierAnnulation)
    {
        $this->idFichierAnnulation = $idFichierAnnulation;

        return $this;
    }

    /**
     * Get idFichierAnnulation.
     *
     * @return int
     */
    public function getIdFichierAnnulation()
    {
        return $this->idFichierAnnulation;
    }

    /**
     * Set idoperation.
     *
     * @param int $idOperation
     * @return Consultation
     */
    public function setIdOperation($idOperation)
    {
        $this->idOperation = $idOperation;

        return $this;
    }

    /**
     * Get idoperation.
     *
     * @return int
     */
    public function getIdOperation()
    {
        return $this->idOperation;
    }

    /**
     * Set marchePublicSimplifie.
     *
     * @param string $marchePublicSimplifie
     *
     * @return Consultation
     */
    public function setMarchePublicSimplifie($marchePublicSimplifie)
    {
        $this->marchePublicSimplifie = $marchePublicSimplifie;

        return $this;
    }

    /**
     * Get marchePublicSimplifie.
     *
     * @return string
     */
    public function getMarchePublicSimplifie()
    {
        return $this->marchePublicSimplifie;
    }

    /**
     * Set infosBlocsAtlas.
     *
     * @param string $infosBlocsAtlas
     *
     * @return Consultation
     */
    public function setInfosBlocsAtlas($infosBlocsAtlas)
    {
        $this->infosBlocsAtlas = $infosBlocsAtlas;

        return $this;
    }

    /**
     * Get infosBlocsAtlas.
     *
     * @return string
     */
    public function getInfosBlocsAtlas()
    {
        return $this->infosBlocsAtlas;
    }

    /**
     * Set dateFinUnix.
     *
     * @param string $dateFinUnix
     *
     * @return Consultation
     */
    public function setDateFinUnix($dateFinUnix)
    {
        $this->dateFinUnix = $dateFinUnix;

        return $this;
    }

    /**
     * Get dateFinUnix.
     *
     * @return string
     */
    public function getDateFinUnix()
    {
        return $this->dateFinUnix;
    }

    /**
     * Set numeroAc.
     *
     * @param string $numeroAc
     *
     * @return Consultation
     */
    public function setNumeroAc($numeroAc)
    {
        $this->numeroAc = $numeroAc;

        return $this;
    }

    /**
     * Get numeroAc.
     *
     * @return string
     */
    public function getNumeroAc()
    {
        return $this->numeroAc;
    }

    /**
     * Set idContrat.
     *
     * @param int $idContrat
     *
     * @return Consultation
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;

        return $this;
    }

    /**
     * Get idContrat.
     *
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * Set pinApiSgmapMps.
     *
     * @param string $pinApiSgmapMps
     *
     * @return Consultation
     */
    public function setPinApiSgmapMps($pinApiSgmapMps)
    {
        $this->pinApiSgmapMps = $pinApiSgmapMps;

        return $this;
    }

    /**
     * Get pinApiSgmapMps.
     *
     * @return string
     */
    public function getPinApiSgmapMps()
    {
        return $this->pinApiSgmapMps;
    }

    /**
     * @return $this
     */
    public function addOffre(Offre $offre)
    {
        $this->offres[] = $offre;
        $offre->setConsultation($this);

        return $this;
    }

    public function removeOffre(Offre $offre)
    {
        $this->offres->removeElement($offre);
    }

    /**
     * @return Collection
     */
    public function getOffres()
    {
        return $this->offres;
    }

    public function setOrganisme(Organisme $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return Organisme
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @return $this
     */
    public function addLot(Lot $lot)
    {
        $this->lots[] = $lot;
        $lot->setConsultation($this);

        return $this;
    }

    public function removeLot(Lot $lot)
    {
        $this->lots->removeElement($lot);
    }

    /**
     * @return Collection
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * @return $this
     */
    public function addOffrePapier(OffrePapier $offrePapier)
    {
        $this->offresPapiers[] = $offrePapier;
        $offrePapier->setConsultation($this);

        return $this;
    }

    public function removeOffrePapier(OffrePapier $offrePapier)
    {
        $this->offresPapiers->removeElement($offrePapier);
    }

    /**
     * @return Collection
     */
    public function getOffresPapiers()
    {
        return $this->offresPapiers;
    }

    public function setService(?Service $service): ?self
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return Service
     */
    public function getService(): ?Service
    {
        return $this->service;
    }

    /**
     * @return TypeProcedure
     */
    public function getProcedureType()
    {
        return $this->typeProcedure;
    }

    /**
     * @param ProcedureType $procedureType
     * @return Consultation
     */
    public function setProcedureType(TypeProcedure $typeProcedure)
    {
        $this->typeProcedure = $typeProcedure;

        return $this;
    }

    public function setTypeProcedureOrganisme(TypeProcedureOrganisme $typeProcedureOrganisme)
    {
        $this->typeProcedureOrganisme = $typeProcedureOrganisme;
    }

    /**
     * @return ?TypeProcedureOrganisme
     */
    public function getTypeProcedureOrganisme(): ?TypeProcedureOrganisme
    {
        return $this->typeProcedureOrganisme;
    }

    public function setAvisType(AvisType $avisType)
    {
        $this->avisType = $avisType;
    }

    /**
     * @return AvisType
     */
    public function getAvisType()
    {
        return $this->avisType;
    }

    /**
     * setConsultationType renamed to setCategorieConsultation
     */
    public function setCategorieConsultation(CategorieConsultation $categorieConsultation): Consultation
    {
        $this->categorieConsultation = $categorieConsultation;

        return $this;
    }

    /**
     * getConsultationType renamed to getCategorieConsultation
     */
    public function getCategorieConsultation(): ?CategorieConsultation
    {
        return $this->categorieConsultation;
    }

    /**
     * @return string
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     */
    public function getDescription()
    {
        return $this->getTitre();
    }

    public function addCertificatChiffrement(CertificatChiffrement $certificatChiffrement): Consultation
    {
        $this->certificatChiffrements[] = $certificatChiffrement;
        $certificatChiffrement->setConsultation($this);

        return $this;
    }

    public function removeCertificatChiffrement(CertificatChiffrement $certificatChiffrement)
    {
        $this->certificatChiffrements->removeElement($certificatChiffrement);
    }

    /**
     * @return Collection
     */
    public function getCertificatChiffrements()
    {
        return $this->certificatChiffrements;
    }

    /**
     * @return mixed
     */
    public function getCertificatChiffrementsNonDouble()
    {
        return $this->certificatChiffrementsNonDouble;
    }

    /**
     * @param mixed $certificatChiffrementsNonDouble
     */
    public function setCertificatChiffrementsNonDouble($certificatChiffrementsNonDouble)
    {
        $this->certificatChiffrementsNonDouble = $certificatChiffrementsNonDouble;
    }

    /**
     * @return Collection
     */
    public function getTelechargementAnonymes()
    {
        return $this->telechargementAnonymes;
    }

    /**
     * @param TelechargementAnonyme $telechargementAnonymes
     *
     * @return $this
     */
    public function addTelechargementAnonyme(TelechargementAnonyme $telechargementAnonyme)
    {
        $this->telechargementAnonymes[] = $telechargementAnonyme;
        $telechargementAnonyme->setConsultation($this);

        return $this;
    }

    /**
     * @param TelechargementAnonyme $telechargementAnonymes
     */
    public function removeTelechargementAnonyme(TelechargementAnonyme $telechargementAnonyme)
    {
        $this->telechargementAnonymes->removeElement($telechargementAnonyme);
    }

    /**
     * @return Collection
     */
    public function getTelechargements()
    {
        return $this->telechargements;
    }

    /**
     * @param Telechargement $telechargements
     *
     * @return $this
     */
    public function addTelechargement(Telechargement $telechargement)
    {
        $this->telechargements[] = $telechargement;
        $telechargement->setConsultation($this);

        return $this;
    }

    /**
     * @param Telechargement $telechargements
     */
    public function removeTelechargement(Telechargement $telechargement)
    {
        $this->telechargements->removeElement($telechargement);
    }

    /**
     * @return $this
     */
    public function addBourse(TBourseCotraitance $bourse)
    {
        $this->bourses[] = $bourse;
        $bourse->setConsultation($this);

        return $this;
    }

    /**
     * @return $this
     */
    public function addDumeContextes(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes[] = $dumeContexte;
        $dumeContexte->setConsultation($this);

        return $this;
    }

    public function removeBourse(TBourseCotraitance $bourse)
    {
        $this->bourses->removeElement($bourse);
    }

    public function removeDumeContextes(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes->removeElement($dumeContexte);
    }

    /**
     * @return Collection
     */
    public function getBourses()
    {
        return $this->bourses;
    }

    /**
     * @return Collection
     */
    public function getDumeContextes()
    {
        return $this->dumeContextes;
    }

    /**
     * @return $this
     */
    public function addCandidatures(TCandidature $candidature)
    {
        $this->candidatures[] = $candidature;
        $candidature->setConsultation($this);

        return $this;
    }

    public function removeCandidatures(TCandidature $candidature)
    {
        $this->candidatures->removeElement($candidature);
    }

    /**
     * @return Collection
     */
    public function getCandidatures()
    {
        return $this->candidatures;
    }

    /**
     * @return $this
     */
    public function addListLots(TListeLotsCandidature $listLot)
    {
        $this->listLots[] = $listLot;
        $listLot->setConsultation($this);

        return $this;
    }

    public function removeListLots(TListeLotsCandidature $listLot)
    {
        $this->listLots->removeElement($listLot);
    }

    /**
     * @return Collection
     */
    public function getListLots()
    {
        return $this->listLots;
    }

    /**
     * @return string
     */
    public function getDumeDemande()
    {
        return $this->dumeDemande;
    }

    /**
     * @param string $dumeDemande
     */
    public function setDumeDemande($dumeDemande)
    {
        $this->dumeDemande = $dumeDemande;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeProcedureDume()
    {
        return $this->typeProcedureDume;
    }

    /**
     * @param int $typeProcedureDume
     */
    public function setTypeProcedureDume($typeProcedureDume)
    {
        $this->typeProcedureDume = $typeProcedureDume;

        return $this;
    }

    /**
     * Set marcheInsertion.
     *
     * @param bool $marcheInsertion
     *
     * @return Consultation
     */
    public function setMarcheInsertion($marcheInsertion)
    {
        $this->marcheInsertion = $marcheInsertion;

        return $this;
    }

    /**
     * Get marcheInsertion.
     *
     * @return bool
     */
    public function getMarcheInsertion()
    {
        return $this->marcheInsertion;
    }

    /**
     * Set clauseSpecificationTechnique.
     *
     * @param string $clauseSpecificationTechnique
     *
     * @return Consultation
     */
    public function setClauseSpecificationTechnique($clauseSpecificationTechnique)
    {
        $this->clauseSpecificationTechnique = $clauseSpecificationTechnique;

        return $this;
    }

    /**
     * Get clauseSpecificationTechnique.
     *
     * @return string
     */
    public function getClauseSpecificationTechnique()
    {
        return $this->clauseSpecificationTechnique;
    }

    /**
     * Add offresPapier.
     *
     *
     * @return Consultation
     */
    public function addOffresPapier(OffrePapier $offresPapier)
    {
        $this->offresPapiers[] = $offresPapier;

        return $this;
    }

    /**
     * Remove offresPapier.
     */
    public function removeOffresPapier(OffrePapier $offresPapier)
    {
        $this->offresPapiers->removeElement($offresPapier);
    }

    /**
     * Add dumeContexte.
     *
     *
     * @return Consultation
     */
    public function addDumeContexte(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes[] = $dumeContexte;

        return $this;
    }

    /**
     * Remove dumeContexte.
     */
    public function removeDumeContexte(TDumeContexte $dumeContexte)
    {
        $this->dumeContextes->removeElement($dumeContexte);
    }

    /**
     * Add candidature.
     *
     *
     * @return Consultation
     */
    public function addCandidature(TCandidature $candidature)
    {
        $this->candidatures[] = $candidature;

        return $this;
    }

    /**
     * Remove candidature.
     */
    public function removeCandidature(TCandidature $candidature)
    {
        $this->candidatures->removeElement($candidature);
    }

    /**
     * Add listLot.
     *
     *
     * @return Consultation
     */
    public function addListLot(TListeLotsCandidature $listLot)
    {
        $this->listLots[] = $listLot;

        return $this;
    }

    /**
     * Remove listLot.
     */
    public function removeListLot(TListeLotsCandidature $listLot)
    {
        $this->listLots->removeElement($listLot);
    }

    /**
     * @return string
     */
    public function getSourceExterne()
    {
        return $this->sourceExterne;
    }

    /**
     * @param string $sourceExterne
     */
    public function setSourceExterne($sourceExterne)
    {
        $this->sourceExterne = $sourceExterne;
    }

    /**
     * @return string
     */
    public function getIdSourceExterne()
    {
        return $this->idSourceExterne;
    }

    /**
     * @param string $idSsourceExterne
     */
    public function setIdSourceExterne($idSourceExterne)
    {
        $this->idSourceExterne = $idSourceExterne;
    }

    /**
     * @return string
     */
    public function getAttestationConsultation()
    {
        return $this->attestationConsultation;
    }

    /**
     * @param string $attestationConsultation
     */
    public function setAttestationConsultation($attestationConsultation)
    {
        $this->attestationConsultation = $attestationConsultation;
    }

    /**
     * @return Collection
     */
    public function getConsultationFavoris()
    {
        return $this->consultationFavoris;
    }

    /**
     * @return DossierVolumineux
     */
    public function getDossierVolumineux()
    {
        return $this->dossierVolumineux;
    }

    public function setDossierVolumineux(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineux = $dossierVolumineux;
    }

    public function addDossierVolumineuxDescripteur(ConsultationFavoris $consultationFavoris)
    {
        $this->consultationFavoris->add($consultationFavoris);
        $consultationFavoris->setConsultation($this);
    }

    /**
     * @return Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @return $this
     */
    public function addQuestion(QuestionDCE $question)
    {
        $this->questions[] = $question;
        $question->setOrganisme($this);

        return $this;
    }

    public function removeQuestion(QuestionDCE $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * @return string
     */
    public function getCodeExterne()
    {
        return $this->codeExterne;
    }

    /**
     * @return Consultation
     */
    public function setCodeExterne(?string $codeExterne)
    {
        $this->codeExterne = $codeExterne;

        return $this;
    }

    public function getVersionMessagerie(): int
    {
        return $this->versionMessagerie;
    }

    public function setVersionMessagerie(int $versionMessagerie)
    {
        $this->versionMessagerie = $versionMessagerie;
    }

    /**
     * @return false|string[]
     */
    public function getArrayCpvSecondaire(): false|array
    {
        return explode('#', ltrim($this->codeCpv2, '#'));
    }

    /**
     * @return false|string[]
     */
    public function getArrayNuts(): false|array
    {
        return explode('#', ltrim($this->codesNuts, '#'));
    }

    public function hasThisClause(string $clauseName): bool
    {
        $getClause = 'get' . ucfirst($clauseName);
        if ($this->getAlloti()) {
            $lots = $this->getLots();
            foreach ($lots as $lot) {
                if ($lot->hasThisClause($clauseName)) {
                    return true;
                }
            }
        } else {
            if (self::CLAUSE_ACTIVE == $this->$getClause()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return DonneeComplementaire|null
     */
    public function getDonneeComplementaire(): ?DonneeComplementaire
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire(DonneeComplementaire $donneeComplementaire = null): self
    {
        $this->donneeComplementaire = $donneeComplementaire;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntitePublique()
    {
        return $this->organisme ?
            $this->organisme->getSigle() . ' - ' . $this->organisme->getDenominationOrg() : '';
    }

    /**
     * @return string
     */
    public function getEntiteAchat()
    {
        $cheminComplet = $this->service ? $this->service->getCheminComplet() : '';

        return (0 == $this->serviceId) ? $this->getEntitePublique() : $cheminComplet;
    }

    /**
     * Set typeProcedure.
     *
     * @param string $typeProcedure
     *
     * @return Consultation
     */
    public function setTypeProcedure($typeProcedure)
    {
        $this->typeProcedure = $typeProcedure;

        return $this;
    }

    /**
     * Get typeProcedure.
     *
     * @return string
     */
    public function getTypeProcedure()
    {
        return $this->typeProcedure;
    }

    /**
     * @return string
     */
    public function getLibelleTypeProcedure()
    {
        return $this->typeProcedureOrganisme ? $this->typeProcedureOrganisme->getLibelleTypeProcedure() : '';
    }

    /**
     * @return string|null
     */
    public function getNaturePrestation(): ?string
    {
        $lib = null;
        $lib = match ($this->categorie) {
            '1' => TypePrestation::TYPE_PRESTATION_TRAVAUX,
            '2' => TypePrestation::TYPE_PRESTATION_FOURNITURES,
            '3' => TypePrestation::TYPE_PRESTATION_SERVICES,
            default => $lib,
        };

        return $lib;
    }

    /**
     * @return PlateformeVirtuelle
     */
    public function getPlateformeVirtuelle(): ?PlateformeVirtuelle
    {
        return $this->plateformeVirtuelle;
    }

    public function setPlateformeVirtuelle(PlateformeVirtuelle $plateformeVirtuelle = null): Consultation
    {
        $this->plateformeVirtuelle = $plateformeVirtuelle;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsEnvoiPubliciteValidation()
    {
        return $this->isEnvoiPubliciteValidation;
    }

    /**
     * @param mixed $isEnvoiPubliciteValidation
     */
    public function setIsEnvoiPubliciteValidation($isEnvoiPubliciteValidation): Consultation
    {
        $this->isEnvoiPubliciteValidation = $isEnvoiPubliciteValidation;

        return $this;
    }

    /**
     * @return bool
     */
    public function getCmsActif(): bool
    {
        return $this->cmsActif;
    }

    /**
     * @param bool $cmsActif
     * @return Consultation
     */
    public function setCmsActif(bool $cmsActif): self
    {
        $this->cmsActif = $cmsActif;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getAutresPieceConsultation(): Collection
    {
        return $this->autresPieceConsultation;
    }

    public function addAutresPieceConsultation(AutrePieceConsultation $autresPieceConsultation): self
    {
        if (!$this->autresPieceConsultation->contains($autresPieceConsultation)) {
            $this->autresPieceConsultation[] = $autresPieceConsultation;
            $autresPieceConsultation->setConsultationId($this);
        }

        return $this;
    }

    public function removeAutresPieceConsultation(AutrePieceConsultation $autresPieceConsultation): self
    {
        if ($this->autresPieceConsultation->removeElement($autresPieceConsultation)) {
            // set the owning side to null (unless already changed)
            if ($autresPieceConsultation->getConsultationId() === $this) {
                $autresPieceConsultation->setConsultationId(null);
            }
        }

        return $this;
    }

    public function getPiecesGenere(): Collection
    {
        return $this->piecesGenere;
    }

    public function addPiecesGenere(PieceGenereConsultation $piecesGenere): self
    {
        if (!$this->piecesGenere->contains($piecesGenere)) {
            $this->piecesGenere[] = $piecesGenere;
            $piecesGenere->setConsultation($this);
        }

        return $this;
    }

    public function removePiecesGenere(PieceGenereConsultation $piecesGenere): self
    {
        if ($this->piecesGenere->removeElement($piecesGenere)) {
            // set the owning side to null (unless already changed)
            if ($piecesGenere->getConsultation() === $this) {
                $piecesGenere->setConsultation(null);
            }
        }

        return $this;
    }

    /**
     * Cette fonction sert à retourner une valeur vide
     * quand on n'a pas encore mis la date de fin pour éviter des bugs d'affichage
     * comme -000-31-01 00:00:00:00.
     */
    public function getAffichageDateFin(): \DateTime|string
    {
        if ($this->datefin->format('Y-m-d h:i:s') <= '1000-00-00 00:00:00') {
            return '';
        }

        return $this->datefin;
    }

    public function isAnnexeFinanciere(): bool
    {
        return $this->annexeFinanciere;
    }

    public function setAnnexeFinanciere(bool $annexeFinanciere): Consultation
    {
        $this->annexeFinanciere = $annexeFinanciere;

        return $this;
    }

    public function isAlloti(): bool
    {
        return $this->alloti === '1';
    }

    /**
     * @return Collection
     */
    public function getAnnexesFinanciereManuel(): ?Collection
    {
        return $this->annexesFinanciereManuel;
    }

    public function addAnnexesFinanciereManuel(AnnexeFinanciere $annexesFinanciereManuel): Consultation
    {
        if (!$this->annexesFinanciereManuel->contains($annexesFinanciereManuel)) {
            $this->annexesFinanciereManuel->add($annexesFinanciereManuel);
        }

        return $this;
    }

    public function removeAnnexesFinanciereManuel(AnnexeFinanciere $annexesFinanciereManuel): Consultation
    {
        if ($this->annexesFinanciereManuel->removeElement($annexesFinanciereManuel)) {
            // set the owning side to null (unless already changed)
            if ($annexesFinanciereManuel->getConsultation() === $this) {
                $annexesFinanciereManuel->setConsultation(null);
            }
        }

        return $this;
    }

    public function getDatefinDate(): ?DateTime
    {
        return $this->datefinDate;
    }

    public function setDatefinDate(?DateTime $datefinDate): Consultation
    {
        $this->datefinDate = $datefinDate;

        return $this;
    }

    public function getDatefinHeure(): ?DateTime
    {
        return $this->datefinHeure;
    }

    public function setDatefinHeure(?DateTime $datefinHeure): Consultation
    {
        $this->datefinHeure = $datefinHeure;

        return $this;
    }

    public function getAnnexeFinanciere(): ?bool
    {
        return $this->annexeFinanciere;
    }

    public function isEnvolActivation(): bool
    {
        return $this->envolActivation;
    }

    public function setEnvolActivation(bool $envolActivation): self
    {
        $this->envolActivation = $envolActivation;

        return $this;
    }

    public function addConsultationFavori(ConsultationFavoris $consultationFavori): self
    {
        if (!$this->consultationFavoris->contains($consultationFavori)) {
            $this->consultationFavoris[] = $consultationFavori;
            $consultationFavori->setConsultation($this);
        }

        return $this;
    }

    public function removeConsultationFavori(ConsultationFavoris $consultationFavori): self
    {
        if ($this->consultationFavoris->removeElement($consultationFavori)) {
            // set the owning side to null (unless already changed)
            if ($consultationFavori->getConsultation() === $this) {
                $consultationFavori->setConsultation(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getReferenceConsultationsChildren(): Collection
    {
        return $this->referenceConsultationsChildren;
    }

    public function getReferenceConsultationParent()
    {
        return $this->referenceConsultationParent;
    }

    public function setReferenceConsultationParent(?Consultation $consultationParent): self
    {
        $this->referenceConsultationParent = $consultationParent;

        return $this;
    }

    public function getCountTelechargement(): int
    {
        return count($this->telechargements);
    }

    public function getCountOffre(): int
    {
        return count($this->offres) + count($this->offresPapiers);
    }

    public function getCountQuestion(): int
    {
        return count($this->questions);
    }

    /**
     * @return bool|null
     */
    public function isGroupement(): ?bool
    {
        return $this->groupement;
    }

    /**
     * @param bool $groupement
     * @return $this
     */
    public function setGroupement(?bool $groupement): self
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAutreTechniqueAchat(): ?string
    {
        return $this->autreTechniqueAchat;
    }

    /**
     * @param string|null $autreTechniqueAchat
     * @return $this
     */
    public function setAutreTechniqueAchat(?string $autreTechniqueAchat): self
    {
        $this->autreTechniqueAchat = $autreTechniqueAchat;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function isProcedureOuverte(): ?bool
    {
        return $this->procedureOuverte;
    }

    /**
     * @param bool|null $procedureOuverte
     */
    public function setProcedureOuverte(?bool $procedureOuverte): self
    {
        $this->procedureOuverte = $procedureOuverte;

        return $this;
    }

    public function getCalculatedStatus(): ?string
    {
        return $this->calculatedStatus;
    }

    public function setCalculatedStatus(?string $calculatedStatus): Consultation
    {
        $this->calculatedStatus = $calculatedStatus;

        return $this;
    }

    public function setCodeDceRestreint(?string $codeDceRestreint): self
    {
        $this->codeDceRestreint = $codeDceRestreint;

        return $this;
    }

    public function getCodeDceRestreint(): ?string
    {
        return $this->codeDceRestreint;
    }

    public function getTypeFormulaireDume(): int
    {
        return $this->typeFormulaireDume;
    }

    public function setTypeFormulaireDume(int $typeFormulaireDume): self
    {
        $this->typeFormulaireDume = $typeFormulaireDume;

        return $this;
    }

    /**
     * @param $controleTailleDepot
     * @return $this
     */
    public function setControleTailleDepot($controleTailleDepot): Consultation
    {
        $this->controleTailleDepot = $controleTailleDepot;

        return $this;
    }

    /**
     * @return int
     */
    public function getControleTailleDepot(): ?int
    {
        return $this->controleTailleDepot;
    }

    /**
     * @return string
     */
    public function getNumeroProjetAchat(): ?string
    {
        return $this->numeroProjetAchat;
    }

    /**
     * @param string $numeroProjetAchat
     */
    public function setNumeroProjetAchat(string $numeroProjetAchat): self
    {
        $this->numeroProjetAchat = $numeroProjetAchat;

        return $this;
    }

    /**
     * @return Collection<int, ClausesN1>
     */
    public function getClausesN1(): Collection
    {
        return $this->clausesN1;
    }

    public function addClausesN1(ClausesN1 $clausesN1): self
    {
        if (!$this->clausesN1->contains($clausesN1)) {
            $this->clausesN1[] = $clausesN1;
            $clausesN1->setConsultation($this);
        }

        return $this;
    }

    public function removeClausesN1(ClausesN1 $clausesN1): self
    {
        if ($this->clausesN1->removeElement($clausesN1)) {
            // set the owning side to null (unless already changed)
            if ($clausesN1->getConsultation() === $this) {
                $clausesN1->setConsultation(null);
            }
        }

        return $this;
    }

    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTags(ConsultationTags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setConsultation($this);
        }

        return $this;
    }

    public function getAgentCreateur(): ?Agent
    {
        return $this->agentCreateur;
    }

    public function setAgentCreateur(?Agent $agentCreateur): self
    {
        $this->agentCreateur = $agentCreateur;

        return $this;
    }

    public function getDce(): Collection
    {
        return $this->dce;
    }

    public function setDce(Collection $dce): self
    {
        $this->dce = $dce;

        return $this;
    }

    public function getDonneePubliciteObligatoire(): ?string
    {
        return $this->donneePubliciteObligatoire;
    }

    public function setDonneePubliciteObligatoire(?string $donneePubliciteObligatoire): self
    {
        $this->donneePubliciteObligatoire = $donneePubliciteObligatoire;

        return $this;
    }

    public function getReferentielAchat(): ?ReferentielAchat
    {
        return $this->referentielAchat;
    }

    public function setReferentielAchat(?ReferentielAchat $referentielAchat): self
    {
        $this->referentielAchat = $referentielAchat;

        return $this;
    }

    public function getAgentTechniqueCreateur(): ?Agent
    {
        return $this->agentTechniqueCreateur;
    }

    public function setAgentTechniqueCreateur(?Agent $agentTechniqueCreateur): void
    {
        $this->agentTechniqueCreateur = $agentTechniqueCreateur;
    }

    public function getRc(): Collection
    {
        return $this->rc;
    }

    public function setRc(Collection $rc): self
    {
        $this->rc = $rc;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getRetaitPapiers()
    {
        return $this->retraitPapiers;
    }

    public function getCountRetrait(): int
    {
        return $this->getCountTelechargement() + count($this->retraitPapiers);
    }

    public function getBesoinRecurrent(): ?bool
    {
        return $this->besoinRecurrent;
    }

    public function setBesoinRecurrent(bool $besoinRecurrent): self
    {
        $this->besoinRecurrent = $besoinRecurrent;

        return $this;
    }

    public function getUuid(): ?UuidInterface
    {
        return $this->uuid;
    }

    public function getContratExecUuid(): ?string
    {
        return $this->contratExecUuid;
    }

    public function setContratExecUuid(?string $contratExecUuid): self
    {
        $this->contratExecUuid = $contratExecUuid;

        return $this;
    }

    public function getAutresPieces(): Collection
    {
        return $this->autresPieces;
    }

    public function setAutresPieces(Collection $autresPieces): Consultation
    {
        $this->autresPieces = $autresPieces;

        return $this;
    }

    public function getOldServiceId(): ?int
    {
        return $this->oldServiceId;
    }
}
