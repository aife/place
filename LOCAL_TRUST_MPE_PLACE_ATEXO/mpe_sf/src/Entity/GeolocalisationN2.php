<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Stringable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * GeolocalisationN2.
 *
 * @ORM\Table(name="GeolocalisationN2",
 *     indexes={@ORM\Index(name="id_geolocalisationN1", columns={"id_geolocalisationN1"})})
 * @ORM\Entity(repositoryClass="App\Repository\GeolocalisationN2Repository")
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [
        'groups' => ['geolocalisationN2Read'],
        AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true
    ],
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'denomination1' => 'partial',
        'denomination2' => 'exact',
        'codeNuts'      => 'partial'
    ]
)]
class GeolocalisationN2 implements Stringable
{
    /**
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     */
    public function __toString(): string
    {
        return $this->getDenomination1();
    }

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\GeolocalisationN1", inversedBy="geolocalisationN2s")
     * @ORM\JoinColumn(name="id_geolocalisationN1", referencedColumnName="id")
     */
    private ?GeolocalisationN1 $geolocalisationN1 = null;

    /**
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    #[Groups('geolocalisationN2Read')]
    private int $id;

    /**
     * @ORM\Column(name="id_geolocalisationN1", type="integer", nullable=false)
     */
    private string|int $idGeolocalisationN1 = '0';

    /**
     * @ORM\Column(name="denomination1", type="string", length=50, nullable=false)
     */
    #[Groups('geolocalisationN2Read')]
    private ?string $denomination1 = null;

    /**
     * @ORM\Column(name="denomination2", type="string", length=30, nullable=false)
     */
    #[Groups('geolocalisationN2Read')]
    private string $denomination2 = '';

    /**
     * @ORM\Column(name="valeur_avec_sous_categorie", type="string", nullable=false)
     */
    private string $valeurAvecSousCategorie = '1';

    /**
     * @ORM\Column(name="denomination1_ar", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Ar = null;

    /**
     * @ORM\Column(name="denomination2_ar", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Ar = null;

    /**
     * @ORM\Column(name="denomination1_fr", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Fr = null;

    /**
     * @ORM\Column(name="denomination2_fr", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Fr = null;

    /**
     * @ORM\Column(name="denomination1_en", type="string", length=50, nullable=false)
     */
    private ?string $denomination1En = null;

    /**
     * @ORM\Column(name="denomination2_en", type="string", length=30, nullable=false)
     */
    private ?string $denomination2En = null;

    /**
     * @ORM\Column(name="denomination1_es", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Es = null;

    /**
     * @ORM\Column(name="denomination2_es", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Es = null;

    /**
     * @ORM\Column(name="denomination1_su", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Su = null;

    /**
     * @ORM\Column(name="denomination2_su", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Su = null;

    /**
     * @ORM\Column(name="denomination1_du", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Du = null;

    /**
     * @ORM\Column(name="denomination2_du", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Du = null;

    /**
     * @ORM\Column(name="denomination1_cz", type="string", length=50, nullable=false)
     */
    private ?string $denomination1Cz = null;

    /**
     * @ORM\Column(name="denomination2_cz", type="string", length=30, nullable=false)
     */
    private ?string $denomination2Cz = null;

    /**
     * @ORM\Column(name="denomination1_it", type="string", length=50, nullable=false)
     */
    private string $denomination1It = '';

    /**
     * @ORM\Column(name="denomination2_it", type="string", length=30, nullable=false)
     */
    private string $denomination2It = '';

    /**
     * @ORM\Column(name="code_interface", type="string", length=255, nullable=true)
     */
    private ?string $codeInterface = null;

    /**
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private ?string $valeurSub = null;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private ?string $typeLieu = null;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    #[Groups('geolocalisationN2Read')]
    private ?string $codeNuts = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idGeolocalisationN1.
     *
     * @param int $idGeolocalisationN1
     *
     * @return GeolocalisationN2
     */
    public function setIdGeolocalisationN1($idGeolocalisationN1)
    {
        $this->idGeolocalisationN1 = $idGeolocalisationN1;

        return $this;
    }

    /**
     * Get idGeolocalisationN1.
     *
     * @return int
     */
    public function getIdGeolocalisationN1()
    {
        return $this->idGeolocalisationN1;
    }

    /**
     * Set denomination1.
     *
     * @param string $denomination1
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1($denomination1)
    {
        $this->denomination1 = $denomination1;

        return $this;
    }

    /**
     * Get denomination1.
     *
     * @return string
     */
    public function getDenomination1()
    {
        return $this->denomination1;
    }

    /**
     * Set denomination2.
     *
     * @param string $denomination2
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2($denomination2)
    {
        $this->denomination2 = $denomination2;

        return $this;
    }

    /**
     * Get denomination2.
     *
     * @return string
     */
    public function getDenomination2()
    {
        return $this->denomination2;
    }

    /**
     * Set valeurAvecSousCategorie.
     *
     * @param string $valeurAvecSousCategorie
     *
     * @return GeolocalisationN2
     */
    public function setValeurAvecSousCategorie($valeurAvecSousCategorie)
    {
        $this->valeurAvecSousCategorie = $valeurAvecSousCategorie;

        return $this;
    }

    /**
     * Get valeurAvecSousCategorie.
     *
     * @return string
     */
    public function getValeurAvecSousCategorie()
    {
        return $this->valeurAvecSousCategorie;
    }

    /**
     * Set denomination1Ar.
     *
     * @param string $denomination1Ar
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Ar($denomination1Ar)
    {
        $this->denomination1Ar = $denomination1Ar;

        return $this;
    }

    /**
     * Get denomination1Ar.
     *
     * @return string
     */
    public function getDenomination1Ar()
    {
        return $this->denomination1Ar;
    }

    /**
     * Set denomination2Ar.
     *
     * @param string $denomination2Ar
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Ar($denomination2Ar)
    {
        $this->denomination2Ar = $denomination2Ar;

        return $this;
    }

    /**
     * Get denomination2Ar.
     *
     * @return string
     */
    public function getDenomination2Ar()
    {
        return $this->denomination2Ar;
    }

    /**
     * Set denomination1Fr.
     *
     * @param string $denomination1Fr
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Fr($denomination1Fr)
    {
        $this->denomination1Fr = $denomination1Fr;

        return $this;
    }

    /**
     * Get denomination1Fr.
     *
     * @return string
     */
    public function getDenomination1Fr()
    {
        return $this->denomination1Fr;
    }

    /**
     * Set denomination2Fr.
     *
     * @param string $denomination2Fr
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Fr($denomination2Fr)
    {
        $this->denomination2Fr = $denomination2Fr;

        return $this;
    }

    /**
     * Get denomination2Fr.
     *
     * @return string
     */
    public function getDenomination2Fr()
    {
        return $this->denomination2Fr;
    }

    /**
     * Set denomination1En.
     *
     * @param string $denomination1En
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1En($denomination1En)
    {
        $this->denomination1En = $denomination1En;

        return $this;
    }

    /**
     * Get denomination1En.
     *
     * @return string
     */
    public function getDenomination1En()
    {
        return $this->denomination1En;
    }

    /**
     * Set denomination2En.
     *
     * @param string $denomination2En
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2En($denomination2En)
    {
        $this->denomination2En = $denomination2En;

        return $this;
    }

    /**
     * Get denomination2En.
     *
     * @return string
     */
    public function getDenomination2En()
    {
        return $this->denomination2En;
    }

    /**
     * Set denomination1Es.
     *
     * @param string $denomination1Es
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Es($denomination1Es)
    {
        $this->denomination1Es = $denomination1Es;

        return $this;
    }

    /**
     * Get denomination1Es.
     *
     * @return string
     */
    public function getDenomination1Es()
    {
        return $this->denomination1Es;
    }

    /**
     * Set denomination2Es.
     *
     * @param string $denomination2Es
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Es($denomination2Es)
    {
        $this->denomination2Es = $denomination2Es;

        return $this;
    }

    /**
     * Get denomination2Es.
     *
     * @return string
     */
    public function getDenomination2Es()
    {
        return $this->denomination2Es;
    }

    /**
     * Set denomination1Su.
     *
     * @param string $denomination1Su
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Su($denomination1Su)
    {
        $this->denomination1Su = $denomination1Su;

        return $this;
    }

    /**
     * Get denomination1Su.
     *
     * @return string
     */
    public function getDenomination1Su()
    {
        return $this->denomination1Su;
    }

    /**
     * Set denomination2Su.
     *
     * @param string $denomination2Su
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Su($denomination2Su)
    {
        $this->denomination2Su = $denomination2Su;

        return $this;
    }

    /**
     * Get denomination2Su.
     *
     * @return string
     */
    public function getDenomination2Su()
    {
        return $this->denomination2Su;
    }

    /**
     * Set denomination1Du.
     *
     * @param string $denomination1Du
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Du($denomination1Du)
    {
        $this->denomination1Du = $denomination1Du;

        return $this;
    }

    /**
     * Get denomination1Du.
     *
     * @return string
     */
    public function getDenomination1Du()
    {
        return $this->denomination1Du;
    }

    /**
     * Set denomination2Du.
     *
     * @param string $denomination2Du
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Du($denomination2Du)
    {
        $this->denomination2Du = $denomination2Du;

        return $this;
    }

    /**
     * Get denomination2Du.
     *
     * @return string
     */
    public function getDenomination2Du()
    {
        return $this->denomination2Du;
    }

    /**
     * Set denomination1Cz.
     *
     * @param string $denomination1Cz
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1Cz($denomination1Cz)
    {
        $this->denomination1Cz = $denomination1Cz;

        return $this;
    }

    /**
     * Get denomination1Cz.
     *
     * @return string
     */
    public function getDenomination1Cz()
    {
        return $this->denomination1Cz;
    }

    /**
     * Set denomination2Cz.
     *
     * @param string $denomination2Cz
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2Cz($denomination2Cz)
    {
        $this->denomination2Cz = $denomination2Cz;

        return $this;
    }

    /**
     * Get denomination2Cz.
     *
     * @return string
     */
    public function getDenomination2Cz()
    {
        return $this->denomination2Cz;
    }

    /**
     * Set denomination1It.
     *
     * @param string $denomination1It
     *
     * @return GeolocalisationN2
     */
    public function setDenomination1It($denomination1It)
    {
        $this->denomination1It = $denomination1It;

        return $this;
    }

    /**
     * Get denomination1It.
     *
     * @return string
     */
    public function getDenomination1It()
    {
        return $this->denomination1It;
    }

    /**
     * Set denomination2It.
     *
     * @param string $denomination2It
     *
     * @return GeolocalisationN2
     */
    public function setDenomination2It($denomination2It)
    {
        $this->denomination2It = $denomination2It;

        return $this;
    }

    /**
     * Get denomination2It.
     *
     * @return string
     */
    public function getDenomination2It()
    {
        return $this->denomination2It;
    }

    /**
     * Set codeInterface.
     *
     * @param string $codeInterface
     *
     * @return GeolocalisationN2
     */
    public function setCodeInterface($codeInterface)
    {
        $this->codeInterface = $codeInterface;

        return $this;
    }

    /**
     * Get codeInterface.
     *
     * @return string
     */
    public function getCodeInterface()
    {
        return $this->codeInterface;
    }

    public function setGeolocalisationN1(GeolocalisationN1 $geolocalisationN1)
    {
        $this->geolocalisationN1 = $geolocalisationN1;
    }

    /**
     * @return GeolocalisationN1
     */
    public function getGeolocalisationN1()
    {
        return $this->geolocalisationN1;
    }

    /**
     * @return string
     */
    public function getValeurSub()
    {
        return $this->valeurSub;
    }

    public function setValeurSub(string $valeurSub): GeolocalisationN2
    {
        $this->valeurSub = $valeurSub;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeLieu()
    {
        return $this->typeLieu;
    }

    public function setTypeLieu(string $typeLieu): GeolocalisationN2
    {
        $this->typeLieu = $typeLieu;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodeNuts()
    {
        return $this->codeNuts;
    }

    public function setCodeNuts(string $codeNuts): GeolocalisationN2
    {
        $this->codeNuts = $codeNuts;

        return $this;
    }
}
