<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConfigurationMessagesTraduction.
 *
 * @ORM\Table(name="configuration_messages_traduction")
 * @ORM\Entity(repositoryClass="App\Repository\ConfigurationMessagesTraductionRepository")
 */
class ConfigurationMessagesTraduction
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="source", type="string", length=300, unique=true)
     */
    private ?string $source = null;

    /**
     * @ORM\Column(name="target", type="text")
     */
    private ?string $target = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Langue", inversedBy="configurationMessagesTraduction")
     * @ORM\JoinColumn(name="langue_id", referencedColumnName="id_langue")
     */
    private $langue;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return ConfigurationMessagesTraduction
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set target.
     *
     * @param string $target
     *
     * @return ConfigurationMessagesTraduction
     */
    public function setTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target.
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getLangue()
    {
        return $this->langue;
    }

    /**
     * @param $langue
     *
     * @return $this
     */
    public function setLangue($langue)
    {
        $this->langue = $langue;

        return $this;
    }
}
