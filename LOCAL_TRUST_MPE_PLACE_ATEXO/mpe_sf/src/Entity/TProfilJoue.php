<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Output\ProfilJoueOutput;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="T_Profil_Joue")
 * @ORM\Entity
 */
#[ApiResource(
    collectionOperations: ['get'],
    itemOperations: ['get'],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: ProfilJoueOutput::class,
)]
#[ApiFilter(SearchFilter::class, properties: ['agent' => 'exact', 'organisme' => 'exact'])]
class TProfilJoue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Agent")
     * @ORM\JoinColumn(name="id_agent", referencedColumnName="id")
     */
    private ?Agent $agent = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_officiel", type="string", length=256, nullable=false)
     */
    private string $nomOfficiel;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=false)
     */
    private string $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=false)
     */
    private string $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=50, nullable=false)
     */
    private string $adresse;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code_postal", type="string", length=50, nullable=true)
     */
    private ?string $codePostal = null;

    /**
     * @var string
     *
     * @ORM\Column(name="point_contact", type="string", length=256, nullable=false)
     */
    private string $pointContact;

    /**
     * @var string
     *
     * @ORM\Column(name="a_attention_de", type="string", length=50, nullable=false)
     */
    private string $aAttentionDe;

    /**
     * @var string|null
     *
     * @ORM\Column(name="telephone", type="string", length=50, nullable=true)
     */
    private ?string $telephone = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fax", type="string", length=50, nullable=true)
     */
    private ?string $fax = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private ?string $email = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse_pouvoir_adjudicateur", type="string", length=256, nullable=true)
     */
    private ?string $adressePouvoirAdjudicateur = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="adresse_profil_acheteur", type="string", length=256, nullable=true)
     */
    private ?string $adresseProfilAcheteur = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="autorite_nationale", type="integer", nullable=true)
     */
    private ?int $autoriteNationale  = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="office_nationale", type="integer", nullable=true)
     */
    private ?int $officeNationale = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="collectivite_territoriale", type="integer", nullable=true)
     */
    private ?int $collectiviteTerritoriale = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="office_regionale", type="integer", nullable=true)
     */
    private ?int $officeRegionale = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="organisme_public", type="integer", nullable=true)
     */
    private ?int $organismePublic = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="organisation_europenne", type="integer", nullable=true)
     */
    private ?int $organisationEuropenne = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="autre_type_pouvoir_adjudicateur", type="integer", nullable=true)
     */
    private ?int $autreTypePouvoirAdjudicateur = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="autre_libelle_type_pouvoir_adjudicateur", type="string", length=50, nullable=true)
     */
    private ?string $autreLibelleTypePouvoirAdjudicateur = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="services_generaux", type="integer", nullable=true)
     */
    private ?int $servicesGeneraux = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="defense", type="integer", nullable=true)
     */
    private ?int $defense  = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="securite_public", type="integer", nullable=true)
     */
    private ?int $securitePublic = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="environnement", type="integer", nullable=true)
     */
    private ?int $environnement = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="affaires_economiques", type="integer", nullable=true)
     */
    private ?int $affairesEconomiques = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sante", type="integer", nullable=true)
     */
    private ?int $sante = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="developpement_collectif", type="integer", nullable=true)
     */
    private ?int $developpementCollectif = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="protection_sociale", type="integer", nullable=true)
     */
    private ?int $protectionSociale = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="loisirs", type="integer", nullable=true)
     */
    private ?int $loisirs = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="eduction", type="integer", nullable=true)
     */
    private ?int $eduction = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="autre_activites_principales", type="integer", nullable=true)
     */
    private ?int $autreActivitesPrincipales = null;

    /**
     * @var string|null
     *
     * @ORM\Column(name="autre_libelle_activites_principales", type="string", length=50, nullable=true)
     */
    private ?string $autreLibelleActivitesPrincipales = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pouvoir_adjudicateur_agit", type="integer", nullable=true)
     */
    private ?int $pouvoirAdjudicateurAgit = null;

    /**
     * @var int|null
     *
     * @ORM\Column(name="pouvoir_adjudicateur_marche_couvert", type="integer", nullable=true)
     */
    private ?int $pouvoirAdjudicateurMarcheCouvert = null;

    /**
     * @var int
     *
     * @ORM\Column(name="entite_adjudicatrice_marche_couvert", type="integer", nullable=false)
     */
    private int $entiteAdjudicatriceMarcheCouvert;

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_national_identification", type="string", length=20, nullable=true)
     */
    private ?string $numeroNationalIdentification  = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAgent(): ?Agent
    {
        return $this->agent;
    }

    public function setAgent(Agent $agent): self
    {
        $this->agent = $agent;

        return $this;
    }

    public function getOrganisme(): ?Organisme
    {
        return $this->organisme;
    }

    public function setOrganisme(Organisme $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getNomOfficiel(): ?string
    {
        return $this->nomOfficiel;
    }

    public function setNomOfficiel(string $nomOfficiel): self
    {
        $this->nomOfficiel = $nomOfficiel;

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = $pays;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getCodePostal(): ?string
    {
        return $this->codePostal;
    }

    public function setCodePostal(?string $codePostal): self
    {
        $this->codePostal = $codePostal;

        return $this;
    }

    public function getPointContact(): ?string
    {
        return $this->pointContact;
    }

    public function setPointContact(string $pointContact): self
    {
        $this->pointContact = $pointContact;

        return $this;
    }

    public function getAAttentionDe(): ?string
    {
        return $this->aAttentionDe;
    }

    public function setAAttentionDe(string $aAttentionDe): self
    {
        $this->aAttentionDe = $aAttentionDe;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getAdressePouvoirAdjudicateur(): ?string
    {
        return $this->adressePouvoirAdjudicateur;
    }

    public function setAdressePouvoirAdjudicateur(?string $adressePouvoirAdjudicateur): self
    {
        $this->adressePouvoirAdjudicateur = $adressePouvoirAdjudicateur;

        return $this;
    }

    public function getAdresseProfilAcheteur(): ?string
    {
        return $this->adresseProfilAcheteur;
    }

    public function setAdresseProfilAcheteur(?string $adresseProfilAcheteur): self
    {
        $this->adresseProfilAcheteur = $adresseProfilAcheteur;

        return $this;
    }

    public function getAutoriteNationale(): ?int
    {
        return $this->autoriteNationale;
    }

    public function setAutoriteNationale(?int $autoriteNationale): self
    {
        $this->autoriteNationale = $autoriteNationale;

        return $this;
    }

    public function getOfficeNationale(): ?int
    {
        return $this->officeNationale;
    }

    public function setOfficeNationale(?int $officeNationale): self
    {
        $this->officeNationale = $officeNationale;

        return $this;
    }

    public function getCollectiviteTerritoriale(): ?int
    {
        return $this->collectiviteTerritoriale;
    }

    public function setCollectiviteTerritoriale(?int $collectiviteTerritoriale): self
    {
        $this->collectiviteTerritoriale = $collectiviteTerritoriale;

        return $this;
    }

    public function getOfficeRegionale(): ?int
    {
        return $this->officeRegionale;
    }

    public function setOfficeRegionale(?int $officeRegionale): self
    {
        $this->officeRegionale = $officeRegionale;

        return $this;
    }

    public function getOrganismePublic(): ?int
    {
        return $this->organismePublic;
    }

    public function setOrganismePublic(?int $organismePublic): self
    {
        $this->organismePublic = $organismePublic;

        return $this;
    }

    public function getOrganisationEuropenne(): ?int
    {
        return $this->organisationEuropenne;
    }

    public function setOrganisationEuropenne(?int $organisationEuropenne): self
    {
        $this->organisationEuropenne = $organisationEuropenne;

        return $this;
    }

    public function getAutreTypePouvoirAdjudicateur(): ?int
    {
        return $this->autreTypePouvoirAdjudicateur;
    }

    public function setAutreTypePouvoirAdjudicateur(?int $autreTypePouvoirAdjudicateur): self
    {
        $this->autreTypePouvoirAdjudicateur = $autreTypePouvoirAdjudicateur;

        return $this;
    }

    public function getAutreLibelleTypePouvoirAdjudicateur(): ?string
    {
        return $this->autreLibelleTypePouvoirAdjudicateur;
    }

    public function setAutreLibelleTypePouvoirAdjudicateur(?string $autreLibelleTypePouvoirAdjudicateur): self
    {
        $this->autreLibelleTypePouvoirAdjudicateur = $autreLibelleTypePouvoirAdjudicateur;

        return $this;
    }

    public function getServicesGeneraux(): ?int
    {
        return $this->servicesGeneraux;
    }

    public function setServicesGeneraux(?int $servicesGeneraux): self
    {
        $this->servicesGeneraux = $servicesGeneraux;

        return $this;
    }

    public function getDefense(): ?int
    {
        return $this->defense;
    }

    public function setDefense(?int $defense): self
    {
        $this->defense = $defense;

        return $this;
    }

    public function getSecuritePublic(): ?int
    {
        return $this->securitePublic;
    }

    public function setSecuritePublic(?int $securitePublic): self
    {
        $this->securitePublic = $securitePublic;

        return $this;
    }

    public function getEnvironnement(): ?int
    {
        return $this->environnement;
    }

    public function setEnvironnement(?int $environnement): self
    {
        $this->environnement = $environnement;

        return $this;
    }

    public function getAffairesEconomiques(): ?int
    {
        return $this->affairesEconomiques;
    }

    public function setAffairesEconomiques(?int $affairesEconomiques): self
    {
        $this->affairesEconomiques = $affairesEconomiques;

        return $this;
    }

    public function getSante(): ?int
    {
        return $this->sante;
    }

    public function setSante(?int $sante): self
    {
        $this->sante = $sante;

        return $this;
    }

    public function getDeveloppementCollectif(): ?int
    {
        return $this->developpementCollectif;
    }

    public function setDeveloppementCollectif(?int $developpementCollectif): self
    {
        $this->developpementCollectif = $developpementCollectif;

        return $this;
    }

    public function getProtectionSociale(): ?int
    {
        return $this->protectionSociale;
    }

    public function setProtectionSociale(?int $protectionSociale): self
    {
        $this->protectionSociale = $protectionSociale;

        return $this;
    }

    public function getLoisirs(): ?int
    {
        return $this->loisirs;
    }

    public function setLoisirs(?int $loisirs): self
    {
        $this->loisirs = $loisirs;

        return $this;
    }

    public function getEduction(): ?int
    {
        return $this->eduction;
    }

    public function setEduction(?int $eduction): self
    {
        $this->eduction = $eduction;

        return $this;
    }

    public function getAutreActivitesPrincipales(): ?int
    {
        return $this->autreActivitesPrincipales;
    }

    public function setAutreActivitesPrincipales(?int $autreActivitesPrincipales): self
    {
        $this->autreActivitesPrincipales = $autreActivitesPrincipales;

        return $this;
    }

    public function getAutreLibelleActivitesPrincipales(): ?string
    {
        return $this->autreLibelleActivitesPrincipales;
    }

    public function setAutreLibelleActivitesPrincipales(?string $autreLibelleActivitesPrincipales): self
    {
        $this->autreLibelleActivitesPrincipales = $autreLibelleActivitesPrincipales;

        return $this;
    }

    public function getPouvoirAdjudicateurAgit(): ?int
    {
        return $this->pouvoirAdjudicateurAgit;
    }

    public function setPouvoirAdjudicateurAgit(?int $pouvoirAdjudicateurAgit): self
    {
        $this->pouvoirAdjudicateurAgit = $pouvoirAdjudicateurAgit;

        return $this;
    }

    public function getPouvoirAdjudicateurMarcheCouvert(): ?int
    {
        return $this->pouvoirAdjudicateurMarcheCouvert;
    }

    public function setPouvoirAdjudicateurMarcheCouvert(?int $pouvoirAdjudicateurMarcheCouvert): self
    {
        $this->pouvoirAdjudicateurMarcheCouvert = $pouvoirAdjudicateurMarcheCouvert;

        return $this;
    }

    public function getEntiteAdjudicatriceMarcheCouvert(): ?int
    {
        return $this->entiteAdjudicatriceMarcheCouvert;
    }

    public function setEntiteAdjudicatriceMarcheCouvert(int $entiteAdjudicatriceMarcheCouvert): self
    {
        $this->entiteAdjudicatriceMarcheCouvert = $entiteAdjudicatriceMarcheCouvert;

        return $this;
    }

    public function getNumeroNationalIdentification(): ?string
    {
        return $this->numeroNationalIdentification;
    }

    public function setNumeroNationalIdentification(?string $numeroNationalIdentification): self
    {
        $this->numeroNationalIdentification = $numeroNationalIdentification;

        return $this;
    }
}
