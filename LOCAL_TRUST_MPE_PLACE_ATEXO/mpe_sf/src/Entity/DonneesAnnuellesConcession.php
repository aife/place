<?php

namespace App\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * DonneesAnnuellesConcession.
 *
 * @ORM\Table(name="donnees_annuelles_concession")
 * @ORM\Entity
 */
class DonneesAnnuellesConcession
{
    /**
     *
     * @ORM\Column(name="id", type="integer", length=11)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\DonneesAnnuellesConcessionTarif", mappedBy="donneesAnnuellesConcession")
     * @ORM\JoinColumn(name="id", referencedColumnName="id_donnees_annuelle")
     */
    private Collection $donneesAnnuellesConcessionTarifs;

    /**
     * @ORM\Column(name="id_contrat", type="integer", length=11, nullable=false)
     */
    private ?int $idContrat = null;

    /**
     * @var string
     *
     * @ORM\Column(name="valeur_depense", type="decimal", nullable=true)
     */
    private $valeurDepense;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_saisie", type="datetime", nullable=true)*/
    private $dateSaisie;

    /**
     * @ORM\Column(name="num_ordre", type="integer", nullable=true)
     */
    private ?int $numOrdre = null;

    /**
     * @var int
     *
     * @ORM\Column(name="suivi_publication_sn", type="integer", nullable=true)
     */
    private $suiviPublicationSn;

    /**
     * @return DateTime
     */
    public function getSuiviPublicationSn()
    {
        return $this->suiviPublicationSn;
    }

    /**
     * @param DateTime $suiviPublicationSn
     */
    public function setSuiviPublicationSn($suiviPublicationSn)
    {
        $this->suiviPublicationSn = $suiviPublicationSn;
    }

    public function getNumOrdre(): int
    {
        return $this->numOrdre;
    }

    public function setNumOrdre(int $numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }

    /**
     * ContratTitulaire constructor.
     */
    public function __construct()
    {
        $this->donneesAnnuellesConcessionTarifs = new ArrayCollection();
        $this->dateSaisie = new DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param int $idContrat
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
    }

    /**
     * @return string
     */
    public function getValeurDepense()
    {
        return $this->valeurDepense;
    }

    /**
     * @param string $valeurDepense
     */
    public function setValeurDepense($valeurDepense)
    {
        $this->valeurDepense = $valeurDepense;
    }

    /**
     * @return DateTime
     */
    public function getDateSaisie()
    {
        return $this->dateSaisie;
    }

    /**
     * @param DateTime $dateSaisie
     */
    public function setDateSaisie($dateSaisie)
    {
        $this->dateSaisie = $dateSaisie;
    }

    /**
     * @return $this
     */
    public function addDonneesAnnuellesConcessionTarif(DonneesAnnuellesConcessionTarif $donneesAnnuellesConcessionTarif)
    {
        $this->donneesAnnuellesConcessionTarifs[] = $donneesAnnuellesConcessionTarif;
        $donneesAnnuellesConcessionTarif->setDonneesAnnuellesConcession($this);

        return $this;
    }

    public function removeDonneesAnnuellesConcessionTarif(DonneesAnnuellesConcessionTarif $donneesAnnuellesConcessionTarif)
    {
        $this->donneesAnnuellesConcessionTarifs->removeElement($donneesAnnuellesConcessionTarif);
    }

    /**
     * @return ArrayCollection
     */
    public function getDonneesAnnuellesConcessionTarifs()
    {
        return $this->donneesAnnuellesConcessionTarifs;
    }
}
