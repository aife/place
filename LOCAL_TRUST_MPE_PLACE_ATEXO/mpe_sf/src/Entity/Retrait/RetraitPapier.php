<?php

namespace App\Entity\Retrait;

use App\Entity\Consultation;
use App\Entity\Organisme;
use DateTime;
use App\Traits\Entity\PlateformeVirtuelleLinkTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * Telechargement.
 *
 * @ORM\Table(name="Retrait_Papier",
 *     indexes={
 *     @ORM\Index(name="organisme", columns={"organisme"}),
 *     @ORM\Index(name="consultation_id", columns={"consultation_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Retrait\RetraitPapierRepository")
 */
class RetraitPapier
{
    use PlateformeVirtuelleLinkTrait;

    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="telechargements")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="consultation_ref", type="integer", nullable=false)*/
    private string|int $consultationRef = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)*/
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=100, nullable=false)*/
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="entreprise", type="string", length=100, nullable=false)*/
    private $entreprise;

    /**
     * @ORM\Column(name="datetelechargement", type="string", length=20, nullable=false)
     */
    private string|DateTime $dateTelechargement = '0000-00-00 00:00:00';

    /**
     * @ORM\Column(name="codepostal", type="string", length=5, nullable=false)
     */
    private string $codepostal = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ville", type="string", length=50, nullable=false)*/
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="pays", type="string", length=50, nullable=false)*/
    private $pays;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=30, nullable=false)*/
    private string $telephone;

    /**
     * @ORM\Column(name="tirage_plan", type="integer", nullable=false)
     */
    private string|int $tiragePlan = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=14, nullable=true)*/
    private ?string $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=30, nullable=false)*/
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=100, nullable=false)
     */
    private $email;

    /**
     * @ORM\Column(name="Observation", type="text", length=65535, nullable=true)
     */
    private ?string $observation = null;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=11, nullable=true)
     */
    private ?string $prenom;

    /**
     * @ORM\Column(name="adresse2", type="string", length=80, nullable=true)
     */
    private ?string $adresse2;

    /**
     * @ORM\Column(name="identifiant_national", type="string", length=20, nullable=true)
     */
    private ?string $identifiantNational;

    /**
     * @var string
     *
     * @ORM\Column(name="acronyme_pays", type="string", length=10, nullable=true)*/
    private ?string $acronymePays;

    /**
     * @ORM\Column(name="support", type="integer", nullable=true)
     */
    private ?int $support = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation", inversedBy="retraitPapiers")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Organisme|null
     */
    public function getOrganisme(): ?Organisme
    {
        return $this->organisme;
    }

    /**
     * @param Organisme|null $organisme
     * @return RetraitPapier
     */
    public function setOrganisme(?Organisme $organisme): RetraitPapier
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getConsultationRef(): int|string
    {
        return $this->consultationRef;
    }

    /**
     * @param int|string $consultationRef
     * @return RetraitPapier
     */
    public function setConsultationRef(int|string $consultationRef): RetraitPapier
    {
        $this->consultationRef = $consultationRef;

        return $this;
    }

    /**
     * @return string
     */
    public function getNom(): string
    {
        return $this->nom;
    }

    /**
     * @param string $nom
     * @return RetraitPapier
     */
    public function setNom(string $nom): RetraitPapier
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdresse(): string
    {
        return $this->adresse;
    }

    /**
     * @param string $adresse
     * @return RetraitPapier
     */
    public function setAdresse(string $adresse): RetraitPapier
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * @return string
     */
    public function getEntreprise(): string
    {
        return $this->entreprise;
    }

    /**
     * @param string $entreprise
     * @return RetraitPapier
     */
    public function setEntreprise(string $entreprise): RetraitPapier
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * @return DateTime|string
     */
    public function getDateTelechargement(): DateTime|string
    {
        return $this->dateTelechargement;
    }

    /**
     * @param DateTime|string $dateTelechargement
     * @return RetraitPapier
     */
    public function setDateTelechargement(DateTime|string $dateTelechargement): RetraitPapier
    {
        $this->dateTelechargement = $dateTelechargement;

        return $this;
    }

    /**
     * @return string
     */
    public function getCodepostal(): string
    {
        return $this->codepostal;
    }

    /**
     * @param string $codepostal
     * @return RetraitPapier
     */
    public function setCodepostal(string $codepostal): RetraitPapier
    {
        $this->codepostal = $codepostal;

        return $this;
    }

    /**
     * @return string
     */
    public function getVille(): string
    {
        return $this->ville;
    }

    /**
     * @param string $ville
     * @return RetraitPapier
     */
    public function setVille(string $ville): RetraitPapier
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * @return string
     */
    public function getPays(): string
    {
        return $this->pays;
    }

    /**
     * @param string $pays
     * @return RetraitPapier
     */
    public function setPays(string $pays): RetraitPapier
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return RetraitPapier
     */
    public function setTelephone(string $telephone): RetraitPapier
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * @return int|string
     */
    public function getTiragePlan(): int|string
    {
        return $this->tiragePlan;
    }

    /**
     * @param int|string $tiragePlan
     * @return RetraitPapier
     */
    public function setTiragePlan(int|string $tiragePlan): RetraitPapier
    {
        $this->tiragePlan = $tiragePlan;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSiret(): ?string
    {
        return $this->siret;
    }

    /**
     * @param string|null $siret
     * @return RetraitPapier
     */
    public function setSiret(?string $siret): RetraitPapier
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * @return string
     */
    public function getFax(): string
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     * @return RetraitPapier
     */
    public function setFax(string $fax): RetraitPapier
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return RetraitPapier
     */
    public function setEmail(string $email): RetraitPapier
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getObservation(): ?string
    {
        return $this->observation;
    }

    /**
     * @param string|null $observation
     * @return RetraitPapier
     */
    public function setObservation(?string $observation): RetraitPapier
    {
        $this->observation = $observation;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    /**
     * @param string|null $prenom
     * @return RetraitPapier
     */
    public function setPrenom(?string $prenom): RetraitPapier
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAdresse2(): ?string
    {
        return $this->adresse2;
    }

    /**
     * @param string|null $adresse2
     * @return RetraitPapier
     */
    public function setAdresse2(?string $adresse2): RetraitPapier
    {
        $this->adresse2 = $adresse2;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifiantNational(): ?string
    {
        return $this->identifiantNational;
    }

    /**
     * @param string|null $identifiantNational
     * @return RetraitPapier
     */
    public function setIdentifiantNational(?string $identifiantNational): RetraitPapier
    {
        $this->identifiantNational = $identifiantNational;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAcronymePays(): ?string
    {
        return $this->acronymePays;
    }

    /**
     * @param string|null $acronymePays
     * @return RetraitPapier
     */
    public function setAcronymePays(?string $acronymePays): RetraitPapier
    {
        $this->acronymePays = $acronymePays;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getSupport(): ?int
    {
        return $this->support;
    }

    /**
     * @param int|null $support
     * @return RetraitPapier
     */
    public function setSupport(?int $support): RetraitPapier
    {
        $this->support = $support;

        return $this;
    }

    /**
     * @return Consultation|null
     */
    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    /**
     * @param Consultation|null $consultation
     * @return RetraitPapier
     */
    public function setConsultation(?Consultation $consultation): RetraitPapier
    {
        $this->consultation = $consultation;

        return $this;
    }
}
