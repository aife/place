<?php

namespace App\Entity;

use App\Repository\HabilitationAdministrateurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HabilitationAdministrateurRepository::class)
 */
class HabilitationAdministrateur
{
    public const ACCESS_ADMINISTRATION_PANELS = 'access_administration_panels';
    public const ACCESS_INTERFACE_SUIVI_WRITE = 'access_interface_suivi_write';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=150)
     */
    private string $habilitation;

    /**
     * @ORM\ManyToMany(targetEntity=Administrateur::class, inversedBy="habilitationAdministrateurs")
     */
    private Collection $administrateurs;

    public function __construct()
    {
        $this->administrateurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHabilitation(): ?string
    {
        return $this->habilitation;
    }

    public function setHabilitation(string $habilitation): self
    {
        $this->habilitation = $habilitation;

        return $this;
    }

    /**
     * @return Collection<int, Administrateur>
     */
    public function getAdministrateurs(): Collection
    {
        return $this->administrateurs;
    }

    public function addAdministrateur(Administrateur $administrateur): self
    {
        if (!$this->administrateurs->contains($administrateur)) {
            $this->administrateurs[] = $administrateur;
        }

        return $this;
    }

    public function removeAdministrateur(Administrateur $administrateur): self
    {
        $this->administrateurs->removeElement($administrateur);

        return $this;
    }
}
