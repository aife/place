<?php

/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace App\Entity;

use DateTimeInterface;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * EnchereOffre
 *
 * @ORM\Table(name="EnchereOffre", indexes={
 *     @ORM\Index(name="FK_1F7C883C2868ECFD", columns={"idEnchere"}),
 *     @ORM\Index(name="FK_1F7C883C84A84093", columns={"idEnchereEntreprise"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\EnchereOffreRepository")
 */
class EnchereOffre
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=false)
     */
    private string $organisme;

    /**
     * @var DateTime
     * @ORM\Column(name="date", type="datetime", nullable=true, options={"default"="0000-00-00 00:00:00"})
     */
    private $date = '0000-00-00 00:00:00';

    /**
     * @var float|null
     *
     * @ORM\Column(name="valeurTIC", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $valeurtic = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valeurTC", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $valeurtc = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valeurNETC", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $valeurnetc = null;

    /**
     * @var float|null
     *
     * @ORM\Column(name="valeurNGC", type="float", precision=10, scale=0, nullable=true)
     */
    private ?float $valeurNGC = null;

    /**
     * @ORM\Column(name="rang", type="integer", nullable= true)
     */
    private ?int $rang = null;

    /**
     * @var Enchereentreprisepmi|null
     *
     * @ORM\ManyToOne(targetEntity="Enchereentreprisepmi")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEnchereEntreprise", referencedColumnName="id")
     * })
     */
    private ?Enchereentreprisepmi $idEnchereEntreprise = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Encherepmi")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEnchere", referencedColumnName="id")
     * })
     */
    private ?Encherepmi $idEnchere = null;


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getIdEnchere()
    {
        return $this->idEnchere;
    }

    public function setIdEnchere($idEnchere): EnchereOffre
    {
        $this->idEnchere = $idEnchere;

        return $this;
    }

    public function getIdenchereentreprise(): ?Enchereentreprisepmi
    {
        return $this->idEnchereEntreprise;
    }

    public function setIdenchereentreprise(?Enchereentreprisepmi $idEnchereEntreprise): self
    {
        $this->idEnchereEntreprise = $idEnchereEntreprise;

        return $this;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function setDate(?DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getValeurTIC(): ?float
    {
        return $this->valeurtic;
    }

    public function setValeurTIC(?float $valeurTIC): self
    {
        $this->valeurtic = $valeurTIC;

        return $this;
    }

    public function getValeurTC(): ?float
    {
        return $this->valeurtc;
    }

    /**
     * @param float|null $valeurTC
     */
    public function setValeurTC(float $valeurTC = null): self
    {
        $this->valeurtc = $valeurTC;

        return $this;
    }

    public function getValeurNETC(): ?float
    {
        return $this->valeurnetc;
    }

    public function setValeurNETC(?float $valeurNETC): self
    {
        $this->valeurnetc = $valeurNETC;

        return $this;
    }

    public function getValeurNGC(): ?float
    {
        return $this->valeurNGC;
    }

    public function setValeurNGC(?float $valeurNGC): self
    {
        $this->valeurNGC = $valeurNGC;

        return $this;
    }

    public function getRang(): ?int
    {
        return $this->rang;
    }

    public function setRang(?int $rang): self
    {
        $this->rang = $rang;

        return $this;
    }
}
