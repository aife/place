<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * TDumeNumero.
 *
 * @ORM\Table(name="t_dume_numero")
 * @ORM\Entity(repositoryClass="App\Repository\TDumeNumeroRepository")
 */
class TDumeNumero
{
    /**
     *
     * @ORM\Column(name="id", type="integer", unique=true, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="numero_dume_national", type="string", length=100)
     */
    private ?string $numeroDumeNational = null;

    /**
     * @var string
     *
     * @ORM\Column(name="blob_id", type="integer")
     */
    private $blobId;

    /**
     * @ORM\Column(name="list_lot", type="string", length=255)
     */
    private ?string $listLot = null;

    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\TDumeContexte", inversedBy="dumeNumeros")
     * @ORM\JoinColumn(name="id_dume_contexte", referencedColumnName="id")
     */
    private $idDumeContexte;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_recuperation_pdf", type="datetime")*/
    private $dateRecuperationPdf;

    /**
     * @ORM\Column(name="blob_id_xml", type="integer")
     */
    private ?int $blobIdXml = null;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_recuperation_xml", type="datetime")*/
    private $dateRecuperationXml;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumeroDumeNational()
    {
        return $this->numeroDumeNational;
    }

    /**
     * @param string $numeroDumeNational
     */
    public function setNumeroDumeNational($numeroDumeNational)
    {
        $this->numeroDumeNational = $numeroDumeNational;
    }

    /**
     * @return string
     */
    public function getBlobId()
    {
        return $this->blobId;
    }

    /**
     * @param string $blobId
     */
    public function setBlobId($blobId)
    {
        $this->blobId = $blobId;
    }

    /**
     * @return string
     */
    public function getListLot()
    {
        return $this->listLot;
    }

    /**
     * @param string $listLot
     */
    public function setListLot($listLot)
    {
        $this->listLot = $listLot;
    }

    /**
     * @return TDumeContexte
     */
    public function getIdDumeContexte()
    {
        return $this->idDumeContexte;
    }

    /**
     * @param TDumeContexte $idDumeContexte
     */
    public function setIdDumeContexte($idDumeContexte)
    {
        $this->idDumeContexte = $idDumeContexte;
    }

    /**
     * @return DateTime
     */
    public function getDateRecuperationPdf()
    {
        return $this->dateRecuperationPdf;
    }

    /**
     * @param DateTime $dateRecuperationPdf
     */
    public function setDateRecuperationPdf($dateRecuperationPdf)
    {
        $this->dateRecuperationPdf = $dateRecuperationPdf;
    }

    /**
     * @return int
     */
    public function getBlobIdXml()
    {
        return $this->blobIdXml;
    }

    /**
     * @param int $blobIdXml
     */
    public function setBlobIdXml($blobIdXml)
    {
        $this->blobIdXml = $blobIdXml;
    }

    /**
     * @return DateTime
     */
    public function getDateRecuperationXml()
    {
        return $this->dateRecuperationXml;
    }

    /**
     * @param DateTime $dateRecuperationXml
     */
    public function setDateRecuperationXml($dateRecuperationXml)
    {
        $this->dateRecuperationXml = $dateRecuperationXml;
    }
}
