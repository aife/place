<?php

namespace App\Entity;

use App\Repository\DecisionLotRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DecisionLotRepository::class)
 * @ORM\Table(name="DecisionLot")
 */
class DecisionLot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $idDecisionLot;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $organisme;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $consultationRef;

    /**
     * @ORM\Column(type="integer")
     */
    private $lot;

    /**
     * @ORM\ManyToOne(targetEntity=TypeDecision::class)
     * @ORM\JoinColumn(name="id_type_decision", referencedColumnName="id_type_decision", nullable=false)
     */
    private $idTypeDecision;

    /**
     * @ORM\Column(name="autre_a_preciser", type="datetime", nullable=true)
     */
    private $autreAPreciser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $dateMaj;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateDecision;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $consultation;

    public function getIdDecisionLot(): ?int
    {
        return $this->idDecisionLot;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): self
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getConsultationRef(): ?int
    {
        return $this->consultationRef;
    }

    public function setConsultationRef(?int $consultationRef): self
    {
        $this->consultationRef = $consultationRef;

        return $this;
    }

    public function getLot(): ?int
    {
        return $this->lot;
    }

    public function setLot(int $lot): self
    {
        $this->lot = $lot;

        return $this;
    }

    public function getIdTypeDecision(): ?TypeDecision
    {
        return $this->idTypeDecision;
    }

    public function setIdTypeDecision(?TypeDecision $idTypeDecision): self
    {
        $this->idTypeDecision = $idTypeDecision;

        return $this;
    }

    public function getAutreAPreciser(): ?\DateTimeInterface
    {
        return $this->autreAPreciser;
    }

    public function setAutreAPreciser(?\DateTimeInterface $autreAPreciser): self
    {
        $this->autreAPreciser = $autreAPreciser;

        return $this;
    }

    public function getCommentaire(): ?string
    {
        return $this->commentaire;
    }

    public function setCommentaire(?string $commentaire): self
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    public function getDateMaj(): ?string
    {
        return $this->dateMaj;
    }

    public function setDateMaj(?string $dateMaj): self
    {
        $this->dateMaj = $dateMaj;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getDateDecision(): ?\DateTimeInterface
    {
        return $this->dateDecision;
    }

    public function setDateDecision(?\DateTimeInterface $dateDecision): self
    {
        $this->dateDecision = $dateDecision;

        return $this;
    }
}
