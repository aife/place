<?php

namespace App\Entity;

use Stringable;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="DCE")
 * @ORM\Entity(repositoryClass="App\Repository\DCERepository")
 */
class DCE implements Stringable
{
    /**
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="organisme", type="string")
     */
    private ?string $organisme = null;

    /**
     * @ORM\Column(name="consultation_id", type="integer")
     */
    private ?int $consultationId = null;

    /**
     * @ORM\Column(name="dce", type="integer")
     */
    private ?int $dce = null;

    /**
     * @ORM\Column(name="nom_dce", type="string")
     */
    private ?string $nomDce = null;

    /**
     * @ORM\Column(name="statut", type="string")
     */
    private ?string $status = null;

    /**
     * @ORM\Column(name="nom_fichier", type="string")
     */
    private ?string $nomFichier = null;

    /**
     * @ORM\Column(name="ancien_fichier", type="string")
     */
    private string $ancienFichier = '';

    /**
     * @ORM\Column(name="horodatage", type="blob")
     */
    private $horodatage = null;

    /**
     * @var DateTime
     * @ORM\Column(name="untrusteddate", type="datetime")
     */
    private $untrusteddate;

    /**
     * @ORM\Column(name="agent_id", type="integer")
     */
    private ?int $agentId = null;

    /**
     * @ORM\Column(name="taille_dce", type="string")
     */
    private ?string $tailleDce = null;

    /**
     * @ORM\ManyToOne(targetEntity=Consultation::class, inversedBy="dce")
     * @ORM\JoinColumn(name="consultation_id", referencedColumnName="id")
     */
    private ?Consultation $consultation = null;

    /**
     * @ORM\OneToOne(targetEntity=HistoriquePurge::class, mappedBy="dce")
     */
    private ?HistoriquePurge $historiquePurge = null;

    public function __toString(): string
    {
        return $this->getNomDce();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getOrganisme(): string
    {
        return $this->organisme;
    }

    public function setOrganisme(string $organisme): DCE
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getConsultationId(): int
    {
        return $this->consultationId;
    }

    public function setConsultationId(int $consultationId): DCE
    {
        $this->consultationId = $consultationId;

        return $this;
    }

    public function getDce(): int
    {
        return $this->dce;
    }

    public function setDce(int $dce): DCE
    {
        $this->dce = $dce;

        return $this;
    }

    public function getNomDce(): string
    {
        return $this->nomDce;
    }

    public function setNomDce(string $nomDce): DCE
    {
        $this->nomDce = $nomDce;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): DCE
    {
        $this->status = $status;

        return $this;
    }

    public function getNomFichier(): string
    {
        return $this->nomFichier;
    }

    public function setNomFichier(string $nomFichier): DCE
    {
        $this->nomFichier = $nomFichier;

        return $this;
    }

    public function getAncienFichier(): string
    {
        return $this->ancienFichier;
    }

    public function setAncienFichier(string $ancienFichier): DCE
    {
        $this->ancienFichier = $ancienFichier;

        return $this;
    }

    public function getHorodatage(): string
    {
        return $this->horodatage;
    }

    public function setHorodatage(string $horodatage): DCE
    {
        $this->horodatage = $horodatage;

        return $this;
    }

    public function getUntrusteddate(): DateTime
    {
        return $this->untrusteddate;
    }

    public function setUntrusteddate(DateTime $untrusteddate): DCE
    {
        $this->untrusteddate = $untrusteddate;

        return $this;
    }

    public function getAgentId(): int
    {
        return $this->agentId;
    }

    public function setAgentId(int $agentId): DCE
    {
        $this->agentId = $agentId;

        return $this;
    }

    public function getTailleDce(): string
    {
        return $this->tailleDce;
    }

    public function setTailleDce(string $tailleDce): DCE
    {
        $this->tailleDce = $tailleDce;

        return $this;
    }

    public function getConsultation(): ?Consultation
    {
        return $this->consultation;
    }

    public function setConsultation(?Consultation $consultation): self
    {
        $this->consultation = $consultation;

        return $this;
    }

    public function getHistoriquePurge(): ?HistoriquePurge
    {
        return $this->historiquePurge;
    }

    public function setHistoriquePurge(?HistoriquePurge $historiquePurge): self
    {
        $this->historiquePurge = $historiquePurge;

        return $this;
    }
}
