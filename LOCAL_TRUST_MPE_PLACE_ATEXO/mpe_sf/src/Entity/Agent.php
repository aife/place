<?php

namespace App\Entity;

use App\Controller\ApiPlatformCustom\PerimeterVisionController;
use DateTime;
use Exception;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Dto\Input\AgentInput;
use App\Dto\Output\AgentOutput;
use App\Entity\Agent\AgentServiceMetier;
use App\Entity\Consultation\ConsultationFavoris;
use App\Entity\DossierVolumineux\DossierVolumineux;
use App\Entity\WS\WebService;
use App\Filter\AgentHabilitationFilter;
use App\Filter\AgentPerimeterVisionSearchFilter;
use App\Serializer\AbstractApiPlatformDisabledNormalizer;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Agent.
 *
 * @ORM\Table(name="Agent", uniqueConstraints={@ORM\UniqueConstraint(name="login",
 *     columns={"login"})},
 *     indexes={@ORM\Index(name="organisme", columns={"organisme"})})
 * @ORM\Entity(repositoryClass="App\Repository\AgentRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=false)
 * @method string getUserIdentifier()
 */
#[ApiResource(
    collectionOperations: [
        "get",
        "post" => ["security_post_denormalize" => "is_granted('AGENT_EDIT', object)"]
    ],
    itemOperations: [
        "get",
        "get_perimeter_vision" => [
            'method'        => 'GET',
            'path'          => '/agents/{id}/perimeter-vision',
            'controller'    => PerimeterVisionController::class
        ],
        "put" => ["security" => "is_granted('AGENT_EDIT', object)"],
        "delete" => ["security" => "is_granted('AGENT_EDIT', object)"],
        "patch" => ["security" => "is_granted('AGENT_EDIT', object)"],
    ],
    denormalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    input: AgentInput::class,
    normalizationContext: [AbstractApiPlatformDisabledNormalizer::CALLED_BY_API_PLATFORM => true],
    output: AgentOutput::class,
)]
#[ApiFilter(
    SearchFilter::class,
    properties: [
        'actif' => 'exact',
        'login' => 'exact',
        'nom' => 'partial',
        'prenom' => 'partial'
    ]
)]
#[ApiFilter(AgentHabilitationFilter::class)]
#[ApiFilter(AgentPerimeterVisionSearchFilter::class)]
#[UniqueEntity('login')]
class Agent extends Rgpd implements UserInterface, EquatableInterface, \Stringable
{
    use SoftDeleteableEntity;

    public const FOREIGN_KEYS = [
        'organisme' => [
            'class' => Organisme::class,
            'property' => 'acronyme',
            'isObject' => true,
            'migrationService' => 'atexo.service.migration.organisme'
        ],
        'service' => [
            'class' => Service::class,
            'isObject' => true,
            'property' => 'id',
            'migrationService' => 'atexo.service.migration.service'
        ],
    ];

    public const ARGON_TYPE = 'ARGON2', ACTIVE = 1;

    /**
     * @var EntityManager
     */
    private EntityManager $em;

    private array $habilitations = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisme", inversedBy="agents")
     * @ORM\JoinColumn(name="organisme", referencedColumnName="acronyme")
     */
    private ?Organisme $organisme = null;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Service", inversedBy="agents")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private Service | array | null $service = null;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\ModificationContrat", mappedBy="idAgent")
     */
    private Collection $modificationContrats;

    /**
     * @var HabilitationAgent
     * @ORM\OneToOne(targetEntity="App\Entity\HabilitationAgent",
     *     mappedBy="agent", cascade={"persist"})
     */
    private $habilitation;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\SocleHabilitationAgent",
     *     mappedBy="agent")
     */
    private ?SocleHabilitationAgent $socleHabilitations = null;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")*/
    #[Groups(['webservice', 'ws-habilitation'])]
    private $id;

    /**
     * @var string
     * @ORM\Column(name="login", type="string", length=255, nullable=true)
     */
    private $login;

    /**
     * @ORM\Column(name="email", type="string", length=100, nullable=false)*/
    #[Assert\Email(message: "The email '{{ value }}' is not a valid email.", mode: 'strict')]
    #[Groups('webservice')]
    private string $email = '';

    /**
     * @ORM\Column(name="mdp", type="string", length=255, nullable=false)
     */
    private string $mdp = '';

    /**
     * @ORM\Column(name="certificat", type="text", length=65535, nullable=true)
     */
    private ?string $certificat = null;

    /**
     * @ORM\Column(name="nom", type="string", length=100, nullable=false)*/
    #[Groups('webservice')]
    private string $nom = '';

    /**
     * @ORM\Column(name="prenom", type="string", length=100, nullable=false)*/
    #[Groups('webservice')]
    private string $prenom = '';

    /**
     * @ORM\Column(name="tentatives_mdp", type="integer", nullable=false)
     */
    private string|int $tentativesMdp = '0';

    /**
     * @ORM\Column(name="organisme", type="string", length=30, nullable=true)*/
    #[Groups('webservice')]
    private ?string $acronymeOrganisme = '';

    /**
     * @ORM\Column(name="service_id", type="integer", nullable=true)
     */
    private ?int $serviceId = null;

    /**
     * @ORM\Column(name="RECEVOIR_MAIL", type="string", nullable=false)
     */
    private string $recevoirMail = '0';

    /**
     * @ORM\Column(name="elu", type="string", length=1, nullable=false)
     */
    private string $elu = '0';

    /**
     * @ORM\Column(name="nom_fonction", type="string", length=100, nullable=false)
     */
    private string $nomFonction = '';

    /**
     * @ORM\Column(name="num_tel", type="string", length=20, nullable=true)
     */
    private ?string $numTel = null;

    /**
     * @ORM\Column(name="num_fax", type="string", length=20, nullable=true)
     */
    private ?string $numFax = null;

    /**
     * @ORM\Column(name="type_comm", type="string", length=1, nullable=false)
     */
    private string $typeComm = '2';

    /**
     * @ORM\Column(name="adr_postale", type="string", length=255, nullable=false)
     */
    private string $adrPostale = '';

    /**
     * @ORM\Column(name="civilite", type="string", length=255, nullable=false)
     */
    private string $civilite = '';

    /**
     * @ORM\Column(name="alerte_reponse_electronique", type="string", nullable=false)
     */
    private string $alerteReponseElectronique = '0';

    /**
     * @ORM\Column(name="alerte_cloture_consultation", type="string", nullable=false)
     */
    private string $alerteClotureConsultation = '0';

    /**
     * @ORM\Column(name="alerte_reception_message", type="string", nullable=false)
     */
    private string $alerteReceptionMessage = '0';

    /**
     * @ORM\Column(name="alerte_publication_boamp", type="string", nullable=false)
     */
    private string $alertePublicationBoamp = '0';

    /**
     * @ORM\Column(name="alerte_echec_publication_boamp", type="string", nullable=false)
     */
    private string $alerteEchecPublicationBoamp = '0';

    /**
     * @ORM\Column(name="alerte_creation_modification_agent", type="string", nullable=false)
     */
    private string $alerteCreationModificationAgent = '0';

    /**
     * @ORM\Column(name="date_creation", type="string", length=20, nullable=true)*/
    #[Groups('webservice')]
    private ?string $dateCreation = null;

    /**
     * @ORM\Column(name="date_modification", type="string", length=20, nullable=true)*/
    #[Groups('webservice')]
    private ?string $dateModification = null;

    /**
     *
     * @ORM\Column(name="id_externe", type="string", nullable=false)*/
    #[Groups('webservice')]
    private string|int $idExterne = '0';

    /**
     * @ORM\Column(name="id_profil_socle_externe", type="integer", nullable=false)
     */
    private int $idProfilSocleExterne = 0;

    /**
     * @ORM\Column(name="lieu_execution", type="text", length=65535, nullable=true)
     */
    private ?string $lieuExecution = null;

    /**
     * @ORM\Column(name="alerte_question_entreprise", type="string", nullable=false)
     */
    private string $alerteQuestionEntreprise = '0';

    /**
     * @ORM\Column(name="actif", type="string", nullable=false)*/
    #[Groups('webservice')]
    private string $actif = '1';

    /**
     * @ORM\Column(name="codes_nuts", type="text", length=65535, nullable=true)
     */
    private ?string $codesNuts = null;

    /**
     * @ORM\Column(name="num_certificat", type="string", length=64, nullable=true)
     */
    private ?string $numCertificat = null;

    /**
     * @ORM\Column(name="alerte_validation_consultation", type="string", nullable=false)
     */
    private string $alerteValidationConsultation = '0';

    /**
     * @ORM\Column(name="alerte_chorus", type="string", nullable=false)
     */
    private string $alerteChorus = '0';

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private ?string $password = '';

    /**
     * @ORM\Column(name="code_theme", type="string", length=255, nullable=true)
     */
    private ?string $codeTheme = '0';

    /**
     * @var string
     * @ORM\Column(name="date_connexion", type="datetime", nullable=true)
     */
    private $dateConnexion;

    /**
     * @var bool
     *
     * @ORM\Column(name="technique", type="boolean", nullable=false)
     */
    private $technique = false;

    /**
     * Many Users have Many webservices.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\WS\WebService", inversedBy="agents")
     * @ORM\JoinTable(name="habilitation_agent_ws")
     */
    private Collection $webservices;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\DossierVolumineux\DossierVolumineux", mappedBy="agent")
     */
    private Collection $dossierVolumineux;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity="App\Entity\Consultation\ConsultationFavoris", mappedBy="agent")
     */
    private Collection $consultationFavoris;

    /**
     * @ORM\Column(name="type_hash", type="string", length=255, nullable=true)
     */
    private ?string $typeHash = null;

    /**
     * @ORM\Column (type="string", nullable=false)
     */
    private string $alerteMesConsultations = '0';

    /**
     * @ORM\Column (type="string", nullable=false)
     */
    private string $alerteConsultationsMonEntite = '0';

    /**
     * @ORM\Column (type="string", nullable=false)
     */
    private string $alerteConsultationsDesEntitesDependantes = '0';

    /**
     * @ORM\Column (type="string", nullable=false)
     */
    private string $alerteConsultationsMesEntitesTransverses = '0';

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Offre\AnnexeFinanciere", mappedBy="agent")
     * @ORM\JoinColumn(referencedColumnName="id_agent")
     */
    private Collection $annexesFinanciereManuel;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="agentCreateur")
     */
    private Collection $consultations;

    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=Consultation::class, mappedBy="agentTechniqueCreateur")
     */
    private Collection $referenceConsultationAgentTechniqueCreateur;

    #[ORM\OneToMany(mappedBy: 'agent', targetEntity: HabilitationTypeProcedure::class)]
    /**
     * @var Collection
     * @ORM\OneToMany(targetEntity=HabilitationTypeProcedure::class, mappedBy="agent")
     */
    private Collection $habilitationTypeProcedures;

    /**
     * @ORM\Column(name="alerte_consultations_invite_ponctuel", type="string", length=1, nullable=false)
     */
    private string $alerteConsultationsInvitePonctuel = '0';

    /**
     * Agent constructor.
     */
    public function __construct()
    {
        $this->modificationContrats = new ArrayCollection();
        $this->webservices = new ArrayCollection();
        $this->dossierVolumineux = new ArrayCollection();
        $this->consultationFavoris = new ArrayCollection();
        $this->annexesFinanciereManuel = new ArrayCollection();
        $this->consultations = new ArrayCollection();
        $this->referenceConsultationAgentTechniqueCreateur = new ArrayCollection();
        $this->habilitationTypeProcedures = new ArrayCollection();
    }

    /**
     * Set id.
     *
     * @param int $id
     *
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set login.
     *
     * @param string $login
     * @return Agent
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set email.
     *
     * @param string $email
     * @return Agent
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mdp.
     *
     * @param string $mdp
     * @return Agent
     */
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get mdp.
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set certificat.
     *
     * @param string $certificat
     * @return Agent
     */
    public function setCertificat($certificat)
    {
        $this->certificat = $certificat;

        return $this;
    }

    /**
     * Get certificat.
     *
     * @return string
     */
    public function getCertificat()
    {
        return $this->certificat;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     * @return Agent
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     * @return Agent
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set tentativesMdp.
     *
     * @param int $tentativesMdp
     * @return Agent
     */
    public function setTentativesMdp($tentativesMdp)
    {
        $this->tentativesMdp = $tentativesMdp;

        return $this;
    }

    /**
     * Get tentativesMdp.
     *
     * @return int
     */
    public function getTentativesMdp()
    {
        return $this->tentativesMdp;
    }

    /**
     * Set acronymeOrganisme.
     *
     * @param string $acronymeOrganisme
     * @return Agent
     */
    public function setAcronymeOrganisme($acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;

        return $this;
    }

    /**
     * Get acronymeOrganisme.
     *
     * @return string
     */
    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }

    /**
     * Set serviceId.
     *
     * @param int $serviceId
     * @return Agent
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * Get serviceId.
     *
     * @return int|null
     */
    public function getServiceId(): ?int
    {
        return $this->serviceId;
    }

    /**
     * Set recevoirMail.
     *
     * @param string $recevoirMail
     * @return Agent
     */
    public function setRecevoirMail($recevoirMail)
    {
        $this->recevoirMail = $recevoirMail;

        return $this;
    }

    /**
     * Get recevoirMail.
     *
     * @return string
     */
    public function getRecevoirMail()
    {
        return $this->recevoirMail;
    }

    /**
     * Set elu.
     *
     * @param string $elu
     * @return Agent
     */
    public function setElu($elu)
    {
        $this->elu = $elu;

        return $this;
    }

    /**
     * Get elu.
     *
     * @return string
     */
    public function getElu()
    {
        return $this->elu;
    }

    /**
     * Set nomFonction.
     *
     * @param string $nomFonction
     * @return Agent
     */
    public function setNomFonction($nomFonction)
    {
        $this->nomFonction = $nomFonction;

        return $this;
    }

    /**
     * Get nomFonction.
     *
     * @return string
     */
    public function getNomFonction()
    {
        return $this->nomFonction;
    }

    /**
     * Set numTel.
     *
     * @param string $numTel
     * @return Agent
     */
    public function setNumTel($numTel)
    {
        $this->numTel = $numTel;

        return $this;
    }

    /**
     * Get numTel.
     *
     * @return string
     */
    public function getNumTel()
    {
        return $this->numTel;
    }

    /**
     * Set numTel.
     *
     * @param string $numTel*/
    #[Groups('webservice')]
    public function setTelephone($numTel)
    {
        $this->numTel = $numTel;
    }

    /**
     * Get numTel.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getTelephone()
    {
        return $this->numTel;
    }

    /**
     * Set numFax.
     *
     * @param string $numFax
     * @return Agent
     */
    public function setNumFax($numFax)
    {
        $this->numFax = $numFax;

        return $this;
    }

    /**
     * Get numFax.
     *
     * @return string
     */
    public function getNumFax()
    {
        return $this->numFax;
    }

    /**
     * Set numFax.
     *
     * @param string $numFax*/
    #[Groups('webservice')]
    public function setFax($numFax)
    {
        $this->numFax = $numFax;
    }

    /**
     * Get numFax.
     *
     * @return string*/
    #[Groups('webservice')]
    public function getFax()
    {
        return $this->numFax;
    }

    /**
     * Set typeComm.
     *
     * @param string $typeComm
     * @return Agent
     */
    public function setTypeComm($typeComm)
    {
        $this->typeComm = $typeComm;

        return $this;
    }

    /**
     * Get typeComm.
     *
     * @return string
     */
    public function getTypeComm()
    {
        return $this->typeComm;
    }

    /**
     * Set adrPostale.
     *
     * @param string $adrPostale
     * @return Agent
     */
    public function setAdrPostale($adrPostale)
    {
        $this->adrPostale = $adrPostale;

        return $this;
    }

    /**
     * Get adrPostale.
     *
     * @return string
     */
    public function getAdrPostale()
    {
        return $this->adrPostale;
    }

    /**
     * Set civilite.
     *
     * @param string $civilite
     * @return Agent
     */
    public function setCivilite($civilite)
    {
        $this->civilite = $civilite;

        return $this;
    }

    /**
     * Get civilite.
     *
     * @return string
     */
    public function getCivilite()
    {
        return $this->civilite;
    }

    /**
     * Set alerteReponseElectronique.
     *
     * @param string $alerteReponseElectronique
     * @return Agent
     */
    public function setAlerteReponseElectronique($alerteReponseElectronique)
    {
        $this->alerteReponseElectronique = $alerteReponseElectronique;

        return $this;
    }

    /**
     * Get alerteReponseElectronique.
     *
     * @return string
     */
    public function getAlerteReponseElectronique()
    {
        return $this->alerteReponseElectronique;
    }

    /**
     * Set alerteClotureConsultation.
     *
     * @param string $alerteClotureConsultation
     * @return Agent
     */
    public function setAlerteClotureConsultation($alerteClotureConsultation)
    {
        $this->alerteClotureConsultation = $alerteClotureConsultation;

        return $this;
    }

    /**
     * Get alerteClotureConsultation.
     *
     * @return string
     */
    public function getAlerteClotureConsultation()
    {
        return $this->alerteClotureConsultation;
    }

    /**
     * Set alerteReceptionMessage.
     *
     * @param string $alerteReceptionMessage
     * @return Agent
     */
    public function setAlerteReceptionMessage($alerteReceptionMessage)
    {
        $this->alerteReceptionMessage = $alerteReceptionMessage;

        return $this;
    }

    /**
     * Get alerteReceptionMessage.
     *
     * @return string
     */
    public function getAlerteReceptionMessage()
    {
        return $this->alerteReceptionMessage;
    }

    /**
     * Set alertePublicationBoamp.
     *
     * @param string $alertePublicationBoamp
     * @return Agent
     */
    public function setAlertePublicationBoamp($alertePublicationBoamp)
    {
        $this->alertePublicationBoamp = $alertePublicationBoamp;

        return $this;
    }

    /**
     * Get alertePublicationBoamp.
     *
     * @return string
     */
    public function getAlertePublicationBoamp()
    {
        return $this->alertePublicationBoamp;
    }

    /**
     * Set alerteEchecPublicationBoamp.
     *
     * @param string $alerteEchecPublicationBoamp
     * @return Agent
     */
    public function setAlerteEchecPublicationBoamp($alerteEchecPublicationBoamp)
    {
        $this->alerteEchecPublicationBoamp = $alerteEchecPublicationBoamp;

        return $this;
    }

    /**
     * Get alerteEchecPublicationBoamp.
     *
     * @return string
     */
    public function getAlerteEchecPublicationBoamp()
    {
        return $this->alerteEchecPublicationBoamp;
    }

    /**
     * Set alerteCreationModificationAgent.
     *
     * @param string $alerteCreationModificationAgent
     * @return Agent
     */
    public function setAlerteCreationModificationAgent($alerteCreationModificationAgent)
    {
        $this->alerteCreationModificationAgent = $alerteCreationModificationAgent;

        return $this;
    }

    /**
     * Get alerteCreationModificationAgent.
     *
     * @return string
     */
    public function getAlerteCreationModificationAgent()
    {
        return $this->alerteCreationModificationAgent;
    }

    /**
     * Set dateCreation.
     *
     * @param string $dateCreation
     * @return Agent
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation.
     *
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification.
     *
     * @param string $dateModification
     * @return Agent
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification.
     *
     * @return string
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Set idExterne.
     *
     * @param int $idExterne
     * @return Agent
     */
    public function setIdExterne($idExterne)
    {
        $this->idExterne = $idExterne;

        return $this;
    }

    /**
     * Get idExterne.
     *
     * @return int
     */
    public function getIdExterne()
    {
        return $this->idExterne;
    }

    /**
     * Set idProfilSocleExterne.
     *
     * @param int $idProfilSocleExterne
     * @return Agent
     */
    public function setIdProfilSocleExterne($idProfilSocleExterne)
    {
        $this->idProfilSocleExterne = $idProfilSocleExterne;

        return $this;
    }

    /**
     * Get idProfilSocleExterne.
     *
     * @return int
     */
    public function getIdProfilSocleExterne()
    {
        return $this->idProfilSocleExterne;
    }

    /**
     * Get idProfilSocleExterne.
     *
     * @return int*/
    #[Groups('webservice')]
    public function getIdProfil()
    {
        return $this->idProfilSocleExterne;
    }

    /**
     * Set idProfilSocleExterne.
     *
     * @param int $idProfilSocleExterne*/
    #[Groups('webservice')]
    public function setIdProfil($idProfilSocleExterne)
    {
        $this->idProfilSocleExterne = $idProfilSocleExterne;
    }

    /**
     * Set lieuExecution.
     *
     * @param string $lieuExecution
     * @return Agent
     */
    public function setLieuExecution($lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;

        return $this;
    }

    /**
     * Get lieuExecution.
     *
     * @return string
     */
    public function getLieuExecution()
    {
        return $this->lieuExecution;
    }

    /**
     * Set alerteQuestionEntreprise.
     *
     * @param string $alerteQuestionEntreprise
     * @return Agent
     */
    public function setAlerteQuestionEntreprise($alerteQuestionEntreprise)
    {
        $this->alerteQuestionEntreprise = $alerteQuestionEntreprise;

        return $this;
    }

    /**
     * Get alerteQuestionEntreprise.
     *
     * @return string
     */
    public function getAlerteQuestionEntreprise()
    {
        return $this->alerteQuestionEntreprise;
    }

    /**
     * Set actif.
     *
     * @param string $actif
     * @return Agent
     */
    public function setActif($actif)
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * Get actif.
     *
     * @return string
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * Set codesNuts.
     *
     * @param string $codesNuts
     * @return Agent
     */
    public function setCodesNuts($codesNuts)
    {
        $this->codesNuts = $codesNuts;

        return $this;
    }

    /**
     * Get codesNuts.
     *
     * @return string
     */
    public function getCodesNuts()
    {
        return $this->codesNuts;
    }

    /**
     * Set numCertificat.
     *
     * @param string $numCertificat
     * @return Agent
     */
    public function setNumCertificat($numCertificat)
    {
        $this->numCertificat = $numCertificat;

        return $this;
    }

    /**
     * Get numCertificat.
     *
     * @return string
     */
    public function getNumCertificat()
    {
        return $this->numCertificat;
    }

    /**
     * Set alerteValidationConsultation.
     *
     * @param string $alerteValidationConsultation
     * @return Agent
     */
    public function setAlerteValidationConsultation($alerteValidationConsultation)
    {
        $this->alerteValidationConsultation = $alerteValidationConsultation;

        return $this;
    }

    /**
     * Get alerteValidationConsultation.
     *
     * @return string
     */
    public function getAlerteValidationConsultation()
    {
        return $this->alerteValidationConsultation;
    }

    /**
     * Set alerteChorus.
     *
     * @param string $alerteChorus
     * @return Agent
     */
    public function setAlerteChorus($alerteChorus)
    {
        $this->alerteChorus = $alerteChorus;

        return $this;
    }

    /**
     * Get alerteChorus.
     *
     * @return string
     */
    public function getAlerteChorus()
    {
        return $this->alerteChorus;
    }

    /**
     * Set password.
     *
     * @param string $password
     * @return Agent
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set codeTheme.
     *
     * @param string $codeTheme
     * @return Agent
     */
    public function setCodeTheme($codeTheme)
    {
        $this->codeTheme = $codeTheme;

        return $this;
    }

    /**
     * Get codeTheme.
     *
     * @return string
     */
    public function getCodeTheme()
    {
        return $this->codeTheme;
    }

    public function setOrganisme(Organisme $organisme = null)
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getOrganisme(): ?Organisme
    {
        return $this->organisme;
    }

    /**
     * @return array
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getRoles()
    {
        return ['ROLE_AGENT'];
    }

    /**
     * @return string
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getUsername()
    {
        return $this->getLogin();
    }

    /**
     * @return string
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function eraseCredentials()
    {
        $this->mot_de_passe = '';
    }

    /**
     * @return bool
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function equals(UserInterface $user)
    {
        return $this->isEqualTo($user);
    }

    /**
     * @return bool
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function isEqualTo(UserInterface $user)
    {
        return $user->getId() == $this->getId() && $user->getUsername() == $this->getUsername();
    }

    /**
     * @return array
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function getHabilitations()
    {
        return $this->habilitations;
    }

    /**
     * @param $habilitation
     * @author Oumar KONATE <oumar.konate@atexo.com>
     */
    public function addHabilitation($habilitation)
    {
        $this->habilitations[] = $habilitation;
    }

    public function getSocleHabilitations(): ?SocleHabilitationAgent
    {
        return $this->socleHabilitations;
    }

    public function setSocleHabilitations(?SocleHabilitationAgent $socleHabilitations): Agent
    {
        $this->socleHabilitations = $socleHabilitations;
        return $this;
    }

    /**
     * @param ModificationContrat $modificationContrat
     *
     * @return $this
     */
    public function addModificationContrat($modificationContrat)
    {
        $this->modificationContrats[] = $modificationContrat;

        return $this;
    }

    /**
     * @param ModificationContrat $modificationContrat
     */
    public function removeModificationContrat($modificationContrat)
    {
        $this->modificationContrats->removeElement($modificationContrat);
    }

    /**
     * @return ArrayCollection
     */
    public function getModificationContrats()
    {
        return $this->modificationContrats;
    }

    public function getService(): ?Service
    {
        return $this->service;
    }

    /**
     * @param Service $service
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * @return string*/
    #[Groups('webservice')]
    public function getIdentifiant()
    {
        return $this->login;
    }

    #[Groups('webservice')]
    public function setIdentifiant(string $login)
    {
        $this->login = $login;
    }

    /**
     * @return mixed
     */
    public function getHabilitation()
    {
        return $this->habilitation;
    }

    /**
     * @param mixed $habilitation
     */
    public function setHabilitation($habilitation)
    {
        $this->habilitation = $habilitation;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateConnexion()
    {
        return $this->dateConnexion;
    }

    public function setDateConnexion(DateTime|null $dateConnexion = null)
    {
        $this->dateConnexion = $dateConnexion;
    }

    /**
     * @return string
     */
    public function getTypeHash()
    {
        return $this->typeHash;
    }

    public function setTypeHash(string $typeHash = 'SHA1')
    {
        $this->typeHash = $typeHash;

        return $this;
    }

    /**
     * Cette fonction permet de déplacer l'Agent en lui attribuant un nouveau service et un nouvel
     * organisme de destination passés en paramètres
     * Il garde l'ancien agent en BD mais le désactive et renomme le nom, le prénom, le login
     * et l'email en les préfixant par 'OLD_'.
     *
     * @param int    $idServiceDestination identifiant du service de destination
     * @param string $organismeDestination acronyme de l'organisme de destination dans lequel se trouve le clone
     *
     * @return bool|Agent false si le clone n'a pas réussi, sinon le nouvel objet Agent créé
     *
     * @throws Exception
     */
    public function deplacer(int $idServiceDestination, string $organismeDestination): bool|\App\Entity\Agent
    {
        $criterias = ['organisme' => $organismeDestination, 'id' => $idServiceDestination];

        $serviceDestinationObject = $this->em->getRepository(Service::class)->findOneBy($criterias);
        $organismeDestinationObject = $this->em->getRepository(Organisme::class)
            ->findByAcronymeOrganisme($organismeDestination);

        if ($serviceDestinationObject instanceof Service && $organismeDestinationObject instanceof Organisme) {
            $nouveauAgent = clone $this;
            $nouveauAgent->setId($this->getMaxId() + 1);
            $nouveauAgent->setService($serviceDestinationObject);
            $nouveauAgent->setOrganisme($organismeDestinationObject);
            $nouveauAgent->setHabilitation(null);

            $this->setNom('OLD_' . $this->getNom());
            $this->setPrenom('OLD_' . $this->getPrenom());
            $this->setLogin('OLD_' . $this->getLogin());
            $this->setEmail('OLD_' . $this->getEmail());
            $this->setTentativesMdp(3);

            $this->em->persist($this);
            $this->em->flush();

            $this->em->persist($nouveauAgent);
            $this->em->flush();

            $habilitationAgent = $this->em->getRepository(HabilitationAgent::class)
                ->findOneBy(
                    ['agent' => $this->getId()]
                );
            if ($habilitationAgent) {
                $newHabilitationAgent = $habilitationAgent->copier($nouveauAgent->getId());
                $nouveauAgent->setHabilitation($newHabilitationAgent);
                $this->em->persist($nouveauAgent);
                $this->em->flush();
            }

            $listAgentServiceMetier = $this->em->getRepository(AgentServiceMetier::class)
                ->findBy(
                    ['idAgent' => $this->getId()]
                );
            foreach ($listAgentServiceMetier as $agentServiceMetier) {
                if ($agentServiceMetier instanceof AgentServiceMetier) {
                    $agentServiceMetier->copier($nouveauAgent->getId());
                }
            }
            $serviceDestinationObject = null;
            $organismeDestinationObject = null;
            $habilitationAgent = null;
            $newHabilitationAgent = null;
            $listAgentServiceMetier = null;

            unset($serviceDestinationObject);
            unset($organismeDestinationObject);
            unset($habilitationAgent);
            unset($newHabilitationAgent);
            unset($listAgentServiceMetier);

            return $nouveauAgent;
        }

        return false;
    }

    public function getMaxId()
    {
        $maxId = $this->em->getRepository(Agent::class)->findBy([], ['id' => 'DESC'], 1, 0)[0];

        return $maxId->getId();
    }

    public function isTechnique(): bool
    {
        return $this->technique;
    }

    /**
     * @return Agent $this
     */
    public function setTechnique(bool $technique)
    {
        $this->technique = $technique;

        return $this;
    }

    /**
     * method qui switch.
     */
    public function switchActif()
    {
        if ('1' === $this->actif) {
            $this->setActif(0);
        } else {
            $this->setActif(1);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWebservices()
    {
        return $this->webservices;
    }

    public function addWebservice(WebService $webservice)
    {
        if (!$this->webservices->contains($webservice)) {
            $this->webservices[] = $webservice;
            $webservice->addAgent($this);
        }

        return $this;
    }

    public function removeWebservice(WebService $webservice)
    {
        if ($this->webservices->removeElement($webservice)) {
            $webservice->removeAgent($this);
        }

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * @return ArrayCollection
     */
    public function getDossiersVolumineux()
    {
        return $this->dossierVolumineux;
    }

    public function addDossierVolumineux(DossierVolumineux $dossierVolumineux)
    {
        $this->dossierVolumineux->add($dossierVolumineux);
        $dossierVolumineux->setAgent($this);
    }

    /**
     * @return ArrayCollection
     */
    public function getConsultationFavoris()
    {
        return $this->consultationFavoris;
    }

    public function addConsultationFavoris(ConsultationFavoris $consultationFavoris)
    {
        $this->consultationFavoris->add($consultationFavoris);

        $consultationFavoris->setAgent($this);
    }

    public function getDossierVolumineux(): Collection
    {
        return $this->dossierVolumineux;
    }

    public function setDossierVolumineux(Collection $dossierVolumineux): Agent
    {
        $this->dossierVolumineux = $dossierVolumineux;

        return $this;
    }

    public function getAlerteMesConsultations(): string
    {
        return $this->alerteMesConsultations;
    }

    public function setAlerteMesConsultations(string $alerteMesConsultations): Agent
    {
        $this->alerteMesConsultations = $alerteMesConsultations;

        return $this;
    }

    public function getAlerteConsultationsMonEntite(): string
    {
        return $this->alerteConsultationsMonEntite;
    }

    public function setAlerteConsultationsMonEntite(string $alerteConsultationsMonEntite): Agent
    {
        $this->alerteConsultationsMonEntite = $alerteConsultationsMonEntite;

        return $this;
    }

    public function getAlerteConsultationsDesEntitesDependantes(): string
    {
        return $this->alerteConsultationsDesEntitesDependantes;
    }

    public function setAlerteConsultationsDesEntitesDependantes(string $alerteConsultationsDesEntitesDependantes): Agent
    {
        $this->alerteConsultationsDesEntitesDependantes = $alerteConsultationsDesEntitesDependantes;

        return $this;
    }

    public function getAlerteConsultationsMesEntitesTransverses(): string
    {
        return $this->alerteConsultationsMesEntitesTransverses;
    }

    public function setAlerteConsultationsMesEntitesTransverses(string $alerteConsultationsMesEntitesTransverses): Agent
    {
        $this->alerteConsultationsMesEntitesTransverses = $alerteConsultationsMesEntitesTransverses;

        return $this;
    }

    public function getAlerteConsultationsInvitePonctuel(): string
    {
        return $this->alerteConsultationsInvitePonctuel;
    }

    public function setAlerteConsultationsInvitePonctuel(string $alerteConsultationsInvitePonctuel): Agent
    {
        $this->alerteConsultationsInvitePonctuel = $alerteConsultationsInvitePonctuel;

        return $this;
    }

    public function getDescriptionDetails(): string
    {
        $serviceLabel =
            $this->service
            ? sprintf("(%s - %s)", $this->service->getSigle(), $this->service->getLibelle())
            : ''
        ;

        return sprintf("%s %s %s", $this->prenom, $this->nom, $serviceLabel);
    }

    public function __serialize(): array
    {
        return [$this->getId(), $this->getUsername()];
    }

    public function __unserialize(array $serialized): void
    {
        $this->id = $serialized[0];
        $this->login = $serialized[1];
    }

    public function setEntityManager(EntityManager $entityManager): void
    {
        $this->em = $entityManager;
    }

    /**
     * @return Collection<int, HabilitationTypeProcedure>
     */
    public function getHabilitationTypeProcedures(): Collection
    {
        return $this->habilitationTypeProcedures;
    }

    public function setHabilitationTypeProcedures(Collection $habilitationsTypeProcedures): static
    {
        $this->habilitationTypeProcedures = $habilitationsTypeProcedures;

        return $this;
    }

    public function addHabilitationTypeProcedure(HabilitationTypeProcedure $habilitationTypeProcedure): static
    {
        if (!$this->habilitationTypeProcedures->contains($habilitationTypeProcedure)) {
            $this->habilitationTypeProcedures->add($habilitationTypeProcedure);
            $habilitationTypeProcedure->setAgent($this);
        }

        return $this;
    }

    public function removeHabilitationTypeProcedure(HabilitationTypeProcedure $habilitationTypeProcedure): static
    {
        if ($this->habilitationTypeProcedures->removeElement($habilitationTypeProcedure)) {
            // set the owning side to null (unless already changed)
            if ($habilitationTypeProcedure->getAgent() === $this) {
                $habilitationTypeProcedure->setAgent(null);
            }
        }

        return $this;
    }
}
