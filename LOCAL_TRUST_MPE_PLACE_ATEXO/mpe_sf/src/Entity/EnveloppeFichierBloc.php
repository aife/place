<?php

namespace App\Entity;

use App\Entity\BloborganismeFile;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EnveloppeFichierBlocRepository;

/**
 * EnveloppeFichierBloc.
 *
 * @ORM\Table(name="blocFichierEnveloppe", indexes={@ORM\Index(name="id_fichier", columns={"id_fichier"}),
 *     @ORM\Index(name="blocFichierEnveloppe_fichierEnveloppe", columns={"id_fichier", "organisme"})})
 * @ORM\Entity(repositoryClass=EnveloppeFichierBlocRepository::class)
 */
class EnveloppeFichierBloc
{
    /**
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\EnveloppeFichier", inversedBy="blocsFichiersEnveloppes", fetch="EAGER")
     * @ORM\JoinColumn(name="id_fichier", referencedColumnName="id_fichier")
     */
    private ?EnveloppeFichier $fichierEnveloppe = null;

    /**
     *
     * @ORM\Column(name="id_bloc_fichier", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?int $idBlocFichier = null;

    /**
     *
     * @ORM\Column(name="organisme", type="string", length=30)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private string $organisme = '';

    /**
     * @ORM\Column(name="id_fichier", type="integer", nullable=false)
     */
    private string|int $idFichier = '0';

    /**
     * @ORM\Column(name="numero_ordre_bloc", type="integer", nullable=false)
     */
    private string|int $numeroOrdreBloc = '0';

    /**
     * @ORM\Column(name="id_blob_chiffre", type="integer", nullable=false)
     */
    private string|int $idBlobChiffre = '0';

    /**
     * @ORM\Column(name="id_blob_dechiffre", type="integer", nullable=true)
     */
    private ?int $idBlobDechiffre = null;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\BloborganismeFile")
     * @ORM\JoinColumn(name="id_blob_chiffre", referencedColumnName="id")
     */
    private ?BloborganismeFile $blobOrganismeChiffre = null;

    /**
     * Set idBlocFichier.
     *
     * @param int $idBlocFichier
     *
     * @return EnveloppeFichierBloc
     */
    public function setIdBlocFichier($idBlocFichier)
    {
        $this->idBlocFichier = $idBlocFichier;

        return $this;
    }

    /**
     * Get idBlocFichier.
     *
     * @return int
     */
    public function getIdBlocFichier()
    {
        return $this->idBlocFichier;
    }

    /**
     * Set organisme.
     *
     * @param string $organisme
     *
     * @return EnveloppeFichierBloc
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;

        return $this;
    }

    /**
     * Get organisme.
     *
     * @return string
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Set idFichier.
     *
     * @param int $idFichier
     *
     * @return EnveloppeFichierBloc
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;

        return $this;
    }

    /**
     * Get idFichier.
     *
     * @return int
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * Set numeroOrdreBloc.
     *
     * @param int $numeroOrdreBloc
     *
     * @return EnveloppeFichierBloc
     */
    public function setNumeroOrdreBloc($numeroOrdreBloc)
    {
        $this->numeroOrdreBloc = $numeroOrdreBloc;

        return $this;
    }

    /**
     * Get numeroOrdreBloc.
     *
     * @return int
     */
    public function getNumeroOrdreBloc()
    {
        return $this->numeroOrdreBloc;
    }

    /**
     * Set idBlobChiffre.
     *
     * @param int $idBlobChiffre
     *
     * @return EnveloppeFichierBloc
     */
    public function setIdBlobChiffre($idBlobChiffre)
    {
        $this->idBlobChiffre = $idBlobChiffre;

        return $this;
    }

    /**
     * Get idBlobChiffre.
     *
     * @return int
     */
    public function getIdBlobChiffre()
    {
        return $this->idBlobChiffre;
    }

    /**
     * Set idBlobDechiffre.
     *
     * @param int $idBlobDechiffre
     *
     * @return EnveloppeFichierBloc
     */
    public function setIdBlobDechiffre($idBlobDechiffre)
    {
        $this->idBlobDechiffre = $idBlobDechiffre;

        return $this;
    }

    /**
     * Get idBlobDechiffre.
     *
     * @return int
     */
    public function getIdBlobDechiffre()
    {
        return $this->idBlobDechiffre;
    }

    public function setFichierEnveloppe(EnveloppeFichier $fichierenveloppe)
    {
        $this->fichierEnveloppe = $fichierenveloppe;
    }

    /**
     * @return EnveloppeFichier
     */
    public function getFichierEnveloppe()
    {
        return $this->fichierEnveloppe;
    }

    public function getBlobOrganismeChiffre(): ?BloborganismeFile
    {
        return $this->blobOrganismeChiffre;
    }

    public function setBlobOrganismeChiffre(?BloborganismeFile $blobOrganismeChiffre): EnveloppeFichierBloc
    {
        $this->blobOrganismeChiffre = $blobOrganismeChiffre;

        return $this;
    }
}
