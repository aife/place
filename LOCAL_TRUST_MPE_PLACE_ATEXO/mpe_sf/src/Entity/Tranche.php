<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Dto\Input\TrancheInput;
use App\Dto\Output\TrancheOutput;
use App\Entity\Consultation\DonneeComplementaire;
use App\Repository\TrancheRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TrancheRepository::class)
 *  @ORM\Table(name="t_tranche")
 *  @ORM\Entity
 */
#[ApiResource(
    collectionOperations: [
        'get' => ['output' => TrancheOutput::class],
        'post' => ['input' => TrancheInput::class, 'output' => TrancheOutput::class],
    ],
    itemOperations: ['get' => ['output' => TrancheOutput::class]],
)]
class Tranche
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $idTranche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?string $IdFormePrix;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Consultation\DonneeComplementaire", inversedBy="tranches", fetch="EAGER")
     * @ORM\JoinColumn(name="id_donnee_complementaire", referencedColumnName="id_donnee_complementaire")
     */
    private ?DonneeComplementaire $idDonneeComplementaire;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private string $natureTranche;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private int $codeTranche;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private string $intituleTranche;

    public function getIdTranche(): ?int
    {
        return $this->idTranche;
    }

    public function getIdFormePrix(): ?int
    {
        return $this->IdFormePrix;
    }

    public function setIdFormePrix(?int $IdFormePrix): self
    {
        $this->IdFormePrix = $IdFormePrix;

        return $this;
    }

    public function getIdDonneeComplementaire(): ?DonneeComplementaire
    {
        return $this->idDonneeComplementaire;
    }

    public function setIdDonneeComplementaire(?DonneeComplementaire $idDonneeComplementaire): self
    {
        $this->idDonneeComplementaire = $idDonneeComplementaire;

        return $this;
    }

    public function getNatureTranche(): ?string
    {
        return $this->natureTranche;
    }

    public function setNatureTranche(?string $natureTranche): self
    {
        $this->natureTranche = $natureTranche;

        return $this;
    }

    public function getCodeTranche(): ?int
    {
        return $this->codeTranche;
    }

    public function setCodeTranche(?int $codeTranche): self
    {
        $this->codeTranche = $codeTranche;

        return $this;
    }

    public function getIntituleTranche(): ?string
    {
        return $this->intituleTranche;
    }

    public function setIntituleTranche(string $intituleTranche): self
    {
        $this->intituleTranche = $intituleTranche;

        return $this;
    }
}
