<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoriesConsiderationsSociales.
 *
 * @ORM\Table(name="categories_considerations_sociales")
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesConsiderationsSocialesRepository")
 */
class CategoriesConsiderationsSociales
{
    /**
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(name="label", type="string")
     */
    private ?string $label = null;

    /**
     * @ORM\Column(name="code", type="string")
     */
    private ?string $code = null;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return CategoriesConsiderationsSociales
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return CategoriesConsiderationsSociales
     */
    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }
}
