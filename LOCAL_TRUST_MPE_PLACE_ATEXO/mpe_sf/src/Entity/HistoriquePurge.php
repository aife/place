<?php

namespace App\Entity;

use App\Repository\HistoriquePurgeRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=HistoriquePurgeRepository::class)
 */
class HistoriquePurge
{
    final public const STATE_INITIAL = 0;
    final public const STATE_DELETED = 1;
    final public const STATE_ERROR_FILE_NOT_FOUND = 2;
    final public const STATE_READY_TO_DELETE = 4;
    final public const PURGE_STATE_ERROR = 3;
    final public const TYPE_DELETE_PHYSIQUE_CRYPTEE = 'DELETE-PHYSIQUE-FILE-OFFRE-CRYPTEE';
    final public const TYPE_DELETE_PHYSIQUE_DECRYPTEE = 'DELETE-PHYSIQUE-FILE-OFFRE-DECRYPTEE';
    final public const TYPE_DELETE_DCE = 'DELETE-PHYSIQUE-FILE-DCE';
    final public const DESCRIPTION_INITIAL = 'Suppression en cours...';
    final public const DESCRIPTION_DELETED_NOT_FOUND =
        "Le fichier %s ne peut pas être supprimé car non trouvé sur le file system" ;
    final public const DESCRIPTION_DELETED_OK = 'Le fichier %s a été supprimé';
    final public const DESCRIPTION_AWAIT_MODE_REAL = 'Le fichier %s peut être supprimé en mode réel (dry-run=false)';
    final public const DESCRIPTION_DELETED_KO = 'Erreur : Problème lors de la suppression du fichier %s';
    final public const FILE_NOT_DELETED = 0;
    final public const ORGANISME_ALL = 'all';

    final public const KEY_FILE_PATH = "filePath";
    final public const KEY_ID_BLOB_ORGANISME_FILE = "idBlobOrganismeFile";
    final public const KEY_TEXT = "informations";
    final public const KEY_CONSULTATION = "id_consultation";
    final public const KEY_CONSULTATION_REFERENCE = "reference_consultation";
    final public const KEY_ORGANISME = "organisme";
    final public const KEY_DATE_DECHIFFREMENT = "date_dechiffrement";
    final public const KEY_DATE_FIN = "date_fin";
    final public const KEY_ID_ENVELOPPE = "id_enveloppe";
    final public const KEY_ID_FICHIER_ENVELOPPE = "id_fichier_enveloppe";
    final public const KEY_CREATION_FICHIER = "date_creation_fichier";
    final public const KEY_UNTRUSTED_DATE = "untrusted_date";

    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private string $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $description;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $createdAt;

    /**
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime_immutable")
     */
    private DateTimeImmutable $updatedAt;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     */
    private bool $deleted = false;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $state = self::STATE_INITIAL;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $data = null;

    /**
     * @ORM\Column(name="id_bloc_fichier_enveloppe", type="integer")
     */
    private ?int $idBlocFichierEnveloppe = null;

    /**
     * @ORM\Column(name="organisme", type="string", length=30)
     */
    private ?string $organisme = null;

    /**
     * @ORM\OneToOne(targetEntity=EnveloppeFichier::class, inversedBy="historiquePurge")
     * @ORM\JoinColumn(name="id_fichier_enveloppe", referencedColumnName="id_fichier")
     */
    private ?EnveloppeFichier $fichierEnveloppe = null;

    /**
     * @ORM\OneToOne(targetEntity=DCE::class, inversedBy="historiquePurge")
     * @ORM\JoinColumn(name="id_dce", referencedColumnName="id")
     */
    private ?DCE $dce = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    public function setDeleted(bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getState(): int
    {
        return $this->state;
    }

    public function setState(int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getOrganisme(): ?string
    {
        return $this->organisme;
    }

    public function setOrganisme(?string $organisme): HistoriquePurge
    {
        $this->organisme = $organisme;

        return $this;
    }

    public function getIdBlocFichierEnveloppe(): ?int
    {
        return $this->idBlocFichierEnveloppe;
    }

    public function setIdBlocFichierEnveloppe(?int $idBlocFichierEnveloppe): HistoriquePurge
    {
        $this->idBlocFichierEnveloppe = $idBlocFichierEnveloppe;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(?string $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getFichierEnveloppe(): ?EnveloppeFichier
    {
        return $this->fichierEnveloppe;
    }

    public function setFichierEnveloppe(?EnveloppeFichier $fichierEnveloppe): self
    {
        $this->fichierEnveloppe = $fichierEnveloppe;

        return $this;
    }

    public function getDce(): ?DCE
    {
        return $this->dce;
    }

    public function setDce(?DCE $dce): self
    {
        $this->dce = $dce;

        return $this;
    }
}
